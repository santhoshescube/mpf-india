﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;


    public class clsTrayAlerts
    {
        private DataLayer ObjCon;
        ArrayList parameters;
        public clsTrayAlerts()
        {
            this.ObjCon = new DataLayer();
        }

        public string GetServerDate()
        {
            return Convert.ToString(ObjCon.ExecuteScalar("Select getdate()"));
        }

        public int GetAlertCount()
        {
            int iCount = 0;
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "UC"));
            DataTable dtCount = ObjCon.ExecuteDataTable("AlertExpiryInfoSummary", parameters);
            if (dtCount.Rows.Count >0)
                iCount = dtCount.Rows[0]["AlCount"].ToInt32();
           
            return iCount;
        }

        public DataTable GetAlerts()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "UTS"));
            return ObjCon.ExecuteDataTable("AlertExpiryInfoSummary", parameters);
        }

        public DataTable GetMailSmsSettings(string sAccType)
        {
            return ObjCon.ExecuteDataTable("Select IncomingServer,OutgoingServer,COALESCE(PortNumber,0) as PortNumber,username,password,EnableSsl from mailsettings where AccountType='" + sAccType + "'");

        }

        public DataTable GetAlertEmails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GAE"));
            return ObjCon.ExecuteDataTable("AlertExpiryInfoSummary", parameters);
        }

        public bool UpdateAlertHistory(int iUserId, int iCommonId, int iDocTypeId, bool iType)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", iType == true ? "UAS" : "UASS"));
            parameters.Add(new SqlParameter("@UserID", iUserId));
            parameters.Add(new SqlParameter("@CommonID", iCommonId));
            parameters.Add(new SqlParameter("@DocumentTypeID", iDocTypeId));
            ObjCon.ExecuteNonQuery("AlertExpiryInfoSummary", parameters);
            return true;
        }

        public bool SmsActive()
        {
            bool smsActive = false;
            DataTable dtSms = ObjCon.ExecuteDataTable("Select ConfigurationID from ConfigurationMaster where upper(ltrim(rtrim(ConfigurationItem)))='SMSALERT' and upper(ConfigurationValue)='YES'");
            if (dtSms.Rows.Count > 0)
                smsActive = true;
            return smsActive;
        }

        public DataTable GetAlertSmsMobile()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", "GASM"));
            return ObjCon.ExecuteDataTable("AlertExpiryInfoSummary", parameters);
        }

        public DataTable CompanyInfo()
        {
            return ObjCon.ExecuteDataTable("Select Top 1 CompanyName,POBox,Telephone from CompanyMaster");
        }
        public DataTable GetNotificationDetails()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 1));
            parameters.Add(new SqlParameter("@SourceID", 1));
            return ObjCon.ExecuteDataTable("spMailHistory", parameters);
        }
        public DataTable GetStaffLateInfo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 2));
            parameters.Add(new SqlParameter("@SourceID", 2));
            return ObjCon.ExecuteDataTable("spMailHistory", parameters);
        }
        public DataTable GetStaffConseqenceInfo()
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 3));
            parameters.Add(new SqlParameter("@SourceID", 2));
            return ObjCon.ExecuteDataTable("spMailHistory", parameters);
        }
        public bool UpdateMailHistory(int SourceID, long EmpID, long RefID, string MailIds, string Mailcontent)
        {
            parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 4));
            parameters.Add(new SqlParameter("@SourceID", SourceID));
            parameters.Add(new SqlParameter("@EmployeeID", EmpID));
            parameters.Add(new SqlParameter("@ReferenceID", RefID));
            parameters.Add(new SqlParameter("@MailIDs", MailIds));
            parameters.Add(new SqlParameter("@Mailcontent", Mailcontent));
            ObjCon.ExecuteNonQuery("spMailHistory", parameters);
            return true;
        }

        public DataTable GetDevices(string sLocation)
        {
            return ObjCon.ExecuteDataTable("SELECT DeviceID,Devicename,IPAddress,Port,CommKey,Model,Delay,TimeOut,DeviceNo,DeviceTypeID from PayAttendanceDeviceSettings where LocationID in( " + sLocation + " )");
        }

        public string GetLastSaveDate()
        {
            string sLastSavedate = "30 Aug 2014";
            try
            {
                ArrayList prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 13));
                DataTable dtDate = ObjCon.ExecuteDataTable("MSAtnFingerTechTransactions", prmAtnSummary);
                if (dtDate.Rows.Count > 0)
                {
                    sLastSavedate = Convert.ToString(dtDate.Rows[0]["LastSaveDate"]);
                }
            }
            catch (Exception)
            {
            }
            return sLastSavedate;
        }

        public long GetEmployeeInfo(string strWorkID, ref string strEmployeeName)
        {
            long intEmployeeID = 0;
            strEmployeeName = "";
            try
            {
                ArrayList prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 5));
                prmAtnSummary.Add(new SqlParameter("@WorkiD", strWorkID));
                DataTable dtEmp = ObjCon.ExecuteDataTable("MSAtnFingerTechTransactions", prmAtnSummary);
                if (dtEmp.Rows.Count > 0)
                {
                    intEmployeeID = dtEmp.Rows[0]["EmployeeID"].ToInt64();
                    strEmployeeName = Convert.ToString(dtEmp.Rows[0]["FirstName"]);
                }
            }
            catch (Exception)
            {
            }
            return intEmployeeID;
        }

        public bool SaveAllLogsFromDevice(int intDeviceID, long lEmployeeID, string strWorkID, string strAtnDate, string strAtnStatus)
        {
            ArrayList prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 6));
            prmAtnSummary.Add(new SqlParameter("@DeviceID", intDeviceID));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", lEmployeeID));
            prmAtnSummary.Add(new SqlParameter("@WorkiD", strWorkID));
            prmAtnSummary.Add(new SqlParameter("@AtnDate", strAtnDate));
            prmAtnSummary.Add(new SqlParameter("@Status", strAtnStatus));
            ObjCon.ExecuteNonQuery("MSAtnFingerTechTransactions", prmAtnSummary);
            return true;


        }

    }

