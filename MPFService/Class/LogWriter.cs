using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
/// <summary>
/// Summary description for LogWriter
/// </summary>
/// <summary>
	/// This class is used for writing logs and error logs
	/// </summary>
	/*
			Class:			LogWriter
			Created by:		Bejoy
			Created on:		08-Jul-2009
	*/
public class LogWriter
{
    public LogWriter()
    {
        
    }

    /// <summary>
    /// Writes the message to the application log file.
    /// </summary>
    /// <param name="message">The message to write.</param>
    /*
        Function:		WriteLog
        Created by:		Bejoy
        Created on:		08-Jul-2009
    */
    public void WriteLog(string message)
    {
        try
        {
            FileInfo objFl = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\MpfSerLgs.txt");
            bool bOverWrite = objFl.Length > 1000500 ? false : true;

            using (StreamWriter Writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\MpfSerLgs.txt", bOverWrite))
            {
                Writer.WriteLine("Date : " + DateTime.Now.ToString());
                Writer.WriteLine("Log : " + message);
                Writer.WriteLine("<------------------Log end------------------------>");
                Writer.WriteLine("");
                Writer.Flush();
                Writer.Close();
            }
        }
        catch (Exception)
        {
            return;
        }
    }
    


   
}