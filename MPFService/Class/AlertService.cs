﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.IO;
using Microsoft.VisualBasic;
using System.Threading;

/// <summary>
/// Created By : Bijoy
/// Purpose    : To send alerts in seperate thread
/// </summary>
public class AlertService
{
    private Thread MailThread;
    private LogWriter objLogwriter;
    private clsTrayAlerts objAlerts;
    private clsSendmail objMail;
    private string IsServer;
    private enum Machine
    {
        Server = 0,
        Client = 1
    }
    private Machine MachineType
    {
        get { return this.IsServer == "0" ? Machine.Server : Machine.Client; }
        set { this.IsServer = (value == Machine.Client) ? "1" : "0"; }
    }
    public void SendNotifications()
    {
        MailThread = new Thread(SendMails);
        MailThread.SetApartmentState(ApartmentState.STA);
        MailThread.IsBackground = true;
        MailThread.Start();
        MailThread.Join();
    }

    private bool InitializeSettings()
    {
        objLogwriter = new LogWriter();
        objMail = new clsSendmail();
        string mServerName = "";
        clsRegistry objReg = new clsRegistry();
        objReg.ReadFromRegistry("SOFTWARE\\Mypayserver", "Mypayservername", out mServerName);

        objReg.ReadFromRegistry("SOFTWARE\\Mindsoft\\MyProducts", "Mypayfriend", out this.IsServer);

        if (mServerName != "" && this.MachineType == Machine.Server )
        {
            MsDb.ConnectionString = "Data source=" + mServerName + ";Initial Catalog=GoldenAnchor;uid=msadmin;pwd=msSoft!234;Connect Timeout=0;";
            return true;
        }
        else
        {
            objLogwriter.WriteLog("No server found");
            return false;
        }
    }
    private void SendMails()
    {
        try
        {
            if (InitializeSettings() && objMail.IsInternet_Connected())
            {
                objAlerts = new clsTrayAlerts();
                string outserver = "";
                int portno = 0;
                string fUser = "";
                string fPwd = "";
                bool bSsl = false;
                bool bSend = false;
                DataTable dtMail = objAlerts.GetMailSmsSettings("Email");
                if (dtMail.Rows.Count > 0)
                {
                    outserver = Convert.ToString(dtMail.Rows[0]["OutgoingServer"]);
                    portno = dtMail.Rows[0]["PortNumber"].ToInt32();
                    fUser = Convert.ToString(dtMail.Rows[0]["username"]);
                    fPwd = Convert.ToString(dtMail.Rows[0]["password"]);
                    bSsl = Convert.ToBoolean(dtMail.Rows[0]["EnableSsl"]);

                    dtMail = objAlerts.GetAlertEmails();
                    foreach (DataRow drCur in dtMail.Rows)
                    {
                        if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["OfficialEmailID"]), "Expiry Alert Message", Convert.ToString(drCur["AlertMessage"]), bSsl))
                        {
                            objAlerts.UpdateAlertHistory(Convert.ToInt32(drCur["UserID"]), Convert.ToInt32(drCur["CommonID"]), Convert.ToInt32(drCur["DocumentTypeID"]), true);
                            bSend = true;
                        }
                    }
                    if (bSend)
                        objLogwriter.WriteLog("Alert emails sent successfully:mailservice");
                    else
                        objLogwriter.WriteLog("No alerts for this time:mailservice");

                    //rejoin alerts

                    string alertMessage = "";
                    bSend = false;
                    dtMail = objAlerts.GetNotificationDetails();
                    foreach (DataRow drCur in dtMail.Rows)
                    {
                        alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Rejoin Alerts</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Vacation Period </td><td>" + Convert.ToString(drCur["StartDate"]) + " - " + Convert.ToString(drCur["EndDate"]) + "</td></tr>" +
                                            "<tr><td><font color=red>Rejoin Date </font></td><td><font color=red><b>" + Convert.ToString(drCur["ReJoinDate"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["OfficialEmailID"]) + "," + Convert.ToString(drCur["HREmailID"]), "Rejoin Alerts", alertMessage, bSsl))
                        {
                            objAlerts.UpdateMailHistory(1, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["ReferenceID"]), Convert.ToString(drCur["OfficialEmailID"]) + "," + Convert.ToString(drCur["HREmailID"]), alertMessage);
                            bSend = true;
                        }

                    }
                    if (bSend)
                        objLogwriter.WriteLog("Rejoin alerts sent successfully:Mailservice");
                    else
                        objLogwriter.WriteLog("No Rejoin alerts for this time:Mailservice");

                    //staff Late alerts
                    bSend = false;
                    dtMail = objAlerts.GetStaffLateInfo();
                    foreach (DataRow drCur in dtMail.Rows)
                    {
                        alertMessage = "";
                        if (Convert.ToInt32(drCur["Late"]) > 0) // late
                        {
                            alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Shift Time </td><td>" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "</td></tr>" +
                                            "<tr><td>Punch In </td><td>" + Convert.ToString(drCur["FirstPunch"]) + "</td></tr>" +
                                            "<tr><td>Late by </td><td>" + Convert.ToString(drCur["Late"]) + " minute(s)</td></tr>" +
                                            "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["LateConseq"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        }
                        if (alertMessage != "")
                        {
                            if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["MailID"]), "Consequence Info", alertMessage, bSsl))
                            {
                                objAlerts.UpdateMailHistory(2, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["EmployeeID"]), Convert.ToString(drCur["MailID"]), alertMessage);
                                bSend = true;
                            }
                        }

                    }
                    if (bSend)
                        objLogwriter.WriteLog("Late Consequence emails sent successfully:Mailservice");
                    else
                        objLogwriter.WriteLog("No Late Consequence mails for this time:Mailservice");

                    //staff conseqance alerts
                    bSend = false;
                    dtMail = objAlerts.GetStaffConseqenceInfo();
                    foreach (DataRow drCur in dtMail.Rows)
                    {
                        alertMessage = "";
                        if (Convert.ToInt32(drCur["Early"]) > 0) // Early
                        {
                            alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Shift Time </td><td>" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "</td></tr>" +
                                            "<tr><td>Punch Out </td><td>" + Convert.ToString(drCur["LastPunch"]) + "</td></tr>" +
                                            "<tr><td>Early by </td><td>" + Convert.ToString(drCur["Early"]) + " minute(s)</td></tr>" +
                                            "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["EarlyConseq"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        }
                        else if (Convert.ToInt32(drCur["Shortage"]) < 0) // worktime
                        {
                            alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Shift Time </td><td>" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "</td></tr>" +
                                            "<tr><td>Punch In </td><td>" + Convert.ToString(drCur["FirstPunch"]) + "</td></tr>" +
                                            "<tr><td>Punch Out </td><td>" + Convert.ToString(drCur["LastPunch"]) + "</td></tr>" +
                                            "<tr><td>Shortage </td><td>" + Convert.ToString(drCur["Shortage"]) + " minute(s)</td></tr>" +
                                            "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["WorkConseq"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        }
                        if (alertMessage != "")
                        {
                            if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["MailID"]), "Consequence Info", alertMessage, bSsl))
                            {
                                objAlerts.UpdateMailHistory(2, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["EmployeeID"]), Convert.ToString(drCur["MailID"]), alertMessage);
                                bSend = true;
                            }
                        }
                    }
                    if (bSend)
                        objLogwriter.WriteLog("Consequence emails sent successfully:Mailservice");
                    else
                        objLogwriter.WriteLog("No Consequence mails for this time:Mailservice");


                }

                //if (objAlerts.SmsActive())
                //{
                //    string sUser = "";
                //    string sSmsUrl = "";
                //    dtMail = objAlerts.GetMailSmsSettings("Sms");
                //    if (dtMail.Rows.Count > 0)
                //    {
                //        sSmsUrl = Convert.ToString(dtMail.Rows[0]["IncomingServer"]);
                //        outserver = Convert.ToString(dtMail.Rows[0]["OutgoingServer"]);
                //        portno = Convert.ToInt32(dtMail.Rows[0]["PortNumber"]);
                //        fUser = Convert.ToString(dtMail.Rows[0]["username"]);
                //        fPwd = Convert.ToString(dtMail.Rows[0]["password"]);
                //        bSsl = Convert.ToBoolean(dtMail.Rows[0]["EnableSsl"]);

                //        if (fUser.IndexOf('@') != -1)
                //            sUser = fUser.Substring(0, fUser.IndexOf('@'));
                //        else
                //            sUser = fUser;

                //        bSend = false;
                //        dtMail = objAlerts.GetAlertSmsMobile();
                //        foreach (DataRow drCur in dtMail.Rows)
                //        {
                //            string toAddress = Convert.ToString(drCur["Mobile"]) + "@" + sSmsUrl;
                //            if (objMail.SendSmsMail(sUser, fPwd, outserver, portno, fUser, toAddress, "", Convert.ToString(drCur["AlertMessage"]), bSsl))
                //            {
                //                objAlerts.UpdateAlertHistory(Convert.ToInt32(drCur["UserID"]), Convert.ToInt32(drCur["CommonID"]), Convert.ToInt32(drCur["DocumentTypeID"]), false);
                //                bSend = true;
                //            }
                //        }

                //        if (bSend)
                //            objLogwriter.WriteLog("Daily alert Sms sent successfully:systemtray");
                //        else
                //            objLogwriter.WriteLog("No alerts for sending sms:systemtray");
                //    }
                //}
            }
            else
            {
                objLogwriter.WriteLog("No server connectivity");
            }
        }
        catch (Exception Ex)
        {
            objLogwriter.WriteLog("Err on SendEmailAlerts." + Ex.Message.ToString());
            MailThread.Abort();
        }
    }


    #region Dispose Members

    // Variable to track whether Dispose method has already been called.
    private bool disposed = false;

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    private void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (!this.disposed)
            {
                if (this.objLogwriter != null)
                    this.objLogwriter = null;
                if (this.objAlerts != null)
                    this.objAlerts = null;
                if (this.MailThread != null)
                    this.MailThread.Abort();
            }
        }
        this.disposed = true;
    }

    #endregion

}

