﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MPFService")]
[assembly: AssemblyDescription("MPFService")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mindsoft Technologies India Pvt Ltd")]
[assembly: AssemblyProduct("MPFService")]
[assembly: AssemblyCopyright("Copyright ©  2009 Mindsoft Technologies India Pvt Ltd")]
[assembly: AssemblyTrademark("Mindsoft")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c824a9a9-c03b-42a7-9097-8d39c590c393")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]
