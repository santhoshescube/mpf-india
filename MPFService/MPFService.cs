﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace MPFService
{
    /// <summary>
    /// Created By  : Bijoy George
    /// Purpose     : To send Mail alerts
    /// Created Date: 02 Dec 2014
    /// </summary>
    public partial class MPFService : ServiceBase
    {
        
        private System.Timers.Timer tmNotification = null;
        public MPFService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            new LogWriter().WriteLog("Service started");

            StartMailService();
        }
        
        private void StartMailService()
        {
            tmNotification = new System.Timers.Timer();
            this.tmNotification.Interval = Convert.ToDouble(12000); //timer intraval in milliseconds
            this.tmNotification.Elapsed += new System.Timers.ElapsedEventHandler(this.tmNotification_Tick);
            tmNotification.Enabled = true;
        }

        private void tmNotification_Tick(object sender, EventArgs e)
        {
            tmNotification.Enabled = false;

            new LogWriter().WriteLog("Starting Mail service");

            AlertService objNotiifcations = new AlertService();
            objNotiifcations.SendNotifications();
            objNotiifcations.Dispose();

            tmNotification.Interval = Convert.ToDouble(4255000);
            tmNotification.Enabled = true;
        }
       
        protected override void OnStop()
        {
            new LogWriter().WriteLog("Service stopped");
        }


    }
}
