﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


public class clsFiles
{
    public bool Checksysfile(string strPath)
    {
        string flName = Path.Combine(strPath, "ProjectSettings.dll");
        FileInfo objfile = new FileInfo(flName);
        if(objfile.Exists)
        {
            if (objfile.Length != 45568)
                return false;
            else
                return true;
        }
        else
            return false;
    }

    public bool CheckProductFile()
    {
        string strFileName = System.Environment.SystemDirectory + "\\pfm7oz3acb_xpc.dll";
        
        FileInfo fInfo = new FileInfo(strFileName);
        if (fInfo.Exists)
            return true;
        else
            return false;

    }

    public bool CheckValidProduct()
    {
        string strRegPath = "TechIntels29O";
        clsRegistry objRegistry = new clsRegistry();
        string strRegKey = "";
        if (objRegistry.ReadFromRegistry("SOFTWARE\\Microsoft\\" + strRegPath, "Sintelanpbmno", out strRegKey))
        {
            if (strRegKey != "")
                return true;
            else
                return false;
        }
        else
            return false;

    }

}

