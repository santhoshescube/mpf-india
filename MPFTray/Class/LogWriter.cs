using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Text;
/// <summary>
/// Summary description for LogWriter
/// </summary>
/// <summary>
	/// This class is used for writing logs and error logs
	/// </summary>
	/*
			Class:			LogWriter
			Created by:		Bejoy
			Created on:		08-Jul-2009
	*/
public class LogWriter
{
    private string LogFolderPath;//Path of the log folder.

    public LogWriter(string logPath)
    {
        this.LogFolderPath = logPath +"\\Logs\\";
    }

    /// <summary>
    /// Writes the message to the application log file.
    /// </summary>
    /// <param name="message">The message to write.</param>
    /*
        Function:		WriteLog
        Created by:		Bejoy
        Created on:		08-Jul-2009
    */
    public void WriteLog(string message)
    {
        try
        {
            if (!Directory.Exists(LogFolderPath))
                Directory.CreateDirectory(LogFolderPath);

            FileInfo objFl = new FileInfo(LogFolderPath + "TrayLgs.txt");
            bool bOverWrite = objFl.Length > 1048500 ? false : true;

            using (StreamWriter Writer = new StreamWriter(LogFolderPath + "TrayLgs.txt", bOverWrite))
            {
                Writer.WriteLine("Date : " + DateTime.Now.ToString());
                Writer.WriteLine("Log : " + message);
                Writer.WriteLine("<------------------Log end------------------------>");
                Writer.WriteLine("");
                Writer.Flush();
                Writer.Close();
            }
        }
        catch (Exception)
        {
            return;
        }
    }
    


   
}