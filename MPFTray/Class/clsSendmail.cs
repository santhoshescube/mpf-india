﻿using System;
using System.Data;
using System.Configuration;
using System.Net.Mail;
using System.Net;
using System.Runtime;
using System.Resources;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;


/// <summary>
/// Summary description for clsSendmail
/// Created by  : Bijoy George
/// Description : To Send Emails from client Side.
/// </summary>
public class clsSendmail
{
    [DllImport("wininet.dll", CharSet = CharSet.Unicode)]
    private static extern bool InternetGetConnectedState(ref InternetConnectionState lpdwFlags, int dwReserved);
    
    public enum InternetConnectionState : int
    {
        INTERNET_CONNECTION_MODEM = 0x1,
        INTERNET_CONNECTION_LAN = 0x2,
        INTERNET_CONNECTION_PROXY = 0x4,
        INTERNET_RAS_INSTALLED = 0x10,
        INTERNET_CONNECTION_OFFLINE = 0x20,
        INTERNET_CONNECTION_CONFIGURED = 0x40
    }

	public clsSendmail()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool SendMail(string host,int portno,string sFromId, string sFPwd, string toId, string subject, string sBody,bool bEnSsl)
    {
        try
        {
            SmtpClient mailClient = new SmtpClient();
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = new System.Net.NetworkCredential(sFromId, sFPwd);
            if (portno>0)
                mailClient.Port = portno;

            mailClient.Host = host;
            if (bEnSsl)
                mailClient.EnableSsl = true;

            MailMessage objMail = new MailMessage();
            objMail.From = new MailAddress(sFromId);
            objMail.To.Add(toId);
            objMail.IsBodyHtml = true;
            objMail.Priority = MailPriority.Normal;
            objMail.Subject = subject;
            objMail.Body = sBody;
            mailClient.Send(objMail);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }
    public bool SendSmsMail(string sUserName,string sPwd,string host, int iPortno, string fromId,  string toId, string subject, string sMailBody, bool bEnSsl)
    {
        try
        {
            SmtpClient mailClient = new SmtpClient();
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = new System.Net.NetworkCredential(sUserName, sPwd);
            
            if (iPortno > 0)
                mailClient.Port = iPortno;

            mailClient.Host = host;

            if (bEnSsl)
                mailClient.EnableSsl = true;

            MailMessage objMail = new MailMessage();
            objMail.From = new MailAddress(fromId);
            objMail.To.Add(toId);
            objMail.Subject = subject;
            objMail.Body = sMailBody;
            mailClient.Send(objMail);

            return true;
        }
        catch (Exception)
        {
            return false;
        }

    }
    private bool CheckNetStatus()
    {
        System.Uri Url = new System.Uri("http://www.microsoft.com"); 
        System.Net.WebRequest WebReq;
        System.Net.WebResponse Resp;
        WebReq = System.Net.WebRequest.Create(Url);
        try
        {
            Resp = WebReq.GetResponse();
            Resp.Close();
            WebReq = null;
            return true;
        }
        catch
        {
            WebReq = null;
            return false;
        } 


    }

    public bool IsUrlAvailable(string sUrl)
    {
        System.Uri Url = new System.Uri(sUrl);
        System.Net.WebRequest WebReq;
        System.Net.WebResponse Resp;
        WebReq = System.Net.WebRequest.Create(Url);
        try
        {
            Resp = WebReq.GetResponse();
            Resp.Close();
            WebReq = null;
            return true;
        }
        catch
        {
            WebReq = null;
            return false;
        }


    }

    public bool IsInternet_Connected()
    {
        try
        {
            InternetConnectionState flags = 0;
            bool isConnected = InternetGetConnectedState(ref flags, 0);
            bool isOffline = (flags & InternetConnectionState.INTERNET_CONNECTION_OFFLINE) != 0;
            if (isConnected)
            {
                if (isOffline)
                    return false;
                else
                {
                    if (CheckNetStatus())
                        return true;
                    else
                        return false;
                }
            }
            else
                return false;
           

        }
        catch (Exception)
        {
            return false;
        }

    }


    private string GetLocalIP()
    {
        string ipAddress = "";
        try
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                for (int i = 0; i < ipAddr.Length; i++)
                {
                    if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        if (!IPAddress.IsLoopback(ipAddr[i]))
                        {
                            ipAddress = ipAddr[i].ToString();
                            break;
                        }
                    }
                }
            }

            return ipAddress;
        }
        catch (Exception)
        {
            return ipAddress;
        }
    }

    private string GetExtIP()
    {
        string extIP = "";
        try
        {
            extIP = new WebClient().DownloadString("http://wanip.info/");
            extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
            extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
            return extIP;
        }
        catch (Exception)
        {
            return extIP;
        }
    }

   

}
