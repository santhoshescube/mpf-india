using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Management;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;
using System.Xml;
using System.Data.SqlClient;
using System.IO;
using DevComponents.DotNetBar;

namespace Balloon
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class frmMain : System.Windows.Forms.Form
	{
		private AlertCustom m_AlertOnLoad=null;
		private System.Windows.Forms.Timer timer1;
        private DevComponents.DotNetBar.BalloonTip balloonTipFocus;
        private DevComponents.DotNetBar.BalloonTip balloonTipHover;
        private GroupBox groupBox1;
        private System.ComponentModel.IContainer components;
        private NotifyIcon notifyIcon1;
        private LinkLabel lnkUpdates;
        private Label label1;
        private ComboBox cmbReminder;
        private Button btnCancel;
        private Button btnOk;
        private Timer tmAlerts;
        private Timer tmCheckUpdate;
        private Timer tmIntPopup;

        public bool mBnotify;
        private int mAlertcount;
        private string mFileName;
        private string mServerName;
        private string sUrlPath;
        private string IsServer;
        private enum Machine
        {
            Server = 0,
            Client = 1
        }
        private Machine MachineType
        {
            get { return this.IsServer == "0" ? Machine.Server : Machine.Client; }
            set { this.IsServer = (value == Machine.Client) ? "1" : "0"; }
        }
        private Timer tmDay;
        private LogWriter mObjLogs;
        private clsTrayAlerts mObjAlerts;
        clsSendmail objMail ;
		public frmMain()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//

            SetInitials();

		}

        private void SetInitials()
        {
            try
            {
                mObjLogs = new LogWriter(Application.StartupPath);
                mFileName = "";
                mBnotify = true;
                mAlertcount = 0;

                sUrlPath = "http://msg.escubetech.com/Updates/MPFV27/";

                this.MachineType = Machine.Client;

                mServerName = "";

                clsRegistry objReg = new clsRegistry();
                objReg.ReadFromRegistry("SOFTWARE\\ES3Tech\\MyProducts", "Mypayfriend", out this.IsServer);

                objReg.ReadFromRegistry("SOFTWARE\\Mypayserver", "Mypayservername", out mServerName);

                MsDb.ConnectionString = "Data source=" + mServerName + ";Initial Catalog=GoldenAnchor;uid=msadmin;pwd=msSoft!234;Connect Timeout=0;";

                mObjAlerts = new clsTrayAlerts();
                objMail = new clsSendmail();
                
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Err on Tray SetInitials." + Ex.Message.ToString());
            }
        }

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.balloonTipFocus = new DevComponents.DotNetBar.BalloonTip();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.balloonTipHover = new DevComponents.DotNetBar.BalloonTip();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.cmbReminder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lnkUpdates = new System.Windows.Forms.LinkLabel();
            this.tmAlerts = new System.Windows.Forms.Timer(this.components);
            this.tmCheckUpdate = new System.Windows.Forms.Timer(this.components);
            this.tmIntPopup = new System.Windows.Forms.Timer(this.components);
            this.tmDay = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "MPF Alerts";
            this.notifyIcon1.BalloonTipTitle = "MPF Alerts";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "MPF Alerts";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.Click += new System.EventHandler(this.notifyIcon1_Click);
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseClick);
            // 
            // balloonTipFocus
            // 
            this.balloonTipFocus.AlertAnimationDuration = 1000;
            this.balloonTipFocus.AutoCloseTimeOut = 3;
            this.balloonTipFocus.CaptionImage = ((System.Drawing.Image)(resources.GetObject("balloonTipFocus.CaptionImage")));
            this.balloonTipFocus.Enabled = false;
            this.balloonTipFocus.ShowBalloonOnFocus = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // balloonTipHover
            // 
            this.balloonTipHover.AlertAnimationDuration = 1000;
            this.balloonTipHover.AutoCloseTimeOut = 4;
            this.balloonTipHover.CaptionImage = ((System.Drawing.Image)(resources.GetObject("balloonTipHover.CaptionImage")));
            this.balloonTipHover.Enabled = false;
            this.balloonTipHover.ShowCloseButton = false;
            this.balloonTipHover.BalloonDisplaying += new System.EventHandler(this.balloonTipHover_BalloonDisplaying);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCancel);
            this.groupBox1.Controls.Add(this.btnOk);
            this.groupBox1.Controls.Add(this.cmbReminder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.lnkUpdates);
            this.groupBox1.Location = new System.Drawing.Point(2, -3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(122, 34);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(160, 71);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(78, 71);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 3;
            this.btnOk.Text = "&Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cmbReminder
            // 
            this.cmbReminder.FormattingEnabled = true;
            this.cmbReminder.Items.AddRange(new object[] {
            "Next Day",
            "Next Week",
            "Next Month"});
            this.cmbReminder.Location = new System.Drawing.Point(78, 44);
            this.cmbReminder.Name = "cmbReminder";
            this.cmbReminder.Size = new System.Drawing.Size(154, 21);
            this.cmbReminder.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Remind Me";
            // 
            // lnkUpdates
            // 
            this.lnkUpdates.AutoSize = true;
            this.lnkUpdates.Location = new System.Drawing.Point(7, 16);
            this.lnkUpdates.Name = "lnkUpdates";
            this.lnkUpdates.Size = new System.Drawing.Size(225, 13);
            this.lnkUpdates.TabIndex = 0;
            this.lnkUpdates.TabStop = true;
            this.lnkUpdates.Text = "Click Here To Install New Visa Friend Updates";
            this.lnkUpdates.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkUpdates_LinkClicked);
            // 
            // tmAlerts
            // 
            this.tmAlerts.Enabled = true;
            this.tmAlerts.Interval = 480000;
            this.tmAlerts.Tick += new System.EventHandler(this.tmAlerts_Tick);
            // 
            // tmCheckUpdate
            // 
            this.tmCheckUpdate.Enabled = true;
            this.tmCheckUpdate.Interval = 960000;
            this.tmCheckUpdate.Tick += new System.EventHandler(this.tmCheckUpdate_Tick);
            // 
            // tmIntPopup
            // 
            this.tmIntPopup.Enabled = true;
            this.tmIntPopup.Interval = 3600000;
            this.tmIntPopup.Tick += new System.EventHandler(this.tmIntPopup_Tick);
            // 
            // tmDay
            // 
            this.tmDay.Enabled = true;
            this.tmDay.Interval = 86400000;
            this.tmDay.Tick += new System.EventHandler(this.tmDay_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(0, 0);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.Opacity = 0;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
            if (!ThisProcessAlreadyExists())
            {
                Application.Run(new frmMain());
            }
		}
        
        private static bool ThisProcessAlreadyExists()
        {
            try
            {
                int count = 0;
                Process thisProcess = Process.GetCurrentProcess();

                Process[] pl = Process.GetProcessesByName("MPFTray");

                foreach (Process p in pl)
                {
                    if (thisProcess.ProcessName == p.ProcessName)
                    {
                        count++;
                    }
                }
                if (count > 1)
                    return true;
                else
                    return false;

            }
            catch (Exception)
            {
                return false;
            }
            
        }

        private bool CheckAppRunning() 
        {
            bool bappFlag = false;
            try
            {
                Process[] myProcess = Process.GetProcessesByName("MyPayfriend");
                foreach (Process p in myProcess)
                {
                    if (p.ProcessName.ToUpper().Trim() == "MyPayfriend".ToUpper().Trim())
                    {
                        bappFlag = true;
                    }
                }
                return bappFlag;
            }
            catch (Exception)
            {
                return bappFlag;
            }
        }

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			timer1.Stop();
			timer1.Enabled=false;
			ShowLoadAlert();
		}

		private void ShowLoadAlert()
		{
			m_AlertOnLoad=new AlertCustom();
            Rectangle r = Screen.GetWorkingArea(this);
            m_AlertOnLoad.Location = new Point(r.Right - m_AlertOnLoad.Width, r.Bottom - m_AlertOnLoad.Height);
            //m_AlertOnLoad.AutoClose = true;
            //m_AlertOnLoad.AutoCloseTimeOut = 15;
            m_AlertOnLoad.AlertAnimation = eAlertAnimation.BottomToTop;
            m_AlertOnLoad.AlertAnimationDuration = 10000;
            m_AlertOnLoad.Show(false);
            m_AlertOnLoad = null;
		}
      
		private void balloonTipHover_BalloonDisplaying(object sender, System.EventArgs e)
		{
			// BalloonTriggerControl property returns control that invoked balloon
			if(balloonTipHover.BalloonTriggerControl==groupBox1)
			{
				// BalloonControl is already prepared Balloon control that is just about to be displayed
				// Setting BalloonControl to null will cancel balloon display
				Point p=Control.MousePosition;
				// Adjust cursor position so cursor is below tip
				p.Offset(-balloonTipHover.BalloonControl.TipOffset,balloonTipHover.BalloonControl.TipLength+4);
				balloonTipHover.BalloonControl.Location=p;
			}
		}
        
        private void Form1_Load(object sender, EventArgs e)
        {
            if (CheckDbConnection() == false)
            {
                Application.Exit();
            }
            else
            {
                GetMyAlertCount();
                cmbReminder.SelectedIndex = 0;
            }
           
        }

        private bool CheckDbConnection()
        {
            try
            {
                string strDate = mObjAlerts.GetServerDate();
                return true;
            }
            catch (Exception Ex)
            {

                mObjLogs.WriteLog("Err on Tray testconnection failed." + Ex.Message.ToString());
                return false;
            }
        }

        private void GetMyAlertCount()
        {
            try
            {
                mAlertcount = mObjAlerts.GetAlertCount();
            }
            catch { }
        }

        public void notifyIcon1_Click(object sender, EventArgs e)
        {
            m_AlertOnLoad = new AlertCustom();
            m_AlertOnLoad.Close();
            m_AlertOnLoad.Dispose();
            m_AlertOnLoad = null;
            ShowLoadAlert();
        }

        private void tmCheckUpdate_Tick(object sender, EventArgs e)
        {
            try
            {
                if (objMail.IsInternet_Connected())
                {
                    tmCheckUpdate.Enabled = false;
                    if (ReadXupdateStats() == 1)
                    {
                        DownLoadPayUpdates();
                        tmCheckUpdate.Enabled = false;
                    }
                }

                StatusCheck();

            }
            catch (Exception)
            {
                return;
            }
        }

        private void StatusCheck()
        {
            try
            {
                clsFiles objFiles = new clsFiles();
                if (!objFiles.CheckProductFile() || !objFiles.Checksysfile(Application.StartupPath) || !objFiles.CheckValidProduct())
                {
                    notifyIcon1.Text = notifyIcon1.BalloonTipText = notifyIcon1.BalloonTipTitle = "You are using a cracked software of ES3 Technovations LLP.";
                   
                }
                else
                    notifyIcon1.Text = notifyIcon1.BalloonTipText = notifyIcon1.BalloonTipTitle = "MPF Alerts(Trial)";
            }
            catch (Exception)
            {
                notifyIcon1.Text = notifyIcon1.BalloonTipText = notifyIcon1.BalloonTipTitle = "You are using a cracked software of ES3 Technovations LLP.";
            }
        }

        private int ReadXupdateStats()
        {
            int iRet = 0;
            try
            {
                string strSerials = "";
                string xPath = sUrlPath + "Updatex.xml";

                if (objMail.IsUrlAvailable(sUrlPath))
                {
                    XmlDocument xDoc = new XmlDocument();
                    xDoc.Load(xPath);

                    XmlNodeList xLst = xDoc.GetElementsByTagName("pay");
                    if (xLst[0] != null)
                        mFileName = xLst[0].Attributes[0].Value;

                    XmlNode xn = xLst[0].SelectSingleNode("version");
                    if (xn != null && xn.Attributes[0].Value == "new")
                        strSerials = xn.InnerText;

                    xn = xLst[0].SelectSingleNode("location");
                    if (xn != null)
                        sUrlPath = xn.InnerText;

                    xDoc = null;

                    if (strSerials.Trim() != "")
                    {
                        string sLNo = "";
                        new clsRegistry().ReadFromRegistry("SOFTWARE\\ES3Tech\\EPeopleSlNo", "EPeopleSlNo", out sLNo);

                        if (strSerials.IndexOf(sLNo) != -1 || sLNo =="")
                            iRet = 1;
                    }
                }
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Unable to find update file server. " + Ex.Message.ToString());
                iRet = 0;
            }
            return iRet;
        }

        private bool DownLoadPayUpdates()
        {
            try
            {
                string sUrl = sUrlPath + mFileName;
                WebClient wbclient = new WebClient();
                wbclient.DownloadFileCompleted += new AsyncCompletedEventHandler(wbclient_DownloadFileCompleted);
                wbclient.DownloadFileAsync(new Uri(sUrl), Application.StartupPath + "\\" + mFileName);
                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Err on Tray DownLoadPayUpdates." + Ex.Message.ToString());
                return false;
            }

        }

        void wbclient_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            try
            {
                FileInfo fdInfo = new FileInfo(Application.StartupPath + "\\" + mFileName);
                if (fdInfo.Exists)
                {
                    System.Threading.Thread.Sleep(1000);
                    System.Diagnostics.Process Proc = new System.Diagnostics.Process();
                    Proc.StartInfo.FileName = Application.StartupPath + "\\" + mFileName;
                    Proc.Start();
                }

            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Err on Tray DocDownloadFileCompleted." + Ex.Message.ToString());
            }
        }

        private void lnkUpdates_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Opacity = 0;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
        }

        private bool SendEmailAlerts()
        {
            try
            {
                string outserver = "";
                int portno = 0;
                string fUser = "";
                string fPwd = "";
                bool bSsl = false;
                bool bSend = false;
                DataTable dtMail = mObjAlerts.GetMailSmsSettings("Email");
                if (dtMail.Rows.Count > 0)
                {
                    outserver = Convert.ToString(dtMail.Rows[0]["OutgoingServer"]);
                    portno = dtMail.Rows[0]["PortNumber"].ToInt32();
                    fUser = Convert.ToString(dtMail.Rows[0]["username"]);
                    fPwd = Convert.ToString(dtMail.Rows[0]["password"]);
                    bSsl = Convert.ToBoolean(dtMail.Rows[0]["EnableSsl"]);

                    dtMail = mObjAlerts.GetAlertEmails();
                    foreach (DataRow drCur in dtMail.Rows)
                    {
                        if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["OfficialEmailID"]), "Expiry Alert Message", Convert.ToString(drCur["AlertMessage"]), bSsl))
                        {
                            mObjAlerts.UpdateAlertHistory(Convert.ToInt32(drCur["UserID"]), Convert.ToInt32(drCur["CommonID"]), Convert.ToInt32(drCur["DocumentTypeID"]), true);
                            bSend = true;
                        }
                    }
                    if (bSend)
                        mObjLogs.WriteLog("Daily alert emails sent successfully:systemtray");
                    else
                        mObjLogs.WriteLog("No alerts for sending mails today:systemtray");
                }

                if (mObjAlerts.SmsActive())
                    SendSmsAlerts();

                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Err on Tray SendEmailAlerts." + Ex.Message.ToString());
                return false;
            }
        }

        private bool SendSmsAlerts()
        {
            try
            {
                string sSmsUrl = "";
                string outserver = "";
                int portno = 0;
                string sUser = "";
                string sFromId = "";
                string fPwd = "";
                bool bSsl = false;
                DataTable sdt = mObjAlerts.GetMailSmsSettings("Sms");
                if (sdt.Rows.Count > 0)
                {
                    sSmsUrl = Convert.ToString(sdt.Rows[0]["IncomingServer"]);
                    outserver = Convert.ToString(sdt.Rows[0]["OutgoingServer"]);
                    portno = sdt.Rows[0]["PortNumber"].ToInt32();
                    sFromId = Convert.ToString(sdt.Rows[0]["username"]);
                    fPwd = Convert.ToString(sdt.Rows[0]["password"]);
                    bSsl = Convert.ToBoolean(sdt.Rows[0]["EnableSsl"]);
                }
                else
                {
                    return false;
                }

                if (sFromId.IndexOf('@') != -1)
                    sUser = sFromId.Substring(0, sFromId.IndexOf('@'));
                else
                    sUser = sFromId;

                bool bSendSms = false;
                sdt = mObjAlerts.GetAlertSmsMobile();
                foreach (DataRow drCur in sdt.Rows)
                {
                    string toAddress = Convert.ToString(drCur["Mobile"]) + "@" + sSmsUrl;
                    if (objMail.SendSmsMail(sUser, fPwd, outserver, portno, sFromId, toAddress, "", Convert.ToString(drCur["AlertMessage"]), bSsl))
                    {
                        mObjAlerts.UpdateAlertHistory(Convert.ToInt32(drCur["UserID"]), Convert.ToInt32(drCur["CommonID"]), Convert.ToInt32(drCur["DocumentTypeID"]), false);
                        bSendSms = true;
                    }
                }

                if (bSendSms)
                    mObjLogs.WriteLog("Daily alert Sms sent successfully:systemtray");
                else
                    mObjLogs.WriteLog("No alerts for sending sms:systemtray");

                return true;
            }
            catch (Exception Ex)
            {
                mObjLogs.WriteLog("Err on Tray SendSmsAlerts." + Ex.Message.ToString());
                return false;
            }
        }

        private void tmAlerts_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.MachineType == Machine.Server && DateTime.Now.Hour > 6)
                {
                    if (SendEmailAlerts())
                    {
                        tmAlerts.Stop();
                        tmAlerts.Enabled = false;
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
            
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
           
        }

        private void SendConsequanceMail()
        {
            try
            {
                if (this.MachineType == Machine.Server && DateTime.Now.Hour >6)
                {
                    string outserver = "";
                    int portno = 0;
                    string fUser = "";
                    string fPwd = "";
                    bool bSsl = false;

                    DataTable sdt = mObjAlerts.GetMailSmsSettings("Email");
                    if (sdt.Rows.Count > 0)
                    {
                        outserver = Convert.ToString(sdt.Rows[0]["OutgoingServer"]);
                        portno = sdt.Rows[0]["PortNumber"].ToInt32();
                        fUser = Convert.ToString(sdt.Rows[0]["username"]);
                        fPwd = Convert.ToString(sdt.Rows[0]["password"]);
                        bSsl = Convert.ToBoolean(sdt.Rows[0]["EnableSsl"]);
                    }
                    else
                        return;

                    string alertMessage = "";
                    bool bSend = false;
                    sdt = mObjAlerts.GetNotificationDetails(); //rejoin alerts
                    foreach (DataRow drCur in sdt.Rows)
                    {
                        alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Rejoin Alerts</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Vacation Period </td><td>" + Convert.ToString(drCur["StartDate"]) + " - " + Convert.ToString(drCur["EndDate"]) + "</td></tr>" +
                                            "<tr><td><font color=red>Rejoin Date </font></td><td><font color=red><b>" + Convert.ToString(drCur["ReJoinDate"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["OfficialEmailID"]) + "," + Convert.ToString(drCur["HREmailID"]), "Rejoin Alerts", alertMessage, bSsl))
                        {
                            mObjAlerts.UpdateMailHistory(1, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["ReferenceID"]), Convert.ToString(drCur["OfficialEmailID"]) + "," + Convert.ToString(drCur["HREmailID"]), alertMessage);
                            bSend = true;
                        }

                    }
                    if (bSend)
                        mObjLogs.WriteLog("Rejoin alerts sent successfully:systemtray");
                    else
                        mObjLogs.WriteLog("No Rejoin alerts for this time:systemtray");

                    bSend = false;
                    sdt = mObjAlerts.GetStaffLateInfo(); //staff Late alerts
                    foreach (DataRow drCur in sdt.Rows)
                    {
                        alertMessage = "";
                        if (drCur["Late"].ToInt32() > 0) // late
                        {
                            alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Shift </td><td>" + Convert.ToString(drCur["ShiftName"]) + "[" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "]</td></tr>" +
                                            "<tr><td>Punch In </td><td>" + Convert.ToString(drCur["FirstPunch"]) + "</td></tr>" +
                                            "<tr><td>Late by </td><td>" + Convert.ToString(drCur["Late"]) + " minute(s)</td></tr>" +
                                            "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["LateConseq"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        }
                        if (alertMessage != "")
                        {
                            if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["MailID"]), "Consequence Info", alertMessage, bSsl))
                            {
                                mObjAlerts.UpdateMailHistory(2, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["EmployeeID"]), Convert.ToString(drCur["MailID"]), alertMessage);
                                bSend = true;
                            }
                        }

                    }
                    if (bSend)
                        mObjLogs.WriteLog("Daily Late Consequence emails sent successfully:systemtray");
                    else
                        mObjLogs.WriteLog("No Late Consequence mails for this time:systemtray");

                    bSend = false;
                    sdt = mObjAlerts.GetStaffConseqenceInfo(); //staff conseqance alerts
                    foreach (DataRow drCur in sdt.Rows)
                    {
                        alertMessage = "";
                        if (drCur["Early"].ToInt32() > 0 && drCur["ShiftType"].ToInt32() != 2) // Early
                        {
                            alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                                            "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                                            "<tr><td>Shift </td><td>" + Convert.ToString(drCur["ShiftName"]) + "[" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "]</td></tr>" +
                                            "<tr><td>Punch Out </td><td>" + Convert.ToString(drCur["LastPunch"]) + "</td></tr>" +
                                            "<tr><td>Early by </td><td>" + Convert.ToString(drCur["Early"]) + " minute(s)</td></tr>" +
                                            "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["EarlyConseq"]) + "</b></font></td></tr>" +
                                            "</body></html>";
                        }
                        //else if (drCur["Shortage"].ToInt32() < 0) // worktime
                        //{
                        //    alertMessage = "<html><body><table border=1 cellpadding=2 cellspacing=4><tr><td colspan=2 align=center><h3>Consequence Info</h3></td></tr>" +
                        //                    "<tr><td>Employee Name </td><td>" + Convert.ToString(drCur["EmployeeFullName"]) + "</td></tr>" +
                        //                    "<tr><td>Shift </td><td>" + Convert.ToString(drCur["ShiftName"]) + "[" + Convert.ToString(drCur["ShiftFT"]) + " - " + Convert.ToString(drCur["ShiftTT"]) + "]</td></tr>" +
                        //                    "<tr><td>Punch In </td><td>" + Convert.ToString(drCur["FirstPunch"]) + "</td></tr>" +
                        //                    "<tr><td>Punch Out </td><td>" + Convert.ToString(drCur["LastPunch"]) + "</td></tr>" +
                        //                    "<tr><td>Shortage </td><td>" + Convert.ToString(drCur["Shortage"]) + " minute(s)</td></tr>" +
                        //                    "<tr><td><font color=red>Remarks</font></td><td><font color=red><b>" + Convert.ToString(drCur["WorkConseq"]) + "</b></font></td></tr>" +
                        //                    "</body></html>";
                        //}
                        if (alertMessage != "")
                        {
                            if (objMail.SendMail(outserver, portno, fUser, fPwd, Convert.ToString(drCur["MailID"]), "Consequence Info", alertMessage, bSsl))
                            {
                                mObjAlerts.UpdateMailHistory(2, Convert.ToInt64(drCur["EmployeeID"]), Convert.ToInt64(drCur["EmployeeID"]), Convert.ToString(drCur["MailID"]), alertMessage);
                                bSend = true;
                            }
                        }
                    }
                    if (bSend)
                        mObjLogs.WriteLog("Daily Consequence emails sent successfully:systemtray");
                    else
                        mObjLogs.WriteLog("No Consequence mails for this time:systemtray");
                }

            }
            catch (Exception)
            {
                return;
            }
        }

        private bool CheckNewAlerts()
        {
            try
            {
                int iAlCount = mObjAlerts.GetAlertCount();
                if (mAlertcount != iAlCount)
                {
                    mAlertcount = iAlCount;
                    return true;
                }
                else
                    return false;

            }
            catch (Exception)
            {
                return false;
            }
        }

        private void tmIntPopup_Tick(object sender, EventArgs e)
        {
            try
            {
                SendConsequanceMail();

                if (CheckNewAlerts())
                {
                    m_AlertOnLoad = new AlertCustom();
                    m_AlertOnLoad.Close();
                    m_AlertOnLoad.Dispose();
                    m_AlertOnLoad = null;
                    ShowLoadAlert();

                    tmAlerts.Enabled = true;
                }
            }
            catch (Exception)
            {
                return;
            }
        }


        private void tmDay_Tick(object sender, EventArgs e)
        {
            tmAlerts.Enabled = true;
            tmCheckUpdate.Enabled = true;
        }
       


	}
}
