using System;
using System.Data;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text;

namespace Balloon
{
	/// <summary>
	/// Summary description for AlertCustom.
	/// </summary>
	public class AlertCustom : DevComponents.DotNetBar.Balloon
    {
        private DevComponents.DotNetBar.Bar bar1;
        private IContainer components;
        private DevComponents.DotNetBar.ButtonItem buttonItemExit;
        public bool updates;
        private Timer tmScrollAuto;
        private DevComponents.DotNetBar.PanelEx WindowsBack;
        private DevComponents.DotNetBar.LabelX lblData;
        private DevComponents.DotNetBar.Controls.ReflectionImage reflectionImage1;
        private PictureBox pctBallon;
        private DevComponents.DotNetBar.LabelX lblNoAlerts;
		public AlertCustom()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AlertCustom));
            this.bar1 = new DevComponents.DotNetBar.Bar();
            this.buttonItemExit = new DevComponents.DotNetBar.ButtonItem();
            this.tmScrollAuto = new System.Windows.Forms.Timer(this.components);
            this.WindowsBack = new DevComponents.DotNetBar.PanelEx();
            this.lblNoAlerts = new DevComponents.DotNetBar.LabelX();
            this.reflectionImage1 = new DevComponents.DotNetBar.Controls.ReflectionImage();
            this.lblData = new DevComponents.DotNetBar.LabelX();
            this.pctBallon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).BeginInit();
            this.WindowsBack.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBallon)).BeginInit();
            this.SuspendLayout();
            // 
            // bar1
            // 
            this.bar1.BackColor = System.Drawing.Color.Transparent;
            this.bar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bar1.Items.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.buttonItemExit});
            this.bar1.Location = new System.Drawing.Point(0, 217);
            this.bar1.Name = "bar1";
            this.bar1.Size = new System.Drawing.Size(253, 25);
            this.bar1.Stretch = true;
            this.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.bar1.TabIndex = 1;
            this.bar1.TabStop = false;
            this.bar1.Text = "bar1";
            // 
            // buttonItemExit
            // 
            this.buttonItemExit.Image = ((System.Drawing.Image)(resources.GetObject("buttonItemExit.Image")));
            this.buttonItemExit.Name = "buttonItemExit";
            this.buttonItemExit.Text = "Exit";
            this.buttonItemExit.Visible = false;
            this.buttonItemExit.Click += new System.EventHandler(this.buttonItemExit_Click);
            // 
            // tmScrollAuto
            // 
            this.tmScrollAuto.Interval = 1000;
            this.tmScrollAuto.Tick += new System.EventHandler(this.tmScrollAuto_Tick);
            // 
            // WindowsBack
            // 
            this.WindowsBack.AutoScroll = true;
            this.WindowsBack.CanvasColor = System.Drawing.Color.Transparent;
            this.WindowsBack.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.WindowsBack.Controls.Add(this.lblNoAlerts);
            this.WindowsBack.Controls.Add(this.reflectionImage1);
            this.WindowsBack.Controls.Add(this.lblData);
            this.WindowsBack.Location = new System.Drawing.Point(7, 59);
            this.WindowsBack.Name = "WindowsBack";
            this.WindowsBack.Size = new System.Drawing.Size(239, 150);
            this.WindowsBack.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.WindowsBack.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.WindowsBack.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.WindowsBack.Style.GradientAngle = 90;
            this.WindowsBack.TabIndex = 7;
            this.WindowsBack.Scroll += new System.Windows.Forms.ScrollEventHandler(this.WindowsBack_Scroll);
            // 
            // lblNoAlerts
            // 
            // 
            // 
            // 
            this.lblNoAlerts.BackgroundStyle.Class = "";
            this.lblNoAlerts.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblNoAlerts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoAlerts.Location = new System.Drawing.Point(85, 61);
            this.lblNoAlerts.Name = "lblNoAlerts";
            this.lblNoAlerts.Size = new System.Drawing.Size(114, 23);
            this.lblNoAlerts.TabIndex = 4;
            this.lblNoAlerts.Text = "No Alerts for Today!";
            this.lblNoAlerts.Visible = false;
            // 
            // reflectionImage1
            // 
            // 
            // 
            // 
            this.reflectionImage1.BackgroundStyle.Class = "";
            this.reflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.reflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.reflectionImage1.Image = ((System.Drawing.Image)(resources.GetObject("reflectionImage1.Image")));
            this.reflectionImage1.Location = new System.Drawing.Point(21, 24);
            this.reflectionImage1.Name = "reflectionImage1";
            this.reflectionImage1.Size = new System.Drawing.Size(71, 112);
            this.reflectionImage1.TabIndex = 3;
            this.reflectionImage1.Visible = false;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            // 
            // 
            // 
            this.lblData.BackgroundStyle.BackColorGradientAngle = 90;
            this.lblData.BackgroundStyle.Class = "";
            this.lblData.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.lblData.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.Location = new System.Drawing.Point(3, 1);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(0, 0);
            this.lblData.Style = DevComponents.DotNetBar.eDotNetBarStyle.Office2010;
            this.lblData.TabIndex = 0;
            // 
            // pctBallon
            // 
            this.pctBallon.BackColor = System.Drawing.Color.Transparent;
            this.pctBallon.BackgroundImage = global::MPFTray.Properties.Resources.Trayheader;
            this.pctBallon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pctBallon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pctBallon.Location = new System.Drawing.Point(12, 4);
            this.pctBallon.Name = "pctBallon";
            this.pctBallon.Size = new System.Drawing.Size(144, 55);
            this.pctBallon.TabIndex = 6;
            this.pctBallon.TabStop = false;
            this.pctBallon.Click += new System.EventHandler(this.pctBallon_Click);
            // 
            // AlertCustom
            // 
            this.AlertAnimationDuration = 1000;
            this.AutoClose = false;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.BorderColor = System.Drawing.Color.SteelBlue;
            this.CaptionFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ClientSize = new System.Drawing.Size(253, 242);
            this.Controls.Add(this.WindowsBack);
            this.Controls.Add(this.bar1);
            this.Controls.Add(this.pctBallon);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(55)))), ((int)(((byte)(114)))));
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "AlertCustom";
            this.TipOffset = 75;
            this.TipPosition = DevComponents.DotNetBar.eTipPosition.Bottom;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AlertCustom_Load);
            this.CloseButtonClick += new System.EventHandler(this.AlertCustom_CloseButtonClick);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AlertCustom_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.bar1)).EndInit();
            this.WindowsBack.ResumeLayout(false);
            this.WindowsBack.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pctBallon)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

        private void AlertCustom_Load(object sender, EventArgs e)
        {
            this.AutoClose = true;
            this.AutoCloseTimeOut = 15;
            FillSummaryGrid();
        }
		
       
        private void FillSummaryGrid()
        {
            try
            {
                clsTrayAlerts objAlert = new clsTrayAlerts();
                DataTable dtData = objAlert.GetAlerts();

                if (dtData.Rows.Count > 0)
                {
                    lblData.Visible = true;
                    lblNoAlerts.Visible = false;
                    reflectionImage1.Visible = false;
                    StringBuilder oString = new StringBuilder();
                    for (int i = 0; i < dtData.Rows.Count ; i++)
                    {
                        string sDes = Convert.ToString(dtData.Rows[i][1]).Remove(0, Convert.ToString(dtData.Rows[i][1]).IndexOf(" "));
                        if (sDes.IndexOf("(s)") > 15 && sDes.Length > 41)
                        {
                            string sFirst = sDes.Substring(0, 15) + "...";
                            sDes = sFirst + sDes.Remove(0, sDes.IndexOf("(s)")+3);
                        }
                        if (sDes.Length > 41)
                            sDes = sDes.Substring(0, 41) + "...";
                        if (Convert.ToString(dtData.Rows[i][2]) == "Y")
                            oString.Append("<br><font size='+4' color='black'><b>" + Convert.ToString(dtData.Rows[i][1]).Substring(0, Convert.ToString(dtData.Rows[i][1]).IndexOf(" ")) + "</b></font><font color='red'>" + sDes + "</font></br>");
                        else
                            oString.Append("<br><font size='+4' color='green'><b>" + Convert.ToString(dtData.Rows[i][1]).Substring(0, Convert.ToString(dtData.Rows[i][1]).IndexOf(" ")) + "</b></font>" + sDes + "</br>");
                    }
                    
                    lblData.Text = oString.ToString();
                }
                else
                {
                    lblData.Visible = false;
                    lblNoAlerts.Visible = true;
                    reflectionImage1.Visible = true;
                    //lblData.Text = "<br> No Alerts Today </br>";
                }
                

            }
            catch (Exception Ex)
            {
                new LogWriter(Application.StartupPath).WriteLog("Failed to FillSummaryGrid." + Ex.Message.ToString());
               
                return;
            }

        }
        
        private void AlertCustom_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                this.Dispose();
            }
            catch (Exception)
            {
                return;
            }
        }

        private void pctBallon_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://www.escubetech.com");
            }
            catch (Exception)
            {
                return;
            }
        }

        private void WindowsBack_Scroll(object sender, ScrollEventArgs e)
        {
            WindowsBack.Refresh();
            tmScrollAuto.Enabled = true;
        }
        private void AlertCustom_CloseButtonClick(object sender, EventArgs e)
        {
            this.AutoClose = true;
            this.AutoCloseTimeOut = 1;
        }

        private void tmScrollAuto_Tick(object sender, EventArgs e)
        {
            try
            {
                tmScrollAuto.Stop();
                tmScrollAuto.Enabled = false;
                this.AutoCloseTimeOut = 15;
            }
            catch (Exception)
            {
                return;
            }
        }

        private void buttonItemExit_Click(object sender, EventArgs e)
        {
            buttonItemExit.Enabled = false;
            this.AutoClose = true;
            this.AutoCloseTimeOut = 1;
        }

       

       
        
        
	}
}
