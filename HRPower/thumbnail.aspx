﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            System.Drawing.Image image;
            clsEmployee objEmployee;
            //string sRootPath = string.Empty;
            
            if (Request.QueryString["FromDB"] == "true")
            {
                byte[] buffer = null;

                switch (Request.QueryString["type"])
                {
                    case "Company":
                        clsCompany objCompany = new clsCompany();

                        objCompany.CompanyID = Convert.ToInt32(Request.QueryString["CompanyId"]);

                        buffer = objCompany.GetLogo();
                        
                        break;

                    case "EmployeeRecentPhoto":

                        buffer = new clsEmployeeNew().GetEmployeeRecentPhoto(Convert.ToInt32(Request.QueryString["EmployeeID"]));  


                        if (buffer == null)
                            buffer = System.IO.File.ReadAllBytes(Server.MapPath("~//images//Employee.jpg"));
                        break;

                    case "EmployeePassportPhoto":
                        objEmployee = new clsEmployee();

                        objEmployee.EmployeeID = Convert.ToInt32(Request.QueryString["EmployeeID"]);

                        buffer = objEmployee.GetEmployeePassportPhoto();
                        
                        if (buffer == null)
                            buffer = System.IO.File.ReadAllBytes(Server.MapPath("~//images//Employee.jpg"));
                        break;


                    case "EmployeeThumbnail":

                        buffer = new clsEmployeeNew().GetEmployeeThumbnail(Convert.ToInt32(Request.QueryString["EmployeeID"]));


                        if (buffer == null)
                            buffer = System.IO.File.ReadAllBytes(Server.MapPath("~//images//Employee.jpg"));
                        break;
  
                        
                }

                if (buffer == null)
                {
                    buffer = System.IO.File.ReadAllBytes(Server.MapPath("~/images/noimage.jpg"));
                }                    

                image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(buffer));
            }
            else
            {
                string sRootPath = string.Empty;

                switch (Request.QueryString["folder"])
                {
                    //case "candidatephotos":
                    //    sRootPath = Server.MapPath("~/documents/") + Request.QueryString["folder"] + "/";
                    //    break;
                    default:
                        sRootPath = Server.MapPath("~/Documents/") + Request.QueryString["folder"] + "/";

                        break;
                }
                if(Request.QueryString["isCandidate"]=="true")
                    image = System.Drawing.Image.FromFile(sRootPath + Request.QueryString["file"]);
                else
                    image = System.Drawing.Image.FromFile(sRootPath + "Recent" + Session.SessionID + ".jpg");
                    
              
              
            }

            //System.Drawing.Image image = System.Drawing.Image.FromFile(sRootPath + Request.QueryString["file"]);

            decimal width = image.Width, height = image.Height;

            if (Request.QueryString["height"] == null && Request.QueryString["width"] == null)
            {
                height = image.Height;
                width = image.Width;
            }
            else
            {

                if (Request.QueryString["height"] == null)
                {
                    if (image.Width > Convert.ToDecimal(Request.QueryString["width"]))
                    {
                        height = Decimal.Divide(Convert.ToDecimal(image.Height), Decimal.Divide(Convert.ToDecimal(image.Width), Convert.ToDecimal(Request.QueryString["width"])));
                        width = Convert.ToDecimal(Request.QueryString["width"]);
                    }
                }
                else if (Request.QueryString["width"] == null)
                {
                    if (image.Height > Convert.ToDecimal(Request.QueryString["height"]))
                    {
                        height = Convert.ToDecimal(Request.QueryString["height"]);
                        width = Decimal.Divide(Convert.ToDecimal(image.Width), Decimal.Divide(Convert.ToDecimal(image.Height), Convert.ToDecimal(Request.QueryString["height"])));
                    }
                }
                else
                {
                    height = Convert.ToDecimal(Request.QueryString["height"]);
                    width = Convert.ToDecimal(Request.QueryString["width"]);
                }
            }

            if (!(Request.QueryString["isCandidate"] == "true"))
            {
                width = 100;
                height = 125;
            }

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image, Convert.ToInt32(width), Convert.ToInt32(height));

            System.Drawing.Imaging.EncoderParameters encoderParams = new System.Drawing.Imaging.EncoderParameters();

            encoderParams.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100);

            System.Drawing.Imaging.ImageCodecInfo[] arr = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();

            System.Drawing.Imaging.ImageCodecInfo jpegICI = null;

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].FormatDescription.Equals("PNG"))
                {
                    jpegICI = arr[i];
                }
            }

            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            if (jpegICI != null)
            {
                bmp.Save(ms, jpegICI, encoderParams);
            }

            Response.ContentType = "image/jpeg";
            Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);

            image.Dispose();
            bmp.Dispose();
            ms.Dispose();

            Response.End();
            
            //decimal width = image.Width, height = image.Height;

            //if (Request.QueryString["height"] == null)
            //{
            //    height = Decimal.Divide(Convert.ToDecimal(image.Height), Decimal.Divide(Convert.ToDecimal(image.Width), Convert.ToDecimal(Request.QueryString["width"])));
            //    width = Convert.ToDecimal(Request.QueryString["width"]);
            //}
            //else if (Request.QueryString["width"] == null)
            //{
            //    height = Convert.ToDecimal(Request.QueryString["height"]);
            //    width = Decimal.Divide(Convert.ToDecimal(image.Width), Decimal.Divide(Convert.ToDecimal(image.Height), Convert.ToDecimal(Request.QueryString["height"])));
            //}
            //else
            //{
            //    height = Convert.ToDecimal(Request.QueryString["height"]);
            //    width = Convert.ToDecimal(Request.QueryString["width"]);
            //}            

            //System.Drawing.Image thumb = image.GetThumbnailImage(Convert.ToInt32(width), Convert.ToInt32(height), null, System.IntPtr.Zero);

            //System.IO.MemoryStream ms = new System.IO.MemoryStream();

            //thumb.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

            //Response.ContentType = "image/jpeg";
            //Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);

            //image.Dispose();
            //thumb.Dispose();
            //ms.Dispose();

            //Response.End();
        }
        catch(Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>    
    </div>
    </form>
</body>
</html>
