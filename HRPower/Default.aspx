﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>HRPower Login</title>

   
    <link href="css/default_login.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
     <script src="js/jsv1_11_1.js" type="text/javascript"></script>
    <script src="css/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
<div id="wrap" style="background:#fff;">
    <div class="container" id="header-wrapper">
        <div class="container" id="header">
            <div id="logo">
                <img src="images/companylogo.png" />
            </div>
            <div id="logo1">
                <img src="images/logohr power.png" />
            </div>
        </div>
    </div>
    <div class="container" style=" background: #fff; padding: 0px;margin: 0px;width: 90%;">
    <div class="container" id="page-wrapper" style="background: #fff;">
        <div class="row">
            <div id="container1" class="container1 col-lg-6 col-md-6 col-sm-6 col-xs-12" runat="server">
                <%--<div id="container1_1">
                    hgggggggggggg
                </div>--%>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 mrg-lgn pull-right">
                 <div class="col-lg-12 ">
                    <div id="divJob" class="pull-right" style="width:100%; margin-top: -5%; float:left;" runat="server">
                        <b style="padding-left: 7px; color:#333; margin-left:-20px;"><a href="JobOpenings.aspx">⇚
                        Current Openings </a></b>
                    </div>
                </div>
                
                <div class="row" id="wrapper" style="height: auto; padding-bottom: 10px;">   
                    <div id="login" class="animate form row" style="margin-top:28px;">
                        <div style="padding-top: 5px; width: 90%; float: left;">
                            <asp:Label ID="lblError" CssClass="error" runat="server" Style="display: block; color: red"></asp:Label></div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 19px;">
                            <%--<label for="username" class="uname" data-icon="" style="margin-right: 12px;">
                                username
                            </label>--%>
                            <asp:TextBox ID="txtUsername" name="username" required="required" type="text" placeholder="username "
                                runat="server"></asp:TextBox>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <%-- <label for="password" class="youpasswd" data-icon="" style="margin-right: 14px;">
                                password
                            </label>--%>
                            <asp:TextBox ID="txtPassword" name="password" required="required" type="password/"
                                placeholder="password" TextMode="Password" runat="server"></asp:TextBox>
                        </div>
                        <p class="col-lg-12" id="keeplogin" style="margin-top: 10px;">
                            <asp:CheckBox ID="chkStaySigned" runat="server" class="col-lg-1"/>
                            <label for="loginkeeping" class="col-lg-6" style="line-height:40px;">
                                Keep me logged in</label>
                        </p>
                        <div style="width: 99%;">
                            <div runat="server" id="divSelectLanguage" style="width: 60%; float: left;">
                                <div style="margin-left: 71px; padding-top: 16px;">
                                    <div id="dvCulture" runat="server">
                                        <asp:RadioButton ID="rbEnglish" runat="server" Text="English" Checked="True" GroupName="Lang"
                                            BackColor="Transparent" />
                                        <asp:RadioButton ID="rbArabic" runat="server" Text="العربية" GroupName="Lang" BackColor="Transparent" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <p class=" col-lg-4 pull-right login button">
                                    <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>    
    <div id="footerwrapper">
        <div id="footer">
             <p> <asp:Label ID ="hrSts" runat="server" text=""></asp:Label>
                 EPeople © 2009-21 Privacy policy All rights reserved. Powered by <a href="http://www.escubetech.com/">ES3 Technovations LLP</a>.
            </p>
        </div>
    </div>
    </form>
    </form>
</body>
</html>
