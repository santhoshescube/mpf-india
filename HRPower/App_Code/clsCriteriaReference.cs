﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsCriteriaReference
/// </summary>
public class clsCriteriaReference : DL
{
    public int CriteriaId { get; set; }
    public string DescriptionEng { get; set; }
    public string RemarksEng { get; set; }
    public string DescriptionArb { get; set; }
    public string RemarksArb { get; set; }

    public clsCriteriaReference()
    { }

    public void AddCriteriaReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "1"));
        parameters.Add(new SqlParameter("@Description", DescriptionEng ));
        parameters.Add(new SqlParameter("@RemarksEng", RemarksEng));
        parameters.Add(new SqlParameter("@DescriptionArb", DescriptionArb));
        parameters.Add(new SqlParameter("@RemarksArb", RemarksArb));

        ExecuteNonQuery("HRspCriteriaReference", parameters);
    }
    public void UpdateCriteriaReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "2"));
        parameters.Add(new SqlParameter("@Description", DescriptionEng));
        parameters.Add(new SqlParameter("@RemarksEng", RemarksEng));
        parameters.Add(new SqlParameter("@DescriptionArb", DescriptionArb));
        parameters.Add(new SqlParameter("@RemarksArb", RemarksArb));
        parameters.Add(new SqlParameter("@CriteriaId", CriteriaId));

        ExecuteNonQuery("HRspCriteriaReference", parameters);
    }
    public DataTable GetCriteria()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "3"));
        //parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspCriteriaReference", parameters);
    }

    public DataTable GetDataById()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "4"));
        parameters.Add(new SqlParameter("@CriteriaId", CriteriaId));
        //parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspCriteriaReference", parameters);
    }

    public void Delete()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "5"));
        parameters.Add(new SqlParameter("@CriteriaId", CriteriaId));
        ExecuteNonQuery("HRspCriteriaReference", parameters);
    }
    public int CheckDescriptionExistence()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "6"));
        if (CriteriaId > 0)
        {
            parameters.Add(new SqlParameter("@CriteriaId", CriteriaId));
            parameters.Add(new SqlParameter("@Description", DescriptionEng));
            parameters.Add(new SqlParameter("@DescriptionArb", DescriptionArb));
        }
        else
        {
            parameters.Add(new SqlParameter("@Description", DescriptionEng));
            parameters.Add(new SqlParameter("@DescriptionArb", DescriptionArb));
        }
        return Convert.ToInt32(ExecuteScalar("HRspCriteriaReference", parameters));
    }
    public int IsCriteriaUsed()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "7"));        
        parameters.Add(new SqlParameter("@CriteriaId", CriteriaId));
            
        return Convert.ToInt32(ExecuteScalar("HRspCriteriaReference", parameters));
    }
}
