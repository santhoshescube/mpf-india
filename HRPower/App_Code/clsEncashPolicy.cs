﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEncashPolicy
/// </summary>
public class clsEncashPolicy :DL 
{
	public clsEncashPolicy()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int EncashPolicyID { get; set; }
    public string Description { get; set; }
    public int CalculationBasedID { get; set; }
    public decimal CalculationPercentage { get; set; }
    public bool RateOnly { get; set; }
    public decimal EncashRate { get; set; }
    public int SalPolicyID { get; set; }
    public int AddDedID { get; set; }
    public int PolicyType { get; set; }
    public int CompanyBasedOn { get; set; }

    public DataTable FillAllCalculationBasedOn()                      // Select All calculation based on parameters
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAD"));
        return ExecuteDataTable("HRspEncashPolicy", arraylst);
    }

    public DataSet GetAllParticulars()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAAP"));
        return ExecuteDataSet("HRspEncashPolicy", arraylst);
    }


    public int InsertUpdateEncashPolicy(bool bIsInsert)                // Insertion when bIsInsert=true , Updation when bIsInsert=false
    {
        ArrayList arraylst = new ArrayList();

        if (bIsInsert)
            arraylst.Add(new SqlParameter("@Mode", "I"));
        else
        {
            arraylst.Add(new SqlParameter("@Mode", "U"));
            arraylst.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID ));
        }

        arraylst.Add(new SqlParameter("@Description", Description));
        arraylst.Add(new SqlParameter("@CalculationBasedID", CalculationBasedID));
        arraylst.Add(new SqlParameter("@CalculationPercentage", CalculationPercentage ));   
        arraylst.Add(new SqlParameter("@RateOnly", RateOnly));
        arraylst.Add(new SqlParameter("@IsCompanyBasedOn", CompanyBasedOn));
        arraylst.Add(new SqlParameter("@EncashRate", EncashRate ));
      


        return Convert.ToInt32(ExecuteScalar("HRspEncashPolicy", arraylst));
    }

    public int DeleteEncashPolicy()                                                // Deletion Of absent Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));

        try
        {
             return (Convert.ToInt32(ExecuteScalar("HRspEncashPolicy", arraylst)));
            
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet GetAllEncashPolicies()    // Getting all absent policies
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));
        return ExecuteDataSet("HRspEncashPolicy", alParameters);
    }
    public int InsertEncashSalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "ISP"));

        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@AddDedID", AddDedID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return Convert.ToInt32(ExecuteScalar("HRspEncashPolicy", alParameters));
    }

    public DataTable GetEncashSalaryIDS()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSP"));
        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return ExecuteDataTable("HRspEncashPolicy", alParameters);
    }


}
