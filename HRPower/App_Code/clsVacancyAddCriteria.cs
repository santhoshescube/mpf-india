﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsVacancyAddCriteria
/// </summary>

public class clsVacancyAddCriteria:DL
{
    #region Variables
    public static string Procedure = "HRspVacancyAddEdit";
    #endregion

    #region Properties
    public static int ID { get; set; }    
    public int JobID { get; set; }
    public bool IsBoardInterview { get; set; }
    public int Weightage { get; set; }
    public int CriteriaId { get; set; }
    public int JobDetailID { get; set; }
    public int JobLevelID { get; set; }
    public int Level { get; set; }   
    public Decimal WeightPercentage { get; set; }
    public Decimal PassPercentage { get; set; }
    public Decimal TotalMark { get; set; }
    public List<clsVacancyCriteria> VacancyCriteria { get; set; }
    #endregion

    public clsVacancyAddCriteria()
    { }

    #region Methods

    public DataSet GetAllCriterias(int JobID)//, bool IsBoardInterview
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAC"));
        arr.Add(new SqlParameter("@JobID", JobID));
        //arr.Add(new SqlParameter("@IsBoardInterview", IsBoardInterview));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, arr);
    }

    public DataTable GetVacancyLevels()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GVL"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, arr);
    }

    public DataTable GetCriterias()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GVC"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, arr);
    }

    public bool IsVacancyScheduled(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "IVS"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return Convert.ToInt32(ExecuteScalar(Procedure, arr)) > 0 ? true : false;
    }

    public void DeleteCriteria(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DC"));
        arr.Add(new SqlParameter("@JobID", JobID));
        ExecuteNonQuery(Procedure, arr).ToInt32();
    }

    public void SaveCriterias()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "SC"));
        arr.Add(new SqlParameter("@JobID", JobID));
        arr.Add(new SqlParameter("@IsBoardInterview", IsBoardInterview));
        arr.Add(new SqlParameter("@TotalMarks", TotalMark));
        arr.Add(new SqlParameter("@PassPercentage", PassPercentage));
        ExecuteNonQuery(Procedure, arr);
    }

    public void SaveCriteriasDetails()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "SCD"));
        arr.Add(new SqlParameter("@JobID", JobID));
        arr.Add(new SqlParameter("@JobLevelID", JobLevelID));
        arr.Add(new SqlParameter("@CriteriaID", CriteriaId));
        arr.Add(new SqlParameter("@WPercentage", WeightPercentage));
        ExecuteNonQuery(Procedure, arr);
    }

    public void InsertInterviewLevels()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "IIL"));
        arr.Add(new SqlParameter("@JobID", JobID));
        arr.Add(new SqlParameter("@JobLevelID", JobLevelID));
        ExecuteNonQuery(Procedure, arr);
    }

    #endregion
}
[Serializable]
public class clsVacancyCriteria
{
    public decimal Weightage { get; set; }
    public int CriteriaId { get; set; }
    public string Criteria { get; set; }
    public int JobDetailID { get; set; }
}
