﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

public class clsHomePage : DL
{
    public clsHomePage() { }
 
    public DataTable GetMyBenefits(int iEmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SEB1"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));

        return ExecuteDataTable("HRspEmployeeBenefits", alParameters);
    }
    public DataTable DisplayReasons(int FormReferenceID,int iRequestId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", -1));
        alParameters.Add(new SqlParameter("@FormReferenceID",FormReferenceID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetAllRequestedBy(int iEmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));


        return ExecuteDataTable("HRspHomePage", alParameters);
    }

    public DataTable GetRequestedByType(int iEmployeeID, string sReqType, string sRequestedDate, int iRequestedBy, int iStatusId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SRB"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@ReqType", sReqType));
        alParameters.Add(new SqlParameter("@RequestedDate", sRequestedDate));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));

        alParameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));

        return ExecuteDataTable("HRspHomePage", alParameters);
    }


    public DataTable GetAllCandidateStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCS"));

        return ExecuteDataTable("HRspHomePage", alParameters);
    }

    public DataSet GetRecentCandidates(int iPageIndex, int iPageSize,int iVacancyId,int iCandidateStatusId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRC"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alParameters.Add(new SqlParameter("@CandidateStatusId", iCandidateStatusId));
        alParameters.Add(new SqlParameter("@RoleId", new clsUserMaster().GetRoleId()));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetNewHires(int iPageIndex, int iPageSize, int JobID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GNH"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@JobId", JobID ));
        alParameters.Add(new SqlParameter("@RoleId", new clsUserMaster().GetRoleId()));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
       // alParameters.Add(new SqlParameter("@CandidateStatusId", iCandidateStatusId));
        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetAllDepartments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAD"));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetCalendarDetails(int iEmployeeId,string sDate)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CLD"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        alParameters.Add(new SqlParameter("@Date", sDate));


        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetAllVacancies(int iDepartmentId, int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAV"));
        alParameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetAllCandidates(int iDepartmentId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAV"));
        alParameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetRecentVacancies(int iPageIndex, int iPageSize, int iDepartmentId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRV"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));
        alParameters.Add(new SqlParameter("@RoleId", new clsUserMaster().GetRoleId()));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataTable GetPettyCashDetails(int iPageSize, int iPageIndex)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@EmployeeID", new clsUserMaster().GetEmployeeId()));

        return ExecuteDataTable("spPayPettyCash", alParameters);
    }
    public int GetPettyCashMessageCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@EmployeeID", new clsUserMaster().GetEmployeeId()));

        return ExecuteScalar("spPayPettyCash", alParameters).ToInt32();
    }


    public DataTable GetAnnouncements( int iEmployeeID , int CompanyID)
    {
        clsCommon objCommon= new clsCommon();

        ArrayList alParameters = new ArrayList();

        
        alParameters.Add(new SqlParameter("@Mode", "S1"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@SysDate", Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"), System.Globalization.CultureInfo.InvariantCulture)));
        return ExecuteDataTable("HRspAnnouncements", alParameters);
    }

    public bool IsAttendanceExists(int iEmployeeID,int iCompanyID,string sDate)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPE"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", sDate));

        return (Convert.ToInt32(ExecuteScalar("HRSpAttDailyReport", alParameters)) > 0 ? true : false);
    }

    public DataTable GetAttendanceDetail(int iEmployeeID, int iCompanyID, int iDayID, string sDate) // From attendancesummary
    {
        ArrayList alParameters = new ArrayList();

    
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", sDate));

        return ExecuteDataTable("AttDailyReport", alParameters);
    }

    public DataTable GetAttendanceSummary(int iEmployeeID, int iCompanyID, int iDayID, string sDate) // From attendancesummary
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAD"));
        alParameters.Add(new SqlParameter("@DayID", iDayID));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", sDate));

        return ExecuteDataTable("HRSpAttDailyReport", alParameters);
    }

    public DataTable GetEmployeeDayEntryExitDetails(int intEmployeeID, string strDate, int intLocationID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@Date", strDate ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("spPayRptAttendanceSummary", alParameters);
    }

    public DataSet  GetAttendance(int iEmployeeID,int iCompanyID,int iDayID,string sDate)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEP"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@DayID", iDayID));
        alParameters.Add(new SqlParameter("@AttDate1", sDate));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataSet("HRSpAttDailyReport", alParameters);
    }

    public DataTable  GetShiftToTime(int iEmployeeID, int iCompanyID, int iDayID, string sDate)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@DayID", iDayID));
        alParameters.Add(new SqlParameter("@AttDate1", sDate));

        return ExecuteDataTable("HRSpAttDailyReport", alParameters);
    }

    public DataTable GetPolicyConsequence(int iEmployeeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPC"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeeID));
       

        return ExecuteDataTable("HRSpAttDailyReport", alParameters);
    }


    public DataTable GetMessages(int iEmployeeID, int iRoleID,int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "MSG1"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@RoleId", iRoleID));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataTable("HRspHomePage", alParameters);
    }

    public DataTable GetHomeMessages(int iEmployeeID, int iRoleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HMSG"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@RoleId", iRoleID));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));


        return ExecuteDataTable("HRspHomePage", alParameters);
    }
    

    public DataTable GetMyRequests(int iEmployeeID, int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "MR1"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspHomePage", alParameters);
    }

    public DataSet GetAlert(int UserID,string Mode , int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@UserId", UserID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetJobTray(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GJT"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetAllJobs(int iEmployeeID, int iActivityTypeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAJ"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@ActivityTypeId", iActivityTypeId));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet SearchResult(int iVacancyId, int iPageIndex, int iPageSize)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SC"));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet AdvanceSearchResult(string sSkills,int iMin,int iMax,int iDegreeId,int iSpecilizationId, int iCompletionYear, int iPageIndex, int iPageSize)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "AS"));
        alParameters.Add(new SqlParameter("@Skills", sSkills));
        alParameters.Add(new SqlParameter("@MinimumExperience", iMin));
        alParameters.Add(new SqlParameter("@MaximumExperience", iMax));
        alParameters.Add(new SqlParameter("@DegreeID", iDegreeId));
        alParameters.Add(new SqlParameter("@SpecializationId", iSpecilizationId));
        alParameters.Add(new SqlParameter("@DegreeCompletionYear",iCompletionYear ));       
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public void Associate(int iVacancyId, int iCandidateId, int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "AC"));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateId));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        ExecuteNonQuery("HRspHomePage", alParameters);
    }
    public void AssociateCandidates(int iVacancyId,string sCandidateIds, int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ACL"));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alParameters.Add(new SqlParameter("@CandidateIds", sCandidateIds));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        ExecuteNonQuery("HRspHomePage", alParameters);
    }

    public DataSet GetFolders(int iParentId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GF"));
        alParameters.Add(new SqlParameter("@ParentId", iParentId));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetResumes(int iFolderId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GR"));
        alParameters.Add(new SqlParameter("@FolderId", iFolderId));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetRecentActivity(int iUserEmployeeID,int iVacancyID,int iCandidateID, int iPageIndex, int iPageSize, string ActivityFromDate , int companyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRA"));
        alParameters.Add(new SqlParameter("@UserEmployeeID", iUserEmployeeID));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyID));
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateID));
        alParameters.Add(new SqlParameter("@CompanyID", companyID));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@ActivityFromDate", ActivityFromDate));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetEmployees(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GS"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
 
    public DataSet GetPendingRequests(int iEmployeeID,int CompanyID, int iPageIndex, int iPageSize,string sReqType,string sRequestedDate,string sRequestedToDate, int iRequestedBy,int iStatusId,int RequestTypeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPR"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@CompanyID",CompanyID));
        alParameters.Add(new SqlParameter("@ReqType", sReqType));
        alParameters.Add(new SqlParameter("@RequestedDate", sRequestedDate));
        alParameters.Add(new SqlParameter("@RequestedToDate", sRequestedToDate));
        //if(iStatusId!=-1)
            alParameters.Add(new SqlParameter("@StatusId", iStatusId));

        alParameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alParameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
        alParameters.Add(new SqlParameter("@RequestedToID", new clsUserMaster().GetEmployeeId()));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public DataSet GetBuddyList(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBL"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetChildList(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEL"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetVacancyDeatils(int iVacancyId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GVD"));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public string GetFullName(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "BN"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return Convert.ToString(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }
    public DataSet GetAllActivityVacancy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRAV"));      
        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public DataSet GetAllActivityCandidates()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRAC"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataSet("HRspHomePage", alParameters);
    }

    public void HireCandidate(int iCandidateId,int iVacancyId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HC"));     
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateId));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        ExecuteNonQuery("HRspHomePage", alParameters);
    }
    public DataSet GetActionDetails(int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "PA"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        return ExecuteDataSet("HRspHomePage", alParameters);
    }
    public string GetShiftTime(string sShiftTime)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GST"));
        alParameters.Add(new SqlParameter("@ShiftToTime", sShiftTime));
        return Convert.ToString(ExecuteScalar("HRSpAttDailyReport", alParameters));
    }
   

    public bool CheckAttendanceTableExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CTE"));
       
        int iCount = ExecuteScalar("HRSpAttDailyReport", alParameters).ToInt32();
        if (iCount > 0)
            return true;
        else
            return false;
    }
    public bool AlterSalary(string strCondition)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ALTER_SALARY"));
        alParameters.Add(new SqlParameter("@strCondition", strCondition));
        int iRowsAffected = Convert.ToInt32(ExecuteScalar("HRspPerformanceAction", alParameters));
        if (iRowsAffected > 0)
            return true;
        else
            return false;
    }
    public bool AlterJob(string strCondition)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ALTER_DESIGNATION"));
        alParameters.Add(new SqlParameter("@strCondition", strCondition));
        int iRowsAffected = Convert.ToInt32(ExecuteScalar("HRspPerformanceAction", alParameters));
        if (iRowsAffected > 0)
            return true;
        else
            return false;
    }
    public DataTable FillDocumentType(int UserID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FD"));
        alParameters.Add(new SqlParameter("@UserID", UserID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()? 1 :0));

       return ExecuteDataTable ("HRspHomePage", alParameters);
     
    }
    public DataTable GetAlerts(int intAlertTypeID, DateTime? dtFrom, DateTime? dtTo, int intUserID, int intAlertType)
    {
        ArrayList prmCommon = new ArrayList();
        try
        {
            prmCommon.Add(new SqlParameter("@Mode", 2));
            //prmCommon.Add(new SqlParameter("@AlertTypeID", intAlertTypeID));
            prmCommon.Add(new SqlParameter("@UserID", intUserID));
            if (dtFrom != null)
                prmCommon.Add(new SqlParameter("@FromDate", dtFrom));
            if (dtTo != null)
                prmCommon.Add(new SqlParameter("@ToDate", dtTo));

            prmCommon.Add(new SqlParameter("@AlertTypeID", intAlertType));
            return ExecuteDataTable("spAlertReport", prmCommon);
        }
        catch (Exception ex) { throw ex; }
        finally { if (prmCommon != null) { prmCommon.Clear(); prmCommon = null; } }
    }

    public bool DeleteAlerts(DataTable datAlert)
    {
        ArrayList prmCommon;
        foreach (DataRow row in datAlert.Rows)
        {
            prmCommon = new ArrayList();
            prmCommon.Add(new SqlParameter("@Mode", 2));
            prmCommon.Add(new SqlParameter("@TYPEID", row["ID"].ToInt32()));
            ExecuteNonQuery ( "spAlertReport",prmCommon);
        }
        return true;
    }

    public static DataTable  GetLeaveDetails(int CompanyID,string LeaveDate,int LeaveTypeID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EL"));
        prmCommon.Add(new SqlParameter("@Companyid", CompanyID));
        prmCommon.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
       prmCommon.Add(new SqlParameter("@LeaveDate",LeaveDate));
        prmCommon.Add(new SqlParameter("@LeaveTypeID", LeaveTypeID));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }


    public static DataTable GetVacationLeaveDetails(int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EVL"));
        prmCommon.Add(new SqlParameter("@Companyid", CompanyID));
        prmCommon.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable GetVacationLeaveProcessing(int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EVLP"));
        prmCommon.Add(new SqlParameter("@Companyid", CompanyID));
        prmCommon.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@SysDate", Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"), System.Globalization.CultureInfo.InvariantCulture)));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable GetLateComers(int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Companyid", CompanyID));
        prmCommon.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@Mode", "LC"));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable GetEvaluations(int EmployeeID , int CompanyId)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "GE"));
        prmCommon.Add(new SqlParameter("@EmployeeID", EmployeeID ));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyId));
        prmCommon.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable GetWidget(int UserID,bool IsManager)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "DW"));
        prmCommon.Add(new SqlParameter("@UserID", UserID ));
        prmCommon.Add(new SqlParameter("@IsManager", IsManager));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public int UpdateUserWidget(int UserID, string WidgetIDs)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UPWID"));
        alParameters.Add(new SqlParameter("@UserID", UserID));
        alParameters.Add(new SqlParameter("@WidgetIDs", WidgetIDs));
        return Convert.ToInt32(ExecuteScalar("HRspHomePage", alParameters));
        
    }

    public static DataTable GetUserWidgets(int UserID, int IsManager)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "SUW"));
        prmCommon.Add(new SqlParameter("@UserID", UserID ));
        prmCommon.Add(new SqlParameter("@IsManager", IsManager));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }


    public static DataTable FillOfferApprovals(int PageIndex, int PageSize, string SearchKey, string SortOrder, string SortExpression,int EmployeeID,int OfferStatusID,int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GOA"));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alParameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID ));
        alParameters.Add(new SqlParameter("@OfferStatusID", OfferStatusID));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return new DataLayer().ExecuteDataTable("HRspHomePage", alParameters);
    }

    public static DataTable FillOfferStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FOS"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspHomePage", alParameters);
    }
    //Chart 
    public static DataTable FillLeaveChartCount(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "HLC"));
        alParameters.Add(new SqlParameter("@SysDate", Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"), System.Globalization.CultureInfo.InvariantCulture)));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspCharts", alParameters);
    }


    public static DataSet FillEmployeeChart(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FE"));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspCharts", alParameters);

    }
    public static DataSet GetLeaveTypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLT"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspHomePage", alParameters);
    }

    public static DataSet GetMessageChart(int iEmployeeID, int iRoleID, int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "PMR"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@RoleId", iRoleID));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return new DataLayer().ExecuteDataSet("HRspCharts", alParameters);
    }

    public static DataSet GetReturnAlert(int UserID,string Mode , int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@UserId", UserID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspCharts", alParameters);
    }


    public static DataSet GetExpiryAlert(int UserID, string Mode,int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@UserId", UserID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
      
        return new DataLayer().ExecuteDataSet("HRspCharts", alParameters);
    }

    public static string GetLateComersList( int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "LC"));
        prmCommon.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
        prmCommon.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspCharts", prmCommon));
    }
    public static string GetLeaveList( int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EL"));
        prmCommon.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
        prmCommon.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspCharts", prmCommon));
    }

    public static string GetVacLeaveList( int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EVL"));
        prmCommon.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
        prmCommon.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspCharts", prmCommon));
    }
    public static string GetVacProcLeaveList( int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "EVLP"));
        prmCommon.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
        prmCommon.Add(new SqlParameter("@SysDate", Convert.ToDateTime(DateTime.Now.ToString("dd MMM yyyy"), System.Globalization.CultureInfo.InvariantCulture)));
        prmCommon.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspCharts", prmCommon));
    }



    public static DataTable FillJob(int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "FJD"));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));
        prmCommon.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable FillJobDetails(int JobID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "FJ"));
        prmCommon.Add(new SqlParameter("@JobID", JobID));

        return new DataLayer().ExecuteDataTable("HRspHomePage", prmCommon);
    }

    public static DataTable FillJobStatus(int CompanyID)
    {
        ArrayList prmCommon = new ArrayList();

        prmCommon.Add(new SqlParameter("@Mode", "CJS"));
        prmCommon.Add(new SqlParameter("@CompanyID", CompanyID));

        return new DataLayer().ExecuteDataTable("HRspCharts", prmCommon);
    }

    public static DataTable GetAttendancePerformanceChart(string Mode, int DepartmentID,int EmployeeID, int Month, int Year,int MonthDays,int CompanyID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", Mode ));
        Parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@MonthDays", MonthDays));
        Parameters.Add(new SqlParameter("@Month", Month));
        Parameters.Add(new SqlParameter("@Year", Year));
        Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        Parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspCharts", Parameters);
    }

    public DataTable GetAllOpenJobs()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAOJ"));
        return ExecuteDataTable("HRspHomePage",parameters);
    }
    public static DataTable FillStatus()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "FRS"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspHomePage", parameters);
    }

    // fill assigned companies for this user
    public DataTable GetAllCompany(int EmployeeID , int UserID)
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@EmployeeID", EmployeeID));
        arraylst.Add(new SqlParameter("@UserID", UserID));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRSpGetUserCompanyDetails", arraylst);
    }
}