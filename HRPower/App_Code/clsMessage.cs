﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;


/// <summary>
/// Author : Ratheesh
/// </summary>
public class clsMessage : DL
{
    public string MessageID { get; set; }
    public DateTime MessageDate { get; set; }
    public int RequestedBy { get; set; }
    public int RequestedTo { get; set; }

    public int ReferenceID { get; set; }
    public eReferenceType ReferenceType { get; set; }
    public int AssociatedID { get; set; }
    public eReferenceType AssociatedReference { get; set; }
    public eMessageType MessageType { get; set; }
    public string Message { get; set; }
    public int CompanyID { get; set; }
    public DateTime MessageDueDate { get; set; }
    public List<clsMessageDetails> MessageDetailList { get; set; }

    private const string PROCEDURE_NAME = "[dbo].[HRspMessage]";

    public clsMessage()
    {
        this.MessageDetailList = new List<clsMessageDetails>();

        // set default values
        this.MessageType = eMessageType.None;
        this.ReferenceType = eReferenceType.None;
        this.AssociatedReference = eReferenceType.None;

        this.MessageDate = DateTime.MinValue;
    }

    /// <summary>
    /// Returns MessageID if insersion is successful, otherwise returns empty string
    /// </summary>
    /// <returns></returns>
    public string Insert()
    {
        ArrayList alParameters = new ArrayList { 
            new SqlParameter("@Mode", "INS"),
            new SqlParameter("@RequestedBy", this.RequestedBy),
            new SqlParameter("@ReferenceID", this.ReferenceID),
            new SqlParameter("@ReferenceTypeID", (int)this.ReferenceType),
            new SqlParameter("@MessageTypeID", (int)this.MessageType),
              new SqlParameter("@CompanyID", (int)this.CompanyID),
            new SqlParameter("@Message", this.Message)
        };

        if(this.MessageDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@MessageDate", this.MessageDate));

        if (this.MessageDueDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@MessageDueDate", this.MessageDueDate));

        if ((int)this.AssociatedReference > 0)
            alParameters.Add(new SqlParameter("@AssociateReferenceTypeID", (int)this.AssociatedReference));

        if (this.AssociatedID > 0)
            alParameters.Add(new SqlParameter("@AssociateID", this.AssociatedID));

        if (this.MessageDetailList.Count > 0)
            alParameters.Add(new SqlParameter("@xmlData", this.MessageDetailList.ToXml()));

        try
        {
            this.BeginTransaction(PROCEDURE_NAME);

            object objGUID = this.ExecuteScalar(alParameters);

            if (objGUID.ToStringCustom().Length > 0)
                this.CommitTransaction();
            else
                throw new ApplicationException("Message insersion failed");

            return objGUID.ToStringCustom();
        }
        catch
        {
            this.RollbackTransaction();

            return string.Empty;
        }
    }

    /// <summary>
    /// Inserts IDs of employees (having specified privilege) into messageDetail table
    /// </summary>
    /// <param name="messageType"></param>
    /// <returns></returns>
    public bool InsertMessageDetails(eMessageType messageType)
    {
        ArrayList alParameters = new ArrayList { 
            new SqlParameter("@MessageID", this.MessageID),
            new SqlParameter("@Mode", "INS_DET")
        };
        
        if(this.CompanyID > 0)
            alParameters.Add(new SqlParameter("@CompanyID", this.CompanyID)); 

        switch (messageType)
        {
            case eMessageType.VacancyRequest:
                alParameters.Add(new SqlParameter("@FormName", clsRoleSettings._MENU.CanApprove.ToString())); 
                break;

            case eMessageType.JobPromotion:
                alParameters.Add(new SqlParameter("@FormName", clsRoleSettings._MENU.Add_Action.ToString()));
                break;

            case eMessageType.AlterSalary:
                alParameters.Add(new SqlParameter("@FormName", clsRoleSettings._MENU.Add_Action.ToString()));
                break;
        }

        object objRowsAffected = this.ExecuteScalar(PROCEDURE_NAME, alParameters); 

        return objRowsAffected.ToInt32() > 0;
    }

    /// <summary>
    /// Deletes message and returns true if deletion is successfull otherwise returns false.
    /// </summary>
    /// <returns></returns>
    public bool Delete()
    {
        ArrayList alParameters = new ArrayList { 
            new SqlParameter("@Mode", "DEL"),
            new SqlParameter("@ReferenceID", this.ReferenceID),
            new SqlParameter("@ReferenceTypeID", (int)this.ReferenceType)
        };

        if((int)this.MessageType > 0)
            alParameters.Add(new SqlParameter("@MessageTypeID", (int)this.MessageType));

        if ((int)this.AssociatedReference > 0)
            alParameters.Add(new SqlParameter("@AssociateReferenceTypeID", (int)this.AssociatedReference));

        if (this.AssociatedID > 0)
            alParameters.Add(new SqlParameter("@AssociateID", this.AssociatedID));

        object objRowsAffected = this.ExecuteScalar(PROCEDURE_NAME, alParameters);

        return objRowsAffected.ToInt32() > 0;
    }

    /// <summary>
    /// delete and Insert Into MessageDetails when updating all Ess requests
    /// </summary>
    /// <returns></returns>
    public void Update()
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", "UPS"),
            new SqlParameter("@ReferenceID", this.ReferenceID),
            new SqlParameter("@ReferenceTypeID", (int)this.ReferenceType)}; 
     
        if (this.MessageDetailList.Count > 0)
            alParameters.Add(new SqlParameter("@xmlData", this.MessageDetailList.ToXml()));

        this.ExecuteScalar(PROCEDURE_NAME, alParameters);       
    }
    /// <summary>
    /// Set Read status as 1
    /// </summary>
    /// <returns></returns>
    public bool SetReadStatus()
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", "READ"),
            new SqlParameter("@RequestedTo", this.RequestedTo),
            new SqlParameter("@MessageTypeID", (int)this.MessageType)};

        return Convert.ToInt32(this.ExecuteScalar(PROCEDURE_NAME, alParameters)) > 0;
    }
    /// <summary>
    /// Set Delete status as 1
    /// </summary>
    /// <returns></returns>
    public bool SetDeleteStatus()
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", "DELMSG"),
            new SqlParameter("@RequestedTo", this.RequestedTo),
            new SqlParameter("@MessageTypeID", (int)this.MessageType)};

        return Convert.ToInt32(this.ExecuteScalar(PROCEDURE_NAME, alParameters)) > 0;
    }

    public bool DeleteMessageByID()
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", "DMGID"),
            new SqlParameter("@RequestedTo", this.RequestedTo),
            new SqlParameter("@MessID", (string)this.MessageID)};

        return Convert.ToInt32(this.ExecuteScalar(PROCEDURE_NAME, alParameters)) > 0;
    }

    /// <summary>
    /// Set Delete status as 1 by passing messageid
    /// </summary>
    /// <returns></returns>
    public bool SetDeleteStatusByMsgID()
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", "DELMSGBYID"),
            new SqlParameter("@MID", this.MessageID)};

        return Convert.ToInt32(this.ExecuteScalar(PROCEDURE_NAME, alParameters)) > 0;
    }

}


[Serializable]
public class clsMessageDetails
{
    /// <summary>
    /// Gets or sets EmployeeID
    /// </summary>
    public int RequestedTo { get; set; }
    /// <summary>
    /// Indicates whether user has read or not
    /// </summary>
    public bool IsRead { get; set; }

    public bool IsDeleted { get; set; }

    public clsMessageDetails()
    {
        this.IsRead = false;
        this.IsDeleted = false;
    }

    public clsMessageDetails(int requstedTo, bool isRead, bool isDeleted)
    {
        this.RequestedTo = requstedTo;
        this.IsRead = isRead;
        this.IsDeleted = isDeleted;
    }
}