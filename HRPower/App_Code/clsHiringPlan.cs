﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.IO;

/// <summary>
/// Summary description for clsHiringPlan
/// </summary>
public class clsHiringPlan:DL
{
	public clsHiringPlan()
	{
	
	}
    private int iVacancyId;
    private int iPlanId;
    private int iPlanTypeId;
    private string sContactPerson;
    private string sWebsite;
    private string sEmail;
    private string sContactNo;
    private int iRequestedBy;
    private string sRequestedTo;
    private int iStatusId;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;

    private int iApprovedBy;

    public int VacancyId
    {
        get { return iVacancyId; }
        set { iVacancyId = value; }
    }
    public int PlanId
    {
        get { return iPlanId; }
        set { iPlanId = value; }
    }
    public int PlanTypeId
    {
        get { return iPlanTypeId; }
        set { iPlanTypeId = value; }
    }
    public string ContactPerson
    {
        get { return sContactPerson; }
        set { sContactPerson = value; }
    }
    public string Website
    {
        get { return sWebsite; }
        set { sWebsite = value; }
    }
    public string Email
    {
        get { return sEmail; }
        set { sEmail = value; }
    }
    public string ContactNo
    {
        get { return sContactNo; }
        set { sContactNo = value; }
    }
    public int RequestedBy
    {
        get { return iRequestedBy; }
        set { iRequestedBy = value; }
    }
    public string RequestedTo
    {
        get { return sRequestedTo; }
        set { sRequestedTo = value; }
    }
    public int StatusId
    {
        get { return iStatusId; }
        set { iStatusId = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }
    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
    public int ApprovedBy
    {
        get { return iApprovedBy; }
        set { iApprovedBy = value; }
    }

    public int InsertPlanMaster()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alparameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        alparameters.Add(new SqlParameter("@ContactPerson", sContactPerson));
        alparameters.Add(new SqlParameter("@ContactNo", sContactNo));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int InsertPlanDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ID"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));
        alparameters.Add(new SqlParameter("@PlanTypeId", iPlanTypeId));
        alparameters.Add(new SqlParameter("@ContactPerson", sContactPerson));
        alparameters.Add(new SqlParameter("@Website", sWebsite));
        alparameters.Add(new SqlParameter("@Email", sEmail));
        alparameters.Add(new SqlParameter("@ContactNo", sContactNo));
        alparameters.Add(new SqlParameter("@StatusId", iStatusId));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdatePlanMaster()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alparameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        alparameters.Add(new SqlParameter("@ContactPerson", sContactPerson));
        alparameters.Add(new SqlParameter("@ContactNo", sContactNo));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdatePlanDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UD"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));
        alparameters.Add(new SqlParameter("@PlanTypeId", iPlanTypeId));
        alparameters.Add(new SqlParameter("@ContactPerson", sContactPerson));
        alparameters.Add(new SqlParameter("@Website", sWebsite));
        alparameters.Add(new SqlParameter("@Email", sEmail));
        alparameters.Add(new SqlParameter("@ContactNo", sContactNo));
        alparameters.Add(new SqlParameter("@StatusId", iStatusId));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateApprovalDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "AP"));

        alparameters.Add(new SqlParameter("@PlanId", iPlanId));
        alparameters.Add(new SqlParameter("@ApprovedBy", iApprovedBy));
        alparameters.Add(new SqlParameter("@StatusId", iStatusId));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataSet BindVacancies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GV"));  
        if(iRequestedBy>0)
            alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
     
    }

    public DataSet BindStatus()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GRS"));
        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public SqlDataReader BindVacancyDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GVI"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
       

        return ExecuteReader("HRspHiringPlanDetails", alparameters);
    }

    public DataSet BindVacancyHiringPlans()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GHI"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public DataSet BindRequestedHiringPlans()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GHR"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));

        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public DataSet BindaPlan()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAP"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));

        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public DataSet BindAllVacancyHiringPlans()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAH"));
       
        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alparameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alparameters.Add(new SqlParameter("@PageSize", iPageSize));
        alparameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", sSortOrder));

        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));

        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
       
        return Convert.ToInt32(ExecuteScalar("HRspHiringPlanDetails", alparameters));
    }

    public DataSet BindaVacancyHiringPlans()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GVH"));

        alparameters.Add(new SqlParameter("@RequestedBy", iRequestedBy));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspHiringPlanDetails", alparameters);
    }

    public void DeleteaPlan()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DP"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));

        ExecuteNonQuery("HRspHiringPlanDetails", alparameters);
        
    }

    public void CancelaPlan()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CP"));
        alparameters.Add(new SqlParameter("@PlanId", iPlanId));

        ExecuteNonQuery("HRspHiringPlanDetails", alparameters);
    }

    public void Begin()
    {
        BeginTransaction("HRspHiringPlanDetails");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }
}
