﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
/// <summary>
/// Summary description for clsEmployeeSalaryCertificate
/// </summary>
public class clsEmployeeSalaryCertificate:DL
{
	public clsEmployeeSalaryCertificate()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable GetEmployees(int iDepartmentId, int iCompanyID)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SE"));
        arraylst.Add(new SqlParameter("@DepartmentId",iDepartmentId ));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));
        arraylst.Add(new SqlParameter("@Isarabic" , clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspEmployeeSalaryCertificate",arraylst );
    }
    public DataTable GetEmail(int iEmployeeID)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "EML"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeID));

        return ExecuteDataTable("HRspEmployeeSalaryCertificate", arraylst);
    }
    public DataSet GetTemplateValues(int iEmployeeId, int CreatedBy)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GTD"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Isarabic", clsGlobalization.IsArabicCulture()));
        arraylst.Add(new SqlParameter("@CreatedBy", CreatedBy));
        return ExecuteDataSet("HRspEmployeeSalaryCertificate", arraylst);

    }
   
} 
