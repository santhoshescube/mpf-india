﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptLabourCost
/// </summary>
public class clsRptLabourCost:DL
{
    public clsRptLabourCost()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static DataSet GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, bool IsMonth, int Month, int Year, DateTime FinYearStartDate, DateTime FinYearEndDate, DateTime MonthEndDate)
    {
        List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID),
                new SqlParameter("@IncludeCompany",IncludeCompany),
                new SqlParameter("@DepartmentID",DepartmentID) ,
                new SqlParameter("@WorkStatusID",WorkStatusID) ,
                new SqlParameter("@EmployeeID",EmployeeID),
                new SqlParameter("@IsMonth",IsMonth),
                new SqlParameter("@Month",Month),
                new SqlParameter("@Year",Year),
                new SqlParameter("@FinYearStartDate",FinYearStartDate),
                new SqlParameter("@FinYearEndDate", FinYearEndDate),
                new SqlParameter("@MonthEndDate", MonthEndDate),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
                     };
        return new DataLayer().ExecuteDataSet("spPayRptLabourCost", sqlParameters);
    }
}
