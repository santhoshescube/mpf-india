﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using ResumeParser;
/// <summary>
/// Summary description for clsResumes
/// purpose : Handle Resumes
/// created : Binoj
/// Date    : 25.10.2010
/// Modified by : Laxmi
/// </summary>
public class clsResumes : DL
{
    Int64 IResumeId;
    int iFolderId;
    string sSalutation;
    string sFirstName;
    string sMiddleName;
    string sLastName;
    string sGender;
    string sPermanentAddress;
    string sCurrentAddress;
    string sEmail;
    string sMobile;
    string sLandPhone;
    string sPassportNumber;
    string sPassportExpiryDate;
    string sDrivingLicenceNumber;
    string sCurrentEmployer;
    string sCurrentJobTitle;
    string sExeperience;
    string sDegree;
    string sSkills;
    string sCertifications;
    decimal dCurrentSalary;
    decimal dExpectedSalary;
    string sAdditionalInfo;
    string  sMetaData;
    string sResumeAttachment;
    int iResumeStatusId;
    DateTime DSubmitDate;
    string sCountryOfOrigin;
    string sNationality;
    string sDOB;

    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }


    public Int64 ResumeId { get { return IResumeId; } set { IResumeId = value; } }
    public int FolderId { get { return iFolderId; } set { iFolderId = value; } }
    public string Salutation { get { return sSalutation; } set { sSalutation = value; } }
    public string FirstName { get { return sFirstName; } set { sFirstName = value; } }
    public string MiddleName { get { return sMiddleName; } set { sMiddleName = value; } }
    public string LastName { get { return sLastName; } set { sLastName = value; } }
    public string Gender { get { return sGender; } set { sGender = value; } }
    public string PermanentAddress { get { return sPermanentAddress; } set { sPermanentAddress = value; } }
    public string CurrentAddress { get { return sCurrentAddress; } set { sCurrentAddress = value; } }
    public string Email { get { return sEmail; } set { sEmail = value; } }
    public string Mobile { get { return sMobile; } set { sMobile = value; } }
    public string LandPhone { get { return sLandPhone; } set { sLandPhone = value; } }
    public string PassportNumber { get { return sPassportNumber; } set { sPassportNumber = value; } }
    public string PassportExpiryDate { get { return sPassportExpiryDate; } set { sPassportExpiryDate = value; } }
    public string DrivingLicenceNumber { get { return sDrivingLicenceNumber; } set { sDrivingLicenceNumber = value; } }
    public string CurrentEmployer { get { return sCurrentEmployer; } set { sCurrentEmployer = value; } }
    public string CurrentJobTitle { get { return sCurrentJobTitle; } set { sCurrentJobTitle = value; } }
    public string Exeperience { get { return sExeperience; } set { sExeperience = value; } }
    public string Degree { get { return sDegree; } set { sDegree = value; } }
    public string Skills { get { return sSkills; } set { sSkills = value; } }
    public string Certifications { get { return sCertifications; } set { sCertifications = value; } }
    public decimal CurrentSalary { get { return dCurrentSalary; } set { dCurrentSalary = value; } }
    public decimal ExpectedSalary { get { return dExpectedSalary; } set { dExpectedSalary = value; } }
    public string AdditionalInfo { get { return sAdditionalInfo; } set { sAdditionalInfo = value; } }
    public string MetaData { get { return sMetaData; } set { sMetaData = value; } }
    public string ResumeAttachment { get { return sResumeAttachment; } set { sResumeAttachment = value; } }
    public int ResumeStatusId { get { return iResumeStatusId; } set { iResumeStatusId = value; } }
    public DateTime SubmitDate { get { return DSubmitDate; } set { DSubmitDate = value; } }
    public string CountryOfOrigin { get { return sCountryOfOrigin; } set { sCountryOfOrigin = value; } }
    public string Nationality { get { return sNationality; } set { sNationality = value; } }
    public string DOB { get { return sDOB; } set { sDOB = value; } }
    
    public string ResumeCode { get; set; }
    public string Rank { get; set; }
    
    public clsResumes() { }
    
    public DataSet GetAllResumes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAR"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataSet("HRspResumes", alParameters);
    }

    public DataSet GetResume()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GR"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));

        return ExecuteDataSet("HRspResumes", alParameters);
    }
    public int DeleteResume()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));

        return Convert.ToInt32(ExecuteNonQuery("HRspResumes", alParameters));
    }
    public int SetRank()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SRR"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));
        alParameters.Add(new SqlParameter("@Rank", Rank));

        return Convert.ToInt32(ExecuteNonQuery("HRspResumes", alParameters));
    }
    public int ShortListResume()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SR"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));
        try
        {
            return Convert.ToInt32(ExecuteScalar("HRspResumes", alParameters));
        }
        catch(Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return 0;
        }
    }
    public int GetParseResume(string sResumeUrl, int iFolderId, string sCountryKey)
    {
 
        try
        {

            Service1 objParser = new Service1();

            // System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("~/xml/Resume.xml"), ack);
            BinaryReader binRaeader = new BinaryReader(File.Open(sResumeUrl, FileMode.Open, FileAccess.Read));
            binRaeader.BaseStream.Position = 0;
            byte[] binFile = binRaeader.ReadBytes(Convert.ToInt32(binRaeader.BaseStream.Length));
            binRaeader.Close();

            XmlNode xmlNodeDoc = objParser.ParseResume(binFile, Path.GetExtension(sResumeUrl));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.AppendChild(xmlDoc.ImportNode(xmlNodeDoc, true));
           
            //xmlDoc.Save(HttpContext.Current.Server.MapPath("~/xml/Resume.xml"));

            //xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Resume.xml"));

            XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//Resume"); // reteriive node with partiicular templateid


            FirstName = xnTemplateNode.SelectSingleNode("FirstName").InnerXml;
            MiddleName = xnTemplateNode.SelectSingleNode("MiddleName").InnerXml;
            LastName = xnTemplateNode.SelectSingleNode("LastName").InnerXml;
            Salutation = "Ms.";
            Gender = xnTemplateNode.SelectSingleNode("Gender").InnerXml;
            PermanentAddress = xnTemplateNode.SelectSingleNode("PermanentAddress").InnerXml;
            Email = xnTemplateNode.SelectSingleNode("Email").InnerXml;
            Mobile = xnTemplateNode.SelectSingleNode("Mobile").InnerXml;
            CurrentJobTitle = xnTemplateNode.SelectSingleNode("CurrentJobTitle ").InnerXml;
            AdditionalInfo = xnTemplateNode.SelectSingleNode("Experience").InnerXml;
            CurrentEmployer = xnTemplateNode.SelectSingleNode("CurrentEmployer ").InnerXml;

            // CurrentSalary = Convert.ToDecimal(xnTemplateNode.SelectSingleNode("CurrentSalary").InnerXml);
            Degree = xnTemplateNode.SelectSingleNode("Degree").InnerXml;
            DrivingLicenceNumber = xnTemplateNode.SelectSingleNode("DrivingLicenseNumber").InnerXml;
            // ExpectedSalary = Convert.ToDecimal( xnTemplateNode.SelectSingleNode("ExpectedSalary").InnerXml);
            LandPhone = xnTemplateNode.SelectSingleNode("LandPhone ").InnerXml;

            MetaData = xnTemplateNode.SelectSingleNode("MetaData").InnerXml;
            PassportNumber = xnTemplateNode.SelectSingleNode("PassportNumber").InnerXml;
            PassportExpiryDate = xnTemplateNode.SelectSingleNode("PassportExpiryDate").InnerXml;
            Skills = xnTemplateNode.SelectSingleNode("Skills").InnerXml;
            ResumeAttachment = Path.GetFileName(sResumeUrl);
            DOB = xnTemplateNode.SelectSingleNode("DOB").InnerXml;
            CountryOfOrigin = xnTemplateNode.SelectSingleNode("Country").InnerXml;
            Nationality = xnTemplateNode.SelectSingleNode("Nationality").InnerXml;

            FolderId = iFolderId;

            return (InsertResume());



        }
        catch (Exception ex)
        {
            return 0;
            // TxtOutput.Text = ex.Message + "\n\r" + ex.StackTrace + "\n\r" + ex.Source;

        }

    }

    public int InsertResume()
    {
        try
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", "IN"));
            alParameters.Add(new SqlParameter("@FolderId", iFolderId));
           // alParameters.Add(new SqlParameter("@Salutation", sSalutation));
            alParameters.Add(new SqlParameter("@FirstName", sFirstName));
            alParameters.Add(new SqlParameter("@MiddleName", sMiddleName));
            alParameters.Add(new SqlParameter("@LastName", sLastName));
            alParameters.Add(new SqlParameter("@PermanentAddress", sPermanentAddress));
            alParameters.Add(new SqlParameter("@PassportNumber", sPassportNumber));
            alParameters.Add(new SqlParameter("@DrivingLicenceNumber", sDrivingLicenceNumber));
            alParameters.Add(new SqlParameter("@CurrentEmployer", sCurrentEmployer));
            alParameters.Add(new SqlParameter("@CurrentJobTitle", sCurrentJobTitle));
            alParameters.Add(new SqlParameter("@Degree", sDegree));
            alParameters.Add(new SqlParameter("@Skills", sSkills));
            alParameters.Add(new SqlParameter("@CurrentSalary", dCurrentSalary));
            alParameters.Add(new SqlParameter("@ExpectedSalary", dExpectedSalary));
            alParameters.Add(new SqlParameter("@Gender", sGender));
            alParameters.Add(new SqlParameter("@Email", sEmail));
            alParameters.Add(new SqlParameter("@Mobile", sMobile));
            alParameters.Add(new SqlParameter("@LandPhone", sLandPhone));
            alParameters.Add(new SqlParameter("@MetaData", sMetaData));
            alParameters.Add(new SqlParameter("@AdditionalInfo", sAdditionalInfo));
            alParameters.Add(new SqlParameter("@ResumeAttachment", sResumeAttachment));
            alParameters.Add(new SqlParameter("@CountryofOrigin", sCountryOfOrigin));
            alParameters.Add(new SqlParameter("@Nationality", sNationality));
            alParameters.Add(new SqlParameter("@DOB", sDOB));

            return Convert.ToInt32(ExecuteScalar("HRspResumes", alParameters));
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public int UpdateResume()
    {

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UR"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));
        alParameters.Add(new SqlParameter("@Salutation", sSalutation));
        alParameters.Add(new SqlParameter("@FirstName", sFirstName));
        alParameters.Add(new SqlParameter("@MiddleName", sMiddleName));
        alParameters.Add(new SqlParameter("@LastName", sLastName));
        alParameters.Add(new SqlParameter("@Gender", sGender));
        alParameters.Add(new SqlParameter("@PermanentAddress", sPermanentAddress));
        alParameters.Add(new SqlParameter("@CurrentAddress", sCurrentAddress));
        alParameters.Add(new SqlParameter("@Email", sEmail));
        alParameters.Add(new SqlParameter("@Mobile", sMobile));
        alParameters.Add(new SqlParameter("@LandPhone", sLandPhone));
        alParameters.Add(new SqlParameter("@PassportNumber", sPassportNumber));
        alParameters.Add(new SqlParameter("@PassportExpiryDate", sPassportExpiryDate));
        alParameters.Add(new SqlParameter("@DrivingLicenceNumber", sDrivingLicenceNumber));
        alParameters.Add(new SqlParameter("@CurrentEmployer", sCurrentEmployer));
        alParameters.Add(new SqlParameter("@CurrentJobTitle", sCurrentJobTitle));
        alParameters.Add(new SqlParameter("@Exeperience", sExeperience));
        alParameters.Add(new SqlParameter("@Degree", sDegree));
        alParameters.Add(new SqlParameter("@Skills", sSkills));
        alParameters.Add(new SqlParameter("@Certifications", sCertifications));
        alParameters.Add(new SqlParameter("@CurrentSalary", dCurrentSalary));
        alParameters.Add(new SqlParameter("@ExpectedSalary", dExpectedSalary));
        alParameters.Add(new SqlParameter("@AdditionalInfo", sAdditionalInfo));
        alParameters.Add(new SqlParameter("@MetaData", sMetaData));
        alParameters.Add(new SqlParameter("@CountryofOrigin", sCountryOfOrigin));
        alParameters.Add(new SqlParameter("@Nationality", sNationality));
        alParameters.Add(new SqlParameter("@DOB", sDOB));
        alParameters.Add(new SqlParameter("@ResumeCode", ResumeCode));

        if (Rank == "") alParameters.Add(new SqlParameter("@Rank", DBNull.Value));
        else alParameters.Add(new SqlParameter("@Rank", Rank));


        return Convert.ToInt32(ExecuteScalar("HRspResumes", alParameters));
    }

    public int UpdateResumeAttachment()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "URA"));
        alParameters.Add(new SqlParameter("@ResumeId", IResumeId));
        alParameters.Add(new SqlParameter("@ResumeAttachment", sResumeAttachment));
        return Convert.ToInt32(ExecuteScalar("HRspResumes", alParameters));
    }

    public bool IsResumeCodeExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IRC"));
        alParameters.Add(new SqlParameter("@ResumeId", ResumeId));
        alParameters.Add(new SqlParameter("@ResumeCode", ResumeCode));

        return (Convert.ToInt32(ExecuteScalar("HRspResumes", alParameters)) > 0 ? true : false);
    }
    public DataSet FillComboDegree()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "fd"));

        return ExecuteDataSet("HRspResumes", alparameters);
    }
    public DataSet GetAdvancedSearchResults()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "AS"));
        alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alparameters.Add(new SqlParameter("@PageSize", PageSize));
        alparameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alparameters.Add(new SqlParameter("@Degree", sDegree ));   
        alparameters.Add(new SqlParameter("@Skills", sSkills));
        alparameters.Add(new SqlParameter("@Exeperience", sExeperience));
        alparameters.Add(new SqlParameter("@ResumeCode", ResumeCode ));
        if (Rank == "") alparameters.Add(new SqlParameter("@Rank", 0));
        else alparameters.Add(new SqlParameter("@Rank", Rank));
      
        return ExecuteDataSet("HRspResumes", alparameters);
    }


}
