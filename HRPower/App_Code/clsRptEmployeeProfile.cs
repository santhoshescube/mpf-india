﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;


   public  class clsRptEmployeeProfile
    {

       public static DataTable GetEmployeeInformation(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intEmployeeID, bool intIncludeComp, int intLocation, int intWorkStatus, int intCountryId, int intReligionId, string dtDOJFrom, string dtDOJTo)
        {
                      
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",intCompanyID), 
                new SqlParameter("@BranchID", intBranchID) ,
                new SqlParameter("@DepartmentID", intDepartmentID) ,
                new SqlParameter("@DesignationID", intDesignationID) ,
                new SqlParameter("@EmploymentTypeID", intEmploymentTypeID),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@IncludeComp", intIncludeComp),
                new SqlParameter("@LocationID", intLocation),
                new SqlParameter("@WorkStatusID", intWorkStatus),
                new SqlParameter("@CountryId",intCountryId),
                new SqlParameter("@NationalityID",-1),
                new SqlParameter("@ReligionId",intReligionId),
                new SqlParameter("@DOJ",dtDOJFrom =="01-Jan-1900"?null: dtDOJFrom),
                new SqlParameter("@DOJTo", dtDOJTo),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
            
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }

        public static DataTable DisplaySingleEmployeeReport(int intEmployeeID)  //Employee  Report
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@EmployeeID", intEmployeeID)
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProfileSummary", sqlParameters);
        }


        public static DataTable GetNationalisationRatioReport(int intCountryID, int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmployeementTypeID, int intEmployeeID, int intLocationID, int intWorkStatusID)  //Nationalisation Ratio Report
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>
            { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID", intCompanyID ),
                new SqlParameter("@BranchID", intBranchID),
                new SqlParameter("@DepartmentID", intDepartmentID),
                new SqlParameter("@DesignationID", intDesignationID),
                new SqlParameter("@EmployeementTypeID", intEmployeementTypeID),
                new SqlParameter("@EmployeeID", intEmployeeID),
                new SqlParameter("@LocationID", intLocationID),
                new SqlParameter("@WorkStatusID", intWorkStatusID),
                new SqlParameter("@CountryID", intCountryID),
                new SqlParameter("@IsArabicView", 0),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
            };
            return new DataLayer().ExecuteDataTable("spRptGenReport", sqlParameters);
        }

        public static DataSet GetEmployeeHistory(int EmployeeID)
        {
            ArrayList prmReportDetails = new ArrayList();
            prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
            prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataSet("spEmployeeHistory", prmReportDetails);
        }
    }
