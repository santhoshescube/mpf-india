﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsLeavePolicy
/// purpose : Handle LeavePolicy
/// created : Binoj
/// Date    : 07.09.2010
/// </summary>

public class clsLeavePolicy : DL
{
    int iLeavePolicyID, iCompanyID, iDepartmentID, iDesignationID, iEmploymentTypeID, iProjectID, iLeaveDetailID, iLeaveTypeID, iUserId, iLeaveConsequenceID, iCLeaveTypeID, iCalculationBasedID,iVacationPolicyId;
    string sLeavePolicyName;
    bool bDefaultPolicy;
    decimal fNoOfLeave, fMonthLeave, fCarryForwardLeave, fEncashDays, fCalculationPercentage, fApplicableDays, fOrderNo,fSerialNo;
    DateTime dApplicableFrom;

    public int VacationPolicyId{set{iVacationPolicyId= value;} get{return iVacationPolicyId;}}
    public int LeavePolicyID { set { iLeavePolicyID = value; } get { return iLeavePolicyID; } }
    public int CompanyID { set { iCompanyID = value; } get { return iCompanyID; } }
    public int DepartmentID { set { iDepartmentID = value; } get { return iDepartmentID; } }
    public int DesignationID { set { iDesignationID = value; } get { return iDesignationID; } }
    public int EmploymentTypeID { set { iEmploymentTypeID = value; } get { return iEmploymentTypeID; } }
    public int ProjectID { set { iProjectID = value; } get { return iProjectID; } }
    public int LeaveDetailID { set { iLeaveDetailID = value; } get { return iLeaveDetailID; } }
    public int LeaveTypeID { set { iLeaveTypeID = value; } get { return iLeaveTypeID; } }
    public string LeavePolicyName { set { sLeavePolicyName = value; } get { return sLeavePolicyName; } }
    public bool DefaultPolicy { set { bDefaultPolicy = value; } get { return bDefaultPolicy; } }
    public decimal NoOfLeave { set { fNoOfLeave = value; } get { return fNoOfLeave; } }
    public decimal MonthLeave { set { fMonthLeave = value; } get { return fMonthLeave; } }
    public decimal CarryForwardLeave { set { fCarryForwardLeave = value; } get { return fCarryForwardLeave; } }
    public decimal EncashDays { set { fEncashDays = value; } get { return fEncashDays; } }
    public DateTime ApplicableFrom { set { dApplicableFrom = value; } get { return dApplicableFrom; } }

    // Leave policy consequences
    public int LeaveConsequenceID { set { iLeaveConsequenceID = value; } get { return iLeaveConsequenceID; } }
    public int CLeaveTypeID { set { iCLeaveTypeID = value; } get { return iCLeaveTypeID; } }
    public int CalculationBasedID { set { iCalculationBasedID = value; } get { return iCalculationBasedID; } }
    public decimal CalculationPercentage { set { fCalculationPercentage = value; } get { return fCalculationPercentage; } }
    public decimal ApplicableDays { set { fApplicableDays = value; } get { return fApplicableDays; } }
    public decimal OrderNo { set { fOrderNo = value; } get { return fOrderNo; } }
    public decimal SerialNo { set { fSerialNo  = value; } get { return fSerialNo; } }

    




     public int UserId { get { return iUserId; } set { iUserId = value; } }
    public clsLeavePolicy() { }



    public DataSet FillAllCombos()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FC"));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataTable GetAllDepartments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDR"));

        return ExecuteDataTable("HRspLeavePolicy", alParameters);

    }

    public DataTable GetLeaveTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GLT"));

        return ExecuteDataTable("HRspLeavePolicy", alParameters);

    }


    public DataTable GetAllDesignations()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDSR"));

        return ExecuteDataTable("HRspLeavePolicy", alParameters);

    }
    public DataTable GetAllEmploymentTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GER"));

        return ExecuteDataTable("HRspLeavePolicy", alParameters);

    }

    
    public DataSet GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataSet GetAllProjects()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataSet GetAllLeaveTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GALT"));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataSet GetAllCalcBasedParameters()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GACB"));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }


    public int InsertLeavePolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ICLM"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@LeavePolicyName", sLeavePolicyName));
        if (iDepartmentID != -1) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        if (iDesignationID != -1) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));
        if (iEmploymentTypeID != -1) alParameters.Add(new SqlParameter("@EmploymentTypeID", iEmploymentTypeID));
        if (iVacationPolicyId != -1) alParameters.Add(new SqlParameter("@VacationPolicyID", iVacationPolicyId));
        //if (iProjectID != -1) alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        //alParameters.Add(new SqlParameter("@DefaultPolicy", bDefaultPolicy));
        alParameters.Add(new SqlParameter("@ApplicableFrom", dApplicableFrom));

        return  Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters));
    }

    public int UpdateLeavePolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UCLM"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@LeavePolicyName", sLeavePolicyName));
        if (iDepartmentID != -1) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        if (iDesignationID != -1) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));
        if (iEmploymentTypeID != -1) alParameters.Add(new SqlParameter("@EmploymentTypeID", iEmploymentTypeID));
        if (iVacationPolicyId != -1) alParameters.Add(new SqlParameter("@VacationPolicyID", iVacationPolicyId));
        //if (iProjectID != -1) alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        //alParameters.Add(new SqlParameter("@DefaultPolicy", bDefaultPolicy));
        alParameters.Add(new SqlParameter("@ApplicableFrom", dApplicableFrom));

        return Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters));
     
    }

    public void InsertLeaveDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ICLD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@LeaveTypeID", iLeaveTypeID));
        alParameters.Add(new SqlParameter("@NoOfLeave", fNoOfLeave));
        alParameters.Add(new SqlParameter("@MonthLeave", fMonthLeave));
        alParameters.Add(new SqlParameter("@CarryForwardLeave", fCarryForwardLeave));
        alParameters.Add(new SqlParameter("@EncashDays", fEncashDays));
      
        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }

    public void InsertLeaveConsequenceDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ICPD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@CLeaveTypeID", iCLeaveTypeID));
        alParameters.Add(new SqlParameter("@CalculationBasedID", iCalculationBasedID));
        alParameters.Add(new SqlParameter("@CalculationPercentage", fCalculationPercentage));
        alParameters.Add(new SqlParameter("@ApplicableDays", fApplicableDays ));
        alParameters.Add(new SqlParameter("@OrderNo", fOrderNo ));

        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }

    public void UpdateLeaveDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UCLD"));
        alParameters.Add(new SqlParameter("@LeavePolicyId", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@LeaveTypeID", iLeaveTypeID));
        alParameters.Add(new SqlParameter("@NoOfLeave", fNoOfLeave));
        alParameters.Add(new SqlParameter("@MonthLeave", fMonthLeave));
        alParameters.Add(new SqlParameter("@CarryForwardLeave", fCarryForwardLeave));
        alParameters.Add(new SqlParameter("@EncashDays", fEncashDays));
        alParameters.Add(new SqlParameter("@OrderNo", fOrderNo));
        alParameters.Add(new SqlParameter("@SerialNO", fSerialNo));

        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }
    public void UpdateLeaveConsequenceDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UCPD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID ));
        alParameters.Add(new SqlParameter("@CLeaveTypeID", iCLeaveTypeID));
        alParameters.Add(new SqlParameter("@CalculationBasedID", iCalculationBasedID));
        alParameters.Add(new SqlParameter("@CalculationPercentage", fCalculationPercentage));
        alParameters.Add(new SqlParameter("@ApplicableDays", fApplicableDays));
        alParameters.Add(new SqlParameter("@OrderNo", fOrderNo));
        alParameters.Add(new SqlParameter("@SerialNo", fSerialNo));

        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }
    public DataSet GetAllLeavePolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GALP"));
        alParameters.Add(new SqlParameter("@UserId", iUserId));
        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataSet GetLeaveDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GLD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }

    public DataSet GetLeaveConsequenceDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GLCD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        return ExecuteDataSet("HRspLeavePolicy", alParameters);
    }


    public DataTable GetApplicableFromDates()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GFSD"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        DateTime dFinYearStartDate = Convert.ToDateTime(ExecuteScalar("HRspLeavePolicy", alParameters));

        DataTable dt = new DataTable();

        dt.Columns.Add("ApplicableFrom");

        DataRow dw;

        for (int i = dFinYearStartDate.Year; i <= DateTime.Now.Year + 1; i++)
        {
            dw = dt.NewRow();

            dw["ApplicableFrom"] = dFinYearStartDate.ToString("dd MMM ") + i.ToString();

            dt.Rows.Add(dw);
        }

        return dt;
    }

    public bool IsPolicyAssigned()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPA"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        return (Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters)) > 0 ? true : false);
    }

    public int Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

       return (Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters)));
    }

    public bool HasDefaultPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CHD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return (Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters)) > 0 ? true : false);
    }

    public bool IsPaymentPending()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEPR"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        return (Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters)) > 0 ? true : false);
    }

    public void DeleteLeaveDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DLD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        if(fOrderNo > 0)
        alParameters.Add(new SqlParameter("@OrderNo", fOrderNo));

        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }

    public bool CheckLeaveDetailExists()
    {
        bool retValue = false;
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CHKLT"));
        alParameters.Add(new SqlParameter("@LeaveDetailID", iLeaveDetailID));
        DataTable dt = ExecuteDataTable("HRspLeavePolicy", alParameters);
        if (dt.Rows.Count > 0)
        {
            retValue = true;
        }
        return retValue;

    }
    public void DeleteLeaveConsequenceDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DLCD"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID ));
        alParameters.Add(new SqlParameter("@SerialNo", fSerialNo));
        ExecuteNonQuery("HRspLeavePolicy", alParameters);
    }

    public DataSet GetAllVacationPolicies()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 20));
        return ExecuteDataSet("spPayVacationPolicy", alParameters);
    }

    public string Print()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GL"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        DataTable dt= ExecuteDataTable("HRspLeavePolicy", alParameters);

        StringBuilder sb = new StringBuilder();

        sb.Append("<h3>Leave Policy Details</h3>");
        sb.Append("<table class='style1' style='font-size: 11px; line-height: 20px; border-collapse: collapse;' border='1'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold' width='120'>");
        sb.Append("Policy Name</td>");
        sb.Append("<td width='150'>");
        sb.Append(dt.Rows[0]["LeavePolicyName"] + "</td>");
        sb.Append("<td style='font-weight: bold' width='120'>");
        sb.Append("Department</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Department"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Applicable From</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["ApplicableFrom"] + "</td>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Designation</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Designation"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Default Policy</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["DefaultPolicy"] + "</td>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Employment Type</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["EmploymentType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("&nbsp;</td>");
        sb.Append("<td>");
        sb.Append("&nbsp;</td>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Project</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["ProjectName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GLDP"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        dt = ExecuteDataTable("HRspLeavePolicy", alParameters);

        sb.Append("<h3>Leave Policy Details</h3>");
        sb.Append("<table style='font-size: 11px; line-height: 20px; border-collapse: collapse;' width='100%' border='1'>");
        sb.Append("<tr style='font-weight: bold'>");
        sb.Append("<td>");
        sb.Append("Leave Type</td>");
        sb.Append("<td align='right' width='50'>");
        sb.Append("Days</td>");
        sb.Append("<td align='right' width='50'>");
        sb.Append("Monthly</td>");
        sb.Append("<td align='right' width='100'>");
        sb.Append("Carry Forward</td>");
        sb.Append("<td align='right' width='120'>");
        sb.Append("Encashable Days</td>");
        sb.Append("</tr>");

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(dt.Rows[i]["LeaveType"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["NoOfLeave"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["MonthLeave"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["CarryForwardLeave"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["EncashDays"] + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</table>");

        return sb.ToString();
    }
    public bool CheckLeaveDetailExistsForEmployee()
    {
        bool retValue = false;
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CHKE"));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        int result = ExecuteScalar("HRspLeavePolicy", alParameters).ToInt32();
        if (result > 0)
        {
            retValue = true;
        }
        else
            retValue = false;
        return retValue;

    }
}