﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCurrencyExchangeRate
/// </summary>
public class clsCurrencyExchangeRate : DL 
{
    public clsCurrencyExchangeRate()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int intCurrencyDetailID;
    private int intCurrencyID;
    private int intCompanyID;
    private double dblExchangeRate;
    private DateTime dtpExchangeDate;

    public int CurrencyDetailID
    {
        get { return intCurrencyDetailID; }
        set { intCurrencyDetailID = value; }
    }

    public int CurrencyID
    {
        get { return intCurrencyID; }
        set { intCurrencyID = value; }
    }

    public int CompanyID
    {
        get { return intCompanyID; }
        set { intCompanyID = value; }
    }

    public double ExchangeRate
    {
        get { return dblExchangeRate; }
        set { dblExchangeRate = value; }
    }

    public DateTime ExchangeDate
    {
        get { return dtpExchangeDate; }
        set { dtpExchangeDate = value; }
    }

    public DataTable getCompany()
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 1));
        return ExecuteDataTable("HRspCurrencyExchangeRate", arraylst);
    }

    public DataTable getCurrency()
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 2));
        return ExecuteDataTable("HRspCurrencyExchangeRate", arraylst);
    }

    public DataTable getCompanyCurrency(int intCompanyID)
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 3));
        arraylst.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspCurrencyExchangeRate", arraylst);
    }

    public int SaveExchangeRate()
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 4));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
        arraylst.Add(new SqlParameter("@CurrencyID", CurrencyID));
        arraylst.Add(new SqlParameter("@ExchangeRate", ExchangeRate));
        arraylst.Add(new SqlParameter("@ExchangeDate", ExchangeDate));
        return Convert.ToInt32(ExecuteScalar("HRspCurrencyExchangeRate", arraylst));
    }

    public int UpdateExchangeRate()
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 5));
        arraylst.Add(new SqlParameter("@CurrencyDetailID", CurrencyDetailID));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
        arraylst.Add(new SqlParameter("@CurrencyID", CurrencyID));
        arraylst.Add(new SqlParameter("@ExchangeRate", ExchangeRate));
        arraylst.Add(new SqlParameter("@ExchangeDate", ExchangeDate));
        return Convert.ToInt32(ExecuteScalar("HRspCurrencyExchangeRate", arraylst));
    }

    public DataTable getCompanyCurrencyDetails(int intCompanyID)
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 6));
        arraylst.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspCurrencyExchangeRate", arraylst);
    }

    public string CheckCurrencyExists(int intCompanyID, int intCurrencyID, DateTime dtpExchangeDate)
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 7));
        arraylst.Add(new SqlParameter("@CompanyID", intCompanyID));
        arraylst.Add(new SqlParameter("@CurrencyID", intCurrencyID));
        arraylst.Add(new SqlParameter("@ExchangeDate", dtpExchangeDate.ToString("dd MMM yyyy")));
        return Convert.ToString(ExecuteScalar("HRspCurrencyExchangeRate", arraylst));
    }
}
