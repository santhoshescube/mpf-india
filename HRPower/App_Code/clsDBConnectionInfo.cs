﻿using System;
using System.Configuration;

/// <summary>
/// Summary description for clsDBConnectionInfo
/// </summary>
public class clsDBConnectionInfo
{
	public clsDBConnectionInfo() { }

    /// <summary>
    /// Returns Database User ID from web.config file
    /// </summary>
    public static string DBUserID
    {
        get 
        {
            return ConfigurationManager.AppSettings["USER_ID"] == null ? null : Convert.ToString(ConfigurationManager.AppSettings["USER_ID"]);
        }
    }

    /// <summary>
    /// Returns Database Password from web.config file
    /// </summary
    public static string DBPassword
    {
        get
        {
            return ConfigurationManager.AppSettings["PASSWORD"] == null ? null : Convert.ToString(ConfigurationManager.AppSettings["PASSWORD"]);
        }
    }

    /// <summary>
    /// Returns Database Server from web.config file
    /// </summary
    public static string DBServer
    {
        get
        {
            return ConfigurationManager.AppSettings["SERVER"] == null ? null : Convert.ToString(ConfigurationManager.AppSettings["SERVER"]);
        }
    }

    /// <summary>
    /// Returns Database Name ID from web.config file
    /// </summary
    public static string DBDatabaseName
    {
        get
        {
            return ConfigurationManager.AppSettings["DATABASE"] == null ? null : Convert.ToString(ConfigurationManager.AppSettings["DATABASE"]);
        }
    }
}
