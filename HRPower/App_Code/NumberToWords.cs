﻿using System;
using System.Collections.Generic;

/// <summary>
/// Converts number (in string) to words
/// </summary>
/************************************************
 * Created By : Ratheesh
 * Created On : 30 June 2011
 ***********************************************/

public class clsNumberToWords
{
    private static string NumberToWords(string strNumberToConvert)
    {
        string strNumber = strNumberToConvert;
        string strOutputString = string.Empty;
        string strChunk = string.Empty;
        string strTensOnes, strHundreds, strTens, strOnes, strWord;
        string strTempTens, strTempOnes, strTemp;

        int intLength, intPosition, intLoops, intCounter = 1;

        intLength = strNumber.Length;
        intPosition = intLength - 3;
        intLoops = (int)intLength / 3;

        // make sure there is an extra loop added for the remaining numbers
        intLoops += intLength % 3 == 0 ? 0 : 1;

        string strRemainingPart = strNumber;
        //intLoops -= 1;

        while (intCounter <= intLoops)
        {
            if (strRemainingPart.Length <= 0)
                break;

            if (intPosition < 0)
                intPosition = 0;

            int n = strRemainingPart.Length - (intPosition < 0 ? 0 : intPosition);

            string subStr = strNumber.Substring(intPosition, n >=3 ? 3 : n);

            strRemainingPart = strRemainingPart.Remove(intPosition, strRemainingPart.Length - intPosition);

            int Len = subStr.Length;
            while (Len < 3)
            {
                subStr = "0" + subStr;
                Len = subStr.Length;
            }

            // get chunks of 3 charaters at a time padded with leading zeros
            strChunk = "000" + subStr;// strNumber.Substring(intPosition, 3);//.Substring(2, 3);
            strChunk = strChunk.Substring(strChunk.Length - 3, 3);
            
            if (strChunk != "000")
            {
                strTens = strChunk.Substring(1, 1);
                strOnes = strChunk.Substring(2, 1);
                strHundreds = strChunk.Substring(0, 1);
                strTensOnes = strChunk.Substring(1, 2);

                // if twenty or less, use the word directly from numbersTable
                if (Convert.ToInt32(strTensOnes) <= 20 || strOnes == "0")
                {
                    strTemp = GetWordFromNumberTable(strTensOnes);
                    strWord = GetWord(intCounter);
                    strOutputString = string.Format("{0}{1}{2}", strTemp, strWord, strOutputString);
                }
                else
                {
                    // break down the ones and the tens separately
                    strTempOnes = GetWordFromNumberTable(strOnes);
                    strTempTens = GetWordFromNumberTable(strTens + "0");
                    strWord = GetWord(intCounter);
                    strOutputString = string.Format(" {0}-{1}{2}{3}", strTempTens, strTempOnes, strWord, strOutputString);
                }
                // now get hundreds
                if (strHundreds != "0")
                {
                    strWord = GetWordFromNumberTable("0" + strHundreds);
                    strOutputString = string.Format(" {0} hundred {1}", strWord, strOutputString);
                }
            }
            // increment counter by 1
            intCounter += 1;
            // decrement position by 3
            intPosition -= 3;
        }

        // remove any double spaces
        strOutputString.Replace("  ", " ").Trim();

        return strOutputString;
    }
    private static string GetRightFraction(string number)
    {
        string outputString = string.Empty;
        string strFirstChar = number.Substring(0, 1);
        if (strFirstChar == "0")
        {
            int length = number.Length;
            int counter = 1;
            string strOneChar;
            while (counter <= length)
            {
                strOneChar = number.Substring(counter, 1);
                outputString += strOneChar == "0" ? "zero" : GetWordFromNumberTable("0" + strOneChar);
                counter += 1;
            }

            // remove any double spaces
            outputString.Replace("  ", " ").Trim();

            if (outputString.Length > 0)
                // Capitalise first character
                outputString = outputString.Substring(0, 1).ToUpper() + outputString.Substring(1, outputString.Length);
        }
        else
        {
            outputString = NumberToWords(number);
        }

        return outputString;
    }
    /// <summary>
    /// Converts number(in string) to words
    /// </summary>
    /// <param name="stringNumber">number to be converted in string</param>
    /// <returns></returns>
    public static string ConvertNumberToWords(string stringNumber)
    {
        char[] chArrInputNumber = stringNumber.ToCharArray();
        string strValidNumbers = "0123456789.";
        bool number = true;
        foreach (char c in chArrInputNumber)
        {
            if (strValidNumbers.IndexOf(c) < 0)
            {
                number = false;
                break;
            }
        }

        if (!number)
            return string.Empty;

        while (stringNumber.StartsWith("0") || stringNumber.StartsWith(" ") || stringNumber.StartsWith("."))
        {
            stringNumber = stringNumber.Remove(0, 1);

            if (stringNumber.Length == 1)
            {
                if (stringNumber.ToInt64() == 0)
                    return "Zero";

                break;
            }
        }

        if (stringNumber.Length > 34)
            stringNumber = stringNumber.Substring(0, 33);

        string strNumber = stringNumber;

        string[] strArrNumbers = strNumber.Split('.');

        string LeftPart = string.Empty, RightPart = string.Empty;
        LeftPart = strArrNumbers[0];
        RightPart = strArrNumbers.Length == 2 ? strArrNumbers[1] : string.Empty;

        string outputString = NumberToWords(LeftPart) + (RightPart == string.Empty ? string.Empty : " " + GetRightFraction(RightPart));

        return outputString;
    }
    private static string GetWordFromNumberTable(string strNumber)
    {
        List<NumberTable> numberTable = new List<NumberTable>() { 
                new NumberTable("01", "one"),       new NumberTable("02", "two"),
                new NumberTable("03", "three"),     new NumberTable("04", "four"),
                new NumberTable("05", "five"),      new NumberTable("06", "six"),
                new NumberTable("07", "seven"),     new NumberTable("08", "eight"),
                new NumberTable("09", "nine"),      new NumberTable("10", "ten"),
                new NumberTable("11", "eleven"),    new NumberTable("12", "twelve"),
                new NumberTable("13", "thirteen"),  new NumberTable("14", "fourteen"),
                new NumberTable("15", "fiftieen"),  new NumberTable("16", "sixteen"),
                new NumberTable("17", "seventeen"), new NumberTable("18", "eighteen"),
                new NumberTable("19", "nineteen"),  new NumberTable("20", "twenty"),
                new NumberTable("30", "thirty"),    new NumberTable("40", "fourty"),
                new NumberTable("50", "fifty"),     new NumberTable("60", "sixty"),
                new NumberTable("70", "seventy"),   new NumberTable("80", "eighty"),
                new NumberTable("90", "ninety")
            };

        string returnValue = string.Empty;

        var temp = numberTable.Find(n => n.Number.ToInt32() == strNumber.ToInt32());
        if (temp != null)
        {
            returnValue = temp.NumberInWord;
            temp = null;
        }

        return returnValue;
    }
    private static string GetWord(int position)
    {
        string strReturnValue = string.Empty;

        switch (position)
        {
            case 1:
                strReturnValue = string.Empty;
                break;
            case 2:
                strReturnValue = " thousand ";
                break;
            case 3:
                strReturnValue = " million ";
                break;
            case 4:
                strReturnValue = " billion ";
                break;
            case 5:
                strReturnValue = " trillion ";
                break;
            case 6:
                strReturnValue = " quadrillion ";
                break;
            case 7:
                strReturnValue = " quintillion ";
                break;
            case 8:
                strReturnValue = " sextillion ";
                break;
            case 9:
                strReturnValue = " septillion ";
                break;
            case 10:
                strReturnValue = " octillion ";
                break;
            case 11:
                strReturnValue = " nonillion ";
                break;
            case 12:
                strReturnValue = " decillion ";
                break;
            case 13:
                strReturnValue = " undecillion ";
                break;
        }

        return strReturnValue;
    }
}

public class NumberTable
{
    public string Number { get; set; }
    public string NumberInWord { get; set; }

    public NumberTable(string number, string numberInWord)
    {
        this.Number = number;
        this.NumberInWord = numberInWord;
    }
}
