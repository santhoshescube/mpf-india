﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using Microsoft.VisualBasic.Devices;
using System.Globalization;
using System.IO;
using System.Text;

public class clsExportToCSV : DL
{
    ArrayList alParameters;
    string strProcName = "spPayGLCodeMapping";

    public clsExportToCSV()
    {
        
    }

    public void ExportToCSV(DateTime dtTransactionDate)
    {
        DataTable dt = new DataTable();

        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@TransactionDate", dtTransactionDate));
        dt = ExecuteDataTable("spPayIntegrationFile", alParameters);

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "\\CSVFiles";

                if (!Directory.Exists(strPath))
                    Directory.CreateDirectory(strPath);

                string csvFile = strPath + "\\HRM" + dtTransactionDate.ToString("ddMMyyyy", CultureInfo.InvariantCulture) + ".CSV";
                System.IO.StreamWriter outFile;
                Computer myC = new Computer();
                outFile = myC.FileSystem.OpenTextFileWriter(csvFile, false, Encoding.ASCII);

                for (int i = 0; dt.Rows.Count - 1 >= i; i++)
                {
                    string strEDRRow = "";

                    for (int j = 0; dt.Columns.Count > j; ++j)
                    {
                        if (j == Convert.ToInt32(dt.Columns.Count) - 1)
                            strEDRRow += Convert.ToString(dt.Rows[i][j]);
                        else
                            strEDRRow += Convert.ToString(dt.Rows[i][j]) + ",";
                    }

                    outFile.WriteLine(strEDRRow);
                }
                outFile.Close();
            }
        }

               
    }
}
