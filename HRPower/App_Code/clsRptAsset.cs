﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsRptAsset
/// </summary>
public class clsRptAsset
{
    public clsRptAsset()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static DataTable GetAllCompanyAssets(int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, int CompanyID, int BranchID, bool IncludeCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@BenefitTypeID", AssetType));
        alParameters.Add(new SqlParameter("@CompanyAssetID", CompanyAsset));
        alParameters.Add(new SqlParameter("@StatusTypeID", StatusType));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", IncludeCompany));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("spPayRptAssets", alParameters);
    }

    public static DataSet DisplayCompanyAssetReport(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, int AssetType, int CompanyAsset, int StatusType, DateTime FromDate, DateTime ToDate, bool IncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        if (CompanyAsset > 0)
        {
            alParameters.Add(new SqlParameter("@Mode", 2));
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", 1));
        }
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@BenefitTypeID", AssetType));
        alParameters.Add(new SqlParameter("@CompanyAssetID", CompanyAsset));
        alParameters.Add(new SqlParameter("@StatusTypeID", StatusType));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@IncludeComp", IncludeComp));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataSet("spPayRptAssets", alParameters);
    }
}
