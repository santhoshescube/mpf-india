﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsCandidateArabicReferenceControl
/// </summary>
public class clsCandidateArabicReferenceControl:DL
{
    private System.Collections.Generic.IList<SqlParameter> lstSqlParams;

	public clsCandidateArabicReferenceControl()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int insert(string TableName, string DataTextField, string DataTextValue, string DataValueField, int DataNoValue, string PredefinedField, string ReferenceField, int referenceId, string DataTextFieldArabic, string DateTextValueArabic)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "INS"));
            alParameters.Add(new SqlParameter("@ReferenceField", ReferenceField));
            alParameters.Add(new SqlParameter("@ReferenceId", referenceId));
            alParameters.Add(new SqlParameter("@PredefinedField", PredefinedField));
            alParameters.Add(new SqlParameter("@DataTextField", DataTextField));
            alParameters.Add(new SqlParameter("@DataTextFieldArabic", DataTextFieldArabic));

            alParameters.Add(new SqlParameter("@DataValueFieldArabic", DateTextValueArabic));

            alParameters.Add(new SqlParameter("@TableName", TableName));
            alParameters.Add(new SqlParameter("@DataTextValue", DataTextValue));
            alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
            alParameters.Add(new SqlParameter("@DataNoValue", DataNoValue));
            return Convert.ToInt32(ExecuteScalar("HRSpCandidateArabicReference", alParameters));
        }
        catch (Exception ex)
        {
            return -1;
        }


    }
    public int Delete(string TableName, string DataValueField, int DataNoValue)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "DEL"));
            alParameters.Add(new SqlParameter("@TableName", TableName));
            alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
            alParameters.Add(new SqlParameter("@DataNoValue", DataNoValue));            
            return ExecuteNonQuery("HRSpCandidateArabicReference", alParameters);
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    public DataSet Select(string TableName, string DataTextField, string DataValueField, bool OrderByDataTextField, int iAddition)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "selectall"));
        alParameters.Add(new SqlParameter("@TableName", TableName));
        alParameters.Add(new SqlParameter("@DataTextField", DataTextField));
        alParameters.Add(new SqlParameter("@Addition", iAddition));
        if (OrderByDataTextField)
            lstSqlParams.Add(new SqlParameter("@OrderField", DataTextField));
        else
            lstSqlParams.Add(new SqlParameter("@OrderField", DataValueField));

        return ExecuteDataSet("HRSpReference", alParameters);
    }
    public DataTable Select(string TableName, string DataValueField, string DataTextField, string ReferenceField, int ReferenceId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GET"));
        alParameters.Add(new SqlParameter("@TableName", TableName));
        alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
        alParameters.Add(new SqlParameter("@DataTextField", DataTextField));
        alParameters.Add(new SqlParameter("@ReferenceField", ReferenceField));
        alParameters.Add(new SqlParameter("@ReferenceId", ReferenceId));
        return ExecuteDataTable("HRSpCandidateArabicReference", alParameters);
    }

    public DataTable SelectById(string TableName, string DataValueField, string DataTextField, int DataNoValue, string DataTextFieldArabic)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GETBYID"));
        alParameters.Add(new SqlParameter("@TableName", TableName));
        alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
        alParameters.Add(new SqlParameter("@DataTextField", DataTextField));
        alParameters.Add(new SqlParameter("@DataNovalue", DataNoValue));
        alParameters.Add(new SqlParameter("@DataTextFieldArabic", @DataTextFieldArabic));
        return ExecuteDataTable("HRSpCandidateArabicReference", alParameters);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="TableName"></param>
    /// <param name="DataValueField"></param>
    /// <param name="DataTextField"></param>
    /// <param name="FilterField"></param>
    /// <param name="FilterValue"></param>
    /// <param name="ExcludeField"></param>
    /// <param name="ExcludeValues"></param>
    /// <returns></returns>
    public DataTable Select(string TableName, string DataValueField, string DataTextField, string FilterField, int FilterValue, string ExcludeField, string ExcludeValues, string DataTextFieldArb)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GET"));
        alParameters.Add(new SqlParameter("@TableName", TableName));
        alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
        alParameters.Add(new SqlParameter("@DataTextField", DataTextField));
        alParameters.Add(new SqlParameter("@ReferenceField", FilterField));
        alParameters.Add(new SqlParameter("@ReferenceId", FilterValue));
        alParameters.Add(new SqlParameter("@ExcludeField", ExcludeField));
        alParameters.Add(new SqlParameter("@ExcludeValues", ExcludeValues));
        alParameters.Add(new SqlParameter("@DataTextFieldArabic", DataTextFieldArb));
        DataTable dt = ExecuteDataTable("HRSpCandidateArabicReference", alParameters);

        return dt;
    }

    public bool IsPredefinedValue(string TableName, string DataValueField, string DataNoValue, string PredefinedField)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "IPV"));
        alParameters.Add(new SqlParameter("@PredefinedField", PredefinedField));
        alParameters.Add(new SqlParameter("@TableName", TableName));
        alParameters.Add(new SqlParameter("@DataValueField", DataValueField));
        alParameters.Add(new SqlParameter("@DataNoValue", DataNoValue));

        object result = ExecuteScalar("HRSpCandidateArabicReference", alParameters);
        if (result == DBNull.Value || result == null)
            return false;
        else
            return true;
    }
}
