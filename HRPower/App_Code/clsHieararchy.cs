﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;


/// <summary>
/// Summary description for clsHieararchy
/// </summary>
public class clsHieararchy : DL
{
    public int CompanyID { get; set; }
    public int DesignationID { get; set; }
    public int ParentId { get; set; }
    public int HierarchyId { get; set; }

    public clsHieararchy()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataTable("HRspOrganizationHierarchy", alParameters);
    }
    public void Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@CompanyID",new clsUserMaster().GetCompanyId()));
        alParameters.Add(new SqlParameter("@DesignationID", this.DesignationID));
        alParameters.Add(new SqlParameter("@ParentId", this.ParentId));
        ExecuteNonQuery(alParameters);
    }

    public DataTable GetAllParents()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID",new clsUserMaster().GetCompanyId()));


        return ExecuteDataTable("HRspCreateHierarchy", alParameters);
    }

    public DataTable GetAllDesignations()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CompanyID",new clsUserMaster().GetCompanyId()));

        return ExecuteDataTable("HRspCreateHierarchy", alParameters);
    }

    public DataTable GetAllChildren(int iParentId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GC"));
        alParameters.Add(new SqlParameter("@CompanyID", this.CompanyID));
        alParameters.Add(new SqlParameter("@ParentId", iParentId));

        return ExecuteDataTable("HRspOrganizationHierarchy", alParameters);
    }
    public DataTable GetAllTreeParents()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyID",new clsUserMaster().GetCompanyId()));
        return ExecuteDataTable("HRspCreateHierarchy", alParameters);
    }

    public DataTable GetAllNodes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", new clsUserMaster().GetCompanyId()));

        alParameters.Add(new SqlParameter("@HierarchyId", this.HierarchyId));
        return ExecuteDataTable("HRspCreateHierarchy", alParameters);
    }
    public void Begin()
    {
        BeginTransaction("HRspCreateHierarchy");
    }

    public void Delete(int iHeirarchyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@HierarchyId", iHeirarchyID));

        ExecuteNonQuery(alParameters);
    }

    public DataTable GetAllTreeParent(int intCompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SP"));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspOrganizationHierarchy", alParameters);
    }

    public DataTable GetAllNode(int intEmployeeID, int intCompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SC"));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        return ExecuteDataTable("HRspOrganizationHierarchy", alParameters);
    }



    public static DataTable GetHierarchy(int ParentDesignationID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GH"));
        alParameters.Add(new SqlParameter("@CompanyID",new clsUserMaster().GetCompanyId()));
        if (ParentDesignationID > 0)
            alParameters.Add(new SqlParameter("@ParentDesignationID", ParentDesignationID));
        return new DataLayer().ExecuteDataTable("HRspOrganizationHierarchy", alParameters);

    }


    public static List<ReportingToHierarchy> GetHierarchyByReportingTo()
    {
        List<ReportingToHierarchy> ReportingTo = new List<ReportingToHierarchy>();


        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRTH"));
        DataTable dt = new DataLayer().ExecuteDataTable("HRspOrganizationHierarchy", alParameters);
        if (dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                ReportingTo.Add(new ReportingToHierarchy()
                {
                    EmployeeID = dr["EmployeeID"].ToInt32(),
                    EmployeeFirstName = dr["FirstName"].ToString(),
                    ReportingTo = dr["ReportingTo"].ToInt32(),
                    Designation = dr["Designation"].ToString(),
                });
            }
        }
        return ReportingTo;
    }








    public void Commit()
    {
        CommitTransaction();
    }

    public void Rollback()
    {
        RollbackTransaction();
    }

}
public class ReportingToHierarchy
{
    public int EmployeeID { get; set; }
    public string EmployeeFirstName { get; set; }
    public int? ReportingTo { get; set; }
    public string Designation { get; set; }
}



