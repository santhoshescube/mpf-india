﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Requests
/// </summary>
public class clsTransferRequests: DL
{
    public clsTransferRequests()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private int iEmployeeId, iStatusId, iRequestId, iPageIndex, iPageSize, iTransferType,iTransferFrom,iTransferTo, iCompanyID;
    private string sMode, sReason, sRequestedTo,sApprovedBy;
    private DateTime dtRequestedDate, dtTransferDate;

    #region properties
    public int EmployeeId
    {
        set { iEmployeeId = value;}
        get { return iEmployeeId; }
    }
    public int CompanyID
    {
        set { iCompanyID = value; }
        get { return iCompanyID; }
    }
    public string Mode
    {
        set{sMode  = value;}
        get{return sMode ;}
    }
    public int RequestId
    {
        set { iRequestId = value; }
        get { return iRequestId; }
    }
    public int StatusId
    {
        set { iStatusId = value; }
        get { return iStatusId; }
    }
    public int PageIndex
    {
        set { iPageIndex = value; }
        get { return iPageIndex; }
    }
    public int PageSize
    {
        set { iPageSize  = value; }
        get { return iPageSize; }
    }
    public int TransferType
    {
        set { iTransferType = value; }
        get { return iTransferType; }
    }
    public int TransferFrom
    {
        set { iTransferFrom = value; }
        get { return iTransferFrom ; }
    }
    public int TransferTo
    {
        set { iTransferTo = value; }
        get { return iTransferTo; }
    }
    public string Reason
    {
        set { sReason = value; }
        get {return sReason;}
    }
    public string ApprovedBy
    {
        get { return sApprovedBy ; }
        set { sApprovedBy = value; }
    }
    public string RequestedTo
    {
        get { return sRequestedTo;}
        set { sRequestedTo = value; }
    }
    public DateTime Requestdate
    {
        get { return dtRequestedDate; }
        set { dtRequestedDate = value; }
    }
    public DateTime Transferdate
    {
        get { return dtTransferDate; }
        set { dtTransferDate = value; }
    }
    #endregion

    public DataTable  GetValue()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()?1:0));
        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }
    
    public DataTable FillTranferType()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GATTS"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }
    
    //public DataTable FillTranferType()
    //{
    //    return ExecuteDataTable("HRspTransferRequest");
    //}
    /// <summary>
    /// insert and update requests.
    /// </summary>
    public int InsertUpdateRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TransferTypeId", iTransferType));
        alParameters.Add(new SqlParameter("@TransferFrom", iTransferFrom));
        alParameters.Add(new SqlParameter("@TransferTo", iTransferTo));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alParameters.Add(new SqlParameter("@TransferDate", dtTransferDate));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        if (iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        return Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
    }
    /// <summary>
    /// get requests.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()?1:0 ));

        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }

    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
        return iCount;
    }
    /// <summary>
    /// delete requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", sMode));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        ExecuteNonQuery("HRspTransferRequest", alParemeters);

    }

    /// <summary>
    /// get transfer request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }

    public bool CheckDuplication()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EXT"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TransferTypeId", iTransferType));
        alParameters.Add(new SqlParameter("@Requestid", iRequestId));

        alParameters.Add(new SqlParameter("@TransferDate", dtTransferDate));
       
        int iCount = Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
        if (iCount > 0)
            return true;
        else
            return false;

    }

    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateTransferStatus(bool IsAuthorised)
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "UPS"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        alParemeters.Add(new SqlParameter("@ApprovedBy", sApprovedBy));
        alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
        alParemeters.Add(new SqlParameter("@Reason", Reason));

        if (iStatusId != 3 && !IsAuthorised)
            alParemeters.Add(new SqlParameter("@Forwarded", 1));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        ExecuteNonQuery("HRspTransferRequest", alParemeters);
    }

    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@Requestid", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
    }

    public DataTable GetRequestedBuddyOfficalEmail()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        ExecuteNonQuery("HRspTransferRequest", alParameters);
    }


    public void TransferRequestCancelled()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        ExecuteNonQuery("HRspTransferRequest", alParameters);
    }

    public bool CheckRequestDuplication()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CDO"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TransferTypeId", iTransferType));
        alParameters.Add(new SqlParameter("@TransferDate", dtTransferDate));

        return Convert.ToBoolean(ExecuteScalar("HRspTransferRequest", alParameters));

    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsTransferRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters)) > 0 ? true : false);
    }

    public string GetRequestedTo()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRT"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspTransferRequest", alParameters));
    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }

    public bool CheckTransferExists()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "TAE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TransferTypeId", iTransferType));
        alParameters.Add(new SqlParameter("@Requestid", iRequestId));

        alParameters.Add(new SqlParameter("@TransferDate", dtTransferDate));

        int iCount = Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
        if (iCount > 0)
            return true;
        else
            return false;

    }

    public bool CheckTransferDone(long EmployeeID, int RequestId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "TRAT"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@Requestid", RequestId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
        if (iCount > 0)
            return true;
        else
            return false;

    }

    public bool CheckSalaryAlreadyProcessed(long EmployeeID,DateTime TransferDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ISP"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@TransferDate", TransferDate));

        int iCount = Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
        if (iCount > 0)
            return true;
        else
            return false;

    }

    /// <summary>
    /// Get workstatusId to check whether the employee is in Service
    /// </summary>
    /// <returns></returns>
    public int GetWorkStatusId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CWS"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspTransferRequest", alParameters));
    }

    public DataTable DisplayReasons(int iRequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Transfer));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspTransferRequest", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspTransferRequest", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GCI"));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspTransferRequest", parameters));
    }
}
