﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsEmployee
/// purpose : Handle EmployeeMaster
/// created : Binoj
/// Date    : 01.10.2010
/// </summary>

public class clsEmployee : DL
{
    #region Declarations
    int iEmployeeID;
    int iCompanyID;
    string sEmployeeNumber;
    int iSalutation;
    string sFirstName;
    string sMiddleName;
    string sLastName;
    string sFathersName;
    string sGrandFatherName;
    string sGrandGrandFathersName;
    string sEmployeeFullName;
    string sEmployeeShortName;
    string sGender;
    int iNationalityID;
    int iEthnicOriginId;
    int iReligionID;
    int iMothertongueID;
    string sLanguagesKnown;
    DateTime dDateofBirth;
    string sIdentificationMarks;
    DateTime dDateofJoining;
    DateTime dProbationEndDate;
    DateTime? dPerformanceReviewDate;
    int iDepartmentID;
    int iDesignationID;
    int iWorkingAt;
    int iVisaObtainedFrom;
    int iVisaTakenBy;
    int iEmploymentType;
    string sLocalAddress;
    string sLocalPhone;
    string sLocalMobile;
    string sLocalFax;
    string sEmail;
    string sOverseasAddress;
    string sOverseasZipCode;
    int iCountryofOriginID;
    string sOverseasPhone;
    string sOverseasMobile;
    string sOverseasFax;
    string sEmergencyContactPerson;
    string sEmergencyContactPhone;
    string sEmergencyContactMobile;
    string sEmergencyContactAddress;
    string sEmbassyRegistrationNumber;
    string sFathersJob;
    string sMothersName;
    string sMothersJob;
    int iMaritalStatusID;
    string sSpouseName;
    int iNoOfChildren;
    string sChildrenName;
    DateTime dDateOfMarrieage;
    int iWorkStatusID;
    int iTransactionTypeID;
    int iBankNameID;
    int iBankBranchID;
    string sAccountNumber;
    DateTime dRejoiningDate;
    string sEmployeeGLAccount;
    bool bPoliceClearenceReq;
    bool bPoliceClearenceCom;
    byte[] BRecentPhoto;
    byte[] BPassportPhoto;
    string sOfficialEmail;
    int iRating;
    string sNotes;
    string sBloodGroup;
    int iPolicyID;
    int iVendorID;
    int iNoticeperiod;
    int iAccountID;
    int iComBankAccId;
    string sPersonID;
    bool bIsAgent;
    Int64 IAgentCode;
    int iLeavePolicyID;
    int iPageIndex;
    int iPageSize;
    string sSearchKey = string.Empty;
    string sOfficialEmailPassword;
    int iCandidateId;
    int iRoleId;
    string sUsername;
    string sPassword;
    int iUserId;
    int iDegreeId;
    int iSpecializationId;
    string sSpecialization;
    int iReportingTo;
    
    public int EmployeeID { get { return iEmployeeID; } set { iEmployeeID = value; } }
    public int CompanyID { get { return iCompanyID; } set { iCompanyID = value; } }
    public string EmployeeNumber { get { return sEmployeeNumber; } set { sEmployeeNumber = value; } }
    public int Salutation { get { return iSalutation; } set { iSalutation = value; } }
    public string FirstName { get { return sFirstName; } set { sFirstName = value; } }
    public string MiddleName { get { return sMiddleName; } set { sMiddleName = value; } }
    public string LastName { get { return sLastName; } set { sLastName = value; } }
    public string FathersName { get { return sFathersName; } set { sFathersName = value; } }
    public string GrandFatherName { get { return sGrandFatherName; } set { sGrandFatherName = value; } }
    public string GrandGrandFathersName { get { return sGrandGrandFathersName; } set { sGrandGrandFathersName = value; } }
    public string EmployeeFullName { get { return sEmployeeFullName; } set { sEmployeeFullName = value; } }
    public string EmployeeShortName { get { return sEmployeeShortName; } set { sEmployeeShortName = value; } }
    public string Gender { get { return sGender; } set { sGender = value; } }
    public int NationalityID { get { return iNationalityID; } set { iNationalityID = value; } }
    public int EthnicOriginId { get { return iEthnicOriginId; } set { iEthnicOriginId = value; } }
    public int ReligionID { get { return iReligionID; } set { iReligionID = value; } }
    public int MothertongueID { get { return iMothertongueID; } set { iMothertongueID = value; } }
    public string LanguagesKnown { get { return sLanguagesKnown; } set { sLanguagesKnown = value; } }
    public DateTime DateofBirth { get { return dDateofBirth; } set { dDateofBirth = value; } }
    public string IdentificationMarks { get { return sIdentificationMarks; } set { sIdentificationMarks = value; } }
    public DateTime DateofJoining { get { return dDateofJoining; } set { dDateofJoining = value; } }
    public DateTime ProbationEndDate { get { return dProbationEndDate; } set { dProbationEndDate = value; } }
    public DateTime? PerformanceReviewDate { get { return dPerformanceReviewDate; } set { dPerformanceReviewDate = value; } }
    public int DepartmentID { get { return iDepartmentID; } set { iDepartmentID = value; } }
    public int DesignationID { get { return iDesignationID; } set { iDesignationID = value; } }
    public int WorkingAt { get { return iWorkingAt; } set { iWorkingAt = value; } }
    public int VisaObtainedFrom { get { return iVisaObtainedFrom; } set { iVisaObtainedFrom = value; } }
    public int VisaTakenBy { get { return iVisaTakenBy; } set { iVisaTakenBy = value; } }
    public int EmploymentType { get { return iEmploymentType; } set { iEmploymentType = value; } }
    public string LocalAddress { get { return sLocalAddress; } set { sLocalAddress = value; } }
    public string LocalPhone { get { return sLocalPhone; } set { sLocalPhone = value; } }
    public string LocalMobile { get { return sLocalMobile; } set { sLocalMobile = value; } }
    public string LocalFax { get { return sLocalFax; } set { sLocalFax = value; } }
    public string Email { get { return sEmail; } set { sEmail = value; } }
    public string OverseasAddress { get { return sOverseasAddress; } set { sOverseasAddress = value; } }
    public string OverseasZipCode { get { return sOverseasZipCode; } set { sOverseasZipCode = value; } }
    public int CountryofOriginID { get { return iCountryofOriginID; } set { iCountryofOriginID = value; } }
    public string OverseasPhone { get { return sOverseasPhone; } set { sOverseasPhone = value; } }
    public string OverseasMobile { get { return sOverseasMobile; } set { sOverseasMobile = value; } }
    public string OverseasFax { get { return sOverseasFax; } set { sOverseasFax = value; } }
    public string EmergencyContactPerson { get { return sEmergencyContactPerson; } set { sEmergencyContactPerson = value; } }
    public string EmergencyContactPhone { get { return sEmergencyContactPhone; } set { sEmergencyContactPhone = value; } }
    public string EmergencyContactMobile { get { return sEmergencyContactMobile; } set { sEmergencyContactMobile = value; } }
    public string EmergencyContactAddress { get { return sEmergencyContactAddress; } set { sEmergencyContactAddress = value; } }
    public string EmbassyRegistrationNumber { get { return sEmbassyRegistrationNumber; } set { sEmbassyRegistrationNumber = value; } }
    public string FathersJob { get { return sFathersJob; } set { sFathersJob = value; } }
    public string MothersName { get { return sMothersName; } set { sMothersName = value; } }
    public string MothersJob { get { return sMothersJob; } set { sMothersJob = value; } }
    public int MaritalStatusID { get { return iMaritalStatusID; } set { iMaritalStatusID = value; } }
    public string SpouseName { get { return sSpouseName; } set { sSpouseName = value; } }
    public int NoOfChildren { get { return iNoOfChildren; } set { iNoOfChildren = value; } }
    public string ChildrenName { get { return sChildrenName; } set { sChildrenName = value; } }
    public DateTime DateOfMarrieage { get { return dDateOfMarrieage; } set { dDateOfMarrieage = value; } }
    public int WorkStatusID { get { return iWorkStatusID; } set { iWorkStatusID = value; } }
    public int TransactionTypeID { get { return iTransactionTypeID; } set { iTransactionTypeID = value; } }
    public int BankNameID { get { return iBankNameID; } set { iBankNameID = value; } }
    public int BankBranchID { get { return iBankBranchID; } set { iBankBranchID = value; } }
    public string AccountNumber { get { return sAccountNumber; } set { sAccountNumber = value; } }
    public DateTime RejoiningDate { get { return dRejoiningDate; } set { dRejoiningDate = value; } }
    public string EmployeeGLAccount { get { return sEmployeeGLAccount; } set { sEmployeeGLAccount = value; } }
    public bool PoliceClearenceReq { get { return bPoliceClearenceReq; } set { bPoliceClearenceReq = value; } }
    public bool PoliceClearenceCom { get { return bPoliceClearenceCom; } set { bPoliceClearenceCom = value; } }
    public byte[] RecentPhoto { get { return BRecentPhoto; } set { BRecentPhoto = value; } }
    public byte[] PassportPhoto { get { return BPassportPhoto; } set { BPassportPhoto = value; } }
    public string OfficialEmail { get { return sOfficialEmail; } set { sOfficialEmail = value; } }
    public int Rating { get { return iRating; } set { iRating = value; } }
    public string Notes { get { return sNotes; } set { sNotes = value; } }
    public string BloodGroup { get { return sBloodGroup; } set { sBloodGroup = value; } }
    public int PolicyID { get { return iPolicyID; } set { iPolicyID = value; } }
    public int VendorID { get { return iVendorID; } set { iVendorID = value; } }
    public int Noticeperiod { get { return iNoticeperiod; } set { iNoticeperiod = value; } }
    public int AccountID { get { return iAccountID; } set { iAccountID = value; } }
    public int ComBankAccId { get { return iComBankAccId; } set { iComBankAccId = value; } }
    public string PersonID { get { return sPersonID; } set { sPersonID = value; } }
    public bool IsAgent { get { return bIsAgent; } set { bIsAgent = value; } }
    public Int64 AgentCode { get { return IAgentCode; } set { IAgentCode = value; } }
    public int LeavePolicyID { get { return iLeavePolicyID; } set { iLeavePolicyID = value; } }
    public int PageIndex { get { return iPageIndex; } set {  iPageIndex = value; } }
    public int PageSize { get { return iPageSize; } set { iPageSize = value; } }
    public string SearchKey { get { return sSearchKey; } set { sSearchKey = value; } }
    public string OfficialEmailPassword { get { return sOfficialEmailPassword; } set { sOfficialEmailPassword = value; } }
    public int CandidateId { get { return iCandidateId; } set { iCandidateId = value; } }
    public int RoleId { get { return iRoleId; } set { iRoleId = value; } }

    public string username { get { return sUsername; } set { sUsername = value; } }
    public string password { get { return sPassword; } set { sPassword = value; } }

    public int UserId { get { return iUserId; } set { iUserId = value; } }
    public int DegreeId { get { return iDegreeId; } set { iDegreeId = value; } }
    public int SpecializationId { get { return iSpecializationId; } set { iSpecializationId = value; } }
    public string Specialization { get { return sSpecialization; } set { sSpecialization = value; } }
    public int ReportingTo { get { return iReportingTo; } set { iReportingTo = value; } }

    public string Fromdate { get; set; }
    public int SortExpression { get; set; }
    public string SortOrder { get; set; }
#endregion
    public clsEmployee() { }

    public int Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@EmployeeNumber", sEmployeeNumber));

        if (iSalutation == -1) alParameters.Add(new SqlParameter("@Salutation", DBNull.Value));
        else alParameters.Add(new SqlParameter("@Salutation", iSalutation));

        alParameters.Add(new SqlParameter("@FirstName", sFirstName));
        alParameters.Add(new SqlParameter("@MiddleName", sMiddleName));
        alParameters.Add(new SqlParameter("@LastName", sLastName));
        alParameters.Add(new SqlParameter("@EmployeeFullName", string.Format("{0}{1}{2}", sFirstName, (sMiddleName == string.Empty ? string.Empty : " " + sMiddleName), (sLastName == string.Empty ? string.Empty : " " + sLastName))));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@WorkingAt", iCompanyID));

        if (iEmploymentType == -1) alParameters.Add(new SqlParameter("@v", DBNull.Value));
        else alParameters.Add(new SqlParameter("@ReportingTo", iReportingTo));

        if (iEmploymentType == -1) alParameters.Add(new SqlParameter("@EmploymentType", DBNull.Value));
        else alParameters.Add(new SqlParameter("@EmploymentType", iEmploymentType));

        alParameters.Add(new SqlParameter("@VendorID", (iEmploymentType == 2 ? iVendorID : 0)));
        alParameters.Add(new SqlParameter("@DateofJoining", dDateofJoining));
        alParameters.Add(new SqlParameter("@ProbationEndDate", dProbationEndDate));
        alParameters.Add(new SqlParameter("@PerformanceReviewDate", dPerformanceReviewDate));
        alParameters.Add(new SqlParameter("@Rating", iRating));
        alParameters.Add(new SqlParameter("@Noticeperiod", iNoticeperiod));

        if (iWorkStatusID == -1) alParameters.Add(new SqlParameter("@WorkStatusID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@WorkStatusID", iWorkStatusID));

        if (iDepartmentID == -1) alParameters.Add(new SqlParameter("@DepartmentID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));

        if (iDesignationID == -1) alParameters.Add(new SqlParameter("@DesignationID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));

        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        alParameters.Add(new SqlParameter("@OfficialEmail", sOfficialEmail));

        alParameters.Add(new SqlParameter("@Gender", sGender));

        if (iCountryofOriginID == -1) alParameters.Add(new SqlParameter("@CountryofOriginID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@CountryofOriginID", iCountryofOriginID));

        if (iNationalityID == -1) alParameters.Add(new SqlParameter("@NationalityID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@NationalityID", iNationalityID));

        if (iEthnicOriginId == -1) alParameters.Add(new SqlParameter("@EthnicOriginId", DBNull.Value));
        else alParameters.Add(new SqlParameter("@EthnicOriginId", iEthnicOriginId));

        if (iReligionID == -1) alParameters.Add(new SqlParameter("@ReligionID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@ReligionID", iReligionID));

        if (iMaritalStatusID == -1) alParameters.Add(new SqlParameter("@MaritalStatusID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@MaritalStatusID", iMaritalStatusID));

        alParameters.Add(new SqlParameter("@DateofBirth", dDateofBirth));

        if (iAccountID == -1) alParameters.Add(new SqlParameter("@AccountID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@AccountID", iAccountID));

        if (iTransactionTypeID == -1) alParameters.Add(new SqlParameter("@TransactionTypeID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@TransactionTypeID", iTransactionTypeID));

        switch (iTransactionTypeID)
        {
            case 1:
                iBankBranchID = iBankNameID;
                break;

            case 5:
                break;

            default:
                iBankBranchID = iComBankAccId = 0;
                break;
        }

        alParameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        alParameters.Add(new SqlParameter("@ComBankAccId", iComBankAccId));
        alParameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));
        alParameters.Add(new SqlParameter("@AccountNumber", sAccountNumber));
        alParameters.Add(new SqlParameter("@PersonID", sPersonID));

        SqlParameter photo = new SqlParameter("@RecentPhoto", BRecentPhoto);
        photo.SqlDbType = SqlDbType.Image;
        alParameters.Add(photo);

        photo = new SqlParameter("@PassportPhoto", BPassportPhoto);
        photo.SqlDbType = SqlDbType.Image;
        alParameters.Add(photo);

        alParameters.Add(new SqlParameter("@OfficialEmailPassword", sOfficialEmailPassword));

        alParameters.Add(new SqlParameter("@LocalAddress", sLocalAddress));
        alParameters.Add(new SqlParameter("@LocalPhone", sLocalPhone));
        alParameters.Add(new SqlParameter("@LocalMobile", sLocalMobile));
        alParameters.Add(new SqlParameter("@LocalFax", sLocalFax));
        alParameters.Add(new SqlParameter("@Email", sEmail));

        alParameters.Add(new SqlParameter("@EmergencyContactPerson", sEmergencyContactPerson));
        alParameters.Add(new SqlParameter("@EmergencyContactPhone", sEmergencyContactPhone));
        alParameters.Add(new SqlParameter("@EmergencyContactMobile", sEmergencyContactMobile));
        alParameters.Add(new SqlParameter("@EmergencyContactAddress", sEmergencyContactAddress));
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateId));

        if (iRoleId == -1) alParameters.Add(new SqlParameter("@RoleId", 0));
        else alParameters.Add(new SqlParameter("@RoleId", iRoleId));

        if (iDegreeId == -1) alParameters.Add(new SqlParameter("@DegreeId", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DegreeId", iDegreeId));
        if (iSpecializationId == -1) alParameters.Add(new SqlParameter("@SpecializationId ", DBNull.Value));
        else alParameters.Add(new SqlParameter("@SpecializationId ", iSpecializationId));


        alParameters.Add(new SqlParameter("@username", sUsername));
        alParameters.Add(new SqlParameter("@password", sPassword));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }

    public int Update()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@EmployeeNumber", sEmployeeNumber));

        if (iSalutation == -1) alParameters.Add(new SqlParameter("@Salutation", DBNull.Value));
        else alParameters.Add(new SqlParameter("@Salutation", iSalutation));

        alParameters.Add(new SqlParameter("@FirstName", sFirstName));
        alParameters.Add(new SqlParameter("@MiddleName", sMiddleName));
        alParameters.Add(new SqlParameter("@LastName", sLastName));
        alParameters.Add(new SqlParameter("@EmployeeFullName", string.Format("{0}{1}{2}", sFirstName, (sMiddleName == string.Empty ? string.Empty : " " + sMiddleName), (sLastName == string.Empty ? string.Empty : " " + sLastName))));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@WorkingAt", iCompanyID));
        alParameters.Add(new SqlParameter("@ReportingTo", iReportingTo));

        if (iEmploymentType == -1) alParameters.Add(new SqlParameter("@EmploymentType", DBNull.Value));
        else alParameters.Add(new SqlParameter("@EmploymentType", iEmploymentType));

        alParameters.Add(new SqlParameter("@VendorID", (iEmploymentType == 2 ? iVendorID : 0)));
        alParameters.Add(new SqlParameter("@DateofJoining", dDateofJoining));
        alParameters.Add(new SqlParameter("@ProbationEndDate", dProbationEndDate));
        alParameters.Add(new SqlParameter("@PerformanceReviewDate", dPerformanceReviewDate));
        alParameters.Add(new SqlParameter("@Rating", iRating));
        alParameters.Add(new SqlParameter("@Noticeperiod", iNoticeperiod));

        if (iWorkStatusID == -1) alParameters.Add(new SqlParameter("@WorkStatusID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@WorkStatusID", iWorkStatusID));

        if (iDepartmentID == -1) alParameters.Add(new SqlParameter("@DepartmentID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));

        if (iDesignationID == -1) alParameters.Add(new SqlParameter("@DesignationID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));

        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        if (iLeavePolicyID == -1) alParameters.Add(new SqlParameter("@LeavePolicyID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));
        //alParameters.Add(new SqlParameter("@LeavePolicyID", iLeavePolicyID));

        alParameters.Add(new SqlParameter("@OfficialEmail", sOfficialEmail));

        alParameters.Add(new SqlParameter("@Gender", sGender));

        if (iCountryofOriginID == -1) alParameters.Add(new SqlParameter("@CountryofOriginID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@CountryofOriginID", iCountryofOriginID));

        if (iNationalityID == -1) alParameters.Add(new SqlParameter("@NationalityID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@NationalityID", iNationalityID));

        if (iEthnicOriginId == -1) alParameters.Add(new SqlParameter("@EthnicOriginId", DBNull.Value));
        else alParameters.Add(new SqlParameter("@EthnicOriginId", iEthnicOriginId));

        if (iReligionID == -1) alParameters.Add(new SqlParameter("@ReligionID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@ReligionID", iReligionID));

        if (iMaritalStatusID == -1) alParameters.Add(new SqlParameter("@MaritalStatusID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@MaritalStatusID", iMaritalStatusID));

        alParameters.Add(new SqlParameter("@DateofBirth", dDateofBirth));

        if (iAccountID == -1) alParameters.Add(new SqlParameter("@AccountID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@AccountID", iAccountID));

        if (iTransactionTypeID == -1) alParameters.Add(new SqlParameter("@TransactionTypeID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@TransactionTypeID", iTransactionTypeID));

        switch (iTransactionTypeID)
        {
            case 1:
                iBankBranchID = iBankNameID;
                break;

            case 5:
                break;

            default:
                iBankBranchID = iComBankAccId = 0;
                break;
        }

        if (iBankNameID == -1) alParameters.Add(new SqlParameter("@BankNameID", DBNull.Value));
        else alParameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        //alParameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        alParameters.Add(new SqlParameter("@ComBankAccId", iComBankAccId));
        alParameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));
        alParameters.Add(new SqlParameter("@AccountNumber", sAccountNumber));
        alParameters.Add(new SqlParameter("@PersonID", sPersonID));

        SqlParameter photo = new SqlParameter("@RecentPhoto", BRecentPhoto);
        photo.SqlDbType = SqlDbType.Image;
        alParameters.Add(photo);

        photo = new SqlParameter("@PassportPhoto", BPassportPhoto);
        photo.SqlDbType = SqlDbType.Image;
        alParameters.Add(photo);

        alParameters.Add(new SqlParameter("@OfficialEmailPassword", sOfficialEmailPassword));

        alParameters.Add(new SqlParameter("@LocalAddress", sLocalAddress));
        alParameters.Add(new SqlParameter("@LocalPhone", sLocalPhone));
        alParameters.Add(new SqlParameter("@LocalMobile", sLocalMobile));
        alParameters.Add(new SqlParameter("@LocalFax", sLocalFax));
        alParameters.Add(new SqlParameter("@Email", sEmail));

        alParameters.Add(new SqlParameter("@EmergencyContactPerson", sEmergencyContactPerson));
        alParameters.Add(new SqlParameter("@EmergencyContactPhone", sEmergencyContactPhone));
        alParameters.Add(new SqlParameter("@EmergencyContactMobile", sEmergencyContactMobile));
        alParameters.Add(new SqlParameter("@EmergencyContactAddress", sEmergencyContactAddress));
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateId));

        alParameters.Add(new SqlParameter("@RoleId", iRoleId));
        alParameters.Add(new SqlParameter("@username", sUsername));
        alParameters.Add(new SqlParameter("@password", sPassword));

        if (iDegreeId == -1) alParameters.Add(new SqlParameter("@DegreeId", DBNull.Value));
        else alParameters.Add(new SqlParameter("@DegreeId", iDegreeId));
        if (iSpecializationId == -1) alParameters.Add(new SqlParameter("@SpecializationId ", DBNull.Value));
        else alParameters.Add(new SqlParameter("@SpecializationId ", iSpecializationId));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }

    public int DeleteDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "DSR"));
            alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));
            return ExecuteNonQuery("HRspVacancyMaster", alparameters);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }
    public void UpdateProfile()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UP"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@LocalAddress", sLocalAddress));
        alParameters.Add(new SqlParameter("@LocalPhone", sLocalPhone));
        alParameters.Add(new SqlParameter("@LocalMobile", sLocalMobile));
        alParameters.Add(new SqlParameter("@Email", sEmail));
        ExecuteNonQuery("HRspEmployeeMaster", alParameters);
    }

    public DataSet FillComboSpecialization()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "fs"));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeId));

        return ExecuteDataSet("HRspCandidates", alparameters);
    }
  
    public bool IsUser()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEU")); // check for user
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public bool CheckSalary()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES")); // check for user
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public DataSet GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public DataSet GetAllVendors()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAV"));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public DataSet GetAllRoles()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAR"));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public DataTable GetReportingto(int intEmployeeID, int intDesignationID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "RTD"));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }
    public DataTable GetHOD(int intEmployeeID, int intDesignationID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "HOD"));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }
    public DataSet GetAllMaritalStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAMS"));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public DataSet GetAllTransactionTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GATT"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }










    public int RemoveEmployeePhoto(string Type)
    {
        ArrayList alParameters = new ArrayList();
        if (Type == "PP")
            alParameters.Add(new SqlParameter("@Mode", "DPP"));
        else
            alParameters.Add(new SqlParameter("@Mode", "DRP"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }


    public DataSet GetEmployee()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public DataSet GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@UserId", iUserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alParameters.Add(new SqlParameter("@SortOrder", SortOrder));
        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

    public string GetConfigurationValue(string sConfigurationItem)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCV"));
        alParameters.Add(new SqlParameter("@ConfigurationItem", sConfigurationItem));

        return Convert.ToString(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }

    public DateTime GetFinancialYearStartDate()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCF"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return Convert.ToDateTime(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }

    public byte[] GetEmployeeRecentPhoto()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GERP"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        object RecentPhoto = ExecuteScalar("HRspEmployeeMaster", alParameters);

        if (RecentPhoto != DBNull.Value)
            return (byte[])RecentPhoto;
        else
            return null;
    }

    public byte[] GetEmployeePassportPhoto()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEPP"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        object PassportPhoto = ExecuteScalar("HRspEmployeeMaster", alParameters);

        if (PassportPhoto != DBNull.Value)
            return (byte[])PassportPhoto;
        else
            return null;
    }

    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }

    public bool Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public bool IsEmployeeIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public DataTable CheckExistReferences()
    {

        string strCondition1 = "'EmployeeMaster','EmployeeDetails','PayWorkPolicyHistoryMaster','PayEmployeeLeaveSummary','PayAttendanceMaster'";
        string strCondition2 = "'AssignedTo','EmployeeID','InChargeID'";

        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 1));
        Param.Add(new SqlParameter("@Value", iEmployeeID));

        Param.Add(new SqlParameter("@ColumnsIn", strCondition2));
        Param.Add(new SqlParameter("@TablesNotIn", strCondition1));

        return ExecuteDataSet("spCheckValueExists", Param).Tables[0];
    }



    public void GetOfficialEmailDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GOED"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        using (SqlDataReader dr = ExecuteReader("HRspEmployeeMaster", alParameters))
        {
            if (dr.Read())
            {
                sOfficialEmail = Convert.ToString(dr["OfficialEmail"]);
                sOfficialEmailPassword = Convert.ToString(dr["OfficialEmailPassword"]);
            }
            dr.Close();
        }
    }

    public string GetWelcomeText()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GWT"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return Convert.ToString(ExecuteScalar("HRspEmployeeMaster", alParameters));
    }


    /// <summary>
    /// Print multiple employees
    /// </summary>
    /// <param name="sEmployeeIds">Comma seperated employee ids</param>
    public string GetPrintText(string sEmployeeIds)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPTAE"));
        alParameters.Add(new SqlParameter("@EmployeeIds", sEmployeeIds));

        DataTable dt = ExecuteDataTable("HRspEmployeeMaster", alParameters);

        StringBuilder sb = new StringBuilder();

        sb.Append("<h3 style='font-family: Verdana'>Employees</h3>");
        sb.Append("<table cellpadding='0' cellspacing='0' width='100%' style='font-family: Verdana; font-size: 11px;'>");

        foreach (DataRow dw in dt.Rows)
        {
            sb.Append("<tr>");
            sb.Append("<td style='border-bottom: 1px solid #F0F0F0'>");
            sb.Append("<table width='100%' style='font-size: 11px; line-height: 20px;'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-size: 13px; font-weight: bold'>");
            sb.Append(string.Format("{0} {1} {2} {3}</td>", dw["Salutation"], dw["FirstName"], dw["MiddleName"], dw["LastName"]));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");

            sb.Append(string.Format("Employee Code : {0} | Working At : {1}</td>", dw["EmployeeNumber"], dw["CompanyName"]));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(string.Format("Date of Joining : {0} | Employment Type : {1}</td>", dw["DateofJoining"], dw["EmploymentType"]));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(string.Format("Department      : {0} | Designation : {1}</td>", dw["Department"], dw["Designation"]));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(string.Format("Work Policy      : {0} | Leave Policy : {1}</td>", dw["Workpolicy"], dw["LeavePolicyName"]));
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(string.Format("Work Status       : {0}</td>", dw["WorkStatus"]));
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
        }

        sb.Append("</table>");

        return sb.ToString();
    }
    public string CreateSelectedEmployeesContent(string sEmployeeIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@EmployeeIds", sEmployeeIds));

        DataTable dt = ExecuteDataTable("HRspEmployeeMaster", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Employee Information</td></tr>");
        sb.Append("<tr><td>");
        //sb.Append("<table width='100%' style='font: 11px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee Number</td><td width='150px'>Name</td><td width='150px'>Working At</td><td width='100px'>Employment Type</td><td width='100px'>Workstatus</td><td width='100px'>Workpolicy</td><td width='150px'>Leavepolicy</td></tr>");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["EmployeeNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Salutation"]) + "-" + Convert.ToString(dt.Rows[i]["FirstName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CompanyName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EmploymentType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["WorkStatus"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Workpolicy"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LeavePolicyName"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public DataTable GetEmployeeSalaryHead(int iEmpID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAES"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmpID));

        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }


    public string GetPrintText(int iEmpID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmpID));

        DataTable dt = ExecuteDataSet("HRspEmployeeMaster", alParameters).Tables[0];

        DataTable dtsalary = GetEmployeeSalaryHead(iEmpID);

        DataRow dw = dt.Rows[0];

        StringBuilder sb = new StringBuilder();

        //sb.Append("<div style='font-family: Verdana; font-size: 11px;'>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>  <td colspan='2' style='font-weight: bold'>");

        sb.Append("<table border='0'>");
        sb.Append("<tr>");

        //return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());


        //src='<%# GetEmployeeRecentPhoto(Eval("EmployeeID"), Eval("RecentPhoto"), 100)%>'
        string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];
        if (Convert.ToString(dt.Rows[0]["Recentphoto"]) != string.Empty)
        {


            sb.Append("<td width='60px'><img src='" + "../thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&Width=70&EmployeeId=" + iEmpID + "&t=" + DateTime.Now.Ticks + "'/></td>");

        }

        else
        {
            sb.Append("<td width='60px'><img src='" + path + "thumbnail.aspx?FromDB=true&type=EmployeePassportPhoto&Width=70&EmployeeId=" + iEmpID + "&t=" + DateTime.Now.Ticks + "'/></td>");

        }
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + dw["FirstName"] + " " + dw["LastName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");














        sb.Append("<tr><td style='font-weight:bold;padding:2px;'>Employee Details</td></tr>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'> ");
        sb.Append("Employee</td>");
        sb.Append("<td>");
        sb.Append("<b>" + dw["Salutation"] + " " + dw["FirstName"] + " " + dw["MiddleName"] + " " + dw["LastName"] + "</b></td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Working At</td>");
        sb.Append("<td>");
        sb.Append(dw["CompanyName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Employee Number</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeNumber"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Department</td>");
        sb.Append("<td>");
        sb.Append(dw["Department"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Designation</td>");
        sb.Append("<td>");
        sb.Append(dw["Designation"] + "</td>");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Employment Type</td>");
        sb.Append("<td>");
        sb.Append(dw["EmploymentType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Date of Joining</td>");
        sb.Append("<td>");
        sb.Append(dw["DateofJoining"].ToDateTime().ToString("dd-MMM-yyyy") + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append("Probation End Date</td>");
        sb.Append("<td>");
        if (Convert.ToString(dw["ProbationEndDate"]) != string.Empty)
            sb.Append(dw["ProbationEndDate"].ToDateTime().ToString("dd-MMM-yyyy") + "");
        sb.Append("</td>");
        sb.Append("</tr>");


        sb.Append("</table>");












        sb.Append("<h5 >");
        sb.Append("Work Profile</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Work Status</td>");
        sb.Append("<td>");
        sb.Append(dw["WorkStatus"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Work Policy</td>");
        sb.Append("<td>");
        sb.Append(dw["WorkPolicy"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Leave Policy</td>");
        sb.Append("<td>");
        sb.Append(dw["LeavePolicy"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        sb.Append("Official Email</td>");
        sb.Append("<td>");
        sb.Append(dw["OfficialEmailID"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<h5>");
        sb.Append("Personal Facts</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Gender</td>");
        sb.Append("<td>");
        sb.Append(dw["Gender"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Nationality</td>");
        sb.Append("<td>");
        sb.Append(dw["Nationality"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Religion</td>");
        sb.Append("<td>");
        sb.Append(dw["Religion"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        sb.Append("Date of Birth</td>");
        sb.Append("<td>");
        sb.Append(dw["DateofBirth"].ToDateTime().ToString("dd-MMM-yyyy") + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");





        sb.Append("<h5>");
        sb.Append("Permanent Info</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Permanent Address</td>");
        sb.Append("<td>");
        sb.Append(dw["AddressLine1"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Permanent Phone</td>");
        sb.Append("<td>");
        sb.Append(dw["Phone"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Mobile</td>");
        sb.Append("<td>");
        sb.Append(dw["Mobile"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        sb.Append("Email</td>");
        sb.Append("<td>");
        sb.Append(dw["EmailID"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        sb.Append("<h5>");










        sb.Append("<h5>");
        sb.Append("Local Info</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Local Address</td>");
        sb.Append("<td>");
        sb.Append(dw["LocalAddress"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Phone</td>");
        sb.Append("<td>");
        sb.Append(dw["LocalPhone"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<h5>");




        sb.Append("<h5>");
        sb.Append("Emergency Info</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Emergency Address</td>");
        sb.Append("<td>");
        sb.Append(dw["EmergencyAddress"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Emergency Phone</td>");
        sb.Append("<td>");
        sb.Append(dw["EmergencyPhone"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<h5>");



        sb.Append("Accounts & Bank Identity</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append("Transaction Type</td>");
        sb.Append("<td>");
        sb.Append(dw["TransactionType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Employer Bank Name</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployerBank"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Employer Bank A/C</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployerAccountNo"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Employee Bank Name</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeBank"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("Employee Bank A/C</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeAccountNo"] + "</td>");
        sb.Append("</tr>");

        sb.Append("</table>");

        return sb.ToString();
    }
    public DataTable GetAllEmployeeInfo() // Get all Employee details
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", "GAER"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeID));
        return ExecuteDataTable("HRspEmployeeMaster", arraylst);
    }
    public DataTable GetEmployeeName() // Get all Employees
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", "SAE"));
        return ExecuteDataTable("HRspEmployeeMaster", arraylst);
    }

    public bool IsEmployeeDocsExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IED"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }


    public void DeleteLeaveSummary()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DLS"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        ExecuteNonQuery("HRspEmployeeMaster", alParameters);
    }

    public void SaveUpdatePolicyHistory() // keeping employee policy history
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@EmpID", iEmployeeID));
        alParameters.Add(new SqlParameter("@ComID", iCompanyID));

        ExecuteNonQuery("PayPolicyHistoryMaster", alParameters);
    }

    public int InsertDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "ISD"));

            alparameters.Add(new SqlParameter("@DegreeID", iDegreeId));
            alparameters.Add(new SqlParameter("@Description", sSpecialization));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }
    public int UpdateDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "USD"));

            alparameters.Add(new SqlParameter("@DegreeID", iDegreeId));
            alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));
            alparameters.Add(new SqlParameter("@Description", sSpecialization));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }


    /// <summary>
    /// Returns CompanyID of the employee
    /// </summary>
    /// <param name="EmployeeID"></param>
    /// <returns></returns>
    public int GetCompanyByEmployee(int EmployeeID)
    {
        object objCompanyID = this.ExecuteScalar("HRspEmployeeMaster", new ArrayList { 
            new SqlParameter("@Mode", "GCBE"),
            new SqlParameter("@EmployeeID", EmployeeID)
        });

        return objCompanyID.ToInt32();
    }



    public static DataSet GetAllEmployeesDocumentsDetails(int Type, string SearchCaption)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 2));
        alparameters.Add(new SqlParameter("@SearchTypeID", Type));
        alparameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alparameters.Add(new SqlParameter("@SearchCaption", SearchCaption));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRSpEmployeeReports", alparameters);
    }
    public static DataSet GetSingleEmployeesDocumentsDetails(int Type, string SearchCaption, int EmployeeID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 3));
        alparameters.Add(new SqlParameter("@SearchTypeID", Type));
        alparameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alparameters.Add(new SqlParameter("@SearchCaption", SearchCaption));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRSpEmployeeReports", alparameters);
    }
    //--------------------------KPI/KRA----------------------------------
    public static DataTable GetKpi()
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKPI"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspEmployeeMaster", arr);
    }

    public static DataTable GetKra()
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKRA"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspEmployeeMaster", arr);
    }

    public static DataSet GetKpiKra(int EmployeeID)
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKK"));
        arr.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataSet("HRspEmployeeMaster", arr);
    }

    public int DeleteKpiKra(int EmployeeID)
    {
        try
        {
            ArrayList arr = new ArrayList();
            arr.Add(new SqlParameter("@Mode", "DELKPIKRA"));
            arr.Add(new SqlParameter("@EmployeeID", EmployeeID));
            object Status = ExecuteScalar("HRspEmployeeMaster", arr);
            return Status.ToInt32(); 
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public int SaveKpi(List<clsKPI> empKpis, int EmployeeID)
    {
        try
        {
            foreach (clsKPI objKpi in empKpis)
            {
                ArrayList arr = new ArrayList();
                arr.Add(new SqlParameter("@Mode", "INSKPI"));
                arr.Add(new SqlParameter("@EmployeeID", EmployeeID));
                arr.Add(new SqlParameter("@KPIId", objKpi.KPIId));
                arr.Add(new SqlParameter("@KPIValue", objKpi.KPIValue));
                ExecuteScalar("HRspEmployeeMaster", arr).ToInt32();
            }
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }


    public int SaveKra(List<clsKRA> empKras, int EmployeeID)
    {
        try
        {
            foreach (clsKRA objKra in empKras)
            {
                ArrayList arr = new ArrayList();
                arr.Add(new SqlParameter("@Mode", "INSKRA"));
                arr.Add(new SqlParameter("@EmployeeID", EmployeeID));
                arr.Add(new SqlParameter("@KRAId", objKra.KRAId));
                arr.Add(new SqlParameter("@KRAValue", objKra.KRAValue));
                ExecuteScalar("HRspEmployeeMaster", arr).ToInt32();
            }
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

   


    public DataSet GetEmployeeKPIKRA()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEK"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));       
        return ExecuteDataSet("HRspEmployeeMaster", alParameters);
    }

   
}