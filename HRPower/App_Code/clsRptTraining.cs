﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsRptTraining
/// </summary>
public class clsRptTraining:DL
{

    /*  TRAINING SCHEDULE REPORT */

    /// <summary>
    /// Fill Company
    /// </summary>
    /// <returns></returns>
    public DataTable FillCompany()
    {
        return ExecuteDataTable("HRspRptTrainingSchedule", new ArrayList
        {
            new SqlParameter("@Mode",2)
        });

    }

    /// <summary>
    /// Fill Branches
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <returns></returns>
    public DataTable FillBranches(int intCompanyID)
    {
        return ExecuteDataTable("HRspRptTrainingSchedule", new ArrayList
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@CompanyID",intCompanyID)

        });

    }

    /// <summary>
    /// Fill Other Dropdowns
    /// </summary>
    /// <returns></returns>
    public DataSet FillDropDown()
    {
        return ExecuteDataSet("HRspRptTrainingSchedule", new ArrayList
        {
            new SqlParameter("@Mode",4)

        });

    }

    /// <summary>
    /// Fill Training Topic
    /// </summary>
    /// <returns></returns>
    public DataTable FillTrainingTopic()
    {
        return ExecuteDataTable("HRspRptTrainingSchedule", new ArrayList
        {
            new SqlParameter("@Mode",5)

        });
    }

    /// <summary>
    /// Get Schedule Report
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTopicID"></param>
    /// <param name="intGroupID"></param>
    /// <param name="intStatusID"></param>
    /// <param name="intchkSFromTo"></param>
    /// <param name="intchkEFromTo"></param>
    /// <param name="dtSFromDate"></param>
    /// <param name="dtSToDate"></param>
    /// <param name="dtEFromDate"></param>
    /// <param name="dtEToDate"></param>
    /// <returns></returns>
    public DataTable GetScheduleReport(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTopicID, int intGroupID, int intStatusID, int intchkSFromTo, int intchkEFromTo, DateTime dtSFromDate, DateTime dtSToDate, DateTime dtEFromDate, DateTime dtEToDate)
    {
        return ExecuteDataTable("HRspRptTrainingSchedule", new ArrayList
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TopicID",intTopicID),
            new SqlParameter("@GroupID",intGroupID),
            new SqlParameter("@StatusID",intStatusID),
            new SqlParameter("@chkSFromTo",intchkSFromTo),
            new SqlParameter("@chkEFromTo",intchkEFromTo),
            new SqlParameter("@SFromDate",dtSFromDate),
            new SqlParameter("@SToDate",dtSToDate),
            new SqlParameter("@EFromDate",dtEFromDate),
            new SqlParameter("@EToDate",dtEToDate)

        });
    }

    /* TRAINING REGISTRATION REPORT */

    /// <summary>
    /// Fill Training Title
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTopicID"></param>
    /// <returns></returns>
    public DataTable FillTrainingTitle(int intCompanyID, int intBranchID, bool blnIncludeCompany,int intTopicID)
    {
        return ExecuteDataTable("HRspRptTrainingRegistration", new ArrayList
        {
            new SqlParameter("@Mode",2),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TopicID",intTopicID)

        });

    }

    /// <summary>
    /// Fill Employee
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTrainingId"></param>
    /// <returns></returns>
    public DataTable FillEmployee(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTrainingId)
    {
        return ExecuteDataTable("HRspRptTrainingRegistration", new ArrayList
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TrainingId",intTrainingId)

        });

    }

    /// <summary>
    /// Get Registration Report
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTrainingId"></param>
    /// <param name="intEmployeeID"></param>
    /// <param name="intchkSFromTo"></param>
    /// <param name="intchkRFromTo"></param>
    /// <param name="dtSFromDate"></param>
    /// <param name="dtSToDate"></param>
    /// <param name="dtRFromDate"></param>
    /// <param name="dtRToDate"></param>
    /// <returns></returns>
    public DataTable GetRegistrationReport(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTrainingId, int intEmployeeID, int intchkSFromTo, int intchkRFromTo, DateTime dtSFromDate, DateTime dtSToDate, DateTime dtRFromDate, DateTime dtRToDate)
    {
        return ExecuteDataTable("HRspRptTrainingRegistration", new ArrayList
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TrainingID",intTrainingId),
            new SqlParameter("@EmployeeID",intEmployeeID),
            new SqlParameter("@chkSFromTo",intchkSFromTo),
            new SqlParameter("@chkRFromTo",intchkRFromTo),
            new SqlParameter("@SFromDate",dtSFromDate),
            new SqlParameter("@SToDate",dtSToDate),
            new SqlParameter("@RFromDate",dtRFromDate),
            new SqlParameter("@RToDate",dtRToDate)

        });
    }

    /* TRAINING FEEDBACK REPORT */

    /// <summary>
    /// Fill Training Title
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTopicID"></param>
    /// <returns></returns>
    public DataTable FillTrainingTitleForFeedback(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTopicID)
    {
        return ExecuteDataTable("HRspRptTrainingFeedback", new ArrayList
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TopicID",intTopicID)

        });

    }

    /// <summary>
    /// Fill Employee
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTrainingId"></param>
    /// <param name="intTopicID"></param>
    /// <returns></returns>
    public DataTable FillEmployeeForFeedback(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTrainingId,int intTopicID)
    {
        return ExecuteDataTable("HRspRptTrainingFeedback", new ArrayList
        {
            new SqlParameter("@Mode",2),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TrainingId",intTrainingId),
            new SqlParameter("@TopicID",intTopicID)

        });

    }

    /// <summary>
    /// Get Feedback Report
    /// </summary>
    /// <param name="intCompanyID"></param>
    /// <param name="intBranchID"></param>
    /// <param name="blnIncludeCompany"></param>
    /// <param name="intTopicID"></param>
    /// <param name="intTrainingID"></param>
    /// <param name="intEmployeeID"></param>
    /// <returns></returns>
    public DataTable GetFeedbackReport(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intTopicID,int intTrainingID,int intEmployeeID)
    {
        return ExecuteDataTable("HRspRptTrainingFeedback", new ArrayList
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@TopicID",intTopicID),
            new SqlParameter("@TrainingID",intTrainingID),
            new SqlParameter("@EmployeeID",intEmployeeID)
           

        });
    }


}
