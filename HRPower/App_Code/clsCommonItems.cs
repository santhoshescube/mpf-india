﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsCommonItems
/// dsd
/// </summary>
/// 

public enum UserRoles
{
    Supportadmin = 1,
    admin = 2,
    HrManager = 3,
    Interviewer = 4,
    Employee = 5,
    Candidate = 9999

}
public enum WidgetReference
{
    Attendance = 1,
    LateComers = 2,
    LeaveDetails = 3,
    DocumentExpiryAlert = 4,
    MyRequests = 5,
    Messages = 6,
    Announcements = 7,
    MyPerformance = 8,
    VacationRequest = 9,
    DocumentReturnAlert = 10,
    VacationProcess = 11,
    RecruitmentInfo = 12
}

public enum PayType
{
    Vendor = 0,
    Employee = 1
}

public enum PayCurrency
{
    Default = 0,
    CompanyCurrency = 1
}

public enum eMessageType
{
    //Commented
    None = 0,
    HiringPlan = 1,
    LeaveRequest = 2,
    LoanRequest = 3,
    SalaryAdvanceRequest = 4,
    TransferRequest = 5,
    TravelRequest = 6,
    VacancyRequest = 7,
    VacancyApproval = 8,
    AlterSalary = 9,
    JobPromotion = 10,
    LeaveApproval = 11,
    TrainingSchedule_Request = 12,
    LeaveRejection = 13,
    LeaveCancelRequest = 14,
    LeaveForward = 15,
    LeaveCancellation = 16,
    HiringPlanApproval = 17,
    LoanApproval = 18,
    LoanRejection = 19,
    LoanCancelRequest = 20,
    LoanForward = 21,
    LoanCancellation = 22,
    SalaryAdvanceApproval = 23,
    SalaryAdvanceRejection = 24,
    SalaryAdvanceCancelRequest = 25,
    SalaryAdvanceForward = 26,
    SalaryAdvanceCancellation = 27,
    TransferApproval = 28,
    TransferRejection = 29,
    TransferCancelRequest = 30,
    TransferForward = 31,
    TransferCancellation = 32,
    TravelApproval = 33,
    TravelRejection = 34,
    TravelCancelRequest = 35,
    TravelForward = 36,
    TravelCancellation = 37,
    HiringPlanCancel = 38,
    TrainingSchedule_Cancel = 39,
    Announcements = 40,
    HireCandidate = 41,
    PendingOfferStatus = 42,
    CandiateEmployeeIntegragtion = 43,
    Confirmation = 47,
    DocumentRequest = 48,
    DocumentForward = 49,
    DocumentApproval = 50,
    DocumentRejection = 51,
    DocumentCancelRequest = 52,
    DocumentCancellation = 53,
    Leave__Extension_Request = 54,
    ExtensionForward = 55,
    ExtensionApproval = 56,
    ExtensionRejection = 57,
    ExtensionCancelRequest = 58,
    ExtensionCancellation = 59,
    VisaRequest = 60,
    VisaForward = 61,
    VisaApproval = 62,
    VisaRejection = 63,
    VisaCancelRequest = 64,
    VisaCancellation = 65,
    PerformanceInitiation = 66,
    Benefits = 67,
    TimeSheetRequest = 68,
    TrainingRequest = 69,
    TrainingApproval = 70,
    TrainingRejection = 71,
    ExpenseApproval = 72,
    ExpenseRejection = 73,
    ExpenseCancelRequest = 74,
    ExpenseForward = 75,
    ExpenseCancellation = 76,
    ExpenseRequest = 77,
    PettyCash = 78,
    PettyCashDocument = 79,
    VacationRequest = 80,
    VacationApproval = 81,
    VacationRejection = 82,
    VacationCancelRequest = 83,
    VacationForward = 84,
    VacationCancellation = 85,
    Time__Extension_Request = 86,
    Time__Extension_Approval = 87,
    Time__Extension_Rejection = 88,
    Time__Extension__Cancel_Request = 89,
    Time__Extension_Forward = 90,
    Time__Extension_Cancellation = 91,
    PerformanceEvaluation = 92,
    PerformanceAction = 93,
    OfferRequest = 94,
    OfferApproval = 95,
    AttendnceRequest = 96,
    AttendnceApproval = 97,
    AttendnceRejection = 98,
    AttendnceCancelRequest = 99,
    AttendnceForward = 100,
    AttendnceCancellation = 101,
    CompensatoryOffExtensionRequest = 102,
    CompensatoryOffExtensionApproval = 103,
    CompensatoryOffExtensionRejection = 104,
    CompensatoryOffExtensionCancelRequest = 105,
    CompensatoryOffExtensionForward = 106,
    CompensatoryOffExtensionCancellation = 107,
    ResignationRequest = 108,
    ResignationApproval = 109,
    ResignationRejection = 110,
    ResignationCancelRequest = 111,
    ResignationForward = 112,
    ResignationCancellation = 113,
    TicketRequest = 114,
    TicketApproval = 115,
    TicketRejection = 116,
    TicketCancelRequest = 117,
    TicketForward = 118,
    TicketCancellation = 119,

    Compensatory__Off_Request = 120,
    Vacation__Extension_Request = 121,
    Vacation__Leave_Request = 122


}

public enum eReferenceType
{
    None = 0,
    Candidate = 1,
    Vacancy = 2,
    OfferLetter = 3,
    HiringPaln = 4,
    LeaveRequest = 5,
    PerformanceAction = 6,
    ActionId = 7,
    TrainingSchedule = 8,
    Announcements = 9,
    LoanRequest = 10,
    Resignation = 11,
    SalaryAdvanceRequest = 12,
    TransferRequest = 14,
    TravelRequest = 15,
    VisaRequest = 16,
    DocumentRequest = 17,
    LeaveExtensionRequest = 18,
    PerformanceInitiation = 19,
    Benefits = 20,
    Attendance = 21,
    TrainingRequest = 22,
    ExpenseRequest = 23,
    PettyCash = 25
}
public enum OperationType
{
    Company = 1,
    Employee = 2

}


public enum DocumentType
{
    Passport = 1,
    Visa = 2,
    Driving_License = 3,
    Health_Card = 4,
    Labour_Card = 5,
    National_ID_Card = 6,
    Insurance = 7,
    Qualification = 8,
    Insurance_Card = 9,
    Expense = 10,
    Transfer = 11,
    Lease_Agreement = 12,
    Trading_License = 13,
    Probation = 15,
    CandidateVisaTicket = 22,
    CandidateArrivalDate = 23

}
public enum RequestType
{
    Leave = 1,
    Loan = 2,
    SalaryAdvance = 3,
    Transfer = 4,
    Expense = 5,
    Visa = 6,
    Document = 7,
    LeaveExtension = 8,
    Resignation = 9,
    Training = 10,
    PerformanceAction = 11,
    OfferLetter = 13,
    AttendanceRequest = 14,
    PerformanceEvaluation = 15,
    Ticket = 16,
    Travel = 17,
    CompensatoryOffRequest = 18,
    VacationLeaveRequest = 19,

    TimeExtensionRequest = 20,
    VacationExtensionRequest = 21,
    Asset = 22,
    General = 23,
    SalaryCertificate = 24,
    SalaryBankStatement = 25,
    SalaryCardLost = 26,
    Salaryslip = 27,
    VisaLetter = 28,
    NocUmarahVisa = 29,
    CardLost = 30

}
public enum eModuleID
{
    Masters = 1,
    Employee = 2,
    Payroll = 3,
    Settings = 4,
    Reports = 5,
    Documents = 6,
    Task = 7,
    Navigator = 8,
    HomePage = 9,
    Recruitment = 10,
    Performance = 11,
    ESS = 12,
    HRSettings = 13
}

public enum eMenuID
{

    NewCompany = 1,
    Bank = 2,
    ExchangeCurrency = 3,
    WorkPolicy = 4,
    ShiftPolicy = 5,
    LeavePolicy = 6,
    HolidayCalender = 7,
    WorkLocation = 8,
    Currency = 9,
    DeductionPolicy = 10,
    AdditionDeduction = 11,
    OvertimePolicy = 12,
    AbsentPolicy = 13,
    CompanyAssets = 14,
    VacationPolicy = 15,
    SettlementPolicy = 16,
    HolidayPolicy = 17,
    EncashPolicy = 18,
    UnearnedPolicy = 19,
    Employee = 20,
    LeaveStructure = 21,
    SalaryStructure = 22,
    LeaveOpening = 23,
    Loan = 24,
    Attendance = 25,
    SalaryProcess = 26,
    SalaryRelease = 27,
    SalaryPayment = 28,
    SettlementProcess = 29,
    VacationProcess = 30,
    AttendanceMapping = 31,
    VacationExtension = 32,
    ChangePassword = 33,
    ConfigurationSetting = 34,
    EmailSettings = 35,
    Role = 36,
    User = 37,
    Backup = 38,
    AttendanceWizard = 39,
    SalaryStructureReport = 40,
    AttendanceReport = 41,
    PaymentReport = 42,
    LeaveSummaryReport = 43,
    PaySlip = 44,
    EmployeeDeduction = 45,
    SalaryAdvanceSummaryReport = 46,
    EmployeeProfileReport = 47,
    DocumentReport = 48,
    AssetReport = 49,
    LabourCostReport = 50,
    VacationReport = 51,
    SettlementReport = 52,
    ExpenseReport = 53,
    CTCReport = 54,
    AlertReport = 55,
    HolidayReport = 56,
    EmployeeTranferReport = 57,
    Qualification = 58,
    Visa = 59,
    DocumentReceipt = 60,
    OtherDocuments = 61,
    DocumentIssue = 62,
    DocumentRegister = 63,
    Passport = 64,
    DrivingLicense = 65,
    HealthCard = 66,
    LabourCard = 67,
    NationalIDCard = 68,
    InsuranceCard = 69,
    ProcessingTime = 70,
    TradeLicense = 71,
    LeaseAgreement = 72,
    Insurance = 73,
    DocumentRenew = 74,
    LeaveExtension = 75,
    LeaveEntry = 76,
    SalaryAdvance = 77,
    EmployeeTransfer = 78,
    AssetHandover = 79,
    ShiftSchedule = 80,
    Expense = 81,
    Deposit = 82,
    LoanRepayment = 83,
    DepositRefund = 84,
    NavigationCompany = 85,
    NavigationEmployee = 86,
    NavigationDocuments = 87,
    NavigationEasyView = 88,
    ViewEmployeeDocuments = 89,
    ViewCompanyDocuments = 90,

    //Homepage
    DashBoard = 91,
    RecentActivity = 92,
    ActiveCandidates = 93,
    ActiveVacancies = 94,
    PendingRequests = 95,
    SearchCandidates = 96,
    NewHires = 97,
    ManagerView = 120,


    //Recruitment
    JobOpening = 98,
    InterviewResult = 99,
    Candidates = 100,
    OfferLetter = 101,
    InterviewSchedule = 102,
    InterviewProcess = 103,
    GeneralExpense = 153,//141,
    CandidateVisaTicket = 140,
    CandidateReport = 128,
    JobOpeningReport = 129,
    ESSReport = 132,
    InterviewProcessReport = 133,
    InterviewScheduleReport = 134,

    /*candidates*/
    AddCandidate = 139,
    ViewCandidate = 140,
    DeleteCandidate = 141,
    PrintCandidate = 142,
    EmailCandidate = 143,


    /*OfferLetter*/
    ViewOffer = 135,
    AddOffer = 136,
    UpdateOffer = 137,
    PrintOffer = 138,
    CancelOffer = 144,




    /*Performance*/

    PerfomanceInitiation = 104,
    PerformanceEvaluation = 105,
    PerformanceAction = 106,
    PerformanceTemplate = 107,
    PerfomanceTargetBased = 108,
    AttendanceTargetBased = 139,
    PerformanceEvaluationReport = 130,
    PerformanceInitiationReport = 131,
    PerformanceActionReport = 137,
    ProjectCostReport = 143,
    GeneralExpenseReport = 144,

    /* Settings */

    CompnayHR = 109,
    BankHR = 110,
    ConfigurationHR = 111,
    announcementsHR = 112,
    RequestSettingHR = 113,
    HierarchyHR = 114,
    UserHR = 115,
    RolesHR = 116,
    MailHR = 117,
    TemplatesHR = 118,
    CalendarHR = 119,
    Projects = 142,

    // Employee
    ChangePolicy = 138,
    EmployeeNationalization = 145,

    LiveAttendanceReport =147,
    WorksheetReport =148,
    OffdayMarkReport =149,

}


public enum RequestStatus
{
    Applied = 1,
    Rejected = 3,
    Pending = 4,
    Approved = 5,
    Cancelled = 6,
    RequestForCancel = 7
}

public enum EmployeeWorkStatus
{
    Absconding = 1,
    Expired = 2,
    Retired = 3,
    Resigned = 4,
    Terminated = 5,
    InService = 6,
    Probation = 7
}

public enum CandidateStatus
{
    New = 1,
    Rejected = 2,
    WaitingList = 3,
    ShortListed = 4,
    Selected = 5,
    OfferSent = 6,
    OfferAccepted = 7,
    OfferRejected = 8,
    Hired = 9,
    RecommendToOther = 10
}
public enum ResultType
{
    Acceptable = 1,
    WaitingList = 2,
    Rejected = 3,
    RecommendToOther = 4
}
public enum OfferStatus
{
    Waitingforapproval = 1,
    Approved = 2,
    Rejected = 3
}

public enum PendingRequestType
{
    DOCUMENT = 1,
    LEAVE = 2,
    LOAN = 3,
    LEAVEEXTENSION = 4,
    TIMEEXTENSION = 5,
    VACATIONEXTENSION = 6,
    COMPENSATORYOFF = 7,
    SALARYADVANCE = 8,
    TRANSFER = 9,
    EXPENSE = 10,
    VACATIONLEAVE = 11,
    RESIGNATION = 12,
    TICKET = 13,
    TRAVEL = 14,
    VACANCY = 15
}
public enum eFormID
{
    Employee =5,
    Loan =165
}