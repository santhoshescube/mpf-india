﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for LoanRequest
/// </summary>
public class clsLoanRequest : DL
{
    private int iEmployeeId,iCompanyId, iStatusId, iRequestId, iPageIndex, iPageSize, iLoanType;
    private string sMode, sReason, sRequestedTo;
    private double dAmount;
    private DateTime dtRequestedDate, dtLoanFromDate, dtToDate;

    #region Properties
    public int EmployeeId
    {
        set { iEmployeeId = value; }
        get { return iEmployeeId; }
    }
    public int CompanyId
    {
        set { iCompanyId = value; }
        get { return iCompanyId; }
    }
    public string Mode
    {
        set { sMode = value; }
        get { return sMode; }
    }
    public int LoanType
    {
        set { iLoanType = value; }
        get { return iLoanType; }
    }
    public DateTime RequestedDate
    {
        set { dtRequestedDate = value; }
        get { return dtRequestedDate; }
    }
    public DateTime LoanFromDate
    {
        set { dtLoanFromDate = value; }
        get { return dtLoanFromDate; }
    }
    public double Amount
    {
        set { dAmount = value; }
        get { return dAmount; }
    }
    public string Reason
    {
        set { sReason = value; }
        get { return sReason; }
    }
    public string RequestedTo
    {
        set { sRequestedTo = value; }
        get { return sRequestedTo; }
    }
    public int StatusId
    {
        set { iStatusId = value; }
        get { return iStatusId; }
    }
    public int RequestId
    {
        set { iRequestId = value; }
        get { return iRequestId; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public DateTime ToDate
    {
        set { dtToDate = value; }
        get { return dtToDate; }
    }

    //Loan details
    public DataTable Installments { get; set; }
    public DateTime LoanDate { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string LoanNumber { get; set; }
    public int InterestType { get; set; }
    public int RequestedBy { get; set; }
    public int NoOfInstallments { get; set; }
    public decimal PaymentAmount { get; set; }
    public decimal InterestRate { get; set; }
    public int LoanID { get; set; }

    #endregion

    public clsLoanRequest()
    {

    }


    /// <summary>
    /// This function is used to insert and update details.
    /// </summary>
    ///<CreatedBy>Jisha Bijoy</CreatedBy>
    ///<CreatedOn>18 Oct 2010</CreatedOn>
    ///<ModifiedBy></ModifiedBy>
    ///<ModifiedOn></ModifiedOn>
    public int InsertRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LoanTypeId", LoanType));
        alParameters.Add(new SqlParameter("@FromDate", dtLoanFromDate));
        alParameters.Add(new SqlParameter("@Amount", dAmount));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        if (iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        alParameters.Add(new SqlParameter("@CompanyIDLoan", new clsUserMaster().GetCompanyId()));
        return Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters));

    }

    /// <summary>
    /// get requests.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspLoanRequest", alParameters);
    }

    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters));
        return iCount;
    }

    /// <summary>
    /// get loan request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails(int Arabic)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@IsArabic", Arabic));

        return ExecuteDataTable("HRspLoanRequest", alParameters);
    }

    /// <summary>
    /// delete requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", sMode));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        ExecuteNonQuery("HRspLoanRequest", alParemeters);

    }

    public DataTable FillLoanType()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "FLT"));
        alParemeters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HrSpBindCombo", alParemeters);
    }


    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsLoanRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters)) > 0 ? true : false);
    }

    public void SaveLoan()
    {
        ArrayList alParameters = new ArrayList();

        if (ExecuteScalar("SELECT ISNULL(LoanID,0) FROM HRLoanRequest WHERE RequestID=" + iRequestId).ToInt32() > 0)
            return;
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CompanyID", new clsUserMaster().GetCompanyId()));
        alParameters.Add(new SqlParameter("@LoanNumber", LoanNumber));
        alParameters.Add(new SqlParameter("@EmployeeID", RequestedBy));
        alParameters.Add(new SqlParameter("@LoanTypeID", LoanType));
        alParameters.Add(new SqlParameter("@LoanDate", LoanDate));
        alParameters.Add(new SqlParameter("@Amount", Amount));
        alParameters.Add(new SqlParameter("@InterestTypeID", InterestType));
        alParameters.Add(new SqlParameter("@InterestRate", InterestRate));
        alParameters.Add(new SqlParameter("@PaymentAmount", PaymentAmount));
        alParameters.Add(new SqlParameter("@DeductStartDate", StartDate));
        alParameters.Add(new SqlParameter("@DeductEndDate", EndDate));
        alParameters.Add(new SqlParameter("@NoOfInstallment", NoOfInstallments));
        alParameters.Add(new SqlParameter("@CurrencyID", 1));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@RequestID", iRequestId));
        LoanID = ExecuteScalar("spPayEmployeeLoan", alParameters).ToInt32();

        if (LoanID > 0)
            SaveInstallmentDetails();
    }


    public void SaveInstallmentDetails()
    {
        ArrayList alParameters = null;
        foreach (DataRow dr in Installments.Rows)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 3));
            alParameters.Add(new SqlParameter("@LoanID", LoanID));
            alParameters.Add(new SqlParameter("@InstallmentNo", dr["InstallmentNo"]));
            alParameters.Add(new SqlParameter("@InstallmentDate", dr["InstallmentDate"]));
            alParameters.Add(new SqlParameter("@Amount", dr["Amount"]));
            alParameters.Add(new SqlParameter("@InterestAmount", dr["InterestAmount"]));
            ExecuteNonQuery("spPayEmployeeLoan", alParameters);
        }
    }

    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateLoanStatus(bool IsAuthorised)
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "UPS"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@LoanTypeId", LoanType));
        alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        alParemeters.Add(new SqlParameter("@ApprovedBy", iEmployeeId));
        alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
        alParemeters.Add(new SqlParameter("@Reason", Reason));
        
        if (!IsAuthorised)
        {
            alParemeters.Add(new SqlParameter("@Amount", Amount));
            if (iStatusId == 5)
                alParemeters.Add(new SqlParameter("@Forwarded", 1));
            else
                alParemeters.Add(new SqlParameter("@Forwarded", 0));
        }
        else
        {
            if (iStatusId == 5)
                SaveLoan();
        }
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        ExecuteNonQuery("HRspLoanRequest", alParemeters);
    }

    /// <summary>
    /// delete requests.
    /// </summary>
    public DataTable GetLoanDetailsReport()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GLDR"));
        if (iStatusId > 0)
            alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        if (dtLoanFromDate != DateTime.MinValue)
            alParemeters.Add(new SqlParameter("@LoanFromDate", dtLoanFromDate));
        if (dtToDate != DateTime.MinValue)
            alParemeters.Add(new SqlParameter("@ToDate", dtToDate));

        return ExecuteDataTable("HRspLoanRequest", alParemeters);

    }

    /// <summary>
    /// Get Employees Currency.
    /// </summary>
    /// <returns></returns>
    public string GetCurrency()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        return Convert.ToString(ExecuteScalar("HRspLoanRequest", alParameters));
    }

    public string GetCurrencyRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GC1"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return Convert.ToString(ExecuteScalar("HRspLoanRequest", alParameters));
    }

    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters));

    }

    public DataTable GetOfficialEmail() // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }

    /// <summary>
    /// Check duplication
    /// </summary>
    /// <returns></returns>
    public bool IsLoanApplied()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CLD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@LoanTypeId", LoanType));
        alParameters.Add(new SqlParameter("@FromDate", dtLoanFromDate));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return (Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters)) > 0 ? true : false);
    }

    public bool IsHigherAuthority()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GHA"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));

        return (Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters)) > 0 ? true : false);
    }
    public int GetHighAuthority()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HAI"));
        alParameters.Add(new SqlParameter("@RequestTypeId", 2));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        return Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters));
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        ExecuteNonQuery("HRspLoanRequest", alParameters);
    }

    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLoanRequest", alParameters));

    }

    public void UpdateLoanCancellation()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        ExecuteNonQuery("HRspLoanRequest", alParameters);
    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }

    public bool CancelValidation(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLCV"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        return (ExecuteScalar("HRspLoanRequest", alParameters)).ToInt32() > 0 ? true : false;
    }

    public bool InsertValidation()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "IV"));
        alParameters.Add(new SqlParameter("@FromDate", LoanFromDate));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        return (ExecuteScalar("HRspLoanRequest", alParameters)).ToInt32() > 0 ? true : false;
    }

    public DataTable GetPreviousInstallments(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@LoanID", "0"));
        return ExecuteDataTable("spPayEmployeeLoan", alParameters);
    }


    public int GetLoanInstallmentPercentage()
    {
        string strSql = "SELECT ConfigurationValue FROM ConfigurationMaster where ConfigurationItem='LoanInstallmentAmountPercentage'";
        return ExecuteScalar(strSql).ToInt32();
    }

    public decimal GetBasicPay(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GBP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteScalar("HRspLoanRequest", alParameters).ToDecimal();
    }
    public bool CheckSalaryStrucutreExistance(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return (ExecuteScalar("HRspLoanRequest", alParameters)).ToInt32() > 0 ? true : false;
    }
    public bool IsLoanNumberExists(string strLoanNumber, int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLND"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@LoanNumber", strLoanNumber));
        return ExecuteScalar("HRspLoanRequest", alParameters).ToInt32() > 0;
    }

    public DateTime GetDateOfJoining(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GJD"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteScalar("HRspLoanRequest", alParameters).ToDateTime();
    }

    public static DataTable GetEmployeePreviousLoanDetails(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAPL"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("HRspLoanRequest", alParameters);
    }

    public DataTable DisplayReasons(int iRequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Loan));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspLoanRequest", alParameters);
    }
    public DataTable GetLoanNumber()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLNO"));
        alParameters.Add(new SqlParameter("@CompanyIDLoan", new clsUserMaster().GetCompanyId()));
        alParameters.Add(new SqlParameter("@FormID", (int)eFormID.Loan));
        return ExecuteDataTable("HRspLoanRequest", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@Company", CompanyId));
        parameters.Add(new SqlParameter("@RequestId", iRequestId)); 
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspLoanRequest", parameters);
    }
    public int GetCompany()
    {
        ArrayList alParameters1 = new ArrayList();
        alParameters1.Add(new SqlParameter("@Mode", "GCI"));
        alParameters1.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspLoanRequest", alParameters1));
        
    }
}
