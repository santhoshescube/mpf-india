﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/* Created By   :   Sruthy K
 * Created Date :   13 Oct 2013
 * Purpose      :   Performance Action
 * */
public class clsAction : DataLayer
{

    public int PerformanceActionID { get; set; }
    public long EmployeeID { get; set; }
    public long? ActionById { get; set; }
    public DateTime ActionDate { get; set; }
    public string Remarks { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public DateTime WithEffectDate { get; set; }
    public bool IsApproved { get; set; }


    public int PerformanceActionDetailID { get; set; }
    public bool IsSalaryAltered { get; set; }
    public bool IsJobPromotion { get; set; }
    public decimal? PreviousNetSalary { get; set; }
    public decimal? CurrentNetSalary { get; set; }
    public int? PreviousDesignationID { get; set; }
    public int? CurrentDesignationID { get; set; }
    public int? PreviousWorkPolicyID { get; set; }
    public int? CurrentWorkPolicyID { get; set; }
    public string Feedback { get; set; }

    public clsPerformanceSalaryStructureMaster objPerformanceSalaryStructureMaster;
    public List<clsPerformanceSalaryStructureDetails> lstPerformanceSalaryStructureDetails;
    public List<clsPerformanceSalaryOtherRemuneration> lstPerformanceSalaryOtherRemuneration;

    public clsAction()
    {
        objPerformanceSalaryStructureMaster = new clsPerformanceSalaryStructureMaster();
        lstPerformanceSalaryStructureDetails = new List<clsPerformanceSalaryStructureDetails>();
        lstPerformanceSalaryOtherRemuneration = new List<clsPerformanceSalaryOtherRemuneration>();
    }

    public static string GetEmployeeName(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 1));
        Parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspPerformanceAction", Parameters));
    }

    public static DataTable GetSalaryStructureMaster(long EmployeeID, int PerformanceActionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 2));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataTable FillCompany(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 3));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataTable FillCurrency(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 4));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataTable FillPaymentMode()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 5));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataTable FillPaymentCalculationType()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 6));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataTable FillAbsentPolicies()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 7));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillEncashPolicies()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 8));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillHolidayPolicies()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 9));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillOTPolicies()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 10));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable GetEmployeeAllowanceDetails(long SalaryStructureID, int PerformanceActionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 11));
        Parameters.Add(new SqlParameter("@SalaryStructureID", SalaryStructureID));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable GetOtherRemuneration(long SalaryStructureID, int PerformanceActionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 12));
        Parameters.Add(new SqlParameter("@SalaryStructureID", SalaryStructureID));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillParticulars()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 13));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture() ));

        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillDeductionPolicy(int AdditionDeductionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 14));
        Parameters.Add(new SqlParameter("@AdditionDeductionID", AdditionDeductionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static bool IsAddition(int AddDedID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 15));
        Parameters.Add(new SqlParameter("@AdditionDeductionID", AddDedID));
        return Convert.ToBoolean(new DataLayer().ExecuteScalar("HRspPerformanceAction", Parameters));
    }
    public static int GetSalaryDay(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 16));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspPerformanceAction", Parameters).ToInt32();
    }
    public static DataTable FillOtherRemunerationParticulars()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 17));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillDesignations()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 18));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable FillWorkPolicies()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 19));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static DataTable GetCurrentWorkPolicyAndDesignation(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 20));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
    public static decimal GetCurrentSalary(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 21));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspPerformanceAction", Parameters).ToDecimal();
    }

    public static DataTable GetJobPromotionInEditMode(int PerformanceActionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 30));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public bool SaveAction()
    {
        try
        {
            BeginTransaction();

            SaveMaster();

            if (PerformanceActionID > 0)
            {
                DeleteDetails();
                SaveDetails();
                SaveSalaryStructure();
                UpdateInitiation();

                if (IsApproved && IsJobPromotion)
                    UpdateJobPromotion();
            }

            CommitTransaction();
            return true;
        }
        catch
        {
            RollbackTransaction();
            return false;
        }
    }

    private void UpdateJobPromotion()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 39));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@CurrentWorkPolicyID", CurrentWorkPolicyID > 0 ? CurrentWorkPolicyID : null));
        Parameters.Add(new SqlParameter("@CurrentDesignationID", CurrentDesignationID > 0 ? CurrentDesignationID : null));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void UpdateInitiation()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 29));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void SaveSalaryStructure()
    {
        DeletePerformanceSalary();
        if (IsSalaryAltered)
        {
            SaveSalaryMaster();
            if (objPerformanceSalaryStructureMaster.SalaryStructureID > 0)
            {
                if (IsApproved)
                    DeleteSalary();
                SaveSalaryDetails();
                if (IsApproved)
                    SaveSalaryStructureHistory();
            }
        }
    }

    private void SaveSalaryStructureHistory()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 37));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@SalaryStructureID", objPerformanceSalaryStructureMaster.SalaryStructureID));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void DeletePerformanceSalary()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 26));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void DeleteSalary()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 33));
        Parameters.Add(new SqlParameter("@SalaryStructureID", objPerformanceSalaryStructureMaster.SalaryStructureID));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }


    private void SaveSalaryDetails()
    {
        foreach (clsPerformanceSalaryStructureDetails objDetails in lstPerformanceSalaryStructureDetails)
        {
            ArrayList Parameters = new ArrayList();

            if (!IsApproved)
                Parameters.Add(new SqlParameter("@Mode", 27));
            else
                Parameters.Add(new SqlParameter("@Mode", 35));
            Parameters.Add(new SqlParameter("@SalaryStructureID", objPerformanceSalaryStructureMaster.SalaryStructureID));
            Parameters.Add(new SqlParameter("@SerialNo", objDetails.SerialNo));
            Parameters.Add(new SqlParameter("@AdditionDeductionID", objDetails.AdditionDeductionID));
            Parameters.Add(new SqlParameter("@CategoryID", objDetails.CategoryID));
            Parameters.Add(new SqlParameter("@Amount", objDetails.Amount));
            Parameters.Add(new SqlParameter("@AdditionDeductionPolicyId", objDetails.AdditionDeductionPolicyId));
            ExecuteNonQuery("HRspPerformanceAction", Parameters);
        }

        foreach (clsPerformanceSalaryOtherRemuneration objOtherRemuneration in lstPerformanceSalaryOtherRemuneration)
        {
            ArrayList Parameters = new ArrayList();
            if (!IsApproved)
                Parameters.Add(new SqlParameter("@Mode", 28));
            else
                Parameters.Add(new SqlParameter("@Mode", 36));
            Parameters.Add(new SqlParameter("@SalaryStructureID", objPerformanceSalaryStructureMaster.SalaryStructureID));
            Parameters.Add(new SqlParameter("@OtherRemunerationID", objOtherRemuneration.OtherRemunerationID));
            Parameters.Add(new SqlParameter("@AmountRemuneration", objOtherRemuneration.Amount));
            ExecuteNonQuery("HRspPerformanceAction", Parameters);
        }
    }

    private void SaveSalaryMaster()
    {
        ArrayList Parameters = new ArrayList();

        if (!IsApproved)
        {
            Parameters.Add(new SqlParameter("@Mode", 25));
        }
        else
        {
            Parameters.Add(new SqlParameter("@Mode", 34));
        }

        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        Parameters.Add(new SqlParameter("@EmployeeID", objPerformanceSalaryStructureMaster.EmployeeID));
        Parameters.Add(new SqlParameter("@PaymentClassificationID", objPerformanceSalaryStructureMaster.PaymentClassificationID));
        Parameters.Add(new SqlParameter("@PayCalculationTypeID", objPerformanceSalaryStructureMaster.PayCalculationTypeID));
        Parameters.Add(new SqlParameter("@SalRemarks", objPerformanceSalaryStructureMaster.Remarks));
        Parameters.Add(new SqlParameter("@SalaryDay", objPerformanceSalaryStructureMaster.SalaryDay));
        Parameters.Add(new SqlParameter("@CurrencyID", objPerformanceSalaryStructureMaster.CurrencyID));
        Parameters.Add(new SqlParameter("@OTPolicyID", objPerformanceSalaryStructureMaster.OTPolicyID > 0 ? objPerformanceSalaryStructureMaster.OTPolicyID : null));
        Parameters.Add(new SqlParameter("@AbsentPolicyID", objPerformanceSalaryStructureMaster.AbsentPolicyID > 0 ? objPerformanceSalaryStructureMaster.AbsentPolicyID : null));
        Parameters.Add(new SqlParameter("@HolidayPolicyID", objPerformanceSalaryStructureMaster.HolidayPolicyID > 0 ? objPerformanceSalaryStructureMaster.HolidayPolicyID : null));
        Parameters.Add(new SqlParameter("@EncashPolicyID", objPerformanceSalaryStructureMaster.EncashPolicyID > 0 ? objPerformanceSalaryStructureMaster.EncashPolicyID : null));
        Parameters.Add(new SqlParameter("@NetAmount", objPerformanceSalaryStructureMaster.NetAmount));
        objPerformanceSalaryStructureMaster.SalaryStructureID = ExecuteScalar("HRspPerformanceAction", Parameters).ToInt64();
    }

    private void DeleteDetails()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 23));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void SaveDetails()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 24));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        Parameters.Add(new SqlParameter("@IsSalaryAltered", IsSalaryAltered));
        Parameters.Add(new SqlParameter("@IsJobPromotion", IsJobPromotion));
        Parameters.Add(new SqlParameter("@PreviousNetSalary", PreviousNetSalary));
        Parameters.Add(new SqlParameter("@CurrentNetSalary", CurrentNetSalary));
        Parameters.Add(new SqlParameter("@PreviousDesignationID", PreviousDesignationID));
        Parameters.Add(new SqlParameter("@CurrentDesignationID", CurrentDesignationID));
        Parameters.Add(new SqlParameter("@PreviousWorkPolicyID", PreviousWorkPolicyID));
        Parameters.Add(new SqlParameter("@CurrentWorkPolicyID", CurrentWorkPolicyID));
        Parameters.Add(new SqlParameter("@Feedback", Feedback));
        ExecuteNonQuery("HRspPerformanceAction", Parameters);
    }

    private void SaveMaster()
    {
        ArrayList Parameters = new ArrayList();
        if (PerformanceActionID == 0)
            Parameters.Add(new SqlParameter("@Mode", 22));
        else
            Parameters.Add(new SqlParameter("@Mode", 31));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@ActionById", ActionById > 0 ? ActionById : null));
        Parameters.Add(new SqlParameter("@ActionDate", ActionDate));
        Parameters.Add(new SqlParameter("@FromDate", FromDate));
        Parameters.Add(new SqlParameter("@ToDate", ToDate));
        Parameters.Add(new SqlParameter("@WithEffectDate", WithEffectDate));
        Parameters.Add(new SqlParameter("@IsApproved", IsApproved));
        Parameters.Add(new SqlParameter("@ApprovedBy", ActionById > 0 ? ActionById : null));
        Parameters.Add(new SqlParameter("@Remarks", Remarks));
        PerformanceActionID = ExecuteScalar("HRspPerformanceAction", Parameters).ToInt32();
    }

    public static DataTable GetActionDetails(int PerformanceActionID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 38));
        Parameters.Add(new SqlParameter("@PerformanceActionID", PerformanceActionID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }

    public static DataSet GetDeductionPolicyDetails(int iDeductionPolicyID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 30));
        Parameters.Add(new SqlParameter("@AdditionDeductionPolicyId", iDeductionPolicyID));
        return new DataLayer().ExecuteDataSet("spPaySalaryStructure", Parameters);
    }


    public static DataTable GetAllCategories()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 40));
        return new DataLayer().ExecuteDataTable("HRspPerformanceAction", Parameters);
    }
}


public class clsPerformanceSalaryStructureMaster
{
    public long SalaryStructureID { get; set; }
    public long EmployeeID { get; set; }
    public int PaymentClassificationID { get; set; }
    public int PayCalculationTypeID { get; set; }
    public string Remarks { get; set; }
    public int SalaryDay { get; set; }
    public int CurrencyID { get; set; }
    public int? OTPolicyID { get; set; }
    public int? AbsentPolicyID { get; set; }
    public int? HolidayPolicyID { get; set; }
    public int? EncashPolicyID { get; set; }
    public decimal NetAmount { get; set; }
   
}

public class clsPerformanceSalaryStructureDetails
{
    public long SerialNo { get; set; }
    public int AdditionDeductionID { get; set; }
    public decimal Amount { get; set; }
    public int? AdditionDeductionPolicyId { get; set; }
    public int CategoryID { get; set; }

}

public class clsPerformanceSalaryOtherRemuneration
{
    public int OtherRemunerationID { get; set; }
    public decimal Amount { get; set; }
}
