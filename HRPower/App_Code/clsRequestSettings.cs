﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
/// <summary>
/// Summary description for clsRequestSettings
/// purpose : Handle Request settings
/// created : Sreelekshmi
/// Date    : 24.09.2011
/// </summary>
/// 
public class clsRequestSettings:DataLayer
{
    public int  RequestId { get; set; }
    public int  RequestTypeId { get; set; }
    public bool ReportingToRequired { get; set; }
    public bool HODRequired { get; set; }
    public bool IsLevelNeeded { get; set; }
    public bool IsLevelBasedForwarding { get; set; }
    public int  NoOfLevel { get; set; }
    public int CompanyID { get; set; }
    public int UserID { get; set; }
    public int EmployeeID { get; set; }

    public List<clsAuthorisedLevels> AuthorisedLevels { get; set; }


    public clsRequestSettings()
	{
	}

    //public void Insert()
    //{
    //    ArrayList alParameters = new ArrayList();

    //    alParameters.Add(new SqlParameter("@Mode", "I"));
       
    //    alParameters.Add(new SqlParameter("@RequestTypeId", RequestTypeId));
    //    alParameters.Add(new SqlParameter("@SuperiorId", SuperiorId));
    //    alParameters.Add(new SqlParameter("@Status", Status));
    //    alParameters.Add(new SqlParameter("@AltSuperiorId", AltSuperiorId));

    //    ExecuteNonQuery(alParameters);
    //}

 
    public DataSet GetAllDesignations()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAD"));

        return ExecuteDataSet("HRspRequestSettings", alParameters);
    }
    public DataSet GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        
        return ExecuteDataSet("HRspRequestSettings", alParameters);
    }
  

    public DataSet GetAllRequests()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GR"));
       
        return ExecuteDataSet("HRspRequestSettings", alParameters);
    }

    public DataTable  GetRequestTypedetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSE"));
        
        alParameters.Add(new SqlParameter("@RequestTypeId", RequestTypeId));
        return ExecuteDataTable("HRspRequestSettings", alParameters);
    }
   
    public DataTable GetAllCompany()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAC"));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
       
        return ExecuteDataTable("HRspRequestSetting", arraylst);
    }


    // Modified By LAxmi  for request setting s approval


    public DataTable GetAllAuthorisedEmployees()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAE"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@UserID", UserID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteDataTable("HRspRequestSetting", parameters);
    }

    public DataSet GetAllRequestTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspRequestSetting", alParameters);
    }

    public int InsertApprovalAuthority()
    {
        // INSERTION INTO REQUEST SETTINGS MASTER
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IRM"));
        parameters.Add(new SqlParameter("@RequestTypeID", RequestTypeId));
        parameters.Add(new SqlParameter("@ReportingToRequired", ReportingToRequired));
        parameters.Add(new SqlParameter("@HODRequired", HODRequired));
        parameters.Add(new SqlParameter("@IsLevelNeeded", IsLevelNeeded));
        parameters.Add(new SqlParameter("@IsLevelBasedForwarding", IsLevelBasedForwarding));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@NoOfLevel", NoOfLevel));

        RequestId =  ExecuteScalar("HRspRequestSetting", parameters).ToInt32();

        // INSERTION INTO REQUEST SETTINGS DETAILS
        foreach (clsAuthorisedLevels objLevels in AuthorisedLevels)
        {
            ArrayList parameters2 = new ArrayList();
            parameters2.Add(new SqlParameter("@Mode", "IRD"));
            parameters2.Add(new SqlParameter("@RequestID", RequestId));
            parameters2.Add(new SqlParameter("@EmployeeID", objLevels.EmployeeID));
            parameters2.Add(new SqlParameter("@LevelId", objLevels.LevelID));
            ExecuteScalar("HRspRequestSetting", parameters2).ToInt32();
        }
        return RequestId;
    }

    public DataSet GetRequestApprovals()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@RequestTypeID", RequestTypeId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataSet("HRspRequestSetting", alParameters);
    }
}

public class clsAuthorisedLevels
{
    public int LevelID { get; set; }
    public int EmployeeID { get; set; }

}