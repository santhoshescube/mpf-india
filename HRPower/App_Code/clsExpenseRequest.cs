﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsExpenseRequest
/// 
/// </summary>
public class clsExpenseRequest:DL
{

    int iRequestId, iEmployeeId, iExpenseHeadID, iStatusId, iCurrencyId, iPageIndex, iPageSize;
    DateTime dtExpenseDate,dtTravelDate;
    double dAmount, dBudgetedAmount;
    string sRequestedTo, sReason, sApprovedBy;
    bool bIsSalaryImpact;

    public int RequestId { get { return iRequestId; }set { iRequestId = value; } }
    public int EmployeeId { get { return iEmployeeId; } set { iEmployeeId = value; } }
    public int ExpenseHeadID { get { return iExpenseHeadID; } set { iExpenseHeadID = value; } }
    public int StatusId { get { return iStatusId; } set { iStatusId = value; } }
    public int CurrencyId { get { return iCurrencyId; } set { iCurrencyId = value; } }
    public DateTime ExpenseDate { get { return dtExpenseDate; } set { dtExpenseDate = value; } }
    public double Amount { get { return dAmount; } set { dAmount = value; } }
    public double BudgetedAmount { get { return dBudgetedAmount; } set { dBudgetedAmount = value; } }
    public string RequestedTo { get { return sRequestedTo; } set { sRequestedTo = value; } }
    public string Reason { get { return sReason; } set { sReason = value; } }
    public string ApprovedBy { get { return sApprovedBy; } set { sApprovedBy = value; } }
    public int PageIndex { get { return iPageIndex; } set { iPageIndex = value; } }
    public int PageSize { get { return iPageSize; } set { iPageSize = value; } }
    public DateTime TravelDate { get { return dtTravelDate; } set { dtTravelDate = value; } }
    public bool IsSalaryImpact { get { return bIsSalaryImpact; } set { bIsSalaryImpact = value; } }

    public clsExpenseRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable BindExpenseHead()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "EH"));

        return ExecuteDataTable("HRspExpenseRequest", alparameters);
    }

    public DataTable BindCurrency()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "CR"));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));

        return ExecuteDataTable("HRspExpenseRequest", alparameters);
    }

    public int AddExpenseRequest()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));

        alparameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alparameters.Add(new SqlParameter("@ExpenseDate", dtExpenseDate));
        alparameters.Add(new SqlParameter("@ExpenseHeadID", iExpenseHeadID));
        alparameters.Add(new SqlParameter("@Amount", dAmount));
        alparameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alparameters.Add(new SqlParameter("@Reason", sReason));
        alparameters.Add(new SqlParameter("@StatusId", iStatusId));
        alparameters.Add(new SqlParameter("@CurrencyId", iCurrencyId));

        return Convert.ToInt32(ExecuteScalar("HRspExpenseRequest", alparameters));
    }

    public int UpdateExpenseRequest()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));

        alparameters.Add(new SqlParameter("@RequestId", iRequestId));
        alparameters.Add(new SqlParameter("@ExpenseDate", dtExpenseDate));
        alparameters.Add(new SqlParameter("@ExpenseHeadID", iExpenseHeadID));
        alparameters.Add(new SqlParameter("@Amount", dAmount));
        alparameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alparameters.Add(new SqlParameter("@Reason", sReason));
        alparameters.Add(new SqlParameter("@CurrencyId", iCurrencyId));

        return Convert.ToInt32(ExecuteScalar("HRspExpenseRequest", alparameters));
    }

    public DataSet BindDataList()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "VIEW"));

        alparameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alparameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alparameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataSet("HRspExpenseRequest", alparameters);
    }

    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ERC"));

        alparameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        return Convert.ToInt32(ExecuteScalar("HRspExpenseRequest", alparameters));
    }

    public DataSet BindaRequestDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GE"));

        alparameters.Add(new SqlParameter("@RequestId", iRequestId));
        return ExecuteDataSet("HRspExpenseRequest", alparameters);
    }

    public void Delete()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));

        alparameters.Add(new SqlParameter("@RequestId", iRequestId));
        ExecuteNonQuery("HRspExpenseRequest", alparameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsExpenseRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspExpenseRequest", alParameters)) > 0 ? true : false);
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));

        ExecuteNonQuery("HRspExpenseRequest", alParameters);
    }

    /// <summary>
    /// Get requested employees
    /// </summary>
    /// <returns></returns>
    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspExpenseRequest", alParameters));
    }

    public void ExpenseRequestCancelled()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        ExecuteNonQuery("HRspExpenseRequest", alParameters);
    }

    /// <summary>
    /// Return Requested by ID
    /// </summary>
    /// <returns></returns>
    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return ExecuteScalar("HRspExpenseRequest", alParameters).ToInt32();
    }

    /// <summary>
    /// update status
    /// </summary>
    public void UpdateExpenseStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspExpenseRequest");
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "UPS"));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
            alParameters.Add(new SqlParameter("@StatusId", iStatusId));
            alParameters.Add(new SqlParameter("@ApprovedBy", sApprovedBy));
            alParameters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));

            if (iStatusId == 5 && IsAuthorised)
            {
                alParameters.Add(new SqlParameter("@TravelDate", dtTravelDate));
                alParameters.Add(new SqlParameter("@BudgetedAmount", dBudgetedAmount));
                alParameters.Add(new SqlParameter("@IsSalaryImpact", bIsSalaryImpact));
            }

            ExecuteNonQuery(alParameters);
            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }

    public DataTable GetRequestedBuddyOfficalEmail()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GRB"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspExpenseRequest", alParemeters);
    }
}
