﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// Summary description for clsPerformanceInitiation
/// </summary>
public class clsPerformanceInitiation : DataLayer
{
    public static int intCompanyID { get; set; }
    public static string sp = "HRSpPerformance";
    public clsPerformanceInitiation()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    public static DataTable GetAllCompanies()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 1));
        parameters.Add(new SqlParameter("@CompanyID",clsPerformanceInitiation.intCompanyID ));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }

    public static DataTable GetAllDepartments()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 2));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }

    public static DataTable GetAllTemplates(int DepartmentID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 3));
        parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }

    public static DataTable GetAllEmployees(int DepartmentID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 4));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        if (DepartmentID > 0)
            parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }





    public static DataTable GetAllEmployees(int CompanyID, int PerformanceInitiationID, int DepartmentID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 20));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitiationID));
        parameters.Add(new SqlParameter("@FromDate", FromDate));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture ()));
        parameters.Add(new SqlParameter("@ToDate", ToDate));

        if (DepartmentID > 0)
            parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }









  


    public static DataTable GetPerformanceInitiationDetails(int PerformanceInitiationID)
    {
        EmployeeInitiation c = GetPerformanceInitationDetails(PerformanceInitiationID);
        StringBuilder EvaluatorName;
        DataTable dtEmployeeList = new DataTable();
        DataRow drEmployeeList = null;
        int RowIndex = 0;

        dtEmployeeList.Columns.Add("SLNo");
        dtEmployeeList.Columns.Add("TemplateName");
        dtEmployeeList.Columns.Add("EmployeeID");
        dtEmployeeList.Columns.Add("EmployeeFullName");
        dtEmployeeList.Columns.Add("EvaluatorFullName");
     


        foreach (InitiationEmployees employee in c.InitiationResult)
        {
            RowIndex = RowIndex + 1;

            drEmployeeList = dtEmployeeList.NewRow();
            drEmployeeList["SLNo"] = RowIndex.ToString();
            drEmployeeList["TemplateName"] = employee.TemplateName;
            drEmployeeList["EmployeeID"] = employee.EmployeeID;
            drEmployeeList["EmployeeFullName"] = employee.EmployeeName;

            EvaluatorName = new StringBuilder();
            foreach (Evaluators evaluator in employee.InitiationEvaluators)
            {
                EvaluatorName.Append(evaluator.EvaluatorName);
                EvaluatorName.Append(",");

            }
            drEmployeeList["EvaluatorFullName"] = EvaluatorName.Remove(EvaluatorName.Length - 1, 1);
            dtEmployeeList.Rows.Add(drEmployeeList);
        }
        return dtEmployeeList;
    }


    public static EmployeeInitiation GetPerformanceInitationDetails(int PerformanceInitationID)
    {


        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 15));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitationID));
        DataSet ds =new DataLayer().ExecuteDataSet(sp, parameters);

   
        DataTable dtEmployeeDetails = ds.Tables[0];
        DataTable dtAllEvaluator = ds.Tables[1];
        DataRow[] drEmployeeEvaluator = null;

        EmployeeInitiation c = new EmployeeInitiation();
        c.InitiationResult = new System.Collections.Generic.List<InitiationEmployees>();

        //ddlCompany.SelectedValue = dtInitiationMainDetails.Rows[0]["CompanyID"].ToString();
        //ddlDepartment.SelectedValue = dtInitiationMainDetails.Rows[0]["DepartmentID"].ToString();
        //txtFromDate.Text = dtInitiationMainDetails.Rows[0]["FromDate"].ToString();
        //txtToDate.Text = dtInitiationMainDetails.Rows[0]["ToDate"].ToString();

        foreach (DataRow dr in dtEmployeeDetails.Rows)
        {
            List<Evaluators> Evaluators = new List<Evaluators>();
            drEmployeeEvaluator = dtAllEvaluator.Select("EmployeeID=" + dr["EmployeeID"].ToInt32());

            foreach (DataRow drEvaluator in drEmployeeEvaluator)
            {
                Evaluators.Add(new Evaluators()
                {
                    EvaluatorID = drEvaluator["EvaluatorID"].ToInt32(),
                    EvaluatorName = drEvaluator["EmployeeFullName"].ToString()
                });
            }
            c.InitiationResult.Add(new InitiationEmployees()
            {
                EmployeeID = dr["EmployeeID"].ToInt32(),
                EmployeeName = dr["EmployeeFullName"].ToString(),
                TemplateID = dr["TemplateID"].ToInt32(),
                TemplateName = dr["TemplateName"].ToString(),
                InitiationEvaluators = Evaluators

            });
        }

        return c;

    }

    public static DataTable GetPerformanceInitiationMainDetails(int PerformanceInitationID)
    {

        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 16));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitationID));
        return new DataLayer().ExecuteDataTable(sp, parameters);

    }
    public static DataTable GetPerformanceInitiationViewDetails(int PageIndex, int PageSize)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 17));
        parameters.Add(new SqlParameter("@PageIndex", PageIndex));
        parameters.Add(new SqlParameter("@PageSize", PageSize));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }
    public static int GetTotalRecordCount()
    {
        int TotalRecordCount = 0;
        TotalRecordCount = new DataLayer().ExecuteScalar(sp, new ArrayList
        {
            new SqlParameter("@Mode",18)
        }).ToInt32();
        return TotalRecordCount;
    }
    public static DataTable GetPerformanceDetailsForControl(int PerformanceInitationID, int EmployeeID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 19));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitationID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }

    public static DataTable GetAllEvaluationDetailByEmployee(int EmployeeID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 21));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@FromDate", null));
        parameters.Add(new SqlParameter("@ToDate", null));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }

    public static DataTable GetAllActions()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 23));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }


    public static DataTable GetAllEvaluators(int CompanyID, bool IncludeAllCompany)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 24));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@IncludeAllCompany", IncludeAllCompany));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable(sp, parameters);
    }
}
