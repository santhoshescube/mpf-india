﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
/// <summary>
/// Summary description for clsRptMISESSApproval
/// </summary>
public class clsRptMISESSApproval:DL
{
	public clsRptMISESSApproval()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static DataTable GetRequests(int Mode, int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        if (FromDate != DateTime.Now)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.Now)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRspRptMISESSApproval", alParameters);
    }
     public static DataSet GetRequestDetails(int RequestID, int FormID, int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", FormID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));        
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
      
        return new DataLayer().ExecuteDataSet("HRspRptMISESSApproval", alParameters);
    }
       
}
