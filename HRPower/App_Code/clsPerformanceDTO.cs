﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsPerformanceDTO
/// </summary>
public class clsPerformanceDTO:DataLayer
{
    public clsPerformanceDTO()
    {
        //
        // TODO: Add constructor logic here
        //
    }



    public int PerformanceInitiationID { get; set; }
    public int PerformanceInitiationDetailID { get; set; }
    public int CompanyID { get; set; }
    public int DepartmentID { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public EmployeeInitiation EmployeeList { get; set; }



    public int SavePerformanceInitiation()
    {
        try
        {
            //INSERTION TO HRPerformanceInitiation TABLE
            BeginTransaction();
            ArrayList arraylst = new ArrayList();
            arraylst.Add(new SqlParameter("@Mode", 5));
            arraylst.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitiationID));
            arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
            arraylst.Add(new SqlParameter("@DepartmentID", DepartmentID));
            arraylst.Add(new SqlParameter("@FromDate", FromDate));
            arraylst.Add(new SqlParameter("@ToDate", ToDate));

            PerformanceInitiationID = ExecuteScalar("HRSpPerformance", arraylst).ToInt32();


            if (PerformanceInitiationID > 0)
            {
                //INSERTION TO HRPerformanceInitiationDetails TABLE
                foreach (InitiationEmployees EmployeeDetails in EmployeeList.InitiationResult)
                {
                    ArrayList arraylst1 = new ArrayList();
                    arraylst1.Add(new SqlParameter("@Mode", 6));
                    arraylst1.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitiationID));
                    arraylst1.Add(new SqlParameter("@EmployeeID", EmployeeDetails.EmployeeID));
                    arraylst1.Add(new SqlParameter("@TemplateID", EmployeeDetails.TemplateID));
                    PerformanceInitiationDetailID = ExecuteScalar("HRSpPerformance", arraylst1).ToInt32();


                    //INSERTION TO dbo.HRPerformanceInitiationEvaluators TABLE
                    foreach (Evaluators eval in EmployeeDetails.InitiationEvaluators)
                    {
                        ArrayList arraylst2 = new ArrayList();
                        arraylst2.Add(new SqlParameter("@Mode", 7));
                        arraylst2.Add(new SqlParameter("@PerformanceInitiationDetailID", PerformanceInitiationDetailID));
                        arraylst2.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitiationID));
                        arraylst2.Add(new SqlParameter("@EvaluatorID", eval.EvaluatorID));
                        ExecuteScalar("HRSpPerformance", arraylst2).ToInt32();

                    }

                }
            }
            CommitTransaction();
          
        }
        catch(Exception ex)
        {
            RollbackTransaction();
            return 0;
        }
        //return ExecuteDataSet("HRspEncashPolicy", arraylst);

        return PerformanceInitiationID;
    }

}
public class InitiationEmployees
{
    public int EmployeeID { get; set; }
    public string EmployeeName { get; set; }
    public int TemplateID { get; set; }
    public string TemplateName { get; set; }

    public List<Evaluators> InitiationEvaluators { get; set; }


    public InitiationEmployees()
    {

        InitiationEvaluators = new List<Evaluators>();
    }
}

public class Evaluators
{
    public int EvaluatorID { get; set; }
    public string EvaluatorName { get; set; }
}

public class EmployeeInitiation
{
    public List<InitiationEmployees> InitiationResult { get; set; }

}
