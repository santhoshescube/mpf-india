﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/*
 CREATED BY :   MEGHA   
 DATE       :   19 NOV 2013
 
 */
/// <summary>
/// Summary description for clsIndividualCandidateJobs
/// </summary>
public class clsIndividualCandidateJobs : DL
{
    #region Variables
    string Procedure = "HRspIndividualCandidateJobs";
    #endregion

    #region Properties

    public int JobId { get; set; }
    public int CandidateID { get; set; }
    public int OfferID { get; set; }
    #endregion

    #region Constructors
    public clsIndividualCandidateJobs()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion Constructors

    #region Methods
    #region GetCandidateID
    /// <summary>
    /// Get offer status :accept offer,reject offer,Enhance Request
    /// </summary>
    /// <returns>datatable of status</returns>
    public Int64 GetCandidateID()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return userData[1].ToInt64();
    }
    #endregion GetCandidateID

    public DataTable GetAllJobs()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GACJ"));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable(Procedure, parameters);
    }

    public string GetHeader()
    {
        clsUserMaster objUserMaster = new clsUserMaster();

        clsReport objReport = new clsReport();

        DataTable dt = GetReportHeader();

        StringBuilder sb = new StringBuilder();

        if (dt.Rows.Count > 0)
        {
            sb.Append("<table cellpadding=\"0\" cellspacing=\"0\" width='100%' class=\"style1\" style=\"font-family: Verdana; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #660033; margin-bottom: 5px;\">");
            sb.Append("<tr valign=\"bottom\">");
            sb.Append("<td width=\"100\" style=\"padding-bottom: 5px\">");

            if (dt.Rows[0]["Logo"] != DBNull.Value)
            {
                sb.Append("<img alt=\"\" src=\"../thumbnail.aspx?FromDB=true&type=Company&CompanyId=" + Convert.ToString(dt.Rows[0]["CompanyID"]) + "&width=100&t=" + DateTime.Now.Ticks.ToString() + "\" style=\"padding: 2px; border: 1px solid #D1D1D1;\" /></td>");
            }
            else
                sb.Append("&nbsp;</td>");

            sb.Append("<td style=\"padding-bottom: 5px\">");
            sb.Append("<table class=\"style1\" width='100%'>");
            sb.Append("<tr>");
            sb.Append("<td align=\"right\">");
            sb.Append("<h3 style=\"padding: 0px; margin: 0px; color: #880000\">" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</h3></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine1"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine2"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["Phone"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
        }

        return sb.ToString();
    }

    public DataTable GetReportHeader()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GCD"));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        DataTable dt = ExecuteDataTable(Procedure, parameters);

        DataTable dtHeaderData = new DataTable();
        dtHeaderData.Columns.Add(new DataColumn("CompanyName", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("POBox", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("AddressLine1", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("AddressLine2", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Phone", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Logo", typeof(byte[])));
        dtHeaderData.Columns.Add(new DataColumn("CompanyID", typeof(int)));
        dtHeaderData.Columns.Add(new DataColumn("Footer", typeof(string)));

        byte[] bytImage = new byte[] { };

        if (dt.Rows.Count > 0)
        {
            string sAddress1 = string.Empty;
            string sAddress2 = string.Empty;
            string sPhoneEmail = string.Empty;

            if (Convert.ToString(dt.Rows[0]["Road"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Road"]);
            if (Convert.ToString(dt.Rows[0]["Area"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Area"]);
            if (Convert.ToString(dt.Rows[0]["Block"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Block"]);

            if (Convert.ToString(dt.Rows[0]["City"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["City"]);
            if (Convert.ToString(dt.Rows[0]["Province"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["Province"]);
            if (Convert.ToString(dt.Rows[0]["Country"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["Country"]);
            //if (Convert.ToString(dt.Rows[0]["POBox"]).Trim() != string.Empty) sAddress2 += ", P.O Box : " + Convert.ToString(dt.Rows[0]["POBox"]);

            if (sAddress1.Length > 0) sAddress1 = sAddress1.Remove(0, 2); // remove first comma

            if (sAddress2.Length > 0) sAddress2 = sAddress2.Remove(0, 2); // remove first comma

            if (Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim() != string.Empty) sPhoneEmail += ", Ph: " + Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim();
            if (Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim() != string.Empty) sPhoneEmail += ", Email: " + Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim();

            if (sPhoneEmail.Length > 0) sPhoneEmail = sPhoneEmail.Remove(0, 2); // remove first comma

            if (dt.Rows[0]["LogoFile"] != DBNull.Value) bytImage = (byte[])dt.Rows[0]["LogoFile"];

            DataRow dr = dtHeaderData.NewRow();

            dr["CompanyName"] = Convert.ToString(dt.Rows[0]["Name"]);
            dr["POBox"] = "P.O Box : " + Convert.ToString(dt.Rows[0]["POBox"]);
            dr["AddressLine1"] = sAddress1;
            dr["AddressLine2"] = sAddress2;
            dr["Phone"] = sPhoneEmail;
            dr["Logo"] = bytImage;
            dr["CompanyID"] = Convert.ToInt32(dt.Rows[0]["CompanyID"]);

            string sFooterData = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter);
            dr["Footer"] = sFooterData;

            dtHeaderData.Rows.Add(dr);

        }
        else
        {
            dtHeaderData.Rows.Add(dtHeaderData.NewRow());
        }

        dtHeaderData.TableName = "ReportHeaders";

        return dtHeaderData;
    }

    public DataSet GetTicketDetails(int CandidateID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GCVTD"));
        arr.Add(new SqlParameter("@CandidateID", CandidateID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspVisaTicketProcessing", arr);
    }

    #endregion Methods
}
