﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsVendor
/// Author:Thasni Latheef
/// Created Date: 06-09-2010

public class clsVendor:DL
{
	public clsVendor()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iVendorID;
    private string sVendorName;
	private string sShortName;
	private int iVendorTypeID;
	private string sContact;
	private string sMailingAddress;
	private string sEmail;
	private string sFax;
	private string sWebsite;
	private string sTelephone;
	private string sAccountNumber;
	private int iAccountID;
    private int iPayPolicyId;
	private int iBankNameId;
	private string sVendorCode;
	private string sAgentCode;
	private int iTransactionTypeID;
    private int iCountryId;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private string sSearchKey;
    public string Searchkey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }
    public int VendorID
    {
        get { return iVendorID; }
        set { iVendorID = value; }
    }
    public int PayPolicyId
    {
        get { return iPayPolicyId; }
        set { iPayPolicyId = value; }
    }
    public int VendorTypeID
    {
        get { return iVendorTypeID; }
        set { iVendorTypeID = value; }
    }
    public int AccountID
    {
        get { return iAccountID; }
        set { iAccountID = value; }
    }
    public int BankNameId
    {
        get { return iBankNameId; }
        set { iBankNameId = value; }
    }
    public int TransactionTypeID
    {
        get { return iTransactionTypeID; }
        set { iTransactionTypeID = value; }
    }

    public int CountryId
    {
        get { return iCountryId; }
        set { iCountryId = value; }
    }
    public string VendorName
    {
        get { return sVendorName; }
        set { sVendorName = value; }
    }
    public string ShortName
    {
        get { return sShortName; }
        set { sShortName = value; }
    }
    public string Contact
    {
        get { return sContact; }
        set { sContact = value; }
    }
    public string MailingAddress
    {
        get { return sMailingAddress; }
        set { sMailingAddress = value; }
    }
    public string Email
    {
        get { return sEmail; }
        set { sEmail = value; }
    }
    public string Fax
    {
        get { return sFax; }
        set { sFax = value; }
    }
    public string Website
    {
        get { return sWebsite; }
        set { sWebsite = value; }

    }
    public string Telephone
    {
        get { return sTelephone; }
        set { sTelephone = value; }
    }
    public string AccountNumber
    {
        get { return sAccountNumber; }
        set { sAccountNumber = value; }
    }
    public string VendorCode
    {
        get { return sVendorCode; }
        set { sVendorCode = value; }
    }
    public string AgentCode
    {
        get { return sAgentCode; }
        set { sAgentCode = value; }
    }
   

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
   


    public int InsertUpdateVendor(bool IsInsert)// Insert/update vendor details 
    {

        ArrayList alParameters = new ArrayList();

        if (IsInsert)

            alParameters.Add(new SqlParameter("@Mode", "I"));
        else
        {
            alParameters.Add(new SqlParameter("@Mode", "U"));
            alParameters.Add(new SqlParameter("@VendorId", iVendorID));
        }

            alParameters.Add(new SqlParameter("@VendorName", sVendorName));
            alParameters.Add(new SqlParameter("@ShortName",sShortName ));
            alParameters.Add(new SqlParameter("@VendorTypeID", iVendorTypeID));
            alParameters.Add(new SqlParameter("@Contact",sContact ));
            alParameters.Add(new SqlParameter("@MailingAddress",sMailingAddress ));
            alParameters.Add(new SqlParameter("@Email",sEmail ));
            alParameters.Add(new SqlParameter("@Fax",sFax ));
            alParameters.Add(new SqlParameter("@Website",sWebsite ));
            alParameters.Add(new SqlParameter("@Telephone",sTelephone ));
            alParameters.Add(new SqlParameter("@AccountNumber",sAccountNumber ));
            if(iAccountID==-1)
                alParameters.Add(new SqlParameter("@AccountID",  DBNull.Value));
            else
                alParameters.Add(new SqlParameter("@AccountID", iAccountID));
            if (iPayPolicyId == -1)
                alParameters.Add(new SqlParameter("@PayPolicyId", DBNull.Value));
            else
                alParameters.Add(new SqlParameter("@PayPolicyId", iPayPolicyId ));
            alParameters.Add(new SqlParameter("@BankNameId",iBankNameId ));
            alParameters.Add(new SqlParameter("@VendorCode",sVendorCode ));
            alParameters.Add(new SqlParameter("@AgentCode", sAgentCode));
            alParameters.Add(new SqlParameter("@TransactionTypeID",iTransactionTypeID ));
            alParameters.Add(new SqlParameter("@CountryId",iCountryId ));

            try
            {
                return Convert.ToInt32(ExecuteScalar("HRspVendor", alParameters));

            }
            catch(Exception ex)
            {
                return 0;
            }
    }

    public DataTable FillTransationType() // Get all transaction types
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "ST"));
        return ExecuteDataTable("HRspVendor", arraylst);
    }

    public DataTable FillBankNames() // Get all bank names with branch
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SB"));
        return ExecuteDataTable("HRspVendor", arraylst);
    }

    public DataTable FillAccountHeads() // get all account heads
    {
        ArrayList arraylst = new ArrayList();
        
        arraylst.Add(new SqlParameter("@ParentID",(iVendorTypeID==2?"21":"13")));
        return ExecuteDataTable("PayGetAccounts", arraylst);
    }
    public DataTable FillPaymentPolicies() // get all payment policies
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SPP"));
        return ExecuteDataTable("HRspVendor", arraylst);
    }

    public DataTable GetVendorDetails() // Get Details of a particular vendor
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "S"));
        arraylst.Add(new SqlParameter("@VendorId",iVendorID));
        return ExecuteDataTable("HRspVendor", arraylst);
    }

    public DataSet GetAllVendorDetails() // Get Details of all vendors
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SALL"));
        arraylst.Add(new SqlParameter("@PageIndex", iPageIndex));
        arraylst.Add(new SqlParameter("@PageSize", iPageSize));
        arraylst.Add(new SqlParameter("@SearchKey", sSearchKey));
        arraylst.Add(new SqlParameter("@SortExpression", sSortExpression));
        arraylst.Add(new SqlParameter("@SortOrder", sSortOrder));

        return ExecuteDataSet("HRspVendor", arraylst);
    }

    public int GetRecordCount() // Get Total Record count
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "RC"));
        arraylst.Add(new SqlParameter("@SearchKey", sSearchKey));
        return Convert.ToInt32(ExecuteScalar("HRspVendor", arraylst));
    }

    public bool IsVendorCodeExists(bool IsInsert) // Is vendor code exists
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CVC"));
        arraylst.Add(new SqlParameter("@VendorCode", sVendorCode));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));

        return((Convert.ToInt32(ExecuteScalar("HRspVendor", arraylst))==1?true:false));
    }
    public bool IsVendorNameExists(bool IsInsert)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CVN"));
        arraylst.Add(new SqlParameter("@VendorName", sVendorName));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));

        return ((Convert.ToInt32(ExecuteScalar("HRspVendor", arraylst)) == 1 ? true : false));
    }
    public bool IsAgentCodeExists(bool IsInsert)
    {
        if (iVendorTypeID != 4)
            return false;
        else
        {
            ArrayList arraylst = new ArrayList();

            arraylst.Add(new SqlParameter("@Mode", "CAC"));
            arraylst.Add(new SqlParameter("@AgentCode", sAgentCode));
            arraylst.Add(new SqlParameter("@VendorId", iVendorID));

            return ((Convert.ToInt32(ExecuteScalar("HRspVendor", arraylst)) == 1 ? true : false));
        }
    }
    public bool IsAccountNumberExists(bool IsInsert)
    {
        if (iVendorTypeID == 4)
        {
            return false;
        }
        else
        {
            ArrayList arraylst = new ArrayList();

            arraylst.Add(new SqlParameter("@Mode", "CAN"));
            arraylst.Add(new SqlParameter("@AccountNumber", sAccountNumber));
            arraylst.Add(new SqlParameter("@BankNameID", iBankNameId));
            arraylst.Add(new SqlParameter("@VendorId", iVendorID));

            return ((Convert.ToInt32(ExecuteScalar("HRspVendor", arraylst)) == 1 ? true : false));
        }
       
    }

    public bool DeleteVendor()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));
        try
        {
            ExecuteNonQuery("HRspVendor", arraylst);
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }

    }
    public bool IsVendorExistInEmployee()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IVE"));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));
        return Convert.ToBoolean(ExecuteScalar("HRspVendor", arraylst));      

    }
    public string GetVendorName()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SVN"));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));
        return Convert.ToString(ExecuteScalar("HRspVendor", arraylst));
    }

    public Table PrintVendor(string sVendorIds)
    {
        Table tblVendor = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblVendor.ID = "PrintVendor";

        if (sVendorIds.Contains(","))

            td.Text = CreateSelectedVendorsContent(sVendorIds);
        else
            td.Text = CreateSingleVendorContent(Convert.ToInt32(sVendorIds));
       
        tr.Cells.Add(td);
        tblVendor.Rows.Add(tr);

        return tblVendor;
    }

    public string CreateSingleVendorContent(int iVendorID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "S"));
        arraylst.Add(new SqlParameter("@VendorId", iVendorID));

        DataTable dt = ExecuteDataTable("HRspVendor", arraylst);
      
        sb.Append("<table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr><td><table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight:bold;font-size:15px;' align='center' valign='center' width='475px' ><u>&nbsp;Vendor Details&nbsp;</u></td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight:bold;font-size:12px;' align='left' valign='left' width='475px' ><br/>General Information</td>");
        sb.Append("</tr>");
        sb.Append("</table></td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr><td width='150px'>Name</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VendorName"]) + "</td></tr>");
        sb.Append("<tr><td >Code</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VendorCode"]) + "</td></tr>");
       
        sb.Append("<tr><td>Short Name</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ShortName"]) + "</td></tr>");
        sb.Append("<tr><td>Vendor Type</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["VendorType"]) + "</td></tr>");
        sb.Append("<tr><td>Agent Code</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["AgentCode"]) + "</td></tr>");
        sb.Append("<tr><td>Transaction Type</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["TransactionType"]) + "</td></tr>");
        sb.Append("<tr><td>Bank Name</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["BankName"]) + "</td></tr>");
        sb.Append("<tr><td>Account Number</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["AccountNumber"]) + "</td></tr>");
        sb.Append("<tr><td>Account Head</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["AccountHead"]) + "</td></tr>");
        sb.Append("</table></td></tr>");       
        sb.Append("<tr><td style='font-weight:bold;font-size:12px;' align='left' valign='left'><br/>Contact Information</td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr><td width='150px'>Contact Person</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Contact"]) + "</td></tr>");
        sb.Append("<tr><td>Address</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["MailingAddress"]) + "</td></tr>");
        sb.Append("<tr><td>Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>Telephone</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Telephone"]) + "</td></tr>");
        sb.Append("<tr><td>Fax</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Fax"]) + "</td></tr>");
        sb.Append("<tr><td>E-mail</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Email"]) + "</td></tr>");
        sb.Append("<tr><td>Website</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Website"]) + "</td></tr>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public string CreateSelectedVendorsContent(string sVendorIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "PV"));
        arraylst.Add(new SqlParameter("@VendorIds", sVendorIds));

        DataTable dt = ExecuteDataTable("HRspVendor", arraylst);

        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:13px;margin:25px' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='center'><u>&nbsp; VENDOR INFORMATION &nbsp;<u/></td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
        //sb.Append("<tr><td colspan='6'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'>Name</td><td width='100px'>Code</td><td width='100px'>Short Name</td><td width='100px'>Type</td><td width='120px'>Country</td><td width='170px'>Contact Person</td></tr>");
        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["VendorName"]) + "</td>");            
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VendorCode"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ShortName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VendorType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Contact"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }


}
