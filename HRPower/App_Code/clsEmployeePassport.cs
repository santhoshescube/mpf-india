﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

using System.Text;


/// <summary>
/// Summary description for clsEmployeePassport
/// </summary>
public class clsEmployeePassport:DL
{
	public clsEmployeePassport()
	{
		
	}

    public int PassportID { get; set; }
    public int EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public string NameinPassport { get; set; }
    public string PassportNumber { get; set; }
    public string PlaceofIssue { get; set; }
    public int CountryID { get; set; }
    public string  PlaceOfBirth { get; set; }
    public DateTime IssueDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public string Remarks { get; set; }
    public bool PagesAvailable { get; set; }
    public bool IsRenewed { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int UserId { get; set; }
    public string SearchKey { get; set; }
    public int Node { get; set; }
    public string Docname { get; set; }
    public string Filename { get; set; }
    public string OldPassport { get; set; }
    public string ResidencePermit { get; set; }
    public string EntryRestrictions { get; set; }
    public int SortExpression { get; set; }
    public string SortOrder { get; set; }

    public void BeginEmp()
    {
        BeginTransaction("HRspEmployeePassport");
    }
   
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

   

    //-----------------------------------------------------------Employee passport ------------------------------------------------------------
    public DataSet GetEmpSalaryStructure()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSS"));
        alparameters.Add(new SqlParameter("@PassportID", PassportID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspEmployeePassport", alparameters);
    }
    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID ));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeePassport", alParameters));
    }

    public DataSet GetAllEmployeeSalaryStructures()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alParameters.Add(new SqlParameter("@SortOrder", SortOrder));
        return ExecuteDataSet("HRspEmployeePassport", alParameters);
    }

    public DataSet GetEmployeeSalaryStructure()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspEmployeePassport", alParameters);
    }

   
      
    public int InsertEmployeePassport() //Insert Employee Salary Structure details
    {
        ArrayList alparameters = new ArrayList();
      
        alparameters.Add(new SqlParameter("@Mode", "IH"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@NameinPassport", NameinPassport));
        alparameters.Add(new SqlParameter("@PassportNumber", PassportNumber));
        alparameters.Add(new SqlParameter("@PlaceofIssue", PlaceofIssue));
        if (CountryID == -1) alparameters.Add(new SqlParameter("@CountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@CountryID", CountryID));
        alparameters.Add(new SqlParameter("@PlaceOfBirth", PlaceOfBirth));
        alparameters.Add(new SqlParameter("@IssueDate", IssueDate));
        alparameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        alparameters.Add(new SqlParameter("@OldPassportNumbers", OldPassport));
        alparameters.Add(new SqlParameter("@EntryRestrictions", EntryRestrictions));
        alparameters.Add(new SqlParameter("@ResidencePermits", ResidencePermit));
        alparameters.Add(new SqlParameter("@PassportPagesAvailable", PagesAvailable));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }


    public int UpdateEmployeePassport()  //update salary structure
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UH"));
        alparameters.Add(new SqlParameter("@PassportID", PassportID));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@NameinPassport", NameinPassport));
        alparameters.Add(new SqlParameter("@PassportNumber", PassportNumber));
        alparameters.Add(new SqlParameter("@PlaceofIssue", PlaceofIssue));
        if (CountryID == -1) alparameters.Add(new SqlParameter("@CountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@CountryID", CountryID));
        alparameters.Add(new SqlParameter("@PlaceOfBirth", PlaceOfBirth));
        alparameters.Add(new SqlParameter("@IssueDate", IssueDate));
        alparameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        alparameters.Add(new SqlParameter("@OldPassportNumbers", OldPassport));
        alparameters.Add(new SqlParameter("@EntryRestrictions", EntryRestrictions));
        alparameters.Add(new SqlParameter("@ResidencePermits", ResidencePermit));
        alparameters.Add(new SqlParameter("@PassportPagesAvailable", PagesAvailable));
        
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateRenewEmployeePassport()  //Update Renew Employee Passport
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "UR"));
        alparameters.Add(new SqlParameter("@PassportID", PassportID));
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable FillCountry()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspEmployeePassport", alparameters);
    }
    public DataTable FillEmployees(long lngEmployeeID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FEM"));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()? 1:0 ));
        return ExecuteDataTable("HRspEmployeePassport", alparameters);
    }

    public DataTable FillFilterEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GES"));
        return ExecuteDataTable("HRspEmployeePassport", alparameters);
    }

    public string GetEmployeeName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspEmployeePassport", alparameters));
    }
    public int GetCompanyId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCI"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeePassport", alparameters));
    }
   
      
    public bool IsPassportIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeePassport", alParameters)) > 0 ? true : false);
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DES"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));

        ExecuteNonQuery("HRspEmployeePassport", alParameters);
    }

    public DataTable  GetPrintText()
    {
       
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return  ExecuteDataSet("HRspEmployeePassport", alParameters).Tables[0];

    }

    public string GetPrintText(int iPassportID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@PassportID", iPassportID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        DataTable dt = ExecuteDataSet("HRspEmployeePassport", alParameters).Tables[0];

    
        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Passport Information</td></tr>");
        sb.Append("<tr><td width='150px'>Passport Number</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["PassportNumber"]) + "</td></tr>");
        sb.Append("<tr><td>Namein Passport</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["NameinPassport"]) + "</td></tr>");
        sb.Append("<tr><td>Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");    
        sb.Append("<tr><td>Placeof Issue</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>Place Of Birth</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceOfBirth"]) + "</td></tr>");
        sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>Is Renewed</td><td>:</td><td>" + dt.Rows[0]["IsRenewed"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>Old Passport Numbers</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["OldPassportNumbers"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>Entry Restrictions</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["EntryRestrictions"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>Residence Permits</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["ResidencePermits"].ToStringCustom() + "</td></tr>"); 
        
             
        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public DataTable CreateSelectedEmployeesPrintContent(string sPassportIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@PassportIds", sPassportIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspEmployeePassport", arraylst);

    }
    public string CreateSelectedEmployeesContent(string sPassportIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@PassportIds", sPassportIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        DataTable dt = ExecuteDataTable("HRspEmployeePassport", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Passport Information</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee</td><td width='150px'>Passport Number</td><td width='150px'>Namein Passport</td><td width='100px'>Country</td><td width='100px'>Placeof Issue</td><td width='100px'>Is Renewed</td><td width='100px'>Old Passport Numbers</td><td width='100px'>Entry Restrictions</td><td width='100px'>Residence Permits</td></tr>");
        sb.Append("<tr><td colspan='10'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PassportNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NameinPassport"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + dt.Rows[i]["IsRenewed"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["OldPassportNumbers"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["EntryRestrictions"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["ResidencePermits"].ToStringCustom() + "</td></tr>"); 

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public bool IsPassportNumberExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEC"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));
        alParameters.Add(new SqlParameter("@PassportNumber", PassportNumber));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeePassport", alParameters)) > 0 ? true : false);
    }

    public DataSet GetPassportDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPD"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID ));

        return ExecuteDataSet("HRspEmployeePassport", alParameters);
    }
    public void DeletePassportDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@Node", Node));

        ExecuteNonQuery("HRspEmployeePassport", alParameters);
    }

    public int InsertEmployeePassportTreemaster() //Insert Employee passport treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ITM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@PassportID", PassportID));
        alparameters.Add(new SqlParameter("@PassportNumber", PassportNumber));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateEmployeePassportTreemaster() //Insert Employee passport treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UTM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@NameinPassport", PassportID));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable GetPassportFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));
        alParameters.Add(new SqlParameter("@PassportID", PassportID));

        return ExecuteDataTable("HRspEmployeePassport", alParameters);
    }

}



