﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsEmployeeTakeOver
/// </summary>
public class clsEmployeeTakeOver:DL
{
    #region Properties

    public int EmployeeId { get; set; }
    public string Particulars { get; set; }
    public bool Status { get; set; }
    public string EmployeeCode { get; set; }
    public int PageSize { get; set; }
    public int PageIndex { get; set; }

    public int EmployeeTakeOverId { get; set; }
    //public int ProjectId { get; set; }
    //public int AssignedToEmployeeId { get; set; }
    //public int SeperatedEmployeeId { get; set; }
    //public DateTime  AssignedDate { get; set; }

    #endregion

    public clsEmployeeTakeOver()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public string PrintSingleItemTakeOver(int EmployeeId)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GEDBEID"));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);

            ArrayList alParameters1 = new ArrayList();
            alParameters1.Add(new SqlParameter("@Mode", "GEP"));
            alParameters1.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dtEmployeeParticular = this.ExecuteDataTable("HRSpExitInterview", alParameters1);

            DataRow dr = dt.Rows[0];
            StringBuilder sb = new StringBuilder();
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];

            sb.Append("<div style='font-family: Verdana; font-size: 11px'>");
            sb.Append("<center>");
            sb.Append("<u><b style='text-align: center'>&nbsp; Employee TakeOver Details&nbsp; </b></u>");
            sb.Append("</center>");
            sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;border-top-width: 1px; border-bottom-width: 0px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            sb.Append("<td style='font-weight: bold; width: 220px;'>");
            sb.Append("<table border='0' cellspacing='2'>");
            sb.Append("<tr>");
            sb.Append("<td width='60px'>");
            sb.Append("<img src='" + "../thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&Width=70&EmployeeId=" + EmployeeId + "&t=" + DateTime.Now.Ticks + "'/>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr>");
            sb.Append("<td colspan='3' style='font-weight: bold; padding: 2px;'>");
            sb.Append("General Information");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>" + dr["EmployeeFullName"] + "</b>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee Number");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["EmployeeNumber"]);
            sb.Append(" </td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date of joining");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["DateOfJoining"]).ToString("dd-MMM-yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Working Status");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["WorkStatus"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Separation Type");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date Of " + dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["SeperatedDate"]).ToString("dd-MMM-yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");

            if (dr["SeperationType"] != null && Convert.ToInt32(dr["SeperationType"]) == 1 || Convert.ToInt32(dr["SeperationType"]) == 5)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(dr["SeperationTypeS"] + " Reason" + "</td>");
                sb.Append("<td>");
                sb.Append(dr["SeperationTypes"] + "</td>");
                sb.Append("</tr>");

            }

            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Remarks");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["Remarks"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</td>");
            sb.Append("</table>");


            sb.Append("<div style='font-size: 12px; font-weight: bold; margin-top: 10px'>");
            sb.Append("Particulars Details");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("&nbsp;</div>");
            sb.Append("<table width='400px;' style='font-size: 11px; border: solid 1px #F0F0F0'>");

            if (dtEmployeeParticular.Rows.Count > 0)
            {


                sb.Append(" <tr style='background-color: #f0f0f0;'>");
                sb.Append("<td style='font-weight: bold; width: 220px'>");
                sb.Append("Particular Name");
                sb.Append("</td>");
                sb.Append("<td style='font-weight: bold'>");
                sb.Append("Status");
                sb.Append("</td>");


                foreach (DataRow dr1 in dtEmployeeParticular.Rows)
                {

                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append(dr1["Particular"]);
                    sb.Append("</td>");
                    sb.Append("<td>");
                    sb.Append(dr1["Status"]);
                    sb.Append("</td>");
                    sb.Append("</tr>");


                }
            }

            else
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("No particular collected from the employee" + "</td>");
                sb.Append("<tr>");
            }
            sb.Append("</table>");
            sb.Append("</div>");
            return sb.ToString();

        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public string PrintMultipleItemTakeOver(string SEmployeeId)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GMEP"));
            alParameters.Add(new SqlParameter("@SEmployeeId", SEmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);
            string [] EmployeeId;
            if(SEmployeeId.Contains(",")) 
                 EmployeeId = SEmployeeId.Split(',');
            else
                 EmployeeId = new string[]{SEmployeeId};   
            StringBuilder sb = new StringBuilder();
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];
           
            sb.Append("<div style='font-family: Verdana; font-size: 11px'>");
            sb.Append("<center>");
            sb.Append("<u><b style='text-align: center'>&nbsp; Employee TakeOver Details&nbsp; </b></u>");
            sb.Append("</center>");
          

            foreach (string  empId in EmployeeId)
            {
                sb.Append("<table width='100%' style='margin-top:10px;font-size: 11px; border-top-style: solid; border-bottom-style: solid;border-top-width: 1px; border-bottom-width: 0px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
                sb.Append("<tr>");
                DataRow[] dr = dt.Select("EmployeeId=" +Convert.ToInt32(empId));
              
                sb.Append("<td colspan='2' padding: 2px;margin-top: 10px'>");
                sb.Append("<table style='font-size: 11px;'> <tr> <td style='width:120px'> Employee Name </td> <td> <b> " + dr[0]["EmployeeFullName"] + "</b></td> </tr></table>");
                sb.Append("</td>");

                sb.Append("</tr>");
                sb.Append("</table>");


                sb.Append("<div style='font-size: 12px; font-weight: bold; margin-top: 10px'>");
                sb.Append("Particulars Details");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("&nbsp;</div>");
                sb.Append("<table width='400px;' style='font-size: 11px; border: solid 1px #F0F0F0'>");

                if (dr[0]["Particular"] != null && dr[0]["Particular"] != DBNull.Value)
                {
                    sb.Append(" <tr style='background-color: #f0f0f0;'>");
                    sb.Append("<td style='font-weight: bold; width: 220px'>");
                    sb.Append("Particular Name");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight: bold'>");
                    sb.Append("Status");
                    sb.Append("</td>");

                    foreach (DataRow dr1 in dr)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dr1["Particular"]);
                        sb.Append("</td>");
                        sb.Append("<td>");
                        sb.Append(dr1["Status"]);
                        sb.Append("</td>");
                        sb.Append("</tr>");


                    }
                }
                else
                {
                    sb.Append("<tr>");
                    sb.Append("<td>");
                    sb.Append("No particulars collected from the employee");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
                
                sb.Append("</table>");

            }
            sb.Append("</div>");
            return sb.ToString();
            
        }
            
            
            
            
            //sb.Append("<div style='font-family: Verdana; font-size: 11px;'>");

            //sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            //sb.Append("<u> <b>&nbsp; Employee Details&nbsp;</b></u></td>");
            //sb.Append("<td style='font-weight: bold; width:220px;'>");
            //sb.Append("</td>");
            //sb.Append("</tr>");
            ////sb.Append("<h5>");
            ////sb.Append("Employee Details </h5>");

            //int EmployeeId = 0;

            //sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>General Information</td></tr>");
            //// sb.Append("<u> <b>&nbsp;Employee Details&nbsp;</b></u></td>");

            //foreach (DataRow dr in dt.Rows)
            //{

            //    if (Convert.ToInt32(dr["EmployeeId"]) != EmployeeId)
            //    {
            //        sb.Append("<tr>");
            //        sb.Append("<td> ");
            //        sb.Append("Employee</td>");
            //        sb.Append("<td>");
            //        sb.Append("<b>" + dr["EmployeeFullName"] + "</b></td>");
            //        sb.Append("</tr>");
            //        sb.Append("</table>");
            //        sb.Append("<h5>");
            //        sb.Append("Particulars Details </h5>");
            //        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            //        sb.Append("<tr>");
            //        sb.Append("<td style='font-weight:bold; width:220px'>");
            //        sb.Append("Particular Name");
            //        sb.Append("</td>");
            //        sb.Append("<td style='font-weight:bold'>");
            //        sb.Append("Status");
            //        sb.Append("</td>");

            //    }
            //    EmployeeId = Convert.ToInt32(dr["EmployeeId"]);
            //    sb.Append("<tr>");
            //    sb.Append("<td>");
            //    sb.Append(dr["Particular"] + "</td>");
            //    sb.Append("<td>");
            //    sb.Append(dr["Status"] + "</td>");
            //    sb.Append("</tr>");
            //}
           
            //sb.Append("</table>");
            //return sb.ToString();

        
        catch (Exception ex)
        {
            return null;
        }
         
    }


    public string PrintMultipleItemHandOver(string SEmployeeId)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GMEHP"));
            alParameters.Add(new SqlParameter("@SEmployeeId", SEmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);


            string[] EmployeeId;
            if (SEmployeeId.Contains(","))
                EmployeeId = SEmployeeId.Split(',');
            else
                EmployeeId = new string[] { SEmployeeId };
            StringBuilder sb = new StringBuilder();
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];

            sb.Append("<div style='font-family: Verdana; font-size: 11px'>");
            sb.Append("<center>");
            sb.Append("<u><b style='text-align: center'>&nbsp; Employee TakeOver Details&nbsp; </b></u>");
            sb.Append("</center>");


            foreach (string empId in EmployeeId)
            {
                sb.Append("<table width='100%' style='margin-top:10px;font-size: 11px; border-top-style: solid; border-bottom-style: solid;border-top-width: 1px; border-bottom-width: 0px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
                sb.Append("<tr>");
                DataRow[] dr = dt.Select("EmployeeId=" + Convert.ToInt32(empId));

                sb.Append("<td colspan='2' padding: 2px;margin-top: 10px'>");
                sb.Append("<table style='font-size: 11px;'> <tr> <td style='width:120px'> Employee Name </td> <td> <b> " + dr[0]["EmployeeFullName"] + "</b></td> </tr></table>");
                sb.Append("</td>");

                sb.Append("</tr>");
                sb.Append("</table>");


                sb.Append("<div style='font-size: 12px; font-weight: bold; margin-top: 10px'>");
                sb.Append("Project Details");
                sb.Append("</div>");
                sb.Append("<div>");
                sb.Append("&nbsp;</div>");
                sb.Append("<table width='500px' style='font-size: 11px; border: solid 1px #F0F0F0'>");

                sb.Append(" <tr style='background-color: #f0f0f0;'>");

                if (dr[0]["ProjectCode"] == null || dr[0]["ProjectCode"] == DBNull.Value)
                {
                    sb.Append("<td'>");
                    sb.Append("No project was assigned to the employee");
                    sb.Append("</td>");

                }
                else
                {
                    sb.Append("<td style='font-weight:bold; width:30px'>");
                    sb.Append("Code");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight:bold;width:100px'>");
                    sb.Append("Description");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight:bold'>");
                    sb.Append("Start Date");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight:bold'>");
                    sb.Append("End Date");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight:bold';>");
                    sb.Append("Project Manager");
                    sb.Append("</td>");
                    sb.Append("<td style='font-weight:bold';>");
                    sb.Append("HandOver To");
                    sb.Append("</td>");



                    foreach (DataRow dr1 in dr)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td>");
                        sb.Append(dr1["ProjectCode"] + "</td>");
                        sb.Append("<td>");
                        sb.Append(dr1["Description"] + "</td>");
                        sb.Append("<td>");
                        sb.Append(Convert.ToDateTime(dr1["StartDate"]).ToString("dd/MM/yyyy") + "</td>");
                        sb.Append("<td>");
                        sb.Append(Convert.ToDateTime(dr1["EstimatedFinishDate"]).ToString("dd/MM/yyyy") + "</td>");
                        sb.Append("<td>");
                        sb.Append(dr1["ProjectManager"] + "</td>");
                        sb.Append("<td>");
                        if (dr1["AssignedTo"] == null || dr1["AssignedTo"] == DBNull.Value)
                        {
                            sb.Append("Not Yet Assigned" + "</td>");

                        }
                        else
                            sb.Append(dr1["AssignedTo"] + "</td>");

                        sb.Append("</tr>");


                    }
                }

                sb.Append("</table>");

            }
            sb.Append("</div>");
            return sb.ToString();
  

        }
        catch (Exception ex)
        {
            return null;
        }

    }


    public string PrintSingleItemHandOver(int EmployeeId)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GEDBEID"));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);

            ArrayList alParameters1 = new ArrayList();
            alParameters1.Add(new SqlParameter("@Mode", "GPDS"));
            alParameters1.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dtEmployeeProject = this.ExecuteDataTable("HRSpExitInterview", alParameters1);

            DataRow dr = dt.Rows[0];

            StringBuilder sb = new StringBuilder();
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];


            sb.Append("<div style='font-family: Verdana; font-size: 11px'>");
            sb.Append("<center>");
            sb.Append("<u><b style='text-align: center'>&nbsp; Employee TakeOver Details&nbsp; </b></u>");
            sb.Append("</center>");
            sb.Append("<table width='100%' cellspacing='1px' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;border-top-width: 1px; border-bottom-width: 0px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            sb.Append("<td style='font-weight: bold; width: 220px;'>");
            sb.Append("<table border='0' cellspacing='2'>");
            sb.Append("<tr>");
            sb.Append("<td width='60px'>");
            sb.Append("<img src='" + path + "thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&Width=70&EmployeeId=" + EmployeeId + "&t=" + DateTime.Now.Ticks + "'/>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr>");
            sb.Append("<td colspan='3' style='font-weight: bold; padding: 2px;'>");
            sb.Append("General Information");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>" + dr["EmployeeFullName"] + "</b>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee Number");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["EmployeeNumber"]);
            sb.Append(" </td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date of joining");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["DateOfJoining"]).ToString("dd/MMM/yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Working Status");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["WorkStatus"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Separation Type");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date Of " + dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["SeperatedDate"]).ToString("dd/MMM/yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");

            if (dr["SeperationType"] != null && Convert.ToInt32(dr["SeperationType"]) == 1 || Convert.ToInt32(dr["SeperationType"]) == 5)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(dr["SeperationTypeS"] + " Reason" + "</td>");
                sb.Append("<td>");
                sb.Append(dr["SeperationTypes"] + "</td>");
                sb.Append("</tr>");

            }

            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Remarks");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["Remarks"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</td>");
            sb.Append("</table>");


            sb.Append("<div style='font-size: 12px; font-weight: bold; margin-top: 10px'>");
            sb.Append("Project Details");
            sb.Append("</div>");
            sb.Append("<div>");
            sb.Append("&nbsp;</div>");
            sb.Append("<table width='550px;' style='font-size: 11px; border: solid 1px #F0F0F0'>");

            if (dtEmployeeProject.Rows.Count > 0)
            {

                sb.Append(" <tr style='background-color: #f0f0f0;'>");
                sb.Append("<td style='font-weight: bold; width: 50px'>");
                sb.Append("Code");
                sb.Append("</td>");

                sb.Append("<td style='font-weight: bold ; width: 170px'>");
                sb.Append("Description");
                sb.Append("</td>");

                sb.Append("<td style='font-weight: bold'>");
                sb.Append("Start Date");
                sb.Append("</td>");

                sb.Append("<td style='font-weight: bold'>");
                sb.Append("End Date");
                sb.Append("</td>");

                sb.Append("<td style='font-weight: bold'>");
                sb.Append("Project Manager");
                sb.Append("</td>");

                sb.Append("<td style='font-weight: bold'>");
                sb.Append("HandOver To");
                sb.Append("</td>");


                foreach (DataRow dr1 in dtEmployeeProject.Rows)
                {

                    sb.Append("<tr>");

                    sb.Append("<td>");
                    sb.Append(dr1["ProjectCode"]);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(dr1["Description"]);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(dr1["StartDate"]);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(dr1["EstimatedFinishDate"]);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    sb.Append(dr1["ProjectManager"]);
                    sb.Append("</td>");

                    sb.Append("<td>");
                    if (dr1["AssignedToEmployee"] == null || dr1["AssignedToEmployee"] == DBNull.Value)
                    {
                        sb.Append("Not Yet Assigned" + "</td>");

                    }
                    else
                        sb.Append(dr1["AssignedToEmployee"]);
                    sb.Append("</td>");


                    sb.Append("</tr>");
                }
            }

            else
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append("No project was assigned was assigned to the employee" + "</td>");
                sb.Append("<tr>");
            }
            sb.Append("</table>");
            sb.Append("</div>");
            return sb.ToString();

        }
        catch (Exception ex)
        {
            return null;
        }

    }




    #region SaveEmployeeParticulars
    public bool SaveEmployeeParticulars(string TakeOverId)
    {
        try
        {   int EmployeeTakeOverId1=0;
        if (TakeOverId != "")
            EmployeeTakeOverId1 = Convert.ToInt32(TakeOverId);
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "SEP"));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            alParameters.Add(new SqlParameter("@Particular", Particulars));
            alParameters.Add(new SqlParameter("@EmployeeTakeOverId",EmployeeTakeOverId1));
            alParameters.Add(new SqlParameter("@Status", Status));
            int i = Convert.ToInt32(this.ExecuteScalar("HRSpExitInterview", alParameters));
            if (i > 0)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    #endregion


    #region SaveEmployeeHandOver
    public bool SaveEmployeeHandOver(int SeperatedEmployeeId,int AssignedToEmployeeId,int ProjectId ,DateTime AssignedDate )
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "SEPHO"));
            alParameters.Add(new SqlParameter("@SeperatedEmployeeId",SeperatedEmployeeId));
            alParameters.Add(new SqlParameter("@AssignedToEmployeeId",AssignedToEmployeeId));
              alParameters.Add(new SqlParameter("@ProjectId",ProjectId));
            alParameters.Add(new SqlParameter("@AssignedDate",AssignedDate));
            int i = Convert.ToInt32(this.ExecuteScalar("HRSpExitInterview", alParameters));
            if (i > 0)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    #endregion

    #region GetEmployeeParticular
    public DataTable GetEmployeeParticular(int EmpID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEP"));
        alParameters.Add(new SqlParameter("@EmployeeId",EmpID));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);

    }


    #region IsExistSamePArticualar
    public bool  IsExistSamePArticualar(object EmployeeTakeOverId,long lngEmployeeID,string Particular)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ISESPN"));
        alParameters.Add(new SqlParameter("@EmployeeTakeOverId", EmployeeTakeOverId));
        alParameters.Add(new SqlParameter("@EmployeeId", lngEmployeeID));
        alParameters.Add(new SqlParameter("@Particular", Particular));
        int Status = Convert.ToInt32(this.ExecuteScalar("HRSpExitInterview", alParameters));
        if (Status > 0)
            return true;
        else
            return false;

    }
    #endregion




    public DataTable GetEmployeeParticularDetail(int EmpTakeOverId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEPD"));
        alParameters.Add(new SqlParameter("@EmployeeTakeOverId", EmpTakeOverId));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);

    }

    #endregion


   
    #region GetEmployeeDetailsByEmployeeId

    public DataTable GetEmployeeDetailsByEmployeeId(int EmployeeId, string EmployeeCde, object EmployeeSeperatedId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEDBEID"));
        if (EmployeeId != 0)
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
        if (EmployeeCde != string.Empty)
            alParameters.Add(new SqlParameter("@EmployeeCode", EmployeeCde));
        if (EmployeeSeperatedId != null)
            alParameters.Add(new SqlParameter("@EmployeeSeperatedId", Convert.ToInt32(EmployeeSeperatedId)));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);
    }

    #endregion


    //public bool IsEmployeeNameExist(string EmpName)
    //{
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@Mode", "IEEMPN"));
    //    alParameters.Add(new SqlParameter("@EmployeeName",EmpName));
    //    return  this.ExecuteDataTable("HRSpExitInterview", alParameters);
    //}

    public DataSet GetSeperatedEmployeesBySearch(string Key,int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ALL"));
        alParameters.Add(new SqlParameter("@Key", Key));
        alParameters.Add(new SqlParameter("@IsAllCompany", CompanyId));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        return ExecuteDataSet("HRspExitInterView", alParameters);
    }
    public DataSet GetSeperatedEmployees(int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GSE"));
        alParameters.Add(new SqlParameter("@IsAllCompany", CompanyId));
        alParameters.Add(new SqlParameter("@PageIndex", this.PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", this.PageSize));
        return ExecuteDataSet("HRspExitInterView", alParameters);
    }

    #region DeleteEmployeeParticular()
    public bool DeleteEmployeeParticular(int EmployeeTakeOverId)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "DELEP"));
            alParameters.Add(new SqlParameter("@EmployeeTakeOverId", EmployeeTakeOverId));
            if (Convert.ToInt32(this.ExecuteScalar("HRSpExitInterview", alParameters)) >= 0)
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    #endregion

    #region GetHandOverEmployeeList
    public DataTable GetHandOVerEmployeeList(int EmployeeId,int CompanyId,int ProjectId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GHOEL"));
        alParameters.Add(new SqlParameter("@EmployeeId",EmployeeId));
        alParameters.Add(new SqlParameter("@ProjectId",ProjectId));
        //alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }
    #endregion 

}

