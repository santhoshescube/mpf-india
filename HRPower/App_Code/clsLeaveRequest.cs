﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

/// <summary>
/// Summary description for clsLeaveRequest
/// </summary>
public class clsLeaveRequest : DL
{
    string sMode, sReason, sRequestedTo, sApprovedBy;
    int iRequestId,iCompanyId, iLeaveType, iStatusId, iHalfDay, iPageIndex, iPageSize, iMonth, iYear, iRequestType, iApprovedById;
    Int64 iEmployeeId;
    DateTime dtLeaveFrom, dtLeaveTo;
    bool dIsCredit;
    decimal fNoOfDays;
    decimal fNoOfHolidays;
    bool bPaidLeave;

    #region Properties
    public Int64 EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }
    public string Mode
    {
        get { return sMode; }
        set { sMode = value; }
    }
    public string Reason
    {
        get { return sReason; }
        set { sReason = value; }
    }
    public string RequestedTo
    {
        get { return sRequestedTo; }
        set { sRequestedTo = value; }
    }
    public string ApprovedBy
    {
        get { return sApprovedBy; }
        set { sApprovedBy = value; }
    }

    public int RequestId
    {
        get { return iRequestId; }
        set { iRequestId = value; }
    }
    public int CompanyId
    {
        get { return iCompanyId; }
        set { iCompanyId = value; }
    }
    public int LeaveType
    {
        get { return iLeaveType; }
        set { iLeaveType = value; }
    }
    public int StatusId
    {
        get { return iStatusId; }
        set { iStatusId = value; }
    }
    public int HalfDay
    {
        get { return iHalfDay; }
        set { iHalfDay = value; }
    }
    public DateTime LeaveFromDate
    {
        get { return dtLeaveFrom; }
        set { dtLeaveFrom = value; }
    }
    public DateTime LeaveToDate
    {
        get { return dtLeaveTo; }
        set { dtLeaveTo = value; }
    }
    public bool IsVacation { get; set; }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public int month
    {
        get { return iMonth; }
        set { iMonth = value; }
    }
    public int year
    {
        get { return iYear; }
        set { iYear = value; }
    }
    public int yearTo
    {
        get;
        set;
    }
    public int RequestType
    {
        get { return iRequestType; }
        set { iRequestType = value; }
    }

    public decimal NoOfDays
    {
        get { return fNoOfDays; }
        set { fNoOfDays = value; }
    }

    public decimal NoOfHolidays
    {
        get { return fNoOfHolidays; }
        set { fNoOfHolidays = value; }
    }
    public bool PaidLeave
    {
        get { return bPaidLeave; }
        set { bPaidLeave = value; }
    }

    public int ApprovedById
    {
        get { return iApprovedById; }
        set { iApprovedById = value; }
    }

    public bool IsCredit
    {
        get { return dIsCredit; }
        set { dIsCredit = value; }
    }
    public string WorkedDay { get; set; }

    public int CompanyID { get; set; }

    public bool IsFlightRequired { get; set; }
    public bool IsSectorRequired { get; set; }
    public string ContactAddress { get; set; }
    public string TelephoneNo { get; set; }
    public bool IsFromComboOff { get; set; }
    public int EncashComboOffDay { get; set; }
    public int VacationID { get; set; }
    public int NumberOfDays { get; set; }
    public bool IsEncash { get; set; }

    #endregion

    public clsLeaveRequest()
    {
    }
    /// <summary>
    /// get leave types
    /// </summary>
    /// <returns>datatable</returns>
    public DataTable GetLeaveTypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));// 1 for Arabic else 0
        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetAllStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAS"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));// 1 for Arabic else 0
        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    /// <summary>
    /// Get reported to employees.(common function for all requests)
    /// Modification:Instead of hierarchy ,now 'reporting to' from employeeForm and 'authority' set in settings form
    /// </summary>
    /// <returns></returns>
    public static string GetRequestedTo(int RequestId, Int64? intEmployeeId, int intRequestType)
    {
        DataTable dt;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRT"));
        if (intEmployeeId != null)
            alParameters.Add(new SqlParameter("@EmployeeId", intEmployeeId));

        if (intRequestType == (int)eReferenceTypes.CompensatoryOffRequest)
            intRequestType = (int)eReferenceTypes.CompensatoryOffRequest;
        else if (intRequestType == (int)eReferenceTypes.TimeExtensionRequest)
            intRequestType = (int)eReferenceTypes.TimeExtensionRequest ;
        else if (intRequestType == (int)eReferenceTypes.VacationLeaveRequest)
            intRequestType = (int)eReferenceTypes.VacationLeaveRequest;

        if (RequestId > 0)
            alParameters.Add(new SqlParameter("@ReferenceID", RequestId));
        alParameters.Add(new SqlParameter("@RequestTypeID", intRequestType));

        return Convert.ToString(new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public static string GetRequestedTo(int RequestId, Int64? intEmployeeId, int intRequestType, int CompanyID)
    {
        DataTable dt;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRT"));
        if (intEmployeeId != null)
            alParameters.Add(new SqlParameter("@EmployeeId", intEmployeeId));

        if (intRequestType == (int)eReferenceTypes.CompensatoryOffRequest)
            intRequestType = (int)eReferenceTypes.CompensatoryOffRequest;
        else if (intRequestType == (int)eReferenceTypes.TimeExtensionRequest)
            intRequestType = (int)eReferenceTypes.TimeExtensionRequest;
        else if (intRequestType == (int)eReferenceTypes.VacationLeaveRequest)
            intRequestType = (int)eReferenceTypes.VacationLeaveRequest;

        if (RequestId > 0)
            alParameters.Add(new SqlParameter("@ReferenceID", RequestId));

        alParameters.Add(new SqlParameter("@TempCompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@RequestTypeID", intRequestType));

        return Convert.ToString(new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    /// <summary>
    /// insert /update leave request.
    /// </summary>
    public int InsertUpdateLeaveRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        if (iLeaveType != -1)
            alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        else
        {
            alParameters.Add(new SqlParameter("@LeaveTypeId", DBNull.Value));
            alParameters.Add(new SqlParameter("@IsVacation", 1));
        }
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alParameters.Add(new SqlParameter("@HalfDay", iHalfDay));
        alParameters.Add(new SqlParameter("@IsCredit", dIsCredit));

        alParameters.Add(new SqlParameter("@IsFlightRequired", IsFlightRequired));
        alParameters.Add(new SqlParameter("@IsSectorRequired", IsSectorRequired));
        alParameters.Add(new SqlParameter("@ContactAddress", ContactAddress));
        alParameters.Add(new SqlParameter("@TelephoneNo", TelephoneNo));
        alParameters.Add(new SqlParameter("@IsFromComboOff", IsFromComboOff));
        alParameters.Add(new SqlParameter("@EncashComboOffDay", EncashComboOffDay));
        if (iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
        alParameters.Add(new SqlParameter("@IsEncash", IsEncash));
        alParameters.Add(new SqlParameter("@EncashNumberOfDays", NumberOfDays));



        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    /// <summary>
    /// get all requests.
    /// </summary>
    /// <returns>data table</returns>
    public DataTable GelAllRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));// 1 for Arabic else 0

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }
    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount(long lngEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", lngEmployeeID));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
        return iCount;
    }

    /// <summary>
    /// get leave request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));// 1 for Arabic else 0

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    /// <summary>
    /// delete requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", sMode));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
        ExecuteNonQuery("HRspLeaveRequest", alParemeters);
    }
    public double GetLeaveCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@Year", iYear));

        if (yearTo > 0)
            alParameters.Add(new SqlParameter("@YearTo", yearTo));
        if (LeaveFromDate.ToString() != "1/1/0001 12:00:00 AM")
            alParameters.Add(new SqlParameter("@LeaveFromDate", LeaveFromDate));
        double dCount = Convert.ToDouble(ExecuteScalar("HRspLeaveRequest", alParameters));
        return dCount;
    }

    public double GetExtension()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@Month", iMonth));
        alParameters.Add(new SqlParameter("@Year", iYear));
        double dCount = 0;
        dCount = Convert.ToDouble(ExecuteScalar("HRspLeaveRequest", alParameters));
        return dCount;
    }

    public void UpdateLeaveStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspLeaveRequest");
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "UPS"));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
            alParameters.Add(new SqlParameter("@StatusId", iStatusId));
            alParameters.Add(new SqlParameter("@ApprovedBy", iApprovedById));
            alParameters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
            alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
            alParameters.Add(new SqlParameter("@Reason", Reason));
            alParameters.Add(new SqlParameter("@IsCredit", IsCredit));
            if (IsAuthorised && iStatusId == 5 && iLeaveType != -1)
            {
                // update Leave Entry Details
                alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
                //alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
                alParameters.Add(new SqlParameter("@HalfDay", iHalfDay));
                alParameters.Add(new SqlParameter("@NoOfDays", fNoOfDays));
                alParameters.Add(new SqlParameter("@NoOfHolidays", fNoOfHolidays));
                alParameters.Add(new SqlParameter("@PaidLeave", bPaidLeave));
            }
            if ((IsAuthorised && iStatusId == 5) || iStatusId == 4)
            {
                alParameters.Add(new SqlParameter("@LeaveFromDate", LeaveFromDate));
                alParameters.Add(new SqlParameter("@LeaveToDate", LeaveToDate));
                alParameters.Add(new SqlParameter("@EncashComboOffDay", EncashComboOffDay));
            }
            if (!IsAuthorised)
            {
                if (iStatusId == 5)
                    alParameters.Add(new SqlParameter("@Forwarded", 1));
                else
                    alParameters.Add(new SqlParameter("@Forwarded", 0));
            }

            alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
            //ExecuteNonQuery(alParameters);
            int leaveid = ExecuteScalar(alParameters).ToInt32();
            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }

    public DataTable GetAllLeaveTypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GALT"));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable LeaveReport()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GLDR"));
        if (iStatusId > 0)
            alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        if (iLeaveType > 0)
            alParemeters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        if (dtLeaveFrom != DateTime.MinValue)
            alParemeters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        if (dtLeaveTo != DateTime.MinValue)
            alParemeters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));

        return ExecuteDataTable("HRspLeaveRequest", alParemeters);
    }

    public DataTable GetEmailIds()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public string GetEmployeeName(int iEmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GN"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        string sName = Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
        return sName;
    }

    public string GetRequestedBy()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public DataTable GetApprovalDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "N"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);

    }

    public string GetRequestedPeriod()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GP"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));

    }

    public int GetTotalHolidays()
    {
        ArrayList alParameters = new ArrayList();
        int Holidaycnt = 0;
        alParameters.Add(new SqlParameter("@Mode", 35));
        alParameters.Add(new SqlParameter("@FromDate1", dtLeaveFrom.ToString("dd-MMM-yyyy")));
        alParameters.Add(new SqlParameter("@ToDate1", dtLeaveTo.ToString("dd-MMM-yyyy")));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));

        DataTable dt = ExecuteDataTable("spPayEmployeeLeave", alParameters);
        if (dt.Rows.Count > 0)
        {
            Holidaycnt = dt.Rows[0][0].ToInt32();
        }
        return Holidaycnt;
    }

    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int GetRequestedByCompanyId()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRBCI"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
        ExecuteNonQuery("HRspLeaveRequest", alParameters);
    }

    /// <summary>
    /// Check duplication
    /// </summary>
    /// <returns></returns>
    public bool IsLeaveApplied()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CLD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        if (iLeaveType != 0)
            alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        if (dtLeaveFrom != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        if (dtLeaveTo != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));

        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return (Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters)) > 0 ? true : false);
    }

    /// <summary>
    /// Check if any vacation is processing
    /// </summary>
    /// <returns></returns>
    public bool IsVacationProcessing()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "LAE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return (Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters)) > 0 ? true : false);
    }


    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsLeaveRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters)) > 0 ? true : false);
    }

    public static bool IsHigherAuthority(int iRequestTypeId, long iEmployeeId, int RequestID) // Predefined 1( leave request),2 (Loan request),3 (Salary advance)
    {
        int TempRequestTypeID = iRequestTypeId;
        int RequestTypeID = 0;

        //if (iRequestTypeId == (int)eReferenceTypes.VacationLeaveRequest || iRequestTypeId == (int)eReferenceTypes.CompensatoryOffRequest)
        //    RequestTypeID = (int)eReferenceTypes.LeaveRequest;
        //else if (iRequestTypeId == (int)eReferenceTypes.VacationExtensionRequest || iRequestTypeId == (int)eReferenceTypes.TimeExtensionRequest)
        //    RequestTypeID = (int)eReferenceTypes.LeaveExtensionRequest;
        //else
        //    RequestTypeID = iRequestTypeId;


        if (iRequestTypeId == (int)eReferenceTypes.CompensatoryOffRequest)
            RequestTypeID = (int)eReferenceTypes.CompensatoryOffRequest;
        else if (iRequestTypeId == (int)eReferenceTypes.TimeExtensionRequest)
            RequestTypeID = (int)eReferenceTypes.TimeExtensionRequest;
        else
            RequestTypeID = iRequestTypeId;

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GHA"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@ReferenceID", RequestID));
        alParameters.Add(new SqlParameter("@TempRequestTypeID", TempRequestTypeID));
        alParameters.Add(new SqlParameter("@RequestTypeId", RequestTypeID));
        return (Convert.ToInt32(new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters)) > 0 ? true : false);
    }
    public int GetHighAuthority(int iRequestTypeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HAI"));
        alParameters.Add(new SqlParameter("@RequestTypeId", iRequestTypeId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public void UpdateLeaveCancellation()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "CLR"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        alParemeters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParemeters.Add(new SqlParameter("@Reason", Reason));
        alParemeters.Add(new SqlParameter("@LeaveTypeId", LeaveType));
        alParemeters.Add(new SqlParameter("@IsCredit", IsCredit));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
        ExecuteNonQuery("HRspLeaveRequest", alParemeters);
    }

    public static string[] GetSuperiorId(long EmployeeId, int RequestType)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GS"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        alParameters.Add(new SqlParameter("@RequestType", RequestType));

        return Convert.ToString(new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters)).Split(',');
    }

    public string GetSuperiorOfficialEmail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEID"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));

        return Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public bool CheckMonthlyLeaveBalance()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "C"));

        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));
        alParameters.Add(new SqlParameter("@HalfDay", iHalfDay));

        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));

    }

    public bool CheckSalaryProcessingOver(int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CP"));

        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@LeaveFromDate", FromDate));
        if(HalfDay == 1)
            alParameters.Add(new SqlParameter("@LeaveToDate", ""));
        else
        alParameters.Add(new SqlParameter("@LeaveToDate", ToDate));
        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));

    }

    public bool CheckSalaryProcessingCancel(int RequestID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CPC"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int GetEmployeeWorkStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEW"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int CheckLeaveEntryExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "LEE"));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));

    }

    public bool CheckAttendanceExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CASE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));
        //  alParameters.Add(new SqlParameter("@HalfDay", iHalfDay));

        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public DataTable GetHolidays()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CH"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);

    }

    public SqlDataReader GetEmployeeIDInTransfer()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ET"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return ExecuteReader("HRspLeaveRequest", alParameters);

    }

    public SqlDataReader GetTransferDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "TD"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return ExecuteReader("HRspLeaveRequest", alParameters);
    }

    /// <summary>
    /// check salary released for the current month
    /// </summary>
    public bool CheckSalaryReleased()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CSR"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));

        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    ///<summary>
    ///  check if LeaveFromDate is a companyHoliday
    ///</summary>
    public bool CheckCompanyHoliday()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CHE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));

        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    ///<summary>
    ///get offdays of the employee from PolicyDetail
    ///</summary>
    public DataTable GetOffDayIds()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "COE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetAllRequestStatus(int RequestMode)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GARS"));
        alParameters.Add(new SqlParameter("@RequestMode", RequestMode));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));// 1 for Arabic else 0

        return ExecuteDataTable("HRspLeaveRequest", alParameters);

    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }
    public bool CheckVacationProcessed(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CVP"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }


    //Vacation Extension
    public void InsertCompensatoryDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ICD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@WorkedDay", WorkedDay));

        ExecuteNonQuery("HRspLeaveRequest", alParameters);
    }

    public void DeleteCompensatoryDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DCD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetCompensatoryOffDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GCD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public bool GetCreditDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "Credit"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToBoolean(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int CheckComboExists(DateTime date, int EID, int RequestID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CCE"));
        alParameters.Add(new SqlParameter("@WorkedDay", date));
        alParameters.Add(new SqlParameter("@EmployeeId", EID));
        alParameters.Add(new SqlParameter("@RequestId", RequestID));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int GetCompanyID()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCID"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }


    public int IsPolicyAvailable()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPA"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public int GetLeaveRequestedCount()
    {

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GLRC"));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public void UpdateIsLeave()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UIL"));

        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@IsCredit", IsCredit));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveFromDate", dtLeaveFrom));
        alParameters.Add(new SqlParameter("@LeaveToDate", dtLeaveTo));

        ExecuteNonQuery("HRspLeaveRequest", alParameters);
    }

    public static DataTable GetEmployeeContactDetails(int EmployeeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GECD"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return new DataLayer().ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetPreviousLoanDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GPLD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public int DoesVacationExtensionExist(int RequestId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DVEE"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public static int CancelIsCreditLeaves(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CIS"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32();
    }

    public bool SaveAttachments(int RequestID, int RequestTypeID, DataTable dt)
    {
        try
        {
            int EmployeeID = 0;
            BeginTransaction("HRspLeaveRequest");

            EmployeeId = new clsUserMaster().GetEmployeeId();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ArrayList alParameters = new ArrayList();
                    alParameters.Add(new SqlParameter("@Mode", "INSAD"));
                    alParameters.Add(new SqlParameter("@RequestID", RequestID));
                    alParameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
                    alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
                    alParameters.Add(new SqlParameter("@FileName", dr["FileName"].ToString()));
                    alParameters.Add(new SqlParameter("@DocumentName", dr["DocumentName"].ToString()));
                    ExecuteScalar("HRspLeaveRequest", alParameters);
                }
            }
            CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return false;

        }

    }

    public bool SaveAttachmentsToTreeMaster(string ServerLocation, int RequestID, int EmployeeID, DataTable dt)
    {
        try
        {

            BeginTransaction("HRspLeaveRequest");

            string PhysicalPath = ConfigurationManager.AppSettings["_PHY_PATH"].ToString();

            if (!Directory.Exists(PhysicalPath + "DocumentImages"))
                Directory.CreateDirectory(PhysicalPath + "DocumentImages");


            EmployeeId = new clsUserMaster().GetEmployeeId();
            foreach (DataRow dr in dt.Rows)
            {
                ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", "INSTM"));
                alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
                alParameters.Add(new SqlParameter("@ReferenceID", RequestID));
                alParameters.Add(new SqlParameter("@FileName", dr["FileName"].ToString()));
                ExecuteScalar("HRspLeaveRequest", alParameters);


                DirectoryInfo d = new DirectoryInfo(PhysicalPath);
                if (!d.Exists)
                    d.Create();

                File.Copy(ServerLocation + "Documents\\LeaveRequest\\" + dr["FileName"].ToString(), PhysicalPath + "DocumentImages\\" + dr["FileName"].ToString());

            }
            CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return false;

        }

    }

    public static DataTable GetAttachedDocumentsDetails(eReferenceTypes RequestTypeID, int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAAD"));
        alParameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
        alParameters.Add(new SqlParameter("@RequestTypes", RequestTypeID.ToString()));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        return new DataLayer().ExecuteDataTable("HRspLeaveRequest", alParameters);
    }


    public bool DeleteAttachedDocument(int DocumentID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DELAD"));
        alParameters.Add(new SqlParameter("@DocumentID", DocumentID));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32() > 0;
    }


    public bool DeleteAttachedDocument(int RequestID, int RequestTypeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DAAD"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        alParameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32() > 0;
    }




    public static int GetApplicableEmergencyLeave(int EmployeeID, string LeaveFromDate, string LeaveToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GELD"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@LeaveFromDate", LeaveFromDate));
        alParameters.Add(new SqlParameter("@LeaveToDate", LeaveToDate));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32();
    }



    public static bool CanRequestEmergencyLeave(int EmployeeID, DateTime LeaveFromDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CAEL"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@LeaveFromDate", LeaveFromDate));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32() > 0;
    }

    /// <summary>
    /// To Get Eligible Leave Pay Days
    /// </summary>
    /// <param name="blnIsLeavePay"></param>
    /// <returns>string</returns>
    public string GetEligibleLeavePayDays(bool blnIsLeavePay)
    {

        DataTable dtVacation;
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", "11"));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        parameters.Add(new SqlParameter("@CompID", CompanyID));
        parameters.Add(new SqlParameter("@FromDate1", LeaveFromDate));
        parameters.Add(new SqlParameter("@ToDate1", LeaveToDate));
        parameters.Add(new SqlParameter("@VacationID", VacationID));
        parameters.Add(new SqlParameter("@IsConsiderAbsentDays", false));
        parameters.Add(new SqlParameter("@IsLeavePay", blnIsLeavePay));
        dtVacation = ExecuteDataTable("spPayVacationProcessTransactions", parameters);

        if (dtVacation.Rows.Count > 0)
        {
            if (dtVacation.Rows[0]["EligibleLeavePayDays"] != System.DBNull.Value)
            {
                return dtVacation.Rows[0]["EligibleLeavePayDays"].ToString();
            }
        }

        return "0";

    }

    public DataTable DisplayReasons()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Leave));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        parameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspLeaveRequest", parameters);
    }
    public int GetCompany()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GC"));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", parameters));
    }
    public int GetHoildaysWithOffDays(int intEmployeeID, DateTime FromDate,DateTime Todate)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 29));
        parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        parameters.Add(new SqlParameter("@FromDate1", FromDate.ToString("dd-MMM-yyyy")));
        parameters.Add(new SqlParameter("@ToDate1", Todate.ToString("dd-MMM-yyyy")));
        return Convert.ToInt32(ExecuteScalar("spPayVacationProcessTransactions", parameters));
    }


    public static bool GetRequestDetails(int intEmployeeID, string LeaveFromDate, string LeaveToDate,int LeaveTypeID )
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GTLD"));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@LeaveFromDate", LeaveFromDate.ToDateTime()));
        alParameters.Add(new SqlParameter("@LeaveToDate", LeaveToDate.ToDateTime()));
        alParameters.Add(new SqlParameter("@LeaveTypeId", LeaveTypeID));
        return new DataLayer().ExecuteScalar("HRspLeaveRequest", alParameters).ToInt32() > 0;
    }
}
