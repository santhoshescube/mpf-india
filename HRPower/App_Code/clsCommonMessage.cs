﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Microsoft.ReportingServices.ReportRendering;
using System.Web.Hosting;
using System.Configuration;
using System.Threading;
using System.Web.UI;

/// <summary>
/// Summary description for clsCommonMessage
/// </summary>
public class clsCommonMessage : DataLayer
{
    private int ReportingTo { get; set; }
    private int HOD { get; set; }
    private int AlternateAuthority { get; set; }
    private int HigherAuthority { get; set; }
    private int FinalAuthority { get; set; }
    private int CurrentLevelID { get; set; }
    private int RequestedByID { get; set; }


    private bool IsReportingToRequired { get; set; }
    private bool IsHODRequired { get; set; }
    private bool IsLevelBasedForwarding { get; set; }
    private bool IsLevelNeeded { get; set; }
    private string FromEmailID { get; set; }


    public eMessageType MessageType { get; set; }
    public DateTime? MessageDueDate;
    public List<ApprovalAuthorities> Authorities { get; set; }
    public List<AlreadySendAuthorities> AlreadySend { get; set; }


    private static string sp
    {
        get
        {
            return "HRspCommonMessage";
        }
    }
    private string MessageEmployee { get; set; }
    private string MessageAuthorities { get; set; }
    private string MessageDate { get; set; }
    private StringBuilder ForwardedEmployees { get; set; }

    private clsCommonMessage objClsCommonMessage;
    private clsCommonMessage CommonMessage
    {
        get
        {
            if (objClsCommonMessage == null)
                return new clsCommonMessage();
            else
                return objClsCommonMessage;
        }
    }

    private List<clsMessageMaster> Messages { get; set; }
    private eActionBy ActionBy { get; set; }

    public clsCommonMessage()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public static void SendMessage(int? RequestedBy, int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, string Type)
    {


        //ReferenceID is the corresponding request id
        //RequestedBy is the employeeid
        new clsCommonMessage().ProcessMessage(RequestedBy, ReferenceID, ReferenceType, MessageType, Action, new clsUserMaster().GetEmployeeId(), Type);
    }

    private void SetPerformanceMessage(int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action)
    {
        //Performance evaluation
        //Send message to all the selected evaluators
        //ReferenceID is the corresponding performance initiationid


        string MessageMainContent = "";

        // PerformanceEvaluationPeriod = GetMessageDate(ReferenceID);
        MessageDate = GetMessageDate(ReferenceID, ReferenceType);

        //RequestedByID = new clsUserMaster().GetEmployeeId();
        MessageMainContent = MessageType.ToString().Replace("__", " ").Split('_')[0];
        MessageDueDate = GetMessageDueDate(ReferenceID, ReferenceType);


        List<ApprovalAuthorities> RequestedTo = new List<ApprovalAuthorities>();
        if (ReferenceType == eReferenceTypes.PerformanceAction)
        {

            if (Action == eAction.Applied)
            {
                RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = new clsUserMaster().GetEmployeeId() });
                MessageAuthorities = MessageMainContent + "[" + clsUserMaster.GetEmployeeFullName(RequestedByID) + "] waiting for approval";

            }
            else
            {
                //RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = new clsUserMaster().GetEmployeeId() });
                //RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = RequestedByID, LevelID = 0 });
                //MessageAuthorities = MessageMainContent +" "+ Action.ToString();

                DeleteExistingMessages(ReferenceID, ReferenceType, null);

                return;
            }


            // RequestedTo.Add(new clsRequestedTo() { RequestedTo = RequestedByID });
            //if (AlternateAuthority != RequestedByID)
            // RequestedTo.Add(new clsRequestedTo() { RequestedTo = AlternateAuthority });

            // if (HigherAuthority != RequestedByID)

            // RequestedTo.Add(new clsRequestedTo() { RequestedTo = HigherAuthority });


        }
        else
        {
            RequestedTo = (List<ApprovalAuthorities>)GetAllPerformanceInitiationEvaluators(ReferenceID);
            MessageAuthorities = MessageMainContent + " waiting for action";
        }



        //Messages.Add(new clsMessageMaster()
        //{
        //    Message = MessageAuthorities,
        //    MessageType = MessageType,
        //    ReferenceID = ReferenceID,


        //    ReferenceType = ReferenceType,
        //    StatusID = (int)eAction.Applied,
        //    RequestedTo = RequestedTo,
        //});



        //         public List<ApprovalAuthorities> RequestedTo { get; set; }


        Messages = new List<clsMessageMaster>();
        Messages.Add(new clsMessageMaster()
        {
            Message = MessageAuthorities,
            MessageType = MessageType,
            ReferenceID = ReferenceID,
            ReferenceType = ReferenceType,
            StatusID = (int)eAction.Applied,
            RequestedTo = RequestedTo
        });



        DeleteExistingMessages(ReferenceID, ReferenceType, new clsUserMaster().GetEmployeeId());

        //SendEmail(Action);
        SaveMessages();
    }


    //private void SetPerformanceMessage(int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType)
    //{
    //    //Performance evaluation
    //    //Send message to all the selected evaluators
    //    //ReferenceID is the corresponding performance initiationid


    //    string MessageMainContent = "";

    //    // PerformanceEvaluationPeriod = GetMessageDate(ReferenceID);
    //    MessageDate = GetMessageDate(ReferenceID, ReferenceType);

    //    RequestedByID = new clsUserMaster().GetEmployeeId();
    //    MessageMainContent = MessageType.ToString().Replace("__", " ").Split('_')[0];
    //    MessageDueDate = GetMessageDueDate(ReferenceID, ReferenceType);


    //    List<ApprovalAuthorities> RequestedTo = new List<ApprovalAuthorities>();
    //    if (ReferenceType == eReferenceTypes.PerformanceAction)
    //    {

    //        RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = RequestedByID, LevelID = 0 });
    //        RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = new clsUserMaster().GetEmployeeId() });


    //        // RequestedTo.Add(new clsRequestedTo() { RequestedTo = RequestedByID });
    //        //if (AlternateAuthority != RequestedByID)
    //        // RequestedTo.Add(new clsRequestedTo() { RequestedTo = AlternateAuthority });

    //        // if (HigherAuthority != RequestedByID)

    //        // RequestedTo.Add(new clsRequestedTo() { RequestedTo = HigherAuthority });
    //        MessageAuthorities = MessageMainContent + GetMessageContent(ReferenceID, (int)ReferenceType) + " waiting for approval";

    //    }
    //    else
    //    {
    //        RequestedTo = (List<ApprovalAuthorities>)GetAllPerformanceInitiationEvaluators(ReferenceID);
    //        MessageAuthorities = MessageMainContent + " waiting for action";
    //    }



    //    //Messages.Add(new clsMessageMaster()
    //    //{
    //    //    Message = MessageAuthorities,
    //    //    MessageType = MessageType,
    //    //    ReferenceID = ReferenceID,


    //    //    ReferenceType = ReferenceType,
    //    //    StatusID = (int)eAction.Applied,
    //    //    RequestedTo = RequestedTo,
    //    //});



    //    //         public List<ApprovalAuthorities> RequestedTo { get; set; }


    //    Messages = new List<clsMessageMaster>();
    //    Messages.Add(new clsMessageMaster()
    //    {
    //        Message = MessageAuthorities,
    //        MessageType = eMessageTypes.Performance__Evaluation,
    //        ReferenceID = ReferenceID,
    //        ReferenceType = eReferenceTypes.PerformanceEvaluation,
    //        StatusID = (int)eAction.Applied,
    //        RequestedTo = RequestedTo
    //    });



    //    DeleteExistingMessages(ReferenceID, ReferenceType, null);

    //    //SendEmail(Action);
    //    SaveMessages();
    //}






    private void SendEmail(string Subject)
    {
        Subject = Subject + " request";
        var AuthorityDetails = Messages.Where(t => t.Type == eType.Authority);

        var AuthorityEmailMessage = AuthorityDetails != null && AuthorityDetails.Count() > 0 ? AuthorityDetails.FirstOrDefault().Email : null;
        //var AuthoritiesEmailIDs = AuthorityDetails.Select(t => t.RequestedTo).ToList();

        var RequestedEmployeeDetails = Messages.Where(t => t.Type == eType.Employee);
        var RequestedEmployeeEmailMessage = RequestedEmployeeDetails != null && RequestedEmployeeDetails.Count() > 0 ? RequestedEmployeeDetails.FirstOrDefault().Email : null;

        FromEmailID = clsUserMaster.GetFromEmailID();

        StringBuilder ToAuthorityEmailIds = new StringBuilder();

        if (FromEmailID == string.Empty)
        {
            // ScriptManager.RegisterClientScriptBlock(new Control(), this.GetType(), "Message", "alert('mail cannot be send sice the from mail id is not set');", true);
            return;
        }
        else
        {
            //Send Message To employee

            foreach (clsMessageMaster EmployeeDetails in RequestedEmployeeDetails)
            {
                foreach (ApprovalAuthorities EmployeeEmailID in EmployeeDetails.RequestedTo)
                {
                    // Thread t1 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, AuthorityEmailID, ReportingToEmailID, Subject, MessageAuthorities, "", false, clsMailSettings.AccountType.Email));
                    Thread t1 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, EmployeeEmailID.OfficialEmailID, "", Subject, RequestedEmployeeEmailMessage, "", false, clsMailSettings.AccountType.Email));
                    t1.Start();
                    break;
                }
                break;
            }
            //Send Message To Authorities
            foreach (clsMessageMaster Authority in AuthorityDetails)
            {
                foreach (ApprovalAuthorities AuthorityEmailIDs in Authority.RequestedTo)
                {
                    if (AuthorityEmailIDs.OfficialEmailID != string.Empty)
                    {
                        ToAuthorityEmailIds.Append(AuthorityEmailIDs.OfficialEmailID);
                        ToAuthorityEmailIds.Append(",");
                    }
                }
                break;
            }

            if (ToAuthorityEmailIds.Length > 1)
                ToAuthorityEmailIds.Remove(ToAuthorityEmailIds.Length - 1, 1);

            if (ToAuthorityEmailIds.Length > 0)
            {
                Thread thread = new Thread(() => new clsMailSettings().SendMail(FromEmailID, ToAuthorityEmailIds.ToString(), "", Subject, AuthorityEmailMessage, "", false, clsMailSettings.AccountType.Email));
                thread.Start();
            }

        }

    }

    private void ProcessMessage(int? RequestedBy, int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, int? ActionByID, string RequestRemarks)
    {

        //Find the logged in employee emailID
        FromEmailID = clsUserMaster.GetFromEmailID();


        string MessageMainContent = "";
        string Remarks = "";
        MessageEmployee = "";
        MessageAuthorities = "";


        RequestedByID = RequestedBy.ToInt32();
        if (ReferenceType == eReferenceTypes.PerformanceAction || ReferenceType == eReferenceTypes.PerformanceEvaluation)
        {
            GetApprovalLevels(RequestedBy, ReferenceID, ReferenceType, eApprovalModule.Ess);
            SetPerformanceMessage(ReferenceID, ReferenceType, MessageType, Action);
            return;
        }

        //GetApprovalLevels(RequestedBy, ReferenceID, ReferenceType,(ReferenceType == eReferenceTypes.JobApproval ? eApprovalModule.Job : eApprovalModule.Ess));

        GetApprovalLevels(RequestedBy, ReferenceID, ReferenceType, eApprovalModule.Ess);

        //Check if reporting to approval is required or not.If required then send message to reporting to first
        //If reporting to reject the message then stop else check if level needed is true.
        //if level needed then check if the approval is level based forwarding ,if yes then send message based on the level mentioned
        //else send message to all the authorities .and the final approval or reject is the last level assigned employee
        if (IsReportingToRequired)
        {
            if( Authorities.Find(t => t.LevelID == -1)!=null)
            ReportingTo = Authorities.Find(t => t.LevelID == -1).EmployeeID;

            //For job approval if there is no reporting to for the employee then send message to next highere level
            if (ReportingTo == 0 && ReferenceType == eReferenceTypes.JobApproval)
                IsReportingToRequired = false;
            if (ReportingTo == 0)
                IsReportingToRequired = false;
        }
        //----------------------------------------HOD-----------------------------------------------------------
        if (IsHODRequired)
        {
            if(Authorities.Find(t => t.LevelID == 0)!=null)
            HOD = Authorities.Find(t => t.LevelID == 0).EmployeeID;
            if (HOD == 0)
                IsHODRequired = false;
            //For job approval if there is no hod for the employee then send message to next higher level
            if (HOD == 0 && ReferenceType == eReferenceTypes.JobApproval)
                IsHODRequired = false;
        }
        //--------------------------------------------------------------------------------------------------------
        //Getting the final authority for the particular request type
        FinalAuthority = Authorities.OrderByDescending(t => t.LevelID).First().EmployeeID.ToInt32();

        //Constructing the message main content from the request 
        MessageMainContent = MessageType.ToString().Replace("__", " ").Split('_')[0];

        //For first time -->Message date is the current date
        //Other -->Message date is the message date saved in the table for the first time request
        MessageDate = GetMessageDate(ReferenceID, ReferenceType);

        if (Action == eAction.Pending)
        {
            UpdatePendingStatus(ReferenceID, ReferenceType, MessageMainContent + " Request(s) Pending");
            return;
        }

        GetCurrentRequestActionByEnum(ActionByID, Action);

        Remarks = (RequestRemarks == "" ? " request(" : " request-" + RequestRemarks + "(");



        //Constructing the Employee message and Approval authorities message
        GetEmployeeAndAuthortiesMessage(Action, MessageMainContent, Remarks);

        //Getting the employees to whom all the messages has to be send
        SetRequestedTo(ReferenceID, ReferenceType, MessageType, Action, MessageEmployee, MessageAuthorities);

        //Proceed only if the action is not pending
        if (Action == eAction.Pending)
            return;

        DeleteExistingMessages(ReferenceID, ReferenceType, null);
        SaveMessages();
        SendEmail(MessageMainContent);
    }





    private void GetCurrentRequestActionByEnum(int? ActionByID, eAction Action)
    {

        if ((Action == eAction.Applied || Action == eAction.RequestForCancel) && (ActionByID == new clsUserMaster().GetEmployeeId()))
        {
            ActionBy = eActionBy.None;
            return;
        }


        if (ActionByID == FinalAuthority)
            ActionBy = eActionBy.FinalAuthority;


         //if the reportig to and middle authority are same,when reporting to approve the request,no need to send the approval message
        //again to the same employee,instead send the approval message to next employee
        else if (ActionByID == ReportingTo && !IsHODRequired  && (ActionByID == Authorities.Where(t => t.LevelID == 1).Select(t => t.EmployeeID).SingleOrDefault()))
        {
            ActionBy = eActionBy.MiddleAuthorities;
            CurrentLevelID = 1;

        }
        else if (ActionByID == ReportingTo)
            ActionBy = eActionBy.ReportingTo;
        //------------------------------------------HOD--------------------------------------------
        else if (ActionByID == HOD && (ActionByID == Authorities.Where(t => t.LevelID == 1).Select(t => t.EmployeeID).SingleOrDefault()))
        {
            ActionBy = eActionBy.MiddleAuthorities;
            CurrentLevelID = 1;
        }

        else if (ActionByID == HOD)
            ActionBy = eActionBy.HOD;
        //------------------------------------------------------------------------------------------
        else
        {
            var MiddleAuthority = Authorities.Where(t => t.EmployeeID != ReportingTo && t.EmployeeID != HOD && t.EmployeeID != FinalAuthority && t.EmployeeID == ActionByID);
            if (MiddleAuthority.Count() > 0)
            {
                ActionBy = eActionBy.MiddleAuthorities;
                foreach (ApprovalAuthorities a in MiddleAuthority)
                {
                    if (MiddleAuthority.Count() > 1)
                    {
                        if (ReportingTo == 0)
                        {
                            if (!(a.IsReportingTo))
                                CurrentLevelID = a.LevelID;
                        }
                        //--------------------------------------HOD----------------------------------------------
                        if (HOD == 0)
                        {
                            if (!(a.IsHOD))
                                CurrentLevelID = a.LevelID;
                        }
                        //------------------------------------------------------------------------------------------
                    }
                    else
                    {
                        CurrentLevelID = a.LevelID;
                        break;
                    }

                }
            }
            else
                ActionBy = eActionBy.None;
        }


    }
    private void GetEmployeeAndAuthortiesMessage(eAction Action, string MessageMainContent, string Remarks)
    {

        bool CanContinue = true;

        switch (Action)
        {
            case eAction.Cancelled:
                MessageEmployee = "Your " + MessageMainContent + Remarks + MessageDate + ") has been cancelled";
                MessageAuthorities = MessageMainContent + " request cancelled";

                CanContinue = false;
                break;


        }


        if (!CanContinue)
            return;

        switch (ActionBy)
        {
            //For all the first time request action by will be none
            case eActionBy.None:
                if (Action == eAction.Applied)
                    MessageAuthorities = MessageMainContent + " request waiting for approval";
                else if (Action == eAction.RequestForCancel)
                    MessageAuthorities = MessageMainContent.Replace("Cancel", "") + " request waiting for cancellation";
                break;

            case eActionBy.ReportingTo:
            case eActionBy.HOD:
                switch (Action)
                {
                    case eAction.Approved:
                    case eAction.Reject:
                        //Checking if the final approval is based on reporting to approval or level based
                        //If only reporting to then the final approval is reporting to approval and no need to forward the message to others
                        if (!IsLevelNeeded && ((ActionBy == eActionBy.ReportingTo && !IsHODRequired) || (ActionBy == eActionBy.HOD && IsHODRequired)))
                        {
                            MessageEmployee = "Your " + MessageMainContent + Remarks + MessageDate + ") has been " + (Action == eAction.Approved ? "approved" : "rejected");
                            MessageAuthorities = MessageMainContent + " request " + (Action == eAction.Approved ? "approved" : "rejected");
                            return;
                        }
                        else
                        {
                            //If level based forwarding is there ,then if the reporting to approve the request ,then forward the request as forwarded to the levels and not the requested employee
                            //if reporting to reject the request then send request rejected message to requested employee and all the levels
                            MessageEmployee = (Action == eAction.Approved ? "" : ("Your " + MessageMainContent + Remarks + MessageDate + ") has been rejected"));
                            MessageAuthorities = MessageMainContent + " request " + (Action == eAction.Approved ? "forwarded" : "rejected");
                        }

                        break;

                    case eAction.Cancelled:
                        if (!IsLevelNeeded)
                        {
                            MessageEmployee = "Your " + MessageMainContent + Remarks + MessageDate + ") has been cancelled";
                            MessageAuthorities = MessageMainContent + " request cancelled";
                            //MessageAuthorities = "Request for " + MessageMainContent + " cancellation forwarded";
                        }
                        else
                        {
                            MessageAuthorities = "Request for " + MessageMainContent + " cancellation forwarded";
                        }
                        break;

                    case eAction.Pending:
                        MessageAuthorities = MessageMainContent + " Request(s) Pending";
                        break;
                }
                break;
            case eActionBy.MiddleAuthorities:
                switch (Action)
                {
                    case eAction.Approved:
                    case eAction.Reject:

                        MessageEmployee = (Action == eAction.Approved ? "" : "Your " + MessageMainContent + Remarks + MessageDate + ") has been rejected");
                        MessageAuthorities = MessageMainContent + " request " + (Action == eAction.Approved ? "forwarded" : "rejected");
                        break;

                    case eAction.Cancelled:
                        MessageAuthorities = "Request for " + MessageMainContent + " cancellation forwarded";
                        break;

                    case eAction.Pending:
                        MessageAuthorities = MessageMainContent + " Request(s) Pending";
                        break;
                }
                break;


            case eActionBy.FinalAuthority:
                switch (Action)
                {
                    case eAction.Applied:
                        MessageAuthorities = MessageMainContent + " request waiting for approval";
                        break;
                    case eAction.Approved:
                    case eAction.Reject:
                        MessageEmployee = "Your " + MessageMainContent + Remarks + MessageDate + ") has been " + (Action == eAction.Approved ? "approved" : "rejected");
                        MessageAuthorities = MessageMainContent + " request " + (Action == eAction.Approved ? "approved" : "rejected");
                        break;

                    case eAction.RequestForCancel:
                        MessageAuthorities = MessageMainContent.Replace("Cancel", "") + " request waiting for cancellation";
                        break;

                    case eAction.Cancelled:
                        MessageEmployee = "Your " + MessageMainContent + Remarks + MessageDate + ") has been cancelled";
                        MessageAuthorities = MessageMainContent + " request cancelled";
                        break;

                    case eAction.Pending:
                        MessageAuthorities = MessageMainContent + " Request(s) Pending";
                        break;
                }
                break;
            default:
                break;
        }
    }



    private void UpdatePendingStatus(int ReferenceID, eReferenceTypes ReferenceType, string Message)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 10));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        Param.Add(new SqlParameter("@RequestTypeID", (int)ReferenceType));
        Param.Add(new SqlParameter("@Message", Message));
        Param.Add(new SqlParameter("@FromAuthorityID", new clsUserMaster().GetEmployeeId()));
        ExecuteScalar(sp, Param);
    }

    //public void SaveOfferMessage(int EmployeeId, int ReferenceId, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, bool isFirstAuthority)
    //{
    //    string sMessage = "";
    //    switch (Action)
    //    {
    //        case eAction.Reject:
    //            sMessage = "Offer letter rejected.";
    //            break;
    //        case eAction.Approved:
    //            sMessage = "Offer letter approved";
    //            break;
    //        case eAction.Applied:
    //            sMessage = isFirstAuthority == true ? "Offer letter  waiting for approval" : "Offer letter forwarded for approval";
    //            break;

    //    }

    //    MessageDate = GetMessageDate(ReferenceId, ReferenceType);
    //    // SetRequestedTo(ReferenceId ,ReferenceType ,MessageType ,Action ,"",sMessage);
    //    DeleteExistingMessages(ReferenceId, ReferenceType, null);
    //    List<clsRequestedTo> RequestedTo = new List<clsRequestedTo>();
    //    Messages = new List<clsMessageMaster>();
    //    RequestedTo.Add(new clsRequestedTo() { RequestedTo = EmployeeId });

    //    //Messages.Add(new clsMessageMaster()
    //    //{
    //    //    Message = sMessage,
    //    //    MessageType = MessageType,
    //    //    ReferenceID = ReferenceId,
    //    //    ReferenceType = ReferenceType,
    //    //    StatusID = (int)Action,
    //    //    RequestedTo = RequestedTo,
    //    //});

    //    SaveMessages();
    //}

    //public void RejectOfferMessage(DataTable EmployeeIds, int ReferenceId, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action)
    //{

    //    MessageDate = GetMessageDate(ReferenceId, ReferenceType);
    //    DeleteExistingMessages(ReferenceId, ReferenceType, null);
    //    List<clsRequestedTo> RequestedTo = new List<clsRequestedTo>();
    //    Messages = new List<clsMessageMaster>();
    //    for (int i = 0; i < EmployeeIds.Rows.Count; i++)
    //    {
    //        RequestedTo.Add(new clsRequestedTo() { RequestedTo = Convert.ToInt32(EmployeeIds.Rows[i]["Employeeid"]) });
    //    }

    //    //Messages.Add(new clsMessageMaster()
    //    //{
    //    //    Message =  "Offer letter rejected.", 
    //    //    MessageType = MessageType,
    //    //    ReferenceID = ReferenceId,
    //    //    ReferenceType = ReferenceType,
    //    //    StatusID = (int)Action,
    //    //    RequestedTo = RequestedTo,
    //    //});

    //    SaveMessages();
    //}



    public void SaveOfferMessage(int EmployeeId, int ReferenceId, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, bool isFirstAuthority)
    {
        string sMessage = "";
        switch (Action)
        {
            case eAction.Reject:
                sMessage = "Offer letter rejected.";
                break;
            case eAction.Approved:
                sMessage = "Offer letter approved";
                break;
            case eAction.Applied:
                sMessage = isFirstAuthority == true ? "Offer letter  waiting for approval" : "Offer letter forwarded for approval";
                break;

        }

        MessageDate = GetMessageDate(ReferenceId, ReferenceType);
        // SetRequestedTo(ReferenceId ,ReferenceType ,MessageType ,Action ,"",sMessage);
        DeleteExistingMessages(ReferenceId, ReferenceType, null);
        List<ApprovalAuthorities> RequestedTo = new List<ApprovalAuthorities>();
        Messages = new List<clsMessageMaster>();
        RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = EmployeeId });

        Messages.Add(new clsMessageMaster()
        {
            Message = sMessage,
            MessageType = MessageType,
            ReferenceID = ReferenceId,
            ReferenceType = ReferenceType,
            StatusID = (int)Action,
            RequestedTo = RequestedTo
        });

        SaveMessages();
    }

    public void RejectOfferMessage(DataTable EmployeeIds, int ReferenceId, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action)
    {

        MessageDate = GetMessageDate(ReferenceId, ReferenceType);
        DeleteExistingMessages(ReferenceId, ReferenceType, null);
        List<ApprovalAuthorities> RequestedTo = new List<ApprovalAuthorities>();
        Messages = new List<clsMessageMaster>();
        for (int i = 0; i < EmployeeIds.Rows.Count; i++)
        {
            RequestedTo.Add(new ApprovalAuthorities() { EmployeeID = Convert.ToInt32(EmployeeIds.Rows[i]["Employeeid"]) });
        }



        Messages.Add(new clsMessageMaster()
        {
            Message = "Offer letter rejected.",
            MessageType = MessageType,
            ReferenceID = ReferenceId,
            ReferenceType = ReferenceType,
            StatusID = (int)Action,
            RequestedTo = RequestedTo,
        });

        SaveMessages();
    }




    private void SaveMessages()
    {
        try
        {
            BeginTransaction();
            Int64 MessageID = 0;
            foreach (clsMessageMaster objMessage in Messages)
            {
                ArrayList Param = new ArrayList();
                Param.Add(new SqlParameter("@Mode", 4));
                Param.Add(new SqlParameter("@MessageDate", MessageDate));
                Param.Add(new SqlParameter("@RequestedBy", RequestedByID));
                Param.Add(new SqlParameter("@ReferenceID", objMessage.ReferenceID));
                Param.Add(new SqlParameter("@RequestTypeID", (int)objMessage.ReferenceType));
                Param.Add(new SqlParameter("@MessageTypeID", (int)objMessage.MessageType));
                Param.Add(new SqlParameter("@Message", objMessage.Message));
                Param.Add(new SqlParameter("@StartDate", MessageDueDate));
                Param.Add(new SqlParameter("@StatusID", objMessage.StatusID));
                MessageID = ExecuteScalar(sp, Param).ToInt64();

                foreach (ApprovalAuthorities RequestedToID in objMessage.RequestedTo)
                {
                    if (RequestedToID.EmployeeID > 0)
                    {
                        ArrayList Param1 = new ArrayList();
                        Param1.Add(new SqlParameter("@Mode", 5));
                        Param1.Add(new SqlParameter("@MessageID", MessageID));

                        Param1.Add(new SqlParameter("@RequestedBy", RequestedByID));
                        Param1.Add(new SqlParameter("@ReferenceID", objMessage.ReferenceID));
                        Param1.Add(new SqlParameter("@MessageTypeID", (int)objMessage.MessageType));
                        Param1.Add(new SqlParameter("@StatusID", objMessage.StatusID));
                        Param1.Add(new SqlParameter("@RequestTypeID", (int)objMessage.ReferenceType));
                        Param1.Add(new SqlParameter("@RequestedTo", RequestedToID.EmployeeID));
                        Param1.Add(new SqlParameter("@FromAuthorityID", new clsUserMaster().GetEmployeeId()));
                        ExecuteScalar(sp, Param1).ToInt64();
                    }
                }
            }
            CommitTransaction();
        }

        catch (Exception)
        {
            RollbackTransaction();
        }
    }

    /// <summary>
    /// This function will returns the corresponding requesting to ids for a particular request
    /// </summary>
    /// <returns></returns>
    private List<ApprovalAuthorities> GetApprovalAuthorities(eAction Action)
    {
        List<ApprovalAuthorities> TempAuthorities = new List<ApprovalAuthorities>();
        //if it is a cancel request,and if any body approve the request
        if (Action == eAction.Cancelled)
        {
            Authorities.Remove(Authorities.Find(t => t.EmployeeID == new clsUserMaster().GetEmployeeId()));

            //If the approving authority is the same as requested employee then no need to send confirmation message to the approving authority
            Authorities.Remove(Authorities.Find(t => t.EmployeeID == RequestedByID));
            return Authorities;
        }



        switch (ActionBy)
        {
            //First time request ,check if reporting to approval is required ,if yes then forward the message to reporting to only
            //Else check if it is level based forwarding if yes then forward message to the first level else forward messsage to all the levels

            //ActionBy = None will be fired for the first time ,means when the employee makes a request
            case eActionBy.None:

                if (IsReportingToRequired)
                {
                    TempAuthorities = Authorities.Where(t => t.IsReportingTo == true).ToList();
                }
                //-----------------------------------HOD---------------------------------------------------
                else if (IsHODRequired)
                {
                    TempAuthorities = Authorities.Where(t => t.IsHOD == true).ToList();

                }
                //------------------------------------------------------------------------------------------
                else
                {
                    if (IsLevelBasedForwarding)
                    {
                        TempAuthorities = Authorities.Where(t => t.LevelID == 1).ToList();
                    }
                    else
                        TempAuthorities = Authorities.Where(t => t.LevelID != -1).ToList();
                }
                break;

            //If the action is of reporting to employee
            case eActionBy.ReportingTo:
                if (Action == eAction.Reject)
                {
                    //Making an empty list since it a reject request no need to forward the message to others
                    TempAuthorities = Authorities.Where(t => t.LevelID == 0).ToList();
                }
                else
                {
                    if (IsLevelBasedForwarding)
                    {
                        if (IsHODRequired)
                            TempAuthorities = Authorities.Where(t => t.LevelID == 0).ToList();
                        else
                            TempAuthorities = Authorities.Where(t => t.LevelID == 1).ToList();
                    }
                    else
                        TempAuthorities = Authorities.Where(t => t.IsReportingTo == false).ToList();
                }
                break;
            //---------------------------------------HOD--------------------------------------------
            //If the action is of HOD  
            case eActionBy.HOD:
                if (Action == eAction.Reject)
                {
                    //Making an empty list since it a reject request no need to forward the message to others
                    TempAuthorities = Authorities.Where(t => t.LevelID == 0).ToList();
                }
                else
                {
                    if (IsLevelBasedForwarding)    
                    {
                        int  CurEmpID1 = Authorities.Find(t => t.LevelID == (CurrentLevelID + 1)).EmployeeID;

                        //-----------Modified by laxmi
                        if (CurEmpID1 == ReportingTo)
                        {
                            TempAuthorities = Authorities.Where(t => t.LevelID == (CurrentLevelID + 2)).ToList();
                        }
                        else if (CurEmpID1 == HOD)
                        {
                            TempAuthorities = Authorities.Where(t => t.LevelID == (CurrentLevelID + 2)).ToList();
                        }
                        else
                        {
                            TempAuthorities = Authorities.Where(t => t.LevelID == 1).ToList();
                        }
                    }
                                           
                    else
                    {
                        TempAuthorities = Authorities.Where(t => t.IsHOD == false).ToList();
                    }
                       
                }
                break;
            //------------------------------------------------------------------------------------------
            //If the action is of any middle authorities
            case eActionBy.MiddleAuthorities:
                //TempAuthorities = Authorities.Where(t => t.LevelID <= (CurrentLevelID + 1)).ToList();

                if (IsLevelBasedForwarding)
                {
                    int CurEmpID = 0;

                    TempAuthorities = Authorities.Where(t => t.LevelID == (CurrentLevelID + 1)).ToList();
                    if (TempAuthorities.Count > 0)
                    {
                        CurEmpID = Authorities.Find(t => t.LevelID == (CurrentLevelID + 1)).EmployeeID;

                        //-----------Modified by laxmi
                        if (CurEmpID == ReportingTo)
                        {
                            TempAuthorities = Authorities.Where(t => t.LevelID == (CurrentLevelID + 2)).ToList();
                        }
                        if (CurEmpID == HOD)
                        {
                            TempAuthorities = Authorities.Where(t => t.LevelID == (CurrentLevelID + 2)).ToList();
                        }
                    }
                }
                else
                {
                    foreach (AlreadySendAuthorities a in AlreadySend)
                    {
                        if (a.EmployeeID != FinalAuthority)
                            Authorities.Remove(Authorities.Find(t => t.EmployeeID == a.EmployeeID));
                    }

                    //Remove the current employee id to avois sending back the message to same employee
                    Authorities.Remove(Authorities.Find(t => t.EmployeeID == new clsUserMaster().GetEmployeeId()));
                    TempAuthorities = Authorities;
                }
                break;

            //If the action is of final authority
            case eActionBy.FinalAuthority:
                //
                if (Action == eAction.Applied)
                    TempAuthorities = Authorities.Where(t => t.EmployeeID == new clsUserMaster().GetEmployeeId()).ToList();
                else if (RequestedByID == FinalAuthority)
                    TempAuthorities = Authorities.Where(t => t.EmployeeID == -2).ToList();//create an empty list
                else
                    TempAuthorities = Authorities.Where(t => t.EmployeeID != new clsUserMaster().GetEmployeeId()).ToList();
                break;
        }
        return TempAuthorities;
    }

    /// <summary>
    /// Getting the authorities ids to whom all the message has to be send
    /// </summary>
    private void SetRequestedTo(int ReferenceID, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action, string EmployeeMessage, string AuthoritiesMessage)
    {
        Messages = new List<clsMessageMaster>();
        GetAlreadySendMessageAuthorities((int)ReferenceType, ReferenceID);
        switch (Action)
        {
            case eAction.Approved:
            case eAction.Reject:
            case eAction.Cancelled:

                eAction _Action = (((Action == eAction.Approved && ((ActionBy == eActionBy.ReportingTo && IsLevelNeeded && IsHODRequired) ||
                    (ActionBy == eActionBy.ReportingTo)  ||
                    (ActionBy == eActionBy.HOD && IsLevelNeeded) || ActionBy == eActionBy.MiddleAuthorities)) ||
                    (Action == eAction.Cancelled && ActionBy == eActionBy.ReportingTo && !IsLevelNeeded) ||
                    (Action == eAction.Cancelled && ActionBy == eActionBy.HOD && !IsLevelNeeded)) ? eAction.Forwarded : Action); // Forwarded Status is 10 hardcoded
                //Send message to the requesting employee only if the action done by finel authority or the action is done by reporting to when there is no level
                if ((ActionBy == eActionBy.FinalAuthority) || (ActionBy == eActionBy.ReportingTo && Action == eAction.Reject) || (ActionBy == eActionBy.HOD && Action == eAction.Reject) ||
                    ((ActionBy == eActionBy.MiddleAuthorities && Action == eAction.Reject)) || (ActionBy == eActionBy.ReportingTo && !IsLevelNeeded && !IsHODRequired) ||
                    (ActionBy == eActionBy.HOD && !IsLevelNeeded) || (Action == eAction.Cancelled))
                {
                    Messages.Add(new clsMessageMaster()
                    {
                        RequestedTo = new List<ApprovalAuthorities>() { new ApprovalAuthorities() { EmployeeID = RequestedByID, LevelID = 0, OfficialEmailID = clsUserMaster.GetEmployeeEmailID(RequestedByID) } },
                        Message = EmployeeMessage,
                        Email = EmployeeEmailBody(GetMailRequestDetails(ReferenceID, ReferenceType), ReferenceType, MessageType, Action.ToString()),
                        MessageType = eMessageTypes.Confirmation,//This confirmation status is to show the message in the requesting employee myrequest dashboard tab
                        ReferenceID = ReferenceID,
                        ReferenceType = ReferenceType,
                        StatusID = (int)Action,
                        Type = eType.Employee
                    });
                }
                //Getting all the approval authorities ids
                Messages.Add(new clsMessageMaster()
                {

                    RequestedTo = GetApprovalAuthorities(Action),
                    Message = AuthoritiesMessage,
                    MessageType = MessageType,
                    ReferenceID = ReferenceID,
                    ReferenceType = ReferenceType,
                    //StatusID = (((Action == eAction.Approved && ((ActionBy == eActionBy.ReportingTo && IsLevelNeeded) || ActionBy == eActionBy.MiddleAuthorities)) || (Action == eAction.Cancelled && ActionBy == eActionBy.ReportingTo && !IsLevelNeeded)) ? (int)eAction.Forwarded : (int)Action), // Forwarded Status is 10 hardcoded
                    StatusID = (int)_Action,
                    Email = GetHtml(GetMailRequestDetails(ReferenceID, ReferenceType), ReferenceType, MessageType, _Action),
                    Type = eType.Authority
                });

                break;

            //The status applied and requestforcancel can be initiated by the requesting employee .so no need to send the message to the requesting employee
            //The requesting message will be forwarded based on the request settings 
            case eAction.Applied:
            case eAction.RequestForCancel:
                Messages.Add(new clsMessageMaster()
                {
                    RequestedTo = GetApprovalAuthorities(Action),
                    Message = AuthoritiesMessage,
                    Email = GetHtml(GetMailRequestDetails(ReferenceID, ReferenceType), ReferenceType, MessageType, Action),
                    MessageType = MessageType,
                    ReferenceID = ReferenceID,
                    ReferenceType = ReferenceType,
                    StatusID = (int)Action,
                    Type = eType.Authority
                });
                break;

            //If the action is pending then we are only updating the existing request message in the db 
            case eAction.Pending:
                UpdatePendingStatus(ReferenceID, ReferenceType, MessageAuthorities);
                break;

            default:
                break;
        }
    }
    private enum eActionBy
    {
        ReportingTo = 1,
        MiddleAuthorities = 2,
        FinalAuthority = 3,
        None = 4,//For first time the action by must be none
        HOD = 5
    }

    public object GetAllPerformanceInitiationEvaluators(int ReferenceID)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 12));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        DataTable dt = new DataLayer().ExecuteDataTable(sp, Param);

        List<ApprovalAuthorities> RequestedTo = new List<ApprovalAuthorities>();
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                RequestedTo.Add(new ApprovalAuthorities()
                    {
                        EmployeeID = dr["EvaluatorID"].ToInt32(),
                        LevelID = 0
                    });
            }

        }
        return RequestedTo;
    }

    private static string GetMessageDate(int ReferenceID, eReferenceTypes RequestTypeID)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 2));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        Param.Add(new SqlParameter("@RequestTypeID", (int)RequestTypeID));
        return new DataLayer().ExecuteScalar(sp, Param).ToString();

    }


    private static string GetMessageDate(int ReferenceID)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 11));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        return new DataLayer().ExecuteScalar(sp, Param).ToString();

    }

    private static string GetMessageContent(int ReferenceID, int ReferenceTypeID)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 14));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        Param.Add(new SqlParameter("@ReferenceTypeID", ReferenceTypeID));
        return new DataLayer().ExecuteScalar(sp, Param).ToString();

    }



    private static DateTime GetMessageDueDate(int ReferenceID, eReferenceTypes ReferenceTypeID)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 13));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
        Param.Add(new SqlParameter("@RequestTypeID", (int)ReferenceTypeID));
        return new DataLayer().ExecuteScalar(sp, Param).ToDateTime();

    }


    public static bool DeleteMessage(int MessageID)
    {
        try
        {
            ArrayList Param = new ArrayList();
            Param.Add(new SqlParameter("@Mode", 6));
            Param.Add(new SqlParameter("@MessageID", MessageID));
            new DataLayer().ExecuteScalar(sp, Param);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }




    public void DeleteMessage(long MessageID, int RequestedTo)
    {
        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 9));
        Param.Add(new SqlParameter("@MessageID", MessageID));
        Param.Add(new SqlParameter("@RequestedTo", RequestedTo));
        ExecuteScalar(sp, Param);

    }

    private bool DeleteExistingMessages(int ReferenceID, eReferenceTypes ReferenceType, int? RequestedTo)
    {
        try
        {
            BeginTransaction();
            ArrayList Param = new ArrayList();
            Param.Add(new SqlParameter("@Mode", 3));
            Param.Add(new SqlParameter("@ReferenceID", ReferenceID));
            Param.Add(new SqlParameter("@RequestTypeID", (int)ReferenceType));

            if (RequestedTo != null)
                Param.Add(new SqlParameter("@RequestedTo", (int)RequestedTo));
            ExecuteScalar(sp, Param).ToDateTime();
            CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return false;
        }

    }

    public static bool DeleteMessages(int ReferenceID, eReferenceTypes ReferenceType)
    {
        return new clsCommonMessage().DeleteExistingMessages(ReferenceID, ReferenceType, null);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="ReferenceID"></param>
    /// <param name="ReferenceType"></param>
    /// <param name="MessageToID"></param>
    /// <returns></returns>
    public static bool DeleteMessages(int ReferenceID, eReferenceTypes ReferenceType, int RequestedToID)
    {
        return new clsCommonMessage().DeleteExistingMessages(ReferenceID, ReferenceType, RequestedToID);
    }



    private class clsMessageMaster
    {
        public int ReferenceID { get; set; }

        public eReferenceTypes ReferenceType { get; set; }

        public eMessageTypes MessageType { get; set; }

        public string Message { get; set; }

        public int StatusID { get; set; }

        public List<ApprovalAuthorities> RequestedTo { get; set; }

        public string Email { get; set; }

        public eType Type { get; set; }

        public clsMessageMaster()
        {
            RequestedTo = new List<ApprovalAuthorities>();
        }
    }

    private class clsRequestedTo
    {
        public int RequestedTo { get; set; }
    }








    private void GetApprovalLevels(int? RequestedBy, int ReferenceID, eReferenceTypes RequestType, eApprovalModule ApprovalModule)
    {

        int RequestTypeID = 0;

        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", 1));
        if (RequestedBy != null && RequestedBy > 0)
            Param.Add(new SqlParameter("@RequestedBy", RequestedBy));
        Param.Add(new SqlParameter("@ReferenceID", ReferenceID));

        Param.Add(new SqlParameter("@MessageRequestTypeID", (int)RequestType));

        if (RequestType == eReferenceTypes.CompensatoryOffRequest)
            RequestTypeID = (int)eReferenceTypes.CompensatoryOffRequest;
        else if (RequestType == eReferenceTypes.TimeExtensionRequest)
            RequestTypeID = (int)eReferenceTypes.TimeExtensionRequest;
        else
            RequestTypeID = (int)RequestType;

        Param.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
        Param.Add(new SqlParameter("@ApprovalModule", (int)ApprovalModule));

        DataSet ds = new DataLayer().ExecuteDataSet(sp, Param);
        DataRow dr;

        if (ds.Tables.Count == 2)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                dr = ds.Tables[1].Rows[0];
                IsReportingToRequired = Convert.ToBoolean(dr["ReportingToRequired"]);
                IsHODRequired = Convert.ToBoolean(dr["HODRequired"]);
                IsLevelBasedForwarding = Convert.ToBoolean(dr["IsLevelBasedForwarding"]);
                IsLevelNeeded = Convert.ToBoolean(dr["IsLevelNeeded"]);
                RequestedByID = (RequestedBy != null && RequestedBy == -1) ? RequestedByID : dr["RequestedBy"].ToInt32();
            }
            
        //    int levelID = 0;
        //abc: for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //    {
        //        if ((!IsReportingToRequired && ds.Tables[0].Rows[i]["LevelID"].ToInt32() == -1) || (!IsHODRequired && ds.Tables[0].Rows[i]["LevelID"].ToInt32() == 0))
        //        {
        //            ds.Tables[0].Rows.RemoveAt(i);
        //            ds.AcceptChanges();
        //            goto abc;
        //        }
        //        for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
        //        {
        //            if (i != j)
        //            {
        //                //////
        //                levelID = (ds.Tables[0].Rows[j]["LevelID"].ToInt32() == 0 || (ds.Tables[0].Rows[j]["LevelID"].ToInt32() == -1 && !IsReportingToRequired)) ? 1 : ds.Tables[0].Rows[j]["LevelID"].ToInt32();
        //                if ((ds.Tables[0].Rows[i]["EmployeeID"].ToInt32() == ds.Tables[0].Rows[j]["EmployeeID"].ToInt32()) || ds.Tables[0].Rows[j]["EmployeeID"].ToInt32() == 0)
        //                {
        //                    if (ds.Tables[0].Rows[j]["LevelID"].ToInt32() == 0)
        //                        IsHODRequired = false;
        //                    for (int k = j + 1; k < ds.Tables[0].Rows.Count; k++)
        //                    {                               
        //                        //ds.Tables[0].Rows[k]["LevelID"] = levelID;
        //                        ds.AcceptChanges();
        //                        levelID = levelID + 1;
        //                    }

        //                    ds.Tables[0].Rows.RemoveAt(j);
        //                    ds.AcceptChanges();
        //                }
        //            }
        //        }
        //    }    
            Authorities = new List<ApprovalAuthorities>();
            foreach (DataRow dr1 in ds.Tables[0].Rows)
            {
                Authorities.Add(new ApprovalAuthorities()
                {
                    EmployeeID = dr1["EmployeeID"].ToInt32(),
                    LevelID = dr1["LevelID"].ToInt32(),
                    OfficialEmailID = dr1["OfficialEmailID"].ToString()
                });
            }
                 
        }
    }

    public class ApprovalAuthorities
    {
        public int LevelID { get; set; }

        public int EmployeeID { get; set; }

        public bool IsReportingTo
        {
            get
            {
                return LevelID == -1;
            }
        }
        public bool IsHOD
        {
            get
            {
                return LevelID == 0;
            }
        }

        public string OfficialEmailID { get; set; }
    }


    private RequestType GetRequestType(eReferenceTypes ReferenceType)
    {
        string ReferenceTypes = ReferenceType.ToString();
        // RequestType RequestTypeID;

        //getting the request type from the reference type to get the higher authorities

        if (ReferenceTypes.StartsWith("DocumentRequest"))
            return RequestType.Document;

        else if (ReferenceTypes.StartsWith("ExpenseRequest"))
            return RequestType.Expense;

        else if (ReferenceTypes.StartsWith("LeaveRequest"))
            return RequestType.Leave;

        else if (ReferenceTypes.StartsWith("LeaveExtensionRequest"))
            return RequestType.LeaveExtension;

        else if (ReferenceTypes.StartsWith("LoanRequest"))
            return RequestType.Loan;

        else if (ReferenceTypes.StartsWith("Resignation"))
            return RequestType.Resignation;

        else if (ReferenceTypes.StartsWith("SalaryAdvanceRequest"))
            return RequestType.SalaryAdvance;

        else if (ReferenceTypes.StartsWith("Training"))
            return RequestType.Training;

        else if (ReferenceTypes.StartsWith("TransferRequest"))
            return RequestType.Transfer;

        else if (ReferenceTypes.StartsWith("Performance"))
            return RequestType.PerformanceAction;

        else if (ReferenceTypes.StartsWith("OfferLetter"))
            return RequestType.OfferLetter;

        else if (ReferenceTypes.StartsWith("AttendanceRequest"))
            return RequestType.AttendanceRequest;
        //else if (ReferenceTypes.StartsWith("RejoinRequest"))
        //    return RequestType.Rejoin;
        else //if (ReferenceTypes.StartsWith("VisaRequest"))
            return RequestType.Visa;
    }







    /// <summary>
    /// Set Delete status as 1
    /// </summary>
    /// <returns></returns>
    public bool SetDeleteStatus(int iRequestedTo, int StatusID)
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", 8),
            new SqlParameter("@RequestedTo", iRequestedTo),
            new SqlParameter("@StatusID", StatusID),
            new SqlParameter("@MessageTypeID", (int)this.MessageType)};

        return Convert.ToInt32(this.ExecuteScalar(sp, alParameters)) > 0;
    }

    /// <summary>
    /// Set Read status as 1
    /// </summary>
    /// <returns></returns>

    public bool SetReadStatus(int RequestedTo)
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", 9),
            new SqlParameter("@RequestedTo",RequestedTo),
            new SqlParameter("@MessageTypeID", (int)this.MessageType)};

        return Convert.ToInt64(this.ExecuteScalar(sp, alParameters)) > 0;
    }

    public static bool CanShowApproveButton(int ReferenceID, int RequestTypeID, int EmployeeID, int RequestedBy)
    {
        //int TempRequestTypeID = 0;
        //if (RequestTypeID == (int)eReferenceTypes.VacationLeaveRequest || RequestTypeID == (int)eReferenceTypes.CompensatoryOffRequest)
        //    TempRequestTypeID = (int)eReferenceTypes.LeaveRequest;
        //else if (RequestTypeID == (int)eReferenceTypes.VacationExtensionRequest || RequestTypeID == (int)eReferenceTypes.TimeExtensionRequest)
        //    TempRequestTypeID = (int)eReferenceTypes.LeaveExtensionRequest;
        //else
        //    TempRequestTypeID = RequestTypeID;






        int TempRequestTypeID = 0;
        if (RequestTypeID == (int)eReferenceTypes.CompensatoryOffRequest)
            TempRequestTypeID = (int)eReferenceTypes.CompensatoryOffRequest;
        else if (RequestTypeID == (int)eReferenceTypes.TimeExtensionRequest)
            TempRequestTypeID = (int)eReferenceTypes.LeaveExtensionRequest;
        else
            TempRequestTypeID = RequestTypeID;



        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", 17),
            new SqlParameter("@ReferenceID",ReferenceID),
              new SqlParameter("@RequestTypeID",RequestTypeID),
                 new SqlParameter("@TempRequestTypeID",TempRequestTypeID),
                  new SqlParameter("@RequestedByID",RequestedBy),
            new SqlParameter("@EmployeeID", EmployeeID)};

        return Convert.ToInt64(new DataLayer().ExecuteScalar(sp, alParameters)) > 0;
    }

    public void GetAlreadySendMessageAuthorities(int RequestTypeID, int ReferenceID)
    {
        ArrayList alParameters = new ArrayList { new SqlParameter("@Mode", 16),
            new SqlParameter("@RequestTypeID",RequestTypeID),
            new SqlParameter("@ReferenceID", ReferenceID)};
        DataTable dt = this.ExecuteDataTable(sp, alParameters);

        AlreadySend = new List<AlreadySendAuthorities>();
        ForwardedEmployees = new StringBuilder();
        foreach (DataRow dr in dt.Rows)
        {
            AlreadySend.Add(new AlreadySendAuthorities()
            {
                EmployeeFullName = dr["EmployeeFullName"].ToString(),
                EmployeeID = dr["FromAuthorityID"].ToInt32(),
                StatusID = (eAction)dr["StatusID"].ToInt32()
            });


            if (((eAction)dr["StatusID"].ToInt32()) == eAction.Forwarded)
            {
                ForwardedEmployees.Append(dr["EmployeeFullName"].ToString());
                ForwardedEmployees.Append(",");
            }
        }
        //Removing the last comma from the string

        if (ForwardedEmployees.Length > 0)
            ForwardedEmployees.Remove(ForwardedEmployees.Length - 1, 1);
    }





    #region MailFunctions

    private string AuthoritiesEmailBody(DataSet dsRequest, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action)
    {
        StringBuilder MessageAuthorities = new StringBuilder();
        StringBuilder messageContent = new StringBuilder();

        DataTable dtRequest = dsRequest.Tables[0];
        if (dtRequest != null && dtRequest.Rows.Count > 0)
        {

            string FirstRow = "<tr><td style='width:20%'> Applied By </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Name"]) + "</td></tr>" +
                                    "<tr><td style='width:20%'> Applied Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + "</td></tr>";

            messageContent.Append("<table style='width: 100%; margin-top: 10px; border: solid 1.2px grey' cellpadding='10px'   cellspacing='10px'>");


            switch (ReferenceType)
            {
                case eReferenceTypes.DocumentRequest:
                    #region Document Request
                    //Document Type
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Type"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Document Number
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Number");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["DocumentNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Document From Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Date"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.LeaveRequest:
                    #region LeaveRequest


                    bool IsHalfDay = dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? true : false;

                    //Leave Type
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkLocation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");



                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Type"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Half Day or Full day
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Is HalfDay");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(IsHalfDay ? "Yes" : "No");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Leave Date OR Leave From Date to Leave ToDate

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append(IsHalfDay ? "Date" : "Period");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(IsHalfDay ? Convert.ToString(dtRequest.Rows[0]["From"]) : Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");



                    #endregion
                    break;

                case eReferenceTypes.VacationLeaveRequest:
                    #region VacationLeaveRequest
                    //TYPE = VACATION
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");





                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append("Vacation");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Leave From Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave from date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["LeaveFromDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Leave To Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave to date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["LeaveToDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Flight Required
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Flight required");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["IsFlightRequired"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Sector Required
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Sector required");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["IsSectorRequired"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Contact Address
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Contact Address");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["ContactAddress"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Contact Address");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["ContactAddress"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Contact Number
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Contact Number");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["TelephoneNo"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    #endregion
                    break;

                case eReferenceTypes.CompensatoryOffRequest:
                    #region CompensatoryOffRequest
                    DataTable dtCompensatoryLeaveDays = dsRequest.Tables[1];
                    //Leave Type Compensatory
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");




                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append("Compensatory");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");



                    //Leave Type Compensatory
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(dtRequest.Rows[0]["IsCredit"].ToInt32() == 1 ? "Credit" : "Leave");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");



                    //Leave from date and leave to date is applicable only to iscredit = 0
                    if (dtRequest.Rows[0]["IsCredit"].ToInt32() == 0)
                    {
                        //Leave From Date
                        messageContent.Append("<tr>");
                        messageContent.Append("<td style='width: 150px' class='label '>");
                        messageContent.Append("Leave from date");
                        messageContent.Append("</td>");
                        messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                        messageContent.Append("<td class='label'>");
                        messageContent.Append(Convert.ToString(dtRequest.Rows[0]["LeaveFromDate"]));
                        messageContent.Append("</td>");
                        messageContent.Append("</tr>");


                        //Leave to Date
                        messageContent.Append("<tr>");
                        messageContent.Append("<td style='width: 150px' class='label '>");
                        messageContent.Append("Leave to date");
                        messageContent.Append("</td>");
                        messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                        messageContent.Append("<td class='label'>");
                        messageContent.Append(Convert.ToString(dtRequest.Rows[0]["LeaveToDate"]));
                        messageContent.Append("</td>");
                        messageContent.Append("</tr>");

                    }
                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Compensatory Leave Days 

                    messageContent.Append(" <tr>");
                    messageContent.Append("<td>Sl.No</td>");
                    messageContent.Append("<td>Date</td>");
                    messageContent.Append(" </tr>");



                    foreach (DataRow dr in dtCompensatoryLeaveDays.Rows)
                    {
                        messageContent.Append("<tr>");

                        //SL.No
                        messageContent.Append("<td style='width: 150px' class='label '>");
                        messageContent.Append(Convert.ToString(dr["SLNo"]));
                        messageContent.Append("</td>");

                        //Worked Days
                        messageContent.Append(" <td class='label'>");
                        messageContent.Append(Convert.ToString(dr["WorkedDay"]));
                        messageContent.Append("</td>");

                        messageContent.Append("</tr>");

                    }

                    #endregion
                    break;

                case eReferenceTypes.LeaveExtensionRequest:
                    #region Leave Extension Request

                    //Extension Type

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Extension Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append("Leave Extension");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Leave Type
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["LeaveType"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Period
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Period");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    //messageContent.Append(dtRequest.Rows[0]["LeaveType"].ToString());
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //No of days
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("No of days");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["NoOfDays"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");




                    #endregion
                    break;

                case eReferenceTypes.TimeExtensionRequest:

                    #region TimeExtension Request

                   // Extension Type;


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Extension Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append("Time Extension");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Extension date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Extension date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["ExtensionFromDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Break Duration
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Break duration");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["NoOfDays"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Break duration");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");



                    #endregion
                    break;

                case eReferenceTypes.VacationExtensionRequest:

                    #region Vacation Extension Request
                    //Extension Type

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Extension Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append("Vacation Extension");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Extension Type
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Mode of extension");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(dtRequest.Rows[0]["IsExtension"].ToInt32() == 1 ? "Extended vacation" : "Shortened vacation");
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //No of days
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("No of days");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["NoOfDays"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    #endregion
                    break;


                //Comment
                case eReferenceTypes.ExpenseRequest:
                    #region ExpenseRequest
                    //Expense Head

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Expense Head");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["ExpenseHead"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Period
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Period");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Amount
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Amount");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Amount"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.SalaryAdvanceRequest:
                    #region Salary Advance Request
                    //Amount
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Amount");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Amount"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("</table>");

                    #endregion
                    break;

                case eReferenceTypes.TransferRequest:
                    #region Transfer Request
                    //Transfer Type
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Transfer Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Type"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Transfer From
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Transfer From");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["From"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Transfer To
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Transfer To");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["To"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Transfer Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Transfer Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["TransferDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.LoanRequest:
                    #region Loan Request
                    //Loan Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Loan Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Date"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Loan Type

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("LoanType");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Type"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Loan Amount

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Loan Amount");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Amount"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Reason

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.TravelRequest:
                    #region Travel Request
                    //Travel date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Leave Type");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("TravelDate");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["TravelDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Place of travel
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Place");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Place"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //No of days
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("No Of Days");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["NoOfDays"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Travel Mode
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Travel Mode");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["TravelMode"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Is Travel Arrangements made
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Travel Arrangements Made");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["TravelArrangementID"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Route
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Route");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Route"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Vehicle required
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Vehicle Required");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["IsVehicleRequired"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Passport Required
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Passport Required");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["IsPassportRequired"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Advance Required
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Advance Required");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["IsAdvanceRequired"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Advance Amount
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Advance Amount");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["AdvanceAmount"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Remarks
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Remarks");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Remarks"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.TicketRequest:
                    #region Ticket Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Flight Number");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["FlightNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");




                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    #endregion
                    break;

                case eReferenceTypes.ResignationRequest:
                    #region Resignation Request
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;
                case eReferenceTypes.SalaryCertificate:
                    #region Salary Certificate Request
                //    messageContent.Append("<tr>");
                //    messageContent.Append("<td style='width: 150px' class='label '>");
                //    messageContent.Append("EmployeeID");
                //    messageContent.Append("</td>");
                //    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                //    messageContent.Append("<td class='label'>");
                //    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                //    messageContent.Append("</td>");
                //    messageContent.Append("</tr>");


                //    messageContent.Append("<tr>");
                //    messageContent.Append("<td style='width: 150px' class='label '>");
                //    messageContent.Append("Designation");
                //    messageContent.Append("</td>");
                //    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                //    messageContent.Append("<td class='label'>");
                //    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                //    messageContent.Append("</td>");
                //    messageContent.Append("</tr>");

                //    messageContent.Append("<tr>");
                //    messageContent.Append("<td style='width: 150px' class='label '>");
                //    messageContent.Append("WorkingAt");
                //    messageContent.Append("</td>");
                //    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                //    messageContent.Append("<td class='label'>");
                //    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                //    messageContent.Append("</td>");
                //    messageContent.Append("</tr>");

                //    messageContent.Append("<tr>");
                //    messageContent.Append("<td style='width: 150px' class='label '>");
                //    messageContent.Append("Work Location");
                //    messageContent.Append("</td>");
                //    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                //    messageContent.Append("<td class='label'>");
                //    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                //    messageContent.Append("</td>");
                //    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;



                case eReferenceTypes.SalaryBankStatement:
                    #region Salary Bank Statement Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.SalaryCardLost:
                    #region Salary CardLost Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.Salaryslip:
                    #region Salary Slip Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.VisaLetter:
                    #region Visa Letter Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.NocUmarahVisa:
                    #region Noc Umarah Visa Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

                case eReferenceTypes.CardLost:
                    #region Card Lost Request

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequestedDate"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Reason");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Reason"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;
                case eReferenceTypes.AttendanceRequest:
                    #region Attendance Request
                    messageContent.Append(" <tr>");
                    messageContent.Append("<td> Date</td>");
                    messageContent.Append("<td>From Time</td>");
                    messageContent.Append("<td>To Time</td>");
                    messageContent.Append("<td >Duration</td>");
                    messageContent.Append(" </tr>");


                    foreach (DataRow dr in dtRequest.Rows)
                    {
                        messageContent.Append("<tr>");
                        //Date
                        messageContent.Append("<td style='width: 150px' class='label '>");
                        messageContent.Append(Convert.ToString(dr["Date"]));
                        messageContent.Append("</td>");

                        //Time From
                        messageContent.Append(" <td class='label'>");
                        messageContent.Append(Convert.ToString(dr["TimeFrom"]));
                        messageContent.Append("</td>");

                        //Time To
                        messageContent.Append(" <td class='label'>");
                        messageContent.Append(Convert.ToString(dr["TimeTo"]));
                        messageContent.Append("</td>");

                        //Duration
                        messageContent.Append(" <td class='label'>");
                        messageContent.Append(Convert.ToString(dr["Duration"]));
                        messageContent.Append("</td>");

                        messageContent.Append("</tr>");

                    }
                    #endregion
                    break;

                case eReferenceTypes.JobApproval:
                    #region JobApproval
                    //Job Code
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("EmployeeID");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["EmployeeNumber"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("WorkingAt");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["CompanyName"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Work Location");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append("<td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["WorkLocation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Job Code");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["JobCode"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Job Title
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Job title");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["JobTitle"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Department
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Department");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Department"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Designation
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Designation");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["Designation"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");


                    //Required Quota
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Required quota");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(Convert.ToString(dtRequest.Rows[0]["RequiredQuota"]));
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    #endregion
                    break;

            }
            string HRPowerURL = "";
            switch (Action)
            {
                case eAction.Applied:
                case eAction.RequestForCancel:
                    HRPowerURL = new clsEmployee().GetConfigurationValue("HRPowerURL");
                    break;

                case eAction.Forwarded:

                    ForwardedEmployees = ForwardedEmployees.Length > 0 ? ForwardedEmployees.Append("," + new clsUserMaster().GetEmployeeFullName()) : ForwardedEmployees.Append(new clsUserMaster().GetEmployeeFullName());

                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append("Forwarded By");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(ForwardedEmployees);
                    messageContent.Append(" </td>");
                    messageContent.Append(" </tr>");

                    HRPowerURL = new clsEmployee().GetConfigurationValue("HRPowerURL");

                    break;

                case eAction.Approved:
                case eAction.Reject:
                case eAction.Cancelled:

                    //Action By
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append(Action + " By ");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(new clsUserMaster().GetEmployeeFullName());
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    //Action Date
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px' class='label '>");
                    messageContent.Append(Action + " Date");
                    messageContent.Append("</td>");
                    messageContent.Append("<td style='width: 10px' class='label'> : </td>");
                    messageContent.Append(" <td class='label'>");
                    messageContent.Append(clsUserMaster.GetcurrentDate());
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");

                    break;

                default:
                    break;

            }
            
            if (HRPowerURL != "")
            {
                messageContent.Append("<tr>");
                messageContent.Append(" <td colspan='3' class='label'>");
                messageContent.Append("<a href=" + HRPowerURL + "/public/HomePendingRequest.aspx><input type='button' value='Approve/Reject'></a>");
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
            }

            return messageContent.Append("</table>").ToString();
        }
        return "";

    }
    private string EmployeeEmailBody(DataSet dsRequest, eReferenceTypes ReferenceType, eMessageTypes MessageType, string Action)
    {
        StringBuilder MessageAuthorities = new StringBuilder();
        StringBuilder messageContent = new StringBuilder();
        DataTable dtRequest = dsRequest.Tables[0];
        string ActionBy = new clsUserMaster().GetEmployeeFullName();
        string CurrentDate = clsUserMaster.GetcurrentDate();

        messageContent.Append(GetEmailTopSection(ReferenceType));

        string FirstRow = "<tr><td style='width:20%'> Applied By </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Name"]) + "</td></tr>" +
                                "<tr><td style='width:20%'> Applied Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + "</td></tr>";

        messageContent.Append("<table style='width: 100%; margin-top: 10px; border: solid 1.2px grey' cellpadding='10px'   cellspacing='10px'>");
        messageContent.Append("<tr>");
        messageContent.Append("<td style='width: 150px' class='label '>");
        messageContent.Append("Dear " + dtRequest.Rows[0]["Salutation"].ToString() + " " + dtRequest.Rows[0]["Name"].ToString() + ",");
        messageContent.Append("</td>");
        messageContent.Append("</tr>");



        switch (ReferenceType)
        {
            case eReferenceTypes.LeaveRequest:

                bool IsHalfDay = dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? true : false;
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                if (IsHalfDay == true)
                    messageContent.Append("Your leave request(Leave type :" + dtRequest.Rows[0]["Type"].ToString() + ", Leave date :" + dtRequest.Rows[0]["From"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                else
                    messageContent.Append("Your leave request(Leave type :" + dtRequest.Rows[0]["Type"].ToString() + ", Period :(" + dtRequest.Rows[0]["From"].ToString() + "-" + dtRequest.Rows[0]["To"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;


            case eReferenceTypes.CompensatoryOffRequest:
                DataTable dtCompensatoryLeaveDays = dsRequest.Tables[1];
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                if (dtRequest.Rows[0]["IsCredit"].ToInt32() == 1)
                    messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your compensatory leave request (Type :Credit)has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                else
                    messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your compensatory leave for the period(" + dtRequest.Rows[0]["LeaveFromDate"].ToString() + "-" + dtRequest.Rows[0]["LeaveToDate"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.VacationLeaveRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your vacation  request(period :" + dtRequest.Rows[0]["LeaveFromDate"].ToString() + "-" + dtRequest.Rows[0]["LeaveToDate"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;


            case eReferenceTypes.LeaveExtensionRequest:

                if (MessageType.ToString().StartsWith("Leave_Extension")) //leave extension request
                {
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                    messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your leave extension request (Leave type :" + dtRequest.Rows[0]["leaveType"].ToString() + ",No of days :" + dtRequest.Rows[0]["NoOfDays"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");
                }
                else if (MessageType.ToString().StartsWith("Time_Extension"))//Time extension request
                {
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                    messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your time extension request (Extension date :" + dtRequest.Rows[0]["ExtensionFromDate"].ToString() + ",Break duration :" + dtRequest.Rows[0]["NoOfDays"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");
                }
                else//Vacation extension request
                {
                    messageContent.Append("<tr>");
                    messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                    messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your vacation extension request (Mode of extension :" + (dtRequest.Rows[0]["IsExtension"].ToInt32() == 1 ? "Extended vacation" : "Shortened vacation").ToString() + ",No of days :" + dtRequest.Rows[0]["NoOfDays"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                    messageContent.Append("</td>");
                    messageContent.Append("</tr>");
                }

                break;

            case eReferenceTypes.AttendanceRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your attendance request for the period (" + Convert.ToString(dtRequest.Rows[0]["FromDate"]) + "-" + dtRequest.Rows[0]["ToDate"].ToString() + ")" + "has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.SalaryAdvanceRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your salary advance request (Requested amount :" + Convert.ToString(dtRequest.Rows[0]["amount"]) + ",Approved Amount:" + Convert.ToString(dtRequest.Rows[0]["ApprovedAmount"]) + ")" + "has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;


            case eReferenceTypes.ExpenseRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your expense request(Expense head :" + dtRequest.Rows[0]["ExpenseHead"].ToString() + ", Expense Amount :" + dtRequest.Rows[0]["Amount"].ToString() + ")for the period (" + Convert.ToString(dtRequest.Rows[0]["From"]) + "-" + dtRequest.Rows[0]["To"].ToString() + ")" + "has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.LoanRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your loan request (Loan Type :" + dtRequest.Rows[0]["Type"].ToString() + ",Loan Amount :" + dtRequest.Rows[0]["Amount"].ToString() + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.TravelRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your travel request to the place-" + Convert.ToString(dtRequest.Rows[0]["place"]) + "(" + dtRequest.Rows[0]["TravelDate"].ToString() + ")" + "has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.TransferRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your transfer request(Transfer Type :" + dtRequest.Rows[0]["Type"].ToString() + ",Transfer from :" + dtRequest.Rows[0]["From"].ToString() + ",Transfer to :" + dtRequest.Rows[0]["To"].ToString() + ") has been" + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.DocumentRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your document request for the document-" + Convert.ToString(dtRequest.Rows[0]["DocumentNumber"]) + "has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;


            case eReferenceTypes.JobApproval:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The vacancy (" + dtRequest.Rows[0]["JobCode"].ToString() + "-" + dtRequest.Rows[0]["JobTitle"].ToString() + ")posted by you has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.TicketRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your ticket request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ",Flight number : " + Convert.ToString(dtRequest.Rows[0]["FlightNumber"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.ResignationRequest:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your resignation request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.SalaryCertificate:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Salary Certificate request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.SalaryBankStatement:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Salary Bank Statement request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.SalaryCardLost:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Salary CardLost request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.Salaryslip:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Salary Slip request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.VisaLetter:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Visa Letter request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.NocUmarahVisa:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Noc -Umarah Visa request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;

            case eReferenceTypes.CardLost:
                messageContent.Append("<tr>");
                messageContent.Append("<td style='width: 150px;padding:0px;line-height:24px' class='label '>");
                messageContent.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Your Card Lost request(Requested date : " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + ")has been " + Action + "by " + ActionBy + "on " + CurrentDate);
                messageContent.Append("</td>");
                messageContent.Append("</tr>");
                break;
        }
        messageContent.Append("</table>");
        messageContent.Append(GetEmailBottomSection());

        return messageContent.ToString();
    }

    private DataSet GetMailRequestDetails(int ReferenceID, eReferenceTypes ReferenceType)
    {
        if (ReferenceType == eReferenceTypes.VacationLeaveRequest || ReferenceType == eReferenceTypes.CompensatoryOffRequest)
            ReferenceType = eReferenceTypes.LeaveRequest;
        else if (ReferenceType == eReferenceTypes.TimeExtensionRequest || ReferenceType == eReferenceTypes.VacationExtensionRequest)
            ReferenceType = eReferenceTypes.LeaveExtensionRequest;



        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 2));
        Parameters.Add(new SqlParameter("@RequestID", ReferenceID));
        Parameters.Add(new SqlParameter("@RequestTypeID", (int)ReferenceType));
        return new DataLayer().ExecuteDataSet("HRspCommonMail", Parameters);
    }



    private string GetHtml(DataSet dsRequest, eReferenceTypes ReferenceType, eMessageTypes MessageType, eAction Action)
    {

        StringBuilder s = new StringBuilder();
        s.Append("<html>");
        s.Append("<head>");

        s.Append("<style type='text/css'>");
        s.Append(".body");
        s.Append("{");
        s.Append("background: url(" + ConfigurationManager.AppSettings["_VIRTUAL_PATH"].ToString() + "images/ecailles.png);");
        s.Append("}");
        s.Append(".label");
        s.Append("{");
        s.Append("font-weight: lighter;");
        s.Append("font-size: small;");
        s.Append("font-family: Verdana;");
        s.Append("}");
        s.Append("</style>");
        s.Append("</head>");
        s.Append("<body>");
        s.Append("<div style='width: 98%; border: solid 1px grey; height: 600px; margin-left: auto;margin-right: auto' class='body'>");
        s.Append("<div style='clear: both; height: auto; margin-left: auto; width: 98%; margin-right: auto; margin-top: 10px'>");
        s.Append("<img id='img' src='" + ConfigurationManager.AppSettings["_VIRTUAL_PATH"].ToString() + "images/logoeng.png' align='middle' style='border-width: 0px;' />");
        s.Append("</div>");
        s.Append("<div style='clear: both; margin-top: 20px; width: 98%; height: 470px; margin-left: auto;margin-right: auto; background-color: White'>");
        s.Append("<div style='clear: both; width: 100%; background-color: Black; height: 60px'>");
        s.Append("<div style='clear: both'>");
        s.Append("<span id='Label2' class='label' style='color: White;'>Requested By :" + clsUserMaster.GetEmployeeFullName(RequestedByID) + " </span>");
        s.Append("</div>");
        s.Append("<div style='margin-left: 200px'>");
        s.Append("<span id='Label3' class='label' style='color: White;'>" + ReferenceType.ToString() + "</span> ");
        s.Append("<span id='Label10' class='label' style='color: White;'>|" + MessageDate.ToString() + "</span>");
        s.Append("</div>");
        s.Append("</div>");
        s.Append("<div>");


        //Append the request details here
        s.Append(AuthoritiesEmailBody(dsRequest, ReferenceType, MessageType, Action));


        s.Append("</div>");
        s.Append("</div>");
        s.Append("<div style='clear: both; margin-bottom: 0px; height: 27px; text-align: center; background-color: ThreeDFace'>");
        s.Append("<span id='lblFooter' class='label'>EPeople © 2009-21 Privacy policy All rights reserved.");
        s.Append("Powered by ES3 Technovations LLP.</span>");
        s.Append("</div>");
        s.Append("</div>");
        s.Append("</body>");
        s.Append("</html>");

        return s.ToString();

    }



    private string GetEmailTopSection(eReferenceTypes ReferenceType)
    {

        StringBuilder s = new StringBuilder();
        s.Append("<html>");
        s.Append("<head>");

        s.Append("<style type='text/css'>");
        s.Append(".body");
        s.Append("{");
        s.Append("background: url(" + ConfigurationManager.AppSettings["_VIRTUAL_PATH"].ToString() + "images/ecailles.png);");
        s.Append("}");
        s.Append(".label");
        s.Append("{");
        s.Append("font-weight: lighter;");
        s.Append("font-size: small;");
        s.Append("font-family: Verdana;");
        s.Append("}");
        s.Append("</style>");
        s.Append("</head>");
        s.Append("<body>");
        s.Append("<div style='width: 98%; border: solid 1px grey; height: 600px; margin-left: auto;margin-right: auto' class='body'>");
        s.Append("<div style='clear: both; height: auto; margin-left: auto; width: 98%; margin-right: auto; margin-top: 10px'>");
        s.Append("<img id='img' src='" + ConfigurationManager.AppSettings["_VIRTUAL_PATH"].ToString() + "images/logoeng.png' align='middle' style='border-width: 0px;' />");
        s.Append("</div>");
        s.Append("<div style='clear: both; margin-top: 20px; width: 98%; height: 470px; margin-left: auto;margin-right: auto; background-color: White'>");
        s.Append("<div style='clear: both; width: 98%; background-color: Black; height: 60px'>");
        s.Append("<div style='clear: both'>");
        s.Append("<span id='Label2' class='label' style='color: White;'>Requested By :" + clsUserMaster.GetEmployeeFullName(RequestedByID) + " </span>");
        s.Append("</div>");
        s.Append("<div style='margin-left: 200px'>");
        s.Append("<span id='Label3' class='label' style='color: White;'>" + ReferenceType.ToString() + "</span> ");
        s.Append("<span id='Label10' class='label' style='color: White;'>|" + MessageDate.ToString() + "</span>");
        s.Append("</div>");
        s.Append("</div>");
        s.Append("<div>");


        //Append the request details here
        // s.Append(AuthoritiesEmailBody(dsRequest, ReferenceType, MessageType, Action));

        return s.ToString();


    }


    private string GetEmailBottomSection()
    {

        StringBuilder s = new StringBuilder();
        s.Append("</div>");
        s.Append("</div>");
        s.Append("<div style='clear: both; margin-bottom: 0px; height: 27px; text-align: center; background-color: ThreeDFace'>");
        s.Append("<span id='lblFooter' class='label'>EPeople © 2009-21 Privacy policy All rights reserved.");
        s.Append("Powered by ES3 Technovations LLP.</span>");
        s.Append("</div>");
        s.Append("</div>");
        s.Append("</body>");
        s.Append("</html>");
        return s.ToString();

    }



    #endregion












    //private void SendMail(string MessageAuthorities, string MessageEmployee, eAction Action)
    //{
    //    try
    //    {
    //        Get Mail IDs 
    //        GetMailIDs(Action);

    //        Page page = (Page)HttpContext.Current.Handler;

    //        if (AuthorityEmailID == string.Empty) // Check whether EmailID exists for Authorities
    //        {
    //            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail for the Recipient(s) as official Email has not been set')", true);
    //            return;
    //        }
    //        else if (FromEmailID == string.Empty)
    //        {
    //            ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail as your official Email has not been set')", true);
    //            return;
    //        }

    //         Mail For Authorities
    //        Thread t1 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, AuthorityEmailID, ReportingToEmailID, Subject, MessageAuthorities, "", false, clsMailSettings.AccountType.Email));
    //        t1.Start();

    //        // Mail For Employee
    //        if (MessageEmployee != string.Empty)//Approved,Rejected,Forwarded and Cancelled
    //        {
    //            Thread t2 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, EmployeeEmailID, Subject, MessageEmployee, clsMailSettings.AccountType.Email, false));
    //            t2.Start();
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        clsCommon.WriteErrorLog(ex);
    //    }

    //}


    private enum eType
    {
        Employee = 1,
        Authority = 2
    }


}







public class AlreadySendAuthorities
{
    public string EmployeeFullName { get; set; }
    public int EmployeeID { get; set; }
    public eAction StatusID { get; set; }
}

public enum eAction
{
    Applied = 1,
    Approved = 5,
    Reject = 3,
    Pending = 4,
    RequestForCancel = 7,
    Cancelled = 6,
    Forwarded = 10
}

public enum eMessageTypes
{
    Hiring_Plan = 1,
    Leave_Request = 2,
    Loan_Request = 3,

    Salary__Advance_Request = 4,

    Transfer_Request = 5,
    Travel_Request = 6,
    Vacancy_Request = 7,
    Vacancy_Approval = 8,
    Alter_Salary = 9,
    //JobPromotion = 10,
    Leave_Approval = 11,
    //Training_Schedule_Request = 12,

    Leave_Rejection = 13,
    Leave__Cancel_Request = 14,
    Leave_Forward = 15,
    Leave_Cancellation = 16,


    //HiringPlan_Approval = 17,
    Loan_Approval = 18,
    Loan_Rejection = 19,
    Loan_Cancel_Request = 20,
    Loan_Forward = 21,
    Loan_Cancellation = 22,


    Salary__Advance_Approval = 23,
    Salary__Advance_Rejection = 24,
    Salary__Advance__Cancel_Request = 25,
    Salary__Advance_Forward = 26,
    Salary__Advance_Cancellation = 27,


    Transfer_Approval = 28,
    Transfer_Rejection = 29,
    Transfer_Cancel_Request = 30,
    Transfer_Forward = 31,
    Transfer_Cancellation = 32,

    Travel_Approval = 33,
    Travel_Rejection = 34,
    Travel_Cancel_Request = 35,
    Travel_Forward = 36,
    Travel_Cancellation = 37,


    HiringPlan_cancellation = 38,
    TrainingSchedule_Cancel = 39,
    //Announcements = 40,
    //Hire_Candidate = 41,
    //PendingOffer_Status = 42,
    //Candiate_Employee_Integragtion = 43,
    //Resignation_Request = 44,
    //Resignation_Approve = 45,
    //Resignation_Reject = 46,
    Confirmation = 47,


    Document_Request = 48,
    Document_Forward = 49,
    Document_Approval = 50,
    Document_Rejection = 51,
    Document__Cancel_Request = 52,
    Document_Cancellation = 53,


    Leave__Extension_Request = 54,
    Leave__Extension_Forward = 55,
    Leave__Extension_Approval = 56,
    Leave__Extension_Rejection = 57,
    Leave__Extension__Cancel_Request = 58,
    Leave__Extension_Cancellation = 59,



    //Visa_Request = 60,
    //Visa_Forward = 61,
    //Visa_Approval = 62,
    //Visa_Rejection = 63,
    //Visa_Cancel_Request = 64,
    //Visa_Cancellation = 65,
    PerformanceInitiation = 66,
    //Benefits = 67,
    //TimeSheet_Request = 68,
    Training_Request = 69,
    Training_Approval = 70,
    Training_Rejection = 71,


    Expense_Approval = 72,
    Expense_Rejection = 73,
    Expense_Cancel_Request = 74,
    Expense_Forward = 75,
    Expense_Cancellation = 76,
    Expense_Request = 77,



    //PettyCash = 78,
    //PettyCashDocument = 79,

    Vacation_Request = 80,
    Vacation_Approval = 81,
    Vacation_Rejection = 82,
    Vacation_Cancel_Request = 83,
    Vacation_Forward = 84,
    Vacation_Cancellation = 85,

    Time__Extension_Request = 86,
    Time__Extension_Approval = 87,
    Time__Extension_Rejection = 88,
    Time__Extension__Cancel_Request = 89,
    Time__Extension_Forward = 90,
    Time__Extension_Cancellation = 91,

    Performance__Evaluation = 92,

    Performance__Action = 93,
    OfferRequest = 94,
    OfferApproval = 95,

    Attendance_Request = 96,
    Attendance_Approval = 97,
    Attendance_Rejection = 98,
    Attendance_Cancel_Request = 99,
    Attendance_Forward = 100,
    Attendance_Cancellation = 101,

    CompensatoryOff_Extension_Request = 102,
    CompensatoryOff_Extension_Approval = 103,
    CompensatoryOff_Extension_Rejection = 104,
    CompensatoryOff_Extension_Cancel_Request = 105,
    CompensatoryOff_Extension_Forward = 106,
    CompensatoryOff_Extension_Cancellation = 107,

    Resignation_Request = 108,
    Resignation_Approval = 109,
    Resignation_Rejection = 1110,
    Resignation_Cancel_Request = 111,
    Resignation_Forward = 112,
    Resignation_Cancellation = 113,

    Ticket_Request = 114,
    Ticket_Approval = 115,
    Ticket_Rejection = 116,
    Ticket_Cancel_Request = 117,
    Ticket_Forward = 118,
    Ticket_Cancellation = 119,

    Compensatory__Of_Request = 120,
    Vacation__Extension_Request = 121,
    Vacation__Leave_Request = 122,

    Asset_Request = 123,
    Asset_Approval = 124,
    Asset_Rejection = 125,
    Asset_Cancel_Request = 126,
    Asset_Forward = 127,
    Asset_Cancellation = 128,

    General_Request = 129,
    General_Approval = 130,
    General_Rejection = 131,
    General_Cancel_Request = 132,
    General_Forward = 133,
    General_Cancellation = 134,

    Rejoin_Request = 135,
    Rejoin_Approval = 136,
    Rejoin_Rejection = 137,
    Rejoin_Cancel_Request = 138,
    Rejoin_Forward = 139,
    Rejoin_Cancellation = 140,

    SalaryCertificate_Request=141,
    SalaryCertificate_Approval=142,
    SalaryCertificate_Rejection=143,
    SalaryCertificate_Cancel_Request=144,
    SalaryCertificate_Forward=145,
    SalaryCertificate_Cancellation=146,



    SalaryBankStatement_Request = 147,
    SalaryBankStatement_Approval = 148,
    SalaryBankStatement_Rejection = 149,
    SalaryBankStatement_Cancel_Request = 150,
    SalaryBankStatement_Forward = 151,
    SalaryBankStatement_Cancellation = 152,

    SalaryCardLost_Request = 153,
    SalaryCardLost_Approval = 154,
    SalaryCardLost_Rejection = 155,
    SalaryCardLost_Cancel_Request = 156,
    SalaryCardLost_Forward = 157,
    SalaryCardLost_Cancellation = 158,


    Salaryslip_Request = 159,
    Salaryslip_Approval = 160,
    Salaryslip_Rejection = 161,
    Salaryslip_Cancel_Request = 162,
    Salaryslip_Forward = 163,
    Salaryslip_Cancellation = 164,




    VisaLetter_Request = 165,
    VisaLetter_Approval = 166,
    VisaLetter_Rejection = 167,
    VisaLetter_Cancel_Request = 168,
    VisaLetter_Forward = 169,
    VisaLetter_Cancellation = 170,

    NocUmarahVisa_Request = 171,
    NocUmarahVisa_Approval = 172,
    NocUmarahVisa_Rejection = 173,
    NocUmarahVisa_Cancel_Request = 174,
    NocUmarahVisa_Forward = 175,
    NocUmarahVisa_Cancellation = 176,



    CardLost_Request = 177,
    CardLost_Approval = 178,
    CardLost_Rejection = 179,
    CardLost_Cancel_Request = 180,
    CardLost_Forward = 181,
    CardLost_Cancellation = 182

    

}

public enum eReferenceTypes
{
    LeaveRequest = 1,
    LoanRequest = 2,
    SalaryAdvanceRequest = 3,
    TransferRequest = 4,
    ExpenseRequest = 5,
    DocumentRequest = 7,
    LeaveExtensionRequest = 8,
    ResignationRequest = 9,
    TrainingRequest = 10,
    PerformanceAction = 11,
    JobApproval = 12,
    OfferLetterApproval = 13,
    AttendanceRequest = 14,
    PerformanceEvaluation = 15,
    TicketRequest = 16,
    TravelRequest = 17,
    CompensatoryOffRequest = 18,
    VacationLeaveRequest = 19,
    TimeExtensionRequest = 20,
    VacationExtensionRequest = 21,
    AssetRequest = 22,
    GeneralRequest = 23,
    SalaryCertificate = 24,
    SalaryBankStatement=25,
    SalaryCardLost = 26,
    Salaryslip = 27,
    VisaLetter=28,
    NocUmarahVisa=29,
    CardLost=30

}

public enum eApprovalModule
{
    Ess = 1,
    Job = 2
}
public enum essReferenceTypes
{
    Leave = 1,
    Extension = 2,
    SalaryAdvance = 3,
    Expense = 4,
    Loan = 5,
    Travel = 6,
    Transfer = 7,
    Document = 8,
    General = 9,
    Attendence = 11,
    Vacation=10,
    Training = 13
}











