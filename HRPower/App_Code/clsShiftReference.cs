﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for clsShiftReference
/// purpose : Handle ShiftReference
/// created : Binoj
/// Date    : 09.08.2010
/// </summary>

public class clsShiftReference : DL
{
    int iShiftID;
    int iCompanyID;
    string sDescription;
    string sFromTime;
    string sToTime;
    string sDuration;
    int iShiftType;
    int iNoOfTimings;
    string sMinWorkingHours;
    int iRoleID;
    int iBufferTime;
    bool bIsMinWorkOT;
    int iAllowedBreakTime;
    int iLateAfter;
    int iEarlyBefore;
    int iShiftOTPolicyID;
    int iBuffer;
    int iMinimumOT;
    int iWeeklyOT;
    public int ShiftID { get { return iShiftID; } set { iShiftID = value; } }
    public int CompanyID { get { return iCompanyID; } set { iCompanyID = value; } }
    public string Description { get { return sDescription; } set { sDescription = value; } }
    public string FromTime { get { return sFromTime; } set { sFromTime = value; } }
    public string ToTime { get { return sToTime; } set { sToTime = value; } }
    public string Duration { get { return sDuration; } set { sDuration = value; } }
    public int ShiftType { get { return iShiftType; } set { iShiftType = value; } }
    public int NoOfTimings { get { return iNoOfTimings; } set { iNoOfTimings = value; } }
    public string MinWorkingHours { get { return sMinWorkingHours; } set { sMinWorkingHours = value; } }
    public int RoleID { get { return iRoleID; } set { iRoleID = value; } }
    public int BufferTime { get { return iBufferTime; } set { iBufferTime = value; } }
    public Boolean IsMinWorkOT { get { return bIsMinWorkOT; } set { bIsMinWorkOT = value; } }
    public int AllowedBreakTime { get { return iAllowedBreakTime; } set { iAllowedBreakTime = value; } }
    public int LateAfter { get { return iLateAfter; } set { iLateAfter = value; } }
    public int EarlyBefore { get { return iEarlyBefore; } set { iEarlyBefore = value; } }
    public int ShiftOTPolicyID { get { return iShiftOTPolicyID; } set { iShiftOTPolicyID = value; } }
    public int Buffer { get { return iBuffer; } set { iBuffer = value; } }
    public int MinimumOT { get { return iMinimumOT; } set { iMinimumOT = value; } }
    public int WeeklyOT { get { return iWeeklyOT; } set { iWeeklyOT = value; } }
    public clsShiftReference() { }

    public int Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@FromTime", sFromTime));
        alParameters.Add(new SqlParameter("@ToTime", sToTime));
        alParameters.Add(new SqlParameter("@ShiftType", iShiftType));
        alParameters.Add(new SqlParameter("@Duration", sDuration));

        if (iShiftOTPolicyID == -1)
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", iShiftOTPolicyID));

        if (iNoOfTimings == -1)
            alParameters.Add(new SqlParameter("@NoOfTimings", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@NoOfTimings", iNoOfTimings));

        alParameters.Add(new SqlParameter("@MinWorkingHours", sMinWorkingHours));
        alParameters.Add(new SqlParameter("@WeeklyWorkingHours", iWeeklyOT));

        alParameters.Add(new SqlParameter("@IsMinWorkOT", bIsMinWorkOT));

        if (iShiftType == 1 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@AllowedBreakTime", iAllowedBreakTime));
        else
            alParameters.Add(new SqlParameter("@AllowedBreakTime", DBNull.Value));

        if (iShiftType == 1 || iShiftType == 3 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@LateAfter", iLateAfter));
        else
            alParameters.Add(new SqlParameter("@LateAfter", DBNull.Value));

        if (iShiftType == 1 || iShiftType == 3 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@EarlyBefore", iEarlyBefore));
        else
            alParameters.Add(new SqlParameter("@EarlyBefore", DBNull.Value));

        alParameters.Add(new SqlParameter("@Buffer", iBuffer));
        alParameters.Add(new SqlParameter("@MinimumOT", iMinimumOT));

        return Convert.ToInt32(ExecuteScalar(alParameters));
        // return Convert.ToInt32(ExecuteScalar("HRspShiftReference", alParameters));
    }

    public int Update()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@FromTime", sFromTime));
        alParameters.Add(new SqlParameter("@ToTime", sToTime));
        alParameters.Add(new SqlParameter("@ShiftType", iShiftType));
        alParameters.Add(new SqlParameter("@Duration", sDuration));


        if (iShiftOTPolicyID == -1)
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", iShiftOTPolicyID));

        if (iNoOfTimings == -1)
            alParameters.Add(new SqlParameter("@NoOfTimings", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@NoOfTimings", iNoOfTimings));

        alParameters.Add(new SqlParameter("@MinWorkingHours", sMinWorkingHours));

        alParameters.Add(new SqlParameter("@IsMinWorkOT", bIsMinWorkOT));

        alParameters.Add(new SqlParameter("@WeeklyWorkingHours", iWeeklyOT));

        if (iShiftType == 1 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@AllowedBreakTime", iAllowedBreakTime));
        else
            alParameters.Add(new SqlParameter("@AllowedBreakTime", DBNull.Value));

        if (iShiftType == 1 || iShiftType == 3 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@LateAfter", iLateAfter));
        else
            alParameters.Add(new SqlParameter("@LateAfter", DBNull.Value));

        if (iShiftType == 1 || iShiftType == 3 || iShiftType == 4)
            alParameters.Add(new SqlParameter("@EarlyBefore", iEarlyBefore));
        else
            alParameters.Add(new SqlParameter("@EarlyBefore", DBNull.Value));

        alParameters.Add(new SqlParameter("@Buffer", iBuffer));
        alParameters.Add(new SqlParameter("@MinimumOT", iMinimumOT));

        return Convert.ToInt32(ExecuteScalar(alParameters));
        // return Convert.ToInt32(ExecuteScalar("HRspShiftReference", alParameters));
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        ExecuteNonQuery("HRspShiftReference", alParameters);
    }

    public DataSet GetAllShifts()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GA"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspShiftReference", alParameters);
    }

    public DataTable GetShiftType()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ST"));
        return ExecuteDataTable("HRspShiftReference", alParameters);
    }

    public DataSet GetShift()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "G"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return ExecuteDataSet("HRspShiftReference", alParameters);
    }

    public SqlDataReader GetShiftDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "G"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return ExecuteReader("HRspShiftReference", alParameters);
    }

    public DataSet GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));
        alParameters.Add(new SqlParameter("@RoleId", iRoleID));
        return ExecuteDataSet("HRspShiftReference", alParameters);
    }

    public bool IsExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IE"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return (Convert.ToInt32(ExecuteScalar("HRspShiftReference", alParameters)) > 0 ? true : false);
    }

    public string Print(string sCompanyName)
    {
        DataSet ds = GetAllShifts();

        StringBuilder sb = new StringBuilder();

        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                sb.Append("<table cellpadding='0' cellspacing='0' width='100%' style='font-family: Verdana'>");
                sb.Append("<tr>");
                sb.Append("<td align='center'><h4 style='text-decoration: underline'>Shift Information</h4></td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style='padding: 3px; background-color: #F0F0F0; font-size: 12px; font-weight: bold;'>Company : " + sCompanyName + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style='padding-top: 5px'>");
                sb.Append("<table border='1' cellpadding='3' cellspacing='0' width='100%' rules='all' style='border-collapse: collapse; font-size: 11px;'>");
                sb.Append("<tr style='font-weight: bold; background-color: #7A363D; color: #F0F0F0;'>");
                sb.Append("<td>Shift Name</td>");
                sb.Append("<td width='80'>From Time</td>");
                sb.Append("<td width='60'>To Time</td>");
                sb.Append("<td width='60'>Flexible</td>");
                sb.Append("<td width='60'>Duration</td>");
                sb.Append("</tr>");

                foreach (DataRow dw in ds.Tables[0].Rows)
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + Convert.ToString(dw["Description"]) + "</td>");
                    sb.Append("<td>" + Convert.ToDateTime(dw["FromTime"]).ToString("hh:mm tt") + "</td>");
                    sb.Append("<td>" + Convert.ToDateTime(dw["ToTime"]).ToString("hh:mm tt") + "</td>");
                    sb.Append("<td>" + Convert.ToBoolean(dw["Flexi"]) + "</td>");
                    sb.Append("<td>" + Convert.ToDateTime(dw["Duration"]).ToString("hh:mm") + "</td>");
                    sb.Append("</tr>");
                }

                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
            }
        }

        return sb.ToString();
    }

    public int GetCompany()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GC"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return Convert.ToInt32(ExecuteScalar("HRspShiftReference", alParameters));
    }

    public void DeleteDynamicShifts()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DST"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        //ExecuteNonQuery("HRspShiftReference", alParameters);
        ExecuteNonQuery(alParameters);
    }

    public void InsertDynamicShiftDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IDS"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));
        alParameters.Add(new SqlParameter("@FromTime", sFromTime));
        alParameters.Add(new SqlParameter("@ToTime", sToTime));
        alParameters.Add(new SqlParameter("@Duration", sDuration));

        if (iBufferTime == -1)
            alParameters.Add(new SqlParameter("@BufferTime", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@BufferTime", iBufferTime));

        alParameters.Add(new SqlParameter("@AllowedBreakTime", iAllowedBreakTime));
        alParameters.Add(new SqlParameter("@MinWorkingHours", sMinWorkingHours));

        if (iShiftOTPolicyID == -1)
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", DBNull.Value));
        else
            alParameters.Add(new SqlParameter("@ShiftOTPolicyID", iShiftOTPolicyID));

        ExecuteNonQuery(alParameters);

        // ExecuteNonQuery("HRspShiftReference", alParameters);
    }

    public DataTable GetDynamicShiftDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDS"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return ExecuteDataTable("HRspShiftReference", alParameters);
    }

    public void DeleteSplitShifts()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DSS"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        ExecuteNonQuery(alParameters);
        // ExecuteNonQuery("HRspShiftReference", alParameters);
    }

    public void InsertSplitShiftDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ISS"));

        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));
        alParameters.Add(new SqlParameter("@FromTime", sFromTime));
        alParameters.Add(new SqlParameter("@ToTime", sToTime));
        alParameters.Add(new SqlParameter("@Duration", sDuration));

        ExecuteNonQuery(alParameters);

        // ExecuteNonQuery("HRspShiftReference", alParameters);
    }

    public DataTable GetSplitShiftDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSD"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        return ExecuteDataTable("HRspShiftReference", alParameters);
    }

    public DataTable BindShiftOTPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "BSO"));

        return ExecuteDataTable("HRspShiftReference", alParameters);
    }

    public void Begin()
    {
        BeginTransaction("HRspShiftReference");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }


}
