﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Created By:    Sanju
/// Created Date:  08-Oct-2013
/// Description:   View Policies
/// </summary>
public class clsViewPolicies : DL 
{
    public Int32 EmployeeID { get; set; }
    public Int32 PolicyID { get; set; }
    public string Type { get; set; }
    public Int32 CompanyID { get; set; }
	public clsViewPolicies()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataTable GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspViewPolicies", alParameters);
    }
    public DataTable GetEmployeePolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspViewPolicies", alParameters);
    }
    public DataTable GetAllPolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@Type", Type));
        return ExecuteDataTable("HRspViewPolicies", alParameters);
    }
    public DataSet GetPolicyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@Type", Type));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@PolicyID", PolicyID));
        return ExecuteDataSet("HRspViewPolicies", alParameters);
    }
}
