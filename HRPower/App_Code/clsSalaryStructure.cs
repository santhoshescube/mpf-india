﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsSalaryStructure
/// </summary>
public class clsSalaryStructure : DL
{
    public clsSalaryStructure()
    { }


    private int iDesignationID;
    private int iPaymentModeId;
    private decimal dBasicPay;
    private string sRemarks;

    private int iAllowanceTypeId;
    private decimal dAmount;
    private bool bAddition;
    private int iEmployeeID;// From employee salary structure
    private int iCompanyID;
    private int iPaymentClassificationID;
    private int iPayCalculationTypeID;
    private int iSalaryID;
    private int iAddDedID;
    private int iCategoryID;
    private int iDeductionPolicyID;
    private decimal dNetAmount;
    private int iSalaryHistoryID;
    private int iSalaryDetailID;
    private int iCurrencyID;
    private int iSalaryDay;
    private int iIncentiveTypeID;
    private int iSetPolicyID;
    private int iScaleID;

    private int iOtherRemunerationID;
    private decimal dRemunerationAmount;

    #region Properties


    public int SalaryDetailID
    {
        get { return iSalaryDetailID; }
        set { iSalaryDetailID = value; }
    }
    public int DesignationID
    {
        get { return iDesignationID; }
        set { iDesignationID = value; }
    }
    public int PaymentModeId
    {
        get { return iPaymentModeId; }
        set { iPaymentModeId = value; }
    }
    public decimal BasicPay
    {
        get { return dBasicPay; }
        set { dBasicPay = value; }
    }
    public int AllowanceTypeId
    {
        get { return iAllowanceTypeId; }
        set { iAllowanceTypeId = value; }
    }
    public decimal Amount
    {
        get { return dAmount; }
        set { dAmount = value; }
    }
    public int EmployeeID
    {
        get { return iEmployeeID; }
        set { iEmployeeID = value; }
    }

    public int CompanyID
    {
        get { return iCompanyID; }
        set { iCompanyID = value; }
    }

    public int PaymentClassificationID
    {
        get { return iPaymentClassificationID; }
        set { iPaymentClassificationID = value; }
    }

    public int PayCalculationTypeID
    {
        get { return iPayCalculationTypeID; }
        set { iPayCalculationTypeID = value; }
    }
    public int SalaryID
    {
        get { return iSalaryID; }
        set { iSalaryID = value; }
    }

    public string Remarks
    {
        get { return sRemarks; }
        set { sRemarks = value; }
    }

    public int DeductionPolicyID
    {
        get { return iDeductionPolicyID; }
        set { iDeductionPolicyID = value; }
    }
    public int AddDedID
    {
        get { return iAddDedID; }
        set { iAddDedID = value; }
    }
    public int CategoryID
    {
        get { return iCategoryID; }
        set { iCategoryID = value; }
    }
    public decimal NetAmount
    {
        get { return dNetAmount; }
        set { dNetAmount = value; }
    }
    public int SalaryHistoryID
    {
        get { return iSalaryHistoryID; }
        set { iSalaryHistoryID = value; }
    }

    public int CurrencyID
    {
        get { return iCurrencyID; }
        set { iCurrencyID = value; }
    }

    public char Type { get; set; }

    public int SalaryDay
    {
        get { return iSalaryDay; }
        set { iSalaryDay = value; }
    }

    public int IncentiveTypeID
    {
        get { return iIncentiveTypeID; }
        set { iIncentiveTypeID = value; }
    }

    public int SetPolicyID
    {
        get { return iSetPolicyID; }
        set { iSetPolicyID = value; }
    }
    public int ScaleID
    {
        get { return iScaleID; }
        set { iScaleID = value; }
    }

    public int OtherRemunerationID
    {
        get { return iOtherRemunerationID; }
        set { iOtherRemunerationID = value; }
    }
    public decimal RemunerationAmount
    {
        get { return dRemunerationAmount; }
        set { dRemunerationAmount = value; }
    }

    public Boolean IsCurrencyForBP { get; set; }
    public int OTPolicyID { get; set; }
    public int AbsentPolicyID { get; set; }
    public int HolidayPolicyID { get; set; }
    public int EncashPolicyID { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int UserId { get; set; }
    public string SearchKey { get; set; }

    #endregion
    public DataSet FillCurrencies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCC"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }



    public DataSet FillAbsentPolicies()
    {
        ArrayList alparameters = new ArrayList();
       // alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        alparameters.Add(new SqlParameter("@Mode", "APM"));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }


    public DataTable FillComboDesignation()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GDR"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }

    public DataSet FillEncashPolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "EPM"));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }
    public DataSet FillHolidayPolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "HPM"));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }
    public DataSet FillOvertimePolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "OPM"));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }

    public DataSet FillPaymentMode()   //Fill Dropdown Payment Mode
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FP"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }

    public DataSet FillPaymentCalculation()   //Fill Dropdown Payment Calculation
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FPC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }

    public DataSet FillPaymentCalculationOther()   //Fill Dropdown Payment Calculation 
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FPCM"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }

    public DataSet FillParticulars()   //Fill Dropdown Particulars
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FA"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }

    public DataSet FillCategory()   //Fill Dropdown Category
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }
    public DataSet FillDeductionPolicy()   //Fill Dropdown deduction Policy
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FDP"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }
    public DataTable GetDeductionPolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GDP"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }
    public DataSet GetSalaryStructureDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@DesignationId", DesignationID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }


    public DataSet GetAllowanceDeductionDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAD"));
        alparameters.Add(new SqlParameter("@DesignationId", DesignationID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspSalaryStructure", alparameters);
    }

    public string GetCompanyName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return Convert.ToString(ExecuteScalar("HRspSalaryStructure", alparameters));
    }

    public int InsertDesignationSalaryHead(bool isHistory) //Insert Salary Structure
    {
        ArrayList alparameters = new ArrayList();
        if (isHistory)
        {
            alparameters.Add(new SqlParameter("@Mode", "ISH"));       //for history table
            alparameters.Add(new SqlParameter("@NetAmount", dNetAmount));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "IH"));
        }
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alparameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));
        alparameters.Add(new SqlParameter("@BasicPay", dBasicPay));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));
        alparameters.Add(new SqlParameter("@CurrencyId", CurrencyID));
        alparameters.Add(new SqlParameter("@IsCurrencyForBP", IsCurrencyForBP));
        if (OTPolicyID == -1)
            alparameters.Add(new SqlParameter("@OTPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@OTPolicyID", OTPolicyID));
        if (AbsentPolicyID == -1)
            alparameters.Add(new SqlParameter("@AbsentPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));
        if (HolidayPolicyID == -1)
            alparameters.Add(new SqlParameter("@HolidayPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));
        if (EncashPolicyID == -1)
            alparameters.Add(new SqlParameter("@EncashPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int InsertDesignationSalaryHeadDetail(bool isHistoryDetail)
    {
        ArrayList alparameters = new ArrayList();

        if (isHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "ISHD"));
            alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "IHD"));
            alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        }

        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        if (iDeductionPolicyID == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        alparameters.Add(new SqlParameter("@Amount", dAmount));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateDesignationSalaryHead(bool isHistory)  //update salary structure
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));

        if (isHistory)
        {
            alparameters.Add(new SqlParameter("@Mode", "USH"));       //for history table
            alparameters.Add(new SqlParameter("@NetAmount", dNetAmount));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "UH"));
        }

        alparameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alparameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));
        alparameters.Add(new SqlParameter("@BasicPay", dBasicPay));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));
        alparameters.Add(new SqlParameter("@CurrencyId", CurrencyID));
        alparameters.Add(new SqlParameter("@IsCurrencyForBP", IsCurrencyForBP));
        if (OTPolicyID == -1)
            alparameters.Add(new SqlParameter("@OTPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@OTPolicyID", OTPolicyID));
        if (AbsentPolicyID == -1)
            alparameters.Add(new SqlParameter("@AbsentPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));
        if (HolidayPolicyID == -1)
            alparameters.Add(new SqlParameter("@HolidayPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));
        if (EncashPolicyID == -1)
            alparameters.Add(new SqlParameter("@EncashPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateDesignationSalaryHeadDetail(bool isHistoryDetail)
    {
        ArrayList alparameters = new ArrayList();

        if (isHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "USHD"));
            alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "UHD"));
            alparameters.Add(new SqlParameter("@SalaryDetailID", iSalaryDetailID));
        }

        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        if (iDeductionPolicyID == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        alparameters.Add(new SqlParameter("@Amount", dAmount));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }


    public string GetDeductionPolicy()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CPE"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        return Convert.ToString(ExecuteScalar("HRspSalaryStructure", alparameters));
    }

    public DataTable GetCompanyDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CAR"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataTable("HRspSalaryStructure", alparameters);
    }
    public bool GetAdditionorDeduction()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAR"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        return Convert.ToBoolean(ExecuteScalar("HRspSalaryStructure", alparameters));
    }
    public bool CheckCurrentDayHistoryExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GH"));

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));

        return Convert.ToBoolean(ExecuteScalar(alparameters));
    }

    public void DeleteHistoryHeadDetail()  //   during current date updation of salary head history detail
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        ExecuteNonQuery(alparameters);
    }

    public void RemoveAllowanceOrDeduction(bool IsHistoryDetail)    //Remove allowance or deduction from datalist
    {
        ArrayList alparameters = new ArrayList();

        if (IsHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "RADH"));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "RAD"));
        }

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        ExecuteNonQuery("HRspSalaryStructure", alparameters);
    }

    public void BeginEmp()
    {
        BeginTransaction("HRspEmployeeSalaryStructure");
    }
    public void Begin()
    {
        BeginTransaction("HRspSalaryStructure");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    public DataTable GetSalaryStructureHistoryReport()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SSHR"));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataTable("HRspSalaryStructure", alparameters);
    }

    public DataTable GetAllCompanies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataTable("HRspSalaryStructure", alparameters);
    }

    public DataTable GetEmployeesByCompany()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GEBC"));

        if (iCompanyID > 0)
            alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataTable("HRspSalaryStructure", alparameters);
    }

    //-----------------------------------------------------------Employee salary structure ------------------------------------------------------------
    public DataSet GetEmpSalaryStructure()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSS"));
        alparameters.Add(new SqlParameter("@SalaryID", SalaryID));
        alparameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }
    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID ));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alParameters));
    }

    public DataSet GetAllEmployeeSalaryStructures()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@UserId", UserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters);
    }

    public DataSet GetEmployeeSalaryStructure(string Type)
    {
        //Type = 'A' -- Alter (From Performance Action' ,'E'---> From Employee
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@SalaryID", SalaryID));
        alParameters.Add(new SqlParameter("@Type", Type));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters);
    }

    public DataSet GetEmployeeSalaryStructureDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }

    public DataSet GetEmployeeAllowanceDeductionDetails(string Type)
    {
        //Type ='A' -- Alter Employee ,'E' -- From employee Page
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GAD"));
        alparameters.Add(new SqlParameter("@Type", Type));
        alparameters.Add(new SqlParameter("@SalaryID", SalaryID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }

    public DataSet GetEmpAllowanceDeductionDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSD"));
        alparameters.Add(new SqlParameter("@SalaryID", SalaryID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }

    public int InsertEmployeeSalaryHead(bool isHistory) //Insert Employee Salary Structure details
    {
        ArrayList alparameters = new ArrayList();
        if (isHistory)
        {
            alparameters.Add(new SqlParameter("@Mode", "ISH"));       //for history table
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "IH"));
        }
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alparameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));
        alparameters.Add(new SqlParameter("@NetAmount", dNetAmount));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));
        if (iCurrencyID == -1) alparameters.Add(new SqlParameter("@CurrencyID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@CurrencyID", iCurrencyID));
        alparameters.Add(new SqlParameter("@IsCurrencyForBP", 0));

        if (OTPolicyID > 0)
            alparameters.Add(new SqlParameter("@OTPolicyID", OTPolicyID));

        if (AbsentPolicyID > 0)
            alparameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        if (HolidayPolicyID > 0)
            alparameters.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));

        if (EncashPolicyID > 0)
            alparameters.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));

        if (iIncentiveTypeID == 0) // before editing Value is -1
            alparameters.Add(new SqlParameter("@IncentiveTypeID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@IncentiveTypeID", iIncentiveTypeID));

        if (iSetPolicyID == 0) // before editing Value is -1
            alparameters.Add(new SqlParameter("@SetPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@SetPolicyID", iSetPolicyID));
        alparameters.Add(new SqlParameter("@SalaryDay", iSalaryDay));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));

    }


    public int InsertAlteredEmployeeSalaryHead() //Insert Employee Salary Structure details
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IASH"));

        alparameters.Add(new SqlParameter("@SalaryId", SalaryID));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alparameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));
        //alparameters.Add(new SqlParameter("@BasicPay", dBasicPay));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));
        if (iCurrencyID == -1) alparameters.Add(new SqlParameter("@CurrencyID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@CurrencyID", iCurrencyID));
        alparameters.Add(new SqlParameter("@IsCurrencyForBP", IsCurrencyForBP));

        if (OTPolicyID > 0)
            alparameters.Add(new SqlParameter("@OTPolicyID", OTPolicyID));

        if (AbsentPolicyID > 0)
            alparameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        if (HolidayPolicyID > 0)
            alparameters.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));

        if (EncashPolicyID > 0)
            alparameters.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));        
        
        alparameters.Add(new SqlParameter("@SalaryDay", SalaryDay));
        alparameters.Add(new SqlParameter("@NetAmount", dNetAmount));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));

    }



    public int InsertAlteredEmployeeSalaryHeadDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IASHD"));
        alparameters.Add(new SqlParameter("@SalaryID", SalaryID));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));
        alparameters.Add(new SqlParameter("@CategoryID", iCategoryID));

        if (iDeductionPolicyID == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        alparameters.Add(new SqlParameter("@Amount", dAmount));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }



    public int InsertEmployeeSalaryHeadDetail(bool isHistoryDetail)
    {
        ArrayList alparameters = new ArrayList();

        if (isHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "ISHD"));
            alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "IHD"));
            alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        }

        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));
        alparameters.Add(new SqlParameter("@CategoryID", iCategoryID));

        if (iDeductionPolicyID == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        alparameters.Add(new SqlParameter("@Amount", dAmount));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }


    public int UpdateEmployeeSalaryHead(bool isHistory)  //update salary structure
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        if (isHistory)
        {
            alparameters.Add(new SqlParameter("@Mode", "USH"));       //for history table
            alparameters.Add(new SqlParameter("@NetAmount", dNetAmount));
            alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
                
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "UH"));
        }

        alparameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alparameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));
        alparameters.Add(new SqlParameter("@BasicPay", dBasicPay));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));
        if (iCurrencyID == -1) alparameters.Add(new SqlParameter("@CurrencyID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@CurrencyID", iCurrencyID));
        alparameters.Add(new SqlParameter("@IsCurrencyForBP", IsCurrencyForBP));

        if (OTPolicyID > 0)
            alparameters.Add(new SqlParameter("@OTPolicyID", OTPolicyID));

        if (AbsentPolicyID > 0)
            alparameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        if (HolidayPolicyID > 0)
            alparameters.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));

        if (EncashPolicyID > 0)
            alparameters.Add(new SqlParameter("@EncashPolicyID", EncashPolicyID));        

        if (iIncentiveTypeID == 0)
            alparameters.Add(new SqlParameter("@IncentiveTypeID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@IncentiveTypeID", iIncentiveTypeID));
        if (iSetPolicyID == 0)
            alparameters.Add(new SqlParameter("@SetPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@SetPolicyID", iSetPolicyID));
        alparameters.Add(new SqlParameter("@SalaryDay", iSalaryDay));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateEmployeeSalaryHeadDetail(bool isHistoryDetail)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@SalaryDetailID", iSalaryDetailID));

        if (isHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "USHD"));
            alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "UHD"));
            alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        }

        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        if (iDeductionPolicyID == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        alparameters.Add(new SqlParameter("@Amount", dAmount));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }
    public int  GetSalaryStructureHistoryID()
    {
        try
        {
            DataTable dt;
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "XC"));
            alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
            alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
            dt=ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);

            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToInt32();
            }
            else
            {
                return 0;
            }
            return 0;
        }
        catch
        {
            return 0;
        }
    }
    public bool CheckEmployeeHistoryExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GH"));

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return Convert.ToBoolean(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
        
    }
    public DataTable FillEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FEM"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }
    public DataTable FillFilterEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GES"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }

    public string GetEmployeeName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
    }
    public int GetCompanyId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCI"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
    }
    public void RemoveEmpSalaryAllowanceOrDeduction(bool IsHistoryDetail)    //Remove allowance or deduction from datalist
    {
        ArrayList alparameters = new ArrayList();

        if (IsHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "RADH"));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "RAD"));
        }

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (Type != null && Convert.ToString(Type) == "1")
            alparameters.Add(new SqlParameter("@Type", Type));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        ExecuteNonQuery("HRspEmployeeSalaryStructure", alparameters);
    }
    public int DeleteSalaryHeadDetailByID()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DU"));
        alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));

        return Convert.ToInt32(ExecuteScalar(alparameters));

    }
    public int DeleteSalaryHistoryDetailByID()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DHU"));
        alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));

        return Convert.ToInt32(ExecuteScalar(alparameters));

    }

    public bool IsSalaryIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@SalaryID", iSalaryID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alParameters)) > 0 ? true : false);
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DES"));
        alParameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        ExecuteNonQuery("HRspEmployeeSalaryStructure", alParameters);
    }

    public DataSet FillEmpCurrencies()
    {
        //Type = 'A' Alter ,'E' -- from Employee
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GCC"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        //alparameters.Add(new SqlParameter("@Type", Type));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }
    public void DeleteEmpHistoryHeadDetail()  //   during current date updation of salary head history detail
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DEH"));
        alparameters.Add(new SqlParameter("@SalaryHistoryID", iSalaryHistoryID));
        ExecuteNonQuery(alparameters);
    }

    public bool CheckDeleteValidation()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CVE"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));

        return Convert.ToBoolean(ExecuteScalar("HRspSalaryStructure", alparameters));

    }

    public void DeleteSalaryStructure()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "DSS"));
            alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
            alparameters.Add(new SqlParameter("@DesignationId", iDesignationID));

            ExecuteNonQuery("HRspSalaryStructure", alparameters);
        }
        catch (Exception ex)
        {
        }

    }

    public DataSet FillSettlementPolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSP"));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);

    }

    public DataSet FillCompanyIncentivePolicies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GIP"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);

    }

    //public int EmployeeSalaryStructureExists()
    //{
    //    ArrayList alparameters = new ArrayList();

    //    alparameters.Add(new SqlParameter("@Mode", "ISE"));
    //    alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
    //    return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));

    //}
    public DataSet GetEmployeeSalaryScaleMaster()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SM"));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);

    }

    public DataTable GetEmployeeSalaryScaleDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SD"));
        alparameters.Add(new SqlParameter("@ScaleID", iScaleID));

        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }

    public int GetAdditionID()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAID"));
        alparameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyID));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
    }

    public DataSet FillRemunerationParticulars()   //Fill Dropdown Particulars
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ORP"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspEmployeeSalaryStructure", alparameters);
    }

    public void RemoveEmpRemuneration(bool IsHistoryDetail)    //Remove allowance or deduction from datalist
    {
        ArrayList alparameters = new ArrayList();

        if (IsHistoryDetail)
        {
            alparameters.Add(new SqlParameter("@Mode", "RARDH"));
        }
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "RARD"));
        }

        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (Type != null && Convert.ToString(Type) == "1")
            alparameters.Add(new SqlParameter("@Type", Type));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@OtherRemunerationID", iOtherRemunerationID));

        ExecuteNonQuery("HRspEmployeeSalaryStructure", alparameters);
    }

    public int InsertEmployeeRemuneration(bool isHistoryDetail)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 24));
        alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        alparameters.Add(new SqlParameter("@OtherRemunerationID", iOtherRemunerationID));
        alparameters.Add(new SqlParameter("@AmountRemuneration", dRemunerationAmount));

        return Convert.ToInt32(ExecuteScalar("spPaySalaryStructure", alparameters));
    }

    public int DeleteEmployeeRemunerationByID()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 26));
        alparameters.Add(new SqlParameter("@SalaryStructureID", iSalaryID));
        return Convert.ToInt32(ExecuteScalar("spPaySalaryStructure", alparameters));

    }

    public DataTable GetEmployeeRemunerationDetails(int intSalaryID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 27));
        alparameters.Add(new SqlParameter("@SalaryID", intSalaryID));
        return ExecuteDataTable("spPaySalaryStructure", alparameters);
    }

    public DataTable GetEmployeeRemuneration(int intSalaryID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 25));
        alparameters.Add(new SqlParameter("@SalaryID", intSalaryID));
        return ExecuteDataTable("spPaySalaryStructure", alparameters);
    }
    public int GetSalaryDay()
    {
        int SalaryDay = 1;
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCSD"));
        alparameters.Add(new SqlParameter("@CompanyID", (iCompanyID == 0 ? 1 : iCompanyID)));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));

    }

    public int InsertAlteredEmployeeRemuneration()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "IAR"));
        alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
        alparameters.Add(new SqlParameter("@OtherRemunerationID", iOtherRemunerationID));
        alparameters.Add(new SqlParameter("@AmountRemuneration", dRemunerationAmount));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
    }

    public DataTable GetAlteredRemuneration(int SalaryID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GOR"));
        alparameters.Add(new SqlParameter("@SalaryID", SalaryID));

        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }

    //public int InsertAlteredEmployeeRemuneration()
    //{
    //    ArrayList alparameters = new ArrayList();
    //    alparameters.Add(new SqlParameter("@Mode", "IASHD"));
    //    alparameters.Add(new SqlParameter("@SalaryID", iSalaryID));
    //    alparameters.Add(new SqlParameter("@OtherRemunerationID", iOtherRemunerationID));
    //    alparameters.Add(new SqlParameter("@AmountRemuneration", dRemunerationAmount));

    //    return Convert.ToInt32(ExecuteScalar("HRspEmployeeSalaryStructure", alparameters));
    //}

    public DataTable GetPolicyPermissions(int IntRoleID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "ROL"));
        alparameters.Add(new SqlParameter("@RoleID", IntRoleID));
        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }

    public DataTable GetSalaryStructureID(int EmployeeID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GID"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteDataTable("HRspEmployeeSalaryStructure", alparameters);
    }


}
