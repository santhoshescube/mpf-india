﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmployeeNew
/// </summary>
public class clsEmployeeNew : DL
{
    public clsEmployeeNew()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    #region Properties

    public int CompanyID { get; set; }

    public int EmployeeID { get; set; }

    public string EmployeeNumber { get; set; }

    public Int16 SalutationID { get; set; }

    public string FirstName { get; set; }

    public string FirstNameArb { get; set; }

    public string LastName { get; set; }

    public string LastNameArb { get; set; }

    public string MiddleName { get; set; }

    public string MiddleNameArb { get; set; }

    public int WorkingAt { get; set; }

    public int CountryID { get; set; }

    public int NationalityID { get; set; }

    public int ReligionID { get; set; }

    public int EmploymentTypeID { get; set; }

    public DateTime DateOfJoining { get; set; }

    public DateTime ProbabtionEndDate { get; set; }

    public int WorkStatusID { get; set; }

    public int ReportingTo { get; set; }
    public int HOD { get; set; }

    public Int16 DepartmentID { get; set; }

    public Int16 DesignationID { get; set; }

    public Int16 WorkPolicyID { get; set; }

    public Int16 LeavePolicyId { get; set; }

    public string OfficialEmailID { get; set; }

    public string OfficialEmailPassword { get; set; }

    public bool IsSetAsUser { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public Int16 RoleID { get; set; }

    public bool IsMale { get; set; }

    public DateTime DateOfBirth { get; set; }

    public string FatherName { get; set; }

    public string MotherName { get; set; }

    public string SpouseName { get; set; }

    public string Languages { get; set; }

    public int MotherTongueID { get; set; }

    public string Identification { get; set; }

    public string BloodGroup { get; set; }

    public byte[] RecentPhoto { get; set; }

    public byte[] Thumbnail { get; set; }

    public Int32 TransactionType { get; set; }

    public Int32  EmployerBank { get; set; }

    public int EmployerACC { get; set; }

    public int EmployeeBank { get; set; }

    public string  EmployeeACC { get; set; }

  
    public string AddressLine1 { get; set; }

    public string AddressLine2 { get; set; }

    public string City { get; set; }

    public string StateProvinceRegion { get; set; }

    public string ZipCode { get; set; }



    public string LocalAddress;

    public string EmergencyAddress;

    public string Phone { get; set; }

    public string LocalPhone;

    public string EmergencyPhone;

    public string Mobile { get; set; }

    public string Email { get; set; }

    public string OtherInfo { get; set; }

    public int WorkLocationID { get; set; }

    public Type eType { get; set; }

    public int BankBranchID { get; set; }

    public string Fromdate { get; set; }


    public int CandidateID { get; set; }

    public DateTime NextAppraisalDate { get; set; }

    public string DeviceUser { get; set; }
    public int ProcessTypeID { get; set; }
    #endregion


    #region Methods

    public int GetEmployeeCount()
    {
        return ExecuteScalar("Select count(*) from EmployeeMaster where WorkStatusID>5").ToInt32();
    }

    #region SaveEmployee

    public int SaveEmployee()
    {
        object objoutEmployeeId = 0;

        ArrayList parameters = new ArrayList();


        if(EmployeeID == 0 )
            parameters.Add(new SqlParameter("@Mode", "I"));
        else
            parameters.Add(new SqlParameter("@Mode", "U"));

        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@EmployeeNumber", EmployeeNumber));
        parameters.Add(new SqlParameter("@SalutationID", SalutationID));
        parameters.Add(new SqlParameter("@FirstName", FirstName));
        parameters.Add(new SqlParameter("@MiddleName", MiddleName));
        parameters.Add(new SqlParameter("@LastName", LastName));

        parameters.Add(new SqlParameter("@FirstNameArb", FirstNameArb));
        parameters.Add(new SqlParameter("@MiddleNameArb", MiddleNameArb));
        parameters.Add(new SqlParameter("@LastNameArb", LastNameArb));


        parameters.Add(new SqlParameter("@EmployeeFullName", (FirstName + " " + MiddleName + " " + LastName)));
        parameters.Add(new SqlParameter("@EmployeeFullNameArb", (FirstNameArb + " " + MiddleNameArb + " " + LastNameArb)));
        parameters.Add(new SqlParameter("@Gender", IsMale));

        if(NationalityID != -1)
            parameters.Add(new SqlParameter("@NationalityID", NationalityID));
        else
            parameters.Add(new SqlParameter("@NationalityID", 0));

        if (CountryID != -1)
            parameters.Add(new SqlParameter("@CountryID", CountryID));
        else
            parameters.Add(new SqlParameter("@CountryID", 0));
        
        if(ReligionID != -1)
            parameters.Add(new SqlParameter("@ReligionID", ReligionID));
        else
            parameters.Add(new SqlParameter("@ReligionID", 0));

        parameters.Add(new SqlParameter("@DateofBirth", DateOfBirth));
        parameters.Add(new SqlParameter("@DateofJoining", DateOfJoining));

        if (WorkStatusID == 7/*Probation*/)
            parameters.Add(new SqlParameter("@ProbationEndDate", ProbabtionEndDate));

        parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));

        if(EmploymentTypeID != - 1)
            parameters.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        else
            parameters.Add(new SqlParameter("@EmploymentTypeID", 0));

        if (ReportingTo > 0)
            parameters.Add(new SqlParameter("@ReportingTo", ReportingTo));

        if (HOD > 0)
            parameters.Add(new SqlParameter("@HOD", HOD));
        if (ProcessTypeID > 0)
            parameters.Add(new SqlParameter("@ProcessTypeID", ProcessTypeID));
        parameters.Add(new SqlParameter("@SpouseName", SpouseName));
        parameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        parameters.Add(new SqlParameter("@OfficialEmailID", OfficialEmailID));

        parameters.Add(new SqlParameter("@OfficialEmailPassword", OfficialEmailPassword));
        parameters.Add(new SqlParameter("@FathersName", FatherName));

        if(MotherTongueID != - 1)
            parameters.Add(new SqlParameter("@MotherTongueID", MotherTongueID));
        else
            parameters.Add(new SqlParameter("@MotherTongueID", 0));

        parameters.Add(new SqlParameter("@LanguagesKnown", Languages));
        parameters.Add(new SqlParameter("@IdentificationMarks", Identification));
       
        parameters.Add(new SqlParameter("@AddressLine1", AddressLine1));
        parameters.Add(new SqlParameter("@AddressLine2", AddressLine2));
        parameters.Add(new SqlParameter("@City", City));
        parameters.Add(new SqlParameter("@StateProvinceRegion", StateProvinceRegion));
        parameters.Add(new SqlParameter("@ZipCode", ZipCode));


        parameters.Add(new SqlParameter("@PermanentPhone", Phone));


        parameters.Add(new SqlParameter("@EmergencyAddress", EmergencyAddress ));
        parameters.Add(new SqlParameter("@EmergencyPhone", EmergencyPhone));


        parameters.Add(new SqlParameter("@LocalAddress",LocalAddress));
        parameters.Add(new SqlParameter("@LocalPhone", LocalPhone));



        parameters.Add(new SqlParameter("@LocalMobile", Mobile));
        parameters.Add(new SqlParameter("@LocalEmailID", Email));
        parameters.Add(new SqlParameter("@MothersName", MotherName));
        parameters.Add(new SqlParameter("@Remarks", OtherInfo));
        parameters.Add(new SqlParameter("@BloodGroup", BloodGroup));

        // Transaction type

        parameters.Add(new SqlParameter("@TransactionTypeID", TransactionType));
        if (EmployerBank > 0)
            parameters.Add(new SqlParameter("@BankID", EmployerBank));
        if (EmployeeBank > 0)
            parameters.Add(new SqlParameter("@BankBranchID", EmployeeBank));
        parameters.Add(new SqlParameter("@AccountNumber", EmployeeACC));
        parameters.Add(new SqlParameter("@CompanyBankAccountID", EmployerACC));

        SqlParameter objSqlParameter = new SqlParameter();
        objSqlParameter.DbType = DbType.Binary;
        objSqlParameter.Value = RecentPhoto;
        objSqlParameter.ParameterName = "@RecentPhoto";
        parameters.Add(objSqlParameter);

        objSqlParameter = new SqlParameter();
        objSqlParameter.DbType = DbType.Binary;
        objSqlParameter.Value = Thumbnail;
        objSqlParameter.ParameterName = "@Thumbnail";
        parameters.Add(objSqlParameter);

        parameters.Add(new SqlParameter("@WorkPolicyID", WorkPolicyID));
        parameters.Add(new SqlParameter("@LeavePolicyID", LeavePolicyId));

        if (WorkLocationID > 0)
            parameters.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

        parameters.Add(new SqlParameter("@RoleID", RoleID));
        parameters.Add(new SqlParameter("@UserName", UserName));
        parameters.Add(new SqlParameter("@Password", Password));
        parameters.Add(new SqlParameter("@CreatedBy", new clsUserMaster().GetUserId()));
        if(NextAppraisalDate.ToString() !="1/1/0001 12:00:00 AM")
            parameters.Add(new SqlParameter("@NextAppraisalDate", NextAppraisalDate));
        parameters.Add(new SqlParameter("@DeviceUserID", DeviceUser));

        object obj = ExecuteScalar("HRspEmployeeMaster", parameters);
        return Convert.ToInt32(obj);
    }

    #endregion

    public bool IsEmployeeCodeExistsForUpdation()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IECU"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@EmployeeNumber", EmployeeNumber));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public void SaveLeavepolicy()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@Leavepolicyid", LeavePolicyId));
        alParameters.Add(new SqlParameter("@Fromdate", Fromdate));
        alParameters.Add(new SqlParameter("@type", 0));

        ExecuteNonQuery("HRPayEmployeeLeaveSummary", alParameters);
    }




    public DataTable  GetAllPolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }


    public byte[] GetEmployeeRecentPhoto(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GERP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        object result = ExecuteScalar("HRspEmployeeMaster", alParameters);

        if(result == DBNull.Value )
            return null;
        else 
            return  (byte [])result;
    }




    public byte[] GetEmployeeThumbnail(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GERT"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        object result = ExecuteScalar("HRspEmployeeMaster", alParameters);

        if (result == DBNull.Value)
            return null;
        else
            return (byte[])result;
    }

    


    public DataTable  GetAllLeavePolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GALP"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }




    public static  DataTable GetAllWorkLocation(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAWLBC"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return new DataLayer().ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }


    public DataTable GetAllCompanyAccounts()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GACA"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BankBranchID", BankBranchID));

        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }

  

    public DataTable GetEmployerBankNames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEB"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ));

        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }


    public bool IsEmployeeCodeExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEC"));
        alParameters.Add(new SqlParameter("@EmployeeNumber", EmployeeNumber));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }

    public bool IsUsernameExists(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IUE"));
        alParameters.Add(new SqlParameter("@username", UserName ));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeMaster", alParameters)) > 0 ? true : false);
    }



    public static DataTable  GetEmployeeCode(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GENO"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@FormID", eFormID.Employee ));
        return new DataLayer(). ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }

    public static DataTable   GetEmplooyeesHavingDocuments()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEHD"));
        return new DataLayer().ExecuteDataTable("HRspEmpViewDocs", alParameters);
    }



    public static DateTime GetCurrentDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 24));
        return new DataLayer().ExecuteScalar("HRSpRptEmployeeNew", alParameters).ToDateTime();

    }
    public static bool IsAttendanceExists(int EMployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEAD"));
        return new DataLayer().ExecuteScalar("HRspEmployeeMaster", alParameters).ToInt32() > 0 ? true : false;

    }

    public DataTable GetDesignation()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }
    /// <summary>
    /// Save mutiple mothertounge
    /// </summary>
    /// Created By  -   Jisha
    /// Created on  -   30/06/2014
    public void SaveEmployeeLanguages()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@LanguageID", MotherTongueID));


        ExecuteNonQuery("HRspEmployeeLanguageDetails", alParameters);
    }
    /// <summary>
    /// get employee languages
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmployeeLanguages()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return ExecuteDataTable("HRspEmployeeLanguageDetails", alParameters);
    }
    #endregion
}
