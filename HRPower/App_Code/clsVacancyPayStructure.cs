﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsVacancyPayStructure
/// </summary>


public class clsVacancyPayStructure:DL
{
    #region Variables
    public static string Procedure = "HRspVacancyAddEdit";
    #endregion

    #region Properties
    public static string Designation { get; set; }
    public static int ID { get; set; }    
    public int JobID { get; set; }
    public string PayStructure { get; set; }
    public int DesignationID { get; set; }
    public int SalaryID { get; set; }
    public string Amount { get; set; }
    public int AdditionDeductionID { get; set; }
    public int PaymentClassificationID { get; set; }
    public int PayCalculationTypeID { get; set; }
    public decimal GrossPay { get; set; }
    public List<clsVacancySalary> VacancyPay { get; set; }  
    #endregion

	public clsVacancyPayStructure()
	{}

    #region Methods

    public DataSet GetAllPayStructures()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAPS"));
        arr.Add(new SqlParameter("@JobID", JobID));       
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, arr);
    }

    public DataTable GetAdditionDeduction()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GPAD"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, arr);
    }

    public DataTable GetPaymentClassificationMode()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GPCM"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, arr);
    }

    public DataTable GetPayStructureName(int DesignationID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GPSN"));
        arr.Add(new SqlParameter("@DesignationID", DesignationID));       
        return ExecuteDataTable(Procedure, arr);
    }
    public string GetVacancyPayStructureName(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GVPSN"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return Convert.ToString(ExecuteScalar(Procedure, arr));
    }

    public bool GetAdditionORDeduction(int AdditionDeductionID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAORD"));
        arr.Add(new SqlParameter("@AdditionDeductionID", AdditionDeductionID));
        return ExecuteScalar(Procedure, arr).ToBoolean(); 
    }

    public int DoesPayStructureExist()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DPSE"));
        if (JobID > 0)
        {
            parameters.Add(new SqlParameter("@JobID", JobID));
            parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        }
        else
            parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }    


    public void DeletePayStructure()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DPS"));
        parameters.Add(new SqlParameter("@JobID", JobID));
        ExecuteNonQuery(Procedure, parameters);
    }

    public void InsertSalaryDetail()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IS"));
        parameters.Add(new SqlParameter("@SalaryId", SalaryID));
        parameters.Add(new SqlParameter("@AdditionDeductionID", AdditionDeductionID));
        parameters.Add(new SqlParameter("@Amount", Amount));

        ExecuteNonQuery(Procedure, parameters);
    }
    public int InsertSalaryMaster()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "ISM"));
        parameters.Add(new SqlParameter("@JobId", JobID));
        parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@PaymentClassificationID", PaymentClassificationID));
        parameters.Add(new SqlParameter("@PayCalculationTypeID", PayCalculationTypeID));
        parameters.Add(new SqlParameter("@GrossPay", GrossPay));

        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }

    #endregion
    
}
[Serializable]
public class clsVacancySalary
{
    public string Amount { get; set; }
    public int AdditionDeductionID { get; set; }   
}
