﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Author : Ratheesh
/// Date   : 28 Dec 2010
/// </summary>

public class clsReport : DL
{

    #region Variables

    private int iCompanyID, iStatusID, iTypeID, iEmployeeID, iProjectID, iEmploymentTypeID, iWorkStatus, iDepartmentID, iDesignationID,iYear, 
                iMonth, iBranchID ,iCompanyTypeID, iCompanyIndustryID, iDegreeID, iExperienceFrom, iExperienceTo;
    private DateTime dtFrom = DateTime.MinValue, dtTo = DateTime.MinValue;
    private DateTime dtSearchDate, dtNullDate = Convert.ToDateTime("1/1/1900"), dtFinYearFrom, dtFinYearTo;

    private bool iIncludeCompany;
    private string sSearchType, sSkills;
    private bool blnIsEvaluationPeriodic;

    #endregion

    #region Properties

    public int CompanyID { get { return iCompanyID; }       set { iCompanyID = value; } }
    public int EmployeeID { get { return iEmployeeID; }     set { iEmployeeID = value; } }
    public DateTime FromDate { get { return dtFrom; } set { dtFrom = value; } }
    public DateTime ToDate { get { return dtTo; } set { dtTo = value; } }
    public int ProjectID { get { return iProjectID; } set { iProjectID = value; } }
    public int EmploymentTypeID { get { return iEmploymentTypeID; } set { iEmploymentTypeID = value; } }
    public int WorkStatus { get { return iWorkStatus; } set { iWorkStatus = value; } }

    public int BranchID { get { return iBranchID; } set { iBranchID = value; } }


    public bool IncludeCompany { get { return iIncludeCompany; } set { iIncludeCompany = value; } }


    public int DepartmentID { get { return iDepartmentID; } set { iDepartmentID = value; } }
    public int DesignationID { get { return iDesignationID; } set { iDesignationID = value; } }
    public int CompanyTypeID { get { return iCompanyTypeID; } set { iCompanyTypeID = value; } }
    public int CompanyIndustryID { get { return iCompanyIndustryID; } set { iCompanyIndustryID = value; } }
    public int DegreeID { get { return iDegreeID; } set { iDegreeID = value; } }
    public int ExperienceFrom { get { return iExperienceFrom; } set { iExperienceFrom = value; } }
    public int ExperienceTo { get { return iExperienceTo; } set { iExperienceTo = value; } }

    public int Year { get { return iYear; } set { iYear = value; } }
    public int Month { get { return iMonth; } set { iMonth = value; } }
    public DateTime SearchDate { get { return dtSearchDate; } set { dtSearchDate = value; } }
    public DateTime NullDate { get { return dtNullDate; } }
    public string SearchType { get { return sSearchType; } set { sSearchType = value; } }
    public string Skills { get { return sSkills; } set { sSkills = value; } }
    public DateTime FinYearFrom { get { return dtFinYearFrom; } set { dtFinYearFrom = value; } }
    public DateTime FinYearTo { get { return dtFinYearTo; } set { dtFinYearTo = value; } }
    public bool IsEvaluationPeriodic { get { return blnIsEvaluationPeriodic; } set { blnIsEvaluationPeriodic = value; } }

  
    /// <summary>
    /// Gets or sets StatusID for general use.(1. LeaveStatusID,)
    /// </summary>
    public int StatusID { get { return iStatusID; } set { iStatusID = value; } }

    /// <summary>
    /// Gets or sets TypeID for general use.(1. LeaveTypeID,)
    /// </summary>
    public int TypeID { get { return iTypeID; } set { iTypeID = value; } }


    #endregion

    public clsReport() { }

    #region GetReportHeader
    public DataTable GetReportHeader()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCD"));
        int EmployeeID = objUserMaster.GetEmployeeId();
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        DataTable dt =  ExecuteDataTable("HRspMailSettings", alParameters);

        DataTable dtHeaderData = new DataTable();
        dtHeaderData.Columns.Add(new DataColumn("CompanyName", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("POBox", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("AddressLine1", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("AddressLine2", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Phone", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Logo", typeof(byte[])));
        dtHeaderData.Columns.Add(new DataColumn("CompanyID", typeof(int)));
        dtHeaderData.Columns.Add(new DataColumn("Footer", typeof(string)));

        byte[] bytImage = new byte[] { };

        if (dt.Rows.Count > 0)
        {
            string sAddress1 = string.Empty;
            string sAddress2 = string.Empty;
            string sPhoneEmail = string.Empty;

            if (Convert.ToString(dt.Rows[0]["Road"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Road"]);
            if (Convert.ToString(dt.Rows[0]["Area"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Area"]);
            if (Convert.ToString(dt.Rows[0]["Block"]).Trim() != string.Empty) sAddress1 += ", " + Convert.ToString(dt.Rows[0]["Block"]);

            if (Convert.ToString(dt.Rows[0]["City"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["City"]);
            if (Convert.ToString(dt.Rows[0]["Province"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["Province"]);
            if (Convert.ToString(dt.Rows[0]["Country"]).Trim() != string.Empty) sAddress2 += ", " + Convert.ToString(dt.Rows[0]["Country"]);
            //if (Convert.ToString(dt.Rows[0]["POBox"]).Trim() != string.Empty) sAddress2 += ", P.O Box : " + Convert.ToString(dt.Rows[0]["POBox"]);

            if (sAddress1.Length > 0) sAddress1 = sAddress1.Remove(0, 2); // remove first comma

            if (sAddress2.Length > 0) sAddress2 = sAddress2.Remove(0, 2); // remove first comma

            if (Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim() != string.Empty) sPhoneEmail += ", Ph: " + Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim();
            if (Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim() != string.Empty) sPhoneEmail += ", Email: " + Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim();

            if (sPhoneEmail.Length > 0) sPhoneEmail = sPhoneEmail.Remove(0, 2); // remove first comma

            if (dt.Rows[0]["LogoFile"] != DBNull.Value) bytImage = (byte[])dt.Rows[0]["LogoFile"];

            DataRow dr = dtHeaderData.NewRow();

            dr["CompanyName"] = Convert.ToString(dt.Rows[0]["Name"]);
            dr["POBox"] = "P.O Box : " + Convert.ToString(dt.Rows[0]["POBox"]);
            dr["AddressLine1"] = sAddress1;
            dr["AddressLine2"] = sAddress2;
            dr["Phone"] = sPhoneEmail;
            dr["Logo"] = bytImage;
            dr["CompanyID"] = Convert.ToInt32(dt.Rows[0]["CompanyID"]);

            string sFooterData = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter);
            dr["Footer"] = sFooterData;

            dtHeaderData.Rows.Add(dr);

        }
        else
        {
            dtHeaderData.Rows.Add(dtHeaderData.NewRow());
        }

        dtHeaderData.TableName = "ReportHeaders";

        return dtHeaderData;
    }

    public DataTable GetAttendanceSummary(int iEmployeeID, int iCompanyID, int iDayID, string sDate) // From attendancesummary
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@DayID", iDayID));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@Date1", sDate));

        DataTable dt = ExecuteDataTable("rptAttendanceSummary", alParameters);

        DataTable dtAtDet = new DataTable();

        dtAtDet.Columns.Add(new DataColumn("ShiftName", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("FromTime", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("ToTime", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Late", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Early", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("WorkPolicy", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Duration", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("AllowedBreakTime", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("BreakTime", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("WorkTime", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("OT", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Consequence", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Employee", typeof(string)));
        dtAtDet.Columns.Add(new DataColumn("Date", typeof(string)));


        DataRow dr = dtAtDet.NewRow();

        dr["ShiftName"] = Convert.ToString(dt.Rows[0]["ShiftName"]);
        dr["FromTime"] = Convert.ToString(dt.Rows[0]["FromTime"]);
        dr["ToTime"] = Convert.ToString(dt.Rows[0]["ToTime"]);
        dr["Late"] = Convert.ToString(dt.Rows[0]["Late"]);
        dr["Early"] = Convert.ToString(dt.Rows[0]["Early"]);
        dr["WorkPolicy"] = Convert.ToString(dt.Rows[0]["WorkPolicy"]);
        dr["Duration"] = Convert.ToString(dt.Rows[0]["Duration"]);
        dr["AllowedBreakTime"] = Convert.ToString(dt.Rows[0]["AllowedBreakTime"]);
        dr["BreakTime"] = Convert.ToString(dt.Rows[0]["BreakTime"]);
        dr["WorkTime"] = Convert.ToString(dt.Rows[0]["WorkTime"]);
        dr["OT"] = Convert.ToString(dt.Rows[0]["OT"]);
        dr["Consequence"] = Convert.ToString(dt.Rows[0]["Consequence"]);
        dr["Employee"] = Convert.ToString(dt.Rows[0]["Employee"]);
        dr["Date"] = Convert.ToString(dt.Rows[0]["Date"]);

        dtAtDet.Rows.Add(dr);

        dtAtDet.TableName = "DtAttendanceDetail";

        return dtAtDet;


    }

    #endregion

    #region GetJobVacancies
    public DataSet GetJobVacancies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GVD"));

        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iDepartmentID > 0) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        if (iDesignationID > 0) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "Vacancy";

            return ds;
        }
    }
    #endregion

    #region GetEmployees
    /// <summary>
    /// Returns all employees for generating report
    /// </summary>
    /// <returns></returns>
    public DataSet GetEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
     

        if (iEmploymentTypeID > 0) alParameters.Add(new SqlParameter("@EmploymentTypeID", iEmploymentTypeID));
        if (iWorkStatus > 0) alParameters.Add(new SqlParameter("@WorkStatus", iWorkStatus));
        if (iDesignationID > 0) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));
        if (iDepartmentID > 0) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));


        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "dtEmployee";

            return ds;
        }
    }
    #endregion

    #region GetLeaveRequests

    /// <summary>
    /// Gets leave request details of employees
    /// </summary>
    /// <returns></returns>
    public DataSet GetLeaveRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLDR"));

        if (iStatusID > 0) alParameters.Add(new SqlParameter("@StatusId", iStatusID));
        if (iTypeID > 0) alParameters.Add(new SqlParameter("@LeaveTypeId", iTypeID));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alParameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alParameters.Add(new SqlParameter("@SearchDate", dtSearchDate));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "Leave";

            return ds;
        }
    }

    #endregion

    #region GetLeaveSummary

    /// <summary>
    /// Gets leave summary
    /// </summary>
    /// <returns></returns>
    public DataSet GetLeaveSummary()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "LSUMM"));

        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if (dtFinYearFrom != dtNullDate) alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        if (dtFinYearTo != dtNullDate) alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "LeaveSummary";

            return ds;
        }
    }

    #endregion

    #region GetLeaveSummarySearch

    /// <summary>
    /// Gets leave summary
    /// </summary>
    /// <returns></returns>
    public DataSet GetLeaveSummaryFromTo()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "LSUMFT"));

        if (iCompanyID > 0) 
            alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iEmployeeID > 0) 
            alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if(this.FinYearFrom != this.NullDate)
            alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        if(this.FinYearTo != this.NullDate)
            alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "LeaveSummaryFilter";

            return ds;
        }
    }

    #endregion

    #region GetLoanDetails
    public DataSet GetLoanDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "LOAN"));

        if (iStatusID > 0) alParameters.Add(new SqlParameter("@StatusId", iStatusID));
        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alParameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alParameters.Add(new SqlParameter("@SearchDate", dtSearchDate));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "Loan";

            return ds;
        }

    }
    #endregion

    #region GetProjects
    /// <summary>
    /// Returns Project details
    /// </summary>
    /// <returns></returns>
    public DataSet GetProjects()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "APD"));

        if (iProjectID > 0) alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        if (iCompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alParameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alParameters.Add(new SqlParameter("@SearchDate", dtSearchDate));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "Projects";

            return ds;
        }

    }
    #endregion

    #region GetSingleProject
    /// <summary>
    /// Returns Project details
    /// </summary>
    /// <returns></returns>
    public DataSet GetSingleProject()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SPD"));

        if (iProjectID > 0)
            alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "SingleProject";

            return ds;
        }
    }
    #endregion

    #region GetCandidates
    /// <summary>
    /// Returns candidate details for displaying in report
    /// </summary>
    /// <returns></returns>
    public DataSet GetCandidates()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GCR"));

        if (iStatusID > 0) alParameters.Add(new SqlParameter("@StatusID", iStatusID));
        if (iDegreeID > 0) alParameters.Add(new SqlParameter("@DegreeID", iDegreeID));
        alParameters.Add(new SqlParameter("@ExperienceFrom", iExperienceFrom));
        if (iExperienceTo != 99) alParameters.Add(new SqlParameter("@ExperienceTo", iExperienceTo));
        if (sSkills != string.Empty) alParameters.Add(new SqlParameter("@Skills", sSkills));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());
          
            ds.Tables[0].TableName = "Candidates";

            return ds;
        }

    }
    #endregion

    #region GetCompanyDetails
    /// <summary>
    /// Returns company details for displaying in report
    /// </summary>
    /// <returns></returns>
    public DataSet GetCompanyDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GACR"));

        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iCompanyTypeID > 0) alParameters.Add(new SqlParameter("@CompanyTypeID", iCompanyTypeID));
        if (iCompanyIndustryID > 0) alParameters.Add(new SqlParameter("@CompanyIndustryID", iCompanyIndustryID));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            ds.Tables[0].TableName = "DtCompany";

            return ds;
        }

    }
    #endregion

    #region GetDefaultCompanyStartDate
    /// <summary>
    /// Returns Default company's start date (finacial year start date). if not exist, returns current date.
    /// </summary>
    /// <returns></returns>
    public DateTime GetDefaultCompanyStartDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GDCSD"));

        DateTime dStartDate = new DateTime();

        object objStartDate = ExecuteScalar("HRspReports", alParameters);

        if (objStartDate != null && objStartDate != DBNull.Value)
            dStartDate = Convert.ToDateTime(objStartDate);
        else
            dStartDate = DateTime.Now;

        return dStartDate;

    }
    #endregion

    #region GetCompanyStartDate
    /// <summary>
    /// Returns company's start date (finacial year start date). if not exist, returns current date.
    /// </summary>
    /// <returns></returns>
    public DateTime GetCompanyStartDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GCSD"));
        // CompanyID = 1 : it is the default company's ID. This company cannot be deleted.(Business Rule)
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID > 0 ? iCompanyID : 1)); 

        DateTime dStartDate = new DateTime();

        object objStartDate = ExecuteScalar("HRspReports", alParameters);

        if (objStartDate != null && objStartDate != DBNull.Value)
            dStartDate = Convert.ToDateTime(objStartDate);
        else
            dStartDate = DateTime.Now;

        return dStartDate;

    }
    #endregion


    #region GetCompanyTypes
    /// <summary>
    /// For filling dropdown list.
    /// </summary>
    /// <returns></returns>
    public DataTable GetCompanyTypes()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GCT"));
        return ExecuteDataTable("HRspReports", arraylst);
    }
    #endregion

    #region GetCompanyIndustries
    /// <summary>
    /// For filling dropdown list.
    /// </summary>
    /// <returns></returns>
    public DataTable GetCompanyIndustries()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GCI"));
        return ExecuteDataTable("HRspReports", arraylst);
    }
    #endregion

    #region GetDegrees
    /// <summary>
    /// For filling dropdown list.
    /// </summary>
    /// <returns></returns>
    public DataTable GetDegrees()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GDR"));
        return ExecuteDataTable("HRspReports", arraylst);
    }
    #endregion

    #region GetPaymentSummaryDetails
    /// <summary>
    /// Returns Payment summary data
    /// </summary>
    /// <returns></returns>
    public DataSet GetPaymentSummaryDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EP"));

        // Financial Year
        //alParameters.Add(new SqlParameter("@Year", Year));
        //alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iDepartmentID > 0) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));

        alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "PaymentByCompany";
                ds.Tables[1].TableName = "PaymentByDepartment";
            }

            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            return ds;
        }

    }
    #endregion

    #region GetAbsentSummaryDetails
    /// <summary>
    /// Returns Absent summary data
    /// </summary>
    /// <returns></returns>
    public DataSet GetAbsentSummaryDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ABR"));

        // Financial Year
        alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "AbsentSummary";
                ds.Tables[1].TableName = "AbsentDetails";
            }

            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            return ds;
        }

    }
    #endregion

    #region GetPerformanceEvaluationDetails
    /// <summary>
    /// Returns Performance Evaluation Details
    /// </summary>
    /// <returns></returns>
    public DataSet GetPerformanceEvaluationDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EPER"));

        alParameters.Add(new SqlParameter("@IsEvaluationPeriodical", blnIsEvaluationPeriodic));
        alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "PerformancEvaluation";
            }

            // add report header details to dataset
            ds.Tables.Add(this.GetReportHeader());

            return ds;
        }

    }
    #endregion

    #region GetRecentActivities
    public DataSet GetRecentActivities()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "RACT"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if(dtFrom != dtNullDate)
            alParameters.Add(new SqlParameter("@FromDate", dtFrom));
        if(dtTo != dtNullDate)
            alParameters.Add(new SqlParameter("@ToDate", dtTo));

        using (DataSet ds = ExecuteDataSet("HRspReports", alParameters))
        {
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "RecentActivity";
                ds.Tables.Add(this.GetReportHeader());
            } 
            return ds;
        }
    }
    #endregion

    #region GetCompanies
    /// <summary>
    /// Returns companies for binding dropdowns
    /// </summary>
    /// <returns></returns>
    public DataTable GetCompanies(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAC"));

        if (CompanyID != 0)
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataTable("HRspReports", alParameters);
    }
    #endregion

    public DataTable GetLeaveExtensionTypes()
    {
        return this.ExecuteDataTable("HRspReports", new ArrayList { 
            new SqlParameter("@Mode", "LET")
        });
    }

}
