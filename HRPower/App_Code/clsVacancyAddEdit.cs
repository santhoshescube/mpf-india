﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsVacancyAddEdit
/// </summary>
public class clsVacancyAddEdit : DL
{
    #region Variables
    public static string Procedure = "HRspVacancyAddEdit";
    #endregion

    #region Properties
    public int JobID { get; set; }   
    public int JobDetailID { get; set; }
    public int JobLevelID { get; set; }
    public int EmployeeID { get; set; }
    public int EmploymentTypeID { get; set; }
    public int Level { get; set; }
    public int SalaryID { get; set; }
    public string PayStructure { get; set; }
    public int DegreeId { get; set; }
    public int CriteriaId { get; set; }
    public int AdditionDeductionId { get; set; }
    public int CompanyId { get; set; }
    public int DepartmentId { get; set; }
    public int DesignationID { get; set; }
    public int RequiredQuota { get; set; }
    public string JobCode { get; set; }
    public string JobTitle { get; set; }
    public string Remarks { get; set; }
    public Decimal WeightPercentage { get; set; }
    public Decimal PassPercentage { get; set; }
    public Decimal TotalMark { get; set; }
    public Decimal Amount { get; set; }
    public Decimal BudgetedAmount { get; set; }
    public string ExpiryDate { get; set; }
    public string Skills { get; set; }
    public string Experience { get; set; }
    public string PlaceOfWork { get; set; }
    public int WeeklyWorkingDays { get; set; }
    public decimal DailyWorkingHours { get; set; }
    public bool IsAccomodation { get; set; }
    public bool IsTransportation { get; set; }
    public string ContractPeriod { get; set; }
    public string AirTicket { get; set; }
    public int AnnualLeaveDays { get; set; }
    public string LegalTerms { get; set; }
    public int CreatedBy { get; set; }
    public int ApprovedBy { get; set; }
    public string ApprovedDate { get; set; }
    public int JobStatusID { get; set; }
    public bool IsBoardInterview { get; set; }
    public bool IsOfferApproval { get; set; }
    public int PaymentClassificationID { get; set; }
    public int PayCalculationTypeID { get; set; }
    public decimal GrossPay { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string SearchKey { get; set; }
    public bool IsVacancyApproval { get; set; }
    public bool IsOfferLetterApproval { get; set; }
    public bool IsForwarded { get; set; }
    public bool IsBudgeted { get; set; }
    public bool RecruitmentFeesPayable { get; set; }
    public int GenderRecruID { get; set; }
    public int RecruitmentTypeID { get; set; }
    public string Languages { get; set; }
    public int MinAge { get; set; }
    public int MaxAge { get; set; }
    public string JoiningDate { get; set; }
    public int ReportingTo { get; set; }
    public bool IsAllEmployee { get; set; }
    #endregion

    public clsVacancyAddEdit()
    { }

    #region Methods

    public static DataTable GetAllQualifications(int JobID)
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GAQ"));
        arr.Add(new SqlParameter("@JobID", JobID));

        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetEmploymentType()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GET"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetGender()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GG"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetAllDesignations(int CompanyID, int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAD"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@JobID", JobID));   
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetRecruitmentLocationType()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GRLT"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetDepartments()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GDEPT"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetCompany(int CompanyID, int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GC"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@JobID", JobID));   
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }
    public static DataTable GetActiveVacanciesCompany(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAVC"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));       
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static int GetRequestedTo(int EmployeeID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GRT"));
        arr.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public int AddUpdateVacancy()
    {
        ArrayList parameters = new ArrayList();
        if (JobStatusID == 5 || JobStatusID == 4 || JobStatusID == 3)
            parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        else
            parameters.Add(new SqlParameter("@JobStatusID", 1));
        if (JobID > 0)
        {
            parameters.Add(new SqlParameter("@Mode", "UPDATE"));
            parameters.Add(new SqlParameter("@JobID", JobID));                     
        }
        else
        {
            parameters.Add(new SqlParameter("@Mode", "I"));
            parameters.Add(new SqlParameter("@ReportingTo", ReportingTo));
        }
        parameters.Add(new SqlParameter("@CompanyID", CompanyId));
        parameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@JobCode", JobCode));
        parameters.Add(new SqlParameter("@JobTitle", JobTitle));
        parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        parameters.Add(new SqlParameter("@RequiredQuota", RequiredQuota));
        parameters.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        //parameters.Add(new SqlParameter("@TotalMarks", TotalMark));
        //parameters.Add(new SqlParameter("@PassPercentage", PassPercentage));
        parameters.Add(new SqlParameter("@Remarks", Remarks));
        parameters.Add(new SqlParameter("@Skills", Skills));
        parameters.Add(new SqlParameter("@Experience", Experience));        
        parameters.Add(new SqlParameter("@PlaceOfWork", PlaceOfWork));
        parameters.Add(new SqlParameter("@WeeklyWorkingDays", WeeklyWorkingDays));
        parameters.Add(new SqlParameter("@DailyWorkingHour", DailyWorkingHours));
        parameters.Add(new SqlParameter("@IsAccomodation", IsAccomodation));
        parameters.Add(new SqlParameter("@IsTransportation", IsTransportation));
        parameters.Add(new SqlParameter("@ContractPeriod", ContractPeriod));
        parameters.Add(new SqlParameter("@AirTicket", AirTicket));
        parameters.Add(new SqlParameter("@AnnualLeaveDays", AnnualLeaveDays));
        parameters.Add(new SqlParameter("@LegalTerms", LegalTerms));
        parameters.Add(new SqlParameter("@CreatedBy", CreatedBy));
        parameters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
        parameters.Add(new SqlParameter("@IsOfferApproval", IsOfferApproval));
       
        parameters.Add(new SqlParameter("@BudgetedAmount", BudgetedAmount));
        parameters.Add(new SqlParameter("@IsForwarded", IsForwarded));
        //tab preference

        parameters.Add(new SqlParameter("@IsBudgeted", IsBudgeted));
        parameters.Add(new SqlParameter("@Languages", Languages));
        parameters.Add(new SqlParameter("@MinAge", MinAge));
        parameters.Add(new SqlParameter("@MaxAge", MaxAge));
        parameters.Add(new SqlParameter("@GenderRecruID", GenderRecruID));
        parameters.Add(new SqlParameter("@RecruitmentTypeID", RecruitmentTypeID));
        parameters.Add(new SqlParameter("@RecruitmentFeesPayable", RecruitmentFeesPayable));
        parameters.Add(new SqlParameter("@JoiningDate", JoiningDate));
       
        return ExecuteScalar(Procedure, parameters).ToInt32();
    }

    public void DeleteGeneralReferenceTables(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DGRT"));
        arr.Add(new SqlParameter("@JobID", JobID));
        ExecuteDataTable(Procedure, arr).ToInt32();
    }

    public void InsertQualificationList()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "IQL"));
        arr.Add(new SqlParameter("@JobID", JobID));
        arr.Add(new SqlParameter("@DegreeId", DegreeId));
        ExecuteNonQuery(Procedure, arr);
    }

    public static bool DoesVacancyExist(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DVE"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32() > 0 ? true : false;
    }

    public static int CheckDuplicateJobCode(int JobID, string JobCode)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "CDJC"));
        if (JobID > 0)
        {
            arr.Add(new SqlParameter("@JobID", JobID));
            arr.Add(new SqlParameter("@JobCode", JobCode));
        }
        else
            arr.Add(new SqlParameter("@JobCode", JobCode));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static DataTable GetJobStatusReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GJSR"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, parameters);
    }

    public static int CheckCriteriaExists(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "CCE"));
        arr.Add(new SqlParameter("@JobID", JobID));        
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static int CheckPayStructureExists(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "CPSE"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public int UpdateJobWhenApproved()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "UWA"));
        parameters.Add(new SqlParameter("@JobID", JobID));
        parameters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
        parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        parameters.Add(new SqlParameter("@IsForwarded", IsForwarded));
        return ExecuteScalar(Procedure, parameters).ToInt32();
    }

    #endregion

    
}
