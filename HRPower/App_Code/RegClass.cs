﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Win32;
using System.Security;
using System.Security.Permissions;
using System.Security.AccessControl;
using System.Security.Principal;

/// <summary>
/// Summary description for RegClass
/// </summary>
public class RegClass
{
	public RegClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public bool ReadFromRegistry(string sMainKey, string sKeyToread, out string rVal)
    {
        rVal = "";
        try
        {
            RegistryKey rKey = Registry.LocalMachine;
            RegistryKey subKey = rKey.OpenSubKey(sMainKey, RegistryKeyPermissionCheck.ReadSubTree, System.Security.AccessControl.RegistryRights.ReadKey);
            if (subKey != null)
            {
                rVal = subKey.GetValue(sKeyToread).ToString();
            }
            if (rVal == "")
                return false;
            else
                return true;
        }
        catch (Exception)
        {
            return false;
        }
    }


}
