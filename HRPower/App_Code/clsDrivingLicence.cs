﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for clsDrivingLicence
/// </summary>
public class clsDrivingLicence:DL
{
	public clsDrivingLicence()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int LicenseID { get; set; }
    public int EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public string LicenceNumber { get; set; }
    public DateTime IssueDate { get; set; }
    public string LicenceHoldersName { get; set; }
    public DateTime ExpiryDate { get; set; }
    public int LicencedCountryID { get; set; }
    public int LicenceTypeID { get; set; }
    public bool IsRenewed { get; set; }

    public string Remarks { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int UserId { get; set; }
    public string SearchKey { get; set; }
    public int Node { get; set; }
    public string Docname { get; set; }
    public string Filename { get; set; }
    public void BeginEmp()
    {
        BeginTransaction("HRspDrivingLicence");
    }

    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    //-----------------------------------------------------------Employee Driving license ------------------------------------------------------------
    public DataSet GetEmpDrivingLicense()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSS"));
        alparameters.Add(new SqlParameter("@LicenseID", LicenseID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspDrivingLicence", alparameters);
    }
    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@UserId", UserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspDrivingLicence", alParameters));
    }

    public DataSet GetAllEmployeeDrivingLicenses()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspDrivingLicence", alParameters);
    }

    public DataSet GetEmployeeLicense()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspDrivingLicence", alParameters);
    }

    public int InsertEmployeeLicense() //Insert Employee  Driving license details
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IH"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        alparameters.Add(new SqlParameter("@IssueDate", IssueDate));
        alparameters.Add(new SqlParameter("@LicenceHoldersName", LicenceHoldersName));
        alparameters.Add(new SqlParameter("@LicenceTypeID", LicenceTypeID));
        if (LicencedCountryID == -1) alparameters.Add(new SqlParameter("@LicencedCountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@LicencedCountryID", LicencedCountryID));
        alparameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));
        alparameters.Add(new SqlParameter("@IsRenewed", IsRenewed));


        LicenseID = Convert.ToInt32(ExecuteScalar(alparameters));

        return LicenseID;
    }


    public int UpdateEmployeeLicense()  //update  Driving license
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UH"));
        alparameters.Add(new SqlParameter("@LicenseID", LicenseID));

        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        alparameters.Add(new SqlParameter("@IssueDate", IssueDate));
        alparameters.Add(new SqlParameter("@LicenceHoldersName", LicenceHoldersName));
        alparameters.Add(new SqlParameter("@LicenceTypeID", LicenceTypeID));
        if (LicencedCountryID == -1) alparameters.Add(new SqlParameter("@LicencedCountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@LicencedCountryID", LicencedCountryID));
        alparameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));
        alparameters.Add(new SqlParameter("@IsRenewed", IsRenewed));

        LicenseID = Convert.ToInt32(ExecuteScalar(alparameters));

        return LicenseID;
    }

    public int UpdateRenewEmployeeLicense()  //Update Renew Employee Driving License
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "UR"));
        alparameters.Add(new SqlParameter("@LicenseID", LicenseID));
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable FillCountry()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDrivingLicence", alparameters);
    }

    public DataTable FillLicenseType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FLT"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDrivingLicence", alparameters);
    }

    public DataTable FillEmployees(long lngEmployeeID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FEM"));
        alparameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDrivingLicence", alparameters);
    }

    public string GetEmployeeName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspDrivingLicence", alparameters));
    }
    public int GetCompanyId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCI"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspDrivingLicence", alparameters));
    }


    public bool IsvisaIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));

        return (Convert.ToInt32(ExecuteScalar("HRspDrivingLicence", alParameters)) > 0 ? true : false);
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DES"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));

        ExecuteNonQuery("HRspDrivingLicence", alParameters);
    }

    //public string GetPrintText(int iVisaID)
    //{
    //    StringBuilder sb = new StringBuilder();
    //    ArrayList alParameters = new ArrayList();

    //    alParameters.Add(new SqlParameter("@Mode", "GSS"));
    //    alParameters.Add(new SqlParameter("@LicenseID", iVisaID));
    //    alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
    //    DataTable dt = ExecuteDataSet("HRspDrivingLicence", alParameters).Tables[0];


    //    sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
    //    sb.Append("<tr><td><table border='0'>");
    //    sb.Append("<tr>");
    //    sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
    //    sb.Append("</tr>");
    //    sb.Append("</table>");
    //    sb.Append("<tr><td style='padding-left:15px;'>");
    //    sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
    //    sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Driving License Information</td></tr>");
    //    sb.Append("<tr><td width='150px'>Driving License Number</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
    //    sb.Append("<tr><td>License Type</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenseType"]) + "</td></tr>");
    //    sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
    //    sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
    //    sb.Append("<tr><td>Licensed Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
    //    sb.Append("<tr><td>Is Renewed</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
    //    sb.Append("<tr><td>Other Info</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

    //    sb.Append("</td>");
    //    sb.Append("</table>");
    //    sb.Append("</td>");
    //    sb.Append("</tr></table>");

    //    return sb.ToString();
    //}
    //public string CreateSelectedEmployeesContent(string sVisaIds)
    //{
    //    StringBuilder sb = new StringBuilder();
    //    ArrayList arraylst = new ArrayList();
    //    arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
    //    arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
    //    arraylst.Add(new SqlParameter("@LicenceIds", sVisaIds));

    //    DataTable dt = ExecuteDataTable("HRspDrivingLicence", arraylst);

    //    sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
    //    sb.Append("<tr><td style='font-weight:bold;' align='center'>Driving License Information</td></tr>");
    //    sb.Append("<tr><td>");
    //    sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

    //    sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee</td><td width='150px'>Driving License Number</td><td width='150px'>License Type</td><td width='100px'>Issue Date</td><td width='100px'>Expiry Date</td><td width='100px'>Licensed Country</td><td width='100px'>Is Renewed</td><td width='100px'>Other Info</td></tr>");
    //    sb.Append("<tr><td colspan='9'><hr></td></tr>");
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenseType"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
    //        sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
    //        sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

    //    }
    //    sb.Append("</td></tr>");
    //    sb.Append("</table>");

    //    return sb.ToString();
    //}

    public DataTable GetPrintText(int iVisaID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@LicenseID", iVisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataSet("HRspDrivingLicence", alParameters).Tables[0];
    }
    public DataTable CreateSelectedEmployeesPrintContent(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@LicenceIds", sVisaIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspDrivingLicence", arraylst);

    }
    public bool IsLicenceNumberExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEC"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));
        alParameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));

        return (Convert.ToInt32(ExecuteScalar("HRspDrivingLicence", alParameters)) > 0 ? true : false);
    }
    public DataSet GetLicenseDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPD"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return ExecuteDataSet("HRspDrivingLicence", alParameters);
    }
    public void DeleteLicenseDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@Node", Node));

        ExecuteNonQuery("HRspDrivingLicence", alParameters);
    }

    public int InsertEmployeeLicenseTreemaster() //Insert Employee  Driving license treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ITM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@LicenseID", LicenseID));
        alparameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateEmployeeVisaTreemaster() //Insert Employee  Driving license treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UTM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@NameinPassport", LicenseID));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable GetLicenseFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));
        alParameters.Add(new SqlParameter("@LicenseID", LicenseID));

        return ExecuteDataTable("HRspDrivingLicence", alParameters);
    }

    public bool CheckIfDocumentIssued(int iLicenseID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDR"));
        alParameters.Add(new SqlParameter("@LicenseID", iLicenseID));

        return ExecuteScalar("HRspDrivingLicence", alParameters).ToInt32()>0;
    }
}
