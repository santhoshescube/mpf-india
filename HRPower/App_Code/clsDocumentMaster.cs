﻿
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;
using System.Text;

    /*==============================================
  Description :	     <Document Transactions>
  Modified By :		 <Laxmi>
  Modified Date :      <13 Apr 2012>
  ================================================*/


public class clsDocumentMaster : DL
    {
        public int DocumentTypeID { get; set; }
        public int CompanyID { get; set; }
        public string DocumentNumber { get; set; }

        public int OperationTypeID { get; set; }

        public long ReferenceID { get; set; }

        public string ReferenceNumber { get; set; }

        public DateTime IssuedDate { get; set; }

        public DateTime ExpiryDate { get; set; }

        public bool IsReceipted1 { get; set; }

        public int DocumentID { get; set; }

        public int OrderNo { get; set; }

        public short DocumentStatusID { get; set; }

        public short BinID { get; set; }

        public DateTime ReceiptIssueDate { get; set; }

        public int Custodian { get; set; }

        public int ReceiptIssueReasonID { get; set; }

        public string Remarks { get; set; }

        public DateTime? ExpectedReturnDate { get; set; }

        public int ReceivedFrom { get; set; }

        public string DocumentName { get; set; }

        public string FileName { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SearchKey { get; set; }

        public int UserID { get; set; }

        public bool IsRenewed { get; set; }
        ArrayList prmDocuments;

        public void BeginEmp()
        {
            BeginTransaction("spDocuments");
        }

        public void Commit()
        {
            CommitTransaction();
        }
        public void RollBack()
        {
            RollbackTransaction();
        }
     

        public static DataTable GetAllOperationTypes()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 1));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static DataTable GetAllReferenceNumberByOperationTypeID(int OperationTypeID, bool InserviceOnly, int CompanyID, int UserID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 2));
            prmDocuments.Add(new SqlParameter("@InserviceOnly", InserviceOnly));
            prmDocuments.Add(new SqlParameter("@OperationTypeID",OperationTypeID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDocuments.Add(new SqlParameter("@UserID", UserID));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }
        public static DataTable GetAllReferenceNumberByOperationTypeIDByCompany(int OperationTypeID, bool InserviceOnly, int UserID,int CompanyID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 2));
            prmDocuments.Add(new SqlParameter("@InserviceOnly", InserviceOnly));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@UserID", UserID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static DataTable GetAllDocumentTypes()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 3));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

      
        public static DataTable GetAllBin()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 4));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }


        public static DataTable GetAllDocumentReceipts()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 5));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static DataTable GetAllDocumentIssues()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 6));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static  DataTable  PrintSelected(string sDocumentIDs)
        {
            StringBuilder sb = new StringBuilder();

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 39));
            alParameters.Add(new SqlParameter("@DocumentIDs", sDocumentIDs));
            alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

           return  new DataLayer().ExecuteDataTable("spDocuments", alParameters);

          
        }


        public static  string GetPrintText(int DocumentID)
        {
            StringBuilder sb = new StringBuilder();

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 38));
            alParameters.Add(new SqlParameter("@DocumentID", DocumentID));

            DataTable dtDocument = new DataLayer().ExecuteDataTable("spDocuments", alParameters);

            if (dtDocument != null && dtDocument.Rows.Count > 0)
            {
               

            }

            return sb.ToString();
        }


        public static DataTable GetOtherDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 24));
            prmDocuments.Add(new SqlParameter("@RowIndex", CurrentIndex));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));            
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static DataTable GetAllCustodian(int UserID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 7));
            prmDocuments.Add(new SqlParameter("@InserviceOnly", 1));
            prmDocuments.Add(new SqlParameter("@UserID", UserID));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static int GetTotalRecordsCount()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 8));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }

        public int SaveDocument()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 9));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID ));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID ));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID ));
            prmDocuments.Add(new SqlParameter("@ReferenceID", ReferenceID ));
            //prmDocuments.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber ));
            prmDocuments.Add(new SqlParameter("@Remarks", Remarks));
            prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            prmDocuments.Add(new SqlParameter("@IssuedDate", IssuedDate));
            prmDocuments.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
            prmDocuments.Add(new SqlParameter("@CreatedBy", new  clsUserMaster().GetUserId() ));
            prmDocuments.Add(new SqlParameter("@IsRenewed",IsRenewed));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
            return ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }

        public int UpdateRenewDocument()  //Update Renew DocumentID
        {
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", 45));
            alparameters.Add(new SqlParameter("@DocumentID", DocumentID));
            return Convert.ToInt32(ExecuteScalar(alparameters));
        }

        public int SaveDocumentReceiptIssue()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 15));
            prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            prmDocuments.Add(new SqlParameter("@ReceiptIssueDate", ReceiptIssueDate));
            prmDocuments.Add(new SqlParameter("@Custodian", Custodian));
            prmDocuments.Add(new SqlParameter("@ReceivedFrom", ReceivedFrom));
            prmDocuments.Add(new SqlParameter("@BinID", BinID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));

            if (DocumentTypeID != 8)
            {
                if (ExpiryDate != null)
                    prmDocuments.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
            }

            if (!IsReceipted1)
            {
                if( ExpectedReturnDate != null)
                        prmDocuments.Add(new SqlParameter("@ExpectedReturnDate", ExpectedReturnDate));
                prmDocuments.Add(new SqlParameter("@ReceiptIssueReasonID", ReceiptIssueReasonID));
            }
            else
            {
                prmDocuments.Add(new SqlParameter("@DocumentStatusID", DocumentStatusID));
            }
            prmDocuments.Add(new SqlParameter("@Remarks", Remarks));
            prmDocuments.Add(new SqlParameter("@IsReceipted", IsReceipted1));
            prmDocuments.Add(new SqlParameter("@CreatedBy",new clsUserMaster().GetUserId()));
            return ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }



        public static DataTable GetDocumentDetails(int CurrentIndex, int DocumentTypeID, int DocumentID, int OperationTypeID,int CompanyID, string SearckKey)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 10));
            prmDocuments.Add(new SqlParameter("@RowIndex", CurrentIndex));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID ));
            prmDocuments.Add(new SqlParameter("@SearchKey", SearckKey));
            prmDocuments.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static bool IsExistsDocumentNumber(int DocumentID,string DocumentNumber)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 11));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID ));
            prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber ));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

        }

        public static DateTime  GetCurrentDate()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 12));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToDateTime();

        }

        public static bool IsReceipted(int OperationTypeID,int DocumentTypeID,int DocumentId)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 16));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID",DocumentTypeID ));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID ));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId ));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0; 

        }


        public static int GetBinID(int OperationTypeID, int DocumentTypeID, int DocumentId)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 17));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }


        public static int GetEmployeeID(int OperationTypeID, int DocumentTypeID, int DocumentId)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 18));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentId));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }


        public static DataTable  GetStockRegisterDocuments(int StatusID,int PageIndex,int PageSize,int OperationTypeID,int DocumentTypeID,string DocumentNumber)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 19));
            prmDocuments.Add(new SqlParameter("@StatusID", StatusID));
            prmDocuments.Add(new SqlParameter("@PageIndex", PageIndex));
            prmDocuments.Add(new SqlParameter("@PageSize", PageSize));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static int GetStockRegisterTotalRecords(int StatusID, int OperationTypeID, int DocumentTypeID, string DocumentNumber)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 20));
            prmDocuments.Add(new SqlParameter("@StatusID", StatusID));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentNumber", DocumentNumber.Trim()));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }


        public static DataTable GetAllTransactionsByDocumentID(int OperationTypeID,int DocumentTypeID,int DocumentID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 21));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }


        public static DataTable GetDocumentDetailsByOrderNo(int OperationTypeID, int DocumentTypeID, int DocumentID,int OrderNo)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 22));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@OrderNo", OrderNo ));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }



        public static bool  DelteOtherDocument(int OperationTypeID, int DocumentTypeID, int DocumentID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 25));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

        }


        public static bool DeleteDocumentReceiptIssue(int OperationTypeID, int DocumentTypeID, int DocumentID,int OrderNo)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 26));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@OrderNo",OrderNo));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32() > 0;

        }


        public static DataTable GetAllOtherDocumentTypes()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 27));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }

        public static int GetDocumentCurrentIndex(int DocumentID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 28));
            prmDocuments.Add(new SqlParameter("@DocumentID",DocumentID));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToInt32();

        }

        public static DataTable GetAllDocumentNumber(int OperationTypeID,int DocumentTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 29));
            prmDocuments.Add(new SqlParameter("@OperationTypeID", OperationTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            return new DataLayer().ExecuteDataTable("spDocuments", prmDocuments);

        }


        public static DataTable GetDocumentsAttached(int intDocumentID,long lngReferenceID)
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", 30));
            alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
            alParameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));

            return new DataLayer().ExecuteDataTable("spDocuments", alParameters);
        }

        public static string GetDocumentType(int intDocumentTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 31));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
            return new DataLayer().ExecuteScalar("spDocuments", prmDocuments).ToStringCustom();

        }
        public static bool DeleteTreeMaster(int intDocumentID, int intDocumentTypeID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 32));
            prmDocuments.Add(new SqlParameter("@DocumentID", intDocumentID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
            return new DataLayer().ExecuteNonQuery("spDocuments", prmDocuments).ToInt32() > 0;
        }

        public bool SaveTreeMaster()
        {
            ArrayList prmDocuments = new ArrayList();

            prmDocuments.Add(new SqlParameter("@Mode",33));
            prmDocuments.Add(new SqlParameter("@ReferenceID", ReferenceID));
            prmDocuments.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
            prmDocuments.Add(new SqlParameter("@DocumentID", DocumentID));
            prmDocuments.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber));
            prmDocuments.Add(new SqlParameter("@Docname", DocumentName));
            prmDocuments.Add(new SqlParameter("@Filename", FileName));
            prmDocuments.Add(new SqlParameter("@NavID", OperationTypeID));

            return new DataLayer().ExecuteScalar("spDocuments",prmDocuments).ToInt32() > 0;
        }

        public static bool DeleteDocument(int intDocumentID)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 34));
            prmDocuments.Add(new SqlParameter("@DocumentID", intDocumentID));
            return new DataLayer().ExecuteNonQuery("spDocuments", prmDocuments) > 0;
        }

        public DataTable GetAllDocuments()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 36));
            prmDocuments.Add(new SqlParameter("@PageIndex", PageIndex));
            prmDocuments.Add(new SqlParameter("@PageSize", PageSize));
            prmDocuments.Add(new SqlParameter("@SearchKey", SearchKey));
            prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
            prmDocuments.Add(new SqlParameter("@UserID", UserID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
            return ExecuteDataTable("spDocuments", prmDocuments);
        }


        public int GetCount()
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 35));
            prmDocuments.Add(new SqlParameter("@SearchKey", SearchKey));
            prmDocuments.Add(new SqlParameter("@UserID", UserID));
            prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
            return Convert.ToInt32(ExecuteScalar("spDocuments", prmDocuments));
        }

        public static bool SingleNodeDelete(int intNode)
        {
            ArrayList prmDocuments = new ArrayList();
            prmDocuments.Add(new SqlParameter("@Mode", 37));
            prmDocuments.Add(new SqlParameter("@Node", intNode));
            return new DataLayer().ExecuteNonQuery("spDocuments", prmDocuments).ToInt32() > 0;
        }
    

    }




