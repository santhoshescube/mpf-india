﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
using Log;

using HRAutoComplete;
using System.Drawing;
using Microsoft.Reporting.WebForms;

/// <summary>
/// Summary description for clsCommon
/// purpose : Handle Common Functions
/// created : Binoj
/// Date    : 22.03.2010
/// </summary>

public class clsCommon : DL
{
    static Random random;
    static string sDefaultTempDirectory;
    public static bool IsESSOnly { get; set; }

   // public static int CompanyID { get; set; }

    public clsCommon() { }

    public int GetCompanyID()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return Convert.ToInt32(userData[1]);
    }

    public string GetSysDate()
    {
        SqlDataReader drDate = ExecuteReader("select convert(nvarchar,getdate(),106) as sdate");

        string strDate = "";

        if (drDate.Read() == true)
            strDate = Convert.ToString(drDate[0]);
        else
            strDate = DateTime.Now.ToString("dd MMM yyyy");

        drDate.Close();

        return strDate;
    }
    public string GetSysMonthYear()
    {
        SqlDataReader drDate = ExecuteReader("select DATENAME(mm,getdate()) + ' ' + DATENAME(Year,getdate()) as sdate");

        string strDate = "";

        if (drDate.Read() == true)
            strDate = Convert.ToString(drDate[0]);
        else
            strDate = DateTime.Now.ToString("MMM yyyy");

        drDate.Close();

        return strDate;
    }
    public string GetSysTime()
    {
        SqlDataReader drTime = ExecuteReader("select right(convert(varchar,getdate(),100),7) as SysTime");

        string strTime = "";

        if (drTime.Read() == true)
            strTime = Convert.ToString(drTime[0]);
        else
            strTime = DateTime.Now.ToString("hh:mm:ss tt");

        drTime.Close();

        return strTime;
    }
    public static string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    public static string Encrypt(string strText, string strEncrKey)
    {
        byte[] byKey = null;
        byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    public static string Decrypt(string strText, string sDecrKey)
    {
        byte[] byKey = null;
        byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
        byte[] inputByteArray = new byte[strText.Length + 1];

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;

            return encoding.GetString(ms.ToArray());

        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }


    public void DeleteAsyncUploadFiles()
    {
        string name = string.Empty;

        for (int i = 0; i < HttpContext.Current.Session.Keys.Count; i++)
        {
            if (HttpContext.Current.Session[i] == null)
                continue;

            if (HttpContext.Current.Session[i].GetType().ToString() == "System.Web.HttpPostedFile")
            {
                name = HttpContext.Current.Session.Keys[i].ToString();
            }
        }

        HttpContext.Current.Session.Remove(name);
    }


    public static DateTime Convert2DateTime(object value)
    {
        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        DateTime date;

        if (value == null)
            return Convert.ToDateTime("1/1/1900", df);

        if (value == DBNull.Value)
            return Convert.ToDateTime("1/1/1900", df);

        if (DateTime.TryParse(value.ToString(), df, DateTimeStyles.None, out date))
            return Convert.ToDateTime(value, df);
        else
            return Convert.ToDateTime("1/1/1900", df);
    }



    //public Byte[] GetImageStream(System.Drawing.Image filePath)
    //{
    //    System.Drawing.Image imgFile;
    //    ImageConverter converter = new ImageConverter();
    //    imgFile = System.Drawing.Image.FromFile(filePath.ToString());
    //    FileStream fs = new FileStream(filePath.ToString(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
    //    return (byte[])converter.ConvertTo(imgFile, typeof(byte[]));
    //}



    //public Byte[] GetImageDataStream(System.Drawing.Image mImage, System.Drawing.Imaging.ImageFormat picFormat)
    //{
    //    try
    //    {
    //        MemoryStream mstream = new MemoryStream();
    //        mImage.Save(mstream, picFormat);
    //        Byte[] bImg = mstream.ToArray();
    //        return bImg;
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //}









    public static void CreateThumbnail(string sSourceFileName, string sDestFileName, int iWidth, bool bIsWidth)
    {
        System.Drawing.Image image = System.Drawing.Image.FromFile(sSourceFileName);

        decimal width, height;

        if (bIsWidth)
        {
            height = Decimal.Divide(Convert.ToDecimal(image.Height), Decimal.Divide(Convert.ToDecimal(image.Width), Convert.ToDecimal(iWidth)));
            width = Convert.ToDecimal(iWidth);
        }
        else
        {
            height = Convert.ToDecimal(iWidth);
            width = Decimal.Divide(Convert.ToDecimal(image.Width), Decimal.Divide(Convert.ToDecimal(image.Height), Convert.ToDecimal(iWidth)));
        }

        System.Drawing.Image thumb = image.GetThumbnailImage(Convert.ToInt32(width), Convert.ToInt32(height), null, System.IntPtr.Zero);

        image.Dispose();

        thumb.Save(sDestFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
    }

    public static void CreateThumbnail(string sSourceFileName, string sDestFileName, int iWidth, int iHeight)
    {
        System.Drawing.Image image = System.Drawing.Image.FromFile(sSourceFileName);

        decimal width, height;

        height = Convert.ToDecimal(iHeight);
        width = Convert.ToDecimal(iWidth);

        System.Drawing.Image thumb = image.GetThumbnailImage(Convert.ToInt32(width), Convert.ToInt32(height), null, System.IntPtr.Zero);

        image.Dispose();

        thumb.Save(sDestFileName, System.Drawing.Imaging.ImageFormat.Jpeg);

        thumb.Dispose();
    }

    public string GetTemplateHeader(int iEmployeeId)
    {
        if (iEmployeeId == -1)
            return string.Empty;

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPT"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));

        DataRow dw = ExecuteDataSet("spEmployeeMaster", alParameters).Tables[0].Rows[0];

        StringBuilder sb = new StringBuilder();

        sb.Append("<table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"font-family: Tahoma\">");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" style=\"margin-bottom: 4px;\">");
        sb.Append("<tr>");
        sb.Append("<td width=\"300\" style=\"font-size: 11px; color: #BE3030; font-size: 28px; letter-spacing: -1px; font-weight: bold\">");
        sb.Append(dw["Name"].ToString() + "</td>");
        sb.Append("<td align=\"right\" style=\"font-size: 11px\">");
        sb.Append("<strong>" + dw["Name"].ToString() + "</strong><br />");
        sb.Append(dw["Road"].ToString() + ", " + dw["Area"].ToString() + "<br />");
        sb.Append(dw["Block"].ToString() + ", " + dw["City"].ToString() + "<br />");
        sb.Append(dw["Province"].ToString() + ", " + dw["Country"].ToString() + "<br />");
        sb.Append(dw["POBox"].ToString() + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<div style=\"min-height: 490px; padding: 5px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #000000; border-bottom-color: #000000; font-family: Tahoma; font-size: 11px; overflow: visible;\">");

        return sb.ToString();
    }

    public string GetPrintWindow()
    {
        string sSciptClose = @"'</script>'";
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type='text/javascript'>");
        sb.Append("function Print(ctrlId)");
        sb.Append("{");
        //sb.Append("alert('fn');");
        sb.Append("var printContent = document.getElementById(ctrlId);");
        sb.Append("var windowUrl = 'about:blank';");
        sb.Append("var uniqueName = new Date();");
        sb.Append("var windowName = 'Print' + uniqueName.getTime();");
        sb.Append("var printWindow = window.open(windowUrl, windowName, 'resizable=1,scrollbars=1,left=' + (100) + ',top=' + (100) + ',width=595,height=' + (this.screen.availHeight - 200));");
        sb.Append("printWindow.document.write(\"<html><head><style type='text/css'>body{font-family: Tahoma;font-size: 11px;color: Black;background-color: White;margin: 5px;width: 100%;}.style1{width: 100%;}</style>\"); ");
        sb.Append("printWindow.document.write('<script type=\"text/javascript\"> function doPrint(ctrl) { ctrl.style.display = \"none\"; this.print(); this.close(); } <\\/script>');");
        sb.Append("printWindow.document.write(\"</head><body><table width='100%'><tr><td align='right'><a href='#' onclick='doPrint(this);'><img src='../App_Themes/Blue/images/print_icon.jpg' alt='Print' border='0' /></a></td></tr></table>\"); ");
        sb.Append("printWindow.document.write(' " + GetHeader() + "');");
        sb.Append("if(printContent) {");
        sb.Append("printWindow.document.write(printContent.innerHTML);}");
        sb.Append("printWindow.document.write('</body></html>');");
        sb.Append("printWindow.document.close();");
        sb.Append("printWindow.focus();");
        sb.Append("}");
        sb.Append("</script>");

        return sb.ToString();
    }

    public static void Download(string fileName)
    {
        FileInfo file = new FileInfo(fileName);

        if (!file.Exists)
        {
            // requested file not exist

            // get current page
            Page page = (Page)HttpContext.Current.Handler;

            if (page != null)
            {
                // get url of calling page
                string url = page.Request.UrlReferrer.AbsoluteUri;

                // construct script
                string script = string.Format("alert('File not found!'); location.href = '{0}';", url);

                // register script
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), script, true);
            }

            return;
        }

        HttpContext.Current.Response.Clear();

        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" + file.Name);

        HttpContext.Current.Response.AppendHeader("Content-Length", file.Length.ToString());

        HttpContext.Current.Response.ContentType = "application/octet-stream";

        HttpContext.Current.Response.WriteFile(file.FullName);

        HttpContext.Current.Response.End();
    }

    public static string TimeDuration(int FromHour, int FromMin, bool isAMFromTime, int ToHour, int ToMin, bool isAMToTime)
    {
        if (isAMFromTime == false && FromHour < 12) FromHour = FromHour + 12;
        if (isAMToTime == false && ToHour < 12) ToHour = ToHour + 12;
        if (isAMFromTime == true && FromHour == 12) FromHour = 24;
        if (isAMToTime == true && ToHour == 12) ToHour = 24;

        if (isAMFromTime == false && isAMToTime == true && ToHour < 24) ToHour = ToHour + 24;
        if (isAMFromTime == isAMToTime && FromHour > ToHour) ToHour = ToHour + 24;
        if (ToMin < FromMin)
        {
            ToMin = ToMin + 60;
            ToHour = ToHour - 1;
        }

        int MinDiff = ToMin - FromMin;
        int HourDiff = ToHour - FromHour;

        if (HourDiff < 0) HourDiff = -(HourDiff);

        return HourDiff.ToString() + ":" + MinDiff.ToString();
    }

    public static void WriteLog(string content)
    {
        try
        {

            File.AppendAllText(HttpContext.Current.Server.MapPath("~/logs/") + "ValidationGroup.txt", content + Environment.NewLine);
        }
        catch { }
    }
    public static void WriteToLog(string content)
    {
        try
        {

            File.AppendAllText(HttpContext.Current.Server.MapPath("~/logs/") + "log.txt", content + Environment.NewLine);
        }
        catch { }
    }
    public static string GetValidationGroup()
    {
        return Guid.NewGuid().ToString("N");
    }

    public static string ToTitleCase(string text)
    {
        System.Globalization.CultureInfo culture = new CultureInfo("en-US");
        return culture.TextInfo.ToTitleCase(text.ToLower());
    }

    public string GetHeader()
    {
        clsUserMaster objUserMaster = new clsUserMaster();

        clsReport objReport = new clsReport();

        DataTable dt = objReport.GetReportHeader();

        StringBuilder sb = new StringBuilder();

        if (dt.Rows.Count > 0)
        {
            sb.Append("<table cellpadding=\"0\" cellspacing=\"0\" width='100%' class=\"style1\" style=\"font-family: Verdana; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #660033; margin-bottom: 5px;\">");
            sb.Append("<tr valign=\"bottom\">");
            sb.Append("<td width=\"100\" style=\"padding-bottom: 5px\">");

            if (dt.Rows[0]["Logo"] != DBNull.Value)
            {
                sb.Append("<img alt=\"\" src=\"../thumbnail.aspx?FromDB=true&type=Company&CompanyId=" + Convert.ToString(dt.Rows[0]["CompanyID"]) + "&width=100&t=" + DateTime.Now.Ticks.ToString() + "\" style=\"padding: 2px; border: 1px solid #D1D1D1;\" /></td>");
            }
            else
                sb.Append("&nbsp;</td>");

            sb.Append("<td style=\"padding-bottom: 5px\">");
            sb.Append("<table class=\"style1\" width='100%'>");
            sb.Append("<tr>");
            sb.Append("<td align=\"right\">");
            sb.Append("<h3 style=\"padding: 0px; margin: 0px; color: #880000\">" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</h3></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine1"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine2"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["Phone"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
        }

        return sb.ToString();
    }

    public DataTable GetReportHeaderDetails()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCD"));
        alParameters.Add(new SqlParameter("@EmployeeID", objUserMaster.GetEmployeeId()));

        DataTable dt = ExecuteDataTable("HRspMailSettings", alParameters);

        DataTable dtHeaderData = new DataTable();
        dtHeaderData.Columns.Add(new DataColumn("CompanyName", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Address", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Phone", typeof(string)));
        dtHeaderData.Columns.Add(new DataColumn("Logo", typeof(byte[])));
        byte[] bytImage = new byte[] { };

        if (dt.Rows.Count > 0)
        {
            string sAddress = string.Empty;
            string sPhoneEmail = string.Empty;

            if (Convert.ToString(dt.Rows[0]["Road"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["Road"]);
            if (Convert.ToString(dt.Rows[0]["Area"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["Area"]);
            if (Convert.ToString(dt.Rows[0]["Block"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["Block"]);
            if (Convert.ToString(dt.Rows[0]["City"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["City"]);
            if (Convert.ToString(dt.Rows[0]["Province"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["Province"]);
            if (Convert.ToString(dt.Rows[0]["Country"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["Country"]);
            if (Convert.ToString(dt.Rows[0]["POBox"]).Trim() != string.Empty) sAddress += ", " + Convert.ToString(dt.Rows[0]["POBox"]);

            if (sAddress.Length > 0)
                sAddress = sAddress.Remove(0, 2); // remove first comma

            if (Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim() != string.Empty) sPhoneEmail += ", Ph: " + Convert.ToString(dt.Rows[0]["ContactPersonPhone"]).Trim();
            if (Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim() != string.Empty) sPhoneEmail += ", Email: " + Convert.ToString(dt.Rows[0]["PrimaryEmail"]).Trim();

            if (sPhoneEmail.Length > 0)
                sPhoneEmail = sPhoneEmail.Remove(0, 2); // remove first comma

            if (dt.Rows[0]["LogoFile"] != DBNull.Value) bytImage = (byte[])dt.Rows[0]["LogoFile"];

            DataRow dr = dtHeaderData.NewRow();

            dr["Address"] = sAddress;
            dr["CompanyName"] = Convert.ToString(dt.Rows[0]["Name"]);
            dr["Phone"] = sPhoneEmail;
            dr["Logo"] = bytImage;

            dtHeaderData.Rows.Add(dr);
        }
        else
        {
            dt.Rows.Add(dt.NewRow());
        }

        return dtHeaderData;
    }

    /// <summary>
    /// Changes default temp directory where crystal report storing temporary files.
    /// Change is required to avoid user level permission issues when application is hosted in web server(IIS)
    /// </summary>
    public static void ChangeCrystalTempDirectory()
    {
        // store default temp directory for later use
        sDefaultTempDirectory = System.Environment.GetEnvironmentVariable("TMP");

        System.IO.DirectoryInfo objDir = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/documents/crystalTemp"));

        // Create directory if it does not exist
        if (!objDir.Exists)
            objDir.Create();

        // Change default temp directory (path in "C" - drive ) to custom location
        System.Environment.SetEnvironmentVariable("TMP", HttpContext.Current.Server.MapPath("~/documents/crystalTemp"));
    }

    /// <summary>
    /// Resets temp directory to default location.
    /// </summary>
    public static void ResetTempDirectory()
    {
        if (!string.IsNullOrEmpty(sDefaultTempDirectory))
            // Reset temp directory back to default.
            System.Environment.SetEnvironmentVariable("TMP", sDefaultTempDirectory);
    }

    public DataTable GetApplicableFromDates(int iCompanyID)// Companies Finyear start date
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GFSD"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        DateTime dFinYearStartDate = Convert.ToDateTime(ExecuteScalar("HRspEvaluationInitiation", alParameters));

        DataTable dt = new DataTable();

        dt.Columns.Add("ApplicableFrom");

        DataRow dw;

        for (int i = dFinYearStartDate.Year; i <= DateTime.Now.Year + 1; i++)
        {
            dw = dt.NewRow();

            dw["ApplicableFrom"] = dFinYearStartDate.ToString("dd MMM ") + i.ToString();

            dt.Rows.Add(dw);
        }

        return dt;
    }
    public DataTable GetFinancialYear(int iCompanyID)// Companies Finyear start date for reports
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GFSD"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        DateTime dFinYearStartDate = Convert.ToDateTime(ExecuteScalar("HRspEvaluationInitiation", alParameters));

        DataTable dt = new DataTable();

        dt.Columns.Add("ApplicableFrom");

        DataRow dw;



        for (int i = DateTime.Now.Year + 1; i >= dFinYearStartDate.Year; i--)
        {
            dw = dt.NewRow();

            //DateTime dCurDate = Convert.ToDateTime("1" + "/" + dFinYearStartDate.Month + "/" + System.DateTime.Now.Year);
            DateTime dCurDate = Convert.ToDateTime(dFinYearStartDate);

            dw["ApplicableFrom"] = dCurDate.ToString("dd MMM ") + i.ToString() + " - " + Convert.ToDateTime(dFinYearStartDate.ToString("dd MMM ") + i.ToString()).AddMonths(12).AddDays(-1).ToString("dd MMM yyyy");

            dt.Rows.Add(dw);
        }

        return dt;
    }
    public DataTable GetFinancialYear(int iCompanyID, int iType)// Companies Finyear start date
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GFSD"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        DateTime dFinYearStartDate = Convert.ToDateTime(ExecuteScalar("HRspEvaluationInitiation", alParameters));

        DataTable dt = new DataTable();

        dt.Columns.Add("ApplicableFrom");

        DataRow dw;



        for (int i = DateTime.Now.Year; i >= dFinYearStartDate.Year; i--)
        {
            dw = dt.NewRow();

            //DateTime dCurDate = Convert.ToDateTime("1" + "/" + dFinYearStartDate.Month + "/" + System.DateTime.Now.Year);
            DateTime dCurDate = Convert.ToDateTime(dFinYearStartDate);

            dw["ApplicableFrom"] = dCurDate.ToString("dd MMM ") + i.ToString() + " - " + Convert.ToDateTime(dFinYearStartDate.ToString("dd MMM ") + i.ToString()).AddMonths(12).AddDays(-1).ToString("dd MMM yyyy");

            dt.Rows.Add(dw);
        }

        return dt;
    }

    public static void WriteErrorLog(Exception ex)
    {
        string filePath = string.Empty;

        if (HttpContext.Current != null)
        {
            if (ConfigurationManager.AppSettings["LOG_FILE"] == null)
                filePath = HttpContext.Current.Server.MapPath("~/logs/log.txt");
            else
                filePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["LOG_FILE"].ToString());

            // Write exception details to file
            ILog logger = new LogToFile(filePath);
            logger.WriteLog(new LogEventArgs(LogSeverity.Error, ex.Message, ex, DateTime.Now));
        }

        // Write exception details to database.
        // logger = new LogToDatabase();
        // logger.WriteLog(new LogEventArgs(LogSeverity.Error, ex.Message, ex, DateTime.Now));

    }

    public static void RegisterAutoComplete(Page page, TextBox txtAutoSuggest)
    {
        string script =
             "$(document).ready(function () {" +
                  "$('#" + txtAutoSuggest.ClientID + "').autocomplete('../AutoComplete.ashx');" +
             "});";

        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), script, true);
    }


    public static void RegisterAutoComplete(Page page, TextBox txtAutoSuggest, HRAutoComplete.AutoCompletePage p,int CompanyID)
    {
        string script =
             "$(document).ready(function () {" +
                  "$('#" + txtAutoSuggest.ClientID + "').autocomplete('../AutoComplete.ashx?p=" + ((int)p) + "&C=" + CompanyID +"');" +
             "});";

        ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), script, true);
    }


    /// <summary>
    /// Returns suggestions for Autocomplete
    /// </summary>
    /// <param name="Prefix"> string to be matched against database fields </param>
    /// <param name="EnumPageIndex"> mode for stored procedure </param>
    /// <returns></returns>
    public string GetSuggestions(string Prefix, string EnumPageIndex , int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", EnumPageIndex));
        alParameters.Add(new SqlParameter("@Prefix", Prefix.EscapeUnsafeChars()));
        alParameters.Add(new SqlParameter("@Count", GlobalAutoComplete.Count));
        alParameters.Add(new SqlParameter("@CompanyID",CompanyID));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        StringBuilder sb = new StringBuilder();

        using (SqlDataReader sdrSuggestions = ExecuteReader(GlobalAutoComplete.CommandName, alParameters))
        {
            if (sdrSuggestions != null)
            {
                while (sdrSuggestions.Read())
                {
                    sb.Append(sdrSuggestions[0].ToString()).Append(Environment.NewLine);
                }
            }
        }

        return sb.ToString();
    }



    public static void PrintReport(Microsoft.Reporting.WebForms.ReportViewer rv, object _this)
    {
        try
        {
            Warning[] warnings;

            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            byte[] bytes = rv.LocalReport.Render("Pdf", null, out mimeType, out encoding, out extension, out streamids, out warnings);


            Control t = (Control)_this;

            if (bytes != null)
            {
                HttpContext.Current.Session["PrintData"] = bytes;
                ScriptManager.RegisterClientScriptBlock(t, t.GetType(), new Guid().ToString(), "window.open('Print.aspx','','left=200,resizable=1');", true);

            }
        }
        catch (Exception ex)
        {
        }
    }
}