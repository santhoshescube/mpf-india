﻿using System;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsTrainingRequest
/// </summary>
public class clsTrainingRequest : DL
{
    string sDescription, sRequestedTo, sApprovedBy;
    int iEmployeeId, iRequestId, iStatusId, iPageIndex, iPageSize,iRequestType,iTopicId ;

    #region Properties
    public int EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }

    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string RequestedTo
    {
        get { return sRequestedTo; }
        set { sRequestedTo = value; }
    }
    public string ApprovedBy
    {
        get { return sApprovedBy; }
        set { sApprovedBy = value; }
    }

    public int RequestId
    {
        get { return iRequestId; }
        set { iRequestId = value; }
    }
    public int RequestType
    {
        get { return iRequestType; }
        set { iRequestType = value; }
    }
    public int TopicId
    {
        get { return iTopicId; }
        set { iTopicId = value; }
    }
    public int StatusId
    {
        get { return iStatusId; }
        set { iStatusId = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public DateTime RequestedDate
    {
        get;
        set;
    }

    public List<clsMessageDetails> RequestedToList { get; set; }

    #endregion
	public clsTrainingRequest()
	{
		//
		// TODO: Add constructor logic here
		//
        
        this.RequestedToList = new List<clsMessageDetails>();
	}

    public DataTable GetStatuses()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAS"));


        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }

    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspTrainingRequest", alParameters);
    }
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SALL"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataTable("HRspTrainingRequest", alParameters);
    }
    public void UpdateLeaveStatus()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "UPS"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        alParemeters.Add(new SqlParameter("@ApprovedBy", iStatusId));

        if (this.RequestedToList.Count > 0)
            alParemeters.Add(new SqlParameter("@RequesteToListXml", this.RequestedToList.ToXml()));

        ExecuteNonQuery("HRspTrainingRequest", alParemeters);
    }
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "DEL"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

        ExecuteNonQuery("HRspTrainingRequest", alParemeters);

    }
    public int InsertUpdateTrainingRequest(bool bIsInsert)
    {
        ArrayList alParameters = new ArrayList();

        if (bIsInsert)
            alParameters.Add(new SqlParameter("@Mode", "I"));
        else
        {
            alParameters.Add(new SqlParameter("@Mode", "U"));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        }

        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TopicId", iTopicId));
        alParameters.Add(new SqlParameter("@RequestType", iRequestType));
        alParameters.Add(new SqlParameter("@Description", sDescription));       
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        if(this.RequestedToList.Count > 0)
            alParameters.Add(new SqlParameter("@RequesteToListXml", this.RequestedToList.ToXml()));

        try
        {
            return Convert.ToInt32(ExecuteScalar("HRspTrainingRequest", alParameters));
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public bool IsRequestExists(bool IsInsert)
    {

        ArrayList alParameters = new ArrayList();

        if (IsInsert)
            alParameters.Add(new SqlParameter("@Mode", "EXT"));
        else
        {
            alParameters.Add(new SqlParameter("@Mode", "EXTE"));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
            alParameters.Add(new SqlParameter("@RequestedDate", RequestedDate));

        }

        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@TopicId", iTopicId));
        alParameters.Add(new SqlParameter("@RequestType", iRequestType));
  

       return (Convert.ToBoolean(ExecuteScalar("HRspTrainingRequest", alParameters)));
     
    }

    /// <summary>
    /// Get email IDs of employees to whom request is sent 
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmailIds()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        return ExecuteDataTable("HRspTrainingRequest", alParameters);
    }

    /// <summary>
    /// Return Requested by ID
    /// </summary>
    /// <returns></returns>
    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return ExecuteScalar("HRspTrainingRequest", alParameters).ToInt32();
    }

    /// <summary>
    /// Get Requested By
    /// </summary>
    /// <returns></returns>
    public string GetRequestedBy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspTrainingRequest", alParameters));
    }

    /// <summary>
    /// Get requested employees
    /// </summary>
    /// <returns></returns>
    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspTrainingRequest", alParameters));
    }

    /// <summary>
    /// Gets approval details 
    /// </summary>
    /// <returns></returns>
    public DataTable GetApprovalDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspTrainingRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsTrainingRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspTrainingRequest", alParameters)) > 0 ? true : false);
    }

    public DataTable FillComboTrainingTopic()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GTT"));
        return ExecuteDataTable("HrSpBindCombo", alParemeters);
    }

    public int GetSuperiorId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GS"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        alParameters.Add(new SqlParameter("@RequestType", iRequestType));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    public DataTable GetAllRequestStatus(int RequestMode)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GARS"));
        alParameters.Add(new SqlParameter("@RequestMode", RequestMode));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);

    }
}
