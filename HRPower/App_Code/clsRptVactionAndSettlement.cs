﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
/// <summary>
/// Summary description for clsRptVactionReport
/// </summary>
public class clsRptVactionAndSettlement : DL
{
	public clsRptVactionAndSettlement()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataTable Getvacation(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int SearchType)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                                new SqlParameter("@Mode", 1),
                      
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                         new SqlParameter("@UserID",new clsUserMaster().GetUserId()),
                        new SqlParameter("@SearchType",SearchType)
                    
                        };
        return new DataLayer().ExecuteDataTable("spPayRptVacationReport", sqlParameter);
    }
    public static DataTable GetvacationDetails(int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
               
                       new SqlParameter("@Mode",2), 
                      
                        new SqlParameter("@EmployeeID",EmployeeID),
                       
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        new SqlParameter("@UserID",new clsUserMaster().GetUserId()),
                       
                        
                        };
        return new DataLayer().ExecuteDataTable("spPayRptVacationReport", sqlParameter);
    }
    public static DataTable GetSettlement(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int SettlementType)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       
                        new SqlParameter("@Mode", 1),
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                        new SqlParameter("@DesignationID",DesignationID),
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@SettlementType",SettlementType),
                        new SqlParameter("@IncludeComp",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate)
                       
                        };
        return new DataLayer().ExecuteDataTable("spPayRptSettlement", sqlParameter);
    }
    public static DataTable GetSettlementDetails(int EmployeeID, DateTime FromDate, DateTime ToDate, int SettlementType)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                       new SqlParameter("@Mode",2), 
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@SettlementType",SettlementType),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        };
        return new DataLayer().ExecuteDataTable("spPayRptSettlement", sqlParameter);
    }





    public static DataTable GetEmpSettlement(int SettlementID) //
    {
        ArrayList prmEmpSettlement = new ArrayList();
        prmEmpSettlement.Add(new SqlParameter("@Mode", 37));
        prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
        return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

    }



    public static DataTable GetEmpSettlementDetails(int SettlementID) //
    {
        ArrayList prmEmpSettlement = new ArrayList();
        prmEmpSettlement.Add(new SqlParameter("@Mode", 36));
        prmEmpSettlement.Add(new SqlParameter("@SettlementID", SettlementID));
        return new DataLayer().ExecuteDataTable("spPayEmployeeSettlementTransaction", prmEmpSettlement);

    }
    public static DataSet EmpGratuityReport(int EmployeeID) //
    {
        ArrayList prmEmpSettlement = new ArrayList();
        //prmEmpSettlement.Add(new SqlParameter("@EmpID", EmployeeID));
        prmEmpSettlement.Add(new SqlParameter("@SettlementID",EmployeeID));
       // prmEmpSettlement.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));

        prmEmpSettlement.Add(new SqlParameter("@IsArabicView",clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataSet("spPayGratuityReport", prmEmpSettlement);
    }









}
