﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmployeeviewDocs
/// </summary>
public class clsEmployeeviewDocs:DL 
{
	public clsEmployeeviewDocs()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int EmployeeID { get; set ;  }
    public int ParentId { get; set; }
    public int HierarchyId { get; set; }

    public DataTable GetAllTreeParents()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteDataTable("HRspEmpViewDocs", alParameters);
    }
    public DataTable GetAllNodes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPC"));
        alParameters.Add(new SqlParameter("@ParentId", ParentId));

        return ExecuteDataTable("HRspEmpViewDocs", alParameters);
    }

    public DataTable GetDocumentDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));
        alParameters.Add(new SqlParameter("@ParentId", ParentId));

        return ExecuteDataTable("HRspEmpViewDocs", alParameters);
    }
    public DataTable FillEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FCM"));
        return ExecuteDataTable("HRspEmployeeVisa", alparameters);
    }
}
