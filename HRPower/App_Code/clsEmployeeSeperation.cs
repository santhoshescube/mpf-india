﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/**/
/// <summary>
/// Summary description for clsTerminateEmployee
/// </summary>
public class clsEmployeeSeperation:DL
{
   
    #region Properties

    public Int64 EmployeeId { get; set; }

    public DateTime SeperatedDate { get; set; }

    public Int64 SeperatedBy { get; set; }

    public int SeperationType { get; set; }

    public string Remarks { get; set; }

    public int SeperationReason { get; set; }

    public int PageSize { get; set; }

    public int PageIndex { get; set; }

    public int EmployeeSeperatedId { get; set; }

    #endregion
    public clsEmployeeSeperation()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region GetAllEmployees
    public DataTable GetAllEmployees(int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);
    }
    #endregion

    #region  GetEmployeeProjectDetails
    public DataTable GetEmployeeProjectDetails(int EmployeeId, int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GPDS"));
        alParameters.Add(new SqlParameter("@EmployeeId",EmployeeId));
        //alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);
        
    }
    #endregion




    public string PrintMultipleItemSeperate(string SEmployeeId)
    {
        try
        {

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GMESD"));
            alParameters.Add(new SqlParameter("@SEmployeeId", SEmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);

            StringBuilder sb = new StringBuilder();

            sb.Append("<div style='font-family: Verdana; font-size: 11px;'>");
            sb.Append("<center>");
            sb.Append("<u> <b>&nbsp; Employee Separation Details&nbsp;</b></u></td>");
            sb.Append("</center>");
            sb.Append("<table width='100%' style='font-size: 11px; margin-top:10px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            sb.Append("<tr>");

            sb.Append("<td ><b> ");
            sb.Append("Employee</b></td>");

            sb.Append("<td><b>");
            sb.Append("Date of joining</b></td>");

            sb.Append("<td><b> ");
            sb.Append("Working Status</b></td>");


            sb.Append("<td><b> ");
            sb.Append("Separation Type</b></td>");

            sb.Append("<td><b> ");
            sb.Append("Date Of Separation" + "</b></td>");

            sb.Append("</tr>");
            foreach (DataRow dr in dt.Rows)
            {


                sb.Append("<tr>");
                sb.Append("<td >");
                sb.Append(dr["EmployeeFullName"] + " </td>");

                sb.Append("<td>");
                sb.Append(Convert.ToDateTime(dr["DateOfJoining"]).ToString("dd/MM/yyyy") + "</td>");


                sb.Append("<td>");
                sb.Append(dr["WorkStatus"] + "</td>");


                sb.Append("<td>");
                sb.Append(dr["SeperationTypeS"] + "</td>");


                sb.Append("<td>");
                sb.Append(Convert.ToDateTime(dr["SeperatedDate"]).ToString("dd/MM/yyyy") + "</td>");
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();

        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public string PrintSingleItemSeperate(int EmployeeId)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GEDBEID"));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dt = this.ExecuteDataTable("HRSpExitInterview", alParameters);

            ArrayList alParameters1 = new ArrayList();
            alParameters1.Add(new SqlParameter("@Mode", "GPDS"));
            alParameters1.Add(new SqlParameter("@EmployeeId", EmployeeId));
            DataTable dtEmployeeProject = this.ExecuteDataTable("HRSpExitInterview", alParameters1);

            DataRow dr = dt.Rows[0];
            StringBuilder sb = new StringBuilder();
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];


            sb.Append("<div style='font-family: Verdana; font-size: 11px'>");
            sb.Append("<center>");
            sb.Append("<u><b style='text-align: center'>&nbsp;Employee Separation Details&nbsp; </b></u>");
            sb.Append("</center>");
            sb.Append("<table width='100%' cellspacing='5px' cellpadding='2px' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;border-top-width: 1px; border-bottom-width: 0px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
            sb.Append("<td style='font-weight: bold; width: 220px;'>");
            sb.Append("<table border='0' >");
            sb.Append("<tr>");
            sb.Append("<td width='60px'>");
            sb.Append("<img src='" + "../thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&Width=70&EmployeeId=" + EmployeeId + "&t=" + DateTime.Now.Ticks + "'/>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr>");
            sb.Append("<td colspan='3' style='font-weight: bold; padding: 2px;'>");
            sb.Append("General Information");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append("<b>" + dr["EmployeeFullName"] + "</b>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Employee Number");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["EmployeeNumber"]);
            sb.Append(" </td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date of joining");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["DateOfJoining"]).ToString("dd/MMM/yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Working Status");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["WorkStatus"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Separation Type");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Date Of " + dr["SeperationTypeS"]);
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(Convert.ToDateTime(dr["SeperatedDate"]).ToString("dd/MMM/yyyy"));
            sb.Append("</td>");
            sb.Append("</tr>");

            if (dr["SeperationType"] != null && Convert.ToInt32(dr["SeperationType"]) == 1 || Convert.ToInt32(dr["SeperationType"]) == 5)
            {
                sb.Append("<tr>");
                sb.Append("<td>");
                sb.Append(dr["SeperationTypeS"] + " Reason" + "</td>");
                sb.Append("<td>");
                sb.Append(dr["SeperatedReason"] + "</td>");
                sb.Append("</tr>");

            }

            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append("Remarks");
            sb.Append("</td>");
            sb.Append("<td>");
            sb.Append(dr["Remarks"]);
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</td>");
            sb.Append("</table>");

            sb.Append("</div>");
            return sb.ToString();

        }
        catch (Exception ex)
        {
            return null;
        }

    }



    #region GetAllSeperatedEmployees
    public DataTable GetAllSeperatedEmployees(int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GASE"));
        alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
        return this.ExecuteDataTable("HRSpExitInterview", alParameters);
    }
    #endregion

    public DataTable GetTerminationReason()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GTR"));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }

    public int GetEmployeeStatusByEmployeeSeperationId(int EmployeeSeperationId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GES"));
        alParameters.Add(new SqlParameter("@EmployeeSeperatedId",EmployeeSeperationId));
        return Convert.ToInt32(ExecuteScalar("HRspExitInterView", alParameters));
    }

    public bool CancelEmployeeSeperation(int EmployeeSeperationId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@EmployeeSeperatedId", EmployeeSeperationId));
        if (Convert.ToInt32(ExecuteScalar("HRspExitInterView", alParameters)) > 0)
        {
            return true;
        }
        else
            return false;
    }
    

    public DataTable GetWorkingStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GWS"));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }


    public DataTable GetEmployeeDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GWS"));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }


    public SqlDataReader  GetEmployeeCode(string EmpCode,int Count)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GEC"));
            alParameters.Add(new SqlParameter("@EmpNo", EmpCode));
            return ExecuteReader("HRspExitInterView", alParameters);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public string GetEmployeePhoto(int EmployeeeId)
    {
        string sFileName = string.Empty;
        string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];
        sFileName = Convert.ToString(EmployeeeId + ".jpg");

        // return string.Format("../thumbnail.aspx?folder=candidatephotos&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);

        return string.Format("" + path + "thumbnail.aspx?folder=candidatephotos&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
    }
    public DataSet  GetSeperatedEmployees(int @IsAllCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GSE"));
          alParameters.Add(new SqlParameter("@IsAllCompany",IsAllCompany));
        alParameters.Add(new SqlParameter("@PageIndex", this.PageIndex));
        alParameters.Add(new SqlParameter("@PageSize",this.PageSize));
        return ExecuteDataSet ("HRspExitInterView", alParameters);
    }

    public  int  GetTotalCount(int IsAllCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GTC"));
        alParameters.Add(new SqlParameter("@IsAllCompany",IsAllCompany));
        return Convert.ToInt32(ExecuteScalar("HRspExitInterView", alParameters));
    }

    public int GetSearchTotalCount(int @IsAllCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GSC"));
          alParameters.Add(new SqlParameter("@IsAllCompany",IsAllCompany));
        return Convert.ToInt32(ExecuteScalar("HRspExitInterView", alParameters));
    }

    public DataSet GetSeperatedEmployeesBySearch(string Key,int IsAllCompany )
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ALL"));
        alParameters.Add(new SqlParameter("@Key",Key));
          alParameters.Add(new SqlParameter("@IsAllCompany",IsAllCompany));
        alParameters.Add(new SqlParameter("@PageIndex",PageIndex));
        alParameters.Add(new SqlParameter("@PageSize",PageSize));
        return ExecuteDataSet("HRspExitInterView", alParameters);
    }


    public bool SeperateEmployee(bool Seperate)
    {
        bool Status = false;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SE"));
        alParameters.Add(new SqlParameter("@EmployeeId",EmployeeId));
        alParameters.Add(new SqlParameter("@EmployeeSeperatedId",EmployeeSeperatedId ));
        alParameters.Add(new SqlParameter("@SeperatedDate",SeperatedDate));
        alParameters.Add(new SqlParameter("@SeperatedBy",SeperatedBy));
        alParameters.Add(new SqlParameter("@SeperationType",SeperationType));
        alParameters.Add(new SqlParameter("@Remarks",Remarks));
        if(SeperationReason != 0 )
            alParameters.Add(new SqlParameter("@ReasonForSeperation",SeperationReason));
        if (Convert.ToInt64(ExecuteScalar("HRspExitInterView", alParameters)) > 0)
        {
            Status = true;
            if (Seperate)
            {
                ArrayList alParameters1 = new ArrayList();
                alParameters1.Add(new SqlParameter("@Mode", "SEPE"));
                alParameters1.Add(new SqlParameter("@EmployeeId", EmployeeId));
                if (!(Convert.ToInt64(ExecuteScalar("HRspExitInterView", alParameters1)) > 0))
                {
                    Status = false;
                }
            }
        }

        return Status;
    }

    public DateTime GetCurrentDate()
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GST"));
            return Convert.ToDateTime(ExecuteScalar("HRspExitInterView", alParameters));
        }
        catch (Exception ex)
        {
            return DateTime.Now;
        }
    }

    public DataTable GetReasonForSeperation()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FRC"));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }
}
