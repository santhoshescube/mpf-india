﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProjectPolicy
/// purpose : Handle Project Policy
/// created : Lekshmi
/// Date    : 24.06.2011
/// </summary>
/// 


public class clsHolidayPolicy :DL 
{
	public clsHolidayPolicy()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public int HolidayPolicyID { get; set; }
    public string Description { get; set; }
    public int CalculationBasedID { get; set; }
    public decimal CalculationPercentage { get; set; }
    public int CompanyBasedOn { get; set; }
    public bool RateOnly { get; set; }
    public decimal HolidayRate { get; set; }
    public bool IsRateBasedOnHour { get; set; }
    public decimal HoursPerDay { get; set; }
    public int SalPolicyID { get; set; }
    public int AddDedID { get; set; }
    public int PolicyType { get; set; }

    public DataTable FillAllCalculationBasedOn()                      // Select All calculation based on parameters
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAD"));
        return ExecuteDataTable("HRspHolidayPolicy", arraylst);
    }

    public DataSet GetAllParticulars()
    {
        ArrayList arraylst = new ArrayList();


        arraylst.Add(new SqlParameter("@Mode", "GAAP"));
        return ExecuteDataSet("HRspHolidayPolicy", arraylst);
    }

    public int InsertUpdateHolidayPolicy(bool bIsInsert)                // Insertion when bIsInsert=true , Updation when bIsInsert=false
    {
        ArrayList arraylst = new ArrayList();

        if (bIsInsert)
            arraylst.Add(new SqlParameter("@Mode", "I"));
        else
        {
            arraylst.Add(new SqlParameter("@Mode", "U"));
            arraylst.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));
        }

        arraylst.Add(new SqlParameter("@Description", Description));
        arraylst.Add(new SqlParameter("@CalculationBasedID", CalculationBasedID));
        arraylst.Add(new SqlParameter("@CalculationPercentage", CalculationPercentage));
        arraylst.Add(new SqlParameter("@CompanyBasedOn", CompanyBasedOn));
        arraylst.Add(new SqlParameter("@RateOnly", RateOnly));
        arraylst.Add(new SqlParameter("@HolidayRate", HolidayRate ));
        arraylst.Add(new SqlParameter("@IsRateBasedOnHour", IsRateBasedOnHour));
        arraylst.Add(new SqlParameter("@HoursPerDay", HoursPerDay));

        return Convert.ToInt32(ExecuteScalar("HRspHolidayPolicy", arraylst));
    }

    public int DeleteHolidayPolicy()                                                // Deletion Of absent Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@HolidayPolicyID", HolidayPolicyID));

        try
        {
            return (Convert.ToInt32(ExecuteScalar("HRspHolidayPolicy", arraylst)));
            
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet GetAllHolidayPolicies()    // Getting all absent policies
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));
        return ExecuteDataSet("HRspHolidayPolicy", alParameters);
    }

    public int InsertHolidaySalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "ISP"));

        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@AddDedID", AddDedID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return Convert.ToInt32(ExecuteScalar("HRspHolidayPolicy", alParameters));
    }

    public DataTable GetHolidaySalaryIDS()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSP"));
        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return ExecuteDataTable("HRspHolidayPolicy", alParameters);
    }

}
