﻿using System;
using System.Data;
using System.Web;

/// <summary>
/// Summary description for GlobalAutoComplete
/// 
/// Created By      : Ratheesh
/// Creation Date   : 1 Feb 2011
/// Description     : Handle Autosuggest requests
/// 
/// </summary>
/// 
namespace HRAutoComplete
{
    public static class GlobalAutoComplete 
    {
        private static string StoredProcedure = "HRspAutoCompleteCommon";
        private static int count = 10;

        /// <summary>
        /// Gets common stored procedure name to retrieve data.
        /// </summary>
        public static string CommandName
        {
            get { return StoredProcedure; }
        }

        /// <summary>
        /// Gets or sets total number of suggestions to be returned. Default value is 10
        /// </summary>
        public static int Count
        {
            get { return count; }
            set { count = value; }
        }

        /// <summary>
        /// Returns auto complete suggestions based on the prefix
        /// </summary>
        /// <param name="Prefix"> </param>
        /// <returns></returns>
        public static string GetSuggestions(string Prefix, string EnumPageIndex , int CompanyID)
        {
            return new clsCommon().GetSuggestions(Prefix, EnumPageIndex, CompanyID);
        }
    }

    /// <summary>
    /// </summary>
    /// <remarks>
    /// Index of enum should be changed with care.
    /// These indices are passing to stored procedure.
    /// if you change any index you should update it in the stored procedure as well
    /// </remarks>
    public enum AutoCompletePage
    {
        Employee = 1,
        Bank = 2,
        Project = 3,
        Vendor = 4,
        Company = 5,
        Heirarchy = 6,
        JobPost = 7,
        Candidate = 8,
        InterviewSchedule = 9,
        WalkinInterview = 10,
        InterviewProcess = 11,
        Benefit = 12,
        Templates = 13,
        Salarystructure =14,
        Passport=15,
        Visa=16,
        OtherDocument=17,
        UserSettings=18,
        HealthCard=19,
        LabourCard=20,
        EmiritesCard=21,
        InsuranceCard=22,
        DrivingLicense=23,
        ViewPolicies=24,
        ViewDocuments = 25,
        Qualification = 26,
        TradeLicense = 27,
        LeaseAgreement = 28,
        Insurance = 29,
        OfferLetter = 30,
        DocumentType = 31,
        AllEmployees = 32,
        TrainingCourse = 33,
        Null = 999 // Default value.
    }
}
