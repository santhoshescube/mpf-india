﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for clsTrainingRegisteration
/// </summary>
public class clsTrainingRegisteration:DL
{
	public clsTrainingRegisteration()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private int iRegistrationId;
    private int iTrainingId;
    private int iEmployeeId;
    private bool bPaidBy;
  
    private string sPaidDate;
    private string sRemarks;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;

    public int RegistrationId
    {
        get { return iRegistrationId; }
        set { iRegistrationId = value; }
    }
    public int TrainingId
    {
        get { return iTrainingId; }
        set { iTrainingId = value; }
    }
    public int EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }
    public bool PaidBy
    {
        get { return bPaidBy; }
        set { bPaidBy = value; }
    }
 
    public string PaidDate
    {
        get { return sPaidDate; }
        set { sPaidDate = value; }
    }
    public string Remarks
    {
        get { return sRemarks; }
        set { sRemarks = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public int InsertTrainingRegisterationDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));

        if(iTrainingId == -1)
            alparameters.Add(new SqlParameter("@TrainingId", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));

        alparameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alparameters.Add(new SqlParameter("@PaidBy", bPaidBy));      
        alparameters.Add(new SqlParameter("@PaidDate", sPaidDate));
        alparameters.Add(new SqlParameter("@Remarks", sRemarks));

        return Convert.ToInt32(ExecuteScalar("HRspTrainingRegistration", alparameters));
    }
    public DataSet FillTraining()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FT"));
        return ExecuteDataSet("HRspTrainingRegistration", alparameters);
    }
    public DataSet BindForm()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@RegistrationId", iRegistrationId));

        return ExecuteDataSet("HRspTrainingRegistration", alparameters);
    }
    public DataSet BindTrainingDetails()  //For Datalist
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alparameters.Add(new SqlParameter("@PageSize", PageSize));
        alparameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));


        return ExecuteDataSet("HRspTrainingRegistration", alparameters);
    }
    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));


        return Convert.ToInt32(ExecuteScalar("HRspTrainingRegistration", alparameters));
    }
    public DataSet BindEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GE"));
        alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));

        return ExecuteDataSet("HRspTrainingRegistration", alparameters);
    }
    public DataSet BindTopicDetails()          /* For dropdown Training selected Index changed */
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ET"));
        alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));

        return ExecuteDataSet("HRspTrainingRegistration", alparameters);
    }

    public Table PrintTrainingRegistrationList(string sTrainingIds)
    {
        Table tblTraining = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblTraining.ID = "PrintRegistrationList";

        if (sTrainingIds.Contains(","))
            td.Text = CreateSelectedRegistrationContent(sTrainingIds);
        else
            td.Text = CreateSingleRegistrationContent(sTrainingIds);

        tr.Cells.Add(td);
        tblTraining.Rows.Add(tr);

        return tblTraining;

    }

    public string CreateSelectedRegistrationContent(string sTrainingIds)
    {
        StringBuilder sb = new StringBuilder();

        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GTR"));
        alparameters.Add(new SqlParameter("@TrainingIds", sTrainingIds));

        DataTable dt = ExecuteDataTable("HRspTrainingRegistration", alparameters);


        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:13px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='center'>Training Registration Details</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma' border='0px' >");
        sb.Append("<tr><td colspan='6'><hr/></td></tr>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'>Company</td><td width='100px'>Training Title</td><td width='200px'>Topic</td><td width='120px'>Date</td><td width='130px'>Venue</td></tr>");
        sb.Append("<tr><td colspan='6'><hr/></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["Company"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["TrainingTitle"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Topic"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Date"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Venue"]) + "</td></tr>");

            DataTable dtEmployees = GetAllEmployeesRegistered(Convert.ToInt32(dt.Rows[i]["TrainingId"]));
            sb.Append("<tr><td colspan='6' style='padding-left:20px;'><table width=100%' style='font-size:11px;font-family:Tahoma'><tr style='font-weight:bold;'><td width='250px'> Employee Name</td><td width='125px'> Employee Number </td><td > Department </td></tr>");
            foreach (DataRow dr in dtEmployees.Rows)
            {
                sb.Append("<tr><td >" + Convert.ToString(dr["EmpName"]) + "</td><td >" + Convert.ToString(dr["EmployeeNumber"]) + "</td><td >" + Convert.ToString(dr["Department"]) + "</td></tr>");
            }
            sb.Append("</table></td></tr>");

        }
        sb.Append("</table></td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }


    public DataTable GetAllEmployeesRegistered(int iTrainingId)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GER"));
        alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));

        return ExecuteDataTable("HRspTrainingRegistration", alparameters);
    }


    public string CreateSingleRegistrationContent(string iTrainingId)
    {
        StringBuilder sb = new StringBuilder();

        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ET"));
        alparameters.Add(new SqlParameter("@TrainingId", Convert.ToInt32(iTrainingId)));

        DataTable dt = ExecuteDataTable("HRspTrainingRegistration", alparameters);
               
        sb.Append("<table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr><td><table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight:bold;font-size:15px;' align='center' valign='center' width='475px'><u>&nbsp;Training Registration Details&nbsp;</u></td>");
        sb.Append("</tr>");
        sb.Append("</table></td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr><td width='150px'>Training Title</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Training"]) + "</td></tr>");
        sb.Append("<tr><td >Topic</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Topic"]) + "</td></tr>");
        sb.Append("<tr><td>Date</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Date"]) + "</td></tr>");
        sb.Append("<tr><td>Venue</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Venue"]) + "</td></tr>");
        sb.Append("<tr><td>Registration fee</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["RegistrationFee"]) + "</td></tr>");
        sb.Append("</table></td></tr>");
        sb.Append("<tr><td style='font-weight:bold;font-size:12px;' align='left' valign='left'><br/>Employee Details</td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'>Employee Name</td><td width='150px'>Employee Number</td><td width='150px'>Department</td></tr>");
        DataTable dtEmployees = GetAllEmployeesRegistered(Convert.ToInt32(iTrainingId));
        foreach (DataRow dr in dtEmployees.Rows)
        {
            sb.Append("<tr><td width='150px'>" + Convert.ToString(dr["EmpName"]) + "</td><td width='150px'>" + Convert.ToString(dr["EmployeeNumber"]) + "</td><td width='150px'>" + Convert.ToString(dr["Department"]) + "</td></tr>");

        }

        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }

}
