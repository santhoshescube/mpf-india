﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Author     : Ratheesh
/// Created On : 2 Dec 2011
/// </summary>
public class clsQualificaitonCriteria : DL
{
    private const string PROCEDURE_NAME = "HRspQualificaitonCriteria";

    public clsQualificaitonCriteria()
    {
    }

    public DataTable GetEducationLevels()
    {
        return this.ExecuteDataTable(PROCEDURE_NAME, new ArrayList { 
            new SqlParameter("@Mode", "GEL")
        });
    }

    public DataTable GetDegreesByEducationLevel(int EducationLevelID)
    {
        return this.ExecuteDataTable(PROCEDURE_NAME, new ArrayList { 
            new SqlParameter("@Mode", "GDBQT"),
            new SqlParameter("@QualificationTypeID", EducationLevelID)
        });
    }

    public DataTable GetSpecializationByDegree(int DegreeID)
    {
        return this.ExecuteDataTable(PROCEDURE_NAME, new ArrayList { 
            new SqlParameter("@Mode", "GSBD"),
            new SqlParameter("@DegreeID", DegreeID)
        });
    }

    public DataTable GetMinimumQualification(int VacancyID)
    {
        return this.ExecuteDataTable(PROCEDURE_NAME, new ArrayList { 
            new SqlParameter("@Mode", "GMQ"),
            new SqlParameter("@VacancyID", VacancyID)
        });
    }
}
