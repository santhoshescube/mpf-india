﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsCandidateHome
/// </summary>
public class clsCandidateHome : DL
{
    private readonly string PROCEDURE_NAME = "HRspCandidateHomePage";
    public Int64 CandidateID { get; set; }
    public int OfferStatusID { get; set; }
    public string Remarks { get; set; }

    //For measurement
    public int UniformSize { get; set; }
    public Decimal NeckAround { get; set; }
    public Decimal ShoulderWidth { get; set; }
    public Decimal ChestAround { get; set; }
    public Decimal SleeveLength { get; set; }
    public Decimal WaistAround { get; set; }
    public Decimal ShirtLength { get; set; }
    public Decimal BicepAround { get; set; }
    public Decimal WristAround { get; set; }
    public Decimal TypeOfFit { get; set; }
    public Decimal Waist { get; set; }
    public Decimal Hips { get; set; }
    public Decimal Abdomen { get; set; }
    public Decimal Thigh { get; set; }
    public Decimal Calf { get; set; }
    public Decimal Instep { get; set; }
    public Decimal SideLength { get; set; }
    public Decimal SideLengthToKnee { get; set; }
    public Decimal Knee { get; set; }

    ArrayList arraylst = null;
    #region SaveMeasurement
    public void SaveMeasurement()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 6));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        arraylst.Add(new SqlParameter("@NeckAround", NeckAround));
        arraylst.Add(new SqlParameter("@ShoulderWidth", ShoulderWidth));
        arraylst.Add(new SqlParameter("@ChestAround", ChestAround));
        arraylst.Add(new SqlParameter("@SleeveLength", SleeveLength));
        arraylst.Add(new SqlParameter("@WaistAround", WaistAround));
        arraylst.Add(new SqlParameter("@ShirtLength", ShirtLength));
        arraylst.Add(new SqlParameter("@BicepAround", BicepAround));
        arraylst.Add(new SqlParameter("@WristAround", WristAround));
        arraylst.Add(new SqlParameter("@TypeOfFit", TypeOfFit));
        arraylst.Add(new SqlParameter("@Waist", Waist));
        arraylst.Add(new SqlParameter("@Hips", Hips));
        arraylst.Add(new SqlParameter("@Abdomen", Abdomen));
        arraylst.Add(new SqlParameter("@Thigh", Thigh));
        arraylst.Add(new SqlParameter("@Calf", Calf));
        arraylst.Add(new SqlParameter("@Instep", Instep));
        arraylst.Add(new SqlParameter("@SideLength", SideLength));
        arraylst.Add(new SqlParameter("@SideLengthToKnee", SideLengthToKnee));
        arraylst.Add(new SqlParameter("@Knee", Knee));

        ExecuteNonQuery(PROCEDURE_NAME, arraylst);
    }
    #endregion SaveMeasurement

    #region GetCandidateID
    /// <summary>
    /// Get offer status :accept offer,reject offer,Enhance Request
    /// </summary>
    /// <returns>datatable of status</returns>
    public Int64 GetCandidateID()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return userData[1].ToInt64();
    }
    #endregion GetCandidateID

    #region GetOfferStatus
    /// <summary>
    /// Get offer status :accept offer,reject offer,Enhance Request
    /// </summary>
    /// <returns>datatable of status</returns>
    public DataTable GetOfferStatus()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 1));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }
    #endregion GetOfferStatus

    #region GetScheduleDetails
    /// <summary>
    /// Get schedule details
    /// </summary>
    /// <returns>datatable of schedules</returns>
    public DataTable GetScheduleDetails()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 2));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }
    #endregion GetScheduleDetails

    #region UpdateOfferStatus
    /// <summary>
    /// Accept/Reject Offerletter
    /// </summary>
    public void UpdateOfferStatus()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 3));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        arraylst.Add(new SqlParameter("@OfferStatusID", OfferStatusID));
        arraylst.Add(new SqlParameter("@Remarks", Remarks));
        ExecuteNonQuery(PROCEDURE_NAME, arraylst);
    }
    #endregion UpdateOfferStatus

    #region GetCurrentStatus
    /// <summary>
    /// Get current status
    /// </summary>
    public DataTable GetCurrentStatus()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 4));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }
    #endregion GetCurrentStatus

    #region GetMeasurements
    /// <summary>
    /// Get Measurements
    /// </summary>
    /// <returns>datatable of Measurements</returns>
    public DataTable GetMeasurements()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 7));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }
    #endregion GetMeasurements

    #region GetOfferLetterDetails
    /// <summary>
    /// Get Measurements
    /// </summary>
    /// <returns>datatable of Measurements</returns>
    public DataTable GetOfferLetterDetails(long lngCandidateID)
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 8));
        arraylst.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }
    #endregion GetOfferLetterDetails

    #region InsertUniformSize
    /// <summary>
    /// InsertUniformSize
    /// </summary>
    public void InsertUniformSize()
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 9));
        arraylst.Add(new SqlParameter("@CandidateID", CandidateID));
        arraylst.Add(new SqlParameter("@UniformSize", UniformSize));
        ExecuteNonQuery(PROCEDURE_NAME, arraylst);
    }
    #endregion InsertUniformSize

    #region GetUniformSize
    /// <summary>
    /// get Uniform Size
    /// </summary>
    public int GetUniformSize(long lngCandidateID)
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 10));
        arraylst.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteScalar(PROCEDURE_NAME, arraylst).ToInt32();
    }
    #endregion GetUniformSize

    public DataTable GetFromToMailIDs(Int64 lngCandidateID)
    {
        arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 11));
        arraylst.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteDataTable(PROCEDURE_NAME, arraylst);
    }

}
