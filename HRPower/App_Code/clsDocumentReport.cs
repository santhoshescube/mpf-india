﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Data;

    public class clsDocumentReport
    {
        ArrayList prmDoc;
        DataLayer MobjDataLayer;
        public clsDocumentReport(DataLayer objDataLayer)
        {
            MobjDataLayer = objDataLayer;
        }
        public DataTable GetDocForAllEmp(int intDocTypeID,bool blnIsPredefined,int intEmpID)
        {
            prmDoc = new ArrayList();
            prmDoc.Add(new SqlParameter("@Mode", "1"));
            prmDoc.Add(new SqlParameter("@DocumentTypeID", intDocTypeID));
            prmDoc.Add(new SqlParameter("@EmployeeID",intEmpID));
            prmDoc.Add(new SqlParameter("@Predefined", blnIsPredefined));
            return MobjDataLayer.ExecuteDataTable("spDocumentAll", prmDoc);
        }
        public DataTable FillCombos(string[] saFieldValues)
        {
            // function for getting datatable for filling combo
            if (saFieldValues.Length == 3 || saFieldValues.Length == 5)
            {
                ArrayList prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "0"));
                prmCommon.Add(new SqlParameter("@Fields", saFieldValues[0]));
                prmCommon.Add(new SqlParameter("@TableName", saFieldValues[1]));
                prmCommon.Add(new SqlParameter("@Condition", saFieldValues[2]));

                return MobjDataLayer.ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
                // return MObjClsCommonUtility.FillCombos(saFieldValues);
            }
            else
                return null;
        }

        public DataTable FillCombos(string sQuery)
        {
            // function for getting datatable for filling combo

            return MobjDataLayer.ExecuteDataTable(sQuery);

        }
        public DataSet GetDocForSingleEmp( int intEmpID)
        {
            prmDoc = new ArrayList();
            prmDoc.Add(new SqlParameter("@Mode", "2"));
          
            prmDoc.Add(new SqlParameter("@EmployeeID", intEmpID));
            
            return MobjDataLayer.ExecuteDataSet("spDocumentAll", prmDoc);
        }



        public static  DataSet AllDocuments(int CompanyID,int BranchID,bool IncludeCompany,int StatusID,int DocumentTypeID,DateTime? FromDate,DateTime? ToDate)
        {
            int TypeID =0;
            ArrayList prmDoc = new ArrayList();
            prmDoc.Add(new SqlParameter("@Mode", 1));
            prmDoc.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));

            if(FromDate != null)
                prmDoc.Add(new SqlParameter("@FromDate", FromDate));

            if(ToDate != null)
                prmDoc.Add(new SqlParameter("@ToDate", ToDate));

            if (FromDate == null && ToDate == null)
                TypeID = 0;
            else if (FromDate != null && ToDate == null)
                TypeID = 1;
            else
                TypeID = 2;

            prmDoc.Add(new SqlParameter("@TypeID", TypeID));
            prmDoc.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDoc.Add(new SqlParameter("@BranchID", BranchID ));
            prmDoc.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            prmDoc.Add(new SqlParameter("@StatusID", StatusID));
            prmDoc.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
            return new DataLayer().ExecuteDataSet("spPayRptDocuments", prmDoc);
        }

        public static DataSet GetAllDocumentsByEmployee(int EmployeeID,int DocumentTypeID,string DocumentNumber,DateTime?FromDate,DateTime? ToDate)
        {
            int TypeID = 0;
            ArrayList prmDoc = new ArrayList();
            prmDoc.Add(new SqlParameter("@Mode", 2));



            if (FromDate != null)
                prmDoc.Add(new SqlParameter("@FromDate", FromDate));


            if (ToDate != null)
                prmDoc.Add(new SqlParameter("@ToDate", ToDate));


            if (FromDate == null && ToDate == null)
                TypeID = 0;
            else if (FromDate != null && ToDate == null)
                TypeID = 1;
            else
                TypeID = 2;


            prmDoc.Add(new SqlParameter("@TypeID", TypeID));
            prmDoc.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID ));
            prmDoc.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
            prmDoc.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        



            prmDoc.Add(new SqlParameter("@EmployeeID", EmployeeID));
            return new DataLayer().ExecuteDataSet("spPayRptDocuments", prmDoc);
        }


        public static DataSet GetSingleDocumentDetails(int CompanyID,int BranchID,bool IncludeCompany,  int DocumentTypeID, string DocumentNumber,DateTime? FromDate,DateTime? ToDate)
        {
            int TypeID = 0;
            ArrayList prmDoc = new ArrayList();
            prmDoc.Add(new SqlParameter("@Mode", 3));
             prmDoc.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));



             if (FromDate != null)
                prmDoc.Add(new SqlParameter("@FromDate", FromDate));


             if (ToDate != null)
             prmDoc.Add(new SqlParameter("@ToDate", ToDate));


             if (FromDate == null && ToDate == null)
                 TypeID = 0;
             else if (FromDate != null && ToDate == null)
                 TypeID = 1;
             else
                 TypeID = 2;


             prmDoc.Add(new SqlParameter("@TypeID", TypeID));

            if (DocumentNumber.ToUpper().Trim() == "-1")
            {
            }
            else
                prmDoc.Add(new SqlParameter("@DocumentNumber", DocumentNumber));


            prmDoc.Add(new SqlParameter("@CompanyID", CompanyID));
            prmDoc.Add(new SqlParameter("@BranchID", BranchID));
                 prmDoc.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
            prmDoc.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
            return new DataLayer().ExecuteDataSet("spPayRptDocuments", prmDoc);
        }

    }


