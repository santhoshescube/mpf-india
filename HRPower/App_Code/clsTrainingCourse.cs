﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Web.UI.MobileControls;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsTrainingCourse
/// </summary>
public class clsTrainingCourse :DL
{
	public clsTrainingCourse()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// Prperties for get and set values
    /// </summary>
    public string CourseCode { get; set; }
    public string Topic { get; set; }
    public string Description { get; set; }
    public bool IsCertification { get; set; }
    public bool Status { get; set; }
    public int CourseID { get; set; }
    public int PageIndex { get; set;  }
    public int PageSize { get; set; }
    public string SearchKey { get; set; }

    public int GetCourseMaxNumber()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "MN"));
        return ExecuteScalar("HRspTrainingCourse", alParameters).ToInt32();
    }
    public int SaveCourse()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@CourseCode", CourseCode));
        alParameters.Add(new SqlParameter("@Topic", Topic));
        alParameters.Add(new SqlParameter("@Description", Description));
        alParameters.Add(new SqlParameter("@IsCertification", IsCertification ));
        alParameters.Add(new SqlParameter("@Status", Status));
      
        if(CourseID > 0)
            alParameters.Add(new SqlParameter("@CourseID", CourseID));
       return ExecuteScalar("HRspTrainingCourse", alParameters).ToInt32();

    }
    public DataSet GetCourse()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SALL"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex ));
        alParameters.Add(new SqlParameter("@PageSize", PageSize ));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        return ExecuteDataSet("HRspTrainingCourse", alParameters);
    }
    public DataTable GetCourseDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S"));
        alParameters.Add(new SqlParameter("@CourseID",CourseID));
        return ExecuteDataTable("HRspTrainingCourse", alParameters);
    }

    public bool  DeleteCourse()
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "D"));
            alParameters.Add(new SqlParameter("@CourseID", CourseID));

            ExecuteNonQuery("HRspTrainingCourse", alParameters);

            return true;
        }
        catch
        {
            return false;
        }
    }
}
    