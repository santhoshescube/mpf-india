﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsJobKRA
/// </summary>
/// 
[Serializable]

public class clsKRA
{
    public int KRAId { get; set; }

    public decimal KRAValue { get; set; }

    public int EmployeeKraID { get; set; }

	public clsKRA()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
