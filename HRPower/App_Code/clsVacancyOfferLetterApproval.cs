﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for VacancyOfferLetterApproval
/// </summary>

public class clsVacancyOfferLetterApproval:DL
{
    #region Variables
    public static string Procedure = "HRspVacancyAddEdit";
    #endregion

    #region Properties
    public static int ID { get; set; }    
    public int JobID { get; set; }
    public int EmployeeID { get; set; }
    public string EmployeeFullName { get; set; }
    public int Level { get; set; }
    public bool IsOfferLetterApproval { get; set; }
    public bool IsAllEmployee { get; set; }
    #endregion


    public clsVacancyOfferLetterApproval()
	{ }

    #region Methods

    public static DataSet GetOfferApprovals(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GOA"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteDataSet(Procedure, arr);
    }
        
    public static DataTable GetAllEmployees(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAE"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));       
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public void InsertApprovalAuthority()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IAA"));
        parameters.Add(new SqlParameter("@JobID", JobID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@Level", Level));
        parameters.Add(new SqlParameter("@IsOfferLetterApproval", IsOfferLetterApproval));
        parameters.Add(new SqlParameter("@IsAllEmployee", IsAllEmployee));
        ExecuteNonQuery(Procedure, parameters);
    }
    public void DeleteApprovalAuthority()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DAA"));
        parameters.Add(new SqlParameter("@JobID", JobID));    
        ExecuteNonQuery(Procedure, parameters);
    }

    #endregion
}
