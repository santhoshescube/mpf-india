﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsAgencyReference
/// </summary>
public class clsAgencyReference:DL
{
    #region Declarations
    
    public int AgencyID { get; set; }
    public string Agency { get; set; }
    public string AgencyArb { get; set; }
    public string Address { get; set; }
    public string Email { get; set; }
    public string Telephone { get; set; }

    #endregion Declarations

    #region Constructor

    public clsAgencyReference()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #endregion Constructor

    #region Methods

    public void AddAgencyReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "INS"));
        parameters.Add(new SqlParameter("@Agency", Agency));
        parameters.Add(new SqlParameter("@AgencyArb", AgencyArb));
        parameters.Add(new SqlParameter("@Address", Address));
        parameters.Add(new SqlParameter("@Email", Email));
        parameters.Add(new SqlParameter("@Telephone", Telephone));

        ExecuteNonQuery("HRspAgencyReference", parameters);
    }
    public void UpdateAgencyReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "UPD"));
        parameters.Add(new SqlParameter("@AgencyID", AgencyID));
        parameters.Add(new SqlParameter("@Agency", Agency));
        parameters.Add(new SqlParameter("@AgencyArb", AgencyArb));
        parameters.Add(new SqlParameter("@Address", Address));
        parameters.Add(new SqlParameter("@Email", Email));
        parameters.Add(new SqlParameter("@Telephone", Telephone));

        ExecuteNonQuery("HRspAgencyReference", parameters);
    }
    public DataTable GetAgency()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "SEL"));

        return ExecuteDataTable("HRspAgencyReference", parameters);
    }
    public DataTable GetDataById()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GBI"));
        parameters.Add(new SqlParameter("@AgencyID", AgencyID));

        return ExecuteDataTable("HRspAgencyReference", parameters);
    }
    public void Delete()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DEL"));
        parameters.Add(new SqlParameter("@AgencyID", AgencyID));
        ExecuteNonQuery("HRspAgencyReference", parameters);
    }
    public int CheckAgencyNameExistence()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DUP"));
        if (AgencyID > 0)
        {
            parameters.Add(new SqlParameter("@AgencyID", AgencyID));
            parameters.Add(new SqlParameter("@Email", Email));
        }
        else
            parameters.Add(new SqlParameter("@Email", Email));
        return Convert.ToInt32(ExecuteScalar("HRspAgencyReference", parameters));
    }
    public int IsAgencyUsed()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IAU"));
        parameters.Add(new SqlParameter("@AgencyID", AgencyID));

        return Convert.ToInt32(ExecuteScalar("HRspAgencyReference", parameters));
    }

    #endregion Methods
}
