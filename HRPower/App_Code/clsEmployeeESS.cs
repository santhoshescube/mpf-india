﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmployeeESS
/// </summary>
public class clsEmployeeESS
{
	public clsEmployeeESS()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataTable GetEmployeeLeaveRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int LeaveTypeID, int StatusID, DateTime  FromDate, DateTime  ToDate,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        alParameters.Add(new SqlParameter("@TypeID", LeaveTypeID));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        
        if(FromDate != DateTime.Now)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if(ToDate != DateTime.Now)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetSalaryRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetLoanRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate, int TypeID,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@TypeID", TypeID ));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID",WorkStatusID));

        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetTransferRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate, int TypeID,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@TypeID", TypeID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetDocumentRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate, int TypeID,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@TypeID", TypeID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID" , WorkStatusID));

        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetResignationRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate, int TypeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@TypeID", TypeID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }

    public static DataTable GetTrainingRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate, int TypeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@TypeID", TypeID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }
    public static DataTable GetExpenseRequestESS(int EmployeeID, int CompanyID, int BranchID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }
    public static DataTable GetTravelRequestESS(int CompanyID, int BranchID, int EmployeeID, bool IncludeCompany, int StatusID, DateTime FromDate, DateTime ToDate,int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));        
        alParameters.Add(new SqlParameter("@StatusID", StatusID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        if (FromDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));

        return new DataLayer().ExecuteDataTable("HRSpRptESS", alParameters);
    }
}
