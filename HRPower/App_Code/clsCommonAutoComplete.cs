﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Created By : Rajesh.R
/// Created On : 12-07-2011
/// </summary>
public class clsCommonAutoComplete:DL
{
    public clsCommonAutoComplete()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public SqlDataReader GetSuggestions(string TableName,string Suggestions,string Key,int Count)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@TableName",TableName));
            alParameters.Add(new SqlParameter("@Suggestions",Suggestions));
            alParameters.Add(new SqlParameter("@Key",Key));
            alParameters.Add(new SqlParameter("@Count",Count));
            return ExecuteReader("AutoComplete",alParameters );
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public SqlDataReader GetSuggestions(string Key,int CompanyId)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode","GEC"));
            alParameters.Add(new SqlParameter("@EmpNo",Key));
            alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
            return ExecuteReader("HRspExitInterview", alParameters);
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public SqlDataReader GetEmployeeTakeOverSuggestions(string Key, int CompanyId)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GETOS"));
            alParameters.Add(new SqlParameter("@CompanyId",CompanyId));
            alParameters.Add(new SqlParameter("@EmpNo", Key));
            return ExecuteReader("HRspExitInterview", alParameters);
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public SqlDataReader GetEmployeeNameSuggestions(string Key, int Count)
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode","GEN"));
            alParameters.Add(new SqlParameter("@Key",Key));
           // alParameters.Add(new SqlParameter("@Count", Count));
            return ExecuteReader("HRspExitInterview", alParameters);
        }
        catch (Exception ex)
        {
            return null;
        }
    } 


    public string[] GetAllSuggestions(string Key, int Count,int @IsAllCompany)
    {
        try
        {
            List<string> suggestions = new List<String>();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GS"));
            alParameters.Add(new SqlParameter("@IsAllCompany", IsAllCompany));
            alParameters.Add(new SqlParameter("@Key", Key));
           
            DataTable dt = ExecuteDataTable("HrSpExitInterview", alParameters);

            foreach (DataRow dr in dt.Rows)
            {
                string s = Convert.ToString(dr["Suggestions"]);
                suggestions.Add(s);
            }
            return suggestions.ToArray();
        }
        catch (Exception ex)
        {
            return null;
        }
    }

}
