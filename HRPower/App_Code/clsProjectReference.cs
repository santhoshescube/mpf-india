﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;



public class clsProjectReference : DL
{
    int iProjectID;
    int iCompanyID;
    string sProjectCode;
    string sDescription;
    DateTime DStartDate;
    DateTime DEstimatedFinishDate;
    string sSponsor;
    Decimal dBudget;
    Decimal dUtilised;
    int iNoOfStaff;
    int iProjectManager;
    int iPMPolicyID;
    string sLocation;
    string sRemarks;

    int iProjectTeamID;
    int iEmployeeID;
    DateTime DEmpWorkStartDate;
    DateTime DEmpFinishDate;
    int iDuration;
    int iEmpProjectPolicyID;
    string sProjectIDs;

    int iPageIndex;
    int iPageSize;
    string sSortExpression;
    string sSortOrder;
    string sSearchKey;
    int iUserId;

    public int ProjectID { get { return iProjectID; } set { iProjectID = value; } }
    public int CompanyID { get { return iCompanyID; } set { iCompanyID = value; } }
    public string ProjectCode { get { return sProjectCode; } set { sProjectCode = value; } }
    public string Description { get { return sDescription; } set { sDescription = value; } }
    public DateTime StartDate { get { return DStartDate; } set { DStartDate = value; } }
    public DateTime EstimatedFinishDate { get { return DEstimatedFinishDate; } set { DEstimatedFinishDate = value; } }
    public string Sponsor { get { return sSponsor; } set { sSponsor = value; } }
    public Decimal Budget { get { return dBudget; } set { dBudget = value; } }
    public Decimal Utilised { get { return dUtilised; } set { dUtilised = value; } }
    public int NoOfStaff { get { return iNoOfStaff; } set { iNoOfStaff = value; } }
    public int ProjectManager { get { return iProjectManager; } set { iProjectManager = value; } }
    public string Location { get { return sLocation; } set { sLocation = value; } }
    public string Remarks { get { return sRemarks; } set { sRemarks = value; } }
    public int PMPolicyID { get { return iPMPolicyID; } set { iPMPolicyID = value; } }

    public int ProjectTeamID { get { return iProjectTeamID; } set { iProjectTeamID = value; } }
    public int EmployeeID { get { return iEmployeeID; } set { iEmployeeID = value; } }
    public DateTime EmpWorkStartDate { get { return DEmpWorkStartDate; } set { DEmpWorkStartDate = value; } }
    public DateTime EmpFinishDate { get { return DEmpFinishDate; } set { DEmpFinishDate = value; } }
    public int Duration { get { return iDuration; } set { iDuration = value; } }
    public int EmpProjectPolicyID { get { return iEmpProjectPolicyID; } set { iEmpProjectPolicyID = value; } }
    public string ProjectIDs { get { return sProjectIDs; } set { sProjectIDs = value; } }

    public int PageIndex { get { return iPageIndex; } set { iPageIndex = value; } }
    public int PageSize { get { return iPageSize; } set { iPageSize = value; } }
    public string SortExpression { get { return sSortExpression; } set { sSortExpression = value; } }
    public string SortOrder { get { return sSortOrder; } set { sSortOrder = value; } }
    public string SearchKey { get { return sSearchKey; } set { sSearchKey = value; } }
    public int UserId { get { return iUserId; } set { iUserId = value; } }
    public int Status { get; set; }

    public clsProjectReference() { }

    /// <summary>
    /// Lists all the companies
    /// </summary>
    /// <returns></returns>
    public DataSet GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataSet("HRspProjects", alParameters);
    }

    /// <summary>
    /// Lists all the employees of specified company
    /// </summary>
    /// <returns></returns>
    public DataTable  GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        
        alParameters.Add(new SqlParameter("@WorkStartDate", DEmpWorkStartDate));
        alParameters.Add(new SqlParameter("@FinishDate", DEmpFinishDate));
     
       
        return ExecuteDataTable("HRspProjects", alParameters);
    }

    public DataSet GetEmployeesForProjectManager()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEP"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspProjects", alParameters);
    }


    /// <summary>
    /// Lists all projects
    /// </summary>
    /// <returns></returns>
    public DataSet GetAllProjects()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        alParameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
       
       // alParameters.Add(new SqlParameter("@UserId", iUserId));

        return ExecuteDataSet("HRspProjects", alParameters);
    }

    /// <summary>
    /// Returns Project detais and team details by project ID
    /// </summary>
    /// <returns></returns>
    public DataSet GetProjectDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GPBI"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
       

        return ExecuteDataSet("HRspProjects", alParameters);
    }

    /// <summary>
    /// Gets total number of projects
    /// </summary>
    /// <returns> Project count</returns>
    public int GetProjectCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPC"));

        return Convert.ToInt32(ExecuteScalar("HRspProjects", alParameters));
    }

    public bool IsProjectExist()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "PE"));
        if (ProjectID > 0)
            alparameters.Add(new SqlParameter("@ProjectId", ProjectID));


        alparameters.Add(new SqlParameter("@ProjectCode", ProjectCode));

        return Convert.ToBoolean(ExecuteScalar("HRspProjects", alparameters));
    }
    /// <summary>
    /// Insert new project
    /// </summary>
    public int Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "I"));
      
        alParameters.Add(new SqlParameter("@ProjectCode", sProjectCode));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@StartDate", DStartDate));
        alParameters.Add(new SqlParameter("@EndDate", DEstimatedFinishDate));        
        alParameters.Add(new SqlParameter("@Remarks", sRemarks));
        alParameters.Add(new SqlParameter("@Budget", dBudget));
        return Convert.ToInt32(ExecuteScalar("HRspProjects",alParameters));
    }

    public int InsertTeamMember()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ITD"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@WorkStartDate", DEmpWorkStartDate));
        alParameters.Add(new SqlParameter("@Duration", iDuration));
        alParameters.Add(new SqlParameter("@FinishDate", DEmpFinishDate));
        //if(iEmpProjectPolicyID > 0) alParameters.Add(new SqlParameter("@ProjectPolicyID", iEmpProjectPolicyID));

        return Convert.ToInt32(ExecuteScalar("HRspProjects", alParameters));
    }

    /// <summary>
    /// Update a project
    /// </summary>
    public int Update()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        
        alParameters.Add(new SqlParameter("@ProjectCode", sProjectCode));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@StartDate", DStartDate));
        alParameters.Add(new SqlParameter("@EndDate", DEstimatedFinishDate));
        alParameters.Add(new SqlParameter("@Status", Status));
        //alParameters.Add(new SqlParameter("@Sponsor", sSponsor));
        alParameters.Add(new SqlParameter("@Budget", dBudget));
        //alParameters.Add(new SqlParameter("@NoOfStaff", iNoOfStaff));
        //alParameters.Add(new SqlParameter("@ProjectManager", iProjectManager));
       // alParameters.Add(new SqlParameter("@Location", sLocation));
        alParameters.Add(new SqlParameter("@Remarks", sRemarks));
        //alParameters.Add(new SqlParameter("@PMPolicyID", PMPolicyID));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    /// <summary>
    /// Update a project
    /// </summary>
    public int UpdateTeamMember()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UTM"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@WorkStartDate", DEmpWorkStartDate));
        alParameters.Add(new SqlParameter("@FinishDate", DEmpFinishDate));
        if(iEmpProjectPolicyID > 0)
            alParameters.Add(new SqlParameter("@ProjectPolicyID", iEmpProjectPolicyID));
        
        alParameters.Add(new SqlParameter("@ProjectTeamID", iProjectTeamID));
        alParameters.Add(new SqlParameter("@Duration", iDuration));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public int DeleteATeamMember()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPTM"));
        alParameters.Add(new SqlParameter("@ProjectTeamID", iProjectTeamID));

        return ExecuteNonQuery("HRspProjects", alParameters);
    }

    public bool IsAssigned()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IA"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        return (Convert.ToInt32(ExecuteScalar("HRspProjects", alParameters)) > 0 ? true : false);
    }

    public bool Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        try
        {
            ExecuteNonQuery("HRspProjects", alParameters);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public DataSet Print()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GMP"));
        alparameters.Add(new SqlParameter("@ProjectIDs", sProjectIDs));
        alparameters.Add(new SqlParameter("@ProjectID", ProjectID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

       return  ExecuteDataSet("HRspProjects", alparameters);

    }

    public void Begin()
    {
        BeginTransaction("HRspProjects");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }


    public string CreateSingleProjctContent()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPS"));
        alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        DataSet ds = ExecuteDataSet("HRspProjects", alParameters);
        StringBuilder sb = new StringBuilder();

        string sOuterBorder = string.Empty; //"border:solid 1px #000000;";

        if (ds.Tables.Count > 0)
        {
            DataTable dt = ds.Tables[0];

            string sFontWeight = "font-weight:normal";
            sb.Append("<div style='"+ sOuterBorder+" padding:10px; margin:5px;'>");
            sb.Append("<center> <h5 style='margin:0; padding:0; text-transform:uppercase;'>");
            sb.Append("Project Details</h5> </center>");
            sb.Append("<hr style='border:solic 1px #CCCCCC;'/>");
            sb.Append("<div style='margin-left:10px; padding:5px 0;'>"); // apply 10px margin

            sb.Append("<span style='font-size:12px; font-weight:bold' > General </span>");
            // General Info
            sb.Append("<table style='font-size: 11px; line-height: 20px; margin-top:5px;'>");
            
            sb.Append("<tr> <td style=' " + sFontWeight + " ' width='200'> Project Code </td> ");
            sb.Append("<td> : " + dt.Rows[0]["ProjectCode"] + "<td> </tr>");

            sb.Append("<tr> <td style='" + sFontWeight + "'> Description </td>");
            sb.Append("<td> : " + dt.Rows[0]["Description"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Start Date </td> ");
            sb.Append("<td> : " + dt.Rows[0]["StartDate"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Estimated Finish Date </td>");
            sb.Append("<td> : " + dt.Rows[0]["EstimatedFinishDate"] + "</td> </tr>");

            sb.Append("<tr> <td style='" + sFontWeight + "'> Project Manager </td>");
            sb.Append("<td> : " + dt.Rows[0]["EmployeeName"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Location </td>");
            sb.Append("<td> : " + dt.Rows[0]["Location"] + "</td> </tr>");

            sb.Append("<tr> <td style='" + sFontWeight + "'> No Of Staff </td>");
            sb.Append("<td> : " + dt.Rows[0]["NoOfStaff"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Sponsor </td>");
            sb.Append("<td> : " + dt.Rows[0]["Sponsor"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Budget</td>");
            sb.Append("<td> : " + dt.Rows[0]["Budget"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Utilised </td>");
            sb.Append("<td> : " + dt.Rows[0]["Utilised"] + "</td> </tr>");
            
            sb.Append("<tr> <td style='" + sFontWeight + "'> Other Info</td>");
            sb.Append("<td> : " + dt.Rows[0]["Remarks"] + "</td> </tr>");

            sb.Append("</table>");

            // Project Team Details
            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                sb.Append("</br>");
                sb.Append("<span style='font-size:12px; font-weight:bold'> Team Information </span>");
                sb.Append("</br> </br>");
                sb.Append("<table style='border:solid 1px #F0F0F0; border-collapse:collapse; width:90%; font-size:11px;' cellpadding='4'>");
                sb.Append("<tr style='background-color:#F0F0F0; font-size:11px'> <td style='font-weight:bold'> Employee Name </td>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Work Start Date </td>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Work Finish Date </td>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Duration (Days) </td> </tr>");
               
                foreach (DataRow Row in ds.Tables[1].Rows)
                {
                    sb.Append("<tr> <td style='border:solid 1px #F0F0F0;'> " + Row["EmployeeName"] + "</td>");
                    sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["WorkStartDate"] + "</td>");
                    sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["FinishDate"] + "</d>");
                    sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["Duration"] + " </td> </tr>");
                }

                sb.Append("</table>");
            }

            sb.Append("</div>");
            sb.Append("</div>"); // outer container

        }
        return sb.ToString();
    }

    public string CreateSelectedProjectContent()  
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GMP"));
        alparameters.Add(new SqlParameter("@ProjectIDs", sProjectIDs));

        DataTable dt = ExecuteDataTable("HRspProjects", alparameters);

        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px; text-transform:uppercase;' align='center'>Project Details</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table style='font-size:11px;font-family:Tahoma; width:100%;' border='0px'>");
        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'>");
        // table header
        sb.Append("<td>Project Code</td>");
        sb.Append("<td>Description</td>");
        sb.Append("<td>Project Manager</td>");
        sb.Append("<td>Start Date</td>");
        sb.Append("<td>Estimated Finish Date</td>");
        sb.Append("<td>Number of staffs</td>");

        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["ProjectCode"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Description"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EmployeeName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["StartDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EstimatedFinishDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NoOfStaff"]) + "</td>");
        }
        sb.Append("</td></tr> </table> </td> </tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string GetPrintText(bool PrintSingleProject)
    {
        StringBuilder sb = new StringBuilder();

        if (PrintSingleProject)
        {
            sb.Append(CreateSingleProjctContent());
        }
        else
        {
            // multiple projects
            sb.Append(CreateSelectedProjectContent());
        }

        return sb.ToString();
    }

    public DataSet GetAllProjectsToBindDropdown()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPD"));

        return ExecuteDataSet("HRspProjects", alParameters);
    }
}
