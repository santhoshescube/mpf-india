﻿using System;
using System.Data;
using System.Web.Security;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsMISReports
/// </summary>
public class clsMISReports : DL
{
    public int iDesignationID;

	public clsMISReports() { }

    public DataSet GetDepartmentWiseLeave()
    {
        return ExecuteDataSet("HRspHrMISReport");
    }

    public DataTable GetEmployeeStrengthTest()
    {
        ArrayList alParameters = new ArrayList();

        if(iDesignationID > 0)
            alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));

        return ExecuteDataTable("HRspTestCrystalReportUsingParameters", alParameters);
    }

    public DataSet GetAllDesignations()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAD"));

        return ExecuteDataSet("HRspProjectReference", alParameters);
    }


}
