﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmployeeDutyRoaster
/// </summary>
public class clsEmployeeDutyRoaster:DL
{

        public clsEmployeeDutyRoaster() {}
        public int intCompanyID { get; set; }
        public int intEmployeeID { get; set; }
        public string strRosterDate { get; set; }
        public int intRosterStatusID { get; set; }
        public int intDepartmentID { get; set; }
        public int intUserID { get; set; }






#region Functions


        public DataTable LoadCombo(int iType)
        {
          
            DataTable datCombos = new DataTable(); 
            DataTable datCombosDepart =new DataTable();
             if (iType == 0 || iType == 1)
               {
 
                string[] saFieldValuesCom = { "CompanyID,CompanyName", "CompanyMaster", "" };
                if (saFieldValuesCom.Length == 3 || saFieldValuesCom.Length == 5)
                {
                ArrayList prmCommon = new ArrayList();
                prmCommon.Add(new SqlParameter("@Mode", "0"));
                prmCommon.Add(new SqlParameter("@Fields", saFieldValuesCom[0]));
                prmCommon.Add(new SqlParameter("@TableName", saFieldValuesCom[1]));
                prmCommon.Add(new SqlParameter("@Condition", saFieldValuesCom[2]));
                datCombos=  ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
                }
               
             
               }
             if (iType == 0 || iType == 2)
             {
                 ArrayList prmReportDetails = new ArrayList();
                 prmReportDetails.Add(new SqlParameter("@Mode", 5));
                 datCombos = new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
                 if (Convert.ToInt32(datCombos.Rows[0][0]) == -1)
                 {
                     datCombos.Rows.RemoveAt(0);

                 }
                 
             }
             return datCombos;
           
        }


        public DataTable Employee(string  CompanyID)
        {
                DataTable dtEmp=new DataTable();
                string[] saFieldValuesCom = { "EmployeeFullName", "EmployeeMaster", "CompanyID=" + CompanyID };
                if (saFieldValuesCom.Length == 3 || saFieldValuesCom.Length == 5)
                {
                    ArrayList prmCommon = new ArrayList();
                    prmCommon.Add(new SqlParameter("@Mode", "0"));
                    prmCommon.Add(new SqlParameter("@Fields", saFieldValuesCom[0]));
                    prmCommon.Add(new SqlParameter("@TableName", saFieldValuesCom[1]));
                    prmCommon.Add(new SqlParameter("@Condition", saFieldValuesCom[2]));
                    dtEmp = ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
                }
            return dtEmp;
        }

        public DataTable FillCombos(string sQuery)
        {
            // function for getting datatable for filling 

            return ExecuteDataTable(sQuery);

        }


        public DataTable GetOffDayMarkDetails(int CompanyId,string Rosterdate,int DepartmentID)
        {
            
            ArrayList ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 1));
            ParameterArray.Add(new SqlParameter("@CompanyID",CompanyId ));
            ParameterArray.Add(new SqlParameter("@RosterDate1", Rosterdate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", DepartmentID));
            return ExecuteDataTable("spPayOffDayMark", ParameterArray);

        }

        public bool SaveDutyRoaster()
        {
           
            ArrayList ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 2));
            ParameterArray.Add(new SqlParameter("@CompanyID", intCompanyID));
            ParameterArray.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", strRosterDate));
            ParameterArray.Add(new SqlParameter("@RosterStatusID", intRosterStatusID));
            ParameterArray.Add(new SqlParameter("@DepartmentID",intDepartmentID));
            ParameterArray.Add(new SqlParameter("@UserID", intUserID));
            ExecuteNonQuery("spPayOffDayMark", ParameterArray);
            return true;
        }
        public void DeleteRoaster()
        {
            ArrayList ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 3));
            ParameterArray.Add(new SqlParameter("@CompanyID", intCompanyID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", strRosterDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", intDepartmentID));
            ExecuteNonQuery("spPayOffDayMark", ParameterArray);
        }


        public DataTable DeleteValid()
        {
            ArrayList ParameterArray = new ArrayList();
            ParameterArray.Add(new SqlParameter("@Mode", 3));
            ParameterArray.Add(new SqlParameter("@CompanyID", intCompanyID));
            ParameterArray.Add(new SqlParameter("@RosterDate1", strRosterDate));
            ParameterArray.Add(new SqlParameter("@DepartmentID", intDepartmentID));
            return ExecuteDataTable("spPayOffDayMark", ParameterArray);
        }
#endregion Functions

}

  

