﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Globalization;

public class clsReportCommon
{


    public static DataTable GetAllCompanies(int CompanyID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 1));
        prmReportDetails.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllBranchesByCompany(int CompanyID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 2));
        prmReportDetails.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmployees(int CompanyID,int BranchID,bool IncludeCompany)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, null, null, null, null, null, null, null, null, false);

    }

    public static DataTable GetEmployeesByLocation(int CompanyID, int BranchID, bool IncludeCompany, int WorkLocationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, null, null, null, null, WorkLocationID, null, null, null, false);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DocumentTypeID, string DocumentNumber)
    {

        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 16));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
         prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        if (DocumentNumber.Trim().ToUpper() == "-1")
        {
        }
        else
            prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID ));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany,int DepartmentID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, null, null, null, null, null, null, null, false);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID,int DesignationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, null, null, null, null, null, null, false);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID,int EmploymentTypeID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, null, null, null, null, null, false);
    }

    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, null, null, null, null, false);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID,bool IncludeSettledEmployees)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, null, true);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID,int WorkLocationID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null, null, false);
    }


    public static DataTable GetAllEmployees(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID, int WorkLocationID,int ProjectID)
    {
        return LoadEmployees(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID, WorkLocationID, null, null,ProjectID,false);
    }

    private static DataTable LoadEmployees(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID,int? WorkLocationID,int? DocumentTypeID,string DocumentNumber,int? ProjectID, bool IncludeSettledEmployees)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 3));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));

        if (DepartmentID != null && DepartmentID > 0)
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        if (DesignationID != null && DesignationID > 0)
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        if (EmploymentTypeID != null && EmploymentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        if (WorkStatusID != null && WorkStatusID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

        if (WorkLocationID != null && WorkLocationID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkLocationID", WorkLocationID));

        if (DocumentTypeID != null && DocumentTypeID  > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID ));

        if (ProjectID != null && ProjectID > 0)
            prmReportDetails.Add(new SqlParameter("@ProjectID", ProjectID));


        if (DocumentNumber != null && DocumentNumber.ToInt32() > 0)
            prmReportDetails.Add(new SqlParameter("@DocumentNumber", DocumentNumber));


        if(IncludeSettledEmployees == true)
            prmReportDetails.Add(new SqlParameter("@LoadSettledEmployees",IncludeSettledEmployees));

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
   



    public static DataTable GetAllDepartments()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@Mode", 5));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDesignation()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 4));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllEmploymentTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 6));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllWorkStatus()
    {
        ArrayList prmReportDetails = new ArrayList();

        prmReportDetails.Add(new SqlParameter("@Mode", 7));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }


    public static DataTable GetAllWorkLocations(int CompanyID, int BranchID, bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 8));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetCompanyHeader(int CompanyID,int BranchID,bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 9));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }


    public static DataTable GetAllDocumentTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 10));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDocumentNumbers(int CompanyID,int BranchID,bool IncludeCompany,int DocumentTypeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 11));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID ));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DocumentTypeID", DocumentTypeID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllDeductions()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 12));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllAssetTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 13));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    public static DataTable GetAllAssets(int AssetTypeID, int CompanyID, int BranchID, bool IncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 14));
        prmReportDetails.Add(new SqlParameter("@BenefitTypeID", AssetTypeID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllFinancialYears(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 32));
        prmReportDetails.Add(new SqlParameter("@CompanyID", intCompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", intBranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", blnIncludeCompany));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetPaymentMonthYear(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 17));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
       return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetAllExpenseTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 18));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllSettlementTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 20));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetMonthForAttendance(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 31));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllAssetStatus(int AssetSearchType)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 33));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@Type", AssetSearchType));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetCompaniesandBranches()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 34));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);

    }

    public static DataTable GetEmployeeForTransfer()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 35));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }
    //Without transfer join fill employees in current working company
    public  static DataTable LoadEmployeesIncurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int? DepartmentID, int? DesignationID, int? EmploymentTypeID, int? WorkStatusID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 36));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@BranchID", BranchID));
        prmReportDetails.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmReportDetails.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));

        if (DepartmentID != null && DepartmentID > 0)
            prmReportDetails.Add(new SqlParameter("@DepartmentID", DepartmentID));
        if (DesignationID != null && DesignationID > 0)
            prmReportDetails.Add(new SqlParameter("@DesignationID", DesignationID));
        if (EmploymentTypeID != null && EmploymentTypeID > 0)
            prmReportDetails.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        if (WorkStatusID != null && WorkStatusID > 0)
            prmReportDetails.Add(new SqlParameter("@WorkStatusID", WorkStatusID));

  

        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllEmployeesInCurrentCompany(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int EmploymentTypeID, int WorkStatusID)
    {
        return LoadEmployeesIncurrentCompany(CompanyID, BranchID, IncludeCompany, DepartmentID, DesignationID, EmploymentTypeID, WorkStatusID);
    }

    public  static  DataTable SetReportHeader(int CompanyId, int BranchId, bool IncludeCompany)
    {
        return  GetCompanyHeader(CompanyId, BranchId, IncludeCompany);
    }

    public void SetReportDisplay(ReportViewer Report)
    {
       // Report.SetDisplayMode(DisplayMode.PrintLayout);
        Report.ZoomMode = ZoomMode.Percent;
        Report.ZoomPercent = 100;

    }

    public void SetDefaultBranch(DropDownList cb)
    {
        cb.SelectedValue = "0";
    }

    public void SetDefaultWorkStatus(DropDownList cb)
    {
        cb.SelectedValue = "6";
    }

    public static DataTable GetPermissions(int RoleID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 37));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@RoleID", RoleID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static string GetMonthStartDate()
    {
        DateTime FirstDay = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
        return FirstDay.ToString("dd MMM yyyy", CultureInfo.InvariantCulture); ;
    }

    public static DataTable GetAllTransactionTypes()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 40));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllProjects()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 41));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetProjects(int EmployeeID,DateTime? ProjectDate,int? Month,int? Year )
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 42));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@EmployeeID", EmployeeID));

        if(ProjectDate != null)
            prmReportDetails.Add(new SqlParameter("@ProjectDate", ProjectDate));
        if (Month >0 && Month   != null) 
        {
            prmReportDetails.Add(new SqlParameter("@Month", Month));
            prmReportDetails.Add(new SqlParameter("@Year", Year));
        }
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllExpenseCategories()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 44));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllExpenseTypeByExpenseCategory(int ExpenseCategoryID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 45));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@ExpenseCategoryID", ExpenseCategoryID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllExpenseNumbers(int ExpenseCategoryID, int ExpenseID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 46));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmReportDetails.Add(new SqlParameter("@ExpenseID", ExpenseID));
        prmReportDetails.Add(new SqlParameter("@ExpenseCategoryID", ExpenseCategoryID));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllCountry()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 47));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllNational()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 49));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static DataTable GetAllReligion()
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 48));
        prmReportDetails.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("SpReportsCommon", prmReportDetails);
    }

    public static bool IsCompanyPermissionExists(int CompanyID)
    {
        ArrayList prmReportDetails = new ArrayList();
        prmReportDetails.Add(new SqlParameter("@Mode", 50));
        prmReportDetails.Add(new SqlParameter("@CompanyID", CompanyID));
        prmReportDetails.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteScalar("SpReportsCommon", prmReportDetails).ToInt32() >0 ;
    }

}

