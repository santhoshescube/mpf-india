﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.IO;

/// <summary>
/// Summary description for clsXML
/// </summary>
/// 
public class clsXML
{
    int iAttributeId;
    string sMessage;


    int iVacancyID;
    string sAnswer1;
    string sAnswer2;
    string sAnswer3;
    string sAnswer4;
    string sCorrectAnswer;


    public int AttributeId
    {
        get { return iAttributeId; }
        set { iAttributeId = value; }
    }
    public string Message
    {
        get { return sMessage; }
        set { sMessage = value; }
    }

    public int VacancyID
    {
        get { return iVacancyID; }
        set { iVacancyID = value; }
    }

    public string Answer1
    {
        get { return sAnswer1; }
        set { sAnswer1 = value; }
    }

    public string Answer2
    {
        get { return sAnswer2; }
        set { sAnswer2 = value; }
    }

    public string Answer3
    {
        get { return sAnswer3; }
        set { sAnswer3 = value; }
    }

    public string Answer4
    {
        get { return sAnswer4; }
        set { sAnswer4 = value; }
    }

    public string  CorrectAnswer
    {
        get { return sCorrectAnswer; }
        set { sCorrectAnswer = value; }
    }

	public clsXML()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string GetTemplateMessage()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));
            XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//Templates[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with partiicular templateid

            XmlNode xnMessage = xnTemplateNode.SelectSingleNode("Message");

            // det the curruspoding message tag
            if (xnMessage != null)
                return xnMessage.InnerText; // retrive the content of messsage tag ie. Temaplate message
            else
                return string.Empty;
        }
        catch (Exception ex)
        {
            return "Template not available";
        }
    }

    public int GetMaxID()
    {
        try
        {
             if (CheckQuestionExistancy() == true)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));

                int MaxID = Convert.ToInt32(xmlDoc.ChildNodes[1].LastChild.Attributes[0].Value)+1 ;
                return MaxID;
            }
            else
                return 1;
        }
        catch (Exception ex)
        {
            return 1;
        }
    }
    public int GetCount()
    {
        try
        {
            if (CheckQuestionExistancy() == true)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));
                int iCount = Convert.ToInt32(xmlDoc.ChildNodes[1].ChildNodes.Count);
                return iCount;
            }
                               
            else
                return 1;
        }
        catch (Exception ex)
        {
            return 1;
        }
    }
    public bool CheckQuestionExistancy()
    {
        try
        {
            if (File.Exists(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml")))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));
                int iCount = Convert.ToInt32(xmlDoc.ChildNodes[1].ChildNodes.Count );
                if (iCount > 0) return true;
                else
                    return false;
            }
            else
                return false ;
        }
        catch (Exception ex)
        {
            return false ;
        }
    }
    public void InsertTemplateXml()
    {
        if (File.Exists(HttpContext.Current.Server.MapPath("~/xml/Templates.xml")))
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));

            XmlElement xeTemplate = xmlDoc.CreateElement("Templates");
            XmlAttribute xaTemplateId;

            xaTemplateId = xmlDoc.CreateAttribute("id");
            xaTemplateId.Value = Convert.ToString(iAttributeId);

            xeTemplate.Attributes.Append(xaTemplateId);

            XmlNode xnTemplateMsg = xmlDoc.CreateElement("Message");
            xnTemplateMsg.AppendChild(xmlDoc.CreateTextNode(sMessage));

            xeTemplate.AppendChild(xnTemplateMsg);
            xmlDoc.SelectSingleNode("TemplatesStore").AppendChild(xeTemplate);

            xmlDoc.Save(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));
        }
        else
        {
            XmlTextWriter xtwTemplates = new XmlTextWriter(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"), System.Text.ASCIIEncoding.UTF8);
            xtwTemplates.WriteStartDocument();

            xtwTemplates.WriteStartElement("TemplatesStore");

            xtwTemplates.WriteStartElement("Templates");
            xtwTemplates.WriteAttributeString("id", Convert.ToString(iAttributeId));

            xtwTemplates.WriteStartElement("Message");
            xtwTemplates.WriteString(sMessage);
            xtwTemplates.WriteEndElement();

            xtwTemplates.WriteEndElement();

            xtwTemplates.WriteEndElement();

            xtwTemplates.WriteEndDocument();

            xtwTemplates.Flush();
            xtwTemplates.Close();
        }
    }

    public void UpdateTemplateXml()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));

            XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//Templates[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with particular templateid

            XmlNode xnMessage = xnTemplateNode.SelectSingleNode("Message"); // det the curruspoding message tag
            xnMessage.InnerText = sMessage;
            xmlDoc.Save(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));
        }
        catch
        {
        }
    }

    public void DeleteTemplateXml()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));

            XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//Templates[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with particular templateid

            if (xnTemplateNode != null)
                xnTemplateNode.ParentNode.RemoveChild(xnTemplateNode);

            xmlDoc.Save(HttpContext.Current.Server.MapPath("~/xml/Templates.xml"));
        }
        catch
        {
        }
    }

    /// <summary>
    /// Function for save offer letter content in xml file
    /// </summary>

    public void AddOfferLetterXml()
    {
        // If file exists append new nodes to the existing files

        if (File.Exists(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml")))
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"));

            XmlNode xnTemplateNode = doc.SelectSingleNode("//OfferLetter[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with particular templateid

            if (xnTemplateNode != null)
                UpdateOfferLetterXml();
            else
            {

                XmlElement xeOfferLetter = doc.CreateElement("OfferLetter");
                XmlAttribute xaId = doc.CreateAttribute("id");

                xaId.Value = Convert.ToString(iAttributeId);
                xeOfferLetter.Attributes.Append(xaId);

                XmlNode xnMessage = doc.CreateElement("Message");
                xnMessage.AppendChild(doc.CreateTextNode(sMessage));

                xeOfferLetter.AppendChild(xnMessage);

                doc.SelectSingleNode("CandidateOffers").AppendChild(xeOfferLetter);

                doc.Save(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"));
            }
        }
        else
        {
            // Creating new xml file with element nodes

            XmlTextWriter xw = new XmlTextWriter(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"), System.Text.ASCIIEncoding.UTF8);

            xw.WriteStartDocument();

            xw.WriteStartElement("CandidateOffers");
            xw.WriteStartElement("OfferLetter");

            xw.WriteAttributeString("id", Convert.ToString(iAttributeId));

            xw.WriteStartElement("Message");
            xw.WriteString(sMessage);

            xw.WriteEndElement();
            xw.WriteEndElement();
            xw.WriteEndElement();

            xw.WriteEndDocument();

            xw.Flush();
            xw.Close();
        }
    }

    public void UpdateOfferLetterXml()
    {
        XmlDocument doc = new XmlDocument();

        doc.Load(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"));

        // Selecting a node by attribute 
        XmlNode xn = doc.SelectSingleNode("//OfferLetter[@id='" + Convert.ToString(iAttributeId) + "']");

        XmlNode innerxn = xn.SelectSingleNode("Message");

        innerxn.InnerText = Message; //Updating existing node value



        doc.Save(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"));
    }

    public string GetOfferLetterXml()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Offerletter.xml"));

            XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//OfferLetter[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with partiicular templateid

            XmlNode xnMessage = xnTemplateNode.SelectSingleNode("Message"); // get the corresponding message tag

            return xnMessage.InnerText; // retrive the content of messsage tag ie. Offer letter content
        }
        catch (Exception ex)
        {
            return "Offer letter not available";
        }
    }
    /// <summary>
    /// Function for save Questions in xml file
    /// </summary>
    /// 

    public void AddQuestionaireXml()
    {
        // If file exists append new nodes to the existing files

        if (File.Exists(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml")))
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));

            XmlElement xeOfferLetter = doc.CreateElement("Questions");
            XmlAttribute xaId = doc.CreateAttribute("id");
         

            xaId.Value = Convert.ToString(iAttributeId);
            xeOfferLetter.Attributes.Append(xaId);

            XmlNode xnVacancyid = doc.CreateElement("Vacancyid");
            xnVacancyid.AppendChild(doc.CreateTextNode(Convert.ToString(iVacancyID)));

            xeOfferLetter.AppendChild(xnVacancyid);
         
            XmlNode xnQuestion = doc.CreateElement("Question");
            xnQuestion.AppendChild(doc.CreateTextNode(sMessage));

            xeOfferLetter.AppendChild(xnQuestion);

            XmlNode xnAnswer1 = doc.CreateElement("Answer1");
            xnAnswer1.AppendChild(doc.CreateTextNode(sAnswer1));

            xeOfferLetter.AppendChild(xnAnswer1);

            XmlNode xnAnswer2 = doc.CreateElement("Answer2");
            xnAnswer2.AppendChild(doc.CreateTextNode(sAnswer2));

            xeOfferLetter.AppendChild(xnAnswer2);

            XmlNode xnAnswer3 = doc.CreateElement("Answer3");
            xnAnswer3.AppendChild(doc.CreateTextNode(sAnswer3));

            xeOfferLetter.AppendChild(xnAnswer3);

            XmlNode xnAnswer4 = doc.CreateElement("Answer4");
            xnAnswer4.AppendChild(doc.CreateTextNode(sAnswer4));

            xeOfferLetter.AppendChild(xnAnswer4);

            XmlNode xnCorrectAnswer = doc.CreateElement("CorrectAnswer");
            xnCorrectAnswer.AppendChild(doc.CreateTextNode(sCorrectAnswer));

            xeOfferLetter.AppendChild(xnCorrectAnswer);

            
            doc.SelectSingleNode("Questionstore").AppendChild(xeOfferLetter);

            doc.Save(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));
        }
        else
        {
            // Creating new xml file with element nodes

            XmlTextWriter xw = new XmlTextWriter(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"), System.Text.ASCIIEncoding.UTF8);

            xw.WriteStartDocument();

            xw.WriteStartElement("Questionstore");
           
            xw.WriteStartElement("Questions");
            xw.WriteAttributeString("id", Convert.ToString(iAttributeId));
            
            xw.WriteStartElement("Vacancyid");
            xw.WriteString(Convert.ToString(iVacancyID));

            xw.WriteEndElement();

            xw.WriteStartElement("Question");
            xw.WriteString(sMessage);

            xw.WriteEndElement();

            xw.WriteStartElement("Answer1");
            xw.WriteString(sAnswer1);

            xw.WriteEndElement();

            xw.WriteStartElement("Answer2");
            xw.WriteString(sAnswer2);

            xw.WriteEndElement();

            xw.WriteStartElement("Answer3");
            xw.WriteString(sAnswer3);

            xw.WriteEndElement();

            xw.WriteStartElement("Answer4");
            xw.WriteString(sAnswer4);

            xw.WriteEndElement();

            xw.WriteStartElement("CorrectAnswer");
            xw.WriteString(Convert.ToString(sCorrectAnswer));

            xw.WriteEndElement();
         
        
            xw.WriteEndDocument();

            xw.Flush();
            xw.Close();
        }
    }
    public void UpdateQuestionaireXml()
    {
        XmlDocument doc = new XmlDocument();

        doc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));

        // Selecting a node by attribute 
        XmlNode xn = doc.SelectSingleNode("//Questions[@id='" + Convert.ToString(iAttributeId) + "']");

        XmlNode innerxnvacancy = xn.SelectSingleNode("Vacancyid");
        innerxnvacancy.InnerText = Convert.ToString(iVacancyID); //Updating existing node value

        XmlNode innerxn = xn.SelectSingleNode("Question");
        innerxn.InnerText = Message ; //Updating existing node value

        XmlNode innerxnan1 = xn.SelectSingleNode("Answer1");
        innerxnan1.InnerText = Answer1 ; //Updating existing node value

        XmlNode innerxnan2 = xn.SelectSingleNode("Answer2");
        innerxnan2.InnerText = Answer2; //Updating existing node value

        XmlNode innerxnan3 = xn.SelectSingleNode("Answer3");
        innerxnan3.InnerText = Answer3; //Updating existing node value

        XmlNode innerxnan4 = xn.SelectSingleNode("Answer4");
        innerxnan4.InnerText = Answer4; //Updating existing node value

        XmlNode innerxcorans = xn.SelectSingleNode("CorrectAnswer");
        innerxcorans.InnerText = CorrectAnswer; //Updating existing node value

        doc.Save(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));
    }
    public void DeleteQuestionaireXml()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));

        XmlNode xnTemplateNode = xmlDoc.SelectSingleNode("//Questions[@id = '" + Convert.ToString(iAttributeId) + "']"); // reteriive node with particular templateid
        xnTemplateNode.ParentNode.RemoveChild(xnTemplateNode);
        xmlDoc.Save(HttpContext.Current.Server.MapPath("~/xml/Questionaire.xml"));
    }
}
