﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsLeaseAgreement
/// Created By:    Sanju
/// Created Date:  22-Oct-2013
/// Description:   Lease Agreement
/// </summary>
public class clsLeaseAgreement : DL
{

    public int intLeaseAgreementID { get; set; }
    public int intCompanyID { get; set; }
    public int intUserID { get; set; }
    public DateTime dteDocumentDate { get; set; }
    public string strAgreementNo { get; set; }
    public string strLessor { get; set; }
    public DateTime dteStartDate { get; set; }
    public DateTime dteEndDate { get; set; }
    public decimal decRentalValue { get; set; }
    public bool blnDepositHeld { get; set; }
    public string strTenant { get; set; }
    public string strContactNumbers { get; set; }
    public string strAddress { get; set; }
    public string strLandLord { get; set; }
    public string strUnitNumber { get; set; }
    public int? intLeaseUnitTypeID { get; set; }
    public string strBuildingNumber { get; set; }
    public string strLocation { get; set; }
    public string strPlotNumber { get; set; }
    public decimal decTotal { get; set; }
    public string strRemarks { get; set; }
    public string strLeasePeriod { get; set; }
    public bool blnIsRenewed { get; set; }
    public Int32 intPageIndex { get; set; }
    public Int32 intPageSize { get; set; }
    public int intNode { get; set; }
    public string strDocname { get; set; }
    public string strFilename { get; set; }
    public string strSearchKey{ get; set; }


    public clsLeaseAgreement()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable SaveLeaseAgreement()
    {
        ArrayList alParameters = new ArrayList();
        if (intLeaseAgreementID > 0)
        {
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", 1));
        }
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@DocumentDate", dteDocumentDate.ToString("dd MMM yyyy")));
        alParameters.Add(new SqlParameter("@AgreementNo", strAgreementNo));
        alParameters.Add(new SqlParameter("@Lessor", strLessor));
        alParameters.Add(new SqlParameter("@StartDate", dteStartDate.ToString("dd MMM yyyy")));
        alParameters.Add(new SqlParameter("@EndDate", dteEndDate.ToString("dd MMM yyyy")));
        alParameters.Add(new SqlParameter("@RentalValue", decRentalValue));
        alParameters.Add(new SqlParameter("@DepositHeld", blnDepositHeld));
        alParameters.Add(new SqlParameter("@Tenant", strTenant));
        alParameters.Add(new SqlParameter("@ContactNumbers", strContactNumbers));
        alParameters.Add(new SqlParameter("@Address", strAddress));
        alParameters.Add(new SqlParameter("@LandLord", strLandLord));
        alParameters.Add(new SqlParameter("@UnitNumber", strUnitNumber));
        alParameters.Add(new SqlParameter("@LeaseUnitTypeID", intLeaseUnitTypeID <= 0 ? null : intLeaseUnitTypeID));
        alParameters.Add(new SqlParameter("@BuildingNumber", strBuildingNumber));
        alParameters.Add(new SqlParameter("@Location", strLocation));
        alParameters.Add(new SqlParameter("@PlotNumber", strPlotNumber));
        alParameters.Add(new SqlParameter("@Total", decTotal));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@LeasePeriod", strLeasePeriod));
        alParameters.Add(new SqlParameter("@IsRenewed", blnIsRenewed));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);

    }

    public DataSet GetReference()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspLeaseAgreement", alParameters);

    }
    public DataTable GetLeaseAgreement()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);

    }
    public DataTable GetMultipleLeaseAgreement(string MultipleAgreementIDs)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@MultipleAgreementID", MultipleAgreementIDs));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);

    }
    public DataSet GetLeaseAgreementToBind()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        alParameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", intPageSize));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@SearchKey", strSearchKey));

        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspLeaseAgreement", alParameters);

    }
    public DataTable GetLeaseAgreementCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);
    }
    public DataTable DeleteLeaseAgreement()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);
    }
    public bool isLeaseAgreementDeletePossible()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        return (ExecuteScalar("HRspLeaseAgreement", alParameters).ToInt32() > 0 ? true : false);
    }
    public DataTable RenewLeaseAgreement()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        return ExecuteDataTable("HRspLeaseAgreement", alParameters);
    }
    public bool isAgreementNumberExist()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        alParameters.Add(new SqlParameter("@AgreementNo", strAgreementNo));
        return (ExecuteScalar("HRspLeaseAgreement", alParameters).ToInt32() > 0 ? true : false);
    }

    public DataSet GetLeaseAgreementDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        return ExecuteDataSet("HRspLeaseAgreement", alParameters);
    }
    public void DeleteLeaseAgreementDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@Node", intNode));
        ExecuteNonQuery("HRspLeaseAgreement", alParameters);
    }
    public DataTable InsertLeaseAgreementTreemaster()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
        alParameters.Add(new SqlParameter("@AgreementNo", strAgreementNo));
        alParameters.Add(new SqlParameter("@Docname", strDocname));
        alParameters.Add(new SqlParameter("@Filename", strFilename));

        return ExecuteDataTable("HRspLeaseAgreement", alParameters);

    }

    //public DataTable UpdateLeaseAgreementTreemaster()
    //{
    //    ArrayList alParameters = new ArrayList();

    //    alParameters.Add(new SqlParameter("@Mode", 15));
    //    alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));
    //    alParameters.Add(new SqlParameter("@AgreementNo", strAgreementNo));
    //    alParameters.Add(new SqlParameter("@Docname", strDocname));
    //    alParameters.Add(new SqlParameter("@Filename", strFilename));

    //    return ExecuteDataTable("HRspLeaseAgreement", alParameters);

    //}

    public DataTable GetLeaseAgreementFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@LeaseAgreementID", intLeaseAgreementID));

        return ExecuteDataTable("HRspLeaseAgreement", alParameters);
    }

    public string GetLeaseAgreementPrint()
    {
        DataTable datData = GetLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Lease Agreement", 1);
        }
        return "";

    }
    public string GetMultipleLeaseAgreementPrint(string MultipleAgreementIDs)
    {
        DataTable datData = GetMultipleLeaseAgreement(MultipleAgreementIDs);
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Lease Agreement", 2);
        }
        return "";

    }

    private string GetDocumentHtml(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        //string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        //string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";
        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
        //string strHeaderRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";


        //string strRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:rgb(232, 238, 240)'";
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";

        //string strRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        //string strRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        //string strRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        //string strAltRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        //string strRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; olor:#000000''";
        //string strAltRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        //string strRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        //string strAltRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">Company : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">Lease Agreement Details</div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Agreement Number</div><div " + strAltRowInnerRightStyle + ">" + DrData["AgreementNo"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Lessor</div><div " + strAltRowInnerRightStyle + ">" + DrData["Lessor"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Start Date</div><div " + strAltRowInnerRightStyle + ">" + DrData["StartDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">End Date</div><div " + strAltRowInnerRightStyle + ">" + DrData["EndDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Document Date</div><div " + strAltRowInnerRightStyle + ">" + DrData["DocumentDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Lease Period</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeasePeriod"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Rental value</div><div " + strAltRowInnerRightStyle + ">" + DrData["RentalValue"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Deposit Held</div><div " + strAltRowInnerRightStyle + ">" + DrData["DepositHeld"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Tenant</div><div " + strAltRowInnerRightStyle + ">" + DrData["Tenant"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Contact Numbers</div><div " + strAltRowInnerRightStyle + ">" + DrData["ContactNumbers"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Address</div><div " + strAltRowInnerRightStyle + ">" + DrData["Address"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Land Lord</div><div " + strAltRowInnerRightStyle + ">" + DrData["Landlord"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Unit Number</div><div " + strAltRowInnerRightStyle + ">" + DrData["UnitNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Lease UnitType</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeaseUnitType"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Building Number</div><div " + strAltRowInnerRightStyle + ">" + DrData["BuildingNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Location</div><div " + strAltRowInnerRightStyle + ">" + DrData["Location"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Plot Number</div><div " + strAltRowInnerRightStyle + ">" + DrData["PlotNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Total</div><div " + strAltRowInnerRightStyle + ">" + DrData["Total"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Renewed</div><div " + strAltRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Remarks</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">Company</div> <div " + strHeaderRowInner10Style + ">Agreement Number</div><div " + strHeaderRowInner10Style + ">Lessor</div> <div " + strHeaderRowInner10Style + ">Tenant</div><div " + strHeaderRowInner10Style + ">Rental Value</div><div " + strHeaderRowInner10Style + ">Contact Numbers</div><div " + strHeaderRowInner10Style + ">From date</div><div " + strHeaderRowInner10Style + ">To date</div><div " + strHeaderRowInner10Style + ">Lease Period</div><div " + strHeaderRowInner10Style + ">Document date</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["AgreementNo"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Tenant"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Lessor"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["RentalValue"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ContactNumbers"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["StartDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["EndDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["LeasePeriod"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["DocumentDate"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }

}
