﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic ; 

using System.Text;

/// <summary>
/// Summary description for clsUserMaster
/// purpose : Handle Users
/// created : Binoj
/// Date    : 22.07.2010
/// </summary>

public class clsUserMaster : DL
{
   
    private int iUserID;
    private int iRoleID;
    private int intCompanyID;
    private string sUserName;
    private string sPassword;
    private int iEmployeeID;
    private bool bRemember;
    private string sEmailID;
    private bool bSendSms;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private string sSearchKey;

    private string sEmployeeFullName;


    public List<UserCompanyRoles> UserCompanyRoleList;


    public clsUserMaster()
    {



        UserCompanyRoleList = new List<UserCompanyRoles>();
    }

    public int UserID
    {
        get { return iUserID; }
        set { iUserID = value; }
    }
    public int CmpID
    {
        get { return intCompanyID; }
        set { intCompanyID = value; }
    }
    public int CompanyID { get; set; }

    public int RoleID
    {
        get { return iRoleID; }
        set { iRoleID = value; }
    }

    public string UserName
    {
        get { return sUserName; }
        set { sUserName = value; }
    }

    public string Password
    {
        get { return sPassword; }
        set { sPassword = value; }
    }

    public int EmployeeID
    {
        get { return iEmployeeID; }
        set { iEmployeeID = value; }
    }

    public bool Remember
    {
        get { return bRemember; }
        set { bRemember = value; }
    }

    public string EmailID
    {
        get { return sEmailID; }
        set { sEmailID = value; }
    }

    public bool SendSms
    {
        get { return bSendSms; }
        set { bSendSms = value; }
    }

    public string EmployeeFullName
    {
        get { return sEmployeeFullName; }
        set { sEmployeeFullName = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public string SearchKey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }


  
    public SqlDataReader IsExists(bool insert)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IE"));
        alParameters.Add(new SqlParameter("@UserName", sUserName));
        alParameters.Add(new SqlParameter("@Password", sPassword));

        return ExecuteReader("spUserMaster", alParameters);
    }
    public int GetCompanyId()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return Convert.ToInt32(userData[3]);
    }




    public static String GetCulture()
    {
        try
        {
            FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;       

            if (id == null)
                return "";

            FormsAuthenticationTicket ticket = id.Ticket;
            string[] userData = ticket.UserData.Split(',');
            return userData[4].ToString();
        }
        catch(Exception)
        {
            return "";
        }
        

    }

    public int GetEmployeeId()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return Convert.ToInt32(userData[2]);
    }

    public DataTable GetUserCompanyDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@CompanyId", CmpID));
        

        return ExecuteDataTable("HRSpGetUserCompanyDetails", alParameters);
    }
    public bool CheckEmployeeRole()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@UserID", UserID ));

        return Convert.ToBoolean(ExecuteScalar("HRSpGetUserRoles", alParameters));
    
    }

    public static DataTable GetUserCompanyDetail()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@CompanyId", new clsUserMaster().GetCompanyId()));

        return new DataLayer().ExecuteDataTable("HRSpGetUserCompanyDetails", alParameters);
    }


    public string GetEmployeeFullName()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEFN"));
        alParameters.Add(new SqlParameter("@EmployeeID", GetEmployeeId()));
        return ExecuteScalar("HRspUserMaster", alParameters).ToString();
    }



    public static string GetEmployeeFullName(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEFN"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspUserMaster", alParameters).ToString();
    }




    public static string GetFromEmailID()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEFE"));
        alParameters.Add(new SqlParameter("@EmployeeID", new clsUserMaster().GetEmployeeId()));
        return new DataLayer().ExecuteScalar("HRspUserMaster", alParameters).ToString();
    }


    public static string GetEmployeeEmailID(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEFE"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspUserMaster", alParameters).ToString();
    }
    




    public static  string GetcurrentDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GCD"));
        return new DataLayer().ExecuteScalar("HRspUserMaster", alParameters).ToString();
    }        
    













    public int GetUserId()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return Convert.ToInt32(userData[1]);
    }

    public int GetRoleId()
    {
        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

        if (id == null)
            return -1;

        FormsAuthenticationTicket ticket = id.Ticket;
        string[] userData = ticket.UserData.Split(',');
        return Convert.ToInt32(userData[0]);
    }

    public SqlDataReader IsExistUserName()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "Gu"));
        alParameters.Add(new SqlParameter("@UserName", sUserName));
        alParameters.Add(new SqlParameter("Password", sPassword));

        return ExecuteReader("HRspUserMaster", alParameters);
    }


    public DataSet FillAllCombos()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FAC"));
        return ExecuteDataSet("HRspUserMaster", alParameters);
    }

    public DataTable  FillComboCountry()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FCC"));
        return ExecuteDataTable("HRspUserMaster", alParameters);
    }

    public DataTable FillComboNationality()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FCN"));
        return ExecuteDataTable("HRspUserMaster", alParameters);
    }

    public DataTable FillComboEthnicOrigin()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FCE"));
        return ExecuteDataTable("HRspUserMaster", alParameters);
    }


    public DataTable FillComboReligion()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FCR"));
        return ExecuteDataTable("HRspUserMaster", alParameters);
    }


    public DataTable FillComboDegree()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FCD"));
        return ExecuteDataTable("HRspUserMaster", alParameters);
    }


    public int InsertUserDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("RoleID", iRoleID));
        alparameters.Add(new SqlParameter("UserName", sUserName));
        alparameters.Add(new SqlParameter("Password", clsCommon.Encrypt(sPassword, ConfigurationManager.AppSettings["_ENCRYPTION"])));
        alparameters.Add(new SqlParameter("EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("SendSms", bSendSms));
        return Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
    }



    public static clsUserMaster GetUserDetails(int EmployeeID)
    {
        DataSet ds = null;
        DataTable dtUserPrimaryDetails = null;
        DataTable dtUserCompanyRoleDetails = null;
        int ID = 0;
        List<UserCompanyRoles> UserCompanyRoleList = new List<UserCompanyRoles>();

        clsUserMaster u = new clsUserMaster();

        ArrayList Param = new ArrayList();
        Param.Add(new SqlParameter("@Mode", "GUD"));
        Param.Add(new SqlParameter("@EmployeeID", EmployeeID));

        ds = new DataLayer().ExecuteDataSet("HRspUserMaster", Param);

        dtUserPrimaryDetails = ds.Tables[0];
        dtUserCompanyRoleDetails = ds.Tables[1];

        if (dtUserPrimaryDetails.Rows.Count > 0)
        {
            u.CompanyID = dtUserPrimaryDetails.Rows[0]["CompanyID"].ToInt32();
            u.UserName = dtUserPrimaryDetails.Rows[0]["UserName"].ToString();
            u.Password = clsCommon.Decrypt(dtUserPrimaryDetails.Rows[0]["Password"].ToString(), ConfigurationManager.AppSettings["_ENCRYPTION"]);
            u.UserID = dtUserPrimaryDetails.Rows[0]["UserID"].ToInt32();
            u.EmployeeID = dtUserPrimaryDetails.Rows[0]["EmployeeID"].ToInt32();
        }

        if (dtUserCompanyRoleDetails.Rows.Count > 0)
        {

            foreach (DataRow dr in dtUserCompanyRoleDetails.Rows)
            {
                ID = ID + 1;
                UserCompanyRoleList.Add(new UserCompanyRoles()
                {
                    ID = ID,
                    CompanyID = dr["CompanyId"].ToInt32(),
                    RoleID = dr["RoleID"].ToInt32(),
                    CurrentCompany = dr["CurrentCompany"].ToInt32()
                });
            }
        }

        u.UserCompanyRoleList = UserCompanyRoleList;
        return u;

    }


    public static bool IsUserNameExists(string UserName,int UserID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "IUV"));
        alparameters.Add(new SqlParameter("UserName", UserName));
        alparameters.Add(new SqlParameter("@UserID", UserID));
        return new DataLayer().ExecuteScalar("HRspUserMaster", alparameters).ToInt32() > 0;
    }
    
    public int SaveUser()
    {
         int Roleid=0;
        try
        {
            this.BeginTransaction("HRspUserMaster");
            //Delete exising details
            ArrayList Param = new ArrayList();
            Param.Add(new SqlParameter("@Mode", "Dr"));
            Param.Add(new SqlParameter("@UserID", UserID));
            ExecuteScalar("HRspUserMaster", Param);

            //Insert to user master table
            ArrayList alparameters = new ArrayList();
            alparameters.Add(new SqlParameter("@Mode", "IU"));
            alparameters.Add(new SqlParameter("UserID", UserID));
            alparameters.Add(new SqlParameter("UserName", UserName));
            alparameters.Add(new SqlParameter("Password", clsCommon.Encrypt(Password, ConfigurationManager.AppSettings["_ENCRYPTION"])));
            alparameters.Add(new SqlParameter("EmployeeID", EmployeeID));
            if (UserID == 0)
            {
                Roleid = 1;
                alparameters.Add(new SqlParameter("@RoleID", Roleid));
            }
            UserID = Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));

            //Insert to user -company wise role details
            

            foreach (UserCompanyRoles r in UserCompanyRoleList)
            {
                ArrayList alparameters1 = new ArrayList();
                alparameters1.Add(new SqlParameter("@Mode", "IUR"));
                alparameters1.Add(new SqlParameter("@UserID", UserID));
                alparameters1.Add(new SqlParameter("@CompanyID", r.CompanyID));
                //alparameters1.Add(new SqlParameter("@CompanyID", r.CMID));
                alparameters1.Add(new SqlParameter("@RoleID", r.RoleID));
                int Retval = Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters1));
            }
            this.CommitTransaction();
            return UserID;
        }
        catch (Exception ex)
        {
            this.RollbackTransaction();
            return 0;
        }
    }

    
    public int UpdateUserDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("UserID", iUserID));
        alparameters.Add(new SqlParameter("RoleID", iRoleID));
        alparameters.Add(new SqlParameter("UserName", sUserName));
        alparameters.Add(new SqlParameter("Password", clsCommon.Encrypt(sPassword, ConfigurationManager.AppSettings["_ENCRYPTION"])));
        alparameters.Add(new SqlParameter("EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("SendSms", bSendSms));
        return Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
    }
    public void DeleteUserDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("UserID", iUserID));
         ExecuteNonQuery("HRspUserMaster", alparameters);
    }
    public DataSet FillEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FE"));
        alparameters.Add(new SqlParameter("UserID", iUserID));
        return ExecuteDataSet("HRspUserMaster", alparameters);
    }


    //Added by rajesh
    public static  DataSet FillAllEmployees(int CompanyID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FAE"));
        alparameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        //alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataSet("HRspUserMaster", alparameters);
    }

    public static  DataSet FillAllCompanies(int  UserId)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FACE"));
        alparameters.Add(new SqlParameter("@UserID", UserId));
        return new DataLayer(). ExecuteDataSet("HRspUserMaster", alparameters);
    }



    public static  DataSet FillAllRoles()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "FR"));
        return new DataLayer().ExecuteDataSet("HRspUserMaster", alparameters);
    }

    //End 





    public DataSet FillRoles()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FR"));
        return ExecuteDataSet("HRspUserMaster", alparameters);
    }
    public DataSet FillDataSet()
    {
      
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alparameters.Add(new SqlParameter("@PageSize", PageSize));
        alparameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alparameters.Add(new SqlParameter("@UserID", iUserID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        return ExecuteDataSet("HRspUserMaster", alparameters);
    }
    public bool CheckUserName()/* for user settings */
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CHK"));
        alparameters.Add(new SqlParameter("UserName", sUserName));
        int count = Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }
    public bool CheckUserNameExist()/* for user settings */
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CHECK"));
        alparameters.Add(new SqlParameter("UserName", sUserName));
        alparameters.Add(new SqlParameter("UserID", iUserID));
        int count = Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }


    public DataTable GetRoleCount(bool lnAddStatus)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@RoleID", iRoleID));
        if (lnAddStatus == true)
        {
            alParameters.Add(new SqlParameter("@CategoryFlg", 1));
        }
        else
        {
            alParameters.Add(new SqlParameter("@CategoryFlg", 0));
            alParameters.Add(new SqlParameter("@UserID", iUserID));
        }
        return ExecuteDataTable("spUser", alParameters);
    }


    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));
        alparameters.Add(new SqlParameter("UserID", iUserID));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alparameters.Add(new SqlParameter("CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
    }

    public void UpdatePassword()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "UP"));
        alParameters.Add(new SqlParameter("@Password",clsCommon.Encrypt(sPassword, ConfigurationManager.AppSettings["_ENCRYPTION"])));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        ExecuteNonQuery("HRspUserMaster", alParameters);
    }

    public DataTable GetEmployeeCompany()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeID));

        return ExecuteDataTable("HRspHolidays", alParameters);
    }


    public static int GetEmployeeCompanyID(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GECID"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));

        return new DataLayer().ExecuteScalar("HRspUserMaster", alParameters).ToInt32();
    }
    public bool UserExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DCHK"));
        alparameters.Add(new SqlParameter("UserID", iUserID));
        int count = Convert.ToInt32(ExecuteScalar("HRspUserMaster", alparameters));
        if (count > 0)
            return true;
        else
            return false;

    }

    //set role when change company
    public DataTable GetEmployeeCompanyRole()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID ));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeID));

        return ExecuteDataTable("HRspGetCompanyRole", alParameters);
    }

    //-------------------------------Code to be replaced------------------------

    public string GetPrintText(int iSalID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@SalaryID", iSalID));

        DataTable dt = ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters).Tables[0];

        ArrayList alParameters1 = new ArrayList();

        alParameters1.Add(new SqlParameter("@Mode", "GSD"));
        alParameters1.Add(new SqlParameter("@SalaryID", iSalID));


        DataTable dtsalaryDet = ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters1).Tables[0];

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Salary Structure Information</td></tr>");
        sb.Append("<tr><td width='150px'>Payment Mode</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Payclassification"]) + "</td></tr>");
        sb.Append("<tr><td>Payment Calculation</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Paycalculation"]) + "</td></tr>");
        sb.Append("<tr><td>Currency</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Currency"]) + "</td></tr>");
        //sb.Append("<tr><td>Basic Pay</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["BasicPay"]) + "</td></tr>");
        sb.Append("<tr><td>Absent Policy</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["AbsentPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>Encash Policy</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["EncashPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>Holiday Policy</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["HolidayPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>Overtime Policy</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["OTPolicy"]) + "</td></tr>");


        if (dtsalaryDet.Rows.Count > 0)
        {
            sb.Append("<tr><td width='150px' valign='top'>Additon/Deduction Details</td><td valign='top'>:</td>");
            sb.Append("<td>");
            sb.Append("<table border='0' cellspacing='0' cellpadding='0' style='font-size:11px;'>");
            sb.Append("<tr><td width='150'><b>Particulars</b></td><td width='150'><b>Category</b></td><td width='150'><b>Amount</b></td><td width='150'><b> Deduction Policy</b></td></tr>");
            for (int i = 0; i < dtsalaryDet.Rows.Count; i++)
            {
                sb.Append("<tr><td>" + Convert.ToString(dtsalaryDet.Rows[i]["AddDed"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["Category"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["Amount"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["payPolicy"]) + "</td></tr>");
            }
            sb.Append("</table>");
        }
        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();        
    }
    public string CreateSelectedEmployeesContent(string sSalaryIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@SalaryIds", sSalaryIds));

        DataTable dt = ExecuteDataTable("HRspEmployeeSalaryStructure", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Salary Structure Information</td></tr>");
        sb.Append("<tr><td>");
        //sb.Append("<table width='100%' style='font: 11px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee</td><td width='150px'>Currency</td><td width='150px'>Payment Mode</td><td width='100px'>Payment Calculation</td></tr>");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td width='150px'>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Currency"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Payclassification"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Paycalculation"]) + "</td></tr>");
            //sb.Append("<td>" + Convert.ToString(dt.Rows[i]["BasicPay"]) + "</td> </tr>");
 
        }
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

   

}

[Serializable]
public class UserCompanyRoles
{
    public int ID { get; set; }

    public int CompanyID { get; set; }
    public int CMID { get; set; }
    public int RoleID { get; set; }
    public int CurrentCompany { get; set; }
}


