﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/* Created By   :   Sruthy K
 * Created Date :   13 Oct 2013
 * Purpose      :   Sales Performance
 * */
public class clsSalesPerformance
{
    #region Properties

    public int PerformanceID { get; set; }
    public int CompanyID { get; set; }
    public long EmployeeID { get; set; }
    public decimal Projection { get; set; }
    public decimal Actual { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public int DepartmentID { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public List<clsSalesPerformance> lstPerformance;
     
    #endregion

    public clsSalesPerformance()
	{
        lstPerformance = new List<clsSalesPerformance>();
	}

    public static DataTable GetAllDepartments()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 1));
        Parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }

    public DataTable GetEmployees()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 2));
        Parameters.Add(new SqlParameter("@DepartmentID",DepartmentID));
        Parameters.Add(new SqlParameter("@PageIndex",PageIndex));
        Parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        Parameters.Add(new SqlParameter("@PageSize",PageSize));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }

    public static int GetTotalRecords(int DepartmentID,long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 3));
        Parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspSalesPerformance", Parameters).ToInt32();
    }

    public static DataTable GetPerformanceDetails(long EmployeeID,DateTime FromDate)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 4));
        Parameters.Add(new SqlParameter("@EmployeeID",EmployeeID));
        Parameters.Add(new SqlParameter("@FromDate", FromDate));
        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }

    public bool Delete()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 5));
        Parameters.Add(new SqlParameter("@XmlPerformance", lstPerformance.ToXml()));
        return new DataLayer().ExecuteNonQuery("HRspSalesPerformance", Parameters) > 0;
    }

    public bool Save()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 6));
        Parameters.Add(new SqlParameter("@XmlPerformance", lstPerformance.ToXml()));
        return new DataLayer().ExecuteScalar("HRspSalesPerformance", Parameters).ToInt32() > 0;
    }

    public DataTable GetDetailsForChart()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 7));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@FromDate", FromDate));
        Parameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }
    public static DataTable GetAllEmployees(int DepartmentID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 8));
        Parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
       
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));

        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }
    public static DataTable GetAllEmployees(int DepartmentID, int CompanyID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 8));
        Parameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        Parameters.Add(new SqlParameter("@ISArabic", clsGlobalization.IsArabicCulture()));

        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }

    public static DataTable GetBarChart(long EmployeeID)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 9));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("HRspSalesPerformance", Parameters);
    }


}
