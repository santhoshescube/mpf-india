﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsBenefits
/// Created By : Ratheesh
/// 
/// </summary>
public class clsBenefits : DL
{
	public clsBenefits() { }

    public int EmployeeBenefitID { get; set; }
    public int BenefitTypeID { get; set; }
    public int EmployeeID { get; set; }
    public decimal BenefitAmount { get; set; }
    public int BenefitDetailID { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }

    public string EmployeeBenefitIDs { get; set; }

    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string SortOrder { get; set; }
    public string SortExpression { get; set; }
    public string SearchKey { get; set; }
    public int UserID { get; set; }
    public int CompanyID { get; set; }

    public enum Mode { View = 1, Edit = 2}

    /// <summary>
    /// Returns employees to fill dropdown.
    /// </summary>
    /// <returns></returns>
    public DataSet GetEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GE"));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyID));
        return ExecuteDataSet("HRspEmployeeBenefits", alParameters);
    }

    public DataSet GetAllEmployeeBenefits()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAEB"));

        alParameters.Add(new SqlParameter("@PageIndex", this.PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", this.PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", this.SearchKey));
        alParameters.Add(new SqlParameter("@SortOrder", this.SortOrder));
        alParameters.Add(new SqlParameter("@SortExpression", this.SortExpression));
        alParameters.Add(new SqlParameter("@EmployeeID", this.EmployeeID));
        alParameters.Add(new SqlParameter("@UserId", this.UserID));
        return ExecuteDataSet("HRspEmployeeBenefits", alParameters);
    }

    public bool IsBenefitExists()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CAE"));
        alParameters.Add(new SqlParameter("EmployeeID", this.EmployeeID));

        int Count = Convert.ToInt32(ExecuteScalar("HRspEmployeeBenefits", alParameters));
        
        return Count > 0;
    }

    public bool Delete()
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "DEL"));
            alParameters.Add(new SqlParameter("EmployeeBenefitID", this.EmployeeBenefitID));

            ExecuteNonQuery("HRspEmployeeBenefits", alParameters);
            
            return true;
        }
        catch(Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
            return false;
        }
    }   

    public DataSet GetBenefitTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBT"));
        alParameters.Add(new SqlParameter("@EmployeeID", this.EmployeeID));

        return ExecuteDataSet("HRspEmployeeBenefits", alParameters);
    }

    public int Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "INS"));
        alParameters.Add(new SqlParameter("@EmployeeID", this.EmployeeID));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }


    public void DeleteUserMessage(int ReferenceId, int ReferenceTypeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL-UM"));
        alParameters.Add(new SqlParameter("@EmployeeID", this.EmployeeID));
        alParameters.Add(new SqlParameter("@ReferenceId", ReferenceId));
        alParameters.Add(new SqlParameter("@ReferenceTypeId", ReferenceTypeId));
         ExecuteScalar("HRspEmployeeBenefits", alParameters);
    }

    public int InsertDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "INSD"));
        alParameters.Add(new SqlParameter("@EmployeeBenefitID", this.EmployeeBenefitID));
        alParameters.Add(new SqlParameter("@BenefitTypeID", this.BenefitTypeID));
        alParameters.Add(new SqlParameter("@BenefitAmount", this.BenefitAmount));
        alParameters.Add(new SqlParameter("@FromDate", this.FromDate));
        alParameters.Add(new SqlParameter("@ToDate", this.ToDate));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public int UpdateDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UPDD"));
        alParameters.Add(new SqlParameter("@EmployeeBenefitID", this.EmployeeBenefitID));
        alParameters.Add(new SqlParameter("@BenefitTypeID", this.BenefitTypeID));
        alParameters.Add(new SqlParameter("@BenefitAmount", this.BenefitAmount));
        alParameters.Add(new SqlParameter("@BenefitDetailID", this.BenefitDetailID));
        alParameters.Add(new SqlParameter("@FromDate", this.FromDate));
        alParameters.Add(new SqlParameter("@ToDate", this.ToDate));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public int DeleteDetailByID()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DELD"));

        alParameters.Add(new SqlParameter("@BenefitDetailID", this.BenefitDetailID));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public DataSet GetBenefitByID(Mode mode)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", mode == Mode.Edit ? "GBID" : "GBIDV"));
        alParameters.Add(new SqlParameter("@EmployeeBenefitID", this.EmployeeBenefitID));

        return ExecuteDataSet("HRspEmployeeBenefits" ,alParameters);
    }

    public string CreateSingleBenefitContent()
    {
        DataSet ds = this.GetBenefitByID(Mode.View);

        StringBuilder sb = new StringBuilder();

        string sOuterBorder = string.Empty; //"border:solid 1px #000000;";

        if (ds.Tables.Count > 0)
        {
            DataTable dt = ds.Tables[0];

            string sFontWeight = "font-weight:normal";

            sb.Append("<div style='" + sOuterBorder + " padding:10px; margin:5px;'>");
            sb.Append("<center> <h5 style='margin:0; padding:0; text-transform:uppercase;'>");
            sb.Append("Employee Benefit Details</h5> </center>");
            sb.Append("<hr style='border:solic 1px #CCCCCC;'/>");
            sb.Append("<div style='margin-left:10px; padding:5px 0;'>"); // apply 10px margin

            sb.Append("<table><tr>");
            sb.Append("<td style='font-size:12px;'>Employee : </td>");
            sb.Append("<td style='font-size:12px;'>" + dt.Rows[0]["EmployeeName"] + "</td>");
            sb.Append("</tr><table>");

            if (ds.Tables.Count == 2 && ds.Tables[1].Rows.Count > 0)
            {
                sb.Append("</br>");
                sb.Append("<span style='font-size:12px; font-weight:bold'> Benefits </span>");
                sb.Append("</br> </br>");
                sb.Append("<table style='border:solid 1px #F0F0F0; border-collapse:collapse; width:90%; font-size:11px;' cellpadding='4'>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Type </td>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Amount" + " " + dt.Rows[0]["Currency"]+ "</td>");
                sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> Duration </td>");
                sb.Append("</tr>");

                foreach (DataRow Row in ds.Tables[1].Rows)
                {
                    sb.Append("<tr> <td style='border:solid 1px #F0F0F0;'> " + Row["Description"] + "</td>");
                    sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["BenefitAmount"]  + "</td>");
                    sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["FromDate"] + " - " + Row["ToDate"] + "</td>");
                    sb.Append("</tr>");
                }

                sb.Append("</table>");
            }

            sb.Append("</div>");
            sb.Append("</div>"); // outer container

        }
        return sb.ToString();
    }

    public string CreateSelectedBenefitContent()
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GPT"));
        alparameters.Add(new SqlParameter("@EmployeeBenefitIDs", this.EmployeeBenefitIDs));

        DataTable dt = ExecuteDataTable("HRspEmployeeBenefits", alparameters);

        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px; text-transform:uppercase;' align='center'>Employee Benefits</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table style='font-size:11px;font-family:Tahoma; width:100%;' border='0px'>");
        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'>");
        // table header
        sb.Append("<td>Employee </td>");
        sb.Append("<td>Total Benefit </td>");

        sb.Append("<tr><td colspan='2'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["EmployeeName"]) + "</td>");
            sb.Append("<td>"  + dt.Rows[i]["Currency"]+ "  " + Convert.ToString(dt.Rows[i]["TotalAmount"]) + "</td></tr>");
        }
        sb.Append("</table> </td> </tr>");//</td></tr>
        sb.Append("</table>");

        return sb.ToString();
    }

    public string GetPrintText(bool PrintSingleBenefit)
    {
        StringBuilder sb = new StringBuilder();

        if (PrintSingleBenefit)
        {
            sb.Append(CreateSingleBenefitContent());
        }
        else
        {
            // multiple benefits
            sb.Append(CreateSelectedBenefitContent());
        }

        return sb.ToString();
    }

    public string GetCurrency()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GC"));
        alParameters.Add(new SqlParameter("@EmployeeID", this.EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspEmployeeBenefits", alParameters));
    }

    public void Begin()
    {
        BeginTransaction("HRspEmployeeBenefits");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }
}
