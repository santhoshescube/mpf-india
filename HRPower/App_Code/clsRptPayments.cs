﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


    public class clsRptPayments
    {
        public clsRptPayments() { }

        public static DataTable GetMonthlyPayments(int CompanyID, int BranchID, int DepartmentID,int DesignationID,int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany,int TransactionTypeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                  new SqlParameter("@TransactionTypeID",TransactionTypeID),
                new SqlParameter("@Year", Year),
                   new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@WorkStatusID", WorkStatusID),
               new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        

                  
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

        public static DataTable GetSummaryPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int TransactionTypeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@TransactionTypeID",TransactionTypeID),
                new SqlParameter("@FromDate", FromDate.AddDays(-FromDate.Day+1)),
                new SqlParameter("@ToDate", ToDate.AddMonths(1).AddDays(-ToDate.Day)),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }



        public static DataTable GetProjectPayments(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate, bool IncludeCompany, int ProjectID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@FromDate", FromDate.AddDays(-FromDate.Day+1)),
                new SqlParameter("@ToDate", ToDate.AddMonths(1).AddDays(-ToDate.Day)),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@ProjectID", ProjectID),
                new SqlParameter("@UserID",new clsUserMaster().GetUserId())
            };
            return new DataLayer().ExecuteDataTable("spPayRptEmployeeProjectExpense", sqlParameters);
        }

        public static DataTable GetMonthlyPaymentsProcessed(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year, bool IncludeCompany, int TransactionTypeID, bool IsPayStructureOnly, string FromDate, string ToDate, int Istransfer)
        {


            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@Month", Month),
                new SqlParameter("@Year", Year),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@DesignationID", DesignationID),
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()),
                new SqlParameter("@WorkStatusID", WorkStatusID),
                new SqlParameter("@TransactionTypeID", TransactionTypeID),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId()),
                new SqlParameter("@FromDate", FromDate),
                new SqlParameter("@ToDate", ToDate),
                new SqlParameter("@IsReleased", 0),
                new SqlParameter("@IsPayStructureOnly", IsPayStructureOnly),
                new SqlParameter("@Istransfer", Istransfer),
            };
            return new DataLayer().ExecuteDataTable("spPayRptPayments", sqlParameters);
        }

    }
