﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsConfiguration
/// </summary>
public class clsConfiguration:DL
{
    public enum ConfigurationItems
    {
        ReportingTime,
        DefaultProbationPeriod,
        Defaultperformancereview,
        ActivityAlertPeriod,
        WelcomeText,
        ReportFooter,
        FromMail,
        MyAttendance,
        CandidateCodePrefix,
        MultiCurrencyMode,
        WeekStartDay,
        SalaryDayIsEditable,
        JobOpeningsEnabled,
        ViewMyDocumentsEnabled,
        DisplayLeaveSummaryInSalarySlip
        //,ArabicViewEnabled
    }

	public clsConfiguration()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iConfigurationID;
    private string sConfigurationItem;
    private string sConfigurationValue;
    private bool bPredefined;
    private int iValueLimt;
    private string sDefaultValue;
    private int iProductId;

    public int ConfigurationID
    {
        get { return iConfigurationID; }
        set { iConfigurationID = value; }
    }

    public string ConfigurationItem
    {
        get { return sConfigurationItem; }
        set { sConfigurationItem = value; }
    }

    public string ConfigurationValue
    {
        get { return sConfigurationValue; }
        set { sConfigurationValue = value; }
    }

    public bool Predefined
    {
        get { return bPredefined; }
        set { bPredefined = value; }
    }

    public int ValueLimt
    {
        get { return iValueLimt; }
        set { iValueLimt = value; }
    }

    public string DefaultValue
    {
        get { return sDefaultValue; }
        set { sDefaultValue = value; }
    }
    public int ProductId
    {
        get { return iProductId; }
        set { iProductId = value; }
    }

    public DataSet FillDataset()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspConfigurationMaster", alparameters);
    }

    public void UpdateConfiguration()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@ConfigurationID", iConfigurationID));
        alparameters.Add(new SqlParameter("@ConfigurationValue", sConfigurationValue));
        ExecuteNonQuery("HRspConfigurationMaster", alparameters);
    }
    public void ResetConfiguration()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "R"));
        alparameters.Add(new SqlParameter("@ConfigurationID", iConfigurationID));
        ExecuteNonQuery("HRspConfigurationMaster", alparameters);
    }

    public string GetConfiguration(ConfigurationItems item)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GC"));
        alparameters.Add(new SqlParameter("@ConfigurationItem", item.ToString()));
        alparameters.Add(new SqlParameter("@ProductId", 3));

        return Convert.ToString(ExecuteScalar("HRspConfigurationMaster", alparameters));
    }

    public string GetConfigurationValue(ConfigurationItems item)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GC"));
        alparameters.Add(new SqlParameter("@ConfigurationItem", item.ToString()));
        alparameters.Add(new SqlParameter("@ProductId", 2));

        return Convert.ToString(ExecuteScalar("HRspConfigurationMaster", alparameters));
    }

}
