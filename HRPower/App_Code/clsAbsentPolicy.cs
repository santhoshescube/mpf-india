﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProjectPolicy
/// purpose : Handle Project Policy
/// created : Lekshmi
/// Date    : 24.06.2011
/// </summary>
public class clsAbsentPolicy : DL
{
    public clsAbsentPolicy() { }

    //
    // TODO: Add constructor logic here
    //

    public int AbsentPolicyID { get; set; }
    public string Description { get; set; }
    public int CalculationBasedID { get; set; }
    public bool CompanyBasedOn { get; set; }
    public bool RateOnly { get; set; }
    public decimal AbsentRate { get; set; }
    public bool IsRateBasedOnHour { get; set; }
    public decimal HoursPerDay { get; set; }

    public int SalPolicyID { get; set; }
    public int AddDedID { get; set; }
    public int PolicyType { get; set; }

    public bool MergeBoth { get; set; }
    public int AbsentType { get; set; }


    public DataTable FillAllCalculationBasedOn()                      // Select All calculation based on parameters
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAD"));
        return ExecuteDataTable("HRspAbsentPolicy", arraylst);
    }

    public int InsertUpdateAbsentPolicy(bool bIsInsert)                // Insertion when bIsInsert=true , Updation when bIsInsert=false
    {
        ArrayList arraylst = new ArrayList();

        if (bIsInsert)
            arraylst.Add(new SqlParameter("@Mode", "I"));
        else
        {
            arraylst.Add(new SqlParameter("@Mode", "U"));
            arraylst.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));
        }

        arraylst.Add(new SqlParameter("@Description", Description));
        arraylst.Add(new SqlParameter("@MergeBoth", MergeBoth));

        return Convert.ToInt32(ExecuteScalar(arraylst));
    }


    public int DeleteAbsentPolicy()                                                // Deletion Of absent Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        try
        {
            return (Convert.ToInt32(ExecuteScalar("HRspAbsentPolicy", arraylst)));
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet GetAllAbsentPolicies()    // Getting all absent policies
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));
        return ExecuteDataSet("HRspAbsentPolicy", alParameters);
    }
    public DataSet GetAllParticulars()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAAP"));
        return ExecuteDataSet("HRspAbsentPolicy", alParameters);
    }

    public int DeleteAbsentSalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "DSP"));

        alParameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public int InsertAbsentSalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "ISP"));

        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@AddDedID", AddDedID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return Convert.ToInt32(ExecuteScalar(alParameters));
    }

    public DataTable GetAbsentSalaryIDS()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSP"));
        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return ExecuteDataTable("HRspAbsentPolicy", alParameters);
    }

    public void InsertAbsentPolicyDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SAS"));

        alParameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));
        alParameters.Add(new SqlParameter("@CalculationBasedID", CalculationBasedID));
        alParameters.Add(new SqlParameter("@CompanyBasedOn", CompanyBasedOn));
        alParameters.Add(new SqlParameter("@RateOnly", RateOnly));
        alParameters.Add(new SqlParameter("@AbsentRate", AbsentRate));
        alParameters.Add(new SqlParameter("@IsRateBasedOnHour", IsRateBasedOnHour));
        alParameters.Add(new SqlParameter("@HoursPerDay", HoursPerDay));
        alParameters.Add(new SqlParameter("@AbsentType", AbsentType));

        ExecuteNonQuery(alParameters);

    }

    public DataSet GetAbsentPolicyDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SAPD"));
        alParameters.Add(new SqlParameter("@AbsentPolicyID", AbsentPolicyID));

        return ExecuteDataSet("HRspAbsentPolicy", alParameters);
    }

    public void Begin()
    {
        BeginTransaction("HRspAbsentPolicy");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }


}


