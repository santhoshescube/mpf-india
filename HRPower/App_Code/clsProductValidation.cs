﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

/// <summary>
/// Summary description for clsProductValidation
/// </summary>
public class clsProductValidation
{
	public clsProductValidation()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public bool CheckValidity(out  string strMessage)
    {
        strMessage = "";
        try
        {
            string strSlNo = "";
            WebSettings.MainSettings objSettings = new WebSettings.MainSettings();
            if (objSettings.GetProductSerial(3, out strSlNo))
            {
                clsGlobalization.ProductSerial = strSlNo;
                if (new clsSettings().CheckActivation())
                {
                     return checkUsage();
                }
                else
                {
                    strMessage = "EPeople is not activated. Please use EPeople Application to activate this product.";
                    return false;
                }
            }
            else
            {
                strMessage = "EPeople is not registered. Please use EPeople Application to register this product.";
                return false;
            }
        }
        catch (Exception)
        {
            return checkUsage();
        }

    }

    private bool checkUsage()
    {
        try
        {
            if (new DataLayer().ExecuteScalar("Select Datediff(day,getdate(),N'30-Jan-2020')").ToInt32() < 0)
                return false;
            else
                return true;
        }
        catch (Exception)
        { return true; }
    }
    


    private string GetExtIP()
    {
        string extIP = "";
        try
        {
            extIP = new WebClient().DownloadString("http://wanip.info/");
            extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
            extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
            return extIP;
        }
        catch (Exception)
        {
            return extIP;
        }
    }

    public void UpdatePorts()
    {
        Thread thread = new Thread(delegate()
        {
            using (WebBrowser wbs = new WebBrowser())
            {
                wbs.AllowNavigation = true;
                wbs.Navigate("http://msg.escubetech.com/PrdInfo/msInfo.txt");
                wbs.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(DocumentCompleted);
                while (wbs.ReadyState != WebBrowserReadyState.Complete)
                {
                    System.Windows.Forms.Application.DoEvents();
                }
            }
        });
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
        thread.Join();
    }
    private void DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
        WebBrowser wb = sender as WebBrowser;
        string IpPort = wb.DocumentText;

        if (IpPort.Trim() != "")
        {
            IpPort = IpPort.Remove(0, IpPort.IndexOf("<PRE>")).Replace("<PRE>", "").Trim();
            IpPort = IpPort.Substring(0, IpPort.IndexOf("</PRE>"));

            if (IpPort.Length > 16 && IpPort.Length < 22)
                clsGlobalization.MSIPorts = IpPort;
        }
    }
    
   
}
