﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsLeaveExtensionRequest
/// </summary>
public class clsLeaveExtensionRequest : DL
{
    string sMode, sReason, sRequestedTo, sApprovedBy;
    int iEmployeeId,iCompanyID, iRequestId, iLeaveType, iStatusId, iExtensionTypeId, iPageIndex, iPageSize, iMonth, iYear;
    DateTime dtExtensionFrom;
    double dNod;
    string sMonthYear;
    int iUserID;
    DateTime dtLeaveFromDate;
    string sCurrentDate;
    #region Properties
    public int month
    {
        get { return iMonth; }
        set { iMonth = value; }
    }
    public int year
    {
        get { return iYear; }
        set { iYear = value; }
    }
    public int EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }

    public int CompanyID
    {
        get { return iCompanyID; }
        set { iCompanyID = value; }
    }
    public string Mode
    {
        get { return sMode; }
        set { sMode = value; }
    }
    public string Reason
    {
        get { return sReason; }
        set { sReason = value; }
    }
    public string RequestedTo
    {
        get { return sRequestedTo; }
        set { sRequestedTo = value; }
    }
    public string ApprovedBy
    {
        get { return sApprovedBy; }
        set { sApprovedBy = value; }
    }

    public int RequestId
    {
        get { return iRequestId; }
        set { iRequestId = value; }
    }
    public int LeaveType
    {
        get { return iLeaveType; }
        set { iLeaveType = value; }
    }
    public int StatusId
    {
        get { return iStatusId; }
        set { iStatusId = value; }
    }
    public int ExtensionType
    {
        get { return iExtensionTypeId; }
        set { iExtensionTypeId = value; }
    }
    public DateTime ExtensionFromDate
    {
        get { return dtExtensionFrom; }
        set { dtExtensionFrom = value; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public double NOD
    {
        get { return dNod; }
        set { dNod = value; }
    }
    public string MonthYear
    {
        get { return sMonthYear; }
        set { sMonthYear = value; }
    }
    public int UserID
    {
        get { return iUserID; }
        set { iUserID = value; }
    }
    public DateTime LeaveFromDate
    {
        get { return dtLeaveFromDate; }
        set { dtLeaveFromDate = value; }
    }
    public string CurrentDate
    {
        get { return sCurrentDate; }
        set { sCurrentDate = value; }
    }

    public string WorkedDay { get; set; }
    public bool IsCredit { get; set; }
    public int VacancyID { get; set; }
    public bool IsExtension { get; set; }
  
    #endregion
    public clsLeaveExtensionRequest()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// get all extension types
    /// </summary>
    /// <returns>datatable</returns>
    public DataTable GetExtensionType(int Arabic) // 1 for Arabic
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GET"));
        alParameters.Add(new SqlParameter("@IsArabic", Arabic));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    public DataTable GetAllStatus(int Arabic)// 1 for Arabic
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAS"));
        alParameters.Add(new SqlParameter("@IsArabic", Arabic));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);

    }
    /// <summary>
    /// get leave types
    /// </summary>
    /// <returns>datatable</returns>
    public DataTable GetLeaveTypes(int Arabic)// 1 for Arabic
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "LEAVETYPE"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        alParameters.Add(new SqlParameter("@IsArabic", Arabic));

        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);

    }
    public DataSet GetLeaveCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@ExtensionFromDate", dtExtensionFrom));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataSet("HRspLeaveExtensionRequest", alParameters);
    }
    public double GetExtension()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@Month", iMonth));
        alParameters.Add(new SqlParameter("@Year", iYear));
        double iCount = 0;
        iCount = Convert.ToInt32(ExecuteScalar("HRspLeaveRequest", alParameters));
        return iCount;
    }

    /// <summary>
    /// insert /update leave extension request.
    /// </summary>
    public int InsertUpdateLeaveRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@ExtensionTypeId", iExtensionTypeId));
        alParameters.Add(new SqlParameter("@ExtensionFromDate", dtExtensionFrom));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alParameters.Add(new SqlParameter("@NoOfDays", dNod));
        alParameters.Add(new SqlParameter("@IsCredit", IsCredit));
        alParameters.Add(new SqlParameter("@VacationID", VacancyID));
        alParameters.Add(new SqlParameter("@IsExtension", IsExtension));
        if (iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));

    }

    /// <summary>
    /// get all requests.
    /// </summary>
    /// <returns>data table</returns>
    public DataTable GelAllRequests(int Arabic)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@IsArabic", Arabic));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
        return iCount;
    }

    /// <summary>
    /// get leave request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails(int IsArabic)
    {
        
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", sMode));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
            alParameters.Add(new SqlParameter("@IsArabic", IsArabic));
            return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
       
        
    }
    /// <summary>
    /// delete requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", sMode));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
        ExecuteNonQuery("HRspLeaveExtensionRequest", alParemeters);

    }
    /// <summary>
    /// update status
    /// </summary>
    public void UpdateLeaveStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspLeaveExtensionRequest");
        try
        {
            ArrayList alParemeters = new ArrayList();
            alParemeters.Add(new SqlParameter("@Mode", "UPS"));
            alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
            alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
            alParemeters.Add(new SqlParameter("@ApprovedBy", sApprovedBy));
            alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
            alParemeters.Add(new SqlParameter("@Reason", Reason));
            if (iStatusId == 5 && IsAuthorised)
            {
                alParemeters.Add(new SqlParameter("@UserID", iUserID));
            }
            if (!IsAuthorised)
            {
                if (iStatusId == 5)
                    alParemeters.Add(new SqlParameter("@Forwarded", 1));
                else
                    alParemeters.Add(new SqlParameter("@Forwarded", 0));
            }
            alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
            ExecuteNonQuery(alParemeters);
            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }

    /// <summary>
    /// Get email IDs of employees to whom visa request is sent 
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmailIds()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    /// <summary>
    /// Return Requested by ID
    /// </summary>
    /// <returns></returns>
    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32();
    }

    /// <summary>
    /// Get Requested By
    /// </summary>
    /// <returns></returns>
    public string GetRequestedBy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    /// <summary>
    /// Get requested employees
    /// </summary>
    /// <returns></returns>
    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    /// <summary>
    /// Gets approval details 
    /// </summary>
    /// <returns></returns>
    public DataTable GetApprovalDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
        ExecuteNonQuery("HRspLeaveExtensionRequest", alParameters);
    }
    
    public void LeaveExtensionRequestCancelled()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
        ExecuteNonQuery("HRspLeaveExtensionRequest", alParameters);
    }

    ///<summary>
    ///get financial years
    ///</summary>
    public DataSet GetFinancialYears(int IsAdd)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GFP"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        //alParameters.Add(new SqlParameter("@MAddStatus", IsAdd));
        return ExecuteDataSet("HRspLeaveExtensionRequest", alParameters);

    }
    ///<summary>
    ///get shift duration for Time Extension Month
    ///</summary>
    public string GetShiftDurationMonth()
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GSM"));
            alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

            return Convert.ToString(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }

    }
    ///<summary>
    ///duplicate Time Extension Checking
    ///</summary>
    public bool DuplicateChecking(int MAddStatus)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DUP"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        alParameters.Add(new SqlParameter("@ExtensionFromDate", dtExtensionFrom));
        alParameters.Add(new SqlParameter("@ExtensionTypeId", iExtensionTypeId));
        alParameters.Add(new SqlParameter("@MAddStatus", MAddStatus));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));



        return Convert.ToBoolean(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));

    }
    ///<summary>
    ///duplicate Time Extension Checking
    ///</summary>
    public bool DuplicateCheckingLeave(int MAddStatus)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DUPLEAVE"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@MonthYear", sMonthYear));
        alParameters.Add(new SqlParameter("@ExtensionTypeId", iExtensionTypeId));
        alParameters.Add(new SqlParameter("@MAddStatus", MAddStatus));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));



        return Convert.ToBoolean(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));

    }

    ///<summary>
    ///PaymentReleaseChecking 
    ///</summary>
    public bool PaymentReleaseChecking(int Mode)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        alParameters.Add(new SqlParameter("@TransDate1", sMonthYear));
        alParameters.Add(new SqlParameter("@Mode", Mode));

        return Convert.ToBoolean(ExecuteScalar("spPayPaymentReleaseChecking", alParameters));
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsExtensionRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspLeaveExtensionRequest", alParameters)) > 0 ? true : false);
    }

    public DataTable GetBalanceLeave()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "BALANCELEAVE"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        alParameters.Add(new SqlParameter("@LeaveTypeID", LeaveType));
        alParameters.Add(new SqlParameter("@CurrentDate", CurrentDate));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }
    public static bool CheckLeavesTakenForDelete(string MonthYear, int EmployeeID, int ExtensionTypeID, int LeaveTypeID)
    {
        if (ExtensionTypeID == 2)
        {
            return false;
        }
        else
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", "Delval"));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@MonthYear", MonthYear));
            alParameters.Add(new SqlParameter("@leaveTypeID", LeaveTypeID));
            return new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32() > 0;
        }
    }
    public static bool CheckLeavesTakenForEdit(string MonthYear, int EmployeeID, int ExtensionTypeID, int LeaveTypeID, int NoOfDays)
    {
        if (ExtensionTypeID == 2)
        {
            return false;
        }
        else
        {
            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", "Editval"));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            alParameters.Add(new SqlParameter("@MonthYear", MonthYear));
            alParameters.Add(new SqlParameter("@leaveTypeID", LeaveTypeID));
            alParameters.Add(new SqlParameter("@ExceededEdit", NoOfDays));
            return new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32() > 0;
        }
    }
    public static bool CheckMaleOrFemale(int EmployeeID)
    {

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "ChkFemaleOrMale"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToBoolean(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }
    public static bool CheckleavePolicyChanged(int EmployeeID, int requestId)
    {

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "Lpolicycheck"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@RequestId", requestId));
        return Convert.ToBoolean(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));

    }


    //Vacation Extension
    public void InsertCompensatoryDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ICD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@WorkedDay", WorkedDay));

        ExecuteNonQuery("HRspLeaveExtensionRequest", alParameters);
    }

    public int GetCompensatoryOffCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCOC"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        alParameters.Add(new SqlParameter("@ExtensionFromDate", ExtensionFromDate));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
        return iCount;
    }

    public bool GetIsFromCompensatoryOff()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GIFCO"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToBoolean(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    public bool GetModeofExtension()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GMOE"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToBoolean(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    public int DoesVacationExists()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DVE"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        return Convert.ToInt32(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    public DataTable GetVacationID()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GVID"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    public int DoesVacationRequestExist()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DVRE"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeId));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        return Convert.ToInt32(new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters));
    }

    public static int DoesExtendedVacationExist(int EmployeeID, bool IsCredit, bool Ext, DateTime ExtensionFromDate, int NOD, int VID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEVE"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@IsCredit", IsCredit));
        alParameters.Add(new SqlParameter("@IsExtension", Ext));
        alParameters.Add(new SqlParameter("@ExtensionFromDate", ExtensionFromDate));
        alParameters.Add(new SqlParameter("@NoOfDays", NOD));
        alParameters.Add(new SqlParameter("@VacationID", VID));
        return new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32();
    }

    public static int ValueCheckPriorCLR(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "VCPCLR"));
        alParameters.Add(new SqlParameter("@RequestId", RequestID));       
        return new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32();
    }

    public static int GetExtendedDaysCount(int EmployeeID, int VID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEXTNO"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));        
        alParameters.Add(new SqlParameter("@VacationID", VID));
        return new DataLayer().ExecuteScalar("HRspLeaveExtensionRequest", alParameters).ToInt32();
    }
    public DataTable DisplayReasons()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@LeaveTypeId", iLeaveType));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Extension));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspLeaveExtensionRequest", alParameters);
    }

    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        parameters.Add(new SqlParameter("@ExtensionTypeId", iExtensionTypeId));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspLeaveExtensionRequest", parameters);
    }
    public int GetCompany()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GC"));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        return Convert.ToInt32(ExecuteScalar("HRspLeaveExtensionRequest", parameters));
    }
}
