﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGeneralRequest
/// </summary>
public class clsGeneralRequest:DL 
{
    public Int32 RequestID { get; set; }
    public DateTime RequestedDate { get; set; }
    public Int64 EmployeeID { get; set; }
    public Int32 CompanyID { get; set; }
    public Int32 RequestTypeId { get; set; }
    public DateTime RequestFromDate { get; set; }
    public DateTime RequestToDate { get; set; }
    public string Reason { get; set; }
    public string RequestedTo { get; set; }
    public string Mode { get; set; }
    public string ApprovedBy { get; set; }
    public string Remarks { get; set; }
    public string FlightNumber { get; set; }
    public decimal StatusID { get; set; }
    public Int32 PageIndex { get; set; }
    public Int32 PageSize { get; set; }
    public Int32 UserID { get; set; }
    public Int32 BenefitTypeID { get; set; }
    public Int32 LeaveTypeID { get; set; }
    public Int32 LeaveReferenceID { get; set; }
    public DateTime RejoinDate { get; set; }
	public clsGeneralRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int InsertRequest()
    {
        try
        {
            this.BeginTransaction("HRspGeneralRequest");

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", Mode));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
            alParameters.Add(new SqlParameter("@RequestTypeId", RequestTypeId));
            alParameters.Add(new SqlParameter("@RequestFromDate", RequestFromDate));
            alParameters.Add(new SqlParameter("@RequestToDate", RequestToDate));
            alParameters.Add(new SqlParameter("@FlightNumber", FlightNumber));
            alParameters.Add(new SqlParameter("@Reason", Reason));
            alParameters.Add(new SqlParameter("@RequestedTo", RequestedTo));
            alParameters.Add(new SqlParameter("@StatusId", StatusID ));
            alParameters.Add(new SqlParameter("@BenefitTypeID", BenefitTypeID));
            alParameters.Add(new SqlParameter("@Type", LeaveTypeID));
            alParameters.Add(new SqlParameter("@LeaveReferenceID", LeaveReferenceID));
            alParameters.Add(new SqlParameter("@RejoinDate", RejoinDate));
            if (RequestID > 0)
                alParameters.Add(new SqlParameter("@RequestID", RequestID ));
            alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
            RequestID = Convert.ToInt32(ExecuteScalar(alParameters));

            this.CommitTransaction();
            return RequestID;
        }
        catch
        {
            this.RollbackTransaction();
            return RequestID;
        }

    }

    /// <summary>
    /// Get requested details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex ));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspGeneralRequest", alParameters);
    }

    /// <summary>
    /// Get request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    
    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID ));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParameters));
        return iCount;
    }
    /// <summary>
    /// delete selected requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID ));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
        ExecuteNonQuery("HRspGeneralRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        return (Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParameters)) > 0 ? true : false);
    }
    /// <summary>
    /// get requested month loan installment amount
    /// </summary>
    /// <returns></returns>

    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateAdvanceStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspGeneralRequest");

        try
        {
            ArrayList alParemeters = new ArrayList();
            alParemeters.Add(new SqlParameter("@Mode", "UPS"));
            alParemeters.Add(new SqlParameter("@RequestID", RequestID));
            alParemeters.Add(new SqlParameter("@StatusId", StatusID ));
            alParemeters.Add(new SqlParameter("@ApprovedBy", ApprovedBy ));
            alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
            alParemeters.Add(new SqlParameter("@Remarks", Remarks));
            if (IsAuthorised && StatusID  == 5)
            {

                alParemeters.Add(new SqlParameter("@UserId", UserID));
               
            }
            else if (StatusID  != 3 && !IsAuthorised)
                alParemeters.Add(new SqlParameter("@Forwarded", 1));
            alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
            RequestID = ExecuteScalar(alParemeters).ToInt32();
          
            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }

    public int GetRequestedById()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GBI"));
        alParemeters.Add(new SqlParameter("@RequestID", RequestID ));

        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParemeters));
    }
    public int GetRequestedByCompanyID()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GEC"));
        alParemeters.Add(new SqlParameter("@EmployeeID", EmployeeID ));

        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParemeters));
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID ));
        alParameters.Add(new SqlParameter("@StatusId", StatusID ));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
        ExecuteNonQuery("HRspGeneralRequest", alParameters);
    }
    public void SalaryAdvanceRequestCancelled()
    {
        this.BeginTransaction("HRspGeneralRequest");

        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "CLR"));
            alParameters.Add(new SqlParameter("@RequestID", RequestID ));
            alParameters.Add(new SqlParameter("@StatusId", StatusID));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
            alParameters.Add(new SqlParameter("@Remarks", Remarks));
            alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
            RequestID = ExecuteScalar(alParameters).ToInt32();          

            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }
    /// <summary>
    /// Get workstatusId to check whether the employee is in Service
    /// </summary>
    /// <returns></returns>
    public int GetWorkStatusId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CWS"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParameters));
    }

    ///<summary>
    ///To check requested date is greater than dateofjoining
    ///</summary>
    public bool CheckDateOfJoining()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDJ"));
        alParameters.Add(new SqlParameter("@RequestedDate", RequestedDate));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        //alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return Convert.ToBoolean(ExecuteScalar("HRspGeneralRequest", alParameters));
    }
    public int GetCompanyID(int RequestID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCI"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", alParameters));

    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }
    public static DataTable FillRequestType()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public static string GetRequestedTo(Int64 intEmployeeId)
    {
        DataTable dt;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRT"));
        alParameters.Add(new SqlParameter("@EmployeeId", intEmployeeId));

        return Convert.ToString(new DataLayer().ExecuteScalar("HRspGeneralRequest", alParameters));
    }
    public static DataTable GetPendingDocs(Int64 EmployeeID)
    {
         ArrayList alParameters = new ArrayList();
         alParameters.Add(new SqlParameter("@Mode", "SPD"));
         alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));

        return new DataLayer().ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public static DataTable FillAssetType()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FAT"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public static DataTable GetAssetDetails(int BenefitTypeID, int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AD"));
        alParameters.Add(new SqlParameter("@BenefitTypeID", BenefitTypeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));       
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return new DataLayer().ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public DataTable DisplayReasons(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.General));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@BenefitTypeID", BenefitTypeID));
        parameters.Add(new SqlParameter("@RequestID", RequestID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspGeneralRequest", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GC"));
        parameters.Add(new SqlParameter("@RequestID", RequestID));

        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", parameters));
    }
    public DataTable GetRejoinDate(Int64 EmployeeID, int type)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        alParameters.Add(new SqlParameter("@Type", type));
        return ExecuteDataTable("HRspGeneralRequest", alParameters);
    }
    public int CheckLeaveExistance(int LeaveReferenceID, int type)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "CLRE"));
        parameters.Add(new SqlParameter("@Type", type));
        parameters.Add(new SqlParameter("@LeaveReferenceID", LeaveReferenceID));
        return Convert.ToInt32(ExecuteScalar("HRspGeneralRequest", parameters));
    }
    public DataTable GetUnclosedLoan()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "EBLD"));
        parameters.Add(new SqlParameter("@EmployeeId", EmployeeID));
        return ExecuteDataTable("HRspGeneralRequest", parameters);
       
    }
}
