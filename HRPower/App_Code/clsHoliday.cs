﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;


/// <summary>
/// Summary description for clsHoliday
/// </summary>
public class clsHoliday : DL
{
    int iCompanyId, iHolidayType, iEmployeeId, iID;
    string sDescription, sTitle;
    DateTime dtDate;
    public int CompanyId { get { return iCompanyId; } set { iCompanyId = value; } }
    public int EmployeeId { get { return iEmployeeId; } set { iEmployeeId = value; } }
    public int HolidayType { get { return iHolidayType; } set { iHolidayType = value; } }
    public string Description { get { return sDescription; } set { sDescription = value; } }
    public string Title { get { return sTitle; } set { sTitle = value; } }
    public DateTime Date { get { return dtDate; } set { dtDate = value; } }
    public int ID { get { return iID; } set { iID = value; } }

    public clsHoliday()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable GetHolidaytypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "T"));

        return ExecuteDataTable("HRspHolidays", alParameters);
    }
    public DataTable GetCompanies()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "C"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyId));
        return ExecuteDataTable("HRspHolidays", alParameters);
    }

    public DataTable GetHolidays()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GHT"));
        return ExecuteDataTable("HRspHolidays", alParameters);
    }

    public void InsertHoliday()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
        alParameters.Add(new SqlParameter("@HolidayTypeID", iHolidayType));
        alParameters.Add(new SqlParameter("@Date", dtDate));
        alParameters.Add(new SqlParameter("@Description", sDescription));

        ExecuteNonQuery("HRspHolidays", alParameters);
    }
    public void UpdateHoliday()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@ID", iID));
        alParameters.Add(new SqlParameter("@HolidayTypeID", iHolidayType));
        alParameters.Add(new SqlParameter("@Description", sDescription));

        ExecuteNonQuery("HRspHolidays", alParameters);
    }
    public void InsertEvent()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
        alParameters.Add(new SqlParameter("@Title", sTitle));
        alParameters.Add(new SqlParameter("@EventDate", dtDate));
        alParameters.Add(new SqlParameter("@Description", sDescription));

        ExecuteNonQuery("HRspEvents", alParameters);
    }
    public void UpdateEvent()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@ID", iID));
        alParameters.Add(new SqlParameter("@Title", sTitle));
        alParameters.Add(new SqlParameter("@Description", sDescription));

        ExecuteNonQuery("HRspEvents", alParameters);
    }

    public bool IsHoliday()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EX"));

        alParameters.Add(new SqlParameter("@Date", dtDate));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));

        if (Convert.ToInt32(ExecuteScalar("HRspHolidays", alParameters)) == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public DataTable GetOffday()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "OD"));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));

        return ExecuteDataTable("HRspHolidays", alParameters);
    }
    public bool IsEvent()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EX"));

        alParameters.Add(new SqlParameter("@EventDate", dtDate));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));


        if (Convert.ToInt32(ExecuteScalar("HRspEvents", alParameters)) == 1)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public DataSet GetEvents()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S"));

        alParameters.Add(new SqlParameter("@EventDate", dtDate));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));

        return ExecuteDataSet("HRspEvents", alParameters);
    }

    public DataSet GetFullEvents(int intEventID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S2"));

        alParameters.Add(new SqlParameter("@EventDate", dtDate));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
        alParameters.Add(new SqlParameter("@ID", intEventID));

        return ExecuteDataSet("HRspEvents", alParameters);
    }
    public DataSet GetParticularEvents(int intEventID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S4"));
        alParameters.Add(new SqlParameter("@ID", intEventID));

        return ExecuteDataSet("HRspEvents", alParameters);
    }
    public void DeleteEvent(int intEventID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SD"));

        alParameters.Add(new SqlParameter("@ID", intEventID));
        ExecuteNonQuery("HRspEvents", alParameters);
    }
    public DataSet GetFullHoliday(int intHolidayID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S3"));

        alParameters.Add(new SqlParameter("@EventDate", dtDate));
        alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
        alParameters.Add(new SqlParameter("@ID", intHolidayID));

        return ExecuteDataSet("HRspEvents", alParameters);
    }
    public DataSet GetParticularHoliday(int intHolidayID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S5"));
        alParameters.Add(new SqlParameter("@ID", intHolidayID));

        return ExecuteDataSet("HRspEvents", alParameters);
    }
    public void DeleteHoliday(int intHolidayID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SD2"));

        alParameters.Add(new SqlParameter("@ID", intHolidayID));
        ExecuteNonQuery("HRspEvents", alParameters);
    }

    public bool CheckPaymentExists()
    {
        int intCount = 0;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode","PEXT"));
        alParameters.Add(new SqlParameter("@CompanyID",iCompanyId));
        alParameters.Add(new SqlParameter("@Date",clsCommon.Convert2DateTime(dtDate)));
      
        intCount = ExecuteScalar("HRspHolidays", alParameters ).ToInt32();
        return (intCount > 0);

    }
}
