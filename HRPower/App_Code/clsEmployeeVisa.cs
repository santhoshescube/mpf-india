﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

using System.Text;
/// <summary>
/// Summary description for clsEmployeeVisa
/// </summary>
public class clsEmployeeVisa:DL
{
	public clsEmployeeVisa()
	{		
	}

    public int VisaID { get; set; }
    public int EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public int VisaTypeID { get; set; }
    public string VisaNumber { get; set; }
    public DateTime VisaIssueDate { get; set; }
    public string PlaceofIssue { get; set; }    
    public DateTime VisaExpiryDate { get; set; }
    public int IssueCountryID { get; set; }
    public bool IsRenewed { get; set; }

    public string Remarks { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int UserId { get; set; }
    public string SearchKey { get; set; }
    public int Node { get; set; }
    public string Docname { get; set; }
    public string Filename { get; set; }
    public int SortExpression { get; set; }
    public string SortOrder { get; set; }

    public void BeginEmp()
    {
        BeginTransaction("HRspEmployeeVisa");
    }

    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    //-----------------------------------------------------------Employee passport ------------------------------------------------------------
    public DataSet GetEmpSalaryStructure()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSS"));
        alparameters.Add(new SqlParameter("@VisaID", VisaID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataSet("HRspEmployeeVisa", alparameters);
    }
    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspEmployeeVisa", alParameters));
    }

    public DataSet GetAllEmployeeSalaryStructures()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alParameters.Add(new SqlParameter("@SortOrder", SortOrder));
        return ExecuteDataSet("HRspEmployeeVisa", alParameters);
    }

    public DataSet GetEmployeeSalaryStructure()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataSet("HRspEmployeeVisa", alParameters);
    }

    public int InsertEmployeevisa() //Insert Employee Salary Structure details
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IH"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@VisaNumber", VisaNumber));
        alparameters.Add(new SqlParameter("@VisaTypeID", VisaTypeID));
        alparameters.Add(new SqlParameter("@VisaIssueDate", VisaIssueDate));
        alparameters.Add(new SqlParameter("@PlaceofIssue", PlaceofIssue));
        if (IssueCountryID == -1) alparameters.Add(new SqlParameter("@IssueCountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@IssueCountryID", IssueCountryID));
        alparameters.Add(new SqlParameter("@VisaExpiryDate", VisaExpiryDate));       
        alparameters.Add(new SqlParameter("@Remarks", Remarks));
        alparameters.Add(new SqlParameter("@IsRenewed", IsRenewed));


        VisaID = Convert.ToInt32(ExecuteScalar(alparameters));

        return VisaID;
    }


    public int UpdateEmployeeVisa()  //update salary structure
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UH"));
        alparameters.Add(new SqlParameter("@VisaID", VisaID));

        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@VisaNumber", VisaNumber));
        alparameters.Add(new SqlParameter("@VisaTypeID", VisaTypeID));
        alparameters.Add(new SqlParameter("@VisaIssueDate", VisaIssueDate));
        alparameters.Add(new SqlParameter("@PlaceofIssue", PlaceofIssue));
        if (IssueCountryID == -1) alparameters.Add(new SqlParameter("@IssueCountryID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@IssueCountryID", IssueCountryID));
        alparameters.Add(new SqlParameter("@VisaExpiryDate", VisaExpiryDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));
        alparameters.Add(new SqlParameter("@IsRenewed", IsRenewed));

        VisaID = Convert.ToInt32(ExecuteScalar(alparameters));

        return VisaID;
    }

    public int UpdateRenewEmployeeVisa()  //Update Renew Employee Visa
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "UR"));
        alparameters.Add(new SqlParameter("@VisaID", VisaID));
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable FillCountry()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HRspEmployeeVisa", alparameters);
    }
    public DataTable FillVisaType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FVT"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HRspEmployeeVisa", alparameters);
    }
    public DataTable FillEmployees(long lngEmployeeID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FEM"));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HRspEmployeeVisa", alparameters);
    }

    public DataTable FillFilterEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GES"));
        return ExecuteDataTable("HRspEmployeeVisa", alparameters);
    }

    public string GetEmployeeName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspEmployeeVisa", alparameters));
    }
    public int GetCompanyId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCI"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspEmployeeVisa", alparameters));
    }


    public bool IsvisaIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeVisa", alParameters)) > 0 ? true : false);
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DES"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));

        ExecuteNonQuery("HRspEmployeeVisa", alParameters);
    }
    public DataTable GetSingleVisa(int iVisaID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@VisaID", iVisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspEmployeeVisa", alParameters).Tables[0];
    }
    public DataTable  GetVisaList(string sVisaIds)
    {
      
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@VisaIds", sVisaIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
       return  ExecuteDataTable("HRspEmployeeVisa", arraylst);
    }
    public string GetPrintText(int iVisaID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@VisaID", iVisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        DataTable dt = ExecuteDataSet("HRspEmployeeVisa", alParameters).Tables[0];


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Visa Information</td></tr>");
        sb.Append("<tr><td width='150px'>Visa Number</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaNumber"]) + "</td></tr>");
        sb.Append("<tr><td width='150px'>Visa Type</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaType"]) + "</td></tr>");
        sb.Append("<tr><td>Placeof Issue</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>Is Renewed</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>Other Info</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public string CreateSelectedEmployeesContent(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@VisaIds", sVisaIds));

        DataTable dt = ExecuteDataTable("HRspEmployeeVisa", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Visa Information</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee</td><td width='150px'>Visa Number</td><td width='150px'>Visa Type</td><td width='150px'>Place Of Issue</td><td width='100px'>Issue Date</td><td width='100px'>Expiry Date</td><td width='100px'>Country</td><td width='100px'>Is Renewed</td><td width='100px'>Other Info</td></tr>");
        sb.Append("<tr><td colspan='9'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public bool IsVisaNumberExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEC"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));
        alParameters.Add(new SqlParameter("@VisaNumber", VisaNumber));

        return (Convert.ToInt32(ExecuteScalar("HRspEmployeeVisa", alParameters)) > 0 ? true : false);
    }
    public DataSet GetVisaDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPD"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return ExecuteDataSet("HRspEmployeeVisa", alParameters);
    }
    public void DeleteVisaDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@Node", Node));

        ExecuteNonQuery("HRspEmployeeVisa", alParameters);
    }

    public int InsertEmployeeVisaTreemaster() //Insert Employee passport treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ITM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@VisaID", VisaID));
        alparameters.Add(new SqlParameter("@VisaNumber", VisaNumber));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int UpdateEmployeeVisaTreemaster() //Insert Employee passport treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UTM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@NameinPassport", VisaID));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public DataTable GetVisaFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));
        alParameters.Add(new SqlParameter("@VisaID", VisaID));

        return ExecuteDataTable("HRspEmployeeVisa", alParameters);
    }

}
