﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsGeneralExpenseReference
/// </summary>
public class clsGeneralExpenseReference:DL
{
    #region Variables
    public static string Procedure = "HRspGeneralExpenseReference";
    #endregion

    #region Properties

    public int ExpenseID { get; set; }
    public string ExpenseNumber { get; set; }
    public int ExpenseCategoryID { get; set; }
    public int ExpenseTypeID { get; set; }
    public int ReferenceID { get; set; }
    public string ExpenseDate { get; set; }
    public decimal ExpenseAmount { get; set; }
    public int CurrencyID { get; set; }
    public int CompanyID { get; set; }
    public string Remarks { get; set; }
    public long CreatedBy { get; set; }
    public string CreatedDate { get; set; }    

    #endregion
	public clsGeneralExpenseReference()
	{ }

    #region Methods

    public int AddUpdateGeneralExpense()
    {
        ArrayList parameters = new ArrayList();
        if (ExpenseID > 0)
        {
            parameters.Add(new SqlParameter("@Mode", "UPDATE"));
            parameters.Add(new SqlParameter("@ExpenseID", ExpenseID));
        }
        else
        {
            parameters.Add(new SqlParameter("@Mode", "INS"));
            parameters.Add(new SqlParameter("@CreatedBy", CreatedBy));
        }
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@ExpenseNumber", ExpenseNumber));
        parameters.Add(new SqlParameter("@ExpenseCategoryID", ExpenseCategoryID));

        if (ExpenseCategoryID == 1)         //General
            parameters.Add(new SqlParameter("@ExpenseTypeID", ExpenseTypeID));
        else if (ExpenseCategoryID == 2)    //Recruitment
            parameters.Add(new SqlParameter("@ReferenceID", ReferenceID));
        else if (ExpenseCategoryID == 3)    //Projects
            parameters.Add(new SqlParameter("@ReferenceID", ReferenceID));

        parameters.Add(new SqlParameter("@ExpenseDate", ExpenseDate));
        parameters.Add(new SqlParameter("@ExpenseAmount", ExpenseAmount));
        parameters.Add(new SqlParameter("@Remarks", Remarks));

        return ExecuteScalar(Procedure, parameters).ToInt32();
    }

    public bool DeleteExpense()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DE"));
        parameters.Add(new SqlParameter("@ExpenseID", ExpenseID));
        return ExecuteScalar(Procedure, parameters).ToInt32() > 0 ? true : false;
    }

    public static DataTable BindCompany()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "BC"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable BindExpenseCategory()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "BEC"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable BindExpenseType(int ExpenseCategoryID, int CompanyID, int ExpenseTypeID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "BET"));
        parameters.Add(new SqlParameter("@ExpenseCategoryID", ExpenseCategoryID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@ReferenceID", ExpenseTypeID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, parameters);
    }

    public int CheckExpenseNumberDuplication()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "CEND"));
        parameters.Add(new SqlParameter("@ExpenseID", ExpenseID));
        parameters.Add(new SqlParameter("@ExpenseNumber", ExpenseNumber));

        return ExecuteScalar(Procedure, parameters).ToInt32();
    }

    public static DataTable GetExpense(int ExpenseID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "VIEW"));
        arr.Add(new SqlParameter("@ExpenseID", ExpenseID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public DataTable GetAllExpenses()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAE"));
        //parameters.Add(new SqlParameter("@PageIndex", PageIndex));
        //parameters.Add(new SqlParameter("@PageSize", PageSize));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable(Procedure, parameters);
    }

    public string GetCurrency(int CompanyID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GC"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return Convert.ToString(ExecuteScalar(Procedure, parameters));
    }

    public static DataTable GetGeneralExpense()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GGE"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable EditExpense(int ExpenseID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "EDIT"));
        arr.Add(new SqlParameter("@ExpenseID", ExpenseID));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    #endregion
}
