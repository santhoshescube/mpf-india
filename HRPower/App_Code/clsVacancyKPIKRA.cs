﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsVacancyKPIKRA
/// </summary>
public class clsVacancyKPIKRA : DL
{
    #region Variables
    public static string Procedure = "HRspVacancyAddEdit";
    #endregion

    #region Properties
    public static int ID { get; set; }
    public int JobID { get; set; }

    #endregion

    public clsVacancyKPIKRA()
    { }

    #region Methods
    public static DataTable GetKpi()
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKPI"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetKra()
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKRA"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataSet GetKpiKra(int JobID)
    {
        ArrayList arr = new ArrayList();

        arr.Add(new SqlParameter("@Mode", "GKK"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteDataSet(Procedure, arr);
    }

    public int DeleteKpiKra(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DELKPIKRA"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return ExecuteScalar(Procedure, arr).ToInt32();
    }

    public int SaveKpi(List<clsVacancyKPI> JobKpis, int JobID)
    {
        try
        {
            foreach (clsVacancyKPI objKpi in JobKpis)
            {
                ArrayList arr = new ArrayList();
                arr.Add(new SqlParameter("@Mode", "INSKPI"));
                arr.Add(new SqlParameter("@JobID", JobID));
                arr.Add(new SqlParameter("@KPIId", objKpi.KPIId));
                arr.Add(new SqlParameter("@KPIValue", objKpi.KPIValue));
                ExecuteScalar(Procedure, arr).ToInt32();
            }
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }


    public int SaveKra(List<clsVacancyKRA> JobKras, int JobID)
    {
        try
        {
            foreach (clsVacancyKRA objKra in JobKras)
            {
                ArrayList arr = new ArrayList();
                arr.Add(new SqlParameter("@Mode", "INSKRA"));
                arr.Add(new SqlParameter("@JobID", JobID));
                arr.Add(new SqlParameter("@KRAId", objKra.KRAId));
                arr.Add(new SqlParameter("@KRAValue", objKra.KRAValue));
                ExecuteScalar(Procedure, arr).ToInt32();
            }
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    #endregion 
}

[Serializable]
public class clsVacancyKPI
{
    public int KPIId { get; set; }

    public decimal KPIValue { get; set; }

    public int JobKpiID { get; set; }
}

[Serializable]

public class clsVacancyKRA
{
    public int KRAId { get; set; }

    public decimal KRAValue { get; set; }

    public int JobKRAID { get; set; }
}
