﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.IO;
using System.Text;

/// <summary>
/// Summary description for clsMailSettings
/// purpose : Handle MailSettings
/// created : Binoj
/// Date    : 03.04.2010
/// </summary>

public class clsMailSettings : DL
{
    public enum AccountType { Email, SMS };

    public clsMailSettings() { }

    private string sUserName;
    private string sPassword;
    private string sIncomingServer;
    private string sOutgoingServer;
    private int iPortNumber;
    private AccountType sAccountType;
    private bool bEnableSsl;

    public string UserName
    {
        get { return sUserName; }
        set { sUserName = value; }
    }

    public string Password
    {
        get { return sPassword; }
        set { sPassword = value; }
    }

    public string IncomingServer
    {
        get { return sIncomingServer; }
        set { sIncomingServer = value; }
    }

    public string OutgoingServer
    {
        get { return sOutgoingServer; }
        set { sOutgoingServer = value; }
    }

    public int PortNumber
    {
        get { return iPortNumber; }
        set { iPortNumber = value; }
    }

    public AccountType aAccountType
    {
        get { return sAccountType; }
        set { sAccountType = value; }
    }

    public bool EnableSsl
    {
        get { return bEnableSsl; }
        set { bEnableSsl = value; }
    }

    public void Insert()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "I"));
        arraylst.Add(new SqlParameter("@UserName", UserName));
        arraylst.Add(new SqlParameter("@Password", Password));
        arraylst.Add(new SqlParameter("@IncomingServer", IncomingServer));
        arraylst.Add(new SqlParameter("@OutgoingServer", OutgoingServer));
        arraylst.Add(new SqlParameter("@PortNumber", PortNumber));
        arraylst.Add(new SqlParameter("@AccountType", aAccountType.ToString()));
        arraylst.Add(new SqlParameter("@EnableSsl", EnableSsl));
        arraylst.Add(new SqlParameter("@ProductId", 3));

        int result = ExecuteNonQuery("HRspMailSettings", arraylst);
    }

    public DataSet GetMailSettings(AccountType sAccountType)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "G"));
        arraylst.Add(new SqlParameter("@AccountType", sAccountType.ToString()));
        arraylst.Add(new SqlParameter("@ProductId", 3));

        return ExecuteDataSet("HRspMailSettings", arraylst);
    }

    public SqlDataReader GetSettings(AccountType sAccountType)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "G"));
        arraylst.Add(new SqlParameter("@AccountType", sAccountType.ToString()));
        arraylst.Add(new SqlParameter("@ProductId", 3));

        return ExecuteReader("HRspMailSettings", arraylst);
    }
    public bool SendScheduleMail(string from, string to,string bcc,string subject, string body, AccountType sAccountType, bool appendHeader)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(from);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            if (bcc.Trim() != string.Empty)
            {
                bcc = bcc.Replace(';', ',');

                if (bcc != "")
                {
                    if (bcc.LastIndexOf(',') != -1)
                    {
                        foreach (string bccAddress in bcc.Split(','))
                            message.Bcc.Add(new MailAddress(bccAddress));
                    }
                    else
                        message.Bcc.Add(bcc);
                }
            }


            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            return true;
        }
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return false;
        }
    }
    public bool SendMail(string to, string subject, string body, AccountType sAccountType, bool appendHeader)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(sUsername);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            return true;
        }
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return false;
        }
    }
    public bool SendMail(string to, string subject, string body, AccountType sAccountType, bool appendHeader, string cc)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(sUsername);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);
            if (cc != "")
            {
                cc = cc.Replace(";", ",");
                if (cc.LastIndexOf(',') != -1)
                {
                    foreach (string ccAddress in cc.Split(','))
                        message.CC.Add(new MailAddress(ccAddress));
                }
                else
                {
                    message.CC.Add(cc);
                }
            }

            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            return true;
        }
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return false;
        }
    }

    public bool SendMail(string from, string to, string subject, string body, AccountType sAccountType, bool appendHeader)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(from);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;
            object oUserToken=new object();
            if (appendHeader)
                AppendHeader(message);
            smtp.SendAsync(message,oUserToken) ;
            //smtp.Send(message);

            return true;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
            return false;
        }
    }

    public bool SendMail(string from, string to, string subject, string body, string attachments, AccountType sAccountType, bool appendHeader)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }

            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(from);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            message.Subject = subject;
            message.Body = body;
            if (attachments.Trim() != string.Empty)
            {
                if (attachments.LastIndexOf(',') != -1)
                {
                    foreach (string attachFile in attachments.Split(','))
                        message.Attachments.Add(new Attachment(attachFile));
                }
                else
                    message.Attachments.Add(new Attachment(attachments));
            }
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            message.Attachments.Dispose();

            return true;
        }
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return false;
        }
    }

    public int SendMail(string from, string to, string cc, string subject, string body, string attachments, bool appendHeader, AccountType sAccountType)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;
            try
            {
                using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
                {
                    if (sdrMailSettings.Read())
                    {
                        sUsername = sdrMailSettings["UserName"].ToString();
                        sPassword = sdrMailSettings["Password"].ToString();
                        sHost = sdrMailSettings["OutgoingServer"].ToString();
                        iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                        bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                    }
                    sdrMailSettings.Close();
                }
            }
            catch { return 0; }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(from);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            cc = cc.Replace(';', ',');

            if (cc != "")
            {
                if (cc.LastIndexOf(',') != -1)
                {
                    foreach (string ccAddress in cc.Split(','))
                        message.CC.Add(new MailAddress(ccAddress));
                }
                else
                    message.To.Add(cc);
            }
            message.Subject = subject;
            message.Body = body;
            if (attachments.Trim() != string.Empty)
            {
                if (attachments.LastIndexOf(',') != -1)
                {
                    foreach (string attachFile in attachments.Split(','))
                        message.Attachments.Add(new Attachment(attachFile));
                }
                else
                    message.Attachments.Add(new Attachment(attachments));
            }

            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            message.Attachments.Dispose();

            return 1;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
            return -1;
        }
    }

    public bool SendMail(string to, string subject, string body, AccountType sAccountType, string attachments, bool appendHeader)
    {
        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
            }

            MailMessage message = new MailMessage();

            message.From = new MailAddress(sUsername);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            message.Subject = subject;
            message.Body = body;
            if (attachments.Trim() != string.Empty)
            {
                if (attachments.LastIndexOf(',') != -1)
                {
                    foreach (string attachFile in attachments.Split(','))
                        message.Attachments.Add(new Attachment(attachFile));
                }
                else
                    message.Attachments.Add(new Attachment(attachments));
            }
            message.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            if (appendHeader)
                AppendHeader(message);

            smtp.Send(message);

            message.Attachments.Dispose();

            return true;
        }
        catch (Exception ex)
        {
            HttpContext.Current.Trace.Write(ex.Message);
            return false;
        }
    }
    public string Send(string to, string cc, string bcc, string subject, string body, string attachments, AccountType sAccountType)
    {
        MailMessage message = new MailMessage();

        try
        {
            string sUsername = string.Empty, sPassword = string.Empty, sHost = string.Empty;
            bool bEnableSsl = false;
            int iPort = -1;

            using (SqlDataReader sdrMailSettings = GetSettings(sAccountType))
            {
                if (sdrMailSettings.Read())
                {
                    sUsername = sdrMailSettings["UserName"].ToString();
                    sPassword = sdrMailSettings["Password"].ToString();
                    sHost = sdrMailSettings["OutgoingServer"].ToString();
                    iPort = Convert.ToInt32(sdrMailSettings["PortNumber"]);
                    bEnableSsl = Convert.ToBoolean(sdrMailSettings["EnableSsl"]);
                }
                sdrMailSettings.Close();
            }

            message.From = new MailAddress(sUsername);

            to = to.Replace(';', ',');

            if (to.LastIndexOf(',') != -1)
            {
                foreach (string toAddress in to.Split(','))
                    message.To.Add(new MailAddress(toAddress));
            }
            else
                message.To.Add(to);

            if (cc.Trim() != string.Empty)
            {
                cc = cc.Replace(';', ',');

                if (cc != "")
                {
                    if (cc.LastIndexOf(',') != -1)
                    {
                        foreach (string ccAddress in cc.Split(','))
                            message.CC.Add(new MailAddress(ccAddress));
                    }
                    else
                        message.CC.Add(cc);
                }
            }

            if (bcc.Trim() != string.Empty)
            {
                bcc = bcc.Replace(';', ',');

                if (bcc != "")
                {
                    if (bcc.LastIndexOf(',') != -1)
                    {
                        foreach (string bccAddress in bcc.Split(','))
                            message.Bcc.Add(new MailAddress(bccAddress));
                    }
                    else
                        message.Bcc.Add(bcc);
                }
            }

            message.Subject = subject;
            message.Body = body;

            if (attachments.Trim() != string.Empty)
            {
                if (attachments.LastIndexOf(',') != -1)
                {
                    foreach (string attachFile in attachments.Split(','))
                        message.Attachments.Add(new Attachment(attachFile));
                }
                else
                    message.Attachments.Add(new Attachment(attachments));
            }

            message.IsBodyHtml = true;

            //LinkResource(message, body);

            SmtpClient smtp = new SmtpClient();

            smtp.Host = sHost;
            if (iPort > 0)
                smtp.Port = iPort;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(sUsername, sPassword);
            smtp.EnableSsl = bEnableSsl;

            smtp.Send(message);

            message.Attachments.Dispose();

            return string.Empty;
        }
        catch (Exception ex)
        {
            message.Attachments.Dispose();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            while (ex != null)
            {
                HttpContext.Current.Trace.Write(ex.Message);
                sb.Append(ex.Message + "<br/>");

                ex = ex.InnerException;
            }

            return sb.ToString();
        }
    }
    public bool CheckNetStatus()
    {
        System.Uri Url = new System.Uri("http://www.microsoft.com");
        System.Net.WebRequest WebReq;
        System.Net.WebResponse Resp;
        WebReq = System.Net.WebRequest.Create(Url);

        try
        {
            Resp = WebReq.GetResponse();
            Resp.Close();
            WebReq = null;
            return true;
        }
        catch
        {
            WebReq = null;
            return false;
        }
    }

    public DataSet FillChkBoxList()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCL"));

        return ExecuteDataSet("HRspMailSettings", arraylst);
    }

    public bool IsMailexists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CMS"));

        return (Convert.ToInt32(ExecuteScalar("HRspMailSettings", alParameters)) > 0 ? true : false);
    }

    public void AppendHeader(MailMessage message)
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        clsReport objReport = new clsReport();

        DataTable dt = objReport.GetReportHeader();

        if (dt.Rows.Count > 0)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellpadding='0' cellspacing='0' class='style1' style='font-family: Verdana; border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #660033; margin-bottom: 5px;'>");
            sb.Append("<tr valign='bottom'>");
            sb.Append("<td width='100%' style='padding-bottom: 5px'>");

            LinkedResource logo = null;

            if (dt.Rows[0]["Logo"] != DBNull.Value)
            {
                logo = new LinkedResource(new MemoryStream((byte[])dt.Rows[0]["Logo"]), MediaTypeNames.Image.Jpeg);

                logo.ContentId = Guid.NewGuid().ToString("N");

                sb.Append("<img alt='' src='cid:" + logo.ContentId + "' style='padding: 2px; border: 1px solid #D1D1D1;' /></td>");
            }
            else
                sb.Append("&nbsp;</td>");

            sb.Append("<td style=\"padding-bottom: 5px\">");
            sb.Append("<table class=\"style1\">");
            sb.Append("<tr>");
            sb.Append("<td align=\"right\">");
            sb.Append("<h3 style=\"padding: 0px; margin: 0px; color: #880000\">" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</h3></td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine1"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["AddressLine2"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td style=\"font-size: x-small; line-height: 15px;\" align=\"right\">" + Convert.ToString(dt.Rows[0]["Phone"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr>");
            sb.Append("</table>"); 

            message.Body = sb.ToString() + message.Body;

            if (dt.Rows[0]["Logo"] != DBNull.Value)
            {
                AlternateView avMessage = AlternateView.CreateAlternateViewFromString(message.Body, null, MediaTypeNames.Text.Html);

                avMessage.LinkedResources.Add(logo);

                message.AlternateViews.Add(avMessage);
            }
        }
    }
}
