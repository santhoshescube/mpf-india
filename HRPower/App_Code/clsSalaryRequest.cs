﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSalaryRequest
/// </summary>
public class clsSalaryRequest:DL 
{
    private int iEmployeeId, iStatusId, iRequestId, iMonth, iYear, iPageIndex, iPageSize, iCompanyId;
    private string sMode, sReason, sRequestedTo,sApprovedBy;
    private double dAmount;
    private DateTime dtRequestedDate;
    private string sDateVal;
    private int iCreditHeadID, iDebitHeadID,iUserId;
	public clsSalaryRequest()
	{
		//
		// TODO: Add constructor logic here
		//
    }
    #region Properties
    public int EmployeeId
    {
        set { iEmployeeId = value; }
        get { return iEmployeeId;  }
    }
    public string Mode
    {
        set { sMode = value;  }
        get { return sMode;   }
    }
    public DateTime  RequestedDate
    {
        set { dtRequestedDate  = value; }
        get { return dtRequestedDate ;  }
    }
    public double  Amount
    {
        set { dAmount  = value; }
        get { return dAmount;   }
    }
    public string Reason
    {
        set { sReason  = value; }
        get { return sReason;   }
    }
    public string RequestedTo
    {
        set { sRequestedTo = value; }
        get { return sRequestedTo; }
    }
    public string ApprovedBy
    {
        set { sApprovedBy  = value; }
        get { return sApprovedBy ; }
    }
    public int StatusId
    {
        set { iStatusId  = value; }
        get { return iStatusId ; }
    }
    public int RequestId
    {
        set { iRequestId  = value; }
        get { return iRequestId  ; }
    }
    public int CompanyId
    {
        set { iCompanyId = value; }
        get { return iCompanyId; }
    }
    public int Month
    {
        set { iMonth = value; }
        get { return iMonth; }
    }
    public int Year
    {
        set { iYear = value; }
        get { return iYear; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public string DateVal
    {
        get { return sDateVal; }
        set { sDateVal = value; }
    }
    public int CreditHeadID
    {
        get { return iCreditHeadID; }
        set { iCreditHeadID = value; }
    }
    public int DebitHeadID
    {
        get { return iDebitHeadID; }
        set { iDebitHeadID = value; }
    }
    public int UserId
    {
        get { return iUserId; }
        set { iUserId = value; }
    }

    #endregion

    #region Functions


  
   /// <summary>
   /// This function is used to insert and update details.
   /// </summary>
   ///<CreatedBy>Jisha Bijoy</CreatedBy>
   ///<CreatedOn>8 Oct 2010</CreatedOn>
   ///<ModifiedBy></ModifiedBy>
   ///<ModifiedOn></ModifiedOn>
    public int InsertRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode",sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@RequestedDate", dtRequestedDate));
        alParameters.Add(new SqlParameter("@Amount", dAmount));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo ));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        if(iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));
        return Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));
      
    }

    /// <summary>
    /// Get requested details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex",iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParameters);
    }

    /// <summary>
    /// Get request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParameters);
    }
    
    /// <summary>
    /// get maximum salary advance limit 
    /// </summary>
    /// <returns>double advance amount.</returns>
    public double GetAdvanceAmount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AMT"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        SqlDataReader oReader = ExecuteReader("HRspSalaryAdvanceRequest", alParameters);
        double dAdvanceAmt = 0;
        if (oReader.Read())
        {
            dAdvanceAmt = Convert.ToDouble (oReader["Amount"]);
        }
        oReader.Close();
        return dAdvanceAmt;
    }

    public double GetAdvaceAmountForThecurrentMonth()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ACM"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Month", iMonth));
        alParameters.Add(new SqlParameter("@Year", iYear));
        SqlDataReader oReader = ExecuteReader("HRspSalaryAdvanceRequest", alParameters);
        double dAdvanceAmtPerMonth = 0;
        if (oReader.Read())
        {
            dAdvanceAmtPerMonth = Convert.ToDouble(oReader["Amount"]);
        }
        oReader.Close();
        return dAdvanceAmtPerMonth;
    }

    public DataTable GetAdvaceAmountAndPercentage()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "SLL"));
        alParemeters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParemeters);
    }

    /// <summary>
    /// Total requested amount in selected month
    /// </summary>
    /// <returns></returns>
    public double GetMonthTotalAdvance()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "TADVAMT"));
        alParemeters.Add(new SqlParameter("@Month", iMonth));
        alParemeters.Add(new SqlParameter("@Year", iYear));
        alParemeters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        SqlDataReader oReader = ExecuteReader("HRspSalaryAdvanceRequest", alParemeters);
        double dTotalAdvAmount = 0;
        if (oReader.Read())
        {
            dTotalAdvAmount = Convert.ToDouble(oReader["Amount"]);
        }
        oReader.Close();
        return dTotalAdvAmount;
    }

    public double GetLastAdvaceAmount()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "LSTADV"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        SqlDataReader oReader = ExecuteReader("HRspSalaryAdvanceRequest", alParemeters);
        double dLastAdvAmount = 0;
        if (oReader.Read())
        {
            dLastAdvAmount = Convert.ToDouble(oReader["Amount"]);
        }
        oReader.Close();
        return dLastAdvAmount;
    }
    public bool CheckDuplication()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EXT"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Month", iMonth ));
        alParameters.Add(new SqlParameter("@Year", iYear ));
        alParameters.Add(new SqlParameter("@RequestId",iRequestId));

        int iCount = Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));
        if (iCount > 0)
            return true;
        else
            return false;

    }
    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));
        return iCount;
    }
    /// <summary>
    /// delete selected requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));
        ExecuteNonQuery("HRspSalaryAdvanceRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsSalaryRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters)) > 0 ? true : false);
    }

    public DataTable GetTotalSalaryAdvance(long lngEmployeeID, DateTime dtFromDate, DateTime dtToDate)
    {
        ArrayList  parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 10));
        parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        parameters.Add(new SqlParameter("@FromDate", dtFromDate));
        parameters.Add(new SqlParameter("@ToDate", dtToDate));
        return ExecuteDataTable("spPaySalaryAdvance", parameters);
    }

    public long CheckEmployeeSalaryIsProcessed(long lngEmployeeID, DateTime dtDate)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 11));
        parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        parameters.Add(new SqlParameter("@Date", dtDate));
        return Convert.ToInt64(ExecuteScalar("spPaySalaryAdvance", parameters));
    }
    /// <summary>
    /// get requested month loan installment amount
    /// </summary>
    /// <returns></returns>
    public DataSet  GetLoanAmount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GLA"));
        alParameters.Add(new SqlParameter("@EmployeeId", Convert.ToInt64(EmployeeId)));
        alParameters.Add(new SqlParameter("@month", iMonth));
        alParameters.Add(new SqlParameter("@year", iYear));

        return ExecuteDataSet("HRspSalaryAdvanceRequest", alParameters);

    }

    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateAdvanceStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspSalaryAdvanceRequest");

        try
        {
            ArrayList alParemeters = new ArrayList();
            alParemeters.Add(new SqlParameter("@Mode", "UPS"));
            alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
            alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
            alParemeters.Add(new SqlParameter("@ApprovedBy", sApprovedBy));
            alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));           
            alParemeters.Add(new SqlParameter("@Reason", Reason));
            if (IsAuthorised && iStatusId == 5)
            {
                alParemeters.Add(new SqlParameter("@RequestedDate", dtRequestedDate));
                alParemeters.Add(new SqlParameter("@Amount", dAmount));                
                alParemeters.Add(new SqlParameter("@UserId", iUserId));
                //alParemeters.Add(new SqlParameter("@Reason", sReason));
            }
            else if (iStatusId != 3 && !IsAuthorised )
                alParemeters.Add(new SqlParameter("@Forwarded", 1));
            alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));

            ExecuteNonQuery(alParemeters);
            this.CommitTransaction();
        }
        catch
        {
             //this.RollbackTransaction();
        }
    }

    //public double GetRequestedAmount(int iRequestId)
    //{
    //    ArrayList alParemeters = new ArrayList();
    //    alParemeters.Add(new SqlParameter("@Mode", "GRA"));
    //    alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

    //    return Convert.ToDouble(ExecuteScalar("HRspSalaryAdvanceRequest", alParemeters));
    //}

    public int GetRequestedById()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GBI"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParemeters));
    }


    public DataTable GetRequestedBuddyOfficalEmail()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GRB"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParemeters);
    }

  

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@Reason", Reason));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));
        ExecuteNonQuery("HRspSalaryAdvanceRequest", alParameters);
    }


    public void SalaryAdvanceRequestCancelled()
    {
        this.BeginTransaction("HRspSalaryAdvanceRequest");

        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "CLR"));
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));
            alParameters.Add(new SqlParameter("@StatusId", iStatusId));
            alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
            alParameters.Add(new SqlParameter("@Reason", Reason));
            alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));
            ExecuteNonQuery(alParameters);
            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }
    /// <summary>
    /// Get workstatusId to check whether the employee is in Service
    /// </summary>
    /// <returns></returns>
    public int GetWorkStatusId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CWS"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));
    }

    ///<summary>
    ///To check requested date is greater than dateofjoining
    ///</summary>
    public bool CheckDateOfJoining()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDJ"));
        alParameters.Add(new SqlParameter("@RequestedDate", dtRequestedDate));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        //alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return Convert.ToBoolean(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));
    }

    ///<summary>
    /// To check Employee's salary is already processed or not
    ///</summary>
    public bool SalaryProcessExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        alParameters.Add(new SqlParameter("@DateVal", sDateVal));

        return Convert.ToBoolean(ExecuteScalar("SalaryProcessExists", alParameters));

    }

    ///<summary>
    ///Bind dropdown ddlDebitHeadID
    ///</summary>
    //public DataSet BindDebitHeadID(int RequestID)
    //{
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@ParentID", 26));
    //    alParameters.Add(new SqlParameter("@AccountID", 0));

    //    int iCompID = GetCompanyID(RequestID);
    //    alParameters.Add(new SqlParameter("@CompID", iCompID));

    //   //  parameters.Add(New SqlParameter("@AccountID", iNavDebitAccID))
    //  //   parameters.Add(New SqlParameter("@CompID", iCmpId))

    //    return ExecuteDataSet("PayGetAccounts", alParameters);
    //}

    //public DataSet BindCreditHeadID(int RequestID)
    //{
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@ParentID", 31));
    //    alParameters.Add(new SqlParameter("@AccountID", 0));

    //    int iCompID = GetCompanyID(RequestID);
    //    alParameters.Add(new SqlParameter("@CompID", iCompID));

    //    return ExecuteDataSet("PayGetAccounts", alParameters);
    //}

    private int GetCompanyID(int iRequestId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", alParameters));

    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }
    /// <summary>
    /// get approval date
    /// </summary>
    /// <returns></returns>
    public DataTable GetApprovalDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParameters);
    }

    public DataTable DisplayReasons(int iRequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.SalaryAdvance));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspSalaryAdvanceRequest", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", iCompanyId));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspSalaryAdvanceRequest", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GCI"));
        parameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspSalaryAdvanceRequest", parameters));
    }
    #endregion
}
