﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsVisaTicketProcessing
/// </summary>
public class clsVisaTicketProcessing:DL
{
	public clsVisaTicketProcessing()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region Variables

    public static string Procedure = "HRspVisaTicketProcessing";

    #endregion

    #region Properties
      
    public int VisaTicketProcessID {get; set;}
    public int CandidateID{get; set;}
    //public int StatusID {get; set;}
    public int CompanyID { get; set; }
    //Visa
    //public bool VisaSentStatus {get; set;}
    //public DateTime? VisaSentDate { get; set; }
    //public int VisaTypeID { get; set; }
    //public string VisaNumber { get; set; }
    //public string PlaceofIssue { get; set; }
    //public DateTime? VisaIssueDate { get; set; }
    //public DateTime? VisaValidityDate { get; set; }
    //public DateTime? VisaExpiryDate { get; set; }
    //public decimal VisaAmount { get; set; }
    public string VisaRemarks { get; set; }
    //Ticket
    public bool TicketSent {get; set;}
    public  DateTime? TicketSentdate {get; set;}
    public DateTime? Arrivaldate  {get; set;}
    public string ArrivalTime { get; set; }
    public int VenueID  {get; set;}
    public bool PickUpReq {get; set;}
    public decimal TicketAmount { get; set; }
    public string TicketRemarks { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }

    #endregion

    #region Methods

    public static DataTable GetCandidateNames(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GCN"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetProcessingStatus()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GPS"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetVisaTypes()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GVT"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static DataTable GetVenue()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GV"));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public int Save()
    {
        ArrayList arr = new ArrayList();        

        if(VisaTicketProcessID == 0)
            arr.Add(new SqlParameter("@Mode", "INS"));
        else
        {
            arr.Add(new SqlParameter("@Mode", "UPD"));
            arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
        }
        arr.Add(new SqlParameter("@CandidateID", CandidateID));
        //arr.Add(new SqlParameter("@StatusID", StatusID));

        //arr.Add(new SqlParameter("@VisaSentStatus", VisaSentStatus));
        //arr.Add(new SqlParameter("@VisaSentDate", VisaSentDate));
        //arr.Add(new SqlParameter("@VisaNumber", VisaNumber));
        //arr.Add(new SqlParameter("@VisaTypeID", VisaTypeID));
        //arr.Add(new SqlParameter("@PlaceofIssue", PlaceofIssue));
        //arr.Add(new SqlParameter("@VisaIssueDate", VisaIssueDate));
        //arr.Add(new SqlParameter("@VisaValidityDate", VisaValidityDate));
        //arr.Add(new SqlParameter("@VisaExpiryDate", VisaExpiryDate));
        //arr.Add(new SqlParameter("@VisaAmount", VisaAmount));
        arr.Add(new SqlParameter("@VisaRemarks", VisaRemarks));

        arr.Add(new SqlParameter("@TicketSent", TicketSent));
        arr.Add(new SqlParameter("@TicketSentdate", TicketSentdate));
        arr.Add(new SqlParameter("@Arrivaldate", Arrivaldate));
        arr.Add(new SqlParameter("@ArrivalTime", ArrivalTime));
        arr.Add(new SqlParameter("@VenueID", VenueID));
        arr.Add(new SqlParameter("@PickUpReq", PickUpReq));
        arr.Add(new SqlParameter("@TicketAmount", TicketAmount));
        arr.Add(new SqlParameter("@TicketRemarks", TicketRemarks));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static int GetTotalRecordCount(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GRC"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32(); 
    }

    public DataTable GetAllRecords(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAR"));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@PageIndex", PageIndex));
        arr.Add(new SqlParameter("@PageSize", PageSize));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, arr);
    }

    public DataSet GetSingleRecord(int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GSR"));
        arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, arr);
    }

    public bool Delete()
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DEL"));
        arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
        return ExecuteScalar(Procedure, arr).ToInt32() > 0 ? false : true;
    }

    //Ticket Details Updation for Candidate Home

    public int UpdateTicketDetails(int VisaTicketProcessID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "UTD"));
        arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
        arr.Add(new SqlParameter("@Arrivaldate", Arrivaldate));
        arr.Add(new SqlParameter("@ArrivalTime", ArrivalTime));
        arr.Add(new SqlParameter("@VenueID", VenueID));
        arr.Add(new SqlParameter("@PickUpReq", PickUpReq));
        arr.Add(new SqlParameter("@TicketAmount", TicketAmount));    
        arr.Add(new SqlParameter("@TicketRemarks", TicketRemarks));        
        return ExecuteScalar(Procedure, arr).ToInt32();
    }

    #endregion


    public int DeleteVisaDetails(int VisaTicketProcessID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DVD"));
        arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
        return ExecuteScalar(Procedure, arr).ToInt32();
    }

    public int SaveVisaDetails(List<clsVisaDetails> visaDetails, int VisaTicketProcessID)
    {
        try
        {
            foreach (clsVisaDetails objVisa in visaDetails)
            {
                ArrayList arr = new ArrayList();
                arr.Add(new SqlParameter("@Mode", "IVD"));
                arr.Add(new SqlParameter("@VisaTicketProcessID", VisaTicketProcessID));
                arr.Add(new SqlParameter("@StatusID", objVisa.StatusID));
                //arr.Add(new SqlParameter("@VisaNumber", objVisa.VisaNumber));
                //arr.Add(new SqlParameter("@VisaTypeID", objVisa.VisaTypeID));
                arr.Add(new SqlParameter("@VisaIssueDate", objVisa.VisaIssueDate));
                //arr.Add(new SqlParameter("@VisaValidityDate", objVisa.VisaValidityDate));
                arr.Add(new SqlParameter("@VisaExpiryDate", objVisa.VisaExpiryDate));
                arr.Add(new SqlParameter("@VisaAmount", objVisa.VisaAmount));
                arr.Add(new SqlParameter("@VisaDetailsRemarks", objVisa.VisaDetailsRemarks));
                arr.Add(new SqlParameter("@IsAlertRequired", objVisa.IsAlertRequired));
                ExecuteScalar(Procedure, arr).ToInt32();
            }
            return 1;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public int CheckCandidateDuplication(int CandidateID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "CCD"));        
        arr.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteScalar(Procedure, arr).ToInt32();
    }
}

[Serializable]
public class clsVisaDetails
{
    //public int VisaTicketProcessID1 { get; set; }
    public int StatusID { get; set; }
    //public string VisaNumber { get; set; }
    //public int VisaTypeID { get; set; }    
    public string VisaIssueDate{ get; set; }
    //public string VisaValidityDate { get; set; }
    public string VisaExpiryDate { get; set; }
    public decimal VisaAmount { get; set; }
    public string VisaDetailsRemarks { get; set; }
    public bool IsAlertRequired { get; set; }
    public int VisaID { get; set; }
}
