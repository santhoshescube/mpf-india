﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsInterviewSchedule
/// </summary>
public class clsInterviewSchedule : DL
{
    public long JobScheduleID { get; set; }
    public int JobID { get; set; }
    public int JobLevelID { get; set; }
    public string ScheduleDate { get; set; }
    public string ScheduleStartTime { get; set; }
    public string Duration { get; set; }
    public string Venue { get; set; }
    public Int16 MaximumCandidates { get; set; }
    public Int16 NoOfInterviewers { get; set; }

    public int CandidateID { get; set; }
    public int InterviewerID { get; set; }
    public string CandidateIDs { get; set; }
    public string InterviewerIDs { get; set; }
    public string @ScheduledTime { get; set; }
    public string @ScheduledTimes { get; set; }

    public int PageSize { get; set; }
    public int PageIndex { get; set; }
    public string SortExpression { get; set; }
    public string SortOrder { get; set; }

    public int CreatedBy { get; set; }
    public int CompanyID { get; set; }

	public clsInterviewSchedule()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int GetRecordCount() // Get Total Record count
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "RC"));
        alParameters.Add(new SqlParameter("@ScheduleDate", ScheduleDate));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspSchedule", alParameters));
    }
    public DataSet GetAllInterviewSchedules()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FAS"));
        alParameters.Add(new SqlParameter("@ScheduleDate", ScheduleDate));

        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        //alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        //alParameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        //alParameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        //alParameters.Add(new SqlParameter("@ScheduledById", iScheduledById));
        //alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspSchedule", alParameters);
    }

    public DataSet FillJobSchedules()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FS"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@ScheduleDate", ScheduleDate));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1: 0));
        return ExecuteDataSet("HRspSchedule", alParameters);
    }

    public DataSet PrintSchedule()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "PRNT"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));

        return ExecuteDataSet("HRspSchedule", alParameters);
    }

    public DataTable FillJobes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FJ"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID ));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyID));
        return ExecuteDataTable("HRspSchedule", alParameters);

    }

    public DataTable FillJobLevels()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SJL"));
        alParameters.Add(new SqlParameter("@JobID", JobID ));
        alParameters.Add(new SqlParameter ("@IsArabic",clsGlobalization.IsArabicCulture() ? 1:0));
        return ExecuteDataTable("HRspSchedule", alParameters);

    }

    public DataTable FillInterviewers()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FI"));
        alParameters.Add(new SqlParameter("@RoleID", (int)UserRoles.Interviewer));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JobLevelID", JobLevelID));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyID));
        return ExecuteDataTable("HRspSchedule", alParameters);

    }

    public DataTable FillCandidates()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FC"));
        alParameters.Add(new SqlParameter("@MaximumCandidates", MaximumCandidates));
        alParameters.Add(new SqlParameter("@ScheduleStartTime", ScheduleStartTime));
        alParameters.Add(new SqlParameter("@Duration", Duration));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JobLevelID", JobLevelID ));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()? 1:0));
        return ExecuteDataTable("HRspSchedule", alParameters);
    }

    public DataTable FillCandidatesForView()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FASC"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspSchedule", alParameters);
    }

    public DataTable FillCandidatesForMail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FCM"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return ExecuteDataTable("HRspSchedule", alParameters);
    }

    public DataTable FillCandidatesForMail(Int64 JobScheduleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FCM"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return ExecuteDataTable("HRspSchedule", alParameters);
    }
    private DataTable FillInterviewersForMail(Int64 JobScheduleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FIM"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return ExecuteDataTable("HRspSchedule", alParameters);
    }
    public DataTable GetFromMail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GMS"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return ExecuteDataTable("HRspSchedule", alParameters);

    }

    public int DeleteSchedule()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DS"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));

        return ExecuteScalar("HRspSchedule", alParameters).ToInt32();

    }

    public long SaveSchedule()
    {
        try
        {
            long ScheduleID = 0;
            BeginTransaction("HRspSchedule");

            ArrayList alParameters = new ArrayList();

            alParameters.Add(new SqlParameter("@Mode", "IS"));
            alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
            alParameters.Add(new SqlParameter("@JobID", JobID));
            alParameters.Add(new SqlParameter("@JobLevelID", JobLevelID ));
            alParameters.Add(new SqlParameter("@ScheduleDate", ScheduleDate));
            alParameters.Add(new SqlParameter("@ScheduleStartTime", ScheduleStartTime));
            alParameters.Add(new SqlParameter("@Duration", Duration));
            alParameters.Add(new SqlParameter("@Venue", Venue));

            alParameters.Add(new SqlParameter("@CandidateIDs", CandidateIDs));
            alParameters.Add(new SqlParameter("@ScheduledTime", ScheduledTime));
            alParameters.Add(new SqlParameter("@InterviewerIDs", InterviewerIDs));
            alParameters.Add(new SqlParameter("@ScheduledTimes", ScheduledTimes));
            alParameters.Add(new SqlParameter("@CreatedBy", CreatedBy));

            ScheduleID = ExecuteScalar("HRspSchedule", alParameters).ToInt64();

            CommitTransaction();
            return ScheduleID;
        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return 0;
        }
    }

    public long UpdateSchedule()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UJS"));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));

        return ExecuteScalar("HRspSchedule", alParameters).ToInt64();

    }

    public void SendMail(string sFrom, long ScheduleID)
    {
        try
        {
            string sBoby = string.Empty;
            clsMailSettings objMailSettings = new clsMailSettings();
            DataTable dtCandidates = FillCandidatesForMail(ScheduleID);
            DataTable dtInterviewers = FillInterviewersForMail(ScheduleID);

            // Sending mail for Candidates
            foreach (DataRow drCAndidates in dtCandidates.Rows)
            {
                StringBuilder sbMessageBody = new StringBuilder();

                sbMessageBody.Append("<div style='padding-left:10px;width:500px;min-height:300px;height:auto; border-style:solid;border-width:1px; border-width:1px;font-family: Tahoma; font-size:12px; vertical-align: top'>");
                //sbMessageBody.Append("<div style='padding-left:10px;width:500px; border-style:solid; '>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;text-align:center;font-weight: bold'>Interview Letter</div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;margin-top:10px;'><div style='width:35%;height:15px;float:left;'> Job Title</div><div style='width:5%;height:15px;float:left;'>:</div><div style='width:60%;height:15px;float:left;'>" + drCAndidates["JobTitle"] + "</div></div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;'><div style='width:35%;height:15px;float:left;'>Time</div><div style='width:5%;height:15px;float:left;'>:</div><div style='width:60%;height:15px;float:left;'>" + drCAndidates["ScheduleDate"] + " At" + drCAndidates["ScheduledTime"] + "</div></div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;'><div style='width:35%;height:15px;float:left;'>Venue</div><div style='width:5%;height:15px;float:left;'>:</div><div style='width:60%;height:15px;float:left;'> " + drCAndidates["Venue"] + "</div></div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;'><div style='width:35%;height:15px;float:left;'>Phone No</div><div style='width:5%;height:15px;float:left;'>:</div><div style='width:60%;height:15px;float:left;'> " + drCAndidates["Telephone"] + "</div></div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;'><div style='width:35%;height:15px;float:left;'>Website</div><div style='width:5%;height:15px;float:left;'>:</div><div style='width:60%;height:15px;float:left;'> " + drCAndidates["Website"] + "</div></div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;margin-top:10px;'>Dear " + drCAndidates["EngName"] + ",</div>");
                sbMessageBody.Append("<div style='width:100%;height:auto;float:left;margin-top:10px;'>Please be present for the interview scheduled on " + drCAndidates["ScheduleDate"] + " @  " + drCAndidates["ScheduledTime"] + " for the post of " + drCAndidates["JobTitle"] + ",Your Username is : " + drCAndidates["Username"] + " and Password is : " + clsCommon.Decrypt(Convert.ToString(drCAndidates["Password"]), ConfigurationManager.AppSettings["_ENCRYPTION"]) +" </div>");
                sbMessageBody.Append("<div style='width:100%;height:15px;float:left;margin-top:10px;'>Thanks & Regards</div>");
                sbMessageBody.Append("</div>");

                //-------------------- not needed

                if (sFrom != "" && drCAndidates["Email"].ToString() != "")
                {
                    if (objMailSettings.SendMail(sFrom, drCAndidates["Email"].ToString(), "Schedule Details", sbMessageBody.ToString(), clsMailSettings.AccountType.Email, false))
                    {
                        JobScheduleID = ScheduleID;
                        CandidateID = drCAndidates["CandidateID"].ToInt32();
                        UpdateSchedule();

                    }
                }
            }
            // Sending mail for Interviewer
            foreach (DataRow drInt in dtInterviewers.Rows)
            {
                StringBuilder sbMessageBody = new StringBuilder();

                sbMessageBody.Append("<div style='padding-left:10px;width:500px;'>");
                sbMessageBody.Append("<table width='100%' border='0' cellpadding='3' style='font-family: Tahoma; font-size:12px; vertical-align: top'>");

                sbMessageBody.Append("<tr>");
                sbMessageBody.Append("<td style='padding:7px 5px;'> Dear " + drInt["Employee"] + ",");
                sbMessageBody.Append("</td>");
                sbMessageBody.Append("</tr>");

                sbMessageBody.Append("<tr>");
                sbMessageBody.Append("<td style='padding:7px 5px;' >Scheduled interview on  " + drInt["ScheduleDate"] + " for " + drInt["CandidateCnt"] + " candidates for the post of " + drInt["JobTitle"] + ",");
                sbMessageBody.Append("</td>");
                sbMessageBody.Append("</tr>");

                //sbMessageBody.Append("<tr>");
                //sbMessageBody.Append("<td style='padding:7px 5px;' >Your Username is : " + drCAndidates["Username"] + " and Password is : " + clsCommon.Decrypt(Convert.ToString(drCAndidates["Password"]), ConfigurationManager.AppSettings["_ENCRYPTION"]) + ",");
                //sbMessageBody.Append("</td>");
                //sbMessageBody.Append("</tr>");

                sbMessageBody.Append("<tr>");

                sbMessageBody.Append("<td style='padding: 7px 5px;'>Thanks & Regards</td>");
                sbMessageBody.Append("</tr>");

                sbMessageBody.Append("</table>");
                sbMessageBody.Append("</div>");

                //-------------------- not needed

                if (sFrom != "" && drInt["Email"].ToString() != "")
                {
                    if (objMailSettings.SendMail(sFrom, drInt["Email"].ToString(), "Schedule Details", sbMessageBody.ToString(), clsMailSettings.AccountType.Email, false))
                    {

                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
    }


    public static DataTable GetAllNewInterviewSchedules()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));

        return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);

    }

    public static DataTable GetMoreCandidates(int JobID,int JoblevelID,int DegreeID,string Skills)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "MC"));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JobLevelID", JoblevelID ));
        alParameters.Add(new SqlParameter("@DegreeID", DegreeID ));
        alParameters.Add(new SqlParameter("@Skills", Skills));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 :0));
        return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);
    }

    public static DataTable GetSelectedCandidates(string CandidateIDs,int JobID,int JobLevelID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SC"));
        alParameters.Add(new SqlParameter("@CandidateIDs", CandidateIDs));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JoblevelID", JobLevelID ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);
    }

    public static int IsBoard(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CJT"));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar("HRspSchedule", alParameters).ToInt32();
    }
    public static DataTable GetJobDetails(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GJD"));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);
    }

    public static bool  IsExists(long JobScheduleID, long CandidateID,string Time,int JobID ,int JobLevelID,string ScheduledDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CCT"));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@JobLevelID", JobLevelID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@ScheduledTime", Time));
        alParameters.Add(new SqlParameter("@ScheduleDate", ScheduledDate));
        return new DataLayer().ExecuteScalar("HRspSchedule", alParameters).ToInt32() > 0? true :false ;
    }

    public static int IsCandidateScheduled(long CandidateID,int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CCS"));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
     
        return new DataLayer().ExecuteScalar("HRspSchedule", alParameters).ToInt32() ;
    }

    public static DataTable FillQualification()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FQ"));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 :0));
        return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);
    }

    //public static DataTable FillTemplates()
    //{
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@Mode", "GT"));
    //    return new DataLayer().ExecuteDataTable("HRspSchedule", alParameters);
    //}
}
