﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptEmployees
/// </summary>
public class clsRptEmployees
{
    public clsRptEmployees()
    {
    }

    public static DataTable GetAllCompanies(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 1));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }


    public static DataTable GetAllBranches(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 2));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }


    public static DataTable GetAllEmployees(int CompanyID,int BranchID,bool IncludeCompany)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 3));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@BranchID", BranchID));
        prmDocuments.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }

    public static DataTable GetAllDesignation()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 4));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }

  

    public static DataTable GetAllDepartments()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 5));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }
    public static DataTable GetAllWorkStatus()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 6));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }
    public static DataTable GetAllEmploymentType()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode",7));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }

    public static DataTable GetBranches(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 8));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }

    public static DataTable GetDesignation()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 9));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }

    public static DataTable GetEmploymentType()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 10));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);

    }

    public static DataTable GetALLLocation()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 11));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }

    public static DataTable GetEmployees(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 12));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }

    public static DataTable GetCompanies(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 13));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetEmployees()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 14));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetAllDeductions()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 15));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetAllEmployees(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 16));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetDocumentTypes()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 17));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetAllAssetTypes()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 18));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetAllAssets(int AssetTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 19));
        prmDocuments.Add(new SqlParameter("@AssetTypeID", AssetTypeID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }
    public static DataTable GetEmp(int CompanyID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 20));
        prmDocuments.Add(new SqlParameter("@CompanyID", CompanyID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }


    public static DataTable GetAllStatus()
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 21));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }


    public static DataTable GetEssType( int EssTypeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 22));
        prmDocuments.Add(new SqlParameter("@EssTypeID", EssTypeID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpRptEmployeeNew", prmDocuments);
    }

    public static int GetCompanyIDByEmployee(int EmployeeID)
    {
        ArrayList prmDocuments = new ArrayList();
        prmDocuments.Add(new SqlParameter("@Mode", 23));
        prmDocuments.Add(new SqlParameter("@EmployeeID", EmployeeID));
        prmDocuments.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteScalar("HRSpRptEmployeeNew", prmDocuments).ToInt32();
    }
}


