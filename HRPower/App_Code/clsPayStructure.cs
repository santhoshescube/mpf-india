﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsPayStructure
/// </summary>
public class clsPayStructure : DL
{
	public clsPayStructure()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public int JobSalaryID { get; set; }
    public string PayStructure { get; set; }
    public int DesignationID { get; set; }
    public int PaymentClassificationID { get; set; }
    public decimal GrossPay { get; set; }
    public int AdditionDeductionID { get; set; }
    public decimal Amount { get; set; }

    public void BeginEmp()
    {
        BeginTransaction("HRspPayStructure");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }

    public DataSet LoadCombos()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 1));
        return ExecuteDataSet("HRspPayStructure", alparameters);
    }

    public DataSet FillParticulars()   //Fill Dropdown Particulars
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 8));
        return ExecuteDataSet("HRspPayStructure", alparameters);
    }
}
