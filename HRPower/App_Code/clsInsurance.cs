﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;


/// <summary>
/// Summary description for clsInsurance
/// Created By:    Sanju
/// Created Date:  29-Oct-2013
/// Description:   Insurance
/// </summary>
public class clsInsurance : DL
{

    public int intInsuranceID { get; set; }
    public int intCompanyID { get; set; }
    public int intUserID { get; set; }
    public int intCompanyAssetID { get; set; }
    public string strPolicyNumber { get; set; }
    public string strPolicyName { get; set; }
    public decimal decPolicyAmount { get; set; }
    public int intInsuranceCompanyID { get; set; }
    public DateTime dteIssueDate { get; set; }
    public DateTime dteExpiryDate { get; set; }
    public DateTime dteRenewalDate { get; set; }
    public string strRemarks { get; set; }
    public bool blnIsRenewed { get; set; }

    public Int32 intPageIndex { get; set; }
    public Int32 intPageSize { get; set; }
    public int intNode { get; set; }
    public string strDocname { get; set; }
    public string strFilename { get; set; }
    public string strSearchKey { get; set; }

    public clsInsurance()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable SaveInsurance()
    {
        ArrayList alParameters = new ArrayList();
        if (intInsuranceID > 0)
        {
            alParameters.Add(new SqlParameter("@Mode", 2));
            alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", 1));
        }
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@CompanyAssetID", intCompanyAssetID));
        alParameters.Add(new SqlParameter("@PolicyNumber", strPolicyNumber));
        alParameters.Add(new SqlParameter("@PolicyName", strPolicyName));
        alParameters.Add(new SqlParameter("@InsuranceCompanyID", intInsuranceCompanyID));
        alParameters.Add(new SqlParameter("@IssueDate", dteIssueDate.ToString("dd MMM yyyy")));
        alParameters.Add(new SqlParameter("@ExpiryDate", dteExpiryDate.ToString("dd MMM yyyy")));
        alParameters.Add(new SqlParameter("@PolicyAmount", decPolicyAmount));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@IsRenewed", blnIsRenewed));
        return ExecuteDataTable("HRspInsurance", alParameters);

    }

    public DataSet GetReference()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspInsurance", alParameters);

    }

    public DataTable GetCompanyAsset()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspInsurance", alParameters);

    }
    public DataTable GetInsurance()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspInsurance", alParameters);

    }
    public DataTable GetMultipleInsurance(string MultipleInsuranceIDs)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@MultipleInsuranceID", MultipleInsuranceIDs));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspInsurance", alParameters);

    }
    public DataSet GetInsuranceToBind()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        alParameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", intPageSize));
        alParameters.Add(new SqlParameter("@SearchKey", strSearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspInsurance", alParameters);

    }
    public DataTable GetInsuranceCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        return ExecuteDataTable("HRspInsurance", alParameters);
    }
    public DataTable DeleteInsurance()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        return ExecuteDataTable("HRspInsurance", alParameters);
    }
    public bool isInsuranceDeletePossible()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        return (ExecuteScalar("HRspInsurance", alParameters).ToInt32() > 0 ? true : false);
    }
    public DataTable RenewInsurance()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        return ExecuteDataTable("HRspInsurance", alParameters);
    }
    public bool isPolicyNumberExist()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        alParameters.Add(new SqlParameter("@PolicyNumber", strPolicyNumber));
        return (ExecuteScalar("HRspInsurance", alParameters).ToInt32() > 0 ? true : false);
    }

    public DataSet GetInsuranceDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        return ExecuteDataSet("HRspInsurance", alParameters);
    }
    public void DeleteInsuranceDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@Node", intNode));
        ExecuteNonQuery("HRspInsurance", alParameters);
    }
    public DataTable InsertInsuranceTreemaster()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
        alParameters.Add(new SqlParameter("@PolicyNumber", strPolicyNumber));
        alParameters.Add(new SqlParameter("@Docname", strDocname));
        alParameters.Add(new SqlParameter("@Filename", strFilename));

        return ExecuteDataTable("HRspInsurance", alParameters);

    }

    //public DataTable UpdateInsuranceTreemaster()
    //{
    //    ArrayList alParameters = new ArrayList();

    //    alParameters.Add(new SqlParameter("@Mode", 15));
    //    alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));
    //    alParameters.Add(new SqlParameter("@AgreementNo", strAgreementNo));
    //    alParameters.Add(new SqlParameter("@Docname", strDocname));
    //    alParameters.Add(new SqlParameter("@Filename", strFilename));

    //    return ExecuteDataTable("HRspInsurance", alParameters);

    //}

    public DataTable GetInsuranceFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@InsuranceID", intInsuranceID));

        return ExecuteDataTable("HRspInsurance", alParameters);
    }

    public string GetInsurancePrint()
    {
        DataTable datData = GetInsurance();
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Insurance", 1);
        }
        return "";

    }
    public string GetMultipleInsurancePrint(string MultipleInsuranceIDs)
    {
        DataTable datData = GetMultipleInsurance(MultipleInsuranceIDs);
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Insurance", 2);
        }
        return "";

    }
    private string GetDocumentHtml(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        //string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        //string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";
        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
        //string strHeaderRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        //string strHeaderRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";


        //string strRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:rgb(232, 238, 240)'";
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";

        //string strRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        //string strRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        //string strRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        //string strAltRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        //string strRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; olor:#000000''";
        //string strAltRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        //string strRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">Company : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">Insurance Details</div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Asset</div><div " + strAltRowInnerRightStyle + ">" + DrData["Asset"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Policy Number</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Policy Name</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyName"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Insurance Company</div><div " + strAltRowInnerRightStyle + ">" + DrData["InsuranceCompany"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Policy Amount</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyAmount"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Issue Date</div><div " + strAltRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Expiry Date</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Remarks</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">Company</div><div " + strHeaderRowInner10Style + ">Asset</div><div " + strHeaderRowInner10Style + ">Policy Number</div><div " + strHeaderRowInner10Style + ">Policy Name</div><div " + strHeaderRowInner10Style + ">Insurance Company</div><div " + strHeaderRowInner10Style + ">Policy Amount</div><div " + strHeaderRowInner10Style + ">Issue Date</div><div " + strHeaderRowInner10Style + ">Expiry Date</div><div " + strHeaderRowInner20Style + ">Remarks</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Asset"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyNumber"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["InsuranceCompany"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyAmount"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["IssueDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ExpiryDate"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["Remarks"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }
}
