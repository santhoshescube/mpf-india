﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsPerformancetemplate
/// </summary>
public class clsPerformancetemplate : DL
{
    
    public List<clsPerformanceTemplatedetails> lstPerformanceTemplateDetails;
	public clsPerformancetemplate()
	{
        lstPerformanceTemplateDetails = new List<clsPerformanceTemplatedetails>();
	}

    public static DataTable BindDepartments()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "1"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceTemplate", alparameters);
    }
    public static DataTable BindGrades()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "2"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspPerformanceTemplate", alparameters);
    }
    public static DataTable GetQuestions()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "3"));
        return new DataLayer().ExecuteDataTable("HRspPerformanceTemplate", alparameters);
    }
    public int InsertUpdatePerformanceTemplate(string Templatename,int DepartmentID,bool IsGrading,decimal MaxMarks,string IsAdd,int EditTemplateID)
    {
        ArrayList alParameters = new ArrayList();

        if (IsAdd=="1")
        {
            alParameters.Add(new SqlParameter("@Mode", "4"));
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", "14"));
            alParameters.Add(new SqlParameter("@TemplateID", EditTemplateID));
        }

        
        alParameters.Add(new SqlParameter("@TemplateName", Templatename));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@IsGrading", IsGrading));
        alParameters.Add(new SqlParameter("@MaxMarksPerQuestion", MaxMarks));
       

        //if (iRequestId > 0)
        //    alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        int TemplateID = ExecuteScalar("HRspPerformanceTemplate", alParameters).ToInt32();

        if (TemplateID > 0)
        {
            if (IsAdd == "1")
            {
            }
            else
            {
                DeleteInsertTemplateDetails(TemplateID);
            }
            return TemplateID;
        }
        return 0;
    }
    public int InsertTemplateDetails(int PerformancetemplateID,int GoalID)
    {
        
        ArrayList prmGoal = new ArrayList();
       
            prmGoal.Add(new SqlParameter("@Mode", "6"));
            prmGoal.Add(new SqlParameter("@TemplateID", PerformancetemplateID));
            prmGoal.Add(new SqlParameter("@GoalID",GoalID));
            int PTemplateDetailID = ExecuteScalar("HRspPerformanceTemplate", prmGoal).ToInt32();
            if (PTemplateDetailID > 0)
                return PTemplateDetailID;
        return 0;
    }
    public bool InsertPerformanceTemplateDetails(int PerformancetemplateID)
    {
        clsPerformancetemplate objclsPerformancetemplate;
        ArrayList prmGoal = new ArrayList();
        foreach (clsPerformanceTemplatedetails objTemplateDetails in lstPerformanceTemplateDetails)
        {


            prmGoal.Add(new SqlParameter("@Mode", "6"));
            prmGoal.Add(new SqlParameter("@TemplatelID", PerformancetemplateID));
            prmGoal.Add(new SqlParameter("@GoalID", objTemplateDetails.GoalID));

            return ExecuteNonQuery("HRspPerformanceTemplate", prmGoal).ToBoolean();
        }
        return true;
    }
    public int SaveGoals(string Goal,int GoalID)
    {
        ArrayList prmGoal = new ArrayList();
      
      
        prmGoal.Add(new SqlParameter("@Mode", "5"));
        prmGoal.Add(new SqlParameter("@GoalID", GoalID));
        prmGoal.Add(new SqlParameter("@Goal", Goal));
        return Convert.ToInt32(ExecuteScalar("HRspPerformanceTemplate", prmGoal));

      
    }
    public static DataTable GetTemplateList(int PageIndex, int PageSize)
    {
         ArrayList alparameters = new ArrayList();
         alparameters.Add(new SqlParameter("@Mode", "7"));
         //alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture ()));
         alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
         alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
         alparameters.Add(new SqlParameter("@PageSize", PageSize));
        return new DataLayer().ExecuteDataTable("HRspPerformanceTemplate", alparameters);
    }
    public bool DeleteTemplate(int TemplateID)
    {
        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "8"));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
        return Convert.ToInt32(ExecuteScalar("HRspPerformanceTemplate", prmTemplate)) > 0;
    }
    public bool DeleteGoals(int TemplateID,int GoalID,string IsAdd)
    {
        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "9"));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
        prmTemplate.Add(new SqlParameter("@GoalID", GoalID));
        prmTemplate.Add(new SqlParameter("@IsAdd", IsAdd));
        return Convert.ToInt32(ExecuteScalar("HRspPerformanceTemplate", prmTemplate)) > 0;
    }
    public static int GetTotalRecordCount()
    {
        int TotalRecordCount = 0;

        TotalRecordCount = new DataLayer().ExecuteScalar("HRspPerformanceTemplate", new ArrayList
        {
            new SqlParameter("@Mode",10)
           
        }).ToInt32();

        return TotalRecordCount;
    }
    public  DataSet GetSingleTemplate(int TemplateID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "12"));
        alparameters.Add(new SqlParameter("@TemplateID", TemplateID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspPerformanceTemplate", alparameters);
    }
    public bool DeleteInsertTemplateDetails(int TemplateID)
    {
        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "13"));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
        return Convert.ToInt32(ExecuteScalar("HRspPerformanceTemplate", prmTemplate)) > 0;
    }
    public static bool IsGoalExists(int TemplateID, int GoalID,string IsAdd)
    {
       
        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "11"));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
        prmTemplate.Add(new SqlParameter("@GoalID", GoalID));
        prmTemplate.Add(new SqlParameter("@IsAdd",IsAdd));
        return  new DataLayer().ExecuteScalar("HRspPerformanceTemplate", prmTemplate).ToInt32() > 0;
    }
    public static bool IsPerformanceInitiationExists(int TemplateID)
    {

        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "15"));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
     
        return new DataLayer().ExecuteScalar("HRspPerformanceTemplate", prmTemplate).ToInt32() > 0;
    }
    public static bool IsGoalNameExists(string Goal,int GoalID)
    {
        ArrayList prmTemplate = new ArrayList();
        prmTemplate.Add(new SqlParameter("@Mode", "16"));
        prmTemplate.Add(new SqlParameter("@GoalID", GoalID));
        prmTemplate.Add(new SqlParameter("@Goal", Goal));
        return new DataLayer().ExecuteScalar("HRspPerformanceTemplate", prmTemplate).ToInt32() > 0;
    }
    public static bool IsTemplateNameExists(string TemplateName,int TemplateID,string IsAdd)
    {
        ArrayList prmTemplate = new ArrayList();
        prmTemplate.Add(new SqlParameter("@Mode", "17"));
        prmTemplate.Add(new SqlParameter("@TemplateName", TemplateName));
        prmTemplate.Add(new SqlParameter("@TemplateID", TemplateID));
        prmTemplate.Add(new SqlParameter("@IsAdd", IsAdd));
        return new DataLayer().ExecuteScalar("HRspPerformanceTemplate", prmTemplate).ToInt32() > 0;
    }
    public static DataTable ShowGoal(int GoalID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "19"));
        alparameters.Add(new SqlParameter("@GoalID", GoalID));
        return new DataLayer().ExecuteDataTable("HRspPerformanceTemplate", alparameters);
    }
    public static bool IsEvaluationExists(int GoalID)
    {

        ArrayList prmTemplate = new ArrayList();

        prmTemplate.Add(new SqlParameter("@Mode", "21"));
        prmTemplate.Add(new SqlParameter("@GoalID", GoalID));

        return new DataLayer().ExecuteScalar("HRspPerformanceTemplate", prmTemplate).ToInt32() > 0;
    }
}
public class clsPerformanceTemplatedetails
{
    public int GoalID { get; set; }
    public int TemplateID { get; set; }
}
