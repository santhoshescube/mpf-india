﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDocumentRequestNew
/// </summary>
public class clsDocumentRequestNew : DL
{
    #region Properties
    public int RequestID { get; set; }
    public long EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public int DocumentID { get; set; }
    public string DocumentNumber { get; set; }
    public int DocumentTypeID { get; set; }
    public DateTime DocumentFromDate { get; set; }
    public string Reason { get; set; }
    public int StatusID { get; set; }
    public string RequestedTo { get; set; }
    public bool Forwarded { get; set; }
    public long ForwardedBy { get; set; }
    public string ApprovedBy { get; set; }
    public int OrderNo { get; set; }

    public int PageSize { get; set; }
    public int PageIndex { get; set; }

    public DateTime IssueDate { get; set; }
    public long IssuedBy { get; set; }
    public int IssueReasonID { get; set; }
    public int BinID { get; set; }
    public DateTime? ExpectedReturnDate { get; set; }
    public string Remarks { get; set; }



    #endregion


    public clsDocumentRequestNew()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable GetAllDocumentTypes()
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0)
        });
    }

    public static DataTable GetDocumentNumberByDocumentType(int DocumentTypeID, long EmployeeID, int RequestID)
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",2),
            new SqlParameter("@DocumentTypeID",DocumentTypeID),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture())
        });
    }

    public bool SaveRequest()
    {
        RequestID = ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",RequestID == 0 ? 4 : 11),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@DocumentID",DocumentID),
            new SqlParameter("@DocumentNumber",DocumentNumber),
            new SqlParameter("@DocumentTypeID",DocumentTypeID),
            new SqlParameter("@DocumentFromDate",DocumentFromDate),
            new SqlParameter("@Reason",Reason),
            new SqlParameter("@StatusID",StatusID),
            new SqlParameter("@RequestedTo",RequestedTo),
            new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)
        }).ToInt32();

        return RequestID > 0;

    }

    public DataTable GetAllRequests()
    {
        return ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",5),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@PageIndex",PageIndex),
            new SqlParameter("@PageSize",PageSize),
             new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0)
          
        });
    }

    public static DataTable GetAllIssueReason()
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",6),
             new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0)
          
        });
    }

    public int GetTotalRecordCount()
    {
        int TotalRecordCount = 0;

        TotalRecordCount = ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",7),
            new SqlParameter("@EmployeeID",EmployeeID)
        }).ToInt32();

        return TotalRecordCount;
    }

    public clsDocumentRequestNew GetSingleForEdit()
    {
        clsDocumentRequestNew objDocumentRequestNew = new clsDocumentRequestNew();

        DataTable dtRequestDetails = ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",8),
            new SqlParameter("@RequestID",RequestID)
        });

        if (dtRequestDetails != null && dtRequestDetails.Rows.Count > 0)
        {
            objDocumentRequestNew.DocumentTypeID = dtRequestDetails.Rows[0]["DocumentTypeID"].ToInt32();
            objDocumentRequestNew.DocumentID = dtRequestDetails.Rows[0]["DocumentID"].ToInt32();
            objDocumentRequestNew.DocumentFromDate = dtRequestDetails.Rows[0]["DocumentFromDate"].ToDateTime();
            objDocumentRequestNew.Reason = Convert.ToString(dtRequestDetails.Rows[0]["Reason"]);
        }
        return objDocumentRequestNew;
    }

    public DataTable GetSingle()
    {
        DataTable dtRequestDetails = ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",9),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()),
        });

        return dtRequestDetails;
    }

    public static int GetRequestedEmployeeID(int RequestID)
    {
        return new DataLayer().ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",24),
            new SqlParameter("@RequestID",RequestID)          
        }).ToInt32();
    }

    public bool UpdateRequestStatus()
    {
        return ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",10),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@StatusID",StatusID),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@Reason",Reason),
            new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)
        }).ToInt32() > 0;
    }

    public static DataTable GetBinNumber(int RequestID)
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",13),
            new SqlParameter("@RequestID",RequestID)
        });
    }
    public bool SaveIssue()
    {
        int? OrderNo = null;

        if (StatusID == (int)RequestStatus.Approved)
        {
            OrderNo = ExecuteScalar("HRspDocumentRequestNew", new ArrayList
            {
                new SqlParameter("@Mode",14),
                new SqlParameter("@RequestID",RequestID),
                new SqlParameter("@StatusID",StatusID),
                new SqlParameter("@DocumentID",DocumentID),
                new SqlParameter("@DocumentTypeID",DocumentTypeID),
                new SqlParameter("@DocumentNumber",DocumentNumber),
                new SqlParameter("@IssueDate",IssueDate),
                new SqlParameter("@IssuedBy",IssuedBy),
                new SqlParameter("@IssueReasonID",IssueReasonID),
                new SqlParameter("@BinID",BinID),
                new SqlParameter("@ExpectedReturnDate",ExpectedReturnDate),
                new SqlParameter("@Remarks",Remarks),
                new SqlParameter("@Reason",Reason),               
                new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)
            }).ToInt32();
        }
        return UpdateRequest(OrderNo);

    }

    private bool UpdateRequest(int? OrderNo)
    {
        return ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",15),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@StatusID",StatusID),
            new SqlParameter("@OrderNo",OrderNo),
            new SqlParameter("@ApprovedBy",ApprovedBy),
             new SqlParameter("@Remarks",Remarks),
            new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)
        }).ToInt32() > 0;
    }

    public bool UpdateForwardedStatus()
    {
        return ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",15),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@StatusID",StatusID),
            new SqlParameter("@Forwarded",Forwarded),
            new SqlParameter("@ForwardedBy",ForwardedBy),
            new SqlParameter("@ApprovedBy",ApprovedBy),
             new SqlParameter("@Remarks",Remarks),
            new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)
        }).ToInt32() > 0;
    }

    public bool DeleteRequest()
    {
        return ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",16),
            new SqlParameter("@RequestID",RequestID),
            new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document)

        }).ToInt32() > 0;
    }

    public static string GetEmployeeName(long EmployeeID)
    {
        return new DataLayer().ExecuteScalar("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",17),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0)

        }).ToStringCustom();
    }

    public static DataTable GetRequestDetailsForMessage(int RequestID)
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
        {
            new SqlParameter("@Mode",18),
            new SqlParameter("@RequestID",RequestID)

        });
    }


    public static DataTable GetStatusForHigherAuthority()
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
       {
            new SqlParameter("@Mode",19),
            new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture())
       });
    }

    public static DataTable GetStatusForReportingTo()
    {
        return new DataLayer().ExecuteDataTable("HRspDocumentRequestNew", new ArrayList
       {
            new SqlParameter("@Mode",3)
       });
    }

    public static bool IsRequestExists(long EmployeeID, int DocumentID, int DocumentTypeID)
    {
        return new DataLayer().ExecuteScalar("HRspDocumentRequestNew", new ArrayList
       {
            new SqlParameter("@Mode",21),
            new SqlParameter("@EmployeeID",EmployeeID),
            new SqlParameter("@DocumentID",DocumentID),
            new SqlParameter("@DocumentTypeID",DocumentTypeID)
       }).ToInt32() > 0;
    }

    public static int GetStatus(int RequestID)
    {
        return new DataLayer().ExecuteScalar("HRspDocumentRequestNew", new ArrayList
       {
            new SqlParameter("@Mode",22),
            new SqlParameter("@RequestID",RequestID)
       }).ToInt32();
    }

    public static bool IsEmployeeInService(int RequestID)
    {
        return new DataLayer().ExecuteScalar("HRspDocumentRequestNew", new ArrayList
       {
            new SqlParameter("@Mode",23),
            new SqlParameter("@RequestID",RequestID)
       }).ToInt32() > 0;
    }
    public DataTable DisplayReasons(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 25));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Document));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspDocumentRequestNew", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 26));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@RequestID", RequestID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDocumentRequestNew", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 27));
        parameters.Add(new SqlParameter("@RequestID", RequestID));
        return Convert.ToInt32(ExecuteScalar("HRspDocumentRequestNew", parameters));
    }
}
