﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsBindComboBox
/// </summary>
public class clsBindComboBox:DL
{

    public clsBindComboBox()
    {
      
    }

    public  DataTable GetSalutationReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetDepartmentReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetWorkStatusReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "WSR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetDesignationReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DSR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetDegreeReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DGR"));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetCountryReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetNationalityReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "NR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }

    public DataTable GetWorkLocations()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAWL"));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }


    public DataTable GetMotherTongue()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAMT"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }

    public DataTable GetEthnicOrgin()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EO"));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetReligionReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "RR"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }
    public DataTable GetAllSpecializations(int DegreeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DS"));
        alParameters.Add(new SqlParameter("@DegreeId", DegreeId));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }

    public DataTable GetEmploymentTypeReference()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ER"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HrSpBindCombo", alParameters);
    }


    public DataSet GetAllData()
    {
        return this.ExecuteDataSet("HrSpBindCombo", new ArrayList 
        { 
            new SqlParameter("@Mode", "ALL"),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture())
        });
    }

    public DataTable GetBankBranches()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GBB")
        });
    }

    public DataTable GetVendorTypes()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GVT")
        });
    }

    public DataTable GetTrainingTopics()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GTP")
        });
    }

    public DataTable GetEvaluationPhases()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GEP")
        });
    }

    public DataTable GetRoles()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GAR")
        });
    }

    public DataTable GetHiringPlanTypes()
    {
        return this.ExecuteDataTable("HrSpBindCombo", new ArrayList { 
            new SqlParameter("@Mode", "GHPT")
        });
    }
}
