﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
/// <summary>
/// Summary description for clsAttendanceRequest
/// </summary>
public class clsAttendanceRequest : DL 
{
    public int EmployeeId { get; set; }
    public int StatusId { get; set; }
    public int RequestId { get; set; }
    public int Month { get; set; }
    public int Year { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string Mode { get; set; }
    public string Reason { get; set; }
    public string RequestedTo { get; set; }
    public string ApprovedBy { get; set; }
    public double Amount { get; set; }
    public DateTime RequestedDate { get; set; }
    public string DateVal { get; set; }
    public int UserId { get; set; }
    public int ProjectID { get; set; }
    public DateTime Fromdate { get; set; }
    public DateTime ToDate { get; set; }

    public List<clsAttnDetails> AttnDetails { get; set; }
    ArrayList prmAtnSummary;

	public clsAttendanceRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    /// <summary>
   /// This function is used to insert and update details.
   /// </summary>
   ///<CreatedBy>Jisha Bijoy</CreatedBy>
   ///<CreatedOn>8 Oct 2010</CreatedOn>
   ///<ModifiedBy></ModifiedBy>
   ///<ModifiedOn></ModifiedOn>
    public int InsertRequest()
    {
        try
        {
            this.BeginTransaction("HRspAttendanceRequest");

            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", Mode));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            alParameters.Add(new SqlParameter("@ProjectID", ProjectID));
            alParameters.Add(new SqlParameter("@FromDate", Fromdate));
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
            alParameters.Add(new SqlParameter("@Remarks", Reason));
            alParameters.Add(new SqlParameter("@RequestedTo", RequestedTo));
            alParameters.Add(new SqlParameter("@StatusId", StatusId));
            if (RequestId > 0)
                alParameters.Add(new SqlParameter("@RequestId", RequestId));

            RequestId = Convert.ToInt32(ExecuteScalar(alParameters));



            // INSERTION INTO Attendance Details

            foreach (clsAttnDetails objAttnDetails in AttnDetails)
            {
                ArrayList parameters2 = new ArrayList();
                parameters2.Add(new SqlParameter("@Mode", "IUD"));
                parameters2.Add(new SqlParameter("@RequestId", RequestId));
                parameters2.Add(new SqlParameter("@Date", objAttnDetails.Date));
                parameters2.Add(new SqlParameter("@TimeFrom", objAttnDetails.TimeFrom));
                parameters2.Add(new SqlParameter("@TimeTo", objAttnDetails.TimeTo));
                parameters2.Add(new SqlParameter("@Duration", objAttnDetails.WorkTime ));
                ExecuteScalar(parameters2).ToInt32();
            }
            this.CommitTransaction();
            return RequestId;
        }
        catch
        {
            this.RollbackTransaction();
            return RequestId;
        }

    }

    /// <summary>
    /// Get requested details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex",PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspAttendanceRequest", alParameters);
    }

    /// <summary>
    /// Get request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@RequestId",RequestId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataTable("HRspAttendanceRequest", alParameters);
    }
 

    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParameters));
        return iCount;
    }
    /// <summary>
    /// delete selected requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        ExecuteNonQuery("HRspAttendanceRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsSalaryRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParameters)) > 0 ? true : false);
    }

    
    /// <summary>
    /// get requested month loan installment amount
    /// </summary>
    /// <returns></returns>
   
    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateAdvanceStatus(bool IsAuthorised)
    {
        this.BeginTransaction("HRspAttendanceRequest");

        try
        {
            ArrayList alParemeters = new ArrayList();
            alParemeters.Add(new SqlParameter("@Mode", "UPS"));
            alParemeters.Add(new SqlParameter("@RequestId", RequestId));
            alParemeters.Add(new SqlParameter("@StatusId", StatusId));
            alParemeters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
            alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));          

            if (IsAuthorised && StatusId == 5)
            {
                                          
                alParemeters.Add(new SqlParameter("@UserId", UserId));
                alParemeters.Add(new SqlParameter("@Remarks", Reason));
            }
            else if (StatusId != 3 && !IsAuthorised )
                alParemeters.Add(new SqlParameter("@Forwarded", 1));

            RequestId = ExecuteScalar(alParemeters).ToInt32();
            // Approval 
            if (StatusId == 5)
            {
                foreach (clsAttnDetails objAttnDetails in AttnDetails)
                {
                    ArrayList parameters2 = new ArrayList();
                    parameters2.Add(new SqlParameter("@Mode", "IAD"));
                    parameters2.Add(new SqlParameter("@RequestId", RequestId));
                    parameters2.Add(new SqlParameter("@Date", objAttnDetails.Date));
                    parameters2.Add(new SqlParameter("@TimeFrom", objAttnDetails.TimeFrom));
                    parameters2.Add(new SqlParameter("@TimeTo", objAttnDetails.TimeTo));
                    parameters2.Add(new SqlParameter("@WorkTime", objAttnDetails.WorkTime));
                    parameters2.Add(new SqlParameter("@BreakTime", objAttnDetails.BreakTime));
                    parameters2.Add(new SqlParameter("@Late", objAttnDetails.Late));
                    parameters2.Add(new SqlParameter("@Early", objAttnDetails.Early));
                    parameters2.Add(new SqlParameter("@Excess", objAttnDetails.Excess));
                    parameters2.Add(new SqlParameter("@WorkPolicyID", objAttnDetails.WorkPolicyID));
                    parameters2.Add(new SqlParameter("@ShiftID", objAttnDetails.ShiftID));
                    parameters2.Add(new SqlParameter("@CompanyID", objAttnDetails.CompanyID));
                    parameters2.Add(new SqlParameter("@Absent", objAttnDetails.AbsentTime));
                    ExecuteScalar(parameters2).ToInt32();
                }
            }
            this.CommitTransaction();
        }
        catch
        {
             this.RollbackTransaction();
        }
    }
  
    public int GetRequestedById()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GBI"));
        alParemeters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParemeters));
    }
    public int GetRequestedByCompanyID()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GEC"));
        alParemeters.Add(new SqlParameter("@EmployeeID", EmployeeId ));

        return Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParemeters));
    }

    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@StatusId", StatusId));

        ExecuteNonQuery("HRspAttendanceRequest", alParameters);
    }


    public void SalaryAdvanceRequestCancelled()
    {
        this.BeginTransaction("HRspAttendanceRequest");

        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "CLR"));
            alParameters.Add(new SqlParameter("@RequestId", RequestId));
            alParameters.Add(new SqlParameter("@StatusId", StatusId));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));        

            RequestId = ExecuteScalar(alParameters).ToInt32();

            // Approval 

            foreach (clsAttnDetails objAttnDetails in AttnDetails)
            {
                ArrayList parameters2 = new ArrayList();
                parameters2.Add(new SqlParameter("@Mode", "CLRD"));
                parameters2.Add(new SqlParameter("@RequestId", RequestId));
                parameters2.Add(new SqlParameter("@StatusId", StatusId));
                parameters2.Add(new SqlParameter("@Date", objAttnDetails.Date));
                parameters2.Add(new SqlParameter("@TimeFrom", objAttnDetails.TimeFrom));
                parameters2.Add(new SqlParameter("@TimeTo", objAttnDetails.TimeTo));
                parameters2.Add(new SqlParameter("@WorkTime", objAttnDetails.WorkTime));
                parameters2.Add(new SqlParameter("@AttendanceID", objAttnDetails.AttendanceID));
                ExecuteScalar(parameters2).ToInt32();
            }

            this.CommitTransaction();
        }
        catch
        {
            this.RollbackTransaction();
        }
    }
    /// <summary>
    /// Get workstatusId to check whether the employee is in Service
    /// </summary>
    /// <returns></returns>
    public int GetWorkStatusId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CWS"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParameters));
    }

    ///<summary>
    ///To check requested date is greater than dateofjoining
    ///</summary>
    public bool CheckDateOfJoining()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDJ"));
        alParameters.Add(new SqlParameter("@RequestedDate", RequestedDate));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        //alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return Convert.ToBoolean(ExecuteScalar("HRspAttendanceRequest", alParameters));
    }



   
    public  int GetCompanyID(int RequestId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GCI"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));

        return Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParameters));

    }
    public DataTable getEmployee(int intEmployeeID) // of requested employee and RequestedTo Ids 
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEN"));
        alParameters.Add(new SqlParameter("@Employeeid", intEmployeeID));

        return ExecuteDataTable("HRspLoanRequest", alParameters);

    }


    public static DataTable FillProjects(long EmployeeID,int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID ));
        alParameters.Add(new SqlParameter("@requestID", RequestID));
        return  new DataLayer().ExecuteDataTable("HRspAttendanceRequest", alParameters);
    }
    public static string GetRequestedTo(Int64 intEmployeeId)
    {
        DataTable dt;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRT"));
        alParameters.Add(new SqlParameter("@EmployeeId", intEmployeeId));

        return Convert.ToString(new DataLayer().ExecuteScalar("HRspAttendanceRequest", alParameters));
    }


    public static DataTable FillAttnDetails(int RequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SAD"));
        alParameters.Add(new SqlParameter("@RequestID", RequestID));

        return new DataLayer().ExecuteDataTable("HRspAttendanceRequest", alParameters);
    }

    /// <summary>
    /// Check duplication
    /// </summary>
    /// <returns></returns>
    public bool IsAttendanceApplied()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CLD"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId ));
        if (ProjectID  != 0)
            alParameters.Add(new SqlParameter("@ProjectID", ProjectID));
     
            alParameters.Add(new SqlParameter("@FromDate", Fromdate ));
       
            alParameters.Add(new SqlParameter("@ToDate", ToDate));

        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        return (Convert.ToInt32(ExecuteScalar("HRspAttendanceRequest", alParameters)) > 0 ? true : false);
    }

    public bool IsEmployeeOnLeave(long EmpId, DateTime Date)
    {
        bool blnReturnValue = false;
        ArrayList prmAtnSummary = new ArrayList();
        prmAtnSummary.Add(new SqlParameter("@Mode", "CEL"));
        prmAtnSummary.Add(new SqlParameter("@EmployeeID", EmpId));
        prmAtnSummary.Add(new SqlParameter("@AtnDate", Date.Date.ToString("dd MMM yyyy")));


        SqlDataReader sdrShift = ExecuteReader("HRspAttendanceRequest", prmAtnSummary);
        if (sdrShift.Read())
        {

            blnReturnValue = true;
        }
        sdrShift.Close();
        return blnReturnValue;
    }




    // Attendance approval same function from MyPayFriend

    public bool CheckOffDay(int intEmpID, string dayOffWeek, DateTime dtDate)
    {
        bool returnvalue = false;
        int ID = 0;
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 11));
        parameters.Add(new SqlParameter("@EmployeeID", intEmpID));
        parameters.Add(new SqlParameter("@DayOfWeek", dayOffWeek));
        parameters.Add(new SqlParameter("@Date", dtDate));

        ID = ExecuteScalar("spPayAttendnacePolicyConseqInfo", parameters).ToInt32();
        if (ID > 0)
        {
            returnvalue = true;

        }

        return returnvalue;
    }

    public bool CheckHoliday(int intCmpID, string strDate)
    {
        bool returnvalue = false;
        ArrayList PrmHoliday = new ArrayList();
        int ID = 0;
        PrmHoliday.Add(new SqlParameter("@Mode", 6));
        PrmHoliday.Add(new SqlParameter("@FromDate", strDate));
        PrmHoliday.Add(new SqlParameter("@CompanyID", intCmpID));
        ID = ExecuteScalar("spPayAttendancePolicy", PrmHoliday).ToInt32();
        if (ID > 0)
        {
            returnvalue = true;
        }

        return returnvalue;

    }
    public DataTable GetEmployeeShiftInfo(int intEmpID, int intCmpID, string strDate)
    {
        ArrayList prmShift = new ArrayList();
        prmShift.Add(new SqlParameter("@Mode", 3));

        prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmShift.Add(new SqlParameter("@CompanyID", intCmpID));
        prmShift.Add(new SqlParameter("@FromDate", strDate));
        prmShift.Add(new SqlParameter("@ToDate", strDate));
        return ExecuteDataTable("spPayAttendancePolicy", prmShift);
    }
    public bool CheckDay(int IntEmpID, int DayID, DateTime dtDate)
    {

        bool returnvalue = false;
        SqlDataReader sdr;
        ArrayList prmShift = new ArrayList();
        prmShift = new ArrayList();
        prmShift.Add(new SqlParameter("@Mode", 7));
        prmShift.Add(new SqlParameter("@EmployeeID", IntEmpID));
        prmShift.Add(new SqlParameter("@DayID", DayID));
        prmShift.Add(new SqlParameter("@Date", dtDate));
        sdr = ExecuteReader("spPayAttendancePolicy", prmShift);
        if (sdr.Read())
        {
            returnvalue = true;
        }
        sdr.Close();
        return returnvalue;
    }

    public DataTable GetDailyShift(int intEmpID, int intDayId, DateTime dtDate)
    {
        ArrayList prmShift = new ArrayList();
        prmShift = new ArrayList();
        prmShift.Add(new SqlParameter("@Mode", 13));
        prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmShift.Add(new SqlParameter("@DayID", intDayId));
        prmShift.Add(new SqlParameter("@Date", dtDate));
        return ExecuteDataTable("spPayAttendnacePolicyConseqInfo", prmShift);
    }
    public DataTable GetOffDayShift(int intEmpID, int DayID, DateTime dtDate)
    {
        ArrayList prmShift = new ArrayList();
        prmShift = new ArrayList();
        prmShift.Add(new SqlParameter("@Mode", 14));
        prmShift.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmShift.Add(new SqlParameter("@DayID", DayID));
        prmShift.Add(new SqlParameter("@Date", dtDate));
        return ExecuteDataTable("spPayAttendnacePolicyConseqInfo", prmShift);
    }

    public bool CheckLeave(int intEmpID, string strDate, ref bool leaveflag)
    {
        leaveflag = false;
        bool returnvalue = false;
        SqlDataReader sdr;
        ArrayList PrmHoliday = new ArrayList();
        PrmHoliday.Add(new SqlParameter("@Mode", 12));
        PrmHoliday.Add(new SqlParameter("@Date", strDate));
        PrmHoliday.Add(new SqlParameter("@EmployeeID", intEmpID));
        sdr = ExecuteReader("spPayAttendnacePolicyConseqInfo", PrmHoliday);
        if (sdr.Read())
        {
            returnvalue = true;
            leaveflag = true;

        }
        sdr.Close();
        return returnvalue;
    }
    public DataTable GetShiftInfo(int intMode, int intEmpID, int intShiftID, int intDayID, string sFromDate, string sToDate, int intCmpID)
    {
        prmAtnSummary = new ArrayList();
        prmAtnSummary.Add(new SqlParameter("@Mode", intMode));
        if (intMode == 4)
        {
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
            prmAtnSummary.Add(new SqlParameter("@ShiftID", intShiftID));
            prmAtnSummary.Add(new SqlParameter("@DayID", intDayID));

        }
        else if (intMode == 5)
        {
            prmAtnSummary.Add(new SqlParameter("@ShiftID", intShiftID));
        }
        else if (intMode == 3)
        {
            prmAtnSummary.Add(new SqlParameter("@CompanyID", intCmpID));
            prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));

            prmAtnSummary.Add(new SqlParameter("@FromDate", sFromDate));
            prmAtnSummary.Add(new SqlParameter("@ToDate", sToDate));
        }

        return ExecuteDataTable("spPayAttendancePolicy", prmAtnSummary);
    }
    public int GetLeaveExAddMinutes(int intEmpID, DateTime dtAtnDate)
    {

        int intAdditinalminutes = 0;
        prmAtnSummary = new ArrayList();
        prmAtnSummary.Add(new SqlParameter("@Mode", 13));
        prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmAtnSummary.Add(new SqlParameter("@AtnDate", dtAtnDate.ToString("dd MMM yyyy")));
        intAdditinalminutes = ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToInt32();
        return intAdditinalminutes;
    }

    public bool IsConsiderBufferTimeForOT(int iShiftID, ref int iBufferTime, ref int iMinOtMinutes)
    {
        //Checking buffer Time is consider for Ot ,if it is true then after (ToTime+ buffer time) Employee Is worked consider ot other wise after Totime, ot is Consider 
        bool blnIsConsiderBufferTimeForOT = false;
        iBufferTime = 0;
        iMinOtMinutes = 0;
        DataTable dtEmp;
        prmAtnSummary = new ArrayList();
        prmAtnSummary.Add(new SqlParameter("@Mode", 7));
        prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
        dtEmp = ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);
        if (dtEmp != null)
        {
            if (dtEmp.Rows.Count > 0)
            {
                iBufferTime = dtEmp.Rows[0]["Buffer"].ToInt32();
                blnIsConsiderBufferTimeForOT = dtEmp.Rows[0]["IsBufferForOT"].ToInt32() > 0 ? true : false;
                iMinOtMinutes = dtEmp.Rows[0]["MinimumOT"].ToInt32();
            }
        }
        return blnIsConsiderBufferTimeForOT;
    }
    public bool isAfterMinWrkHrsAsOT(int iShiftID)
    {
        bool retValue = false;
        prmAtnSummary = new ArrayList();
        prmAtnSummary.Add(new SqlParameter("@Mode", 20));
        prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
        retValue = ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToBoolean();
        return retValue;
    }
      public bool IsHoliday(int empId, int iCmpID, DateTime dtDate, int iDayID)
        {
            bool bHoliday = false;
            int ID = 0;
            prmAtnSummary = new ArrayList();
            prmAtnSummary.Add(new SqlParameter("@Mode", 7));
            prmAtnSummary.Add(new SqlParameter("@CompanyID", iCmpID));
            prmAtnSummary.Add(new SqlParameter("@Date", dtDate.ToString("dd MMM yyyy")));
            ID = ExecuteScalar("spPayAttendnacePolicyConseqInfo", prmAtnSummary).ToInt32();
            if (ID > 0)
                bHoliday = true;

            if (bHoliday == false)
            {
                ID = 0;
                prmAtnSummary = new ArrayList();
                prmAtnSummary.Add(new SqlParameter("@Mode", 8));
                prmAtnSummary.Add(new SqlParameter("@EmployeeID", empId));
                prmAtnSummary.Add(new SqlParameter("@DayID", iDayID));
                ID = ExecuteScalar("spPayAttendnacePolicyConseqInfo", prmAtnSummary).ToInt32();

                if (ID > 0)
                {
                    bHoliday = false;
                }
                else
                {
                    bHoliday = true;
                }

            }
            return bHoliday;
        }

      public string GetDynamicShiftDuration(int iShiftID, int iShiftOrderNo, ref string sMinWorkHours, ref string sAllowedBreakTime)
      {
          string sDuration = "";
          sMinWorkHours = "0";
          sAllowedBreakTime = "0";
          prmAtnSummary = new ArrayList();
          prmAtnSummary.Add(new SqlParameter("@Mode", 15));
          prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftID));
          prmAtnSummary.Add(new SqlParameter("@ShiftOrderNo", iShiftOrderNo));
          SqlDataReader sdrEmployee = ExecuteReader("spPayAttendancePolicy", prmAtnSummary);
          if (sdrEmployee.Read())
          {
              sDuration = Convert.ToString(sdrEmployee["Duration"]);
              sMinWorkHours = Convert.ToString(sdrEmployee["MinWorkingHours"]);
              sAllowedBreakTime = Convert.ToString(sdrEmployee["AllowedBreakTime"]);
          }
          sdrEmployee.Close();
          return sDuration;
      }
      public DataTable GetOtDetails()
      {
          prmAtnSummary = new ArrayList();
          prmAtnSummary.Add(new SqlParameter("@Mode", 10));
          return ExecuteDataTable("spPayAttendanceDetails", prmAtnSummary);

      }
      public int GetEmpPolicyID(int intEmpID)
      {
          int intPolicyID = 0;
          prmAtnSummary = new ArrayList();
          prmAtnSummary.Add(new SqlParameter("@Mode", 8));
          prmAtnSummary.Add(new SqlParameter("@EmployeeID", intEmpID));
          intPolicyID = ExecuteScalar("spPayAttendancePolicy", prmAtnSummary).ToInt32();
          return intPolicyID;

      }

      public DataTable GetDynamicShiftDetails(int iShiftId)
      {
          // gettting Dynamic and Split Shift  details 
          prmAtnSummary = new ArrayList();
          prmAtnSummary.Add(new SqlParameter("@Mode", 16));
          prmAtnSummary.Add(new SqlParameter("@ShiftID", iShiftId));
          return ExecuteDataTable("spPayAttendancePolicy", prmAtnSummary);
      }
}
public class clsAttnDetails
{

    public DateTime Date { get; set; }
    public string TimeFrom { get; set; }
    public string TimeTo { get; set; }
    public decimal AttendanceID { get; set; }
    public int CompanyID { get; set; }
    public int WorkPolicyID { get; set; }
    public int ShiftID { get; set; }
    public string WorkTime { get; set; }// worktime
    public string BreakTime { get; set; }
    public string Late { get; set; }
    public string Early { get; set; }
    public string Excess { get; set; }
    public int AbsentTime { get; set; }

}