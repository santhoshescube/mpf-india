﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsInterviewProcess
/// </summary>
public class clsInterviewProcess : DataLayer
{
    public clsInterviewProcess()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataTable GetAllNewInterviewSchedules()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));

        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);

    }

    public static DataTable GetAllOngoingAndCompletedSchedules()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);

    }

    public bool SetCurrentInterviewCandidate(int JobScheduleID, int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return ExecuteScalar("HRspInterviewProcess", alParameters).ToInt32() > 0;

    }


    public static int GetCurrentInterviewCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        //alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters).ToInt32();
    }



    public static int GetCurrentInterviewCount(int InterviewerID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@InterviewerID", InterviewerID));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        object obj = new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters);
        if (obj != null && obj != DBNull.Value)
            return obj.ToInt32();
        else
            return 0;
    }



    public static DataSet GetCurrentActiveScheduleForEvaluation(int InterviewerID, bool IsManual, Int64  CandidateID, Int32 JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@InterviewerID", InterviewerID));
        alParameters.Add(new SqlParameter("@IsManual", IsManual));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        //alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspInterviewProcess", alParameters);
    }




    public static DataSet GetAllInterviewersEvaluationResult(Int64 InterviewProcessID,Int64 CandidateID,Int32 JobID,Int32 LevelID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@JobLevelID", LevelID));
        //alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspInterviewProcess", alParameters);
    }
    public static DataSet GetAllInterviewersEvaluationResultLevels(Int64 InterviewProcessID, Int64 CandidateID,Int32 JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 27));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID ));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        //alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter ("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 :0));
        return new DataLayer().ExecuteDataSet("HRspInterviewProcess", alParameters);
    }



    public static DataTable CheckInterviewStatus(Int64 InterviewProcessID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        //alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }



    public bool UpdateInterviewStatus(Int64 InterviewProcessID, int ResultTypeID, bool SendOfferLetter,int CandidateStatusID)
    {
        try
        {


            BeginTransaction();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 10));
            alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
            alParameters.Add(new SqlParameter("@ResultTypeID", ResultTypeID));
            alParameters.Add(new SqlParameter("@IsOfferLetterSend", SendOfferLetter));
            alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
            alParameters.Add(new SqlParameter("@CandidateStatusID", CandidateStatusID));
            CommitTransaction();
            return ExecuteScalar("HRspInterviewProcess", alParameters).ToInt32() > 0;


        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return false;
        }

    }



    public Int64 SaveEvaluationResult(clsInterviewEvaluationResult objEvaluationResult)
    {
        try
        {
            Int64 InterviewProcessID;
            BeginTransaction();
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", 6));
            alParameters.Add(new SqlParameter("@InterviewProcessID", objEvaluationResult.InterviewProcessID));
            alParameters.Add(new SqlParameter("@JobScheduleDetailID", objEvaluationResult.JobScheduleDetailID));
            alParameters.Add(new SqlParameter("@CandidateID", objEvaluationResult.CandidateID));

            alParameters.Add(new SqlParameter("@InterviewerID", objEvaluationResult.InterviewerID));
            alParameters.Add(new SqlParameter("@JobID", objEvaluationResult.JobID));

            InterviewProcessID = ExecuteScalar("HRspInterviewProcess", alParameters).ToInt64();


            foreach (clsInterviewCriteria objCriteria in objEvaluationResult.Criteria)
            {
                ArrayList alParameters1 = new ArrayList();
                alParameters1.Add(new SqlParameter("@Mode", 7));
                alParameters1.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
                alParameters1.Add(new SqlParameter("@InterviewerID", objEvaluationResult.InterviewerID));
                alParameters1.Add(new SqlParameter("@JobScheduleDetailID", objEvaluationResult.JobScheduleDetailID));
                alParameters1.Add(new SqlParameter("@JobID", objEvaluationResult.JobID));
                alParameters1.Add(new SqlParameter("@JobLevelID", objEvaluationResult.JobLevelID));
                alParameters1.Add(new SqlParameter("@CriteriaID", objCriteria.CriteriaID));
                alParameters1.Add(new SqlParameter("@AssignedMarks", objCriteria.AssignedMark));
                ExecuteScalar("HRspInterviewProcess", alParameters1).ToInt64();
            }
            CommitTransaction();
            return InterviewProcessID;

        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return 0;
        }

    }



    public static string GetLastCompletedEmployee()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        return Convert.ToString(new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters));

    }


    public static void GetNextCandidate(Int64 InterviewProcessID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters);

    }


    public static int GetInterviewProcessID(Int32 InterviewerID, int CandidateID, int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 14));
      
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@InterviewerID", InterviewerID));
        alParameters.Add(new SqlParameter("@JobID", JobID ));
        return new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters).ToInt32();

    }



    public static int GetInterviewResult(Int64 InterviewProcessID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        object status = new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters);
        if (status != DBNull.Value && status != null)
            return status.ToInt32();
        else
            return 0;


    }

    public static DataTable GetCandidateOfferLetter(Int64 InterviewProcessID)
    {

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@InterviewProcessID", InterviewProcessID));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);

    }


    public void SaveOfferLetter(int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        ExecuteScalar("HRspInterviewProcess", alParameters);
    }


    public static void UpdateOfferLetterStatus(Int64 CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 17));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));

        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        new DataLayer().ExecuteScalar("HRspInterviewProcess", alParameters);
    }



    public static DataTable GetFromToMailIDs(Int64 CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 18));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataTable GetAllSchedules()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 20));
        alParameters.Add(new SqlParameter("@CurrentDate", DateTime.Now.Date));
        alParameters.Add(new SqlParameter("@InterviewerID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataTable GetAllCandidates( int JobScheduleID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 19));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@InterviewerID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataTable FillSchedules(int PageIndex, int PageSize, string SearchKey, string SortOrder, string SortExpression,int JobID,long CandidateID,int StatusID,int InterviewerID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 25));
        alParameters.Add(new SqlParameter("@PageSize", PageSize ));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex ));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey ));
        alParameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alParameters.Add(new SqlParameter("@SortExpression", SortExpression));



        alParameters.Add(new SqlParameter("@InterviewerID", InterviewerID));

        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@CandidateStatusID", StatusID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 :0));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataTable FillScheduleLevels(Int64 CandidateID,int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 26));

        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@JobID", JobID ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataSet FillJobAndStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 28));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRspInterviewProcess", alParameters);
    }

    public static DataTable  FillInterviewer(int InterviewerID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 30));
        alParameters.Add(new SqlParameter("@InterviewerID", InterviewerID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

    public static DataTable FillCandidates(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 29));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 :0));
        return new DataLayer().ExecuteDataTable("HRspInterviewProcess", alParameters);
    }

}

public class clsInterviewEvaluationResult
{

    public int JobScheduleDetailID { get; set; }

    public int CandidateID { get; set; }

    public int InterviewerID { get; set; }

    public int InterviewProcessID { get; set; }

    public int JobID { get; set; }

    public int JobLevelID { get; set; }

    public List<clsInterviewCriteria> Criteria { get; set; }

}



public class clsInterviewCriteria
{
    public int CriteriaID { get; set; }

    public decimal AssignedMark { get; set; }
}