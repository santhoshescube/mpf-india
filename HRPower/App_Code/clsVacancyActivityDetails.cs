﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Data.SqlClient;
using System.Xml.Linq;

// <summary>
/// Summary description for clsInterviewSchedule
/// Author:Thasni Latheef
/// Created Date: 09-08-2010

/// </summary>
public class clsVacancyActivityDetails : DL
{
    public clsVacancyActivityDetails()
    {
        //
        // TODO: Add constructor logic here
        //

    }
    public enum ActivityType
    {
        ResumeCollection = 1,
        ResumeShortlisting = 2,
        Association = 3,
        InterviewSchedule = 4,
        InterviewProcess = 5
    }

    private ActivityType iActivityTypeId;
    private int iActivity;
    private int iAssignedEmployeeId;
    private long lActivityId;
    private int iVacancyId;
    private string sActivityName;
    private int iInterviewTypeId;
    private string sStartDate;
    private string sEndDate;
    private int iActivityStatusId;
    private int iOrderNo;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private int iParentId;
    private int iTotalMarks;

    // for report.
    private int iEmployeeID;

    public ActivityType ActivityTypeId
    {
        get { return iActivityTypeId; }
        set { iActivityTypeId = value; }
    }

    public int Activity
    {
        get { return iActivity; }
        set { iActivity = value; }
    }

    public int AssignedEmployeeId
    {
        get { return iAssignedEmployeeId; }
        set { iAssignedEmployeeId = value; }
    }

    public long ActivityId
    {
        get { return lActivityId; }
        set { lActivityId = value; }
    }

    public int VacancyId
    {
        get { return iVacancyId; }
        set { iVacancyId = value; }
    }

    public string ActivityName
    {
        get { return sActivityName; }
        set { sActivityName = value; }
    }

    public int InterviewTypeId
    {
        get { return iInterviewTypeId; }
        set { iInterviewTypeId = value; }
    }

    public string StartDate
    {
        get { return sStartDate; }
        set { sStartDate = value; }
    }

    public string EndDate
    {
        get { return sEndDate; }
        set { sEndDate = value; }
    }

    public int ActivityStatusId
    {
        get { return iActivityStatusId; }
        set { iActivityStatusId = value; }
    }

    public int OrderNo
    {
        get { return iOrderNo; }
        set { iOrderNo = value; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public int ParentId
    {
        get { return iParentId; }
        set { iParentId = value; }
    }
    public int TotalMarks
    {
        get { return iTotalMarks; }
        set { iTotalMarks = value; }
    }
   

    /// <summary>
    /// Gets or sets EmployeeID used to retrieve logo of the company in which employee is working.
    /// </summary>
    public int EmployeeID
    {
        get { return iEmployeeID; }
        set { iEmployeeID = value; }
    }



    public DataSet GetActivityList()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SA"));
        alParameters.Add(new SqlParameter("@ActivityTypeId", (int)ActivityTypeId));
        alParameters.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));

        return ExecuteDataSet("HRspVacancyActivityDetails", alParameters);

    }

    public DataSet GetActivityListWalkin()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SAW"));
        alParameters.Add(new SqlParameter("@ActivityTypeId", (int)iActivityTypeId));
        alParameters.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));
        return ExecuteDataSet("HRspVacancyActivityDetails", alParameters);

    }

    public DataSet GetVacancyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SV"));
        alParameters.Add(new SqlParameter("@ActivityId", lActivityId));

        return ExecuteDataSet("HRspVacancyActivityDetails", alParameters);

    }

    public DataSet GetAllVacancies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GV"));
        alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alparameters.Add(new SqlParameter("@PageSize", PageSize));
        alparameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", SortOrder));

        return ExecuteDataSet("HRspVacancyActivityDetails", alparameters);

    }

    public int InsertVacancyActivityDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityTypeId", iActivity));
        alparameters.Add(new SqlParameter("@ActivityName", sActivityName));

        if (iActivity == 4)
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", iInterviewTypeId));
            if (iTotalMarks == -1)
                alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
            else
                alparameters.Add(new SqlParameter("@TotalMarks", iTotalMarks));
        }

        else
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", -1));
            alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
        }

        alparameters.Add(new SqlParameter("@StartDate", sStartDate));
        alparameters.Add(new SqlParameter("@EndDate", sEndDate));
        alparameters.Add(new SqlParameter("@ActivityStatusId", 1));
        alparameters.Add(new SqlParameter("@OrderNo", iOrderNo));

        return Convert.ToInt32(ExecuteScalar(alparameters));

    }

    public void InsertActivityEmployeeDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));

        ExecuteNonQuery(alparameters);
    }

    public void DeleteActivityEmployee()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DAE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        ExecuteNonQuery(alparameters);
    }

    public int UpdateVacancyActivityDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityTypeId", iActivity));
        alparameters.Add(new SqlParameter("@ActivityName", sActivityName));

        if (iActivity == 4)
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", iInterviewTypeId));
            if(iTotalMarks==-1)
                alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
            else
                alparameters.Add(new SqlParameter("@TotalMarks", iTotalMarks));
        }
        else
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", -1));
            alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
        }

        alparameters.Add(new SqlParameter("@StartDate", sStartDate));
        alparameters.Add(new SqlParameter("@EndDate", sEndDate));
        // alparameters.Add(new SqlParameter("@ActivityStatusId", iActivityTypeId));

        return Convert.ToInt32(ExecuteScalar(alparameters));

    }

    public DataSet GetVacancyActivityDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GVA"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspVacancyActivityDetails", alparameters);

    }

    public DataTable GetEmployees()       /* For gridview mode view of Activity  assigned employees*/
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "AED"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@ParentId", iParentId));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteDataTable("HRspVacancyActivityDetails", alparameters);
    }

    public DataSet FillActivity()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        return ExecuteDataSet("HRspVacancyActivityDetails", alparameters);
    }

    public int GetOrderNoOfActivity()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return Convert.ToInt32(ExecuteScalar(alparameters));

    }

    public DataSet FillActivityType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FAT"));

        return ExecuteDataSet("HRspVacancyActivityDetails", alparameters);
    }

    public DataSet FillInterviewType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FIT"));

        return ExecuteDataSet("HRspVacancyActivityDetails", alparameters);
    }

    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "RC"));
        return Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
    }

    public bool ActivityNameDuplication()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DA"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityName", sActivityName));
        int count = Convert.ToInt32(ExecuteScalar(alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public bool ActivityNameExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DAU"));

        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityName", sActivityName));
        int count = Convert.ToInt32(ExecuteScalar(alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public SqlDataReader GetActivityEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        return ExecuteReader("HRspVacancyActivityDetails", alparameters);

    }

    

    public bool IsJobDefined()  /* check is Activity type exists for the same vacancy */
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GD"));

        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityTypeId", iActivity));
        // alparameters.Add(new SqlParameter("@ActivityName", sActivityName));

        int count = Convert.ToInt32(ExecuteScalar(alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public bool IsInterviewScheduled()  /* check if Activity type is InterviewSchedule then Interviewtype exists for the same vacancy */
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GI"));

        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityTypeId", iActivity));
        alparameters.Add(new SqlParameter("@InterviewTypeId", iInterviewTypeId));
        //  alparameters.Add(new SqlParameter("@ActivityName", sActivityName));

        int count = Convert.ToInt32(ExecuteScalar(alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public bool CheckDeleteActivity()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CAI"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count > 0)
            return true;
        else
            return false;

    }

    public bool IsDeleteActivity()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CAIP"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count > 0)
            return true;
        else
            return false;

    }

    public void DeleteActivityDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        ExecuteNonQuery("HRspVacancyActivityDetails", alparameters);

    }

    public bool CheckActivityUpdation()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CDF"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count >= 0)
            return true;
        else
            return false;

    }
    public bool CheckActivityAssociationStarted()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "AS"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public bool CheckInterviewScheduleStarted()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IS"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public bool CheckInterviewProcessStarted()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IP"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public int CheckActivityEndDate()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CAD"));
        arraylst.Add(new SqlParameter("@ActivityId", lActivityId));
        arraylst.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));

        return Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", arraylst));
    }

    //public SqlDataReader GetAssignedEmployees()       /* For Formview mode view of Activity */
    //{
    //    ArrayList alparameters = new ArrayList();

    //    alparameters.Add(new SqlParameter("@Mode", "AED"));
    //    alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
    //    alparameters.Add(new SqlParameter("@ParentId", iParentId));
    //    alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

    //    return ExecuteReader("HRspVacancyActivityDetails", alparameters);
    //}

    public void Begin()
    {
        BeginTransaction("HRspVacancyActivityDetails");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }


    public DataSet GetVacancies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GV"));
        return ExecuteDataSet("HRspVacancyActivityDetails", alParameters);
    }

    public int GetPerformanceTypeId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "PV"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return Convert.ToInt32(ExecuteScalar("HRspVacancyActivityDetails", alparameters));
    }

    public DataTable GetAssignedEmployeesIds()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SAE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        return ExecuteDataTable("HRspVacancyActivityDetails", alparameters);

    }
}
