﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

public enum eTrainingGroup
{
    DepartmentWise = 1,
    EmployeeWise = 2
}

public class clsTrainingDetails
{
    public List<int> EmployeeIDs { get; set; }
    public List<int> DepartmentIDs { get; set; }



    public clsTrainingDetails()
    {
        this.EmployeeIDs = new List<int>();
        this.DepartmentIDs = new List<int>();
    }
}

public class clsTrainingSchedule : DL
{

    public clsTrainingSchedule()
    {
        this.TrainingDetails = new clsTrainingDetails();
    }
    public string ScheduleName { get; set; }
    public string Venue { get; set; }
    public string StartDate { get; set; }
    public string StartTime { get; set; }
    public string EndDate { get; set; }
    public string EndTime { get; set; }
    public string Purpose { get; set; }
    public decimal Fee { get; set; }
    public int CompanyPercentage { get; set; }
    public int ScheduleType { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int CourseId { get; set; }
    public int ScheduleById { get; set; }
    public bool IsRegistration { get; set; }
    public int ScheduleID { get; set; }
    public long EmployeeID { get; set; }
    public  bool IsDepartmentWise{get;set;}
    
    public eTrainingGroup TrainingGroup { get; set; }
    public clsTrainingDetails TrainingDetails { get; set; }

    /// <summary>
    /// Returns all departments 
    /// </summary>
    /// <returns></returns>
    public DataTable GetDepatments()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GD"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return this.ExecuteDataTable("HRspTrainingSchedule", alParameters);
    }

    /// <summary>
    /// Returns employees by company
    /// </summary>
    /// <param name="CompanyID">Company ID</param>
    /// <param name="Department"> pass zero value to get employees belongs to all departments </param>
    /// <returns></returns>
    public DataTable GetEmployees( int DepartmentID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GE"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("DepartmentID", DepartmentID));

        return this.ExecuteDataTable("HRspTrainingSchedule", alParameters);
    }

    public DataTable GetCourse()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GC"));
        return this.ExecuteDataTable("HRspTrainingSchedule",alParameters);
    }

    public int InsertSchedule()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@ScheduleID",ScheduleID));
        alParameters.Add(new SqlParameter("@ScheduleName", ScheduleName));
        alParameters.Add(new SqlParameter("@ScheduledBy", ScheduleById));
        alParameters.Add(new SqlParameter("@CourseID", CourseId));
        alParameters.Add(new SqlParameter("@Purpose", Purpose));
        alParameters.Add(new SqlParameter("@StartDate", StartDate));
        alParameters.Add(new SqlParameter("@EndDate", EndDate));
        alParameters.Add(new SqlParameter("@StartTime", StartTime));
        alParameters.Add(new SqlParameter("@EndTime", EndDate));
        alParameters.Add(new SqlParameter("@IsRegistration", IsRegistration));
        alParameters.Add(new SqlParameter("@RegistrationFee", Fee));
        alParameters.Add(new SqlParameter("@CompanyContribution", CompanyPercentage));
        alParameters.Add(new SqlParameter("@Venue", Venue));
        alParameters.Add(new SqlParameter("@ScheduleTypeID", ScheduleType));
        alParameters.Add(new SqlParameter("@ScheduledBy", ScheduleById));
        alParameters.Add(new SqlParameter("@IsDepartmentWise" , IsDepartmentWise));
        return ExecuteScalar("HRspTrainingSchedule", alParameters).ToInt32(); 
                
    }

    public void InsertScheduleDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@ScheduleID", ScheduleID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        ExecuteNonQuery("HRspTrainingSchedule", alParameters);
    }
}

public class EmployeeTemp
{
    public string EmployeeName { get; set; }
    public int EmployeeID { get; set; }
}

