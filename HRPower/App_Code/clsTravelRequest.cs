﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsTravelRequest
/// </summary>
public class clsTravelRequest:DL 
{
  
	public clsTravelRequest()
	{
		//
		// TODO: Add constructor logic here
		//
        //
	}
    #region Properties
    public int EmployeeId
    {
       set;
       get;
    }
    public int TravelDays
    {
        set;
       get;
    }
    public string Mode
    {
       set;
      get;
    }
    public DateTime TravelDate
    {
        set;
       get;
    }

    public DateTime TravelToDate
    {
        set;
       get;
    }
    public DateTime RequestedDate
    {
        set;
      get;
    }
    public double Amount
    {
        set;
      get;
    }
    public string Purpose
    {
        set;
      get;
    }
    public string RequestedTo
    {
        set;
        get;
    }
    public int TravelMode
    {
       set;
      get;
    }
    public string ApprovedBy
    {
       set;
      get;
    }
    public int StatusId
    {
        set;
      get;
    }
    public int RequestId
    {
       set;
      get;
    }
    public int CompanyId
    {
        set;
        get;
    }
    public string Route
    {
       set;
      get;
    }
    public string  Place
    {
       set;
       get;
    }
    public int PageIndex
    {
      get;
        set;
    }

    public int PageSize
    {
       get;
        set;
    }
    public bool IsArrangementsMade
    {
        get;
        set;
    }
    public bool IsVehicleRequired
    {
        get;
        set;
    }
    public bool IsPassport
    {
        get;
        set;
    }
    public bool IsPassportWithEmployee
    {
        get;
        set;
    }
    public bool IsAdvance
    {
        get;
        set;
    }
   
    public DateTime Date
    {
       set;
      get;
    }
    public string VehicleRemarks
    {
        set;
        get;
    }
    public string Remarks
    {
        set;
        get;
    }
    

    #endregion

    /// <summary>
    /// This function is used to insert or update travel request.
    /// </summary>
   
    public int InsertUpdateRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", Mode));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));        
        alParameters.Add(new SqlParameter("@TravelDate", TravelDate ));
        alParameters.Add(new SqlParameter("@Place", Place));
        alParameters.Add(new SqlParameter("@TravelDays", TravelDays ));
        alParameters.Add(new SqlParameter("@Purpose", Purpose));
        alParameters.Add(new SqlParameter("@TravelModeID", TravelMode));
        alParameters.Add(new SqlParameter("@StatusId", StatusId));
        alParameters.Add(new SqlParameter("@IsArrangementsMade", IsArrangementsMade ));
        alParameters.Add(new SqlParameter("@Route", Route));
        alParameters.Add(new SqlParameter("@IsVehicleRequired", IsVehicleRequired));
        alParameters.Add(new SqlParameter("@VehicleRemarks", VehicleRemarks));
        alParameters.Add(new SqlParameter("RequestedTo", RequestedTo));
        alParameters.Add(new SqlParameter("@IsPassportRequired", IsPassport ));
        alParameters.Add(new SqlParameter("@IsAdvanceRequired", IsAdvance));
        alParameters.Add(new SqlParameter("@AdvanceAmount", Amount));
        alParameters.Add(new SqlParameter("@Remarks" , Remarks));
        alParameters.Add(new SqlParameter("@IsPassportWithEmployee", IsPassportWithEmployee));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
        if(RequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", RequestId ));

        return Convert.ToInt32(ExecuteScalar("HRspTravelRequest", alParameters));

    }

    public DataTable GetTravelMode()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@Mode", "STM"));
        return ExecuteDataTable("HRspTravelRequest", alParameters);

    }


   /// <summary>
    /// Get requested details.
    /// </summary>
    /// <returns></returns>
    public DataSet GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "S"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex ));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        return ExecuteDataSet("HRspTravelRequest", alParameters);
    }


    /// <summary>
    /// Get request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ED"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspTravelRequest", alParameters);
    }

    /// <summary>
    /// delete selected requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL"));
        alParameters.Add(new SqlParameter("@RequestID", RequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
        ExecuteNonQuery("HRspTravelRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestID", RequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspTravelRequest", alParameters)) > 0 ? true : false);
    }

    public  void ApproveRequest(bool IsAuthorised)
    {
         
          ArrayList alParemeters = new ArrayList();
          alParemeters.Add(new SqlParameter("@Mode", "UPS"));
          alParemeters.Add(new SqlParameter("@RequestID", RequestId));
          alParemeters.Add(new SqlParameter("@StatusId", StatusId));
          alParemeters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
          alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));
          alParemeters.Add(new SqlParameter("@Remarks", Remarks));
          alParemeters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
          if (StatusId != 3 && !IsAuthorised)
              alParemeters.Add(new SqlParameter("@IsForwarded", 1));

          ExecuteScalar("HRspTravelRequest", alParemeters).ToInt32();

         
    }
    /// <summary>
    /// Get workstatusId to check whether the employee is in Service
    /// </summary>
    /// <returns></returns>
    public int GetWorkStatusId()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CWS"));
        alParameters.Add(new SqlParameter("@RequestID", RequestId));

        return Convert.ToInt32(ExecuteScalar("HRspTravelRequest", alParameters));
    }
    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));       
        alParameters.Add(new SqlParameter("@StatusId", StatusId));
        alParameters.Add(new SqlParameter("@Remarks", Remarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
        ExecuteNonQuery("HRspTravelRequest", alParameters);
    }
    public void CancelRequest()
    {
        this.BeginTransaction("HRspTravelRequest");       
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "CLR"));
            alParameters.Add(new SqlParameter("@RequestID", RequestId));
            alParameters.Add(new SqlParameter("@StatusId", StatusId));
            alParameters.Add(new SqlParameter("@Remarks", Remarks));
            alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
            alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
            RequestId  = ExecuteScalar(alParameters).ToInt32();

            this.CommitTransaction();

         
        }
        catch
        {
            this.RollbackTransaction();
        }
    }
    public DataTable DisplayReasons()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DR"));
        alParameters.Add(new SqlParameter("@RequestId", RequestId));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Travel));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspTravelRequest", alParameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAH"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyId));
        parameters.Add(new SqlParameter("@RequestId", RequestId));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspTravelRequest", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GCI"));
        parameters.Add(new SqlParameter("@RequestId", RequestId));
        return Convert.ToInt32(ExecuteScalar("HRspTravelRequest", parameters));
    }
}
