﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsAccountsReference
/// Author:Thasni Latheef
/// Created Date: 17-09-2010
/// </summary>
public class clsAccountsReference:DL
{
	public clsAccountsReference()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private int iParentId, iCompanyId, iGlAccountId, iAccountId;
    private string sDescription, sShortName, sGlAccountIds, sAccountIds;
    private decimal dOpeningBalance;

    public decimal OpeningBalance
    {
        get { return dOpeningBalance; }
        set { dOpeningBalance = value; }
    }
    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string ShortName
    {
        get { return sShortName; }
        set { sShortName = value; }
    }
    public int ParentId
    {
        get { return iParentId; }
        set { iParentId = value; }
    }
    public int CompanyId
    {
        get { return iCompanyId; }
        set { iCompanyId = value; }
    }
    public int GLAccountId
    {
        get { return iGlAccountId; }
        set { iGlAccountId = value; }
    }
    public int AccountId
    {
        get { return iAccountId; }
        set { iAccountId = value; }
    }

    public string GLAccountIds
    {
        get { return sGlAccountIds; }
        set { sGlAccountIds = value; }
    }
    public string AccountIds
    {
        get { return sAccountIds; }
        set { sAccountIds = value; }
    }
    public DataTable GetAccounts() // select all the accounts to generate the tree structure
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GA"));
        arraylst.Add(new SqlParameter("@ParentId", iParentId));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyId));
        DataTable dt = ExecuteDataTable("HRspAccountsReference", arraylst);
        return dt ;

    }

    public DataTable GetCompanies() // select all comapnies
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SC"));
       return ExecuteDataTable("HRspAccountsReference", arraylst);
       
    }
    public DataTable GetAccountDetails() // Get accont details of a particular account/account head
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SA"));
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        return ExecuteDataTable("HRspAccountsReference", arraylst);

    }

    public int UpdateAccountHead() // update accounthead
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "UAH"));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@ShortName", sShortName));
        return Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst));
    }

    
     public int UpdateAccount() // update account
     {
         ArrayList arraylst = new ArrayList();

         arraylst.Add(new SqlParameter("@Mode", "UAC"));
         arraylst.Add(new SqlParameter("@ParentId", iParentId));
         arraylst.Add(new SqlParameter("@Description", sDescription));
         arraylst.Add(new SqlParameter("@ShortName", sShortName));
         arraylst.Add(new SqlParameter("@AccountID", iAccountId));
         arraylst.Add(new SqlParameter("@OpeningBalance", dOpeningBalance));
         return Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst));
     }

    public bool IsAccountExistsEdit()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "EXE"));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyId));
        return ((Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst))==1 ?true:false));
    }
    public bool IsAccountExistsInsert()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "EXI"));
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyId));
        return ((Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst)) == 1 ? true : false));
    }
    public int InsertAccountHead() // insert accounthead
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IAH"));      
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@ShortName", sShortName));
        arraylst.Add(new SqlParameter("@ParentID", iParentId));
        return Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst));
    }
    public int InsertAccount() // update account
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IAC"));
        arraylst.Add(new SqlParameter("@Description", sDescription));
        arraylst.Add(new SqlParameter("@ShortName", sShortName));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyId));
        arraylst.Add(new SqlParameter("@OpeningBalance", dOpeningBalance));
       
        return Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst));
    }

    public bool IsAccountHead() // checking is account head or accont
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CAH"));
        arraylst.Add(new SqlParameter("@Description",sDescription ));
        return ((Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst)) == 1 ? true : false));
    }
    public void DeleteAccountHead() // delete account head
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "DAH"));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyId));
        ExecuteNonQuery("HRspAccountsReference", arraylst);
         
    }
    public void DeleteAccount() // delete account
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "DAC"));
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
        ExecuteNonQuery("HRspAccountsReference", arraylst);

    }
    public void DeleteAccounts()// Delete more than one accounts
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "DACS"));
        arraylst.Add(new SqlParameter("@AccountIds", sAccountIds));
        ExecuteNonQuery("HRspAccountsReference", arraylst);

    }
    public void DeleteAccountHeads()// Delete more than one account heads
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "DAHS"));
        arraylst.Add(new SqlParameter("@GLAccountIds", sGlAccountIds));
        ExecuteNonQuery("HRspAccountsReference", arraylst);

    }
    public int GetCompanyId()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GC"));      
        arraylst.Add(new SqlParameter("@GLAccountId", iGlAccountId));
       

        return Convert.ToInt32(ExecuteScalar("HRspAccountsReference", arraylst));
    }



}
