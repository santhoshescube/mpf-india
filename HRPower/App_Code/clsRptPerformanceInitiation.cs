﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptPerformanceInitiation
/// </summary>
public class clsRptPerformanceInitiation
{
	public clsRptPerformanceInitiation()
	{
		//
		// TODO: Add constructor logic here
		//
	}



    public static DataTable GetInitiationmaster(int CompanyID, int BranchID, int DepartmentID, bool IsCompanyOnly, int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@DepartmentId", DepartmentID));
        alParameters.Add(new SqlParameter("@IncludeComp", IsCompanyOnly));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
   
    public static DataTable GetInitiationDetails(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID ));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        

        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
    public static DataTable GetEvaluationMaster(int CompanyID, int BranchID, int DepartmentID, bool IsCompanyOnly, int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@DepartmentId", DepartmentID));
        alParameters.Add(new SqlParameter("@IncludeComp", IsCompanyOnly));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }

    public static DataTable GetEvaluationDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
    public static DataTable GetEvaluationGoals()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        

        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
    public static DataTable GetActionMaster(int CompanyID, int BranchID, int DepartmentID, bool IsCompanyOnly, int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@DepartmentId", DepartmentID));
        alParameters.Add(new SqlParameter("@IncludeComp", IsCompanyOnly));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
    public static DataTable GetActionDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        

        return new DataLayer().ExecuteDataTable("HRspRptPerformance", alParameters);
    }
}
