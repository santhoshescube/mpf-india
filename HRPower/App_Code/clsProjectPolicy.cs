﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsProjectPolicy
/// purpose : Handle Project Policy
/// created : Lekshmi
/// Date    : 07.12.2010
/// </summary>
public class clsProjectPolicy : DL
{
    public clsProjectPolicy() { }

    private int iProjectPolicyID;
    private string sProjectPolicyName;
    private string sOffdayID;
    private decimal dRate;
    private string sRemarks;
    private bool bHolidayApplicable;

    public int ProjectPolicyID
    {
        get { return iProjectPolicyID; }
        set { iProjectPolicyID = value; }
    }

    public string ProjectPolicyName
    {
        get { return sProjectPolicyName; }
        set { sProjectPolicyName = value; }
    }

    public string OffdayID
    {
        get { return sOffdayID; }
        set { sOffdayID = value; }
    }

    public decimal Rate
    {
        get { return dRate; }
        set { dRate = value; }
    }

    public string Remarks
    {
        get { return sRemarks; }
        set { sRemarks = value; }
    }
    public bool HolidayApplicable
    {
        get { return bHolidayApplicable; }
        set { bHolidayApplicable = value; }
    }

    public DataTable FillAllOffDays()                                       // Select All Off days from DayReference 
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAD"));
        return ExecuteDataTable("HRspProjectPolicy", arraylst);
    }

    public int InsertUpdateProjectPolicy(bool bIsInsert)                // Insertion when bIsInsert=true , Updation when bIsInsert=false
    {
        ArrayList arraylst = new ArrayList();

        if (bIsInsert)
            arraylst.Add(new SqlParameter("@Mode", "I"));
        else
        {
            arraylst.Add(new SqlParameter("@Mode", "U"));
            arraylst.Add(new SqlParameter("@ProjectPolicyID", iProjectPolicyID));
        }

        arraylst.Add(new SqlParameter("@ProjectPolicyName", sProjectPolicyName));
        arraylst.Add(new SqlParameter("@OffdayID", sOffdayID));
        arraylst.Add(new SqlParameter("@Rate", dRate));
        arraylst.Add(new SqlParameter("@Remarks", sRemarks));
        arraylst.Add(new SqlParameter("@HolidayApplicable", bHolidayApplicable));

        return  Convert.ToInt32(ExecuteScalar("HRspProjectPolicy", arraylst));
    }

    public bool DeleteProjectPolicy()                                                // Deletion Of Project Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@ProjectPolicyID", iProjectPolicyID));

        try
        {
            // first check if current project-policy is assigned to any projects
            // if assigned, it should not be deleted.
            if (!this.IsDeleteable())
                return false;

            ExecuteNonQuery("HRspProjectPolicy", arraylst);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool IsDeleteable()
    { 
        object objProjectCount = this.ExecuteScalar("HRspProjectPolicy", new ArrayList{
            new SqlParameter("@Mode", "ID"),
            new SqlParameter("@ProjectPolicyID", this.iProjectPolicyID)
        });

        return objProjectCount.ToInt32() == 0;
    }

    public DataSet GetAllProjectPolicies()    // Getting all project policies
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));
        return ExecuteDataSet("HRspProjectPolicy", alParameters);
    }
}
