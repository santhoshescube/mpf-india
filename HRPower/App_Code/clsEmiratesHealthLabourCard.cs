﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.IO;

/// <summary>
/// Summary description for clsEmiratesHealthLabourCard
/// </summary>
public class clsEmiratesHealthLabourCard:DL
{
	public clsEmiratesHealthLabourCard()
	{	}

    public int CardID { get; set; }
    public int EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public int UserId { get; set; }
    public string CardNumber { get; set; }
    public string CardPersonalIDNumber { get; set; }
    public DateTime IssueDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public DateTime RenewalDate { get; set; }
    public string Remarks { get; set; }
    public bool IsRenewed { get; set; }
    public int PageSize { get; set; }
    public int PageIndex { get; set; }

    public void BeginEmp(int iFormType)
    {
        if (iFormType == 1)
            BeginTransaction("HRspEmployeeHealthCardTransactions");
        else if (iFormType == 2)
            BeginTransaction("MvfEmployeeLabourCardTransactions");
        else if (iFormType == 3)
            BeginTransaction("MvfEmployeeEmiratesCardTransactions");
    }

    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    #region GetEmployees
    public DataTable GetEmployees(int intEmployeeID,bool blnAddStatus)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 12));
        alparameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alparameters.Add(new SqlParameter("@AddStatus", blnAddStatus));
        alparameters.Add(new SqlParameter("@CompanyId", CompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspEmployeeHealthCardTransactions", alparameters);
    }
    #endregion GetEmployees

    #region SaveCardDetails
    public int SaveCardDetails(bool blAddStatus,int iFormType)
    {
        ArrayList parameters = new ArrayList();
        int iResult = 0;
        //parameters.Add(new SqlParameter("@HealthCardId", CardID));
        parameters.Add(new SqlParameter("@IssueDate", IssueDate));
        parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        parameters.Add(new SqlParameter("@CardNumber", CardNumber));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@Remarks", Remarks));      
        parameters.Add(new SqlParameter("@IsRenewed", IsRenewed));

        if (iFormType == 1)
        {            
            parameters.Add(new SqlParameter("@CardPersonalNumber", CardPersonalIDNumber));
            parameters.Add(new SqlParameter("@Mode", (blAddStatus ? 2 : 3)));
            parameters.Add(new SqlParameter("@HealthCardId", CardID));
            iResult = (ExecuteScalar("HRspEmployeeHealthCardTransactions", parameters)).ToInt32();
        }
        else if (iFormType == 2)
        {
            parameters.Add(new SqlParameter("@CardPersonalNumber", CardPersonalIDNumber));
            parameters.Add(new SqlParameter("@Mode", (blAddStatus ? 2 : 3)));
            parameters.Add(new SqlParameter("@LabourCardId", CardID));
            iResult = (ExecuteScalar("MvfEmployeeLabourCardTransactions", parameters)).ToInt32();
        }
        else
        {
            parameters.Add(new SqlParameter("@IDNumber", CardPersonalIDNumber));
            parameters.Add(new SqlParameter("@Mode", (blAddStatus ? 1 : 2)));
            parameters.Add(new SqlParameter("@EmiratesID", CardID));
            iResult = (ExecuteScalar("MvfEmployeeEmiratesCardTransactions", parameters)).ToInt32();
        }
        return iResult;
    }
    #endregion SaveCardDetails

    #region GetCards
    public DataTable GetCards(int iPageSize, int iPageIndex, string sSearchKey, int CompanyId, int iFormType)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 13));
        alparameters.Add(new SqlParameter("@PageSize", iPageSize));
        alparameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alparameters.Add(new SqlParameter("@FormType", iFormType));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey.Trim()));
        alparameters.Add(new SqlParameter("@CompanyId", CompanyId));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspEmployeeHealthCardTransactions", alparameters);
    }
    #endregion GetCards

    #region GetCount
    public int GetCount(string sSearchKey, int iFormType,int CompanyID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 14));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey.EscapeUnsafeChars()));
        alparameters.Add(new SqlParameter("@FormType", iFormType));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteScalar("HRspEmployeeHealthCardTransactions", alparameters).ToInt32();
    }
    #endregion GetCount

    #region GetCardDetails
    public DataTable GetCardDetails(int intCardID, int iFormType,int iDocType)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 15));
        alparameters.Add(new SqlParameter("@CardID", intCardID));
        alparameters.Add(new SqlParameter("@FormType", iFormType));
        alparameters.Add(new SqlParameter("@DocumentTypeID", iDocType));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspEmployeeHealthCardTransactions", alparameters);
    }
    #endregion GetCardDetails

    #region DeleteCard
    public bool DeleteCard(int CardID,int iFormType)
    {
        ArrayList parameters = new ArrayList();
        bool blnResult = false;
        parameters.Add(new SqlParameter("@Mode", 4));
        parameters.Add(new SqlParameter("@CardID", CardID));
        if(iFormType==1)
           blnResult= (ExecuteNonQuery("HRspEmployeeHealthCardTransactions", parameters)) > 0 ? true : false;
        else if(iFormType==2)
            blnResult = (ExecuteNonQuery("MvfEmployeeLabourCardTransactions", parameters)) > 0 ? true : false;
        else
            blnResult = (ExecuteNonQuery("MvfEmployeeEmiratesCardTransactions", parameters)) > 0 ? true : false;
        return blnResult;
    }
    #endregion DeleteCard

    #region CheckDuplication
    public bool CheckDuplication(int CardID, string CardNumber,int iFormType)
    {
        Boolean blnExists = false;
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 6));
        parameters.Add(new SqlParameter("@CardID", CardID));
        parameters.Add(new SqlParameter("@CardNumber", CardNumber));
        parameters.Add(new SqlParameter("@FormType", iFormType));
        blnExists = (ExecuteScalar("HRspEmployeeHealthCardTransactions", parameters)).ToInt32() > 0 ? true : false;
        return blnExists;
    }
    #endregion CheckDuplication

    #region GetAttachments
    public DataTable GetAttachments(int iCardID, int iEmployeeID,int iDocType)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 18));
        parameters.Add(new SqlParameter("@CardID", iCardID));
        parameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        parameters.Add(new SqlParameter("@DocumentTypeID", iDocType));
        return ExecuteDataTable("HRspEmployeeHealthCardTransactions", parameters);
    }
    #endregion GetAttachments

    #region GetPrintText
    public string GetPrintText(int CardID,int FormType)
    {
        StringBuilder sb = new StringBuilder();
        DataTable dtDocument = null;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@CardID", CardID));
        if (FormType == 1)
        {
            alParameters.Add(new SqlParameter("@Mode", 9));
            dtDocument = new DataLayer().ExecuteDataTable("HRspEmployeeHealthCardTransactions", alParameters);
        }
        else if (FormType == 2)
        {
            alParameters.Add(new SqlParameter("@Mode", 8));
            dtDocument = new DataLayer().ExecuteDataTable("MvfEmployeeLabourCardTransactions", alParameters);
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", 17));
            dtDocument = new DataLayer().ExecuteDataTable("HRspEmployeeHealthCardTransactions", alParameters);
        }

        if (dtDocument != null && dtDocument.Rows.Count > 0)
        {
            string renew = dtDocument.Rows[0]["IsRenewed"].ToString();
            if (renew == "Renewed")
            {
                renew = "Yes";
            }
            else
            {
                renew = "No";
            }
            sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
            sb.Append("<tr><td><table border='0'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dtDocument.Rows[0]["DocumentType"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr><td style='padding-left:15px;'>");
            sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

            sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;word-break:break-all'>" + Convert.ToString(dtDocument.Rows[0]["CardNumber"]) + "</td></tr>");
            sb.Append("<tr><td width='150px'>Employee</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["EmployeeFullName"]) + "</td></tr>");
            sb.Append("<tr><td width='150px' style='word-break:break-all'>Card Personal Number</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["CardPersonalNumber"]) + "</td></tr>");
            sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + (dtDocument.Rows[0]["IssueDate"]).ToString() + "</td></tr>");
            sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + (dtDocument.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
            sb.Append("<tr><td>Is Renewed</td><td>:</td><td>" + renew + "</td></tr>");
            sb.Append("<tr><td width='150px' style='word-break:break-all'>Remarks</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["Remarks"]) + "</td></tr>");

            sb.Append("</td>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr></table>");

        }

        return sb.ToString();
    }
    #endregion GetPrintText

    public DataTable PrintSelected(string sCardIDs,int iFormType)
    {
        StringBuilder sb = new StringBuilder();

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@CardIDs", sCardIDs));
        alParameters.Add(new SqlParameter("@FormType", iFormType));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return  ExecuteDataTable("HRspEmployeeHealthCardTransactions", alParameters);

        //if (datPrint != null && datPrint.Rows.Count > 0)
        //{
        //    sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        //    sb.Append("<tr><td style='font-weight:bold;' align='center'>" + Convert.ToString(datPrint.Rows[0]["DocumentType"])+ "</td></tr>");
        //    sb.Append("<tr><td>");
        //    sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
        //    sb.Append("<tr style='font-weight:bold;'><td width='100px'> Employee </td><td width='100px'> Card Number </td><td width='150px'>Card Personal Number</td><td width='100px'>Issue Date</td><td width='100px'>Expiry Date</td><td width='100px'>Is renewed</td><td width='100px'>Remarks</td></tr>");
        //    sb.Append("<tr><td colspan='7'><hr></td></tr>");
        //    for (int i = 0; i < datPrint.Rows.Count; i++)
        //    {
        //        string renew = datPrint.Rows[i]["IsRenewed"].ToString();
        //        if (renew == "Renewed")
        //        {
        //            renew = "Yes";
        //        }
        //        else
        //        {
        //            renew = "No";
        //        }
        //        sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["EmployeeFullName"]) + "</td>");
        //        sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardNumber"]) + "</td>");
        //        sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardPersonalNumber"]) + "</td>");
        //        sb.Append("<td>" + datPrint.Rows[i]["IssueDate"].ToString() + "</td>");
        //        sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
        //        sb.Append("<td>" + renew + "</td>");
        //        sb.Append("<td style='word-break:break-all'>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

        //    }
        //    sb.Append("</td></tr>");
        //    sb.Append("</table>");
        //}
        //return sb.ToString();
    }

    #region SaveTreeMaster
    public bool SaveTreeMaster(int iCardID, int iDocType, int iEmployeeID, string sReferenceNumber, string sDocumentName, string sFileName)
    {
        ArrayList prmDocuments = new ArrayList();

        prmDocuments.Add(new SqlParameter("@Mode", 19));
        prmDocuments.Add(new SqlParameter("@ReferenceID", iEmployeeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", iDocType));
        prmDocuments.Add(new SqlParameter("@CardID", iCardID));
        prmDocuments.Add(new SqlParameter("@ReferenceNumber", sReferenceNumber));
        prmDocuments.Add(new SqlParameter("@Docname", sDocumentName));
        prmDocuments.Add(new SqlParameter("@Filename", sFileName));

        return ExecuteScalar("HRspEmployeeHealthCardTransactions", prmDocuments).ToInt32() > 0;
    }
    #endregion SaveTreeMaster

    public int UpdateRenewEmployeeEmiratesCard(int iFormType)  //Update Renew Employee Emirates Card
    {
        ArrayList parameters = new ArrayList();
        int iResult = 0;

        if (iFormType == 1)
        {
            parameters.Add(new SqlParameter("@Mode", 20));
            parameters.Add(new SqlParameter("@HealthCardId", CardID));
            iResult = (ExecuteScalar("HRspEmployeeHealthCardTransactions", parameters)).ToInt32();
        }
        else if (iFormType == 2)
        {
            parameters.Add(new SqlParameter("@Mode", 9));
            parameters.Add(new SqlParameter("@LabourCardId", CardID));
            iResult = (ExecuteScalar("MvfEmployeeLabourCardTransactions", parameters)).ToInt32();
        }
        else
        {
            parameters.Add(new SqlParameter("@Mode", 14));
            parameters.Add(new SqlParameter("@EmiratesID", CardID));
            iResult = (ExecuteScalar("MvfEmployeeEmiratesCardTransactions", parameters)).ToInt32();
        }
        return iResult;
    }
}