﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsTimeSheet
/// </summary>
public class clsTimeSheet : DL
{
    public clsTimeSheet() { }

    private int iEmployeeId;
    private int iShiftId;
    private string sDate;
    private int iMonth;
    private string sDayofWeek;
    private string sWorkTime;
    private string sBreakTime;
    private int iLate;
    private int iEarly;
    private string sOverTime;
    private string sFoodBreakTime;
    private string sTime;
    private int iDailyId;
    private int iAttendanceId;
    private int iCompanyId; //Attendance report
    private int iType;
    private int iYear;
    private bool bIsLateComing;
    private bool bIsEarlyGoing;
    private bool bBreakExceed;
    private bool bLessWorkTime;
    private int iStatus;
    private string sRequestedTo;
    private bool bIsRequest;

    public int EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }

    public int ShiftId
    {
        get { return iShiftId; }
        set { iShiftId = value; }
    }

    public string Date
    {
        get { return sDate; }
        set { sDate = value; }
    }

    public int Month
    {
        get { return iMonth; }
        set { iMonth = value; }
    }

    public string DayofWeek
    {
        get { return sDayofWeek; }
        set { sDayofWeek = value; }
    }

    public string WorkTime
    {
        get { return sWorkTime; }
        set { sWorkTime = value; }
    }

    public string BreakTime
    {
        get { return sBreakTime; }
        set { sBreakTime = value; }
    }

    public string OverTime
    {
        get { return sOverTime; }
        set { sOverTime = value; }
    }

    public string FoodBreakTime
    {
        get { return sFoodBreakTime; }
        set { sFoodBreakTime = value; }
    }

    public string Time
    {
        get { return sTime; }
        set { sTime = value; }
    }

    public int DayId
    {
        get { return iDailyId; }
        set { iDailyId = value; }
    }

    public int AttendanceId
    {
        get { return iAttendanceId; }
        set { iAttendanceId = value; }
    }

    public int CompanyId
    {
        get { return iCompanyId; }
        set { iCompanyId = value; }
    }

    public int Type
    {
        get { return iType ; }
        set { iType = value; }
    }

    public int Year
    {
        get { return iYear; }
        set { iYear = value; }
    }

    public bool IsLateComing
    {
        get { return bIsLateComing; }
        set { bIsLateComing = value; }
    }

    public bool IsEarlyGoing
    {
        get { return bIsEarlyGoing; }
        set { bIsEarlyGoing = value; }
    }

    public bool BreakExceed
    {
        get { return bBreakExceed; }
        set { bBreakExceed = value; }
    }

    public bool LessWorkTime
    {
        get { return bLessWorkTime; }
        set { bLessWorkTime = value; }
    }

    public int Status
    {
        get { return iStatus; }
        set { iStatus = value; }
    }

    public string RequstedTo
    {
        get { return sRequestedTo; }
        set { sRequestedTo = value; }
    }

    public bool IsRequest
    {
        get { return bIsRequest; }
        set { bIsRequest = value; }
    }

    public string CameLateBy { get; set; }
    public string WentEarlyBy { get; set; }
    public int ShiftOrderNo { get; set; }

    public string XmlData { get; set; }

    /// <summary>
    /// Gets or sets shortage in working time of an employee in minutes
    /// </summary>
    public int ShortageInMinutes { get; set; }

    public int ListHolidays(int iDayId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "HL"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));
        arraylst.Add(new SqlParameter("@DayId", iDayId));

        return Convert.ToInt32(ExecuteScalar("HRspTimeSheet", arraylst));
    }

    public DataTable GetShiftDetails(int iDayId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GSD"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));
        arraylst.Add(new SqlParameter("@DayId", iDayId));

        return ExecuteDataTable("HRspTimeSheet", arraylst);
    }

    public DataTable GetDynamicShiftDetails(int iDayID)
    {
        ArrayList alParameters = new ArrayList() { 
            new SqlParameter("@Mode", "GSHD"),
            new SqlParameter("@EmployeeID", iEmployeeId),
            new SqlParameter("@DayID", iDayID)
        };

        return ExecuteDataTable("HRspTimeSheet", alParameters);
    }

    public DataTable GetSplitShiftDetails(int iDayID)
    {
        ArrayList alParameters = new ArrayList() { 
            new SqlParameter("@Mode", "GSSD"),
            new SqlParameter("@EmployeeID", iEmployeeId),
            new SqlParameter("@DayID", iDayID)
        };

        return ExecuteDataTable("HRspTimeSheet", alParameters);
    }
    public DataTable GetAttendanceDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAD"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));

        return ExecuteDataTable("HRspTimeSheet", arraylst);
    }

    public DataTable GetPolicyDetails(int iDayId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPC"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@DayId", iDayId));

        return ExecuteDataTable("HRspTimeSheet", arraylst);
    }

    public int InsertAttendanceSummary(int iDayId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IAS"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));
        arraylst.Add(new SqlParameter("@DayId", iDayId));
        arraylst.Add(new SqlParameter("@ShiftId", iShiftId));
        arraylst.Add(new SqlParameter("@WorkTime", sWorkTime));
        arraylst.Add(new SqlParameter("@BreakTime", sBreakTime));
        arraylst.Add(new SqlParameter("@OverTime", sOverTime));
        arraylst.Add(new SqlParameter("@Late", CameLateBy));
        arraylst.Add(new SqlParameter("@Early", WentEarlyBy));
        arraylst.Add(new SqlParameter("@BreakExceed", bBreakExceed));
        arraylst.Add(new SqlParameter("@LessWorkTime", bLessWorkTime));
        arraylst.Add(new SqlParameter("@AttendanceId", iAttendanceId));
        arraylst.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        arraylst.Add(new SqlParameter("@IsRequest", bIsRequest));
        arraylst.Add(new SqlParameter("@ShortageInMinutes", this.ShortageInMinutes));
        arraylst.Add(new SqlParameter("@xmlData", this.XmlData));

        if(this.ShiftOrderNo > 0) arraylst.Add(new SqlParameter("@ShiftOrderNo", this.ShiftOrderNo));

        return Convert.ToInt32(ExecuteScalar(arraylst));
    }

    public int InsertTimeEntry(int iOrderNo)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "ITE"));
        arraylst.Add(new SqlParameter("@AttendanceId", iAttendanceId));
        arraylst.Add(new SqlParameter("@Time", sTime));
        arraylst.Add(new SqlParameter("@OrderNo", iOrderNo));

        return Convert.ToInt32(ExecuteScalar(arraylst));
    }

    public DataTable GetEmployees()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GE"));
        arraylst.Add(new SqlParameter("@EmployeeId",iEmployeeId));

        return ExecuteDataTable("HRspTimeSheet",arraylst);
    }

    public DataTable GetStatus()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GS"));
        return ExecuteDataTable("HRspTimeSheet", arraylst);
    }

    public int ApproveEntry()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "AE"));
        arraylst.Add(new SqlParameter("@AttendanceId", iAttendanceId));
        arraylst.Add(new SqlParameter("@Status", iStatus));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));

        return Convert.ToInt32(ExecuteScalar("HRspTimeSheet", arraylst));
    }

    public int ApproveAllEntries()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "AE"));
        arraylst.Add(new SqlParameter("@AttendanceId", iAttendanceId));
        arraylst.Add(new SqlParameter("@Status", iStatus));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        arraylst.Add(new SqlParameter("@Date", sDate));

        return Convert.ToInt32(ExecuteScalar(arraylst));
    }

    public DataTable GetRequestedEntry(int iRequestId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAR"));
        arraylst.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspTimeSheet", arraylst);
    }

    public string GetLoginEmpName()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "EN"));
        arraylst.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return Convert.ToString(ExecuteScalar("HRspTimeSheet", arraylst));
    }

    public string GetDate(int iRequestId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GD"));
        arraylst.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspTimeSheet", arraylst));
    }

    public void Begin()
    {
        BeginTransaction("HRspTimeSheet");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }

    // ---------------------------------------------- Reporst section ----------------------------------------------------

    public DataTable GetCompanylist() //Get all company for report
    {
        ArrayList arraylist = new ArrayList();

        arraylist.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataTable("HRspTimeSheet", arraylist);
    }

    public DataTable GetEmployeelist() //Get all Employee for report
    {
        ArrayList arraylist = new ArrayList();

        arraylist.Add(new SqlParameter("@Mode", "GAE"));
        arraylist.Add(new SqlParameter("@ComID", iCompanyId));

        return ExecuteDataTable("HRspTimeSheet", arraylist);
    }

    public DataSet GetAttendance()// Attendance data for Report
    {
        ArrayList arraylist = new ArrayList();

        arraylist.Add(new SqlParameter("@Mode", "0"));
        arraylist.Add(new SqlParameter("@Type", iType));
        arraylist.Add(new SqlParameter("@CompanyID", iCompanyId));
        arraylist.Add(new SqlParameter("@Month", iMonth));
        arraylist.Add(new SqlParameter("@Year", iYear));
        arraylist.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        arraylist.Add(new SqlParameter("@Date",  sDate));

        using (DataSet ds = ExecuteDataSet("PayRptAttendancesummary", arraylist))
        {
            if (ds.Tables.Count > 0)
                ds.Tables[0].TableName = "DtAttendance";

            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }
    }

    public DataSet GetAttendanceByDate()// Attendance report datewise
    {
        ArrayList arraylist = new ArrayList();

        DateTime dtDate = clsCommon.Convert2DateTime(sDate);
     
        arraylist.Add(new SqlParameter("@Mode", 2));
        arraylist.Add(new SqlParameter("@Type", iType));
        arraylist.Add(new SqlParameter("@CompanyID", iCompanyId));
        arraylist.Add(new SqlParameter("@Month", iMonth));
        arraylist.Add(new SqlParameter("@Year", iYear));
        arraylist.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        arraylist.Add(new SqlParameter("@Date", dtDate));

        using (DataSet ds = ExecuteDataSet("PayRptAttendancesummary", arraylist))
        {
            if (ds.Tables.Count > 0)
                ds.Tables[0].TableName = "DtAttendanceSingle";

            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }
    }

    public DataSet GetAttendanceSingle()// Attendance data for Report fro a single employee
    {
        ArrayList arraylist = new ArrayList();

        arraylist.Add(new SqlParameter("@Mode", 2));
        arraylist.Add(new SqlParameter("@Type", iType));
        arraylist.Add(new SqlParameter("@CompanyID", iCompanyId));
        arraylist.Add(new SqlParameter("@Month", iMonth));
        arraylist.Add(new SqlParameter("@Year", iYear));
        arraylist.Add(new SqlParameter("@EmployeeID", iEmployeeId));
        arraylist.Add(new SqlParameter("@Date", sDate));

        using (DataSet ds = ExecuteDataSet("PayRptAttendancesummary", arraylist))
        {
            if (ds.Tables.Count > 0)
                ds.Tables[0].TableName = "DtAttendanceSingle";

            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }


    }

    public DataSet  GetAttendanceDetail(int iEmployeeID, int iCompanyID, int iDayID, string sDate) // From attendancesummary
    {
       
            ArrayList alParameters = new ArrayList();


            alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
            alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
            alParameters.Add(new SqlParameter("@AttDate1", sDate));


            using (DataSet ds = ExecuteDataSet("AttDailyReport", alParameters))
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ds.Tables[0].TableName = "DtAttendanceSummary";
                    ds.Tables.Add(new clsReport().GetReportHeader());
                    //DataTable dtDet = GetAttendanceSummary(iEmployeeID, iCompanyID, iDayID, sDate);
                    ds.Tables.Add(new clsReport().GetAttendanceSummary(iEmployeeID, iCompanyID, iDayID, sDate));
                }
                   
               
                return ds;
            }
      
      
    }

  


}
