﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsTradeLicense
/// </summary>
public class clsTradeLicense : DL
{
	public clsTradeLicense()
	{
		//
		// TODO: Add constructor logic here
		//

	}

    public int TradeLicenseID { get; set; }
    public int CompanyID { get; set; }
    public string Sponsor { get; set; }
    public decimal SponserFee { get; set; }
    public string LicenceNumber { get; set; }
    public string City { get; set; }
    public int ProvinceID { get; set; }
    public string Partner { get; set; }
    public DateTime IssueDate { get; set; }
    public DateTime ExpiryDate { get; set; }
    public string Remarks { get; set; }
    public bool IsRenewed { get; set; }
    public int DocumentTypeID { get; set; }

    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public int UserId { get; set; }
    public string SearchKey { get; set; }
    public int Node { get; set; }
    public string Docname { get; set; }
    public string Filename { get; set; }

    public void BeginEmp()
    {
        BeginTransaction("HRspTradeLicense");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }

    public DataTable FillProvince()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 1));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspTradeLicense", alparameters);
    }

    public int SaveUpdateTradeLicense() //Insert / Update Trade License
    {
        ArrayList alparameters = new ArrayList();

        if (TradeLicenseID == 0)
            alparameters.Add(new SqlParameter("@Mode", 2)); //Insert
        else
            alparameters.Add(new SqlParameter("@Mode", 3)); //Update

        alparameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@Sponsor", Sponsor));
        alparameters.Add(new SqlParameter("@SponserFee", SponserFee));
        alparameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        alparameters.Add(new SqlParameter("@City", City));

        if (ProvinceID > 0)
            alparameters.Add(new SqlParameter("@ProvinceID", ProvinceID));

        alparameters.Add(new SqlParameter("@Partner", Partner));
        alparameters.Add(new SqlParameter("@IssueDate", IssueDate));
        alparameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));
        TradeLicenseID = Convert.ToInt32(ExecuteScalar(alparameters));
        return TradeLicenseID;
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        ExecuteNonQuery("HRspTradeLicense", alParameters);
    }

    public bool IsTradeLicenseNumberExists()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alParameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        return (Convert.ToInt32(ExecuteScalar("HRspTradeLicense", alParameters)) > 0 ? true : false);
    }

    public int SaveTradeLicenseTreemaster() //Insert Trade License Tree Master
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 6));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alparameters.Add(new SqlParameter("@LicenceNumber", LicenceNumber));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@UserId", UserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspTradeLicense", alParameters));
    }

    public DataSet GetAllTradeLicense()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@UserId", UserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspTradeLicense", alParameters);
    }

    public DataSet GetTradeLicenseDocuments()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataSet("HRspTradeLicense", alParameters);
    }

    public DataSet GetTradeLicense()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspTradeLicense", alParameters);
    }

    public DataSet GetTradeLicenseDetailView()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 11));
        alparameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspTradeLicense", alparameters);
    }

    //public string GetSinglePrintText(int iTradeLicenseID)
    //{
    //    StringBuilder sb = new StringBuilder();

    //    DataTable dt = GetPrintText(TradeLicenseID);

    //    sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
    //    sb.Append("<tr><td><table border='0'>");
    //    sb.Append("<tr>");
    //    sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</td>");
    //    sb.Append("</tr>");
    //    sb.Append("</table>");
    //    sb.Append("<tr><td style='padding-left:15px;'>");
    //    sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
    //    sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
    //    sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("Sponsor.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Sponsor"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetLocalResourceObject("SponserFee.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SponserFee"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetLocalResourceObject("LicenseNumber.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetLocalResourceObject("City.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["City"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetLocalResourceObject("Province.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Province"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetLocalResourceObject("Partner.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Partner"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
    //    sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
    //    sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
    //    sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");
    //    sb.Append("</td>");
    //    sb.Append("</table>");
    //    sb.Append("</td>");
    //    sb.Append("</tr></table>");

    //    return sb.ToString();
    //}

    public DataTable GetPrintText(int iTradeLicenseID)
    {       
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@TradeLicenseID", iTradeLicenseID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
       
        return ExecuteDataSet("HRspTradeLicense", alParameters).Tables[0];
    }

    public DataTable GetTradeLicenseFilenames()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@TradeLicenseID", TradeLicenseID));
        return ExecuteDataTable("HRspTradeLicense", alParameters);
    }

    //public string CreateSelectedEmployeesContent(string sTradeLicenseIDs)
    //{
    //    //StringBuilder sb = new StringBuilder();
    //    //ArrayList arraylst = new ArrayList();
    //    //arraylst.Add(new SqlParameter("@Mode", 13));
    //    //arraylst.Add(new SqlParameter("@TradeLicenseIDs", sTradeLicenseIDs));
    //    //DataTable dt = ExecuteDataTable("HRspTradeLicense", arraylst);

    //    StringBuilder sb = new StringBuilder();

    //    DataTable dt = CreateSelectedEmployeesPrintContent(sTradeLicenseIDs);

    //    sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
    //    sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
    //    sb.Append("<tr><td>");
    //    sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

    //    sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetLocalResourceObject("Company.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("Sponsor.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("SponsorFee.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("LicenceNumber.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("City.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("Province.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetLocalResourceObject("Partner.Text") + "</td>" +
    //                                            "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td>" +
    //                                            "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td>" +
    //                                            "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td>" +
    //                                            "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td></tr>");
    //    sb.Append("<tr><td colspan='9'><hr></td></tr>");
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CompanyName"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Sponsor"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SponserFee"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["City"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Province"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Partner"]) + "</td>");
    //        sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
    //        sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
    //        sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
    //        sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");
    //    }
    //    sb.Append("</td></tr>");
    //    sb.Append("</table>");

    //    return sb.ToString();
    //}

    public DataTable CreateSelectedEmployeesPrintContent(string sTradeLicenseIDs)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", 13));
        arraylst.Add(new SqlParameter("@TradeLicenseIDs", sTradeLicenseIDs));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        
        return ExecuteDataTable("HRspTradeLicense", arraylst);

    }
    public void DeleteTradeLicenseDocument()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@Node", Node));
        ExecuteNonQuery("HRspTradeLicense", alParameters);
    }
}