﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsDocumentAlert
/// </summary>
public class clsDocumentAlert : DL
{
    public clsDocumentAlert()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    clsUserMaster objUser;
    public bool AlertMessage(int DoctypeID, string DocType, string Description, int CommonID, string RefNo, DateTime ExpiryDate, string Filter, int iReferenceId, string sEmpName, bool IsCompany)
    {
        try
        {

            DateTime DueDate;
            string AlertMess;
            int ProcessingTime = 0;
            ArrayList parameters = new ArrayList();
            objUser = new clsUserMaster();
            int uId = objUser.GetUserId();
            bool bAlertExists = false;
            bool isExpiry = true;
            DataTable DsProcess = null;
            if (Filter == "Handover")
            {
                isExpiry = false;

                DsProcess = FillCombos(new string[] { "userid,ReturnTime", "processingtimemaster", "ReturnTime<>0 and documenttypeid=" + DoctypeID + "", "", "" });

                if (DsProcess.Rows.Count > 0)
                {

                    ProcessingTime = DsProcess.Rows[0]["ReturnTime"].ToInt32();

                }
            }
            else


                DsProcess = FillCombos(new string[] { "userid,processingtime", "processingtimemaster", "ProcessingTime<>0 and documenttypeid=" + DoctypeID + "", "", "" });


            if (DsProcess.Rows.Count > 0)
            {
                for (int i = 0; i <= DsProcess.Rows.Count - 1; i++)
                {
                    uId = Convert.ToInt32(DsProcess.Rows[i]["userid"]);
                    if (isExpiry)
                    {
                        ProcessingTime = Convert.ToInt32(DsProcess.Rows[i]["processingtime"]);

                    }
                    if (ProcessingTime <= 0) continue;

                    //DueDate = (DateAdd(DateInterval.Day, -(ProcessingTime), ExpiryDate));

                    DueDate = ExpiryDate.AddDays(-(ProcessingTime));

                    AlertMess = GenerateAlertMess(Filter, DueDate, Description, CommonID, RefNo, ExpiryDate, iReferenceId, sEmpName);

                    DataTable datData = FillCombos(new string[] { "1", "Alerts", "CommonId=" + CommonID + " and DocumentTYpeID=" + DoctypeID + " and UserID=" + uId + " and IsExpiry= " + (isExpiry ? 1 : 0), "", "" });

                    if (datData.Rows.Count > 0)
                    {
                        bAlertExists = true;
                    }
                    else
                    {
                        bAlertExists = false;
                    }
                    if (bAlertExists)
                    {
                        parameters = new ArrayList();
                        parameters.Add(new SqlParameter("@Mode", 2));
                        parameters.Add(new SqlParameter("@userID", uId));
                        parameters.Add(new SqlParameter("@CommonID", CommonID));
                        parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
                        parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
                        parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
                        parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
                        parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
                        parameters.Add(new SqlParameter("@IsCompany", IsCompany));
                        parameters.Add(new SqlParameter("@IsExpiry", isExpiry));
                        parameters.Add(new SqlParameter("@EmployeeID", iReferenceId));
                        new DataLayer().ExecuteNonQuery("SpAlertMessages", parameters);
                    }
                    else
                    {
                        parameters = new ArrayList();
                        parameters.Add(new SqlParameter("@Mode", 1));
                        parameters.Add(new SqlParameter("@userID", uId));
                        parameters.Add(new SqlParameter("@CommonID", CommonID));
                        parameters.Add(new SqlParameter("@DocumentTypeID", DoctypeID));
                        parameters.Add(new SqlParameter("@ReferenceNo", RefNo));
                        parameters.Add(new SqlParameter("@AlertMessage", AlertMess));
                        parameters.Add(new SqlParameter("@AlertDueDate", DueDate));
                        parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
                        parameters.Add(new SqlParameter("@IsCompany", IsCompany));
                        parameters.Add(new SqlParameter("@IsExpiry", isExpiry));
                        parameters.Add(new SqlParameter("@EmployeeID", iReferenceId));
                        new DataLayer().ExecuteNonQuery("SpAlertMessages", parameters);
                    }
                }
            }


            DsProcess = null;

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }


    private string GenerateAlertMess(string filterpass, DateTime DueDate, string Description, int CommonID, string RefNo, DateTime ExpiryDate, int EmpId, string EmpName)
    {

        string AlertMsg = "";
        try
        {
            switch (filterpass)
            {
                case "Emp":

                    //'Message 
                    AlertMsg = "Employee: " + EmpName + ", " + Description + " " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// .Please start the renewal process by " + DueDate.ToString("dd/MMM/yyyy") + "";
                    break;

                case "Comp":
                    //'Message 
                    AlertMsg = "Company: " + EmpName + " , " + Description + " " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// .Please start the renewal process by " + DueDate.ToString("dd/MMM/yyyy") + "";
                    break;

                case "Others":
                    AlertMsg = "" + Description + " - " + RefNo + " expires on " + ExpiryDate.ToString("dd/MMM/yyyy") + "";// . Please start the renewal process by  " + DueDate.ToString("dd/MMM/yyyy") + "";
                    break;
                case "Employee":

                    AlertMsg = "Employee: " + EmpName + ", " + Description + " is " + ExpiryDate.ToString("dd/MMM/yyyy") + " ";
                    break;
                case "Handover":
                    AlertMsg = "" + Description + " - " + RefNo + " issued to " + EmpName.Trim() + " and expected to return on " + ExpiryDate.ToString("dd/MMM/yyyy") + " . ";
                    break;
                case "Candidate":
                    AlertMsg = "" + Description + " - " + RefNo + " issued to " + EmpName.Trim() + " will expire on " + ExpiryDate.ToString("dd/MMM/yyyy") + " . ";
                    break;
            }
            return AlertMsg;
        }
        catch (Exception)
        {
            return AlertMsg;
        }

    }

    public int GetUserAlertCount()
    {
        objUser = new clsUserMaster();
        int uId = objUser.GetUserId();

        int iCount = 0;
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "UC"));
        parameters.Add(new SqlParameter("@UserID", uId));
        SqlDataReader sdr = new DataLayer().ExecuteReader("AlertExpiryInfoSummary", parameters, CommandBehavior.CloseConnection);
        if (sdr.Read())
            iCount = sdr["AlCount"].ToInt32();
        sdr.Close();
        return iCount;
    }

    public DataTable GetUserAlerts()
    {
        objUser = new clsUserMaster();
        int uId = objUser.GetUserId();

        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "UTS"));
        parameters.Add(new SqlParameter("@UserID", uId));
        return new DataLayer().ExecuteDataTable("AlertExpiryInfoSummary", parameters);
    }

    public DataTable FillCombos(string[] saFilterValues)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", "0"));
        prmCommon.Add(new SqlParameter("@Fields", saFilterValues[0]));
        prmCommon.Add(new SqlParameter("@TableName", saFilterValues[1]));
        prmCommon.Add(new SqlParameter("@Condition", saFilterValues[2]));
        return ExecuteDataTable("spCommonReferenceTransaction", prmCommon);
    }

    /* FOR ALERT REPORT */

    /* Modified By : Sruthy K
     * Date        : 14 Aug 2013
     * */

    //Function for getting all the alerts based on the filteration
    public static DataTable GetAlerts(DateTime? FromDate, DateTime? ToDate, int UserID, int AlertTypeID, string SearchKey, int PageSize, int PageIndex)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", 2));
        prmCommon.Add(new SqlParameter("@UserID", UserID));
        prmCommon.Add(new SqlParameter("@SearchKey", SearchKey));
        if (FromDate != null)
            prmCommon.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != null)
            prmCommon.Add(new SqlParameter("@ToDate", ToDate));

        prmCommon.Add(new SqlParameter("@AlertTypeID", AlertTypeID));
        prmCommon.Add(new SqlParameter("@PageIndex", PageIndex));
        prmCommon.Add(new SqlParameter("@PageSize", PageSize));
        return new DataLayer().ExecuteDataTable("spAlertReport", prmCommon);
    }

    //Function for getting the total record count in the alert based on the parameters specified
    public static int GetTotalRecordCount(DateTime? FromDate, DateTime? ToDate, int UserID, int AlertTypeID, string SearchKey)
    {
        ArrayList prmCommon = new ArrayList();
        prmCommon.Add(new SqlParameter("@Mode", 4));
        prmCommon.Add(new SqlParameter("@UserID", UserID));
        prmCommon.Add(new SqlParameter("@SearchKey", SearchKey));
        if (FromDate != null)
            prmCommon.Add(new SqlParameter("@FromDate", FromDate));
        if (ToDate != null)
            prmCommon.Add(new SqlParameter("@ToDate", ToDate));
        prmCommon.Add(new SqlParameter("@AlertTypeID", AlertTypeID));
        return new DataLayer().ExecuteScalar("spAlertReport", prmCommon).ToInt32();
    }

    public bool DeleteExpiryAlertMessage(int iDocumentTypeID, int iCommonID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 4));
        parameters.Add(new SqlParameter("@DocumentTypeID", iDocumentTypeID));
        parameters.Add(new SqlParameter("@CommonID", iCommonID));
        ExecuteNonQuery("SpAlertMessages", parameters);
        return true;
    }
}