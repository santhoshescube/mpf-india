﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSalaryScale
/// </summary>
public class clsSalaryScale : DL
{
    int intSalaryScaleID;
    int intRowNumber;
    int intAddDedID;
    int intCreatedBy;
    int intDeductionPolicyID;

    bool blnIsAddition;
    string strDescription;
    decimal decGrossSalary;

    public int SalaryScaleID { set { intSalaryScaleID = value; } get { return intSalaryScaleID; } }
    public int RowNumber { set { intRowNumber = value; } get { return intRowNumber; } }
    public string Description { set { strDescription = value; } get { return strDescription; } }
    public decimal GrossSalary { set { decGrossSalary = value; } get { return decGrossSalary; } }
    public int CreatedBy { set { intCreatedBy = value; } get { return intCreatedBy; } }

    public int AddDedID { set { intAddDedID = value; } get { return intAddDedID; } }
    public bool IsAddition { set { blnIsAddition = value; } get { return blnIsAddition; } }
    public int DeductionPolicyID { set { intDeductionPolicyID = value; } get { return intDeductionPolicyID; } }

    public clsSalaryScale() { }

    public DataSet FillAllCombos()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "FC"));
        return ExecuteDataSet("SalaryScaleTransactions", alParameters);
    }

    public DataSet FillParticulars()   //Fill Dropdown Particulars
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 9));
        alparameters.Add(new SqlParameter("@IsAddition", blnIsAddition));

        return ExecuteDataSet("SalaryScaleTransactions", alparameters);
    }

    public DataSet GetPolicyDetails()   //To get the policy details of the selected policy
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 10));
        alparameters.Add(new SqlParameter("@AddDedID", intAddDedID));

        return ExecuteDataSet("SalaryScaleTransactions", alparameters);
    }

    public DataSet GetSalaryScaleDetails()   //To get the SalaryScale Details of the selected Row
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 1));
        alparameters.Add(new SqlParameter("@RowNum", intRowNumber));

        return ExecuteDataSet("SalaryScaleTransactions", alparameters);
    }

    public int GetSalaryScaleCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 11));
        return Convert.ToInt32(ExecuteScalar("SalaryScaleTransactions", alParameters));
    }

    public int SaveSalaryScaleMaster()
    {
        ArrayList alParameters = new ArrayList();

        if (intSalaryScaleID == 0)
            alParameters.Add(new SqlParameter("@Mode", 2));
        else
            alParameters.Add(new SqlParameter("@Mode", 3));

        alParameters.Add(new SqlParameter("@Description", strDescription));
        alParameters.Add(new SqlParameter("@GrossSalary", decGrossSalary));
        alParameters.Add(new SqlParameter("@CreatedBy", intCreatedBy));

        return Convert.ToInt32(ExecuteScalar("SalaryScaleTransactions", alParameters));
    }

    public int SaveSalaryScaleDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 5));

        alParameters.Add(new SqlParameter("@ScaleID", intSalaryScaleID));
        alParameters.Add(new SqlParameter("@AddDedID", intAddDedID));
        alParameters.Add(new SqlParameter("@IsAddition", blnIsAddition));
        alParameters.Add(new SqlParameter("@DeductionPolicyID", intDeductionPolicyID));

        return ExecuteNonQuery("SalaryScaleTransactions", alParameters);
    }
}