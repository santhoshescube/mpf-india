﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsPaymentPolicy
/// purpose : Handle PaymentPolicy
/// created : Lekshmi
/// Date    : 09.12.2010
/// </summary>
public class clsPaymentPolicy:DL 
{
    public clsPaymentPolicy() { }

    private int iPayPolicyID;
    private string sPolicyName;
    private int iPaymentClassificationID;
    private int iPayCalculationTypeID;

    private int iPayDtlsID;
    private int iJobID;
    private int iUnitItemID;
    private decimal dLimit;
    private decimal dNormalRate;
    private decimal dBelowRate;
    private decimal dAboveRate;
    private decimal dOverTimeLimit;
    private decimal dHolidayRate;

    public int PayPolicyID
    {
        get { return iPayPolicyID ; }
        set { iPayPolicyID  = value; }
    }

    public string PolicyName
    {
        get { return sPolicyName; }
        set { sPolicyName = value; }
    }

    public int PaymentClassificationID
    {
        get { return iPaymentClassificationID; }
        set { iPaymentClassificationID = value; }
    }

    public int PayCalculationTypeID
    {
        get { return iPayCalculationTypeID; }
        set { iPayCalculationTypeID = value; }
    }

    public int PayDtlsID
    {
        get { return iPayDtlsID; }
        set { iPayDtlsID = value; }
    }

    public int JobID
    {
        get { return iJobID; }
        set { iJobID = value; }
    }

    public int UnitItemID
    {
        get { return iUnitItemID; }
        set { iUnitItemID = value; }
    }

    public decimal Limit
    {
        get { return dLimit; }
        set { dLimit = value; }
    }

    public decimal NormalRate
    {
        get { return dNormalRate; }
        set { dNormalRate = value; }
    }

    public decimal BelowRate
    {
        get { return dBelowRate; }
        set { dBelowRate = value; }
    }

    public decimal AboveRate
    {
        get { return dAboveRate; }
        set { dAboveRate = value; }
    }

    public decimal OverTimeLimit
    {
        get { return dOverTimeLimit; }
        set { dOverTimeLimit = value; }
    }

    public decimal HolidayRate
    {
        get { return dHolidayRate; }
        set { dHolidayRate = value; }
    }

    public DataSet GetAllPaymentClassifications()// Get payment Classiifications 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));

         return ExecuteDataSet("HRspPaymentPolicy", alParameters);
    }

    public DataSet GetAllpaymentCalculations()// Get payment Calculations based on payment classification
    {
        ArrayList alParameters=new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAL")); 
        alParameters.Add(new SqlParameter("PaymentClassificationID", iPaymentClassificationID));

        return ExecuteDataSet("HRspPaymentPolicy", alParameters);
    }

    public DataSet GetAllJobs() // Get Jobs 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAJ")); 
      
        return ExecuteDataSet("HRspPaymentPolicy", alParameters);
    }

    public DataSet GetAllUnitItems()// Get UnitItems  
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAU")); 

        return ExecuteDataSet("HRspPaymentPolicy", alParameters);
    }

    public int InsertPaymentPolicy()// Insertion into Payment policymaster
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode","IPPM"));
        alParameters.Add(new SqlParameter("@PolicyName", sPolicyName ));
        alParameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alParameters.Add(new SqlParameter("@PayCalculationTypeID",iPayCalculationTypeID )) ;

        return Convert.ToInt32(ExecuteScalar("HRspPaymentPolicy", alParameters));
    }

    public int updatePaymentPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UPPM"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));
        alParameters.Add(new SqlParameter("@PolicyName", sPolicyName));
        alParameters.Add(new SqlParameter("@PaymentClassificationID", iPaymentClassificationID));
        alParameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));

        return Convert.ToInt32(ExecuteScalar("HRspPaymentPolicy", alParameters));
    }

    public void InsertPaymentPolicyDetail() 
    {
        ArrayList alParameters=new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPPD"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));
        alParameters.Add(new SqlParameter("@JobID", iJobID ));
        alParameters.Add(new SqlParameter("@UnitItemID", iUnitItemID));
        alParameters.Add(new SqlParameter("@Limit", dLimit));
        alParameters.Add(new SqlParameter("@NormalRate", dNormalRate));
        alParameters.Add(new SqlParameter("@BelowRate", dBelowRate));
        alParameters.Add(new SqlParameter("@AboveRate", dAboveRate));
        alParameters.Add(new SqlParameter("@OverTimeLimit", dOverTimeLimit));
        alParameters.Add(new SqlParameter("@HolidayRate", dHolidayRate));

        ExecuteNonQuery("HRspPaymentPolicy", alParameters);

    }

    public void UpdatePaymentPolicyDetail()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UPPD"));
        alParameters.Add(new SqlParameter("@PayDtlsID", iPayDtlsID));
        alParameters.Add(new SqlParameter("@JobID", iJobID));
        alParameters.Add(new SqlParameter("@UnitItemID", iUnitItemID));
        alParameters.Add(new SqlParameter("@Limit", dLimit));
        alParameters.Add(new SqlParameter("@NormalRate", dNormalRate));
        alParameters.Add(new SqlParameter("@BelowRate", dBelowRate));
        alParameters.Add(new SqlParameter("@AboveRate", dAboveRate));
        alParameters.Add(new SqlParameter("@OverTimeLimit", dOverTimeLimit));
        alParameters.Add(new SqlParameter("@HolidayRate", dHolidayRate));

        ExecuteNonQuery("HRspPaymentPolicy", alParameters);
    }

    public DataSet  GetAllPaymentPolicies() // Get all payment policies from payment policymaster
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));

        return ExecuteDataSet("HRspPaymentPolicy", alParameters);
    }

    public DataSet  GetAllPaypolicydetails() // Get all payment policy details from payment policydetails 
    {
        ArrayList alParameters=new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPD"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));

        return ExecuteDataSet("HRspPaymentPolicy", alParameters);

    }

    public DataSet GetAllPaypolicydetailsBasedOnPayCalculation() // Get all payment policy details from payment policydetails 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPDP"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));
        alParameters.Add(new SqlParameter("@PayCalculationTypeID", iPayCalculationTypeID));

        return ExecuteDataSet("HRspPaymentPolicy", alParameters);

    }

    public bool DeletePaymentPolicy() // deletion of payment policy
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));

        try
        {
            ExecuteNonQuery("HRspPaymentPolicy", alParameters);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
       
    }

    public void DeletePaymentPolicyDetail() // deletion of payment policy detail
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@PayDtlsID", iPayDtlsID));

        ExecuteNonQuery("HRspPaymentPolicy", alParameters);
    }
    public void DeleteAllPaymentPolicyDetail() // deletion of all payment policy detail
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DAPD"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));

        ExecuteNonQuery("HRspPaymentPolicy", alParameters);
    }

    public bool IsPolicyExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CPPER"));
        alParameters.Add(new SqlParameter("@PayPolicyID", iPayPolicyID));

        return (Convert.ToInt32(ExecuteScalar("HRspPaymentPolicy", alParameters)) > 0 ? true : false);
    }
}
