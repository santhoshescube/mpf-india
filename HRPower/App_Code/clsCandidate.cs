﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsCandidate
/// </summary>
public class clsCandidate : DL
{
	public clsCandidate()
	{
	}

    // Basic Info
    private long lngCandidateID;
    private int intJobID;
	private bool blnIsThroughAgency;
    private int intAgencyID;
    private string strCandidateCode;
    private int intSalutationID;
    private string strFirstNameEng;
    private string strSecondNameEng;
    private string strThirdNameEng;
    private string strFirstNameArb;
    private string strSecondNameArb;
    private string strThirdNameArb;
    private string strCitizenship;
    private string strCitizenshipArb;
    private string strPlaceofBirth;
    private string strPlaceofBirthArb;
    private string dtDateofBirth;
    private string dtDateofBirthArb;
	private bool blnGender;
    private int intMaritalStatusID;
    private int intNoofSon;
    private string  intNoofSonArb;
    private string strMotherName;
    private string strMotherNameArb;
    private int intReligionID;
    private string strCurrentAddress;
    private string strCurrentAddressArb;
	private string strCurrentPOBox;
    private string strCurrentPOBoxArb;
    private int intCountryID;
	private string strCurrentTelephone;
    private string strCurrentTelephoneArb;
    private int intCurrentNationalityID;
    private int intPreviousNationalityID;
	private string strDoctrine;
    private string strDoctrineArb;
    private string strBasicBusinessPair;
    private string strBasicBusinessPairArb;
    private string strBasicBusinessPairAddress;
    private string strBasicBusinessPairAddressArb;
    private int intBasicCompanyTypeID;
    private int intOfferStatusID;
	private string strRemarks;
    private string strRemarksArb;
    private string strPassword;
    private bool blnIsSelf;
    private int intCreatedBy;

    // Professional Info
	private string strPreviousJob;
    private string strPreviousJobArb;
    private int intQualificationCategoryID;
    private int intQualificationID;
    private string strGraduation;
    private string strGraduationArb;
    private int intGraduationYear;
    private int intGraduationYearArb;
	private string strPlaceOfGraduation;
    private string strPlaceOfGraduationArb;
	private string strCollegeSchool;
    private string strCollegeSchoolArb;
	private string strTypeofExperience;
    private string strTypeofExperienceArb;
	private string strSector;
    private string strSectorArb;
	private string strPosition;
    private string strPositionArb;
    private int intExperienceYear;
    private int intExperienceYearArb;
    private int intExperienceMonth;
    private int intExperienceMonthArb;
	private string strCompensation;
    private string strCompensationArb;
	private string strSpecialization;
    private string strSpecializationArb;
    private string dtTransactionEntryDate;
    private string dtTransactionEntryDateArb;
    private string dtExpectedJoinDate;
    private string dtExpectedJoinDateArb;
	private string strResumeAttachment;

    // Document Info
	private string strPassportNumber;
    private string strPassportNumberArb;
    private int intPassportCountryID;
	private string strPassportIssuePlace;
    private string strPassportIssuePlaceArb;
    private string dtPassportExpiryDate;
    private string dtPassportExpiryDateArb;
    private string dtPassportReleaseDate;
    private string dtPassportReleaseDateArb;
	private string strVisaNumber;
    private string strVisaNumberArb;
	private string strUnifiedNumber;
    private string strUnifiedNumberArb;
    private int intVisaCountryID;
	private string strVisaIssuePlace;
    private string strVisaIssuePlaceArb;
    private string dtVisaExpiryDate;
    private string dtVisaExpiryDateArb;
    private string dtVisaReleaseDate;
    private string dtVisaReleaseDateArb;
	private string strNationalCardNumber;
    private string strNationalCardNumberArb;
    private string strCardNumber;
    private string strCardNumberArb;
    private string dtNationalCardExpiryDate;
    private string dtNationalCardExpiryDateArb;
	private byte[] strRecentPhoto;

    // Other Info
	private string strPermanentAddress;
    private string strPermanentAddressArb;
	private string strHomeStreet;
    private string strHomeStreetArb;
	private string strBusinessAddress;
    private string strBusinessAddressArb;
    private string strBusinessStreet;
    private string strBusinessStreetArb;
	private string strArea;
    private string strAreaArb;
	private string strCity;
    private string strCityArb;
	private string strHomeTelephone;
    private string strHomeTelephoneArb;
	private string strPermanentTelephone;
    private string strPermanentTelephoneArb;
	private string strMobileNumber;
    private string strMobileNumberArb;	
	private string strFax;
    private string strFaxArb;
    private string strEmailID;
	private string strPermanentPOBox;
    private string strPermanentPOBoxArb;
	private string strInsuranceNumber;
    private string strInsuranceNumberArb;
    private string dtSignUpDate;
    private string dtSignUpDateArb;

    private int intSkillID;

    private bool blnReadingArb;
    private bool blnWritingArb;
    private bool blnReadingEng;
    private bool blnWritingEng;
	private string strOtherLanguages;
    private bool blnReadingOtherLanguages;
    private bool blnWritingOtherLanguages;
    private int intPageIndex;
    private int intPageSize;

    private int intNode;
    private string strDocumentName;
    private string strFileName;

    public string SearchKey { get; set; }
    public string SortExpression { get; set; }
     public string SortOrder { get; set; }



     public int CompanyID
     {
         get;
         set;
     }
    // Basic Info
    public long CandidateID
    {
        get { return lngCandidateID; }
        set { lngCandidateID = value; }
    }
    public int ReferralType
    {
        get;
        set;
    }
    public int ReferredBy
    {
        get;
        set;
    }
    public int JobID
    {
        get { return intJobID; }
        set { intJobID = value; }
    }
    public int MinExp
    {
        get;set;
    }
    public int MaxExp
    {
        get;set;
    }
    public bool IsThroughAgency
    {
        get { return blnIsThroughAgency; }
        set { blnIsThroughAgency = value; }
    }
    public int AgencyID
    {
        get { return intAgencyID; }
        set { intAgencyID = value; }
    }
    public string CandidateCode
    {
        get { return strCandidateCode; }
        set { strCandidateCode = value; }
    }
    public int SalutationID
    {
        get { return intSalutationID; }
        set { intSalutationID = value; }
    }
    public string FirstNameEng
    {
        get { return strFirstNameEng; }
        set { strFirstNameEng = value; }
    }
    public string SecondNameEng
    {
        get { return strSecondNameEng; }
        set { strSecondNameEng = value; }
    }  
    public string ThirdNameEng
    {
        get { return strThirdNameEng; }
        set { strThirdNameEng = value; }
    }
    public string FirstNameArb
    {
        get { return strFirstNameArb; }
        set { strFirstNameArb = value; }
    }
    public string SecondNameArb
    {
        get { return strSecondNameArb; }
        set { strSecondNameArb = value; }
    }
    public string ThirdNameArb
    {
        get { return strThirdNameArb; }
        set { strThirdNameArb = value; }
    }
    public string Citizenship
    {
        get { return strCitizenship; }
        set { strCitizenship = value; }
    }
    public string CitizenshipArb
    {
        get { return strCitizenshipArb; }
        set { strCitizenshipArb = value; }
    }
    public string PlaceofBirth
    {
        get { return strPlaceofBirth; }
        set { strPlaceofBirth = value; }
    }
    public string PlaceofBirthArb
    {
        get { return strPlaceofBirthArb; }
        set { strPlaceofBirthArb = value; }
    }
    public string DateofBirth
    {
        get { return dtDateofBirth; }
        set { dtDateofBirth = value; }
    }
    public string DateofBirthArb
    {
        get { return dtDateofBirthArb; }
        set { dtDateofBirthArb = value; }
    }
    public bool Gender
    {
        get { return blnGender; }
        set { blnGender = value; }
    }
    public int MaritalStatusID
    {
        get { return intMaritalStatusID; }
        set { intMaritalStatusID = value; }
    }
    public int NoofSon
    {
        get { return intNoofSon; }
        set { intNoofSon = value; }
    }
    public int NoofSonArb
    {
        get;
        set;
    }
    public string CandidatesID
    {
        get;
        set;
    }
    public string MotherName
    {
        get { return strMotherName; }
        set { strMotherName = value; }
    }
    public string MotherNameArb
    {
        get { return strMotherNameArb; }
        set { strMotherNameArb = value; }
    }
    public int ReligionID
    {
        get { return intReligionID; }
        set { intReligionID = value; }
    }
    public string CurrentAddress
    {
        get { return strCurrentAddress; }
        set { strCurrentAddress = value; }
    }
    public string CurrentAddressArb
    {
        get { return strCurrentAddressArb; }
        set { strCurrentAddressArb = value; }
    }
    public string CurrentPOBox
    {
        get { return strCurrentPOBox; }
        set { strCurrentPOBox = value; }
    }
    public string CurrentPOBoxArb
    {
        get { return strCurrentPOBoxArb; }
        set { strCurrentPOBoxArb = value; }
    }
    public int CountryID
    {
        get { return intCountryID; }
        set { intCountryID = value; }
    }
    public string CurrentTelephone
    {
        get { return strCurrentTelephone; }
        set { strCurrentTelephone = value; }
    }
    public string CurrentTelephoneArb
    {
        get { return strCurrentTelephoneArb; }
        set { strCurrentTelephoneArb = value; }
    }
    public int CurrentNationalityID
    {
        get { return intCurrentNationalityID; }
        set { intCurrentNationalityID = value; }
    }
    public int PreviousNationalityID
    {
        get { return intPreviousNationalityID; }
        set { intPreviousNationalityID = value; }
    }
    public string Doctrine
    {
        get { return strDoctrine; }
        set { strDoctrine = value; }
    }
    public string DoctrineArb
    {
        get { return strDoctrineArb; }
        set { strDoctrineArb = value; }
    }
    public string BasicBusinessPair
    {
        get { return strBasicBusinessPair; }
        set { strBasicBusinessPair = value; }
    }
    public string BasicBusinessPairArb
    {
        get { return strBasicBusinessPairArb; }
        set { strBasicBusinessPairArb = value; }
    }
    public string BasicBusinessPairAddress
    {
        get { return strBasicBusinessPairAddress; }
        set { strBasicBusinessPairAddress = value; }
    }
    public string BasicBusinessPairAddressArb
    {
        get { return strBasicBusinessPairAddressArb; }
        set { strBasicBusinessPairAddressArb = value; }
    }
    public int BasicCompanyTypeID
    {
        get { return intBasicCompanyTypeID; }
        set { intBasicCompanyTypeID = value; }
    }
    public int OfferStatusID
    {
        get { return intOfferStatusID; }
        set { intOfferStatusID = value; }
    }
    public string Remarks
    {
        get { return strRemarks; }
        set { strRemarks = value; }
    }
    public string RemarksArb
    {
        get { return strRemarksArb; }
        set { strRemarksArb = value; }
    }
    public string Password
    {
        get { return strPassword; }
        set { strPassword = value; }
    }
    public int CreatedBy
    {
        get { return intCreatedBy; }
        set { intCreatedBy = value; }
    }
    public bool IsSelf
    {
        get { return blnIsSelf; }
        set { blnIsSelf = value; }
    }

    // Professional Info
    public string PreviousJob
    {
        get { return strPreviousJob; }
        set { strPreviousJob = value; }
    }
    public string PreviousJobArb
    {
        get { return strPreviousJobArb; }
        set { strPreviousJobArb = value; }
    }
    public int QualificationCategoryID
    {
        get { return intQualificationCategoryID; }
        set { intQualificationCategoryID = value; }
    }
    public int QualificationID
    {
        get { return intQualificationID; }
        set { intQualificationID = value; }
    }
    public string Graduation
    {
        get { return strGraduation; }
        set { strGraduation = value; }
    }
    public string GraduationArb
    {
        get { return strGraduationArb; }
        set { strGraduationArb = value; }
    }
    public int GraduationYear
    {
        get { return intGraduationYear; }
        set { intGraduationYear = value; }
    }
    public int GraduationYearArb
    {
        get { return intGraduationYearArb; }
        set { intGraduationYearArb = value; }
    }
    public string PlaceOfGraduation
    {
        get { return strPlaceOfGraduation; }
        set { strPlaceOfGraduation = value; }
    }
    public string PlaceOfGraduationArb
    {
        get { return strPlaceOfGraduationArb; }
        set { strPlaceOfGraduationArb = value; }
    }
    public string CollegeSchool
    {
        get { return strCollegeSchool; }
        set { strCollegeSchool = value; }
    }
    public string CollegeSchoolArb
    {
        get { return strCollegeSchoolArb; }
        set { strCollegeSchoolArb = value; }
    }
    public string TypeofExperience
    {
        get { return strTypeofExperience; }
        set { strTypeofExperience = value; }
    }
    public string TypeofExperienceArb
    {
        get { return strTypeofExperienceArb; }
        set { strTypeofExperienceArb = value; }
    }    
    public string Sector
    {
        get { return strSector; }
        set { strSector = value; }
    }
    public string SectorArb
    {
        get { return strSectorArb; }
        set { strSectorArb = value; }
    }
    public string Position
    {
        get { return strPosition; }
        set { strPosition = value; }
    }
    public string PositionArb
    {
        get { return strPositionArb; }
        set { strPositionArb = value; }
    }
    public int ExperienceYear
    {
        get { return intExperienceYear; }
        set { intExperienceYear = value; }
    }
    public int ExperienceYearArb
    {
        get { return intExperienceYearArb; }
        set { intExperienceYearArb = value; }
    }
    public int ExperienceMonth
    {
        get { return intExperienceMonth; }
        set { intExperienceMonth = value; }
    }
    public int ExperienceMonthArb
    {
        get { return intExperienceMonthArb; }
        set { intExperienceMonthArb = value; }
    }
    public string Compensation
    {
        get { return strCompensation; }
        set { strCompensation = value; }
    }
    public string CompensationArb
    {
        get { return strCompensationArb; }
        set { strCompensationArb = value; }
    }
    public string Specialization
    {
        get { return strSpecialization; }
        set { strSpecialization = value; }
    }
    public string SpecializationArb
    {
        get { return strSpecializationArb; }
        set { strSpecializationArb = value; }
    }
    public string TransactionEntryDate
    {
        get { return dtTransactionEntryDate; }
        set { dtTransactionEntryDate = value; }
    }
    public string TransactionEntryDateArb
    {
        get { return dtTransactionEntryDateArb; }
        set { dtTransactionEntryDateArb = value; }
    }
    public string ExpectedJoinDate
    {
        get { return dtExpectedJoinDate; }
        set { dtExpectedJoinDate = value; }
    }
    public string ExpectedJoinDateArb
    {
        get { return dtExpectedJoinDateArb; }
        set { dtExpectedJoinDateArb = value; }
    }
    public string ResumeAttachment
    {
        get { return strResumeAttachment; }
        set { strResumeAttachment = value; }
    }

    // Document Info
    public string PassportNumber
    {
        get { return strPassportNumber; }
        set { strPassportNumber = value; }
    }
    public string PassportNumberArb
    {
        get { return strPassportNumberArb; }
        set { strPassportNumberArb = value; }
    }
    public int PassportCountryID
    {
        get { return intPassportCountryID; }
        set { intPassportCountryID = value; }
    }
    public string PassportIssuePlace
    {
        get { return strPassportIssuePlace; }
        set { strPassportIssuePlace = value; }
    }
    public string PassportIssuePlaceArb
    {
        get { return strPassportIssuePlaceArb; }
        set { strPassportIssuePlaceArb = value; }
    }
    public string PassportExpiryDate
    {
        get { return dtPassportExpiryDate; }
        set { dtPassportExpiryDate = value; }
    }
    public string PassportExpiryDateArb
    {
        get { return dtPassportExpiryDateArb; }
        set { dtPassportExpiryDateArb = value; }
    }
    public string PassportReleaseDate
    {
        get { return dtPassportReleaseDate; }
        set { dtPassportReleaseDate = value; }
    }
    public string PassportReleaseDateArb
    {
        get { return dtPassportReleaseDateArb; }
        set { dtPassportReleaseDateArb = value; }
    }
    public string VisaNumber
    {
        get { return strVisaNumber; }
        set { strVisaNumber = value; }
    }
    public string VisaNumberArb
    {
        get { return strVisaNumberArb; }
        set { strVisaNumberArb = value; }
    }
    public string UnifiedNumber
    {
        get { return strUnifiedNumber; }
        set { strUnifiedNumber = value; }
    }
    public string UnifiedNumberArb
    {
        get { return strUnifiedNumberArb; }
        set { strUnifiedNumberArb = value; }
    }
    public int VisaCountryID
    {
        get { return intVisaCountryID; }
        set { intVisaCountryID = value; }
    }
    public string VisaIssuePlace
    {
        get { return strVisaIssuePlace; }
        set { strVisaIssuePlace = value; }
    }
    public string VisaIssuePlaceArb
    {
        get { return strVisaIssuePlaceArb; }
        set { strVisaIssuePlaceArb = value; }
    }
    public string VisaExpiryDate
    {
        get { return dtVisaExpiryDate; }
        set { dtVisaExpiryDate = value; }
    }
    public string VisaExpiryDateArb
    {
        get { return dtVisaExpiryDateArb; }
        set { dtVisaExpiryDateArb = value; }
    }
    public string VisaReleaseDate
    {
        get { return dtVisaReleaseDate; }
        set { dtVisaReleaseDate = value; }
    }
    public string VisaReleaseDateArb
    {
        get { return dtVisaReleaseDateArb; }
        set { dtVisaReleaseDateArb = value; }
    }
    public string NationalCardNumber
    {
        get { return strNationalCardNumber; }
        set { strNationalCardNumber = value; }
    }
    public string NationalCardNumberArb
    {
        get { return strNationalCardNumberArb; }
        set { strNationalCardNumberArb = value; }
    }
    public string CardNumber
    {
        get { return strCardNumber; }
        set { strCardNumber = value; }
    }
    public string CardNumberArb
    {
        get { return strCardNumberArb; }
        set { strCardNumberArb = value; }
    }
    public string NationalCardExpiryDate
    {
        get { return dtNationalCardExpiryDate; }
        set { dtNationalCardExpiryDate = value; }
    }
    public string NationalCardExpiryDateArb
    {
        get { return dtNationalCardExpiryDateArb; }
        set { dtNationalCardExpiryDateArb = value; }
    }
    public byte[] RecentPhoto
    {
        get { return strRecentPhoto; }
        set { strRecentPhoto = value; }
    }

    // Other Info
    public string PermanentAddress
    {
        get { return strPermanentAddress; }
        set { strPermanentAddress = value; }
    }
    public string PermanentAddressArb
    {
        get { return strPermanentAddressArb; }
        set { strPermanentAddressArb = value; }
    }
    public string HomeStreet
    {
        get { return strHomeStreet; }
        set { strHomeStreet = value; }
    }
    public string HomeStreetArb
    {
        get { return strHomeStreetArb; }
        set { strHomeStreetArb = value; }
    }
    public string BusinessAddress
    {
        get { return strBusinessAddress; }
        set { strBusinessAddress = value; }
    }
    public string BusinessAddressArb
    {
        get { return strBusinessAddressArb; }
        set { strBusinessAddressArb = value; }
    }
    public string BusinessStreet
    {
        get { return strBusinessStreet; }
        set { strBusinessStreet = value; }
    }
    public string BusinessStreetArb
    {
        get { return strBusinessStreetArb; }
        set { strBusinessStreetArb = value; }
    }
    public string Area
    {
        get { return strArea; }
        set { strArea = value; }
    }
    public string AreaArb
    {
        get { return strAreaArb; }
        set { strAreaArb = value; }
    }
    public string City
    {
        get { return strCity; }
        set { strCity = value; }
    }
    public string CityArb
    {
        get { return strCityArb; }
        set { strCityArb = value; }
    }
    public string HomeTelephone
    {
        get { return strHomeTelephone; }
        set { strHomeTelephone = value; }
    }
    public string HomeTelephoneArb
    {
        get { return strHomeTelephoneArb; }
        set { strHomeTelephoneArb = value; }
    }
    public string PermanentTelephone
    {
        get { return strPermanentTelephone; }
        set { strPermanentTelephone = value; }
    }
    public string PermanentTelephoneArb
    {
        get { return strPermanentTelephoneArb; }
        set { strPermanentTelephoneArb = value; }
    }
    public string MobileNumber
    {
        get { return strMobileNumber; }
        set { strMobileNumber = value; }
    }
    public string MobileNumberArb
    {
        get { return strMobileNumberArb; }
        set { strMobileNumberArb = value; }
    }
    public string Fax
    {
        get { return strFax; }
        set { strFax = value; }
    }
    public string FaxArb
    {
        get { return strFaxArb; }
        set { strFaxArb = value; }
    }
    public string EmailID
    {
        get { return strEmailID; }
        set { strEmailID = value; }
    }
    public string PermanentPOBox
    {
        get { return strPermanentPOBox; }
        set { strPermanentPOBox = value; }
    }
    public string PermanentPOBoxArb
    {
        get { return strPermanentPOBoxArb; }
        set { strPermanentPOBoxArb = value; }
    }
    public string InsuranceNumber
    {
        get { return strInsuranceNumber; }
        set { strInsuranceNumber = value; }
    }
    public string InsuranceNumberArb
    {
        get { return strInsuranceNumberArb; }
        set { strInsuranceNumberArb = value; }
    }
    public string SignUpDate
    {
        get { return dtSignUpDate; }
        set { dtSignUpDate = value; }
    }
    public string SignUpDateArb
    {
        get { return dtSignUpDateArb; }
        set { dtSignUpDateArb = value; }
    }

    public string Skill
    {
        get;
        set;
    }
    public bool ReadingArb
    {
        get { return blnReadingArb; }
        set { blnReadingArb = value; }
    }
    public bool WritingArb
    {
        get { return blnWritingArb; }
        set { blnWritingArb = value; }
    }
    public bool ReadingEng
    {
        get { return blnReadingEng; }
        set { blnReadingEng = value; }
    }
    public bool WritingEng
    {
        get { return blnWritingEng; }
        set { blnWritingEng = value; }
    }
    public string OtherLanguages
    {
        get { return strOtherLanguages; }
        set { strOtherLanguages = value; }
    }
    public bool ReadingOtherLanguages
    {
        get { return blnReadingOtherLanguages; }
        set { blnReadingOtherLanguages = value; }
    }
    public bool WritingOtherLanguages
    {
        get { return blnWritingOtherLanguages; }
        set { blnWritingOtherLanguages = value; }
    }

    public int PageIndex
    {
        get { return intPageIndex; }
        set { intPageIndex = value; }
    }

    public int PageSize
    {
        get { return intPageSize; }
        set { intPageSize = value; }
    }

    public int Node
    {
        get { return intNode; }
        set { intPageSize = value; }
    }

    public string DocumentName
    {
        get { return strDocumentName; }
        set { strDocumentName = value; }
    }

    public string FileName
    {
        get { return strFileName; }
        set { strFileName = value; }
    }
 
    
    public DataSet LoadCombos()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 1));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRCandidate", parameters);
    }

    public DataSet LoadCombos(bool fromLogin)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 1));      
        return ExecuteDataSet("HRCandidate", parameters);
    }


    public long InsertCandidateBasicInfo(DataTable datTemp, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

        if (blnIsAddMode)
            parameters.Add(new SqlParameter("@Mode", 2));
        else
            parameters.Add(new SqlParameter("@Mode", 8));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@JobID", intJobID));
        if(ReferralType >0)
            parameters.Add(new SqlParameter("@ReferralTypeId", ReferralType ));
        parameters.Add(new SqlParameter("@RefferedBy", ReferredBy ));
        parameters.Add(new SqlParameter("@CandidateCode", strCandidateCode));
        parameters.Add(new SqlParameter("@SalutationID", intSalutationID));
        parameters.Add(new SqlParameter("@FirstNameEng", strFirstNameEng));
        parameters.Add(new SqlParameter("@SecondNameEng", strSecondNameEng));
        parameters.Add(new SqlParameter("@ThirdNameEng", strThirdNameEng));
        parameters.Add(new SqlParameter("@FirstNameArb", strFirstNameArb));
        parameters.Add(new SqlParameter("@SecondNameArb", strSecondNameArb));
        parameters.Add(new SqlParameter("@ThirdNameArb", strThirdNameArb));
        parameters.Add(new SqlParameter("@Citizenship", strCitizenship));
        parameters.Add(new SqlParameter("@CitizenshipArb", strCitizenshipArb));
        parameters.Add(new SqlParameter("@PlaceofBirth", strPlaceofBirth));
        parameters.Add(new SqlParameter("@EmailID", strEmailID));
        parameters.Add(new SqlParameter("@PlaceofBirthArb", strPlaceofBirthArb));

        if (dtDateofBirth != "" && dtDateofBirth != null)
            parameters.Add(new SqlParameter("@DateofBirth", Convert.ToDateTime(dtDateofBirth)));
        //if (dtDateofBirthArb != "" && dtDateofBirthArb != null)
        //    parameters.Add(new SqlParameter("@DateofBirthArb", Convert.ToDateTime(dtDateofBirthArb)));

        parameters.Add(new SqlParameter("@Gender", blnGender));
        parameters.Add(new SqlParameter("@MaritalStatusID", intMaritalStatusID));
        parameters.Add(new SqlParameter("@NoofSon", intNoofSon));
      
        parameters.Add(new SqlParameter("@MotherName", strMotherName));
        parameters.Add(new SqlParameter("@MotherNameArb", strMotherNameArb));
        if(intReligionID > 0)
            parameters.Add(new SqlParameter("@ReligionID", intReligionID));
        parameters.Add(new SqlParameter("@CurrentAddress", strCurrentAddress));
        parameters.Add(new SqlParameter("@CurrentAddressArb", strCurrentAddressArb));
        parameters.Add(new SqlParameter("@CurrentPOBox", strCurrentPOBox));
     
        if(intCountryID > 0)
            parameters.Add(new SqlParameter("@CountryID", intCountryID));
        parameters.Add(new SqlParameter("@CurrentTelephone", strCurrentTelephone));
        if(intCurrentNationalityID > 0)
            parameters.Add(new SqlParameter("@CurrentNationalityID", intCurrentNationalityID));
        if(intPreviousNationalityID > 0)
            parameters.Add(new SqlParameter("@PreviousNationalityID", intPreviousNationalityID));
        parameters.Add(new SqlParameter("@Doctrine", strDoctrine));
        parameters.Add(new SqlParameter("@DoctrineArb", strDoctrineArb));
        parameters.Add(new SqlParameter("@BasicBusinessPair", strBasicBusinessPair));
        parameters.Add(new SqlParameter("@BasicBusinessPairArb", strBasicBusinessPairArb));
        parameters.Add(new SqlParameter("@BasicBusinessPairAddress", strBasicBusinessPairAddress));
        parameters.Add(new SqlParameter("@BasicBusinessPairAddressArb", strBasicBusinessPairAddressArb));

        if(intBasicCompanyTypeID > 0)
            parameters.Add(new SqlParameter("@BasicCompanyTypeID", intBasicCompanyTypeID));
        parameters.Add(new SqlParameter("@IsSelf", blnIsSelf));
        parameters.Add(new SqlParameter("@CreatedBy", intCreatedBy));
        parameters.Add(new SqlParameter("@Password", strPassword));

       // long lngTempCandidateID = (ExecuteScalar(parameters)).ToInt64();

        long lngTempCandidateID = 0;
        string sCode = "";

        SqlDataReader obj = new DataLayer().ExecuteReader("HRCandidate", parameters);
        if (obj.Read())
        {
            lngTempCandidateID = obj[0].ToInt64();
            sCode = obj[1].ToString();
            obj.Close();

        }

        sCode = clsCommon.Encrypt(sCode, ConfigurationManager.AppSettings["_ENCRYPTION"]);

        if (lngTempCandidateID > 0)
        {
            InsertCandidateProfessionalInfo(lngTempCandidateID, blnIsAddMode,sCode);
            InsertCandidateDocumentInfo(lngTempCandidateID, blnIsAddMode);
            InsertCandidateOtherInfo(lngTempCandidateID, blnIsAddMode);
           
            InsertCandidateLanguages(lngTempCandidateID);

            return lngTempCandidateID;
        }
        return 0;
    }

    public long InsertCandidateProfessionalInfo(long lngCandidateID, bool blnIsAddMode,string sPassword)
    {
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", 53));
        parameters.Add(new SqlParameter("@Password", sPassword));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PreviousJob", strPreviousJob));
        parameters.Add(new SqlParameter("@PreviousJobArb", strPreviousJobArb));
        parameters.Add(new SqlParameter("@QualificationCategoryID", intQualificationCategoryID));
        parameters.Add(new SqlParameter("@DegreeID", intQualificationID));
        parameters.Add(new SqlParameter("@GraduationYear", intGraduationYear));
        //parameters.Add(new SqlParameter("@GraduationArb", strGraduationArb));
      
        //parameters.Add(new SqlParameter("@GraduationYearArb", intGraduationYearArb));
        parameters.Add(new SqlParameter("@PlaceOfGraduation", strPlaceOfGraduation));
        parameters.Add(new SqlParameter("@PlaceOfGraduationArb", strPlaceOfGraduationArb));
        parameters.Add(new SqlParameter("@CollegeSchool", strCollegeSchool));
        parameters.Add(new SqlParameter("@CollegeSchoolArb", strCollegeSchoolArb));
        parameters.Add(new SqlParameter("@TypeofExperience", strTypeofExperience));
        parameters.Add(new SqlParameter("@TypeofExperienceArb", strTypeofExperienceArb));
        parameters.Add(new SqlParameter("@Sector", strSector));
        parameters.Add(new SqlParameter("@SectorArb", strSectorArb));
        parameters.Add(new SqlParameter("@Position", strPosition));
        parameters.Add(new SqlParameter("@PositionArb", strPositionArb));
        parameters.Add(new SqlParameter("@ExperienceYear", intExperienceYear));
        //parameters.Add(new SqlParameter("@ExperienceYearArb", intExperienceYearArb));
        parameters.Add(new SqlParameter("@ExperienceMonth", intExperienceMonth));
        //parameters.Add(new SqlParameter("@ExperienceMonthArb", intExperienceMonthArb));
        parameters.Add(new SqlParameter("@Compensation", strCompensation));
        parameters.Add(new SqlParameter("@CompensationArb", strCompensationArb));
        parameters.Add(new SqlParameter("@Specialization", strSpecialization));
        parameters.Add(new SqlParameter("@SpecializationArb", strSpecializationArb));

        if (dtTransactionEntryDate != "" && dtTransactionEntryDate != null)
            parameters.Add(new SqlParameter("@TransactionEntryDate", Convert.ToDateTime(dtTransactionEntryDate)));
        //if (dtTransactionEntryDateArb != "" && dtTransactionEntryDateArb != null)
        //    parameters.Add(new SqlParameter("@TransactionEntryDateArb", Convert.ToDateTime(dtTransactionEntryDateArb)));

        if (dtExpectedJoinDate != "" && dtExpectedJoinDate != null)
            parameters.Add(new SqlParameter("@ExpectedJoinDate", Convert.ToDateTime(dtExpectedJoinDate)));
        //if (dtExpectedJoinDateArb != "" && dtExpectedJoinDateArb != null)
        //    parameters.Add(new SqlParameter("@ExpectedJoinDateArb", Convert.ToDateTime(dtExpectedJoinDateArb)));

        //parameters.Add(new SqlParameter("@ResumeAttachment", strResumeAttachment));
        parameters.Add(new SqlParameter("@Skills", Skill));
        return (ExecuteScalar(parameters)).ToInt64();
    }

    public long InsertCandidateDocumentInfo(long lngCandidateID, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", 54));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PassportNumber", strPassportNumber));
        parameters.Add(new SqlParameter("@PassportNumberArb", strPassportNumberArb));
        if(intPassportCountryID > 0)
            parameters.Add(new SqlParameter("@PassportCountryID", intPassportCountryID));
        parameters.Add(new SqlParameter("@PassportIssuePlace", strPassportIssuePlace));
        parameters.Add(new SqlParameter("@PassportIssuePlaceArb", strPassportIssuePlaceArb));

        if (dtPassportExpiryDate != "" && dtPassportExpiryDate != null)
            parameters.Add(new SqlParameter("@PassportExpiryDate", Convert.ToDateTime(dtPassportExpiryDate)));
        //if (dtPassportExpiryDateArb != "" && dtPassportExpiryDateArb != null)
        //    parameters.Add(new SqlParameter("@PassportExpiryDateArb", Convert.ToDateTime(dtPassportExpiryDateArb)));

        if (dtPassportReleaseDate != "" && dtPassportReleaseDate != null)
            parameters.Add(new SqlParameter("@PassportReleaseDate", Convert.ToDateTime(dtPassportReleaseDate)));
        //if (dtPassportReleaseDateArb != "" && dtPassportReleaseDateArb != null)
        //    parameters.Add(new SqlParameter("@PassportReleaseDateArb", Convert.ToDateTime(dtPassportReleaseDateArb)));

        parameters.Add(new SqlParameter("@VisaNumber", strVisaNumber));
        parameters.Add(new SqlParameter("@VisaNumberArb", strVisaNumberArb));
        parameters.Add(new SqlParameter("@UnifiedNumber", strUnifiedNumber));
        parameters.Add(new SqlParameter("@UnifiedNumberArb", strUnifiedNumberArb));
        if(intVisaCountryID > 0)
            parameters.Add(new SqlParameter("@VisaCountryID", intVisaCountryID));
        parameters.Add(new SqlParameter("@VisaIssuePlace", strVisaIssuePlace));
        parameters.Add(new SqlParameter("@VisaIssuePlaceArb", strVisaIssuePlaceArb));

        if (dtVisaExpiryDate != "" && dtVisaExpiryDate != null)
            parameters.Add(new SqlParameter("@VisaExpiryDate", Convert.ToDateTime(dtVisaExpiryDate)));
        //if (dtVisaExpiryDateArb != "" && dtVisaExpiryDateArb != null)
        //    parameters.Add(new SqlParameter("@VisaExpiryDateArb", Convert.ToDateTime(dtVisaExpiryDateArb)));

        if (dtVisaReleaseDate != "" && dtVisaReleaseDate != null)
            parameters.Add(new SqlParameter("@VisaReleaseDate", Convert.ToDateTime(dtVisaReleaseDate)));
        //if (dtVisaReleaseDateArb != "" && dtVisaReleaseDateArb != null)
        //    parameters.Add(new SqlParameter("@VisaReleaseDateArb", Convert.ToDateTime(dtVisaReleaseDateArb)));

        parameters.Add(new SqlParameter("@NationalCardNumber", strNationalCardNumber));
        parameters.Add(new SqlParameter("@NationalCardNumberArb", strNationalCardNumberArb));
        parameters.Add(new SqlParameter("@CardNumber", strCardNumber));
        parameters.Add(new SqlParameter("@CardNumberArb", strCardNumberArb));

        if (dtNationalCardExpiryDate != "" && dtNationalCardExpiryDate != null)
            parameters.Add(new SqlParameter("@NationalCardExpiryDate", Convert.ToDateTime(dtNationalCardExpiryDate)));
        //if (dtNationalCardExpiryDateArb != "" && dtNationalCardExpiryDateArb != null)
        //    parameters.Add(new SqlParameter("@NationalCardExpiryDateArb", Convert.ToDateTime(dtNationalCardExpiryDateArb)));

        parameters.Add(new SqlParameter("@RecentPhoto", strRecentPhoto));
        return (ExecuteScalar(parameters)).ToInt64();
    }

    public long InsertCandidateOtherInfo(long lngCandidateID, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", 55));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PermanentAddress", strPermanentAddress));
        parameters.Add(new SqlParameter("@PermanentAddressArb", strPermanentAddressArb));
        parameters.Add(new SqlParameter("@HomeStreet", strHomeStreet));
        parameters.Add(new SqlParameter("@HomeStreetArb", strHomeStreetArb));
        parameters.Add(new SqlParameter("@BusinessAddress", strBusinessAddress));
        parameters.Add(new SqlParameter("@BusinessAddressArb", strBusinessAddressArb));
        parameters.Add(new SqlParameter("@BusinessStreet", strBusinessStreet));
        parameters.Add(new SqlParameter("@BusinessStreetArb", strBusinessStreetArb));
        parameters.Add(new SqlParameter("@Area", strArea));
        parameters.Add(new SqlParameter("@AreaArb", strAreaArb));
        parameters.Add(new SqlParameter("@City", strCity));
        parameters.Add(new SqlParameter("@CityArb", strCityArb));
        parameters.Add(new SqlParameter("@HomeTelephone", strHomeTelephone));
        //parameters.Add(new SqlParameter("@HomeTelephoneArb", strHomeTelephoneArb));
        parameters.Add(new SqlParameter("@PermanentTelephone", strPermanentTelephone));
        //parameters.Add(new SqlParameter("@PermanentTelephoneArb", strPermanentTelephoneArb));
        parameters.Add(new SqlParameter("@MobileNumber", strMobileNumber));
        //parameters.Add(new SqlParameter("@MobileNumberArb", strMobileNumberArb));
        parameters.Add(new SqlParameter("@Fax", strFax));
        //parameters.Add(new SqlParameter("@FaxArb", strFaxArb));
        parameters.Add(new SqlParameter("@EmailID", strEmailID));
        parameters.Add(new SqlParameter("@PermanentPOBox", strPermanentPOBox));
        parameters.Add(new SqlParameter("@PermanentPOBoxArb", strPermanentPOBoxArb));
        parameters.Add(new SqlParameter("@InsuranceNumber", strInsuranceNumber));
        parameters.Add(new SqlParameter("@InsuranceNumberArb", strInsuranceNumberArb));

        if (dtSignUpDate != "" && dtSignUpDate != null)
            parameters.Add(new SqlParameter("@SignUpDate", Convert.ToDateTime(dtSignUpDate)));
        //if (dtSignUpDateArb != "" && dtSignUpDateArb != null)
        //    parameters.Add(new SqlParameter("@SignUpDateArb", Convert.ToDateTime(dtSignUpDateArb)));

        return (ExecuteScalar(parameters)).ToInt64();
    }


    public long InsertCandidateBasicInfoEng(DataTable datTemp, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

        if (blnIsAddMode)
            parameters.Add(new SqlParameter("@Mode", 2));
        else
            parameters.Add(new SqlParameter("@Mode", 41));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@JobID", intJobID));
        if(ReferralType > 0)
            parameters.Add(new SqlParameter("@ReferralTypeId", ReferralType));
        if(ReferredBy > 0)
            parameters.Add(new SqlParameter("@RefferedBy", ReferredBy));

        
        parameters.Add(new SqlParameter("@SalutationID", intSalutationID));
        parameters.Add(new SqlParameter("@FirstNameEng", strFirstNameEng));
        parameters.Add(new SqlParameter("@SecondNameEng", strSecondNameEng));
        parameters.Add(new SqlParameter("@ThirdNameEng", strThirdNameEng));
        parameters.Add(new SqlParameter("@FirstNameArb", strFirstNameArb));
        parameters.Add(new SqlParameter("@SecondNameArb", strSecondNameArb));
        parameters.Add(new SqlParameter("@ThirdNameArb", strThirdNameArb));
        parameters.Add(new SqlParameter("@Citizenship", strCitizenship));
        parameters.Add(new SqlParameter("@PlaceofBirth", strPlaceofBirth));
        parameters.Add(new SqlParameter("@EmailID", strEmailID));

        if (dtDateofBirth != "" && dtDateofBirth != null)
            parameters.Add(new SqlParameter("@DateofBirth", Convert.ToDateTime(dtDateofBirth)));

        parameters.Add(new SqlParameter("@Gender", blnGender));
        parameters.Add(new SqlParameter("@MaritalStatusID", intMaritalStatusID));
        parameters.Add(new SqlParameter("@NoofSon", intNoofSon));
        parameters.Add(new SqlParameter("@MotherName", strMotherName));
        if(intReligionID > 0)
         parameters.Add(new SqlParameter("@ReligionID", intReligionID));
        parameters.Add(new SqlParameter("@CurrentAddress", strCurrentAddress));
        parameters.Add(new SqlParameter("@CurrentPOBox", strCurrentPOBox));
        if(intCountryID > 0)
            parameters.Add(new SqlParameter("@CountryID", intCountryID));
        parameters.Add(new SqlParameter("@CurrentTelephone", strCurrentTelephone));
        if(intCurrentNationalityID > 0)
             parameters.Add(new SqlParameter("@CurrentNationalityID", intCurrentNationalityID));
        if(intPreviousNationalityID > 0)
            parameters.Add(new SqlParameter("@PreviousNationalityID", intPreviousNationalityID));
        parameters.Add(new SqlParameter("@Doctrine", strDoctrine));
        parameters.Add(new SqlParameter("@BasicBusinessPair", strBasicBusinessPair));
        parameters.Add(new SqlParameter("@BasicBusinessPairAddress", strBasicBusinessPairAddress));
        if(intBasicCompanyTypeID > 0)
            parameters.Add(new SqlParameter("@BasicCompanyTypeID", intBasicCompanyTypeID));
        parameters.Add(new SqlParameter("@IsSelf", blnIsSelf));
        parameters.Add(new SqlParameter("@CreatedBy", intCreatedBy));
        parameters.Add(new SqlParameter("@Password", strPassword));


        long lngTempCandidateID = 0;
        string sCode = "";
    
        SqlDataReader obj = new DataLayer().ExecuteReader("HRCandidate", parameters);
        if (obj.Read())
        {
            lngTempCandidateID = obj[0].ToInt64();
            sCode = obj[1].ToString();
            obj.Close();
            
        }

        sCode = clsCommon.Encrypt(sCode, ConfigurationManager.AppSettings["_ENCRYPTION"]);
     //   DataTable dt = (DataTable)obj;


       // long lngTempCandidateID = (ExecuteScalar(parameters)).ToInt64();

        if (lngTempCandidateID > 0)
        {
            InsertCandidateProfessionalInfoEng(lngTempCandidateID, blnIsAddMode,sCode);
            InsertCandidateDocumentInfoEng(lngTempCandidateID, blnIsAddMode);
            InsertCandidateOtherInfoEng(lngTempCandidateID, blnIsAddMode);
          
            InsertCandidateLanguages(lngTempCandidateID);
            
            return lngTempCandidateID;
        }
       
        return 0;
    }

    public long InsertCandidateProfessionalInfoEng(long lngCandidateID, bool blnIsAddMode,string sPassword)
    {
        ArrayList parameters = new ArrayList();

       
        parameters.Add(new SqlParameter("@Mode", 42));
        parameters.Add(new SqlParameter("@Password", sPassword));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PreviousJob", strPreviousJob));
        parameters.Add(new SqlParameter("@QualificationCategoryID", intQualificationCategoryID));
        parameters.Add(new SqlParameter("@DegreeID", intQualificationID));
        parameters.Add(new SqlParameter("@GraduationYear", intGraduationYear));
        parameters.Add(new SqlParameter("@PlaceOfGraduation", strPlaceOfGraduation));
        parameters.Add(new SqlParameter("@CollegeSchool", strCollegeSchool));
        parameters.Add(new SqlParameter("@TypeofExperience", strTypeofExperience));
        parameters.Add(new SqlParameter("@Sector", strSector));
        parameters.Add(new SqlParameter("@Position", strPosition));
        parameters.Add(new SqlParameter("@ExperienceYear", intExperienceYear));
        parameters.Add(new SqlParameter("@ExperienceMonth", intExperienceMonth));
        parameters.Add(new SqlParameter("@Compensation", strCompensation));
        parameters.Add(new SqlParameter("@Specialization", strSpecialization));
        parameters.Add(new SqlParameter("@Skills", Skill));
       
        if (dtTransactionEntryDate != "" && dtTransactionEntryDate != null)
            parameters.Add(new SqlParameter("@TransactionEntryDate", Convert.ToDateTime(dtTransactionEntryDate)));

        if (dtExpectedJoinDate != "" && dtExpectedJoinDate != null)
            parameters.Add(new SqlParameter("@ExpectedJoinDate", Convert.ToDateTime(dtExpectedJoinDate)));

      //parameters.Add(new SqlParameter("@ResumeAttachment", strResumeAttachment));
        return (ExecuteScalar(parameters)).ToInt64();
    }

    public long InsertCandidateDocumentInfoEng(long lngCandidateID, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

      
        parameters.Add(new SqlParameter("@Mode", 43));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PassportNumber", strPassportNumber));
        if(intPassportCountryID > 0)
            parameters.Add(new SqlParameter("@PassportCountryID", intPassportCountryID));
        parameters.Add(new SqlParameter("@PassportIssuePlace", strPassportIssuePlace));

        if (dtPassportExpiryDate != "" && dtPassportExpiryDate != null)
            parameters.Add(new SqlParameter("@PassportExpiryDate", Convert.ToDateTime(dtPassportExpiryDate)));

        if (dtPassportReleaseDate != "" && dtPassportReleaseDate != null)
            parameters.Add(new SqlParameter("@PassportReleaseDate", Convert.ToDateTime(dtPassportReleaseDate)));

        parameters.Add(new SqlParameter("@VisaNumber", strVisaNumber));
        parameters.Add(new SqlParameter("@UnifiedNumber", strUnifiedNumber));
        if(intVisaCountryID > 0)
            parameters.Add(new SqlParameter("@VisaCountryID", intVisaCountryID));
        parameters.Add(new SqlParameter("@VisaIssuePlace", strVisaIssuePlace));

        if (dtVisaExpiryDate != "" && dtVisaExpiryDate != null)
            parameters.Add(new SqlParameter("@VisaExpiryDate", Convert.ToDateTime(dtVisaExpiryDate)));

        if (dtVisaReleaseDate != "" && dtVisaReleaseDate != null)
            parameters.Add(new SqlParameter("@VisaReleaseDate", Convert.ToDateTime(dtVisaReleaseDate)));

        parameters.Add(new SqlParameter("@NationalCardNumber", strNationalCardNumber));;
        parameters.Add(new SqlParameter("@CardNumber", strCardNumber));

        if (dtNationalCardExpiryDate != "" && dtNationalCardExpiryDate != null)
            parameters.Add(new SqlParameter("@NationalCardExpiryDate", Convert.ToDateTime(dtNationalCardExpiryDate)));
        parameters.Add(new SqlParameter("@ResumeAttachment", strResumeAttachment));
        parameters.Add(new SqlParameter("@RecentPhoto", strRecentPhoto));
        return (ExecuteScalar(parameters)).ToInt64();
    }

    public long InsertCandidateOtherInfoEng(long lngCandidateID, bool blnIsAddMode)
    {
        ArrayList parameters = new ArrayList();

      
        parameters.Add(new SqlParameter("@Mode", 44));

        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PermanentAddress", strPermanentAddress));
        parameters.Add(new SqlParameter("@HomeStreet", strHomeStreet));
        parameters.Add(new SqlParameter("@BusinessAddress", strBusinessAddress));
        parameters.Add(new SqlParameter("@BusinessStreet", strBusinessStreet));
        parameters.Add(new SqlParameter("@Area", strArea));
        parameters.Add(new SqlParameter("@City", strCity));
        parameters.Add(new SqlParameter("@HomeTelephone", strHomeTelephone));
        parameters.Add(new SqlParameter("@PermanentTelephone", strPermanentTelephone));
        parameters.Add(new SqlParameter("@MobileNumber", strMobileNumber));
        parameters.Add(new SqlParameter("@Fax", strFax));
       
        parameters.Add(new SqlParameter("@PermanentPOBox", strPermanentPOBox));
        parameters.Add(new SqlParameter("@InsuranceNumber", strInsuranceNumber));

        if (dtSignUpDate != "" && dtSignUpDate != null)
            parameters.Add(new SqlParameter("@SignUpDate", Convert.ToDateTime(dtSignUpDate)));
        return (ExecuteScalar(parameters)).ToInt64();
    }


   

    public long InsertCandidateLanguages(long lngCandidateID)
    {        
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 7));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@ReadingArb", blnReadingArb));
        parameters.Add(new SqlParameter("@WritingArb", blnWritingArb));
        parameters.Add(new SqlParameter("@ReadingEng", blnReadingEng));
        parameters.Add(new SqlParameter("@WritingEng", blnWritingEng));
        parameters.Add(new SqlParameter("@Other", strOtherLanguages));
        parameters.Add(new SqlParameter("@ReadingOther", blnReadingOtherLanguages));
        parameters.Add(new SqlParameter("@WritingOther", blnWritingOtherLanguages));
        return (ExecuteScalar(parameters)).ToInt64();
    }

    public void DeleteCandidate()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 13));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        ExecuteNonQuery("HRCandidate", parameters);
    }

    public void DeleteCandidateSkills()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 14));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        ExecuteNonQuery("HRCandidate", parameters);
    }

    public int GetRecordCount()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 15));
        parameters.Add(new SqlParameter("@SearchKey",SearchKey ));
        return Convert.ToInt32(ExecuteScalar("HRCandidate", parameters));
    }

    public DataSet FillDataList()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 16));
        parameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        parameters.Add(new SqlParameter("@PageSize", intPageSize));
        parameters.Add(new SqlParameter("@SearchKey", SearchKey));
        parameters.Add(new SqlParameter("@SortExpression", SortExpression ));
        parameters.Add(new SqlParameter("@SortOrder", SortOrder ));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRCandidate", parameters);
    }

    public DataSet getCandidateDetails()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 17));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteDataSet("HRCandidate", parameters);
    }

    public DataSet getCandidateViewDetails()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 19));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteDataSet("HRCandidate", parameters);
    }

    public DataTable GetCandidateDocuments()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 20));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        return ExecuteDataTable("HRCandidate", parameters);
    }

    public void DeleteCandidateDocument()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 21));
        parameters.Add(new SqlParameter("@Node", intNode));
        ExecuteNonQuery("HRCandidate", parameters);
    }

    public int InsertCandidateDocument() //Insert Petty Cash Document
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 22));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@CandidateCode", strCandidateCode));
        parameters.Add(new SqlParameter("@DocumentName", strDocumentName));
        parameters.Add(new SqlParameter("@FileName", strFileName));
        return Convert.ToInt32(ExecuteScalar("HRCandidate", parameters));
    }

    public bool CheckExistsCandidatePassport()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 23));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PassportNumber", strPassportNumber));
        return Convert.ToBoolean(ExecuteScalar("HRCandidate", parameters));
    }

    public bool CheckExistsCandidateCode()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 24));
        parameters.Add(new SqlParameter("@CandidateID", lngCandidateID));
        parameters.Add(new SqlParameter("@PassportNumber", strPassportNumber));
        return Convert.ToBoolean(ExecuteScalar("HRCandidate", parameters));
    }

    public void SaveCandidateDocuments(string FileName,string DocumentName,Int64 CandidateID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 26));
        parameters.Add(new SqlParameter("@FileName", FileName));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@DocumentName", DocumentName));
        ExecuteScalar("HRCandidate", parameters);
    }

    public static DataTable  GetCandidateAttachedDocuments(Int64 CandidateId)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 27));
        parameters.Add(new SqlParameter("@CandidateID", CandidateId));
        return new DataLayer().ExecuteDataTable("HRCandidate", parameters);
    }

    public static DataSet  GetCandidateDetails(Int64 CandidateId)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 29));
        parameters.Add(new SqlParameter("@CandidateID", CandidateId));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet("HRCandidate", parameters);
    }

    public  void DeleteCandidateAttachedDocuments(Int64 CandidateID,string FileName)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 28));
       
        if(FileName != string.Empty)
            parameters.Add(new SqlParameter("@FileName", FileName));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        ExecuteScalar("HRCandidate", parameters);
    }

    public int  GetResultStatus()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 31));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteScalar("HRCandidate", parameters).ToInt32();
    }
    public int GetOfferStatudID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 32));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return ExecuteScalar("HRCandidate", parameters).ToInt32();
    }
    public DataTable  GetOfferRemarks()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 58));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRCandidate", parameters);
    }
    public bool CheckCandidateManadatoryFields(long CandidateID)
    {
        bool isExists = false;
        SqlDataReader dr = ExecuteReader("SELECT [dbo].fnCheckCandidateFields('" + CandidateID + "' )");

        if (dr.Read() == true)
            isExists = Convert.ToBoolean(dr[0]);

        dr.Close();
        return isExists;
    }

    #region UpdateOfferStatus
    /// <summary>
    /// Accept/Reject Offerletter
    /// </summary>
    public long  UpdateOfferStatus()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 30));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        parameters.Add(new SqlParameter("@CandidateStatusId", OfferStatusID));
        parameters.Add(new SqlParameter("@Remarks", Remarks));
        return ExecuteScalar("HRCandidate", parameters).ToInt64();
    }
    #endregion UpdateOfferStatus



    public bool DeleteCandidates()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 50));
        parameters.Add(new SqlParameter("@CandidateID", CandidateID));
        try
        {
            ExecuteScalar("HRCandidate", parameters);
            return true;
        }
        catch
        {
            return false;
        }
      
    }
    public Table  PrintCandidates()
    {
        Table tblCandidate = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblCandidate.ID = "PrintCandidate";
       
        //td.Text = CreateSelectedCandidateContent();
     
        tr.Cells.Add(td);
        tblCandidate.Rows.Add(tr);

        return tblCandidate;
    }

    public bool CheckEmployeeExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "56"));

        alparameters.Add(new SqlParameter("@CandidateID", CandidateID));

        return Convert.ToBoolean(ExecuteScalar("HRCandidate", alparameters));
    }

    public bool CheckCandidateExists()
    {
        ArrayList alparameters = new ArrayList();       
        alparameters.Add(new SqlParameter("@Mode", "57"));
        if (CandidateID > 0)
            alparameters.Add(new SqlParameter("@CandidateId", CandidateID));
      

        alparameters.Add(new SqlParameter("@EmailID", EmailID));

        return Convert.ToBoolean(ExecuteScalar("HRCandidate", alparameters));
    }
   

    public DataSet GetAdvanceSearchDetails()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 52));
        parameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        parameters.Add(new SqlParameter("@PageSize", intPageSize));
        parameters.Add(new SqlParameter("@MinimumExperience", MinExp));
        parameters.Add(new SqlParameter("@QualificationCategoryID", QualificationCategoryID));
        parameters.Add(new SqlParameter("@GraduationYear", GraduationYear));
        parameters.Add(new SqlParameter("@MaximumExperience", MaxExp));
        parameters.Add(new SqlParameter("@DegreeID", intQualificationID));
        parameters.Add(new SqlParameter("@Skills", Skill));
        parameters.Add(new SqlParameter("@CountryID", CountryID));
        parameters.Add(new SqlParameter("@ReferralTypeId", ReferralType));
        parameters.Add(new SqlParameter("@RefferedBy", ReferredBy));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRCandidate", parameters);


    }

    public DataTable CreateSelectedCandidateContent()
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", 51));
        alparameters.Add(new SqlParameter("@CandidateIds", CandidatesID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRCandidate", alparameters);
       
    }
    public void Begin()
    {
        BeginTransaction("HRCandidate");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
         RollbackTransaction();
    }
}
