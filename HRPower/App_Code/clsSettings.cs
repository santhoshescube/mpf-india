﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using System.IO;

/// <summary>
/// Summary description for clsSettings
/// </summary>
public class clsSettings
{
	public clsSettings()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public bool CheckActivation()
    {
        string strRegPassKey = GetRegistrykeyNo();
        if (strRegPassKey == "")
            return false;
        else
            return true;

    }

    private string GetRegistrykeyNo()
    {
        string strRegistryNo = "";
        new RegClass().ReadFromRegistry("SOFTWARE\\Microsoft\\Adbphrstance", "Adbphrstanceno", out strRegistryNo);
        return strRegistryNo;
    }

      
    private string GetMachineIP()
    {
        try
        {
            return System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[0].ToString().Trim();
        }
        catch (Exception)
        {
            return "";
        }
    }

   
    private string GetIP()
    {
        string ipAddress = "";
        try
        {
            if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
            {
                IPAddress[] ipAddr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                for (int i = 0; i < ipAddr.Length; i++)
                {
                    if (ipAddr[i].AddressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        if (!IPAddress.IsLoopback(ipAddr[i]))
                        {
                            ipAddress = ipAddr[i].ToString();
                            break;
                        }
                    }
                }
            }

            return ipAddress;
        }
        catch (Exception)
        {
            return ipAddress;
        }
    }

    private string GetExtIP()
    {
        string extIP = "";
        try
        {
            extIP = new WebClient().DownloadString("http://wanip.info/");
            extIP = new WebClient().DownloadString("http://checkip.dyndns.org/");
            extIP = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}").Matches(extIP)[0].ToString();
            if (extIP.Length > 30)
                extIP = extIP.Substring(0, 30);

            return extIP;
        }
        catch (Exception)
        {
            return extIP;
        }
    }

    public static string EncryptCrypto(string strText)
    {
        string strEncrKey = "Belives-in-God";
        byte[] byKey = null;
        byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            return Convert.ToBase64String(ms.ToArray());

        }
        catch (Exception)
        {
            return strText;
        }
    }

    public static string DecryptCrypto(string strText)
    {
        string sDecrKey = "Belives-in-God";
        byte[] byKey = null;
        byte[] IV = { 0X12, 0X34, 0X56, 0X78, 0X90, 0XAB, 0XCD, 0XEF };
        byte[] inputByteArray = new byte[strText.Length + 1];

        try
        {
            byKey = System.Text.Encoding.UTF8.GetBytes(sDecrKey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByteArray = Convert.FromBase64String(strText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;

            return encoding.GetString(ms.ToArray());

        }
        catch (Exception)
        {
            return strText;
        }
    }

}
