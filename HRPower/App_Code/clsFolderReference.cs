﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsFolderReference
/// purpose : Handle Folders
/// created : Binoj
/// Date    : 02.11.2010
/// </summary>

public class clsFolderReference:DL
{
    int iFolderId, iParentId;
    string sFolder;

    public int FolderId
    {
        get { return iFolderId; }
        set { iFolderId = value; }
    }

    public int ParentId
    {
        get { return iParentId; }
        set { iParentId = value; }
    }

    public string Folder
    {
        get { return sFolder; }
        set { sFolder = value; }
    }

    public clsFolderReference() { }

    public int Insert()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@ParentId", iParentId));
        alParameters.Add(new SqlParameter("@Folder", sFolder));

        return Convert.ToInt32(ExecuteScalar("HRspFolderReference", alParameters));
    }

    public int Update()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "U"));
        alParameters.Add(new SqlParameter("@FolderId", iFolderId));
        alParameters.Add(new SqlParameter("@Folder", sFolder));

        return Convert.ToInt32(ExecuteScalar("HRspFolderReference", alParameters));
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "D"));
        alParameters.Add(new SqlParameter("@FolderId", iFolderId));

        ExecuteNonQuery("HRspFolderReference", alParameters);
    }

    public DataSet GetAllFolders()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GA"));
        alParameters.Add(new SqlParameter("@ParentId", iParentId));

         return ExecuteDataSet("HRspFolderReference", alParameters);
    }
}
