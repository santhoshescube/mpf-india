﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsTrainingFeedback
/// </summary>
public class clsTrainingFeedback:DL
{
	public clsTrainingFeedback()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iFeedBackId;
    private int iEmployeeId;
    private int iTrainingId;
    private bool bIsUseful;
    private string sLeastSubject;
    private bool iIsLearnable;
    private bool bIsInline;
    private string sComments;
    private string sChanges;
    private string sMostSubject;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    

    public int FeedBackId
    {
        get { return iFeedBackId; }
        set { iFeedBackId = value; }
    }
    public int EmployeeId
    {
        get { return iEmployeeId; }
        set { iEmployeeId = value; }
    }
    public int TrainingId
    {
        get { return iTrainingId; }
        set { iTrainingId = value; }
    }
    public bool IsUseful
    {
        get { return bIsUseful; }
        set { bIsUseful = value; }
    }
    public string LeastSubject
    {
        get { return sLeastSubject; }
        set { sLeastSubject = value; }
    }
    public bool IsLearnable
    {
        get { return iIsLearnable; }
        set { iIsLearnable = value; }
    }
    public bool IsInline
    {
        get { return bIsInline; }
        set { bIsInline = value; }
    }
    public string Comments
    {
        get { return sComments; }
        set { sComments = value; }
    }
    public string Changes
    {
        get { return sChanges; }
        set { sChanges = value; }
    }
    public string MostSubject
    {
        get { return sMostSubject; }
        set { sMostSubject = value; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public int InsertTrainingFeedbackDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));
        alparameters.Add(new SqlParameter("@IsUseful", bIsUseful));
        alparameters.Add(new SqlParameter("@LeastSubject", sLeastSubject));
        alparameters.Add(new SqlParameter("@IsLearnable", iIsLearnable));
        alparameters.Add(new SqlParameter("@IsInline", bIsInline));
        alparameters.Add(new SqlParameter("@Comments", sComments));
        alparameters.Add(new SqlParameter("@Changes", sChanges));
        alparameters.Add(new SqlParameter("@MostSubject", sMostSubject));

        return Convert.ToInt32(ExecuteScalar("HRspTrainingFeedback", alparameters));
    }
    public DataSet BindForm()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@FeedBackId", iFeedBackId));

        return ExecuteDataSet("HRspTrainingFeedback", alparameters);
    }

    public DataSet BindFeedback()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alparameters.Add(new SqlParameter("@PageSize", iPageSize));
        alparameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspTrainingFeedback", alparameters);
    }

    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return Convert.ToInt32(ExecuteScalar("HRspTrainingFeedback", alparameters));
    }

    public void DeleteTrainingFeedBacks()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@TrainingId", iTrainingId));

         ExecuteNonQuery("HRspTrainingFeedback", alparameters);
    }

    public DataSet FillTrainingTitle()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "FTR"));

        return ExecuteDataSet("HRspTrainingFeedback", alparameters);
    }
    public DataTable GetFeedbackDetails(int iFeedBackId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DETS"));
        alParameters.Add(new SqlParameter("@FeedBackId", iFeedBackId));

        return ExecuteDataTable("HRspTrainingFeedback", alParameters);
    }
}
