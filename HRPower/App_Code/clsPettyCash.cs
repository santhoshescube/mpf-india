﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
/*
 * Created By   : Tijo
 * Created Date : 01 Aug 2013
 */
public class clsPettyCash : DL
{
    int intEmployeeID;
    public int EmployeeID { get { return intEmployeeID; } set { intEmployeeID = value; } }

    public long lngPettyCashID { get; set; }
    public string strPettyCashNo { get; set; }
    public bool blnIsDate { get; set; }
    public DateTime dtDate { get; set; }
    public decimal decAmount { get; set; }
    public decimal decActualAmount { get; set; }
    public string strEmployeeRemarks { get; set; }
    public string strDescription { get; set; }
    public int intNode { get; set; }
    public string strDocumentName { get; set; }
    public string strFileName { get; set; }
    public int intPageIndex { get; set; }
    public int intPageSize { get; set; }
    public int intCreatedBy { get; set; }

	public clsPettyCash()
	{
        
	}

    public void BeginEmp()
    {
        BeginTransaction("spPayPettyCash");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }
        
    public long SavePettyCash()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 4)); // Update
        parameters.Add(new SqlParameter("@PettyCashID", lngPettyCashID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@Date", dtDate));
        parameters.Add(new SqlParameter("@Amount", decAmount));
        parameters.Add(new SqlParameter("@ActualAmount", decActualAmount));
        parameters.Add(new SqlParameter("@EmployeeRemarks", strEmployeeRemarks));
        parameters.Add(new SqlParameter("@Description", strDescription));
        SqlParameter objParam = new SqlParameter("ReturnVal", SqlDbType.BigInt);
        objParam.Direction = ParameterDirection.ReturnValue;
        parameters.Add(objParam);

        object objPettyCashID = null;

        if (ExecuteNonQuery("spPayPettyCash", parameters, out objPettyCashID) != 0)
        {
            if (objPettyCashID.ToInt64() > 0)
                return objPettyCashID.ToInt64();
        }
        return 0;
    }

    public DataTable DisplayPettyCashDetails()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 7));
        Parameters.Add(new SqlParameter("@PettyCashID", lngPettyCashID));
        return ExecuteDataTable("spPayPettyCash", Parameters);
    }

    public DataTable GetPettyCashDocuments()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 8));
        Parameters.Add(new SqlParameter("@PettyCashID", lngPettyCashID));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteDataTable("spPayPettyCash", Parameters);
    }

    public void DeletePettyCashDocument()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 9));
        Parameters.Add(new SqlParameter("@Node", intNode));
        ExecuteNonQuery("spPayPettyCash", Parameters);
    }

    public int InsertPettyCashDocument() //Insert Petty Cash Document
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 10));
        Parameters.Add(new SqlParameter("@PettyCashID", lngPettyCashID));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        Parameters.Add(new SqlParameter("@PettyCashNo", strPettyCashNo));
        Parameters.Add(new SqlParameter("@DocumentName", strDocumentName));
        Parameters.Add(new SqlParameter("@FileName", strFileName));
        return Convert.ToInt32(ExecuteScalar("spPayPettyCash", Parameters));
    }

    public DataTable GetPagewisePettyCash()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 11));
        Parameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        Parameters.Add(new SqlParameter("@PageSize", intPageSize));
        if (blnIsDate == true)
            Parameters.Add(new SqlParameter("@Date", dtDate));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));        
        return ExecuteDataTable("spPayPettyCash", Parameters);
    }

    public int GetPageCountPettyCash()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 12));
        if (blnIsDate == true)
            Parameters.Add(new SqlParameter("@Date", dtDate));
        Parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteScalar("spPayPettyCash", Parameters).ToInt32();
    }
}