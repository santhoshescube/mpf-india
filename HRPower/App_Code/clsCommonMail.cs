﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Threading;

/// <summary> 
/// Created By   :  Sruthy K
/// Created Date :  04-Oct-2013
/// Description  :  Common Mail For All Requests
/// </summary>


public class clsCommonMail
{
    #region Properties

    private StringBuilder MessageEmployee { get; set; } // Message For Requested By

    private StringBuilder MessageAuthorities { get; set; } // Message For Authorities

    private eActionBy ActionBy { get; set; } // Specify action performed person

    public DataTable dtRequest { get; set; } // Get Request Details

    private int ReportingTo { get; set; }// Set Reporting To ID
    private int AlternateAuthority { get; set; }// Set Alternate Authority ID 
    private int HigherAuthority { get; set; }// Set Superior Authority ID 

    private int RequestTypeID { get; set; }//Set RequestTypeID
    private string EmployeeName { get; set; }//Set EmployeeName
    private string CurrentDate { get; set; }//Set Current Date

    private string EmployeeEmailID { get; set; }//Set Employee Mail ID
    private string AuthorityEmailID { get; set; }//Set Authority Mail ID
    private string FromEmailID { get; set; }//Set EmailID of Login Person
    private string ReportingToEmailID { get; set; }//Set Reporting To EmailID

    private string Subject { get; set; }//Set Subject

    private StringBuilder MessagePerformance { get; set; }

    #endregion

    #region Constructor
    public clsCommonMail()
    {

    }
    #endregion

    /// <summary>
    /// Send Mail
    /// </summary>
    /// <param name="RequestID">RequestID</param>
    /// <param name="RequestType">RequestType</param>
    /// <param name="Action">Process Performed</param>
    public static void SendMail(int RequestID, MailRequestType RequestType, eAction Action)
    {
       // new clsCommonMail().ProcessMail(RequestID, RequestType, Action);
    }

    /// <summary>
    /// Process Mail 
    /// </summary>
    /// <param name="RequestID">RequestID</param>
    /// <param name="MRequestType">RequestType</param>
    /// <param name="Action">Process Performed</param>
    private void ProcessMail(int RequestID, MailRequestType MRequestType, eAction Action)
    {

        if (MRequestType == MailRequestType.PerformanceAction || MRequestType == MailRequestType.PerformanceInitiation)
        {
            ProcessPerformanceMail(RequestID, MRequestType, Action);
        }
        else
        {
            //Get Login EmployeeID
            int ActionByID = new clsUserMaster().GetEmployeeId();

            //Array for Messages
            MessageEmployee = new StringBuilder();
            MessageAuthorities = new StringBuilder();

            //Get Request Details
            dtRequest = GetRequestDetails(RequestID, MRequestType);

            //Get Login Employee Name
            GetEmployeeName();
            //Get Current Date
            CurrentDate = new clsCommon().GetSysDate();
            //Get Request Type
            GetRequestType(MRequestType);
            //Get ReportingToIDs
            GetReportingIds(dtRequest.Rows[0]["EmployeeID"].ToInt32());


            if (Action == eAction.Applied)
                Subject = MRequestType + " Request";
            else
                Subject = "Re : " + MRequestType + " Request";

            //Set ActionBy ID
            if (ActionByID == ReportingTo)
                ActionBy = eActionBy.ReportingTo;
            else if (ActionByID == AlternateAuthority)
                ActionBy = eActionBy.AlternateAuthority;
            else if (ActionByID == HigherAuthority)
                ActionBy = eActionBy.HigherAuthority;
            else
                ActionBy = eActionBy.Employee;

            switch (Action)
            {
                //Applied
                case eAction.Applied:
                    AuthoritiesEmailBody(dtRequest, MRequestType, "Applied");
                    break;

                //RequestForCancel
                case eAction.RequestForCancel:
                    AuthoritiesEmailBody(dtRequest, MRequestType, "RequestForCancel");

                    break;
                //Approved
                case eAction.Approved:
                    switch (ActionBy)
                    {
                        case eActionBy.ReportingTo:
                            AuthoritiesEmailBody(dtRequest, MRequestType, "Forwarded");
                            break;

                        case eActionBy.AlternateAuthority:
                        case eActionBy.HigherAuthority:
                            AuthoritiesEmailBody(dtRequest, MRequestType, "Approved");
                            EmployeeEmailBody(dtRequest, MRequestType, "Approved");
                            break;
                    }
                    break;

                // Rejected
                case eAction.Reject:
                    AuthoritiesEmailBody(dtRequest, MRequestType, "Rejected");
                    EmployeeEmailBody(dtRequest, MRequestType, "Rejected");
                    break;

                // Cancelled
                case eAction.Cancelled:
                    AuthoritiesEmailBody(dtRequest, MRequestType, "Cancelled");
                    EmployeeEmailBody(dtRequest, MRequestType, "Cancelled");
                    break;
            }
            //Send Mail
            SendMail(Convert.ToString(MessageAuthorities), Convert.ToString(MessageEmployee), Action);
        }
    }

    private void ProcessPerformanceMail(int RequestID, MailRequestType MRequestType, eAction Action)
    {
        DataTable dtPerformance = GetPerformanceDetails(RequestID,MRequestType);
        MessagePerformance = new StringBuilder();
        if (dtPerformance.Rows.Count > 0)
        {
            if (MRequestType == MailRequestType.PerformanceInitiation)
            {
                Subject = "Performance Evaluation";
                MessagePerformance.Append("<table width='100%' border='0' cellpadding='3' style='font-family: Tahoma; font-size:12px; vertical-align: top'><tr><td colspan = '2'> Performance Evaluation Between " + Convert.ToString(dtPerformance.Rows[0]["FromDate"]) + " and " + Convert.ToString(dtPerformance.Rows[0]["ToDate"]) + " is waiting for action </td></tr></table>");
            }
            else
            {
                Subject = "Performance Evaluation Result";
                //MessagePerformance.Append("<table width='100%' border='0' cellpadding='3' style='font-family: Tahoma; font-size:12px; vertical-align: top'><tr><td colspan='2'> Your Performance Evaluation Between " + Convert.ToString(dtPerformance.Rows[0]["FromDate"]) + " and " + Convert.ToString(dtPerformance.Rows[0]["ToDate"]) + " has been evaluated </td></tr>" +
                //                          "<tr><td style='width:25%'>Job Promotion : </td><td style='width:70%'>" + Convert.ToString(dtPerformance.Rows[0]["IsJobPromotion"]) + "</td></tr>" 
                //                           );
                //if(Convert.ToString(dtPerformance.Rows[0]["IsJobPromotion"]) == "Yes")
                //MessagePerformance.Append("<tr><td style='width:25%'> New Designation : </td><td style='width:70%'>" + Convert.ToString(dtPerformance.Rows[0]["Designation"]) + "</td></tr>"+
                //                          "<tr><td style='width:25%'> New WorkPolicy : </td><td style='width:70%'>" + Convert.ToString(dtPerformance.Rows[0]["WorkPolicy"]) + "</td></tr>" 
                //                          );
                //if (Convert.ToString(dtPerformance.Rows[0]["IsSalaryAltered"]) == "Yes")
                //    MessagePerformance.Append("<tr><td style='width:25%'> New Salary : </td><td style='width:70%'>" + Convert.ToString(dtPerformance.Rows[0]["CurrentNetSalary"]) + "</td></tr>" 
                //                              );



                //if (Convert.ToString(dtPerformance.Rows[0]["FeedBack"]) != "")
                //    MessagePerformance.Append("<tr><td style='width:25%'> Remarks: </td><td style='width:70%'>" + Convert.ToString(dtPerformance.Rows[0]["Feedback"]) + "</td></tr>"
                //                              );
                //MessagePerformance.Append("</table>");








                    MessagePerformance.Append("<table width='100%' border='0' cellpadding='3' style='font-family: Tahoma; font-size: 12px;      vertical-align: top'>");
                    MessagePerformance.Append("<tr>");
                    MessagePerformance.Append("<td colspan ='2'>");
                    MessagePerformance.Append("Your Performance Evaluation Between " + Convert.ToString(dtPerformance.Rows[0]["FromDate"]) + " and " + Convert.ToString(dtPerformance.Rows[0]["ToDate"]) + " has been evaluated");
                    MessagePerformance.Append("</td>");
                    MessagePerformance.Append("</tr>");
      
                    MessagePerformance.Append("<tr>");
                    MessagePerformance.Append("<td  colspan ='2'  style ='font-weight :bold'>");
                    MessagePerformance.Append("Evaluation Details :- ");
                    MessagePerformance.Append("</td>");
                    MessagePerformance.Append("</tr>");



                    if (Convert.ToString(dtPerformance.Rows[0]["IsJobPromotion"]) == "Yes")
                    {
                        MessagePerformance.Append("<tr>");
                        MessagePerformance.Append("<td colspan ='2' style ='padding-left:50px'>");
                        MessagePerformance.Append("Your designation has been changed to " + Convert.ToString(dtPerformance.Rows[0]["Designation"]));
                        MessagePerformance.Append("</td>");
                        MessagePerformance.Append("</tr>");
                    }
                    else
                    {
                        MessagePerformance.Append("<tr>");
                        MessagePerformance.Append("<td colspan ='2' style ='padding-left:50px'>");
                        MessagePerformance.Append("You have no job promotion");
                        MessagePerformance.Append("</td>");
                        MessagePerformance.Append("</tr>");
                    }
        
                    MessagePerformance.Append("<tr>");
                    MessagePerformance.Append("<td style='width: 25%' style ='padding-left:50px'>");
                    MessagePerformance.Append("Your salary has been altered to " + Convert.ToString(dtPerformance.Rows[0]["CurrentNetSalary"]));
                    MessagePerformance.Append("</td>");
                    MessagePerformance.Append("</tr>");

                    if (Convert.ToString(dtPerformance.Rows[0]["FeedBack"]) != "")
                    {
                        MessagePerformance.Append("<tr>");
                        MessagePerformance.Append("<td colspan ='2' style ='padding-left:50px'>");
                        MessagePerformance.Append("Remarks :" + Convert.ToString(dtPerformance.Rows[0]["Feedback"]));
                        MessagePerformance.Append("</td>");
                        MessagePerformance.Append("</tr>");
                    }
                    MessagePerformance.Append("</table> ");




            }
            SendPerformanceMail(RequestID, MRequestType);
        }
    }

    private void SendPerformanceMail(int RequestID, MailRequestType MRequestType)
    {
        try
        {
            DataTable dtMail = GetPerforamceMailID(RequestID, MRequestType);

            Page page = (Page)HttpContext.Current.Handler;

            if (Convert.ToString(dtMail.Rows[0]["FromMailID"]) == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail as your official Email has not been set')", true);
                return;
            }
            else if (Convert.ToString(dtMail.Rows[0]["ToMailID"]) == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail for the Recipient(s) as official Email has not been set')", true);
                return;
            }

            Thread t1 = new Thread(() => new clsMailSettings().SendMail(Convert.ToString(dtMail.Rows[0]["FromMailID"]), Convert.ToString(dtMail.Rows[0]["ToMailID"]), Subject, MessagePerformance.ToString(), clsMailSettings.AccountType.Email, false));
            t1.Start();
        }
        catch
        {
        }

    }

    private DataTable GetPerformanceDetails(int RequestID, MailRequestType MRequestType)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 6));
        Parameters.Add(new SqlParameter("@RequestID", RequestID));
        Parameters.Add(new SqlParameter("@RequestTypeID", (int)MRequestType));
        return new DataLayer().ExecuteDataTable("HRspCommonMail", Parameters);
    }
    private DataTable GetPerforamceMailID(int RequestID,MailRequestType MRequestType)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 5));
        Parameters.Add(new SqlParameter("@RequestID", RequestID));
        Parameters.Add(new SqlParameter("@RequestTypeID", (int)MRequestType));
        return new DataLayer().ExecuteDataTable("HRspCommonMail", Parameters);
    }

    private void AuthoritiesEmailBody(DataTable dtRequest,MailRequestType MRequestType,string Status)
    {
        
        string TableHeader  =   "<table width='100%' border='0' cellpadding='3' style='border: 1px solid;padding: 2%; width: 90%; font-family: Tahoma; font-size:12px; vertical-align: top'>";
        string FirstRow     =   "<tr><td style='width:20%'> Applied By </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Name"]) + "</td></tr>" +
                                "<tr><td style='width:20%'> Applied Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + "</td></tr>";
        
        string LastRow      =   string.Empty;

        if(Status != "Applied" && Status != "RequestForCancel")
            LastRow = "<tr><td style='width:20%'>" + Status + " by </td><td style='width : 5%'> : </td><td style='width:70%'>" + EmployeeName + "</td></tr>" +
                     "<tr><td style='width:20%'>" + Status + " Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + CurrentDate + "</td></tr>";

        string TableFooter = "</table>";

        switch (MRequestType)
        {
            case MailRequestType.Document: MessageAuthorities.Append(
                                           TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Document Request " + Status + "</td></tr>" + FirstRow +
                                           "<tr><td style='width:20%'> Type </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Type"]) + "</td></tr>" +
                                           "<tr><td style='width:20%'> Number </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["DocumentNumber"]) + "</td></tr>" +
                                           "<tr><td style='width:20%'> Document From Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Date"]) + "</td></tr>" +
                                           "<tr><td valign='top' style='width:20%;'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                           LastRow+TableFooter
                                           );
                break;
            case MailRequestType.Vacation:  MessageAuthorities.Append(
                                            TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Vacation Request " + Status + "</td></tr>" + FirstRow +
                                            "<tr><td style='width:20%'> Period </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]) + "</td></tr>" +
                                            "<tr><td valign='top'style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                            LastRow + "</table>"
                                            );
                break;
            case MailRequestType.Leave: MessageAuthorities.Append(
                                        TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Leave Request " + Status + "</td></tr>" + FirstRow +
                                        "<tr><td style='width:20%'> Type </td><td style='width : 5%'> : </td><td style='width:70%'>" + (dtRequest.Rows[0]["HalfDay"].ToInt32() == 1? "HalfDay" : "FullDay") + "</td></tr>" +
                                        "<tr><td style='width:20%'>" + (dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? "Date" : "Period")+" </td><td style='width : 5%'> : </td><td style='width:70%'>" + (dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? Convert.ToString(dtRequest.Rows[0]["From"]) : Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"])) + "</td></tr>" +
                                        "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                        LastRow + TableFooter
                                        );
                break;
            case MailRequestType.LeaveExtension: MessageAuthorities.Append(
                                           TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Leave Extension Request " + Status + "</td></tr>" + FirstRow +
                                           "<tr><td style='width:20%'> Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Date"]) + "</td></tr>" +
                                           "<tr><td style='width:20%'> Duration </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Duration"]) + "</td></tr>" +
                                           "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                           LastRow + TableFooter
                                           );
                break;
            case MailRequestType.TimeExtension: MessageAuthorities.Append(
                                       TableHeader + "<tr style='font-weight:bold' > <td colspan = '2'> Time Extension Request " + Status + "</td></tr>" + FirstRow +
                                       "<tr><td style='width:20%'> Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Date"]) + "</td></tr>" +
                                       "<tr><td style='width:20%'> Duration </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Duration"]) + "</td></tr>" +
                                       "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                       LastRow + TableFooter
                                       );
                break;
            case MailRequestType.Expense: MessageAuthorities.Append(
                                   TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Expense Request " + Status + "</td></tr>" + FirstRow +
                                   "<tr><td style='width:20%'> Expense Head </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["ExpenseHead"]) + "</td></tr>" +
                                   "<tr><td style='width:20%'> Period </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]) + "</td></tr>" +
                                   "<tr><td style='width:20%'> Amount </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Amount"]) + "</td></tr>" +
                                   "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                                   LastRow + TableFooter
                                   );
                break;
            case MailRequestType.SalaryAdvance: MessageAuthorities.Append(
                             TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Salary Advance Request " + Status + "</td></tr>" + FirstRow +
                             "<tr><td style='width:20%'> Amount </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Amount"]) + "</td></tr>" +
                             "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                             LastRow + TableFooter
                             );
                break;
            case MailRequestType.Transfer: MessageAuthorities.Append(
                        TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Transfer Request " + Status + "</td></tr>" + FirstRow +
                        "<tr><td style='width:20%'> Type  </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Type"]) + "</td></tr>" +
                        "<tr><td style='width:20%'> From </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["From"]) + "</td></tr>" +
                        "<tr><td style='width:20%'> To </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["To"]) + "</td></tr>" +
                        "<tr><td style='width:20%'> Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Date"]) + "</td></tr>"+
                        "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                        LastRow + TableFooter
                        );
                break;
            case MailRequestType.Loan: MessageAuthorities.Append(
                  TableHeader + "<tr style='font-weight:bold' ><td colspan = '2'> Loan Request " + Status + "</td></tr>" + FirstRow +
                  "<tr><td style='width:20%'> Date </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Date"]) + "</td></tr>" +
                  "<tr><td style='width:20%'> Type  </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Type"]) + "</td></tr>" +
                  "<tr><td style='width:20%'> Amount </td><td style='width : 5%'> : </td><td style='width:70%'>" + Convert.ToString(dtRequest.Rows[0]["Amount"]) + "</td></tr>" +
                  "<tr><td valign='top' style='width:20%'> Reason </td><td valign='top' style='width : 5%'> : </td><td style='width:70%;word-break:break-all'>" + Convert.ToString(dtRequest.Rows[0]["Reason"]) + "</td></tr>" +
                  LastRow + TableFooter
                  );
                break;
 
        }
    }

    private void EmployeeEmailBody(DataTable dtRequest, MailRequestType MRequestType, string Status)
    {
        string TableHeader = "<table width='100%' border='0' cellpadding='3' style='font-family: Tahoma; font-size:12px; vertical-align: top'><tr><td>";
        string TableFooter = "</td></tr></table>"; 
        switch (MRequestType)
        {
            case MailRequestType.Document: MessageEmployee.Append(TableHeader + "Your " + Convert.ToString(dtRequest.Rows[0]["Type"]) + " - " + Convert.ToString(dtRequest.Rows[0]["DocumentNumber"]) + " Request has been "+ Status + " by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.Expense: MessageEmployee.Append(TableHeader + "Your Expense Request [" + Convert.ToString(dtRequest.Rows[0]["ExpenseHead"]) + "] for an amount of Rs." + Convert.ToString(dtRequest.Rows[0]["Amount"]) + " for " + Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]) + " has been " + Status + " by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.Leave: MessageEmployee.Append(TableHeader + "Your " + (dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? "HalfDay " : "") + Convert.ToString(dtRequest.Rows[0]["Type"]) + " Request For " + (dtRequest.Rows[0]["HalfDay"].ToInt32() == 1 ? Convert.ToString(dtRequest.Rows[0]["From"]) : (Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]))) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.LeaveExtension: MessageEmployee.Append(TableHeader + "Your Leave Extension Request from " + Convert.ToString(dtRequest.Rows[0]["Date"]) + " for " + Convert.ToString(dtRequest.Rows[0]["Duration"]) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.SalaryAdvance: MessageEmployee.Append(TableHeader + "Your Salary Advance requested on " + Convert.ToString(dtRequest.Rows[0]["RequestedDate"]) + " for an amount of Rs." + Convert.ToString(dtRequest.Rows[0]["Amount"]) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.TimeExtension: MessageEmployee.Append(TableHeader + "Your Time Extension Request on " + Convert.ToString(dtRequest.Rows[0]["Date"]) + " for " + Convert.ToString(dtRequest.Rows[0]["Duration"]) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.Transfer: MessageEmployee.Append(TableHeader + "Your Transfer Request [" + Convert.ToString(dtRequest.Rows[0]["Type"]) + "] from " + Convert.ToString(dtRequest.Rows[0]["From"]) + " to " + Convert.ToString(dtRequest.Rows[0]["To"]) + " on " + Convert.ToString(dtRequest.Rows[0]["Date"]) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.Vacation: MessageEmployee.Append(TableHeader + "Your Vacation Request For " + Convert.ToString(dtRequest.Rows[0]["From"]) + " - " + Convert.ToString(dtRequest.Rows[0]["To"]) + " has been "+Status+" by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
            case MailRequestType.Loan: MessageEmployee.Append(TableHeader + "Your  Loan Request [" + Convert.ToString(dtRequest.Rows[0]["Type"])+"]" + Convert.ToString(dtRequest.Rows[0]["Date"]) + " has been " + Status + " by " + EmployeeName + " on " + CurrentDate + TableFooter);
                break;
        }
    }

    /// <summary>
    /// Send Mail
    /// </summary>
    /// <param name="MailAuthorities">Message For Authorities</param>
    /// <param name="MailEmployee">Message For Employee</param>
    /// <param name="Action">Action Performed</param>
    private void SendMail(string MessageAuthorities, string MessageEmployee, eAction Action)
    {
        try
        {
            //Get Mail IDs 
            GetMailIDs(Action);

            Page page = (Page)HttpContext.Current.Handler;

            if (AuthorityEmailID == string.Empty) // Check whether EmailID exists for Authorities
            {
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail for the Recipient(s) as official Email has not been set')", true);
                return;
            }
            else if(FromEmailID == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(page, page.GetType(), Guid.NewGuid().ToString(), "alert('Unable to send mail as your official Email has not been set')", true);
                return;
            }

            // Mail For Authorities
            Thread t1 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, AuthorityEmailID, ReportingToEmailID, Subject, MessageAuthorities, "", false, clsMailSettings.AccountType.Email));
            t1.Start();

            // Mail For Employee
            if (MessageEmployee != string.Empty)//Approved,Rejected,Forwarded and Cancelled
            {
                Thread t2 = new Thread(() => new clsMailSettings().SendMail(FromEmailID, EmployeeEmailID, Subject, MessageEmployee, clsMailSettings.AccountType.Email, false));
                t2.Start();
            }
           
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }

    }

    private void GetMailIDs(eAction Action)
    {
        try
        {
            AuthorityEmailID = string.Empty;
            EmployeeEmailID = string.Empty;
            FromEmailID = string.Empty;
            ReportingToEmailID = string.Empty;

            ArrayList Parameters = new ArrayList();
            Parameters.Add(new SqlParameter("@Mode", 4));
            Parameters.Add(new SqlParameter("@RequestedBy", dtRequest.Rows[0]["EmployeeID"].ToInt32()));
            Parameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));
            DataTable dtTemp = new DataLayer().ExecuteDataTable("HRspCommonMail", Parameters);

            foreach (DataRow dr in dtTemp.Rows)
            {
                if (Convert.ToString(dr["Email"]) != string.Empty)
                {
                    if (dr["Priority"].ToInt32() == (int)ActionBy) // Login Employee
                    {
                        FromEmailID = Convert.ToString(dr["Email"]);
                    }
                    else if (dr["Priority"].ToInt32() == 1)//Requested By
                    {
                        EmployeeEmailID = Convert.ToString(dr["Email"]);
                    }
                    else if (dr["Priority"].ToInt32() == 2 && Action != eAction.RequestForCancel)//Reporting To
                    {
                        ReportingToEmailID = Convert.ToString(dr["Email"]);
                    }
                    else if (dr["Priority"].ToInt32() == 3 || dr["Priority"].ToInt32() == 4)//Higher Authorities
                    {
                        AuthorityEmailID = AuthorityEmailID + "," + Convert.ToString(dr["Email"]);
                    }
                }
            }
            if (AuthorityEmailID.Length > 0)
                AuthorityEmailID = AuthorityEmailID.Substring(1, AuthorityEmailID.Length - 1);
        }
        catch
        {
        }
    }
    /// <summary>
    /// Get Request Details
    /// </summary>
    /// <param name="RequestID">RequestID</param>
    /// <param name="RequestType">RequestType</param>
    /// <returns></returns>
    private DataTable GetRequestDetails(int RequestID, MailRequestType RequestType)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 2));
        Parameters.Add(new SqlParameter("@RequestID", RequestID));
        Parameters.Add(new SqlParameter("@RequestTypeID", (int)RequestType));
        return new DataLayer().ExecuteDataTable("HRspCommonMail", Parameters);
    }

    /// <summary>
    /// Get ReportingIDs
    /// </summary>
    /// <param name="RequestedBy">RequestedBy</param>
    private void GetReportingIds(int RequestedBy)
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 1));
        Parameters.Add(new SqlParameter("@RequestedBy", RequestedBy));
        Parameters.Add(new SqlParameter("@RequestTypeID", RequestTypeID));

        DataRow dr = new DataLayer().ExecuteDataTable("HRspCommonMail", Parameters).Rows[0];

        if (dr.ItemArray.Length > 0)
        {
            ReportingTo = dr["ReportingTo"].ToInt32();
            AlternateAuthority = dr["AlternateAuthority"].ToInt32();
            HigherAuthority = dr["HigherAuthority"].ToInt32();
        }
    }

    /// <summary>
    /// Get Request Type
    /// </summary>
    /// <param name="MRequestType">Mail Request Type</param>
    private void GetRequestType(MailRequestType MRequestType)
    {
        switch (MRequestType)
        {
            case MailRequestType.Document: RequestTypeID = (int)RequestType.Document;
                break;
            case MailRequestType.Expense: RequestTypeID = (int)RequestType.Expense;
                break;
            case MailRequestType.Leave:
            case MailRequestType.Vacation: RequestTypeID = (int)RequestType.Leave;
                break;
            case MailRequestType.TimeExtension:
            case MailRequestType.LeaveExtension: RequestTypeID = (int)RequestType.LeaveExtension;
                break;
            case MailRequestType.Loan: RequestTypeID = (int)RequestType.Loan;
                break;
            case MailRequestType.SalaryAdvance: RequestTypeID = (int)RequestType.SalaryAdvance;
                break;
            case MailRequestType.Transfer: RequestTypeID = (int)RequestType.Transfer;
                break;
            case MailRequestType.AttendanceRequest: RequestTypeID = (int)RequestType.AttendanceRequest;
                break;

        }
    }

    /// <summary>
    /// Get Login Employee Name
    /// </summary>
    private void GetEmployeeName()
    {
        ArrayList Parameters = new ArrayList();
        Parameters.Add(new SqlParameter("@Mode", 3));
        Parameters.Add(new SqlParameter("@EmployeeID", new clsUserMaster().GetEmployeeId()));
        EmployeeName = Convert.ToString(new DataLayer().ExecuteScalar("HRspCommonMail", Parameters));
    }

    /// <summary>
    /// Action By
    /// </summary>
    private enum eActionBy
    {
        Employee = 1,
        ReportingTo = 2,
        AlternateAuthority = 3,
        HigherAuthority = 4
    }

}

/// <summary>
/// Mail Request Type
/// </summary>
public enum MailRequestType
{
    Leave = 1,
    Loan = 2,
    SalaryAdvance = 3,
    Transfer = 4,
    Expense = 5,
    Document = 6,
    LeaveExtension = 7,
    TimeExtension = 8,
    Vacation = 9,
    PerformanceInitiation = 10,
    PerformanceAction  = 11,
    AttendanceRequest =14,
    PerformanceEvaluation=15,
    TicketRequest=16,
    TravelRequest=17,
    ResignationRequest=18, // mismatch with table
    AssetRequest = 19,
    GeneralRequest = 20,
    RejoinRequest = 21,
    SalaryCertificate=22,
    SalaryBankStatement=23,
    SalaryCardLost=24,
    Salaryslip=25,
    VisaLetter=26,
    NocUmarahVisa=27,
    CardLost=28

}

