﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsVacancyView
/// </summary>
public class clsVacancyView:DL
{
    #region Variables
    public static string Procedure = "HRspVacancyView";
    #endregion

    #region Properties
    public int JobID { get; set; }   
    #endregion

	public clsVacancyView()
	{ }

    #region Methods

    public static DataSet GetVacancyDetails(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GVD"));
        arr.Add(new SqlParameter("@JobID", JobID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataSet(Procedure, arr);
    }

    #endregion
}
