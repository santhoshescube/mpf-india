﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for clsJobKPI
/// </summary>
/// 
[Serializable]
public class clsKPI
{
    public int KPIId { get; set; }

    public decimal KPIValue { get; set; }

    public int EmployeeKpiID { get; set; }

	public clsKPI()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
