﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDesignationReference
/// </summary>
public class clsDesignationReference:DL
{
	

    #region Declarations
    
    public int DesignationID { get; set; }
    public string Designation { get; set; }
    public string DesignationArb { get; set; }
    public string Parent { get; set; }
    public int ParentDesignationID { get; set; }
    public int CompanyID { get;set; }
    #endregion Declarations

    #region Constructor

    public clsDesignationReference()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #endregion Constructor

    #region Methods

    public int AddDesignationReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "INS"));
        parameters.Add(new SqlParameter("@Designation", Designation));
        parameters.Add(new SqlParameter("@DesignationArb", DesignationArb));       
        parameters.Add(new SqlParameter("@ParentDesignationID", ParentDesignationID));

        return ExecuteScalar("HRspDesignationReference", parameters).ToInt32();
       
    }
    public void InsertHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "INSH"));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@ParentDesignationID", ParentDesignationID));

        ExecuteNonQuery("HRspDesignationReference", parameters);
    }

    public void UpdateDesignationReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "UPD"));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@Designation", Designation));
        parameters.Add(new SqlParameter("@DesignationArb", DesignationArb));
       // parameters.Add(new SqlParameter("@ParentDesignationID", ParentDesignationID));
       
        ExecuteNonQuery("HRspDesignationReference", parameters);
    }
    public DataTable GetDesignation(int CompanyID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "SEL"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDesignationReference", parameters);
    }

    public DataTable BindGrid(int CompanyID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GRID"));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDesignationReference", parameters);
    }
    public DataTable GetDataById(int DesignationID, int CompanyID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GBI"));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspDesignationReference", parameters);
    }
   
    public int CheckDesignationNameExistence()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DUP"));
        if (DesignationID > 0)
        {
            parameters.Add(new SqlParameter("@DesignationID", DesignationID));
            parameters.Add(new SqlParameter("@Designation", Designation));
            parameters.Add(new SqlParameter("@DesignationArb", DesignationArb));
        }
        else
        {
            parameters.Add(new SqlParameter("@Designation", Designation));
            parameters.Add(new SqlParameter("@DesignationArb", DesignationArb));
        }
        return Convert.ToInt32(ExecuteScalar("HRspDesignationReference", parameters));
    }
    //public int IsDesignationUsed()
    //{
    //    ArrayList parameters = new ArrayList();
    //    parameters.Add(new SqlParameter("@Mode", "IAU"));
    //    //parameters.Add(new SqlParameter("@AgencyID", AgencyID));

    //    return Convert.ToInt32(ExecuteScalar("HRspDesignationReference", parameters));
    //}

    #endregion Methods
}
