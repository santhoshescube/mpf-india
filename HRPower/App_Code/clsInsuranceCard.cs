﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/*==============================================
Description :	     <Insurance Card>
Modified By :		 <Megha>
Modified Date :      <15 July 2013>
================================================*/

/// <summary>
/// Summary description for clsInsuranceCard
/// </summary>
public class clsInsuranceCard : DL
{
    private ArrayList InsuranceArrayList = null;
    private string ProcedureName = "MvfEmployeeInsuranceCardTransactions";

    int iPageIndex;
    int iPageSize;
    string sSearchKey = string.Empty;

    //BO Section
    public int InsuranceCardID { get; set; }

    public int EmployeeID { get; set; }

    public string CardNumber { get; set; }

    public string PolicyNumber { get; set; }

    public string PolicyName { get; set; }

    public int InsuranceCompanyID { get; set; }
    public int CompanyID { get; set; }
    public DateTime IssueDate { get; set; }

    public DateTime ExpiryDate { get; set; }

    public DateTime RenewalDate { get; set; }

    public string Remarks { get; set; }

    public int? IssueID { get; set; }

    public bool IsRenewed { get; set; }

    public int PageIndex { get { return iPageIndex; } set { iPageIndex = value; } }

    public int PageSize { get { return iPageSize; } set { iPageSize = value; } }

    public string SearchKey { get { return sSearchKey; } set { sSearchKey = value; } }

    //DAL Section

    public void BeginEmp()
    {
        BeginTransaction(ProcedureName);
    }

    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    public bool SaveInsuranceCard()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 2));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardId", InsuranceCardID));
        InsuranceArrayList.Add(new SqlParameter("@EmployeeID", EmployeeID));
        InsuranceArrayList.Add(new SqlParameter("@CardNumber", CardNumber));
        InsuranceArrayList.Add(new SqlParameter("@PolicyNumber", PolicyNumber));
        InsuranceArrayList.Add(new SqlParameter("@PolicyName", PolicyName));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCompanyID", InsuranceCompanyID));
        InsuranceArrayList.Add(new SqlParameter("@IssueDate", IssueDate.ToString("dd MMM yyyy")));
        InsuranceArrayList.Add(new SqlParameter("@ExpiryDate", ExpiryDate.ToString("dd MMM yyyy")));
        InsuranceArrayList.Add(new SqlParameter("@Remarks", Remarks));
        InsuranceArrayList.Add(new SqlParameter("@IssuedID", IssueID));

        InsuranceCardID = ExecuteScalar(ProcedureName, InsuranceArrayList).ToInt32();
        return InsuranceCardID > 0;
    }

    public bool UpdateInsuranceCard()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 3));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardId", InsuranceCardID));
        InsuranceArrayList.Add(new SqlParameter("@EmployeeID", EmployeeID));
        InsuranceArrayList.Add(new SqlParameter("@CardNumber", CardNumber));
        InsuranceArrayList.Add(new SqlParameter("@PolicyNumber", PolicyNumber));
        InsuranceArrayList.Add(new SqlParameter("@PolicyName", PolicyName));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCompanyID", InsuranceCompanyID));
        InsuranceArrayList.Add(new SqlParameter("@IssueDate", IssueDate.ToString("dd MMM yyyy")));
        InsuranceArrayList.Add(new SqlParameter("@ExpiryDate", ExpiryDate.ToString("dd MMM yyyy")));
        InsuranceArrayList.Add(new SqlParameter("@IsRenewed", IsRenewed));
        InsuranceArrayList.Add(new SqlParameter("@Remarks", Remarks));
        InsuranceArrayList.Add(new SqlParameter("@IssuedID", IssueID));

        InsuranceCardID = ExecuteScalar(ProcedureName, InsuranceArrayList).ToInt32();
        return InsuranceCardID > 0;
    }

    public int UpdateRenewEmployeeInsuranceCard()  //Update Renew Employee Insurance Card
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", 20));
        alparameters.Add(new SqlParameter("@InsuranceCardId", InsuranceCardID));
        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public bool DeleteInsuranceCard(int InsuranceCardID)
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 4));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID", InsuranceCardID));

        return new DataLayer().ExecuteNonQuery(ProcedureName, InsuranceArrayList).ToInt32() > 0;
    }

    public DataTable GetInsuranceCardIssueReference()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 9));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));


        return ExecuteDataTable(ProcedureName, InsuranceArrayList);
    }

    public DataTable GetInsuranceCompanyReference()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 10));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable(ProcedureName, InsuranceArrayList);
    }

    public DataTable GetEmployeeNames()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 11));
        InsuranceArrayList.Add(new SqlParameter("@EmployeeID", EmployeeID));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID", InsuranceCardID));
        InsuranceArrayList.Add(new SqlParameter("@CompanyID", CompanyID));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(ProcedureName, InsuranceArrayList);
    }

    public DataSet GetAllInsuranceCardDetails()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 12));
        InsuranceArrayList.Add(new SqlParameter("@PageIndex", PageIndex));
        InsuranceArrayList.Add(new SqlParameter("@PageSize", PageSize));
        InsuranceArrayList.Add(new SqlParameter("@SearchKey", SearchKey));
        InsuranceArrayList.Add(new SqlParameter("@CompanyID", CompanyID));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        
        return ExecuteDataSet(ProcedureName, InsuranceArrayList);
    }

    public bool GetInsuranceCardByPNO()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 17));
        InsuranceArrayList.Add(new SqlParameter("@PolicyNumber", PolicyNumber));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID", InsuranceCardID));

        return new DataLayer().ExecuteScalar(ProcedureName, InsuranceArrayList).ToInt32() > 0;
        
    }
    public bool GetInsuranceCardByCNO()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 16));
        InsuranceArrayList.Add(new SqlParameter("@CardNumber", CardNumber));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID", InsuranceCardID));

        return new DataLayer().ExecuteScalar(ProcedureName, InsuranceArrayList).ToInt32() > 0;
        
    }

    public DataTable GetInsuranceCardByID(int InsuranceCardID, int iDocType)
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("Mode", 13));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID" , InsuranceCardID));
        InsuranceArrayList.Add(new SqlParameter("@DocumentTypeID", iDocType));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));


        return ExecuteDataTable(ProcedureName, InsuranceArrayList);
    }

    public int GetCardCount()
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 14));
        InsuranceArrayList.Add(new SqlParameter("@SearchKey", sSearchKey.EscapeUnsafeChars()));
        InsuranceArrayList.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar(ProcedureName, InsuranceArrayList));
    }

    public string PrintInsuranceCard(string InsuranceCardIDs)
    {
        StringBuilder sb = new StringBuilder();
        
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 15));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardIDs", InsuranceCardIDs));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));


        DataTable dtPrint = new DataLayer().ExecuteDataTable(ProcedureName, InsuranceArrayList);

        if (dtPrint != null && dtPrint.Rows.Count > 0)
        {
            sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
            sb.Append("<tr><td style='font-weight:bold;' align='center'>Insurance Card Details</td></tr>");
            sb.Append("<tr><td>");
            sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
            sb.Append("<tr style='font-weight:bold;'><td width='100px'> Employee Name </td><td width='100px'> Card Number </td><td width='150px'>Policy Number</td><td width='150px'>Policy Name</td><td width='150px'>Insurance Company</td><td width='100px'>Issued By</td><td width='100px'>Issue Date</td><td width='100px'>Expiry Date</td><td width='100px'> Is Renewed </td><td width='100px'>Other Info</td></tr>");
            sb.Append("<tr><td colspan='11'><hr></td></tr>");
            for (int i = 0; i < dtPrint.Rows.Count; i++)
            {
                string renew = dtPrint.Rows[i]["IsRenewed"].ToString();
                if (renew.ToBoolean())
                {
                    renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                }
                else
                {
                    renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                }
                sb.Append("<td>" + dtPrint.Rows[i]["EmployeeFullName"].ToString() + "</td>");
                sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["CardNumber"]) + "</td>");
                sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["PolicyNumber"]) + "</td>");
                sb.Append("<td>" + dtPrint.Rows[i]["PolicyName"].ToString() + "</td>");
                sb.Append("<td>" + dtPrint.Rows[i]["Description"].ToString() + "</td>");
                sb.Append("<td>" + dtPrint.Rows[i]["IssuedBy"].ToString() + "</td>");
                sb.Append("<td>" + dtPrint.Rows[i]["IssueDate"].ToString() + "</td>");
                sb.Append("<td>" + dtPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                sb.Append("<td>" + renew + "</td>");
                sb.Append("<td style='word-break:break-all'>" + dtPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

            }
            sb.Append("</td></tr>");
            sb.Append("</table>");
        }
        return sb.ToString();
    }
    public  DataTable  PrintSingleInsuranceCard(string InsuranceCardIds)
    {
        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 15));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardIDs", InsuranceCardIds));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));


       return new DataLayer().ExecuteDataTable(ProcedureName, InsuranceArrayList);

    }
   
    public string GetPrintText(int InsuranceID)
    {
        StringBuilder sb = new StringBuilder();

        InsuranceArrayList = new ArrayList();
        InsuranceArrayList.Add(new SqlParameter("@Mode", 13));
        InsuranceArrayList.Add(new SqlParameter("@InsuranceCardID", InsuranceID));
        InsuranceArrayList.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        DataTable dtCard = new DataLayer().ExecuteDataTable(ProcedureName, InsuranceArrayList);

        if (dtCard != null && dtCard.Rows.Count > 0)
        {
            string renew=dtCard.Rows[0]["IsRenewed"].ToString();
            
            if(renew.ToBoolean())
            {
                renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
            }
            else
            {
                renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
             }
            sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
            sb.Append("<tr><td><table border='0'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + dtCard.Rows[0]["EmployeeFullName"].ToString() + "  [" + Convert.ToString(dtCard.Rows[0]["CardNumber"]) + "]" + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr><td style='padding-left:15px;'>");
            sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
            
            sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Insurance Card Details</td></tr>");
            sb.Append("<tr><td width='150px'>Card Number</td><td width='5px'>:</td><td>" + Convert.ToString(dtCard.Rows[0]["CardNumber"]) + "</td></tr>");
            sb.Append("<tr><td width='150px'>Policy Number</td><td width='5px'>:</td><td>" + Convert.ToString(dtCard.Rows[0]["PolicyNumber"]) + "</td></tr>");
            sb.Append("<tr><td width='150px'>Policy Name</td><td width='5px'>:</td><td>" + dtCard.Rows[0]["PolicyName"].ToString() + "</td></tr>");
            sb.Append("<tr><td width='150px'>Insurance Company</td><td width='5px'>:</td><td>" + dtCard.Rows[0]["Description"].ToString() + "</td></tr>");
            sb.Append("<tr><td width='150px'>Issued By</td><td width='5px'>:</td><td>" + dtCard.Rows[0]["IssuedBy"].ToString() + "</td></tr>");
            sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + dtCard.Rows[0]["IssueDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
            sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + dtCard.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
            sb.Append("<tr><td>Is Renewed</td><td>:</td><td>" + renew + "</td></tr>");
            sb.Append("<tr><td width='150px'>Other Info</td><td width='5px'>:</td><td style='word-break:break-all'>" + Convert.ToString(dtCard.Rows[0]["Remarks"]) + "</td></tr>");

            sb.Append("</td>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr></table>");

        }

        return sb.ToString();
    }

    public DataTable GetAttachments(int CardID, int EmployeeID, int DocType)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 18));
        parameters.Add(new SqlParameter("@InsuranceCardID", CardID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@DocumentTypeID", DocType));
        return ExecuteDataTable("MvfEmployeeInsuranceCardTransactions", parameters);
    }

    public bool CheckDuplication(int CardID, string CardNumber)
    {
        Boolean blnExists = false;
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 19));
        parameters.Add(new SqlParameter("@InsuranceCardID", InsuranceCardID));
        parameters.Add(new SqlParameter("@CardNumber", CardNumber));
        //parameters.Add(new SqlParameter("@FormType", iFormType));
        blnExists = (ExecuteScalar("MvfEmployeeInsuranceCardTransactions", parameters)).ToInt32() > 0 ? true : false;
        return blnExists;
    }
    public bool SaveTreeMaster(int iCardID, int iDocType, int iEmployeeID, string sReferenceNumber, string sDocumentName, string sFileName)
    {
        ArrayList prmDocuments = new ArrayList();

        prmDocuments.Add(new SqlParameter("@Mode", 19));
        prmDocuments.Add(new SqlParameter("@ReferenceID", iEmployeeID));
        prmDocuments.Add(new SqlParameter("@DocumentTypeID", iDocType));
        prmDocuments.Add(new SqlParameter("@CardID", iCardID));
        prmDocuments.Add(new SqlParameter("@ReferenceNumber", sReferenceNumber));
        prmDocuments.Add(new SqlParameter("@Docname", sDocumentName));
        prmDocuments.Add(new SqlParameter("@Filename", sFileName));

        return ExecuteScalar("HRspEmployeeHealthCardTransactions", prmDocuments).ToInt32() > 0;
    }
}