﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsVisaRequest
/// </summary>
public class clsVisaRequest:DL
{
    private int iEmployeeId, iStatusId, iRequestId, iPageIndex, iPageSize, iVisaType;
    private string sMode, sReason, sRequestedTo, sApprovedBy;
    private DateTime dtVisaFromDate;

    #region Properties
    public int EmployeeId
    {
        set { iEmployeeId = value; }
        get { return iEmployeeId; }
    }
    public string Mode
    {
        set { sMode = value; }
        get { return sMode; }
    }
    public int VisaType
    {
        set { iVisaType = value; }
        get { return iVisaType; }
    }

    public DateTime VisaFromDate
    {
        set { dtVisaFromDate = value; }
        get { return dtVisaFromDate; }
    }

    public string Reason
    {
        set { sReason = value; }
        get { return sReason; }
    }
    public string RequestedTo
    {
        set { sRequestedTo = value; }
        get { return sRequestedTo; }
    }
    public string ApprovedBy
    {
        set { sApprovedBy = value; }
        get { return sApprovedBy; }
    }
    public int StatusId
    {
        set { iStatusId = value; }
        get { return iStatusId; }
    }
    public int RequestId
    {
        set { iRequestId = value; }
        get { return iRequestId; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    #endregion
	public clsVisaRequest()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    /// <summary>
    /// insert / update visa request
    /// </summary>
    ///<CreatedBy>Jisha Bijoy</CreatedBy>
    ///<CreatedOn>08 Nov 2010</CreatedOn>
    ///<ModifiedBy></ModifiedBy>
    ///<ModifiedOn></ModifiedOn>
    public int InsertUpdateRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@VisaTypeId", iVisaType));
        alParameters.Add(new SqlParameter("@VisaFromDate", dtVisaFromDate ));
        alParameters.Add(new SqlParameter("@Reason", sReason));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        if (iRequestId > 0)
            alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToInt32(ExecuteScalar("HRspVisaRequest", alParameters));

    }
    /// <summary>
    /// get requests.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequests()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));

        return ExecuteDataTable("HRspVisaRequest", alParameters);
    }

    /// <summary>
    /// return record count.
    /// </summary>
    /// <returns></returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ERC"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspVisaRequest", alParameters));
        return iCount;
    }
    /// <summary>
    /// get loan request details.
    /// </summary>
    /// <returns></returns>
    public DataTable GetRequestDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", sMode));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspVisaRequest", alParameters);
    }
    /// <summary>
    /// update status.
    /// </summary>
    public void UpdateVisaStatus(bool IsAuthorised)
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "UPS"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));
        alParemeters.Add(new SqlParameter("@StatusId", iStatusId));
        alParemeters.Add(new SqlParameter("@ApprovedBy", sApprovedBy));
        alParemeters.Add(new SqlParameter("@IsAuthorised", IsAuthorised));

        ExecuteNonQuery("HRspVisaRequest", alParemeters);
    }
    /// <summary>
    /// delete requests.
    /// </summary>
    public void DeleteRequest()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "DEL"));
        alParemeters.Add(new SqlParameter("@RequestId", iRequestId));

        ExecuteNonQuery("HRspVisaRequest", alParemeters);

    }
    /// <summary>
    /// Return Requested by ID
    /// </summary>
    /// <returns></returns>
    public int GetRequestedById()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GBI"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        return ExecuteScalar("HRspVisaRequest", alParameters).ToInt32();
    }

    /// <summary>
    /// Get Requested By
    /// </summary>
    /// <returns></returns>
    public string GetRequestedBy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRB"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspVisaRequest", alParameters));
    }

    /// <summary>
    /// Get employee name by employeeID
    /// </summary>
    /// <param name="iEmployeeId"></param>
    /// <returns></returns>
    public string GetEmployeeNameByEmployeeID(int iEmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GN"));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        return Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
    }

    /// <summary>
    /// Get email IDs of employees to whom visa request is sent 
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmailIds()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        return ExecuteDataTable("HRspVisaRequest", alParameters);
    }

    /// <summary>
    /// Get requested employees
    /// </summary>
    /// <returns></returns>
    public string GetRequestedEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return Convert.ToString(ExecuteScalar("HRspVisaRequest", alParameters));
    }

    /// <summary>
    /// Gets visa approval details 
    /// </summary>
    /// <returns></returns>
    public DataTable GetApprovalDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return ExecuteDataTable("HRspVisaRequest", alParameters);
    }

    /// <summary>
    /// check this request is approved.
    /// </summary>
    public bool IsVisaRequestApproved()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CEE"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));

        return (Convert.ToInt32(ExecuteScalar("HRspVisaRequest", alParameters)) > 0 ? true : false);
    }


    public void UpdateCancelStatus()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UC"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));

        ExecuteNonQuery("HRspVisaRequest", alParameters);
    }


    public void VisaRequestCancelled()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@RequestId", iRequestId));
        alParameters.Add(new SqlParameter("@StatusId", iStatusId));
        alParameters.Add(new SqlParameter("@EmployeeId", iEmployeeId));

        ExecuteNonQuery("HRspVisaRequest", alParameters);
    }
    public DataTable FillComboVisaType()
    {
        ArrayList alParemeters = new ArrayList();
        alParemeters.Add(new SqlParameter("@Mode", "GVD"));
        return ExecuteDataTable("HrSpBindCombo", alParemeters);
    }
}
