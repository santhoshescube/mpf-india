﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsQuestionposting
/// purpose : Handle Question posting
/// created : Lekshmi
/// Date    : 07.12.2010
/// </summary>
public class clsQuestionPosting:DL 
{
	public clsQuestionPosting()
	{

    }

    public DataTable FillAllVacancies()                                // Select All Vacancies from Vacancy type reference 
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAV"));
        return ExecuteDataTable("HRspQuestionPosting", arraylst);
    }



}
