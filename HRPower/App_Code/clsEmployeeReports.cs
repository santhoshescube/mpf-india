﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;


/// <summary>
/// 
/// </summary>
public class clsEmployeeReports : DL
{
    #region Variables

    private int iEmployeeID;
    private int iProjectID;
    private int iCompanyiD;
    private int iYear, iMonth;
    private DateTime dtSearchDate, dtNullDate = Convert.ToDateTime("1/1/1900"), dtFinYearFrom, dtFinYearTo;
    private string sSearchType;


    #endregion

    #region Properties

    public int EmployeeID { get { return iEmployeeID; }  set { iEmployeeID = value; } }
    public int ProjectID { get { return iProjectID; } set { iProjectID = value; } }
    public int CompanyID { get { return iCompanyiD; } set { iCompanyiD = value; } }
    public int Year { get { return iYear; } set { iYear = value; } }
    public int Month { get { return iMonth; } set { iMonth = value; } }
    public DateTime SearchDate { get { return dtSearchDate; } set { dtSearchDate = value; } }
    public DateTime NullDate { get { return dtNullDate; } }
    public string SearchType { get { return sSearchType; } set { sSearchType = value; } }
    public DateTime FinYearFrom { get { return dtFinYearFrom; } set { dtFinYearFrom = value; } }
    public DateTime FinYearTo { get { return dtFinYearTo; } set { dtFinYearTo = value; } }


    #endregion


    #region Constructor
    public clsEmployeeReports() { }
    #endregion

    #region GetAllEmployees
    /// <summary>
    /// Returns all employees. To fill dropdown box
    /// </summary>
    /// <returns></returns>
    public DataTable GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAE"));

        if(iCompanyiD > 0) alParameters.Add(new SqlParameter("@CompanyID", iCompanyiD));

        return ExecuteDataTable("HRspRptEmployee", alParameters);
    }

    public DataTable GetAllEmployees(int CompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAE"));

        if (CompanyID > 0) alParameters.Add(new SqlParameter("@CompanyID", CompanyID));

        return ExecuteDataTable("HRspRptEmployee", alParameters);
    }
    #endregion

    #region GetPaymentHistory
    /// <summary>
    /// Returns Payment history of employees.
    /// </summary>
    /// <returns></returns>
    public DataSet GetPaymentHistory()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EPH"));
 
        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if (iCompanyiD > 0) alParameters.Add(new SqlParameter("@CompanyiD", iCompanyiD));
        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alParameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alParameters.Add(new SqlParameter("@SearchDate", dtSearchDate));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count > 0) ds.Tables[0].TableName = "PaymentHistory";
            ds.Tables.Add(new clsReport().GetReportHeader());
            return ds;
        }

    }
    #endregion

    #region GetEmployeeProjectWorkSheet
    /// <summary>
    /// Returns Project work sheet details of employees
    /// </summary>
    /// <returns></returns>
    public DataSet GetEmployeeProjectWorkSheet()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "EPWS"));

        if (iEmployeeID > 0) alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        if (iCompanyiD > 0) alParameters.Add(new SqlParameter("@CompanyiD", iCompanyiD));

        alParameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alParameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alParameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alParameters.Add(new SqlParameter("@SearchDate", dtSearchDate));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count > 0) ds.Tables[0].TableName = "EmployeeProjectWorkSheet";
            ds.Tables.Add(new clsReport().GetReportHeader());
            return ds;
        }

    }
    #endregion

    #region GetProjectReport
    /// <summary>
    /// Returns Project details
    /// </summary>
    /// <returns></returns>
    public DataSet GetProjectReport()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "PD"));

        if (iProjectID > 0)
            alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        return ExecuteDataSet("HRspRptEmployee", alParameters);
    }
    #endregion

    #region GetSalaryStructureHistoryReport
    public DataSet GetSalaryStructureHistoryReport()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SSHR"));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alparameters.Add(new SqlParameter("@SearchType", sSearchType));

        if (iYear > 0) alparameters.Add(new SqlParameter("@Year", iYear));
        if (iMonth > 0) alparameters.Add(new SqlParameter("@Month", iMonth));
        if (dtSearchDate != dtNullDate) alparameters.Add(new SqlParameter("@SearchDate", dtSearchDate));


        using (DataSet ds = ExecuteDataSet("HRspReports", alparameters))
        {
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "SalaryHistory";
                ds.Tables[1].TableName = "SalaryHistoryDetails";
            }
            ds.Tables.Add(new clsReport().GetReportHeader());
            return ds;
        }
    }
    #endregion

    public DataSet GetSalaryStructure(int CompanyID, int DepartmentID, int EmploymentTypeID)
    {
        ArrayList alParameters = new ArrayList() { 
            new SqlParameter("@Mode", "SS"),
            new SqlParameter("@CompanyID", CompanyID),
            new SqlParameter("@DepartmentID", DepartmentID),
            new SqlParameter("@EmploymentType", EmploymentTypeID)
        };

        using (DataSet ds = this.ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "SalaryStructure";
            }
            ds.Tables.Add(new clsReport().GetReportHeader());
            return ds;
        }
    }

    public DataSet GetPaymentHistory(int CompanyID, PayType payType, int Year, int Month, int EmployeeID, int MonthlyMonth, PayCurrency payCurrency)
    {
        using (DataSet ds = new DataSet())
        {
            DataTable dt = new DataTable();

            // Employee Payment details
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GET_PAY_SLIP_MASTER"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@EmpVenFlag", (int)payType));
            alParameters.Add(new SqlParameter("@Year", Year));
            alParameters.Add(new SqlParameter("@Month", Month));
            alParameters.Add(new SqlParameter("@MonthlyMonth", MonthlyMonth));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            //alParameters.Add(new SqlParameter("@PayID", PayIDs));
            alParameters.Add(new SqlParameter("@CompanyPayFlag", (int)payCurrency));
            alParameters.Add(new SqlParameter("@PayDate", string.Empty));
            dt = this.ExecuteDataTable("HRspRptEmployee", alParameters); 
            //dt = this.ExecuteDataTable("EmployeePaymentSalarySlipMaster", alParameters);
            dt.TableName = "EmployeePaymentSalarySlipMaster";

           
            ds.Tables.Add(dt.Copy());

            if (dt.Rows.Count <= 0)
                return ds;

            dt = new DataTable();
            // Employee Leave details
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GET_LEAVE_DET"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@EmpVenFlag", (int)payType));
            alParameters.Add(new SqlParameter("@Year", Year));
            alParameters.Add(new SqlParameter("@Month", Month));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            dt = this.ExecuteDataTable("HRspRptEmployee", alParameters);
            dt.TableName = "GetLeaveDetailSalarySlip";

            DataTable dt2 = dt.Copy();
            if (dt2.Rows.Count <= 1)
                dt2.Rows.Clear();

            ds.Tables.Add(dt2);

            dt = new DataTable();
            // Employee Work info
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "GET_WORK_INFO"));
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
            alParameters.Add(new SqlParameter("@EmpVenFlag", (int)payType));
            alParameters.Add(new SqlParameter("@Year", Year));
            alParameters.Add(new SqlParameter("@Month", Month));
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            dt = this.ExecuteDataTable("HRspRptEmployee", alParameters);
            //dt = this.ExecuteDataTable("GetSalarySlipWorkInfo", alParameters);
            dt.TableName = "GetSalarySlipWorkInfo";

            ds.Tables.Add(dt.Copy());
            ds.Tables.Add(new clsReport().GetReportHeader());
            dt.Dispose();
            return ds;

        }
    }

    public DataSet GetSalaryStructureHistory(int CompanyID, int DepartmentID, int EmploymentTypeID)
    {
        ArrayList alParameters = new ArrayList() { 
            new SqlParameter("@Mode", "SSH"),
            new SqlParameter("@CompanyID", CompanyID),
            new SqlParameter("@DepartmentID", DepartmentID),
            new SqlParameter("@EmploymentType", EmploymentTypeID)
        };

        using (DataSet ds = this.ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "STSalaryHistory";
            }
            ds.Tables.Add(new clsReport().GetReportHeader());
            return ds;
        }
    }

    #region GetEmploymentTypes
    /// <summary>
    /// Returns all employment types : for filling dropdown 
    /// </summary>
    /// <returns></returns>
    public DataTable GetEmploymentTypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GET"));

        return ExecuteDataTable("HRspRptEmployee", alParameters);
    }
    #endregion

    #region GetDesignations
    /// <summary>
    /// Returns all employment types : for filling dropdown 
    /// </summary>
    /// <returns></returns>
    public DataTable GetDesignations()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GDSG"));

        return ExecuteDataTable("HRspReports", alParameters);
    }
    #endregion

    #region GetDepartments
    /// <summary>
    /// Returns all employment types : for filling dropdown 
    /// </summary>
    /// <returns></returns>
    public DataTable GetDepartments()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GDPT"));

        return ExecuteDataTable("HRspReports", alParameters);
    }
    #endregion

    #region GetWorkStatuses
    /// <summary>
    /// Returns all employment types : for filling dropdown 
    /// </summary>
    /// <returns></returns>
    public DataTable GetWorkStatuses()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GWS"));

        return ExecuteDataTable("HRspReports", alParameters);
    }
    #endregion

    #region GetAbsentReportDetails
    /// <summary>
    /// Returns Absent Report Details
    /// </summary>
    /// <returns></returns>
    public DataSet GetAbsentReportDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ARD"));

        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));
        alParameters.Add(new SqlParameter("@FinYearFrom", dtFinYearFrom));
        alParameters.Add(new SqlParameter("@FinYearTo", dtFinYearTo));

        using (DataSet ds = ExecuteDataSet("HRspRptEmployee", alParameters))
        {
            ds.Tables[0].TableName = "AbsentReportEmployee";
            ds.Tables[1].TableName = "AbsentEmployee";
            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }
    }
    #endregion

    public DataTable GetCompanyPayYears(int CompanyID)
    {
        return this.ExecuteDataTable("HRspRptEmployee", new ArrayList() { 
                    new SqlParameter("@Mode", "GET_PAY_YEARS"),
                    new SqlParameter("@CompanyID", CompanyID)
                });
    }

    public DataTable GetCompanyPayYears(int CompanyID, int EmployeeID)
    {
        return this.ExecuteDataTable("HRspRptEmployee", new ArrayList() { 
                    new SqlParameter("@Mode", "GET_PAY_YRS_EMP"),
                    new SqlParameter("@CompanyID", CompanyID),
                    new SqlParameter("@EmployeeID", EmployeeID)
                });
    }

    public DataTable GetCompanyPayMonths(int CompanyID, int Year)
    {
        return this.ExecuteDataTable("HRspRptEmployee", new ArrayList() { 
                    new SqlParameter("@Mode", "GET_PAY_MONTHS"),
                    new SqlParameter("@CompanyID", CompanyID),
                    new SqlParameter("@Year", Year)
                });
    }

    public DataTable GetCompanyPayMonths(int CompanyID, int Year, int EmployeeID)
    {
        return this.ExecuteDataTable("HRspRptEmployee", new ArrayList() { 
                    new SqlParameter("@Mode", "GET_PAY_MNT_EMP"),
                    new SqlParameter("@CompanyID", CompanyID),
                    new SqlParameter("@EmployeeID", EmployeeID),
                    new SqlParameter("@Year", Year)
                });
    }

    public DataSet GetEmployeeLeaveExtensionReport(int EmployeeID, int LeaveExtensionTypeID, DateTime Fromdate, DateTime ToDate, int CompanyID)
    {
        ArrayList alParameters = new ArrayList() {  
            new SqlParameter("@Mode", "LER"),
            new SqlParameter("@CompanyID", CompanyID)
        };

        if (EmployeeID > 0)
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        if (LeaveExtensionTypeID > 0)
            alParameters.Add(new SqlParameter("@ExtensionTypeID", LeaveExtensionTypeID));

        if (Fromdate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@Fromdate", Fromdate.ToString("dd MMM yyyy")));

        if (ToDate != DateTime.MinValue)
            alParameters.Add(new SqlParameter("@ToDate", ToDate.ToString("dd MMM yyyy")));


        using (DataSet ds = this.ExecuteDataSet("HRspReports", alParameters))
        {
            if (ds.Tables.Count > 0)
                ds.Tables[0].TableName = "LeaveExtensionRequests";

            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }
    }

}
