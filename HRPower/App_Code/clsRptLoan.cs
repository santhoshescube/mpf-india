﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for clsRptLoan
/// </summary>
public class clsRptLoan:DL
{
	public clsRptLoan()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataTable GetSalaryAdvanceOrLoanSummary(int CompanyID, int BranchID, int DepartmentID, int WorkStatusID, int EmployeeID, bool IncludeCompany, DateTime FromDate, DateTime ToDate, int IsSalaryReport)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                        IsSalaryReport == 0?new SqlParameter("@Mode", 1):new SqlParameter("@Mode", 2),
                        new SqlParameter("@CompanyID",CompanyID), 
                        new SqlParameter("@BranchID",BranchID),
                        new SqlParameter("@DepartmentID",DepartmentID),
                       
                        new SqlParameter("@WorkStatusID",WorkStatusID) ,
                        new SqlParameter("@EmployeeID",EmployeeID),
                        new SqlParameter("@IncludeCompany",IncludeCompany),
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate),
                        new SqlParameter("@UserID", new clsUserMaster().GetUserId())
                      
                        };
        return new DataLayer().ExecuteDataTable("spPayRptSalaryAdvanceSummary", sqlParameter);
    }
    public static DataTable GetLoanDetails(int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        List<SqlParameter> sqlParameter = new List<SqlParameter> { 
                        new SqlParameter("@Mode", 3),
                        
                        new SqlParameter("@EmployeeID",EmployeeID),
                        
                        new SqlParameter("@FromDate",FromDate),
                        new SqlParameter("@ToDate",ToDate)
                        
                        };
        return new DataLayer().ExecuteDataTable("spPayRptSalaryAdvanceSummary", sqlParameter);
    }

}
