﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// Summary description for clsPaymentPolicy
/// purpose : Handle PaymentPolicy
/// created : Lekshmi
/// Date    : 04.01.2011
/// </summary>
/// 
public class clsDeductionPolicy : DL
{
    public clsDeductionPolicy()
    { }

    private int iDeductionPolicyId;
    private int iRowNumber;
    private string sPolicyName;
    private int iAdditionID;
    private int iAddDedID;
    private int iParameterId;
    private decimal dAmountLimit;
    private decimal fEmployerPart;
    private decimal fEmployeePart;
    private DateTime sWithEffect;
    private bool bAddition;
    private List<int> lstAdditionParticulars;

    private bool bRateOnly;

    public int DeductionPolicyId
    {
        get { return iDeductionPolicyId; }
        set { iDeductionPolicyId = value; }
    }

    public int RowNumber
    {
        get { return iRowNumber; }
        set { iRowNumber = value; }
    }

    public string PolicyName
    {
        get { return sPolicyName; }
        set { sPolicyName = value; }
    }

    public bool Addition
    {
        get { return bAddition; }
        set { bAddition = value; }
    }

    public int AdditionID
    {
        get { return iAdditionID; }
        set { iAdditionID = value; }
    }

    public int ParameterId
    {
        get { return iParameterId; }
        set { iParameterId = value; }
    }

    public decimal AmountLimit
    {
        get { return dAmountLimit; }
        set { dAmountLimit = value; }
    }
    public decimal EmployerPart
    {
        get { return fEmployerPart; }
        set { fEmployerPart = value; }
    }

    public decimal EmployeePart
    {
        get { return fEmployeePart; }
        set { fEmployeePart = value; }
    }

    public DateTime WithEffect
    {
        get { return sWithEffect; }
        set { sWithEffect = value; }
    }

    public int AddDedID
    {
        get { return iAddDedID; }
        set { iAddDedID = value; }
    }

    public bool RateOnly
    {
        get { return bRateOnly; }
        set { bRateOnly = value; }
    }


    public List<int> DeductionPolicyDetails
    {
        get { return lstAdditionParticulars; }
        set { lstAdditionParticulars = value; }
    }


    public DataSet GetAllParameters()// Get Parameters 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetAllParameterNames()// Get parameter names 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPN"));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetAllParticulars()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPP"));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetAllAdditionParticulars()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAAP"));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetAllDeductionParticulars()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAD"));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetDeductionPolicyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));

        alParameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));


        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public DataSet GetAllDeductionPolicies(bool blnAddition)    // Getting all deduction policies
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GADP"));

        int intAddition = 0;
        if (blnAddition == true)
            intAddition = 1;

        alParameters.Add(new SqlParameter("@Addition", intAddition));

        return ExecuteDataSet("HRspDeductionPolicy", alParameters);
    }

    public bool InsertDeductionPolicy(bool bInsert)// Insertion into Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();

        if (bInsert)
        {
            alParameters.Add(new SqlParameter("@Mode", "IDPM"));
            alParameters.Add(new SqlParameter("@DeductionPolicyId", 0));
        }
        else
        {
            DeleteDeductionPolicyDetail();
            alParameters.Add(new SqlParameter("@Mode", "UDPM"));
            alParameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));
        }
        alParameters.Add(new SqlParameter("@PolicyName", sPolicyName));

        alParameters.Add(new SqlParameter("@AddDedID", iAddDedID));
        if(iAdditionID >0)
        alParameters.Add(new SqlParameter("@AdditionID", iAdditionID));
        if(iParameterId >0)
        alParameters.Add(new SqlParameter("@ParameterId", iParameterId));
        alParameters.Add(new SqlParameter("@AmountLimit", dAmountLimit));
        alParameters.Add(new SqlParameter("@EmployerPart", fEmployerPart));

        alParameters.Add(new SqlParameter("@EmployeePart", fEmployeePart));
        alParameters.Add(new SqlParameter("@WithEffect", sWithEffect));

        if (bAddition)
            alParameters.Add(new SqlParameter("@Addition", Convert.ToInt16("1")));
        else
            alParameters.Add(new SqlParameter("@Addition", Convert.ToInt16("0")));

        alParameters.Add(new SqlParameter("@RateOnly", bRateOnly));

        object objretValue = 0;
        SqlParameter sqlparamter = new SqlParameter("@RetValue", 0);
        sqlparamter.Direction = ParameterDirection.ReturnValue;
        alParameters.Add(sqlparamter);

        ExecuteNonQuery("HRspDeductionPolicy", alParameters, out objretValue);

        iDeductionPolicyId = objretValue.ToInt32();

        InsertDeductionPolicyDetail();

        return true;
    }

    public bool InsertDeductionPolicyDetail()// Insertion into Deduction policymaster
    {


        ArrayList alParameters;
        foreach (int intAddDedID in lstAdditionParticulars)
        {
            alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "IDPD"));

            alParameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));
            alParameters.Add(new SqlParameter("@AddDedID", intAddDedID));

            ExecuteNonQuery("HRspDeductionPolicy", alParameters);
        }
        return true;
    }

    public bool ChckIfDeductionPolicyExists() //
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPE"));

        alParameters.Add(new SqlParameter("@DeductionPolicyID", iDeductionPolicyId));

        return ((Convert.ToInt32(ExecuteScalar("HRspDeductionPolicy", alParameters)) == 1 ? true : false));
       
    }

    public bool DeleteDeductionPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "DDPD"));

        alParameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));

        ExecuteNonQuery("HRspDeductionPolicy", alParameters);
        return true;
    }

    public DataTable GetDedIDS()// 
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "GPD"));

        alParameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));

        return ExecuteDataTable("HRspDeductionPolicy", alParameters);
    }

    public bool DeleteDeductionPolicy()                                                // Deletion Of Project Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));

        try
        {
            ExecuteNonQuery("HRspDeductionPolicy", arraylst);
            return true;
        }
        catch (Exception ex)
        {

        }
        return false;
    }

    public bool GetDeductionPolicy(bool blnAddition)    // Getting deduction policy details
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDPD"));
        alParameters.Add(new SqlParameter("@RowNumber", RowNumber));
        alParameters.Add(new SqlParameter("@DeductionPolicyID", DeductionPolicyId));

        int intAddition = 0;
        if (blnAddition)
            intAddition = 1;

        alParameters.Add(new SqlParameter("@Addition", intAddition));

        DataSet ds = ExecuteDataSet("HRspDeductionPolicy", alParameters);

        if (ds.Tables.Count >= 0 && ds.Tables[0].Rows.Count > 0)
        {
            DeductionPolicyId = ds.Tables[0].Rows[0]["DeductionPolicyID"].ToInt32();
            RowNumber = ds.Tables[0].Rows[0]["RowNumber"].ToInt32();
            PolicyName = ds.Tables[0].Rows[0]["PolicyName"].ToString();
            AddDedID = ds.Tables[0].Rows[0]["AddDedID"].ToInt32();
            AdditionID = ds.Tables[0].Rows[0]["AdditionID"].ToInt32();
            ParameterId = ds.Tables[0].Rows[0]["ParameterID"].ToInt32();
            AmountLimit = ds.Tables[0].Rows[0]["AmountLimit"].ToDecimal();
            EmployerPart = ds.Tables[0].Rows[0]["EmployerPart"].ToDecimal();
            EmployeePart = ds.Tables[0].Rows[0]["EmployeePart"].ToDecimal();
            WithEffect = ds.Tables[0].Rows[0]["WithEffect"].ToDateTime();
            RateOnly = ds.Tables[0].Rows[0]["RateOnly"].ToInt32() == 1?true : false;
        }
        if (ds.Tables.Count >= 1)
        {
            lstAdditionParticulars = new List<int>();
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                lstAdditionParticulars.Add(dr["AddDedID"].ToInt32());
            }
        }

        return true;
    }
}
