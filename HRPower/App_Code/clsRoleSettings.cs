﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsRoleSettings
/// </summary>
public class clsRoleSettings : DL
{
    public enum _MENU
    {
        /*Company*/
        Add_Company,
        View_Company,
        Delete_Company,
        Print_Company,
        Email_Company,
        Manage_All,
        View_ShiftPolicy,
        View_WorkPolicy,
        View_LeavePolicy,


        /*Employee*/
        Add_Employee,
        View_Employee,
        Delete_Employee,
        Print_Employee,
        Email_Employee,
        View_Documents,

        /*Employee Salary Structure*/
        Add_Salarystructure,
        View_Salarystructure,
        Delete_Salarystructure,
        Print_Salarystructure,
        Email_Salarystructure,

        /*Employee Passport*/
        Add_Passport,
        View_Passport,
        Delete_Passport,
        Print_Passport,
        Email_Passport,

        /*Employee Visa*/
        Add_Visa,
        View_Visa,
        Delete_Visa,
        Print_Visa,
        Email_Visa,

        /*Employee Otherdocument*/
        Add_OtherDocument,
        View_OtherDocument,
        Delete_OtherDocument,
        Print_OtherDocument,
        Email_OtherDocument,


        /*Vendor*/
        Add_Vendor,
        View_Vendor,
        Delete_Vendor,
        PrintEmail_Vendor,

        /*Bank*/
        Add_Bank,
        View_Bank,
        Delete_Bank,
        Print_Bank,
        Email_Bank,

        /*Project*/
        Add_Project,
        View_Project,
        Delete_Project,
        Print_Project,
        Email_Project,


        /*Perforance Initiation*/
        Add_Initiation,
        View_Initiation,

        /*Performance Evaluation*/
        Add_Evaluation,
        View_Evaluation,
        Asssess_Evaluation,

        /*Performance Action*/
        Add_Action,
        View_Action,

      
        /*Candidate*/
        Add_Candidate,
        View_Candidate,
        Delete_Candidate,
        Print_Candidate,
        Email_Candidate,
        Associate_Candidate,
        Hot_Candidate,


     

        /*Job Posting*/
        CanApprove,
        Add_JobOpening,
        Delete_JobOpening,
        Email_Vacancy,
        Print_Vacancy,
        Hot_Vacancy,
        View_JobOpening,
      

        /*Home Page*/
        Hire_Candidate,
        Make_AsEmployee,
        HomeNewHires,
        Manager_View,
        Active_Vacancies,

        /*OfferLetter*/
        Add_offer,
        View_Offer,
        Update_offer,
        Cancel_Offer,
        Print_Offer,
    

        /*Reports*/
        JobVacancy_Report,
        Candidate_Report,
        Interview_Report,
        InterviewProcess_Report,

        Company_Report,
        Projects_Report,
        ProjectWorksheet_Report,

        Employee_Report,
        SalarystructureHistory_Report,
        PaymentHistory_Report,
        LeaveRequest_Report,
        LoanRequest_Report,
        Absent_Report,
        AttendanceSummary_Report,
        LeaveExtension_Report,
        PaymentSummary_Report,
        LeaveSummary_Report,
        PerformanceEvaluation_Report,
        RecentActivity_Report,
        Salary_Structure,
        Employee_Separation,

        /*Settings*/

        Configuration,
        RoleSettings,
        Calender,
        User_Settings,
        Mail_Settings,

        announcements,

        Add_PerformanceQuestion,
        View_PerformanceQuestion,
        Templates,
        Heirarchy,
        RequestSettings,

        /*Benefits*/

        AddEdit_Benefit,
        Delete_Benefit,
        PrintEmail_Benefits,
        View_Benefits,

        /*Employee Separation*/
        AddEdit_Separation,
        View_Separation,
        Cancel_Separation,
        Print_Separation,
        Email_Separation,

        /*Employee Take Over*/
        AddEdit_Takeover,
        View_Takeover,
        Delete_Takeover,
        Print_TakeOver,
        Email_TakeOver,

        /*Employee Handover*/
        AddEdit_Handover,
        View_Handover,
        Print_HanOver,
        Email_HandOver,

        /*Employee Health Card*/
        AddEdit_HealthCard,
        View_HealthCard,
        Print_HealthCard,
        Email_HealthCard,
        Delete_HealthCard,

        /*Employee Labour Card*/
        AddEdit_LabourCard,
        View_LabourCard,
        Print_LabourCard,
        Email_LabourCard,
        Delete_LabourCard,

        /*Employee Emirites Card*/
        AddEdit_EmiritesCard,
        View_EmiritesCard,
        Print_EmiritesCard,
        Email_EmiritesCard,
        Delete_EmiritesCard





    }

    public clsRoleSettings()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private int iRoleID;
    private int iModuleId;
    private int iMenuID;

    private bool bIsView;
    private bool bIsCreate;
    private bool bIsUpdate;
    private bool bIsDelete;
    private bool bIsPrintEmail;
    private bool bIsEnabled;

    private int iProductId;
    private string sModuleIDList;

    public int RoleID
    {
        get { return iRoleID; }
        set { iRoleID = value; }
    }
    public int ModuleId
    {
        get { return iModuleId; }
        set { iModuleId = value; }
    }

    public int MenuID
    {
        get { return iMenuID; }
        set { iMenuID = value; }
    }
    public bool IsEnabled
    {
        get { return bIsEnabled; }
        set { bIsEnabled = value; }
    }
    public bool IsView
    {
        get { return bIsView; }
        set { bIsView = value; }
    }
    public bool IsCreate
    {
        get { return bIsCreate; }
        set { bIsCreate = value; }
    }
    public bool IsUpdate
    {
        get { return bIsUpdate; }
        set { bIsUpdate = value; }
    }
    public bool IsDelete
    {
        get { return bIsDelete; }
        set { bIsDelete = value; }
    }
    public bool IsPrintEmail
    {
        get { return bIsPrintEmail; }
        set { bIsPrintEmail = value; }
    }
    public int ProductId
    {
        get { return iProductId; }
        set { iProductId = value; }
    }
    public string ModuleIDList
    {
        get { return sModuleIDList; }
        set { sModuleIDList = value; }
    }

    public bool InsertRoleDetails()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@RoleID", iRoleID));
        alparameters.Add(new SqlParameter("@ModuleId", iModuleId));
        alparameters.Add(new SqlParameter("@MenuID", iMenuID));

        alparameters.Add(new SqlParameter("@IsEnabled", bIsEnabled));
        alparameters.Add(new SqlParameter("@IsCreate", bIsCreate));
        alparameters.Add(new SqlParameter("@IsDelete", bIsDelete));
        alparameters.Add(new SqlParameter("@IsPrintEmail", bIsPrintEmail));
        alparameters.Add(new SqlParameter("@IsUpdate", bIsUpdate));
        alparameters.Add(new SqlParameter("@IsView", bIsView));

        alparameters.Add(new SqlParameter("@ProductId", 3));
        int count = Convert.ToInt32(ExecuteScalar("HRspRoleDetails", alparameters));
        if (count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public DataSet FillDataSet()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspRoleDetails", alparameters);
    }
    public bool CheckEnabled()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "G"));
        alparameters.Add(new SqlParameter("@RoleID", iRoleID));
        alparameters.Add(new SqlParameter("@MenuID", iMenuID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        int count = Convert.ToInt32(ExecuteScalar("HRspRoleDetails", alparameters));
        if (count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public DataSet FillDropdown()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "F"));
        return ExecuteDataSet("HRspRoleDetails", alparameters);
    }
    public bool DeleteRoleDetails()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@RoleID", iRoleID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        //alparameters.Add(new SqlParameter("@MenuID", iMenuID));
        //alparameters.Add(new SqlParameter("@ModuleId", iModuleId));
       // alparameters.Add(new SqlParameter("@ProductId", 3));
        int count = Convert.ToInt32(ExecuteScalar("HRspRoleDetails", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }
    public DataSet GetAllModules()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspRoleDetails", alparameters);
    }
    public DataSet GetAllMenusOfModules()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GM"));
        alparameters.Add(new SqlParameter("ModuleIDList", sModuleIDList));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspRoleDetails", alparameters);
    }
    public DataSet GetAllEnability()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "SME"));
        alparameters.Add(new SqlParameter("@ModuleId", iModuleId));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alparameters.Add(new SqlParameter("@RoleID", iRoleID));

        return ExecuteDataSet("HRspRoleDetails", alparameters);
    }
    public bool IsMenuEnabled(int iRoleId, _MENU Menu)
    {
        int iMenuID = Convert.ToInt32(ExecuteScalar("select MenuID from MenuMaster where FormName = '" + Menu.ToString() + "'"));

        if (iRoleId <= 3) return true;

        int count = Convert.ToInt32(ExecuteScalar("select count(*) from RoleDetails where RoleID = " + iRoleId + " and MenuID = " + iMenuID + ""));

        if (count > 0)
            return true;
        else
            return false;
    }

    public bool IsMenuEnabled(int iRoleId, int MenuID)
    {
       

        if (iRoleId <= 3) return true;

        int count = Convert.ToInt32(ExecuteScalar("select count(*) from RoleDetails where RoleID = " + iRoleId + " and MenuID = " + MenuID + " AND (IsView=1 OR IsCreate=1 OR IsUpdate=1)"));

        if (count > 0)
            return true;
        else
            return false;
    }

    public bool IsMenuEnabled(int iRoleId, int MenuID,string sFieldname)
    {
        if (iRoleId <= 3) return true;

        int count = Convert.ToInt32(ExecuteScalar("select count(*) from RoleDetails where RoleID = " + iRoleId + " and MenuID = " + MenuID + " AND " + sFieldname + " = 1"));

        if (count > 0)
            return true;
        else
            return false;
    }
    public bool IsMenuEnabledForInterviewer(int iRoleId, int MenuID, string sFieldname)
    {
        if (iRoleId <= 4) return true;

        int count = Convert.ToInt32(ExecuteScalar("select count(*) from RoleDetails where RoleID = " + iRoleId + " and MenuID = " + MenuID + " AND " + sFieldname + " = 1"));

        if (count > 0)
            return true;
        else
            return false;
    }

    public DataTable GetPermissions(int iRoleId, int IntMenuID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "PER"));
        alparameters.Add(new SqlParameter("@MenuID", IntMenuID));
        alparameters.Add(new SqlParameter("@RoleID", iRoleId));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspRoleDetails", alparameters); ;
    }




    public static  DataTable GetCompanyWisePermission(int CompanyID,int UserID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GCWP"));
        alparameters.Add(new SqlParameter("@MenuID", (int)eMenuID.Employee));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@UserID", UserID));
        return new DataLayer().ExecuteDataTable("HRspRoleDetails", alparameters); ;
    }


    public DataTable  GetModulePermissions(int iRoleId,int ModuleID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GP"));
        alparameters.Add(new SqlParameter("@RoleID", iRoleId));
        alparameters.Add(new SqlParameter("@ModuleID", ModuleID));
        return ExecuteDataTable("HRspHomePage", alparameters); ;
    }

}
