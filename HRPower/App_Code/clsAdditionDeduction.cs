﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;

/// <summary>
/// Summary description for clsAdditionDeduction
/// </summary>
public class clsAdditionDeduction:DL
{
	public clsAdditionDeduction()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iAddDedID;
    private string sDescription;
    private string sDescriptionArb;
    private int iAddition;
    private bool bPredefined;
    private int iReferenceID;
    private int iDeductionPolicyId;
    private bool bPredefinedLocal;

    public int AddDedID
    {
        get { return iAddDedID; }
        set { iAddDedID = value; }
    }
    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string DescriptionArb
    {
        get { return sDescriptionArb; }
        set { sDescriptionArb = value; }
    }
    public int Addition
    {
        get { return iAddition; }
        set { iAddition = value; }
    }
    public bool Predefined
    {
        get { return bPredefined; }
        set { bPredefined = value; }
    }
    public int ReferenceID
    {
        get { return iReferenceID; }
        set { iReferenceID = value; }
    }
    public int DeductionPolicyId
    {
        get { return iDeductionPolicyId; }
        set { iDeductionPolicyId = value; }
    }
    public bool PredefinedLocal
    {
        get { return bPredefinedLocal; }
        set { bPredefinedLocal = value; }
    }

    public int InsertAdditionDeductionDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        alparameters.Add(new SqlParameter("@Addition", iAddition));
        alparameters.Add(new SqlParameter("@Predefined", "False"));
        alparameters.Add(new SqlParameter("@ReferenceID", DBNull.Value));

        if(iDeductionPolicyId==-1)
            alparameters.Add(new SqlParameter("@DeductionPolicyId", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));

        alparameters.Add(new SqlParameter("@PredefinedLocal", DBNull.Value));

        return Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
    }

    public int UpdateAdditionDeductionDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        alparameters.Add(new SqlParameter("@Addition", iAddition));

        if (iDeductionPolicyId == -1)
            alparameters.Add(new SqlParameter("@DeductionPolicyId", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@DeductionPolicyId", iDeductionPolicyId));

        return Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
    }

    public DataSet FillDeductionPolicy()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DP"));

        return ExecuteDataSet("HRspAdditionDeductionReference",alparameters);
    }

    public DataSet BindAdditionDeductions()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));

        return ExecuteDataSet("HRspAdditionDeductionReference", alparameters);
    }

    public DataTable BindSelectedAdditionDeduction()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GS"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        return ExecuteDataTable("HRspAdditionDeductionReference", alparameters);
    }

    public bool CheckAdditionDeductionExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CE"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        int count = Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public bool CheckUpdation()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CU"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        int count = Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public bool CheckDeletionUpdation()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CD"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

         int count = Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

    public bool Delete()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "D"));
            alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

            ExecuteNonQuery("HRspAdditionDeductionReference", alparameters);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public bool CheckParticularExists()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ADE"));
        alparameters.Add(new SqlParameter("@AddDedID", iAddDedID));

        int count = Convert.ToInt32(ExecuteScalar("HRspAdditionDeductionReference", alparameters));
        if (count == 1)
            return true;
        else
            return false;
    }

}
