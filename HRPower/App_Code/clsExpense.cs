﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;


/// <summary>
/// Summary description for clsExpense
/// Created By:    Sanju
/// Created Date:  19-Sept-2013
/// Description:   Expense
/// </summary>
public class clsExpense : DL
{
    public Int32 intExpenseRequestID { get; set; }
    public Int64 lngEmployeeID { get; set; }
    public Int64 CompanyID { get; set; }
    public Int32 intExpenseTypeID { get; set; }
    public DateTime dteFromDate { get; set; }
    public DateTime dteToDate { get; set; }
    public DateTime dteExpenseDate { get; set; }
    public decimal decAmount { get; set; }
    public string strRemarks { get; set; }
    public string strRequestTo { get; set; }
    public Int32 intAccountabilityType { get; set; }
    public decimal decActualAmount { get; set; }
    public decimal intStatusID { get; set; }
    public Int32 intPageIndex { get; set; }
    public Int32 intPageSize { get; set; }
    
    public clsExpense()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable getExpenseType()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable getExpenseForwardStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable getExpenseApproveStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable saveExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", dteFromDate));
        alParameters.Add(new SqlParameter("@ToDate", dteToDate));
        alParameters.Add(new SqlParameter("@Amount", decAmount));
        alParameters.Add(new SqlParameter("@RequestedTo", strRequestTo));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable ApproveExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@ApprovedBy", lngEmployeeID));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
        alParameters.Add(new SqlParameter("@ExpenseDate", dteExpenseDate));
        alParameters.Add(new SqlParameter("@Amount", decAmount));
        alParameters.Add(new SqlParameter("@AccountabilityType", intAccountabilityType));
        alParameters.Add(new SqlParameter("@ActualAmount", decActualAmount));
        alParameters.Add(new SqlParameter("@StatusID", intStatusID));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable CancelExpenseRequest()
    {
        //cancel a expense request after request for cancel
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@ApprovedBy", lngEmployeeID));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
        alParameters.Add(new SqlParameter("@StatusID", intStatusID));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable ForwardExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
        alParameters.Add(new SqlParameter("@StatusID", intStatusID));
        alParameters.Add(new SqlParameter("@ApprovedBy", lngEmployeeID));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataTable updateExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", intExpenseTypeID));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", dteFromDate));
        alParameters.Add(new SqlParameter("@ToDate", dteToDate));
        alParameters.Add(new SqlParameter("@RequestedTo", strRequestTo));
        alParameters.Add(new SqlParameter("@Amount", decAmount));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public DataSet  getExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataSet("HRspExpense", alParameters);
    }
    public DataSet getExpenseRequestToBind()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alParameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", intPageSize));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataSet("HRspExpense", alParameters);
    }
    public DataTable getExpenseRequestCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public void requestForCancelExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));       
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        ExecuteNonQuery("HRspExpense", alParameters);
    }
    //public void cancelExpenseRequest()
    //{
    //    //cancel
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@Mode", 6));
    //    alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
    //    ExecuteNonQuery("HRspExpense", alParameters);
    //}
    public bool isExpenseDeletePossible()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        return (ExecuteScalar("HRspExpense", alParameters).ToInt32() > 0 ? true : false);
    }
    public void DeleteExpenseRequest()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        alParameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        ExecuteNonQuery("HRspExpense", alParameters);
    }
    public DataTable getExpenseRequestSettings()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        return ExecuteDataTable("HRspExpense", alParameters);
    }
    public bool isSalaryExist(DateTime dteCurrentExpenseDate, Int64 CurrentRequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@ExpenseDate", dteCurrentExpenseDate));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", CurrentRequestID));
        DataTable datDateExist = ExecuteDataTable("HRspExpense", alParameters);
        if (datDateExist.Rows.Count > 0)
        {
            return datDateExist.Rows[0]["ID"].ToInt32() > 0 ? true : false;
        }
        return true;
    }
    public bool isSalaryExistToCancel(Int64 CurrentRequestID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 17));
        alParameters.Add(new SqlParameter("@ExpenseRequestID", CurrentRequestID));
        DataTable datDateExist = ExecuteDataTable("HRspExpense", alParameters);
        if (datDateExist.Rows.Count > 0)
        {
            return datDateExist.Rows[0]["ID"].ToInt32() > 0 ? true : false;
        }
        return true;
    }
    public bool isSalaryStructureNotExist()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 18));
        alParameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        DataTable datDateExist = ExecuteDataTable("HRspExpense", alParameters);
        if (datDateExist.Rows.Count > 0)
        {
            return datDateExist.Rows[0]["ID"].ToInt32() > 0 ? false : true;
        }
        return true;
    }
    public bool isEmployeeNotInService(Int64 lngRequesterID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 19));
        alParameters.Add(new SqlParameter("@EmployeeID", lngRequesterID));
        DataTable datDateExist = ExecuteDataTable("HRspExpense", alParameters);
        if (datDateExist.Rows.Count > 0)
        {
            return datDateExist.Rows[0]["ID"].ToInt32() > 0 ? false : true;
        }
        return true;
    }
    public void DeleteExpenseAttachedDocuments( string FileName)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 21));

        if (FileName != string.Empty)
            parameters.Add(new SqlParameter("@FileName", FileName));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        ExecuteScalar("HRspExpense", parameters);
    }

    public void SaveExpenseDocuments(string FileName, string DocumentName)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 20));
        parameters.Add(new SqlParameter("@FileName", FileName));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        parameters.Add(new SqlParameter("@DocumentName", DocumentName));
        ExecuteScalar("HRspExpense", parameters);
    }

    public void UpdateExpenseDocumentsToTreemaster(string fileName ,string docDescription)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 22));
        parameters.Add(new SqlParameter("@FileName", fileName));
        parameters.Add(new SqlParameter("@DocumentName", docDescription));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        parameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        ExecuteScalar("HRspExpense", parameters);
    }

    public DataTable DisplayReasons(int iRequestID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 23));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@FormReferenceID", (int)essReferenceTypes.Expense));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspExpense", parameters);
    }
    public DataTable GetApprovalHierarchy()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 24));      
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspExpense", parameters);
    }
    public int GetCompanyID()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 25));
        parameters.Add(new SqlParameter("@ExpenseRequestID", intExpenseRequestID));
        
        return Convert.ToInt32(ExecuteScalar("HRspExpense", parameters));
    }
}