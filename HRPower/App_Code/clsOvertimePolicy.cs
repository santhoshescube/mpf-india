﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsOvertimePolicy
/// </summary>
public class clsOvertimePolicy:DL
{
	public clsOvertimePolicy()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int iOTPolicyID;
    public string sDescription;
    public int iCalculationBasedID;
    public double fTotalWorkingHours;
    public bool bRateOnly;
    public bool bCompanyBasedOn;
    public decimal dOTRate;
    public double fCalculationPercentage;
    public int iOTPolicyDetailsID;
    public string sHourSlot;
    public int iHourSlotValue;

    public int OTPolicyID { get { return iOTPolicyID; } set { iOTPolicyID = value; } }
    public string Description { get { return sDescription; } set { sDescription = value; } }
    public int CalculationBasedID { get { return iCalculationBasedID; } set { iCalculationBasedID = value; } }
    public double TotalWorkingHours { get { return fTotalWorkingHours; } set { fTotalWorkingHours = value; } }
    public bool RateOnly { get { return bRateOnly; } set { bRateOnly = value; } }
    public bool CompanyBasedOn { get { return bCompanyBasedOn; } set { bCompanyBasedOn = value; } }
    public decimal OTRate { get { return dOTRate; } set { dOTRate = value; } }
    public double CalculationPercentage { get { return fCalculationPercentage; } set { fCalculationPercentage = value; } }
    public int OTPolicyDetailsID { get { return iOTPolicyDetailsID; } set { iOTPolicyDetailsID = value; } }
    public string HourSlot { get { return sHourSlot; } set { sHourSlot = value; } }
    public int HourSlotValue { get { return iHourSlotValue; } set { iHourSlotValue = value; } }
    public int SalPolicyID { get; set; }
    public int AddDedID { get; set; }
    public int PolicyType { get; set; }

    public int InsertOTPolicyMaster()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        if(iCalculationBasedID==-1)
            alparameters.Add(new SqlParameter("@CalculationBasedID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@CalculationBasedID", iCalculationBasedID));
        if(fTotalWorkingHours==0.0)
            alparameters.Add(new SqlParameter("@TotalWorkingHours", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@TotalWorkingHours", fTotalWorkingHours));
        alparameters.Add(new SqlParameter("@RateOnly", bRateOnly));
        alparameters.Add(new SqlParameter("@CompanyBasedOn", bCompanyBasedOn));
        if(dOTRate==0)
            alparameters.Add(new SqlParameter("@OTRate", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@OTRate", dOTRate));
        if(fCalculationPercentage==0.0)
            alparameters.Add(new SqlParameter("@CalculationPercentage", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@CalculationPercentage", fCalculationPercentage));

        return Convert.ToInt32(ExecuteScalar("HRspOTPolicyMaster", alparameters));
    }

    public int InsertOTPolicyDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ID"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));
        alparameters.Add(new SqlParameter("HourSlot", sHourSlot));
        alparameters.Add(new SqlParameter("HourSlotValue", iHourSlotValue));

        return Convert.ToInt32(ExecuteScalar("HRspOTPolicyMaster", alparameters));
    }

    public int UpdateOTPolicyMaster()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        if (iCalculationBasedID == -1)
            alparameters.Add(new SqlParameter("@CalculationBasedID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@CalculationBasedID", iCalculationBasedID));
        if (fTotalWorkingHours == 0.0)
            alparameters.Add(new SqlParameter("@TotalWorkingHours", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@TotalWorkingHours", fTotalWorkingHours));
        alparameters.Add(new SqlParameter("@RateOnly", bRateOnly));
        alparameters.Add(new SqlParameter("@CompanyBasedOn", bCompanyBasedOn));
        if (dOTRate == 0)
            alparameters.Add(new SqlParameter("@OTRate", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@OTRate", dOTRate));

        if (fCalculationPercentage == 0.0)
            alparameters.Add(new SqlParameter("@CalculationPercentage", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@CalculationPercentage", fCalculationPercentage));

        return Convert.ToInt32(ExecuteScalar("HRspOTPolicyMaster", alparameters));
    }

    public void DeleteOTPolicyDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DP"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));

        ExecuteNonQuery("HRspOTPolicyMaster", alparameters);
    }

    public void Delete()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));

        ExecuteNonQuery("HRspOTPolicyMaster", alparameters);
    }

    public bool PolicyExist()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DE"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));

        return Convert.ToBoolean(ExecuteScalar("HRspOTPolicyMaster", alparameters));

    }

    public DataSet BindCalculationBasedon()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        return ExecuteDataSet("HRspOTPolicyMaster", alparameters);
    }

    public DataSet GetAllPolicy()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GPM"));
        return ExecuteDataSet("HRspOTPolicyMaster", alparameters);
    }

    public int GetCount()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GC"));
        return Convert.ToInt32(ExecuteScalar("HRspOTPolicyMaster", alparameters));
    }

    public DataSet GetOTPolicyDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GPD"));
        alparameters.Add(new SqlParameter("OTPolicyID", iOTPolicyID));

        return ExecuteDataSet("HRspOTPolicyMaster", alparameters);
    }
    public DataSet GetAllParticulars()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GAAP"));
        return ExecuteDataSet("HRspOTPolicyMaster", alparameters);

    }
    public int InsertOTSalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", "ISP"));

        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@AddDedID", AddDedID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return Convert.ToInt32(ExecuteScalar("HRspOTPolicyMaster", alParameters));
    }

    public DataTable GetOTSalaryIDS()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSP"));
        alParameters.Add(new SqlParameter("@SalPolicyID", SalPolicyID));
        alParameters.Add(new SqlParameter("@PolicyType", PolicyType));

        return ExecuteDataTable("HRspOTPolicyMaster", alParameters);
    }



}
