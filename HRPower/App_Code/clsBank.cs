﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for clsBank
/// </summary>
public class clsBank:DL
{
	public clsBank()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iBankNameID;
    private string sDescription;
    private string sBankCode;
    private string sAddress;
    private string sTelephone;
    private string sFax;
    private int iCountryID;
    private string sEmail;
    private string sWebsite;
    private int iBankBranchID;
    private int iAccountID;
    private string iUAEBankCode;
    private string sMode;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private string sSearchKey;

    public int BankNameID
    {
        get { return iBankNameID; }
        set { iBankNameID = value; }
    }
    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string BankCode
    {
        get { return sBankCode; }
        set { sBankCode = value; }
    }
    public string Address
    {
        get { return sAddress; }
        set { sAddress = value; }
    }
    public string Telephone
    {
        get { return sTelephone; }
        set { sTelephone = value; }
    }
    public string Fax
    {
        get { return sFax; }
        set { sFax = value; }
    }
    public int CountryID
    {
        get { return iCountryID; }
        set { iCountryID = value; }
    }
    public string Email
    {
        get { return sEmail; }
        set { sEmail = value; }
    }
    public string Website
    {
        get { return sWebsite; }
        set { sWebsite = value; }
    }
    public int BankBranchID
    {
        get { return iBankBranchID; }
        set { iBankBranchID = value; }
    }
    public int AccountID
    {
        get { return iAccountID; }
        set { iAccountID = value; }
    }
    public string UAEBankCode
    {
        get { return iUAEBankCode; }
        set { iUAEBankCode = value; }
    }
    public string Mode
    {
        get { return sMode; }
        set { sMode = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
    public string SearchKey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }

    public int insertBankDetails()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@BankNameId", iBankNameID));
        alparameters.Add(new SqlParameter("@BankCode", sBankCode));
        alparameters.Add(new SqlParameter("@Address", sAddress));
        alparameters.Add(new SqlParameter("@Telephone", sTelephone));
        alparameters.Add(new SqlParameter("@Fax", sFax));

        if (iCountryID == -1)
            alparameters.Add(new SqlParameter("@CountryID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@CountryID", iCountryID));

        alparameters.Add(new SqlParameter("@Email", sEmail));
        alparameters.Add(new SqlParameter("@Website", sWebsite));

        if (iBankBranchID == -1)
            alparameters.Add(new SqlParameter("@BankBranchID", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));

        if(iUAEBankCode == "-1")
            alparameters.Add(new SqlParameter("@UAEBankCode", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@UAEBankCode", iUAEBankCode));

        return Convert.ToInt32(ExecuteScalar("HRspBankNameReference", alparameters));
    }


    /// <summary>
    /// returns data for binding combos
    /// </summary>
    /// <returns></returns>
    public DataSet FillCombos()
    {
        return this.ExecuteDataSet("HRspBankNameReference", new ArrayList { 
            new SqlParameter("@Mode", "FA"),
        });
    }

    public int updateBankDetails(int iBankBranchID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@BankCode", sBankCode));
        alparameters.Add(new SqlParameter("@Address", sAddress));
        alparameters.Add(new SqlParameter("@Telephone", sTelephone));
        alparameters.Add(new SqlParameter("@Fax", sFax));

        if (iCountryID == -1)
            alparameters.Add(new SqlParameter("@CountryID", DBNull.Value));
        else
        alparameters.Add(new SqlParameter("@CountryID", iCountryID));

        alparameters.Add(new SqlParameter("@Email", sEmail));
        alparameters.Add(new SqlParameter("@Website", sWebsite));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));
       
        if (iUAEBankCode == "-1")
            alparameters.Add(new SqlParameter("@UAEBankCode", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@UAEBankCode", iUAEBankCode));

        return Convert.ToInt32(ExecuteScalar("HRspBankNameReference", alparameters));
    }
    public bool checkBankNameDuplication()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "IEB"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));

        int count = Convert.ToInt32(ExecuteScalar("HRspBankNameReference", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }
    public bool checkDuplication(int iBankNameID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "IEB"));
        alparameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));

        int count = Convert.ToInt32(ExecuteScalar("HRspBankNameReference", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }
    public string GetName()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "G"));
        alparameters.Add(new SqlParameter("@BankNameID", iBankNameID));
        return Convert.ToString(ExecuteScalar("HRspBankNameReference", alparameters));

    }
    public void DeleteBankDetails()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankNameID));

        ExecuteNonQuery("HRspBankNameReference", alparameters);

    }
    public DataSet FillDataList()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alparameters.Add(new SqlParameter("@PageSize", PageSize));
        alparameters.Add(new SqlParameter("@SortExpression", SortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", SortOrder));
        alparameters.Add(new SqlParameter("@SearchKey",SearchKey.EscapeUnsafeChars()));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataSet("HRspBankNameReference", alparameters);
    }
    public DataSet BindForm()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankBranchID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspBankNameReference", alparameters);
    }
    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));
        alparameters.Add(new SqlParameter("@SearchKey", SearchKey));

        return Convert.ToInt32(ExecuteScalar("HRspBankNameReference", alparameters));
    }

    public Table PrintBank(string sBankNameIDs)
    {
        Table tblBank = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblBank.ID = "PrintBank";

        if (sBankNameIDs.Contains(","))

            td.Text = CreateSelectedBankContent(sBankNameIDs);
        else
            td.Text = CreateSingleBankContent(Convert.ToInt32(sBankNameIDs));

        tr.Cells.Add(td);
        tblBank.Rows.Add(tr);

        return tblBank;
    }

    public string CreateSingleBankContent(int iBankNameID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "S"));
        alparameters.Add(new SqlParameter("@BankBranchID", iBankNameID));

        DataTable dt = ExecuteDataTable("HRspBankNameReference", alparameters);

        if (dt.Rows.Count > 0)
        {
            sb.Append("<table width='100%' border='0' style='font-family:Tahoma;'>");
            sb.Append("<tr><td><table width='100%' border='0' style='font-family:Tahoma;'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-weight:bold;font-size:15px;' align='center' valign='center' width='475px' ><u>&nbsp;Bank Details&nbsp;</u></td>");
            sb.Append("</tr>");
            sb.Append("</table></td></tr>");
            sb.Append("<tr><td style='padding-left:15px;'>");
            sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
            sb.Append("<tr><td width='120px'>Name</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["BankName"]) + "</td></tr>");
            sb.Append("<tr><td>Branch Name</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["BranchName"]) + "</td></tr>");
            sb.Append("<tr><td>Bank Code</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["BankCode"]) + "</td></tr>");
            sb.Append("<tr><td>Address</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Address"]) + "</td></tr>");
            sb.Append("<tr><td>Telephone</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Telephone"]) + "</td></tr>");
            sb.Append("<tr><td>Fax</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Fax"]) + "</td></tr>");
            sb.Append("<tr><td>Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
            sb.Append("<tr><td>Email</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Email"]) + "</td></tr>");
            sb.Append("<tr><td>Website</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Website"]) + "</td></tr>");
            sb.Append("<tr><td>IBAN Code</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["UAEBankCode"]) + "</td></tr>");
            sb.Append("</table></td></tr>");

            sb.Append("</table>");
        }

        return sb.ToString();
    }

    public string CreateSelectedBankContent(string sBankNameIDs)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "PB"));
        alparameters.Add(new SqlParameter("@BankNameIDs", sBankNameIDs));

        DataTable dt = ExecuteDataTable("HRspBankNameReference", alparameters);

        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:13px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='center'>Bank Details</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma' border='0px' >");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'>Name</td><td width='100px'>Branch Name</td><td width='100px'>Bank Code</td><td width='100px'>Telephone</td><td width='120px'>Email</td><td width='170px'>IBAN Code</td></tr>");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["BankName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["BranchName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["BankBranchCode"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Telephone"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Email"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["UAEBankCode"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();


    }

}
