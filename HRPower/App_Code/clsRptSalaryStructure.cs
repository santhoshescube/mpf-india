﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


   public  class clsRptSalaryStructure
    {
       
        public static DataTable GetSalaryStructureSummary(int CompanyID, int BranchID, int DepartmentID,int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp,int EmployeeID)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@EmployeeID",EmployeeID),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
                  
                     };
            return new  DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }

        public static DataTable GetSalaryStructureHistorySummary(int CompanyID, int BranchID, int DepartmentID, int DesignationID, int WorkStatusID, int EmploymentTypeID, bool IncludeComp, int EmployeeID, bool IsEmployeeHistory)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 2),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@DesignationID", DesignationID) ,
                new SqlParameter("@WorkStatusID", WorkStatusID) ,
                new SqlParameter("@EmploymentTypeID", EmploymentTypeID),
                new SqlParameter("@IsEmployeeHistory", IsEmployeeHistory),
                new SqlParameter("@IncludeComp", IncludeComp),
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()),
                new SqlParameter("@EmployeeID",EmployeeID),
                new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
                  
                     };
            return new DataLayer().ExecuteDataTable("spPayRptSalaryStructure", sqlParameters);
        }


    }


