﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsWorkPolicyReference
/// purpose         : Handle WorkPolicyReference
/// created         : Binoj
/// Date            : 03.09.2010
/// Modified BY     : Laxmi
/// Modified date   : 13.12.2010
/// </summary>

public class clsWorkPolicy : DL
{
    int iPolicyID;
    string sDescription;
    int iCompanyID;
    int iShiftID;
    int iDepartmentID;
    int iDesignationID;
    int iEmploymentTypeID;
    int iProjectID;
    bool bDefaultPolicy;
    bool bActive;

    int iPolicyDetailID;
    int iDayID;
    int iShiftDetailID;

    int iPolicyConsequenceID;
    int iConsequenceID;
    int iLOP;
    int iCasual;
    decimal dAmount;
    int iUserId;
    int iAlloweddaysPerMonth;

    public int PolicyID { get { return iPolicyID; } set { iPolicyID = value; } }
    public string Description { get { return sDescription; } set { sDescription = value; } }
    public int CompanyID { get { return iCompanyID; } set { iCompanyID = value; } }
    public int ShiftID { get { return iShiftID; } set { iShiftID = value; } }
    public int DepartmentID { get { return iDepartmentID; } set { iDepartmentID = value; } }
    public int DesignationID { get { return iDesignationID; } set { iDesignationID = value; } }
    public int EmploymentTypeID { get { return iEmploymentTypeID; } set { iEmploymentTypeID = value; } }
    public int ProjectID { get { return iProjectID; } set { iProjectID = value; } }
    public bool DefaultPolicy { get { return bDefaultPolicy; } set { bDefaultPolicy = value; } }
    public bool Active { get { return bActive; } set { bActive = value; } }

    public int PolicyDetailID { get { return iPolicyDetailID; } set { iPolicyDetailID = value; } }
    public int DayID { get { return iDayID; } set { iDayID = value; } }
    public int ShiftDetailID { get { return iShiftDetailID; } set { iShiftDetailID = value; } }

    public int PolicyConsequenceID { get { return iPolicyConsequenceID; } set { iPolicyConsequenceID = value; } }
    public int ConsequenceID { get { return iConsequenceID; } set { iConsequenceID = value; } }
    public int LOP { get { return iLOP; } set { iLOP = value; } }
    public int Casual { get { return iCasual; } set { iCasual = value; } }
    public decimal Amount { get { return dAmount; } set { dAmount = value; } }
    public int AlloweddaysPerMonth { get { return iAlloweddaysPerMonth; } set { iAlloweddaysPerMonth = value; } }
    public int UserId { get { return iUserId; } set { iUserId = value; } }

    public clsWorkPolicy() { }

    public DataSet GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    public DataSet GetAllShifts()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAS"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }
    public DataTable GetAllWorkShift(int intCompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAS"));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspWorkPolicyReference", alParameters);
    }
    public DataSet GetAllProjects()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAP"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    /// <summary>
    /// Get day types like full day, half day...
    /// </summary>
    public DataSet GetAllDayTypes()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDT"));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    public string GetShiftDuration()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSD"));
        alParameters.Add(new SqlParameter("@ShiftID", iShiftID));

        DataSet ds = ExecuteDataSet("HRspWorkPolicyReference", alParameters);

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                return string.Format("{0} To {1} (Duration : {2})",
                    Convert.ToDateTime(ds.Tables[0].Rows[0]["FromTime"]).ToString("hh:mm tt"),
                    Convert.ToDateTime(ds.Tables[0].Rows[0]["ToTime"]).ToString("hh:mm tt"),
                    Convert.ToDateTime(ds.Tables[0].Rows[0]["Duration"]).ToString("HH:mm"));
            }
        }

        return string.Empty;
    }

    public DataSet GetWeekDays()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GWD"));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    /// <summary>
    /// Gets off day of a company
    /// </summary>
    public int GetOffDay()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GOD"));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters));
    }

    /// <summary>
    /// Get all work policies
    /// </summary>
    /// <returns></returns>
    public DataSet GetAllWorkPolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAWP"));
        alParameters.Add(new SqlParameter("@UserId", iUserId));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    public int InsertPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPM"));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iShiftID != -1) alParameters.Add(new SqlParameter("@ShiftID", iShiftID));
        //if (iDepartmentID != -1) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        //if (iDesignationID != -1) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));
        //if (iEmploymentTypeID != -1) alParameters.Add(new SqlParameter("@EmploymentTypeID", iEmploymentTypeID));
        //if (iProjectID != -1) alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        return Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters));
    }

    public int UpdatePolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UPM"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));
        if (iShiftID != -1) alParameters.Add(new SqlParameter("@ShiftID", iShiftID));
        //if (iDepartmentID != -1) alParameters.Add(new SqlParameter("@DepartmentID", iDepartmentID));
        //if (iDesignationID != -1) alParameters.Add(new SqlParameter("@DesignationID", iDesignationID));
        //if (iEmploymentTypeID != -1) alParameters.Add(new SqlParameter("@EmploymentTypeID", iEmploymentTypeID));
        //if (iProjectID != -1) alParameters.Add(new SqlParameter("@ProjectID", iProjectID));

        return Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters));
    }

    public bool IsAssociated()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPA"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        return (Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters)) > 0 ? true : false);
    }

    public int DeletePolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPM"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        try
        {
            return (Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters)));

        }
        catch (Exception ex)
        {
            return 0;
        }

    }

    public DataSet GetPolicyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPD"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    public DataSet GetPolicyConsequences()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAPC"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        return ExecuteDataSet("HRspWorkPolicyReference", alParameters);
    }

    public void DeletePolicyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        ExecuteNonQuery("HRspWorkPolicyReference", alParameters);
    }

    public void InsertPolicyDetails()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPD"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@DayID", iDayID));
        alParameters.Add(new SqlParameter("@ShiftDetailID", iShiftDetailID));

        ExecuteNonQuery("HRspWorkPolicyReference", alParameters);
    }

    public void InsertPolicyConsequences()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IPC"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@ConsequenceID", iConsequenceID));
        alParameters.Add(new SqlParameter("@AllowedDaysPerMonth", iAlloweddaysPerMonth));
        alParameters.Add(new SqlParameter("@LOP", iLOP));
        alParameters.Add(new SqlParameter("@Casual", iCasual));
        alParameters.Add(new SqlParameter("@Amount", dAmount));

        ExecuteNonQuery("HRspWorkPolicyReference", alParameters);
    }

    public bool HasDefaultPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CHD"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));
        alParameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return (Convert.ToInt32(ExecuteScalar("HRspWorkPolicyReference", alParameters)) > 0 ? true : false);
    }

    public string Print()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GWP"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        DataTable dt = ExecuteDataTable("HRspWorkPolicyReference", alParameters);

        StringBuilder sb = new StringBuilder();

        sb.Append("<h3>Work Policy</h3>");
        sb.Append("<table style='font-family: Verdana; font-size: 11px; line-height: 20px;' width='100%'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold' width='120'>");
        sb.Append("Policy Name</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Description"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Shift</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Shift"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Department</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Department"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Designation</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Designation"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Employment Type</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["EmploymentType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Project</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["ProjectName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("DefaultPolicy</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["DefaultPolicy"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight: bold'>");
        sb.Append("Active Policy</td>");
        sb.Append("<td>");
        sb.Append(dt.Rows[0]["Active"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GWPD"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        dt = ExecuteDataTable("HRspWorkPolicyReference", alParameters);

        sb.Append("<h5>Policy Details</h5>");
        sb.Append("<table border='1' style='font-family: Verdana; font-size: 11px; line-height: 20px; border-collapse: collapse; width: 100%'>");
        sb.Append("<tr style='font-weight: bold'>");
        sb.Append("<td>");
        sb.Append("Day</td>");
        sb.Append("<td align='right' width='80'>");
        sb.Append("Break Duration</td>");
        sb.Append("<td align='right' width='100'>");
        sb.Append("Food Break Time Out</td>");
        sb.Append("<td align='right' width='100'>");
        sb.Append("Food Break Time In</td>");
        sb.Append("<td align='right' width='80'>");
        sb.Append("Variable Time</td>");
        sb.Append("<td align='right' width='100'>");
        sb.Append("Consider Late After</td>");
        sb.Append("<td align='right' width='100'>");
        sb.Append("Leaving Early Before</td>");
        sb.Append("</tr>");

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(dt.Rows[i]["Day"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["TimeAllowed"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["TimeOut"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["TimeIn"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["VariableTime"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["LateAfter"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["EarlyBefore"] + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</table>");

        alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GWPC"));
        alParameters.Add(new SqlParameter("@PolicyID", iPolicyID));

        dt = ExecuteDataTable("HRspWorkPolicyReference", alParameters);

        sb.Append("<h5>Policy Consequences</h5>");
        sb.Append("<table border='1' style='font-family: Verdana; font-size: 11px; line-height: 20px; border-collapse: collapse; width: 100%'>");
        sb.Append("<tr style='font-weight: bold'>");
        sb.Append("<td>");
        sb.Append("Consequence Type</td>");
        sb.Append("<td width='75'>");
        sb.Append("LOP</td>");
        sb.Append("<td width='75'>");
        sb.Append("Casual</td>");
        sb.Append("<td align='right' width='75'>");
        sb.Append("Amount</td>");
        sb.Append("</tr>");

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr>");
            sb.Append("<td>");
            sb.Append(dt.Rows[i]["Type"] + "</td>");
            sb.Append("<td>");
            sb.Append(dt.Rows[i]["LOP"] + "</td>");
            sb.Append("<td>");
            sb.Append(dt.Rows[i]["Casual"] + "</td>");
            sb.Append("<td align='right'>");
            sb.Append(dt.Rows[i]["Amount"] + "</td>");
            sb.Append("</tr>");
        }

        sb.Append("</table>");

        return sb.ToString();
    }

}
