﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsVacationPolicy
/// purpose : Handle Vacation Policy
/// created : Midhun
/// Date    : 09.03.2012
/// </summary>
public class clsVacationPolicy: DL 
{
	public  clsVacationPolicy() {}
	
		//
		// TODO: Add constructor logic here
		//

    public int VacationPolicyID { get; set; }
    public string Description { get; set; }
    public int CalculationBasedID { get; set; }
    public decimal decNoOfTickets { get; set; }
    public bool RateOnly { get; set; }
    public decimal RatePerDay { get; set; }
    public decimal TicketAmount { get; set; }
    public decimal OnTimeRejoinBenefit { get; set; }
    public int RowNumber { get; set; }

    public List<int> lstParticulars { get; set; }

    public List<clsVacationPolicyDetail> lstVacationPolicyDetails { get; set; }


    public DataTable FillAllCalculationBasedOn()                      // Select All calculation based on parameters
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode",17));
        return ExecuteDataTable("spPayVacationPolicy", arraylst);
    }

    public DataTable FillAllParameters()                      // Select All calculation based on parameters
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", 16));
        return ExecuteDataTable("spPayVacationPolicy", arraylst);
    }


    public bool InsertUpdateVacationPolicy(bool bIsInsert)                // Insertion when bIsInsert=true , Updation when bIsInsert=false
    {
        try
        {
           // base.BeginTransaction();
            ArrayList arraylst = new ArrayList();


            if (bIsInsert)
                arraylst.Add(new SqlParameter("@Mode", 2));
            else
            {
                arraylst.Add(new SqlParameter("@Mode", 7));
                arraylst.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));
                DeleteVacationPolicyDetails();
                DeleteVacationSalaryPolicyDetail();
            }

            arraylst.Add(new SqlParameter("@Description", Description));
            arraylst.Add(new SqlParameter("@OnTimeRejoinBenefit", OnTimeRejoinBenefit));
            arraylst.Add(new SqlParameter("@IsRateOnly", RateOnly));
            //if (RateOnly == true)
            //{
                arraylst.Add(new SqlParameter("@RatePerDay", RatePerDay));
                arraylst.Add(new SqlParameter("@NoOfTickets", decNoOfTickets));
                arraylst.Add(new SqlParameter("@TicketAmount", TicketAmount));
            //}
            //else
            //{
                arraylst.Add(new SqlParameter("@CalculationID", CalculationBasedID));
            //}

            object objretValue = 0;

            SqlParameter sQlparam = new SqlParameter("@ReturnVal", SqlDbType.Int);
            sQlparam.Direction = ParameterDirection.ReturnValue;
            arraylst.Add(sQlparam);
           
           // this.ExecuteNonQuery("spPayVacationPolicy", arraylst, out objretValue);

            objretValue = this.ExecuteScalar("spPayVacationPolicy", arraylst);

             VacationPolicyID = Convert.ToInt32(objretValue);
            if (RateOnly == false)
            {
                InsertVacationPolicyDetails();

                InsertVacationSalaryPolicyDetail();

            }
            base.CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            base.RollbackTransaction();
            return false;
        }
    }

    public DataSet GetAllVacationPolicies()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 20));
        return ExecuteDataSet("spPayVacationPolicy", alParameters);
    }

    public bool  InsertVacationPolicyDetails()
    {
        foreach (clsVacationPolicyDetail objDetail in lstVacationPolicyDetails)
        {
            ArrayList arrayLst = new ArrayList();
            arrayLst.Add(new SqlParameter("@Mode", 3));
            arrayLst.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));
            arrayLst.Add(new SqlParameter("@Parameter", objDetail.Parameter));
            arrayLst.Add(new SqlParameter("@NoOfMonths", objDetail.NoOfMonths));
            arrayLst.Add(new SqlParameter("@NoOfDays", objDetail.decNoOfDays));
            arrayLst.Add(new SqlParameter("@NoOfTickets", objDetail.decNoOfTickets));
            arrayLst.Add(new SqlParameter("@TicketAmount", objDetail.TicketAmount));
            ExecuteNonQuery("spPayVacationPolicy", arrayLst);
        }
        return true ;
    }

    public bool  DeleteVacationPolicyDetails()
    {
        try
        {
            //base.BeginTransaction();
            ArrayList arraylst = new ArrayList();

            arraylst.Add(new SqlParameter("@Mode", 5));
            arraylst.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));


            ExecuteNonQuery("spPayVacationPolicy", arraylst);
            base.CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            base.RollbackTransaction();
            return false;
        }

    }

    public bool  DeleteVacationPolicyDetails(int intOrderNo)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", 18));
        arraylst.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));
        arraylst.Add(new SqlParameter("@OrderNo", intOrderNo));

        ExecuteNonQuery("spPayVacationPolicy", arraylst);

        return true;
    }



    public int  DeleteVacationPolicy()                                                // Deletion Of absent Policy
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", 21));
        arraylst.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID ));
        //ExecuteNonQuery ("spPayVacationPolicy", arraylst);
       // return true ;
      //  return (Convert.ToInt32(ExecuteScalar("HRspLeavePolicy", alParameters)));
        return (Convert.ToInt32(ExecuteScalar("spPayVacationPolicy", arraylst)));
    
    }

    public bool  GetVacationPolicyDetails()    // Getting vacation policy details
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@RowNumber", RowNumber));
        alParameters.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));

        DataSet ds = ExecuteDataSet("spPayVacationPolicy", alParameters);

        if (ds.Tables.Count >= 0)
        {
            VacationPolicyID = ds.Tables[0].Rows[0]["VacationPolicyID"].ToInt32();
            RowNumber = ds.Tables[0].Rows[0]["RowNumber"].ToInt32();
            Description = ds.Tables[0].Rows[0]["VacationPolicy"].ToString();
            RateOnly = ds.Tables[0].Rows[0]["IsRateOnly"].ToBoolean();
            RatePerDay = ds.Tables[0].Rows[0]["RatePerDay"].ToDecimal();
            CalculationBasedID = ds.Tables[0].Rows[0]["CalculationID"].ToInt32();
            decNoOfTickets = ds.Tables[0].Rows[0]["NoOfTickets"].ToInt32();
            TicketAmount = ds.Tables[0].Rows[0]["TicketAmount"].ToDecimal();
            OnTimeRejoinBenefit = ds.Tables[0].Rows[0]["OnTimeRejoinBenefit"].ToDecimal();
        }
        if (ds.Tables.Count >= 1)
        {
            lstVacationPolicyDetails = new List<clsVacationPolicyDetail>();
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                clsVacationPolicyDetail objDetail = new clsVacationPolicyDetail();
                objDetail.OrderNo = dr["SerialNo"].ToInt32();
                objDetail.Parameter = dr["ParameterID"].ToInt32();
                objDetail.NoOfMonths = dr["NoOfMonths"].ToInt32();
                objDetail.decNoOfDays = dr["NoOfDays"].ToDecimal();
                objDetail.decNoOfTickets = dr["NoOfTickets"].ToDecimal();
                objDetail.TicketAmount = dr["TicketAmount"].ToDecimal();
               // objDetail.OnTimeRejoinBenefit = dr["OnTimeRejoinBenefit"].ToDecimal();
                lstVacationPolicyDetails.Add(objDetail);
            }
        }
        if (ds.Tables.Count >= 2)
        {
            lstParticulars = new List<int>();
            foreach (DataRow dr in ds.Tables[2].Rows)
            {
                lstParticulars.Add(dr["AdditionDeductionID"].ToInt32());
            }
        }
        return true;
    }
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 19));
        return ExecuteScalar("spPayVacationPolicy", alParameters).ToInt32();
    }
    public DataSet GetAllParticulars()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 14));
        return ExecuteDataSet("spPayVacationPolicy", alParameters);
    }

    public bool  DeleteVacationSalaryPolicyDetail()// delete Deduction policymaster
    {
        ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", 6));

        alParameters.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID  ));

       ExecuteNonQuery("spPayVacationPolicy", alParameters);
       return true;
    }

    public bool  InsertVacationSalaryPolicyDetail()// delete Deduction policymaster
    {

        foreach (int intAddDedID in lstParticulars)
        {
            ArrayList alParameters = new ArrayList();


            alParameters.Add(new SqlParameter("@Mode", 4));

            alParameters.Add(new SqlParameter("@VacationPolicyID", VacationPolicyID));
            alParameters.Add(new SqlParameter("@ParticularID", intAddDedID));

            ExecuteNonQuery("spPayVacationPolicy", alParameters);
        }
        return true;
    }

    public DataTable GetVacationSalaryIDS()
    {
         ArrayList alParameters = new ArrayList();


        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@SalPolicyID", VacationPolicyID  ));
        return ExecuteDataTable("spPayVacationPolicy", alParameters);
    }

    }
public class clsVacationPolicyDetail
{
    public int OrderNo { get; set; }
    public int Parameter { get; set; }
    public int NoOfMonths { get; set; }
    public decimal decNoOfDays { get; set; }
    public decimal decNoOfTickets { get; set; }
    public decimal TicketAmount { get; set; }
    public decimal OnTimeRejoinBenefit { get; set; }
}
