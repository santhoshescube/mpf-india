﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptInterviewProcess
/// </summary>
public class clsRptInterviewProcess
{
	public clsRptInterviewProcess()
	{
	}

    public static DataTable GetInterviewProcessReport(int JobID, long CandidateID, DateTime FromDate, DateTime ToDate)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewProcess", new ArrayList()
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@JobID",JobID),
            new SqlParameter("@CandidateID",CandidateID),
            new SqlParameter("@FromDate",FromDate),
            new SqlParameter("@ToDate",ToDate)
         
        
        });
    }

    public static DataTable GetCandidateProcessDetails(int JobID, long CandidateID, DateTime FromDate, DateTime ToDate)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewProcess", new ArrayList()
        {
            new SqlParameter("@Mode",2),
             new SqlParameter("@JobID",JobID),
            new SqlParameter("@CandidateID",CandidateID),
            new SqlParameter("@FromDate",FromDate),
            new SqlParameter("@ToDate",ToDate),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0),
            new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
        });
    }

    public static DataTable GetCandidates(int JobID)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewProcess", new ArrayList()
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@JobID",JobID),
            new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture())
        });
    }
}
