﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for clsQualification
/// </summary>
public class clsQualification:DL
{
	public clsQualification()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int QualificationID { get; set; }
    public int EmployeeID { get; set; }
    public int CompanyID { get; set; }
    public string ReferenceNumber { get; set; }
    public DateTime FromDate { get; set; }
    public string SchoolOrCollege { get; set; }
    public string CertificateTitle { get; set; }
    public string GradeorPercentage { get; set; }
    public bool CertificatesAttested { get; set; }
    public bool CertificatesVerified { get; set; }
    public bool UniversityClearenceRequired { get; set; }
    public bool ClearenceCompleted { get; set; }
    public DateTime ToDate { get; set; }
    public int DegreeID { get; set; }
    public int UniversityID { get; set; }
    public int UserId { get; set; }
    public string Remarks { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string SearchKey { get; set; }
    public int Node { get; set; }
    public string Docname { get; set; }
    public string Filename { get; set; }
    public void BeginEmp()
    {
        BeginTransaction("HRspQualification");
    }

    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    //-----------------------------------------------------------Employee Qualification------------------------------------------------------------
    public DataSet GetEmpDrivingLicense()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GSS"));
        alparameters.Add(new SqlParameter("@QualificationID", QualificationID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspQualification", alparameters);
    }
    public int GetCount()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEC"));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@UserId", UserId));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return Convert.ToInt32(ExecuteScalar("HRspQualification", alParameters));
    }

    public DataSet GetAllEmployeeDrivingLicenses()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAE"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@SearchKey", SearchKey));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspQualification", alParameters);
    }

    public DataSet GetEmployeeLicense()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));

        return ExecuteDataSet("HRspQualification", alParameters);
    }

    public int InsertEmployeeQualification() //Insert Employee  qualification details
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IH"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber));
        alparameters.Add(new SqlParameter("@CertificateTitle", CertificateTitle));
        alparameters.Add(new SqlParameter("@GradeorPercentage", GradeorPercentage));
        alparameters.Add(new SqlParameter("@CertificatesAttested", CertificatesAttested));
        alparameters.Add(new SqlParameter("@CertificatesVerified", CertificatesVerified));
        alparameters.Add(new SqlParameter("@UniversityClearenceRequired", UniversityClearenceRequired));
        alparameters.Add(new SqlParameter("@ClearenceCompleted", ClearenceCompleted));
        alparameters.Add(new SqlParameter("@FromDate", FromDate));
        alparameters.Add(new SqlParameter("@SchoolOrCollege", SchoolOrCollege));
        if (UniversityID == -1) alparameters.Add(new SqlParameter("@UniversityID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@UniversityID", UniversityID));
        if (DegreeID == -1) alparameters.Add(new SqlParameter("@DegreeID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@DegreeID", DegreeID));
        alparameters.Add(new SqlParameter("@ToDate", ToDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));

        QualificationID = Convert.ToInt32(ExecuteScalar(alparameters));

        return QualificationID;
    }


    public int UpdateEmployeeQualification()  //update  qualification
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UH"));
        alparameters.Add(new SqlParameter("@QualificationID", QualificationID));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber));
        alparameters.Add(new SqlParameter("@CertificateTitle", CertificateTitle));
        alparameters.Add(new SqlParameter("@GradeorPercentage", GradeorPercentage));
        alparameters.Add(new SqlParameter("@CertificatesAttested", CertificatesAttested));
        alparameters.Add(new SqlParameter("@CertificatesVerified", CertificatesVerified));
        alparameters.Add(new SqlParameter("@UniversityClearenceRequired", UniversityClearenceRequired));
        alparameters.Add(new SqlParameter("@ClearenceCompleted", ClearenceCompleted));
        alparameters.Add(new SqlParameter("@FromDate", FromDate));
        alparameters.Add(new SqlParameter("@SchoolOrCollege", SchoolOrCollege));
        if (UniversityID == -1) alparameters.Add(new SqlParameter("@UniversityID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@UniversityID", UniversityID));
        if (DegreeID == -1) alparameters.Add(new SqlParameter("@DegreeID", DBNull.Value));
        else alparameters.Add(new SqlParameter("@DegreeID", DegreeID));
        alparameters.Add(new SqlParameter("@ToDate", ToDate));
        alparameters.Add(new SqlParameter("@Remarks", Remarks));

        QualificationID = Convert.ToInt32(ExecuteScalar(alparameters));

        return QualificationID;
    }
  

    public DataTable FillDegree()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FC"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1:0));
        return ExecuteDataTable("HRspQualification", alparameters);
    }

    public DataTable FillUniversity()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FLT"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspQualification", alparameters);
    }

    public DataTable FillEmployees(long lngEmployeeID)
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FEM"));
        alparameters.Add(new SqlParameter("@EmployeeID", lngEmployeeID));
        alparameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspQualification", alparameters);
    }

    public string GetEmployeeName()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCN"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToString(ExecuteScalar("HRspQualification", alparameters));
    }
    public int GetCompanyId()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GCI"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return Convert.ToInt32(ExecuteScalar("HRspQualification", alparameters));
    }


    public bool IsvisaIDExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CES"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));

        return (Convert.ToInt32(ExecuteScalar("HRspQualification", alParameters)) > 0 ? true : false);
    }

    public void Delete()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DES"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));

        ExecuteNonQuery("HRspQualification", alParameters);
    }

    public string GetPrintText(int iVisaID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@QualificationID", iVisaID));

        DataTable dt = ExecuteDataSet("HRspQualification", alParameters).Tables[0];


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Qualification Information</td></tr>");
        sb.Append("<tr><td width='150px'>Reference Number</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["ReferenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>University</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["University"]) + "</td></tr>");
        sb.Append("<tr><td>School/College</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SchoolOrCollege"]) + "</td></tr>");
        sb.Append("<tr><td>Qualification</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Degree"]) + "</td></tr>");
        sb.Append("<tr><td>From Period</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["FromDate"]) + "</td></tr>");
        sb.Append("<tr><td>To Period</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ToDate"]) + "</td></tr>");
        sb.Append("<tr><td>Certificate Title</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificateTitle"]) + "</td></tr>");
        sb.Append("<tr><td>Grade/Percentage</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["GradeorPercentage"]) + "</td></tr>");
        sb.Append("<tr><td>Certificates Attested?</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesAttested"]) + "</td></tr>");
        sb.Append("<tr><td>Certificates Verified?</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesVerified"]) + "</td></tr>");
        sb.Append("<tr><td>University Clearence Required?</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["UniversityClearenceRequired"]) + "</td></tr>");
        sb.Append("<tr><td>Clearence Completed?</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ClearenceCompleted"]) + "</td></tr>");
        sb.Append("<tr><td>Other Info</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public string CreateSelectedEmployeesContent(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@QualificationIds", sVisaIds));

        DataTable dt = ExecuteDataTable("HRspQualification", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Qualification Information</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Employee</td><td width='90px'>Reference Number</td>");
        sb.Append("<td width='90px'>University</td><td width='90px'>School/College</td><td width='90px'>Qualification</td><td width='90px'>From Period</td><td width='90px'>To Period</td>");
        sb.Append("<td width='90px'>Certificate Title</td><td width='50px'>Grade/Percentage</td><td width='50px'>Certificates Attested?</td>");
        sb.Append("<td width='50px'>Certificates Verified?</td><td width='50px'>University Clearence Required?</td>");
        sb.Append("<td width='50px'>Clearence Completed?</td><td width='200px'>Other Info</td></tr>");
        sb.Append("<tr><td colspan='15'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ReferenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["University"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SchoolOrCollege"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Degree"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["FromDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ToDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificateTitle"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["GradeorPercentage"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesAttested"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesVerified"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["UniversityClearenceRequired"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ClearenceCompleted"]) + "</td>");
            sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }


    public DataTable GetPrintAll(string sVisaIds)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@QualificationIds", sVisaIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return  ExecuteDataTable("HRspQualification", arraylst);
    }

    public DataTable GetPrintSingle(int iVisaID)
    {
        
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@QualificationID", iVisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
       return  ExecuteDataSet("HRspQualification", alParameters).Tables[0];
    }
    public bool IsReferenceNumberExists()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IEC"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));
        alParameters.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber));

        return (Convert.ToInt32(ExecuteScalar("HRspQualification", alParameters)) > 0 ? true : false);
    }
    public DataSet GetQualificationDocuments()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GPD"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));

        return ExecuteDataSet("HRspQualification", alParameters);
    }
    public void DeleteLicenseDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DPD"));
        alParameters.Add(new SqlParameter("@Node", Node));

        ExecuteNonQuery("HRspQualification", alParameters);
    }

    public int InsertEmployeeLicenseTreemaster() //Insert Employee  Driving license treemasterhead
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ITM"));
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@QualificationID", QualificationID));
        alparameters.Add(new SqlParameter("@ReferenceNumber", ReferenceNumber));
        alparameters.Add(new SqlParameter("@Docname", Docname));
        alparameters.Add(new SqlParameter("@Filename", Filename));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

  
    public DataTable GetLicenseFilenames()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GDD"));
        alParameters.Add(new SqlParameter("@QualificationID", QualificationID));

        return ExecuteDataTable("HRspQualification", alParameters);
    }

    public bool CheckIfDocumentIssued(int iQualificationID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDR"));
        alParameters.Add(new SqlParameter("@QualificationID", iQualificationID));

        return ExecuteScalar("HRspQualification", alParameters).ToInt32() > 0;
    }

    public bool CheckIfDegreeExists(int iQualificationID,int iDegreeID,int iEmployeeID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "CDE"));
        alParameters.Add(new SqlParameter("@QualificationID", iQualificationID));
        alParameters.Add(new SqlParameter("@DegreeID", iDegreeID));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        return ExecuteScalar("HRspQualification", alParameters).ToInt32() > 0;
    }
}
