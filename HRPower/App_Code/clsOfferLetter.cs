﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsOfferLetter
/// </summary>
public class clsOfferLetter:DL
{
	public clsOfferLetter(){}

    private int iCandidateId;
    private int iVacancyId;
    private decimal dBasicPay;
    private int iPaymentModeId;
	private string sSentDate ;
	private string sDateofJoining;
	private string sAllowanceName;
	private decimal dAllowanceAmount;
	private bool bAddition;
	private int iOfferId;
    private int iAllowanceTypeId;    
    private int iTemplateId;
    private string sSubject;
    private bool bHasSend;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private int iCompanyId;
    private int iDesignationId;

    public int TemplateId
    {
        get { return iTemplateId; }
        set { iTemplateId = value; }
    }
    public int CandidateStatusId
    {
        get;
        set;
    }

    public int CandidateId
    {
        get { return iCandidateId; }
        set { iCandidateId = value; }
    }
    public int CreatedBy
    {
        get;
        set;
    }
    public int OfferStatus
    {
        get;
        set;
    }
    public int VacancyId
    {
        get { return iVacancyId; }
        set { iVacancyId = value; }
    }

    public decimal BasicPay
    {
        get { return dBasicPay; }
        set { dBasicPay = value; }
    }

    public decimal GrossSalary
    {
        get;
        set;
    }
    public string Remarks
    {
        get;
        set;
    }
    public string SearchKey { get; set; }

    public int PaymentModeId
    {
        get { return iPaymentModeId; }
        set { iPaymentModeId = value; }
    }

    public string SentDate
    {
        get { return sSentDate; }
        set { sSentDate = value; }
    }

    public string DateofJoining
    {
        get { return sDateofJoining; }
        set { sDateofJoining = value; }
    }

    public string Terms
    {
        get;
        set;

    }

    public string AllowanceName
    {
        get { return sAllowanceName; }
        set { sAllowanceName = value; }
    }

    public decimal AllowanceAmount
    {
        get { return dAllowanceAmount; }
        set { dAllowanceAmount = value; }
    }

    public bool Addition
    {
        get { return bAddition; }
        set { bAddition = value; }
    }

    public int OfferId
    {
        get { return iOfferId; }
        set { iOfferId = value; }
    }

    public int AllowanceTypeId
    {
        get { return iAllowanceTypeId; }
        set { iAllowanceTypeId = value; }
    }

    public string Subject
    {
        get { return sSubject; }
        set { sSubject = value; }
    }

    public bool HasSend
    {
        get { return bHasSend; }
        set { bHasSend = value; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public int CompanyId
    {
        get { return iCompanyId; }
        set { iCompanyId = value; }
    }

    public int Designation
    {
        get { return iDesignationId; }
        set { iDesignationId = value; }
    }
    public int AddDedID { get; set; }

    public int InsertCandidateOffer(int iNeedHistory)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "ICO"));
        arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));
        arraylst.Add(new SqlParameter("@GrossSalary", GrossSalary));
        arraylst.Add(new SqlParameter("@Basicpay",dBasicPay ));
        arraylst.Add(new SqlParameter("@PaymentModeId", iPaymentModeId));
        arraylst.Add(new SqlParameter("@SentDate", sSentDate));
        arraylst.Add(new SqlParameter("@DateofJoining", sDateofJoining));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));
        arraylst.Add(new SqlParameter("@Subject", sSubject));
        arraylst.Add(new SqlParameter("@Terms", Terms));
        arraylst.Add(new SqlParameter("@HasSend", bHasSend));
        if(iTemplateId>0)
            arraylst.Add(new SqlParameter("@TemplateId", iTemplateId));
        arraylst.Add(new SqlParameter("@CreatedBy", CreatedBy) );
        if(Designation > 0)
        arraylst.Add(new SqlParameter("@DesignationId", Designation));

        return Convert.ToInt32(ExecuteScalar(arraylst));
    }

    public void InsertOfferletterDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IOD"));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));
        arraylst.Add(new SqlParameter("@AddDedID", AddDedID));
        arraylst.Add(new SqlParameter("@AllowanceAmount", dAllowanceAmount));
        arraylst.Add(new SqlParameter("@Addition", bAddition));

       ExecuteScalar(arraylst);
    }

    public void UpdateOfferStatus()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "UOF"));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));
        arraylst.Add(new SqlParameter("@OfferStatusId", OfferStatus));       
        arraylst.Add(new SqlParameter("@ApprovedBy", CreatedBy));
        arraylst.Add(new SqlParameter("@Remarks", Remarks));

        ExecuteScalar(arraylst);
    }

    public DataTable GetOfferLetters()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SCO"));
        arraylst.Add(new SqlParameter("@PageIndex", iPageIndex));
        arraylst.Add(new SqlParameter("@PageSize", iPageSize));
        arraylst.Add(new SqlParameter("@SortExpression", sSortExpression));
        arraylst.Add(new SqlParameter("@SortOrder", sSortOrder));
        arraylst.Add(new SqlParameter("@SearchKey", SearchKey));
        arraylst.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));
        arraylst.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1 : 0));
        

        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public DataSet IsAllowanceorDeduction()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "AOD"));
        arraylst.Add(new SqlParameter("@AllowanceTypeId", iAllowanceTypeId));

        return ExecuteDataSet("HRspOfferLetter",arraylst);
    }

    public string DeductionHasPolicy(int iAllowanceId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "HPD"));
        arraylst.Add(new SqlParameter("@AllowanceTypeId", iAllowanceId));

        return Convert.ToString(ExecuteScalar("HRspOfferLetter",arraylst));
     }    

    /// <summary>
    /// Getting candidate details for ending offer letter 
    /// </summary>
    /// <returns></returns>

    public DataSet GetCandidateDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SCD"));
        arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));

        return ExecuteDataSet("HRspOfferLetter", arraylst);
    }
    /// <summary>
    /// get offer status
    /// </summary>
    /// <returns></returns>
    public DataSet GetOfferStatus()
    {
        ArrayList arraylst = new ArrayList();
        arraylst.Add(new SqlParameter("@Mode", "GOS"));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet ("HRspOfferLetter", arraylst);

    }
    /// <summary>
    /// Get vacancy details for sending offer letter
    /// </summary>
    /// <returns></returns>

    public DataTable GetVacancyDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SVD"));
        arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));

        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public DataTable GetJobApprovalLevels()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GJA"));
    
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));
       
        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }
    /// <summary>
    /// get template details from 
    /// </summary>
    /// <returns></returns>
    public DataTable GetTemplateFieldDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "TMF"));
        arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));

        return ExecuteDataTable("HRspOfferLetter", arraylst);

    }

    /// <summary>
    /// Get allowance details specified for the vacancy
    /// </summary>
    /// <returns></returns>

    public DataTable GetAllowances()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SA"));
        arraylst.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ? 1:0));
        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public DataTable GetPaymentMode()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPM"));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public int HasSentOfferLetter()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "HSO"));
        arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));

        return Convert.ToInt32(ExecuteScalar("HRspOfferLetter", arraylst));
    }

    public DataTable BindVacancyAllowanceDeduction(bool bHasOfferId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GVA"));
        arraylst.Add(new SqlParameter("@JobId", iVacancyId));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));
        arraylst.Add(new SqlParameter("@HasOfferId", bHasOfferId));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));

        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public DataTable ShowHistory()
    {
         ArrayList arraylst = new ArrayList();

         arraylst.Add(new SqlParameter("@Mode", "SH"));
         arraylst.Add(new SqlParameter("@CandidateId", iCandidateId));
         arraylst.Add(new SqlParameter("@JobId", iVacancyId));

         return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public DataTable ShowHistoryAllowanceDeductions()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SHAD"));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));

        return ExecuteDataTable("HRspOfferLetter", arraylst);
    }

    public void SuccessSend()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "USOF"));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));

        ExecuteNonQuery("HRspOfferLetter", arraylst);
    }
    public void UpdateCandidateStatus()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "UCS"));
        arraylst.Add(new SqlParameter("@Offerid", iOfferId));
        arraylst.Add(new SqlParameter("@candidateid", iCandidateId));
        arraylst.Add(new SqlParameter("@CandidateStatusId", CandidateStatusId));

        ExecuteNonQuery("HRspOfferLetter", arraylst);
    }
    public DataSet GetSavedOfferLetter()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GSO"));
        arraylst.Add(new SqlParameter("@Offerid", iOfferId ));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1:0));
        return ExecuteDataSet("HRspOfferLetter", arraylst);
    }

    public int GetTotalCount()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GOLC"));
        arraylst.Add(new SqlParameter("@Searchkey", SearchKey));
        arraylst.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return Convert.ToInt32(ExecuteScalar("HRspOfferLetter", arraylst));
    }

    public DataSet GetOfferletterDetails()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GOLD"));
        arraylst.Add(new SqlParameter("@OfferId", iOfferId));

        return ExecuteDataSet("HRspOfferLetter", arraylst);
    }
    public bool CancelOfferLetter(System.Collections.Generic.List<int> xmlOfferLetterIds)
    {
        object objRowsAffected = this.ExecuteScalar("HRspOfferLetter", new ArrayList { 
            new SqlParameter("@Mode", "COL"),
            new SqlParameter("@xmlOfferLetterIds", xmlOfferLetterIds.ToXml())
        });

        return objRowsAffected.ToInt32() > 0;
    }

    public void DeleteMessage()
    {
        object objRowsAffected = this.ExecuteScalar("HRspOfferLetter", new ArrayList { 
            new SqlParameter("@Mode", "DM"),
            new SqlParameter("@OfferId", OfferId)
        });

      
    }


    public void Begin()
    {
        BeginTransaction("HRspOfferLetter");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }
}
