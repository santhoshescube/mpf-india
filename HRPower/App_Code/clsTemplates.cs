﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

//// <summary>
/// Summary description for clsTemaplates
/// Author:Thasni Latheef
/// Created Date: 13-10-2010 
/// </summary>
public class clsTemplates:DL
{
	public clsTemplates()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iActivityId;
    private int iTemplateTypeId, iTemplateId, iPageIndex, iPageSize;
    private string sSubject, sTitle, sMessage,sSortExpression, sSortOrder;
    private bool bIsCurrent;
    private string sSearchKey;
    public string Searchkey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }
    public enum TemplateType
    {
        VacancyPosting = 1,
        InterviewScheduleForCandidates = 2,
        InterviewScheduleForIinterviewers = 3,
        WalkinScheduleForIinterviewers = 4,
        CancelScheduleForCandidates = 5,
        CancelScheduleForInterviewers = 6,
        OfferLetter = 7,
        SalaryCertificate = 8
    }
    private TemplateType tTemplateType;
    public TemplateType Type
    {
        get { return tTemplateType; }
        set { tTemplateType = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }
    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
    public int TemplateId
    {
        get { return iTemplateId; }
        set { iTemplateId = value; }
    }		
    public int TemplateTypeId
    {
        get { return iTemplateTypeId; }
        set { iTemplateTypeId = value; }
    }
    public string Subject
    {
        get { return sSubject; }
        set { sSubject = value; }
    }
    public string Title
    {
        get { return sTitle; }
        set { sTitle = value; }
    }
    public string Message
    {
        get { return sMessage; }
        set { sMessage = value; }
    }
    public bool IsCurrent
    {
        get { return bIsCurrent; }
        set { bIsCurrent = value; }
    }

    public int ActivityId
    {
        get { return iActivityId; }
        set { iActivityId = value; }
    }

    public bool DeleteTemplates()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "D"));
        arraylst.Add(new SqlParameter("@TemplateId", iTemplateId));
        return (Convert.ToBoolean(ExecuteScalar("HRspTemplates", arraylst)));
    }
    

    public DataSet GetTemplatesTypes() // get all the TemplateTypes
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GTT"));     

        return ExecuteDataSet("HRspTemplates", alParameters);

    }

    public DataSet GetFields() // get all the MergeFieldsof a Particular templatetype
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GF"));
        alParameters.Add(new SqlParameter("@TemplateTypeId", iTemplateTypeId));
        return ExecuteDataSet("HRspTemplates", alParameters);

    }

    public bool IsDuplicate(bool IsEdit)
    {
        object objDuplicateCount = this.ExecuteScalar("HRSpTemplates", new ArrayList { 
            new SqlParameter("@Mode", "ID"),
            new SqlParameter("@IsEdit", IsEdit),
            new SqlParameter("@TemplateID", this.TemplateId),
            new SqlParameter("@TemplateTypeID", this.TemplateTypeId),
            new SqlParameter("@Subject", this.Subject),
            new SqlParameter("@Title", this.Title)
        });

        return objDuplicateCount.ToInt32() > 0;
    }

    public int InsertUpdateTemplate(bool IsInsert) // insert into HRTemplates
    {

        ArrayList alParameters = new ArrayList();

        if (IsInsert)
            alParameters.Add(new SqlParameter("@Mode", "I"));
        else
        {
            alParameters.Add(new SqlParameter("@Mode", "U"));
            alParameters.Add(new SqlParameter("@TemplateId", iTemplateId));
        }

        alParameters.Add(new SqlParameter("@TemplateTypeId", iTemplateTypeId));
        alParameters.Add(new SqlParameter("@Subject", sSubject));
        alParameters.Add(new SqlParameter("@Title", sTitle));
        alParameters.Add(new SqlParameter("@Message", sMessage));
        alParameters.Add(new SqlParameter("@IsCurrent", (bIsCurrent == true ? 1 : 0)));


        try
        {
            return Convert.ToInt32(ExecuteScalar("HRspTemplates", alParameters));

        }
        catch
        {
            return 0;
        }

    }

    public DataTable GetTemplatesDetails() // get details of a particular template
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SI"));
        alParameters.Add(new SqlParameter("@TemplateId", iTemplateId));
        return ExecuteDataTable("HRspTemplates", alParameters);
    }

    public DataSet GetAllTemplates() // get all templates 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SALL"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alParameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        return ExecuteDataSet("HRspTemplates", alParameters);

    }

    public DataTable GetTemplates() // get all the Templates of particularType
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GT"));
        alParameters.Add(new SqlParameter("@TemplateTypeId", tTemplateType));
        return ExecuteDataTable("HRspTemplates", alParameters);

    }

    public DataTable GetTemplatesCurrent() // get currentTemplate of particularType
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GTC"));
        alParameters.Add(new SqlParameter("@TemplateTypeId", tTemplateType));
        return ExecuteDataTable("HRspTemplates", alParameters);

    }
    public DataTable GetActivityVacancyDetails() // get vacancy details for send mail
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SAVA"));
        alParameters.Add(new SqlParameter("@ActivityId", iActivityId));
        return ExecuteDataTable("HRspTemplates", alParameters);
    }


    
}
