﻿using System;
using System.Data;

using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptCandidates
/// </summary>
public class clsRptCandidates:DL
{
    ArrayList prmCandidates = null;

	public clsRptCandidates()
	{
		
	}

    public DataSet FillCombos()
    {
        prmCandidates = new ArrayList();
        prmCandidates.Add(new SqlParameter("@Mode", 2));
        return ExecuteDataSet("HRspRptCandidates", prmCandidates);
    }

    public DataTable FillVaccancy(int intCompanyID)
    {
        prmCandidates = new ArrayList();
        prmCandidates.Add(new SqlParameter("@Mode", 3));
        prmCandidates.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("HRspRptCandidates", prmCandidates);
    }

    public DataTable GetReport(int intCandidateID, int intCandidateStatusID,int intDegreeID,int intVaccancyID)
    {
        return ExecuteDataTable("HRspRptCandidates", new ArrayList()
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@CandidateID",intCandidateID),
            new SqlParameter("@CandidateStatusID",intCandidateStatusID), 
            new SqlParameter("@DegreeID",intDegreeID),
            new SqlParameter("@VaccancyID",intVaccancyID)
        });
    }

    /* FOR JOB OPENING */
    public DataSet FillCompany()
    {
        return ExecuteDataSet("HRspRptJobOpening", new ArrayList()
        {
            new SqlParameter("@Mode",1)
        });
    }

    public DataSet FillVaccancyStatus()
    {
        return ExecuteDataSet("HRspRptJobOpening", new ArrayList()
        {
            new SqlParameter("@Mode",5)
        });
    }

    public DataSet FillBranches(int intCompanyID)
    {
        return ExecuteDataSet("HRspRptJobOpening", new ArrayList()
        {
            new SqlParameter("@Mode",2),
            new SqlParameter("@CompanyID",intCompanyID)
        });
    }

    public DataSet FillJobTitle(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        return ExecuteDataSet("HRspRptJobOpening", new ArrayList()
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany)
        });
    }
    public DataTable GetJobOpeningReport(int intCompanyID, int intBranchID, bool blnIncludeCompany, int intVaccancyStatusID, int intVaccancyID, DateTime dtPFromDate, DateTime dtPToDate, DateTime dtLFromDate, DateTime dtLToDate,int intchkLFromTo,int intchkPFromTo)
    {
        return ExecuteDataTable("HRspRptJobOpening", new ArrayList()
        {
            new SqlParameter("@Mode",4),
            new SqlParameter("@CompanyID",intCompanyID),
            new SqlParameter("@BranchID",intBranchID),
            new SqlParameter("@IncludeCompany",blnIncludeCompany),
            new SqlParameter("@VaccancyStatusID",intVaccancyStatusID),
            new SqlParameter("@VaccancyID",intVaccancyID),
            new SqlParameter("@PFromDate",dtPFromDate),
            new SqlParameter("@PToDate",dtPToDate),
            new SqlParameter("@LFromDate",dtLFromDate),
            new SqlParameter("@LToDate",dtLToDate),
            new SqlParameter("@chkLFromTo",intchkLFromTo),
            new SqlParameter("@chkPFromTo",intchkPFromTo)

        });
    }


    ////////////////////////////////////JOB REPORT-----------START/////////////////////////////////////////////
    #region JOB REPORT
    public static DataTable FillJobs(int intCompanyID, int intBranchID, bool blnIncludeCompany,int DepartmentID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchId", intBranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", blnIncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }
    public static DataTable FillJobMasterReport(int intCompanyID, int intBranchID, bool blnIncludeCompany, int DepartmentID, int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchId", intBranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", blnIncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);

    }
    public static DataTable FillJobDetailsReport(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }
    public static DataSet FillJobScheduleDetailsReport(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));

        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataSet("HRspRptJob", alParameters);
    }


    public static DataTable GetInterviewScheduleDetails(int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }


    public static DataTable GetEvaluationReport(int CandidateID,int JobScheduleID     )
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@JobScheduleID", JobScheduleID));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }

    public static DataTable GetCandidateOfferDetails(int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }

    public static DataTable GetCandidateBasicDetails(int? EmployeeID, int? CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        if(CandidateID != null )
            alParameters.Add(new SqlParameter("@CandidateID", CandidateID));

        if(EmployeeID != null )
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }



    public static Int32 GetCandidateID(int EmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return new DataLayer().ExecuteScalar("HRspRptJob", alParameters).ToInt32();
    }



    public static DataTable  GetJobSalaryStructureReport(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }



    public static DataTable GetGeneralExpenseReport(int CompanyID,int BranchID,bool IncludeCompany,int ExpenseCategoryID,int ExpenseTypeID,int ExpenseID,DateTime FromDate,DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", IncludeCompany));
        alParameters.Add(new SqlParameter("@ExpenseCategoryID", ExpenseCategoryID));
        alParameters.Add(new SqlParameter("@ExpenseTypeID", ExpenseTypeID));
        alParameters.Add(new SqlParameter("@ExpenseID", ExpenseID));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        
        return new DataLayer().ExecuteDataTable("HRspRptJob", alParameters);
    }


    #endregion
    //////////////////////////////////JOB REPORT-----------  END//////////////////////////////////////////////

    #region CANDIDATE REPORT
    public static DataTable FillcandidateJobs(int intCompanyID, int intBranchID, bool blnIncludeCompany, int DepartmentID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchId", intBranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", blnIncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }
    public static DataTable FillCandidates(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }
    public static DataTable ShowCandidateList(int JobID,int StatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@JobID", JobID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CandidateStatusId", StatusID));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }
    public static DataTable CandidateStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }
    public static DataTable ShowCandidatemasterReport(int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }
    public static DataTable ShowCandidatescheduleReport(int CandidateID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CandidateID", CandidateID));
        return new DataLayer().ExecuteDataTable("HRspRptCandidate", alParameters);
    }

    #endregion

    public static DataSet GetRecruitmentProcess(int JobID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteDataSet("HRspRptRecruitmentProcess", alParameters);
    }
}
