﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Created By:    Sanju
/// Created Date:  16-Oct-2013
/// Description:   View Document
/// </summary>
public class clsViewDocument : DL 
{
    public Int32 EmployeeID { get; set; }
    public Int32 CompanyID { get; set; }
    public Int32 DocumentID { get; set; }
    public string Type { get; set; }
    public clsViewDocument()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public DataTable GetAllEmployees()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspViewDocuments", alParameters);
    }
    public DataTable GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@UserID",new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspViewDocuments", alParameters);
    }
    public DataTable GetDocumentTypes(int intMode)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", intMode));//2:Employee,6:Company
        if (intMode == 2)
        {
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        }
        else
        {
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        }
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataTable("HRspViewDocuments", alParameters);
    }
    public DataTable GetAllDocument()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@Type", Type));
        return ExecuteDataTable("HRspViewDocuments", alParameters);
    }
    public DataSet GetDocuments(int intMode)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", intMode));//3:Employee,7:Company
        alParameters.Add(new SqlParameter("@Type", Type));
        if (intMode == 3)
        {
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        }
        else
        {
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        }
        return ExecuteDataSet("HRspViewDocuments", alParameters);
    }

    public DataSet GetDocumentDetails(int intMode)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", intMode));//4:Employee,8:Company
        alParameters.Add(new SqlParameter("@Type", Type));
        if (intMode == 4)
        {
            alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        }
        else
        {
            alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        }
        alParameters.Add(new SqlParameter("@DocumentID", DocumentID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return ExecuteDataSet("HRspViewDocuments", alParameters);
    }
    public DataTable GetEmployee()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        return ExecuteDataTable("HRspViewDocuments", alParameters);
    }
}
