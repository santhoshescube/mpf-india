﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections;
using System.Data.SqlClient;
using System.Xml.Linq;


/// <summary>
/// Summary description for clsWalkinInterview
/// Author:Thasni Latheef
/// Created Date: 13-10-2010 
/// </summary>
public class clsWalkinInterview:DL
{
	public clsWalkinInterview()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string sStartDate, sEndDate, sVenue, sComments, sStartTime, sEndTime, sScheduleTitle, sInterviewerIds, sSortExpression, sSortOrder, sReason, sRMode;
    private int iInterviewScheduleId, iScheduleStatusId, iScheduledById, iPageIndex, iPageSize, iInsertionMode, iParentId, iVacancyId,iMonth,iYear, iCompanyId;
    private long iActivityId;
    private string sSearchKey;
    public string Searchkey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }
    private DateTime dSearchDate, dNullDate; // Report

    public int InsertionMode
    {
        get { return iInsertionMode; }
        set { iInsertionMode = value; }
    }
    public int ParentId
    {
        get { return iParentId; }
        set { iParentId = value; }
    }
    public int ScheduledById
    {
        get { return iScheduledById; }
        set { iScheduledById = value; }
    }
    public int InterviewScheduleId
    {
        get { return iInterviewScheduleId; }
        set { iInterviewScheduleId = value; }
    }   
    public int ScheduleStatusId
    {
        get { return iScheduleStatusId; }
        set { iScheduleStatusId = value; }
    }
    public int VacancyId // Report
    {
        get { return iVacancyId; }
        set { iVacancyId = value; }
    }
    public int Month // Report
    {
        get { return iMonth; }
        set { iMonth = value; }
    }
    public int Year // Report
    {
        get { return iYear; }
        set { iYear = value; }
    }
    public int CompanyId // Report
    {
        get { return iCompanyId; }
        set { iCompanyId = value; }
    }
    public string RMode // Report
    {
        get { return sRMode; }
        set { sRMode = value; }
    }   
    public string InterviewerIds
    {
        get { return sInterviewerIds; }
        set { sInterviewerIds = value; }
    }
    public string Venue
    {
        get { return sVenue; }
        set { sVenue = value; }
    }
    public string ScheduleTitle
    {
        get { return sScheduleTitle; }
        set { sScheduleTitle = value; }
    }
    public string StartDate
    {
        get { return sStartDate; }
        set { sStartDate = value; }
    }
    public string EndDate
    {
        get { return sEndDate; }
        set { sEndDate = value; }
    }
    public long ActivityId
    {
        get { return iActivityId; }
        set { iActivityId = value; }
    }
    public string Comments
    {
        get { return sComments; }
        set { sComments = value; }
    }
    public string StartTime
    {
        get { return sStartTime; }
        set { sStartTime = value; }
    }
    public string EndTime
    {
        get { return sEndTime; }
        set { sEndTime = value; }
    }   
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }
    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }
    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
    public string Reason
    {
        get { return sReason; }
        set { sReason = value; }
    }

    public DateTime SearchDate 
    {
        get { return dSearchDate; }
        set { dSearchDate = value; }
    }

    public DateTime NullDate // Report
    {
        get { return dNullDate; }
        set { dNullDate = value; }
    }

    public int InsertUpdateWalkin() // insert into interview schedule master
    {

        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "I"));
        alParameters.Add(new SqlParameter("@InsertionMode", iInsertionMode));
        alParameters.Add(new SqlParameter("@InterviewScheduleId", iInterviewScheduleId));
        alParameters.Add(new SqlParameter("@StartDate", sStartDate));
        alParameters.Add(new SqlParameter("@EndDate", sEndDate));
        alParameters.Add(new SqlParameter("@StartTime", sStartTime));
        alParameters.Add(new SqlParameter("@EndTime", sEndTime));
        alParameters.Add(new SqlParameter("@Venue", sVenue));
        alParameters.Add(new SqlParameter("@ScheduleTitle", sScheduleTitle));
        alParameters.Add(new SqlParameter("@ActivityId", iActivityId));
        alParameters.Add(new SqlParameter("@Comments", sComments));
        alParameters.Add(new SqlParameter("@ParentId", iParentId));            
        alParameters.Add(new SqlParameter("@InterviewerIds", sInterviewerIds));
        alParameters.Add(new SqlParameter("@ScheduledById", iScheduledById));



        try
        {
            return Convert.ToInt32(ExecuteScalar("HRspWalkinInterview", alParameters));

        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public DataSet GetAllWalkinSchedules() // get interview schedule 
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "SALL"));
        alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", iPageSize));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alParameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        alParameters.Add(new SqlParameter("@ScheduledById", iScheduledById));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspWalkinInterview", alParameters);

    }
    public int GetRecordCount() // Get Total Record count
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "RC"));
        alParameters.Add(new SqlParameter("@ScheduledById", iScheduledById));
        alParameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return Convert.ToInt32(ExecuteScalar("HRspWalkinInterview", alParameters));
    }

    /// <summary>
    /// Walk in interview Report
    /// Author:Laxmi
    /// Created Date: 17-12-2010
    /// 
    /// </summary>

    // Modified By : Ratheesh, Date : 3 Jan 2010

    public DataSet GetWalkinInterviewList()
    {

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GWDR"));

        if (iVacancyId > 0) alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        if (iScheduleStatusId > 0) alParameters.Add(new SqlParameter("@ScheduleStatusId", iScheduleStatusId));
        alParameters.Add(new SqlParameter("@Month", iMonth));
        alParameters.Add(new SqlParameter("@Year", iYear));
        if (dSearchDate != dNullDate) alParameters.Add(new SqlParameter("@Date", dSearchDate));
        if (iCompanyId > 0) alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
        alParameters.Add(new SqlParameter("@RMode", sRMode));

        using (DataSet ds = ExecuteDataSet("HRspWalkinInterview", alParameters))
        {
            if (ds.Tables.Count > 0)
                ds.Tables[0].TableName = "DtWalkinInterview";

            ds.Tables.Add(new clsReport().GetReportHeader());

            return ds;
        }
    }
}
