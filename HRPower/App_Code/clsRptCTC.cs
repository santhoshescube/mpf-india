﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptCTC
/// </summary>
public class clsRptCTC:DL
{
	public clsRptCTC()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataSet GetEmployeeCTCReport(int CompanyID, int BranchID, bool IncludeCompany, int EmployeeID, DateTime? Date, string FinYear,int WorkStatusID)
    {
        List<SqlParameter> sqlParameters = new List<SqlParameter>();
        sqlParameters.Add(new SqlParameter("@Mode", 1));
        sqlParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        sqlParameters.Add(new SqlParameter("@BranchID", BranchID));
        sqlParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        sqlParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        sqlParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        sqlParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        if (Date != null)
            sqlParameters.Add(new SqlParameter("@Date", Date));
        if (FinYear != "")
            sqlParameters.Add(new SqlParameter("@FinYear", FinYear));
        return new DataLayer().ExecuteDataSet("spRPTCTC", sqlParameters);
    }
}
