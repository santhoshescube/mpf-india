﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Threading;

/// <summary>
/// Summary description for clsGlobalization
/// </summary>
public class  clsGlobalization : System.Web.UI.Page 
{
	public clsGlobalization()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    static string cultureName;

    public static string CultureName
    {
        get { return cultureName; }
        set { cultureName = value; }
    }

    protected override void InitializeCulture()
    {
        System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("ar-AE");
        System.Threading.Thread.CurrentThread.CurrentCulture = c;
        System.Threading.Thread.CurrentThread.CurrentUICulture = c;

        base.InitializeCulture();
    }


    public static void SetCulture(Page _page)
    {
        _page.UICulture = clsUserMaster.GetCulture();
        //_page.Culture = clsUserMaster.GetCulture();

    }

    public static bool IsArabicViewEnabled()
    {
        return false;
    }

    public static bool IsArabicCulture()
    {
        return clsUserMaster.GetCulture() == "ar-AE";
    }

    public static bool LicenseStatus { get; set; }

    public static string ProductSerial { get; set; }

    public static string MSIPorts { get; set; }

    public static string ClientCmp { get; set; }

}
