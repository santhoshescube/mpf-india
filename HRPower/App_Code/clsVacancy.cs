﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Retrieves and displays all vacancies
/// </summary>
public class clsVacancy : DL
{
    public clsVacancy()
    { }

    #region Variables
    public static string Procedure = "HRspVacancyListAll";
    #endregion

    #region Properties
    public int JobID { get; set; }
    public int PageIndex { get; set; }
    public int PageSize { get; set; }
    public string SearchKey { get; set; }
    #endregion

    #region Methods

    public static int GetTotalRecordCount(string SearchKey, int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "TRC"));
        arr.Add(new SqlParameter("@SearchKey", SearchKey.EscapeUnsafeChars()));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static DataTable GetAllVacancies(int PageSize, int PageIndex, string SearchKey, int CompanyID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "GAV"));
        arr.Add(new SqlParameter("@PageSize", PageSize));
        arr.Add(new SqlParameter("@PageIndex", PageIndex));
        arr.Add(new SqlParameter("@SearchKey", SearchKey));
        arr.Add(new SqlParameter("@CompanyID", CompanyID));
        arr.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, arr);
    }

    public static int IsVacancyScheduled(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "IVS"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static int UpdateVacancyWhenClosed(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "UVWC"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static int IsCandidateAssigned(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "ICM"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public static int IsUsedInGE(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "IUIGE"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return new DataLayer().ExecuteScalar(Procedure, arr).ToInt32();
    }

    public bool DeleteVacancy(int JobID)
    {
        ArrayList arr = new ArrayList();
        arr.Add(new SqlParameter("@Mode", "DJ"));
        arr.Add(new SqlParameter("@JobID", JobID));
        return ExecuteScalar(Procedure, arr).ToInt32() > 0 ? true : false;
    }
    
    //Home Active Vacancies
    public static int GetRecordCount(int DepartmentId, int JobStatusID, int CompanyID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "AJRC"));
        parameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        parameters.Add(new SqlParameter("@CompanyID", CompanyID));
        return new DataLayer().ExecuteScalar(Procedure, parameters).ToInt32();
    }

    public static DataTable GetRecentVacancies(int PageIndex, int PageSize, int DepartmentId, int JobStatusID, int CompanyID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HAV"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        alParameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));       
        return new DataLayer().ExecuteDataTable(Procedure, alParameters);
    }

    public static DataTable GetHomeVacancyStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SF"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, alParameters);
    }

    public static DataTable GetDepartmentList()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FD"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, alParameters);
    }

    #endregion
}
