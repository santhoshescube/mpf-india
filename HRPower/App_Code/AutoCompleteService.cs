﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Data.SqlClient;

/// <summary>
/// Summary description for AutoCompleteService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoCompleteService : System.Web.Services.WebService
{
    public string TableName { get; set; }
    public string Key { get; set; }
    public int Count { get; set; }
    public string Suggestions { get; set; }

    private clsCommonAutoComplete objAutoComplete;
    private clsEmployeeSeperation objTerminate;
    public AutoCompleteService()
    {
        objTerminate = new clsEmployeeSeperation();
        objAutoComplete = new clsCommonAutoComplete();
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [System.Web.Services.WebMethod(EnableSession =true)]
    [System.Web.Script.Services.ScriptMethod]

    public string[] GetSuggestions(string prefixText, int count,string contextKey)
    {
        List<string> s = new List<String>();
        if (Session["TerminateCompanyId"] != null)
        {
            using (SqlDataReader dr = objAutoComplete.GetSuggestions(prefixText, Convert.ToInt32(Session["TerminateCompanyId"])))
            {
                while (dr.Read())
                {
                    string tempName = Convert.ToString(dr["EmployeeNumber"]);
                    s.Add(tempName);

                }
            }
            //Session["TerminateCompanyId"] = null;
            return s.ToArray();

        }
        else return s.ToArray();
    }

    [System.Web.Services.WebMethod (EnableSession =true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetEmployeeTakeOverSuggestions(string prefixText, int count, string contextKey)
    { 
        List<string> s = new List<String>();
        if (Session["TakeOverCompanyId"] != null && Session["IsAllCompany"] != null)
        {
            using (SqlDataReader dr = objAutoComplete.GetEmployeeTakeOverSuggestions(prefixText,Convert.ToInt32(Session["TakeOverCompanyId"])))
            {
                while (dr.Read())
                {
                    string tempName = Convert.ToString(dr["EmployeeNumber"]);
                    s.Add(tempName);

                }
            }
            return s.ToArray();
        }
        else
            return s.ToArray();
    }
    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetEmployeeNameSuggestions(string prefixText, int count)
    {
        List<string> s = new List<String>();
        using (SqlDataReader dr = objAutoComplete.GetEmployeeNameSuggestions(prefixText, count))
        {
            while (dr.Read())
            {
                string tempName = Convert.ToString(dr["EmployeeFullName"]);
                s.Add(tempName);

            }
        }
        return s.ToArray();
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetAllSuggestions(string prefixText, int count)
  {
        int IsAllCompany = 0;
        if (Session["IsAllCompany"] != null)
            IsAllCompany = Convert.ToInt32(Session["IsAllCompany"]);


        return objAutoComplete.GetAllSuggestions(prefixText, 10,IsAllCompany); 
    }


    //public string[] GetSuggestions()
    //{
    //    List<string> s = new List<String>();
    //    using (SqlDataReader dr = objAutoComplete.GetSuggestions(TableName,Suggestions,Key,Count))
    //    {
    //        while (dr.Read())
    //        {
    //            string tempName = Convert.ToString(dr["EmployeeNumber"]);
    //            s.Add(tempName);

    //        }
    //    }
    //    return s.ToArray();
    //}


}

