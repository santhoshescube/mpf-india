﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsChangePolicy
/// </summary>
public class clsChangePolicy : DL
{

    public Int32 intPageIndex { get; set; }
    public Int32 intPageSize { get; set; }
    public Int32 intCompanyID { get; set; }
    public Int32 intDepartmentID { get; set; }
    public Int32 intDesignationID { get; set; }
    public Int32 intEmployeeID { get; set; }
    public Int32 intWorkPolicyID { get; set; }
    public Int32 intLeavePolicyID { get; set; }
    public Int32 intAbsentPolicyID { get; set; }
    public Int32 intEncashPolicyID { get; set; }
    public Int32 intHolidayPolicyID { get; set; }
    public Int32 intOvertimePolicyID { get; set; }
    public Int32 intPayStructureID { get; set; }


    public clsChangePolicy()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet GetCombo()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 1));
        return ExecuteDataSet("HRspChangePolicy", alParameters);
    }
    public DataSet GetEmployeesPolicies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@PageIndex", intPageIndex));
        alParameters.Add(new SqlParameter("@PageSize", intPageSize));
        return ExecuteDataSet("HRspChangePolicy", alParameters);
    }
    public DataSet GetGridCombo()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 3));
        return ExecuteDataSet("HRspChangePolicy", alParameters);
    }

    public DataSet AddPolicy()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@WorkPolicyID", intWorkPolicyID));
        alParameters.Add(new SqlParameter("@LeavePolicyID", intLeavePolicyID));
        alParameters.Add(new SqlParameter("@AbsentPolicyID", intAbsentPolicyID));
        alParameters.Add(new SqlParameter("@EncashPolicyID", intEncashPolicyID));
        alParameters.Add(new SqlParameter("@HolidayPolicyID", intHolidayPolicyID));
        alParameters.Add(new SqlParameter("@OvertimePolicyID", intOvertimePolicyID));
        alParameters.Add(new SqlParameter("@PayStructureID", intPayStructureID));

        return ExecuteDataSet("HRspChangePolicy", alParameters);
    }
}
