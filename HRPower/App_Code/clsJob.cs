﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsJob
/// </summary>
public class clsJob : DL
{
    public clsJob()
    { }

    #region Variables
    public static string Procedure = "HRspJobDetails";
    #endregion

    #region Properties

    public int JobId { get; set; }
    public int JobDetailID { get; set; }
    public int JobLevelID { get; set; }
    public int EmployeeID { get; set; }
    public int EmploymentTypeID { get; set; }
    public int Level { get; set; }
    public int SalaryID { get; set; }
    public string PayStructure { get; set; }
    public int DegreeId { get; set; }
    public int CriteriaId { get; set; }
    public int AdditionDeductionId { get; set; }
    public int CompanyId { get; set; }
    public int DepartmentId { get; set; }
    public int DesignationID { get; set; }
    public int RequiredQuota { get; set; }
    public string JobCode { get; set; }
    public string JobTitle { get; set; }
    public string Remarks { get; set; }
    public Decimal WeightPercentage { get; set; }
    public Decimal PassPercentage { get; set; }
    public Decimal TotalMark { get; set; }
    public Decimal Amount { get; set; }
    public Decimal BudgetedAmount { get; set; }
    public string ExpiryDate { get; set; }
    public string Skills { get; set; }
    public string Experience { get; set; }
    public string PlaceOfWork { get; set; }
    public int WeeklyWorkingDays { get; set; }
    public int DailyWorkingHours { get; set; }
    public bool IsAccomodation { get; set; }
    public bool IsTransportation { get; set; }
    public string ContractPeriod { get; set; }
    public string AirTicket { get; set; }
    public int AnnualLeaveDays { get; set; }
    public string LegalTerms { get; set; }
    public int CreatedBy { get; set; }
    public int ApprovedBy { get; set; }
    public string ApprovedDate { get; set; }
    public int JobStatusID { get; set; }
    public bool IsBoardInterview { get; set; }
    public bool IsOfferApproval { get; set; }
    public int PaymentClassificationID { get; set; }
    public int PayCalculationTypeID { get; set; }
    public decimal GrossPay { get; set; }
    public int PageIndex { get; set; }
    public int PageSize {get; set; }    
    public string SearchKey { get; set; }
    public bool IsVacancyApproval { get; set; }
    public bool IsOfferLetterApproval { get; set; }
    public bool IsForwarded { get; set; }
    public bool IsBudgeted { get; set; }
    public bool RecruitmentFeesPayable { get; set; }
    public int GenderRecruID { get; set; }
    public int RecruitmentTypeID { get; set; }
    public string Languages { get; set; }
    public int MinAge { get; set; }
    public int MaxAge { get; set; }
    public string JoiningDate { get; set; }
    public int ReportingTo { get; set; }

   
    #endregion

    /// <summary>
    /// Add/update Job details 
    /// </summary>
    /// <param name="isAdd">add / update flag</param>
    /// <returns>int - jobid</returns>
    public int AddUpdateJob()
    {

        ArrayList parameters = new ArrayList();
        if (JobStatusID == 5 || JobStatusID == 4 || JobStatusID == 3)
            parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        else
            parameters.Add(new SqlParameter("@JobStatusID", 1));
        if (JobId > 0)
        {
            parameters.Add(new SqlParameter("@Mode", "UPDATE"));
            parameters.Add(new SqlParameter("@JobId", JobId));
            //parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));            
        }
        else
        {
            parameters.Add(new SqlParameter("@Mode", "I"));
            parameters.Add(new SqlParameter("@ReportingTo", ReportingTo));
        }
        parameters.Add(new SqlParameter("@CompanyID", CompanyId));
        parameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@JobCode", JobCode));
        parameters.Add(new SqlParameter("@JobTitle", JobTitle));
        parameters.Add(new SqlParameter("@ExpiryDate", ExpiryDate));
        parameters.Add(new SqlParameter("@RequiredQuota", RequiredQuota));
        parameters.Add(new SqlParameter("@EmploymentTypeID", EmploymentTypeID));
        parameters.Add(new SqlParameter("@TotalMarks", TotalMark));
        parameters.Add(new SqlParameter("@PassPercentage", PassPercentage));
        parameters.Add(new SqlParameter("@Remarks", Remarks));
        parameters.Add(new SqlParameter("@Skills", Skills));
        parameters.Add(new SqlParameter("@Experience", Experience));
        parameters.Add(new SqlParameter("@IsBoardInterview", IsBoardInterview));
        parameters.Add(new SqlParameter("@PlaceOfWork", PlaceOfWork));
        parameters.Add(new SqlParameter("@WeeklyWorkingDays", WeeklyWorkingDays));
        parameters.Add(new SqlParameter("@DailyWorkingHour", DailyWorkingHours));
        parameters.Add(new SqlParameter("@IsAccomodation", IsAccomodation));
        parameters.Add(new SqlParameter("@IsTransportation", IsTransportation));
        parameters.Add(new SqlParameter("@ContractPeriod", ContractPeriod));
        parameters.Add(new SqlParameter("@AirTicket", AirTicket));
        parameters.Add(new SqlParameter("@AnnualLeaveDays", AnnualLeaveDays));
        parameters.Add(new SqlParameter("@LegalTerms", LegalTerms));
        parameters.Add(new SqlParameter("@CreatedBy", CreatedBy));
        parameters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
        parameters.Add(new SqlParameter("@IsOfferApproval", IsOfferApproval));
        parameters.Add(new SqlParameter("@IsVacancyApproval", IsVacancyApproval));
        parameters.Add(new SqlParameter("@BudgetedAmount", BudgetedAmount));
        parameters.Add(new SqlParameter("@IsForwarded", IsForwarded));
        //tab preference

        parameters.Add(new SqlParameter("@IsBudgeted", IsBudgeted));
        parameters.Add(new SqlParameter("@Languages", Languages));
        parameters.Add(new SqlParameter("@MinAge", MinAge));
        parameters.Add(new SqlParameter("@MaxAge", MaxAge));
        parameters.Add(new SqlParameter("@GenderRecruID", GenderRecruID));
        parameters.Add(new SqlParameter("@RecruitmentTypeID", RecruitmentTypeID));
        parameters.Add(new SqlParameter("@RecruitmentFeesPayable", RecruitmentFeesPayable));
        parameters.Add(new SqlParameter("@JoiningDate", JoiningDate));

        return ExecuteScalar(Procedure, parameters).ToInt32();
    }
    public void InsertLevels()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "L"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@JobLevelID", JobLevelID));

        ExecuteNonQuery(Procedure, parameters);
    }
    public void InsertCriteria()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "ID"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@JobLevelID", JobLevelID));
        parameters.Add(new SqlParameter("@CriteriaID", CriteriaId));
        parameters.Add(new SqlParameter("@WPercentage", WeightPercentage));
        ExecuteNonQuery(Procedure, parameters);
    }
    public void InsertApprovalAuthority()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IAA"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@Level", Level));
        parameters.Add(new SqlParameter("@IsOfferLetterApproval", IsOfferLetterApproval));
        
        ExecuteNonQuery(Procedure, parameters);
    }
    public void InsertSalaryDetail()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IS"));        
        parameters.Add(new SqlParameter("@SalaryId", SalaryID));
        parameters.Add(new SqlParameter("@AdditionDeductionID", AdditionDeductionId));
        parameters.Add(new SqlParameter("@Amount", Amount));

        ExecuteNonQuery(Procedure, parameters);
    }
    public int InsertSalaryMaster()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "ISM"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        parameters.Add(new SqlParameter("@DesignationID", DesignationID));
        parameters.Add(new SqlParameter("@PaymentClassificationID", PaymentClassificationID));
        parameters.Add(new SqlParameter("@PayCalculationTypeID", PayCalculationTypeID));
        parameters.Add(new SqlParameter("@GrossPay", GrossPay));

        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }
    public void InsertQualificationList()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IQL"));       
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@DegreeId", DegreeId));

        ExecuteNonQuery(Procedure, parameters);
    }

    public int UpdateJobWhenApproved()
    {
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", "UWA"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        parameters.Add(new SqlParameter("@ApprovedBy", ApprovedBy));
        parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        parameters.Add(new SqlParameter("@IsForwarded", IsForwarded));

        return ExecuteScalar(Procedure, parameters).ToInt32();
    }
    public int UpdateJobWhenClosed()
    {
        ArrayList parameters = new ArrayList();

        parameters.Add(new SqlParameter("@Mode", "UWC"));
        parameters.Add(new SqlParameter("@JobId", JobId));

        return ExecuteScalar(Procedure, parameters).ToInt32();
    }

    // get all job criteria
    public DataTable FillCriteria()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "JC"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, parameters);
    }
    // get all job Addition deduction
    public DataTable FillAdditionDeduction()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "AD"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, parameters);
    }

    public void DeleteCategory()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DC"));
        parameters.Add(new SqlParameter("@JobDetailID", JobDetailID));

        ExecuteDataTable(Procedure, parameters);
    }
    public void DeleteParticular()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DP"));
        parameters.Add(new SqlParameter("@SalaryID", SalaryID));

        ExecuteDataTable(Procedure, parameters);
    }
    public void DeleteAllCriteria()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DCA"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        ExecuteDataTable(Procedure, parameters);
    }
    public void DeleteAllParticular()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DPA"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        ExecuteDataTable(Procedure, parameters);
    }
    public void DeleteAllQualifications()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DAQ"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        ExecuteDataTable(Procedure, parameters);
    }
    public bool DeleteJob()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DJ"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        return ExecuteScalar(Procedure, parameters).ToInt32() > 0 ? true : false;
    }

    public DataSet GetJobDetails()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "S"));
        parameters.Add(new SqlParameter("@JobID", JobId));       
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, parameters);
    }
    public DataTable GetJobId()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "J"));

        return ExecuteDataTable(Procedure, parameters);
    }
    public bool GetAdditionorDeduction()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SAD"));
        alparameters.Add(new SqlParameter("@AdditionDeductionID", AdditionDeductionId));

        return Convert.ToBoolean(ExecuteScalar(Procedure, alparameters));
    }    
    public DataSet GetQualificationList()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FQ"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataSet GetCompanies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GC"));

        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataSet getHomeVacancyStatus()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "SF"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }

    public DataSet GetDepartmentList()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FD"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }

    public DataSet GetDesignations()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GD"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataSet GetEmploymentTypes()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "ET"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataSet GetJobLevels()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "FJL"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataSet GetPaymentClassificationMode()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "PCM"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }
    public DataTable GetJobs()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GJ"));

        return ExecuteDataTable(Procedure, parameters);
    }
    public DataSet GetAllJobs()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "VA"));
        parameters.Add(new SqlParameter("@PageIndex", PageIndex));
        parameters.Add(new SqlParameter("@PageSize", PageSize));        
        parameters.Add(new SqlParameter("@SearchKey", SearchKey));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, parameters);
    }
    public DataTable GetAuthorities()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAUTH"));
        return ExecuteDataTable(Procedure, parameters);
    }
    public DataTable GetRecentVacancies()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "HAV"));
        alParameters.Add(new SqlParameter("@PageIndex", PageIndex));
        alParameters.Add(new SqlParameter("@PageSize", PageSize));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        alParameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        //alParameters.Add(new SqlParameter("@RoleId", new clsUserMaster().GetRoleId()));
        //alParameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataTable(Procedure, alParameters);
    }
    public DataTable GetJobStatusReference()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GJSR"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, parameters);
    }
    public DataTable GetAllEmployees()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GAE"));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable(Procedure, parameters);
    }
    public int GetRecordCount()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "AJRC"));
        parameters.Add(new SqlParameter("@DepartmentID", DepartmentId));
        parameters.Add(new SqlParameter("@JobStatusID", JobStatusID));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }
    public int GetTotalRecordCount()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "RC"));
        parameters.Add(new SqlParameter("@SearchKey", SearchKey.EscapeUnsafeChars()));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }
    public DataSet GetSelectedJobDetails()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GSJD"));
        parameters.Add(new SqlParameter("@JobID", JobId));

        return ExecuteDataSet(Procedure, parameters);
    }

    public int IsJobScheduled()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IJS"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }
    public bool IsJobExist()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IJE"));
        parameters.Add(new SqlParameter("@JobID", JobId));

        return (Convert.ToInt32(ExecuteScalar(Procedure, parameters)) > 0 ? true : false);

    }
    
    public int DoesPayStructureExist()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DPSE"));
        if (JobId > 0)
        {
            parameters.Add(new SqlParameter("@JobID", JobId));
            parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        }
        else
            parameters.Add(new SqlParameter("@PayStructure", PayStructure));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }    

    public int IsCandidateAssigned()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "CM"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }    
    public int LevelExists(int JobLevelID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "LE"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        parameters.Add(new SqlParameter("@JobLevelID", JobLevelID));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }
    public int CriteriaExists(int CriteriaID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "CE"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        parameters.Add(new SqlParameter("@CriteriaID", CriteriaID));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }    
    public int CheckDuplicateJobCode()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "DJC"));
        if (JobId > 0)
        {
            parameters.Add(new SqlParameter("@JobID", JobId));
            parameters.Add(new SqlParameter("@JobCode", JobCode));
        }
        else
            parameters.Add(new SqlParameter("@JobCode", JobCode));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }


    public int IsUsedInGE()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "IUIGE"));
        parameters.Add(new SqlParameter("@JobID", JobId));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }

    public int GetRequestedTo(int EID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GRT"));
        parameters.Add(new SqlParameter("@EmployeeID", EID));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
   
    }

    public DataSet GetRecruitmentLocationType()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GRLT"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }

    public DataSet GetGender()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GG"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet(Procedure, alParameters);
    }

    public int GetAuthority(int JobId)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", "GA"));
        parameters.Add(new SqlParameter("@JobId", JobId));
        return Convert.ToInt32(ExecuteScalar(Procedure, parameters));
    }

    public static DataTable GetKpi()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GKPI"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, alParameters);
    }

    public static DataTable GetKra()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GKRA"));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable(Procedure, alParameters);
    }
}
