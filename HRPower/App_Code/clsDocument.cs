﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsDocument
/// </summary>
public class clsDocument : DL
{
	public clsDocument()
	{
		
	}
    private ArrayList alParameters = null;
    private string strProcedureName = "spDocuments";

    public int intDocumentID { get; set; }
    public int intDocumentTypeID { get; set; }
    public int intOperationTypeID { get; set; }
    public long lngReferenceID { get; set; }
    public string strReferenceNo { get; set; }
    public string strDocumentNo { get; set; }
    public DateTime dtIssueDate { get; set; }
    public DateTime dtExpiryDate { get; set; }
    public string strRemarks { get; set; }
    public string strDocName { get; set; }
    public string strFileName { get; set; }
    public int intNode { get; set; }


    public int intPageIndex { get; set; }
    public int intPageSize { get; set; }
    public string strSearchKey { get; set; }

    /* METHODS **************************** */

    public DataTable GetAllCompanies()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public DataTable GetAllEmployees()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public DataTable GetAllDocumentTypes()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public DataTable GetAllDocumentBins()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public bool SaveDocument()
    {
        alParameters = new ArrayList();
        if(intDocumentID == 0)
            alParameters.Add(new SqlParameter("@Mode",5));
        else
            alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        alParameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
        alParameters.Add(new SqlParameter("@OperationTypeID", intOperationTypeID));
        alParameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));
        alParameters.Add(new SqlParameter("@ReferenceNumber", strReferenceNo));
        alParameters.Add(new SqlParameter("@DocumentNumber", strDocumentNo));
        alParameters.Add(new SqlParameter("@IssuedDate", dtIssueDate));
        alParameters.Add(new SqlParameter("@ExpiryDate", dtExpiryDate));
        alParameters.Add(new SqlParameter("@Remarks", strRemarks));

        intDocumentID = ExecuteScalar(strProcedureName, alParameters).ToInt32();

        return intDocumentID > 0;
    }

    public DataTable GetAllDocuments()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 7));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public DataTable GetSingle()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        return ExecuteDataTable(strProcedureName, alParameters);
    }
    public DataTable EditDocument()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public string GetPrintText()
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        DataTable dtDocument = GetSingle();

        if (dtDocument != null && dtDocument.Rows.Count > 0)
        {
            sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
            sb.Append("<tr><td><table border='0'>");
            sb.Append("<tr>");
            sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dtDocument.Rows[0]["DocumentNumber"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("</table>");
            sb.Append("<tr><td style='padding-left:15px;'>");
            sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

            sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Other Document Information</td></tr>");
            sb.Append("<tr><td width='150px'>" + Convert.ToString(dtDocument.Rows[0]["OperationType"]) + "</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["ReferenceNumber"]) + "</td></tr>");
            sb.Append("<tr><td width='150px'>Document Type</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["DocumentType"]) + "</td></tr>");
            sb.Append("<tr><td>Issue Date</td><td>:</td><td>" + (dtDocument.Rows[0]["IssuedDate"]).ToString() + "</td></tr>");
            sb.Append("<tr><td>Expiry Date</td><td>:</td><td>" + (dtDocument.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
            sb.Append("<tr><td width='150px'>Other Info</td><td width='5px'>:</td><td>" + Convert.ToString(dtDocument.Rows[0]["Remarks"]) + "</td></tr>");

            sb.Append("</td>");
            sb.Append("</table>");
            sb.Append("</td>");
            sb.Append("</tr></table>");

        }

        return sb.ToString();
    }

    public string PrintSelected(string sDocumentIDs)
    {
        StringBuilder sb = new StringBuilder();

        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@DocumentIDs", sDocumentIDs));

        DataTable datPrint = ExecuteDataTable(strProcedureName, alParameters);

        if(datPrint != null && datPrint.Rows.Count > 0)
        {
            sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
            sb.Append("<tr><td style='font-weight:bold;' align='center'>Other Document Information</td></tr>");
            sb.Append("<tr><td>");
            sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
            sb.Append("<tr style='font-weight:bold;'><td width='100px'> Operation Type </td><td width='100px'> Reference Number </td><td width='150px'>Document Number</td><td width='100px'>Issue Date</td><td width='100px'>Expiry Date</td><td width='100px'>Other Info</td></tr>");
            sb.Append("<tr><td colspan='7'><hr></td></tr>");
            for (int i = 0; i < datPrint.Rows.Count; i++)
            {
                sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["OperationType"]) + "</td>");
                sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["ReferenceNumber"]) + "</td>");
                sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["DocumentNumber"]) + "</td>");
                sb.Append("<td>" + datPrint.Rows[i]["IssuedDate"].ToString() + "</td>");
                sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                sb.Append("<td>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

            }
            sb.Append("</td></tr>");
            sb.Append("</table>");
        }
        return sb.ToString();
    }

    public bool DeleteDocument()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        return ExecuteNonQuery(strProcedureName, alParameters).ToInt32() > 0;
    }

    public DataTable GetDocuments()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        alParameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));
        return ExecuteDataTable(strProcedureName, alParameters);
    }

    public int SaveTreeMaster() 
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 13));
        alParameters.Add(new SqlParameter("@ReferenceID", lngReferenceID));
        alParameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        alParameters.Add(new SqlParameter("@ReferenceNumber", strReferenceNo));
        alParameters.Add(new SqlParameter("@DocName", strDocName));
        alParameters.Add(new SqlParameter("@FileName", strFileName));

        return ExecuteScalar(strProcedureName, alParameters).ToInt32();
    }

    public bool DeleteTreeMaster()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 14));
        alParameters.Add(new SqlParameter("@DocumentID", intDocumentID));
        alParameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
        return ExecuteNonQuery(strProcedureName, alParameters).ToInt32() > 0;
    }

    public string GetDocumentType()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
        return ExecuteScalar(strProcedureName, alParameters).ToStringCustom();
    }

    public bool SingleNodeDelete()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@Node", intNode));
        return ExecuteNonQuery(strProcedureName, alParameters).ToInt32() > 0;
    }

    public bool GetRecordCount()
    {
        alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@Node", intNode));
        return ExecuteNonQuery(strProcedureName, alParameters).ToInt32() > 0;
    }
}
