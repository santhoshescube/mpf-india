﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;


public class clsCurrencyReference : DL
{
	public clsCurrencyReference() { }

    private int iCurrencyID;
    private string sShortDescription;
    private string sDescription;
    private int iCountryID;
    private Int16 iScale;    

    public int CurrencyID
    {
        get { return iCurrencyID; }
        set { iCurrencyID = value; }
    }

    public string ShortName
    {
        get { return sShortDescription; }
        set { sShortDescription = value; }
    }

    public int Country
    {
        get { return iCountryID; }
        set { iCountryID = value; }
    }

    public string Currency
    {
        get { return sDescription; }
        set { sDescription = value; }
    }

    public Int16 DecimalPoint
    {
        get { return iScale; }
        set { iScale = value; }
    }

    public DataSet GetAllCurrency()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SAC"));

        return ExecuteDataSet("HRspCurrencyReference", arraylst);
    }


    public DataTable FillComboCountry()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataTable("HRspCurrencyReference", arraylst);
    }




    public DataTable GetaCurrency()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "SC"));
        arraylst.Add(new SqlParameter("@CurrencyId", CurrencyID));

        return ExecuteDataTable("HRspCurrencyReference", arraylst);
    }

    public int InsertCurrency()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "I"));
        arraylst.Add(new SqlParameter("@ShortDescription", ShortName));
        arraylst.Add(new SqlParameter("@Description", Currency));
        arraylst.Add(new SqlParameter("@CountryID", Country));
        arraylst.Add(new SqlParameter("@Scale", DecimalPoint));

        return Convert.ToInt32(ExecuteScalar("HRspCurrencyReference", arraylst));
    }

    public int UpdateCurrency()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "U"));
        arraylst.Add(new SqlParameter("@ShortDescription", ShortName));
        arraylst.Add(new SqlParameter("@Description", Currency));
        arraylst.Add(new SqlParameter("@CountryID", Country));
        arraylst.Add(new SqlParameter("@Scale", DecimalPoint));
        arraylst.Add(new SqlParameter("@CurrencyId", CurrencyID));

        return Convert.ToInt32(ExecuteScalar("HRspCurrencyReference", arraylst));
    }

    public string DeleteCurrency()
    {        
        try
        {
            ArrayList arraylst = new ArrayList();

            arraylst.Add(new SqlParameter("@Mode", "D"));
            arraylst.Add(new SqlParameter("@CurrencyId", CurrencyID));

            return Convert.ToString(ExecuteScalar("HRspCurrencyReference", arraylst));
        }
        catch (Exception e)
        {
            return e.ToString();
        }
    }

    public string CanDeleteCurrency()
    {
        try
        {
            ArrayList arraylst = new ArrayList();

            arraylst.Add(new SqlParameter("@Mode", "CD"));
            arraylst.Add(new SqlParameter("@CurrencyId", CurrencyID));

            return Convert.ToString(ExecuteScalar("HRspCurrencyReference", arraylst));
        }
        catch (Exception e)
        {
            return e.ToString();
        }
    }
}
