﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic ;

/// <summary>
/// Summary description for clsPerformanceEvaluation
/// </summary>
public class clsPerformanceEvaluation:DataLayer
{
	public clsPerformanceEvaluation()
	{
	
	}

    public int PeformanceInitiationID { get; set; }
    public int EmployeeID { get; set; }
    public int EvaluatorID { get; set; }
    public int EvaluationID { get; set; }
    public int EvaluationDetailsID { get; set; }
    public bool IsGrade { get; set; }
    public int PerformanceInitiationDetailID { get; set; }
    public int PerformanceInitiationEvaluationID { get; set; }
    public int TemplateID { get; set; }


    public List<clsGoalDetails> GoalList { get; set; }






    public bool SavePerformanceEvaluation()
    {
        try
        {
            BeginTransaction();

            //INSERTION TO PEFORMANCE EVALUATION MASTER
            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@Mode", 11));
            parameters.Add(new SqlParameter("@EvaluationDetailID", EvaluationDetailsID));
            parameters.Add(new SqlParameter("@PerformanceInitiationID", PeformanceInitiationID));
            parameters.Add(new SqlParameter("@IsGrade", IsGrade));
            parameters.Add(new SqlParameter("@TemplateID", TemplateID ));
            parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
            EvaluationID = ExecuteScalar("HRSpPerformance", parameters).ToInt32();

            //INSERTION TO PERFORMANCE EVALUATION DETAIL
            ArrayList parameters1 = new ArrayList();
            parameters1.Add(new SqlParameter("@Mode", 12));
            parameters1.Add(new SqlParameter("@PerformanceInitiationID", PeformanceInitiationID));
            parameters1.Add(new SqlParameter("@EvaluationID", EvaluationID));
            parameters1.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters1.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
            EvaluationDetailsID = ExecuteScalar("HRSpPerformance", parameters1).ToInt32();

            //INSERTION TO PERFORMANCE EVALUATION GOAL DETAILS


            foreach (clsGoalDetails objGoal in GoalList)
            {
                ArrayList parameters2 = new ArrayList();
                parameters2.Add(new SqlParameter("@Mode", 13));
                parameters2.Add(new SqlParameter("@PerformanceInitiationID", PeformanceInitiationID));
                parameters2.Add(new SqlParameter("@EvaluationDetailID", EvaluationDetailsID));
                parameters2.Add(new SqlParameter("@EmployeeID", EmployeeID));
                parameters2.Add(new SqlParameter("@TemplateID", TemplateID));
                parameters2.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
                parameters2.Add(new SqlParameter("@GoalID", objGoal.GoalID));
                parameters2.Add(new SqlParameter("@Mark", objGoal.Mark));
                if (IsGrade)
                    parameters2.Add(new SqlParameter("@GradeID", objGoal.GradeID));
                ExecuteScalar("HRSpPerformance", parameters2).ToInt32();
            }


            //UPDATE THE MARKS
            ArrayList parameters3 = new ArrayList();
            parameters3.Add(new SqlParameter("@Mode", 22));
            parameters3.Add(new SqlParameter("@PerformanceInitiationID", PeformanceInitiationID));
            parameters3.Add(new SqlParameter("@EmployeeID", EmployeeID));
            parameters3.Add(new SqlParameter("@TemplateID", TemplateID));
            parameters3.Add(new SqlParameter("@IsGrade", IsGrade));
            ExecuteScalar("HRSpPerformance", parameters3).ToInt32();



            UpdateEvaluationStatus(PerformanceInitiationEvaluationID);
            CommitTransaction();
            return true;
        }
        catch (Exception ex)
        {
            RollbackTransaction();
            return false;
        }
    }




    public void UpdateEvaluationStatus(int PerformanceInitiationEvaluationID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 14));
        parameters.Add(new SqlParameter("@PerformanceInitiationEvaluationID", PerformanceInitiationEvaluationID));
        ExecuteScalar("HRSpPerformance", parameters);

    }

    public static DataTable GetAllPendingEvaluation(int EvaluatorID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 8));
        parameters.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));

        return new DataLayer().ExecuteDataTable("HRSpPerformance", parameters);

    }


    public static DataTable GetAllCompletedEvaluation(int EvaluatorID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 8));
        parameters.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
        parameters.Add(new SqlParameter("@IsEvaluationCompleted", 1));
        parameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture() ));
        return new DataLayer().ExecuteDataTable("HRSpPerformance", parameters);
    }
    public static DataTable GetAllGoals(int PerformanceInitiationID, int TemplateID,int EvaluatorID,int EmployeeID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 9));
        parameters.Add(new SqlParameter("@TemplateID", TemplateID));
        parameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", PerformanceInitiationID));
        parameters.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpPerformance", parameters);
    }

    public static DataTable GetAllGrade()
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 10));
        parameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        return new DataLayer().ExecuteDataTable("HRSpPerformance", parameters);
    }


    public static int GetPendingEvaluationsByInitiationID(int PerformanceInitiationID,int EvaluatorID)
    {
        ArrayList parameters = new ArrayList();
        parameters.Add(new SqlParameter("@Mode", 25));
        parameters.Add(new SqlParameter("@PerformanceInitiationID", @PerformanceInitiationID));
        parameters.Add(new SqlParameter("@EvaluatorID", EvaluatorID));
        return new DataLayer().ExecuteScalar("HRSpPerformance", parameters).ToInt32();

    }
}

public class clsGoalDetails
{
    public int GoalID { get; set; }
    public decimal Mark { get; set; }
    public int GradeID { get; set; }
}
