﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;


/// <summary>
/// Summary description for clsMISEmployeeReport
/// </summary>
public class clsMISEmployeeReport : DL
{
    #region Variables

    private int iEmployeeID;
    private int iBranchID;
    private int iCompanyiD;
    private int iIncludeComapny;
    private int iEmployementType;
    private int iDepartmentID;
    private int iType;

    #endregion

    #region Properties

    public int EmployeeID { get { return iEmployeeID; } set { iEmployeeID = value; } }
    public int BranchID { get { return iBranchID; } set { iBranchID = value; } }
    public int CompanyID { get { return iCompanyiD; } set { iCompanyiD = value; } }
    public int IncludeCompany { get { return iIncludeComapny; } set { iIncludeComapny = value; } }
    public int EmployementType { get { return iEmployementType; } set { iEmployementType = value; } }
    public int DepartmentID { get { return iDepartmentID; } set { iDepartmentID = value; } }
    public int Type { get { return iType; } set { iType = value; } }

    private List<SqlParameter> sqlParameters = null;

    #endregion

    public clsMISEmployeeReport()
    {

    }
    public DataTable FillCompany()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 41));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillBranchs(int intCompanyID, int intMode) //Mode- 23,22
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", intMode));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillEmployee(int intCompanyID, int intBranchID, int intMode) //24-> -1 AS ALL,30-> 0 AS ALL
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", intMode));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillMonth(int intCompanyID, int intBranchID, int intEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 32));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));

        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetEmployeeProfile(int intCompanyID, int intBranchID, int intEmployeeID, int intIncludeCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 15));
        alParameters.Add(new SqlParameter("@CompanyId", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchId", intBranchID));
        alParameters.Add(new SqlParameter("@EmployeeId", intEmployeeID));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentId", -1));

        return ExecuteDataTable("PayrptWorksheet", alParameters);
    }
    public DataTable GetCompanyHeader(int intCompanyID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 44));
        alParameters.Add(new SqlParameter("@CompanyId", intCompanyID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillDepartment(int intMode)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", intMode));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillEmployementType()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 28));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillEmployeeWithDepAndEmpType(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployementType)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 43));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@EmploymentType", intEmployementType));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillEmployeeWithDep(int intCompanyID, int intBranchID, int intDepartmentID, int intMode, int intIncludeCmpny)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", intMode));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeCmpny));
        return ExecuteDataTable("spPayRptPayments", alParameters);
    }
    public DataTable GetSalaryStructure(int intCompany, int intbranchId, int intDepartment, int intEmploymentType, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intbranchId));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@EmploymentType", intEmploymentType));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }
    //
    public DataTable GetSalaryIDs(int intEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 40));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetSalaryHead(int intSalaryID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 10));
        alParameters.Add(new SqlParameter("@SalaryID", intSalaryID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetSalaryHeadDetail(int intSalaryID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@SalaryID", intSalaryID));
        return ExecuteDataTable("PayReports", alParameters);
    }

    public DataTable GetSalaryHistory(int intCompany, int intbranchId, int intDepartment, int intEmploymentType, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 6));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intbranchId));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@EmploymentType", intEmploymentType));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }


    public DataTable GetSalaryHistoryForSingleEmployee(int intEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 16));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));

        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillLeavePeriod(int intCompany, int intBranch, int intEmployee)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 29));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetLeaveSummary(int intCompany, int intBranch, string strPeriod, int intEmployee, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 8));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@FinYearStartDate", strPeriod));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetLeaveDetails(int intCompany, int intBranch, string strFromDate, string strToDate, int intEmployee, int intIncludeCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 9));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@From", strFromDate));
        alParameters.Add(new SqlParameter("@To", strToDate));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeCompany));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetSalaryAdvance(int intCompany, int intBranch, int intEmployee, string strFromDate, string strToDate, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 18));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@FromDate1", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate1", strToDate));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetLoanDetails(int intCompany, int intBranch, int intEmployee, string strFromDate, string strToDate, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 20));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@FromDate1", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate1", strToDate));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetAbsentReport(int intCompany, int intBranch, int intIncludeComp, int intEmployee, string strStartdate, string strEnddate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        //alParameters.Add(new SqlParameter("@Month", iMonth)) ;
        //alParameters.Add(new SqlParameter("@Year", iYear)) ;
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@StartDate", strStartdate));
        alParameters.Add(new SqlParameter("@EndDate", strEnddate));
        return ExecuteDataTable("PayRptAttendancesummary", alParameters);
    }
    public DataTable GetAttendanceSummaryReport(int intCompany, int intBranch, int intIncludeComp, int intEmployee, string strStartdate, string strEnddate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        alParameters.Add(new SqlParameter("@StartDate", strStartdate));
        alParameters.Add(new SqlParameter("@EndDate", strEnddate));
        return ExecuteDataTable("PayRptAttendancesummary", alParameters);
    }

    public DataTable GetPunchingDetailsReport(int intCompany, int intBranch, int intIncludeComp, int intEmployee, string strStartdate, string strEnddate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyId", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        alParameters.Add(new SqlParameter("@FromDate1", strStartdate));
        alParameters.Add(new SqlParameter("@ToDate1", strEnddate));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetWorkSheetAttendancePayment(int intCompany, int intBranch, int intDepartment, int intEmployee, int intMonth, int intYear, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 17));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillMonthforEmployeePayment(int intCompany, int intBranch, int intEmployee)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 34));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        return ExecuteDataTable("PayReports", alParameters);
    }

    public DataTable FillDateForEmployeePayment(int intCompany, int intBranch, int intEmployee, string strMonth, int intYear)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 35));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@Month", strMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable GetEmployeeYearlyPayment(int intCompany, int intBranch, int intDepartment, int intEmployee, string strFromDate, string strToDate, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Type", 0));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@FromDate1", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate1", strToDate));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayRptEmployeePayment", alParameters);
    }
    public DataTable GetEmployeePayment(int intCompany, int intBranch, int intMonth, int intYear, int intDepartment, int intIncludeComp, int intEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@Type", 1));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@EmpVenFlag", 1));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("PayRptEmployeePayment", alParameters);
    }
    public DataTable GetAllEmployeeAttSummary(int intCompany, int intBranch, int intMonth, int intYear, int intEmployee, string strDate, int intIncludeComp, string strStartdate, string strEnddate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@Date", strDate));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        alParameters.Add(new SqlParameter("@StartDate", strStartdate));
        alParameters.Add(new SqlParameter("@EndDate", strEnddate));
        return ExecuteDataTable("PayRptAttendancesummary", alParameters);
    }
    public DataTable GetAllEmployeeAttendance(int intType, int intCompany, int intBranch, int intMonth, int intYear, int intIncludeComp, int intEmployee, string strDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 0));
        alParameters.Add(new SqlParameter("@Type", intType));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@Date", strDate));
        return ExecuteDataTable("PayRptAttendancesummary", alParameters);
    }
    public DataTable GetEmployeeAttendance(int intType, int intCompany, int intBranch, int intMonth, int intYear, int intEmployee, string strDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@Type", intType));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@Date", strDate));
        return ExecuteDataTable("PayRptAttendancesummary", alParameters);
    }
    public DataTable GetAttendanceDetail(int intCompany, int intEmployee, string strAttDate)//Detailed Attendance Details
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@AttDate1", strAttDate));
        return ExecuteDataTable("PayRptAttendanceDetails", alParameters);
    }
    public DataTable GetDetailedAttendanceSummary(int intCompany, int intEmployee, int intDay, string strDate)//Detailed Attendance Summary
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 37));
        alParameters.Add(new SqlParameter("@CompanyId", intCompany));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        alParameters.Add(new SqlParameter("@DayID", intDay));
        alParameters.Add(new SqlParameter("@Date1", strDate));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataTable FillMonthForAttDetail(int intCompany, int intBranch, int intEmployee)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 38));
        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployee));
        return ExecuteDataTable("PayReports", alParameters);
    }

    public DataTable DisplayCompanyReportHeader(int intCompanyID) //CompnayReportHeader
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 31));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID)); ;
        return ExecuteDataTable("spPayModuleFunctions", alParameters);


    }

    public DataTable GetSalaryStructureSummary(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@EmploymentTypeID", intEmploymentTypeID));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("spPayRptSalaryStructure", alParameters);

    }

    public DataTable GetSalaryStructureHistorySummary(int intCompanyID, int intBranchID, int intDepartmentID, int intDesignationID, int intEmploymentTypeID, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@EmploymentTypeID", intEmploymentTypeID));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("spPayRptSalaryStructure", alParameters);

    }






    public DataTable GetSalaryAdvanceSummary(int intCompanyID, int intBranchID, int intEmployeeID, int intIncludeCompany, string strFromDate, string strToDate, int intLocationID)
    {

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate", strToDate));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeCompany));
        alParameters.Add(new SqlParameter("@LocationID", intLocationID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return ExecuteDataTable("spPayRptSalaryAdvanceSummary", alParameters);
    }
    public string GetPaymentIDList(int intYearInt, int intMonthInt, int intCompanyID, int intDepartmentID, int intDesignationID, int intEmployeeID,int intWorkStatusID)
    {
        DataTable DT;
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Type", 2));
        alParameters.Add(new SqlParameter("@intYearInt", intYearInt));
        alParameters.Add(new SqlParameter("@intMonthInt", intMonthInt));
        alParameters.Add(new SqlParameter("@intCompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@intDepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@intDesignationID", intDesignationID));
        alParameters.Add(new SqlParameter("@intEmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@WorkStatusID", intWorkStatusID));
        DT = ExecuteDataTable("spPayGetInfoWebformMain", alParameters);

        if (DT.Rows.Count > 0)
        {
            return DT.Rows[0][0].ToString().Trim();
        }
        else
        {
            return "";
        }

    }
    public DataTable datMaster(string strPayID, int intCompanyPayFlag, int intDivisionID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@PayID", strPayID));
        alParameters.Add(new SqlParameter("@CompanyPayFlag", intCompanyPayFlag));
        alParameters.Add(new SqlParameter("@DivisionID", intDivisionID));
        return ExecuteDataTable("spPayEmployeePaymentSalarySlipMaster", alParameters);
    }

    public DataTable datDetail(string strPayID, int intMonthInt, int intYearInt)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PayID", strPayID));
        prmSlip.Add(new SqlParameter("@Month", intMonthInt));
        prmSlip.Add(new SqlParameter("@year", intYearInt));
        return ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
    }
    public DataTable MdatLeaveDed(string strPayID)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PaymentID", strPayID));
        return ExecuteDataSet("spPayGetLeaveDetailSalarySlip", prmSlip).Tables[0];
    }
    public DataTable datDetailDed(string strPayID, int intMonthInt, int intYearInt)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PayID", strPayID));
        prmSlip.Add(new SqlParameter("@Month", intMonthInt));
        prmSlip.Add(new SqlParameter("@year", intYearInt));
        return ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
    }

    public DataTable MdatOTDed(string strPayID)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PaymentID", strPayID));
        return ExecuteDataSet("spPayOTPolicyDetailList", prmSlip).Tables[0];
    }
    public DataTable MdatHourDetail(string strPayID, int intMonthInt, int intYearInt)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PayID", strPayID));
        prmSlip.Add(new SqlParameter("@Month", intMonthInt));
        prmSlip.Add(new SqlParameter("@year", intYearInt));
        return ExecuteDataTable("spPayGetHourlyDetail", prmSlip);
    }
    public DataTable MdatSalarySlipWorkInfo(string strPayID)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PaymentID", strPayID));
        return ExecuteDataSet("spPayGetSalarySlipWorkInfo", prmSlip).Tables[0];
    }
    public DataTable MdatVacationDtls(string strPayID)
    {
        ArrayList prmSlip = new ArrayList();
        prmSlip.Add(new SqlParameter("@PaymentID", strPayID));
        return ExecuteDataTable("PayVacationDays", prmSlip);
    }
    public DataTable GetCompanyDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        return ExecuteDataTable("spRptEmployeeDeduction", alParameters);
    }
    public DataTable GetEmployeeDeductionDetails(int intEmployeeID, int intDeductionID, string strMonth, int intYear)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@AdditionDeductionID", intDeductionID));
        alParameters.Add(new SqlParameter("@Month", strMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        return ExecuteDataTable("spRptEmployeeDeduction", alParameters);
    }
    public DataTable GetDocumentDetails(int intDocumentTypeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 55));
        alParameters.Add(new SqlParameter("@DocumentTypeID", intDocumentTypeID));
        return ExecuteDataTable("PayReports", alParameters);
    }
    public DataSet GetDocForSingleEmp(int intEmpID, int intCompanyID)
    {
        ArrayList prmDoc = new ArrayList();
        prmDoc.Add(new SqlParameter("@Mode", "2"));

        prmDoc.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmDoc.Add(new SqlParameter("@CompanyID", intCompanyID));
        return ExecuteDataSet("spDocumentAll", prmDoc);
    }
    public DataTable GetDocForAllEmp(int intDocTypeID, bool blnIsPredefined, int intEmpID, int intCompID)
    {
        ArrayList prmDoc = new ArrayList();
        prmDoc.Add(new SqlParameter("@Mode", "1"));
        prmDoc.Add(new SqlParameter("@DocumentTypeID", intDocTypeID));
        prmDoc.Add(new SqlParameter("@EmployeeID", intEmpID));
        prmDoc.Add(new SqlParameter("@Predefined", blnIsPredefined));
        prmDoc.Add(new SqlParameter("@CompanyID", intCompID));
        return ExecuteDataTable("spDocumentAll", prmDoc);
    }

    public DataSet DisplayCompanyAssetReport(Int32 IntCompanyID, Int32 IntEmployeeID, Int32 IntAssetType, Int32 IntCompanyAsset, Int32 IntStatusType, String strFromDate, String strToDate)
    {
        ArrayList alParameters = new ArrayList();
        if (IntCompanyAsset > 0)
        {
            alParameters.Add(new SqlParameter("@Mode", 15));
        }
        else
        {
            alParameters.Add(new SqlParameter("@Mode", 14));
        }
        alParameters.Add(new SqlParameter("@CompanyID", IntCompanyID));
        alParameters.Add(new SqlParameter("@EmployeeID", IntEmployeeID));
        alParameters.Add(new SqlParameter("@BenefitTypeID", IntAssetType));
        alParameters.Add(new SqlParameter("@CompanyAssetID", IntCompanyAsset));
        alParameters.Add(new SqlParameter("@StatusTypeID", IntStatusType));
        alParameters.Add(new SqlParameter("@FromDate", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate", strToDate));
        return ExecuteDataSet("PayCompanyAssetOperations", alParameters);
    }
    //public DataTable FillMonthAndyear(int intCompanyID, int intBranchID, int intDepartmentID) 
    public DataTable FillMonthAndyear(int intCompanyID, int intBranchID, int intEmployeeID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 5));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        return ExecuteDataTable("spPayRptPayments", alParameters);
    }
    public DataTable GetEmployeePaymentMonthlyReport(int intCompany, int intBranch, int intDepartment, int intEmployeeID, int intMonth, int intYear, int intIncludeComp)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));

        alParameters.Add(new SqlParameter("@CompanyID", intCompany));
        alParameters.Add(new SqlParameter("@BranchID", intBranch));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@DepartmentID", intDepartment));
        alParameters.Add(new SqlParameter("@Month", intMonth));
        alParameters.Add(new SqlParameter("@Year", intYear));
        alParameters.Add(new SqlParameter("@EmpVenFlag", 1));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("spPayRptPayments", alParameters);
    }
    public DataTable GetFinancialYear(int intEmployeeID, DateTime dteFrom, DateTime dteTo)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));


        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@From", dteFrom));
        alParameters.Add(new SqlParameter("@To", dteTo));

        return ExecuteDataTable("spPayRptPayments", alParameters);
    }
    public DataTable GetSummaryPaymentsReport(int intCompanyID, int intBranchID, int intDepartmentID, int intEmployeeID, string strFromDate, string strToDate, int intIncludeComp)
    {

        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));

        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));

        alParameters.Add(new SqlParameter("@DepartmentID", intDepartmentID));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@FromDate1", strFromDate));
        alParameters.Add(new SqlParameter("@ToDate1", strToDate));

        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeComp));
        return ExecuteDataTable("spPayRptPayments", alParameters);
    }

    public static DataTable FillLocation(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 11));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", blnIncludeCompany));
        return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", alParameters);

    }

    public static DataTable FillEmployee(int intCompanyID, int intBranchID, bool blnIncludeCompany)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 12));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@IncludeComp", blnIncludeCompany));
        return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", alParameters);
    }



    #region ATTENDANCE REPORTS
    /////////////////////////////////////ATTENDANCE REPORT///////////////////////////////////////////
    public static DataTable GetSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int Month, int Year,string strDate,string strToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 3));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@Month", Month));
        alParameters.Add(new SqlParameter("@Year", Year));
        alParameters.Add(new SqlParameter("@Date",strDate ));
        alParameters.Add(new SqlParameter("@ToDate", strToDate));

        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
    }
    public static DataSet GetPunchingReport(int EmployeeID, DateTime AttendanceDate, DateTime? ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@Date", AttendanceDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        if (ToDate != null)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        return new DataLayer().ExecuteDataSet("spPayRptAttendace", alParameters);
    }
    public static DataTable GetReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID, int EmployeeID, int FilterTypeID, bool IsMonth, DateTime AttendanceDate, DateTime? ToDate, int Month, int Year, int ProjectID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FilterTypeID", FilterTypeID));
        alParameters.Add(new SqlParameter("@IsMonth", IsMonth));
        alParameters.Add(new SqlParameter("@ProjectID", ProjectID));
        alParameters.Add(new SqlParameter("@Date", AttendanceDate));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        if (ToDate != null)
            alParameters.Add(new SqlParameter("@ToDate", ToDate));
        else
            alParameters.Add(new SqlParameter("@ToDate", AttendanceDate));
        alParameters.Add(new SqlParameter("@Month", Month));
        alParameters.Add(new SqlParameter("@Year", Year));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
    }


    public static DataTable GetProjectSummaryAttendance(int Month, int Year)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 4));
        alParameters.Add(new SqlParameter("@Month", Month));
        alParameters.Add(new SqlParameter("@Year", Year));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        return new DataLayer().ExecuteDataTable("spPayRptAttendace", alParameters);
    }

    public static DataSet GetPunchingLogReport(string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AGEP"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
        alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
    }
    public static DataSet GetPunchingLogReport(int EmployeeID, string AttendanceDate, string AttendanceDateTo, int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AGEP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
        alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
    }

    public static DataSet GetPunchingLogReportGrid(int EmployeeID, string AttendanceDate, string AttendanceDateTo,
           int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AGEP"));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
        alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@ReportView", 2)); // Grid Format
        return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
    }

    public static DataSet GetPunchingLogReportGrid(string AttendanceDate, string AttendanceDateTo,
        int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int DesignationID, int WorkStatusID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AGEP"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@AttDate1", AttendanceDate));
        alParameters.Add(new SqlParameter("@AttDateTo1", AttendanceDateTo));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@DesignationID", DesignationID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@ReportView", 2)); // Grid Format
        return new DataLayer().ExecuteDataSet("HRSpAttDailyReport", alParameters);
    }


    public static DataTable GetEmployeeAttendnaceAllEmployee(int intMode, int intCompanyID, int intBranchID, int intEmployeeID, string strDate,string strToDate, int intIncludeCompany, int intLocationID)
    {
        //intMode=5 All Employee in one month
        //intMode=9 All Employee in one Day
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", intMode));
        alParameters.Add(new SqlParameter("@CompanyID", intCompanyID));
        alParameters.Add(new SqlParameter("@IncludeComp", intIncludeCompany));
        alParameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
        alParameters.Add(new SqlParameter("@BranchID", intBranchID));
        alParameters.Add(new SqlParameter("@StartDate", strDate));
        alParameters.Add(new SqlParameter("@EndDate", strToDate));
        alParameters.Add(new SqlParameter("@LocationID", intLocationID));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));
        return new DataLayer().ExecuteDataTable("spPayRptAttendanceSummary", alParameters);

    }




    //////////////////////////////////////////////////////////ATTENDANCE///////////////////////////////////////////////////////////////////////////
    #endregion


    #region LEAVE REPORTS

    public static DataTable GetLeaveReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, DateTime FromDate, DateTime ToDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 1));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FromDate", FromDate));
        alParameters.Add(new SqlParameter("@ToDate", ToDate));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        return new DataLayer().ExecuteDataTable("spPayRptLeaveSummary", alParameters);
    }

    public static DataTable GetLeaveSummaryReport(int CompanyID, int BranchID, bool IncludeCompany, int DepartmentID, int WorkStatusID, int EmployeeID, string FinYearStartDate)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", 2));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("@BranchID", BranchID));
        alParameters.Add(new SqlParameter("@IncludeCompany", IncludeCompany));
        alParameters.Add(new SqlParameter("@DepartmentID", DepartmentID));
        alParameters.Add(new SqlParameter("@WorkStatusID", WorkStatusID));
        alParameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alParameters.Add(new SqlParameter("@FinYearStartDate", FinYearStartDate));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        alParameters.Add(new SqlParameter("@UserID", new clsUserMaster().GetUserId()));

        return new DataLayer().ExecuteDataTable("spPayRptLeaveSummary", alParameters);
    }
    #endregion
}

