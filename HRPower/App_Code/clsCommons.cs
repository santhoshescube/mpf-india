﻿using System;

public class clsKeyValue
{
    public int Id { get; set; }
    public string Description { get; set; }

    public clsKeyValue()
    {
        this.Id = 0;
        this.Description = string.Empty;
    }
}
