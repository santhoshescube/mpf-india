﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for clsJobPost
/// </summary>
public class clsJobPost : DL
{
	public clsJobPost()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private int iVacancyId;
    private int iCompanyID;
    private int iDepartmentId;
    private int iDesignationId;
    private string sJobTitle;
    private string sJobCode;
    private int iNoOfVacancies;
    private int iJobTypeId;
    private string sResponsibilities;
    private string sProbationDetails;
    private string sOvertimeDetails;
    private string sLastDate;
    private int iMinAge;
    private int iMaxAge;
    private int iGender;
    private int iCareerLevelId;
    private decimal dMinExperience;
    private decimal dMaxExperience;
    private int iSalarayRangeId;
    private string sSkills;
    private string sDescription;
    private string sAccommodation;
    private string sVisaProcessing;
    private int iPerformanceTypeId;
    private int iPostedById;
    private bool bIsWalkin;
    private bool bIsHot;
    private string sRelatedDepartments;
    private int iVacancyStatusId;
    private string sAttachment;
    private int iDegreeID;
    private int iSpecializationId;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private string sSearchKey;
    private int iCandidateId;
    private int iModifiedByEmployeeID;
    private int iActivityTypeId;
    private string sActivityName;
    private int iInterviewTypeId;
    private string sStartDate;
    private string sEndDate;
    private int iActivityStatusId;
    private int iOrderNo;
    private int iAssignedEmployeeId;
    private long lActivityId;
    private int iEmployeeID;
    private bool bIsVisa;
    private bool bIsAccomodation;
    private int iLocationId;
    private string sLocation;
    private int iTotalMarks;
    private int iApprovedById;
    private string sRemark;
    private int iWorkLocationID;
    private string sLocationName;
    private string sLOAddress;
    private string sLOPhone;
    private string sLOEmail;


    public int VacancyId
    {
        get { return iVacancyId; }
        set { iVacancyId = value; }
    }
    public int CompanyID
    {
        get { return iCompanyID; }
        set { iCompanyID = value; }
    }
    public int DepartmentId
    {
        get { return iDepartmentId; }
        set { iDepartmentId = value; }
    }
    public int DesignationId
    {
        get { return iDesignationId; }
        set { iDesignationId = value; }
    }
    public string JobTitle
    {
        get { return sJobTitle; }
        set { sJobTitle = value; }
    }
    public string JobCode
    {
        get { return sJobCode; }
        set { sJobCode = value; }
    }
    public int NoOfVacancies
    {
        get { return iNoOfVacancies; }
        set { iNoOfVacancies = value; }
    }
    public int JobTypeId
    {
        get { return iJobTypeId; }
        set { iJobTypeId = value; }
    }
    public string Responsibilities
    {
        get { return sResponsibilities; }
        set { sResponsibilities = value; }
    }
    public string ProbationDetails
    {
        get { return sProbationDetails; }
        set { sProbationDetails = value; }
    }
    public string OvertimeDetails
    {
        get { return sOvertimeDetails; }
        set { sOvertimeDetails = value; }
    }
    public string LastDate
    {
        get { return sLastDate; }
        set { sLastDate = value; }
    }
    public int MinAge
    {
        get { return iMinAge; }
        set { iMinAge = value; }
    }
    public int MaxAge
    {
        get { return iMaxAge; }
        set { iMaxAge = value; }
    }
    public int Gender
    {
        get { return iGender; }
        set { iGender = value; }
    }
    public int CareerLevelId
    {
        get { return iCareerLevelId; }
        set { iCareerLevelId = value; }
    }
    public decimal MinExperience
    {
        get { return dMinExperience; }
        set { dMinExperience = value; }
    }
    public decimal MaxExperience
    {
        get { return dMaxExperience; }
        set { dMaxExperience = value; }
    }
    public int SalarayRangeId
    {
        get { return iSalarayRangeId; }
        set { iSalarayRangeId = value; }
    }
    public string Skills
    {
        get { return sSkills; }
        set { sSkills = value; }
    }
    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string Accommodation
    {
        get { return sAccommodation; }
        set { sAccommodation = value; }
    }
    public string VisaProcessing
    {
        get { return sVisaProcessing; }
        set { sVisaProcessing = value; }
    }
    public int PerformanceTypeId
    {
        get { return iPerformanceTypeId; }
        set { iPerformanceTypeId = value; }
    }
    public int PostedById
    {
        get { return iPostedById; }
        set { iPostedById = value; }
    }
    public int VacancyStatusId
    {
        get { return iVacancyStatusId; }
        set { iVacancyStatusId = value; }
    }
    public string Attachment
    {
        get { return sAttachment; }
        set { sAttachment = value; }
    }
    public int DegreeID
    {
        get { return iDegreeID; }
        set { iDegreeID = value; }
    }
    public int SpecializationId
    {
        get { return iSpecializationId; }
        set { iSpecializationId = value; }
    }
    public string RelatedDepartments
    {
        get { return sRelatedDepartments; }
        set { sRelatedDepartments = value; }
    }
    public bool IsWalkin
    {
        get { return bIsWalkin; }
        set { bIsWalkin = value; }
    }
    public bool IsHot
    {
        get { return bIsHot; }
        set { bIsHot = value; }
    }
    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }
    public string SearchKey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }
    public int CandidateId
    {
        get { return iCandidateId; }
        set { iCandidateId = value; }
    }
    public int ModifiedByEmployeeID
    {
        get { return iModifiedByEmployeeID; }
        set { iModifiedByEmployeeID = value; }
    }
    public string ActivityName
    {
        get { return sActivityName; }
        set { sActivityName = value; }
    }
    public int InterviewTypeId
    {
        get { return iInterviewTypeId; }
        set { iInterviewTypeId = value; }
    }
    public string StartDate
    {
        get { return sStartDate; }
        set { sStartDate = value; }
    }
    public string EndDate
    {
        get { return sEndDate; }
        set { sEndDate = value; }
    }
    public int ActivityStatusId
    {
        get { return iActivityStatusId ; }
        set { iActivityStatusId = value; }
    }
    public int OrderNo
    {
         get { return iOrderNo; }
         set { iOrderNo = value; }
    }
    public int ActivityTypeId
    {
         get { return iActivityTypeId; }
         set { iActivityTypeId = value; }
    }
    public long ActivityId
    {
        get { return lActivityId; }
        set { lActivityId = value; }
    }
    public int AssignedEmployeeId
    {
        get { return iAssignedEmployeeId; }
        set { iAssignedEmployeeId = value; }
    }
    public int EmployeeID
    {
        get { return iEmployeeID; }
        set { iEmployeeID = value; }
    }
    public bool IsAccomodation
    {
        get { return bIsAccomodation; }
        set { bIsAccomodation = value; }
    }
    public bool IsVisa
    {
        get { return bIsVisa; }
        set { bIsVisa = value; }
    }
    public int LocationId
    {
        get { return iLocationId; }
        set { iLocationId = value; }
    }
    public string Location
    {
        get { return sLocation; }
        set { sLocation = value; }
    }
    public int TotalMarks
    {
        get { return iTotalMarks; }
        set { iTotalMarks = value; }
    }
    public int ApprovedById
    {
        get { return iApprovedById; }
        set { iApprovedById = value; }
    }
    public string Remark
    {
        get { return sRemark; }
        set { sRemark = value; }
    }

    public int WorkLocationID
    {
        get { return iWorkLocationID; }
        set { iWorkLocationID = value; }
    }
    public string LocationName
    {
        get { return sLocationName; }
        set { sLocationName = value; }
    }
    public string LOAddress
    {
        get { return sLOAddress; }
        set { sLOAddress = value; }
    }
    public string LOPhone
    {
        get { return sLOPhone; }
        set { sLOPhone = value; }
    }

    public string LOEmail
    {
        get { return sLOEmail; }
        set { sLOEmail = value; }
    }


    public string QualificationCriteria { get; set; }

    public int InsertVacancyDetails()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationId));
        alparameters.Add(new SqlParameter("@JobTitle", sJobTitle));
        alparameters.Add(new SqlParameter("@JobCode", sJobCode));
        alparameters.Add(new SqlParameter("@NoOfVacancies", iNoOfVacancies));
        alparameters.Add(new SqlParameter("@JobTypeId", iJobTypeId));
        alparameters.Add(new SqlParameter("@Responsibilities", sResponsibilities));
        alparameters.Add(new SqlParameter("@ProbationDetails", sProbationDetails));
        alparameters.Add(new SqlParameter("@OvertimeDetails", sOvertimeDetails));
        alparameters.Add(new SqlParameter("@LastDate", sLastDate));



        if (iMinAge == -1)
            alparameters.Add(new SqlParameter("@MinAge", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MinAge", iMinAge));

        if(iMaxAge == -1)
            alparameters.Add(new SqlParameter("@MaxAge", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MaxAge", iMaxAge));

        if(iGender == -1)
            alparameters.Add(new SqlParameter("@Gender", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@Gender", iGender));

        //if(iCareerLevelId==-1)
        //    alparameters.Add(new SqlParameter("@CareerLevelId", DBNull.Value)); 
        //else
        //    alparameters.Add(new SqlParameter("@CareerLevelId", iCareerLevelId));

        if(dMinExperience == -1)
            alparameters.Add(new SqlParameter("@MinExperience", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MinExperience", dMinExperience));
       
        if(dMaxExperience == -1)
            alparameters.Add(new SqlParameter("@MaxExperience", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MaxExperience", dMaxExperience));

        if(iSalarayRangeId==-1)
            alparameters.Add(new SqlParameter("@SalarayRangeId", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@SalarayRangeId", iSalarayRangeId));

        alparameters.Add(new SqlParameter("@Skills", sSkills));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@IsWalkin", bIsWalkin));

        if(sRelatedDepartments == "")
            alparameters.Add(new SqlParameter("@RelatedDepartments", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@RelatedDepartments", sRelatedDepartments));

        alparameters.Add(new SqlParameter("@Accommodation", sAccommodation));
        alparameters.Add(new SqlParameter("@VisaProcessing", sVisaProcessing));
        alparameters.Add(new SqlParameter("@PerformanceTypeId", iPerformanceTypeId));
        alparameters.Add(new SqlParameter("@PostedById", iPostedById));
        alparameters.Add(new SqlParameter("@VacancyStatusId", 1));
        alparameters.Add(new SqlParameter("@Attachment", sAttachment));     
        alparameters.Add(new SqlParameter("@IsHot", "0"));
        alparameters.Add(new SqlParameter("@IsAccomodation", bIsAccomodation));
        alparameters.Add(new SqlParameter("@IsVisa", bIsVisa));
        if (iTotalMarks == -1)
            alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@TotalMarks", iTotalMarks));


        alparameters.Add(new SqlParameter("@QualificationCriteria", this.QualificationCriteria));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public void InsertVacancyDegreeDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "ID"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
        if (iSpecializationId == -1)
            alparameters.Add(new SqlParameter("@SpecializationId", DBNull.Value));
        else
        alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));

        ExecuteNonQuery(alparameters);
    }

    public int UpdateVacancyDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("CompanyID", iCompanyID));
        alparameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));
        alparameters.Add(new SqlParameter("@DesignationId", iDesignationId));
        alparameters.Add(new SqlParameter("@JobTitle", sJobTitle));
        alparameters.Add(new SqlParameter("@JobCode", sJobCode));
        alparameters.Add(new SqlParameter("@NoOfVacancies", iNoOfVacancies));
        alparameters.Add(new SqlParameter("@JobTypeId", iJobTypeId));
        alparameters.Add(new SqlParameter("@Responsibilities", sResponsibilities));
        alparameters.Add(new SqlParameter("@ProbationDetails", sProbationDetails));
        alparameters.Add(new SqlParameter("@OvertimeDetails", sOvertimeDetails));
        alparameters.Add(new SqlParameter("@LastDate", sLastDate));

        if (iMinAge == -1)
            alparameters.Add(new SqlParameter("@MinAge", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MinAge", iMinAge));

        if (iMaxAge == -1)
            alparameters.Add(new SqlParameter("@MaxAge", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MaxAge", iMaxAge));

        if (iGender == -1)
            alparameters.Add(new SqlParameter("@Gender", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@Gender", iGender));

        //if(iCareerLevelId==-1)
        //    alparameters.Add(new SqlParameter("@CareerLevelId", DBNull.Value));
        //else
        //    alparameters.Add(new SqlParameter("@CareerLevelId", iCareerLevelId));

        if (dMinExperience == -1)
            alparameters.Add(new SqlParameter("@MinExperience", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MinExperience", dMinExperience));

        if (dMaxExperience == -1)
            alparameters.Add(new SqlParameter("@MaxExperience", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@MaxExperience", dMaxExperience));

        if(iSalarayRangeId==-1)
            alparameters.Add(new SqlParameter("@SalarayRangeId", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@SalarayRangeId", iSalarayRangeId));

        alparameters.Add(new SqlParameter("@Skills", sSkills));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@IsWalkin", bIsWalkin));
        alparameters.Add(new SqlParameter("@Accommodation", sAccommodation));
        alparameters.Add(new SqlParameter("@VisaProcessing", sVisaProcessing));
        if(sRelatedDepartments=="")
            alparameters.Add(new SqlParameter("@RelatedDepartments", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@RelatedDepartments", sRelatedDepartments));
        alparameters.Add(new SqlParameter("@PerformanceTypeId", iPerformanceTypeId));
        alparameters.Add(new SqlParameter("@PostedById", iPostedById));
       
        alparameters.Add(new SqlParameter("@Attachment", sAttachment));
        alparameters.Add(new SqlParameter("@IsAccomodation", bIsAccomodation));
        alparameters.Add(new SqlParameter("@IsVisa", bIsVisa));
        if(iTotalMarks==-1)
            alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@TotalMarks", iTotalMarks));


        alparameters.Add(new SqlParameter("@VacancyStatusId", iVacancyStatusId));

        alparameters.Add(new SqlParameter("@QualificationCriteria", this.QualificationCriteria));

        return Convert.ToInt32(ExecuteScalar(alparameters));
    }

    public void UpdateVacancyStatus()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UV"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@VacancyStatusId", iVacancyStatusId));
        alparameters.Add(new SqlParameter("@ApprovedById", iApprovedById));

        if(iVacancyStatusId==2)
            alparameters.Add(new SqlParameter("@Remark", DBNull.Value));
        else
            alparameters.Add(new SqlParameter("@Remark", sRemark));

        ExecuteNonQuery(alparameters);
    }

    public DataSet GetVacancyDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GS"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public DateTime GetCandidateHireDate()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GCHD"));
        alParameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alParameters.Add(new SqlParameter("@CandidateId", iCandidateId));

        return Convert.ToDateTime(ExecuteScalar("HRspVacancyMaster", alParameters));
    }

    public DataSet FillForm()  //Auto filling
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "ANJ"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public DataSet FillDataList()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GA"));
        alparameters.Add(new SqlParameter("@PageIndex", iPageIndex));
        alparameters.Add(new SqlParameter("@PageSize", iPageSize));
        alparameters.Add(new SqlParameter("@SortExpression", sSortExpression));
        alparameters.Add(new SqlParameter("@SortOrder", sSortOrder));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public DataSet FillCompany()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fc"));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);

    }

    public DataTable FillTemplates()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "SAV"));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);

    }
    public DataSet FillVacancyStatus()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fvs"));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);

    }
    public DataSet FillQualification()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fd"));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);

    }
    public DataSet FillSpecialization()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fs"));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public DataSet FillPerformanceType()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fp"));
        
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public DataSet FillRelatedDepartments()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fm"));
        
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public DataSet FillDesignations()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "fv"));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public DataSet FillDataListDegree()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GD"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }
    public SqlDataReader GetRelatedDepartments()
    {
        ArrayList alparameters = new ArrayList();
       
        alparameters.Add(new SqlParameter("@Mode", "GR"));
        alparameters.Add(new SqlParameter("@DepartmentId", iDepartmentId));
        return ExecuteReader("HRspVacancyMaster", alparameters);   
    }

    public int GetRecordCount()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "RC"));
        alparameters.Add(new SqlParameter("@SearchKey", sSearchKey));
        alparameters.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
    }
    public SqlDataReader GetVacancyDegrees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GH"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        return ExecuteReader("HRspVacancyMaster", alparameters);
    }

    public DataSet SearchResult()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SC"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspVacancyMaster",alparameters);
    }

    public void InsertCandidateAssociationDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IVC"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@CandidateId", iCandidateId));
        alparameters.Add(new SqlParameter("@CandidateStatusId", 1));
        alparameters.Add(new SqlParameter("@ThroughWalkin", "False"));
        alparameters.Add(new SqlParameter("@ModifiedByEmployeeID",iModifiedByEmployeeID));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public DataTable GetCandidateAssociation()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CA"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@CandidateId", iCandidateId));

        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public void DeleteVacancyCandidate()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DVC"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@CandidateId", iCandidateId));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public bool CheckActivityInserted()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CE"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        int count = Convert.ToInt32(ExecuteScalar(alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public int InsertActivityDetails() /* in approve mode */
    {  
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IVA"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@ActivityTypeId", iActivityTypeId));
        alparameters.Add(new SqlParameter("@ActivityName", sActivityName));

        if (iActivityTypeId == 4)
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", 1));
            if(iTotalMarks==-1)
                alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
            else
                alparameters.Add(new SqlParameter("@TotalMarks", iTotalMarks));
        }
        else
        {
            alparameters.Add(new SqlParameter("@InterviewTypeId", -1));
            alparameters.Add(new SqlParameter("@TotalMarks", DBNull.Value));
        }

        alparameters.Add(new SqlParameter("@EndDate", sEndDate));
        alparameters.Add(new SqlParameter("@ActivityStatusId", 1));
        alparameters.Add(new SqlParameter("@OrderNo", iOrderNo)); 

        return Convert.ToInt32(ExecuteScalar(alparameters));    
    }

    public DataTable GetActivityType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FAT"));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public int AddActivityEmployeeDetails()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));

        return Convert.ToInt32(ExecuteScalar(alparameters));    
    }

    public void DeleteVacancy()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public void MakeAsHot()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "MV"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@IsHot", bIsHot));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public bool IsVacancyApproved()  //To check during deletion
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CVA"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public bool IsVacancyApprovedorCancelled()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CVC"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        int count = Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        if (count > 0)
            return true;
        else
            return false;
    }

    public string GetJobTitle()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GJn"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return Convert.ToString(ExecuteScalar("HRspVacancyMaster", alparameters));
    }

    public Table PrintVacancy(string sVacancyIds)
    {
        Table tblVacancy = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblVacancy.ID = "PrintVacancy";

        if (sVacancyIds.Contains(","))
            td.Text = CreateSelectedVacancyContent(sVacancyIds);
        else
            td.Text = CreateSingleVacancyContent(Convert.ToInt32(sVacancyIds));

        tr.Cells.Add(td);
        tblVacancy.Rows.Add(tr);

        return tblVacancy;
    }

    public string CreateSingleVacancyContent(int iVacancyId)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GS"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        DataTable dt = ExecuteDataTable("HRspVacancyMaster", alparameters);

        sb.Append("<table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr><td><table width='100%' border='0' style='font-family:Tahoma;'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight:bold;font-size:15px;' align='center' valign='center' width='475px' ><u>&nbsp;Vacancy Details&nbsp;</u></td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td style='font-weight:bold;font-size:12px;' align='left' valign='left' width='475px' ><br/>General Information</td>");
        sb.Append("</tr>");
        sb.Append("</table></td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr><td width='150px'>Company</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Company"]) + "</td></tr>");
        sb.Append("<tr><td >Department</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Department"]) + "</td></tr>");

        sb.Append("<tr><td>Designation</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Designation"]) + "</td></tr>");
        sb.Append("<tr><td>Job Code</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["JobCode"]) + "</td></tr>");
        sb.Append("<tr><td>Vacancy Title</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["JobTitle"]) + "</td></tr>");
        sb.Append("<tr><td>No. of vacancies</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["NoOfVacancies"]) + "</td></tr>");
        sb.Append("<tr><td>Job Type</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["JobType"]) + "</td></tr>");
        sb.Append("<tr><td>Responsibilities</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Responsibilities"]) + "</td></tr>");
        sb.Append("<tr><td>Probation Details</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ProbationDetails"]) + "</td></tr>");
        sb.Append("<tr><td>Overtime Details</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["OvertimeDetails"]) + "</td></tr>");
        sb.Append("<tr><td>Last Date</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LastDate"]) + "</td></tr>");
        sb.Append("</table></td></tr>");
        sb.Append("<tr><td style='font-weight:bold;font-size:12px;' align='left' valign='left'><br/>Required Qualifications</td></tr>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;'>");
        sb.Append("<tr><td width='150px'>Minimum Qualification</td><td>:</td><td>" + GetQualifications(Convert.ToInt32(dt.Rows[0]["VacancyId"])) + "</td></tr>");
        sb.Append("<tr><td>Min Age</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["MinAge"]) + "</td></tr>");
        sb.Append("<tr><td>Max Age</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["MaxAge"]) + "</td></tr>");
        sb.Append("<tr><td>Gender Preference</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Gender"]) + "</td></tr>");
        sb.Append("<tr><td>Career Level</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CareerLevel"]) + "</td></tr>");
        sb.Append("<tr><td>Min Experience</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["MinExperience"]) + "</td></tr>");
        sb.Append("<tr><td>Max Experience</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["MaxExperience"]) + "</td></tr>");
        sb.Append("<tr><td>Salary Range</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SalaryRange"]) + "</td></tr>");
        sb.Append("<tr><td>Skills</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Skills"]) + "</td></tr>");
        sb.Append("<tr><td>Description</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Description"]) + "</td></tr>");
        sb.Append("<tr><td>Related Departments</td><td>:</td><td>" + GetRelatedDept(Convert.ToString(dt.Rows[0]["RelatedDepartments"])) + "</td></tr>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();              
    }

    public string CreateSelectedVacancyContent(string sVacancyIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "PV"));
        alparameters.Add(new SqlParameter("@VacancyIds", sVacancyIds));

        DataTable dt = ExecuteDataTable("HRspVacancyMaster", alparameters);

        sb.Append("<table  width='100%' style='font-family:Tahoma;font-size:13px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='center'>Vacancy Details</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma' border='0'>");
        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'>Company</td><td width='200px'>Department</td><td width='150px'>Designation</td><td width='80px'>Job Code</td><td width='100px'>Vacancy Title</td><td width='120px'>No. of vacancies</td></tr>");
        sb.Append("<tr><td colspan='6'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["Company"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Department"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Designation"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["JobCode"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["JobTitle"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NoOfVacancies"]) + "</td></tr>");
        }
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string GetQualifications(int iVacancyId)
    {
        string sDegrees = string.Empty;
        VacancyId = iVacancyId;
        SqlDataReader sdr = GetVacancyDegrees();
        while (sdr.Read())
        {
            sDegrees += "," + sdr["Degree"].ToString() + " " + sdr["Specialization"].ToString();

        }
        return sDegrees.Remove(0, 1);
    }

    public string GetRelatedDept(string sRelatedDepartments)
    {   
        string sDeptNames = string.Empty;
        if (sRelatedDepartments != "")
        {
            string[] sDepts = Convert.ToString(sRelatedDepartments).Split(',');

            foreach (string s in sDepts)
            {
                DepartmentId = Convert.ToInt32(s);
                SqlDataReader sdr = GetRelatedDepartments();
                while (sdr.Read())
                {
                    sDeptNames += "," + sdr["Description"].ToString();
                }
            }
            return sDeptNames.Remove(0, 1);
        }
        else
            return sDeptNames;
    }
    public void InsertActivityEmployees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@AssignedEmployeeId", iAssignedEmployeeId));

        ExecuteNonQuery("HRspVacancyActivityDetails", alparameters);
    }

    public void DeleteActivityEmployee()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DAE"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        alparameters.Add(new SqlParameter("@EmployeeID", iEmployeeID));

        ExecuteNonQuery("HRspVacancyActivityDetails",alparameters);
    }

    public int InsertDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "ISD"));

            alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
            alparameters.Add(new SqlParameter("@Description", sDescription));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public int UpdateDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "USD"));

            alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
            alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));
            alparameters.Add(new SqlParameter("@Description", sDescription));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public int DeleteDegreeSpecializationReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "DSR"));
            alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));
            return ExecuteNonQuery("HRspVacancyMaster", alparameters);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public DataSet FillLocations()
    {
        ArrayList alparameters = new ArrayList();
        
        alparameters.Add(new SqlParameter("@Mode", "GL"));
        alparameters.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataSet("HRspLocationReference", alparameters);
    }

    public DataSet GetJobLocations()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SL"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public void InsertJobLocations()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "IL"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));
        alparameters.Add(new SqlParameter("@LocationId", iLocationId));

        ExecuteNonQuery(alparameters);
    }

    public int InsertLocationReference()      //LocationReference.ascx
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "INR"));
            alparameters.Add(new SqlParameter("@CompanyId", iCompanyID));
            alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));
            alparameters.Add(new SqlParameter("@LocationName", sLocationName));
            alparameters.Add(new SqlParameter("@Address", LOAddress));
            alparameters.Add(new SqlParameter("@Phone", LOPhone));
            alparameters.Add(new SqlParameter("@Email", LOEmail));
            return Convert.ToInt32(ExecuteScalar("HRspLocationReference", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public int UpdateLocationReference()      //LocationReference.ascx
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "UPD"));
        alparameters.Add(new SqlParameter("@CompanyId", iCompanyID));
        alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));
        alparameters.Add(new SqlParameter("@LocationName", sLocationName));
        alparameters.Add(new SqlParameter("@Address", LOAddress));
        alparameters.Add(new SqlParameter("@Phone", LOPhone));
        alparameters.Add(new SqlParameter("@Email", LOEmail));
        return Convert.ToInt32(ExecuteScalar("HRspLocationReference", alparameters));
    }

    public DataSet BindLocations()   //WorkLocationReference.ascx
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SLR1"));
        alparameters.Add(new SqlParameter("@CompanyId", iCompanyID));

        return ExecuteDataSet("HRspLocationReference", alparameters);
    }

    public bool IsLocationExists()  //WorkLocationReference.ascx
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "LE"));
        alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));

        return Convert.ToBoolean(ExecuteScalar("HRspLocationReference", alparameters));
    }
    public bool IsExists()  //WorkLocationReference.ascx
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "Exist"));
        alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));

        return Convert.ToBoolean(ExecuteScalar("HRspLocationReference", alparameters));
    }
    public SqlDataReader BindaLocation()  //WorkLocationReference.ascx
    { 
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "SLD1"));
        alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));

        return ExecuteReader("HRspLocationReference", alparameters);
    }

    public void DeleteaLocation()   //WorkLocationReference.ascx
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DL"));
        alparameters.Add(new SqlParameter("@WorkLocationID", iWorkLocationID));

        ExecuteNonQuery("HRspLocationReference", alparameters);
    }

    public void DeleteJobLocations()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DLV"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        ExecuteNonQuery(alparameters);
    }

    public DataSet GetParentCompanies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GPC"));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public int GetParent()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "PC"));
        alparameters.Add(new SqlParameter("@CompanyId", iCompanyID));

        return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
    }
    
    public void Begin()
    {
        BeginTransaction("HRspVacancyMaster");
    }
    public void Commit()
    {
        CommitTransaction();
    }
    public void RollBack()
    {
        RollbackTransaction();
    }

    public void DeleteAttachment()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "DR"));

        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public DataSet BindJobTrayVacancies()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "BV"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));

        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public DataSet BindAllCombos()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "FAC"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public DataTable  GetAllDepartments()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GAD"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllDesignations()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GADS"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllDepartment()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "FAC"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllEmploymentTypes()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GAET"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllCareerLevels()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GACL"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllSalaryRange()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GASR"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public DataTable GetAllInterviewTypes()
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "GAIT"));
        alparameters.Add(new SqlParameter("@ActivityId", lActivityId));
        return ExecuteDataTable("HRspVacancyMaster", alparameters);
    }

    public void CancelJobOpenings()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "CV"));
        alparameters.Add(new SqlParameter("@VacancyId", iVacancyId));

        ExecuteNonQuery("HRspVacancyMaster", alparameters);
    }

    public DataSet BindDegrees()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "BCD"));
        return ExecuteDataSet("HRspVacancyMaster", alparameters);
    }

    public int InsertDegreeReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "IDR"));

            alparameters.Add(new SqlParameter("@Description", sDescription));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public int UpdateDegreeReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "UDR"));

            alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
           
            alparameters.Add(new SqlParameter("@Description", sDescription));

            return Convert.ToInt32(ExecuteScalar("HRspVacancyMaster", alparameters));
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public int DeleteDegreeReference()
    {
        try
        {
            ArrayList alparameters = new ArrayList();

            alparameters.Add(new SqlParameter("@Mode", "DDR"));
            alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
            return ExecuteNonQuery("HRspVacancyMaster", alparameters);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public bool CheckDuplication()
    {
        ArrayList alparameters = new ArrayList();

        if (iDegreeID == -1)
            alparameters.Add(new SqlParameter("@Mode", "CD"));
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "CDD"));
            alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
        }
        
        alparameters.Add(new SqlParameter("@Description", sDescription));

        return Convert.ToBoolean(ExecuteScalar("HRspVacancyMaster", alparameters));
    }

    public bool CheckDuplicationSpecialization()
    {
        ArrayList alparameters = new ArrayList();

        if (iSpecializationId == 0)
            alparameters.Add(new SqlParameter("@Mode", "CSD"));
        else
        {
            alparameters.Add(new SqlParameter("@Mode", "CSDD"));
            alparameters.Add(new SqlParameter("@SpecializationId", iSpecializationId));
        }
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));
        alparameters.Add(new SqlParameter("@Description", sDescription));

        return Convert.ToBoolean(ExecuteScalar("HRspVacancyMaster", alparameters));
    }

    public bool InsertQualificationCriteria(string xmlData)
    {
        if (xmlData.Trim() == string.Empty)
            return false;

        object objRowsAffected = this.ExecuteScalar(new ArrayList { 
            new SqlParameter("@Mode", "IVDR"),
            new SqlParameter("@VacancyID", this.VacancyId),
            new SqlParameter("@xmlData", xmlData)
        });

        return objRowsAffected.ToInt32() > 0;
    }

    public DataTable GetQualificationLevels()
    {
        return this.ExecuteDataTable("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "GQL")
        });
    }

    public bool IsInterviewScheduled(int DepartmentID) //Interview scheduled against this department
    {
        return  Convert.ToInt32(this.ExecuteScalar("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "IDS"),
            new SqlParameter("@DepartmentID", DepartmentID) 
        })) > 0 ? false  :true  ;
    }
   
}
