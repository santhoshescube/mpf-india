﻿using System;
using System.Data;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsSpecializationReference
/// </summary>
public class clsSpecializationReference : DL
{
    public int SpecializationID { get; set; }
    public int DegreeID { get; set; }
    public string SpecializationName { get; set; }

    public clsSpecializationReference()
    {
    }

    /// <summary>
    /// For binding dropdown list
    /// </summary>
    /// <param name="DegreeID"></param>
    /// <returns></returns>
    public DataTable GetDegrees()
    {
        return this.ExecuteDataTable("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "GDSS")
        });
    }

    /// <summary>
    /// For binding gridview
    /// </summary>
    /// <param name="DegreeID"></param>
    /// <returns></returns>
    public DataTable GetSpecializationsByDegree(int DegreeID)
    {
        return this.ExecuteDataTable("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "GSBDG"),
            new SqlParameter("@DegreeID", DegreeID)
        });
    }

    /// <summary>
    /// Inserts new specialization 
    /// </summary>
    /// <returns>returns true if insersion is successful otherwise false</returns>
    public bool Insert()
    {
        object objStatus = this.ExecuteScalar("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "INS_SP"),
            new SqlParameter("@DegreeID", this.DegreeID),
            new SqlParameter("@Description", this.SpecializationName)
        });

        return objStatus.ToInt32() > 0;
    }

    /// <summary>
    /// Updates specialization.
    /// </summary>
    /// <returns>returns true if updation is successful otherwise returns false</returns>
    public bool Update()
    {
        object objRowsAffected = this.ExecuteScalar("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "UPD_SP"),
            new SqlParameter("@DegreeID", this.DegreeID),
            new SqlParameter("@Description", this.SpecializationName),
            new SqlParameter("@SpecializationID", this.SpecializationID)
        });

        return objRowsAffected.ToInt32() > 0;
    }

    /// <summary>
    /// Deletes a specialization
    /// </summary>
    /// <returns>returns true if deletion is successfull otherwise returns false</returns>
    public bool Delete()
    {
        object objRowsAffected = this.ExecuteScalar("HRspVacancyMaster", new ArrayList { 
            new SqlParameter("@Mode", "DEL_SP"),
            new SqlParameter("@SpecializationID", this.SpecializationID)
        });

        return objRowsAffected.ToInt32() > 0;
    }
}
