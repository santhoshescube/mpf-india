﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

public class clsPermissions:DL
{
    public clsPermissions() { }

    public void InsertModule(string sDescription)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IMO"));
        alParameters.Add(new SqlParameter("@Description", sDescription));
        alParameters.Add(new SqlParameter("@ProductId", 3));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }

    public void UpdateModule(int iModuleID, string sDescription)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UMO"));
        alParameters.Add(new SqlParameter("@ModuleID", iModuleID));
        alParameters.Add(new SqlParameter("@Description", sDescription));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }

    public DataSet GetAllModules()
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GMO"));
        alParameters.Add(new SqlParameter("@ProductId", 3));

        return ExecuteDataSet("HRspPermissions", alParameters);
    }

    public void DeleteModule(int iModuleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DMO"));
        alParameters.Add(new SqlParameter("@ModuleID", iModuleID));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }

    public string GetModuleName(int iModuleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GMN"));
        alParameters.Add(new SqlParameter("@ModuleID", iModuleID));

        return Convert.ToString(ExecuteScalar("HRspPermissions", alParameters));
    }

    public void InsertMenu(string sMenuName, string sFormName, int iModuleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "IME"));
        alParameters.Add(new SqlParameter("@MenuName", sMenuName)); 
        alParameters.Add(new SqlParameter("@FormName", sFormName)); 
        alParameters.Add(new SqlParameter("@ModuleID", iModuleID)); 
        alParameters.Add(new SqlParameter("@ProductId", 3));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }

    public void UpdateMenu(int iMenuID, string sMenuName, string sFormName)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "UME"));
        alParameters.Add(new SqlParameter("@MenuName", sMenuName));
        alParameters.Add(new SqlParameter("@FormName", sFormName));
        alParameters.Add(new SqlParameter("@MenuID", iMenuID));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }

    public DataSet GetAllMenus(int iModuleID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@ModuleID", iModuleID)); 

        return ExecuteDataSet("HRspPermissions", alParameters);
    }

    public void DeleteMenu(int iMenuID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "DME"));
        alParameters.Add(new SqlParameter("@MenuID", iMenuID));

        ExecuteNonQuery("HRspPermissions", alParameters);
    }
}
