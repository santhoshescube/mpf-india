﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsEmployeeSeperationReport
/// </summary>
public class clsEmployeeSeperationReport:DL
{
   
    public clsEmployeeSeperationReport()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet GetSeperatedEmployees(int EmployeeId, DateTime? FromDate, DateTime? ToDate, DateTime? OnDate)
    {
        try
        {
            if (EmployeeId != 0)
            {
                ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", "GSRBE"));
                alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));
                using (DataSet ds = ExecuteDataSet("HRspExitInterView", alParameters))
                {
                    if (ds.Tables.Count == 2)
                    {
                        ds.Tables[0].TableName = "EmployeeSettlement(P.F)";
                        ds.Tables[1].TableName = "EmployeeTakeOver(P.F)";

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables.Add(new clsReport().GetReportHeader());
                        }
                    }

                    else
                        if (ds.Tables.Count == 3)
                        {
                            ds.Tables[0].TableName = "EmployeeSettlement(H.R)";
                            ds.Tables[1].TableName = "EmployeeTakeOver(H.R)";
                            ds.Tables[2].TableName = "EmployeeProjectDetails(H.R)";

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                ds.Tables.Add(new clsReport().GetReportHeader());
                            }
                        }

                    return ds;
                }
            }
            else
            {
                ArrayList alParameters = new ArrayList();
                alParameters.Add(new SqlParameter("@Mode", "GSRBD"));
                if (OnDate != null)
                    alParameters.Add(new SqlParameter("@OnDate", OnDate));
                else
                {
                    alParameters.Add(new SqlParameter("@FromDate", FromDate));
                    alParameters.Add(new SqlParameter("@ToDate", ToDate));
                }
                using (DataSet ds = ExecuteDataSet("HRspExitInterView", alParameters))
                {
                    if (ds.Tables.Count == 1)
                    {
                        ds.Tables[0].TableName = "EmployeeSettlementByDate";

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables.Add(new clsReport().GetReportHeader());
                        }

                    }
                    return ds;
                }
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    


    public DataTable  GetAllCompanies()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GAC"));
       return  ExecuteDataTable("HRspExitInterView", alParameters);
    }

    public DataTable GetAllEmployeesByCompany(int CompanyId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GASEBC"));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyId));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }



}
