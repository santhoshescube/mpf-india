﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for clsCompany
/// </summary>
public class clsCompany : DL
{
    public clsCompany() { }

    private int iCompanyFlag;
    private int iCompanyID;
    private int iParentID;
    private bool bCompanyBranchIndicator;
    private string sName ;
    private string sShortName ;
    private string sPOBox ; 
    private string sRoad;
    private string sArea;
    private string sBlock;
    private string sCity;
    private int iProvinceID;
    private int iCountryID;
    private string sPrimaryEmail;
    private string sSecondaryEmail;
    private string sContactPerson;
    private int iContactPersonDesignationID;
    private string sContactPersonPhone;
    private string sPABXNumber;
    private string sPANNumber;
    private string sWebSite;
    private int iCompanyIndustryID;
    private int iCompanyTypeID;
    private byte[] sLogoFile;
    private string sOtherInfo;
    private int iCurrencyId;
    private string sFinYearStartDate1;
    private string sWorkingDaysInMonth;
    private int iOffDayID;
    private string sBookStartDate1;
    private bool bIsMonth;
    private string sEPID;
    private int iPageIndex;
    private int iPageSize;
    private string sSortExpression;
    private string sSortOrder;
    private string sSearchKey;
    private int iBankNameId;
    private string sAccountNo;
    private int iSalaryDay;
 
   
    public int CompanyFlag
    {
        get { return iCompanyFlag; }
        set { iCompanyFlag = value; }
    }

    public int CompanyID
    {
        get { return iCompanyID; }
        set { iCompanyID = value; }
    }

    public int ParentID
    {
        get { return iParentID; }
        set { iParentID = value; }
    }

    public bool CompanyBranchIndicator
    {
        get { return bCompanyBranchIndicator; }
        set { bCompanyBranchIndicator = value; }
    }

    public string Name
    {
        get { return sName; }
        set { sName = value; }
    }

    public string ShortName
    {
        get { return sShortName; }
        set { sShortName = value; }
    }

    public string POBox
    {
        get { return sPOBox; }
        set { sPOBox = value; }
    }

    public string Road
    {
        get { return sRoad; }
        set { sRoad = value; }
    }

    public string Area
    {
        get { return sArea; }
        set { sArea = value; }
    }

    public string Block
    {
        get { return sBlock; }
        set { sBlock = value; }
    }

    public string City
    {
        get { return sCity; }
        set { sCity = value; }
    }

    public int ProvinceID
    {
        get { return iProvinceID; }
        set { iProvinceID = value; }
    }

    public int CountryID
    {
        get { return iCountryID; }
        set { iCountryID = value; }
    }

    public string PrimaryEmail
    {
        get { return sPrimaryEmail; }
        set { sPrimaryEmail = value; }
    }

    public string SecondaryEmail
    {
        get { return sSecondaryEmail; }
        set { sSecondaryEmail = value; }
    }

    public string ContactPerson
    {
        get { return sContactPerson; }
        set { sContactPerson = value; }
    }

    public int ContactPersonDesignationID
    {
        get { return iContactPersonDesignationID; }
        set { iContactPersonDesignationID = value; }
    }

    public string ContactPersonPhone
    {
        get { return sContactPersonPhone; }
        set { sContactPersonPhone = value; }
    }

    public string PABXNumber
    {
        get { return sPABXNumber; }
        set { sPABXNumber = value; }
    }
    public string PANNumber
    {
        get { return sPANNumber; }
        set { sPANNumber = value; }
    }

    public string WebSite
    {
        get { return sWebSite; }
        set { sWebSite = value; }
    }

    public int CompanyIndustryID
    {
        get { return iCompanyIndustryID; }
        set { iCompanyIndustryID = value; }
    }

    public int CompanyTypeID
    {
        get { return iCompanyTypeID; }
        set { iCompanyTypeID = value; }
    }

    public byte[] LogoFile
    {
        get { return sLogoFile; }
        set { sLogoFile = value; }
    }

    public string OtherInfo
    {
        get { return sOtherInfo; }
        set { sOtherInfo = value; }
    }

    public int CurrencyId
    {
        get { return iCurrencyId; }
        set { iCurrencyId = value; }
    }

    public string FinYearStartDate1
    {
        get { return sFinYearStartDate1; }
        set { sFinYearStartDate1 = value; }
    }

    public string WorkingDaysInMonth
    {
        get { return sWorkingDaysInMonth; }
        set { sWorkingDaysInMonth = value; }
    }

    public int OffDayID
    {
        get { return iOffDayID; }
        set { iOffDayID = value; }
    }

    public int SalaryDay
    {
        get { return iSalaryDay; }
        set { iSalaryDay = value; }
    }

    public string BookStartDate1
    {
        get { return sBookStartDate1; }
        set { sBookStartDate1 = value; }
    }

    public bool IsMonth
    {
        get { return bIsMonth; }
        set { bIsMonth = value; }
    }

    public string EPID
    {
        get { return sEPID; }
        set { sEPID = value; }
    }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }

    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public string SortExpression
    {
        get { return sSortExpression; }
        set { sSortExpression = value; }
    }

    public string SortOrder
    {
        get { return sSortOrder; }
        set { sSortOrder = value; }
    }

    public string SearchKey
    {
        get { return sSearchKey; }
        set { sSearchKey = value; }
    }

    public int BankNameId
    {
        get { return iBankNameId; }
        set { iBankNameId = value; }
    }

    public string AccountNo
    {
        get { return sAccountNo; }
        set { sAccountNo = value; }
    }

    public DataSet GetAllCompanies()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "All"));
        arraylst.Add(new SqlParameter("@PageIndex", iPageIndex));
        arraylst.Add(new SqlParameter("@PageSize", iPageSize));
        arraylst.Add(new SqlParameter("@SortExpression", sSortExpression));
        arraylst.Add(new SqlParameter("@SortOrder", sSortOrder));
        arraylst.Add(new SqlParameter("@SearchKey", sSearchKey.EscapeUnsafeChars()));
        arraylst.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));


        return ExecuteDataSet("HRspCompanyMasterObtain", arraylst);
    }

    public byte[] GetLogo()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode","GI"));
        arraylst.Add(new SqlParameter("@CompanyID",iCompanyID));

        object Logo = ExecuteScalar("HRspCompanyMasterObtain", arraylst);

        if (Logo != DBNull.Value)
            return (byte[])Logo;
        else
            return null;
    }

    public DataTable FillWeekdays()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FWD"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public DataTable FillCompany()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FC"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public DataTable FillCurrency(int iCompanyID)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCY"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public DataSet FillAllCombos()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FAC"));
        return ExecuteDataSet("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable  GetAllCountries()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCC"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable GetAllIndustries()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCI"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable GetAllProvince()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCP"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable GetAllCompanyTypes()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCCT"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable GetAllDesignationType()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FCD"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }


    public DataTable FillBank()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "FBN"));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public DataTable SelectedCompany()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "One"));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public string GetParentCompanyName()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPC"));
        arraylst.Add(new SqlParameter("@CompanyID", ParentID));

        return Convert.ToString(ExecuteScalar("HRspCompanyMasterObtain", arraylst));
    }

    public int SubmitCompanyDetails()
    {
        ArrayList arraylst = new ArrayList();

        if(iCompanyFlag == 1)
            arraylst.Add(new SqlParameter("@Mode", "2"));
        else if (iCompanyFlag == 2)
            arraylst.Add(new SqlParameter("@Mode", "3"));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
        arraylst.Add(new SqlParameter("@ParentID", ParentID));
        arraylst.Add(new SqlParameter("@IsBranch", CompanyBranchIndicator));
        arraylst.Add(new SqlParameter("@CompanyName", Name));
        arraylst.Add(new SqlParameter("@ShortName", ShortName));
        arraylst.Add(new SqlParameter("@POBox", POBox));
        arraylst.Add(new SqlParameter("@Road", Road));
        arraylst.Add(new SqlParameter("@Area", Area));
        arraylst.Add(new SqlParameter("@Block", Block));
        arraylst.Add(new SqlParameter("@City", City));
        if (ProvinceID == -1)
            arraylst.Add(new SqlParameter("@ProvinceID", null));
        else
            arraylst.Add(new SqlParameter("@ProvinceID", ProvinceID));

        arraylst.Add(new SqlParameter("@CountryID", CountryID));
        arraylst.Add(new SqlParameter("@PrimaryEmail", PrimaryEmail));
        arraylst.Add(new SqlParameter("@SecondaryEmail", SecondaryEmail));
        arraylst.Add(new SqlParameter("@ContactPerson", ContactPerson));
        if (ContactPersonDesignationID == -1)
            arraylst.Add(new SqlParameter("@DesignationID", null));
        else
            arraylst.Add(new SqlParameter("@DesignationID", ContactPersonDesignationID));

        arraylst.Add(new SqlParameter("@ContactPersonPhone", ContactPersonPhone));
        arraylst.Add(new SqlParameter("@FaxNumber", PABXNumber));
        arraylst.Add(new SqlParameter("@PanNumber", PANNumber));
        arraylst.Add(new SqlParameter("@WebSite", WebSite));
        if (CompanyIndustryID!=-1)
        arraylst.Add(new SqlParameter("@CompanyIndustryID", CompanyIndustryID));
        if (CompanyTypeID == -1)
            arraylst.Add(new SqlParameter("@CompanyTypeID", null));
        else
            arraylst.Add(new SqlParameter("@CompanyTypeID", CompanyTypeID));

        if (LogoFile != null)
            arraylst.Add(new SqlParameter("@LogoFile", LogoFile));

        arraylst.Add(new SqlParameter("@OtherInfo", OtherInfo));
        arraylst.Add(new SqlParameter("@CurrencyId", CurrencyId));
        arraylst.Add(new SqlParameter("@FinYearStartDate1", FinYearStartDate1));
        arraylst.Add(new SqlParameter("@WorkingDaysInMonth", WorkingDaysInMonth));
        if (OffDayID == -1)
            arraylst.Add(new SqlParameter("@DayID", null));
        else
            arraylst.Add(new SqlParameter("@DayID", OffDayID));
        if (SalaryDay == 0)
            arraylst.Add(new SqlParameter("@SalaryDay", null));
        else
            arraylst.Add(new SqlParameter("@SalaryDay", SalaryDay));
        arraylst.Add(new SqlParameter("@BookStartDate1", BookStartDate1));
        arraylst.Add(new SqlParameter("@IsMonth", IsMonth));
        arraylst.Add(new SqlParameter("@EPID", EPID));

        SqlParameter sqlPAramOut = new SqlParameter( "@ReturnValue",SqlDbType.Int);

        sqlPAramOut.Direction = ParameterDirection.ReturnValue;
        arraylst.Add(sqlPAramOut);

        object iCId ;

        ExecuteNonQuery("spCompany", arraylst,out iCId);

        return Convert.ToInt32(iCId);
    }

    public int IsCompanyExists()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CE"));
        arraylst.Add(new SqlParameter("@CompanyID", CompanyID));
        arraylst.Add(new SqlParameter("@CompanyName", Name));

        return Convert.ToInt32(ExecuteScalar("HRspCompanyMasterObtain",arraylst));
    }

    public int GetCompanyCount()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GCC"));
        arraylst.Add(new SqlParameter("@SearchKey", sSearchKey));
        arraylst.Add(new SqlParameter("@UserId", new clsUserMaster().GetUserId()));

        return Convert.ToInt32(ExecuteScalar("HRspCompanyMasterObtain", arraylst));
    }

    public Table PrintCompany(string sCompanyIds)
    {
        try
        {
            Table tblCompany = new Table();
            TableRow tr = new TableRow();
            TableCell td = new TableCell();

            tblCompany.ID = "PrintCompany";

            if (sCompanyIds.Contains(","))
            {
                td.Text = CreateSelectedCompaniesContent(sCompanyIds);
            }
            else if (sCompanyIds != "")
            {
                td.Text = CreateSingleCompanyContent(Convert.ToInt32(sCompanyIds));
            }

            tr.Cells.Add(td);
            tblCompany.Rows.Add(tr);

            return tblCompany;
        }
        catch
        {
            Table tblCompany = new Table();
            return tblCompany;
        }
    }

    public string CreateSingleCompanyContent(int iCompanyId)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode","One"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyId));

        DataSet ds = ExecuteDataSet("HRspCompanyMasterObtain", arraylst);

        DataTable dt = ds.Tables[0];
        DataTable dtBank = ds.Tables[1];

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");

        if (Convert.ToString(dt.Rows[0]["LogoFile"]) != string.Empty)
        {
            string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];

            sb.Append("<td width='60px'><img src='" + path + "thumbnail.aspx?FromDB=true&type=Company&Width=70&CompanyId=" + iCompanyId + "&t=" + DateTime.Now.Ticks + "'/></td>");
        }
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["Name"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>General Information</td></tr>");
        sb.Append("<tr><td width='150px'>Name</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Name"]) + "</td></tr>");
        sb.Append("<tr><td>Employer Code</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["EPID"]) + "</td></tr>");
        sb.Append("<tr><td>Short Name</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ShortName"]) + "</td></tr>");
        sb.Append("<tr><td>Company Industry</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Industry"]) + "</td></tr>");
        sb.Append("<tr><td>Company Type</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CompanyType"]) + "</td></tr>");
        sb.Append("<tr><td>Company or Branch</td><td>:</td><td>" + ((Convert.ToBoolean(dt.Rows[0]["CompanyBranchIndicator"])) ? "Company" : "Branch") + "</td></tr>");
        sb.Append("<tr><td>Telephone</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ContactPersonPhone"]) + "</td></tr>");
        sb.Append("<tr><td>Fax</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["FaxNumber"]) + "</td></tr>");
        sb.Append("<tr><td>PAN Number</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PanNumber"]) + "</td></tr>");
        sb.Append("<tr><td>WebSite</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["WebSite"]) + "</td></tr>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Contact Information</td></tr>");
        sb.Append("<tr><td>Contact Person Name</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ContactPerson"]) + "</td></tr>");
        sb.Append("<tr><td>Contact Person Designation</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Designation"]) + "</td></tr>");
        sb.Append("<tr><td>Primary Email</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PrimaryEmail"]) + "</td></tr>");
        sb.Append("<tr><td>Secondary Email</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SecondaryEmail"]) + "</td></tr>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Postal Address</td></tr>");
        sb.Append("<tr><td>PO Box</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["POBox"]) + "</td></tr>");
        sb.Append("<tr><td>Street</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Road"]) + "</td></tr>");
        sb.Append("<tr><td>Area</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Area"]) + "</td></tr>");
        sb.Append("<tr><td>Block</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Block"]) + "</td></tr>");
        sb.Append("<tr><td>City</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["City"]) + "</td></tr>");
        sb.Append("<tr><td>Province</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Province"]) + "</td></tr>");
        sb.Append("<tr><td>Country</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>Other Information</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["OtherInfo"]) + "</td></tr>");        
        sb.Append("<tr><td>Currency</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Currency"]) + "</td></tr>");
        sb.Append("<tr><td>Daily Pay Based On</td><td>:</td><td>" + (Convert.ToString(dt.Rows[0]["IsMonth"]) == "True" ? "Month" : "Year") + "</td></tr>");
        sb.Append("<tr><td>Working Days</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["WorkingDaysInMonth"]) + "</td></tr>");
        sb.Append("<tr><td>Off Days</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["WeekDay"]) + "</td></tr>");
        sb.Append("<tr><td>Salary Day</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SalaryDay"]) + "</td></tr>");
        sb.Append("<tr><td>Financial Year Start</td><td>:</td><td>" + (Convert.ToString(dt.Rows[0]["FinYearStartDate"])=="01 Jan 1900"?string.Empty:Convert.ToString(dt.Rows[0]["FinYearStartDate"])) + "</td></tr>");
        sb.Append("<tr><td>Book Start Date</td><td>:</td><td>" + (Convert.ToString(dt.Rows[0]["BookStartDate"]) == "01 Jan 1900" ? string.Empty : Convert.ToString(dt.Rows[0]["BookStartDate"])) + "</td></tr>");

        if (dtBank.Rows.Count > 0)
        {
            sb.Append("<tr><td width='150px' valign='top'>Bank Details</td><td valign='top'>:</td>");
            sb.Append("<td>");
            sb.Append("<table border='0' cellspacing='0' cellpadding='0' style='font-size:11px;'>");
            sb.Append("<tr><td width='250'><b>Bank Name</b></td><td><b>Account Number</b></td></tr>");
            for (int i = 0; i < dtBank.Rows.Count; i++)
            {
                sb.Append("<tr><td>" + Convert.ToString(dtBank.Rows[i]["Bank"]) + "</td><td>" + Convert.ToString(dtBank.Rows[i]["AccountNo"]) + "</td></tr>");
            }
            sb.Append("</table>");
        }
        sb.Append("</td>");   
        sb.Append("</table>");        
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();        
    }

    public string CreateSelectedCompaniesContent(string sCompanyIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "PC"));
        arraylst.Add(new SqlParameter("@CompanyIds", sCompanyIds));

        DataTable dt = ExecuteDataTable("HRspCompanyMasterObtain", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Company Information</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font: 11px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr style='font-weight:bold;'><td width='100px'>Company Name</td><td width='150px'>Address</td><td width='80px'>Province</td><td width='80px'>Country</td><td width='100px'>Website</td></tr>");
        sb.Append("<tr><td colspan='5'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>"+Convert.ToString(dt.Rows[i]["Name"])+"</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["POBox"]) + (Convert.ToString(dt.Rows[i]["Road"]) == string.Empty ? string.Empty : ","));
            sb.Append(Convert.ToString(dt.Rows[i]["Road"]) + (Convert.ToString(dt.Rows[i]["Area"]) == string.Empty ? string.Empty : ",") + Convert.ToString(dt.Rows[i]["Area"]) + (Convert.ToString(dt.Rows[i]["Block"]) == string.Empty ? string.Empty : ","));
            sb.Append(Convert.ToString(dt.Rows[i]["Block"]) + (Convert.ToString(dt.Rows[i]["City"]) == string.Empty ? string.Empty : ",") + Convert.ToString(dt.Rows[i]["City"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Province"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["WebSite"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }


    public DataTable GetCompanies() // Company name filling
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GAC"));

        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public DataSet GetAllCompanyInfo() // Get all company details info
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GACR"));
        arraylst.Add(new SqlParameter("@CompanyId", iCompanyID));

        return ExecuteDataSet("HRspCompanyMasterObtain", arraylst);
    }

    public DataTable GetCompanyBanks()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GCB"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));

        return ExecuteDataTable("HRspCompanyMasterObtain", arraylst);
    }

    public void InsertCompanyBankDetails(bool bIsInsert,int iAccountId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "IB"));
        arraylst.Add(new SqlParameter("@IsInsert", bIsInsert));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));
        arraylst.Add(new SqlParameter("@BankNameID", iBankNameId));
        arraylst.Add(new SqlParameter("@AccountNo", sAccountNo));
        arraylst.Add(new SqlParameter("@CompanyAccID", iAccountId));

        ExecuteNonQuery(arraylst);
    }

    public bool CheckAccIdforEmp(int iAccountId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "ECA"));
        arraylst.Add(new SqlParameter("@CompanyAccID", iAccountId));

        return (Convert.ToInt32(ExecuteScalar("HRspCompanyMasterObtain", arraylst)) == 1 ? true : false);
    }

    public bool CheckBankExistforCompany()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "BE"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));
        arraylst.Add(new SqlParameter("@BankNameID", iBankNameId));

        return (Convert.ToInt32(ExecuteScalar("HRspCompanyMasterObtain", arraylst)) == 1 ? true : false);
    }

    public void DeleteBank(int iAccountId)
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "DCB"));
        arraylst.Add(new SqlParameter("@CompanyAccID", iAccountId));

        ExecuteNonQuery("HRspCompanyMasterObtain", arraylst);
    }

    public bool CompanyHasLeavePolicy()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CHL"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));

        return (Convert.ToInt32(ExecuteScalar("HRspCompanyMasterObtain", arraylst)) == 1 ? true : false);
    }

    public DateTime GetFinancialYear()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GF"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));

        return Convert.ToDateTime(ExecuteScalar("HRspCompanyMasterObtain", arraylst));
    }

    public void Begin()
    {
        BeginTransaction("HRspCompanyMasterObtain");
    }

    public void Commit()
    {
        CommitTransaction();
    }

    public void RollBack()
    {
        RollbackTransaction();
    }


    public bool CheckBranchExists()
    {
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "CBE"));
        arraylst.Add(new SqlParameter("@CompanyID", iCompanyID));

        return Convert.ToBoolean(ExecuteScalar("HRspCompanyMasterObtain", arraylst));
    }

    public DataTable GetPolicyPermissions(int IntRoleID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@Mode", "ROL"));
        alparameters.Add(new SqlParameter("@RoleIID", IntRoleID));
        return ExecuteDataTable("HRspCompanyMasterObtain", alparameters);
    }

    public DataTable GetUserCompanies(int EmployeeID)
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@EmployeeID", EmployeeID));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataTable("HRSpGetUserCompanyDetails", alparameters);
    }

    public DataTable GetCompaniesbyUser(int UserID )
    {
        ArrayList alparameters = new ArrayList();
        alparameters.Add(new SqlParameter("@UserID", UserID ));

        return ExecuteDataTable("HRSpGetUserCompanyDetails", alparameters);
    }
}
