﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient; 
using System.Collections.Generic;
using System.Collections;

/// <summary>
/// Created By : Rajesh.R
/// Created On : 07-07-2011
/// </summary>
public class clsResignation:DL
{
   
#region Properties

    public int RequestedBy{get;set;}

    public DateTime RequestedDate{get;set;}

    public int NoticePeriod{get;set;}

    public string Reason{get;set;}

    public string Message{get;set;}

    public List<Int64> RequestedTo { get; set; }

    public string RequestedToV { get; set; }

    public int StatusId { get; set; }
#endregion

#region Constructor
    public clsResignation()
    {
        RequestedTo = new List<long>();
        //
        // TODO: Add constructor logic here
        //
    }
#endregion


    public DataTable GetEmailIds(string sRequestedTo)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GME"));
        alParameters.Add(new SqlParameter("@RequestedTo", sRequestedTo));

        return ExecuteDataTable("HRspLeaveRequest", alParameters);
    }


    //public DataTable GetEmailIds()
    //{
    //    ArrayList alParameters = new ArrayList();
    //    alParameters.Add(new SqlParameter("@Mode", "GME"));
    //    alParameters.Add(new SqlParameter("@RequestedTo", RequestedToV));

    //    return ExecuteDataTable("HRspLeaveRequest", alParameters);
    //}

    public DataTable GetResignationDetailsByRequestId(int RequestId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRRDBID"));
        alParameters.Add(new SqlParameter("@ResignationId", RequestId));
        return ExecuteDataTable("HRspExitInterView", alParameters);
    }


    public string GetEmployeeName(int EmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GN"));
        alParameters.Add(new SqlParameter("@EmployeeId", EmployeeId));

        string sName = Convert.ToString(ExecuteScalar("HRspLeaveRequest", alParameters));
        return sName;
    }


    public string GetRequestedEmployeeEmail(int ResignationId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GREMID"));
        alParameters.Add(new SqlParameter("@ResignationId", ResignationId));

        string sName = Convert.ToString(ExecuteScalar("HRspExitInterView", alParameters));
        return sName;
    }


    public string GetApprovedEmployeeEmail(int EmployeeId)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GEOE"));
        alParameters.Add(new SqlParameter("@EmployeeId",EmployeeId));

        string sName = Convert.ToString(ExecuteScalar("HRspExitInterView", alParameters));
        return sName;
    }




    #region SaveResignation
    public int SaveResignation()
    {
        try
        {
            ArrayList alParameters = new ArrayList();
            alParameters.Add(new SqlParameter("@Mode", "SR"));
            alParameters.Add(new SqlParameter("@Message", Message));
            alParameters.Add(new SqlParameter("@Reason", Reason));
            alParameters.Add(new SqlParameter("@RequestedBy", RequestedBy));
            alParameters.Add(new SqlParameter("@RequestedOn", RequestedDate));
            alParameters.Add(new SqlParameter("@NoticePeriod", NoticePeriod));
            alParameters.Add(new SqlParameter("@Status", StatusId));
            alParameters.Add(new SqlParameter("@RequestedToV",RequestedToV));

            return  Convert.ToInt32(ExecuteScalar("[HRspExitInterView]", alParameters));

        }
        catch (Exception ex)
        {
            throw;
        }
    }
#endregion

    #region GetResignationDetails
    public DataTable  GetResignationDetails()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRD"));
        alParameters.Add(new SqlParameter("@RequestedBy",RequestedBy));
        return this.ExecuteDataTable("[HRspExitInterView]", alParameters);
    }
    #endregion

    #region GetResignationDetails
    public DataTable GetResignationDetails(int ResignationId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GRDBRID"));
        alParameters.Add(new SqlParameter("@ResignationId", ResignationId));
        return this.ExecuteDataTable("[HRspExitInterView]", alParameters);
    }
    #endregion
    
    #region GetEmployeeLastResignationDetails
    public DataTable GetEmployeeLastResignationDetails(int EmployeeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GELRD"));
        alParameters.Add(new SqlParameter("@RequestedBy",EmployeeId));
        return this.ExecuteDataTable("[HRspExitInterView]", alParameters);
    }
    #endregion


    #region CancelResignation
    public bool  CancelResignation(int ResignationId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CLR"));
        alParameters.Add(new SqlParameter("@ResignationId",ResignationId));
        if (Convert.ToInt32(this.ExecuteScalar("[HRspExitInterView]", alParameters)) > 0)
            return true;
        else
            return false;
    }
    #endregion



    #region DeleteResignationMessage
    public void DeleteResignationMessage(int ReferenceId, int ReferenceTypeId, int MessageTypeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "DEL-RM"));
        alParameters.Add(new SqlParameter("@ReferenceId", ReferenceId));
        alParameters.Add(new SqlParameter("@ReferenceTypeId", ReferenceTypeId));
        alParameters.Add(new SqlParameter("@MessageTypeId", MessageTypeId));
        this.ExecuteScalar("[HRspExitInterView]", alParameters);

    }
    #endregion









    /// <summary>
    /// /
    /// </summary>
    /// <param name="ResignationId"></param>
    /// <param name="Status"></param>
    /// <param name="ApprovedBy"></param>
    /// <param name="Remarks"></param>
    /// <returns></returns>
    /// /**/
    /// 


    /*  Commented */
    #region ApproveORRejectResignation
    public bool ApproveORRejectResignation(int ResignationId,int Status,int ApprovedBy,string Remarks)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "AOR"));
        alParameters.Add(new SqlParameter("@ResignationId", ResignationId));
        alParameters.Add(new SqlParameter("@Status",Status));
        alParameters.Add(new SqlParameter("@Remarks",Remarks));
        alParameters.Add(new SqlParameter("@ApprovedBy",ApprovedBy));
       if(Convert.ToInt32(this.ExecuteScalar("[HRspExitInterView]", alParameters))>0)
           return true;
        else
           return false;
    }


    #endregion

}

