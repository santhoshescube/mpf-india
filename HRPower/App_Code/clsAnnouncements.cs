﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Summary description for clsAnnouncements
/// </summary>

public enum eAnnouncementGroup
{
    DepartmentWise = 1,
    EmployeeWise = 2,
    General = 3
}

public class clsAnnouncementDetails
{
    public List<int> EmployeeIDs { get; set; }
    public List<int> DepartmentIDs { get; set; }

    public clsAnnouncementDetails()
    {
        this.EmployeeIDs = new List<int>();
        this.DepartmentIDs = new List<int>();
    }
}

public class clsAnnouncements : DL
{
    int iAnnouncementId, iPageIndex, iPageSize, iCompanyId, iUserId;
    string sMessage;
    DateTime dtSDate, dtEDate;

    public int AnnouncementId { get { return iAnnouncementId; } set { iAnnouncementId = value; } }
    public int CompanyId { get { return iCompanyId; } set { iCompanyId = value; } }
    public string Message { get { return sMessage; } set { sMessage = value; } }
    public DateTime StartDate { get { return dtSDate; } set { dtSDate = value; } }
    public DateTime EndDate { get { return dtEDate; } set { dtEDate = value; } }
    public int UserId { get { return iUserId; } set { iUserId = value; } }

    // Added by Ratheesh
    public eAnnouncementGroup AnnouncementGroup { get; set; }
    public clsAnnouncementDetails AnnouncementDetails { get; set; }

    public int PageIndex
    {
        get { return iPageIndex; }
        set { iPageIndex = value; }
    }
    public int PageSize
    {
        get { return iPageSize; }
        set { iPageSize = value; }
    }

    public clsAnnouncements()
    {
        this.AnnouncementDetails = new clsAnnouncementDetails();
    }


    public DataTable GetCompanies()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "C"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyId  ));
        return ExecuteDataTable("HRspAnnouncements", alParameters);
    }

    /// <summary>
    /// Insert/Update announcements.
    /// </summary>
    /// <Created By>Jisha Bijoy</Created>
    /// <Created on>17 Jan 2011</Created>
    /// 
    /// <Modified By> Ratheesh </Modified>
    /// <Modified On> 28 July 2011</Modified>
    /// <Purpose> Include group-wise announcement functionality </Purpose>
    public int InserUpdateAnnouncements()
    {
        ArrayList alParameters = new ArrayList();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        string sMode = "";
        if (AnnouncementId != 0)
        {
            alParameters.Add(new SqlParameter("@AnnouncementId", iAnnouncementId));
            sMode = "U";
        }
        else
            sMode = "I";

        try
        {
            this.BeginTransaction("HRspAnnouncements");

            alParameters.Add(new SqlParameter("@Mode", sMode));
            alParameters.Add(new SqlParameter("@StartDate", dtSDate));
            alParameters.Add(new SqlParameter("@EndDate", dtEDate));
            alParameters.Add(new SqlParameter("@Message", sMessage));
            alParameters.Add(new SqlParameter("@CompanyId", iCompanyId));
            alParameters.Add(new SqlParameter("@UserId", iUserId));
            alParameters.Add(new SqlParameter("@AnnouncementGroupID", (int)this.AnnouncementGroup));
            this.AnnouncementId = this.ExecuteScalar(alParameters).ToInt32();

            if (sMode == "U")
            {
                // Delete details and re-insert
                alParameters = new ArrayList { 
                    new SqlParameter("@Mode", "DEL_DET"),
                    new SqlParameter("@AnnouncementID", this.AnnouncementId),
                    new SqlParameter("@AnnouncementGroupID", (int)this.AnnouncementGroup)
                };
                int RowsAffected = this.ExecuteScalar(alParameters).ToInt32();
            }

            // Insert announcemnt details
            if (this.AnnouncementGroup == eAnnouncementGroup.DepartmentWise)
            {
                string strDepartmentIDs = this.GetCommaSeperatedFormat(this.AnnouncementDetails.DepartmentIDs);

                if (strDepartmentIDs.Length > 0)
                {
                    alParameters = new ArrayList { 
                        new SqlParameter("@Mode", "INS_DET"),
                        new SqlParameter("@AnnouncementGroupID", (int)AnnouncementGroup),
                        new SqlParameter("@AnnouncementID", this.AnnouncementId),
                        new SqlParameter("@DepartmentIDs", strDepartmentIDs) // comma seperated DepartmentIDs.
                    };

                    int RowsAffected = this.ExecuteScalar(alParameters).ToInt32();

                    if (RowsAffected <= 0)
                        throw new ApplicationException("Insersion failed.");
                }
            }
            else if (this.AnnouncementGroup == eAnnouncementGroup.EmployeeWise)
            {
                string strEmployeeIDs = this.GetCommaSeperatedFormat(this.AnnouncementDetails.EmployeeIDs);

                if (strEmployeeIDs.Length > 0)
                {
                    alParameters = new ArrayList { 
                        new SqlParameter("@Mode", "INS_DET"),
                        new SqlParameter("@AnnouncementGroupID", (int)AnnouncementGroup),
                        new SqlParameter("@AnnouncementID", this.AnnouncementId),
                        new SqlParameter("@EmployeeIDs", strEmployeeIDs) // comma seperated EmployeeIDs.
                    };

                    int RowsAffected = this.ExecuteScalar(alParameters).ToInt32();

                    if (RowsAffected <= 0)
                        throw new ApplicationException("Insersion failed.");
                }
            }

            this.CommitTransaction();
            return this.AnnouncementId;
        }
        catch
        {
            this.RollbackTransaction();
            throw;
        }
    }

    private string GetCommaSeperatedFormat(List<int> List)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        string Reslut = string.Empty;
        foreach (int intValue in List)
            sb.Append(intValue.ToString()).Append(",");

        Reslut = sb.ToString();

        if (sb.Length > 0)
            // remove last comma
            Reslut = Reslut.Remove(Reslut.Length - 1, 1);

        return Reslut;
    }

    /// <summary>
    /// select announcements
    /// </summary>
    /// <returns>datatable</returns>
    public DataTable GetAnnouncements()
    {
        ArrayList alParameters = new ArrayList();
        string sMode = "";
        if (AnnouncementId != 0)
        {
            alParameters.Add(new SqlParameter("@AnnouncementId", iAnnouncementId));
            sMode = "V";
        }
        else
        {
            sMode = "VA";
            alParameters.Add(new SqlParameter("@PageIndex", iPageIndex));
            alParameters.Add(new SqlParameter("@PageSize", iPageSize));
            alParameters.Add(new SqlParameter("@UserId", iUserId));
            alParameters.Add(new SqlParameter("@CompanyId", CompanyId));
        }

        alParameters.Add(new SqlParameter("@Mode", sMode));


        return ExecuteDataTable("HRspAnnouncements", alParameters);


    }
    /// <summary>
    /// get records count
    /// </summary>
    /// <returns>int</returns>
    public int GetRecordCount()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "CO"));
        alParameters.Add(new SqlParameter("@CompanyId", CompanyId));
        int iCount = Convert.ToInt32(ExecuteScalar("HRspAnnouncements", alParameters));
        return iCount;
    }
    /// <summary>
    /// Delete announcements.
    /// </summary>
    public void DeleteAnnouncements()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@AnnouncementId", iAnnouncementId));
        alParameters.Add(new SqlParameter("@Mode", "D"));

        ExecuteNonQuery("HRspAnnouncements", alParameters);
    }

    public void DeleteAnnouncementMessage(int ReferenceId, int MessageTypeId)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@ReferenceId", ReferenceId));
        alParameters.Add(new SqlParameter("@MessageTypeID", MessageTypeId));
        alParameters.Add(new SqlParameter("@Mode", "DELUM"));
        ExecuteNonQuery("HRspAnnouncements", alParameters);
    }

    /// <summary>
    /// Update announcements.
    /// </summary>
    public void UpdateAnnouncements()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@AnnouncementId", iAnnouncementId));
        alParameters.Add(new SqlParameter("@Mode", "U"));

        ExecuteNonQuery("HRspAnnouncements", alParameters);
    }

    public DataSet GetAnnouncementDetails()
    {
        ArrayList alParameters = new ArrayList { 
            new SqlParameter("@Mode", "SEL_DET"),
            new SqlParameter("@AnnouncementID", this.AnnouncementId)
        };

        return this.ExecuteDataSet("HRspAnnouncements", alParameters);
    }

    public bool InsertMessageDetails(string MessageID, int CompanyID)
    {
        object objRowsAffected = this.ExecuteScalar("HRspAnnouncements", new ArrayList { 
            new SqlParameter("@Mode", "INS_UM"),
            new SqlParameter("@CompanyID", CompanyID),
            new SqlParameter("@MessageID", MessageID)
        });

        return objRowsAffected.ToInt32() > 0;
    }
    public DataTable GetEmployees(int CompanyID, string DepartmentIDs)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEBD"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("DepartmentIDs", DepartmentIDs));

        return this.ExecuteDataTable("HRspTrainingSchedule", alParameters);
    }
    /// <summary>
    /// Returns employees by company
    /// </summary>
    /// <param name="CompanyID">Company ID</param>
    /// <param name="Department"> pass zero value to get employees belongs to all departments </param>
    /// <returns></returns>
    public DataTable GetEmployees(int CompanyID, int DepartmentID)
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GEBCAD"));
        alParameters.Add(new SqlParameter("@CompanyID", CompanyID));
        alParameters.Add(new SqlParameter("DepartmentID", DepartmentID));

        return this.ExecuteDataTable("HRspTrainingSchedule", alParameters);
    }
    /// <summary>
    /// Returns all departments 
    /// </summary>
    /// <returns></returns>
    public DataTable GetDepatments()
    {
        ArrayList alParameters = new ArrayList();
        alParameters.Add(new SqlParameter("@Mode", "GD"));
        return this.ExecuteDataTable("HRspTrainingSchedule", alParameters);
    }

}

//public class EmployeeTemp
//{
//    public string EmployeeName { get; set; }
//    public int EmployeeID { get; set; }
//}
