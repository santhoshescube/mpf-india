﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsRptPayroll
/// </summary>
public class clsRptPayroll
{
	public clsRptPayroll()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public static DataTable GetWorkSheetInfo(int CompanyID, int BranchID, int DepartmentID, int EmployeeID, DateTime dtAttendanceDate, bool IncludeCompany,int WorkStatusID)
    {
        List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 1),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@AttendanceDate", dtAttendanceDate.ToString("dd-MMM-yyyy")),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()),
                new SqlParameter("@UserID",new clsUserMaster().GetUserId()),
                new SqlParameter("@WorkStatusID",WorkStatusID)
            };
        return new DataLayer().ExecuteDataTable("spPayRptAttendanceWorkSheet", sqlParameters);
    }

    public static DataTable GetOffDayDetails(int CompanyID, int BranchID, int DepartmentID, int EmployeeID, DateTime dtAttendanceDate, bool IncludeCompany,int WorkStatusID)
    {
        List<SqlParameter> sqlParameters = new List<SqlParameter> { 
                new SqlParameter("@Mode", 4),
                new SqlParameter("@CompanyID",CompanyID), 
                new SqlParameter("@BranchID", BranchID) ,
                new SqlParameter("@DepartmentID", DepartmentID) ,
                new SqlParameter("@EmployeeID", EmployeeID),
                new SqlParameter("@RosterDate1", dtAttendanceDate.ToString("dd-MMM-yyyy")),
                new SqlParameter("@IncludeCompany", IncludeCompany),
                new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()),
                new SqlParameter("@UserID",new clsUserMaster().GetUserId()),
                new SqlParameter("@WorkStatusID",WorkStatusID)
            };
        return new DataLayer().ExecuteDataTable("spPayOffDayMark", sqlParameters);
    }

}
