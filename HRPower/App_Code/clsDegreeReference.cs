﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Collections;
using System.Data.SqlClient;

/// <summary>
/// Summary description for clsDegreeReference
/// </summary>
public class clsDegreeReference:DL
{
	public clsDegreeReference()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private int iDegreeID;
    private string sDescription;
    private string sDescriptionArb;
    private int iQualificationTypeID;

    public int DegreeID
    {
        get { return iDegreeID; }
        set { iDegreeID = value; }
    }
    public string Description
    {
        get { return sDescription; }
        set { sDescription = value; }
    }
    public string DescriptionArb
    {
        get { return sDescriptionArb; }
        set { sDescriptionArb = value; }
    }
    public int QualificationTypeID
    {
        get { return iQualificationTypeID; }
        set { iQualificationTypeID = value; }
    }

    public int InsertDegreeReference()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "I"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        alparameters.Add(new SqlParameter("@QualificationTypeID", iQualificationTypeID));

        return Convert.ToInt32(ExecuteScalar("HRspDegreeReference", alparameters));
 
    }

    public int UpdateDegreeReference()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "U"));
        alparameters.Add(new SqlParameter("@Description", sDescription));
        alparameters.Add(new SqlParameter("@DescriptionArb", sDescriptionArb));
        alparameters.Add(new SqlParameter("@QualificationTypeID", iQualificationTypeID));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));

        return Convert.ToInt32(ExecuteScalar("HRspDegreeReference", alparameters));
 
    }

    public DataSet FillQualificationTypes()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "FQT"));
        alparameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture() ? 1 : 0));
        return ExecuteDataSet("HRspDegreeReference", alparameters);
    }

    public DataSet BindAllDegreeReferences()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GA"));
        return ExecuteDataSet("HRspDegreeReference", alparameters);
    }

    public DataTable GetDegreeDetail()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GS"));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));

        return ExecuteDataTable("HRspDegreeReference", alparameters);
    }

    public void Delete()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "D"));
        alparameters.Add(new SqlParameter("@DegreeID", iDegreeID));

        ExecuteNonQuery("HRspDegreeReference", alparameters);
    }

    public DataSet BindDegreesByQualType()
    {
        ArrayList alparameters = new ArrayList();

        alparameters.Add(new SqlParameter("@Mode", "GF"));
        alparameters.Add(new SqlParameter("@QualificationTypeID", iQualificationTypeID));
        return ExecuteDataSet("HRspDegreeReference", alparameters);
    }

}
