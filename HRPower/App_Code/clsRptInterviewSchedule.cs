﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;

/* Created By   : Sruthy K
 * Created Date : Oct 17 2013
 * Purpose      : Functions For Interview Schedule Report 
 * */
public class clsRptInterviewSchedule
{
	public clsRptInterviewSchedule()
	{
	}

    public static DataTable FillJobLevel(int CompanyID, int BranchID, bool IncludeCompany, int JobID)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewSchedule", new ArrayList()
        {
            new SqlParameter("@Mode",1),
            new SqlParameter("@CompanyID",CompanyID),
            new SqlParameter("@BranchID",BranchID),
            new SqlParameter("@IncludeCompany",IncludeCompany),
            new SqlParameter("@JobID",JobID),
            new SqlParameter ("@IsArabic", clsGlobalization.IsArabicCulture()),
        });
    }

    public static DataTable GetInterviewScheduleReport(int CompanyID, int BranchID, bool IncludeCompany, int JobID,int JobLevelID,DateTime FromDate, DateTime ToDate)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewSchedule", new ArrayList()
        {
            new SqlParameter("@Mode",2),
            new SqlParameter("@CompanyID",CompanyID),
            new SqlParameter("@BranchID",BranchID),
            new SqlParameter("@IncludeCompany",IncludeCompany),
            new SqlParameter("@JobID",JobID),
            new SqlParameter("@JobLevelID",JobLevelID),
            new SqlParameter("@FromDate",FromDate),
            new SqlParameter("@ToDate",ToDate),
         new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
        });
    }
    public static DataTable GetDetailedReport(int JobID, int JobLevelID,DateTime FromDate, DateTime ToDate)
    {
        return new DataLayer().ExecuteDataTable("HRspRptInterviewSchedule", new ArrayList()
        {
            new SqlParameter("@Mode",3),
            new SqlParameter("@JobID",JobID),
            new SqlParameter("@JobLevelID",JobLevelID),
            new SqlParameter("@FromDate",FromDate),
            new SqlParameter("@ToDate",ToDate),
            new SqlParameter("@UserID", new clsUserMaster().GetUserId())
        
        });
    }


}
