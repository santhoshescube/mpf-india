﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class Default3 : System.Web.UI.Page
{
    clsUserMaster objUserMaster;
    // clsUserThemes objUserThemes;



    private void SetCulture()
    {
        if (clsGlobalization.IsArabicViewEnabled())
        {
            dvCulture.Visible = true;
        }
        else
        {
            rbEnglish.Checked = true; 
            dvCulture.Visible = false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetCulture();
        if (!IsPostBack)
        {
            txtUsername.Focus();
            string strMess = "";
            bool bValid = new clsProductValidation().CheckValidity(out strMess);
            if (!bValid)
            {
                lblError.Text = strMess;
                btnLogin.Enabled = false;
                return;
            }

            divJob.Visible = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.JobOpeningsEnabled).ToUpper() == "YES";
            clsCommon.IsESSOnly = true;
            if (clsGlobalization.IsArabicViewEnabled())
            {
                container1.Attributes["class"] = "container1Arabic";
            }
            else
            {
                rbEnglish.Checked = true;
                divSelectLanguage.Visible = false;

            }
        }
        if (Context.User.Identity.IsAuthenticated)
        {
            if (clsCommon.IsESSOnly)
            {
                objUserMaster = new clsUserMaster();

                if (objUserMaster.GetRoleId() == (Int32)UserRoles.Candidate)
                {

                    Response.Redirect("~/Candidate/CandidateHome.aspx");

                }
                else if (objUserMaster.GetRoleId() == (Int32)UserRoles.Employee)
                {
                    Response.Redirect("~/public/Home.aspx");
                    
                }
                else
                {

                  Response.Redirect("~/public/ManagerHomeNew.aspx");

                }
            }

        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sUsername"></param>
    /// <param name="sPassword"></param>
    private void CheckLogin(string sUsername, string sPassword)
    {
        if (rbEnglish.Checked)
        {
            clsGlobalization.CultureName = "en-US";
        }
        else
        {
            clsGlobalization.CultureName = "ar-AE";
        }
        objUserMaster = new clsUserMaster();                                            
        SqlDataReader dr;
        objUserMaster.UserName = sUsername;
        string strPassword = clsCommon.Encrypt(txtPassword.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);

        objUserMaster.Password = strPassword;
        dr = objUserMaster.IsExistUserName();
        int iUserID = -1;

        if (dr.Read())
        {
            FormsAuthentication.Initialize();
            int iEmployeeID = -1;

            if (dr["EmployeeID"] != DBNull.Value)
                iEmployeeID = Convert.ToInt32(dr["EmployeeID"]);

            iUserID = Convert.ToInt32(dr["UserID"]);

            //clsCommon.CompanyID = Convert.ToInt32(dr["CompanyID"] );

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                          1,
                          txtUsername.Text,
                          DateTime.Now,
                          DateTime.Now.AddDays(14),
                          chkStaySigned.Checked,
                          string.Format("{0},{1},{2},{3},{4}", Convert.ToInt32(dr["RoleID"]), Convert.ToInt32(dr["UserID"]), iEmployeeID, Convert.ToInt32(dr["CompanyID"] == DBNull.Value ? "-1" : dr["CompanyID"]), rbEnglish.Checked ? "en-US" : "ar-AE"),
                          
                          FormsAuthentication.FormsCookiePath
                          );
            string hash = FormsAuthentication.Encrypt(ticket);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

            if (chkStaySigned.Checked) cookie.Expires = ticket.Expiration;

            cookie.Path = ticket.CookiePath;

            Response.Cookies.Add(cookie);

           



            HttpCookie cookieCulture = new HttpCookie("CurrentCulture");
            cookieCulture.Value = clsGlobalization.CultureName;
            Response.Cookies.Add(cookieCulture);
            //objUserThemes = new clsUserThemes();

            //objUserThemes.UserID = iUserID;

            // objUserThemes.GetTheme();

            //Session["_THEME"] = objUserThemes.Theme;

            if (Request.QueryString["ReturnUrl"] == null)
            {
                if (Convert.ToInt32(dr["RoleID"]) == (Int32)UserRoles.Candidate)
                {

                    Response.Redirect("~/Candidate/CandidateHome.aspx");

                }
                else if (Convert.ToInt32(dr["RoleID"]) == (Int32)UserRoles.Employee)
                {
                    Response.Redirect("~/public/Home.aspx");                    

                }
                else if(Convert.ToInt32(dr["RoleID"]) == (Int32)UserRoles.HrManager)
                {

                   Response.Redirect("~/public/ManagerHomeNew.aspx");

                }
                else
                    Response.Redirect("~/public/Home.aspx"); 

            }
            else
                Response.Redirect(Request.QueryString["ReturnUrl"]);

            dr.Close();
        }
        else
        {
            lblError.Text = "Invalid login";
            dr.Close();
        }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        CheckLogin(txtUsername.Text, txtPassword.Text);
    }

}
