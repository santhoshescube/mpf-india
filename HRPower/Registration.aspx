﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>

<%@ Register Src="~/Controls/ReferenceControl.ascx" TagName="Reference" TagPrefix="uc" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="HandheldFriendly" content="true" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" type="text/css" media="all" href="css/global.css" />
    <link href="css/default_common.css" rel="stylesheet" type="text/css" />
    <link href="css/managerview.css" rel="stylesheet" type="text/css" />
    <link href="css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="css/popup.css" rel="stylesheet" type="text/css" />
    <link href="css/Controls.css" rel="stylesheet" type="text/css" />

    <script src="js/HRPower.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.min.js"></script>

    <script src="js/Common.js" type="text/javascript"></script>

    <script src="js/Controls.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript" charset="utf-8" src="js/bootstrap.min.js"></script>


<style type="text/css">
   #logo
   {
   	margin-left: 0px; 
}
    
select
{
	margin: 5px 5px !important;
}
label
{
	display: initial !important;
	padding-left: 6px;
}


</style>
</head>
<body>

    <form id="form1"  runat="server">
    
    <AjaxControlToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ajxManager"
        runat="server">
    </AjaxControlToolkit:ToolkitScriptManager>
 
    <div class="row-fluid">
        <div class="span12" style="margin-top: 10px; padding-bottom: 5px; border-bottom: 1px solid #0FABEC;">
            <div class="span1">
            </div>
            <div class="span3">
                <div id="logo">
                    <img src="images/companylogo.png" />
                </div>
            </div>
            <div class="span5">
            </div>
            <div class="span3">
                <div id="logo1">
                    <img src="images/logohr power.png" />
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid">
        <div class="span12" style="padding-top: 10px; padding-bottom: 40px; background-color: White;">
            <div class="span3">
            </div>
            <div class="span6">
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnSubmitMessage" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="upnlMessage" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <div style="width: 100%; font-size: 12px; border: solid 1px gray; padding: 15px 0px 15px 0px">
                    <asp:UpdatePanel ID="upCandidate" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div style="float: right; padding-right: 5px; font-weight: bold">
                                <a href="JobOpenings.aspx" class="btn btn-info ">⇦&nbsp;Back</a></div>
                            <label for="Job" class="text-info" style="margin-right: 12px; font-size: 14px; padding-left: 5px;
                                height: 29px; font-weight: bold">
                                Personal Info
                            </label>
                            <div style="width: 100%; height: 35px; padding-top: 5px; display: none">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Job :
                                </div>
                                <div style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                    <asp:UpdatePanel ID="updJob" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlJob" runat="server" Width="90%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px;">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Vacancy
                                </div>
                                <div style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                    <asp:Label ID="lbjob" runat="server" CssClass="text-info" Font-Size="13px"></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px;">
                                    Salutation<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 230px; height: 29px">
                                    <asp:UpdatePanel ID="updSalutation" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="rcSalutation" runat="server" CssClass="dropdownlist" Width="75%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvSalutation" runat="server" ControlToValidate="rcSalutation"
                                        ErrorMessage="*" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    First name <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtEnglishFirstName" runat="server" MaxLength="25" Width="82%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEnglishFirstName" runat="server" ControlToValidate="txtEnglishFirstName"
                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Second name <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtEnglishSecondName" runat="server" CssClass="textbox" MaxLength="25"
                                        Width="82%">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEnglishSecondName"
                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Third name <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtEnglishThirdName" runat="server" CssClass="textbox" MaxLength="25"
                                        Width="82%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEnglishThirdName"
                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 10px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Gender
                                </div>
                                <div style="float: left; width: 300px; height: 29px">
                                    <asp:RadioButtonList ID="rblGender" runat="server" style="padding-bottom: 12px;float:left" CellPadding="4" Width="150px"
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="0">Male</asp:ListItem>
                                        <asp:ListItem  Value="1">Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 10px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Marital Status <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updMaritalStatus" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlMaritalStatus" runat="server" Width="87%">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus"
                                                ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Religion<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updReligion" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="rcReligion" runat="server" Width="75%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="rcReligion"
                                        ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Place of Birth<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtPlaceofbirth" runat="server" CssClass="textbox" MaxLength="25"
                                        Width="82%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPlaceofbirth"
                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; border-bottom: 1px solid #cdcece; padding-bottom: 25px;">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Date of Birth<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 350px; height: 29px">
                                    <asp:TextBox ID="txtDateofBirth" runat="server" MaxLength="10" Width="125px"></asp:TextBox>
                                    &nbsp;
                                    <asp:ImageButton ID="btnDateofBirth" runat="server" CausesValidation="False" style="margin-top:9px;" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                    <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Format="dd/MM/yyyy"
                                        PopupButtonID="btnDateofBirth" TargetControlID="txtDateofBirth" />
                                    <asp:RequiredFieldValidator ID="RfvDOB" runat="server" ControlToValidate="txtDateofBirth"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                    <br />
                                    <font color="Red">
                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ClientValidationFunction="validateDateofBirth"
                                            ControlToValidate="txtDateofBirth" Display="Dynamic" ErrorMessage="Invalid Age"
                                            ForeColor="" ValidateEmptyText="True" ValidationGroup="submit"></asp:CustomValidator>
                                    </font>
                                </div>
                            </div>
                            <label for="Job" class="text-info" style="margin-right: 12px; font-size: 14px; padding-left: 5px;
                                padding-top: 20px; height: 29px; font-weight: bold">
                                Contact Info
                            </label>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Nationality<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updCurrentNationality" runat="server" UpdateMode="Conditional"
                                        RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlCurrentNationality" runat="server" Width="75%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCurrentNationality"
                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"
                                        InitialValue="-1">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Telephone <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtCurrentTelephoneNumber" runat="server" CssClass="textbox" MaxLength="20"
                                        Width="83%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCurrentTelephoneNumber" runat="server" ControlToValidate="txtCurrentTelephoneNumber"
                                        ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 60px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Address<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 60px; padding-bottom: 5px;">
                                    <asp:TextBox ID="txtCurrentAddress" runat="server" MaxLength="250" Height="100%"
                                        onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLengthCandidate(this, 250);"
                                        TextMode="MultiLine" Width="85%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCurrentAddress" runat="server" ControlToValidate="txtCurrentAddress"
                                        ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 20px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Country<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="rcCountry" runat="server" Width="75%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="rcCountry"
                                        ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px; border-bottom: 1px solid #cdcece;
                                padding-bottom: 25px;">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px;">
                                    E-mail <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" MaxLength="30" Width="84%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmailID" runat="server" ControlToValidate="txtEmail"
                                        ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmailpnlTab1" runat="server" ControlToValidate="txtEmail"
                                        CssClass="error" Display="Dynamic" ErrorMessage="Invalid Email" SetFocusOnError="False"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <label for="Job" class="text-info flt-lbl" style="margin-right: 12px; font-size: 14px; padding-left: 5px;
                                padding-top: 10px; height: 29px; font-weight: bold ;">
                                Professional Info
                            </label>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Qualification<font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="uprcDegree" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="rcDegree" runat="server" Width="77%" AutoPostBack="true" ValidationGroup="submit">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvQualificationpnlTab2" runat="server" ControlToValidate="rcDegree"
                                        ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Category <font color="Red">*</font>
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updQualificationCategory" runat="server" UpdateMode="Conditional"
                                        RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlQualificationCategory" runat="server" Width="74%">
                                            </asp:DropDownList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:RequiredFieldValidator ID="rfvQualificationCategorypnlTab2" runat="server" ControlToValidate="ddlQualificationCategory"
                                        ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Total Experience
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <table border="0" cellpadding="2" cellspacing="5">
                                        <tr>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="dropdownlist" Width="75px"
                                                            Style="font-size: 12px">
                                                            <asp:ListItem Value="-1">Years</asp:ListItem>
                                                            <asp:ListItem Value="0"></asp:ListItem>
                                                            <asp:ListItem Value="1"></asp:ListItem>
                                                            <asp:ListItem Value="2"></asp:ListItem>
                                                            <asp:ListItem Value="3"></asp:ListItem>
                                                            <asp:ListItem Value="4"></asp:ListItem>
                                                            <asp:ListItem Value="5"></asp:ListItem>
                                                            <asp:ListItem Value="6"></asp:ListItem>
                                                            <asp:ListItem Value="7"></asp:ListItem>
                                                            <asp:ListItem Value="8"></asp:ListItem>
                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                            <asp:ListItem Value="12"></asp:ListItem>
                                                            <asp:ListItem Value="13"></asp:ListItem>
                                                            <asp:ListItem Value="14"></asp:ListItem>
                                                            <asp:ListItem Value="15"></asp:ListItem>
                                                            <asp:ListItem Value="16"></asp:ListItem>
                                                            <asp:ListItem Value="17"></asp:ListItem>
                                                            <asp:ListItem Value="18"></asp:ListItem>
                                                            <asp:ListItem Value="19"></asp:ListItem>
                                                            <asp:ListItem Value="20"></asp:ListItem>
                                                            <asp:ListItem Value="21"></asp:ListItem>
                                                            <asp:ListItem Value="22"></asp:ListItem>
                                                            <asp:ListItem Value="23"></asp:ListItem>
                                                            <asp:ListItem Value="24"></asp:ListItem>
                                                            <asp:ListItem Value="25"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td valign="top">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="dropdownlist" Width="75px"
                                                            Style="font-size: 12px">
                                                            <asp:ListItem Value="-1">Months</asp:ListItem>
                                                            <asp:ListItem Value="0"></asp:ListItem>
                                                            <asp:ListItem Value="1"></asp:ListItem>
                                                            <asp:ListItem Value="2"></asp:ListItem>
                                                            <asp:ListItem Value="3"></asp:ListItem>
                                                            <asp:ListItem Value="4"></asp:ListItem>
                                                            <asp:ListItem Value="5"></asp:ListItem>
                                                            <asp:ListItem Value="6"></asp:ListItem>
                                                            <asp:ListItem Value="7"></asp:ListItem>
                                                            <asp:ListItem Value="8"></asp:ListItem>
                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 15px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Skills
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:TextBox ID="txtSkills" runat="server" TextMode="MultiLine" Width="85%" onchange="RestrictMulilineLength(this, 500);"
                                        onkeyup="RestrictMulilineLengthCandidate(this, 500);"></asp:TextBox>
                                </div>
                            </div>
                         
                            <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 15px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Referral Type
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updReferralType" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlReferraltype" runat="server" CssClass="dropdownlist" Width="77%"
                                                AutoPostBack="true" OnSelectedIndexChanged="ddlReferraltype_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnReferralType" runat="server" Width="30px" CausesValidation="False"
                                                CssClass="referencebutton" Text="..." OnClick="btnReferralType_Click" Visible="false" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 15px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px;  padding-top: 5px">
                                    Referred By
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                    <asp:UpdatePanel ID="updReferredBy" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlReferredBy" runat="server" CssClass="dropdownlist" Width="77%"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnReferredBy" runat="server" CausesValidation="False" style="margin-top: 5px;" CssClass="referencebutton"
                                                Text="..." OnClick="btnReferredBy_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            
                               <div style="width: 100%; height: 35px; padding-top: 5px; padding-bottom: 15px">
                                <div style="float: left; width: 150px; padding-left: 15px; height: 29px; padding-top: 5px">
                                    Attach Resume
                                </div>
                                <div style="float: left; width: 250px; height: 29px">
                                   
                                            <AjaxControlToolkit:AsyncFileUpload  ID="fuResumeAttach" runat="server" CompleteBackColor="Green"
                                                OnClientUploadComplete="ValidateResume" PersistedStoreType="Session" OnUploadedComplete="fuResumeAttach_UploadedComplete"
                                                PersistFile="true" />
                                            <span style="color: Black; font-size: 10px;">(File formats of type 
                                            *.doc,*.pdf,*.txt,*.rtf are only accepted.)</span>
                                      
                                    
                                    
                                  
                                </div>
                            </div>
                            <table border="0" cellpadding="5" cellspacing="0" width="96%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lbMsg" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                    <td align="right" style="text-align: right">
                                        <asp:Button ID="btnSubmit" runat="server" class="btn btn-warning" ToolTip="Submit"
                                            ValidationGroup="submit" Text="Submit" Width="75px" OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancelSubmit" runat="server" class="btn btn-warning" Text="Cancel"
                                            ToolTip="Cancel" CausesValidation="false" Width="75px" OnClick="btnCancelSubmit_Click" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="display: none">
                                <asp:Button ID="Button1" runat="server" />
                            </div>
                            <div id="pnlModalPopUp" runat="server" style="display: none;">
                                <uc:Reference ID="ReferenceControl" runat="server" ModalPopupID="mdlPopUpReference" />
                            </div>
                            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                            </AjaxControlToolkit:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updAgency" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="display: none">
                                <asp:Button ID="Button2" runat="server" />
                            </div>
                            <div id="divModalPopoUpAgency" runat="server" style="display: none; width: 650px">
                                <uc:AgencyReference ID="AgencyReference" runat="server" ModalPopupID="ModalPopoUpAgency">
                                </uc:AgencyReference>
                            </div>
                            <AjaxControlToolkit:ModalPopupExtender ID="ModalPopoUpAgency" runat="server" TargetControlID="Button2"
                                PopupControlID="divModalPopoUpAgency" BackgroundCssClass="modalBackground">
                            </AjaxControlToolkit:ModalPopupExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        
        
        
        <div class="row-fluid">
            <div class="span12" style="border-top: 5px solid #00BE9C; background: Black; padding-bottom: 20px;
                padding-top: 20px;">
                <div class="span2">
                </div>
                <div class="span8" style="padding: 20px;">
                    
                            
                            <p> <asp:Label ID ="hrSts" runat="server" text=""></asp:Label>
                EPeople © 2009-21 Privacy policy All rights reserved. Powered by <a href="http://www.escubetech.com/">ES3 Technovations LLP</a>.
            </p>
            
                </div>
                <div class="span2">
                </div>
            </div>
        </div>
    </div>

    </form>
</body>
</html>
