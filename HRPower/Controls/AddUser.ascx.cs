﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;


public partial class Controls_AddUser : System.Web.UI.UserControl
{
    public int CompanyID { get; set; }
    public delegate void Refresh();

    public event Refresh OnRefresh;
    int Editcnt = 0;
   
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            //FillAllCompanies(ddlCompany);
           // ddlCompany.Enabled = false;

            FillAllEmployees(); 

        }
        txtPassword.Attributes["value"] = "";

    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {



    }
    public void ClearControls()
    {
       // ddlCompany.SelectedIndex = 0;
        ddlEmployee.SelectedIndex = 0;
       // ddlCompany.Enabled = true;
        ddlEmployee.Enabled = true;
        txtPassword.Text = "";
        txtUserName.Text = "";
        ViewState["TempUserCompanyRolesList"] = null;
        dlRoleDetails.DataBind();
        clsUserMaster objUser = new clsUserMaster();
        objUser.CompanyID = objUser.GetCompanyId();
        //ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(objUser.GetCompanyId())));
       //ddlCompany.Enabled = false;
        
    }


    private void FillAllCompanies(DropDownList ddlCompany)
    {
       // ddlCompany.Enabled = false;
        clsUserMaster objUser=new clsUserMaster();
        clsUserMaster obj=new clsUserMaster();
        DataTable dt = clsUserMaster.FillAllCompanies(obj.UserID).Tables[0];
        DataRow dr = dt.NewRow();

        dr["CompanyID"] = -1;
        dr["CompanyName"] = "Select";

        dt.Rows.InsertAt(dr, 0);



        obj.UserID= objUser.GetUserId();
        ddlCompany.DataSource = dt;
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        objUser.CompanyID = objUser.GetCompanyId();       
        ddlCompany.DataBind();
        ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(objUser.GetCompanyId())));
        
     


    }

    private void FillAllEmployees()
    {

        DataTable dt = clsUserMaster.FillAllEmployees(0).Tables[0];
        DataRow dr = dt.NewRow();

        dr["EmployeeID"] = -1;
        dr["Employee"] = "Select";

        dt.Rows.InsertAt(dr, 0);

        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "Employee";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();

    }




    public void Edit(int EmployeeID)
    {
        Editcnt++;
        //FillAllCompanies(ddlCompany);
        //ddlCompany.SelectedValue = clsUserMaster.GetEmployeeCompanyID(EmployeeID).ToString();
             
        FillAllEmployees();
        //ddlCompany.Enabled = 
        ddlEmployee.Enabled = false;
        ddlEmployee.SelectedValue = EmployeeID.ToString();
        GetEmployeeDetails();
    }

    private void FillAllRoles(DropDownList ddlRole)
    {



        DataTable dt = clsUserMaster.FillAllRoles().Tables[0];
        DataRow dr = dt.NewRow();

        dr["RoleID"] = -1;
        dr["RoleName"] = "Select";

        dt.Rows.Add(dr);
        ddlRole.DataSource = dt;
        ddlRole.DataTextField = "RoleName";
        ddlRole.DataValueField = "RoleID";
        ddlRole.DataBind();
    }

 






    private void EnableDisableControls(int RowCount)
    {
        dvNoEmployee.Visible = (RowCount == 0);
        //ddlCompany.Enabled = false;
    }
    protected void btnAddEvaluator_Click(object sender, EventArgs e)
    {

    }
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {


    }

  




    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session["cmpID"] = (ddlCompany.SelectedValue.ToInt32()).ToString();
       
        FillAllEmployees();
        //ddlCompany.Enabled = false;
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetEmployeeDetails();

    }
    protected void dlRoleDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            
            DropDownList ddlCompany = (DropDownList)e.Item.FindControl("ddlCompany");
            DropDownList ddlRole = (DropDownList)e.Item.FindControl("ddlRole");
            HiddenField hfCompanyID = (HiddenField)e.Item.FindControl("hfCompanyID");
            HiddenField hfRoleID = (HiddenField)e.Item.FindControl("hfRoleID");
            HiddenField hfCurrentCompanyID = (HiddenField)e.Item.FindControl("hfCurrentCompanyID");
            Button btnDelete =  (Button )e.Item.FindControl("btnDelete");

            if (ddlCompany != null && ddlRole != null && hfCompanyID != null && hfRoleID != null && btnDelete != null)
            {
                FillAllCompanies(ddlCompany);
                FillAllRoles(ddlRole);

                btnDelete.Enabled=hfCurrentCompanyID.Value.ToInt32() == 0;
                ddlCompany.SelectedValue = hfCompanyID.Value;
                ddlRole.SelectedValue = hfRoleID.Value;

                 //= ((List<UserCompanyRoles>)ViewState["TempUserCompanyRolesList"]).Count > 1;
            }

        }
    }

 

    private void BindUserCompanyRoles()
    {
        if (ViewState["TempUserCompanyRolesList"] != null)
        {
            dlRoleDetails.DataSource = (List <UserCompanyRoles>) ViewState["TempUserCompanyRolesList"];
            dlRoleDetails.DataBind();
        }

        txtPassword.Attributes["value"] = txtPassword.Text;
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        List<UserCompanyRoles> UserCompanyRolesList = (List<UserCompanyRoles>)ViewState["TempUserCompanyRolesList"];
        Button btnDelete = (Button)sender;
        int ID = 0;
        if (UserCompanyRolesList != null && btnDelete != null)
        {
            ID = btnDelete.CommandArgument.ToInt32();
            UserCompanyRolesList.Remove(UserCompanyRolesList.Find(t => t.ID == ID));

            ViewState["TempUserCompanyRolesList"] = UserCompanyRolesList;

            BindUserCompanyRoles(); 
        }

    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        List<UserCompanyRoles> UserCompanyRolesList = new List<UserCompanyRoles>();
        int ID = 0;
        if (UserCompanyRolesList != null)
        {
            foreach (DataListItem i in dlRoleDetails.Items)
            {
                if (i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
                {
                    ID = ID + 1;
                    DropDownList ddlCompany = (DropDownList)i.FindControl("ddlCompany");
                    DropDownList ddlRole = (DropDownList)i.FindControl("ddlRole");
                    UserCompanyRolesList.Add(new UserCompanyRoles()
                    {
                        ID = ID,
                        CompanyID = ddlCompany.SelectedValue.ToInt32(),
                        RoleID = ddlRole.SelectedValue.ToInt32()
                    });
                }
            }

            UserCompanyRolesList.Add(new UserCompanyRoles() { CompanyID = -1, RoleID = -1 }); 
        }


        ViewState["TempUserCompanyRolesList"] = UserCompanyRolesList;
        BindUserCompanyRoles();
    }


    private bool Validate()
    {
        clsUserMaster obj = new clsUserMaster();
        foreach (DataListItem i in dlRoleDetails.Items)
        {
            if (i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlCompany = (DropDownList)i.FindControl("ddlCompany");
                DropDownList ddlRole = (DropDownList)i.FindControl("ddlRole");


                if (ddlCompany.SelectedValue == "-1")
                {
                    ScriptManager.RegisterClientScriptBlock(upd, upd.GetType(), "Alert", "alert('Please add the company for the applicable roles');", true);
                    return false;
                }
                else if (ddlRole.SelectedValue == "-1")
                {
                    ScriptManager.RegisterClientScriptBlock(upd, upd.GetType(), "Alert", "alert('Please add role for the selected company');", true);
                    return false;
                }
               // obj.UserName = txtUserName.Text;
                //obj.UserID = hfUserID.Value.ToInt32();

                bool ReturnVal = clsUserMaster.IsUserNameExists(txtUserName.Text, hfUserID.Value.ToInt32());
                    if (ReturnVal == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(upd, upd.GetType(), "Alert", "alert('UserName already Exist');", true);
                        return false;
                    }
                    /*bool val = clsUserMaster.IsSameCompanyExist(hfUserID.Value.ToInt32(), ddlCompany.SelectedValue.ToInt32());
                    if (val == true)
                    {
                        ScriptManager.RegisterClientScriptBlock(upd, upd.GetType(), "Alert", "alert('Assigining multipule role in same company not allowed');", true);
                        return false;
                    }*/
                

            }
        }
        return true;
    }

    protected void save_Click(object sender, EventArgs e)
    {
        if (Validate())
        {

            Save();
        }

        txtPassword.Attributes["value"] = txtPassword.Text;
    }


    private void Save()
    {
        int Companycount = 0;
        int UserID = 0;
        clsUserMaster obj = new clsUserMaster();
        List<UserCompanyRoles> UserCompanyRoleList = new List<UserCompanyRoles>();

        UserCompanyRoleList = new List<UserCompanyRoles>();
        foreach (DataListItem i in dlRoleDetails.Items)
        {
            if (i.ItemType == ListItemType.Item || i.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlCompany = (DropDownList)i.FindControl("ddlCompany");
                DropDownList ddlRole = (DropDownList)i.FindControl("ddlRole");


                UserCompanyRoleList.Add(new UserCompanyRoles()
                {
                    CompanyID = ddlCompany.SelectedValue.ToInt32(),
                    RoleID = ddlRole.SelectedValue.ToInt32()
                });
            }
        }








        for(int i=0;i< dlRoleDetails.Items.Count;i++)
        {
            DropDownList ddlCompany = (DropDownList)dlRoleDetails.Items[i].FindControl("ddlCompany");
            for (int j = i + 1; j < dlRoleDetails.Items.Count; j++)
            {
                DropDownList ddlCompany1 = (DropDownList)dlRoleDetails.Items[j].FindControl("ddlCompany");
                if(ddlCompany.SelectedValue==ddlCompany1.SelectedValue)
                {
                    Companycount++;
                      
                }
            }

        }
        if (Companycount >= 1)
        {
            ScriptManager.RegisterClientScriptBlock(ddlEmployee, ddlEmployee.GetType(), new Guid().ToString(), "alert('Assigning multiple roles within same company is not allowed');", true);
        }
        else
        {








            obj.UserCompanyRoleList = UserCompanyRoleList;
            obj.UserName = txtUserName.Text.Trim();
            obj.Password = txtPassword.Text.Trim();
            obj.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            obj.UserID = hfUserID.Value.ToInt32();

            UserID = obj.SaveUser();
            if (UserID  >0)
            {
                hfUserID.Value = UserID.ToString();
                ScriptManager.RegisterClientScriptBlock(ddlEmployee, ddlEmployee.GetType(), new Guid().ToString(), "alert('successfully saved ');", true);
                if (OnRefresh != null)
                    OnRefresh();
            }
        }


        
    }

    private void GetEmployeeDetails()
    {
        clsUserMaster u = clsUserMaster.GetUserDetails(ddlEmployee.SelectedValue.ToInt32());

        if (u != null)
        {
            txtUserName.Text = u.UserName;

             txtPassword.Attributes["value"] = u.Password;
            txtPassword.Text = u.Password;
            hfUserID.Value = u.UserID.ToString();

            //if (Editcnt>= 1)
            //{
            //    ddlCompany.SelectedValue = u.CompanyID.ToString();
            //}

            ViewState["CompanyID"] = u.CompanyID;
           if (u.UserCompanyRoleList.Count == 0)
                u.UserCompanyRoleList.Add(new UserCompanyRoles() { CompanyID = -1, CurrentCompany = 1, RoleID = -1, ID = 1 });
            ViewState["TempUserCompanyRolesList"] = u.UserCompanyRoleList;

            BindUserCompanyRoles(); 
        }
    }

}




