﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class controls_SalaryScale : System.Web.UI.UserControl
{
    clsSalaryScale objclsSalaryScale;
    clsUserMaster objclsUserMaster;

    bool blnAddStatus = false;

    string strModalPopupID;

    private string CurrentSelectedValue
    {
        get;
        set;
    }

    public string ModalPopupID
    {
        set
        {
            strModalPopupID = value;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(strModalPopupID);
        mpe.PopupDragHandleControlID = tdDrag.ClientID;
        mpe.CancelControlID = btnClose.ClientID;
    }

    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(strModalPopupID);

        //if (Update != null && iLeavePolicyID != 0)
        //    Update();

        mpe.Hide();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentSelectedValue = "-1";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(strModalPopupID);

        //if (Update != null && iLeavePolicyID != 0)
        //    Update();

        mpe.Hide();
    }

    protected void btnAdditions_Click(object sender, EventArgs e)
    {
        ViewState["Additions"] = null;

        UpdateDataList();
        NewAllowancesAdd();
    }

    private void UpdateDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("Particulars");
        dt.Columns.Add("AddDedID");
        dt.Columns.Add("RateOnly");
        dt.Columns.Add("Type");
        dt.Columns.Add("PolicyName");
        dt.Columns.Add("EmployerAmount");
        dt.Columns.Add("DeductionPolicyID");

        DataRow dw;

        foreach (DataListItem item in dlSalaryScale.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
            HiddenField hdPolicyID = (HiddenField)item.FindControl("DeductionPolicyID");
            Label lblPolicy = (Label)item.FindControl("PolicyName");
            Label lblType = (Label)item.FindControl("Type");
            Label lblEmployerAmount = (Label)item.FindControl("EmployerAmount");

            dw = dt.NewRow();

            dw["Particulars"] = ddlParticulars.SelectedItem.Text;
            dw["AddDedID"] = Convert.ToString(hdParticulars.Value);
            dw["DeductionPolicyID"] = hdPolicyID.Value.ToInt32();
            dw["RateOnly"] = (lblType.Text == "Rate") ? 1 : 0;
            dw["PolicyName"] = lblPolicy.Text;
            dw["Type"] = lblType.Text;
            dw["EmployerAmount"] = lblEmployerAmount.Text;

            dt.Rows.Add(dw);
        }
        ViewState["Additions"] = dt;
    }

    private void NewAllowancesAdd()
    {
        DataTable dt = (DataTable)ViewState["Additions"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("Particulars");
            dt.Columns.Add("AddDedID");
            dt.Columns.Add("RateOnly");
            dt.Columns.Add("Type");
            dt.Columns.Add("PolicyName");
            dt.Columns.Add("EmployerAmount");
            dt.Columns.Add("DeductionPolicyID");
        }
        DataRow dw = dt.NewRow();

        dt.Rows.Add(dw);

        dlSalaryScale.DataSource = dt;
        dlSalaryScale.DataBind();

        upList.Update();
    }

    protected void dlSalaryScale_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            objclsSalaryScale = new clsSalaryScale();

            DropDownList ddlParticulars = (DropDownList)e.Item.FindControl("ddlParticulars");
            objclsSalaryScale.IsAddition = true; 
            ddlParticulars.DataSource = objclsSalaryScale.FillParticulars();
            ddlParticulars.DataBind();
            ddlParticulars.Items.Insert(0, new ListItem("--select--", "-1"));

            int AddDedId = dlSalaryScale.DataKeys[e.Item.ItemIndex].ToInt32();

            if (ddlParticulars.Items.FindByValue(AddDedId.ToString()) != null)
                ddlParticulars.Items.FindByValue(AddDedId.ToString()).Selected = true;
        }
    }

    protected void ddlParticulars_SelectedIndexChanged(object sender, EventArgs e)
    {
        objclsSalaryScale = new clsSalaryScale();

        DropDownList ddlParticulars = (DropDownList)sender;
        DataListItem dli = (DataListItem)(((DropDownList)sender).Parent);
        objclsSalaryScale.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);

        HiddenField hdnAddDedID = (HiddenField)dli.FindControl("hdAddDedID");

        hdnAddDedID.Value = ddlParticulars.SelectedValue ;

        DataSet ds = objclsSalaryScale.GetPolicyDetails();
        DataTable dt = ds.Tables[0];

        if (dt.DefaultView.Count > 0)
        {
            Label lblPolicy = (Label)dli.FindControl("PolicyName");
            Label lblType = (Label)dli.FindControl("Type");
            Label lblEmployerAmount = (Label)dli.FindControl("EmployerAmount");

            lblPolicy.Text = dt.Rows[0]["PolicyName"].ToString();
            lblType.Text = (dt.Rows[0]["RateOnly"].ToString() == "1") ? "Rate" : (dt.Rows[0]["RateOnly"].ToString() == "0") ? "Percentage" : "";
            lblEmployerAmount.Text = dt.Rows[0]["EmployerPart"].ToString();
        }
    }

    protected void dlSalaryScale_ItemCommand(object source, DataListCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        objclsSalaryScale = new clsSalaryScale();

        switch (e.CommandName)
        {
            case "REMOVE_ALLOWANCE":

                dt.Columns.Add("Particulars");
                dt.Columns.Add("AddDedID");
                dt.Columns.Add("RateOnly");
                dt.Columns.Add("Type");
                dt.Columns.Add("PolicyName");
                dt.Columns.Add("EmployerAmount");
                dt.Columns.Add("DeductionPolicyID");

                DataRow dw;

                foreach (DataListItem item in dlSalaryScale.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
                    HiddenField hdPolicyID = (HiddenField)item.FindControl("hdDeductionPolicyID");
                    Label lblPolicy = (Label)item.FindControl("PolicyName");
                    Label lblType = (Label)item.FindControl("Type");

                    dw = dt.NewRow();

                    dw["Particulars"] = ddlParticulars.SelectedItem.Text;
                    dw["AddDedID"] = Convert.ToString(hdParticulars.Value);
                    dw["DeductionPolicyID"] = hdPolicyID.Value.ToInt32();
                    dw["Type"] = lblType.Text;
                    dw["PolicyName"] = lblPolicy.Text;

                    if (item != e.Item)
                    {
                        dt.Rows.Add(dw);
                    }
                }
                ViewState["Additions"] = dt;

                dlSalaryScale.DataSource = dt;
                dlSalaryScale.DataBind();

                upList.Update();

                break;
        }
    }

    protected void btnDedAdditions_Click(object sender, EventArgs e)
    {
        ViewState["Deductions"] = null;

        UpdateDeductionDataList();
        NewDeductionAdd();
    }

    private void UpdateDeductionDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("Particulars");
        dt.Columns.Add("AddDedID");
        dt.Columns.Add("RateOnly");
        dt.Columns.Add("Type");
        dt.Columns.Add("PolicyName");
        dt.Columns.Add("EmployerAmount");
        dt.Columns.Add("EmployeeAmount");
        dt.Columns.Add("DeductionPolicyID");

        DataRow dw;

        foreach (DataListItem item in dlDeductions.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlDedParticulars");
            HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
            HiddenField hdPolicyID = (HiddenField)item.FindControl("hdDeductionPolicyID");
            Label lblPolicy = (Label)item.FindControl("PolicyName");
            Label lblType = (Label)item.FindControl("Type");
            Label lblEmployerAmount = (Label)item.FindControl("EmployerAmount");
            Label lblEmployeeAmount = (Label)item.FindControl("EmployeeAmount");

            dw = dt.NewRow();

            dw["Particulars"] = ddlParticulars.SelectedItem.Text;
            dw["AddDedID"] = Convert.ToString(hdParticulars.Value);
            dw["DeductionPolicyID"] = hdPolicyID.Value.ToInt32();
            dw["RateOnly"] = (lblType.Text == "Rate") ? 1 : 0;
            dw["PolicyName"] = lblPolicy.Text;
            dw["Type"] = lblType.Text;
            dw["EmployerAmount"] = lblEmployerAmount.Text;
            dw["EmployeeAmount"] = lblEmployeeAmount.Text;

            dt.Rows.Add(dw);
        }
        ViewState["Deductions"] = dt;
    }

    private void NewDeductionAdd()
    {
        DataTable dt = (DataTable)ViewState["Deductions"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("Particulars");
            dt.Columns.Add("AddDedID");
            dt.Columns.Add("RateOnly");
            dt.Columns.Add("Type");
            dt.Columns.Add("PolicyName");
            dt.Columns.Add("EmployerAmount");
            dt.Columns.Add("EmployeeAmount");
            dt.Columns.Add("DeductionPolicyID");
        }
        DataRow dw = dt.NewRow();

        dt.Rows.Add(dw);

        dlDeductions.DataSource = dt;
        dlDeductions.DataBind();

        UpDedList.Update();
    }

    protected void dlDeductions_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            objclsSalaryScale = new clsSalaryScale();

            DropDownList ddlDedParticulars = (DropDownList)e.Item.FindControl("ddlDedParticulars");
            objclsSalaryScale.IsAddition = false; 
            ddlDedParticulars.DataSource = objclsSalaryScale.FillParticulars();
            ddlDedParticulars.DataBind();
            ddlDedParticulars.Items.Insert(0, new ListItem("--select--", "-1"));

            int AddDedId = dlDeductions.DataKeys[e.Item.ItemIndex].ToInt32();

            if (ddlDedParticulars.Items.FindByValue(AddDedId.ToString()) != null)
                ddlDedParticulars.Items.FindByValue(AddDedId.ToString()).Selected = true;
        }
    }

    protected void ddlDedParticulars_SelectedIndexChanged(object sender, EventArgs e)
    {
        objclsSalaryScale = new clsSalaryScale();

        DropDownList ddlParticulars = (DropDownList)sender;
        DataListItem dli = (DataListItem)(((DropDownList)sender).Parent);
        objclsSalaryScale.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);

        HiddenField hdnAddDedID = (HiddenField)dli.FindControl("hdAddDedID");

        hdnAddDedID.Value = ddlParticulars.SelectedValue;

        DataSet ds = objclsSalaryScale.GetPolicyDetails();
        DataTable dt = ds.Tables[0];

        if (dt.DefaultView.Count > 0)
        {
            Label lblPolicy = (Label)dli.FindControl("PolicyName");
            Label lblType = (Label)dli.FindControl("Type");
            Label lblEmployerAmount = (Label)dli.FindControl("EmployerAmount");
            Label lblEmployeeAmount = (Label)dli.FindControl("EmployeeAmount");

            lblPolicy.Text = dt.Rows[0]["PolicyName"].ToString();
            lblType.Text = (dt.Rows[0]["RateOnly"].ToString() == "1") ? "Rate" : (dt.Rows[0]["RateOnly"].ToString() == "0") ? "Percentage" : "";
            lblEmployerAmount.Text = dt.Rows[0]["EmployerPart"].ToString();
            lblEmployeeAmount.Text = dt.Rows[0]["EmployeePart"].ToString();
        }
    }

    protected void dlDeductions_ItemCommand(object source, DataListCommandEventArgs e)
    {
        DataTable dt = new DataTable();
        objclsSalaryScale = new clsSalaryScale();

        switch (e.CommandName)
        {
            case "REMOVE_DEDUCTIONS":

                dt.Columns.Add("Particulars");
                dt.Columns.Add("AddDedID");
                dt.Columns.Add("RateOnly");
                dt.Columns.Add("Type");
                dt.Columns.Add("PolicyName");
                dt.Columns.Add("EmployerAmount");
                dt.Columns.Add("EmployeeAmount");
                dt.Columns.Add("DeductionPolicyID");

                DataRow dw;

                foreach (DataListItem item in dlDeductions.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlDedParticulars");
                    HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
                    HiddenField hdPolicyID = (HiddenField)item.FindControl("hdDeductionPolicyID");
                    Label lblPolicy = (Label)item.FindControl("PolicyName");
                    Label lblType = (Label)item.FindControl("Type");

                    dw = dt.NewRow();

                    dw["Particulars"] = ddlParticulars.SelectedItem.Text;
                    dw["AddDedID"] = Convert.ToString(hdParticulars.Value);
                    dw["DeductionPolicyID"] = hdPolicyID.Value.ToInt32();
                    dw["Type"] = lblType.Text;
                    dw["PolicyName"] = lblPolicy.Text;

                    if (item != e.Item)
                    {
                        dt.Rows.Add(dw);
                    }
                }
                ViewState["Deductions"] = dt;

                dlDeductions.DataSource = dt;
                dlDeductions.DataBind();

                upList.Update();

                break;
        }
    }

       /// <summary>
    /// To Validate SalaryScale details
    /// </summary>
    private bool FormValidation()
    {
        return true;
    }

     /// <summary>
    /// Save/update SalaryScale and its details
    /// </summary>
    private bool Save()
    {
        if (FormValidation())
        {
            objclsSalaryScale = new clsSalaryScale();
            objclsUserMaster = new clsUserMaster();

            if (blnAddStatus)
            {
                objclsSalaryScale.SalaryScaleID = 0;
            }

            objclsSalaryScale.GrossSalary = txtGrossAmount.Text.ToDecimal();
            objclsSalaryScale.Description = txtScaleName.Text.Trim();
            objclsSalaryScale.CreatedBy = objclsUserMaster.GetUserId();

            objclsSalaryScale.SalaryScaleID = objclsSalaryScale.SaveSalaryScaleMaster();

            if (objclsSalaryScale.SalaryScaleID > 0)
            {
                foreach (DataListItem item in dlSalaryScale.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
                    HiddenField hdPolicyID = (HiddenField)item.FindControl("hdDeductionPolicyID");

                    objclsSalaryScale.AddDedID = hdParticulars.Value.ToInt32();
                    objclsSalaryScale.DeductionPolicyID = hdPolicyID.Value.ToInt32();
                    objclsSalaryScale.IsAddition = true;

                    objclsSalaryScale.SaveSalaryScaleDetail();
                }
                foreach (DataListItem item in dlDeductions.Items)
                {
                    HiddenField hdParticulars = (HiddenField)item.FindControl("hdAddDedID");
                    HiddenField hdPolicyID = (HiddenField)item.FindControl("hdDeductionPolicyID");

                    objclsSalaryScale.AddDedID = hdParticulars.Value.ToInt32();
                    objclsSalaryScale.DeductionPolicyID = hdPolicyID.Value.ToInt32();
                    objclsSalaryScale.IsAddition = false;

                    objclsSalaryScale.SaveSalaryScaleDetail();
                }
            }
        }
        return true;
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        Save();
    }

    private void Bind()
    {
        objclsSalaryScale = new clsSalaryScale();

        lblWarning.Text = string.Empty;
        objclsSalaryScale.RowNumber = fvpSalaryScale.PageIndex;

        DataSet ds = objclsSalaryScale.GetSalaryScaleDetails();

        if (ds.Tables[0]!= null && ds.Tables[0].Rows.Count > 0)
        {
            FillMasterDetails(ds.Tables[0]);
            FillAdditions(ds.Tables[1]);
            FillDeductions(ds.Tables[2]);
        }
    }

    private bool FillMasterDetails(DataTable dtMaster)
    {
        txtScaleName.Text = dtMaster.Rows[0]["ScaleName"].ToString();
        txtGrossAmount.Text = dtMaster.Rows[0]["GrossAmount"].ToString();
        return true;
    }

    private bool FillDeductions(DataTable dtDedDetails)
    {
        ViewState["Deductions"] = dtDedDetails;

        dlDeductions.DataSource = dtDedDetails;
        dlDeductions.DataBind();

        upList.Update();
        return true;
    }

    private bool FillAdditions(DataTable dtAddDetails)
    {
        ViewState["Additions"] = dtAddDetails;

        dlSalaryScale.DataSource = dtAddDetails;
        dlSalaryScale.DataBind();

        upList.Update();
        return true;
    }

    private void NewSalaryScale()
    {
        objclsSalaryScale = new clsSalaryScale();

        lblWarning.Text = string.Empty;

        int intTotalRowCount = objclsSalaryScale.GetSalaryScaleCount();

        fvpSalaryScale.PageIndex = intTotalRowCount + 1;
        fvpSalaryScale.PageCount = intTotalRowCount;

        btnDelete.Enabled = false;

        Bind();
    }
}