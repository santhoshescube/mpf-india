﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;


public partial class controls_CurrencyReference : System.Web.UI.UserControl
{
    clsCurrencyReference objCurrencyReference;
    clsCompany objCompany;
    private string CurrentSelectedValue { get; set; }
    public delegate void OnUpdate();
    public event OnUpdate Update;

    string sModalPopupID;

    int iCurrencyId;

    public int CurrencyId
    {
        set
        {
            iCurrencyId = value;
        }
        get
        {
            return iCurrencyId;
        }
    }

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        ///mpe.PopupDragHandleControlID = tdDrag.ClientID;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //cssMaroon.Attributes.Clear();
        if (!IsPostBack)
            lblWarning.Text = string.Empty;


        ReferenceControlNew.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew_OnSave);
        ReferenceControlNew.onRefresh -= new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew_onRefresh);

        ReferenceControlNew.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew_OnSave);
        ReferenceControlNew.onRefresh += new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew_onRefresh);
    }

    void ReferenceControlNew_onRefresh()
    {
        mdlPopUpReference.Show(); 

    }

    void ReferenceControlNew_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);

    }

    #region Events

    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        btnAddNew.Enabled = btnDelete.Enabled = false;
        btnSaveData.Enabled = btnClear.Enabled = true;

        //SetImages();  
        Clear();
        if (Update != null  && iCurrencyId !=0)
        {
            
            Update();
        }
        mpe.Hide();
    }


    public void FillComboCountry()
    {
        objCurrencyReference = new clsCurrencyReference();
        rcCountry.DataSource = objCurrencyReference.FillComboCountry();
        rcCountry.DataTextField = "Countryname";
        rcCountry.DataValueField = "CountryId";
        rcCountry.DataBind();
        rcCountry.Items.Insert(0, new ListItem("Select", "-1"));
        rcCountry.SelectedValue = CurrentSelectedValue;
        updCountry.Update();
        
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        btnAddNew.Enabled = btnDelete.Enabled = false;
        btnSaveData.Enabled = btnClear.Enabled = true;

        //SetImages();
        Clear();

        if (Update != null && iCurrencyId != 0)
        {
            Update();
        }
        mpe.Hide();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (SaveCurrency())
        {
         Clear();

        //if (Update != null)
        //{
        //    Update();
        //}
        mpe.Hide();    
        }
       
          
    }   

    protected void btnSave_Click(object sender, EventArgs e)
    {
      if  (SaveCurrency())

        Clear();
    }

    protected void dlCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        dlCurrency.SelectedItem.CssClass = "selected_item";

        btnAddNew.Enabled = btnSaveData.Enabled = btnDelete.Enabled = btnClear.Enabled =btnDelete.Enabled= true;

        //SetImages();

        BindaCurrency(Convert.ToInt32(dlCurrency.DataKeys[dlCurrency.SelectedIndex]));
    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {
        lblWarning.Text = string.Empty;

        Clear();
    }

    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        if (SaveCurrency())
            Clear();
    }

    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {
       // btnAddNew.Enabled = btnSaveData.Enabled = btnDelete.Enabled = false;
        btnAddNew.Enabled = btnSaveData.Enabled = btnSave.Enabled = btnOK.Enabled = true;

        //SetImages();

        Clear();
    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {
        DeleteCurrency();
    }

    private bool DeleteCurrency()
    {
        objCurrencyReference = new clsCurrencyReference();

        objCurrencyReference.CurrencyID = Convert.ToInt32(dlCurrency.DataKeys[dlCurrency.SelectedIndex]);

        string result = objCurrencyReference.DeleteCurrency();

        if (result != "0")
        {
            lblWarning.Text = "Cannot delete currency details.";
            return false;
        }
            
        else
        {
            lblWarning.Text = "Currency details deleted.";

            if (Update != null)
                Update();
      
            Clear(); 
            BindDatalist();
           
        }
        return true;    
    }

    #endregion

    #region Functions

    private void BindDatalist()
    {
        lblWarning.Text = string.Empty;
        objCurrencyReference = new clsCurrencyReference();

        btnDelete.Enabled = false;

        string guid = clsCommon.GetValidationGroup();
        clsCommon.WriteLog(guid);

        rfvCurrency.ValidationGroup = rfvShortName.ValidationGroup = rfvDecimal.ValidationGroup = rcCountry.ValidationGroup = rvDecimal.ValidationGroup = vsCurrency.ValidationGroup = btnOK.ValidationGroup = btnSave.ValidationGroup = btnSaveData.ValidationGroup = guid;
        btnOK.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        btnSaveData.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        btnSave.Attributes.Add("onclick", "return confirmSave('" + guid + "');");

        dlCurrency.DataSource = objCurrencyReference.GetAllCurrency();
        dlCurrency.DataBind();

    }

    private void BindaCurrency(int iCurrencyId)
    {
        objCurrencyReference = new clsCurrencyReference();
        
        objCurrencyReference.CurrencyID = iCurrencyId;
        DataTable dt = objCurrencyReference.GetaCurrency();

       // rcCountry.FillList();
        FillComboCountry();

        txtCurrency.Text = Convert.ToString(dt.Rows[0]["Currency"]);
        txtShortName.Text = Convert.ToString(dt.Rows[0]["Code"]);
        //rcCountry.ChangeIndex(Convert.ToInt32(dt.Rows[0]["CountryID"]));
        rcCountry.SelectedValue = dt.Rows[0]["CountryID"].ToString();
        txtDecimal.Text = Convert.ToString(dt.Rows[0]["Scale"]);

        btnDelete.Enabled = true;

        //bool bResult =  CanDeleteCurrency();

        //if (bResult)
        //    btnDelete.OnClientClick = "return true;";
        //else
        //{
        //    btnDelete.OnClientClick = "return false;";
            
        //}

        //SetImages();
    }

    private void SetImages()
    {
        if (btnAddNew.Enabled)
            btnAddNew.ImageUrl = "~/images/Plus_red.png";
        else
            btnAddNew.ImageUrl = "~/images/Plus_blue1.png";
        
        if(btnSaveData.Enabled)
            btnSaveData.ImageUrl = "~/images/save_icon.png";
        else
            btnSaveData.ImageUrl = "~/images/save_inactive.png";

        if(btnDelete.Enabled)
            btnDelete.ImageUrl = "~/images/Delete_red.png";
        else
            btnDelete.ImageUrl = "~/images/Delete__blue.png";

        if(btnClear.Enabled)
            btnClear.ImageUrl = "~/images/Document_Red.png";
        else
            btnClear.ImageUrl = "~/images/Document.png";
    }

    private bool CanDeleteCurrency()
    {
        objCurrencyReference = new clsCurrencyReference();

        objCurrencyReference.CurrencyID = Convert.ToInt32(dlCurrency.DataKeys[dlCurrency.SelectedIndex]);       
        string result = objCurrencyReference.CanDeleteCurrency();

        if (result != "0")
            return false;
        else
            return true;
    }

    private bool SaveCurrency()
    {
        objCompany = new clsCompany();
        objCurrencyReference = new clsCurrencyReference();

        int result = -1;
        int uResult = -1;

        objCurrencyReference.Currency = txtCurrency.Text;
        objCurrencyReference.ShortName = txtShortName.Text;
        objCurrencyReference.Country = Convert.ToInt32(rcCountry.SelectedValue);
        objCurrencyReference.DecimalPoint = Convert.ToInt16(txtDecimal.Text);

        if (dlCurrency.SelectedIndex == -1)
        {
            iCurrencyId  = objCurrencyReference.InsertCurrency();

            if (iCurrencyId == 0)
             {
                 lblWarning.Text = string.Format("Cannot add. {0} already exists.", txtCurrency.Text );
                 return false;
             }
            else if (iCurrencyId == -1)
            {
                lblWarning.Text = string.Format("Cannot add, currency already defined for the country {0}", this.rcCountry.SelectedItem.Text);
                return false;
            }
            else
            {
                lblWarning.Text = "New currency added sucessfully.";
            }
        }
        else
        {
            objCurrencyReference.CurrencyID = Convert.ToInt32(dlCurrency.DataKeys[dlCurrency.SelectedIndex]);
            iCurrencyId = objCurrencyReference.UpdateCurrency();

            if (iCurrencyId == 0)
            {
                lblWarning.Text = "Cannot update. Currency already exists.";
                return false;
            }
            else if (iCurrencyId == -1)
            {
                lblWarning.Text = string.Format("Cannot update, currency already defined for the country {0}", this.rcCountry.Text );
                return false;
            }
        }
        if (iCurrencyId == 0) return false;

        if (Update != null)  
            Update();
      
        BindDatalist();

        iCurrencyId = (result == -1 ? Convert.ToInt32(dlCurrency.DataKeys[dlCurrency.SelectedIndex]) : result);
        return true;
    }

    private void Clear()
    {
        txtCurrency.Text = string.Empty;
        txtShortName.Text = string.Empty;
        //rcCountry.FillList();
        FillComboCountry();
        //txtDecimal.Text = string.Empty;
        dlCurrency.SelectedIndex = -1;
        lblWarning.Text = string.Empty;

        btnDelete.Enabled = false;
    }

    public void FillList()
    {
        objCurrencyReference = new clsCurrencyReference();

       // rcCountry.FillList();
        FillComboCountry();
       // rcCountry.UpdatePanel();
        BindDatalist(); 

        if (iCurrencyId != -1)
        {
            objCurrencyReference.CurrencyID = iCurrencyId; 

            BindaCurrency(iCurrencyId);
        }
    }

    #endregion  

    protected void dlCurrency_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            object dlItem = e.Item.DataItem;

            int iCurrency = Convert.ToInt32(DataBinder.Eval(dlItem, "CurrencyID"));

            if (iCurrency == iCurrencyId)
            {
                dlCurrency.SelectedIndex = e.Item.ItemIndex;

                e.Item.CssClass = "selected_item";
            }
        }
    }
    //protected void imgCountry_Click(object sender, ImageClickEventArgs e)
    //{
    //    ReferenceControlNew.TableName = "CountryReference";
    //    ReferenceControlNew.DataTextField = "Description";
    //    ReferenceControlNew.DataValueField = "CountryId";
    //    ReferenceControlNew.FunctionName = "FillComboCountry";
    //    ReferenceControlNew.SelectedValue = rcCountry.SelectedValue;
    //    ReferenceControlNew.DisplayName = "Country";
    //    ReferenceControlNew.PopulateData();

    //    mdlPopUpReference.Show();
    //    updModalPopUp.Update();
    //}
    protected void btnCountryRef_Click(object sender, EventArgs e)
    {
        ReferenceControlNew.TableName = "CountryReference";
        ReferenceControlNew.DataTextField = "Countryname";
        ReferenceControlNew.DataValueField = "CountryId";
        ReferenceControlNew.FunctionName = "FillComboCountry";
        ReferenceControlNew.SelectedValue = rcCountry.SelectedValue;
        ReferenceControlNew.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
        ReferenceControlNew.PopulateData();

        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }
}
