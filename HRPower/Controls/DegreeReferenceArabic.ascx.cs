﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class Controls_DegreeReferenceArabic : System.Web.UI.UserControl
{
    #region Declarations
    clsDegreeReferenceArabic objclsDegreeReference;
    public delegate void OnUpdate();
    public event OnUpdate Update;

    string sModalPopupID;

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }
    #endregion Declarations

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatusMessage.Text = "";
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);               

        if (Update != null)
        {

            Update();
        }
        mpe.Hide();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            ClearControls();
            BindGrid();

            if (Update != null)
            {

                Update();
            }
        }
    }

    protected void gvDegreeReference_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objclsDegreeReference = new clsDegreeReferenceArabic();
        switch (e.CommandName)
        {
            case "_ALTER":

                objclsDegreeReference.DegreeID = Convert.ToInt32(e.CommandArgument);
                ViewState["DegreeID"] = Convert.ToInt32(e.CommandArgument);

                DataTable dt = objclsDegreeReference.GetDegreeDetail();

                if (dt.Rows.Count > 0)
                {
                    txtDegreeName.Text = dt.Rows[0]["Description"].ToString();
                    txtArabic.Text = dt.Rows[0]["DegreeArb"].ToString();
                    ddlQualificationType.SelectedIndex = ddlQualificationType.Items.IndexOf(ddlQualificationType.Items.FindByValue(dt.Rows[0]["QualificationTypeID"].ToString()));
                }

                break;

            case "_REMOVE":

                GridViewRow gr = (GridViewRow)((ImageButton)e.CommandSource).Parent.Parent;
                ImageButton ImgDelete = (ImageButton)gr.FindControl("ImgDelete");
                try
                {
                    objclsDegreeReference.DegreeID = Convert.ToInt32(e.CommandArgument);
                    objclsDegreeReference.Delete();

                    BindGrid();
                    ClearControls();

                    if (Update != null)
                    {

                        Update();
                    }
                }
                catch (SqlException ex)
                {
                    Trace.Write(ex.Message);
                    ScriptManager.RegisterClientScriptBlock(ImgDelete, ImgDelete.GetType(), "DeleteDegree", "alert('Sorry, Unable to delete.This degree details exists in the system.')", true);
                }

                break;
        }
    }

    protected void imgClear_Click(object sender, EventArgs e)
    {
        ClearControls();
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = divHeaderMove.ClientID;
    }
    protected void ddlQualificationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid();
    }

    #endregion Events

    #region Functions

    public void FillList()
    {
        objclsDegreeReference = new clsDegreeReferenceArabic();

        ddlQualificationType.DataSource = objclsDegreeReference.FillQualificationTypes();
        ddlQualificationType.DataBind();

        BindGrid();

    }

    private void BindGrid()
    {
        lblStatusMessage.Text = "";
        DataSet ds = null;
        objclsDegreeReference = new clsDegreeReferenceArabic();

        if (ddlQualificationType.SelectedValue.Trim() != string.Empty && ddlQualificationType.SelectedValue.Trim() != "-1")
        {
            objclsDegreeReference.QualificationTypeID = int.Parse(ddlQualificationType.SelectedValue);
            ds = objclsDegreeReference.BindDegreesByQualType();
            gvDegreeReference.DataSource = ds;
        }
        else
        {
            gvDegreeReference.DataSource = objclsDegreeReference.BindAllDegreeReferences();
        }
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblStatusMessage.Text = "";
                gvDegreeReference.DataBind();
            }
            else
            {
                gvDegreeReference.DataBind();
                lblStatusMessage.Text = "No Such Record !!";
            }
        }
    }

    private bool Save()
    {
        objclsDegreeReference = new clsDegreeReferenceArabic();

        objclsDegreeReference.Description = txtDegreeName.Text;
        objclsDegreeReference.DescriptionArabic = txtArabic.Text;
        objclsDegreeReference.QualificationTypeID = Convert.ToInt32(ddlQualificationType.SelectedValue);

        if (Convert.ToInt32(ViewState["DegreeID"]) > 0 && ViewState["DegreeID"] != null)
        {
            objclsDegreeReference.DegreeID = Convert.ToInt32(ViewState["DegreeID"]);
            int res = objclsDegreeReference.UpdateDegreeReference();
            if (res == 0)
            {
                lblStatusMessage.Text = "English degree name already exists.";
                return false;
            }
            else if (res < 0)
            {
                lblStatusMessage.Text = "Arabic degree name already exists.";
                return false;
            }

            else
            {
                lblStatusMessage.Text = "Successfully updated degree details.";
                return true;
            }
        }
        else
        {
            int res = objclsDegreeReference.InsertDegreeReference();
            if (res == 0)
            {
                lblStatusMessage.Text = "English degree name already exists.";
                return false;
            }
            else if (res < 0)
            {
                lblStatusMessage.Text = "Arabic degree name already exists.";
                return false;
            }
            else
            {
                lblStatusMessage.Text = "Successfully added degree details.";
                return true;
            }
        }
    }    

    private void ClearControls()
    {
        txtDegreeName.Text = string.Empty;
        ddlQualificationType.SelectedIndex = 0;
        txtArabic.Text = "";

        ViewState["DegreeID"] = null;
    }

    #endregion Functions   
}
