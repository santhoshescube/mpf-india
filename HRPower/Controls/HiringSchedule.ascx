﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HiringSchedule.ascx.cs"
    Inherits="Controls_HiringSchedule" %>
<div id="popupmain1" style="width: 70%; height: auto; overflow: auto; background-color: White">
    <div id="header11" style="width: 100%">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" Text="Hiring Schedule"></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                        OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <div style="width: 100%; overflow: hidden;">
        <asp:UpdatePanel ID="updScheduleHierarchy" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="clear: both">
                </div>
                <div style="display: block; border: solid 1px black; padding-top: 10px; min-height: 170px">
                    <div style="margin-top: 15px">
                        <div style="clear: both">
                            <asp:DataList ID="dlHiringSchedules" Width="100%" runat="server" CellPadding="4"
                                ForeColor="#333333" onitemdatabound="dlHiringSchedules_ItemDataBound" 
                                onselectedindexchanged="dlHiringSchedules_SelectedIndexChanged">
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                           <td style="width: 253px">
                                                <asp:Literal ID="litTotal" Text="Designation" runat="server">
                                                </asp:Literal>
                                            </td>
                                             <td style="width: 218px">
                                                <asp:Literal ID="Literal1" Text="Month" runat="server">
                                                </asp:Literal>
                                            </td>
                                             <td style="width: 200px">
                                                <asp:Literal ID="Literal2" Text="Hire" runat="server">
                                                </asp:Literal>
                                            </td>
                                            
                                            <td style="width: 20px">
                                            </td>
                                           
                                        </tr>
                                        
                                        
                                        
                                        
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <ItemTemplate>
                                    <table>
                                          <tr>
                                           <td style="width: 184px">
                                              <asp:DropDownList ID="ddlDesignation"  Width ="180px"  runat ="server" Enabled = "false"></asp:DropDownList> 
                                              <asp:HiddenField ID="hfDesignationID"  Value='<%#Eval("DesignationID") %>' runat ="server" ></asp:HiddenField> 
                                            </td>
                                             <td style="width: 150px">
                                              <asp:TextBox ID="txtMonth" Width="120px" Height ="7px" runat ="server" ></asp:TextBox>
                                              
                                              <AjaxControlToolkit:CalendarExtender ID="CalMonth" Format ="MMM-yyyy" runat="server" TargetControlID ="txtMonth"  PopupButtonID="imgMonth">
                                              </AjaxControlToolkit:CalendarExtender>
                                              <asp:ImageButton style="padding-left:4px" ID="imgMonth" runat ="server" ImageUrl ="~/images/calendar.png" />
                                            </td>
                                             <td style="width: 100px">
                                                  <asp:TextBox ID="txtNewHire"  Height ="7px"  Width="100px" runat ="server" ></asp:TextBox>
                                            </td>
                                            
                                            <td style="width: 20px">
                                            <asp:Button ID="btnAdd" runat ="server" Text ="+" />
                                            </td>
                                           
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both;" id="footerwrapper">
        <div id="footer11" style="width: 100%">
            <asp:UpdatePanel ID="updSubmit" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="float: right">
                        <asp:Button ID="Button1" CssClass="btnsubmit" runat="server" Text="Save" OnClick="Button1_Click" />
                   
                    <asp:Button ID="Button2" runat="server" CssClass="btnsubmit" 
                        onclick="Button2_Click" Text="Cancel" />
                         </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
