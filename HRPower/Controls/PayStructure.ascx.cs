﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_PayStructure : System.Web.UI.UserControl
{
    clsPayStructure objPayStructure;

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadCombos();
    }

    private void LoadCombos()
    {
        if (objPayStructure == null)
            objPayStructure = new clsPayStructure();

        DataSet dsTemp = new DataSet();
        dsTemp = objPayStructure.LoadCombos();

        // Designation
        ddlDesignation.DataSource = dsTemp.Tables[0];
        ddlDesignation.DataBind();

        if (ddlDesignation.Items.Count == 0)
            ddlDesignation.Items.Add(new ListItem("Select", "-1"));

        // Payment Classification
        ddlPaymentMode.DataSource = dsTemp.Tables[1];
        ddlPaymentMode.DataBind();

        if (ddlPaymentMode.Items.Count == 0)
            ddlPaymentMode.Items.Add(new ListItem("Select", "-1"));
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void btnAllowances_Click(object sender, EventArgs e)
    {
        ViewState["Allowances"] = null;
        UpdateDataList();
        NewAllowancesAdd();
    }

    private void UpdateDataList()
    {
        DataTable dt = new DataTable();
        DataRow dw;

        dt.Columns.Add("JobSalaryID");
        dt.Columns.Add("AdditionDeductionID");
        dt.Columns.Add("Amount");

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

            dw = dt.NewRow();
            dw["JobSalaryID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
            dw["AdditionDeductionID"] = Convert.ToInt32(ddlParticulars.SelectedValue);
            dw["Amount"] = Convert.ToDecimal(txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text);
            dt.Rows.Add(dw);
        }

        ViewState["Allowances"] = dt;
    }

    private void NewAllowancesAdd()
    {
        DataTable dt = (DataTable)ViewState["Allowances"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("JobSalaryID");
            dt.Columns.Add("AdditionDeductionID");
            dt.Columns.Add("Amount");
        }

        DataRow dw = dt.NewRow();
        dt.Rows.Add(dw);
        dlDesignationAllowances.DataSource = dt;
        dlDesignationAllowances.DataBind();
    }

    protected void dlDesignationAllowances_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ALLOWANCE":
                DataTable dt = new DataTable();
                DataRow dw;

                dt.Columns.Add("JobSalaryID");
                dt.Columns.Add("AdditionDeductionID");
                dt.Columns.Add("Amount");

                foreach (DataListItem item in dlDesignationAllowances.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

                    dw = dt.NewRow();
                    dw["JobSalaryID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
                    dw["AdditionDeductionID"] = Convert.ToInt32(ddlParticulars.SelectedValue);
                    dw["Amount"] = Convert.ToDecimal(txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text);

                    if (item != e.Item)
                        dt.Rows.Add(dw);

                    if (e.CommandArgument.ToInt32() == 1)
                    {
                        lblStatusMessage.Text = "Basic pay is essential thing in pay structure.";

                        bool IsExists = false;

                        foreach (DataRow row in dt.Rows)
                        {
                            if (dw["AdditionDeductionID"].ToInt32() == row["AdditionDeductionID"].ToInt32())
                            {
                                IsExists = true;
                                break;
                            }
                        }

                        if (!IsExists)
                            dt.Rows.Add(dw);
                    }

                    ViewState["Allowances"] = dt;
                }

                dlDesignationAllowances.DataSource = ViewState["Allowances"];
                dlDesignationAllowances.DataBind();
                CalculateNetAmount();
                break;
        }
    }

    protected void dlDesignationAllowances_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objPayStructure = new clsPayStructure();

        DropDownList ddlParticulars = (DropDownList)e.Item.FindControl("ddlParticulars");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");
        HiddenField hdParticulars = (HiddenField)e.Item.FindControl("hdParticulars");

        ddlParticulars.DataSource = objPayStructure.FillParticulars();
        ddlParticulars.DataBind();
        ddlParticulars.Items.Insert(0, new ListItem("--select--", "-1"));

        if (hdParticulars.Value.ToInt64() > 0)
            objPayStructure.AdditionDeductionID = Convert.ToInt32(hdParticulars.Value);

        objPayStructure.AdditionDeductionID = Convert.ToInt32(ddlParticulars.SelectedValue);

        if (Convert.ToString(hdParticulars.Value) != "")
            ddlParticulars.SelectedIndex = ddlParticulars.Items.IndexOf(ddlParticulars.Items.FindByValue(Convert.ToString(hdParticulars.Value)));
    }

    private void CalculateNetAmount()
    {
        bool bAddDedID = true;
        decimal dNetAmt = 0.000M;

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

            if (txtAmount.Text != "")
                dNetAmt = Convert.ToDecimal(dNetAmt) + Convert.ToDecimal(txtAmount.Text);

            if (txtAmount == null) return;

            ScriptManager.RegisterClientScriptBlock(txtAmount, txtAmount.GetType(), "Calculate", "CalculateSalaryStructure('" + txtAmount.ClientID + "')", true);
        }

        lblAddition.Text = string.Format("{0:#,###.000}", dNetAmt);
    }
}