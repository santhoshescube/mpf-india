﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_CandidateViewArabic : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (IsPostBack)
        //    GetCandidateInfo();

    }


    public void GetCandidateInfo(Int64 CandidateID)
    {
        DataRow DrCandidateBasicInfo;
        DataRow DrCandidateProfessionalInfo;
        DataRow DrCandidateDocumentInfo;
        DataRow DrCandidateOtherInfo;


        string English = "";
        string Arabic = "";
        string OtherLan = "";
        using (DataSet ds = clsCandidate.GetCandidateDetails(CandidateID))
        {
            if (ds.Tables.Count == 4)
            {

                //Candidat Basic info
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DrCandidateBasicInfo = ds.Tables[0].Rows[0];

                    divCandidateCode.InnerText = DrCandidateBasicInfo["CandidateCode"].ToString();
                    dvApplicationNo.InnerText = DrCandidateBasicInfo["ApplicationNo"].ToString();


                    //dvAgencyArabic.InnerText = DrCandidateBasicInfo["AgencyArb"].ToString();
                    //dvAgency.InnerText = DrCandidateBasicInfo["Agency"].ToString();


                    dvApplicationNo.InnerText = DrCandidateBasicInfo["ApplicationNo"].ToString();
                
                    dvCandidateNameEng.InnerText = DrCandidateBasicInfo["CandidateNameEnglish"].ToString();
                    dvCandidateNameAr.InnerText = DrCandidateBasicInfo["CandidateNameArabic"].ToString();

                    dvCitizenShip.InnerText = DrCandidateBasicInfo["CitizenShip"].ToString();
                    dvCitizenShipArabic.InnerText = DrCandidateBasicInfo["CitizenShipArb"].ToString();


                    dvGender.InnerText = DrCandidateBasicInfo["Gender"].ToString();
                    if (DrCandidateBasicInfo["Gender"].ToString() == "Male")
                        dvGenderArabic.InnerText = "ذكر";
                    else
                        dvGenderArabic.InnerText = "أنثى";   

                    dvPlaceOfBirth.InnerText = DrCandidateBasicInfo["PlaceOfBirth"].ToString();
                    dvPlaceOfBirthArabic.InnerText = DrCandidateBasicInfo["PlaceOfBirthArb"].ToString();

                    dvDateOfBirth.InnerText = DrCandidateBasicInfo["DateOfBirth"].ToString();
                    dvDateOfBirthArabic.InnerText = DrCandidateBasicInfo["DateOfBirth"].ToString();

                    dvMaritalStatus.InnerText = DrCandidateBasicInfo["MaritalStatus"].ToString();
                    dvMaritalStatusArabic.InnerText = DrCandidateBasicInfo["MaritalStatusArb"].ToString();

                    dvNoOfSon.InnerText = DrCandidateBasicInfo["NoOfSon"].ToString();
                    dvNoOfSonArabic.InnerText = DrCandidateBasicInfo["NoOfSon"].ToString();

                    dvReligion.InnerText = DrCandidateBasicInfo["Religion"].ToString();
                    dvReligionArabic.InnerText = DrCandidateBasicInfo["ReligionArb"].ToString();

                    dvMothersName.InnerText = DrCandidateBasicInfo["MothersName"].ToString();
                    dvMothersNameArabic.InnerText = DrCandidateBasicInfo["MothersNameArb"].ToString();

                    dvCurrentNationlity.InnerText = DrCandidateBasicInfo["CurrentNationlity"].ToString();
                    dvCurrentNationlityArabic.InnerText = DrCandidateBasicInfo["CurrentNationlityArb"].ToString();

                    dvPreviousNationality.InnerText = DrCandidateBasicInfo["PreviousNationality"].ToString();
                    dvPreviousNationalityArabic.InnerText = DrCandidateBasicInfo["PreviousNationalityArb"].ToString();

                    dvDoctrine.InnerText = DrCandidateBasicInfo["Doctrine"].ToString();
                    dvDoctrineArabic.InnerText = DrCandidateBasicInfo["DoctrineArb"].ToString();

                    dvTelephone.InnerText = DrCandidateBasicInfo["Telephone"].ToString();
                    dvTelephoneArabic.InnerText = DrCandidateBasicInfo["Telephone"].ToString();

                    dvAddress.InnerText = DrCandidateBasicInfo["BusinessPairAddress"].ToString();
                    dvAddressArabic.InnerText = DrCandidateBasicInfo["BusinessPairAddressArb"].ToString();

                    dvPOBOX.InnerText = DrCandidateBasicInfo["POBOX"].ToString();
                    dvPOBOXArabic.InnerText = DrCandidateBasicInfo["POBOX"].ToString();

                    dvPlaceOfBusinessPair.InnerText = DrCandidateBasicInfo["PlaceOfBusinessPair"].ToString();
                    dvPlaceOfBusinessPairArabic.InnerText = DrCandidateBasicInfo["PlaceOfBusinessPairArb"].ToString();

                    dvReferralType.InnerText = DrCandidateBasicInfo["ReferralType"].ToString();
                    dvReferralTypeArb.InnerText = DrCandidateBasicInfo["ReferralTypeArb"].ToString();

                    if (DrCandidateBasicInfo["Agency"].ToString() != "")    //If Referred By Agency
                    {
                        dvReferredBy.InnerText = DrCandidateBasicInfo["Agency"].ToString();                        
                        dvReferredByArb.InnerText = DrCandidateBasicInfo["AgencyArb"].ToString();
                    }
                    else if (DrCandidateBasicInfo["Description"].ToString() != "")      //If Referred By JobPortal
                    {                        
                        dvReferredBy.InnerText = DrCandidateBasicInfo["Description"].ToString();
                        dvReferredByArb.InnerText = DrCandidateBasicInfo["Description"].ToString();
                    }
                    else if (DrCandidateBasicInfo["EmployeeFullName"].ToString() != "")     //If Referred By Employee
                    {                        
                        dvReferredBy.InnerText = DrCandidateBasicInfo["EmployeeFullName"].ToString();
                        dvReferredByArb.InnerText = DrCandidateBasicInfo["EmployeeFullNameArb"].ToString();
                    }
                    else
                    {
                        dvReferredBy.InnerText = "";
                        dvReferredByArb.InnerText = "";
                    }                    

                    dvCompanyType.InnerText = DrCandidateBasicInfo["CompanyType"].ToString();
                    dvCompanyTypeArabic.InnerText = DrCandidateBasicInfo["CompanyTypeArb"].ToString();

                    dvCurrentAddress.InnerText = DrCandidateBasicInfo["CurrentAddress"].ToString();
                    dvCurrentAddressArabic.InnerText = DrCandidateBasicInfo["CurrentAddressArb"].ToString();

                    dvCountry.InnerText = DrCandidateBasicInfo["Country"].ToString();
                    dvCountryArabic.InnerText = DrCandidateBasicInfo["CountryArb"].ToString();

                    dvEmail.InnerText = DrCandidateBasicInfo["EmailID"].ToString();

                }


                if (ds.Tables[1].Rows.Count > 0)
                {
                    DrCandidateProfessionalInfo = ds.Tables[1].Rows[0];

                    dvQualification.InnerText = DrCandidateProfessionalInfo["QualificationType"].ToString();
                    dvQualificationArabic.InnerText = DrCandidateProfessionalInfo["QualificationTypeArb"].ToString();


                    dvCAtegory.InnerText = DrCandidateProfessionalInfo["QualificationCategory"].ToString();
                    dvCAtegoryArabic.InnerText = DrCandidateProfessionalInfo["QualificationCategoryArb"].ToString();

                    //dvGraduation.InnerText = DrCandidateProfessionalInfo["GraduationYear"].ToString();
                    //dvGraduationArabic.InnerText = DrCandidateProfessionalInfo["GraduationArb"].ToString();

                    dvGraduationYear.InnerText = DrCandidateProfessionalInfo["GraduationYear"].ToInt32() > 0 ? DrCandidateProfessionalInfo["GraduationYear"].ToString() : "";
                    dvGraduationYearArabic.InnerText = DrCandidateProfessionalInfo["GraduationYear"].ToInt32() > 0 ? DrCandidateProfessionalInfo["GraduationYear"].ToString() : "";

                    dvPlaceOfGraduation.InnerText = DrCandidateProfessionalInfo["PlaceOfGraduation"].ToString();
                    dvPlaceOfGraduationArabic.InnerText = DrCandidateProfessionalInfo["PlaceOfGraduationArb"].ToString();

                    dvCollegeOrSchool.InnerText = DrCandidateProfessionalInfo["CollegeSchool"].ToString();
                    dvCollegeOrSchoolArabic.InnerText = DrCandidateProfessionalInfo["CollegeSchoolArb"].ToString();

                    dvPreviousJob.InnerText = DrCandidateProfessionalInfo["PreviousJob"].ToString();
                    dvPreviousJobArabic.InnerText = DrCandidateProfessionalInfo["PreviousJobArb"].ToString();

                    dvSector.InnerText = DrCandidateProfessionalInfo["Sector"].ToString();
                    dvSectorArabic.InnerText = DrCandidateProfessionalInfo["SectorArb"].ToString();

                    dvPosition.InnerText = DrCandidateProfessionalInfo["Position"].ToString();
                    dvPositionArabic.InnerText = DrCandidateProfessionalInfo["PositionArb"].ToString();
                    

                    dvTypeOfExperience.InnerText = DrCandidateProfessionalInfo["TypeOfExperience"].ToString();
                    dvTypeOfExperienceArabic.InnerText = DrCandidateProfessionalInfo["TypeOfExperienceArb"].ToString();

                    dvYearsOfExperience.InnerText = DrCandidateProfessionalInfo["Experienceyear"].ToString();
                    dvYearsOfExperienceArabic.InnerText = DrCandidateProfessionalInfo["Experienceyear"].ToString();

                    dvCompensation.InnerText = DrCandidateProfessionalInfo["Compensation"].ToString();
                    dvCompensationArabic.InnerText = DrCandidateProfessionalInfo["CompensationArb"].ToString();

                    dvSpecialization.InnerText = DrCandidateProfessionalInfo["Specialization"].ToString();
                    dvSpecializationArabic.InnerText = DrCandidateProfessionalInfo["SpecializationArb"].ToString();

                    dvDateOfTransaction.InnerText = DrCandidateProfessionalInfo["TransactionEntryDate"].ToString();
                    dvDateOfTransactionArabic.InnerText = DrCandidateProfessionalInfo["TransactionEntryDate"].ToString();

                    dvExpectedJoinDate.InnerText = DrCandidateProfessionalInfo["ExpectedJoinDate"].ToString();
                    dvExpectedJoinDateArabic.InnerText = DrCandidateProfessionalInfo["ExpectedJoinDate"].ToString();

                    dvSkills.InnerText = DrCandidateProfessionalInfo["Skills"].ToString();
                    dvSkillsArabic.InnerText = DrCandidateProfessionalInfo["Skills"].ToString();



                    if (Convert.ToString(DrCandidateProfessionalInfo["ReadingEng"])!="")
                    {
                        English = Convert.ToString(DrCandidateProfessionalInfo["ReadingEng"]);
                    }

                    if (Convert.ToString(DrCandidateProfessionalInfo["WritingEng"])!= "")
                    {
                        if (English != "")
                            English = English + "/";
                        English = English + Convert.ToString(DrCandidateProfessionalInfo["WritingEng"]);
                    }

                    dvLanguagesEnglish.InnerText = English;




                    if (Convert.ToString(DrCandidateProfessionalInfo["ReadingArb"])!= "")
                    {
                        Arabic = Convert.ToString(DrCandidateProfessionalInfo["ReadingArb"]);
                    }

                    if (Convert.ToString(DrCandidateProfessionalInfo["WritingArb"]) != "")
                    {
                        if (Arabic != "")
                            Arabic = Arabic + "/";
                        Arabic = Arabic + Convert.ToString(DrCandidateProfessionalInfo["WritingArb"]);
                    }



                    dvLaunguagesArabic.InnerText = Arabic;


                    dvOtherLangC1.InnerText =
                          dvOtherLanguagesC2.InnerText = "";


                    if (DrCandidateProfessionalInfo["Other"].ToString() == "")
                    {
                        dvOtherLangC1.InnerText =
                        dvOtherLanguagesC2.InnerText =dvOtherLangb.InnerText = "";

                        //dvOtherLangC1.Attributes.Add("style", "display:none");
                        //dvOtherLanguagesC2.Attributes.Add("style", "display:none");
                    }
                    else
                    {
                        //dvOtherLangC1.Attributes.Add("style", "display:block");
                        //dvOtherLanguagesC2.Attributes.Add("style", "display:block");
                        dvOtherLangb.InnerText = ":";
                        if (Convert.ToBoolean(DrCandidateProfessionalInfo["ReadingOther"]))
                        {
                            OtherLan = "Read";
                        }

                        if (Convert.ToBoolean(DrCandidateProfessionalInfo["WritingOther"]))
                        {
                            if (OtherLan != "")
                                OtherLan = OtherLan + "/";
                            OtherLan = OtherLan + "Write ";
                        }



                        dvOtherLangC1.InnerText = DrCandidateProfessionalInfo["Other"].ToString();
                        dvOtherLanguagesC2.InnerText = OtherLan;


                    }



                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    DrCandidateDocumentInfo = ds.Tables[2].Rows[0];

                    dvPassportNumber.InnerText = DrCandidateDocumentInfo["PassportNumber"].ToString();
                    dvPassportNumberArabic.InnerText = DrCandidateDocumentInfo["PassportNumberArb"].ToString();

                    dvPassportCountry.InnerText = DrCandidateDocumentInfo["PassportCountry"].ToString();
                    dvPassportCountryArabic.InnerText = DrCandidateDocumentInfo["PassportCountryArb"].ToString();

                    dvPassportExpiryDate.InnerText = DrCandidateDocumentInfo["PassportExpiryDate"].ToString();
                    dvPassportExpiryDateArabic.InnerText = DrCandidateDocumentInfo["PassportExpiryDate"].ToString();

                    dvPassportIssueDate.InnerText = DrCandidateDocumentInfo["PassportIssueDate"].ToString();
                    dvPassportIssueDateArabic.InnerText = DrCandidateDocumentInfo["PassportIssueDate"].ToString();

                    dvPassportPlaceOfIssue.InnerText = DrCandidateDocumentInfo["PassportIssuePlace"].ToString();
                    dvPassportPlaceOfIssueArabic.InnerText = DrCandidateDocumentInfo["PassportIssuePlaceArb"].ToString();

                    dvVisaCountry.InnerText = DrCandidateDocumentInfo["VisaCountry"].ToString();
                    dvVisaCountryArabic.InnerText = DrCandidateDocumentInfo["VisaCountryArb"].ToString();

                    dvVisaExpiryDate.InnerText = DrCandidateDocumentInfo["VisaExpiryDate"].ToString();
                    dvVisaExpiryDateArabic.InnerText = DrCandidateDocumentInfo["VisaExpiryDate"].ToString();


                    dvVisaIssueDate.InnerText = DrCandidateDocumentInfo["VisaIssueDate"].ToString();
                    dvVisaIssueDateArabic.InnerText = DrCandidateDocumentInfo["VisaIssueDate"].ToString();

                    dvVisaNumber.InnerText = DrCandidateDocumentInfo["VisaNumber"].ToString();
                    dvVisaNumberArabic.InnerText = DrCandidateDocumentInfo["VisaNumberArb"].ToString();

                    dvVisaPlaceOfIssue.InnerText = DrCandidateDocumentInfo["VisaISsuePlace"].ToString();
                    dvVisaPlaceOfIssueArabic.InnerText = DrCandidateDocumentInfo["VisaISsuePlaceArb"].ToString();

                    dvVisaUnifiedNumber.InnerText = DrCandidateDocumentInfo["UnifiedNumber"].ToString();
                    dvVisaUnifiedNumberArabic.InnerText = DrCandidateDocumentInfo["UnifiedNumberArb"].ToString();



                    dvNationalCardNumber.InnerText = DrCandidateDocumentInfo["NationalCardNumber"].ToString();
                    dvNationalCardNumberArabic.InnerText = DrCandidateDocumentInfo["NationalCardNumberArb"].ToString();

                    dvCardNo.InnerText = DrCandidateDocumentInfo["CardNumber"].ToString();
                    dvCardNoArabic.InnerText = DrCandidateDocumentInfo["CardNumberArb"].ToString();

                    dvCardNoExpiry.InnerText = DrCandidateDocumentInfo["NationalCardExpiryDate"].ToString();
                    dvCardNoExpiryArabic.InnerText = DrCandidateDocumentInfo["NationalCardExpiryDate"].ToString();




                }

                if (ds.Tables[3].Rows.Count > 0)
                {
                    DrCandidateOtherInfo = ds.Tables[3].Rows[0];

                    dvOtherPermanentAddress.InnerText = DrCandidateOtherInfo["PermanentAddress"].ToString();
                    dvOtherPermanentAddressArabic.InnerText = DrCandidateOtherInfo["PermanentAddressArb"].ToString();

                    dvOtherHomeStreet.InnerText = DrCandidateOtherInfo["HomeStreet"].ToString();
                    dvOtherHomeStreetArabic.InnerText = DrCandidateOtherInfo["HomeStreetArb"].ToString();

                    dvOtherCity.InnerText = DrCandidateOtherInfo["City"].ToString();
                    dvOtherCityArabic.InnerText = DrCandidateOtherInfo["CityArb"].ToString();


                    dvOtherHomeTelephone.InnerText = DrCandidateOtherInfo["HomeTelephone"].ToString();
                    dvOtherHomeTelephoneArabic.InnerText = DrCandidateOtherInfo["HomeTelephone"].ToString();


                    dvOtherBusinessAddress.InnerText = DrCandidateOtherInfo["BusinessAddress"].ToString();
                    dvOtherBusinessAddressArabic.InnerText = DrCandidateOtherInfo["BusinessAddressArb"].ToString();

                    dvOtherBusinessStreet.InnerText = DrCandidateOtherInfo["BusinessStreet"].ToString();
                    dvOtherBusinessStreetArabic.InnerText = DrCandidateOtherInfo["BusinessStreetArb"].ToString();


                    dvOtherArea.InnerText = DrCandidateOtherInfo["Area"].ToString();
                    dvOtherAreaArabic.InnerText = DrCandidateOtherInfo["AreaArb"].ToString();

                    dvOtherPOBox.InnerText = DrCandidateOtherInfo["POBOX"].ToString();
                    dvOtherPOBoxArabic.InnerText = DrCandidateOtherInfo["POBOX"].ToString();

                    dvOtherFax.InnerText = DrCandidateOtherInfo["Fax"].ToString();
                    dvOtherFaxArabic.InnerText = DrCandidateOtherInfo["Fax"].ToString();



                    dvOtherTelephone.InnerText = DrCandidateOtherInfo["TelephoneNumber"].ToString();
                    dvOtherTelephoneArabic.InnerText = DrCandidateOtherInfo["TelephoneNumber"].ToString();


                    dvOtherMob.InnerText = DrCandidateOtherInfo["MobileNumber"].ToString();
                    dvOtherMobArabic.InnerText = DrCandidateOtherInfo["MobileNumber"].ToString();

                    dvOtherSignUpDate.InnerText = DrCandidateOtherInfo["SignUpdate"].ToString();
                    dvOtherSignUpDateArabic.InnerText = DrCandidateOtherInfo["SignUpdate"].ToString();

                    //dvOtherLoginPassword.InnerText = DrCandidateOtherInfo[""].ToString();

                    dvOtherInsurance.InnerText = DrCandidateOtherInfo["InsuranceNumber"].ToString();
                    dvOtherInsuranceArabic.InnerText = DrCandidateOtherInfo["InsuranceNumberArb"].ToString();

                    //dvOtherPlaceOfBusiness.InnerText = DrCandidateOtherInfo["PlaceOfBusinessPair"].ToString();

                    //dvOtherAddress.InnerText = DrCandidateOtherInfo["BusinessPairAddress"].ToString();

                    //dvOtherCompanyType.InnerText = DrCandidateOtherInfo["CompanyType"].ToString();


                }
            }
        }
    }

}
