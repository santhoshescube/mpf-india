﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentReceiptIssue.ascx.cs"
    Inherits="Controls_DocumentReceiptIssue" %>

<script type="text/javascript">

 function validateReceivedFrom(source,args)
 {

     var Validator = document.getElementById(source.id);
     var cboReferenceNumber = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"cboReferenceNumber");
     var lblTypeName = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"lblTypeName");
     var Message ="";
     
     
     var SelectedValue = cboReferenceNumber.value;
     if(cboReferenceNumber)
     {
        if(SelectedValue < 0)
        {
            if(lblTypeName.innerText.startsWith("Issue"))
                Message ="*";
            else
                Message ="*";
            
            if(source.innerText == undefined) 
             { 
                source.textContent =  Message; 
             }
            else
             {
                source.innerText =  Message; 
             }
            args.IsValid = false;
   
      }
      else
            args.IsValid = true;
 }
 
 
 }
 
 
 
 function validateCustodian(source,args)
 {

     var Validator = document.getElementById(source.id);
     var cboCustodian = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"cboCustodian");
     var lblTypeName = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"lblTypeName");
     var Message ="";
     
     
     var SelectedValue = cboCustodian.value;
     if(cboCustodian)
     {
        if(SelectedValue < 0)
        {
            if(lblTypeName.innerText.startsWith("Issue"))
                Message ="*";
            else
                Message ="*";
            
            if(source.innerText == undefined) 
             { 
                source.textContent =  Message; 
             }
            else
             {
                source.innerText =  Message; 
             }
            args.IsValid = false;
   
      }
      else
            args.IsValid = true;
 }
 
 }
 
 
  
 function validateDocumentStatus(source,args)
 {
     var Validator = document.getElementById(source.id);
     var cboDocumentStatus = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"cboDocumentStatus");
     var lblTypeName = document.getElementById(source.id.substring(0,source.id.lastIndexOf("_")+1)+"lblTypeName");
     var Message ="";
     
     
     var SelectedValue = cboDocumentStatus.value;
     if(cboDocumentStatus)
     {
        if(SelectedValue < 0)
        {
            if(lblTypeName.innerText.startsWith("Issue"))
                Message ="*";
            else
                Message ="*";
            
            if(source.innerText == undefined) 
             { 
                source.textContent =  Message; 
             }
            else
             {
                source.innerText =  Message; 
             }
            args.IsValid = false;
   
      }
      else
            args.IsValid = true;
 }
 
 }
 
 
 
 
 
 
function validateDocumentIssueReceipt(source, args)
{

//    //Receipt Date
    var txtTransactionDate = document.getElementById(source.controltovalidate);
    var txtExpectedReturnDate = document.getElementById(source.controltovalidate.replace("txtTransactionDate","txtExpectedReturnDate"));
    var hfCurrentDate = document.getElementById(source.controltovalidate.replace("txtTransactionDate","hfCurrentDate"));
    var message ="";
      var hidControl = document.getElementById("ctl00_hidCulture");
    if(!isValidDate(txtTransactionDate.value))
    {
//        if(txtExpectedReturnDate)
//            message ='issue date';
//        else
//            message ='receipt date';
    
       if(hidControl.value == "en-US")
            source.textContent =  'Please enter valid date'; 
          else
            source.textContent =  'الرجاء إدخال تاريخ صالح';
            
        args.IsValid = false;
      return ;
    }
    
    if(txtExpectedReturnDate)
    {
    
      if(txtExpectedReturnDate.value =='')
      {
      
//        if(source.innerText == undefined) { source.textContent =  'Please enter expected return date'; }
//                else { source.innerText =  'Please enter expected return date'; }
//                
//            args.IsValid = false;
//            canContinue = false;
//            return ;
      }
      else
      {
          if(!isValidDate(txtExpectedReturnDate.value))
          {
             if(hidControl.value == "en-US")
                 source.textContent =  'Please enter valid date'; 
             else
                source.textContent =  'الرجاء إدخال تاريخ صالح';
       
            args.IsValid = false;
            canContinue = false;
            return ;
          }
      }
    
    }
    

    var validReceiptDate  = Convert2Date(txtTransactionDate.value);
    var hfCurrentDate  = Convert2Date(hfCurrentDate.defaultValue);
    
    if(txtExpectedReturnDate)
    {
        if(txtExpectedReturnDate.value != '')
        {
            var validIssueDate  = Convert2Date(txtExpectedReturnDate.value);

            if(validIssueDate < validReceiptDate)
            {

               if(hidControl.value == "en-US")
                 source.textContent =  'Expected return date must be greater than issued date'; 
             else
                source.textContent =  ' يجب أن يكون التاريخ المتوقع عائد أكبر من تاريخ صدر';

                args.IsValid = false;
                return ;
            }
        }
    }
    else
    {
        if(validReceiptDate > hfCurrentDate)
        {
        
        
      
        
            if(hidControl.value == "en-US")
                 source.textContent =  'Receipt date must not be a future date'; 
             else
                source.textContent =  '  يجب ألا يكون تاريخ استلام موعد لاحق';

           

            args.IsValid = false;
            return ;
        }

    }

  }
 
 
 
 
 
 
 
 
</script>

<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/default_common.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    .Left
    {
        float: left;
        width: 150px;
        height: 30px;
        margin-left: 6px;
     
    }
    .Right
    {
        float: left;
        width: 220px;
        height: 30px;
    }
</style>
<div id="popupmain" style="width: 500px; height: auto; overflow: auto">
    <div id="header11">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" Text="Document Receipt"></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                        OnClick="ibtnClose_Click" CausesValidation="true"   ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <div id="toolbar">
        <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <div style="float: left; height: 18px;display:none">
                    <div id="imgbuttonss">
                        <asp:ImageButton ID="btnSaveItem" CssClass="imgbuttons" runat="server" ToolTip="Save Data"
                            OnClick="btnSaveData_Click" ValidationGroup="submit" ImageUrl="~/images/Save%20Blue.png" />
                        <asp:ImageButton ID="btnPrint" CssClass="imgbuttons" runat="server" CausesValidation="False"
                            ToolTip="Delete" OnClick="btnDelete_Click" OnClientClick="return confirm('Do you wish to delete this information?');"
                            ImageUrl="~/images/delete_icon_popup.png" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="width: 480px; overflow: hidden">
        <asp:UpdatePanel ID="upd" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <div style="margin-top: 10px;">
                    <div>
                        <div class="Left">
                            <asp:Label ID="Label1" CssClass="labeltext" runat="server" meta:resourcekey="Type" />
                        </div>
                        <div class="Right">
                            <asp:RadioButton Checked="true" ID="rdbEmployee" runat="server" Text='<%$Resources:DocumentsCommon,Employee%>' GroupName="Company" />
                            <asp:RadioButton ID="rdbCompany" runat="server" Text='<%$Resources:DocumentsCommon,Company%>' GroupName="Company" />
                        </div>
                    </div>
                    <div>
                        <div class="Left">
                            <asp:Label ID="Label2" CssClass="labeltext" runat="server" meta:resourcekey="DocumentType" />
                        </div>
                        <div class="Right">
                            <asp:DropDownList Width="220px" ID="cboDocumentType" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvEmployeeNumberpnlTab1" runat="server" ControlToValidate="cboDocumentType"
                                CssClass="error" Display="Dynamic" InitialValue="-1" ErrorMessage="*"
                                ValidationGroup="Document"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div>
                        <div class="Left">
                            <asp:Label ID="Label3" CssClass="labeltext" runat="server" meta:resourcekey="DocumentNumber" />
                        </div>
                        <div class="Right">
                            <asp:TextBox ID="txtDocumentNumber" Width="200px" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDocumentNumber"
                                CssClass="error" ErrorMessage="*" ValidationGroup="Document"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div>
                        <div class="Left"  >
                            <asp:Label ID="lblTypeName" CssClass="labeltext" runat="server" meta:resourcekey="ReceivedFrom">
                            </asp:Label>
                        </div>
                        <div class="Right"  style="height: auto">
                            <asp:DropDownList ID="cboReferenceNumber" Width="200px" runat="server">
                            </asp:DropDownList>
                            <asp:CustomValidator ID="cus" runat="server" ValidationGroup="Document" Display="Dynamic"
                                ControlToValidate="cboReferenceNumber" ClientValidationFunction="validateReceivedFrom" ></asp:CustomValidator>
                        </div>
                         
                          
                    </div>
                    <div style ="clear:both">
                        <div class="Left" >
                            <asp:Label ID="lblReceivedBy" CssClass="labeltext" runat="server" meta:resourcekey="ReceivedBy">
                            </asp:Label>
                        </div>
                        <div class="Right" style="height: auto">
                            <asp:DropDownList ID="cboCustodian" Width="200px" runat="server">
                            </asp:DropDownList>
                            <asp:CustomValidator ID="cusCustodian" runat="server" ValidationGroup="Document"
                                Display="Dynamic" ControlToValidate="cboCustodian" ClientValidationFunction="validateCustodian"></asp:CustomValidator>
                        </div>
                      
                    </div>
                    <div style ="clear:both">
                        <div class="Left" >
                            <asp:Label CssClass="labeltext" ID="lblDocumentStatus" runat="server" meta:resourcekey="DocumentStatus">
                            </asp:Label>
                        </div>
                        <div class="Right" style="height: auto">
                            <asp:DropDownList ID="cboDocumentStatus" Width="200px" runat="server">
                            </asp:DropDownList>
                            <asp:CustomValidator ID="cusDocumentStatus" runat="server" ValidationGroup="Document"
                                Display="Dynamic" ControlToValidate="cboDocumentStatus" ClientValidationFunction="validateDocumentStatus"></asp:CustomValidator>
                        </div>
                         
                           
                    </div>
                    <div style ="clear:both">
                        <div class="Left">
                            <asp:Label ID="BinNumber" CssClass="labeltext" runat="server" meta:resourcekey="BinNumber">
                            </asp:Label>
                        </div>
                        <div class="Right">
                            <asp:DropDownList ID="cboBinNumber" Width="200px" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cboBinNumber"
                                ErrorMessage="*" InitialValue="-1" ValidationGroup="Document"></asp:RequiredFieldValidator>

                        </div>
                    </div>
                   <div style="clear: both; ">
                      <div class="Left" >
                            <asp:Label ID="lblTransactionDate" CssClass="labeltext" runat="server" meta:resourcekey="ReceiptDate" />
                        </div>
                        <div class="Right" >
                            <asp:TextBox Width="100px" onkeypress ="return false;" ID="txtTransactionDate" runat="server"></asp:TextBox>
                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" Format="dd/MM/yyyy"
                                TargetControlID="txtTransactionDate" runat="server">
                            </AjaxControlToolkit:CalendarExtender>
                        </div> 
                    </div>
                    <div>
                        
            
                        <div class="Left" style="width: 150px">
                            <asp:Label ID="lblExpRetDate" CssClass="labeltext" runat="server" Text="Expected Return Date"></asp:Label>
                        </div>
                        <div class="Right" style="width: 70px">
                            <asp:TextBox Width="100px" onkeypress ="return false;" ID="txtExpectedReturnDate"  runat="server"></asp:TextBox>
                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" Format="dd/MM/yyyy"
                                TargetControlID="txtExpectedReturnDate" runat="server">
                            </AjaxControlToolkit:CalendarExtender>
                        </div>
                        
                        <div style ="clear:both ">
                        
                                  <asp:CustomValidator ID="cvDocumentDates" runat="server" ControlToValidate="txtTransactionDate"
                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Document" ClientValidationFunction="validateDocumentIssueReceipt"
                                    Display="Dynamic"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="clear: both; margin-left: 6px;font-weight :bold">
                        <h5>
                              <asp:Literal ID="Literal21" runat ="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal> </h5>
                    </div>
                    <div style="height: auto; overflow: auto">
                        <div class="Left" style="width: 40px">
                        </div>
                        <div class="Right" style="width: 300px; height: 70px; margin-left: 30px">
                            <asp:TextBox Width="300px" Height="50px" ID="txtRemarks" runat="server" TextMode="MultiLine" MaxLength="1000"></asp:TextBox>
                        </div>
                    </div>
                    <asp:HiddenField ID="hfDocumentType" runat="server" />
                    <asp:HiddenField ID="hfCurrentDate" runat="server" />
                    <asp:HiddenField ID="hfOperationType" runat="server" />
                    <asp:HiddenField ID="hfDocumentID" runat="server" />
                    <asp:HiddenField ID="hfOrderNo" runat="server" />
                    <asp:HiddenField ID="hfIsReceipt" runat="server" />
                    <asp:HiddenField ID="hfExpiryDate" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both" id="footerwrapper">
        <div id="footer11">
            <div id="buttons">
                <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="float: left; width: 35%;">
                            <asp:Label ID="lblWarning" runat="server"></asp:Label>
                        </div>
                        <div style="float: right;" align="left">
                            <asp:Button ID="btnSave" runat="server" ValidationGroup="Document" CssClass="popupbutton"
                                OnClick="btnOK_Click" Text='<%$Resources:ControlsCommon,Submit%>'  Width="60" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Width="60" OnClick="btnCancel_Click"
                               Text='<%$Resources:ControlsCommon,Cancel%>'  CausesValidation="False" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
