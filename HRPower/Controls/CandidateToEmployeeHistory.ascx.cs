﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_CandidateToEmployeeHistory : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
    }


    public void GetHistory(int? EmployeeID, int? CandidateID)
    {

        ViewState["CandidateID"] = CandidateID;// CandidateID == null ? clsRptCandidates.GetCandidateID(EmployeeID.ToInt32()) : CandidateID;
        ViewState["EmployeeID"] = EmployeeID;
        FillBasicDetails();
        FillInterviewDetails();
        FillCandidateOfferDetails();
    }


    private void FillBasicDetails()
    {
        try
        {

            int? EmployeeID = 0;
            int? CandidateID = 0;

            if(ViewState["EmployeeID"] == null)
                   EmployeeID = null;
            else
                    EmployeeID = ViewState["EmployeeID"].ToInt32();

            if (ViewState["CandidateID"] == null)
                CandidateID = null;
            else
                CandidateID = ViewState["CandidateID"].ToInt32();

            DataTable dt = clsRptCandidates.GetCandidateBasicDetails(EmployeeID,CandidateID);
            if (dt.Rows.Count > 0)
            {
                lblJobTitleR.Text = dt.Rows[0]["JobTitle"].ToString();
                lblDepartmentR.Text = dt.Rows[0]["Department"].ToString();
                lblDesignationR.Text = dt.Rows[0]["Designation"].ToString();
                lblEmployeeJoiningDateR.Text = dt.Rows[0]["DateOfJoin"].ToString();
                lblFirstNameR.Text = dt.Rows[0]["EmployeeName"].ToString();
            }
        }
        catch (Exception ex)
        {
        }
    }


    private void FillInterviewDetails()
    {
        try
        {
            int CandidateID = 0;
            if (ViewState["CandidateID"] == null)
                CandidateID = clsRptCandidates.GetCandidateID(ViewState["EmployeeID"].ToInt32());
            else
                CandidateID = ViewState["CandidateID"].ToInt32();


            //if(CandidateID)
            //        lblJobTitleL.Text = lblJobTitleR.Text =lblJobTitleColon.Text =   "";
            //else


            if (CandidateID > 0)
            {
                dvJobTitle.Visible = true;
            }
            else
                dvJobTitle.Visible = false;


            DataTable dt = clsRptCandidates.GetInterviewScheduleDetails(CandidateID);
            dlInterviewDetails.DataSource = dt;
            dlInterviewDetails.DataBind();
            dvNoScheduleDetails.Visible = dt.Rows.Count == 0;
            dvEvaluation.Visible = dt.Rows.Count > 0; 
        }
        catch (Exception ex)
        {
        }
    }


    private void FillCandidateOfferDetails()
    {
        try
        {
            int CandidateID = 0;
            if (ViewState["CandidateID"] == null)
                CandidateID = clsRptCandidates.GetCandidateID(ViewState["EmployeeID"].ToInt32());
            else
                CandidateID = ViewState["CandidateID"].ToInt32();


            DataTable dt = clsRptCandidates.GetCandidateOfferDetails(CandidateID);
            dlOfferLetter.DataSource = dt;
            dlOfferLetter.DataBind();

            if (dt.Rows.Count > 0)
            {
                lblOfferSentDate.Text = dt.Rows[0]["OfferSendDate"].ToString();
            }
            dvOfferLetterDetails.Visible = dt.Rows.Count > 0;




            if (ViewState["EmployeeID"] != null)
            {

                DataTable dt1 = clsRptSalaryStructure.GetSalaryStructureHistorySummary(1, 0, -1, -1, -1, -1, true, ViewState["EmployeeID"].ToInt32(), true);
                dlSalaryHistory.DataSource = dt1;
                dlSalaryHistory.DataBind();
                
                dvSalaryHistoryDetails.Visible = dt1.Rows.Count > 0;

                DateTime LastDate = DateTime.Now;

                foreach (DataListItem dl in dlSalaryHistory.Items)
                {
                    Label lblHistoryDate = (Label)dl.FindControl("lblHistoryDate");
                    HiddenField hfDate = (HiddenField)dl.FindControl("hfDate");


                    if (lblHistoryDate != null && hfDate != null)
                    {

                        if (hfDate.Value.ToDateTime() == LastDate.ToDateTime())
                        {
                            lblHistoryDate.Text = "";
                        }
                        LastDate = hfDate.Value.ToDateTime();

                    }

                }
            }
            else
            {
                dvSalaryHistoryDetails.Visible = false;
            }



        }
        catch (Exception ex)
        {
        }
    }


    protected void imgPrint_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + dvDetails.ClientID + "')", true);


    }
    protected void dlInterviewDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataGrid dgvInnerDetails =(DataGrid) e.Item.FindControl("dgvInnerEvaluationDetails");
        HiddenField hfJobScheduleID = (HiddenField)e.Item.FindControl("hfJobScheduleID");

        int CandidateID = 0;

        if (ViewState["CandidateID"] == null)
            CandidateID = clsRptCandidates.GetCandidateID(ViewState["EmployeeID"].ToInt32());
        else
            CandidateID = ViewState["CandidateID"].ToInt32();


        if (dgvInnerDetails != null && hfJobScheduleID != null)
        {


            dgvInnerDetails.DataSource = clsRptCandidates.GetEvaluationReport(CandidateID, hfJobScheduleID.Value.ToInt32());
            dgvInnerDetails.DataBind();
        }
    }
    protected void dlSalaryHistory_ItemCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void dlSalaryHistory_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblParicular =(Label) e.Item.FindControl("lblParticular");
            Label lblAmount = (Label)e.Item.FindControl("lblAmount");

            if (lblParicular != null && lblAmount != null)
            {
                if (lblParicular.Text.StartsWith("Gross Salary"))
                {
                    lblParicular.Font.Bold = true;
                    lblAmount.Font.Bold = true;
                }
            }


        }
    }
}
