﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;


public partial class controls_AdditionDeduction : System.Web.UI.UserControl
{
    #region Declarations

    clsAdditionDeduction objAdditionDeduction;
    string sModalPopupID;
    private bool MblnUpdatePermission = true;  // Edit Button Permission In Datalist
    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    public delegate void OnUpdate();
    public event OnUpdate Update;

    #endregion Declarations

    #region Events

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = headername.ClientID;
        //BindDataList();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        lblWarning.Visible = false;
        btnDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل ترغب في حذف هذه المعلومات؟');") : ("return confirm('Do you wish to delete this information?');"); //"return confirm('Are you sure to delete this Job?');";
    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {       
        hfMode.Value = "Insert";
        Clear();
        BindDataList();
    }

    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        if (Save())
        {
            Clear();
            BindDataList();
        }
    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {
        objAdditionDeduction = new clsAdditionDeduction();

        objAdditionDeduction.AddDedID = Convert.ToInt32(ViewState["AddDedID"]);
        if (objAdditionDeduction.CheckDeletionUpdation() || objAdditionDeduction.CheckParticularExists())
        {
            lblWarning.Visible = true;
            lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذفها.") : ("Cannot delete."); //"Cannot delete.";
        }
        else
        {
            if (!objAdditionDeduction.Delete())
            {
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذفها.") : ("Cannot delete.");
                return;
            }
            
            lblWarning.Visible = true;
            lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب حذف بنجاح.") : ("Deleted Successfully."); 
            txtDescription.Text = string.Empty;
            txtDescriptionArb.Text = string.Empty;
            btnOK.Enabled = btnSave.Enabled = btnSaveData.Enabled = txtDescription.Enabled = true;

            rblAdditionDeduction.SelectedIndex = 0;
            rblAdditionDeduction.Enabled = true;

            btnDelete.Enabled = false;

            hfMode.Value = "Insert";
           BindDataList();                   
        }
    }

    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {
        Clear();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {       
        if (Save())
        {
            BindDataList();
            txtDescription.Text = string.Empty;
            txtDescriptionArb.Text = string.Empty;
            btnOK.Enabled = btnSave.Enabled = btnSaveData.Enabled = txtDescription.Enabled = true;
            
            rblAdditionDeduction.SelectedIndex = 0;
            rblAdditionDeduction.Enabled = true;
            
            btnDelete.Enabled = false;

            hfMode.Value = "Insert";
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Save())
        {
            BindDataList();

            if (Update != null)
                Update();
            Clear();
            mpe.Hide();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        Clear();
        mpe.Hide();
    }

    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        if (Update != null)
        {
            Update();
        }
        Clear();
        mpe.Hide();
    }
        
    protected void dlAdditionDeduction_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {                       
            objAdditionDeduction = new clsAdditionDeduction();
            lblWarning.Visible = false;
            lblWarning.Text = string.Empty;
            btnDelete.Enabled = true;

            objAdditionDeduction.AddDedID = Convert.ToInt32(dlAdditionDeduction.DataKeys[dlAdditionDeduction.SelectedIndex]);
            ViewState["AddDedID"] = Convert.ToInt32(dlAdditionDeduction.DataKeys[dlAdditionDeduction.SelectedIndex]);

            dlAdditionDeduction.SelectedItem.CssClass = "selected_item";
            hfMode.Value = "Edit";

            DataTable dtAdditionDeduction = objAdditionDeduction.BindSelectedAdditionDeduction();

            txtDescription.Text = dtAdditionDeduction.Rows[0]["Description"].ToString();
            txtDescriptionArb.Text = dtAdditionDeduction.Rows[0]["DescriptionArb"].ToString();
            string strValue = "1";
            if (dtAdditionDeduction.Rows[0]["Addition"].ToBoolean() == false)
                strValue = "0";

            rblAdditionDeduction.SelectedIndex = rblAdditionDeduction.Items.IndexOf(rblAdditionDeduction.Items.FindByValue(strValue));
            btnDelete.Enabled = txtDescription.Enabled = rblAdditionDeduction.Enabled = btnSaveData.Enabled = btnSave.Enabled = btnOK.Enabled = true;

            if (objAdditionDeduction.CheckDeletionUpdation())
            {
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("آسف، غير قادر على تحرير / حذف.") : ("Sorry, Unable to edit/delete."); //"Sorry, Unable to edit/delete.";
                btnDelete.Enabled = txtDescription.Enabled = rblAdditionDeduction.Enabled = btnSaveData.Enabled = btnSave.Enabled = btnOK.Enabled = false;
            }
            else
            {

            }
        }
        catch (Exception ex)
        { }

    }

    #endregion Events

    #region Methods

    private bool Save()
    {
        objAdditionDeduction = new clsAdditionDeduction();

        objAdditionDeduction.Description = txtDescription.Text;
        objAdditionDeduction.DescriptionArb = txtDescriptionArb.Text;
        objAdditionDeduction.Addition = Convert.ToInt32(rblAdditionDeduction.SelectedValue);        

        if (hfMode.Value == "Insert")
        {
            if (objAdditionDeduction.CheckAdditionDeductionExists())
            {
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إدراج. لا يسمح قيم مكررة.") : ("Cannot insert.Duplicate values are not permitted."); //"Cannot insert.Duplicate values are not permitted.";
                return false;
            }
            else
            {
                int id = objAdditionDeduction.InsertAdditionDeductionDetails();
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حفظ بنجاح.") : ("Successfully Saved");
                return true;
            }
        }
        else
        {
            objAdditionDeduction.AddDedID = Convert.ToInt32(ViewState["AddDedID"]);

            if (objAdditionDeduction.CheckUpdation())
            {
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحديث. لا يسمح قيم مكررة.") : ("Cannot update.Duplicate values are not permitted."); //"Cannot update.Duplicate values are not permitted.";
                return false;
            }
            else
            {
                lblWarning.Visible = true;
                lblWarning.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث بنجاح.") : ("Successfully Updated");
                int id = objAdditionDeduction.UpdateAdditionDeductionDetails();
                return true;
            }
        }
    }

    public void BindDataList()
    {
        objAdditionDeduction = new clsAdditionDeduction();

        DataSet dsAdditionDeduction = objAdditionDeduction.BindAdditionDeductions();
        if (dsAdditionDeduction.Tables[0].Rows.Count > 0)
        {
            dlAdditionDeduction.DataSource = dsAdditionDeduction;
            dlAdditionDeduction.DataBind();
        }
        else
        {
            dlAdditionDeduction.DataSource = null;
            dlAdditionDeduction.DataBind();
        }

        ViewState["AddDedID"] = null;

        string guid = clsCommon.GetValidationGroup();
        clsCommon.WriteLog(guid);

        if (clsGlobalization.IsArabicCulture())
        {
            rfvDescriptionArb.ValidationGroup = rfvDescription.ValidationGroup = vsAdditionDeduction.ValidationGroup = btnOK.ValidationGroup = guid;
       
            btnOK.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
            btnSaveData.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
            btnSave.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        }
        else
        {
            rfvDescription.ValidationGroup = vsAdditionDeduction.ValidationGroup = btnOK.ValidationGroup = guid;
            rfvDescriptionArb.ValidationGroup = "";
            btnOK.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
            btnSaveData.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
            btnSave.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        }

        DataSet dsDeductionPolicy = objAdditionDeduction.FillDeductionPolicy();

        btnDelete.Enabled = false;
    }

    public string GetAdditionDeduction(object oAddition)
    {
        string sAddition = string.Empty;

        if (Convert.ToInt32(oAddition) == 1)
        {
            sAddition = clsUserMaster.GetCulture() == "ar-AE" ? ("إضافة") : ("Addition"); //"Addition";
        }
        else
        {
            sAddition = clsUserMaster.GetCulture() == "ar-AE" ? ("حسم") : ("Deduction"); //"Deduction";
        }
        return sAddition;        
    }

    private void Clear()
    {
        txtDescription.Text = string.Empty;
        txtDescriptionArb.Text = string.Empty;
        btnOK.Enabled = btnSave.Enabled = btnSaveData.Enabled = txtDescription.Enabled = true;

        rblAdditionDeduction.SelectedIndex = 0;
        rblAdditionDeduction.Enabled = true;
        lblWarning.Visible = false;
        lblWarning.Text = string.Empty;
        btnDelete.Enabled = false;

        hfMode.Value = "Insert";
    }

    #endregion Methods
}
