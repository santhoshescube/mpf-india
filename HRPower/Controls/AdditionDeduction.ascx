﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdditionDeduction.ascx.cs"
    Inherits="controls_AdditionDeduction" %>
<%--<%@ Register Src="~/controls/ReferenceControl.ascx" TagPrefix="uc1" TagName="ReferenceControl" %>--%>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
    type="text/css" disabled="disabled" />--%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<div id="popupmainwrap">
    <div id="popupmain" style="width:500px !important;">
        <div id="header11">
            <div id="hbox1">
                <div id="headername" runat="server" style="color: White; font-size: medium;">
                    <%--Addition / Deduction--%>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="AdditionDeduction"></asp:Literal>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:ImageButton ID="btnClose" runat="server" meta:resourcekey="Close" OnClick="btnClose_Click"
                                CausesValidation="False" ImageUrl="images/icon-close.png" />
                            <%--ToolTip="Close"--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="toolbar">
            <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" >
                <ContentTemplate>
                    <div style="float: left; width: 100%;">
                        <div id="imgbuttonss">
                            <asp:ImageButton ID="btnAddNew" CssClass="imgbuttons" runat="server" meta:resourcekey="AddNew"
                                CausesValidation="False" OnClick="btnAddNew_Click" ImageUrl="~/images/Plus.png" /><%-- ToolTip="Add New"--%>
                            <asp:ImageButton ID="btnSaveData" CssClass="imgbuttons" runat="server" meta:resourcekey="SaveData"
                                OnClick="btnSaveData_Click"  ImageUrl="~/images/Save%20Blue.png" />
                            <%--ToolTip="Save Data"--%>
                            <asp:ImageButton ID="btnDelete" CssClass="imgbuttons" runat="server" CausesValidation="False"
                                meta:resourcekey="Delete" OnClick="btnDelete_Click" 
                                ImageUrl="~/images/delete_icon_popup.png" />
                            <%--ToolTip="Delete" --%>
                            <asp:ImageButton ID="btnClear" CssClass="imgbuttons" runat="server" CausesValidation="False"
                                meta:resourcekey="Clear" OnClick="btnClear_Click" ImageUrl="~/images/clear.png" />
                            <%--ToolTip="Clear"--%>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlAdditionDeduction" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div id="contentwrap">
            <div id="content" style="width: 496px; height: 270px;">
                <div style="width: 100%; height: 300px;">
                    <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="width: 98%">
                                <asp:UpdatePanel ID="upnlAdditionDeduction" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 50%;">
                                            <div style="float: left; width: 30%; height: 29px; padding-top: 10px" class="labeltext">
                                                <%--Description--%>
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 50%; padding-top: 10px" align="left">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox_mandatory" Width="150px" MaxLength="50"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 48%;">
                                            <div style="float: left; width: 30%; height: 29px; padding-top: 10px" class="labeltext">
                                                <%--Description--%>
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="DescriptionArb"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 50%; padding-top: 10px" align="left">
                                                <asp:TextBox ID="txtDescriptionArb" runat="server" CssClass="textbox_mandatory"  MaxLength="50" Width="150px"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%;">
                                            <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                        </div>
                                        <div style="float: left; width: 90%;">
                                            <div style="float: left; width: 60%;padding-left:50px">
                                                <asp:RadioButtonList ID="rblAdditionDeduction" CellPadding="0" CellSpacing="2" runat="server"
                                                    RepeatDirection="Horizontal" AutoPostBack="True" CssClass="labeltext">
                                                    <asp:ListItem meta:resourcekey="Addition" style="font-size: 13px;" Value="1" Selected="True"></asp:ListItem>
                                                    <%--Text="Addition" --%>
                                                    <asp:ListItem meta:resourcekey="Deduction" style="font-size: 13px;" Value="0"></asp:ListItem>
                                                    <%--Text="Deduction"--%>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div style="float: right; width: 27%;" align="left">
                                                <asp:Button ID="btnSave" runat="server" CssClass="popupbutton" Text='<%$Resources:ControlsCommon,Save%>'
                                                    OnClick="btnSave_Click" />
                                                <%--Text="Save"--%>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="dlAdditionDeduction" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div style="float: left; width: 98%;">
                                <fieldset style="height: 180px">
                                    <legend style="font-size: 13px;">
                                        <%--Addition & Deduction Details--%>
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="AdditionDeductionDetails"></asp:Literal>
                                    </legend>
                                    <div style="width: 100%; height: 150px; float: left;">
                                        <div style="margin: 5px 0 0 0; width: 100%">
                                            <div style="font-size: 11px; width: 99%; float: left;" class="datalistheader">
                                                <div style="float: left; width: 31%; height: 13px;">
                                                    <%--Description--%>
                                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Description"></asp:Literal>
                                                </div>
                                                <div style="float: left; width: 23%;">
                                                    <%--Description--%>
                                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="DescriptionArb"></asp:Literal>
                                                </div>
                                                <div style="float: left; width: 36%; height: 14px;">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%--Addition/Deduction--%>
                                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="AdditionDeduction"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="float: left; width: 99%; height: 120px; overflow: auto">
                                                    <asp:DataList ID="dlAdditionDeduction" runat="server" DataKeyField="AddDedID" OnSelectedIndexChanged="dlAdditionDeduction_SelectedIndexChanged"
                                                        Width=" 100%" ><%--OnItemCommand="dlAdditionDeduction_ItemCommand"--%>
                                                        <%--<HeaderTemplate>
                                                        <div class="container_head" style=" width: 100%">
                                                            <div style="font-size: 11px; width: 100%;">
                                                                <div class="container_head" style="float: left; width: 50%;">
                                                                    Description
                                                                </div>
                                                                <div class="container_head" style="float: left; width: 48%;">
                                                                    Addition/Deduction
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <itemstyle cssclass="item" />
                                                            <div style="width: 100%; height: auto; padding-left: 10px;">
                                                                <div id="trDescription" style="width: 100%;" runat="server">
                                                                    <div style="float: left; width: 300px;font-size:13px;">
                                                                        <div style="float: left; width: 150px;font-size:13px;">
                                                                            <asp:LinkButton ID="lnkDescription" runat="server" Text='<%# Eval("Description") %>'
                                                                                CommandArgument='<%# Eval("AddDedID") %>' CommandName="SELECT" CssClass="linkbutton"
                                                                                CausesValidation="false"></asp:LinkButton>
                                                                        </div>
                                                                        <div style="float: left; width: 150px;">
                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("DescriptionArb") %>'
                                                                                CommandArgument='<%# Eval("AddDedID") %>' CommandName="SELECT" CssClass="linkbutton"
                                                                                CausesValidation="false"></asp:LinkButton>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float: left; width: 70px; margin-left: 2px;font-size:13px;" align="center">
                                                                        <%# GetAdditionDeduction(Eval("Addition"))%>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </fieldset>
                            </div>
                            <asp:RequiredFieldValidator ID="rfvDescription" runat="server" meta:resourcekey="Pleaseenterdescription"
                                ControlToValidate="txtDescription" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter description"--%>
                            <asp:RequiredFieldValidator ID="rfvDescriptionArb" runat="server" meta:resourcekey="PleaseenterdescriptionArb"
                                ControlToValidate="txtDescriptionArb" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                            <asp:ValidationSummary ID="vsAdditionDeduction" runat="server" ShowMessageBox="True"
                                ShowSummary="False" CssClass="validation_summary" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                            <%-- <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />--%>
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11">
                <div id="buttons">
                    <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="float: left; width: 35%;">
                                <asp:Label ID="lblWarning" Text="" Width="250px" CssClass="error" runat="server"
                                    Visible="true"></asp:Label>
                            </div>
                            <div style="float: right;" align="left">
                                <asp:Button ID="btnOK" runat="server" CssClass="popupbutton" OnClick="btnOK_Click"
                                    Text='<%$Resources:ControlsCommon,Ok%>' Width="60" />&nbsp;
                                <%-- Text="Ok" --%>
                                <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Width="60" OnClick="btnCancel_Click"
                                    Text='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="False" />
                                <%--Text="Cancel" --%>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="dlAdditionDeduction" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</div>
