﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PayStructure.ascx.cs" Inherits="Controls_PayStructure" %>
<%@ Register Src="~/controls/FormViewPager.ascx" TagPrefix="cc" TagName="FormViewPager" %>

<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled"/>
<link href="../css/popup.css" rel="stylesheet" type="text/css"/>
<script src="../js/HRPower.js" type="text/javascript"></script>
<script type="text/javascript">
    function closThis(elementID) 
    {
        var e = document.getElementById(elementID);

        if (e) 
        {
            e.style.display = 'none';
            return false;
        }
    }
</script>

<div id="popupmainwrap">
    <div id="popupmain" style="width:500px">
        <div id="header11">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" Text="HR Power"></asp:Label>
                </div>
            </div>            
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        
        <div style="float:left; width:100%; padding-top: 8px; padding-bottom: 8px;">
            <div style="float:left; width:40%; padding-left:15px;">
                <asp:TextBox ID="txtPayStructureSearch" runat="server" CssClass="textbox" style="width: 95%;"></asp:TextBox> 
            </div>
            <div style="float:left; width:55%;">
                <div id="imgbuttons">
                    <asp:ImageButton Style="padding-left: 10px; height: 24px; padding-right: 140px;" ID="btnSearch" runat="server" ToolTip="Add New"
                        CausesValidation="False" OnClick="btnAddNew_Click" ImageUrl="~/images/search.png" />
                    <asp:ImageButton Style="padding-left: 10px;" ID="btnAddNew" runat="server" ToolTip="Add New"
                        CausesValidation="False" OnClick="btnAddNew_Click" ImageUrl="~/images/Plus.png" />
                    <asp:ImageButton Style="padding-left: 10px;" ID="btnSaveData" runat="server" ToolTip="Save Data"
                        OnClick="btnSaveData_Click" ImageUrl="~/images/Save%20Blue.png" />
                    <asp:ImageButton Style="padding-left: 10px;" ID="btnDelete" runat="server" CausesValidation="False"
                        ToolTip="Delete" OnClientClick="return confirm('Do you wish to delete this information?');"
                        OnClick="btnDelete_Click" ImageUrl="~/images/delete_icon_popup.png" />
                </div>
            </div>
        </div>
        
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset>
                        <div id="content1">
                            <div style="float:left; width:100%; height:30px;">
                                <div style="float:left; width:45%;">
                                    Pay Structure Name
                                </div>
                                <div style="float:left; width:55%;">
                                    <asp:TextBox ID="txtPaystructure" runat="server" Width="97%" MaxLength="75">
                                    </asp:TextBox>
                                </div>
                            </div>
                            
                            <div style="float:left; width:100%; height:25px;">
                                <div style="float:left; width:45%;">
                                    Designation
                                </div>
                                <div style="float:left; width:55%;">
                                    <asp:DropDownList ID="ddlDesignation" runat="server" BorderStyle="Solid" BorderWidth="1px" Width="99%" 
                                        DataTextField="Designation" DataValueField="DesignationID">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div style="float:left; width:100%; height:25px;">
                                <div style="float:left; width:45%;">
                                    Payment Calculation Mode
                                </div>
                                <div style="float:left; width:55%;">
                                    <asp:DropDownList ID="ddlPaymentMode" runat="server" DataTextField="PaymentClassification"
                                        Enabled="false" DataValueField="PaymentClassificationID" Width="99%" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <div id="divpTab1" style="width: 100%; height: 150px; float: left;">
                                <div style="width: 100%; height:100%; float: left">
                                    <div style="width: 100%; height:30px; float: left;  background-color:#2AB0D5;">
                                        <div style="width: 44%; float: left" class="labeltext">
                                            Particulars
                                        </div>
                                        <div style="width: 40%; float: left" class="labeltext">
                                            Amount
                                        </div>
                                        <div style="width: 6%; float: left" class="labeltext">
                                            &nbsp
                                        </div>
                                        <div style="width: 6%; float: left; padding-top:5px;">
                                            <asp:Button ID="btnAllowances" runat="server" CssClass="referencebutton" Text="..."
                                                ToolTip="Add more particulars" OnClick="btnAllowances_Click" />
                                        </div>
                                    </div>
                                    
                                    <div style="width:100%; height: 115px; float: left; padding-top:5px; overflow: auto; background-color: White">
                                        <asp:UpdatePanel ID="updlDesignationAllowance" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DataList ID="dlDesignationAllowances" runat="server" Width="96%" BackColor="White"
                                                    DataKeyField="JobSalaryID" CssClass="labeltext"
                                                    OnItemCommand="dlDesignationAllowances_ItemCommand"
                                                    OnItemDataBound="dlDesignationAllowances_ItemDataBound">
                                                    <ItemTemplate>
                                                        <div style="width: 100%; float: left;">
                                                            <div style="width: 44%; float: left" class="labeltext">
                                                                <asp:DropDownList ID="ddlParticulars" runat="server" DataTextField="Description"
                                                                    AutoPostBack="true" CssClass="dropdownlist" Width="100%" height="25px" 
                                                                    DataValueField="AddDedID">
                                                                    <%--OnSelectedIndexChanged="ddlParticulars_SelectedIndexChanged" 
                                                                    onchange="chkDuplicateParticulars(this)">--%>
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdParticulars" runat="server" Value='<%# Eval("AdditionDeductionID") %>' />
                                                            </div>
                                                            <div style="width: 45%; float: left; padding-left:10px;" class="labeltext">
                                                                <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                    MaxLength="12" Width="95%" Text='<% # Eval("Amount") %>'></asp:TextBox> 
                                                                    <%--onblur="CalculateSalaryStructure(this.id)" onchange="setVisibility(this);"></asp:TextBox>--%>
                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterMode="ValidChars"
                                                                    FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                <asp:HiddenField ID="hdtxtAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                            </div>
                                                            <div style="width: 3%; float: left; padding-top: 8px;" class="labeltext">
                                                                <asp:ImageButton ID="btnRemove" runat="server" ToolTip="Delete Particular" SkinID="DeleteIconDatalist"
                                                                    CommandArgument='<% # Eval("AdditionDeductionID") %>' CommandName="REMOVE_ALLOWANCE" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                            </div>
                                                        </div>
                                                        
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:CustomValidator ID="cvAllowances" runat="server" ClientValidationFunction="validateAllowanceDeduction"
                                                    ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please select particulars."></asp:CustomValidator>
                                                <asp:CustomValidator ID="cvAmount" runat="server" ClientValidationFunction="validateAmount"
                                                    ValidationGroup="Employee" Display="Dynamic" ErrorMessage="Please enter the amount"></asp:CustomValidator>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            
                            <div style="float:left; width:100%; height:25px; padding-top:5px;">
                                Gross Salary : &nbsp
                                <asp:Label ID="lblAddition" runat="server"></asp:Label>
                            </div>                            
                            
                            <div style="float:left; width:100%; height:30px;">
                                <div style="float:left; width:83%;">
                                    &nbsp
                                </div>
                                <div style="float:left; width:16%;">
                                    <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text="Submit" ValidationGroup="submit"
                                        Width="75px" ToolTip="Submit" OnClick="btnSubmit_Click" />
                                    <asp:HiddenField ID="hfMode" runat="server" />
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                
                
                
                <div id="footerwrapper" >
                    <div id="footer11" style ="height:30px;" >
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="False"></asp:Label>
                    </div>
                </div>
                
                <div style="display: none">
                    <asp:HiddenField ID="hfId" runat="server" />
                    <asp:HiddenField ID="hfDataTextField" runat="server" />
                    <asp:HiddenField ID="hfDataValueField" runat="server" />
                    <asp:HiddenField ID="hfDataTextValue" runat="server" />
                    <asp:HiddenField ID="hfDataNoValue" runat="server" />
                    <asp:HiddenField ID="hfPredefinedField" runat="server" />
                    <asp:HiddenField ID="hfTableName" runat="server" />
                    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
                    <asp:HiddenField ID="hfFunctionName" runat="server" />
                    <asp:HiddenField ID="hfReferenceField" runat="server" />
                    <asp:HiddenField ID="hfReferenceId" runat="server" />
                    <asp:HiddenField ID="hfDisplayName" runat="server" />
                    <asp:HiddenField ID="hfPreviousID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfVaidationGroup" runat="server" Value="" />
                    <asp:HiddenField ID="hfParentElementId" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeFiled" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeValues" runat="server" Value="" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>