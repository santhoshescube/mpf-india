﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CriteriaReference.ascx.cs"
    Inherits="Controls_CriteriaReference" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/default_common.css" rel="stylesheet" type="text/css" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<script type="text/javascript">

    function closThis(elementID) {
        var e = document.getElementById(elementID);

        if (e) {
            e.style.display = 'none';
            return false;
        }
    }

</script>

<style type="text/css">
    .style1
    {
        width: 484px;
    }
    .style2
    {
        width: 113px;
    }
</style>

<div id="popupmainwrap">
    <div id="popupmain" style="width: 700px !important;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="HRPower" ></asp:Label><%--Text="HR Power"--%>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div id="toolbar">
            <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div style="float: left; width: 100%;">
                        <div id="imgbuttonss">
                            <asp:ImageButton ID="btnAddNew" CssClass="imgbuttons" runat="server" meta:resourcekey="AddNew"
                                CausesValidation="False" ImageUrl="~/images/Plus.png" OnClick="btnAddNew_Click" /><%-- ToolTip="Add New"--%>
                            <asp:ImageButton ID="btnSaveData" CssClass="imgbuttons" runat="server" meta:resourcekey="SaveData"
                                ValidationGroup="Submit" ImageUrl="~/images/Save%20Blue.png" OnClick="btnSaveData_Click" /> <%--ToolTip="Save Data"--%>
                            <asp:ImageButton ID="btnClear" CssClass="imgbuttons" runat="server" CausesValidation="False"
                              meta:resourcekey="Clear" ImageUrl="~/images/clear.png" OnClick="btnClear_Click" /> <%--ToolTip="Clear"--%>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset>
                        <table cellpadding="5" cellspacing="0" border="0" class="labeltext">
                            <tr>
                                <td class="style2">
                                <%--    Criteria--%>
                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Criteria"></asp:Literal>
                                </td>
                                <td class="style1">
                                    <asp:TextBox ID="txtCriteriaEng" runat="server" Width="200px" MaxLength="100" onchange="RestrictMulilineLength(this, 100);"
                                    onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="rfvEngCri" runat="server" 
                                      meta:resourcekey="Entercriteria"  ControlToValidate="txtCriteriaEng" CssClass="error" Display="Dynamic" 
                                         ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter criteria"--%>
                                </td>
                                <td class="style2">
                                <%--    Criteria--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="CriteriaArb"></asp:Literal>
                                </td>
                                <td class="style1">
                                    <asp:TextBox ID="txtCriteriaArb" runat="server" Width="200px" MaxLength="100" onchange="RestrictMulilineLength(this, 100);"
                                    onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="rfvArbCri" runat="server" 
                                      meta:resourcekey="EntercriteriaArb"  ControlToValidate="txtCriteriaArb" CssClass="error" Display="Dynamic" 
                                         ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter criteria"--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    <%--Description--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description"></asp:Literal>
                                </td>
                                <td class="style1">
                                    <asp:TextBox ID="txtDescEng" runat="server" Rows="5" TextMode="MultiLine" 
                                        MaxLength="250" onchange="RestrictMulilineLength(this, 250);"
                                    onkeyup="RestrictMulilineLength(this, 250);"
                                        Width="200px"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="rfvDesEng" runat="server" 
                                        ControlToValidate="txtDescEng" CssClass="error" Display="Dynamic" 
                                       meta:resourcekey="Enterdescription" ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter description"--%>
                                </td>
                                <td class="style2">
                                    <%--Description--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="DescriptionArb"></asp:Literal>
                                </td>
                                <td class="style1">
                                    <asp:TextBox ID="txtDescArb" runat="server" Rows="5" TextMode="MultiLine" 
                                        MaxLength="250" onchange="RestrictMulilineLength(this, 250);"
                                    onkeyup="RestrictMulilineLength(this, 250);"
                                        Width="200px"></asp:TextBox><br />
                                    <asp:RequiredFieldValidator ID="rfvDesArb" runat="server" 
                                        ControlToValidate="txtDescArb" CssClass="error" Display="Dynamic" 
                                       meta:resourcekey="EnterdescriptionArb" ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter description"--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                </td>
                                <td class="style1">
                                    
                                </td>
                                <td class="style1">
                                    
                                </td>
                                <td>
                                    &nbsp;<asp:Button ID="btnSave" runat="server" CssClass="popupbutton" 
                                        OnClick="btnSave_Click" meta:resourcekey="Save"  ValidationGroup="Submit" /><%--Text="Save" --%></td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <div id="content" style="max-height: 226px; width: 670px; overflow: auto;">
                            <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                Width="100%" CssClass="labeltext">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Criteria" ItemStyle-CssClass="datalistTrLeft"  
                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDesc" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField  ItemStyle-CssClass="datalistTrLeft" HeaderText="Description"
                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremarks" runat="server" Text='<%# Eval("RemarksEng") %>' style='word-break:break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="35%" 
                                            Wrap="True" />
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="Criteria1" ItemStyle-CssClass="datalistTrLeft"  
                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDescArb" runat="server" Text='<%# Eval("DescriptionArb") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="15%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField  ItemStyle-CssClass="datalistTrLeft" meta:resourcekey="Description1"
                                        ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremarksArb" runat="server" Text='<%# Eval("RemarksArb") %>' style='word-break:break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="50%" 
                                            Wrap="True" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("CriteriaId") %>'
                                                OnClientClick="return true" OnClick="ImgEdit_Click" meta:resourcekey="Edit" ImageUrl="~/images/edit_popup.png" /><%--ToolTip="edit"--%>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("CriteriaId") %>'
                                              meta:resourcekey="Delete" ImageUrl="~/images/delete_popup.png" CausesValidation="false" OnClick="ImgDelete_Click" /> <%--ToolTip="Delete" --%>
                                            <AjaxControlToolkit:ConfirmButtonExtender ID="CBE1" runat="server" TargetControlID="ImgDelete"
                                             meta:resourcekey="Areyousuretodeletethiscriteria">
                                            </AjaxControlToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div id="footerwrapper">
                    <div id="footer11" style="height: 40px;">
                        <%--       <div id="buttons">--%>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="false"></asp:Label>
                        <%--  </div>--%>
                        <div id="buttons">
                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 45%;">
                                        <asp:Label ID="lblWarning" runat="server"></asp:Label>
                                    </div>
                                    <div style="float: right;" align="left">
                                        <asp:Button ID="btnOK" ValidationGroup="Submit" runat="server" CssClass="popupbutton" OnClick="btnOK_Click"
                                           meta:resourcekey="Ok" Width="60" />&nbsp; <%--Text="Ok"--%>
                                        <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Width="60" OnClick="btnCancel_Click"
                                           meta:resourcekey="Cancel" CausesValidation="False" /> <%--Text="Cancel"--%>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ibtnClose" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
