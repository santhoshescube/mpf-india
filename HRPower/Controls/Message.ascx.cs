﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class controls_Message : System.Web.UI.UserControl
{
    string sModalPopupId;
    public string ModalPopupId
    {
        set
        {
            sModalPopupId = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public void WarningMessage(string Message)
    {
        lblMessage.ForeColor = System.Drawing.Color.Red;
        lblMessage.Text = Message;

        //string script = "$(function() {$(\"#" + dialogModal.ClientID + "\").dialog({width: 500, resizable: false,draggable:false,show: 'fade',hide: 'fade',modal: true,buttons: {Ok: function() {$(this).dialog('close');}}});});";
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), UniqueID, script, true);
    }
    public void InformationalMessage(string Message)
    {
        lblMessage.ForeColor = System.Drawing.Color.Black;
        lblMessage.Text = Message;

        //string script = "$(function() {$(\"#" + dialogModal.ClientID + "\").dialog({width: 500, resizable: false,draggable:false,show: 'fade',hide: 'fade',modal: true,buttons: {Ok: function() {$(this).dialog('close');}}});});";
        //ScriptManager.RegisterStartupScript(Page, Page.GetType(), UniqueID, script, true);
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
       
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupId);

        mpe.Hide();
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
      
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupId);

        mpe.Hide();
    }
    
}
