﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_HiringSchedule : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        
    }

    public  void LoadHiringSchedule(DataTable dt)
    {
        FillAllDesignation();
        dlHiringSchedules.DataSource = dt;
        dlHiringSchedules.DataBind();

    }

    private void FillAllDesignation()
    {
        DataTable dt = clsReportCommon.GetAllDesignation();
        dt.Rows.RemoveAt(0);


        foreach (DataListItem i in dlHiringSchedules.Items)
        {
            DropDownList ddlDesignation = (DropDownList)i.FindControl("ddlDesignation");

            if (ddlDesignation != null)
            {
                ddlDesignation.DataSource = dt;
                
                ddlDesignation.DataBind();
            }
        }

    }
    protected void dlHiringSchedules_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DataTable dt = clsReportCommon.GetAllDesignation();
        dt.Rows.RemoveAt(0);


        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            HiddenField hfDesignationID = (HiddenField) e.Item.FindControl("hfDesignationID");
            DropDownList ddlDesignation =(DropDownList) e.Item.FindControl("ddlDesignation");

            if (hfDesignationID != null && ddlDesignation != null)
            {
                ddlDesignation.DataSource = dt;
                ddlDesignation.DataTextField = "Designation";
                ddlDesignation.DataValueField = "DesignationID";
                ddlDesignation.DataBind();
                ddlDesignation.SelectedValue = hfDesignationID.Value.ToString();
            }

        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {

    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void dlHiringSchedules_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
