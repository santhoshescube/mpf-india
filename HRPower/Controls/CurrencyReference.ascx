﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrencyReference.ascx.cs"
    Inherits="controls_CurrencyReference" %>
<%@ Register Src="Report.ascx" TagName="Report" TagPrefix="uc1" %>
<%@ Register Src="~/controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 450px;">
        <div id="header11">
            <div id="hbox1">
                <div id="headername" runat="server" style="color: White;">
                    Currency
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <asp:ImageButton ID="btnClose" runat="server" ToolTip="Close" OnClick="btnClose_Click"
                        CausesValidation="False" ImageUrl="images/icon-close.png" />
                </div>
            </div>
        </div>
        <div id="toolbar">
            <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div style="float: left; width: 100%;">
                        <div style="float: left; width: 40%; margin-left: 10px;">
                            <div id="imgbuttonss">
                                <asp:ImageButton ID="btnAddNew" runat="server" ToolTip="Add New" CausesValidation="False"
                                    OnClick="btnAddNew_Click" ImageUrl="~/images/Plus.png"  CssClass="imgbuttons" />
                                <asp:ImageButton ID="btnSaveData" runat="server" ToolTip="Save Data" CssClass="imgbuttons" 
                                    OnClick="btnSaveData_Click" ValidationGroup="submit" ImageUrl="~/images/Save Blue.png" />
                                <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" CssClass="imgbuttons" ToolTip="Delete"
                                    OnClick="btnDelete_Click" OnClientClick="return confirm('Do you wish to delete this information?');"
                                     ImageUrl="~/images/delete_icon_popup.png" />
                                <asp:ImageButton ID="btnClear" runat="server" CausesValidation="False" CssClass="imgbuttons" ToolTip="Clear"
                                    OnClick="btnClear_Click"  ImageUrl="~/images/clear.png" />
                            </div>
                        </div>
                        <div style="float: right; width: 10%;" align="left">
                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upMainWindow">
                                <ProgressTemplate>
                                    <img alt="" src="../images/circle-loading.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div id="contentwrap">
            <div id="content" style="height: 330px;">
                <div style="float: left; width: 100%;">
                    <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset style="padding: 10px; height: 310px">
                                <div style="width: 100%; float: left;">
                                    <asp:UpdatePanel ID="upnlCurrencyDetail" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="width: 100%; font-size: 11px;">
                                                <div style="width: 100%;">
                                                    <div style="width: 20%; float: left;">
                                                        Currency
                                                    </div>
                                                    <div style="width: 80%; float: left;">
                                                        <asp:TextBox ID="txtCurrency" runat="server" CssClass="textbox_mandatory" Width="280px"
                                                            MaxLength="40"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="width: 100%;">
                                                    <div style="width: 20%; float: left;">
                                                        Short Name
                                                    </div>
                                                    <div style="width: 80%; float: left;">
                                                        <asp:TextBox ID="txtShortName" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                            MaxLength="5"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="width: 100%;">
                                                    <div style="width: 20%; float: left;">
                                                        Country
                                                    </div>
                                                    <div style="width: 80%; float: left;">
                                                        <%--   <uc1:ReferenceControl ID="rcCountry" runat="server" ValueType="Country" TableName="CountryReference"
                                                                        DataTextField="Description" DataValueField="CountryID"
                                                                        MessageBox="true" ShowMessageBox="True" AutoPostBack="False" 
                                                                        MaxLength="50" />--%>
                                                        <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList CssClass="dropdownlist_mandatory" Width="170px" ID="rcCountry"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnCountryRef" runat="server" Text="..." CausesValidation="False"
                                                                    CssClass="referencebutton" OnClick="btnCountryRef_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div style="width: 100%;">
                                                    <div style="width: 20%; float: left;">
                                                        Decimal Place
                                                    </div>
                                                    <div style="width: 80%; float: left;">
                                                        <div style="width: 100%;">
                                                            <div style="float: left">
                                                                <asp:TextBox ID="txtDecimal" runat="server" CssClass="textbox_mandatory" Width="50px"
                                                                    MaxLength="1"></asp:TextBox>
                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                    FilterType="Numbers" TargetControlID="txtDecimal">
                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:Button ID="btnSave" runat="server" CssClass="popupbutton" Text="Save" OnClick="btnSave_Click" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <hr />
                                </div>
                                <div style="width: 100%; float: left;" class="container">
                                    <div class="container_content" style="float: left; width: 100%;">
                                        <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:DataList ID="dlCurrency" runat="server" DataKeyField="CurrencyID" OnSelectedIndexChanged="dlCurrency_SelectedIndexChanged"
                                                    OnItemDataBound="dlCurrency_ItemDataBound" Style="width: 97%">
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; font-size: 11px;float:left;" >
                                                        <div style="width:100%; float:left;" class="datalistheader">
                                                            <div style="float: left; width: 30%;">
                                                                Currency
                                                            </div>
                                                            <div style="float: left; width: 20%;">
                                                                Short Name
                                                            </div>
                                                            <div style="float: left; width: 25%;">
                                                                Country
                                                            </div>
                                                            <div style="float: left; width: 20%">
                                                                Decimal
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div style="width: 100%; font-size: 11px; float: left;">
                                                            <div id="trCurrency" style="width: 100%; float: left;" runat="server" onclick="ChangeCurrencyIndex(this.id);">
                                                                <div style="width: 30%; float: left;">
                                                                    <asp:LinkButton ID="lnkCurrency" runat="server" Text='<%# Eval("Currency") %>' CommandArgument='<%# Eval("CurrencyID") %>'
                                                                        CommandName="SELECT" CssClass="linkbutton" CausesValidation="false"></asp:LinkButton>
                                                                </div>
                                                                <div style="width: 20%; float: left;">
                                                                    <%# Eval("Code") %>
                                                                </div>
                                                                <div style="width: 25%; float: left;">
                                                                    <%# Eval("Country") %>
                                                                </div>
                                                                <div style="width: 20%; float: left;">
                                                                    <%# Eval("Scale") %>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle CssClass="item" />
                                                </asp:DataList>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="ItemDataBound" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                </table>
                            </fieldset>
                            <asp:RequiredFieldValidator ID="rfvCurrency" runat="server" ErrorMessage="Please enter currency"
                                ControlToValidate="txtCurrency" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvShortName" runat="server" ErrorMessage="Please enter short name"
                                ControlToValidate="txtShortName" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfvDecimal" runat="server" ErrorMessage="Please enter decimal place"
                                ControlToValidate="txtDecimal" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rvDecimal" runat="server" ControlToValidate="txtDecimal"
                                ErrorMessage="Decimal point should not be greater than 3." CssClass="error" Display="None"
                                MaximumValue="3" MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                            <asp:ValidationSummary ID="vsCurrency" runat="server" ShowMessageBox="True" ShowSummary="False" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11">
                <div id="buttons">
                    <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="float: left; width: 65%;">
                                <asp:Label ID="lblWarning" runat="server"></asp:Label>
                            </div>
                            <div style="float: right;" align="left">
                                <asp:Button ID="btnOK" runat="server" CssClass="popupbutton" Text="OK" OnClick="btnOK_Click"
                                    Width="60" />&nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Text="Cancel" Width="60"
                                    OnClick="btnCancel_Click" CausesValidation="False" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div>
        <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="display: none">
                    <asp:Button ID="Button1" runat="server" />
                </div>
                <asp:Panel ID="pnlModalPopUp" Style="display: none" runat="server">
                    <uc1:ReferenceControlNew ID="ReferenceControlNew" runat="server" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                </AjaxControlToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<%---------------------------------------------------------------------------------------------------------------------------------%>
<%--<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CurrencyReference.ascx.cs"
    Inherits="controls_CurrencyReference" %>
<%@ Register Src="Report.ascx" TagName="Report" TagPrefix="uc1" %>
<%@ Register Src="~/controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>
<table cellpadding="0" cellspacing="0" width="490">
    <tr>
        <td height="5" width="20" class="tdTopLeft">
            &nbsp;
        </td>
        <td class="tdTopCenter">
            <table cellpadding="0" cellspacing="0" width='100%'>
                <tr>
                    <td class="header_text popup_header_padding" id="tdDrag" runat="server">
                        Currency
                    </td>
                    <td align="right" width="20" class="popup_close">
                        <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:ImageButton ID="btnClose" runat="server" ToolTip="Close" OnClick="btnClose_Click"
                                    CausesValidation="False" SkinID="CloseIcon" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
        <td width="20" class="tdTopRight">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="tdMiddleLeft">
            &nbsp;
        </td>
        <td>
            <table width='100%' style="font-family: Verdana; font-size: 11px" bgcolor="white">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%" style="font-size: 11px">
                            <tr>
                                <td class="popupWindow_headerbg">
                                    <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:ImageButton ID="btnAddNew" runat="server" ToolTip="Add New" CausesValidation="False"
                                                OnClick="btnAddNew_Click" SkinID="NewIcon" />
                                            <asp:ImageButton ID="btnSaveData" runat="server" ToolTip="Save Data" OnClick="btnSaveData_Click"
                                                SkinID="PopupSave" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" ToolTip="Delete"
                                                OnClientClick="return confirm('Do you wish to delete this information?');" OnClick="btnDelete_Click"
                                                SkinID="DeleteIcon" />
                                            <asp:ImageButton ID="btnClear" runat="server" CausesValidation="False" ToolTip="Clear"
                                                OnClick="btnClear_Click" SkinID="ClearIcon" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                        <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <fieldset style="padding: 10px; height: 350px">
                                    <div style="width: 100%; float: left;">
                                        <asp:UpdatePanel ID="upnlCurrencyDetail" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="width: 100%; font-size: 11px;">
                                                    <div style="width: 100%;">
                                                        <div style="width: 20%; float: left;">
                                                            Currency
                                                        </div>
                                                        <div style="width: 80%; float: left;">
                                                            <asp:TextBox ID="txtCurrency" runat="server" CssClass="textbox_mandatory" Width="280px"
                                                                MaxLength="40"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div style="width: 20%; float: left;">
                                                            Short Name
                                                        </div>
                                                        <div style="width: 80%; float: left;">
                                                            <asp:TextBox ID="txtShortName" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                                MaxLength="5"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div style="width: 20%; float: left;">
                                                            Country
                                                        </div>
                                                        <div style="width: 80%; float: left;">
                                                            <%--   <uc1:ReferenceControl ID="rcCountry" runat="server" ValueType="Country" TableName="CountryReference"
                                                                        DataTextField="Description" DataValueField="CountryID"
                                                                        MessageBox="true" ShowMessageBox="True" AutoPostBack="False" 
                                                                        MaxLength="50" />--%
                                                            <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList CssClass="dropdownlist_mandatory" Width="170px" ID="rcCountry"
                                                           runat="server">
                                                                    </asp:DropDownList>
                                                                    <asp:ImageButton ID="imgCountry" runat="server" CssClass="imagebutton" SkinID="Reference"
                                                                        CausesValidation="False" OnClick="imgCountry_Click" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <div style="width: 20%; float: left;">
                                                            Decimal Place
                                                        </div>
                                                        <div style="width: 80%; float: left;">
                                                            <div style="width: 100%;">
                                                                <div style="float: left">
                                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeDecimal" runat="server" FilterType="Numbers"
                                                                        TargetControlID="txtDecimal">
                                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                    <asp:TextBox ID="txtDecimal" runat="server" CssClass="textbox_mandatory" Width="50px"
                                                                        MaxLength="1"></asp:TextBox>
                                                                </div>
                                                                <div style="float: right">
                                                                    <asp:Button ID="btnSave" runat="server" CssClass="save" OnClick="btnSave_Click" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div style="width: 100%; float: left;">
                                        <hr />
                                    </div>
                                    <div style="width: 100%; float: left;" class="container">
                                        <div class="container_head">
                                            <div style="width: 400px; font-size: 11px">
                                                <div style="float: left; width: 125px;">
                                                    Currency
                                                </div>
                                                <div style="float: left; width: 95px;">
                                                    Short Name
                                                </div>
                                                <div style="float: left; width: 100px;">
                                                    Country
                                                </div>
                                                <div style="float: left; width: 50px">
                                                    Decimal
                                                </div>
                                            </div>
                                        </div>
                                        <div class="container_content">
                                            <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DataList ID="dlCurrency" runat="server" DataKeyField="CurrencyID" OnSelectedIndexChanged="dlCurrency_SelectedIndexChanged"
                                                        OnItemDataBound="dlCurrency_ItemDataBound" Style="background-color: White;">
                                                        <ItemTemplate>
                                                            <div style="width: 400px; font-size: 11px;">
                                                                <div id="trCurrency" style="width: 100%;" runat="server" onclick="ChangeCurrencyIndex(this.id);">
                                                                    <div style="width: 125px; float: left;">
                                                                        <asp:LinkButton ID="lnkCurrency" runat="server" Text='<%# Eval("Currency") %>' CommandArgument='<%# Eval("CurrencyID") %>'
                                                                            CommandName="SELECT" CssClass="linkbutton" CausesValidation="false"></asp:LinkButton>
                                                                    </div>
                                                                    <div style="width: 95px; float: left;">
                                                                        <%# Eval("ShortDescription") %>
                                                                    </div>
                                                                    <div style="width: 100px; float: left;">
                                                                        <%# Eval("Country") %>
                                                                    </div>
                                                                    <div style="width: 50px; float: left;">
                                                                        <%# Eval("Scale") %>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </ItemTemplate>
                                                        <ItemStyle CssClass="item" />
                                                    </asp:DataList>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="ItemDataBound" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    </table>
                                </fieldset>
                                <asp:RequiredFieldValidator ID="rfvCurrency" runat="server" ErrorMessage="Please enter currency"
                                    ControlToValidate="txtCurrency" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvShortName" runat="server" ErrorMessage="Please enter short name"
                                    ControlToValidate="txtShortName" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rfvDecimal" runat="server" ErrorMessage="Please enter decimal place"
                                    ControlToValidate="txtDecimal" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="rvDecimal" runat="server" ControlToValidate="txtDecimal"
                                    ErrorMessage="Decimal point should not be greater than 3." CssClass="error" Display="None"
                                    MaximumValue="3" MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                                <asp:ValidationSummary ID="vsCurrency" runat="server" ShowMessageBox="True" ShowSummary="False" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table width="100%" style="font-size: 11px">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblWarning" runat="server" CssClass="error"></asp:Label>
                                        </td>
                                        <td width="60" height="35">
                                            <asp:Button ID="btnOK" runat="server" CssClass="ok" OnClick="btnOK_Click" />
                                        </td>
                                        <td width="60">
                                            <asp:Button ID="btnCancel" runat="server" CssClass="cancel" OnClick="btnCancel_Click"
                                                CausesValidation="False" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="dlCurrency" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnClose" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
        <td class="tdMiddleRight">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="tdBottomLeft" width="20">
            &nbsp;
        </td>
        <td class="tdBottomCenter">
            &nbsp;
        </td>
        <td class="tdBottomRight">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button1" runat="server" />
                    </div>
                    <asp:Panel ID="pnlModalPopUp" Style="display: none" runat="server">
                        <uc1:ReferenceControlNew ID="ReferenceControlNew" runat="server" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                        PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
--%>