﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateToEmployeeHistory.ascx.cs"
    Inherits="Controls_CandidateToEmployeeHistory" %>

<script runat="server">

   
    
</script>

<div style="width: 98%">
    <%--/*Candidate Basic Details*/--%>
    <div style="float: right; width: 4%; padding-left: 7px">
        <asp:ImageButton ID="imgPrint" runat="server" ImageUrl="~/images/Print.PNG" OnClick="imgPrint_Click"
            Style="width: 16px" />
    </div>
    <div id="dvDetails" runat ="server"  style="width: 100%; padding-left: 10px">
        <div>
            <div style="float: left; width: 95%">
                <h4 style="padding-top: 5px; padding-left: 0px; padding-bottom: 5px; color: #108dbc;">
                  <%--  Basic Info:---%>
                    <asp:Literal ID="Literal4" runat ="server" Text ='<%$Resources:ControlsCommon,BasicInfo %>'>
                
                </asp:Literal>
                    
                    </h4>
            </div>
            <div style="float: left; width: 150px">
                <asp:Label CssClass="labeltext" ID="lblFirstNameL" runat="server" Text='<%$Resources:ControlsCommon,Employee %>'></asp:Label>
            </div>
            <div style="float: left; width: 02%">
                <asp:Label CssClass="labeltext" ID="Label1" runat="server" Text=":"></asp:Label>
            </div>
            <div style="float: left; width:250px">
                <asp:Label CssClass="labeltext" ID="lblFirstNameR" runat="server" Text=""></asp:Label>
            </div>
            <div id="dvJobTitle" runat="server">
                <div style="float: left; width: 150px">
                    <asp:Label CssClass="labeltext" ID="lblJobTitleL" runat="server" Text='<%$Resources:ControlsCommon,JobTitle %>'></asp:Label>
                </div>
                <div style="float: left; width: 02%">
                    <asp:Label CssClass="labeltext" ID="lblJobTitleColon" runat="server" Text=":"></asp:Label>
                </div>
                <div style="float: left; width: 150px">
                    <asp:Label CssClass="labeltext" ID="lblJobTitleR" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div style="clear:both; float: left; width: 150px">
                <asp:Label CssClass="labeltext" ID="Label2" runat="server" Text='<%$Resources:ControlsCommon,Department %>'></asp:Label>
            </div>
            <div style="float: left; width: 02%">
                <asp:Label CssClass="labeltext" ID="Label5" runat="server" Text=":"></asp:Label>
            </div>
            <div style="float: left; width: 250px">
                <asp:Label CssClass="labeltext" ID="lblDepartmentR" runat="server" Text=""></asp:Label>
            </div>
            <div style="float: left; width: 150px">
                <asp:Label CssClass="labeltext" ID="Label7" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'></asp:Label>
            </div>
            <div style="float: left; width: 02%">
                <asp:Label CssClass="labeltext" ID="Label8" runat="server" Text=":"></asp:Label>
            </div>
            <div style="float: left; width: 150px">
                <asp:Label CssClass="labeltext" ID="lblDesignationR" runat="server" Text=""></asp:Label>
            </div>
            <div style="clear:both; float: left; width: 150px">
                <asp:Label CssClass="labeltext" ID="lblEmployeeJoiningDateL" runat="server" Text='<%$Resources:ControlsCommon,DateOfJoining %>'></asp:Label>
            </div>
            <div style="float: left; width: 02%">
                <asp:Label CssClass="labeltext" ID="Label4" runat="server" Text=":"></asp:Label>
            </div>
            <div style="float: left; width: 250px">
                <asp:Label CssClass="labeltext" ID="lblEmployeeJoiningDateR" runat="server" Text=""></asp:Label>
            </div>
            <div style="clear: both">
            </div>
        </div>
        <div id="dvEvaluation" runat ="server" >
            <h4 style="padding-top: 5px; padding-left: 0px; padding-bottom: 5px; color: #108dbc;">
              <%--  Evaluation Details:---%>
                
                <asp:Literal ID="Lit1" runat ="server" Text ='<%$Resources:ControlsCommon,EvaluationDetails %>'>
                
                </asp:Literal>
                </h4>
            <div>
                <div>
                    <asp:DataList ID="dlInterviewDetails" Width="100%" runat="server" BackColor="White"
                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both"
                        OnItemDataBound="dlInterviewDetails_ItemDataBound">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <ItemStyle ForeColor="#000066" />
                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle Font-Bold="false" Font-Size="Smaller" BackColor="#006699" ForeColor="White" />
                        <HeaderTemplate>
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 7%">
                                       <%-- Sl.No--%>
                                       <asp:Literal ID="Lit" runat ="server" Text ='<%$Resources:ControlsCommon,SlNo%>'></asp:Literal>
                                    </td>
                                    <td>
                                       <%-- Schedule Details--%>
                                       <asp:Literal ID="Lit1" runat ="server" Text ='<%$Resources:ControlsCommon,ScheduleDetails %>'>
                
                </asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <ItemStyle Font-Bold="false" Font-Size="Smaller" />
                        <ItemTemplate>
                            <div>
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 4%">
                                            <%#Eval("SLNo") %>.
                                            <asp:HiddenField ID="hfJobScheduleID" runat="server" Value='<%# Eval("JobScheduleID") %>' />
                                        </td>
                                        <td style="width: 20%">
                                        <%--    Job Level--%>
                                        
                                        <asp:Literal ID="Lit12" runat ="server" Text ='<%$Resources:ControlsCommon,JobLevel %>'>
                                        </asp:Literal>
                                        
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 33%">
                                            <%#Eval("Level")%>
                                        </td>
                                        <td style="width: 15%">
                                           <%-- Interview Date--%>
                                           <asp:Literal ID="Literal7" runat ="server" Text ='<%$Resources:ControlsCommon,InterviewDate %>'>
                                        </asp:Literal>
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 20%">
                                            <%#Eval("InterviewDate")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 6%">
                                        </td>
                                        <td style="width: 20%">
                                           <%-- Venue--%>
                                           <asp:Literal ID="Literal8" runat ="server" Text ='<%$Resources:ControlsCommon,Venue %>'>
                                        </asp:Literal>
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 33%">
                                            <%#Eval("Venue")%>
                                        </td>
                                        <td style="width: 15%">
                                          <%--  No of interviewers--%>
                                          
                                          
                                          <asp:Literal ID="Literal9" runat ="server" Text ='<%$Resources:ControlsCommon,NoOFInterviewers %>'>
                                        </asp:Literal>
                                          
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 20%">
                                            <%#Eval("NoOfInterviewers")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 6%">
                                        </td>
                                        <td style="width: 20%">
                                           <%-- Total weighted mark--%>
                                           <asp:Literal ID="Literal10" runat ="server" Text ='<%$Resources:ControlsCommon,TotalWeightedMark %>'>
                                        </asp:Literal>
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 33%">
                                            <%#Eval("TotalWeightedMark")%>
                                        </td>
                                        <td style="width: 15%">
                                           <%-- Mark Percentage--%>
                                           <asp:Literal ID="Literal11" runat ="server" Text ='<%$Resources:ControlsCommon,MarkPercentage %>'>
                                        </asp:Literal>
                                        </td>
                                        <td style="width: 02%">
                                            :
                                        </td>
                                        <td style="width: 20%">
                                            <%#Eval("MarkPercentage")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="3">
                                            <h4 style="padding-top: 5px; padding-left: 0px; padding-bottom: 5px; color: #108dbc;">
                                      <%--          Interview Details:---%>
                                                
                                               <asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:ControlsCommon,InterviewDetails %>'>
                                        </asp:Literal> 
                                                </h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td colspan="6">
                                            <div style="margin-left: 0px; widows: 100%">
                                                <asp:DataGrid Width="100%" ID="dgvInnerEvaluationDetails" runat="server">
                                                </asp:DataGrid>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div id="dvNoScheduleDetails" runat="server" visible="false">
                    <asp:Label ID="lblNoEvaluationRecord" runat="server" CssClass="error" Text="No record found !!!"></asp:Label>
                </div>
            </div>
            <div style="clear: both">
            </div>
        </div>
        <div id="dvOfferLetterDetails" runat="server">
            <h4 style="padding-top: 5px; padding-left: 0px; padding-bottom: 5px; color: #108dbc;">
                <%--Offer Letter Details:---%>
                
                <asp:Literal ID="Literal5" runat ="server" Text ='<%$Resources:ControlsCommon,OfferLetterDetails %>'>
                
                </asp:Literal>
                </h4>
            <div style="float: left; width: 20%">
                <asp:Label CssClass="labeltext" ID="Label6" runat="server" Text='<%$Resources:ControlsCommon,OfferSentDate %>'></asp:Label>
            </div>
            <div style="float: left; width: 02%">
                <asp:Label CssClass="labeltext" ID="Label9" runat="server" Text=":"></asp:Label>
            </div>
            <div style="float: left; width: 28%">
                <asp:Label CssClass="labeltext" ID="lblOfferSentDate" runat="server"></asp:Label>
            </div>
            <div style="clear: both">
                <asp:DataList Width="100%" ID="dlOfferLetter" runat="server" BackColor="White" BorderColor="#CCCCCC"
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <ItemStyle Font-Size="Smaller" ForeColor="#000066" />
                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 60%">
                                  <%--  Particular--%>
                                  <asp:Literal ID="Literal2" runat ="server" Text='<%$Resources:ControlsCommon,Particulars %>'>
                                  </asp:Literal>
                                </td>
                                <td style="width: 30%">
                                   <%-- Type--%>
                                    <asp:Literal ID="Literal13" runat ="server" Text='<%$Resources:ControlsCommon,Type %>'>
                                  </asp:Literal>
                                </td>
                                <td style="width: 10%">
                                  <%--  Amount--%>
                                  <asp:Literal ID="Literal1" runat ="server" Text='<%$Resources:ControlsCommon,Amount %>'>
                                  </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 60%">
                                    <%#Eval("Particular")%>
                                </td>
                                <td style="width: 30%">
                                    <%#Eval("Remarks")%>
                                </td>
                                <td style="width: 10%">
                                    <%#Eval("Amount")%>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
        
        
        
        
           <div id="dvSalaryHistoryDetails" runat="server">
            <h4 style="padding-top: 5px; padding-left: 0px; padding-bottom: 5px; color: #108dbc;">
               <%-- Salary History Details:---%>
                
                <asp:Literal ID="Literal6" runat ="server" Text ='<%$Resources:ControlsCommon,SalaryHistoryDetails %>'>
                
                </asp:Literal>
                </h4>
           
          
            <div style="clear: both">
                <asp:DataList Width="100%" ID="dlSalaryHistory" runat="server" 
                    BackColor="White" BorderColor="#CCCCCC"
                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both" 
                    onitemcommand="dlSalaryHistory_ItemCommand" 
                    onitemdatabound="dlSalaryHistory_ItemDataBound">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <ItemStyle Font-Size="Smaller" ForeColor="#000066" />
                    <SelectedItemStyle BackColor="#669999" Font-Bold="false" ForeColor="White" />
                    <HeaderTemplate>
                    
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 30%" >
                                 <%--   History Date--%>
                                   <asp:Literal ID="Literal14" runat ="server" Text='<%$Resources:ControlsCommon,HistoryDate %>'>
                                  </asp:Literal>
                                </td>
                                <td style="width: 40%">
                                    <%--Particular--%>
                                    <asp:Literal ID="Literal1" runat ="server" Text='<%$Resources:ControlsCommon,Particulars %>'>
                                  </asp:Literal>
                                </td>
                                <td style="width: 20%">
                                   <%-- Amount--%>
                                   <asp:Literal ID="Literal3" runat ="server" Text='<%$Resources:ControlsCommon,Amount %>'>
                                  </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table style="width: 100%">
                            <tr>
                             
                              <td style ="width:30%">
                                 <asp:Label ID="lblHistoryDate" runat ="server" Text ='  <%#Eval("HistoryDate")%>' ></asp:Label>   
                                  <asp:HiddenField ID="hfDate" runat ="server" Value =' <%#Eval("HistoryDateD")%>' ></asp:HiddenField>   
                                 

                              </td>
                              
                              
                              
                                                          
                              <td    style="width: 40%">
                              
                              
                               <asp:Label ID="lblParticular" runat ="server" Text ='<%#Eval("AdditionDeduction")%>'></asp:Label>
                              </td>
                              
                              
                                 <td  style="width: 20%">
                                 
                                     <asp:Label ID="lblAmount" runat ="server" Text ='<%#Eval("Amount")%>'></asp:Label>

                              
                              </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
        
    </div>
</div>

  <div id="divPintDetails" runat="server" style="float: left; width: 100%;">
        <asp:UpdatePanel ID="upPrint" runat="server">
            <ContentTemplate>
                <div id="divPrint" runat="server" style="display: none">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
