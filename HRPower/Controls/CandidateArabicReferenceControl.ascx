﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateArabicReferenceControl.ascx.cs"
    Inherits="Controls_CandidateArabicReferenceControl" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<script type="text/javascript">

    function closThis(elementID) {
        var e = document.getElementById(elementID);

        if (e) {
            e.style.display = 'none';
            return false;
        }
    }
 
    
function ValidateReferenceControlNew(ctrl)
{

debugger ;
   var txtBoxEnglish = document.getElementById(ctrl.id.replace('btnSave', 'txtReferenceName'));
   var txtBoxArabic = document.getElementById(ctrl.id.replace('btnSave', 'txtReferenceNameArabic'));
   
   if(txtBoxEnglish && txtBoxArabic )
   {
        if(txtBoxEnglish.value == '' && txtBoxArabic.value == '') 
        {
            alert('Both english and arabic field required');
            txtBoxEnglish.focus();
            return false;
        }
        else if(txtBoxEnglish.value == '' ) 
        {
            alert('English field required');
            txtBoxEnglish.focus();
            return false;
        }
         else if(txtBoxArabic.value == '' ) 
        {
            alert('Arabic field required');
            txtBoxArabic.focus();
            return false;
        }
        
        
        else {
            return true;
        }
   }
   return false;
}
    
  

</script>

<div id="popupmainwrap">
    <div id="popupmain" style="width: 450px !important">
        <div id="header11">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" Text="HR Power"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset>
                        <div id="content1">
                            <div style="float: left; width: 200px">
                                <asp:Label ID="lblEnglish" Text="English" runat="server"></asp:Label>
                            </div>
                            <div style="float: right; width: 38px; padding-right: 50px;">
                                <asp:Label ID="Label2" Text="العربية" runat="server"></asp:Label>
                            </div>
                            <div style="clear: both">
                                <div style="float: left; width: 200px">
                                    <asp:TextBox ID="txtReferenceName" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                        Width="180px" MaxLength="50"></asp:TextBox>
                                </div>
                                <div style="float: left; width: 175px">
                                    <asp:TextBox ID="txtReferenceNameArabic" onkeyup="arabicValue(this);" runat="server"
                                        BorderStyle="Solid" BorderWidth="1px" Width="171px" MaxLength="50"></asp:TextBox>
                                </div>
                                <asp:ImageButton ID="btnSave" runat="server" vertical-align="top" Style="margin-right: 3px;
                                    margin-left: 5px; width: 14px; height: 16px;" CausesValidation="false" OnClick="btnAdd_Click"
                                    Width="16px" ToolTip="Save" OnClientClick="return ValidateReferenceControlNew(this);"
                                    ImageUrl="~/images/save_icon.jpg" />
                                <asp:ImageButton ID="imgClear" runat="server" vertical-align="top" Style="margin-right: 3px;
                                    margin-left: -1px; width: 14px; height: 16px;" CausesValidation="false" Width="16px"
                                    OnClick="imgClear_Click" ToolTip="Add New Item" ImageUrl="~/images/new_icon_reference.PNG" />
                            </div>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <div id="content" style="max-height: 226px; overflow: auto;">
                            <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                Width="100%">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Particular(Eng)" ItemStyle-CssClass="datalistTrLeft"
                                        ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <div style="width: 180px; overflow: hidden">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("DataText") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Particular(Arabic)" ItemStyle-CssClass="datalistTrLeft"
                                        ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <div style="width: 180px; overflow: hidden; text-align: right">
                                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("DataTextArabic") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                OnClick="ImgEdit_Click1" OnClientClick="return true" ToolTip="edit" ImageUrl="~/images/edit_popup.png" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                OnClick="ImgDelete_Click" OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);"
                                                ToolTip="Delete" ImageUrl="~/images/delete_popup.png" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div id="footerwrapper">
                    <div id="footer11" style="height: 30px;">
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="False"></asp:Label>
                    </div>
                </div>
                <div style="display: none">
                    <asp:HiddenField ID="hfId" runat="server" />
                    <asp:HiddenField ID="hfDataTextField" runat="server" />
                    <asp:HiddenField ID="hfDataValueField" runat="server" />
                    <asp:HiddenField ID="hfDataTextValue" runat="server" />
                    <asp:HiddenField ID="hfDataTextFieldArabic" runat="server" />
                    <asp:HiddenField ID="hfDataValueFieldArabic" runat="server" />
                    <asp:HiddenField ID="hfDataNoValue" runat="server" />
                    <asp:HiddenField ID="hfPredefinedField" runat="server" />
                    <asp:HiddenField ID="hfTableName" runat="server" />
                    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
                    <asp:HiddenField ID="hfFunctionName" runat="server" />
                    <asp:HiddenField ID="hfReferenceField" runat="server" />
                    <asp:HiddenField ID="hfReferenceId" runat="server" />
                    <asp:HiddenField ID="hfDisplayName" runat="server" />
                    <asp:HiddenField ID="hfPreviousID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfVaidationGroup" runat="server" Value="" />
                    <asp:HiddenField ID="hfParentElementId" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeFiled" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeValues" runat="server" Value="" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
