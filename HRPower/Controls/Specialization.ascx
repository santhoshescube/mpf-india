﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Specialization.ascx.cs"
    Inherits="controls_Specialization" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link id="cssMaroon" runat="server" href="../App_Themes/Maroon/css/Maroon.css" rel="stylesheet"
    type="text/css" disabled="disabled" />
<%--<script type ="text/javascript">
function ValidateReferenceControl()
{
alert("fdfdf");
return false;
}
--%>

<script src="../js/HRPower.js" type="text/javascript"></script>

<style type="text/css">
    .style1
    {
        height: 33px;
    }
</style>
<div style="display: none">
    <asp:HiddenField ID="hfId" runat="server" />
    <asp:HiddenField ID="hfDataTextField" runat="server" />
    <asp:HiddenField ID="hfDataValueField" runat="server" />
    <asp:HiddenField ID="hfDataTextValue" runat="server" />
    <asp:HiddenField ID="hfDataNoValue" runat="server" />
    <asp:HiddenField ID="hfPredefinedField" runat="server" />
    <asp:HiddenField ID="hfTableName" runat="server" />
    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
    <asp:HiddenField ID="hfFunctionName" runat="server" />
    <asp:HiddenField ID="hfReferenceField" runat="server" />
    <asp:HiddenField ID="hfReferenceId" runat="server" />
    <asp:HiddenField ID="hfDisplayName" runat="server" />
</div>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 500px;">
        <div id="divHeaderMove" runat="server">
            <div id="header11">
                <div id="hbox1">
                    <div id="headername">
                        Specialization
                    </div>
                </div>
                <div id="hbox2">
                    <div id="close1">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="False" />
                    </div>
                </div>
            </div>
        </div>
        <div id="toolbar" style="height: 10px;">
            <div id="imgbuttons">
            </div>
        </div>
        <div id="contentwrap">
            <div id="content">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%;">
                            <div style="width: 100%;">
                                <fieldset>
                                    <div style="width: 100%; height: 30px;margin-top:10px;">
                                        <div style="width: 20%; height: 30px; float: left; margin-right:5%;">
                                            Specialization
                                        </div>
                                        <div style="width: 75%; height: 30px;">
                                            <asp:TextBox ID="txtSpecialization" runat="server" BorderStyle="Solid" ValidationGroup="Save"
                                                MaxLength="50" BorderWidth="1px" Height="8px" Width="200px"></asp:TextBox>
                                           <%-- <asp:RequiredFieldValidator ID="rfvtxtDegreeName" runat="server" CssClass="error"
                                                ValidationGroup="Save" ControlToValidate="txtSpecialization" ErrorMessage="Field required"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 20%; height: 30px; float: left;margin-right:5%;">
                                            Degree
                                        </div>
                                        <div style="width: 75%; height: 30px; float: left;">
                                            <asp:DropDownList ID="ddlDegree" runat="server" ValidationGroup="Save" CssClass="dropdownlist_mandatory"
                                                Width="203px" Height="25px">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnSave" runat="server" Style="margin-right: 3px; margin-left: 10px;
                                                margin-bottom: 0px; padding-top: 5px;" class="popupbutton" ValidationGroup="Save" OnClick="btnSave_Click" OnClientClick="return ValidateSaveDegree(this);"
                                                ToolTip="Save" Text="Save" />
                                            <asp:Button ID="imgClear" runat="server" Style="margin-right: 3px; margin-left: 3px;" class="popupbutton"
                                                CausesValidation="false" OnClick="imgClear_Click" ToolTip="Clear"
                                                Text="Clear" />
                                            <asp:RequiredFieldValidator ID="rfvddlQualificationType" runat="server" CssClass="error"
                                                InitialValue="-1" ValidationGroup="Save" ControlToValidate="ddlDegree" ErrorMessage="Field required"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div style="width: 100%;">
                                <fieldset>
                                    <div style="max-height: 215px; overflow: auto;">
                                        <asp:GridView ID="gvDegreeReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                            Width="100%" DataKeyNames="SpecializationID,DegreeID" OnRowCommand="gvDegreeReference_RowCommand">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Specialization" ItemStyle-CssClass="datalistTrLeft"
                                                    ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 150px; overflow: hidden">
                                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Specialization") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Degree" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 125px; overflow: hidden">
                                                            <asp:Label ID="lblQualificationType" runat="server" Text='<%# Eval("Degree") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:Button ID="ImgEdit" class="popupbutton" runat="server" CommandName="_ALTER" Text="Edit" CommandArgument='<%# Eval("SpecializationID") %>'
                                                            OnClientClick="return true" ToolTip="edit" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:Button ID="ImgDelete" class="popupbutton" runat="server" CommandArgument='<%# Eval("SpecializationID") %>'
                                                            Text="Delete" CommandName="_REMOVE" OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);"
                                                            ToolTip="Delete" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11" style="height: 20px;">
                <asp:UpdatePanel ID="upfooter" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" Style="margin-left: 10px;"
                            runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
