﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SalaryScale.ascx.cs" Inherits="controls_SalaryScale" %>
<%@ Register Src="~/controls/FormViewPager.ascx" TagPrefix="cc" TagName="FormViewPager" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link id="cssMaroon" runat="server" href="../App_Themes/Maroon/css/Maroon.css" rel="stylesheet" type="text/css" disabled="disabled" />

<style type="text/css">
 
 
</style>

<table cellpadding="0" cellspacing="0" width="508">
    <tr>
        <td height="5" width="20" class="tdTopLeft">
            &nbsp;
        </td>
        <td class="tdTopCenter">
            <table cellpadding="0" cellspacing="0" width='100%'>
                <tr>
                    <td class="header_text popup_header_padding" id="tdDrag" runat="server">
                        Salary Scale
                    </td>
                    <td align="right" width="20" class="popup_close">
                        <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:ImageButton ID="btnClose" runat="server" ToolTip="Close" 
                                CausesValidation="False" SkinID="CloseIcon" onclick="btnClose_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
        <td width="20" class="tdTopRight">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="tdMiddleLeft">
            &nbsp;
        </td>
        <td>
            <table width='100%' cellpadding="0" cellspacing="0" style="font-family: Verdana;" bgcolor="White">
                <tr>
                    <td valign="top">
                        <table width="100%" bgcolor="White" style="font-size: 11px" bgcolor="White">
                            <tr>
                                <td width="50%">
                                    <cc:FormViewPager ID="fvpSalaryScale" runat="server" />
                                </td>
                                <td class="popupWindow_headerbg">
                                    <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:ImageButton ID="btnAddNew" runat="server" ToolTip="Add New" CausesValidation="False" SkinID="NewIcon" />
                                            <asp:ImageButton ID="btnSaveData" runat="server" ToolTip="Save Data" SkinID="PopupSave" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" ToolTip="Delete" Enabled="False" 
                                            OnClientClick="return confirm('Do you wish to delete this information?');" SkinID="DeleteIcon" />
                                            <asp:ImageButton ID="btnClear" runat="server" CausesValidation="False" ToolTip="Clear" SkinID="ClearIcon" />
                                            <asp:ImageButton ID="btnPrint" runat="server" CausesValidation="False" ToolTip="Print" SkinID="PrintIcon" Visible="False" />
                                            <asp:ImageButton ID="btnEmail" runat="server" CausesValidation="False" ToolTip="Email" SkinID="EmailIcon" Visible="False" />
                                        </ContentTemplate>
                                        <Triggers>
                                           
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                   <td>
                      <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <fieldset style="padding: 10px; height: 300px">
                                    <table cellpadding="1" cellspacing="0" width="100%" style="font-size: 11px">
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="100%" style="font-size: 11px">
                                                            <tr>
                                                                <td>Scale Name</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtScaleName" runat="server" CssClass="textbox" Width="303px" 
                                                                    BackColor="#FFFFE1" MaxLength="30"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Gross Amount</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtGrossAmount" runat="server" CssClass="textbox" 
                                                                        Width="158px"></asp:TextBox>
                                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="txtGrossAmount_FilteredTextBoxExtender" 
                                                                        runat="server" FilterMode="ValidChars" FilterType="Numbers" 
                                                                        TargetControlID="txtGrossAmount">
                                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                    </table>
                                                       
                                                    </ContentTemplate>
                                                    <Triggers>
                                                       
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                <tr>
                                <td>
                                <div id="div1" class="TabbedPanels" style="width: 100%" >                               
                                    <ul class="TabbedPanelsTabGroup" id="ulTabGroup" runat="server">
                                        <li class="TabbedPanelsTabSelected" id="pnlpTab1" runat="server" onclick="SetAdditionDeductionPolicyTab(this.id);">
                                            Additions</li>
                                        <li class="TabbedPanelsTab" id="pnlpTab2" runat="server" onclick="SetAdditionDeductionPolicyTab(this.id);">
                                            Deductions</li>       
                                    </ul>
                                    <div class="TabbedPanelsContentGroup">
                                        <div class="TabbedPanelsContent" id="divpTab1" runat="server" style="display: block; "> 
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="container">
                                                        <div class="container_head">
                                                            <table width='100%' style="font-size: 11px">
                                                                <tr>
                                                                    <td width="100" >Particulars</td>
                                                                    <td width="70" >Policy</td>
                                                                    <td width="40" >Type</td>
                                                                    <td width="40" >Amount</td>
                                                                    <td width="5" align="left">
                                                                        <asp:UpdatePanel ID="upButton" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="btnAdditions" runat="server" CssClass="plus_button"  
                                                                                ToolTip="Add more particulars" Font-Size="11px" Text=""   
                                                                                    onclick="btnAdditions_Click" CausesValidation="false" />
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <%--  <div class="container_content">--%>
                                                        <div style="height:164px;overflow:auto;">
                                                            <asp:UpdatePanel ID="upList" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DataList ID="dlSalaryScale" runat="server" Width="100%" 
                                                                    BorderWidth="0px" CellPadding="0" 
                                                                    DataKeyField="AddDedID" onitemdatabound="dlSalaryScale_ItemDataBound" 
                                                                        onitemcommand="dlSalaryScale_ItemCommand" >
                                                                        <ItemStyle CssClass="item" />
                                                                            <ItemTemplate>
                                                                                <table width='100%'>
                                                                                    <tr>
                                                                                        <td width="100" >
                                                                                            <asp:DropDownList ID="ddlParticulars" width="100%" CssClass="dropdownlist_mandatory"
                                                                                                DataTextField="Description" DataValueField="AddDedID" runat="server" AutoPostBack="True"
                                                                                                OnSelectedIndexChanged="ddlParticulars_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                            <asp:HiddenField ID="hdAddDedID" runat="server" Value='<%# Eval("AddDedID") %>' />
                                                                                        </td>
                                                                                        <td width="70">
                                                                                            <asp:Label ID="PolicyName" Text='  <%# Eval("PolicyName")%>' runat="server"></asp:Label>
                                                                                            <asp:HiddenField ID="RateOnly" runat="server" Value='<%# Eval("RateOnly") %>' />
                                                                                            <asp:HiddenField ID="DeductionPolicyID" runat="server" Value='<%# Eval("DeductionPolicyID") %>' />
                                                                                        </td>
                                                                                        <td width="40" >
                                                                                            <asp:Label ID="Type" Text='  <%# Eval("Type")%>' runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td width="40" >
                                                                                            <asp:Label ID="EmployerAmount" Text='  <%# Eval("EmployerAmount")%>' runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td width="5" align="left">
                                                                                            <asp:ImageButton ID="btnRemove" runat="server" CausesValidation="false" SkinID="DeleteIconDatalist"
                                                                                                CssClass="imagebutton" CommandArgument='<%# Eval("AddDedID") %>' CommandName="REMOVE_ALLOWANCE"
                                                                                                OnClientClick="return confirm('Are you sure to remove this particular detail?');" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                    </asp:DataList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        
                                        <div class="TabbedPanelsContent" id="divpTab2" runat="server" style="display: none; "> 
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="container">
                                                        <div class="container_head">
                                                            <table width='100%' style="font-size: 11px">
                                                                <tr>
                                                                    <td width="100" >Particulars</td>
                                                                    <td width="70" >Policy</td>
                                                                    <td width="40" >Type</td>
                                                                    <td width="40" >Employer Amount</td>
                                                                    <td width="40" >Employee Amount</td>
                                                                     <td width="5" align="left">
                                                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:Button ID="btnDedAdditions" runat="server" CssClass="plus_button"  
                                                                                ToolTip="Add more particulars" Font-Size="11px" Text=""  
                                                                                    onclick="btnDedAdditions_Click" CausesValidation="false" />
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="btnDedAdditions" EventName="Click" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <%--  <div class="container_content">--%>
                                                        <div style="height:145px;overflow:auto;">
                                                            <asp:UpdatePanel ID="UpDedList" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DataList ID="dlDeductions" runat="server" Width="100%" 
                                                                    DataKeyField="AddDedID" BorderWidth="0px" CellPadding="0" 
                                                                        onitemdatabound="dlDeductions_ItemDataBound" 
                                                                        onitemcommand="dlDeductions_ItemCommand" >
                                                                        <ItemStyle CssClass="item" />
                                                                        <ItemTemplate>
                                                                           <table width='100%'>
                                                                                    <tr>
                                                                                        <td width="100" >
                                                                                            <asp:DropDownList ID="ddlDedParticulars" width="100%" CssClass="dropdownlist_mandatory"
                                                                                                DataTextField="Description" DataValueField="AddDedID" runat="server" 
                                                                                                AutoPostBack="True" 
                                                                                                onselectedindexchanged="ddlDedParticulars_SelectedIndexChanged">
                                                                                            </asp:DropDownList>
                                                                                            <asp:HiddenField ID="hdAddDedID" runat="server" Value='<%# Eval("AddDedID") %>' />
                                                                                        </td>
                                                                                        <td width="70">
                                                                                            <asp:Label ID="PolicyName" Text='  <%# Eval("PolicyName")%>' runat="server"></asp:Label>
                                                                                            <asp:HiddenField ID="RateOnly" runat="server" Value='<%# Eval("RateOnly") %>' />
                                                                                             <asp:HiddenField ID="hdDeductionPolicyID" runat="server" Value='<%# Eval("DeductionPolicyID") %>' />
                                                                                        </td>
                                                                                        <td width="40" >
                                                                                            <asp:Label ID="Type" Text='  <%# Eval("Type")%>' runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td width="40" >
                                                                                            <asp:Label ID="EmployerAmount" Text='  <%# Eval("EmployerAmount")%>' runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td  width="40" >
                                                                                            <asp:Label ID="EmployeeAmount" Text='  <%# Eval("EmployeeAmount")%>' runat="server"></asp:Label>
                                                                                        </td>
                                                                                        <td width="5" align="left">
                                                                                            <asp:ImageButton ID="btnRemove" runat="server" CausesValidation="false" SkinID="DeleteIconDatalist"
                                                                                                CssClass="imagebutton" CommandArgument='<%# Eval("AddDedID") %>' CommandName="REMOVE_ALLOWANCE"
                                                                                                OnClientClick="return confirm('Are you sure to remove this particular detail?');" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                </td>
                                </tr>
                                </table>
                                </fieldset>
                             </div>
                        </ContentTemplate>
                          <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                        </Triggers>
                      </asp:UpdatePanel>
                   </td>
                </tr>
                           <tr>
                                <td>
                                    <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table width="100%" style="font-size: 11px">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblWarning" runat="server"></asp:Label>
                                                    </td>
                                                    <td width="60" height="35">
                                                        <asp:Button ID="btnOK" ValidationGroup="ScaleSubmit" runat="server" CssClass="ok" onclick="btnOK_Click" />
                                                    </td>
                                                    <td width="6">
                                                        <asp:Button ID="btnCancel" runat="server" CssClass="cancel" 
                                                            CausesValidation="False" onclick="btnCancel_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                        <Triggers>
                                           <%-- <asp:AsyncPostBackTrigger ControlID="dlSalaryScale" 
                                                EventName="SelectedIndexChanged" />--%>
                                            <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                                             <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
            </table>
        </td>
        <td class="tdMiddleRight">&nbsp;</td>
    </tr>
    <tr>
        <td height="9" class="tdBottomLeft">&nbsp;</td>
        <td class="tdBottomCenter">&nbsp;</td>
        <td class="tdBottomRight">&nbsp;</td>
    </tr>
</table>