﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class controls_FolderReference : System.Web.UI.UserControl
{
    clsFolderReference objFolderReference;

    string sModalPopupID;

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    public int SelectedFolder
    {
        get
        {
            return (tvFolders.SelectedValue == "" ? 0 : Convert.ToInt32(tvFolders.SelectedValue));
        }
    }

    public delegate void OnUpdate();
    public event OnUpdate Update;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblWarning.Text = string.Empty;
     
      //  upSubmit.Update();
        this.btnEdit.Enabled = false;

    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {
        ViewState["_EDIT_FOLDER"] = false;
        txtFolder.Text = string.Empty;
        txtFolder.Focus();
        mpeFolderEntry.Show();
    }
    protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
    {
        mpeFolderEntry.Hide();

        if (tvFolders.SelectedValue != "")
            btnEdit.Enabled = true;


    }

    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {
        if (tvFolders.SelectedValue == "")
        {
            lblWarning.Style["display"] = "block";
            lblWarning.Text = "Please select a folder";
        }
        else
        {
            objFolderReference = new clsFolderReference();

            objFolderReference.FolderId = Convert.ToInt32(tvFolders.SelectedValue);

            objFolderReference.Delete();

            DeleteSubFolders(Convert.ToInt32(tvFolders.SelectedValue));
        }
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = headername.ClientID;
        //mpe.CancelControlID = btnClose.ClientID;
        
    }

    private void DeleteSubFolders(int ParentId)
    {
        objFolderReference = new clsFolderReference();

        objFolderReference.ParentId = ParentId;

        DataTable dt = objFolderReference.GetAllFolders().Tables[0];

        foreach (DataRow dw in dt.Rows)
        {
            objFolderReference.FolderId = Convert.ToInt32(dw["FolderId"]);

            objFolderReference.Delete();

            DeleteSubFolders(Convert.ToInt32(dw["FolderId"]));
        }

        Fill();
        this.btnEdit.Enabled = false;
    }

    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        mpe.Hide();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        mpe.Hide();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        SaveResume();
    }

    public bool SaveResume()
    {
        if (tvFolders.SelectedValue == "")
        {
            lblWarning.Style["display"] = "block";
            lblWarning.Text = "Please select any folder.";
            return false;
        }
        else
            lblWarning.Text = string.Empty;

        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        if (Update != null)
            Update();
        mpe.Hide();
        return true;
    }

    public void Fill()
    {
        tvFolders.Nodes.Clear();

        objFolderReference = new clsFolderReference();

        objFolderReference.ParentId = 0;
        txtFolder.Text = "";

        DataTable dt = objFolderReference.GetAllFolders().Tables[0];

        PopulateFolders(dt, tvFolders.Nodes);

        tvFolders.ExpandAll();
    }

    private void PopulateFolders(DataTable dt, TreeNodeCollection nodes)
    {
        TreeNode tn;

        foreach (DataRow dw in dt.Rows)
        {
            tn = new TreeNode();

            tn.Value = Convert.ToString(dw["FolderId"]);
            tn.Text = Convert.ToString(dw["Folder"]);
            tn.ImageUrl = "~/images/folder.jpg";

            tn.PopulateOnDemand = (Convert.ToInt32(dw["ChildCount"]) > 0 ? true : false);

            nodes.Add(tn);
        }
    }

    protected void tvFolders_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        objFolderReference = new clsFolderReference();

        objFolderReference.ParentId = Convert.ToInt32(e.Node.Value);

        DataTable dt = objFolderReference.GetAllFolders().Tables[0];

        PopulateFolders(dt, e.Node.ChildNodes);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objFolderReference = new clsFolderReference();

        objFolderReference.ParentId = (tvFolders.SelectedValue == "" ? 0 : Convert.ToInt32(tvFolders.SelectedValue));
        objFolderReference.Folder = txtFolder.Text;

        if (Convert.ToBoolean(ViewState["_EDIT_FOLDER"]))
        {
            if (tvFolders.SelectedValue == "")
            {
                lblWarning.Style["display"] = "block";
                lblWarning.Text = "Please select a folder";
            }
            else
            {
                objFolderReference.FolderId = Convert.ToInt32(tvFolders.SelectedValue);

                if (objFolderReference.Update() == -1)
                {
                    lblWarning.Style["display"] = "block";
                    lblWarning.Text = "Folder name exists";
                }
                else
                {
                   
                    Fill();
                    mpeFolderEntry.Hide();
                    //if (Update != null)
                    //    Update();
                }
            }
        }
        else
        {
            if (objFolderReference.Insert() == -1)
            {
                lblWarning.Style["display"] = "block";
                lblWarning.Text = "Folder name exists";
            }
            else
            {
                Fill();
                mpeFolderEntry.Hide();
                //if (Update != null)
                //    Update();
            }
        }
    }
    protected void tvFolders_SelectedNodeChanged(object sender, EventArgs e)
    {
        btnDelete.Enabled = true;
        btnEdit.Enabled = true;
    }
    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        if (tvFolders.SelectedValue == "")
        {
            lblWarning.Style["display"] = "block";
            lblWarning.Text = "Please select a folder";
        }
        else
        {
            this.btnEdit.Enabled = false;
            ViewState["_EDIT_FOLDER"] = true;
            txtFolder.Text = tvFolders.SelectedNode.Text;
            txtFolder.Focus();
            mpeFolderEntry.Show();
        }
        

    }
    protected void btnCancelFolderEntry_Click(object sender, EventArgs e)
    {

    }
}
