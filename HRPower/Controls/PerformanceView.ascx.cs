﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_PerformanceView : System.Web.UI.UserControl
{
    public string Title
    {
        set
        {
            lblHeading.Text = value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
      //  this.lblHeading.Text  = Title;
    }
    public void SetGrid(int PerformanceInitiationID, int EmployeeID)
    {

       
        string Name = "";

        ViewState["PerformanceInitiationID"] = PerformanceInitiationID;
        ViewState["EmployeeID"] = EmployeeID;
        DataTable dt = clsPerformanceInitiation.GetPerformanceDetailsForControl(PerformanceInitiationID, EmployeeID);


        dt.Columns[0].Caption =  dt.Columns[0].ColumnName =  GetGlobalResourceObject("ControlsCommon", "SLNo").ToString();
        dt.Columns[1].Caption = dt.Columns[1].ColumnName = GetGlobalResourceObject("ControlsCommon", "Goal").ToString();

        dgv.DataSource = dt;
        dgv.DataBind();

      

      
     }
    

}
