﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DegreeReferenceArabic.ascx.cs"
    Inherits="Controls_DegreeReferenceArabic" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<script type ="text/javascript">
function ValidateReferenceControl()
{
alert("fdfdf");
return false;
}
--%>

<script src="../js/yasArabic.js" type="text/javascript"></script>

<script src="../js/HRPower.js" type="text/javascript"></script>

<style type="text/css">
    .style1
    {
        height: 33px;
    }
</style>
<div style="display: none">
    <asp:HiddenField ID="hfId" runat="server" />
    <asp:HiddenField ID="hfDataTextField" runat="server" />
    <asp:HiddenField ID="hfDataValueField" runat="server" />
    <asp:HiddenField ID="hfDataTextValue" runat="server" />
    <asp:HiddenField ID="hfDataNoValue" runat="server" />
    <asp:HiddenField ID="hfPredefinedField" runat="server" />
    <asp:HiddenField ID="hfTableName" runat="server" />
    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
    <asp:HiddenField ID="hfFunctionName" runat="server" />
    <asp:HiddenField ID="hfReferenceField" runat="server" />
    <asp:HiddenField ID="hfReferenceId" runat="server" />
    <asp:HiddenField ID="hfDisplayName" runat="server" />
</div>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 506px;">
        <div id="divHeaderMove" runat="server">
            <div id="header11">
                <div id="hbox1">
                    <div id="headername">
                        Degree
                    </div>
                </div>
                <div id="hbox2">
                    <div id="close1">
                        <asp:ImageButton ID="imgPopupClose" runat="server" OnClick="ibtnClose_Click" ImageUrl="~/Controls/images/icon-close.png"
                            CausesValidation="False" />
                    </div>
                </div>
            </div>
        </div>
        <div id="toolbar" style="height: 10px;">
            <div id="imgbuttons">
                <!-- <img src="images/save_icon.jpg" />
                    <img src="images/clear.png" />
                    <img src="images/clear.png" />
                    <img src="images/clear.png" />-->
            </div>
        </div>
        <div id="contentwrap">
            <div id="content">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%;">
                            <div style="width: 100%;">
                                <fieldset>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 10%; height: 30px; float: left;">
                                            Eng
                                        </div>
                                        <div style="width: 40%; height: 30px; float: left">
                                            <asp:TextBox ID="txtDegreeName" runat="server" BorderStyle="Solid" ValidationGroup="Save"
                                                MaxLength="50" BorderWidth="1px" Height="8px" Width="150px"></asp:TextBox>
                                        </div>
                                        <div style="width: 37%; height: 30px; float: left">
                                            <asp:TextBox ID="txtArabic" onkeyup="arabicValue(this);" runat="server" BorderStyle="Solid"
                                                ValidationGroup="Save" MaxLength="50" BorderWidth="1px" Height="8px" Width="150px"></asp:TextBox>
                                        </div>
                                        <div style="width: 10%; height: 30px; float: left;">
                                            Arabic
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 10%; height: 30px; float: left;">
                                            Type
                                        </div>
                                        <div style="width: 80%; height: 30px; float: left;">
                                            <asp:DropDownList ID="ddlQualificationType" runat="server" ValidationGroup="Save"
                                                AutoPostBack="true" DataTextField="QualificationType" DataValueField="QualificationTypeId"
                                                CssClass="dropdownlist_mandatory" Width="203px" Height="25px" OnSelectedIndexChanged="ddlQualificationType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnSave" class="popupbutton" runat="server" Style="margin-right: 3px;
                                                margin-left: 10px; margin-bottom: 0px; padding-top: 5px;" ValidationGroup="Save"
                                                OnClick="btnSave_Click" OnClientClick="return ValidateSaveQualification(this);"
                                                Text="Save" ToolTip="Save" />
                                            <asp:Button ID="imgClear" class="popupbutton" runat="server" Style="margin-right: 3px;
                                                margin-left: 3px;" CausesValidation="false" OnClick="imgClear_Click" ToolTip="Clear"
                                                Text="Clear" />
                                            <asp:RequiredFieldValidator ID="rfvddlQualificationType" runat="server" CssClass="error"
                                                InitialValue="-1" ValidationGroup="Save" ControlToValidate="ddlQualificationType"
                                                ErrorMessage="Field required"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div style="width: 100%;">
                                <fieldset>
                                    <div style="max-height: 215px; overflow: auto;">
                                        <asp:GridView ID="gvDegreeReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                            Width="100%" DataKeyNames="DegreeID" OnRowCommand="gvDegreeReference_RowCommand">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Degree English" ItemStyle-CssClass="datalistTrLeft"
                                                    ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 130px; overflow: hidden">
                                                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Degree Arabic">
                                                    <ItemTemplate>
                                                        <div style="width: 130px; overflow: hidden">
                                                            <asp:Label ID="lblDescriptionArb" runat="server" Text='<%# Eval("DegreeArb") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 100px; overflow: hidden">
                                                            <asp:Label ID="lblQualificationType" runat="server" Text='<%# Eval("QualificationType") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" ImageUrl="~/images/edit_popup.png"
                                                            runat="server" CommandName="_ALTER" CommandArgument='<%# Eval("DegreeID") %>'
                                                            OnClientClick="return true" ToolTip="edit" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" ImageUrl="~/images/delete_popup.png"
                                                            runat="server" CommandArgument='<%# Eval("DegreeID") %>' CommandName="_REMOVE"
                                                            OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);"
                                                            ToolTip="Delete" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11" style="height: 20px;">
                <asp:UpdatePanel ID="upfooter" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" Style="margin-left: 10px;"
                            runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
