﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;
using System.Text;


public partial class Controls_AssignEvaluators : System.Web.UI.UserControl
{
    public int CompanyID { get; set; }
    public List<InitiationEmployees> EmployeesList { get; set; }
    public EmployeeInitiation AlreadyAssignedEmployees { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public int PeformanceInitiationID { get; set; }

    public delegate void Result(object ResultList);

    public event Result eResult;
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {



    }

    public void FillAllTemplates(int DepartmentID)
    {
        ddlTemplate.DataSource = clsPerformanceInitiation.GetAllTemplates(DepartmentID);
        ddlTemplate.DataTextField = "TemplateName";
        ddlTemplate.DataValueField = "TemplateID";
        ddlTemplate.DataBind();
    }



    public void ClearAll()
    {
        dl.DataSource = null;
        dl.DataBind();

        dlEvaluator.DataSource = null;
        dlEvaluator.DataBind();
        ViewState["Evaluator"] = null;
    }


    public void LoadEmployeeAndEvaluators(int CompanyID, int PerformanceInitiationID, int DepartmentID, DateTime FromDate, DateTime ToDate)
    {
        ClearAll();
        
        FillAllTemplates(DepartmentID);
        DataTable dt = clsPerformanceInitiation.GetAllEmployees(CompanyID, PerformanceInitiationID, DepartmentID, FromDate, ToDate);
        if (dt.Rows.Count == 0)
        {
          //  lblNoEmployee.Text = "No employee exists in the selected department";
            lblNoEmployee.Text = GetLocalResourceObject("NoEmployeeExists.Text").ToString();
        }

        if (AlreadyAssignedEmployees != null)
        {
            foreach (InitiationEmployees objAssignedEmployees in AlreadyAssignedEmployees.InitiationResult)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["EmployeeID"].ToInt32() == objAssignedEmployees.EmployeeID)
                    {
                        dt.Rows.Remove(dr);
                        break;
                    }
                }
            }

            if (dt.Rows.Count == 0)
            {
                //lblNoEmployee.Text = "No pending employees exists ..All employees are added to the initiation";
                lblNoEmployee.Text = GetLocalResourceObject("NoPendingEmployeeExists.Text").ToString();
            }
        }

        //ViewState["AlreadyAssignedEmployees"] = AlreadyAssignedEmployees;

        EnableDisableControls(dt.Rows.Count);
        dl.DataSource = dt;
        dl.DataBind();

        BindEvaluators(CompanyID );
    }
    private void BindEvaluators(int CompanyID)
    {
        ddlEvaluator.DataSource = clsPerformanceInitiation.GetAllEvaluators(new clsUserMaster().GetCompanyId(), chkIncludeAllCompany.Checked);
        ddlEvaluator.DataTextField = "EmployeeFullName";
        ddlEvaluator.DataValueField = "EmployeeID";
        ddlEvaluator.DataBind();
    }

    private void EnableDisableControls(int RowCount)
    {
        dvNoEmployee.Visible = (RowCount == 0);
        btnAddEvaluator.Enabled = btnsave.Enabled = dl.Visible = (RowCount > 0);
    }
    protected void btnAddEvaluator_Click(object sender, EventArgs e)
    {
        DataTable dtEvaluator;
        bool CanContinue = true;
        DataRow dr;
        if (ViewState["Evaluator"] == null)
        {
            dtEvaluator = new DataTable();

            dtEvaluator.Columns.Add("EvaluatorID",typeof(long));
            dtEvaluator.Columns.Add("EvaluatorName");
        }
        else
        {
            dtEvaluator = ((DataTable)ViewState["Evaluator"]);
        }



        foreach (DataRow drEval in dtEvaluator.Rows)
        {
            if (drEval["EvaluatorID"].ToInt32() == ddlEvaluator.SelectedValue.ToInt32())
            {
                ScriptManager.RegisterClientScriptBlock(updEvaluator, updEvaluator.GetType(), new Guid().ToString(), "alert('"+GetLocalResourceObject("EvaluatorAlreadyExists.Text")+"');", true);
                CanContinue = false;
                break;
            }
        }

        if (CanContinue)
        {
            dr = dtEvaluator.NewRow();

            if (ddlEvaluator.SelectedItem != null)
            {
                dr["EvaluatorID"] = ddlEvaluator.SelectedValue.ToInt32();
                dr["EvaluatorName"] = ddlEvaluator.SelectedItem.Text;
                dtEvaluator.Rows.Add(dr);


                ViewState["Evaluator"] = dtEvaluator;
                dlEvaluator.DataSource = dtEvaluator;
                dlEvaluator.DataBind();

                updEvaluator.Update();
            }
        }

    }
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {


    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {

        foreach (DataListItem item in dl.Items)
        {

            object chkSelect = item.FindControl("chkSelect");

            if (chkSelect != null)
            {
                ((CheckBox)chkSelect).Checked = ((System.Web.UI.WebControls.CheckBox)(sender)).Checked;
            }

            //if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.EditItem)
            //{


            //}
        }

    }
    protected void save_Click(object sender, EventArgs e)
    {
        if (Save())
            updEvaluator.Update();
    }




    public bool Save()
    {

        StringBuilder EvaluatorsName = new StringBuilder();
        int Iterator = 0;

        if (Session["ResultList"] != null)
        {
            AlreadyAssignedEmployees = ((EmployeeInitiation)Session["ResultList"]);
            EmployeesList = AlreadyAssignedEmployees.InitiationResult;
        }
        else
            EmployeesList = new List<InitiationEmployees>();


        DataTable dtEvaluator = ((DataTable)ViewState["Evaluator"]);
        InitiationEmployees Employee = null;

        if (ddlTemplate.Items.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(updEvaluatorSubmit, updEvaluatorSubmit.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PleaseSelectATemplate.Text") + "');", true);
            return false;
        }

        if (dlEvaluator.Items.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(updEvaluatorSubmit, updEvaluatorSubmit.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PleaseSelectAnyEvaluator.Text") + "');", true);
            return false;
        }

        foreach (DataListItem item in dl.Items)
        {

            EvaluatorsName = new StringBuilder();
            CheckBox chkSelect = ((CheckBox)item.FindControl("chkSelect"));
            HiddenField hfEmployeeID = ((HiddenField)item.FindControl("hfEmployeeID"));
            Label lblEmployee = ((Label)item.FindControl("lblEmployee"));

            if (chkSelect.Checked && chkSelect != null && hfEmployeeID != null && lblEmployee != null)
            {
                Iterator = Iterator + 1;
                Employee = new InitiationEmployees()
                {
                    EmployeeID = hfEmployeeID.Value.ToInt32(),
                    EmployeeName = lblEmployee.Text,
                    TemplateID = ddlTemplate.SelectedValue.ToInt32(),
                    TemplateName = ddlTemplate.SelectedItem.Text
                };






                if (dtEvaluator != null && dtEvaluator.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtEvaluator.Rows)
                    {
                        Evaluators eval = new Evaluators()
                        {
                            EvaluatorID = dr["EvaluatorID"].ToInt32(),
                            EvaluatorName = dr["EvaluatorName"].ToString()
                        };

                        EvaluatorsName.Append(dr["EvaluatorName"].ToString());
                        EvaluatorsName.Append(",");
                        Employee.InitiationEvaluators.Add(eval);
                    }

                }

                EmployeesList.Add(Employee);
            }
        }




        if (Iterator == 0)
        {
            ScriptManager.RegisterClientScriptBlock(updEvaluatorSubmit, updEvaluatorSubmit.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PleaseSelectAnyEmployee.Text") + "');", true);
            return false;
        }






        EmployeeInitiation ResultList = new EmployeeInitiation();
        ResultList.InitiationResult = EmployeesList;




        if (eResult != null)
        {
            eResult(ResultList);

        }
        return true;
    }
    protected void imgRemoveEvaluator_Click(object sender, ImageClickEventArgs e)
    {
        if (ViewState["Evaluator"] != null)
        {
            Int64 EvaluatorID = ((ImageButton) sender).CommandArgument.ToInt64(); 
            DataTable dtEvaluator = ((DataTable)ViewState["Evaluator"]);
            int RowCount = dtEvaluator.Rows.Count ;
            if (dtEvaluator != null)
            {
                for(int i = 0;i< RowCount ; i++)
                {
                    if (dtEvaluator.Rows[i]["EvaluatorID"].ToInt64() == EvaluatorID)
                    {
                        dtEvaluator.Rows.Remove(dtEvaluator.Rows[i]);
                        dlEvaluator.DataSource = dtEvaluator;
                        dlEvaluator.DataBind();
                        break;
                    }
                }
            }


            
        }
    }

    protected void chkIncludeAllCompany_CheckedChanged(object sender, EventArgs e)
    {
        BindEvaluators(CompanyID);


    }
}



