﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PerformanceView.ascx.cs" Inherits="Controls_PerformanceView" %>
<div id="popupmain1" style="width: 1000px; height: auto; overflow: auto">
                <div id="header11" style ="width:100%">
                    <div id="hbox1">
                        <div id="headername">
                            <asp:Label ID="lblHeading" runat="server" Text="Performance Initiation"></asp:Label>
                            
                            <%-- <asp:Label ID="lblName" runat="server" Text="Performance Initiation"></asp:Label>--%>
                        </div>
                    </div>
                    <div id="hbox2">
                        <div id="close1">
                            <a href="">
                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                     CausesValidation="true" ValidationGroup="dummy_not_using" />
                            </a>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; overflow: hidden;">
                    <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <div style="display: block">
                             
                                
                                <div style ="min-width :100%;overflow:auto" >
                                <asp:DataGrid ID="dgv"  CssClass ="trLeft" runat ="server" BackColor="White"   BorderColor="Black" 
                                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3"  Width="100%" >
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" 
                                        Mode="NumericPages" />
                                    <ItemStyle ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    
                                    <HeaderStyle   Font-Bold ="false"  CssClass ="trLeft" />
                                </asp:DataGrid>
                                
                                </div>
                                
                       
                                
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="clear: both;height:100%" id="footerwrapper">
                    <div id="footer11" style ="width:100%">
                        <div id="buttons">
                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 35%;">
                                        <asp:Label ID="lblWarning" runat="server"></asp:Label>
                                    </div>
                                    <div style="float: right;" align="left">
                                        &nbsp;
                                        </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>