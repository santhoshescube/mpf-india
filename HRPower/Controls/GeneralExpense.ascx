﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GeneralExpense.ascx.cs"
    Inherits="Controls_GeneralExpense" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/default_common.css" rel="stylesheet" type="text/css" />
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<%--<script type="text/javascript">

//    function closThis(elementID) {
//        var e = document.getElementById(elementID);

//        if (e) {
//            e.style.display = 'none';
//            return false;
//        }
//    }

</script>--%>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 886px;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="HRPower"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div id="toolbar">
            <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div style="float: left; width: 100%;">
                        <div id="imgbuttonss">
                            <asp:ImageButton ID="btnAddNew" CssClass="imgbuttons" runat="server" meta:resourcekey="AddNew"
                                CausesValidation="False" ImageUrl="~/images/Plus.png" OnClick="btnAddNew_Click" />
                            <asp:ImageButton ID="btnSaveData" CssClass="imgbuttons" runat="server" meta:resourcekey="SaveData"
                                ValidationGroup="Submit" ImageUrl="~/images/Save%20Blue.png" OnClick="btnSaveData_Click" />
                            <asp:ImageButton ID="btnClear" CssClass="imgbuttons" runat="server" CausesValidation="False"
                                meta:resourcekey="Clear" ImageUrl="~/images/clear.png" OnClick="btnClear_Click" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="contentwrap">
                    <div id="content">
                        <fieldset>
                            <div style="float: left; width: 100%; height: 30px;">
                                <div style="float: left; width: 50%;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,Company%>'></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName" DataValueField="CompanyID"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="200px" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddlCompanyName" runat="server" CssClass="error"
                                            ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlCompany" ValidationGroup="submit"
                                            InitialValue="-1"></asp:RequiredFieldValidator></div>
                                </div>
                                <div style="float: left; width: 50%;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="ExpenseNumber"></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:TextBox ID="txtExpenseNumber" MaxLength="30" runat="server" CssClass="textbox_mandatory"
                                            Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvExpenseNumber" runat="server" ErrorMessage="*"
                                            ControlToValidate="txtExpenseNumber" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; height: 30px;">
                                <div style="float: left; width: 50%;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="ExpenseCategory"></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:DropDownList ID="ddlExpenseCategory" DataTextField="ExpenseCategory" DataValueField="ExpenseCategoryID"
                                            OnSelectedIndexChanged="ddlExpenseCategory_SelectedIndexChanged" runat="server"
                                            Width="200px" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddlExpenseCategory" runat="server" CssClass="error"
                                            ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlExpenseCategory" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 50%;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="ExpenseDate"></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:TextBox ID="txtExpenseDate" runat="server" CssClass="textbox_mandatory" Width="100px"></asp:TextBox>&nbsp;
                                        <asp:ImageButton ID="ibtnExpenseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CausesValidation="false" CssClass="imagebutton" />
                                        <AjaxControlToolkit:CalendarExtender ID="cextExpenseDate" runat="server" TargetControlID="txtExpenseDate"
                                            Format="dd/MM/yyyy" PopupButtonID="ibtnExpenseDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:CustomValidator ID="cvtxtExpenseDate" runat="server" CssClass="error" ControlToValidate="txtExpenseDate"
                                            Display="Dynamic" ValidationGroup="submit" ClientValidationFunction="CheckFuturedate"></asp:CustomValidator>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; height: 30px;">
                                <div style="float: left; width: 50%;">
                                    <asp:UpdatePanel ID="updVacancy" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="float: left; width: 30%">
                                                <asp:Label ID="lblVacancy" runat="server"></asp:Label>
                                            </div>
                                            <div style="float: right; width: 70%">
                                                <asp:DropDownList ID="ddlVacancy" runat="server" Width="200px" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvVacancy" runat="server" CssClass="error" Display="Dynamic"
                                                    ControlToValidate="ddlVacancy" InitialValue="-1"></asp:RequiredFieldValidator>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="float: left; width: 50%;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="ExpenseAmount"></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:TextBox ID="txtExpenseAmount" runat="server" MaxLength="7" Width="10%" Text="0"></asp:TextBox>
                                        &nbsp;
                                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeExpenseAmount" runat="server"
                                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789." TargetControlID="txtExpenseAmount">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; height: 30px;">
                                <div style="float: left; width: 50%;">
                                    <asp:UpdatePanel ID="updExpenseType" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="float: left; width: 30%">
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="ExpenseType"></asp:Literal>
                                            </div>
                                            <div style="float: right; width: 70%">
                                                <asp:DropDownList ID="ddlExpenseType" runat="server" Width="200px" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:Button ID="btnExpenseType" runat="server" CausesValidation="False" CommandName="ExpenseType"
                                                    CssClass="referencebutton" OnClick="btnExpenseType_Click" Text="..." />
                                                <asp:RequiredFieldValidator ID="rfvExpenseType" runat="server" CssClass="error" Display="Dynamic"
                                                    ControlToValidate="ddlExpenseType" InitialValue="-1"></asp:RequiredFieldValidator>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div style="float: left; width: 50%; height: 47px;">
                                    <div style="float: left; width: 30%">
                                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                    </div>
                                    <div style="float: right; width: 70%">
                                        <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                                            onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                                            TextMode="MultiLine" Height="50px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnSave" runat="server" CssClass="popupbutton" OnClick="btnSave_Click"
                                meta:resourcekey="Save" ValidationGroup="Submit" />
                        </fieldset>
                        <div style="width: 866px">
                            <fieldset style="width: 851px">
                                <div style="max-height: 226px; width: 862px; overflow: auto;">
                                    <asp:GridView ID="dgvGEReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                        HorizontalAlign="Center" Width="100%" CssClass="labeltext" Height="185px">
                                        <HeaderStyle CssClass="datalistheader" Height="28px" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Company" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCompany" runat="server" Text='<%# Eval("CompanyName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="26%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="datalistTrLeft" HeaderText="Expense Category"
                                                ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCategory" runat="server" Text='<%# Eval("ExpenseCategory") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="15%" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expense Type" ItemStyle-CssClass="datalistTrLeft"
                                                ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("ExpenseType") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="13%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="datalistTrLeft" HeaderText="Expense Number"
                                                ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNumber" runat="server" Text='<%# Eval("ExpenseNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="16%" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="datalistTrLeft" HeaderText="Expense Date"
                                                ItemStyle-VerticalAlign="Top" ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("ExpenseDate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="12%" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="datalistTrLeft" HeaderText="Amount" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("ExpenseAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="0%" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="datalistTrLeft" HeaderText="Remarks" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="55%" ItemStyle-Wrap="true">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("Remarks") %>' Style='word-break: break-all'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="30%" Wrap="True" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("ExpenseID") %>'
                                                        OnClientClick="return true" OnClick="ImgEdit_Click" meta:resourcekey="Edit" ImageUrl="~/images/edit_popup.png" />
                                                </ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                                ItemStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("ExpenseID") %>'
                                                        meta:resourcekey="Delete" ImageUrl="~/images/delete_popup.png" CausesValidation="false"
                                                        OnClick="ImgDelete_Click" />
                                                    <AjaxControlToolkit:ConfirmButtonExtender ID="CBE1" runat="server" TargetControlID="ImgDelete"
                                                        meta:resourcekey="Areyousuretodelete">
                                                    </AjaxControlToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                                <ItemStyle Width="5%" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </fieldset>
                        </div>
                        <div style="display: none">
                            <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                            <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                        </div>
                        <%-- <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btn" Text="" runat="server" />
                        </div>
                        <div id="pnlModalPopUp" runat="server" style="display: none;">
                            <uc:ReferenceNew ID="ReferenceNew1" runat="server" />
                        </div>
                        <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                            PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>--%>
                    </div>
                </div>
                <div id="footerwrapper">
                    <div id="footer11" style="height: 40px;">
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="false"></asp:Label>
                        <div id="buttons">
                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 45%;">
                                        <asp:Label ID="lblWarning" runat="server"></asp:Label>
                                    </div>
                                    <div style="float: right;" align="left">
                                        <asp:Button ID="btnOK" ValidationGroup="Submit" runat="server" CssClass="popupbutton"
                                            OnClick="btnOK_Click" meta:resourcekey="Ok" Width="60" />&nbsp;
                                        <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Width="60" OnClick="btnCancel_Click"
                                            meta:resourcekey="Cancel" CausesValidation="False" />
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <%-- <asp:AsyncPostBackTrigger ControlID="ibtnClose" EventName="Click" />--%>
                                    <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
