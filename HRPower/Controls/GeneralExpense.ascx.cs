﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Data.SqlClient;
using System.Reflection;


public partial class Controls_GeneralExpense : System.Web.UI.UserControl
{
    #region Declarations

    clsGeneralExpenseReference objGE = new clsGeneralExpenseReference();
    public delegate void OnUpdate();
    public event OnUpdate Update;

    string sModalPopupID;
    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    private string CurrentSelectedValue {  get; set; }

    #endregion Declarations


    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatusMessage.Text = string.Empty;
        lblStatusMessage.Visible = false;

        if (clsGlobalization.IsArabicCulture())
        {            
            lblStatusMessage.Text = string.Empty;
        }
        else
        {
            lblStatusMessage.Text = string.Empty;
        }
        if (!IsPostBack)
        {
            //    Clear();
            Bindcombos();
        }

        //ReferenceNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        //ReferenceNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }
       
    private void Bindcombos()
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");

        //Binds company details
        ddlCompany.DataSource = clsGeneralExpenseReference.BindCompany();
        ddlCompany.DataBind();
        ddlCompany.Items.Insert(0, new ListItem(str, "-1"));        
    }
    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
    protected void btnOK_Click(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }

    protected void ImgEdit_Click(object sender, ImageClickEventArgs e)
    {
        hfMode.Value = "Update";
        lblStatusMessage.Text = "";
        btnSave.Enabled = true;
        string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
        ViewState["id"] = Id;
        Edit(Convert.ToInt32(Id));
    }

    private void Edit(int ExpenseID)
    {       
        using (DataTable dt = clsGeneralExpenseReference.EditExpense(ExpenseID))
        {
            if (dt.Rows.Count > 0)
            {
                ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
                ddlExpenseCategory.SelectedValue = dt.Rows[0]["ExpenseCategoryID"].ToString();
                FillExpenseType();
                if (dt.Rows[0]["ExpenseTypeID"].ToInt32() > 0)
                {
                    ddlExpenseType.SelectedValue = dt.Rows[0]["ExpenseTypeID"].ToString();
                    lblVacancy.Text = "Select Expense Type";
                    ddlVacancy.Enabled = false;
                }
                else
                {
                    ddlExpenseType.Enabled = false;
                    btnExpenseType.Enabled = false;
                    ddlVacancy.Enabled = true;
                    ddlVacancy.SelectedValue = dt.Rows[0]["ReferenceID"].ToString();
                    lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                }
                
                txtExpenseNumber.Text = dt.Rows[0]["ExpenseNumber"].ToString();
                txtExpenseDate.Text = dt.Rows[0]["ExpenseDate"].ToString();
                txtExpenseAmount.Text = dt.Rows[0]["ExpenseAmount"].ToString();
                txtRemarks.Text = dt.Rows[0]["remarks"].ToString();
            }
            //upnlAdd.Update();
            upnlReference.Update();
        }
    }

    protected void ImgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //lblStatusMessage.Text = "";
            //btnSave.Enabled = true;
            //string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
            //objCriteria = new clsCriteriaReference();
            //objCriteria.CriteriaId = Id.ToInt32();
            //if (objCriteria.IsCriteriaUsed() > 0)
            //{
            //    lblStatusMessage.Visible = true;
            //    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف المعايير، وجود المرجعية.") : ("Cannot delete criteria, reference exists."); //"Cannot delete criteria, reference exists.";
            //    return;
            //}
            //else
            //{
            //    DeleteCriteria(Convert.ToInt32(Id));

            //    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير حذف بنجاح.") : ("Criteria deleted successfully."); //"Criteria deleted successfully.";
            //    lblStatusMessage.Visible = true;

            //    BindDataList();
            //    Clear();
            //}
        }
        catch (SqlException)
        {
            lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف المعايير، وجود المرجعية.") : ("Cannot delete criteria, reference exists."); //"Cannot delete criteria, reference exists.";
            lblStatusMessage.Visible = true;
        }
    }

    /// <summary>
    /// For binding ExpenseType dropdown based on Company selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        BindExpenseCategory();

        objGE.CompanyID = ddlCompany.SelectedValue.ToInt32();
        BindCurrency(ddlCompany.SelectedValue.ToInt32());
        DataTable dt = clsGeneralExpenseReference.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32(),
                                                                  ddlCompany.SelectedValue.ToInt32(),
                                                                  ddlExpenseType.SelectedValue.ToInt32());
        if (dt.Rows.Count > 0)
        {
            if (ddlExpenseCategory.SelectedValue == "1")
            {
                ddlExpenseType.DataSource = dt;
                ddlExpenseType.DataValueField = dt.Columns[0].ToString();
                ddlExpenseType.DataTextField = dt.Columns[1].ToString();
                ddlExpenseType.DataBind();
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
            }
            else
            {
                ddlVacancy.DataSource = dt;
                ddlVacancy.DataValueField = dt.Columns[0].ToString();
                ddlVacancy.DataTextField = dt.Columns[1].ToString();
                ddlVacancy.DataBind();
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
            }
        }
        else
        {
            ddlExpenseType.DataSource = dt;
            ddlExpenseType.DataBind();
            ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
            DataTable dtclear = new DataTable();
            ddlVacancy.DataSource = dtclear;
            ddlVacancy.DataBind();
            ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
        }
        updExpenseType.Update();
        updVacancy.Update();
        //upnlAdd.Update();
        upnlReference.Update();
    }

    private void BindCurrency(int CompanyID)
    {
        string Currency = objGE.GetCurrency(CompanyID);
        lblCurrency.Text = "( " + Currency + ")";
    }

    private void BindExpenseCategory()
    {
        //Binds Expense Categories
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlExpenseCategory.DataSource = clsGeneralExpenseReference.BindExpenseCategory();
        ddlExpenseCategory.DataBind();
        ddlExpenseCategory.Items.Insert(0, new ListItem(str, "-1"));
    }

    /// <summary>
    /// For binding ExpenseType dropdown based on Category selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlExpenseCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");

        if (ddlExpenseCategory.SelectedValue.ToInt32() == -1)
        {
            ddlExpenseType.Enabled = btnExpenseType.Enabled = false;
            ddlVacancy.Enabled = true;
            lblVacancy.Text = "Select Expense Type";
        }
        else
        {
            ddlExpenseType.Enabled = true;
            if (ddlExpenseCategory.SelectedValue.ToInt32() == 1)
            {
                lblVacancy.Text = "Select Expense Type";
                btnExpenseType.Enabled = ddlExpenseType.Enabled = true;
                ddlVacancy.Enabled = false;
                rfvExpenseType.ValidationGroup = "submit";
                rfvExpenseType.ErrorMessage = "*";
                rfvVacancy.ValidationGroup = "";
                rfvVacancy.ErrorMessage = "";
            }
            else
            {
                lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                btnExpenseType.Enabled = ddlExpenseType.Enabled = false;
                ddlVacancy.Enabled = true;
                rfvVacancy.ValidationGroup = "submit";
                rfvVacancy.ErrorMessage = "*";
                rfvExpenseType.ValidationGroup = "";
                rfvExpenseType.ErrorMessage = "";
            }

            objGE.CompanyID = ddlCompany.SelectedValue.ToInt32();
            DataTable dt = clsGeneralExpenseReference.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32(),
                                                                  ddlCompany.SelectedValue.ToInt32(),
                                                                  ddlExpenseType.SelectedValue.ToInt32());
            if (dt.Rows.Count > 0)
            {
                if (ddlExpenseCategory.SelectedValue.ToInt32() == 1)
                {
                    ddlExpenseType.DataSource = dt;
                    ddlExpenseType.DataValueField = dt.Columns[0].ToString();
                    ddlExpenseType.DataTextField = dt.Columns[1].ToString();
                    ddlExpenseType.DataBind();
                    ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
                    ddlVacancy.DataSource = null;
                    ddlVacancy.DataBind();
                }
                else
                {
                    ddlVacancy.DataSource = dt;
                    ddlVacancy.DataValueField = dt.Columns[0].ToString();
                    ddlVacancy.DataTextField = dt.Columns[1].ToString();
                    ddlVacancy.DataBind();
                    ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
                    ddlExpenseType.DataSource = null;
                    ddlExpenseType.DataBind();
                }
            }
            else
            {
                DataTable dtclear = new DataTable();
                ddlExpenseType.DataSource = dtclear;
                ddlExpenseType.DataBind();
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
                ddlVacancy.DataSource = dtclear;
                ddlVacancy.DataBind();
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
            }
            updExpenseType.Update();
            updVacancy.Update();
        }
        upnlReference.Update();
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {        
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        mpe.Hide();
    }

    private void Clear()
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        
        DataTable dtclear = new DataTable();
        ddlExpenseType.DataSource = dtclear;
        ddlExpenseType.DataBind();
        ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));

        ddlVacancy.DataSource = dtclear;
        ddlVacancy.DataBind();
        ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));

        ddlExpenseCategory.DataSource = dtclear;
        ddlExpenseCategory.DataBind();
        ddlExpenseCategory.Items.Insert(0, new ListItem(str, "-1"));

        lblVacancy.Text = "Select Expense Type";
        ddlCompany.SelectedIndex = ddlExpenseCategory.SelectedIndex = ddlExpenseType.SelectedIndex = ddlVacancy.SelectedIndex = -1;
        txtExpenseDate.Text = txtExpenseNumber.Text = txtRemarks.Text = lblCurrency.Text = string.Empty;
        rfvVacancy.ValidationGroup = rfvExpenseType.ValidationGroup = "";
        txtExpenseAmount.Text = "0";  
    }

    public void BindDataList()
    {
        dgvGEReference.DataSource = clsGeneralExpenseReference.GetGeneralExpense();
        dgvGEReference.DataBind();
        upnlReference.Update();
    }

    /// <summary>
    /// Loads the reference control for General ExpenseType
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExpenseType_Click(object sender, EventArgs e)
    {
        //if (ddlExpenseType != null)
        //{
        //    ReferenceNew1.ClearAll();
        //    ReferenceNew1.TableName = "HRExpenseTypeReference";
        //    ReferenceNew1.DataTextField = "ExpenseType";
        //    ReferenceNew1.DataTextFieldArabic = "ExpenseTypeArb";
        //    ReferenceNew1.DataValueField = "ExpenseTypeID";
        //    ReferenceNew1.FunctionName = "FillExpenseType";
        //    ReferenceNew1.SelectedValue = ddlExpenseType.SelectedValue;
        //    ReferenceNew1.DisplayName = GetLocalResourceObject("ExpenseType.Text").ToString();
        //    ReferenceNew1.PopulateData();

        //    mdlPopUpReference.Show();
        //    updModalPopUp.Update();
        //}
    }

    //void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    //{
    //    CurrentSelectedValue = SelectedValue;
    //    Type thisType = this.GetType();
    //    MethodInfo theMethod = thisType.GetMethod(functionName);
    //    theMethod.Invoke(this, null);
    //}

    //void ReferenceControlNew1_onRefresh()
    //{
    //    mdlPopUpReference.Show();
    //}

    public void FillExpenseType()
    {
        objGE.CompanyID = ddlCompany.SelectedValue.ToInt32();
        DataTable dtTemp = clsGeneralExpenseReference.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32(),
                                                                  ddlCompany.SelectedValue.ToInt32(),
                                                                  ddlExpenseType.SelectedValue.ToInt32());

        if (dtTemp.Rows.Count > 0)
        {
            if (ddlExpenseCategory.SelectedValue == "1")
            {
                lblVacancy.Text = "Select Expense Type";
                ddlExpenseType.DataSource = dtTemp;
                ddlExpenseType.DataTextField = dtTemp.Columns[1].ToString();
                ddlExpenseType.DataValueField = dtTemp.Columns[0].ToString();
                ddlExpenseType.DataBind();
                string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));

                this.SetSelectedIndex(this.ddlExpenseType);
                updExpenseType.Update();
            }
            else
            {
                lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                ddlVacancy.DataSource = dtTemp;
                ddlVacancy.DataTextField = dtTemp.Columns[1].ToString();
                ddlVacancy.DataValueField = dtTemp.Columns[0].ToString();
                ddlVacancy.DataBind();
                string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));

                this.SetSelectedIndex(this.ddlVacancy);
                updVacancy.Update();
            }
        } upnlReference.Update();
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

}
