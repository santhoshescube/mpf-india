﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddUser.ascx.cs" Inherits="Controls_AddUser" %>
<div id="popupmain1" style="width: 42%; height: auto; overflow: auto; background-color: White">
    <div id="header11" style="width: 100%">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" meta:resourcekey="AssignEmployees"></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                        OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <div style="width: 100%; overflow: hidden;">
    <asp:UpdatePanel ID="upd" runat ="server" >
    <ContentTemplate>
    
    
        <div style="display: block; border: solid 1px black">
        <%--
          <div class="firstTrLeft" style="float: left; width: 15%; height: 29px">
                <asp:Label ID="Label5" runat="server" Text="Company" CssClass="labeltext"></asp:Label>
            </div>
            <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                <asp:DropDownList ID="ddlCompany" AutoPostBack="true" Width="200px" runat="server" CssClass="textbox_mandatory"
                    MaxLength="25"
                    onselectedindexchanged="ddlCompany_SelectedIndexChanged">
                </asp:DropDownList>
                
            
               
              </div>--%>    <asp:HiddenField ID="hfUserID" runat="server" />
                
            <div style="clear: both">
        
        </div>
        
            <div class="firstTrLeft" style="float: left; width: 15%; height: 29px">
                <asp:Label ID="Label2" runat="server" Text="Employee" CssClass="labeltext"></asp:Label>
            </div>
            <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                <asp:DropDownList ID="ddlEmployee" AutoPostBack ="true" Width="200px" runat="server" CssClass="textbox_mandatory"
                    MaxLength="25" onselectedindexchanged="ddlEmployee_SelectedIndexChanged">
                </asp:DropDownList>
                
                 <asp:RequiredFieldValidator ErrorMessage ="Please select an employee" ID="rfEmployee" runat ="server" InitialValue ="-1" ValidationGroup ="Save"
                Display="None" ControlToValidate ="ddlEmployee">
                </asp:RequiredFieldValidator>
              
            </div>
            <div style="clear: both">
                <div class="firstTrLeft" style="float: left; width: 15%; height: 29px">
                    <asp:Label ID="Label1" runat="server" Text="UserName" CssClass="labeltext"></asp:Label>
                </div>
                <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                    <asp:TextBox ID="txtUserName" Width ="192px" runat="server"></asp:TextBox>
                    
                        <asp:RequiredFieldValidator ErrorMessage ="Please enter user name" ID="RequiredFieldValidator2" runat ="server"   ValidationGroup ="Save"
                Display="None" ControlToValidate ="txtUserName">
                </asp:RequiredFieldValidator>
                    
                    
                </div>
                <div style="clear: both">
                </div>
                <div class="firstTrLeft" style="float: left; width: 15%; height: 29px">
                    <asp:Label ID="Label3" runat="server" Text="Password" CssClass="labeltext"></asp:Label>
                </div>
                <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                    <asp:TextBox ID="txtPassword" Width ="192px" runat="server" 
                        TextMode="Password" ></asp:TextBox>
                    
                    
                    
                    
                        <asp:RequiredFieldValidator ErrorMessage ="Please enter password" ID="RequiredFieldValidator3" runat ="server"   ValidationGroup ="Save"
                Display="None" ControlToValidate ="txtPassword">
                </asp:RequiredFieldValidator>
                </div>
                <div style="clear: both">
                    <div style="overflow: auto">
                        <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" 
                            ID="dlRoleDetails" Width="100%"
                            runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                            CellPadding="3" GridLines="Both" 
                            onitemdatabound="dlRoleDetails_ItemDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <ItemStyle ForeColor="#000066" />
                            <SeparatorStyle BackColor="AliceBlue" />
                            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <HeaderTemplate>
                                <table style="width: 90%">
                                    <tr>
                                     <td style=" overflow: hidden; vertical-align: top">
                                            <asp:Label ID="Label4" runat="server" Text="Company"></asp:Label>
                                        </td>
                                        
                                        <td style="width:45%; overflow: hidden; vertical-align: top">
                                            <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>
                                        </td>
                                       
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <ItemTemplate>
                                <table style="width: 100%; height: 30px;">
                                    <tr>
                                    
                                    <td style=" overflow: hidden; vertical-align: top">
                                            <asp:DropDownList ID="ddlCompany" Width ="200px" runat="server">
                                            </asp:DropDownList>
                                            
                                            <div style ="clear:both">
                                            </div>
                                              <asp:RequiredFieldValidator ID="rfvCompanyId" runat ="server" 
                                                InitialValue ="-1" ErrorMessage ="Please select a company" Display ="Dynamic" ControlToValidate ="ddlCompany" ValidationGroup ="Add">
                                                </asp:RequiredFieldValidator>
                                                
                                            <asp:HiddenField ID="hfCompanyID" runat ="server" 
                                                Value='<%# Eval("CompanyID") %>' />
                                        </td>
                                        <td style="width:50%;overflow: hidden; vertical-align: top">
                                            <asp:DropDownList ID="ddlRole" Width ="200px" runat="server">
                                            </asp:DropDownList>
                                            
                                            <asp:HiddenField ID="hfRoleID"  runat ="server" Value='<%# Eval("RoleID") %>' />
                                            
                                              <asp:HiddenField ID="hfCurrentCompanyID"  runat ="server" Value='<%# Eval("CurrentCompany") %>' />

                                            
                                            <asp:Button ID="btnAdd" Width ="22px" ValidationGroup="Add"
                                                style="margin:0px;padding:0px;margin-left:10px" runat ="server" Text ="+" 
                                                onclick="btnAdd_Click" />
                                                
                                                
                                                  <asp:Button ID="btnDelete" Width ="22px" 
                                                style="margin:0px;padding:0px;margin-left:10px" runat ="server" Text ="x" 
                                                onclick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' />
                                                
                                                
                                                
                                                
                                              
                                                
                                                
                                                  
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat ="server" 
                                                InitialValue ="-1" ErrorMessage ="Please select a role" Display ="Dynamic" ControlToValidate ="ddlRole" ValidationGroup ="Add">
                                                </asp:RequiredFieldValidator>
                                                
                                                
                                          <%--      <asp:ValidationSummary ID="vs" runat ="server" 
                                                  ShowMessageBox ="true"  ShowSummary ="false" ValidationGroup ="Add" />--%>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <div id="dvNoEmployee" runat="server" visible="false">
                            <asp:Label ID="lblNoEmployee" runat="server" Text="" CssClass="error"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both;" id="footerwrapper">
                <div id="footer11" style="width: 100%">
                    <div style="float: right; margin: 10px;">
                        <asp:Button ID="btnsave" ValidationGroup ="Save" CssClass="btnsubmit" runat="server" Text ="Save"
                            OnClick="save_Click" />
                            
                            
                            
                            
                           <asp:ValidationSummary ID="vs" runat ="server" ValidationGroup ="Save"
                            ShowSummary ="false" ShowMessageBox ="true" />
                           
                            
                           
                    </div>
                </div>
            </div>
        </div>
        
        </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
</div>
