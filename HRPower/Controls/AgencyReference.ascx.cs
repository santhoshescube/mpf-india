﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Reflection;

public partial class Controls_AgencyReference : System.Web.UI.UserControl
{
    #region Declarations
    //Object Instantiation 
    clsAgencyReference objAgency;

    public delegate void OnUpdate(DataTable dt);    
    public event OnUpdate OnSave;
 
    string sModalPopupID;
    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }
  
    #endregion Declarations

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatusMessage.Text = string.Empty;
        lblStatusMessage.Visible = false;
        if (clsGlobalization.IsArabicViewEnabled())
        {
            if (clsGlobalization.IsArabicCulture())
            {
                rfvEng.ValidationGroup = rfvArb.ValidationGroup = "Submit";

            }
        }
        else
        {
            rfvArb.ValidationGroup = "";
            rfvEng.ValidationGroup = "Submit";
            rfvArb.ErrorMessage = "";
        }
    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {
        hfMode.Value = "Insert";

        Clear();
        lblStatusMessage.Text = string.Empty;
        lblStatusMessage.Visible = false;
        BindDataList();
    }

    protected void ImgEdit_Click(object sender, ImageClickEventArgs e)
    {
        hfMode.Value = "Update";
        lblStatusMessage.Text = "";
        btnSave.Enabled = true;
        string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
        ViewState["id"] = Id;
        Edit(Convert.ToInt32(Id));
    }

    protected void ImgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            lblStatusMessage.Text = "";
            btnSave.Enabled = true;
            string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
            objAgency = new clsAgencyReference();
            objAgency.AgencyID = Id.ToInt32();
            if (objAgency.IsAgencyUsed() > 0)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوكالة؛ جود مرجعية.") : ("Cannot delete Agency; reference exists."); //"Cannot delete Agency; reference exists.";
                return;

            }
            else
            {
                DeleteAgency(Convert.ToInt32(Id));

                if (OnSave != null)
                {
                    OnSave(objAgency.GetAgency());
                }

                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("وكالة حذف بنجاح.") : ("Agencys deleted successfully."); //"Agencys deleted successfully.";
                lblStatusMessage.Visible = true;

                BindDataList();
                Clear();
            }
        }
        catch (SqlException)
        {
            lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوكالة؛ جود مرجعية.") : ("Cannot delete Agency; reference exists."); //"Cannot delete Agency; reference exists.";
            lblStatusMessage.Visible = true;
        }
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        objAgency = new clsAgencyReference();
        if (OnSave != null)
        {
            OnSave(objAgency.GetAgency());
        }
        Clear();
        mpe.Hide();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        AddAgency();
        BindDataList();
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AddAgency();
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (OnSave != null)
        {
            OnSave(objAgency.GetAgency());
        }
        Clear();
        mpe.Hide();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        objAgency = new clsAgencyReference();
        if (OnSave != null)
        {
            OnSave(objAgency.GetAgency());
        }
        Clear();
        mpe.Hide();
    }

    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        AddAgency();        
    }

    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {
        Clear();
    }

    #endregion Events

    #region Methods

    private void AddAgency()
    {
        objAgency = new clsAgencyReference();
        objAgency.Agency = txtAgencyName.Text.Trim();
        objAgency.AgencyArb = txtAgencyNameArb.Text.Trim();
        objAgency.Address = txtAddress.Text.Trim();
        objAgency.Email = txtEmail.Text.Trim();
        objAgency.Telephone = txtTelephone.Text.Trim();
        if (txtAddress.Text == "" || txtEmail.Text == "" || txtTelephone.Text == "")
        {
            lblStatusMessage.Visible = true;
            lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("إدخال كافة القيم في جميع الحقول.") : ("Enter all values into all Fields."); //"Enter all values into all Fields.";
        }
        else
        {
            if (hfMode.Value == "Insert")
            {
                if (objAgency.CheckAgencyNameExistence() > 0)
                {
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("وكالة يحمل هذا الرقم البريد الإلكتروني موجود.") : ("Agency with this Email ID Exists."); //"Agency with this Email ID Exists.";
                }
                else
                {
                    objAgency.AddAgencyReference();
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("وأضاف كالة بنجاح.") : ("Agency added successfully."); //"Agency added successfully.";
                    Clear();
                }
            }
            else
            {
                objAgency.AgencyID = Convert.ToInt32(ViewState["id"]);
                if (objAgency.CheckAgencyNameExistence() > 0)
                {
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("وكالة يحمل هذا الرقم البريد الإلكتروني موجود.") : ("Agency with this Email ID Exists."); //"Agency with this Email ID Exists.";
                }
                else
                {
                    
                    objAgency.UpdateAgencyReference();

                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث كالة بنجاح.") : ("Agency updated successfully."); //"Agency updated successfully.";
                    Clear();
                }
            }
        }
    }

    private void Clear()
    {
        txtAgencyName.Text = string.Empty;
        txtAgencyNameArb.Text = string.Empty;
        txtAddress.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtTelephone.Text = string.Empty;

        btnOK.Enabled = btnSave.Enabled = btnSaveData.Enabled = true;
        //lblWarning.Text = string.Empty;

        hfMode.Value = "Insert";
    }

    public void BindDataList()
    {
        objAgency = new clsAgencyReference();
        dgvReference.DataSource = objAgency.GetAgency();
        dgvReference.DataBind();
        
    }

    private void DeleteAgency(int Id)
    {
        objAgency = new clsAgencyReference();
        objAgency.AgencyID = Id;

        objAgency.Delete();
    }

    private void Edit(int Id)
    {
        objAgency = new clsAgencyReference();
        objAgency.AgencyID = Id;
        using (DataTable dt = objAgency.GetDataById())
        {
            if (dt.Rows.Count > 0)
            {
                txtAgencyName.Text = dt.Rows[0]["Agency"].ToString();
                txtAgencyNameArb.Text = dt.Rows[0]["AgencyArb"].ToString();
                txtAddress.Text = dt.Rows[0]["Address"].ToString();
                txtEmail.Text = dt.Rows[0]["Email"].ToString();
                txtTelephone.Text = dt.Rows[0]["Telephone"].ToString();
            }
        }
    }


   


    #endregion Methods
}
