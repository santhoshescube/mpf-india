﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using BasicPrimitives.OrgDiagram;
using System.Data;

public partial class Controls_HierarchyViewControl : System.Web.UI.UserControl
{

   private List<ReportingToHierarchy> ReportingToList { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    { // only bind the data on the first page load...
        //if (!Page.IsPostBack)
        //{
        //    //LoadReportingToHierarchy();

        //    //LoadOrganisationHierarchy();
        //}
        //orgDiagram.Width = ao;
      
       
    }



    public void ShowHierarchy(eType Type)
    {
       // dvDetails.Visible = (Type == eType.OrganizationHierarchy);
        if (Type == eType.OrganizationHierarchy)
        {
            LoadOrganisationHierarchy();
            lblHeading.Text = "Organization Hierarchy";
        }
        else
        {
            LoadReportingToHierarchy();
            lblHeading.Text = "Employee Hierarchy";
        }
    }

    private void LoadReportingToHierarchy()
    {
        this.orgDiagram.Items.Clear();

        ReportingToList = clsHieararchy.GetHierarchyByReportingTo();
        //Item Parent = new Item("Test","Test", "", "../img/parent.png");

        //Parent.TitleColor = Color.Red;
        //Parent.ShowCheckBox = Enabled.False; ;
        //Parent.ChildrenPlacementType = ChildrenPlacementType.Auto;
        //Parent.AdviserPlacementType = AdviserPlacementType.Auto;
        //this.orgDiagram.Items.Add(Parent);

        var MainReportingTo = ReportingToList.Where(t => t.ReportingTo == 0);

        foreach (ReportingToHierarchy objReportingTo in MainReportingTo)
        {
            Item Child = new Item(objReportingTo.EmployeeFirstName.ToString(), objReportingTo.EmployeeID.ToString(), objReportingTo.Designation, GetLogo(objReportingTo.EmployeeID, 100));
            Child.TitleColor = Color.Tan;
            Child.ShowCheckBox = Enabled.False; ;
            //Child.ChildrenPlacementType = ChildrenPlacementType.Auto;
            //Child.AdviserPlacementType = AdviserPlacementType.Auto;
            AddReportingToChild(Child, objReportingTo.EmployeeID.ToInt32());
            //Parent.Items.Add(Child);
            Child.Selected = true;
            this.orgDiagram.Items.Add(Child);
            //orgDiagram.ChildrenPlacementType = ChildrenPlacementType.Matrix ;
            
        }

       // orgDiagram.PageFitMode = PageFitMode.FitToPage  ;
       // orgDiagram.AutoSizeOnWindowSize = false;

        //orgDiagram.ControlStyle.BackColor = Color.Green;
        //orgDiagram.ControlStyle.Width = new Unit(1000);

        
    }


    private void LoadOrganisationHierarchy()
    {
        this.orgDiagram.Items.Clear();

        DataTable dt = clsHieararchy.GetHierarchy(0);
        DataTable dt1;

        if (dt.Rows.Count > 0)
        {
            Item Parent = new Item(dt.Rows[0]["Designation"].ToString(), dt.Rows[0]["DesignationID"].ToString(), dt.Rows[0]["Details"].ToString(), "../images/Hierarchy.png");

            Parent.TitleColor = Color.Tan;
           // Parent.GroupTitleColor = Color.SteelBlue;
            Parent.ShowCheckBox = Enabled.False; ;
            Parent.ChildrenPlacementType = ChildrenPlacementType.Auto;
            Parent.AdviserPlacementType = AdviserPlacementType.Auto;
            //this.orgDiagram.Items.Add(Parent);

            dt1 = clsHieararchy.GetHierarchy(dt.Rows[0]["DesignationID"].ToInt32());
           
               
           
            foreach (DataRow dr in dt.Rows)
            {
                Item Child = new Item(dr["Designation"].ToString(), dr["DesignationID"].ToString(), dt.Rows[0]["Details"].ToString(), "../images/Hierarchy.png");
                Child.TitleColor = Color.Tan;
                Child.ShowCheckBox = Enabled.False; ;
                //Child.ChildrenPlacementType = ChildrenPlacementType.Auto;
                //Child.AdviserPlacementType = AdviserPlacementType.Auto;
                AddChild(Child, dr["DesignationID"].ToInt32());

                this.orgDiagram.Items.Add(Child);

               // Parent.Items.Add(Child);


                //Child.ImageUrl = Child.Items.Count > 0 ? "../images/child1.png" : "../images/Hierarchy.png";

                //this.orgDiagram.Items.Add(Child);
            }
        }






        /////* Right Adviser & its team */
        //Item BrianSelby = new Item("Brian Selby", "2", "VP, Global Accounts", "~/images/photos/b.png");
        //BrianSelby.TitleColor = Color.Orange;
        //BrianSelby.ShowCheckBox = Enabled.False;

        //BrianSelby.ChildrenPlacementType = ChildrenPlacementType.Horizontal;
        //MarkKornegay.Items.Add(BrianSelby);




        //Item BrianSelby1 = new Item("Brian Selby", "2", "VP, Global Accounts", "~/images/photos/b.png");
        //BrianSelby1.TitleColor = Color.Orange;
        //BrianSelby1.ShowCheckBox = Enabled.False;
        //BrianSelby1.ChildrenPlacementType = ChildrenPlacementType.Horizontal;
        //MarkKornegay.Items.Add(BrianSelby1);

    }


    private void AddReportingToChild(Item Parent, int EmployeeID)
    {

        var child = ReportingToList.Where(t => t.ReportingTo == EmployeeID);

        foreach (ReportingToHierarchy obj in child)
        {
            Item Child = new Item(obj.EmployeeFirstName.ToString(), obj.EmployeeID.ToString(), obj.Designation, GetLogo(obj.EmployeeID, 100));
            Child.TitleColor = Color.Tan;
            Child.ShowCheckBox = Enabled.False; ;
            Child.ChildrenPlacementType = ChildrenPlacementType.Auto;
            Child.AdviserPlacementType = AdviserPlacementType.Auto;
            Parent.Items.Add(Child);
            AddReportingToChild(Child, obj.EmployeeID.ToInt32());
        }



    }
    private void AddChild(Item Parent, int DesignationID)
    {
        DataTable dt3 = clsHieararchy.GetHierarchy(DesignationID);

        foreach (DataRow dr in dt3.Rows)
        {
            Item Child = new Item(dr["Designation"].ToString(), dr["DesignationID"].ToString(), dr["Details"].ToString(), "../images/Hierarchy.png");
            Child.TitleColor = Color.Tan;
            Child.ShowCheckBox = Enabled.False; ;
            Child.ChildrenPlacementType = ChildrenPlacementType.Auto;
            Child.AdviserPlacementType = AdviserPlacementType.Auto;
            Parent.Items.Add(Child);
            AddChild(Child, dr["DesignationID"].ToInt32());

            //Child.ImageUrl = Child.Items.Count > 0 ? "../images/child1.png" : "../images/Hierarchy.png";
        }



    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }



    private string GetLogo(object oEmployeeID, int width)
    {
        return "../thumbnail.aspx?FromDB=true&type=EmployeeThumbnail&width=" + width + "&EmployeeID=" + Convert.ToInt32(oEmployeeID) + "&t=" + DateTime.Now.Ticks;


    }

    protected void btnClose_Click(object sender, EventArgs e)
    {

    }
}


public enum eType
{
    OrganizationHierarchy = 1,
    EmployeeHierarchy = 2
}