﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttachDocument.ascx.cs"
    Inherits="Controls_AttachDocument" %>
    <script type ="text/javascript">
    
        function filRecentPhoto_UploadedComplete(source, args)
        {
        
        debugger;
            var length = args.get_length();
         
         
       alert('hi');
            var fileupload = $get('ctl00_public_content_fvLeaveRequest_AttachDocument1_FUFile_ctl02');
            
            if(!isValidFile('ctl00_public_content_fvLeaveRequest_AttachDocument1_FUFile_ctl02'))
            {
                alert('Invalid file type');    
                return false;        
            }
            
            return true;

        }
    </script> 
    
<div id="dvMain" runat="server">
    <fieldset>
        <legend><asp:Literal ID="Literal1" runat="server" meta:resourcekey="AttachDocument"></asp:Literal><%--Attach Document--%> </legend>
        <div style="float: left; width: 100%">
            <div id="dvNoInformation" runat="server" visible="false">
                <asp:Label ID="lbl" runat="server" meta:resourcekey="Nodocumentsadded"></asp:Label>
            </div>
            <div id="dvMainDocument" runat="server">
                <div>
                    <div style="float: left; width: 40%">
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AttachDocument"></asp:Literal><%--Document Name--%>
                    </div>
                    <div style="float: left; width: 40%">
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="ChooseFile"></asp:Literal><%--Choose File--%>
                    </div>
                </div>
                <div>
                    <div style="float: left; width: 40%">
                        <asp:TextBox ID="txtDocumentName" MaxLength="60" runat="server"></asp:TextBox>
                    </div>
                    <div style="float: left; width: 40%">
                        <AjaxControlToolkit:AsyncFileUpload Width="100%" ID="FUFile" runat="server" 
                            OnUploadedComplete="FUFile_UploadedComplete" />
                    </div>
                    <div style="float: left; width: 10%">
                        <asp:Button ID="lnkAddtoList" runat="server" Text="+" OnClick="lnkAddtoList_Click" CausesValidation="false">
                        </asp:Button>
                    </div>
                </div>
            </div>
            <div id="dvAttachedDocuments" runat="server">
                <asp:DataList ID="dlAttachedDocuments" runat="server" GridLines="Horizontal" OnItemCommand="dlCandidateDocument_ItemCommand"
                    Width="100%" BorderColor="#33a3d5" OnItemDataBound="dlAttachedDocuments_ItemDataBound">
                    <HeaderStyle CssClass="datalistheader" />
                    <HeaderTemplate>
                        <div style="float: left; width: 100%;">
                            <div style="float: left; width: 70%;" class="trLeft">
                               <asp:Literal ID="Literal3" runat="server" meta:resourcekey="DocumentName"></asp:Literal> <%--Document Name--%>
                            </div>
                            <%--  <div visible ="false"  style="float: left; width: 40%;" class="trLeft">
                                File Name
                            </div>--%>
                            <div style="float: left; width: 15%;" class="trRight">
                                 <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Image"></asp:Literal>  
                            </div>
                            <div style="float: left; width: 05%;" class="trRight">
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="float: left; width: 100%;">
                            <div style="float: left; width: 70%;" class="trLeft">
                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                    Width="70%"></asp:Label>
                            </div>
                            <%--<div visible ="false"  style="float: left; width: 40%;" class="trLeft">
                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="150px"></asp:Label>
                            </div>--%>
                            <div style="float: left; width: 15%;" class="trRight">
                                <asp:ImageButton ID="img" Width="50px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                    CommandArgument='<%# Eval("FileName") %>' OnClick="img_Click" />
                            </div>
                            <div style="float: left; width: 5%;" class="trRight">
                                <asp:ImageButton ID="imgDelete" ImageUrl="~/images/Delete18x18.png" runat="server"
                                    CommandArgument='<%# Eval("FileName")+"@"+Eval("DocumentID") %>' OnClientClick="return confirm('Are you sure you want to delete');"
                                    OnClick="imgDelete_Click" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
    </fieldset>
</div>
