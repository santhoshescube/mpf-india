﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Collections.Generic;

public partial class controls_Report : System.Web.UI.UserControl
{
    private byte[] bytLogo = new byte[]{};
    private ReportParameter[] parameters = new ReportParameter[] { };
    private string sReportPath = string.Empty;
    private IList<ReportDataSource> listDataSource = new List<ReportDataSource>();
    
    // default zoom value
    private int iZoomPercent = 130;

    public string ReportPath { set { this.sReportPath = value; } }

    public int ZoomPercent { set { this.iZoomPercent = value; } }

    public ReportParameter[] ReportParameters { set { parameters = value; } }

    public Microsoft.Reporting.WebForms.ReportViewer ReportViewer { get { return this.rvCommon; } }

    public System.Collections.Generic.IList<ReportDataSource>  ReportDataSourceList { get { return this.listDataSource; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    /// <summary>
    /// Sets DataSouce to report and run it.
    /// </summary>
    /// <param name="sDataSourceName">DataSource Name</param>
    /// <param name="dt">DataSource</param>
    public void Run(string sDataSourceName, DataTable dt)
    {
        ReportDataSource rdsReportData;
        DataTable dtHeaderData = new clsCommon().GetReportHeaderDetails();
        ReportDataSource rdsReportHeader = new ReportDataSource("dsCompany_ReportHeader", dtHeaderData);
        if (dt.Rows.Count > 0)
        {
            rdsReportData = new ReportDataSource(sDataSourceName, dt);
            this.rvCommon.Reset();
            this.rvCommon.LocalReport.ReportPath = sReportPath;
            if(this.parameters.Length > 0)
                this.rvCommon.LocalReport.SetParameters(parameters);
            this.rvCommon.LocalReport.DataSources.Clear();
            this.rvCommon.LocalReport.DataSources.Add(rdsReportData);
            if (this.listDataSource.Count > 0)
            {
                foreach (ReportDataSource rds in listDataSource)
                    this.rvCommon.LocalReport.DataSources.Add(rds);
            }
            this.rvCommon.LocalReport.DataSources.Add(rdsReportHeader);
            this.rvCommon.ZoomMode = ZoomMode.Percent;
            this.rvCommon.ZoomPercent = iZoomPercent;
            this.rvCommon.Height = new Unit(700, UnitType.Pixel);
            this.rvCommon.LocalReport.Refresh();

            this.divMessage.Style["display"] = "none";
            this.rvCommon.Visible = true;
        }
        else
        {
            this.rvCommon.Visible = false;
            this.divMessage.Style["display"] = "block";
        }
    }
}
