﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_DesignationReference : System.Web.UI.UserControl
{
    public delegate void OnPageRefresh();
    public event OnPageRefresh onRefresh;
    public clsReferenceNew objReference;
    public delegate void saveHandler(string SelectedValue);
    public event saveHandler OnSave;
    clsDesignationReference objdesignation;
    clsUserMaster objUserMaster;
    #region Properties
    int mode = 0;

    public string ModalPopupID
    {
        get { return this.hfModalPopupID.Value; }
        set { this.hfModalPopupID.Value = value; }
    }
    public string DataTextField
    {
        get { return hfDataTextField.Value; }
        set { hfDataTextField.Value = value; }
    }
    public string DataValueField
    {
        get { return hfDataValueField.Value; }
        set { hfDataValueField.Value = value; }
    }
    public string TableName
    {
        get { return hfTableName.Value; }
        set { hfTableName.Value = value; }
    }
    public string DataTextValue
    {
        get { return hfDataTextValue.Value; }
        set { hfDataTextValue.Value = value; }
    }
    public int DataNoValue
    {
        get { return Convert.ToInt32(hfDataNoValue.Value); }
        set { hfDataNoValue.Value = value.ToString(); }
    }
    public string PredefinedField
    {
        get { return hfPredefinedField.Value; }
        set { hfPredefinedField.Value = value; }
    }
    public string FilterColumn
    {
        get { return hfReferenceField.Value; }
        set { hfReferenceField.Value = value.ToString(); }
    }
    public int FilterId
    {
        get
        {
            if (FilterColumn == "")
                return 0;
            else
                return Convert.ToInt32(hfReferenceId.Value);
        }
        set { hfReferenceId.Value = value.ToString(); }
    }
    public string SelectedValue
    {
        get { return hfCurrentIndex.Value; }
        set { hfCurrentIndex.Value = value; }
    }
    public string PreviousID
    {
        get { return this.hfPreviousID.Value; }
        set { this.hfPreviousID.Value = value; }
    }
    public string FunctionName
    {
        get { return hfFunctionName.Value; }
        set { hfFunctionName.Value = value; }
    }
    public string DisplayName
    {
        get { return hfDisplayName.Value; }
        set { hfDisplayName.Value = value; }
    }
    public string ParentElementId
    {
        get { return hfParentElementId.Value; }
        set { hfParentElementId.Value = value; }
    }
    #endregion

    public string ExcludeFiled
    {
        get { return this.hfExcludeFiled.Value; }
        set { this.hfExcludeFiled.Value = value; }
    }

    /// <summary>
    /// Gets or sets values seperated by comma. 
    /// </summary>
    public string ExcludeValues
    {
        get { return this.hfExcludeValues.Value; }
        set { this.hfExcludeValues.Value = value.Trim(); }
    }

    public Controls_DesignationReference()
    {
    }

    private string ValidationGroup
    {
        get
        {
            if (this.hfVaidationGroup.Value.Trim() == string.Empty)
                this.hfVaidationGroup.Value = Guid.NewGuid().ToString();

            return this.hfVaidationGroup.Value.ToString();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.ModalPopupID != null)
        {
            AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(this.ModalPopupID);
            if (mpe != null)
            {
                mpe.PopupDragHandleControlID = "DragHandle";
                mpe.CancelControlID = this.ibtnClose.ClientID;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataNoValue = 0;
            Clear();
        }
        if (clsGlobalization.IsArabicCulture())
        {
            rfvtxtDesignation.ValidationGroup = RequiredFieldValidator1.ValidationGroup = "Save";

        }
        else
        {
            RequiredFieldValidator1.ValidationGroup = "Save";
            RequiredFieldValidator1.ValidationGroup = "";
            RequiredFieldValidator1.ErrorMessage = "";
        }
       
    }

    public void GetData()
    {
        DataTable dt = null;
        clsUserMaster objUserMaster = new clsUserMaster();
        ViewState["details"] = dt = new clsDesignationReference().GetDesignation(objUserMaster.GetCompanyId());
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("--اختر--") : ("Select");
        ddlParent.DataSource = dt;
        ddlParent.DataBind();
        ddlParent.Items.Insert(0, new ListItem(str, "-1"));

        using (DataTable dtgrid = new clsDesignationReference().BindGrid(objUserMaster.GetCompanyId()))
        {
            dgvReference.DataSource = dtgrid;
            dgvReference.DataBind();
        }
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {

            Clear();
            this.Dispose();
        }
        finally
        {
            objReference = null;
        }
    }

    public void ClearAll()
    {
        Clear();
    }

    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        Save();        
        txtDesignation.Text = "";
        txtDesignationArb.Text = "";
        ddlParent.Enabled = true;
        ddlParent.SelectedIndex = -1;
    }

    protected void ImgEdit_Click1(object sender, ImageClickEventArgs e)
    {
        lblStatusMessage.Text = "";
        btnSave.Enabled = true;
        ddlParent.Enabled = false;
        string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
        ViewState["ID"] = Id.ToInt32();
        Edit(Convert.ToInt32(Id));

    }


    private bool IsPredefinedValue(string DataValue)
    {
        if (hfPredefinedField.Value != "")
            return new clsReferenceNew().IsPredefinedValue(hfTableName.Value, hfDataValueField.Value, DataValue, hfPredefinedField.Value);
        else
            return false;

    }
    private void Edit(int Id)
    {
        clsDesignationReference objDesignation = new clsDesignationReference();
        objUserMaster = new clsUserMaster();
        objDesignation.DesignationID = Id;
        using (DataTable dt = new clsDesignationReference().GetDataById(Id, objUserMaster.GetCompanyId()))
        {
            if (dt.Rows.Count > 0)
            {
                txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                txtDesignationArb.Text = dt.Rows[0]["DesignationArb"].ToString();
                ddlParent.SelectedValue = dt.Rows[0]["ParentDesignationID"].ToString();
            }
        }
    }

    private DataTable GetDataById(int Id)
    {
        try
        {
            objReference = new clsReferenceNew();
            return null;
            //return objReference.SelectById(TableName, DataValueField, DataTextField, Id);
        }
        catch
        {
            throw;
        }
        finally
        {
            objReference = null;
        }
    }


    private void Save()
    {
        try
        {
            objdesignation = new clsDesignationReference();
            objUserMaster = new clsUserMaster();
            lblStatusMessage.Text = "";

            if (ddlParent.SelectedValue.ToInt32() == -1)
                objdesignation.ParentDesignationID = 0;
            else
                objdesignation.ParentDesignationID = ddlParent.SelectedValue.ToInt32();
            objdesignation.Designation = txtDesignation.Text;
            objdesignation.DesignationArb = txtDesignationArb.Text;
            objdesignation.CompanyID = objUserMaster.GetCompanyId();
            if (ViewState["ID"].ToInt32() == 0)
            {
                
                if (objdesignation.CheckDesignationNameExistence() == 0)
                {
                    int DesignationID = objdesignation.AddDesignationReference();

                    objdesignation.DesignationID = DesignationID;

                    if (DesignationID > 0)
                    {
                        objdesignation.InsertHierarchy();
                      
                        GetData();
                        lblStatusMessage.Visible = true;
                        lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حفظ بنجاح") : ("Saved successfully"); //"Saved successfully";

                        if (OnSave != null)
                        {
                            OnSave(DesignationID.ToString());
                        }
                        Clear();
                    }
                    else if (DesignationID == 0)
                    {
                        lblStatusMessage.Visible = true;
                        lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("نفس الاسم موجود بالفعل!!!") : ("same name already exists !!!"); //"same name already exists !!!";

                       
                    }
                    else
                    {
                        lblStatusMessage.Visible = true;
                        lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("انقاذ فاشلة") : ("Save failed"); //"Save failed";
                    }

                }
                else
                {
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("نفس الاسم موجود بالفعل!!!") : ("same name already exists !!!"); //"same name already exists !!!";
                }
            }


            else
            {
                objdesignation.DesignationID = ViewState["ID"].ToInt32();
                if (objdesignation.CheckDesignationNameExistence() == 0)
                {
                    objdesignation.UpdateDesignationReference();
                   
                    GetData();
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث بنجاح") : ("Updated successfully"); //"Updated successfully";

                    if (OnSave != null)
                    {
                        OnSave(ViewState["ID"].ToString());

                    }
                    Clear();
                }
                else
                {
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("نفس الاسم موجود بالفعل!!!") : ("same name already exists !!!"); //"same name already exists !!!";
                }
            }

        }

        catch
        {
            throw;
        }
        finally
        {
            objReference = null;
        }
    }


    private void Clear()
    {
        txtDesignation.Text = "";
        txtDesignationArb.Text = "";
        lblStatusMessage.Text = "";
        lblStatusMessage.Visible = false;
        DataNoValue = 0;
        btnSave.Enabled = true;
        ddlParent.Enabled = true;
        ViewState["ID"] = 0;
        ddlParent.SelectedIndex = -1;
    }

    protected void imgClear_Click(object sender, ImageClickEventArgs e)
    {
        Save();
        GetData();
        ddlParent.Enabled = true;
        this.Clear();
    
    }

}
