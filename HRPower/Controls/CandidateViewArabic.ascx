﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateViewArabic.ascx.cs"
    Inherits="Controls_CandidateViewArabic" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
<link href="../css/singleview.css" rel="stylesheet" type="text/css" />
<div id="divViewCandidate" runat="server">
    <h4 style="padding-top: 20px;">
        Basic Info</h4>
        
        
        
        <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Application No
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvApplicationNo" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
        
      <div style ="clear:both"></div>
        
        
        
        
        
        
        
        
        
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Candidate Code
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="divCandidateCode" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
   
   
   <div style ="clear:both"></div>
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Name(Eng)
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCandidateNameEng" runat="server" class="trLeft" style="float: left; width: 74%;
        height: 20px;padding-left:0px">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Name(Arabic)</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCandidateNameAr"  class="trLeft" style="word-break:break-all; float: left; width: 74%; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Citizenship
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCitizenShip" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
    
    
  
    
      <div id="dvCitizenShipArabic" class="trLeft" style="float: left; width: 26%;text-align :right; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
    
    
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
  
    
      <div class="trLeft" style="float: left; width: 14%; height: 20px;text-align :right;padding-left:0px">
        مواطنية
    </div>
    
    
    <div style ="clear:both"></div>
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Place of birth</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPlaceOfBirth" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
    
    
    
    
    
    
    
     
     
     
      <div id="dvPlaceOfBirthArabic" class="trLeft" style="float: left; width: 26%; height: 20px;padding-left:0px;text-align:right"
        runat="server">
    </div>
    
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
   
    
     <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px;text-align:right ">
       مكان الميلاد</div>
    
    
    <div style ="clear:both"></div>
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Gender
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvGender" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
     <div id="dvGenderArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
         جنس </div>
    
    <div style ="clear:both"></div>
    
    <%--<div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Agency</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvAgency" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvAgencyArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
       وكالة</div>--%>
        
        
    <div style="clear: both">
    </div>
    
    
    
    
    
    <div style ="clear :both"></div>
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Date of birth</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvDateOfBirth" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>


     <div id="dvDateOfBirthArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
        تاريخ الميلاد</div>
        
        
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Marital Status</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    
    
    <div id="dvMaritalStatus" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px"
        runat="server">
    </div>
    
    
    
    
    
    
         <div id="dvMaritalStatusArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
      لحالة الاجتماعية</div>
        
    
    
    
    
    
    
    
    
    <div style ="clear:both"></div>
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        No of son</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvNoOfSon" class="trLeft" style="float: left; width: 30%;
        height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
       
         <div id="dvNoOfSonArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
 عدد الابناء</div>
        
    
    
    
    <div style ="clear :both"></div>
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Religion</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvReligion" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
         <div id="dvReligionArabic" class="trLeft" style="text-align :right;  float: left; width: 26%; height: 20px; padding-left: 0px;
        text-align: right" runat="server">
    </div>
    
   
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
  
    
      <div class="trLeft" style="text-align :right;float: left; width: 14%; height: 20px; padding-left: 0px;
        text-align: right">
 الديانة</div>
        
    
    
    
    <div style ="clear :both"></div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Mothers Name
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvMothersName" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    <div id="dvMothersNameArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
       إسم الام</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Cur.Nationality</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCurrentNationlity" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     <div id="dvCurrentNationlityArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
       الجنسية الحالية </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Pre.Nationality</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPreviousNationality" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     
    
     <div id="dvPreviousNationalityArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
     الجنسية السابقة </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Doctrine</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvDoctrine" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvDoctrineArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
     المذهب </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Telephone</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvTelephone" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
   
   
   
   
     <div id="dvTelephoneArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
    رقم الهاتف خارج الدولة </div>
    <div style="clear: both">
    </div>
    
    
   
   
   
   
   
   
   
   
   
   
   
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       P.O Box</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPOBOX" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
    
      <div id="dvPOBOXArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
    صندوق البريد </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Country</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    
    
    
    
    
    
    
    
    <div id="dvCountry" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
    
      <div id="dvCountryArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
   صندوق البريد خارج الدولة</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Company Type
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCompanyType" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvCompanyTypeArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
   نوع المؤسسة</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Email
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvEmail" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
   
    
    
    
    
    <div style ="clear:both"></div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px;padding-left:0px">
       BusinessPair(place)
    </div>
    
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPlaceOfBusinessPair" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     
      <div id="dvPlaceOfBusinessPairArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  اسم مكان عمل الزوج</div>
    <div style="clear: both">
    </div>
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Referral Type
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvReferralType" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvReferralTypeArb" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  نوع الإحالة</div>
    <div style="clear: both">
    </div>
    
    
    
   <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Referred By
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvReferredBy" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvReferredByArb" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
   ٌيحيلها</div>
    <div style="clear: both">
    </div> 
    
    
    
    
    
    
    
    
    
    <div style ="clear:both"></div>
    
    <div style ="height:auto">
    
    
     <div class="trLeft" style="float: left; width: 14%;height: 20px;padding-left:0px">
        Cur.Address
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCurrentAddress" class="trLeft" style="float: left; width: 40.5%; min-height: 20px;padding-left:0px; word-break: break-all;" runat="server">
    </div>
    
    </div> 
    
    <div style ="clear:both"></div>
    
    
    
    
    
    
    <div style ="height:auto">
     <div class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px">
        
    </div>
          <div id="dvCurrentAddressArabic" class="trLeft" style="text-align: right; float: left; width: 40.5%;
        min-height: 20px; padding-left: 0px; word-break:break-all; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; min-height: 20px;
        padding-left: 0px; text-align: right">
  العنوان</div>
   
   </div>
   
    <div style ="clear:both"></div>
   
   
   
   
   
   
   
   
   
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div style ="height:auto">
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Address</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvAddress" class="trLeft" style="float: left; width: 40.5%; min-height: 20px;padding-left:0px; word-break:break-all;" runat="server">
    </div>
    
     
    </div> 
    
    <div style ="clear:both"></div>
    
    
    <div style="height: auto">
     <div class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px">
        
    </div>
        <div id="dvAddressArabic" class="trLeft" style="text-align: right; word-break:break-all; float: left; width: 40.5%;
            min-height: 20px; padding-left: 0px; text-align: right" runat="server">
        </div>
        <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
            :
        </div>
        <div class="trLeft" style="text-align: right; float: left; width: 14%; min-height: 20px;
            padding-left: 0px; text-align: right">
            العنوان</div>
    </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <h4 style="padding-top: 10px">
        Professional Info</h4>
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Qualification
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvQualification" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvQualificationArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  المؤهل الدراسي</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Category
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCAtegory" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
      <div id="dvCAtegoryArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  فئة المؤهل/ التخصص</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    <%--<div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Graduation
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    
    <div id="dvGraduation" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
      <div id="dvGraduationArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  سنة التخرج</div>--%>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Gradudation Year
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvGraduationYear" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvGraduationYearArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  سنة التخرج</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left;  width: 14.1%; height: 20px;padding-left:0px;padding-left:0px">
       Place of Graduation
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPlaceOfGraduation" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
      <div id="dvPlaceOfGraduationArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
  مكان التخرج</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       College/School
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    
    <div id="dvCollegeOrSchool" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     
     <div id="dvCollegeOrSchoolArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
        الكلية / المدرسة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Previous job
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPreviousJob" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvPreviousJobArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
       الوظيفة السابقة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Sector
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvSector" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
     <div id="dvSectorArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
      الجهة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Position
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPosition" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvPositionArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
    الوظيفة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Type of experience
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvTypeOfExperience" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvTypeOfExperienceArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
   نوع الخبرة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14.3%; height: 20px;padding-left:0px">
       Years of experience
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvYearsOfExperience" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     
     <div id="dvYearsOfExperienceArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
سنوات الخبرة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Compensation
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCompensation" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvCompensationArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
الراتب المتوقع
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Specialization
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvSpecialization" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
     <div id="dvSpecializationArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
التخصص
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
     <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Date of transaction
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvDateOfTransaction" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
      <div id="dvDateOfTransactionArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
تاريخ إدخال المعاملة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Expected join date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvExpectedJoinDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
      <div id="dvExpectedJoinDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
تاريخ التعيين المتوقع </div>
    <div style="clear: both">
    </div>
    
    
    
    
    <div style="height: 50px;">
     <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Skills
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvSkills" class="trLeft" style="float: left; width: 40.5%; height: 50px;padding-left:0px; 
    word-break:break-all; overflow:scroll; overflow-x: hidden;" runat="server">
    </div>
    </div>
    
    
    
    
    
    <div style="height: 50px;">
    <div class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px;">
        
    </div>
      <div id="dvSkillsArabic" class="trLeft" style="text-align: right; float: left; width: 40.5%;
        height: 50px; padding-left: 0px; text-align: right; overflow:scroll; overflow-x: hidden;" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
       المهارات</div>
    <div style="clear: both">
    </div>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
      <div id="dvLaunguagesArabic" class="trLeft" style="text-align: right; float: left; width: 71%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
      العربية</div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
     <div class="trLeft" style="float: left; width: 14.8%; height: 20px;padding-left:0px">
      Languages (English)
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvLanguagesEnglish" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
  
    
    
    
    
    
    
    
    <div style ="clear :both"></div>
    
    
    
    
    
    
     <div class="trLeft" id="dvOtherLangC1" runat ="server"  style="float: left; width: 14.8%; height: 20px;padding-left:0px">
      
    </div>
    <div class="trLeft" id="dvOtherLangb" runat ="server"  style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherLanguagesC2" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
   
    
    
    
    
    
    
    
    
    
    
    
    
    <div style="clear: both">
    </div>
    <h4 style="padding-top: 10px">
        Document Info</h4>
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Passport Number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPassportNumber" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     
      <div id="dvPassportNumberArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
رقم جواز السفر 
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Country
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPassportCountry" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     
      <div id="dvPassportCountryArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
جهة الاصدار
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Place of issue
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPassportPlaceOfIssue" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     
      <div id="dvPassportPlaceOfIssueArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
مكان الاصدار
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Expiry date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPassportExpiryDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
     
      <div id="dvPassportExpiryDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
تاريخ الانتهاء
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Issue date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvPassportIssueDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvPassportIssueDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
تاريخ الاصدار
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Visa number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaNumber" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaNumberArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
رقم الاقامة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Unified number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaUnifiedNumber" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaUnifiedNumberArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

الرقم الموحد
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Country
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaCountry" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaCountryArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

جهة الاصدار
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Place of issue
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaPlaceOfIssue" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaPlaceOfIssueArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

مكان الاصدار

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Expiry date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaExpiryDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaExpiryDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

تاريخ الانتهاء

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Issue date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvVisaIssueDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvVisaIssueDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

تاريخ الاصدار

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
   
    
    
    
     <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvNationalCardNumber" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvNationalCardNumberArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

رقم البطاقة

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
     Card Number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCardNo" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
     <div id="dvCardNoArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

رقم الهوية

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
   <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
      Expiry Date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvCardNoExpiry" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
      
     <div id="dvCardNoExpiryArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

تاريخ الانتهاء

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
    
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        
    </div>
    <div id="div63" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
  
  
    
    <div style ="clear:both"></div>
    <h4 style="padding-top: 10px">
        Other Info</h4>
        
         <div style ="height:auto">
    
    <div class="trLeft" style="float: left; width: 14.22%; height: 20px;padding-left:0px;word-break:break-all">
        Permanent Address
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherPermanentAddress" class="trLeft" style="float: left; width: 41.6%;word-break:break-all; min-height: 20px;padding-left:0px" runat="server">
    </div>
    
        </div>
    
    <div style ="clear :both"></div>
    
    
    
    
    
    
    
    
         <div style ="height:auto">
         <div class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px">
        
    </div>
     <div id="dvOtherPermanentAddressArabic" class="trLeft" style="text-align: right; float: left; width: 41%;
        min-height: 20px;word-break:break-all; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

العنوان التفصيلي

 </div>
 </div> 
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <div style ="height:auto">
    
    
     <div class="trLeft" style="float: left; width: 14.22%; height: 20px;padding-left:0px">
        Business Address
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherBusinessAddress" class="trLeft" style="float: left; width: 41.6%;word-break:break-all; min-height: 20px;padding-left:0px" runat="server">
    </div>
    
    </div>
    
    
    <div style="clear: both">
    </div>
    
    
    
    
    
    
         <div style ="height:auto">
         <div class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px">
        
    </div>
     <div id="dvOtherBusinessAddressArabic" class="trLeft" style="text-align: right; float: left; width: 41%;
        min-height: 20px;word-break:break-all; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

عنوان العمل

 </div>
 </div> 
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Home Street</div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherHomeStreet" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
       
     <div id="dvOtherHomeStreetArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

اسم شارع المنزل

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        City
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    
    
    
    <div id="dvOtherCity" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
     <div id="dvOtherCityArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

الإمارة

 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Home Telephone
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherHomeTelephone" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
   
   
   
   
    
     <div id="dvOtherHomeTelephoneArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

هاتف المنزل

 </div>
    <div style="clear: both">
    </div>
    
    
    
   
   
   
   
   
   
   
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Business Street
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherBusinessStreet" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
      <div id="dvOtherBusinessStreetArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">

اسم شارع الأعمال
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Area
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherArea" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
      <div id="dvOtherAreaArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
المنطقة
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        PO Box
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherPOBox" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
      <div id="dvOtherPOBoxArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
صندوق البريد
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Fax
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherFax" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
    
    
      <div id="dvOtherFaxArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
الفاكس
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Telephone number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherTelephone" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
      <div id="dvOtherTelephoneArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
هاتف العمل
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
       Mobile Number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherMob" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>
    
    
    
    
    
      <div id="dvOtherMobArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
رقم الهاتف المتحرك
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
    
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Sign up date
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherSignUpDate" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>    
    
    
    
    
    
      <div id="dvOtherSignUpDateArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
تاريخ الاشتراك
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
     
    <div class="trLeft" style="float: left; width: 14%; height: 20px;padding-left:0px">
        Insurance number
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px;padding-left:0px">
        :
    </div>
    <div id="dvOtherInsurance" class="trLeft" style="float: left; width: 30%; height: 20px;padding-left:0px" runat="server">
    </div>   
    
    
    
      <div id="dvOtherInsuranceArabic" class="trLeft" style="text-align: right; float: left; width: 26%;
        height: 20px; padding-left: 0px; text-align: right" runat="server">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        :
    </div>
    <div class="trLeft" style="text-align: right; float: left; width: 14%; height: 20px;
        padding-left: 0px; text-align: right">
الرقم التاميني
 </div>
    <div style="clear: both">
    </div>
    
    
    
    
    
    
    
    
    
</div>
