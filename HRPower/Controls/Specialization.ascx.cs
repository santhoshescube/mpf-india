﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class controls_Specialization : System.Web.UI.UserControl
{
    clsSpecializationReference objSepcialization;

    public delegate void OnUpdate();
    public event OnUpdate Update;

    string sModalPopupID;

    private clsSpecializationReference Specialization
    {
        get 
        {
            if (this.objSepcialization == null)
                this.objSepcialization = new clsSpecializationReference();

            return this.objSepcialization;
        }
    }

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        cssMaroon.Attributes.Clear();
        lblStatusMessage.Text = "";
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
            Update();

        mpe.Hide();
    }

    public void FillList()
    {
        this.ClearControls();

        this.ddlDegree.DataValueField = "DegreeID";
        this.ddlDegree.DataTextField = "Description";
        this.ddlDegree.DataSource = this.Specialization.GetDegrees();
        this.ddlDegree.DataBind();

        this.BindGrid();
    }

    private void BindGrid()
    {
        int DegreeID = 0;

        gvDegreeReference.DataSource = Specialization.GetSpecializationsByDegree(DegreeID);
        gvDegreeReference.DataBind();
    }

    private bool Save()
    {
        this.Specialization.SpecializationName = this.txtSpecialization.Text;
        this.Specialization.DegreeID = this.ddlDegree.SelectedValue.ToInt32();

        bool success = false;

        if (ViewState["SpecializationID"] != null && ViewState["SpecializationID"].ToInt32() > 0)
        {
            this.Specialization.SpecializationID = this.ViewState["SpecializationID"].ToInt32();
            success = this.Specialization.Update();

            this.lblStatusMessage.Text = (success) ? "Data updated successfully." : "Could not update specialization.";
        }
        else
        {
            success = this.Specialization.Insert();
            this.lblStatusMessage.Text = (success) ? "Data inserted successfully." :  "Could not insert specialization.";
        }
        return success;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            ClearControls();
            BindGrid();

            if (Update != null)
                Update();
        }
    }

    protected void gvDegreeReference_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_ALTER":

                this.Specialization.SpecializationID = e.CommandArgument.ToInt32();

                this.ViewState["SpecializationID"] = e.CommandArgument.ToInt32();
                
                int RowIndex = ((GridViewRow)((TableCell)((Button)e.CommandSource).Parent).Parent).RowIndex;

                Label lblDescription = (Label)this.gvDegreeReference.Rows[RowIndex].FindControl("lblDescription");

                this.txtSpecialization.Text = lblDescription.Text;

                int DegreeID = this.gvDegreeReference.DataKeys[RowIndex].Values["DegreeID"].ToInt32();
                
                this.ddlDegree.ClearSelection();

                if (this.ddlDegree.Items.FindByValue(DegreeID.ToString()) != null)
                    this.ddlDegree.Items.FindByValue(DegreeID.ToString()).Selected = true;

                this.upForm.Update();
                
                break;

            case "_REMOVE":

                GridViewRow gr = (GridViewRow)((Button)e.CommandSource).Parent.Parent;
                Button ImgDelete = (Button)gr.FindControl("ImgDelete");

                try
                {
                    this.lblStatusMessage.Text = string.Empty;

                    this.Specialization.SpecializationID = e.CommandArgument.ToInt32();
                    bool success = this.Specialization.Delete();

                    if (success)
                    {
                        this.BindGrid();
                        this.ClearControls();
                        this.ViewState["SpecializationID"] = null;
                        if (Update != null)
                            Update();
                    }
                    else
                        this.lblStatusMessage.Text = "Deletion failed.";
                }
                catch (SqlException ex)
                {
                    clsCommon.WriteErrorLog(ex);
                    ScriptManager.RegisterClientScriptBlock(ImgDelete, ImgDelete.GetType(), Guid.NewGuid().ToString(), "alert('Sorry, Unable to delete. This specialization details exists in the system.')", true);
                }

                break;
        }
    }

    private void ClearControls()
    {
        this.txtSpecialization.Text = string.Empty;
        this.ddlDegree.SelectedIndex = 0;
        this.txtSpecialization.Focus();
    }

    protected void imgClear_Click(object sender, EventArgs e)
    {
        this.ClearControls();
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = divHeaderMove.ClientID;
    }
}