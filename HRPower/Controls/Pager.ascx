﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pager.ascx.cs" Inherits="controls_Pager" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/default_common.css" rel="stylesheet" type="text/css" />

<div style ="width:100%; padding-top:10px;">
<table cellpadding="0" cellspacing="0" width="99%" style="border-top:solid 1px #66bce2; padding-top:10px;" >
    <tr>
        <td>
            <table style="font-size: 10px">
                <tr>
                    <td>
                       <%-- Select Rows--%><asp:Literal ID="lit1" runat ="server" Text ='<%$Resources:ControlsCommon,SelectRows%>'></asp:Literal>
                       </td>
                    <td>
                        <asp:DropDownList ID="ddlPaging" runat="server" AutoPostBack="true"   Width="50px" 
                            OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged" style="border:solid 1px #66bce2;">
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="20" Text="20"></asp:ListItem>
                            <asp:ListItem Value="30" Text="30"></asp:ListItem>
                            <asp:ListItem Value="40" Text="40"></asp:ListItem>
                            <asp:ListItem Value="50" Text="50"></asp:ListItem>
                            <asp:ListItem Value="60" Text="60"></asp:ListItem>
                            <asp:ListItem Value="70" Text="70"></asp:ListItem>
                            <asp:ListItem Value="80" Text="80"></asp:ListItem>
                            <asp:ListItem Value="90" Text="90"></asp:ListItem>
                            <asp:ListItem Value="100" Text="100"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
        <td>
            &nbsp;
        </td>
        <td align="right">
            <table style="font-size: 10px">
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkpageingPrev" runat="server"  CssClass ="linknext" Text="<<"
                            OnClick="lnkpageingPrev_click" CausesValidation="False"></asp:LinkButton>
                    </td>
                    <td>
                        <%--Goto Page--%><asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,GotoPage%>'></asp:Literal>
                        </td>
                    <td>
                        <asp:TextBox ID="txtGotoPage" AutoPostBack="true" MaxLength ="9"
                            OnTextChanged="txtGotoPage_Textchanged" Width="35px" Height="20px" style="padding:0;border:solid 1px #66bce2;" runat="server"></asp:TextBox>
                        <div style="display: none">
                            <asp:HiddenField ID="hdCurrentPage" runat="server" Value="0" />
                            <asp:HiddenField ID="hdPageCount" runat="server" Value="0" />
                            <AjaxControlToolkit:FilteredTextBoxExtender ID="fteGotoPage" FilterType="Numbers"
                                TargetControlID="txtGotoPage" runat="server">
                            </AjaxControlToolkit:FilteredTextBoxExtender>
                        </div>
                    </td>
                    <td>
                        <asp:Label ID="lblPagecaption" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:LinkButton ID="lnkpageingNext" CssClass ="linknext"  runat="server" Text=">>" OnClick="lnkpageingNext_click"
                            CausesValidation="False"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</div>

