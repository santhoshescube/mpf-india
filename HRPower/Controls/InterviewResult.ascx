﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InterviewResult.ascx.cs"
    Inherits="Controls_InterviewResult" %>
<asp:HiddenField ID="hfPendingCount" runat="server" />

<script type="text/javascript">

function ValidateSave()
{
var hfPendingCount = document.getElementById('<%=hfPendingCount.ClientID%>');

if(hfPendingCount)
{

if(hfPendingCount.value != "" )
{
    if(parseInt(hfPendingCount.value) >0)
    {
        alert("You can save only after all the interviewers finished their evaluation");
        return false;
    }
  
}
}

return true;

}
</script>

<div id="popupmain1" style="width: 1000px; height: auto; overflow: auto">
    <div id="header11" style="width: 100%">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" meta:resourcekey="InterviewResult"></asp:Label>
                <asp:Label ID="lblName" runat="server" meta:resourcekey="InterviewResult"></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                        OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <div style="width: 100%; overflow: hidden;">
        <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="display: block; max-height:500px; overflow:auto ; width:99.8%;">

                     <div id="Div1" style="width: 99.8%; background-color: White; border: solid 1px grey;border-top: 0px;>
                    <asp:Repeater ID="rpLevels" runat="server"  OnItemDataBound="rpLevels_ItemDataBound"  >
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkLevels" runat="server" Style="color: #Blue; font-size: 15px;
                                font-family: Times New Roman"><%#Eval("JobLevels")%></asp:LinkButton>
                                <asp:HiddenField ID="hfdLevelID" runat ="server"  Value = '<%#Eval("JobLevelID")%>'/>
                                
                            <div id="divJobDetails" runat="server" style="width: 99.8%; display: block;">
                                <div style="min-width: 100%; overflow: auto">
                                    <asp:DataGrid ID="dgv" CssClass="trLeft" runat="server" BackColor="White" BorderColor="Black"
                                        BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Width="100%">
                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" Mode="NumericPages" />
                                        <ItemStyle ForeColor="#000066" />
                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle Font-Bold="false" CssClass="trLeft" />
                                    </asp:DataGrid>
                                </div>
                              
                            </div>
                               <div id="dvMark" style="width: 99.8%; background-color: White; border: solid 1px grey;
                                    border-top: 0px; height: 100px">
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                       <%-- Max Marks--%>
                                       <asp:Literal ID="lit1" runat ="server" meta:resourcekey="Maxmarks"></asp:Literal>
                                    </div>
                                    <div id="div2" class="trLeft" style="padding-top: 5px; float: left; width: 45%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblTotalMarks" runat="server"></asp:Label>
                                    </div>
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                       <%-- Total Weighted Marks--%>
                                       <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="TotalWeightedMarks"></asp:Literal>
                                    </div>
                                    <div id="div5" class="trLeft" style="padding-top: 5px; float: left; width: 10%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblTotalWeightedMarks" runat="server"></asp:Label>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                       <%-- Pass Percentage--%>
                                        <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="PassPercentage"></asp:Literal>
                                    </div>
                                    <div id="div3" class="trLeft" style="padding-top: 5px; float: left; width: 45%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblPassPercentage" runat="server"></asp:Label>
                                    </div>
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                        <%--% Mark--%>
                                        <asp:Literal ID="Literal3" runat ="server" meta:resourcekey="Mark"></asp:Literal>
                                    </div>
                                    <div id="div6" class="trLeft" style="padding-top: 5px; float: left; width: 10%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblTotalPercentage" runat="server"></asp:Label>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                        <%--Interviewers--%>
                                         <asp:Literal ID="Literal4" runat ="server" meta:resourcekey="Interviewers"></asp:Literal>
                                    </div>
                                    <div id="div4" class="trLeft" style="padding-top: 5px; float: left; width: 45%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblNoOfInterviewers" runat="server"></asp:Label>
                                    </div>
                                    <div class="trLeft" style="padding-top: 5px; float: left; width: 20%; height: 20px;">
                                        <%--Result--%>
                                         <asp:Literal ID="Literal5" runat ="server" meta:resourcekey="Result"></asp:Literal>
                                    </div>
                                    <div id="div7" class="trLeft" style="padding-top: 5px; float: left; width: 10%; height: 20px"
                                        runat="server">
                                        :
                                        <asp:Label ID="lblResult" runat="server"></asp:Label>
                                    </div>
                                    <div style="clear: both">
                                    </div>
                                    <br />
                                    </div>
                        </ItemTemplate>
                    </asp:Repeater> 
                    </div>
                    <div id="DivStatus"  style="width: 99%; background-color: White; border: solid 0px grey;
                        border-top: 0px; height: 100px">
                        <div class="trLeft" style="width: 100%; margin-left: 6px; padding: 0px">
                            <table>
                                <tr>
                                    <td style="text-align: right">
                                    </td>
                                    <td style="text-align: right">
                                    </td>
                                    <td style="text-align: right">
                                    </td>
                                    <td style="text-align: right">
                                    </td>
                                </tr>
                                <tr >
                                    <td>
                                        <asp:RadioButton runat="server" GroupName="Status" ID="rdbAccept" meta:resourcekey="Selected" />
                                    </td>
                                    <td>
                                        <asp:RadioButton runat="server" GroupName="Status" ID="rdbWaitingList" meta:resourcekey="WaitingList"  />
                                    </td>
                                    <td>
                                        <asp:RadioButton runat="server" GroupName="Status" ID="rdbRejected" meta:resourcekey="Rejected" />
                                    </td>
                                    <td>
                                        <asp:RadioButton runat="server" GroupName="Status" ID="rdbRecommended" meta:resourcekey="Recommendedtootherjob"  />
                                    </td>
                                </tr>
                            </table>
                        </div>
                       <div class="trLeft" style=" float: left; margin-left: 20px; padding: 0px">
                            <%--<asp:ImageButton ID="ibtnSendOffer" runat="server" ImageUrl="~/images/sent_icon.jpg"
                                CausesValidation="false" ToolTip="Send Offer Letter" Enabled ="false" 
                                onclick="ibtnSendOffer_Click"  /> Send OfferLetter
                            <asp:CheckBox ID="chk" runat="server"  Visible ="false"  Text="Send offer letter" />--%>
                            <asp:Label ID="lblMessage" runat ="server" ForeColor="Red"   ></asp:Label>
                        </div>
                        <div style="float: right; margin-right: 10px">
                            <asp:Label ID="lblPendingEvaluatorCount" runat="server" ForeColor="Red" ></asp:Label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div style="clear: both; height: 100%" id="footerwrapper">
        <div id="footer11" style="width: 100%">
            <div id="buttons">
                <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="float: left; width: 35%;">
                            <asp:Label ID="lblWarning" runat="server"></asp:Label>
                        </div>
                        <div style="float: right;" align="left">
                            <asp:Button ID="btnSave" runat="server" CssClass="popupbutton" OnClick="btnOK_Click" 
                                Text='<%$Resources:ControlsCommon,Save%>'  Width="60" ValidationGroup="newSave";" />&nbsp;
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
