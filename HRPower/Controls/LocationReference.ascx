﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LocationReference.ascx.cs"
    Inherits="controls_LocationReference" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<div id="popupmainwrap">
    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="popupmain" style="width: 450px">
                <div id="divHeaderMove" runat="server">
                    <div id="header11">
                        <div id="hbox1">
                            <div id="headername">
                                Work Location
                            </div>
                        </div>
                        <div id="hbox2">
                            <div id="close1">
                                <asp:ImageButton ID="btnClose" runat="server" ToolTip="Close" OnClick="btnClose_Click"
                                    ImageUrl="~/Controls/images/icon-close.png" CausesValidation="False" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="toolbar">
                    <div id="imgbuttons" style="padding-top: 8px;">
                        <asp:ImageButton Style="padding-left: 10px;" ID="btnAddNew" runat="server" ToolTip="Add New"
                            CausesValidation="False" OnClick="btnAddNew_Click" ImageUrl="~/images/Plus.png" />
                        <asp:ImageButton Style="padding-left: 10px;" ID="btnSaveData" runat="server" ToolTip="Save Data"
                            OnClick="btnSaveData_Click" ImageUrl="~/images/Save%20Blue.png" />
                        <asp:ImageButton Style="padding-left: 10px;" ID="btnDelete" runat="server" CausesValidation="False"
                            ToolTip="Delete" OnClientClick="return confirm('Do you wish to delete this information?');"
                            OnClick="btnDelete_Click" ImageUrl="~/images/delete_icon_popup.png" />
                        <asp:ImageButton Style="padding-left: 10px;" ID="btnClear" runat="server" CausesValidation="False"
                            ToolTip="Clear" OnClick="btnClear_Click" ImageUrl="~/images/clear.png" />
                    </div>
                </div>
                <div id="contentwrap">
                    <div id="content">
                        <fieldset style="height: 400px">
                            <div style="width: 100%;">
                                <div style="width: 100%; height: 30px; margin-top: 6px;">
                                    <div style="width: 30%; height: 30px; float: left;">
                                        Company
                                    </div>
                                    <div style="width: 70%; height: 30px; float: left;">
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist_mandatory"
                                            DataTextField="CompanyName" DataValueField="CompanyID" Width="252px" Height="28px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 30px; margin-top: 6px;">
                                    <div style="width: 30%; height: 30px; float: left;">
                                        Location Name
                                    </div>
                                    <div style="width: 70%; height: 30px; float: left;">
                                        <asp:TextBox ID="txtLocation" runat="server" CssClass="textbox_mandatory" Width="245px"
                                            MaxLength="50"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 30px; margin-top: 6px;">
                                    <div style="width: 30%; height: 30px; float: left;">
                                        Address
                                    </div>
                                    <div style="width: 70%; height: 30px; float: left;">
                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="textbox_mandatory" Width="248px"
                                            MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 30px; margin-top: 10px;">
                                    <div style="width: 30%; height: 30px; float: left;">
                                        Phone Number
                                    </div>
                                    <div style="width: 70%; height: 30px; float: left;">
                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="textbox_mandatory" MaxLength="25"
                                            Width="245px"></asp:TextBox>
                                       <%-- <asp:RequiredFieldValidator ID="rfvtxtMobileNumberpnlTab1" runat="server" ControlToValidate="txtMobileNumber"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter mobile number."
                                            SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator>--%>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtMobileNumber" runat="server"
                                            FilterType="Numbers,Custom" TargetControlID="txtMobileNumber" ValidChars="+ ()-">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 30px; margin-top: 6px;">
                                    <div style="width: 30%; height: 30px; float: left;">
                                        Email ID
                                    </div>
                                    <div style="width: 70%; height: 30px; float: left;">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                            Width="245px"></asp:TextBox>
                                       <%-- <asp:RequiredFieldValidator ID="rfvtxtEmailpnlTab1" runat="server" ControlToValidate="txtEmail"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter email." SetFocusOnError="False"
                                            ValidationGroup="submit"></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="revEmailpnlTab1" runat="server" ControlToValidate="txtEmail"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid Email" SetFocusOnError="true"  
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; height: 30px;">
                                    <div style="width: 80%; height: 30px; float: left; padding-top: 5px;">
                                    </div>
                                    <div style="width: 20%; height: 30px; float: left; padding-top: 5px; margin-bottom: 5px;">
                                        <asp:Button ID="btnSave" runat="server" Text=" Save " class="popupbutton" OnClick="btnSave_Click" />
                                    </div>
                                </div>
                            </div>
                            <div style="width: 100%; overflow: auto; height: 150px; margin-top: 15px;">
                                <div style="width: 100%;">
                                    <asp:DataList ID="dlLocations" runat="server" DataKeyField="WorkLocationID" OnSelectedIndexChanged="dlLocations_SelectedIndexChanged"
                                        Style="background-color: White;" HeaderStyle-BorderStyle="Solid" HeaderStyle-BorderWidth="1px"
                                        HeaderStyle-BackColor="#AEEBFF" HeaderStyle-Width="420px">
                                        <HeaderTemplate>
                                            <div style="width: 98%; height: 30px;">
                                                <div style="width: 49%; height: 15px; padding-top: 5px; float: left; text-align: center;
                                                    background-color: #95E4FF;">
                                                    Company
                                                </div>
                                                <div style="width: 50%; height: 25px; padding-top: 5px; float: left; text-align: center;">
                                                    Location
                                                </div>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="width: 98%; height: 25px;">
                                                <div style="width: 49%; height: 20px; padding-top: 5px; float: left; text-align: center;
                                                    background-color: #F2F2F2;">
                                                    <asp:LinkButton ID="lnkCompany" runat="server" Text='<%# Eval("Company") %>' CommandArgument='<%# Eval("CompanyId") %>'
                                                        CommandName="SELECT" CssClass="linkbutton" CausesValidation="false"></asp:LinkButton>
                                                </div>
                                                <div style="width: 50%; height: 20px; padding-top: 5px; float: left; text-align: center;">
                                                    <%# Eval("LocationName") %>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="Solid" BorderWidth="1px" Width="100%" />
                                    </asp:DataList>
                                </div>
                            </div>
                        </fieldset>
                        <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" CssClass="error" ErrorMessage="Please select company"
                            Display="None" ControlToValidate="ddlCompany" InitialValue="-1" ValidationGroup = "LocationReference" ></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ErrorMessage="Please enter location"
                            ControlToValidate="txtLocation" SetFocusOnError="True" Display="None" ValidationGroup = "LocationReference" ></asp:RequiredFieldValidator>
                        <asp:ValidationSummary ID="vsLocation" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup = "LocationReference"/>
                    </div>
                </div>
                <div id="footerwrapper" style="height: 40px;">
                    <div id="footer11" style="height: 40px;">
                        <asp:Label ID="lblWarning" runat="server" CssClass="error"></asp:Label>
                        <div id="buttons">
                            <div style="float: left;">
                                <asp:Button ID="btnOK" runat="server" CssClass="popupbutton" Text="OK" OnClick="btnOK_Click" />
                            </div>
                            <div style="float: left;">
                                <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" OnClick="btnCancel_Click"
                                    CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
