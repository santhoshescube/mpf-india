﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class FormViewPager : System.Web.UI.UserControl
{
    /// <summary>
    /// Sets number of pages in the datasource
    /// </summary>
    public int PageCount
    {
        set
        {
            hdPageCount.Value = value.ToString();
        }
    }

    /// <summary>
    /// Gets or sets the current page index
    /// </summary>
    public int PageIndex
    {
        set
        {
            hdPageIndex.Value = value.ToString();
            txtPageIndex.Text = (value + 1).ToString();
        }
        get
        {
            return Convert.ToInt32(hdPageIndex.Value);
        }
    }

    public delegate void OnChange();
    public event OnChange Change;

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        Refresh();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        cssMaroon.Attributes.Clear();  
    }
    protected void btnFirst_Click(object sender, ImageClickEventArgs e)
    {
        hdPageIndex.Value = "0";

        if (Change != null)
            Change();
    }
    protected void btnPrevious_Click(object sender, ImageClickEventArgs e)
    {
        hdPageIndex.Value = ((Convert.ToInt32(hdPageIndex.Value) - 1) < 0 ? 0 : (Convert.ToInt32(hdPageIndex.Value) - 1)).ToString();

        if (Change != null)
            Change();
    }
    protected void btnNext_Click(object sender, ImageClickEventArgs e)
    {
        hdPageIndex.Value = (Convert.ToInt32(hdPageIndex.Value) + 1).ToString();

        if (Change != null)
            Change();
    }
    protected void btnLast_Click(object sender, ImageClickEventArgs e)
    {
        hdPageIndex.Value = (Convert.ToInt32(hdPageCount.Value) - 1).ToString();

        if (Change != null)
            Change();
    }
    protected void txtPageIndex_TextChanged(object sender, EventArgs e)
    {
        if ((Convert.ToInt32(txtPageIndex.Text) - 1) <= Convert.ToInt32(hdPageCount.Value))
            hdPageIndex.Value = (Convert.ToInt32(txtPageIndex.Text) - 1).ToString();

        if (Change != null)
            Change();
    }
    public void Refresh()
    {
        if (Convert.ToInt32(hdPageCount.Value) == 0)
        {
            btnFirst.Enabled = btnPrevious.Enabled = txtPageIndex.Enabled = btnNext.Enabled = btnLast.Enabled = false;
            txtPageIndex.Text = lblPageCount.Text = "0";
        }
        else
        {
            btnFirst.Enabled = btnPrevious.Enabled = txtPageIndex.Enabled = btnNext.Enabled = btnLast.Enabled = true;
            lblPageCount.Text = Convert.ToInt32(hdPageCount.Value).ToString();
            txtPageIndex.Text = (Convert.ToInt32(hdPageIndex.Value) + 1).ToString();

            if (Convert.ToInt32(hdPageIndex.Value) == 0)
            {
                btnFirst.Enabled = false;
                btnPrevious.Enabled = false;
            }

            if (Convert.ToInt32(hdPageIndex.Value) == (Convert.ToInt32(hdPageCount.Value) - 1))
            {
                btnNext.Enabled = false;
                btnLast.Enabled = false;
            }
        }
    }
}