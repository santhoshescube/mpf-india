﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_CandidateArabicReferenceControl : System.Web.UI.UserControl
{
    #region Declarations

    public delegate void OnPageRefresh();
    public event OnPageRefresh onRefresh;
    public clsCandidateArabicReferenceControl objReference;
    public delegate void saveHandler(DataTable dt,string SelectedValue,string functionName);
    public event saveHandler OnSave;

    #region Properties
    public string ModalPopupID 
    {
        get { return this.hfModalPopupID.Value; }
        set { this.hfModalPopupID.Value = value; }
    }
    public string DataTextField 
    {
        get { return hfDataTextField.Value; }
        set { hfDataTextField.Value = value; }
    }

    public string DataTextFieldArabic
    {
        get { return hfDataTextFieldArabic.Value   ; }
        set { hfDataTextFieldArabic.Value = value; }
    }

    public string DataValueFieldArabic
    {
        get { return hfDataValueFieldArabic.Value; }
        set { hfDataValueFieldArabic.Value = value; }
    }

    public string DataValueField
    {
        get { return hfDataValueField.Value; }
        set { hfDataValueField.Value = value; }
    }
    public string TableName 
    {
        get { return hfTableName.Value; }
        set { hfTableName.Value = value; }
    }
    public string DataTextValue 
    {
        get { return hfDataTextValue.Value; }
        set { hfDataTextValue.Value = value; }
    }
    public int DataNoValue 
    {
        get { return Convert.ToInt32(hfDataNoValue.Value); }
        set { hfDataNoValue.Value =  value.ToString(); }
    }
    public string PredefinedField
    {
        get { return hfPredefinedField.Value; }
        set { hfPredefinedField.Value = value; }
    }
    public string FilterColumn
    {
        get { return hfReferenceField.Value; }
        set { hfReferenceField.Value = value.ToString(); }
    }
    public int FilterId
    {
        get {
            if (FilterColumn == "")
                return 0;
            else
                return Convert.ToInt32(hfReferenceId.Value);
        }
        set { hfReferenceId.Value = value.ToString(); }
    }
    public string SelectedValue
    {
        get { return hfCurrentIndex.Value; }
        set { hfCurrentIndex.Value = value; }
    }
    public string PreviousID
    {
        get { return this.hfPreviousID.Value; }
        set { this.hfPreviousID.Value = value; }
    }
    public string FunctionName
    {
        get { return hfFunctionName.Value; }
        set { hfFunctionName.Value = value; } 
    }
    public string DisplayName
    {
        get { return hfDisplayName.Value; }
        set { hfDisplayName.Value = value; }
    }
    public string ParentElementId
    {
        get { return hfParentElementId.Value; }
        set { hfParentElementId.Value = value; }
    }
    #endregion
    public string ExcludeFiled
    {
        get { return this.hfExcludeFiled.Value; }
        set { this.hfExcludeFiled.Value = value; }
    }
    
    /// <summary>
    /// Gets or sets values seperated by comma. 
    /// </summary>
    public string ExcludeValues
    {
        get { return this.hfExcludeValues.Value; }
        set { this.hfExcludeValues.Value = value.Trim(); }
    }

    #endregion Declarations

    public Controls_CandidateArabicReferenceControl()
    {
    }

    private string ValidationGroup
    {
        get
        {
            if (this.hfVaidationGroup.Value.Trim() == string.Empty)
                this.hfVaidationGroup.Value = Guid.NewGuid().ToString();

            return this.hfVaidationGroup.Value.ToString();
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.ModalPopupID != null)
        {
            AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(this.ModalPopupID);
            if (mpe != null)
            {
                mpe.PopupDragHandleControlID = "DragHandle";
                mpe.CancelControlID = this.ibtnClose.ClientID;
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            DataNoValue = 0;
    }
     private void GetData()
    {
        using (DataTable dt = new clsCandidateArabicReferenceControl().Select(this.TableName, this.DataValueField, this.DataTextField, this.FilterColumn , this.FilterId, this.ExcludeFiled, this.ExcludeValues,this.DataTextFieldArabic))
        {
            dgvReference.DataSource = dt;
            dgvReference.DataBind();           
        }
    }

    private void Test()
    {
    }

    public void PopulateData()
    {
        DataNoValue = 0;
        this.PreviousID = this.SelectedValue;
        GetData();
        this.lblHeading.Text = DisplayName;
        //lblDisplayName.Text = DisplayName;
        Edit(SelectedValue.ToInt32()); 
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {           
                ClearHiddenFields();
                Clear();
                this.Dispose();            
        }
        finally
        {
            objReference = null;
        }
    }

    public void ClearAll()
    {
        ClearHiddenFields();
        Clear();
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
       if(onRefresh != null)
           onRefresh(); 
    }
    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        Save();
    }
  
    protected void ImgDelete_Click(object sender, ImageClickEventArgs e)
    {
        btnSave.Enabled = true; 
        lblStatusMessage.Text  = ""; ;
        //Clear();


        int ID = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument.ToInt32(); 
        if (!IsPredefinedValue(((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument.ToString()))
        {
            Delete(Convert.ToInt32(((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument));
            GetData();
            Refresh();



            if (ID == DataNoValue)
                DataNoValue = 0;
        }
        else
        {
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('Predefined value cannot be deleted');", true); 
            btnSave.Enabled = false;
            lblStatusMessage.Visible = true;
            lblStatusMessage.Text = "Predefined value cannot be deleted";

        }
    }
    protected void ImgEdit_Click1(object sender, ImageClickEventArgs e)
    {
        lblStatusMessage.Text = "";
        btnSave.Enabled = true;
        string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
        Edit( Convert.ToInt32(Id));
        if (!IsPredefinedValue(Id))
        {
            Refresh();
        }
        else
        {
            //ScriptManager.RegisterClientScriptBlock(upnlReference, upnlReference.GetType(), Guid.NewGuid().ToString() , "alert('Predefined value cannot be edited');", true); 
            btnSave.Enabled = false;
            lblStatusMessage.Visible = true;
            lblStatusMessage.Text = "Predefined value cannot be edited";
        }
    }


    private bool IsPredefinedValue(string DataValue)
    {
        if (hfPredefinedField.Value != "")
            return new clsCandidateArabicReferenceControl().IsPredefinedValue(hfTableName.Value, hfDataValueField.Value, DataValue, hfPredefinedField.Value);
        else
            return false;

    }
    private void Edit(int Id)
    {
        using (DataTable dt = GetDataById(Id))
        {
            if (dt.Rows.Count > 0)
            {
                txtReferenceName.Text = dt.Rows[0]["DataText"].ToString();
                txtReferenceNameArabic.Text = dt.Rows[0]["DataTextArabic"].ToString();
                DataNoValue = Convert.ToInt32(dt.Rows[0]["DataValue"]);
            }
        }
    }

    private DataTable GetDataById(int Id)
    {
        try
        {
            objReference = new clsCandidateArabicReferenceControl();
            return objReference.SelectById(TableName, DataValueField, DataTextField, Id,DataTextFieldArabic);
        }
        catch
        {
            throw;
        }
        finally
        {
            objReference = null;
        }
    }

    private void Delete(int Id)
    {
        try
        {
            objReference = new clsCandidateArabicReferenceControl();
            int DeleteStatus = objReference.Delete(TableName, DataValueField, Id);
            if (DeleteStatus > 0)
            {
                if (Id == Convert.ToInt32(SelectedValue))
                    SelectedValue = this.PreviousID;
                if (OnSave != null)
                    OnSave(objReference.Select(TableName, DataValueField, DataTextField, FilterColumn, FilterId, this.ExcludeFiled, this.ExcludeValues,DataTextFieldArabic), SelectedValue, FunctionName);

                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = "Deleted successfully";
            }
        }
        catch 
        {
            lblStatusMessage.Visible = true;
            lblStatusMessage.Text = "Cannot delete some reference exists!!!";
        }
        finally
        {
            objReference = null; 
        }
    }

    private void Save()
    {
        try
        {
            objReference = new clsCandidateArabicReferenceControl();
            lblStatusMessage.Text = "";
            int saveStatusId = objReference.insert(TableName, DataTextField, txtReferenceName.Text, DataValueField, DataNoValue, PredefinedField, FilterColumn, FilterId, DataTextFieldArabic,txtReferenceNameArabic.Text  );
            if (saveStatusId > 0)
            {
                this.SelectedValue = Convert.ToInt32(DataNoValue) == 0 ? saveStatusId.ToString() : DataNoValue.ToString();
                Clear();
                GetData();
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = "Saved successfully";
                if (OnSave != null)
                    OnSave(objReference.Select(TableName, DataValueField, DataTextField, FilterColumn, FilterId, ExcludeFiled, ExcludeValues,DataTextFieldArabic), SelectedValue, FunctionName);


            }
            else if (saveStatusId == 0)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = "Same english name already exists !!!";
            }

            else if (saveStatusId == -1)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = "Same arabic name already exists !!!";
            }
            else
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = "Save failed";
            }           
        }
        catch
        {
            throw;
        }
        finally
        {
            objReference = null;
        }
    }

    private void Refresh()
    {
        //if (onRefresh != null)
        //    onRefresh(); 
    }

    private void Clear()
    {
        txtReferenceName.Text = txtReferenceNameArabic.Text = lblStatusMessage.Text = "";
        lblStatusMessage.Visible = false;
        DataNoValue = 0;
        btnSave.Enabled = true;
    }

    private  void ClearHiddenFields()
    {
        hfId.Value =
        hfDataTextField.Value =
        hfDataTextValue.Value =
        hfDataNoValue.Value =
        hfPredefinedField.Value =
        hfTableName.Value =
        hfCurrentIndex.Value =
        hfFunctionName.Value = 
        hfReferenceField.Value =
        hfReferenceId.Value = string.Empty;
    }
    
    protected void imgClear_Click(object sender, ImageClickEventArgs e)
    {
        this.Clear();
    }
}
