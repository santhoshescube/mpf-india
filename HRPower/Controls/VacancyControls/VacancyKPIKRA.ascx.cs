﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Controls_VacancyControls_VacancyKPIKRA : System.Web.UI.UserControl
{
    #region Declarations

    private string CurrentSelectedValue { get; set; }
    List<clsVacancyKPI> JobKpi = new List<clsVacancyKPI>();
    List<clsVacancyKRA> jobKra = new List<clsVacancyKRA>();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ClearControls();
            BindInitials();
        }
    }

    private void ClearControls()
    {      
        ViewState["TableKPI"] = ViewState["TableKRA"] = null;
        JobKpi.Clear();
        jobKra.Clear();
        gvKpi.DataSource = null;
        gvKpi.DataBind();
        gvKra.DataSource = null;
        gvKra.DataBind();
    }

    private void BindInitials()
    {
        DataSet ds = clsVacancyKPIKRA.GetKpiKra(clsVacancyKPIKRA.ID);
        if (ds.Tables.Count > 0)
        {           
            //KPI details
            DataTable dtKpi = ds.Tables[0];
            if (dtKpi.Rows.Count > 0)
            {
                ViewState["TableKPI"] = dtKpi;
                gvKpi.DataSource = dtKpi;
                gvKpi.DataBind();
                SetPreviousKpi();
            }
            else
                SetInitialRow();

            //KRA details
            DataTable dtKra = ds.Tables[1];
            if (dtKra.Rows.Count > 0)
            {
                ViewState["TableKRA"] = dtKra;
                gvKra.DataSource = dtKra;
                gvKra.DataBind();
                SetPreviousKra();
            }
            else
                SetInitialRowKRA();
        }
        else
        {           
            SetInitialRow();
            SetInitialRowKRA();
        }
    }

    #region KPI

    //sets empty datalist
    private void SetInitialRow()
    {
        JobKpi.Add(new clsVacancyKPI() { KPIId = 0, KPIValue = 0, JobKpiID = 0 });
        ViewState["TableKPI"] = JobKpi;

        gvKpi.DataSource = JobKpi;
        gvKpi.DataBind();
    }

    private void AddNewRowKPI()
    {
        if (ViewState["TableKPI"] != null)
        {
            int rCount = 0;
            JobKpi = new List<clsVacancyKPI>();
            foreach (GridViewRow dr in gvKpi.Rows)
            {
                TextBox txtKpiMarks = (TextBox)dr.FindControl("txtKpiMarks");
                DropDownList ddlKpi = (DropDownList)dr.FindControl("ddlKpi");
                if (txtKpiMarks.Text == "0" || txtKpiMarks.Text == string.Empty)
                {
                    DispEmptyControlMessage("Enter Marks for KPI", "txt");
                    break;
                }
                if (ddlKpi.SelectedIndex == -1 || ddlKpi.SelectedIndex == 0)
                {
                    DispEmptyControlMessage("Select KPI", "ddl");
                    break;
                }
                if (txtKpiMarks != null && ddlKpi != null)
                {
                    JobKpi.Add(new clsVacancyKPI()
                    {
                        KPIId = ddlKpi.SelectedValue.ToInt32(),
                        KPIValue = txtKpiMarks.Text.Trim().ToDecimal(),
                        JobKpiID = rCount
                    });
                }
                rCount++;
            }
            JobKpi.Add(new clsVacancyKPI() { KPIId = -1, KPIValue = 0, JobKpiID = rCount });

            ViewState["TableKPI"] = JobKpi;
            gvKpi.DataSource = JobKpi;
            gvKpi.DataBind();
        }
        SetPreviousKpi();
        updKpiKra.Update();
        upnlKPI.Update();
    }

    private void DispEmptyControlMessage(string dispMsg, string val)
    {
        string msg = string.Empty;
        switch (val)
        {
            case "txt":
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : (dispMsg);
                mcMessage.WarningMessage(msg);
                mpeMessage.Show();
                break;
            case "ddl":
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : (dispMsg);
                mcMessage.WarningMessage(msg);
                mpeMessage.Show();
                break;

        }
    }

    private void SetPreviousKpi()
    {
        if (ViewState["TableKPI"] != null)
        {
            if (JobKpi.Count > 0)
            {
                int i = 0;
                foreach (clsVacancyKPI jKpi in JobKpi)
                {
                    TextBox txtKpiMarks = (TextBox)gvKpi.Rows[i].FindControl("txtKpiMarks");
                    DropDownList ddlKpi = (DropDownList)gvKpi.Rows[i].FindControl("ddlKpi");

                    ddlKpi.SelectedValue = jKpi.KPIId.ToString();
                    txtKpiMarks.Text = jKpi.KPIValue.ToString();
                    i++;
                }
            }
            else if (((DataTable)ViewState["TableKPI"]).Rows.Count > 0)
            {
                DataTable dtKPI = (DataTable)ViewState["TableKPI"];
                for (int iCounter = 0; iCounter < gvKpi.Rows.Count; iCounter++)
                {
                    TextBox txtKpiMarks = (TextBox)gvKpi.Rows[iCounter].FindControl("txtKpiMarks");
                    DropDownList ddlKpi = (DropDownList)gvKpi.Rows[iCounter].FindControl("ddlKpi");

                    ddlKpi.SelectedValue = dtKPI.Rows[iCounter]["KPIID"].ToString();
                    txtKpiMarks.Text = dtKPI.Rows[iCounter]["KPIValue"].ToString();
                }
            }
        }
    }

    protected void imgKpiDel_Click(object sender, ImageClickEventArgs e)
    {
        string msg = string.Empty;
        if (ViewState["TableKPI"] != null)
        {
            int Id = ((ImageButton)sender).CommandArgument.ToInt32();
            if (Id > 0)
            {
                //JobKpi.Remove(JobKpi.Find(t => t.Rowindex == Id));
                int rCount = 0;
                foreach (GridViewRow dr in gvKpi.Rows)
                {
                    TextBox txtKpiMarks = (TextBox)dr.FindControl("txtKpiMarks");
                    DropDownList ddlKpi = (DropDownList)dr.FindControl("ddlKpi");
                    if (rCount != Id)
                    {
                        if (txtKpiMarks != null && ddlKpi != null)
                        {
                            JobKpi.Add(new clsVacancyKPI()
                            {
                                KPIId = ddlKpi.SelectedValue.ToInt32(),
                                KPIValue = txtKpiMarks.Text.Trim().ToDecimal(),
                                JobKpiID = rCount
                            });
                        }
                    }
                    rCount++;
                }
                ViewState["TableKPI"] = JobKpi;
                gvKpi.DataSource = JobKpi;
                gvKpi.DataBind();
                SetPreviousKpi();
                upnlKPI.Update();
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                mcMessage.WarningMessage(msg);
                mpeMessage.Show();
            }
        }
    }

    protected void gvKpi_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            DropDownList ddlKpi = (DropDownList)e.Row.FindControl("ddlKpi");
            ImageButton btnAddKpi = (ImageButton)e.Row.FindControl("btnAddKpi");

            if (ddlKpi != null && btnAddKpi != null)
            {
                ddlKpi.DataSource = clsVacancyKPIKRA.GetKpi();
                ddlKpi.DataBind();               
            }
            if (JobKpi.Count > 0)
                btnAddKpi.Visible = (e.Row.RowIndex == JobKpi.Count - 1);
            else if (((DataTable)ViewState["TableKPI"]).Rows.Count > 0)
                btnAddKpi.Visible = (e.Row.RowIndex == ((DataTable)ViewState["TableKPI"]).Rows.Count - 1);
        }
    }

    protected void btnAddKpi_Click(object sender, ImageClickEventArgs e)
    {
        AddNewRowKPI();
    }
  
    #endregion

    #region KRA

    private void SetInitialRowKRA()
    {
        jobKra.Add(new clsVacancyKRA() { KRAId = 0, KRAValue = 0, JobKRAID = 0 });
        ViewState["TableKRA"] = jobKra;

        gvKra.DataSource = jobKra;
        gvKra.DataBind();
    }

    private void AddNewRowKRA()
    {
        if (ViewState["TableKRA"] != null)
        {
            int rCount = 0;
            jobKra = new List<clsVacancyKRA>();
            foreach (GridViewRow dr in gvKra.Rows)
            {
                TextBox txtKraMarks = (TextBox)dr.FindControl("txtKraMarks");
                DropDownList ddlKra = (DropDownList)dr.FindControl("ddlKra");

                if (txtKraMarks.Text == "0" || txtKraMarks.Text == string.Empty)
                {
                    DispEmptyControlMessage("Enter Marks for KRA", "txt");
                    break;
                }
                if (ddlKra.SelectedIndex == -1 || ddlKra.SelectedIndex == 0)
                {
                    DispEmptyControlMessage("Select KRA", "ddl");
                    break;
                }

                if (txtKraMarks != null && ddlKra != null)
                {
                    jobKra.Add(new clsVacancyKRA()
                    {
                        KRAId = ddlKra.SelectedValue.ToInt32(),
                        KRAValue = txtKraMarks.Text.Trim().ToDecimal(),
                        JobKRAID = rCount
                    });
                }
                rCount++;
            }
            jobKra.Add(new clsVacancyKRA() { KRAId = -1, KRAValue = 0, JobKRAID = rCount });

            ViewState["TableKRA"] = jobKra;
            gvKra.DataSource = jobKra;
            gvKra.DataBind();
        }
        SetPreviousKra();
        updKpiKra.Update();
    }

    private void SetPreviousKra()
    {
        if (ViewState["TableKRA"] != null)
        {
            if (jobKra.Count > 0)
            {
                int i = 0;
                foreach (clsVacancyKRA jKra in jobKra)
                {
                    TextBox txtKraMarks = (TextBox)gvKra.Rows[i].FindControl("txtKraMarks");
                    DropDownList ddlKra = (DropDownList)gvKra.Rows[i].FindControl("ddlKra");

                    ddlKra.SelectedValue = jKra.KRAId.ToString();
                    txtKraMarks.Text = jKra.KRAValue.ToString();
                    i++;
                }
            }
            else if (((DataTable)ViewState["TableKRA"]).Rows.Count > 0)
            {
                DataTable dtKRA = (DataTable)ViewState["TableKRA"];
                for (int iCounter = 0; iCounter < gvKra.Rows.Count; iCounter++)
                {
                    TextBox txtKraMarks = (TextBox)gvKra.Rows[iCounter].FindControl("txtKraMarks");
                    DropDownList ddlKra = (DropDownList)gvKra.Rows[iCounter].FindControl("ddlKra");

                    ddlKra.SelectedValue = dtKRA.Rows[iCounter]["KRAID"].ToString();
                    txtKraMarks.Text = dtKRA.Rows[iCounter]["KRAValue"].ToString();
                }
            }
        }
    }

    protected void btnAddKra_onClick(object sender, ImageClickEventArgs e)
    {
        AddNewRowKRA();
    }

    protected void imgKraDel_Click(object sender, ImageClickEventArgs e)
    {
        string msg = string.Empty;
        if (ViewState["TableKRA"] != null)
        {
            int Id = ((ImageButton)sender).CommandArgument.ToInt32();
            if (Id > 0)
            {
                //JobKpi.Remove(JobKpi.Find(t => t.Rowindex == Id));
                int rCount = 0;
                foreach (GridViewRow dr in gvKra.Rows)
                {
                    TextBox txtKraMarks = (TextBox)dr.FindControl("txtKraMarks");
                    DropDownList ddlKra = (DropDownList)dr.FindControl("ddlKra");
                    if (rCount != Id)
                    {
                        if (txtKraMarks != null && ddlKra != null)
                        {
                            jobKra.Add(new clsVacancyKRA()
                            {
                                KRAId = ddlKra.SelectedValue.ToInt32(),
                                KRAValue = txtKraMarks.Text.Trim().ToDecimal(),
                                JobKRAID = rCount
                            });
                        }
                    }
                    rCount++;
                }
                ViewState["TableKRA"] = jobKra;
                gvKra.DataSource = jobKra;
                gvKra.DataBind();
                SetPreviousKra();
                upnlKRA.Update();
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                mcMessage.WarningMessage(msg);
                mpeMessage.Show();
            }
        }
    }

    protected void gvKra_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            DropDownList ddlKra = (DropDownList)e.Row.FindControl("ddlKra");
            ImageButton btnAddKra = (ImageButton)e.Row.FindControl("btnAddKra");

            if (ddlKra != null && btnAddKra != null)
            {
                ddlKra.DataSource = clsVacancyKPIKRA.GetKra();
                ddlKra.DataBind();               
            }
            if (jobKra.Count > 0)
                btnAddKra.Visible = (e.Row.RowIndex == jobKra.Count - 1);
            else if (((DataTable)ViewState["TableKRA"]).Rows.Count > 0)
                btnAddKra.Visible = (e.Row.RowIndex == ((DataTable)ViewState["TableKRA"]).Rows.Count - 1);
        }
    }

    #endregion

    protected void btnSubmit_Click(object sender, EventArgs e)
     {
        if (Validate())
        {
            clsVacancyKPIKRA obj = new clsVacancyKPIKRA();
            obj.JobID = clsVacancyKPIKRA.ID;

            foreach (GridViewRow gv in gvKpi.Rows)
            {
                TextBox txtKpiMarks = (TextBox)gv.FindControl("txtKpiMarks");
                DropDownList ddlKpi = (DropDownList)gv.FindControl("ddlKpi");
                JobKpi.Add(new clsVacancyKPI()
                {
                    KPIId = ddlKpi.SelectedValue.ToInt32(),
                    KPIValue = txtKpiMarks.Text.Trim().ToDecimal()
                });
            }

            foreach (GridViewRow gv2 in gvKra.Rows)
            {
                TextBox txtKraMarks = (TextBox)gv2.FindControl("txtKraMarks");
                DropDownList ddlKra = (DropDownList)gv2.FindControl("ddlKra");
                jobKra.Add(new clsVacancyKRA()
                {
                    KRAId = ddlKra.SelectedValue.ToInt32(),
                    KRAValue = txtKraMarks.Text.Trim().ToDecimal()
                });
            }
            if (obj.DeleteKpiKra(clsVacancyKPIKRA.ID) > 0)
            {
                int id = obj.SaveKpi(JobKpi, clsVacancyKPIKRA.ID);
                int id2 = obj.SaveKra(jobKra, clsVacancyKPIKRA.ID);
                if (id > 0 && id2 > 0)
                {
                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Saved Successfully") : ("Saved Successfully");                    
                    mcMessage.WarningMessage(msg);
                    mpeMessage.Show();
                }
            }
        }
    }

    private bool Validate()
    {
        bool val = true;
        string msg = string.Empty;
        if (gvKpi.Rows.Count > 0)
        {
            for (int i = 0; i < gvKpi.Rows.Count; i++)
            {
                TextBox txtKpiMarks = (TextBox)gvKpi.Rows[i].FindControl("txtKpiMarks");
                DropDownList ddlKpi1 = (DropDownList)gvKpi.Rows[i].FindControl("ddlKpi");
                for (int j = i + 1; j < gvKpi.Rows.Count; j++)
                {
                    DropDownList ddlKpi2 = (DropDownList)gvKpi.Rows[j].FindControl("ddlKpi");
                    if (ddlKpi1.SelectedValue == ddlKpi2.SelectedValue)
                    {
                        val = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Duplication in KPI's are not allowed") : ("Duplication in KPI's are not allowed");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        break;
                    }                    
                }
                if (ddlKpi1.SelectedValue == "-1")
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Select a KPI") : ("Select a KPI");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
                if (txtKpiMarks.Text.Trim() == string.Empty || txtKpiMarks.Text.ToDecimal() == 0)
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Enter KPI marks") : ("Enter KPI marks");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
            }
        }
        if (gvKra.Rows.Count > 0)
        {
            for (int i = 0; i < gvKra.Rows.Count; i++)
            {
                TextBox txtKraMarks = (TextBox)gvKra.Rows[i].FindControl("txtKraMarks");
                DropDownList ddlKra1 = (DropDownList)gvKra.Rows[i].FindControl("ddlKra");
                for (int j = i + 1; j < gvKra.Rows.Count; j++)
                {
                    DropDownList ddlKra2 = (DropDownList)gvKra.Rows[j].FindControl("ddlKra");
                    if (ddlKra1.SelectedValue == ddlKra2.SelectedValue)
                    {
                        val = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Duplication in KRA's are not allowed") : ("Duplication in KRA's are not allowed");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        break;
                    }                    
                }
                if (ddlKra1.SelectedValue == "-1")
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Select a KRA") : ("Select a KRA");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
                if (txtKraMarks.Text.Trim() == string.Empty || txtKraMarks.Text.ToDecimal() == 0)
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Enter KRA marks") : ("Enter KRA marks");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
            }
        }
        return val;
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.Dispose();
        }
        finally
        { }

    }
}
