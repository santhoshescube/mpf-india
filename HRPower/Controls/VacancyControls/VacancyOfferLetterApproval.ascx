﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VacancyOfferLetterApproval.ascx.cs"
    Inherits="Controls_VacancyControls_VacancyOfferLetterApproval" %>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 520px; height: auto; max-height: 500px; overflow: scroll; background-color:White;
        overflow-x: hidden; overflow-y: scroll; margin-left: 350px;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="OfferApproval"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div style="margin-left: 25px; width: 680px;">
            <div style="width: 100%; overflow: hidden;">
                <asp:UpdatePanel ID="updTxtOfferNo" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 35%; height: 25px">
                                <asp:Literal ID="Literal39" runat="server" meta:resourcekey="OfferLetterApprovalRequired"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 60%; height: 31px">
                                <asp:RadioButtonList ID="rblOfferLevel" runat="server" RepeatDirection="Horizontal"
                                    OnSelectedIndexChanged="rblOfferLevel_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Value="1" meta:resourcekey="Yes"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="0" meta:resourcekey="No"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div id="divOfferNumber" style="float: left; width: 96%;" runat="server">
                            <div class="firstTrLeft" style="float: left; width: 35%; height: 29px">
                                <asp:Literal ID="Literal40" runat="server" meta:resourcekey="ApprovalLevel"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 60%; height: 29px">
                                <asp:TextBox ID="txtOfferLevel" runat="server" Width="50px" MaxLength="1" OnTextChanged="txtOfferLevel_OnTextChanged"
                                    AutoPostBack="true">
                                </asp:TextBox>
                                <asp:RangeValidator ID="RvOfferLevel" runat="server" ControlToValidate="txtOfferLevel" Type ="Integer"
                                    MinimumValue="1" MaximumValue="5" Display="Dynamic" CssClass="error" ValidationGroup="submit"
                                    meta:resourcekey="Levelnumbersshouldbeinbetweentherange"></asp:RangeValidator>
                            </div>
                        </div>
                        <div id="divChkEmployee" style="float: left; width: 96%;" runat="server">
                            <%--<div class="firstTrLeft" style="float: left; width: 35%; height: 29px">
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="SelectEmployeesFromOtherCompanies"></asp:Literal>
                            </div>--%>
                            <div class="firstTrLeft" style="float: left; width: 90%; height: 29px">
                                <asp:CheckBox ID="chkEmployee" runat="server" AutoPostBack="true" meta:resourcekey="SelectEmployeesFromOtherCompanies" TextAlign="Right" OnCheckedChanged="chkEmployee_CheckedChanged" />
                            </div>
                        </div>
                        <div style="height: auto;">
                            <div class="firstTrLeft" style="display: none; float: left; width: 75%; height: auto;"
                                runat="server" id="divOfferNo">
                                <div style="float: left; width: 90%; height: auto; max-height: 300px; overflow: scroll;">
                                    <asp:DataList ID="dlOfferNo" runat="server" Width="100%" CellPadding="3" DataKeyField="EmployeeId"
                                        OnItemDataBound="dlOfferNo_ItemDataBound" BorderColor="#307296" BorderWidth="1px"
                                        GridLines="Both">
                                        <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 95%; height: 50px;" align="center">
                                                        <asp:Literal ID="Literal40" runat="server" meta:resourcekey="PleaseSelectAuthoritiesforApproval"></asp:Literal>
                                                        <p style="font-size: 9px;">
                                                            (<asp:Literal ID="Literal41" runat="server" meta:resourcekey="FollowingareinthehierarchyToptoBottom"></asp:Literal>)</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle BackColor="#17acf6" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <table style="width: 100%">
                                                <tr style="height: 30px;">
                                                    <td>
                                                        <div style="width: 100%; margin-left: 144px;">
                                                            <asp:DropDownList ID="ddlAuthority" runat="server" Width="200px" DataTextField="EmployeeFullName"
                                                                AutoPostBack="true" DataValueField="EmployeeID" CssClass="dropdownlist" OnSelectedIndexChanged="ddlAuthority_OnSelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="rvAuthority" runat="server" CssClass="error" meta:resourcekey="PleaseselectanAuthority"
                                                        Display="Dynamic" ControlToValidate="ddlAuthority" Visible="false" InitialValue="-1"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hfEmpID" runat="server" />--%>
                                                            <%--ValidationGroup="submit"--%>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                            <div class="trRight" style="display: none; float: left; width: 75%; height: 29px"
                                id="divNoOffer" runat="server">
                                <div style="float: left; width: 40%; height: 29px" class="error">
                                    <asp:Literal ID="Literal42" runat="server" meta:resourcekey="NoEmployeeshavebeenAssociatedwithOfferLevels"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div style="width: 26%; float: left; margin-left: 164px; text-align: right;">
                            <asp:Button ID="btnSubmitOffer" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" CausesValidation="false"
                                OnClick="btnSubmitOffer_Click" />
                            <%--ValidationGroup="submit" --%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="height: auto">
                    <div>
                        <div style="display: none">
                            <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                        </div>
                        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                            BorderStyle="None">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"  X="400" Y="250"
                            Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
