﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;

public partial class Controls_VacancyControls_VacancyPayStructure : System.Web.UI.UserControl
{
    #region Declarations

    clsVacancyPayStructure objPay = new clsVacancyPayStructure();
    clsVacancyAddCriteria objCriteria = new clsVacancyAddCriteria();
    private string CurrentSelectedValue { get; set; }
    List<clsVacancySalary> VacancyPay = new List<clsVacancySalary>();

    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        int JobID = 0;

        if (Request.QueryString["JobID"] != null)
        {
            JobID = Convert.ToInt32(Server.UrlDecode(Request.QueryString["JobID"].ToString()));
        }
        if ((clsVacancyPayStructure.Designation != null && clsVacancyPayStructure.ID > 0))
        {
            if (txtPaystructure.Text.Trim() == string.Empty)
            {
                string txt= objPay.GetVacancyPayStructureName(clsVacancyPayStructure.ID).ToString();
                if (txt != "")
                    txtPaystructure.Text = txt;
                else
                    txtPaystructure.Text = clsVacancyPayStructure.Designation;
            } 
            hfMode.Value = "1";
            JobID = clsVacancyPayStructure.ID;
        }

        objPay.JobID = JobID;
        ViewState["JobID"] = JobID;

        if (!IsPostBack)
        {
            ClearControls();
            Bindinitials(JobID);
        }
        //ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        //ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    protected void dlParticular_SelectedIndexChanged(object source, EventArgs e)
    {
        Calculate(dlSalaryDetails, "txtAmount", true);
    }
    protected void imgAddParticular_Click(object sender, ImageClickEventArgs e)
    {
        decimal netAmount = 0;
        string msg = string.Empty;

        foreach (DataListItem item in dlSalaryDetails.Items)
        {
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            DropDownList dlParticular = (DropDownList)item.FindControl("dlParticular");

            if (txtAmount.Text != "" && Convert.ToDecimal(txtAmount.Text) > 0)
            {
                netAmount = netAmount + Convert.ToDecimal(txtAmount.Text);

                VacancyPay.Add(new clsVacancySalary()
                {
                    Amount = txtAmount.Text,
                    AdditionDeductionID = dlParticular.SelectedValue.ToInt32()
                });
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Enter Amount") : ("Enter Amount");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
        }
        VacancyPay.Add(new clsVacancySalary() { Amount = "0", AdditionDeductionID = 1 });

        ViewState["TablePay"] = VacancyPay;
        dlSalaryDetails.DataSource = VacancyPay;
        dlSalaryDetails.DataBind();

        //set values
        if (VacancyPay.Count > 0)
        {
            int i = 0;
            foreach (clsVacancySalary items in VacancyPay)
            {
                TextBox txtAmount = (TextBox)dlSalaryDetails.Items[i].FindControl("txtAmount");
                DropDownList dlParticular = (DropDownList)dlSalaryDetails.Items[i].FindControl("dlParticular");

                dlParticular.SelectedValue = items.AdditionDeductionID.ToString();
                txtAmount.Text = items.Amount.ToString();
                i++;
            }
        }

        lbAddition.Text = lbNetAmount.Text = netAmount.ToString();
        upnlPayStructure.Update();
    }
    protected void dlSalaryDetails_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;
        if (e.CommandName == "Remove")
        {
            if (objCriteria.IsVacancyScheduled(ViewState["JobID"].ToInt32()))//Pass JobID
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إزالة وجه الخصوص، بدأت عملية مقابلة .") : ("Cannot remove particular, Interview process started .");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
            else
            {
                if (e.Item.ItemIndex > 0)
                {
                    foreach (DataListItem item in dlSalaryDetails.Items)
                    {
                        if (item != e.Item)
                        {
                            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                            DropDownList dlParticular = (DropDownList)item.FindControl("dlParticular");

                            VacancyPay.Add(new clsVacancySalary()
                            {
                                AdditionDeductionID = dlParticular.Text.ToInt32(),
                                Amount = txtAmount.Text
                            });
                        }
                    }
                    ViewState["TablePay"] = VacancyPay;
                    dlSalaryDetails.DataSource = VacancyPay;
                    dlSalaryDetails.DataBind();

                    int i = 0;
                    foreach (clsVacancySalary item in VacancyPay)
                    {
                        DropDownList dlParticular = (DropDownList)dlSalaryDetails.Items[i].FindControl("dlParticular");
                        dlParticular.SelectedValue = item.AdditionDeductionID.ToString();
                        i++;
                    }
                    decimal netAmount = 0;
                    netAmount = Calculate(dlSalaryDetails, "txtAmount", true);
                    lbAddition.Text = lbNetAmount.Text = netAmount.ToString();
                }
                else
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                    mcMessage.WarningMessage(msg);
                    mpeMessage.Show();
                }
            }
        }
    }
    protected void dlSalaryDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DropDownList dlParticular = (DropDownList)e.Item.FindControl("dlParticular");
        if (dlParticular != null && dlParticular.SelectedValue.ToInt32() > -1)
        {
            dlParticular.DataSource = objPay.GetAdditionDeduction();
            dlParticular.DataBind();
        }
        if (e.Item.ItemIndex >= 0)
        {
            ImageButton imgAddParticular = (ImageButton)e.Item.FindControl("imgAddParticular");
            imgAddParticular.Visible = (e.Item.ItemIndex == VacancyPay.Count - 1);
            if (e.Item.ItemIndex == 0)
            {
                if (VacancyPay.Count == 1)
                    imgAddParticular.Visible = (e.Item.ItemIndex == VacancyPay.Count - 1);
                else
                    imgAddParticular.Visible = (e.Item.ItemIndex == VacancyPay.Count);
            }
            else
                imgAddParticular.Visible = (e.Item.ItemIndex == VacancyPay.Count - 1);
        }
    }

    protected void btnSubmitPay_Click(object sender, EventArgs e)
    {
        string msg = string.Empty;
        if (ValidateData())
        {
            Save();
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث المهمة بنجاح") : ("Pay structure saved successfully");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
    }
    protected void btnPayStructure_Click(object sender, EventArgs e)
    {
        //if (ddlPayStructure != null)
        //{
        //    ReferenceControlNew1.ClearAll();
        //    ReferenceControlNew1.TableName = "HRJobSalaryMaster";
        //    ReferenceControlNew1.DataTextField = "Paystructure";
        //    ReferenceControlNew1.DataValueField = "DesignationID";
        //    ReferenceControlNew1.FunctionName = "BindPayStructure";
        //    ReferenceControlNew1.SelectedValue = ddlPayStructure.SelectedValue;
        //    ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("HR Power") : ("HR Power");
        //    //ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
        //    ReferenceControlNew1.PopulateData();

        //    mdlPopUp.Show();
        //    updPay.Update();           
        //}
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.Dispose();
        }
        finally
        {

        }
    }

    #endregion

    #region Methods
    private void ClearControls()
    {
        dlSalaryDetails.DataSource = null;
        dlSalaryDetails.DataBind();
        ViewState["TablePay"] = null;
        lbAddition.Text = lbNetAmount.Text = string.Empty;
        VacancyPay.Clear();  
    }
    private void Bindinitials(int JobID)
    {
        //Binds Salary Details
        BindPaymentCalculationMode();
        DataSet ds = objPay.GetAllPayStructures();
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                    txtPaystructure.Text = dt.Rows[0]["PayStructure"].ToString();

                DataTable dtList = ds.Tables[1];
                if (dtList.Rows.Count > 0)
                {
                    dlSalaryDetails.DataSource = dtList;
                    dlSalaryDetails.DataBind();
                    if (dlSalaryDetails.Items.Count > 0)
                    {
                        decimal amount = 0;
                        for (int i = 0; i < dlSalaryDetails.Items.Count; i++)
                        {
                            DropDownList dlParticular = (DropDownList)dlSalaryDetails.Items[i].FindControl("dlParticular");
                            TextBox txtAmount = (TextBox)dlSalaryDetails.Items[i].FindControl("txtAmount");
                            Label lbAddDedId = (Label)dlSalaryDetails.Items[i].FindControl("lbAddDedId");

                            if (dlParticular != null)
                            {
                                if (dlParticular.Items.Count > 0)
                                {
                                    dlParticular.SelectedValue = dtList.Rows[i]["AdditionDeductionID"].ToString();
                                    txtAmount.Text = dtList.Rows[i]["Amount"].ToString();
                                    amount = amount + dtList.Rows[i]["Amount"].ToDecimal();
                                }
                            }
                        }
                        lbAddition.Text = lbNetAmount.Text = amount.ToString();
                    }
                }
                //BindPayStructure();
            }
            else
            {
                VacancyPay.Add(new clsVacancySalary() { Amount = "0", AdditionDeductionID = 1 });
                ViewState["TablePay"] = VacancyPay;
                dlSalaryDetails.DataSource = VacancyPay;
                dlSalaryDetails.DataBind();
                lbAddition.Text = lbNetAmount.Text = "0";
            }
        }
        else
        {
            VacancyPay.Add(new clsVacancySalary() { Amount = "0", AdditionDeductionID = 1 });
            ViewState["TablePay"] = VacancyPay;
            dlSalaryDetails.DataSource = VacancyPay;
            dlSalaryDetails.DataBind();
            lbAddition.Text = lbNetAmount.Text = "0";
        }
        upnlPayStructure.Update();
    }
    //private void BindPayStructure()
    //{
    //    ddlPayStructure.DataSource = objPay.GetPayStructureName(1);
    //    ddlPayStructure.DataBind();
    //    ddlPayStructure.Items.Insert(0, new ListItem(str, "-1"));
    //}
    private void BindPaymentCalculationMode()
    {
        ddlPaymentMode.DataSource = objPay.GetPaymentClassificationMode();
        ddlPaymentMode.DataBind();
    }
    //void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    //{
    //    CurrentSelectedValue = SelectedValue;
    //    Type thisType = this.GetType();
    //    MethodInfo theMethod = thisType.GetMethod(functionName);
    //    theMethod.Invoke(this, null);
    //}

    //void ReferenceControlNew1_onRefresh()
    //{
    //    mdlPopUp.Show();
    //}

    private decimal Calculate(DataList dList, string control, bool isAddition)
    {
        decimal netAmount = 0;
        decimal dAmount = 0;

        foreach (DataListItem item in dList.Items)
        {
            TextBox txtAmount = (TextBox)item.FindControl(control);

            if (isAddition)
            {
                DropDownList dlParticular = (DropDownList)item.FindControl("dlParticular");
                if (objPay.GetAdditionORDeduction(Convert.ToInt32(dlParticular.SelectedValue)))

                    netAmount = netAmount + (txtAmount.Text == "" ? 0 : Convert.ToDecimal(txtAmount.Text));
                else
                {
                    dAmount = dAmount + (txtAmount.Text == "" ? 0 : Convert.ToDecimal(txtAmount.Text));
                    netAmount = netAmount - (txtAmount.Text == "" ? 0 : Convert.ToDecimal(txtAmount.Text));
                }
            }
            else
            {
                if (txtAmount.Text != "")
                    netAmount = netAmount + (txtAmount.Text == "" ? 0 : Convert.ToDecimal(txtAmount.Text));
            }
        }
        return netAmount;
    }
    protected void CalculateTotalAmount(object sender, EventArgs e)
    {
        decimal netAmount = Calculate(dlSalaryDetails, "txtAmount", true);
        lbNetAmount.Text = netAmount.ToString();
        lbAddition.Text = netAmount.ToString();
    }

    private bool ValidateData()
    {
        string msg = string.Empty;
        bool val = true;
        objPay.PayStructure = txtPaystructure.Text.ToString();
        if (objPay.DoesPayStructureExist() > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("هيكل الأجور موجود") : ("Pay Structure Name Exist");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            val = false;
        }
        else if (txtPaystructure.Text == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("أدخل اسم هيكل الأجور") : ("Enter Pay Structure Name");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            val = false;
        }
        if (dlSalaryDetails.Items.Count > 0)
        {
            for (int i = 0; i < dlSalaryDetails.Items.Count; i++)
            {
                DropDownList dlParticular = (DropDownList)dlSalaryDetails.Items[i].FindControl("dlParticular");
                TextBox txtAmount = (TextBox)dlSalaryDetails.Items[i].FindControl("txtAmount");
                for (int j = i + 1; j < dlSalaryDetails.Items.Count; j++)
                {
                    DropDownList dlNParticular = (DropDownList)dlSalaryDetails.Items[j].FindControl("dlParticular");

                    if (dlParticular.SelectedValue == dlNParticular.SelectedValue)
                    {

                        val = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تكرار معين لا يسمح") : ("Duplicate particular not allowed");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        break;
                    }
                }
                if (dlSalaryDetails.Items.Count > 1)
                {
                    if (txtAmount.Text.ToDecimal() == 0)
                    {
                        val = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("من فضلك ادخل المبلغ") : ("Please enter Amount");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        break;
                    }
                }
            }
        }
        return val;
    }
    private void Save()
    {
        objPay.DeletePayStructure();
        objPay.PayStructure = txtPaystructure.Text.Trim();      
        objPay.PaymentClassificationID = ddlPaymentMode.SelectedValue.ToInt32();
        objPay.GrossPay = lbAddition.Text.ToInt32();
        int SalaryID = objPay.InsertSalaryMaster();
        if (SalaryID > 0)
        {
            foreach (DataListItem item in dlSalaryDetails.Items)
            {
                DropDownList dlParticular = (DropDownList)item.FindControl("dlParticular");
                TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                Label lbSalaryID = (Label)item.FindControl("lbSalaryID");

                if (dlParticular != null)
                {
                    objPay.AdditionDeductionID = Convert.ToInt32(dlParticular.SelectedValue);
                    objPay.PayCalculationTypeID = Convert.ToInt32(dlParticular.SelectedValue);
                }
                objPay.Amount = txtAmount.Text.ToString();
                objPay.SalaryID = SalaryID;
                objPay.InsertSalaryDetail();
            }
        }

    }
    #endregion
}
