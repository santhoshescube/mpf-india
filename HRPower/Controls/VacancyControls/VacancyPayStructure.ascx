﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VacancyPayStructure.ascx.cs"
    Inherits="Controls_VacancyControls_VacancyPayStructure" %>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 520px !important ; height: auto; max-height: 500px; overflow: scroll;  background-color:White;
        overflow-x: hidden; overflow-y: scroll; margin-left: 350px;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="PayStructure"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div style="margin-left: 25px; width: 680px;">
            <div style="width: 100%; overflow: hidden;">
                <asp:UpdatePanel ID="upnlPayStructure" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="div3" style="float: left; width: 96%;" runat="server">
                            <div class="firstTrLeft" style="float: left; width: 35%; height: 29px">
                                <asp:Literal ID="Literal34" runat="server" meta:resourcekey="PayStructureName"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 40%; height: 29px">
                                <%-- <asp:DropDownList ID="ddlPayStructure" DataTextField="Paystructure" DataValueField="DesignationID"
                            runat="server" Width="200px">
                        </asp:DropDownList>
                        <asp:Button ID="btnPayStructure" runat="server" Text="..." CausesValidation="False"
                            CssClass="referencebutton" OnClick="btnPayStructure_Click" />--%>
                                <asp:TextBox ID="txtPaystructure" runat="server" Width="200px" MaxLength="75">
                                </asp:TextBox>
                            </div>
                        </div>
                        <div id="div1" style="float: left; width: 96%;" runat="server">
                            <div class="firstTrLeft" style="float: left; width: 35%; height: 29px">
                                <asp:Literal ID="Literal35" runat="server" meta:resourcekey="PaymentCalculationMode"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 40%; height: 29px">
                                <asp:DropDownList ID="ddlPaymentMode" runat="server" DataTextField="PaymentClassification"
                                    Enabled="false" DataValueField="PaymentClassificationID" Width="100px" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <asp:DataList ID="dlSalaryDetails" runat="server" Width="67%" CellPadding="10" BorderColor="#307296"
                            OnItemCommand="dlSalaryDetails_ItemCommand" OnItemDataBound="dlSalaryDetails_ItemDataBound" ItemStyle-BackColor="White"
                            BorderWidth="1">
                            <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                            <HeaderTemplate>
                                <div style="width: 95%; font-size: 11px; float: left;" class="datalistheader">
                                    <div style="width: 40%; float: left;">
                                        <asp:Literal ID="Literal35" runat="server" meta:resourcekey="Particular"></asp:Literal>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        &nbsp;<asp:Literal ID="Literal36" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                    </div>
                                    <div style="width: 20%; float: right;">
                                        &nbsp;
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        &nbsp;
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="width: 100%; float: left; padding-top: 4px">
                                    <div style="width: 40%; float: left;">
                                        <asp:DropDownList ID="dlParticular" runat="server" Width="145px" DataTextField="description"
                                            DataValueField="id" CssClass="dropdownlist" OnSelectedIndexChanged="dlParticular_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 20%; float: left; margin-left: 10px;">
                                        <asp:TextBox runat="server" ID="txtAmount" Width="60px" MaxLength="10" CssClass="textbox"
                                            AutoPostBack="true" OnTextChanged="CalculateTotalAmount" Text='<%# Eval("Amount") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error"
                                            Display="Dynamic" ControlToValidate="txtAmount" ValidationGroup="AddnewP" meta:resourcekey="Enteramount"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter amount"--%>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbetxtNoOfMonths" runat="server"
                                            FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtAmount">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                    <div style="width: 20%; float: left;">
                                        <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/Delete18x18.png"
                                            meta:resourcekey="Remove" CommandName="Remove" CausesValidation="false" CommandArgument='<%# Eval("AdditionDeductionID") %>' /><%--ToolTip="Remove"--%>
                                    </div>
                                    <div style="width: 20%; float: left; margin-left: 40px;">
                                        <asp:ImageButton ID="imgAddParticular" runat="server" ImageUrl="~/images/Addition Deduction.png"
                                            meta:resourcekey="Addnew" CausesValidation="false" ValidationGroup="AddnewP"
                                            OnClick="imgAddParticular_Click" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                        <div class="firstTrLeft" style="float: left; width: 30%; height: 29px">
                            <asp:Literal ID="Literal37" runat="server" meta:resourcekey="GrossSalary"></asp:Literal>
                            <asp:Label ID="lbAddition" runat="server"></asp:Label>
                        </div>
                        <div class="firstTrRight" style="float: left; width: 29%; height: 29px">
                            &nbsp; &nbsp;
                            <asp:Literal ID="Literal38" runat="server" meta:resourcekey="NetAmount"></asp:Literal>
                            <asp:Label ID="lbNetAmount" runat="server"></asp:Label>
                        </div>
                        <div style="width: 44%; float: left; margin-left: 156px; text-align: right;">
                            <asp:Button ID="btnSubmitPay" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" CausesValidation="false"
                                OnClick="btnSubmitPay_Click" />
                            <asp:HiddenField ID="hfMode" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="height: auto">
                    <div>
                        <div style="display: none">
                            <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                        </div>
                        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                            BorderStyle="None">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"  X="450" Y="350"
                            Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </div>
                </div>
                <asp:UpdatePanel ID="updPay" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnPopPay" runat="server" />
                        </div>
                        <div id="divPopup" runat="server" style="display: none;">
                            <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUp" />
                        </div>
                        <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUp" runat="server" TargetControlID="btnPopPay"
                            PopupControlID="divPopup" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
