﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;

public partial class Controls_VacancyControls_VacancyCriteria : System.Web.UI.UserControl
{
    #region Declarations

    private string CurrentSelectedValue { get; set; }
    List<clsVacancyCriteria> VacancyCriteria = new List<clsVacancyCriteria>();
    List<clsVacancyCriteria> VacancyCriteriaMul = new List<clsVacancyCriteria>();
    clsVacancyAddCriteria objCriteria = new clsVacancyAddCriteria();
    string str;

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        int JobID = 0;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (clsVacancyAddCriteria.ID > 0)
        {
            JobID = clsVacancyAddCriteria.ID;
            hfMode.Value = "1";
        }        
        else if (Request.QueryString["JobID"] != null)
        {
            JobID = Convert.ToInt32(Server.UrlDecode(Request.QueryString["JobID"].ToString()));
            hfMode.Value = "1";
        }
        else
        {
            hfMode.Value = "0";           
        }
        objCriteria.JobID = JobID;
        ViewState["JobID"] = JobID;        
        if (!IsPostBack)
        {
            ClearControls();
            Bindinitials(JobID);
        }

        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    /// <summary>
    /// Blocks and displays contents based on the job type
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void rblType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblType.SelectedValue == "0")
        {
            if (ViewState["TableCriteria"] != null)
            {
                Bindinitials(ViewState["JobID"].ToInt32());
            }
            else
                BindEmptyDataList();
        }
        else
        {
            if (ViewState["temp"] != null)
            {
                Bindinitials(ViewState["JobID"].ToInt32());
            }
            else
                BindEmptyDataList();
        }        
        UpdDetails.Update();
    }

    #region Boardlevel Events
    protected void dlSearchCriteria_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;
        if (e.CommandName == "Remove")
        {
            if (objCriteria.IsVacancyScheduled(ViewState["JobID"].ToInt32()))
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إزالة المعايير، التي عملية مقابلة") : ("Cannot remove Criteria, Interview process started .");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
            else
            {
                if (e.Item.ItemIndex > 0)
                {
                    foreach (DataListItem item in dlSearchCriteria.Items)
                    {
                        if (item != e.Item)
                        {
                            DropDownList dlCriteria = (DropDownList)item.FindControl("dlCriteria");
                            TextBox txtWeightage = (TextBox)item.FindControl("txtWeightage");
                            Label lbcriteriaID = (Label)e.Item.FindControl("lbcriteriaID");

                            VacancyCriteria.Add(new clsVacancyCriteria()
                            {
                                Weightage = txtWeightage.Text.ToDecimal(),
                                CriteriaId = dlCriteria.SelectedValue.ToInt32()
                            });
                            lbcriteriaID.Text = dlCriteria.SelectedValue.ToString();
                        }
                    }
                    ViewState["TableCriteria"] = VacancyCriteria;
                    dlSearchCriteria.DataSource = VacancyCriteria;
                    dlSearchCriteria.DataBind();
                    int i = 0;
                    foreach (clsVacancyCriteria item in VacancyCriteria)
                    {
                        DropDownList dlCriteria = (DropDownList)dlSearchCriteria.Items[i].FindControl("dlCriteria");
                        dlCriteria.SelectedValue = item.CriteriaId.ToString();
                        i++;
                    }

                    decimal weigtage = Calculate(dlSearchCriteria, "txtWeightage", false);
                    lbTotalWeightage.Text = weigtage.ToString();
                }
                else
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                }
            }
        }
    }
    protected void dlSearchCriteria_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DropDownList dlCriteria = (DropDownList)e.Item.FindControl("dlCriteria");
        if (dlCriteria != null && dlCriteria.SelectedValue.ToInt32() > -1)
        {
            dlCriteria.DataSource = objCriteria.GetCriterias();
            dlCriteria.DataBind();
            dlCriteria.Items.Insert(0, new ListItem(str, "-1"));
        }

        if (e.Item.ItemIndex >= 0)
        {
            ImageButton imgAddnewCriteria = (ImageButton)e.Item.FindControl("imgAddnewCriteria");
            imgAddnewCriteria.Visible = (e.Item.ItemIndex == VacancyCriteria.Count - 1);
            if (e.Item.ItemIndex == 0)
            {
                if (VacancyCriteria.Count == 1)
                    imgAddnewCriteria.Visible = (e.Item.ItemIndex == VacancyCriteria.Count - 1);
                else
                    imgAddnewCriteria.Visible = (e.Item.ItemIndex == VacancyCriteria.Count);
            }
            else
                imgAddnewCriteria.Visible = (e.Item.ItemIndex == VacancyCriteria.Count - 1);
        }
    }
    protected void imgAddnewCriteria_Click(object sender, ImageClickEventArgs e)
    {
        decimal weigtage = 0;
        string msg = string.Empty;

        if (lbTotalWeightage.Text.ToDecimal() > 100)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("Total weightage must be equal to 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        else
        {
            foreach (DataListItem item in dlSearchCriteria.Items)
            {
                TextBox txtWeightage = (TextBox)item.FindControl("txtWeightage");
                DropDownList dlCriteria = (DropDownList)item.FindControl("dlCriteria");
                Label lbcriteriaID = (Label)item.FindControl("lbcriteriaID");

                if (txtWeightage.Text != "" && Convert.ToDecimal(txtWeightage.Text) > 0)
                {
                    weigtage = weigtage + Convert.ToDecimal(txtWeightage.Text);
                    VacancyCriteria.Add(new clsVacancyCriteria()
                    {
                        Weightage = txtWeightage.Text.ToDecimal(),
                        CriteriaId = dlCriteria.SelectedValue.ToInt32(),
                        Criteria = dlCriteria.SelectedItem.Text
                    });
                }
                else
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("enter Weightage");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;
                }
            }
            VacancyCriteria.Add(new clsVacancyCriteria() { Weightage = 0, CriteriaId = -1, Criteria = "Select" });

            ViewState["TableCriteria"] = VacancyCriteria;
            dlSearchCriteria.DataSource = VacancyCriteria;
            dlSearchCriteria.DataBind();

            //set values
            if (VacancyCriteria.Count > 0)
            {
                int i = 0;
                foreach (clsVacancyCriteria items in VacancyCriteria)
                {
                    TextBox txtWeightage = (TextBox)dlSearchCriteria.Items[i].FindControl("txtWeightage");
                    DropDownList dlCriteria = (DropDownList)dlSearchCriteria.Items[i].FindControl("dlCriteria");
                    Label lbcriteriaID = (Label)dlSearchCriteria.Items[i].FindControl("lbcriteriaID");

                    dlCriteria.SelectedValue = items.CriteriaId.ToString();
                    txtWeightage.Text = items.Weightage.ToString();
                    lbcriteriaID.Text = items.CriteriaId.ToString();
                    i++;
                }
            }            

            lbTotalWeightage.Text = weigtage.ToString();
            upnlCriteria.Update();
        }
    }
    #endregion

    #region Multilevel Events
    protected void btnLevel_Click(object sender, EventArgs e)
    {
        if (ddlLevel != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "HRJobLevelReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataValueField = "JobLevelId";
            ReferenceControlNew1.FunctionName = "BindInterviewLevels";
            ReferenceControlNew1.SelectedValue = ddlLevel.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("المستوى الوظيفي") : ("Job Level");
            ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUp.Show();
            updLevel.Update();
            updJobLevel.Update();
        }
    }
    protected void lnkCriteria_Click(object sender, EventArgs e)
    {
        divMultiLevel.Style["display"] = "block";
        DataTable dt1 = null;
        dt1 = new DataTable();
        dt1.Columns.Add("Criteria", typeof(string));
        dt1.Columns.Add("Weightage", typeof(int));
        dt1.Columns.Add("CriteriaId", typeof(int));
        int JoblevelID = ddlLevel.SelectedValue.ToInt32();
        DataTable dt = (DataTable)ViewState["temp"];

        if (dt != null)
        {
            DataRow[] row1 = dt.Select("JobLevelID=" + JoblevelID);
            if (row1.Length > 0)
            {
                DataRow[] result = dt.Select("JobLevelID= " + JoblevelID);
                foreach (DataRow dr in result)
                    dt1.ImportRow(dr);
                lbTotalWeightage1.Text = "100";
            }
            else
            {
                DataRow dr1 = dt1.NewRow();
                dt1.Rows.Add(dr1);
                lbTotalWeightage1.Text = "0";
            }

        }
        else
        {
            DataRow dr1 = dt1.NewRow();
            dt1.Rows.Add(dr1);
            lbTotalWeightage1.Text = "0";
        }

        dlSearchCriteria1.DataSource = dt1;
        dlSearchCriteria1.DataBind();

        foreach (DataListItem row in dlSearchCriteria1.Items)
        {
            HiddenField hfdCriteria = (HiddenField)row.FindControl("hfdCriteria");
            DropDownList dlCriteria1 = (DropDownList)row.FindControl("dlCriteria1");
            dlCriteria1.SelectedValue = hfdCriteria.Value;
        }
        //lbTotalWeightage1.Text = "0";
        dlSearchCriteria1.Visible = true;
        divLevel.Style["display"] = "block";
        upnlCriteria.Update();
    }
    protected void dlSearchCriteriapop_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;
        if (e.CommandName == "Remove")
        {
            if (objCriteria.IsVacancyScheduled(0))
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إزالة المعايير، التي عملية مقابلة") : ("Cannot remove Criteria, Interview process started .");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
            else
            {
                if (e.Item.ItemIndex > 0)
                {
                    foreach (DataListItem item in dlSearchCriteria1.Items)
                    {
                        if (item != e.Item)
                        {
                            DropDownList dlCriteria1 = (DropDownList)item.FindControl("dlCriteria1");
                            TextBox txtWeightage1 = (TextBox)item.FindControl("txtWeightage1");
                            Label lbcriteriaID1 = (Label)e.Item.FindControl("lbcriteriaID1");

                            VacancyCriteriaMul.Add(new clsVacancyCriteria()
                            {
                                Weightage = txtWeightage1.Text.ToDecimal(),
                                CriteriaId = dlCriteria1.SelectedValue.ToInt32()
                            });
                        }
                    }
                    ViewState["TableCriteriaMul"] = VacancyCriteriaMul;
                    dlSearchCriteria1.DataSource = VacancyCriteriaMul;
                    dlSearchCriteria1.DataBind();

                    int i = 0;
                    foreach (clsVacancyCriteria item in VacancyCriteriaMul)
                    {
                        DropDownList dlCriteria1 = (DropDownList)dlSearchCriteria1.Items[i].FindControl("dlCriteria1");
                        dlCriteria1.SelectedValue = item.CriteriaId.ToString();
                        i++;
                    }

                    decimal weigtage = Calculate(dlSearchCriteria1, "txtWeightage1", false);
                    lbTotalWeightage1.Text = weigtage.ToString();
                }
                else
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                }
            }
        }
    }
    protected void dlSearchCriteriapop_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DropDownList dlCriteria1 = (DropDownList)e.Item.FindControl("dlCriteria1");
        if (dlCriteria1 != null && dlCriteria1.SelectedValue.ToInt32() > -1)
        {
            dlCriteria1.DataSource = objCriteria.GetCriterias();
            dlCriteria1.DataBind();
            dlCriteria1.Items.Insert(0, new ListItem(str, "-1"));
        }

        if (e.Item.ItemIndex >= 0)
        {
            ImageButton imgAddnewCriteria1 = (ImageButton)e.Item.FindControl("imgAddnewCriteria1");
            imgAddnewCriteria1.Visible = (e.Item.ItemIndex == VacancyCriteriaMul.Count - 1);
            if (e.Item.ItemIndex == 0)
            {
                if (VacancyCriteriaMul.Count == 1)
                    imgAddnewCriteria1.Visible = (e.Item.ItemIndex == VacancyCriteriaMul.Count - 1);
                else
                    imgAddnewCriteria1.Visible = (e.Item.ItemIndex == VacancyCriteriaMul.Count);
            }
            else
                imgAddnewCriteria1.Visible = (e.Item.ItemIndex == VacancyCriteriaMul.Count - 1);
        }
    }
    protected void imgAddnewCriteriapop_Click(object sender, ImageClickEventArgs e)
    {
        decimal weigtage = 0;
        string msg = string.Empty;

        if (lbTotalWeightage1.Text.ToDecimal() > 100)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("Total weightage must be equal to 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        foreach (DataListItem item in dlSearchCriteria1.Items)
        {
            TextBox txtWeightage1 = (TextBox)item.FindControl("txtWeightage1");
            DropDownList dlCriteria1 = (DropDownList)item.FindControl("dlCriteria1");
            Label lbcriteriaID1 = (Label)item.FindControl("lbcriteriaID1");

            if (txtWeightage1.Text != "" && Convert.ToDecimal(txtWeightage1.Text) > 0)
            {
                weigtage = weigtage + Convert.ToDecimal(txtWeightage1.Text);

                VacancyCriteriaMul.Add(new clsVacancyCriteria()
                {
                    Weightage = txtWeightage1.Text.ToDecimal(),
                    CriteriaId = dlCriteria1.SelectedValue.ToInt32(),
                    Criteria = dlCriteria1.SelectedItem.Text
                });
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("enter Weightage");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
        }
        VacancyCriteriaMul.Add(new clsVacancyCriteria() { Weightage = 0, JobDetailID = 1, CriteriaId = -1, Criteria = "Select" });

        ViewState["TableCriteriaMul"] = VacancyCriteriaMul;
        dlSearchCriteria1.DataSource = VacancyCriteriaMul;
        dlSearchCriteria1.DataBind();
        //set values
        if (VacancyCriteriaMul.Count > 0)
        {
            int i = 0;
            foreach (clsVacancyCriteria items in VacancyCriteriaMul)
            {
                TextBox txtWeightage1 = (TextBox)dlSearchCriteria1.Items[i].FindControl("txtWeightage1");
                DropDownList dlCriteria1 = (DropDownList)dlSearchCriteria1.Items[i].FindControl("dlCriteria1");
                Label lbcriteriaID1 = (Label)dlSearchCriteria1.Items[i].FindControl("lbcriteriaID1");

                dlCriteria1.SelectedValue = items.CriteriaId.ToString();
                txtWeightage1.Text = items.Weightage.ToString();
                lbcriteriaID1.Text = items.CriteriaId.ToString();
                i++;
            }
        }

        lbTotalWeightage1.Text = weigtage.ToString();
        updCriteriapopup.Update();
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        lnkCriteria_Click(null, null);
    }

    //Adding/Updating Existing and new level criterias
    protected void btnSave_Click(object sender, EventArgs e)
    {
        int JobLevelID = ddlLevel.SelectedValue.ToInt32();
        string msg = string.Empty;
        bool validatechk = true;
        if (dlSearchCriteria1.Items.Count > 0)
        {
            for (int i = 0; i < dlSearchCriteria1.Items.Count; i++)
            {
                DropDownList dlCriteria1 = (DropDownList)dlSearchCriteria1.Items[i].FindControl("dlCriteria1");
                TextBox txtWeightage1 = (TextBox)dlSearchCriteria1.Items[i].FindControl("txtWeightage1");
                for (int j = i + 1; j < dlSearchCriteria1.Items.Count; j++)
                {
                    DropDownList dlNCriteria1 = (DropDownList)dlSearchCriteria1.Items[j].FindControl("dlCriteria1");

                    if (dlCriteria1.SelectedValue == dlNCriteria1.SelectedValue)
                    {
                        validatechk = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير مكررة غير مسموح") : ("Duplicate criteria not allowed");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        return;
                    }                   
                }
                if (txtWeightage1.Text.ToInt32() == 0 || txtWeightage1.Text == "")
                {
                    validatechk = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الترجيح") : ("Please enter weightage");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;
                }
                if (dlCriteria1.SelectedValue == "-1")
                {
                    validatechk = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير مكررة غير مسموح") : ("Select a Criteria");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;
                }
            }
        }

        if (lbTotalWeightage1.Text.ToInt32() < 100)
        {
            validatechk = false;
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("مجموع الترجيح هو أقل من 100") : ("Total Weightage is less than 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }

        else if (lbTotalWeightage1.Text.ToInt32() > 100)
        {
            validatechk = false;
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("مجموع الترجيح أكبر من 100") : ("Total Weightage is greater than 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }

        bool isValidate = true;
        if (isValidate)
        {
            DataTable dtTemp = new DataTable();
            dtTemp.Columns.Add("Count");
            dtTemp.Columns.Add("Criteria");
            dtTemp.Columns.Add("Weightage");
            dtTemp.Columns.Add("CriteriaID");
            dtTemp.Columns.Add("LevelName");
            dtTemp.Columns.Add("JobLevelID");

            int j = 0, count = 0;
            count = dlSearchCriteria1.Items.Count + j;
            int LevelID = ddlLevel.SelectedValue.ToInt32();

            DataTable dtTemp1 = (DataTable)ViewState["temp"];
            if (dtTemp1 != null)
            {
                int GridCount = dtTemp1.Rows.Count - 1;

                for (int i = GridCount; i >= 0; i--)
                {
                    if (LevelID == dtTemp1.Rows[i]["JobLevelID"].ToInt32())
                        dtTemp1.Rows.RemoveAt(i);
                }
            }

            if (dtTemp1 != null) dtTemp = dtTemp1;
            foreach (DataListItem item in dlSearchCriteria1.Items)
            {
                if (ViewState["temp"] != null)
                    dtTemp = ((DataTable)ViewState["temp"]);
                else
                    ViewState["temp"] = dtTemp;

                if (j <= count)
                {
                    DataRow dr = dtTemp.NewRow();
                    DropDownList dlCriteria1 = (DropDownList)item.FindControl("dlCriteria1");
                    TextBox txtWeightage1 = (TextBox)item.FindControl("txtWeightage1");

                    Label lbcriteriaID1 = (Label)item.FindControl("lbcriteriaID1");
                    Label lblcid = (Label)dlCriteriatemp.FindControl("lblcid");
                    Label lbllid = (Label)dlCriteriatemp.FindControl("lbllid");

                    dr["Count"] = j;
                    dr["Criteria"] = dlCriteria1.SelectedItem.Text;
                    dr["Weightage"] = txtWeightage1.Text.ToString();
                    dr["CriteriaID"] = dlCriteria1.SelectedValue;
                    dr["LevelName"] = ddlLevel.SelectedItem.Text;
                    dr["JobLevelID"] = ddlLevel.SelectedValue;
                    dtTemp.Rows.Add(dr);

                    j = j + 1;
                }
            }
            j = count;

            ViewState["temp"] = dtTemp;
            dlCriteriatemp.DataSource = dtTemp;
            dlCriteriatemp.DataBind();
            if (dlCriteriatemp.Items.Count > 0)
            {
                divscroll.Style["display"] = "block";
                lbTotalWeightage1.Text = "0";
                divMultiLevel.Style["display"] = "none";
                VacancyCriteriaMul.Add(new clsVacancyCriteria() { Weightage = 0, JobDetailID = 1, CriteriaId = -1, Criteria = "Select" });

                ViewState["TableCriteriaMul"] = VacancyCriteriaMul;
                dlSearchCriteria1.DataSource = VacancyCriteriaMul;
                dlSearchCriteria1.DataBind();
            }
            else
                divscroll.Style["display"] = "none";

            if (validatechk)
            {
                updCriteriapopup.Update();
                upnlCriteria.Update();
            }
            dlCriteriatemp.Visible = true;
            UpdDetails.Update();
        }
        else
        { }
    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        dlSearchCriteria1.Visible = false;
        VacancyCriteriaMul.Add(new clsVacancyCriteria() { Weightage = 0, CriteriaId = -1, Criteria = "Select" });
        ViewState["TableCriteriaMul"] = VacancyCriteriaMul;
        dlSearchCriteria1.DataSource = VacancyCriteriaMul;
        dlSearchCriteria1.DataBind();
        divMultiLevel.Style["display"] = "none";
        upnlCriteria.Update();
    }
    protected void btnDeleteLevel_Click(object sender, EventArgs e)
    {
        int LevelID = ddlLevel.SelectedValue.ToInt32();

        DataTable dtTemp1 = (DataTable)ViewState["temp"];
        if (dtTemp1 != null)
        {
            int GridCount = dtTemp1.Rows.Count - 1;

            for (int i = GridCount; i >= 0; i--)
            {
                if (LevelID == dtTemp1.Rows[i]["JobLevelID"].ToInt32())
                    dtTemp1.Rows.RemoveAt(i);
            }

            if (dtTemp1.Rows.Count > 0)
            {
                dlCriteriatemp.Visible = true;
                divscroll.Style["display"] = "block";
            }
            else
            {
                divscroll.Style["display"] = "none";
                dlCriteriatemp.Visible = false;
            }
        }
        dlCriteriatemp.DataSource = dtTemp1;
        dlCriteriatemp.DataBind();
        dlSearchCriteria1.DataSource = null;
        dlSearchCriteria1.DataBind();
        ViewState["temp"] = dlCriteriatemp.DataSource;
        lbTotalWeightage1.Text = "0";

        upnlCriteria.Update();
        updCriteriapopup.Update();
        UpdDetails.Update();
    }
    #endregion

    protected void btnSubmitCriteria_Click(object sender, EventArgs e)
    {
        string msg = string.Empty;
        if (ValidateData())
        {
            if (SaveCriterias())
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث المهمة بنجاح") : ("Criterias have been saved successfully");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }
        updCriteria.Update();
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.Dispose();
        }
        finally
        {

        }
    }

    #endregion

    #region Methods
    private void ClearControls()
    {
        dlSearchCriteria.DataSource = null;
        dlSearchCriteria.DataBind();
        dlSearchCriteria1.DataSource = null;
        dlSearchCriteria1.DataBind();
        dlCriteriatemp.DataSource = null;
        dlCriteriatemp.DataBind();

        rblType.SelectedValue = "0";
        txtMaxMark.Text = txtPassMark.Text = string.Empty;
        lbTotalWeightage.Text = lbTotalWeightage1.Text = "0";
        ViewState["TableCriteria"] = null;
        VacancyCriteria.Clear();
        VacancyCriteriaMul.Clear();
        ViewState["temp"] = null;
    }
    private void Bindinitials(int JobID)
    {
        DataSet ds = objCriteria.GetAllCriterias(JobID);//, ((rblType.SelectedValue == "0") ? true : false)
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                txtPassMark.Text = dt.Rows[0]["PassPercentage"].ToString();
                txtMaxMark.Text = dt.Rows[0]["TotalMarks"].ToString();
                if (dt.Rows[0]["IsBoardInterviewBool"].ToBoolean() == true)
                {
                    divLevel.Style["display"] = "none";
                    rblType.SelectedValue = "0";
                }
                else
                {
                    divLevel.Style["display"] = "block";
                    rblType.SelectedValue = "1";

                }               
            }
            //Binds Criterias
            DataTable dtCriteria = ds.Tables[1];
            if (dtCriteria.Rows.Count == 0)
                dtCriteria.Rows.Add(dtCriteria.NewRow());

            if (rblType.SelectedValue == "0")
            {
                dlSearchCriteria.DataSource = dtCriteria;
                dlSearchCriteria.DataBind();
                ViewState["TableCriteria"] = dtCriteria;
                if (dlSearchCriteria.Items.Count > 0)
                {
                    for (int i = 0; i < dlSearchCriteria.Items.Count; i++)
                    {
                        DropDownList dlCriteria = (DropDownList)dlSearchCriteria.Items[i].FindControl("dlCriteria");
                        TextBox txtWeightage = (TextBox)dlSearchCriteria.Items[i].FindControl("txtWeightage");
                        Label lbcriteriaID = (Label)dlSearchCriteria.Items[i].FindControl("lbcriteriaID");

                        dlCriteria.SelectedValue = dtCriteria.Rows[i]["CriteriaID"].ToString();
                        txtWeightage.Text = dtCriteria.Rows[i]["Weightage"].ToString();
                        lbcriteriaID.Text = dtCriteria.Rows[i]["CriteriaID"].ToString();
                    }                    
                }
                else
                {
                    VacancyCriteria.Add(new clsVacancyCriteria() { Weightage = 0, CriteriaId = -1, Criteria = "select" });

                    ViewState["TableCriteria"] = VacancyCriteria;
                    dlSearchCriteria.DataSource = VacancyCriteria;
                    dlSearchCriteria.DataBind();
                }
                lbTotalWeightage.Text = "100";
                dlCriteriatemp.Visible = false;
                divscroll.Style["display"] = "none";
                dlSearchCriteria.Visible = true;
                divSingle.Style["Display"] = "block";
                divtotWeightage.Style["display"] = "block";
                divtotWeightageValue.Style["display"] = "block";
            }
            else
            {
                BindInterviewLevels();
                lbTotalWeightage1.Text = "100";
                if (dt != null && dt.Rows.Count > 3)
                    divscroll.Style["height"] = "300px";
                else
                    divscroll.Style["height"] = "auto";

                dlCriteriatemp.DataSource = dtCriteria;
                dlCriteriatemp.DataBind();
                divscroll.Style["display"] = "block";
                dlCriteriatemp.Visible = true;
                dlSearchCriteria.Visible = false;
                divSingle.Style["display"] = "none";
                divMultiLevel.Style["display"] = "none";
                divtotWeightage.Style["display"] = "none";
                divtotWeightageValue.Style["display"] = "none";
                ViewState["temp"] = dtCriteria;
            }
        }
        else
        {
            rblType.SelectedValue = "0";
            BindEmptyDataList();
        }
        upnlCriteria.Update();
    }

    private void BindEmptyDataList()
    {
        if (rblType.SelectedValue.ToInt32() == 0)
        {
            dlCriteriatemp.Visible = false;
            dlSearchCriteria.Visible = true;
            dlSearchCriteria.DataSource = null;
            dlSearchCriteria.DataBind();
            if (dlSearchCriteria.Items.Count == 0)
            {
                VacancyCriteria.Add(new clsVacancyCriteria() { Weightage = 0, CriteriaId = -1, Criteria = "Select" });
                dlSearchCriteria.DataSource = VacancyCriteria;
                dlSearchCriteria.DataBind();
            }
            divtotWeightage.Style["display"] = "block";
            divtotWeightageValue.Style["display"] = "block";
            divLevel.Style["display"] = "none";
            divSingle.Style["display"] = "block";
            divscroll.Style["display"] = "none";
        }
        else
        {
            BindInterviewLevels();
            dlCriteriatemp.Visible = true;
            dlSearchCriteria.Visible = false;
            dlSearchCriteria1.Visible = true;
            dlSearchCriteria1.DataSource = null;
            dlSearchCriteria1.DataBind();
            if (dlSearchCriteria1.Items.Count == 0)
            {
                if (dlCriteriatemp.Items.Count == 0)
                {
                    dlCriteriatemp.Visible = true;
                }
                {
                    dlCriteriatemp.DataSource = null;
                    dlCriteriatemp.DataBind();
                    VacancyCriteriaMul.Add(new clsVacancyCriteria() { Weightage = 0, JobDetailID = 1, CriteriaId = -1, Criteria = "Select" });
                    dlSearchCriteria1.DataSource = VacancyCriteriaMul;
                    dlSearchCriteria1.DataBind();
                }
            }
            ViewState["temp"] = null;
            dlCriteriatemp.DataSource = null;
            dlCriteriatemp.DataBind();

            divtotWeightage.Style["display"] = "none";
            divtotWeightageValue.Style["display"] = "none";
            divLevel.Style["display"] = "block";
            divMultiLevel.Style["display"] = "block";
            divSingle.Style["display"] = "none";
            upnlCriteria.Update();
        }
    }
    public void BindInterviewLevels()
    {
        DataTable ds = objCriteria.GetVacancyLevels();
        ddlLevel.DataSource = ds;
        ddlLevel.DataBind();

        DataTable dtTemp1 = (DataTable)ViewState["temp"];
        if (dtTemp1 != null)
        {
            int GridCount = dtTemp1.Rows.Count - 1;
            int val = 0;
            for (int i = 0; i < dtTemp1.Rows.Count; i++)
            {
                for (int j = 0; j < ds.Rows.Count; j++)
                {
                    if (dtTemp1.Rows[i]["JobLevelID"].ToInt32() == ds.Rows[j]["JobLevelID"].ToInt32())
                    {
                        val = 0;
                        break;
                    }
                    else
                    {
                        val = dtTemp1.Rows[i]["JobLevelID"].ToInt32();
                    }
                }
                if (val != 0)
                {
                    DeleteFromTemp(val);
                }
            }
        }
        updJobLevel.Update();
    }
       
    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUp.Show();
    }

    public void DeleteFromTemp(int LevelID)
    {
        DataTable dtTemp1 = (DataTable)ViewState["temp"];
        if (dtTemp1 != null)
        {
            int GridCount = dtTemp1.Rows.Count - 1;

            for (int i = GridCount; i >= 0; i--)
            {
                if (LevelID == dtTemp1.Rows[i]["JobLevelID"].ToInt32())
                    dtTemp1.Rows.RemoveAt(i);
            }

            if (dtTemp1.Rows.Count > 0)
            {
                dlCriteriatemp.Visible = true;
                divscroll.Style["display"] = "block";
            }
            else
            {
                divscroll.Style["display"] = "none";
                dlCriteriatemp.Visible = false;
            }
        }
        dlCriteriatemp.DataSource = dtTemp1;
        dlCriteriatemp.DataBind();
        ViewState["temp"] = dlCriteriatemp.DataSource;

        upnlCriteria.Update();
        updCriteriapopup.Update();
        UpdDetails.Update();
    }

    public void CalculateWeightage(object sender, EventArgs e)
    {
        decimal weigtage = Calculate(dlSearchCriteria, "txtWeightage", false);
        lbTotalWeightage.Text = weigtage.ToString();

        decimal weigtage1 = Calculate(dlSearchCriteria1, "txtWeightage1", false);
        lbTotalWeightage1.Text = weigtage1.ToString();
    }
    private decimal Calculate(DataList dList, string control, bool isAddition)
    {
        decimal netAmount = 0;
        foreach (DataListItem item in dList.Items)
        {
            TextBox txtAmount = (TextBox)item.FindControl(control);
            if (txtAmount.Text != "")
                netAmount = netAmount + (txtAmount.Text == "" ? 0 : Convert.ToDecimal(txtAmount.Text));
        }
        return netAmount;
    }
       
    private bool ValidateData()
    {
        string msg = string.Empty;
        bool val = true;
        if (lbTotalWeightage.Text.ToDecimal() != 100 && rblType.SelectedValue.ToInt32() == 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("Total weightage must be equal to 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            val = false;
        }
        else if (dlCriteriatemp.Items.Count == 0 && lbTotalWeightage1.Text.ToDecimal() != 100 && rblType.SelectedValue.ToInt32() == 1)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون إجمالي الترجيح يساوي 100") : ("Total weightage must be equal to 100");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            val = false;
        }
        else if (dlSearchCriteria.Items.Count > 0 && rblType.SelectedValue.ToInt32() == 0)
        {
            for (int i = 0; i < dlSearchCriteria.Items.Count; i++)
            {
                DropDownList dlCriteria = (DropDownList)dlSearchCriteria.Items[i].FindControl("dlCriteria");
                TextBox txtWeightage = (TextBox)dlSearchCriteria.Items[i].FindControl("txtWeightage");
                for (int j = i + 1; j < dlSearchCriteria.Items.Count; j++)
                {
                    DropDownList dlNCriteria = (DropDownList)dlSearchCriteria.Items[j].FindControl("dlCriteria");

                    if (dlCriteria.SelectedValue == dlNCriteria.SelectedValue)
                    {
                        val = false;
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير مكررة غير مسموح") : ("Duplicate criteria not allowed");
                        mcMessage.InformationalMessage(msg);                       
                        mpeMessage.Show();
                        break;
                    }                    
                }
                if ((txtWeightage.Text.ToDecimal() == 0) && (rblType.SelectedValue.ToInt32() == 0))
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الترجيح") : ("Please enter weightage");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();            
                    
                }
                if (dlCriteria.SelectedValue == "-1")
                {
                    val = false;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير مكررة غير مسموح") : ("Select a Criteria");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
            }
        }
        return val;
    }
    public bool SaveCriterias()
    {
        string msg = string.Empty;
        try
        {
            if (objCriteria.JobID > 0)
            {
                if (hfMode.Value == "1")
                    objCriteria.DeleteCriteria(objCriteria.JobID);
                if (rblType.SelectedValue == "1")
                    objCriteria.IsBoardInterview = false;
                else
                    objCriteria.IsBoardInterview = true;
                objCriteria.TotalMark = Convert.ToDecimal(txtMaxMark.Text);
                objCriteria.PassPercentage = Convert.ToDecimal(txtPassMark.Text);
                objCriteria.SaveCriterias();

                if (rblType.SelectedValue == "0")
                {                    
                    foreach (DataListItem item in dlSearchCriteria.Items)
                    {
                        DropDownList dlCriteria = (DropDownList)item.FindControl("dlCriteria");
                        TextBox txtWeightage = (TextBox)item.FindControl("txtWeightage");
                        Label lbJobDetailID = (Label)item.FindControl("lbJobDetailID");

                        if (dlCriteria != null)
                            objCriteria.CriteriaId = Convert.ToInt32(dlCriteria.SelectedValue);
                        objCriteria.JobDetailID = lbJobDetailID.Text == "" ? 0 : Convert.ToInt32(lbJobDetailID.Text);
                        objCriteria.WeightPercentage = Convert.ToDecimal(txtWeightage.Text);

                        objCriteria.SaveCriteriasDetails();
                    }
                    return true;
                }
                else
                {
                    DataTable dtnew = (DataTable)ViewState["temp"];
                    if (dtnew == null)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الترجيح") : ("Click Save to add Criterias to List and continue with Submission");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        return false;
                    }
                    else if (dtnew.Rows.Count > 0)
                    {
                        var temp = (from DataRow dr in dtnew.Rows
                                    select dr["JobLevelID"]).Distinct();

                        for (int j = 0; j < temp.Count(); j++)
                        {
                            objCriteria.JobLevelID = temp.ElementAt(j).ToInt32();
                            objCriteria.InsertInterviewLevels();
                        }
                        for (int i = 0; i < dtnew.Rows.Count; i++)
                        {
                            objCriteria.CriteriaId = Convert.ToInt32(dtnew.Rows[i]["CriteriaID"]);
                            objCriteria.JobLevelID = Convert.ToInt32(dtnew.Rows[i]["JobLevelID"]);
                            if (dtnew.Rows[i]["Weightage"].ToDecimal() == 0)
                            {

                                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الترجيح") : ("Please enter weightage");
                                mcMessage.InformationalMessage(msg);
                                mpeMessage.Show();
                                return false;
                                break;
                            }
                            else
                                objCriteria.WeightPercentage = dtnew.Rows[i]["Weightage"].ToDecimal();

                            objCriteria.SaveCriteriasDetails();

                        }
                    }
                    else if (dlCriteriatemp.Items.Count == 0)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الترجيح") : ("Please enter weightage");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                        return false;
                    }
                }
            } return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
    #endregion
}
