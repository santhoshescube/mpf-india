﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Controls_VacancyControls_VacancyOfferLetterApproval : System.Web.UI.UserControl
{
    #region Declarations

    List<clsVacancyOfferLetterApproval> VacancyOffer = new List<clsVacancyOfferLetterApproval>();
    clsVacancyOfferLetterApproval objOffer = new clsVacancyOfferLetterApproval();
    string str;
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        int JobID = 0;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (Request.QueryString["JobID"] != null)
        {
            JobID = Convert.ToInt32(Server.UrlDecode(Request.QueryString["JobID"].ToString()));           
        }
        else if (clsVacancyOfferLetterApproval.ID > 0)
        {
            JobID = clsVacancyOfferLetterApproval.ID;           
        }
        objOffer.JobID = JobID;
        ViewState["JobID"] = JobID;
             
        if (!IsPostBack)
        {
            ClearControls();
            BindInitials(JobID);
        }
    }

    protected void rblOfferLevel_SelectedIndexChanged(object sender, EventArgs e)
    {       
        if (rblOfferLevel.SelectedValue.ToInt32() == 0)
        {
            divOfferNumber.Style["display"] = "none";
            divChkEmployee.Style["display"] = "none";
            divOfferNo.Style["display"] = "none";
            txtOfferLevel.Text = "";
            dlOfferNo.DataSource = null;
            dlOfferNo.DataBind();
        }
        else
        {
            divOfferNumber.Style["display"] = "block";
            divChkEmployee.Style["display"] = "block";
            if (ViewState["TablePay"] != null && rblOfferLevel.SelectedValue == "1")
            {
                BindInitials(ViewState["JobID"].ToInt32());
            }            
        }
        updTxtOfferNo.Update();
    }
    protected void txtOfferLevel_OnTextChanged(object sender, EventArgs e)
    {
        if (txtOfferLevel.Text.ToInt32() <= 5 && txtOfferLevel.Text.ToInt32() >= 1)
        {//Appends new row to existing
            int count = 0, count2 = 0, iCounter = 0;
            if (dlOfferNo.Items.Count > 0)
            {                
                if (dlOfferNo.Items.Count < txtOfferLevel.Text.ToInt32())
                    count = dlOfferNo.Items.Count;
                else
                    count2 = txtOfferLevel.Text.ToInt32();

                foreach (DataListItem item in dlOfferNo.Items)
                {
                    iCounter++;               
                    DropDownList ddlAuthority = (DropDownList)item.FindControl("ddlAuthority");
                    VacancyOffer.Add(new clsVacancyOfferLetterApproval()
                    {
                        EmployeeID = ddlAuthority.SelectedValue.ToInt32(),
                        EmployeeFullName = ddlAuthority.SelectedItem.Text
                    });
                    if (count == iCounter || count2 == iCounter)
                        break;                    
                }                
            }
            else
            {//Adds a new row for the first time
                dlOfferNo.DataSource = null;
                dlOfferNo.DataBind();
                for (int i = 0; i < txtOfferLevel.Text.ToInt32(); i++)
                {
                    VacancyOffer.Add(new clsVacancyOfferLetterApproval() { EmployeeID = -1, EmployeeFullName = "Select" });
                }
            }
            if (count > 0)
            {
                for (int i = 0; i < (txtOfferLevel.Text.ToInt32() - dlOfferNo.Items.Count); i++)
                {
                    VacancyOffer.Add(new clsVacancyOfferLetterApproval() { EmployeeID = -1, EmployeeFullName = "Select" });
                }
            }
            dlOfferNo.DataSource = VacancyOffer;
            dlOfferNo.DataBind();
            divOfferNo.Style["display"] = "block";
        }
        //Sets selected authorities
        if (VacancyOffer.Count > 0)
        {
            int i = 0;
            foreach (clsVacancyOfferLetterApproval item in VacancyOffer)
            {
                DropDownList ddlAuthority = (DropDownList)dlOfferNo.Items[i].FindControl("ddlAuthority");
                ddlAuthority.SelectedValue = item.EmployeeID.ToString();
                i++;
            }
        }
        updTxtOfferNo.Update();
    }
    protected void chkEmployee_CheckedChanged(object sender, EventArgs e)
    {
        txtOfferLevel_OnTextChanged(txtOfferLevel, null);

    }
    protected void dlOfferNo_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        DropDownList ddlAuthority = (DropDownList)e.Item.FindControl("ddlAuthority");
        if (ddlAuthority != null && ddlAuthority.SelectedValue.ToInt32() > -1)
        {
            if (chkEmployee.Checked)
                ddlAuthority.DataSource = clsVacancyOfferLetterApproval.GetAllEmployees(0);
            else
                ddlAuthority.DataSource = clsVacancyOfferLetterApproval.GetAllEmployees(objUserMaster.GetCompanyId());
            ddlAuthority.DataBind();
            ddlAuthority.Items.Insert(0, new ListItem(str, "-1"));
        }
    }
    protected void ddlAuthority_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string msg = string.Empty;

        DropDownList ddlAuthority = (DropDownList)sender;
        var ddlList = (DropDownList)sender;
        var rowIndex = ((DataListItem)ddlList.NamingContainer).ItemIndex;

        if (dlOfferNo.Items.Count > 0)
        {
            for (int i = 0; i < dlOfferNo.Items.Count; i++)
            {
                DropDownList ddlAuthority1 = (DropDownList)dlOfferNo.Items[i].FindControl("ddlAuthority");
                if (ddlAuthority1.SelectedValue == ddlAuthority.SelectedValue && ((DataListItem)ddlAuthority1.NamingContainer).ItemIndex != rowIndex.ToInt32())
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("هيئة مكررة لا يسمح") : ("Duplicate Authority not allowed");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
            }
        }
    }

    protected void btnSubmitOffer_Click(object sender, EventArgs e)
    {
        string msg = string.Empty;
        if (ValidateData())
        {
            Save();
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث المهمة بنجاح") : ("Offer approval levels saved successfully");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            this.Dispose();
        }
        finally
        {

        }
    }
    #endregion

    #region Methods
    private void ClearControls()
    {
        txtOfferLevel.Text = string.Empty;      
        dlOfferNo.DataSource = null;
        dlOfferNo.DataBind();
        VacancyOffer.Clear();
        ViewState["TablePay"] = null;
    }
    private void BindInitials(int JobID)
    {
        DataSet ds = clsVacancyOfferLetterApproval.GetOfferApprovals(JobID);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dtChk = ds.Tables[1];
            if (dtChk.Rows.Count > 0)
            {
                chkEmployee.Checked = dtChk.Rows[0]["IsAllEmployee"].ToBoolean();
            }
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                rblOfferLevel.SelectedValue = "1";
                txtOfferLevel.Text = dt.Rows.Count.ToString();
                divOfferNo.Style["display"] = "block";
                divOfferNumber.Style["display"] = "block";
                divChkEmployee.Style["display"] = "block";
                dlOfferNo.DataSource = dt;
                dlOfferNo.DataBind();

                ViewState["TablePay"] = dt;
                for (int i = 0; i < dlOfferNo.Items.Count; i++)
                {
                    DropDownList ddlAuthority = (DropDownList)dlOfferNo.Items[i].FindControl("ddlAuthority");
                    if (ddlAuthority != null)
                    {
                        if (ddlAuthority.Items.Count > 0)
                        {
                            ddlAuthority.SelectedValue = dt.Rows[i]["EmployeeID"].ToString();
                        }
                    }
                }
            }
            else
            {                
                rblOfferLevel.SelectedValue = "0";
                divOfferNumber.Style["display"] = "none";
                divChkEmployee.Style["display"] = "none";
            }           
        }
        else
        {          
            rblOfferLevel.SelectedValue = "0";
            divOfferNumber.Style["display"] = "none";
            divChkEmployee.Style["display"] = "none";
        }
    }

    private bool ValidateData()
    {
        string msg = string.Empty;
        bool val = true;
        if (txtOfferLevel.Text.Trim() != string.Empty && (txtOfferLevel.Text.ToInt32() < 1 && txtOfferLevel.Text.ToInt32() > 5))
        {
            RvOfferLevel.Visible = true;
            RvOfferLevel.Validate();
            val = false;
        }
        else if (rblOfferLevel.SelectedIndex.ToInt32() == 0 && (txtOfferLevel.Text == "" || txtOfferLevel.Text.ToInt32() ==0))
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("هيئة مكررة لا يسمح") : ("Please enter no. of approval levels");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            val = false;
        }
        else if (dlOfferNo.Items.Count > 0)
        {
            for (int i = 0; i < dlOfferNo.Items.Count; i++)
            {
                DropDownList ddlAuthority = (DropDownList)dlOfferNo.Items[i].FindControl("ddlAuthority");
                if (ddlAuthority != null && ddlAuthority.SelectedValue == "-1")
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("هيئة مكررة لا يسمح") : ("Please Select an Authority");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    val = false;
                    break;
                }
            }
        }
        else
            val = true;
        return val;
    }
    private void Save()
    {
        objOffer.DeleteApprovalAuthority();
        if (rblOfferLevel.SelectedValue == "1" && txtOfferLevel.Text.ToInt32() >= 1)
        {
            for (int i = 0; i < dlOfferNo.Items.Count; i++)
            {
                DropDownList ddlAuthority = (DropDownList)dlOfferNo.Items[i].FindControl("ddlAuthority");

                if (ddlAuthority != null)
                {
                    objOffer.EmployeeID = Convert.ToInt32(ddlAuthority.SelectedValue);
                    objOffer.Level = i + 1;
                    objOffer.IsOfferLetterApproval = true;
                }
                objOffer.IsAllEmployee = chkEmployee.Checked;
                objOffer.InsertApprovalAuthority();
            }
        }
    }
    #endregion

}
