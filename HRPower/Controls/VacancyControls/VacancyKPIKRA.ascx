﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VacancyKPIKRA.ascx.cs"
    Inherits="Controls_VacancyControls_VacancyKPIKRA" %>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 825px; height: auto; max-height: 500px; overflow: scroll;
        background-color: White; overflow-x: hidden; overflow-y: scroll; margin-left: 350px;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="KPIKRA"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div style="margin-left: 25px; width: 900px;">
            <div style="width: 100%; overflow: hidden;">
                <asp:UpdatePanel ID="updKpiKra" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divKPIKRA" style="float: left; width: 96%; display: block;" runat="server">
                            <div id="divKPI" style="float: left; width: 42%;" runat="server">
                                <asp:UpdatePanel ID="upnlKPI" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <fieldset>
                                            <legend>KPI</legend>
                                            <asp:GridView ID="gvKpi" runat="server" AutoGenerateColumns="False" Width="250px"
                                                RowStyle-HorizontalAlign="Center" BorderColor="#307296" OnRowDataBound="gvKpi_RowDataBound">
                                                <HeaderStyle CssClass="datalistheader" />
                                                <RowStyle HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:Literal ID="ltlKpi" runat="server" Text="KPI"></asp:Literal>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlKpi" runat="server" Width="232px" DataTextField="Kpi" DataValueField="KpiID">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:Literal ID="ltlKpiMarks" runat="server" Text="Marks"></asp:Literal>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtKpiMarks" runat="server" MaxLength="3" Width="60px" Text='<%Eval("KPIValue") %>'></asp:TextBox>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeKpiMarks" FilterType="Numbers,Custom"
                                                                ValidChars="." TargetControlID="txtKpiMarks" runat="server">
                                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updbtnkpi" runat="server" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnAddKpi" runat="server" CausesValidation="false" ImageUrl="~/images/AddSymbol.png"
                                                                        OnClick="btnAddKpi_Click"></asp:ImageButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnAddKpi" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updbtnkpidel" runat="server" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="imgKpiDel" ImageUrl="~/images/image/Delete_16px.png" runat="server"
                                                                        CommandArgument='<%# Eval("JobKpiID") %>' OnClick="imgKpiDel_Click" CausesValidation="false" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="imgKpiDel" EventName="click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </fieldset>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div id="divKRA" style="float: left; width: 47%;" runat="server">
                                <asp:UpdatePanel ID="upnlKRA" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <fieldset style="margin-left: 36px;">
                                            <legend>KRA</legend>
                                            <asp:GridView ID="gvKra" runat="server" AutoGenerateColumns="False" Width="250px"
                                                RowStyle-HorizontalAlign="Center" BorderColor="#307296" OnRowDataBound="gvKra_RowDataBound">
                                                <HeaderStyle CssClass="datalistheader" />
                                                <RowStyle HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:Literal ID="ltlKra" runat="server" Text="KRA"></asp:Literal>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlKra" runat="server" Width="232px" DataTextField="Kra" DataValueField="KraID">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:Literal ID="ltlKraMarks" runat="server" Text="Marks"></asp:Literal>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtKraMarks" runat="server" MaxLength="3" Width="60px"></asp:TextBox>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeKraMarks" FilterType="Numbers,Custom"
                                                                ValidChars="." TargetControlID="txtKraMarks" runat="server">
                                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updbtnkra" runat="server" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="btnAddKra" runat="server" OnClick="btnAddKra_onClick" CausesValidation="false"
                                                                        ImageUrl="~/images/AddSymbol.png"></asp:ImageButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnAddKra" EventName="click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel ID="updbtnkradel" runat="server" UpdateMode="Always">
                                                                <ContentTemplate>
                                                                    <asp:ImageButton ID="imgKraDel" ImageUrl="~/images/image/Delete_16px.png" runat="server"
                                                                        CommandArgument='<%# Eval("JobKRAID") %>' OnClick="imgKraDel_Click" CausesValidation="false" /><%-- --%>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="imgKraDel" EventName="click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </fieldset>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="width: 44%; float: left; margin-left: 370px; text-align: right; padding-top: 20px;">
                            <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                ToolTip='<%$Resources:ControlsCommon,Submit%>' CausesValidation="false" Width="75px"
                                OnClick="btnSubmit_Click" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="height: auto">
                    <div>
                        <div style="display: none">
                            <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                        </div>
                        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                            BorderStyle="None">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                            X="500" Y="200" Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
