﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="VacancyCriteria.ascx.cs"
    Inherits="Controls_VacancyControls_VacancyCriteria" EnableViewState="true" %>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 680px !important; height: auto; max-height: 500px; overflow: scroll;
        overflow-x: hidden; overflow-y: scroll; margin-left: 350px; background-color:White">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername" >
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="Criteria"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div style="margin-left: 25px; width: 710px;">
            <div style="width: 100%; overflow: hidden;">
                <asp:UpdatePanel ID="upnlCriteria" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%;">
                            <div style="float: left; width: 96%; padding-left: 5px;">
                                <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="InterviewType"></asp:Literal>
                                </div>
                                <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                    <asp:RadioButtonList ID="rblType" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblType_SelectedIndexChanged"
                                        AutoPostBack="True">
                                        <asp:ListItem Selected="True" Value="0" meta:resourcekey="BoardLevel"></asp:ListItem>
                                        <asp:ListItem Value="1" meta:resourcekey="MultipleLevels"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div id="divSingle" runat="server" style="display: none; padding-top: 20px; padding-left: 4px;">
                                <asp:DataList ID="dlSearchCriteria" runat="server" Width="80%" CellPadding="10" OnItemDataBound="dlSearchCriteria_ItemDataBound"
                                    OnItemCommand="dlSearchCriteria_ItemCommand" DataKeyField="JobDetailID" BorderColor="#307296"
                                    BorderWidth="1">
                                    <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                                    <HeaderTemplate>
                                        <div style="width: 95%; font-size: 11px; float: left;" class="datalistheader">
                                            <div style="display: none">
                                                <asp:Literal ID="Literal27" runat="server" meta:resourcekey="JobDetailID"></asp:Literal>
                                            </div>
                                            <div style="width: 50%; float: left;">
                                                <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Criteria"></asp:Literal>
                                            </div>
                                            <div style="width: 20%; float: left;">
                                                &nbsp;<asp:Literal ID="Literal29" runat="server" meta:resourcekey="Weightage"></asp:Literal>
                                            </div>
                                            <div style="width: 15%; float: right;">
                                                &nbsp;
                                            </div>
                                            <div style="width: 15%; float: left;">
                                                &nbsp;
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100%; float: left; padding-top: 4px">
                                            <div style="display: none">
                                                <asp:Label ID="lbJobDetailID" Text='<%# Eval("JobDetailID") %>' runat="server"></asp:Label>
                                            </div>
                                            <div style="display: none">
                                                <asp:Label ID="lbcriteriaID" Text='<%# Eval("CriteriaID") %>' runat="server"></asp:Label>
                                            </div>
                                            <div style="width: 50%; float: left;">
                                                <asp:DropDownList ID="dlCriteria" runat="server" Width="90%" DataTextField="Criteria"
                                                    DataValueField="CriteriaId" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 20%; float: left;">
                                                <asp:TextBox runat="server" ID="txtWeightage" Width="60px" MaxLength="3" CssClass="textbox"
                                                    OnTextChanged="CalculateWeightage" Text='<%# Eval("weightage") %>' AutoPostBack="true"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error"
                                                    Display="Dynamic" ControlToValidate="txtWeightage" ValidationGroup="submit" meta:resourcekey="EnterWeightage"></asp:RequiredFieldValidator>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbetxtNoOfMonths" runat="server"
                                                    FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtWeightage">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                            </div>
                                            <div style="width: 15%; float: left;">
                                                <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/Delete18x18.png"
                                                    CausesValidation="false" meta:resourcekey="Remove" CommandName="Remove" />
                                            </div>
                                            <div style="width: 15%; float: left;">
                                                <asp:ImageButton ID="imgAddnewCriteria" runat="server" meta:resourcekey="Addnew"
                                                    OnClick="imgAddnewCriteria_Click" CausesValidation="false" ImageUrl="~/images/AddCriteria.png"
                                                    ValidationGroup="Addnew" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                                <div class="firstTrLeft" id="divtotWeightage" runat="server" style="float: left;
                                    width: 27%; height: 29px">
                                    <asp:Literal ID="Literal29" runat="server" meta:resourcekey="TotalWeightage"></asp:Literal>
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 10%; height: 29px">
                                    :
                                </div>
                                <div class="firstTrRight" style="float: left; width: 50%; height: 29px" runat="server"
                                    id="divtotWeightageValue">
                                    <asp:Label ID="lbTotalWeightage" runat="server" Text="0"></asp:Label>
                                </div>
                            </div>
                            <div id="divLevel" style="float: left; width: 96%; display: none; padding-top: 20px;
                                padding-left: 15px;" runat="server">
                                <div class="trleft" style="float: left; width: 15%; height: 29px">
                                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Level"></asp:Literal>
                                </div>
                                <div class="trleft" style="float: left; width: 45%; height: 29px">
                                    <asp:UpdatePanel ID="updJobLevel" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlLevel" runat="server" DataTextField="Description" DataValueField="JobLevelId" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged"
                                                Width="200px" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnLevel" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                Text="..." OnClick="btnLevel_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div class="trleft" style="float: left; width: 40%; height: 29px">
                                    <asp:UpdatePanel ID="updCriteria" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="lnkCriteria" CausesValidation="false" runat="server" OnClick="lnkCriteria_Click"
                                                meta:resourcekey="SelectaCriteria">                         
                                            </asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%--  Joblevel criteria popup  --%>
                                <asp:UpdatePanel ID="updCriteriapopup" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divMultiLevel" runat="server">
                                            <asp:DataList ID="dlSearchCriteria1" runat="server" Width="80%" CellPadding="10"
                                                DataKeyField="CriteriaID" OnItemDataBound="dlSearchCriteriapop_ItemDataBound"
                                                OnItemCommand="dlSearchCriteriapop_ItemCommand" BorderColor="#307296" BorderWidth="1">
                                                <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                                                <HeaderTemplate>
                                                    <div style="width: 95%; font-size: 11px; float: left;" class="datalistheader">
                                                        <div style="display: none">
                                                            <asp:Literal ID="Literal27" runat="server" meta:resourcekey="JobDetailID"></asp:Literal>
                                                        </div>
                                                        <div style="width: 50%; float: left;">
                                                            <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Criteria"></asp:Literal>
                                                        </div>
                                                        <div style="width: 20%; float: left;">
                                                            &nbsp;<asp:Literal ID="Literal29" runat="server" meta:resourcekey="Weightage"></asp:Literal>
                                                        </div>
                                                        <div style="width: 15%; float: right;">
                                                            &nbsp;
                                                        </div>
                                                        <div style="width: 15%; float: left;">
                                                            &nbsp;
                                                        </div>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div style="width: 100%; float: left; padding-top: 4px">
                                                        <div style="display: none">
                                                        </div>
                                                        <div style="display: none">
                                                            <asp:Label ID="lbcriteriaID1" Text='<%# Eval("CriteriaId") %>' runat="server"></asp:Label>
                                                        </div>
                                                        <div style="width: 50%; float: left;">
                                                            <asp:DropDownList ID="dlCriteria1" runat="server" Width="200px" DataTextField="Criteria"
                                                                DataValueField="CriteriaId" CssClass="dropdownlist">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hfdCriteria" runat="server" Value='<%# Eval("CriteriaId") %>' />
                                                        </div>
                                                        <div style="width: 20%; float: left;">
                                                            <asp:TextBox runat="server" ID="txtWeightage1" Width="60px" MaxLength="3" CssClass="textbox"
                                                                OnTextChanged="CalculateWeightage" Text='<%# Eval("weightage") %>' AutoPostBack="true"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="error"
                                                                Display="Dynamic" ControlToValidate="txtWeightage1" ValidationGroup="Addnew"
                                                                meta:resourcekey="EnterWeightage"></asp:RequiredFieldValidator>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbetxtNoOfMonths1" runat="server"
                                                                FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtWeightage1">
                                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                                        </div>
                                                        <div style="width: 15%; float: left;">
                                                            <asp:ImageButton ID="imgRemove1" runat="server" ImageUrl="~/images/Delete18x18.png"
                                                                CausesValidation="false" meta:resourcekey="Remove" CommandName="Remove" />
                                                        </div>
                                                        <div style="width: 15%; float: left;">
                                                            <asp:ImageButton ID="imgAddnewCriteria1" runat="server" meta:resourcekey="Addnew"
                                                                OnClick="imgAddnewCriteriapop_Click" ImageUrl="~/images/AddCriteria.png" CausesValidation="false"
                                                                ValidationGroup="Addnew" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:UpdatePanel ID="upnlbtnPopup" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnSave" runat="server" ValidationGroup="Save" CssClass="btnsubmit"
                                                                CausesValidation="false" meta:resourcekey="Save" OnClick="btnSave_Click" />
                                                            <asp:Button ID="btnDeleteLevel" runat="server" CssClass="btnsubmit" meta:resourcekey="DeleteLevel"
                                                                CausesValidation="false" OnClick="btnDeleteLevel_Click" />
                                                            <asp:Button ID="btnExit" runat="server" ValidationGroup="Save" CssClass="btnsubmit"
                                                                CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>' OnClick="btnExit_Click" />
                                                            <AjaxControlToolkit:ConfirmButtonExtender ID="cbeDeleteLevel" runat="server" TargetControlID="btnDeleteLevel"
                                                                meta:resourcekey="AreyousuretodeletethisLevel">
                                                            </AjaxControlToolkit:ConfirmButtonExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </FooterTemplate>
                                            </asp:DataList>
                                            <div class="firstTrLeft" id="divTot1" runat="server" style="float: left; width: 27%;
                                                height: 29px">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TotalWeightage"></asp:Literal>
                                            </div>
                                            <div class="firstTrLeft" style="float: left; width: 10%; height: 29px">
                                                :
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 50%; height: 29px" runat="server"
                                                id="divTotval1">
                                                <asp:Label ID="lbTotalWeightage1" runat="server" Text="100"></asp:Label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <%--  Joblevel criteria temp  --%>
                                <asp:UpdatePanel ID="UpdDetails" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divscroll" runat="server" style="height: auto; max-height: 300px; width: 100%;
                                            overflow: scroll; overflow-x: hidden;">
                                            <asp:DataList ID="dlCriteriatemp" runat="server" Width="100%" DataKeyField="CriteriaId">
                                                <ItemStyle CssClass="item" />
                                                <HeaderStyle CssClass="listItem" />
                                                <HeaderTemplate>
                                                    <div id="Div2" style="float: left; height: 30px; width: 100%; margin-top: 30px;"
                                                        runat="server">
                                                        <div style="float: left; width: 60%;">
                                                            <asp:Label ID="lblc" runat="server" meta:resourcekey="Criteria"></asp:Label>
                                                        </div>
                                                        <div style="float: left; width: 20%;">
                                                            <asp:Label ID="lblw" runat="server" meta:resourcekey="Weightage"></asp:Label>
                                                        </div>
                                                        <div style="float: right; width: 20%;">
                                                            <asp:Label ID="lbll" runat="server" meta:resourcekey="Level"></asp:Label>
                                                        </div>
                                                        <div style="float: right;">
                                                        </div>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 50%;" id="tblCriteriatemp"
                                                        runat="server">
                                                        <tr>
                                                            <td valign="top">
                                                                <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <table style="width: 100%; margin-top: 0px" cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td style="width: 50%;">
                                                                                        <asp:Label ID="lnkcriteriatemp" Text='<%# string.Format("{0}", Eval("Criteria")) %>'
                                                                                            CausesValidation="false" runat="server" CssClass="listHeader bold"></asp:Label>
                                                                                        <asp:Label ID="lblcid" Text='<%# string.Format("{0}", Eval("CriteriaID")) %>' Visible="false"
                                                                                            CausesValidation="false" runat="server" CssClass="listHeader bold"></asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:Label ID="lblweight" Text='<%# string.Format("{0}", Eval("Weightage")) %>' CausesValidation="false"
                                                                                            runat="server" CssClass="listHeader bold"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 20%;">
                                                                                        <asp:Label ID="lbllevelview" Text='<%# string.Format("{0}", Eval("LevelName")) %>'
                                                                                            CausesValidation="false" runat="server" CssClass="listHeader bold"></asp:Label>
                                                                                        <asp:Label ID="lbllid" Text='<%# string.Format("{0}", Eval("JobLevelID")) %>' Visible="false"
                                                                                            CausesValidation="false" runat="server" CssClass="listHeader bold"></asp:Label>
                                                                                    </td>
                                                                                    <td valign="top" align="right">
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 27%; height: 29px; padding-left: 10px;">
                                <asp:Literal ID="Literal32" runat="server" meta:resourcekey="MaximummarkInterviewer"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 10%; height: 29px">
                                :
                            </div>
                            <div id="Div1" class="firstTrRight" style="float: left; width: 50%; height: 29px"
                                runat="server">
                                <asp:TextBox ID="txtMaxMark" runat="server" CssClass="textbox_mandatory" MaxLength="3"
                                    Text='<%# Eval("TotalMarks") %>' Width="45px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvMaxMark" runat="server" meta:resourcekey="Pleaseentermaximummarkperinterviewer"
                                    ControlToValidate="txtMaxMark" Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtMaxMark" runat="server" FilterType="Numbers"
                                    TargetControlID="txtMaxMark">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                <asp:RangeValidator ID="rvMaxMark" ControlToValidate="txtMaxMark" MinimumValue="1" Type="Integer"
                                    runat="server" MaximumValue="100" Display="Dynamic" ValidationGroup="submit" ErrorMessage="Mark should be within the range: 1-100"
                                    CssClass="error"></asp:RangeValidator>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 27%; height: 29px; padding-left: 10px;">
                                <asp:Literal ID="Literal33" runat="server" meta:resourcekey="MinimumPassMark"></asp:Literal>
                            </div>
                            <div class="firstTrLeft" style="float: left; width: 10%; height: 29px">
                                :
                            </div>
                            <div id="Div3" class="firstTrRight" style="float: left; width: 50%; height: 29px"
                                runat="server">
                                <asp:TextBox ID="txtPassMark" runat="server" CssClass="textbox_mandatory" MaxLength="3"
                                    Text='<%# Eval("PassPercentage") %>' Width="45px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPassmark" runat="server" meta:resourcekey="Pleaseenterpassmark"
                                    ControlToValidate="txtPassMark" Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtPassMark" runat="server" FilterType="Numbers"
                                    TargetControlID="txtPassMark">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                <asp:RangeValidator ID="rvtxtPassMark" ControlToValidate="txtPassMark" MinimumValue="1" Type="Integer"
                                    runat="server" MaximumValue="100" Display="Dynamic" ValidationGroup="submit" ErrorMessage="Pass Mark should be within the range: 1-100"
                                    CssClass="error"></asp:RangeValidator>
                            </div>
                        </div>
                        <div style="width: 44%; float: left; margin-left: 243px; text-align: right;">
                            <asp:Button ID="btnSubmitCriteria" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" ValidationGroup="submit"
                                OnClick="btnSubmitCriteria_Click" /> <%--CausesValidation="false"--%>
                            <asp:HiddenField ID="hfMode" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="height: auto">
                    <div>
                        <div style="display: none">
                            <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                        </div>
                        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                            BorderStyle="None">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground" X="450" Y="350"
                            Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </div>
                </div>
                <asp:UpdatePanel ID="updLevel" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnPopLevel" runat="server" />
                        </div>
                        <div id="divPopup" runat="server" style="display: none;">
                            <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUp" />
                        </div>
                        <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUp" runat="server" TargetControlID="btnPopLevel"
                            PopupControlID="divPopup" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
