﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_ChangeCompany : System.Web.UI.UserControl
{
    public string ModalPopupID
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindCompany();
        }
    }

    private void BindCompany()
    {
        clsCompany objCompany = new clsCompany();
        clsUserMaster objUser = new clsUserMaster();

        DataTable dt = objCompany.GetCompaniesbyUser(objUser.GetUserId());

        if (dt.Rows.Count > 0)
        {
            ddlCompanyList.DataSource = dt;
            ddlCompanyList.DataTextField = "CompanyName";
            ddlCompanyList.DataValueField = "CompanyId";

            ddlCompanyList.DataBind();
        }
        ddlCompanyList.SelectedValue = objUser.GetCompanyId().ToString();
    }
    protected void ibtnchangeCompanyClose_Click(object sender, ImageClickEventArgs e)
    {
        this.Dispose();
    }
    protected void btnchangeCompanySave_Click(object sender, EventArgs e)
    {
       // clsCommon.CompanyID = rdbCompanyList.SelectedValue.ToInt32();
        clsUserMaster objUser = new clsUserMaster();
        clsEmployee objEmployee = new clsEmployee();
        objUser.EmployeeID = objUser.GetEmployeeId();
        objUser.CompanyID = ddlCompanyList.SelectedValue.ToInt32();
        DataTable dt = objUser.GetEmployeeCompanyRole();
        if (dt.Rows.Count > 0)
        {
            int RoleID = dt.Rows[0]["RoleID"].ToInt32();
            objEmployee.EmployeeID = objUser.GetEmployeeId();
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                           1,
                           objEmployee.GetWelcomeText(),
                           DateTime.Now,
                           DateTime.Now.AddDays(14),
                           true,
                           string.Format("{0},{1},{2},{3},{4}", RoleID, objUser.GetUserId(), objUser.GetEmployeeId(), ddlCompanyList.SelectedValue.ToInt32(), clsGlobalization.CultureName),
                           FormsAuthentication.FormsCookiePath
                           );
            string hash = FormsAuthentication.Encrypt(ticket);

            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

            //if (chkStaySigned.Checked) cookie.Expires = ticket.Expiration;

            cookie.Path = ticket.CookiePath;

            Response.Cookies.Add(cookie);


            //AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(this.ModalPopupID);
            //if (mpe != null)
            //{
            //    mpe.Hide();
            //}
            if (RoleID == (Int32)UserRoles.Candidate)
            {
                Response.Redirect("~/Candidate/CandidateHome.aspx");

            }
            else if (RoleID == (Int32)UserRoles.Employee)
            {
                Response.Redirect("~/public/Home.aspx");

            }
            else
            {

                Response.Redirect("~/public/ManagerHomeNew.aspx");

            }
            // Response.Redirect(((System.Web.UI.TemplateControl)(this.Page)).AppRelativeVirtualPath);
        }
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.ModalPopupID != null)
        {
            AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(this.ModalPopupID);
            if (mpe != null)
            {
                mpe.PopupDragHandleControlID = "DragHandle";
                mpe.CancelControlID = ibtnchangeCompanyClose.ClientID;
            }
        }
    }

}
