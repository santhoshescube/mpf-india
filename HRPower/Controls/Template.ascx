﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Template.ascx.cs" Inherits="controls_Template" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
<link id="cssMaroon" runat="server" href="../App_Themes/Maroon/css/Maroon.css" rel="stylesheet" type="text/css" disabled="disabled" />
<table cellpadding="0" cellspacing="0" width="778">
    <tr>
        <td height="9" 
            style="background-repeat: no-repeat; background-position: bottom" 
            width="9">
            &nbsp;</td>
        <td style="background-image: url('../images/shadow_top_resizable.png'); background-repeat: repeat-x; background-position: bottom">
            &nbsp;</td>
        <td width="9" 
            style="background-repeat: no-repeat; background-position: bottom">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="background-position: right; background-image: url('../images/shadow_left_side.png'); background-repeat: repeat-y">
            &nbsp;</td>
        <td>
            <table width='100%' 
                style="font-family: Verdana" bgcolor="White">
                <tr>
                    <td class="header">
                        <table cellpadding="0" cellspacing="0" width='100%'>
                            <tr>
                                <td class="header_text" id="tdDrag" runat="server">
                                    Company Work Policy</td>
                                <td align="right" width="20">
                                    <asp:UpdatePanel ID="upHeader" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:ImageButton ID="btnClose" runat="server"
                                                ToolTip="Close" OnClick="btnClose_Click" CausesValidation="False" 
                                                SkinID="CloseIcon" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" width="100%" style="font-size: 11px">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" 
                                        RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:ImageButton ID="btnAddNew" runat="server"
                                                ToolTip="Add New" CausesValidation="False" SkinID="NewIcon" />
                                            <asp:ImageButton ID="btnSaveData" runat="server"
                                                ToolTip="Save Data" Width="16px" SkinID="SaveIcon" />
                                            <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" 
                                                ToolTip="Delete" Enabled="False" 
                                                OnClientClick="return confirm('Do you wish to delete this information?');" />
                                            <asp:ImageButton ID="btnClear" runat="server" CausesValidation="False" 
                                                ToolTip="Clear" />
                                            <asp:ImageButton ID="btnPrint" runat="server" CausesValidation="False" 
                                                ToolTip="Print" SkinID="PrintIcon" />
                                            <asp:ImageButton ID="btnEmail" runat="server" CausesValidation="False" 
                                                ToolTip="Email" SkinID="EmailIcon" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td align="right" style="padding-right: 10px" width="30">
                                    <asp:UpdateProgress ID="prgMainWindow" runat="server" 
                                        AssociatedUpdatePanelID="upMainWindow">
                                        <ProgressTemplate>
                                            <img alt="" src="../images/circle-loading.gif" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <fieldset style="padding: 10px; height: 375px">
                                    
                                </fieldset>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table width="100%" style="font-size: 11px">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblWarning" runat="server"></asp:Label>
                                        </td>
                                        <td width="90" height="35">
                                            <asp:Button ID="btnOK" runat="server" CssClass="button" Text="OK" 
                                                Width="80px" onclick="btnOK_Click" />
                                        </td>
                                        <td width="90">
                                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" 
                                                Width="80px" onclick="btnCancel_Click" CausesValidation="False" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </td>
        <td style="background-image: url('../images/shadow_right_side.png'); background-repeat: repeat-y; background-position: left">
            &nbsp;</td>
    </tr>
    <tr>
        <td height="9" 
            style="background-repeat: no-repeat; background-position: top">
            &nbsp;</td>
        <td style="background-image: url('../images/shadow_bottom_resizable.png'); background-repeat: repeat-x; background-position: top">
            &nbsp;</td>
        <td style="background-repeat: no-repeat; background-position: top">
            &nbsp;</td>
    </tr>
</table>

