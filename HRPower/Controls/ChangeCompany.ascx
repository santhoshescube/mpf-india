﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangeCompany.ascx.cs" Inherits="Controls_ChangeCompany" %>


        <div id="popupmain" style="width: 42%; height: auto; overflow: auto; background-color: White">
            <div id="header11" style="width: 100%">
                <div id="hbox1">
                    <div id="headername">
                        <asp:Label ID="changeCompanylblHeading" runat="server" Text='<%$Resources:ControlsCommon,ChangeCompany%>'></asp:Label>
                    </div>
                </div>
                <div id="hbox2">
                    <div id="close1">
                        <a href="">
                            <asp:ImageButton ID="ibtnchangeCompanyClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                CausesValidation="true" ValidationGroup="dummy_not_using" OnClick="ibtnchangeCompanyClose_Click" />
                        </a>
                    </div>
                </div>
            </div>
            <div style="width: 100%; overflow: hidden;">
                <asp:UpdatePanel ID="updchangeCompany" runat="server">
                    <ContentTemplate>
                        <div style="display: block; border: solid 1px black;">
                            <div  class="firstTrLeft" style="float: left; width: 25%; height: auto; padding-bottom: 5px;">
                               <asp:Literal ID="Literal1" runat="server" text='<%$Resources:ControlsCommon,Company%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 60%; height: auto; padding-bottom: 5px;">
                                <asp:DropDownList ID="ddlCompanyList" runat="server" CssClass="textbox" width="200px">
                                </asp:DropDownList>
                            </div>
                            <div style="clear: both">
                            </div>
                            <div style="clear: both;" id="footerwrapper">
                                <div id="footer11" style="width: 100%">
                                    <div style="float: right; padding-right: 10px; padding-top: 10px">
                                        <asp:Button ID="btnchangeCompanySave" ValidationGroup="Save" CssClass="btnsubmit"
                                            Width="75px" runat="server" Text='<%$Resources:ControlsCommon,Save%>' OnClick="btnchangeCompanySave_Click"
                                            CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
   