﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Alert.ascx.cs" Inherits="Controls_Alert" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />


         <div id="divAlertPopup" runat="server" style="width: 680px; ">
                <div id="popupmainwrap">
                    <div id="popupmain">
                        <div id="header11">
                            <div id="hbox1">
                                <div id="headername">
                                    <asp:Label ID="Label1" runat="server" Text="Alert"></asp:Label>
                                </div>
                            </div>
                            <div id="hbox2">
                                <div id="close1">
                                    <a href="">
                                        <asp:ImageButton ID="imgPopupClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                            CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="contentwrap">
                            <div id="content">
                                <table style="width: 100%;">
                                    <tr>
                                        <td width="100%">
                                            <asp:UpdatePanel ID="upnlPopup" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                <fieldset style="width:95%; border-color: rgb(10, 194, 252);border-width: 1px;border-radius: 10px;">
                                                    <div style="width: 100%; float: left; padding-top: 4px">
                                                        <div style="width: 50%; float: left; padding-top: 4px">
                                                            <div style="width: 35%; float: left; padding-top: 4px">
                                                                Document Type
                                                            </div>
                                                            <div style="float: right; padding-top: 4px;">
                                                                <asp:DropDownList ID="ddlDocType" runat="server" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:HiddenField ID= "hfdIsExpiry" runat ="server" />
                                                        </div>
                                                        <div style="width: 50%; float: right; padding-top: 4px">
                                                             
                                                             <div style="width:2%; float: left; padding-top: 4px">
                                                               
                                                            </div>
                                                            <div style="width:10%; float: left; padding-top: 4px">
                                                                Date
                                                            </div>
                                                            <div style="width:2%; float: left; padding-top: 4px">
                                                               
                                                            </div>
                                                            <div style="width: 25%;float: left; padding-top: 4px">
                                                                <asp:TextBox runat="server" ID="txtfrom" Width="85px" AutoPostBack="true" 
                                                                    ontextchanged="txtfrom_TextChanged" >
                                                                </asp:TextBox>
                                                                <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtfrom"
                                                                    PopupButtonID="txtfrom" Format="dd/MM/yyyy">
                                                                </AjaxControlToolkit:CalendarExtender>
                                                               
                                                            </div>
                                                           <div style="width:3%; float: left; padding-top: 4px">
                                                               
                                                            </div>
                                                             <div style="width: 25%;float: left; padding-top: 4px ;margin-left:10px;">
                                                              <asp:TextBox runat="server" ID="txtTo" Width="85px"  AutoPostBack ="true"  
                                                                     ontextchanged="txtTo_TextChanged" >
                                                                </asp:TextBox>
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTo"
                                                                    PopupButtonID="txtTo" Format="dd/MM/yyyy">
                                                                </AjaxControlToolkit:CalendarExtender>
                                                             </div>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                   <div style="width: 100%; float: left;padding-top:4px ; max-height: 190px; overflow: auto;">
                                                    <asp:DataList ID="dlAlerts" runat="server" BorderWidth="0px" CellPadding="0"
                                                CellSpacing="0" Width="100%" >
                                                        <HeaderTemplate>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="30" width="30" valign="top" align="left">
                                                                      <asp:CheckBox ID="chk_All" runat="server" onclick="selectHeaderAll(this.id,'chkAlert')" />
                                                                    </td>
                                                                    <td style="color: rgb(9, 101, 153);" align ="left" >
                                                                        Message
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                               
                                                <ItemTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="font-size:12px"; color:#6a8bab;" >
                                                            <td  width="30" style="vertical-align: bottom;">
                                                                 <asp:CheckBox ID="chkAlert" runat="server" />
                                                                 <asp:HiddenField ID="hfdDocTypeID" runat ="server" Value ='<%# Eval("DocumentTypeID")%>' />
                                                                 <asp:HiddenField ID="hfdCommonID" runat ="server" Value ='<%# Eval("CommonID")%>' />
                                                            </td>
                                                            <td  style="padding-top:8px;color: rgb(9, 101, 153)"; align ="left" >
                                                                <%# Eval("Message")%>
                                                              
                                                            </td>
                                                           
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                        <FooterTemplate>
                                                            <table width="100%" style ="display:none;">
                                                                <tr>
                                                                    <td width="25" valign="top" style="padding-left: 5px">
                                                                        <asp:CheckBox ID="chk_FooterAll" runat="server" onclick="selectFooterAll(this.id,'chkAlert')" />
                                                                    </td>
                                                                    <td style="padding-left: 7px">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </FooterTemplate>
                                            </asp:DataList>
                                                  </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table width="100%" style="font-size: 11px" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" style="padding-left: 5px;">
                                                                <asp:Label ID="lblInfoEmployeeAdd" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="footerwrapper">
                            <div id="footer11">
                                <div id="buttons">
                                    <asp:UpdatePanel ID="upnlAddEmployee" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnAddEmployee" runat="server"  ValidationGroup="AddPhase" OnClick ="btnAddEmployee_Click" 
                                                CssClass="btnsubmit" Text="Do Not Notify Again" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" OnClick="btnCancel_Click"
                                                ToolTip="Cancel" Text="Cancel" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
