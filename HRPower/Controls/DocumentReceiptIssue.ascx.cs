﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Controls_DocumentReceiptIssue : System.Web.UI.UserControl
{
    public delegate void Save(string Result);

    public event Save OnSave;

    string sModalPopupID;

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    #region Declarations

    public DocumentType eDocumentType
    {
        get{

            if (hfDocumentType.Value != "")
                return (DocumentType)Convert.ToInt16(hfDocumentType.Value);
            else
                return DocumentType.Driving_License ;
        }
        set
        {
            hfDocumentType.Value = Convert.ToInt16(value).ToString();
        }
    }

    public OperationType eOperationType {

        get
        {

            if (hfOperationType.Value != "")
                return (OperationType)Convert.ToInt16(hfOperationType.Value);
            else
                return OperationType.Employee ;
        }
        set
        {
            hfOperationType.Value = Convert.ToInt16(value).ToString();
        }
    
    }

    public int DocumentID
    {
        get
        {
            if (hfDocumentID.Value != "")
                return Convert.ToInt32(hfDocumentID.Value);
            else
                return 0;
        }
        set
        {
            hfDocumentID.Value =  value.ToString();
        }
    }

    public DateTime dtExpiryDate
    {
        get
        {
            if (hfExpiryDate.Value != "")
                return Convert.ToDateTime(hfExpiryDate.Value);
            else
                return txtTransactionDate.Text.ToDateTime();
        }
        set
        {
            hfExpiryDate.Value = value.ToString();
        }
    }        

    public string DocumentNumber {  get; set; }

    public int EmployeeID { get; set; }

    public bool IsFromStockRegister { get; set; }

    public int OrderNo
    {
        //get
        //{
        //    if (hfOrderNo.Value != "")
        //        return Convert.ToInt16(hfOrderNo.Value);
        //    else
        //        return 0;

        //}
        //set
        //{
        //    hfOrderNo.Value = value.ToString();
        //}
        get;
        set;
    }

    private clsMessage ObjUserMessage = null;

    private bool IsReceipted 
    {
        get
        {
            if (hfIsReceipt.Value != "")
               return  Convert.ToBoolean(hfIsReceipt.Value);
            else
                return false;
        }
        set
        {
            hfIsReceipt.Value = value.ToString();
        }
    }


    private bool MblnAddPermission;
    private bool MblnAddUpdatePermission;
    private bool MblnPrintEmailPermission;
    private bool MblnUpdatePermission;
    private bool MblnDeletePermission;


    private clsDocumentMaster objDocumentMasterBLL { get; set; }



   

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        hfCurrentDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        if (!IsPostBack)
        {

          //  SetPermissions();
          
        }
    }



    public void Call()
    {
        ClearControls();
        SetInitials();
    }

    private void ClearControls()
    {
        rdbCompany.Checked = false;
        rdbEmployee.Checked = true;
        cboDocumentType.SelectedIndex =0;
        txtDocumentNumber.Text = string.Empty;

        if(cboReferenceNumber.DataSource != null)
        cboReferenceNumber.SelectedIndex = 0;
        cboCustodian.SelectedIndex = 0;
        cboDocumentStatus.SelectedIndex = 0;
        cboBinNumber.SelectedIndex = 0;
        txtTransactionDate.Text = string.Empty;
        txtExpectedReturnDate.Text = string.Empty;
        txtRemarks.Text = string.Empty;
    }

    private void GetAllReferenceNumberByOperationTypeID()
    {
        clsUserMaster objUsermaster = new clsUserMaster();
        cboReferenceNumber.DataSource = clsDocumentMaster.GetAllReferenceNumberByOperationTypeID((int)OperationType.Employee, true, objUsermaster.GetCompanyId(), objUsermaster.GetUserId());
        cboReferenceNumber.DataTextField = "ReferenceNumber";
        cboReferenceNumber.DataValueField = "ReferenceID";
        cboReferenceNumber.DataBind();
        cboReferenceNumber.SelectedIndex = -1;
    }

    private void GetAllDocumentTypes()
    {
        cboDocumentType.DataSource = clsDocumentMaster.GetAllDocumentTypes();
        cboDocumentType.DataTextField = "DocumentType";
        cboDocumentType.DataValueField = "DocumentTypeID";
        cboDocumentType.DataBind(); 
       
    }

    private void GetAllBin()
    {

        DataTable dt =  clsDocumentMaster.GetAllBin();
        DataRow dr = dt.NewRow();
        dr["BinName"] = GetGlobalResourceObject("DocumentsCommon", "Select");
        dr["BinID"] ="-1";
        dt.Rows.InsertAt(dr, 0);
        cboBinNumber.DataSource = dt;
        cboBinNumber.DataTextField = "BinName";
        cboBinNumber.DataValueField = "BinID";
        cboBinNumber.DataBind();
        cboBinNumber.SelectedIndex = 0;
       
    }

    private void GetAllCustodian()
    {
        clsUserMaster objUsermaster = new clsUserMaster();
        DataTable dt = clsDocumentMaster.GetAllCustodian(objUsermaster.GetUserId());
        DataRow dr = dt.NewRow();
        dr["EmployeeFullName"] = GetGlobalResourceObject("DocumentsCommon", "Select");
        dr["EmployeeID"] = "-1";
        dt.Rows.InsertAt(dr, 0);

        cboCustodian.DataSource = dt;
        cboCustodian.DataTextField = "EmployeeFullName";
        cboCustodian.DataValueField = "EmployeeID";
        cboCustodian.DataBind();
        cboCustodian.SelectedIndex = 0;
    }
    
    public void SetInitials()
    {
     
        if (eOperationType == OperationType.Company)
            rdbCompany.Checked = true;
        else
            rdbEmployee.Checked = true;

        GetAllDocumentTypes();
        GetAllReferenceNumberByOperationTypeID();
        GetAllBin();
        GetAllCustodian();
        IsReceipted = !clsDocumentMaster.IsReceipted((int)eOperationType, (int)eDocumentType, DocumentID);
        DisableControls();

         
        if (!IsFromStockRegister)
        {
            GetAllDocumentReceiptIssueStatus();
            cboDocumentType.SelectedValue = ((int)eDocumentType).ToString();
            txtDocumentNumber.Text = DocumentNumber;
                            
            int BinID = clsDocumentMaster.GetBinID((int)eOperationType, (int)eDocumentType, DocumentID).ToInt32();
            if (BinID > 0)
                cboBinNumber.SelectedValue = BinID.ToString();
            else
                cboBinNumber.SelectedIndex = 0;

            ChangeFormForReceipt_Issue();
            btnPrint.Enabled = false;
        }
        else
        {
            GetDocumentDetailsByOrderNo();
        }

    }

    private void GetDocumentDetailsByOrderNo()
    {
        using (DataTable dt = clsDocumentMaster.GetDocumentDetailsByOrderNo((int)eOperationType, (int)eDocumentType, DocumentID, OrderNo))
        {
            if (dt.Rows.Count > 0)
            {

                btnPrint.Enabled = MblnPrintEmailPermission;

                //EnableDisableNavigatorActionButtons(true); 
                DataRow dr = dt.Rows[0];
                DocumentID = dr["DocumentID"].ToInt32();
                OrderNo = dr["OrderNo"].ToInt32();

                IsReceipted = Convert.ToBoolean(dr["IsReceipted"]);
                //receiptIssueToolStripMenuItem.Text = IsReceipted ? "Issue" : "Receipt";
                ChangeFormForReceipt_Issue();

                GetAllDocumentReceiptIssueStatus();

                // cboOperationType.SelectedValue = dr["OperationTypeID"].ToInt16();
                cboDocumentType.SelectedValue = dr["DocumentTypeID"].ToString();
                cboReferenceNumber.SelectedValue = dr["ReceivedFrom"].ToString();
                cboCustodian.SelectedValue = dr["Custodian"].ToString();
                cboBinNumber.SelectedValue = dr["BinID"].ToString();

                if (IsReceipted)
                    cboDocumentStatus.SelectedValue = dr["DocumentStatusID"].ToString();
                else
                    cboDocumentStatus.SelectedValue = dr["IssueReasonID"].ToString();

                txtDocumentNumber.Text = dr["DocumentNumber"].ToString();
                txtRemarks.Text = dr["Remarks"].ToString();



                if (dr["ReceiptIssueDate"] != null && dr["ReceiptIssueDate"] != DBNull.Value)
                    txtTransactionDate.Text = dr["ReceiptIssueDate"].ToString();

                if (dr["ExpectedReturnDate"] != null && dr["ExpectedReturnDate"] != DBNull.Value)
                {
                    //txtExpectedReturnDate.Checked = true;
                    txtExpectedReturnDate.Text = dr["ExpectedReturnDate"].ToString ();
                }
                //else
                   // dtpExpectedReturnDate.Checked = false;
            }
        }
    }

    private void DisableControls()
    {
        rdbCompany.Enabled = rdbEmployee.Enabled =
      cboDocumentType.Enabled = txtDocumentNumber.Enabled = false;

        if (eOperationType == OperationType.Company)
        {
            if (IsReceipted)
                cboReferenceNumber.Enabled = true;
            else
            {
                cboReferenceNumber.Enabled = false;
                cboReferenceNumber.SelectedValue = clsDocumentMaster.GetEmployeeID((int)eOperationType, (int)eDocumentType, DocumentID).ToString();
            }
        }
        else
        {
            cboReferenceNumber.Enabled = false;
            cboReferenceNumber.SelectedValue = EmployeeID.ToString();// clsDocumentMaster.GetEmployeeID((int)eOperationType, (int)eDocumentType, DocumentID);
        }

    }

    private void ChangeFormForReceipt_Issue()
    {
        txtRemarks.Text = "";
        if (IsReceipted)  // Receipt
        {
            lblHeading.Text = GetLocalResourceObject("Heading.Text").ToString();

            lblTransactionDate.Text = GetLocalResourceObject("ReceiptDate.Text").ToString();
            lblReceivedBy.Text = GetLocalResourceObject("ReceivedBy.Text").ToString();
            lblDocumentStatus.Text = GetLocalResourceObject("DocumentStatus.Text").ToString();

            lblTypeName.Text =  GetLocalResourceObject("ReceivedFrom.Text").ToString();
            lblExpRetDate.Visible = false;
            txtExpectedReturnDate.Visible = false;
            cboBinNumber.Enabled = true;



        }
        else
        {
            lblHeading.Text = "Documents - Issue";

            lblTransactionDate.Text = "Issue Date";
            lblReceivedBy.Text = "Issued By";
            lblDocumentStatus.Text = "Reason for Issue";
            lblTypeName.Text = "Issued To";
            txtExpectedReturnDate.Visible = true;
            lblExpRetDate.Visible = true;
            cboBinNumber.Enabled = false;

        }
    }

    private void GetAllDocumentReceiptIssueStatus()
    {
        DataTable dt = null;
        if (IsReceipted)
        {
            dt = clsDocumentMaster.GetAllDocumentReceipts();
            DataRow dr = dt.NewRow();
            dr["DocumentReceipt"] = GetGlobalResourceObject("DocumentsCommon","Select");
            dr["DocumentReceiptID"] = "-1";

            dt.Rows.InsertAt(dr, 0);
            cboDocumentStatus.DataSource = dt;
            cboDocumentStatus.DataTextField  = "DocumentReceipt";
            cboDocumentStatus.DataValueField = "DocumentReceiptID";
            cboDocumentStatus.DataBind();

        }
        else
        {
            dt = clsDocumentMaster.GetAllDocumentIssues();
            DataRow dr = dt.NewRow();
            dr["IssueReason"] = "Select";
            dr["IssueReasonID"] = "-1";
            dt.Rows.InsertAt(dr, 0);

            cboDocumentStatus.DataSource =dt ;
            cboDocumentStatus.DataTextField = "IssueReason";
            cboDocumentStatus.DataValueField = "IssueReasonID";
            cboDocumentStatus.DataBind();

        }
        cboDocumentStatus.SelectedIndex = -1;
    }

    //private void SetPermissions()
    //{

    //    clsBLLPermissionSettings objClsBLLPermissionSettings = new clsBLLPermissionSettings();
    //    if (ClsCommonSettings.RoleID != 1 && ClsCommonSettings.RoleID != 2)
    //    {
    //        if (IsReceipted)
    //            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentReceipt, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
    //        else
    //            objClsBLLPermissionSettings.GetPermissions(ClsCommonSettings.RoleID, ClsCommonSettings.CompanyID, (Int32)eModuleID.Documents, (Int32)eMenuID.DocumentIssue, out MblnPrintEmailPermission, out MblnAddPermission, out MblnUpdatePermission, out MblnDeletePermission);
    //    }
    //    else
    //        MblnAddPermission = MblnPrintEmailPermission = MblnUpdatePermission = MblnDeletePermission = true;


    //    btnSaveItem.Enabled = btnSaveItem.Enabled = (MblnAddPermission || MblnAddUpdatePermission);
    //    //btnPrint.Enabled = false;
    //}

    protected void btnOK_Click(object sender, EventArgs e)
    {

        SaveDocumentReceiptIssue();
        ClearControls();
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.Hide(); 
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearControls();
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.Hide(); 
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        CloseForm();
    }
    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {

    }

    private void SaveDocumentReceiptIssue()
    {
        try
        {
            DateTime? ExpectedReturnDate = null;
            DateTime TempExpectedReturnDate ;
            DateTime TransactionDate ;
            DateTime ExpiryDate;
            string Message = "";


          
            if (txtExpectedReturnDate.Text != "")
            {
                DateTime.TryParseExact(txtExpectedReturnDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out TempExpectedReturnDate);
                ExpectedReturnDate = TempExpectedReturnDate;
            }
           
            DateTime.TryParseExact(txtTransactionDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out TransactionDate);

            hfExpiryDate.Value = (hfExpiryDate.Value.ToDateTime()).ToString("dd/MM/yyyy");
            DateTime.TryParseExact(hfExpiryDate.Value.ToString(), "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            
            objDocumentMasterBLL = new clsDocumentMaster()
                        {
                            DocumentID = DocumentID,
                            OperationTypeID = (int)eOperationType,
                            DocumentTypeID = cboDocumentType.SelectedValue.ToInt32(),
                            DocumentNumber = txtDocumentNumber.Text.Trim(),
                            ReceiptIssueDate = TransactionDate,
                            Custodian = cboCustodian.SelectedValue.ToInt32(),
                            DocumentStatusID = cboDocumentStatus.SelectedValue.ToInt16(),
                            BinID = cboBinNumber.SelectedValue.ToInt16(),
                            ReceiptIssueReasonID = cboDocumentStatus.SelectedValue.ToInt16(),
                            Remarks = txtRemarks.Text.Trim(),
                            IsReceipted1 = IsReceipted,
                            OrderNo = OrderNo,
                            ExpectedReturnDate = ExpectedReturnDate,
                            ReceivedFrom = Convert.ToInt32(cboReferenceNumber.SelectedValue),
                            ExpiryDate = ExpiryDate,
                            CompanyID = new clsUserMaster().GetCompanyId()
                        };

            OrderNo = objDocumentMasterBLL.SaveDocumentReceiptIssue();
            if (OrderNo > 0)
            {

                
                // clsAlerts objclsAlerts = new clsAlerts();
                if (IsReceipted)
                {

                    // objclsAlerts.DeleteAlertMessage(cboDocumentType.SelectedValue.ToInt32(), DocumentID, 2);
                }
                else
                {
                    if (ExpectedReturnDate != null)
                    {
                        // objclsAlerts.AlertMessage(cboDocumentType.SelectedValue.ToInt32(), cboDocumentType.Text, cboDocumentType.Text + " Document Number", DocumentID, txtDocumentNumber.Text.Trim(), dtpExpectedReturnDate.Value, "Handover", cboCustodian.SelectedValue.ToInt32(), cboReferenceNumber.Text);
                    }
                }
                // MessageBox.Show("Document saved successfully", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //btnPrint.Enabled = MblnPrintEmailPermission;

                //if (bnPrint.Enabled)
                //{
                // if (MessageBox.Show("Do you want to generate a receipt for this transaction?", ClsCommonSettings.MessageCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                // {
                //LoadReport();
                // }
                //}

                //Message = "Document ";

                //if (IsReceipted)
                //    Message = Message + " receipt";
                //else
                //    Message = Message + " issue";

                //Message = Message + " saved successfully";

                Message = GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString();
                IsReceipted = !IsReceipted;
                ScriptManager.RegisterClientScriptBlock(txtRemarks, txtRemarks.GetType(), new Guid().ToString(), "alert('"+Message+"')", true); 
            }
            //else
                //MessageBox.Show("Failed to save documents", ClsCommonSettings.MessageCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
        catch (Exception ex)
        {

        }
    }

    private void CloseForm()
    {
        string Result = IsReceipted ? "Receipt" : "Issue";
        if (OnSave != null)
        {
            OnSave(Result);
        }
    }
}
