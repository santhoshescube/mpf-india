﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderReference.ascx.cs"
    Inherits="controls_FolderReference" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<div id="popupmainwrap">
    <div id="popupmain" style="width: 450px">
        <div id="header11">
            <div id="hbox1">
                <div id="headername" runat="server" style="color: White;">
                    Folders
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Controls/images/icon-close.png" />
                    </a>
                </div>
            </div>
        </div>
        <div id="toolbar">
            <div id="imgbuttons">
             <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="always">
                    <ContentTemplate>
                <asp:ImageButton ID="btnAddNew" runat="server" ToolTip="New Folder" CausesValidation="False"
                    OnClick="btnAddNew_Click" ImageUrl="~/images/save_icon.jpg" />
                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" ToolTip="Edit Folder"
                     OnClick="btnEdit_Click" ImageUrl="~/images/edit_icon.png" />
                <asp:ImageButton ID="btnDelete" runat="server" CausesValidation="False" ToolTip="Delete Folder"
                    Enabled="False" OnClientClick="return confirm('Do you wish to delete this folder?');"
                    OnClick="btnDelete_Click" ImageUrl="~/images/delete_icon.png" />
                <AjaxControlToolkit:ModalPopupExtender ID="mpeFolderEntry" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="pnlFolderEntry" TargetControlID="btnDummy" CancelControlID="btnCancelFolderEntry" />
                <asp:Button ID="btnDummy" runat="server" Text="Button" Style="display: none;" />
                </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
           <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="always">
                    <ContentTemplate>
        <asp:Panel ID="pnlFolderEntry" runat="server" Style="display: none;">
            <div id="popupmainwrap">
                <div id="popupmain" style="width: 350px">
                    <div id="header11">
                        <div id="hbox1">
                            <div id="headername" style="color: White;">
                                Folder
                            </div>
                        </div>
                        <div id="hbox2">
                            <div id="close1">
                                <a href="">
                                    <asp:ImageButton ID="ImageButton2" OnClick="ImageButton2_Click" runat="server" ImageUrl="~/Controls/images/icon-close.png" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <%-- <div id="toolbar">
         <div id="imgbuttons">
                                   
         </div>
        </div>--%>
                    <div id="contentwrap">
                        <div id="content">
                      
                            <div style="float: left; width: 100%; height: 29px; margin-left: 5px; margin-top: 5px;">
                                <asp:TextBox ID="txtFolder" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                    Width="200px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFolder"
                                    Display="Dynamic" ErrorMessage="Please enter folder" Style="display: block" ValidationGroup="FolderEntry"></asp:RequiredFieldValidator>
                            </div>
                            <div style="text-align: right;" class="trLeft">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="popupbutton" Text="Submit" ValidationGroup="FolderEntry"
                                    OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancelFolderEntry" runat="server" CausesValidation="False" CssClass="popupbutton"
                                    Text="Cancel" />
                            </div>
                           
                        </div>
                    </div>
                    <%--<div id="footerwrapper">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="Label1" runat="server" CssClass="error" Style="display: none;"></asp:Label>
                    <div id="footer11">
                        <div id="buttons">
                            <asp:Button ID="Button1" runat="server" CssClass="btnsubmit" OnClick="btnOK_Click"
                                Width="50px" Text="Ok" />&nbsp;
                            <asp:Button ID="Button2" runat="server" CssClass="btnsubmit" OnClick="btnCancel_Click"
                                Text="Cancel" CausesValidation="False" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>--%>
                </div>
            </div>
        </asp:Panel>
        
         </ContentTemplate>
                            </asp:UpdatePanel>
        <div id="contentwrap">
            <div id="content">
                <asp:UpdatePanel ID="upMainWindow" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--  <fieldset style="padding: 10px; height: 200px; overflow: auto">--%>
                        <div style="max-height: 250px; overflow: auto; margin-left: 5px;">
                            <asp:TreeView ID="tvFolders" runat="server" Font-Size="XX-Small" Font-Strikeout="False"
                                ForeColor="Black" OnTreeNodePopulate="tvFolders_TreeNodePopulate" ShowLines="True"
                                OnSelectedNodeChanged="tvFolders_SelectedNodeChanged">
                                <SelectedNodeStyle BackColor="#000066" ForeColor="White" />
                            </asp:TreeView>
                        </div>
                        <%-- </fieldset>--%>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="footerwrapper">
            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblWarning" runat="server" CssClass="error" Style="display: none;"></asp:Label>
                    <div id="footer11">
                        <div id="buttons">
                            <asp:Button ID="btnOK" runat="server" CssClass="popupbutton" OnClick="btnOK_Click"
                                Width="50px" Text="Ok" />&nbsp;
                            <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" OnClick="btnCancel_Click"
                                Text="Cancel" CausesValidation="False" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
