﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;

public partial class Controls_AttachDocument : System.Web.UI.UserControl
{
    public string Width { get; set; }
    public bool IsViewOnly { get; set; }
    public eReferenceTypes RequestType { get; set; }
    public int RequestID { get; set; }
    public int Test { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        dvMain.Attributes.Add("style", "width:" + Width);

        dvMainDocument.Visible = !IsViewOnly;

      //  if (!IsPostBack)
            //LoadData();
    }


    public void LoadData(int ReferenceId)
    {
        //if (HttpContext.Current.Session["LeaveRequestID"] != null)
        //{
        DataTable dt = clsLeaveRequest.GetAttachedDocumentsDetails(RequestType, (ReferenceId == 0 ?RequestID:ReferenceId ));
        Session["Documents"] = ViewState["Documents"] = dt;
       
            if (dt.Rows.Count > 0)
            {
                dlAttachedDocuments.DataSource = dt;
                dlAttachedDocuments.DataBind();
                Session["Documents"] = dt;
            }
            dvNoInformation.Visible = (IsViewOnly && dt.Rows.Count == 0);
        //}
    }

    public void SaveDocuments()
    {

    }

    protected void lnkAddtoList_Click(object sender, EventArgs e)
    {


        //if (ViewState["FileName"] != null)
        //{
        //    if (!IsValidFile(ViewState["FileName"].ToString()))
        //    {
        //        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Tests", "alert('Invalid file type');", true);
        //        return;
        //    }
        //}
        if (txtDocumentName.Text == "")
            ScriptManager.RegisterClientScriptBlock(txtDocumentName, txtDocumentName.GetType(), new Guid().ToString(), "alert('Please enter document name');", true);

        else if (Session["FileName"]== null)
            ScriptManager.RegisterClientScriptBlock(txtDocumentName, txtDocumentName.GetType(), new Guid().ToString(), "alert('Please add a document');", true);

        else
        {

            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);
            else
            {
                dt = new DataTable();
                dt.Columns.Add("DocumentID");
                dt.Columns.Add("FileName");
                dt.Columns.Add("DocumentName");
                dt.Columns.Add("Location");
                //dt.Columns.Add("DocumentID");
                
            }

            DataRow dr = dt.NewRow();
            dr["DocumentID"] = 0;
            dr["FileName"] = Session["FileName"];
            dr["DocumentName"] = txtDocumentName.Text;
            dr["Location"] = "../Documents/"+RequestType.ToString()+"/" + Session["FileName"].ToString();
            dt.Rows.Add(dr);

            dlAttachedDocuments.DataSource = dt;
            dlAttachedDocuments.DataBind();
            ViewState["Documents"] = dt;
            Session["Documents"] = dt;
            ClearAll();
        }
    }


    private void ClearAll()
    {
        txtDocumentName.Text = "";
        Session["FileName"] = null;
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {           
            DataTable dt = null;
            string FileName = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument.Split('@')[0];
            int DocumentID = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument.Split('@')[1].ToInt32() ;
            

           // new clsCandidate().DeleteCandidateAttachedDocuments(hfCandidateID.Value.ToInt64(), FileName);

            new clsLeaveRequest().DeleteAttachedDocument(DocumentID);

            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FileName"].ToString() == FileName)
                {
                    dt.Rows.Remove(dt.Rows[i]);
                }
            }

            Session["Documents"] = dt;
            dlAttachedDocuments.DataSource = dt;
            dlAttachedDocuments.DataBind();

        }
        catch (Exception ex)
        {
        }

    }
    protected void dlCandidateDocument_ItemCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void FUFile_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string FileName = string.Empty;
        string ActualFileName = string.Empty;


        if (!Directory.Exists(Server.MapPath("~/Documents/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/"));

        if (!Directory.Exists(Server.MapPath("~/Documents/" + RequestType.ToString() + "/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/" + RequestType.ToString() + "/"));

        if (FUFile.HasFile)
        {
            ActualFileName = FUFile.FileName;
            // ViewState["FileName"] = FUFile.FileName;
            if (IsValidFile(ActualFileName))
            {
                FileName = DateTime.Now.Ticks.ToString() + new Random().Next(0, 1000).ToString() + Path.GetExtension(FUFile.FileName);
                Session["FileName"] = FileName;
                FUFile.SaveAs(Server.MapPath("~/Documents/" + RequestType.ToString() + "/") + FileName);
            }
        }
    }


    private bool IsValidFile(string FileName)
    {
        string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
        if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif"

            || ext.ToLower() == ".pdf" || ext.ToLower() == ".docx" || ext.ToLower() == ".xls" || ext.ToLower() == ".xlsx" || ext.ToLower() == ".txt")
        {
            return true;
        }

        //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Extension", "alert('Invalid file type');", true);
        return false;
    }

    public void SaveAttachedDocuments(int RequestID)
    {
        new clsLeaveRequest().DeleteAttachedDocument(RequestID, (int)RequestType);
        new clsLeaveRequest().SaveAttachments(RequestID, (int)RequestType, ((DataTable)ViewState["Documents"]));

    }
    protected void img_Click(object sender, ImageClickEventArgs e)
    {
        string FileName = ((ImageButton)sender).CommandArgument.ToString();

        if (IsValidImage(FileName))
        {
            string Page = "ViewImage.aspx?RequestType= "+RequestType.ToString()+ "&FileName="+((ImageButton)sender).CommandArgument.ToString();

           

            ScriptManager.RegisterClientScriptBlock(txtDocumentName, txtDocumentName.GetType(), "OPen", "window.open('" + Page + "');", true);
        }
        else
        {
            //System.Diagnostics.Process objProc = new System.Diagnostics.Process();
            //objProc.StartInfo.FileName = Server.MapPath("~/Documents/MedicalCertificate/" + FileName);
            //objProc.Start();

            //FileInfo file = new FileInfo(Server.MapPath("~/Documents/MedicalCertificate/" + FileName));
            string Page = "ViewImage.aspx?RequestType= "+RequestType.ToString()+ "&FileName="+((ImageButton)sender).CommandArgument.ToString();

            ScriptManager.RegisterClientScriptBlock(txtDocumentName, txtDocumentName.GetType(), "OPen", "window.open('" + Page + "','','width=400,height=600');", true);

           /* if (file.Exists)
            {
               // WebClient client = new WebClient();
               // Byte[] buffer = client.DownloadData(Server.MapPath("~/Documents/MedicalCertificate/" + FileName));
               // Response.ContentType = "application/pdf";
               // Response.AddHeader("content-length", buffer.Length.ToString());
               // Response.BinaryWrite(buffer);

               //// Response.ClearHeaders();
               // //Response.ClearContent();
               //// Response.Redirect(Server.MapPath("~/Documents/MedicalCertificate/" + FileName));


                string FileNameT = Server.MapPath("~/Documents/MedicalCertificate/" + FileName);
              
                FileStream s = File.Open(FileNameT,  FileMode.Open);

                int i = Convert.ToInt32(s.Length);

                byte[] b = new byte[i];

                s.Read(b, 0, i);

                s.Close();


                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "filename=" + FileNameT.Remove(0, FileNameT.LastIndexOf("\\") + 1));

                Response.OutputStream.Write(b, 0, b.Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();

                Response.Flush();
                Response.Close();
  
            }*/

        }
        //Response.Redirect("ViewImage.aspx?FileName="+((ImageButton)sender).CommandArgument.ToString());
    }


    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }

    protected void dlAttachedDocuments_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            Image imgDelete = (Image)e.Item.FindControl("imgDelete");            
            if (img != null)
            {
                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }

            if(imgDelete != null)
                imgDelete.Visible = !IsViewOnly;
        }
    }
}
