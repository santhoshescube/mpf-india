﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HierarchyViewControl.ascx.cs"
    Inherits="Controls_HierarchyViewControl" %>
<%@ Register Assembly="BPOrgDiagram" Namespace="BasicPrimitives.OrgDiagram" TagPrefix="cc1" %>

<script type="text/javascript" src="../Scripts/jquery-1.9.1.js"></script>

<script type="text/javascript" src="../Scripts/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript" src="../Scripts/json3.min.js"></script>

<link rel="stylesheet" type="text/css" href="../Scripts/ui-lightness/jquery-ui-1.10.2.custom.min.css" />
<link href="../Scripts/primitives.latest.css" rel="stylesheet" />

<script type="text/javascript" src="../Scripts/primitives.min.js?1029"></script>

<script src="../Scripts/UserTemplates.js?1029"></script>

<script type="text/javascript">
    function printFunction()
    {
        
        
        var divContent = document.getElementById("<%=divPrint.ClientID %>");
        link = "about:blank";
        var win = window.open(link, "_new"); 
        win.document.open();
        win.document.write(makepage(divContent.innerHTML));
        win.document.close();
    }

    function makepage(Src) {
        return "<html>\n" +
    "<head>\n" +
    "<title>Hierarchy</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";    
    }
</script>

<div id="popupmain1" style="width: 100%; height: 100%; overflow: auto; background-color: White">
    <div id="header11" style="width: 100%">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnPrint" runat="server" ImageUrl="~/images/print _offer_letter.png" ToolTip="Print"
                        CausesValidation="true" OnClientClick="printFunction();" />
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png" ToolTip="Close"
                        CausesValidation="true" ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <%-- 
    <div id="dvDetails" runat="server" style ="width:100%;height:35px">
        <table>
            <tr>
                <td  style ="width:130px">
                    <asp:Image ID="imgRoot" runat="server" Width="30px" Height="30px" ImageUrl="~/images/Parent.png" />
                    <asp:Label  ID="lblRoot" style="vertical-align:0.3cm" runat="server" CssClass ="label" Text ="Root" />
                </td>
                   <td style ="width:130px">
                    <asp:Image ID="imgHaveChild" runat="server" Width="30px" Height="30px" ImageUrl="~/images/Child1.png" />
                 <asp:Label  ID="Label2" style="vertical-align:0.3cm" runat="server" CssClass ="label" Text ="Have child" />

                </td>
                <td style ="width:100px">
                    <asp:Image ID="imgNoChild" runat="server" Width="30px" Height="30px" ImageUrl="~/images/Child0.png" />
                     <asp:Label  ID="Label1" style="vertical-align:0.3cm" runat="server" CssClass ="label" Text ="No child" />

                </td>
             
            </tr>
        </table>
   
 
    </div>--%>
    <div style="clear: both; width: 100%; height: 89%; overflow: hidden;">
        <div id="divPrint" runat="server" style="display: block; border: solid 1px black; background-color:Azure;">
            <cc1:OrgDiagramServerControl BackColor="Azure" Width="1338px" Height="577px"  AutoPostBack="false" ID="orgDiagram"
                runat="server" MaximumColumnsInMatrix="8" SelectionPathMode="None"  
                DotItemsInterval="10" DotLevelShift="10" ConnectorType="Angular" OrientationType="Top">
            </cc1:OrgDiagramServerControl> 
        </div>
    </div>
    <div style="clear: both;" id="footerwrapper">
        <div id="footer11" style="width: 100%; height: 30px">
            <%--  <asp:Button ID="btnClose" CssClass="btnsubmit" runat="server" Text="Close" OnClick="btnClose_Click" />--%>
        </div>
    </div>
</div>
