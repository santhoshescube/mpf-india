﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReferenceControl.ascx.cs"
    Inherits="controls_ReferenceControl" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
    type="text/css" disabled="disabled" />--%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<script type="text/javascript">

    function closThis(elementID) {
        var e = document.getElementById(elementID);

        if (e) {
            e.style.display = 'none';
            return false;
        }
    }

</script>

<%--<table width="400" border="0" id="tblReference" cellspacing="0" cellpadding="0" align="center">
    <tr>
        <td width="20" height="41" align="left" valign="top" class="tdTopLeft">
            &nbsp;
        </td>
        <td height="41" width="360" align="left" valign="top" class="tdTopCenter">
            <table cellpadding="0" border="0" cellspacing="0" width="100%" style="padding-top: 20px"
                id="DragHandle">
                <tr>
                    <td class="header_text">
                        <asp:Label ID="lblHeading" runat="server" Text="HR Power"></asp:Label>
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="ibtnClose" runat="server" SkinID="CloseIcon" CssClass="imagebutton" 
                            OnClick="ibtnClose_Click"   CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </td>
                </tr>
            </table>
        </td>
        <td width="20" height="41" class="tdTopRight">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="20" class="tdMiddleLeft">
            &nbsp;
        </td>
        <td height="75" width="360" align="center" style="background-color: White;">
            <asp:UpdatePanel ID="upnlReference" runat="server">
                <ContentTemplate>
                    <table cellpadding="5" cellspacing="5" border="0" width="100%">
                        <tr>
                            <td align="left" valign="middle">
                                <fieldset>
                                    <div style="padding: 4px;">
                                        <asp:TextBox ID="txtReferenceName" runat="server" BorderStyle="Solid" 
                                            BorderWidth="1px" MaxLength="50"></asp:TextBox>
                                        <asp:ImageButton ID="btnSave" runat="server" Style="margin-right: 3px; margin-left: 3px;
                                            width: 14px; height: 16px;" CausesValidation="false" OnClick="btnAdd_Click"
                                            Width="16px" ToolTip="Save" 
                                            OnClientClick="return ValidateReferenceControl1(this);" 
                                            ImageUrl="~/images/save_icon.jpg"/>
                                        <asp:ImageButton ID="imgClear" runat="server" Style="margin-right: 3px; margin-left: 3px;
                                            width: 14px; height: 16px;" CausesValidation="false" Width="16px"
                                            OnClick="imgClear_Click" ToolTip="Add New Item" 
                                            ImageUrl="~/images/new_icon_reference.PNG" />
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div style="max-height: 200px; overflow: auto;">
                                        <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                            Width="95%">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Particular" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <div style="width: 180px; overflow: hidden">
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("DataText") %>'></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                    
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgEdit" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                            OnClick="ImgEdit_Click1" OnClientClick="return true" ToolTip="edit" ImageUrl="~/images/edit_icon_popup.jpg" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgDelete" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                            OnClick="ImgDelete_Click" OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);" ToolTip="Delete" ImageUrl="~/images/delete_icon_popup.png" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                        <tr style="margin: 0px">
                            <td align="left" style="margin-top: 0px">
                                <div style="height: 15px; margin-top: 0px">
                                    <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                <div style="display: none">
                
                    <asp:HiddenField ID="hfId" runat="server" />
                    <asp:HiddenField ID="hfDataTextField" runat="server" />
                    <asp:HiddenField ID="hfDataValueField" runat="server" />
                    <asp:HiddenField ID="hfDataTextValue" runat="server" />
                    <asp:HiddenField ID="hfDataNoValue" runat="server" />
                    <asp:HiddenField ID="hfPredefinedField" runat="server" />
                    <asp:HiddenField ID="hfTableName" runat="server" />
                    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
                    <asp:HiddenField ID="hfFunctionName" runat="server" />
                    <asp:HiddenField ID="hfReferenceField" runat="server" />
                    <asp:HiddenField ID="hfReferenceId" runat="server" />
                    <asp:HiddenField ID="hfDisplayName" runat="server" />
                    <asp:HiddenField ID="hfPreviousID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfVaidationGroup" runat="server" Value="" />
                    <asp:HiddenField ID="hfParentElementId" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeFiled" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeValues" runat="server" Value="" />
                    
   
                </div>                    
                </ContentTemplate>
                
            </asp:UpdatePanel>
        </td>
        <td align="right" valign="top" width="20" class="tdMiddleRight">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td width="20" height="10" align="right" valign="top" class="tdBottomLeft">
            &nbsp;
        </td>
        <td height="10" width="360" align="left" valign="top" class="tdBottomCenter">
            &nbsp;
        </td>
        <td width="20" height="10" align="left" valign="top" class="tdBottomRight">
        </td>
    </tr>
</table>
--%>
<div id="popupmainwrap">
    <div id="popupmain" style="width:350px">
        <div id="header11">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" Text="HR Power"></asp:Label>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset>
                        <div id="content1">
                            <asp:TextBox ID="txtReferenceName" runat="server" BorderStyle="Solid" BorderWidth="1px" Width ="200px"
                                MaxLength="50"></asp:TextBox>
                            <asp:ImageButton ID="btnSave" runat="server" vertical-align="top" Style="margin-right: 3px;
                                margin-left: 3px; width: 14px; height: 16px;" CausesValidation="false" OnClick="btnAdd_Click"
                                Width="16px" ToolTip="Save" OnClientClick="return ValidateReferenceControl1(this);"
                                ImageUrl="~/images/save_icon.jpg" />
                            <asp:ImageButton ID="imgClear" runat="server" vertical-align="top" Style="margin-right: 3px;
                                margin-left: 3px; width: 14px; height: 16px;" CausesValidation="false" Width="16px"
                                OnClick="imgClear_Click" ToolTip="Add New Item" ImageUrl="~/images/new_icon_reference.PNG" />
                        </div>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <div id="content" style="max-height: 226px; overflow: auto;">
                            <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                Width="100%">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Particular" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <div style="width: 180px; overflow: hidden">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("DataText") %>'></asp:Label>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:ImageButton style ="padding-right:5px;"  ID="ImgEdit" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                OnClick="ImgEdit_Click1" OnClientClick="return true" ToolTip="edit" ImageUrl="~/images/edit_popup.png" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:ImageButton style ="padding-right:5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("DataValue") %>'
                                                OnClick="ImgDelete_Click" OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);"
                                                ToolTip="Delete" ImageUrl="~/images/delete_popup.png"/>
                                        </ItemTemplate>
                                        <ItemStyle Width="15px"  />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div id="footerwrapper" >
                    <div id="footer11" style ="height:30px;" >
                        <%--       <div id="buttons">--%>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="False"></asp:Label>
                        <%--  </div>--%>
                    </div>
                </div>
                <div style="display: none">
                    <asp:HiddenField ID="hfId" runat="server" />
                    <asp:HiddenField ID="hfDataTextField" runat="server" />
                    <asp:HiddenField ID="hfDataValueField" runat="server" />
                    <asp:HiddenField ID="hfDataTextValue" runat="server" />
                    <asp:HiddenField ID="hfDataNoValue" runat="server" />
                    <asp:HiddenField ID="hfPredefinedField" runat="server" />
                    <asp:HiddenField ID="hfTableName" runat="server" />
                    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
                    <asp:HiddenField ID="hfFunctionName" runat="server" />
                    <asp:HiddenField ID="hfReferenceField" runat="server" />
                    <asp:HiddenField ID="hfReferenceId" runat="server" />
                    <asp:HiddenField ID="hfDisplayName" runat="server" />
                    <asp:HiddenField ID="hfPreviousID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfVaidationGroup" runat="server" Value="" />
                    <asp:HiddenField ID="hfParentElementId" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeFiled" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeValues" runat="server" Value="" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
