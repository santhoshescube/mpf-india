﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Data.SqlClient;
using System.Reflection;

public partial class Controls_CriteriaReference : System.Web.UI.UserControl
{
    #region Declarations

    clsCriteriaReference objCriteria;
    public delegate void OnUpdate();    
    public event OnUpdate Update;

    string sModalPopupID;
    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    #endregion Declarations

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatusMessage.Text = string.Empty;
        lblStatusMessage.Visible = false;

        if (clsGlobalization.IsArabicCulture())
        {
            rfvEngCri.ValidationGroup = rfvDesEng.ValidationGroup = rfvArbCri.ValidationGroup = rfvDesArb.ValidationGroup = "Submit";
            lblStatusMessage.Text = string.Empty;
        }
        else
        {
            rfvEngCri.ValidationGroup = rfvDesEng.ValidationGroup = "Submit";
            rfvArbCri.ValidationGroup = rfvDesArb.ValidationGroup = "";
            rfvArbCri.ErrorMessage = rfvDesArb.ErrorMessage = "";
            lblStatusMessage.Text = string.Empty;
        }
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        Clear();
        mpe.Hide();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        AddCriteria();
        BindDataList();
    }

    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {
        hfMode.Value = "Insert";

        Clear();
        lblStatusMessage.Text = string.Empty;
        lblStatusMessage.Visible = false;
        BindDataList();
    }

    protected void ImgEdit_Click(object sender, ImageClickEventArgs e)
    {
        hfMode.Value = "Update";
        lblStatusMessage.Text = "";
        btnSave.Enabled = true;
        string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
        ViewState["id"] = Id;
        Edit(Convert.ToInt32(Id));
    }

    protected void ImgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            lblStatusMessage.Text = "";
            btnSave.Enabled = true;
            string Id = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
            objCriteria = new clsCriteriaReference();
            objCriteria.CriteriaId = Id.ToInt32();
            if (objCriteria.IsCriteriaUsed() > 0)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف المعايير، وجود المرجعية.") : ("Cannot delete criteria, reference exists."); //"Cannot delete criteria, reference exists.";
                return;
            }
            else
            {
                DeleteCriteria(Convert.ToInt32(Id));

                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير حذف بنجاح.") : ("Criteria deleted successfully."); //"Criteria deleted successfully.";
                lblStatusMessage.Visible = true;

                BindDataList();
                Clear();
            }
        }
        catch (SqlException)
        {
            lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف المعايير، وجود المرجعية.") : ("Cannot delete criteria, reference exists."); //"Cannot delete criteria, reference exists.";
            lblStatusMessage.Visible = true;
        }
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        AddCriteria();
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        Clear();
        mpe.Hide();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        Clear();
        mpe.Hide();
    }

    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        AddCriteria();
    }

    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {
        Clear();
        lblStatusMessage.Text = string.Empty;
    }

    #endregion Events

    #region Methods

    private void AddCriteria()
    {
        lblStatusMessage.Text = string.Empty;
        
        objCriteria = new clsCriteriaReference();
        objCriteria.DescriptionEng = txtCriteriaEng.Text.Trim();
        objCriteria.RemarksEng = txtDescEng.Text;
        objCriteria.DescriptionArb = txtCriteriaArb.Text.Trim();
        objCriteria.RemarksArb = txtDescArb.Text;

        if (hfMode.Value == "Insert")
        {
            if (objCriteria.CheckDescriptionExistence() > 0)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("موجود معايير مماثلة.") : ("Similar Criteria Exists."); //"Similar Criteria Exists.";
            }
            else
            {
                objCriteria.AddCriteriaReference();
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير وأضاف بنجاح.") : ("Criteria added successfully."); //"Criteria added successfully.";
            }
        }
        else
        {
            objCriteria.CriteriaId = Convert.ToInt32(ViewState["id"]);
            if (objCriteria.CheckDescriptionExistence() > 0)
            {
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("موجود معايير مماثلة.") : ("Similar Criteria Exists."); //"Similar Criteria Exists.";
            }
            else
            {               
                objCriteria.UpdateCriteriaReference();
                lblStatusMessage.Visible = true;
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("معايير تحديثها بنجاح.") : ("Criteria updated successfully."); //"Criteria updated successfully.";
            }
        }
        Clear();
    }

    private void Clear()
    {
        txtCriteriaEng.Text = string.Empty;
        txtDescEng.Text = string.Empty;
        txtCriteriaArb.Text = string.Empty;
        txtDescArb.Text = string.Empty;

        btnOK.Enabled = btnSave.Enabled = btnSaveData.Enabled = true;
        lblWarning.Text = string.Empty;

        hfMode.Value = "Insert";
    }

    public void BindDataList()
    {
        objCriteria = new clsCriteriaReference();
        dgvReference.DataSource = objCriteria.GetCriteria();
        dgvReference.DataBind();
    }

    private void DeleteCriteria(int Id)
    {
        objCriteria = new clsCriteriaReference();
        objCriteria.CriteriaId = Id;

        objCriteria.Delete();
    }

    private void Edit(int Id)
    {
        objCriteria = new clsCriteriaReference();
        objCriteria.CriteriaId = Id;
        using (DataTable dt = objCriteria.GetDataById())
        {
            if (dt.Rows.Count > 0)
            {
                txtCriteriaEng.Text = dt.Rows[0]["Description"].ToString();
                txtDescEng.Text = dt.Rows[0]["RemarksEng"].ToString();
                txtCriteriaArb.Text = dt.Rows[0]["DescriptionArb"].ToString();
                txtDescArb.Text = dt.Rows[0]["RemarksArb"].ToString();
            }
        }
    }

    #endregion Methods
}
