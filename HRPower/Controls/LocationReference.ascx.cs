﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class controls_LocationReference : System.Web.UI.UserControl
{
    clsJobPost objJobPost;

    public delegate void OnUpdate();
    public event OnUpdate Update;

    string sModalPopupID;

    int iLocationId;
    int iCompanyId;

    public int LocationId
    {
        set
        {
            iLocationId = value;
        }
        get
        {
            return iLocationId;
        }
    }

    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    public int CompanyId
    {
        set
        {
            iCompanyId = value;
        }
        get
        {
            return iCompanyId;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
      
       
    }
    protected void btnAddNew_Click(object sender, ImageClickEventArgs e)
    {
        Clear();
    }
    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        Save();
        mpe.Show();
      
    }
    protected void btnDelete_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        objJobPost = new clsJobPost();

        objJobPost.WorkLocationID = Convert.ToInt32(dlLocations.DataKeys[dlLocations.SelectedIndex]);

        try
        {
          
            if (objJobPost.IsExists())
            {
                lblWarning.Text = "Sorry, Unable to edit/delete.";
                return;
            }

            objJobPost.DeleteaLocation();

            if (Update != null)
                Update();

            Clear();

            Bind();
        }
        catch (SqlException ex)
        {
            Trace.Write(ex.Message);

            ScriptManager.RegisterClientScriptBlock(btnDelete, btnDelete.GetType(), "DeleteLocation", "alert('Sorry, Unable to delete.This location exists in the system.')", true);
        }
        mpe.Show();
    }
    protected void btnClear_Click(object sender, ImageClickEventArgs e)
    {
        Clear();
    }
    protected void btnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        btnAddNew.Enabled = btnDelete.Enabled = false;
        btnSaveData.Enabled = btnClear.Enabled = true;

        Clear();
        if (Update != null && iLocationId != 0)
        {

            Update();
        }
        mpe.Hide();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        Save();
        mpe.Show();

    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

            mpe.Hide();
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        btnAddNew.Enabled = btnDelete.Enabled = false;
        btnSaveData.Enabled = btnClear.Enabled = true;

        Clear();

        if (Update != null && iLocationId != 0)
        {
            Update();
        }
        mpe.Hide();
    }
    protected void dlLocations_SelectedIndexChanged(object sender, EventArgs e)
    {

        lblWarning.Text = string.Empty;
        objJobPost = new clsJobPost();

        dlLocations.SelectedItem.CssClass = "selected_item";

        objJobPost.WorkLocationID = Convert.ToInt32(dlLocations.DataKeys[dlLocations.SelectedIndex]);


        if (objJobPost.IsLocationExists())
        {
            btnDelete.Enabled = btnSaveData.Enabled = txtLocation.Enabled =  btnSave.Enabled = btnOK.Enabled = false;
            lblWarning.Text = "Sorry, Unable to edit/delete.";
        }
        else
        {
            btnDelete.Enabled = btnSaveData.Enabled = txtLocation.Enabled =  btnSave.Enabled = btnOK.Enabled = true;
        }

        using (SqlDataReader sdrLocation = objJobPost.BindaLocation())
        {
            if (sdrLocation.Read())
            {
                txtLocation.Text = Convert.ToString(sdrLocation["LocationName"]);
                txtAddress.Text = Convert.ToString(sdrLocation["Address"]);
                txtMobileNumber.Text = Convert.ToString(sdrLocation["Phone"]);
                txtEmail.Text = Convert.ToString(sdrLocation["Email"]).Trim();
            }
            sdrLocation.Close();
        }
       
    }

    private void Clear()
    {
        txtLocation.Text = string.Empty;
        txtAddress.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtMobileNumber.Text = string.Empty;

        dlLocations.SelectedIndex = -1;
        lblWarning.Text = string.Empty;

        btnDelete.Enabled = false;
    }

    private bool Save()
    {
        objJobPost = new clsJobPost();

        objJobPost.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        objJobPost.LocationName = txtLocation.Text;
        objJobPost.LOAddress = txtAddress.Text;
        objJobPost.LOPhone = txtMobileNumber.Text;
        objJobPost.LOEmail = txtEmail.Text;

        if (dlLocations.SelectedIndex == -1)
            iLocationId = objJobPost.InsertLocationReference();
        else
        {
           iLocationId = Convert.ToInt32(dlLocations.DataKeys[dlLocations.SelectedIndex]);
           objJobPost.WorkLocationID = Convert.ToInt32(dlLocations.DataKeys[dlLocations.SelectedIndex]);

           iLocationId = objJobPost.UpdateLocationReference();
        }
      
       if (iLocationId == 0)
       {
           lblWarning.Text = "Location already exists.";
           return false;
       }
       if (Update != null)
           Update();

       Bind();

       Clear();
       return true;
    }

    private void Bind()
    {
        lblWarning.Text = string.Empty;
        objJobPost = new clsJobPost();

        btnDelete.Enabled = false;

        objJobPost.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);

        dlLocations.DataSource = objJobPost.BindLocations();
        dlLocations.DataBind();
    }

    public void Fill()
    {
        objJobPost = new clsJobPost();

        string guid = clsCommon.GetValidationGroup();
        clsCommon.WriteLog(guid);
        // rfvtxtEmailpnlTab1.ValidationGroup = rfvtxtMobileNumberpnlTab1.ValidationGroup =revEmailpnlTab1.ValidationGroup
       
        revEmailpnlTab1.ValidationGroup =  rfvddlCompany.ValidationGroup  = rfvLocation.ValidationGroup = vsLocation.ValidationGroup = btnOK.ValidationGroup = btnSave.ValidationGroup = btnSaveData.ValidationGroup = guid;
        btnOK.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        btnSaveData.Attributes.Add("onclick", "return confirmSave('" + guid + "');");
        btnSave.Attributes.Add("onclick", "return confirmSave('" + guid + "');");

        ddlCompany.DataSource = objJobPost.GetParentCompanies();
        ddlCompany.DataBind();

        if (iCompanyId != 0 || iCompanyId != -1)
        {
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(iCompanyId.ToString()));
        }


        Bind();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        Clear();
       
        iCompanyId = Convert.ToInt32(ddlCompany.SelectedValue);

        Bind();
    }
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = divHeaderMove.ClientID;
    }
    
}
