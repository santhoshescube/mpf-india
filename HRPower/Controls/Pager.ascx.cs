﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class controls_Pager : System.Web.UI.UserControl
{
    public delegate void FillPager();

    public event FillPager Fill;

    public int CurrentPage
    {
        get
        {
            return Convert.ToInt16(hdCurrentPage.Value);
        }
        set
        {
            hdCurrentPage.Value = value.ToString();
        }
    }

    public int Total
    {
        set
        {
            hdPageCount.Value = (value / Convert.ToInt32(ddlPaging.SelectedValue) + (value % Convert.ToInt32(ddlPaging.SelectedValue) == 0 ? 0 : 1)).ToString();
            Refresh();
        }
    }

    public int PageSize
    {
        get
        {
            return Convert.ToInt32(ddlPaging.SelectedValue);
        }
        set
        {
            ddlPaging.SelectedIndex = ddlPaging.Items.IndexOf(ddlPaging.Items.FindByValue(value.ToString()));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      
    }

    private void Refresh()
    {
        lblPagecaption.Text = " of " + Convert.ToInt16(hdPageCount.Value);
        txtGotoPage.Text = Convert.ToString(CurrentPage + 1);

        lnkpageingNext.Enabled = true;
        lnkpageingPrev.Enabled = true;

        if (CurrentPage == Convert.ToInt16(hdPageCount.Value) - 1)
        {
            lnkpageingNext.Enabled = false;
        }

        if (CurrentPage == 0)
        {
            lnkpageingPrev.Enabled = false;
        }

        if ((CurrentPage == Convert.ToInt16(hdPageCount.Value) - 1) && (CurrentPage == 0))
        {
            lnkpageingNext.Enabled = false;
            lnkpageingPrev.Enabled = false;
        }
    }

    protected void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
    {
        int pagesize = Convert.ToInt32(ddlPaging.SelectedValue);
        CurrentPage = 0;
        Fill();
        lblPagecaption.Text = " of " + Convert.ToInt16(hdPageCount.Value);
        txtGotoPage.Text = Convert.ToString(CurrentPage + 1);       
    }

    protected void lnkpageingNext_click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage + 1; 
        Fill();
        if (CurrentPage == Convert.ToInt16(hdPageCount.Value) - 1) { lnkpageingNext.Enabled = false; }
        else { lnkpageingNext.Enabled = true; } lnkpageingPrev.Enabled = true;
        lblPagecaption.Text = " of " + Convert.ToInt16(hdPageCount.Value);
        txtGotoPage.Text =Convert.ToString(  CurrentPage + 1);
    }

    protected void lnkpageingPrev_click(object sender, EventArgs e)
    {
        CurrentPage = CurrentPage - 1;
        Fill();
        if (CurrentPage == 0) { lnkpageingPrev.Enabled = false;  }
        else { lnkpageingPrev.Enabled = true; } lnkpageingNext.Enabled = true;
        lblPagecaption.Text = " of " + Convert.ToInt16(hdPageCount.Value);
        txtGotoPage.Text = Convert.ToString( CurrentPage + 1);
    }    

    protected void txtGotoPage_Textchanged(object sender, EventArgs e)
    {
        if ( txtGotoPage.Text == string.Empty)
            txtGotoPage.Text = "1";

        int ipagesize = Convert.ToInt32(txtGotoPage.Text);
        if ((ipagesize > Convert.ToInt16(hdPageCount.Value)) || (ipagesize < 1)) { ipagesize = 1; }
        CurrentPage = ipagesize - 1; 
        Fill();
        lblPagecaption.Text = " of " + Convert.ToInt16(hdPageCount.Value);
        txtGotoPage.Text = Convert.ToString(CurrentPage + 1);
    }
}
