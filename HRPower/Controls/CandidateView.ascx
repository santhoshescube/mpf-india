﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CandidateView.ascx.cs"
    Inherits="Controls_CandidateView" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
<link href="../css/singleview.css" rel="stylesheet" type="text/css" />
<div id="divViewCandidate" runat="server">
    <h4 style="padding-top: 5px; padding-left: 5px; padding-bottom: 5px; color: #108dbc;">
        <asp:Literal ID="Literal4" runat ="server" meta:resourcekey="BasicInfo"></asp:Literal></h4>
    <div class="maindiv">
        <div class="innerdiv1">
           <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="CandidateCode"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="divCandidateCode" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1" style="display: none">
             <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="Salutation"></asp:Literal> 
        </div>
        <div class="innerdiv2" style="display: none">
            :
        </div>
        <div id="dvSalutation" class="innerdiv3" runat="server" style="display: none">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
           <asp:Literal ID="Literal3" runat ="server" meta:resourcekey="NameInEnglish"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCandidateNameEng" runat="server" class="innerdiv3">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal5" runat ="server" meta:resourcekey="NameinArabic"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCandidateNameAr" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="Citizenship"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCitizenShip" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Placeofbirth"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPlaceOfBirth" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
             <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="Gender"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvGender" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Dateofbirth"></asp:Literal>   </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvDateOfBirth" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="MaritalStatus"></asp:Literal>   </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvMaritalStatus" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal11" runat ="server" meta:resourcekey="NumberofSon"></asp:Literal> </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvNoOfSon" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
             <asp:Literal ID="Literal12" runat ="server" meta:resourcekey="Religion"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvReligion" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal13" runat ="server" meta:resourcekey="MotherName"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvMothersName" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal14" runat ="server" meta:resourcekey="CurrentNationality"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCurrentNationlity" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="PreviousNationality"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPreviousNationality" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal16" runat ="server" meta:resourcekey="Doctrine"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvDoctrine" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal17" runat ="server" meta:resourcekey="Telephone"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvTelephone" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal18" runat ="server" meta:resourcekey="POBox"></asp:Literal>   </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPOBOX" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal19" runat ="server" meta:resourcekey="Country"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCountry" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal20" runat ="server" meta:resourcekey="Email"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvEmail" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal21" runat ="server" meta:resourcekey="ReferralType"></asp:Literal>  </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvReferralType" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
             <asp:Literal ID="Literal22" runat ="server" meta:resourcekey="PlaceofBusinessPair"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPlaceOfBusinessPair" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal23" runat ="server" meta:resourcekey="ReferredBy"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvReferredBy" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
              <asp:Literal ID="Literal24" runat ="server" meta:resourcekey="CompanyType"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCompanyType" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal25" runat ="server" meta:resourcekey="Address"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCurrentAddress" class="innerdiv3" runat="server">
        </div>
    </div>
    <div style="height: auto">
        <div class="maindiv">
            <div style="height: auto">
                <div class="innerdiv1">
                    <asp:Literal ID="Literal26" runat ="server" meta:resourcekey="Address"></asp:Literal> </div>
                <div class="innerdiv2">
                    :
                </div>
                <div id="dvAddress" class="innerdiv3" runat="server">
                </div>
            </div>
        </div>
        <div style="clear: both">
        </div>
    </div>
    <div style="clear: both">
    </div>
    <h4 style="padding-top: 5px; padding-left: 5px; padding-bottom: 5px; color: #108dbc;">
         <asp:Literal ID="Literal27" runat ="server" meta:resourcekey="ProfessionalInfo"></asp:Literal></h4>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal28" runat ="server" meta:resourcekey="Qualification"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvQualification" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal29" runat ="server" meta:resourcekey="Category"></asp:Literal>
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCAtegory" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <%--<div class="innerdiv1">
            Graduation
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvGraduation" class="innerdiv3" runat="server">
        </div>--%>
        <div class="innerdiv1">
             <asp:Literal ID="Literal30" runat ="server" meta:resourcekey="GraduationYear"></asp:Literal>
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvGraduationYear" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
             <asp:Literal ID="Literal31" runat ="server" meta:resourcekey="PlaceofGraduation"></asp:Literal>
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPlaceOfGraduation" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal32" runat ="server" meta:resourcekey="College"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCollegeOrSchool" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal33" runat ="server" meta:resourcekey="Previousjob"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPreviousJob" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal34" runat ="server" meta:resourcekey="Sector"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvSector" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal35" runat ="server" meta:resourcekey="Position"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPosition" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
           <asp:Literal ID="Literal36" runat ="server" meta:resourcekey="Typeofexperience"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvTypeOfExperience" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal37" runat ="server" meta:resourcekey="Yearofexperience"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvYearsOfExperience" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal38" runat ="server" meta:resourcekey="Compensation"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCompensation" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal39" runat ="server" meta:resourcekey="Specialization"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvSpecialization" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
           <asp:Literal ID="Literal40" runat ="server" meta:resourcekey="Dateoftransaction"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvDateOfTransaction" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
           <asp:Literal ID="Literal41" runat ="server" meta:resourcekey="Expectedjoindate"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvExpectedJoinDate" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal42" runat ="server" meta:resourcekey="Skills"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvSkills" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal43" runat ="server" meta:resourcekey="LanguagesEng"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvLanguagesEnglish" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
          <asp:Literal ID="Literal44" runat ="server" meta:resourcekey="LanguagesArb"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvLaunguagesArabic" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal45" runat ="server" meta:resourcekey="LanguagesOth"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherLanguagesC2" class="innerdiv3" runat="server">
        </div>
        <%--<div class="innerdiv1">
            Languages (other 2)
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherLangC1" class="innerdiv3" runat="server">
        </div>--%>
    </div>
    <div style="clear: both">
    </div>
    <h4 style="padding-top: 5px; padding-left: 5px; padding-bottom: 5px; color: #108dbc;">
      <asp:Literal ID="Literal46" runat ="server" meta:resourcekey="DocumentInfo"></asp:Literal>    </h4>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal47" runat ="server" meta:resourcekey="PassportNumber"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPassportNumber" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
           <asp:Literal ID="Literal48" runat ="server" meta:resourcekey="Country"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPassportCountry" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal49" runat ="server" meta:resourcekey="Placeofissue"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPassportPlaceOfIssue" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal50" runat ="server" meta:resourcekey="Expirydate"></asp:Literal>  
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPassportExpiryDate" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
           <asp:Literal ID="Literal51" runat ="server" meta:resourcekey="Issuedate"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvPassportIssueDate" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal52" runat ="server" meta:resourcekey="Visanumber"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaNumber" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal53" runat ="server" meta:resourcekey="Unifiednumber"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaUnifiedNumber" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal54" runat ="server" meta:resourcekey="Country"></asp:Literal> 
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaCountry" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal55" runat ="server" meta:resourcekey="Placeofissue"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaPlaceOfIssue" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
          <asp:Literal ID="Literal56" runat ="server" meta:resourcekey="Expirydate"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaExpiryDate" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal57" runat ="server" meta:resourcekey="Issuedate"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvVisaIssueDate" class="innerdiv3" runat="server">
        </div>
        <div class="trLeft" style="float: left; width: 14%; height: 20px; padding-left: 0px">
        </div>
        <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
        </div>
        <div id="div35" class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px"
            runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
           <asp:Literal ID="Literal58" runat ="server" meta:resourcekey="Number"></asp:Literal>     
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvNationalCardNumber" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
           <asp:Literal ID="Literal59" runat ="server" meta:resourcekey="CardNumber"></asp:Literal>     
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCardNo" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
          <asp:Literal ID="Literal60" runat ="server" meta:resourcekey="Expirydate"></asp:Literal>     
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvCardNoExpiry" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="trLeft" style="float: left; width: 14%; height: 20px; padding-left: 0px">
    </div>
    <div class="trLeft" style="float: left; width: 01%; height: 20px; padding-left: 0px">
    </div>
    <div id="div63" class="trLeft" style="float: left; width: 30%; height: 20px; padding-left: 0px"
        runat="server">
    </div>
    <div style="clear: both">
    </div>
    <h4 style="padding-top: 5px; padding-left: 5px; padding-bottom: 5px; color: #108dbc;">
      <asp:Literal ID="Literal61" runat ="server" meta:resourcekey="OtherInfo"></asp:Literal>      </h4>
    <div style="height: auto">
        <div class="maindiv">
            <div class="innerdiv1">
               <asp:Literal ID="Literal62" runat ="server" meta:resourcekey="PermanentAddress"></asp:Literal>     
            </div>
            <div class="innerdiv2">
                :
            </div>
            <div id="dvOtherPermanentAddress" class="innerdiv3" runat="server">
            </div>
            <div class="innerdiv1">
                <asp:Literal ID="Literal63" runat ="server" meta:resourcekey="City"></asp:Literal>    
            </div>
            <div class="innerdiv2">
                :
            </div>
            <div id="dvOtherCity" class="innerdiv3" runat="server">
            </div>
        </div>
        <div style="clear: both">
        </div>
    </div>
    <div class="maindiv">
        <div style="height: auto">
            <div class="innerdiv1">
               <asp:Literal ID="Literal64" runat ="server" meta:resourcekey="BusinessAddress"></asp:Literal>     
            </div>
            <div class="innerdiv2">
                :
            </div>
            <div id="dvOtherBusinessAddress" class="innerdiv3" runat="server">
            </div>
            <div class="innerdiv1">
                <asp:Literal ID="Literal65" runat ="server" meta:resourcekey="BusinessStreet"></asp:Literal>    
            </div>
            <div class="innerdiv2">
                :
            </div>
            <div id="dvOtherBusinessStreet" class="innerdiv3" runat="server">
            </div>
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal66" runat ="server" meta:resourcekey="HomeStreet"></asp:Literal>    </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherHomeStreet" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal67" runat ="server" meta:resourcekey="HomeTelephone"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherHomeTelephone" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal68" runat ="server" meta:resourcekey="Area"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherArea" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
           <asp:Literal ID="Literal69" runat ="server" meta:resourcekey="POBox"></asp:Literal>     
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherPOBox" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
             <asp:Literal ID="Literal70" runat ="server" meta:resourcekey="Fax"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherFax" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
            <asp:Literal ID="Literal71" runat ="server" meta:resourcekey="Telephone"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherTelephone" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal72" runat ="server" meta:resourcekey="MobileNumber"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherMob" class="innerdiv3" runat="server">
        </div>
        <div class="innerdiv1">
             <asp:Literal ID="Literal73" runat ="server" meta:resourcekey="Signupdate"></asp:Literal>   
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherSignUpDate" class="innerdiv3" runat="server">
        </div>
    </div>
    <div class="maindiv">
        <div class="innerdiv1">
            <asp:Literal ID="Literal74" runat ="server" meta:resourcekey="Insurancenumber"></asp:Literal>    
        </div>
        <div class="innerdiv2">
            :
        </div>
        <div id="dvOtherInsurance" class="innerdiv3" runat="server">
        </div>
    </div>
</div>
