﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Controls_CandidateView : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (IsPostBack)
        //    GetCandidateInfo();

    }


    public void GetCandidateInfo(Int64 CandidateID)
    {
        DataRow DrCandidateBasicInfo;
        DataRow DrCandidateProfessionalInfo;
        DataRow DrCandidateDocumentInfo;
        DataRow DrCandidateOtherInfo;


        string English ="";
        string Arabic ="";
        string OtherLan = "";
        using (DataSet ds = clsCandidate.GetCandidateDetails(CandidateID))
        {
            if (ds.Tables.Count == 4)
            {

                //Candidat Basic info
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DrCandidateBasicInfo = ds.Tables[0].Rows[0];

                    divCandidateCode.InnerText = DrCandidateBasicInfo["CandidateCode"].ToString();

                    dvSalutation.InnerText =  DrCandidateBasicInfo["Salutation"].ToString();
  
                    dvCandidateNameEng.InnerText =  DrCandidateBasicInfo["CandidateNameEnglish"].ToString();
 
                    dvCandidateNameAr.InnerText = DrCandidateBasicInfo["CandidateNameArabic"].ToString();

                    dvCitizenShip.InnerText = DrCandidateBasicInfo["CitizenShip"].ToString();

                    dvGender.InnerText = DrCandidateBasicInfo["Gender"].ToString();

                    dvPlaceOfBirth.InnerText = DrCandidateBasicInfo["PlaceOfBirth"].ToString();

                    dvDateOfBirth .InnerText = DrCandidateBasicInfo["DateOfBirth"].ToString();

                    dvMaritalStatus.InnerText = DrCandidateBasicInfo["MaritalStatus"].ToString();

                    dvNoOfSon.InnerText = DrCandidateBasicInfo["NoOfSon"].ToString();

                    dvReligion.InnerText = DrCandidateBasicInfo["Religion"].ToString();

                    dvMothersName.InnerText = DrCandidateBasicInfo["MothersName"].ToString();

                    dvCurrentNationlity .InnerText = DrCandidateBasicInfo["CurrentNationlity"].ToString();

                    dvPreviousNationality.InnerText = DrCandidateBasicInfo["PreviousNationality"].ToString();
 
                    dvDoctrine.InnerText = DrCandidateBasicInfo["Doctrine"].ToString();

                    dvTelephone.InnerText = DrCandidateBasicInfo["Telephone"].ToString();

                    dvAddress.InnerText = DrCandidateBasicInfo["BusinessPairAddress"].ToString();
 
                    dvPOBOX.InnerText = DrCandidateBasicInfo["POBOX"].ToString();

                    dvPlaceOfBusinessPair .InnerText = DrCandidateBasicInfo["PlaceOfBusinessPair"].ToString();

                    dvCompanyType.InnerText = DrCandidateBasicInfo["CompanyType"].ToString();

                    dvCurrentAddress.InnerText = DrCandidateBasicInfo["CurrentAddress"].ToString();

                    dvEmail.InnerText  = DrCandidateBasicInfo["EmailID"].ToString();

                    dvReferralType.InnerText = DrCandidateBasicInfo["ReferralType"].ToString();

                    if (DrCandidateBasicInfo["Agency"].ToString() != "")    //If Referred By Agency
                    {
                        dvReferredBy.InnerText = DrCandidateBasicInfo["Agency"].ToString();
                    }
                    else if (DrCandidateBasicInfo["Description"].ToString() != "")      //If Referred By JobPortal
                    {
                        dvReferredBy.InnerText = DrCandidateBasicInfo["Description"].ToString();
                    }
                    else if (DrCandidateBasicInfo["EmployeeFullName"].ToString() != "")     //If Referred By Employee
                    {
                        dvReferredBy.InnerText = DrCandidateBasicInfo["EmployeeFullName"].ToString();
                    }
                    else
                    {
                        dvReferredBy.InnerText = "";
                    }

                    dvCountry.InnerText = DrCandidateBasicInfo["Country"].ToString();

                }


                if (ds.Tables[1].Rows.Count > 0)
                {
                    DrCandidateProfessionalInfo = ds.Tables[1].Rows[0];

                    dvQualification.InnerText = DrCandidateProfessionalInfo["QualificationType"].ToString();

                    dvCAtegory.InnerText = DrCandidateProfessionalInfo["QualificationCategory"].ToString();

                    //dvGraduation.InnerText = DrCandidateProfessionalInfo["Graduation"].ToString();

                    dvGraduationYear.InnerText = DrCandidateProfessionalInfo["GraduationYear"].ToString();

                    dvPlaceOfGraduation.InnerText = DrCandidateProfessionalInfo["PlaceOfGraduation"].ToString();

                    dvCollegeOrSchool.InnerText = DrCandidateProfessionalInfo["CollegeSchool"].ToString();

                    dvPreviousJob.InnerText = DrCandidateProfessionalInfo["PreviousJob"].ToString();

                    dvSector.InnerText = DrCandidateProfessionalInfo["Sector"].ToString();

                    dvPosition.InnerText = DrCandidateProfessionalInfo["Position"].ToString();

                    //dvPosition.InnerText = DrCandidateProfessionalInfo[""].ToString();

                    dvTypeOfExperience.InnerText = DrCandidateProfessionalInfo["TypeOfExperience"].ToString();

                    dvYearsOfExperience.InnerText = DrCandidateProfessionalInfo["Experienceyear"].ToString();

                    dvCompensation.InnerText = DrCandidateProfessionalInfo["Compensation"].ToString();

                    dvSpecialization.InnerText = DrCandidateProfessionalInfo["Specialization"].ToString();

                    dvDateOfTransaction.InnerText = DrCandidateProfessionalInfo["TransactionEntryDate"].ToString();

                    dvExpectedJoinDate.InnerText = DrCandidateProfessionalInfo["ExpectedJoinDate"].ToString();

                    dvSkills.InnerText = DrCandidateProfessionalInfo["Skills"].ToString();

                  

                        //if(Convert.ToBoolean(DrCandidateProfessionalInfo["ReadingEng"]))
                        //{
                        //   English = "Read ";
                        //}

                        //if(Convert.ToBoolean(DrCandidateProfessionalInfo["WritingEng"]))
                        //{
                        //   English =English + " Write ";
                        //}

                        //dvLanguagesEnglish.InnerText = English;




                        //if (Convert.ToBoolean(DrCandidateProfessionalInfo["ReadingArb"]))
                        //{
                        //    Arabic  = "Read ";
                        //}

                        //if (Convert.ToBoolean(DrCandidateProfessionalInfo["WritingArb"]))
                        //{
                        //    Arabic = Arabic + " Write ";
                        //}



                        //dvLaunguagesArabic.InnerText = Arabic;




                    if (Convert.ToString(DrCandidateProfessionalInfo["ReadingEng"]) != "")
                    {
                        English = Convert.ToString(DrCandidateProfessionalInfo["ReadingEng"]);
                    }

                    if (Convert.ToString(DrCandidateProfessionalInfo["WritingEng"]) != "")
                    {
                        English = English + ' '+  Convert.ToString(DrCandidateProfessionalInfo["WritingEng"]);
                    }

                    dvLanguagesEnglish.InnerText = English;




                    if (Convert.ToString(DrCandidateProfessionalInfo["ReadingArb"]) != "")
                    {
                        Arabic = Convert.ToString(DrCandidateProfessionalInfo["ReadingArb"]);
                    }

                    if (Convert.ToString(DrCandidateProfessionalInfo["WritingArb"]) != "")
                    {
                        Arabic = Arabic + ' ' + Convert.ToString(DrCandidateProfessionalInfo["WritingArb"]);
                    }



                    dvLaunguagesArabic.InnerText = Arabic;


//dvOtherLangC1.InnerText =

                     dvOtherLanguagesC2.InnerText = "";


                    if (DrCandidateProfessionalInfo["Other"].ToString() == "")
                    {
                        //dvOtherLangC1.InnerText =
                        dvOtherLanguagesC2.InnerText = "";

                        //dvOtherLangC1.Attributes.Add("style", "display:none");
                        dvOtherLanguagesC2.Attributes.Add("style", "display:none");
                    }
                    else
                    {
                       // dvOtherLangC1.Attributes.Add("style", "display:block");
                        dvOtherLanguagesC2.Attributes.Add("style", "display:block");

                        if (Convert.ToBoolean(DrCandidateProfessionalInfo["ReadingOther"]))
                        {
                            OtherLan = clsGlobalization.IsArabicCulture() ? "قراءة" : "  Reading"; 
                        }

                        if (Convert.ToBoolean(DrCandidateProfessionalInfo["WritingOther"]))
                        {
                            string sRead = clsGlobalization.IsArabicCulture() ? "كتابة" : "  Writing";
                            OtherLan = OtherLan + " " + sRead;
                        }



                        dvOtherLanguagesC2.InnerText = DrCandidateProfessionalInfo["Other"].ToString() + "  -  " + OtherLan;
                       // dvOtherLanguagesC2.InnerText = OtherLan;


                    }
                 

                  
 
                }


                if (ds.Tables[2].Rows.Count > 0)
                {
                    DrCandidateDocumentInfo = ds.Tables[2].Rows[0];

                    dvPassportNumber.InnerText = DrCandidateDocumentInfo["PassportNumber"].ToString();

                    dvPassportCountry.InnerText = DrCandidateDocumentInfo["PassportCountry"].ToString();

                    dvPassportExpiryDate.InnerText = DrCandidateDocumentInfo["PassportExpiryDate"].ToString();

                    dvPassportIssueDate.InnerText = DrCandidateDocumentInfo["PassportIssueDate"].ToString();

                    dvPassportPlaceOfIssue.InnerText = DrCandidateDocumentInfo["PassportIssuePlace"].ToString();



                    dvVisaCountry.InnerText = DrCandidateDocumentInfo["VisaCountry"].ToString();

                    dvVisaExpiryDate.InnerText = DrCandidateDocumentInfo["VisaExpiryDate"].ToString();

                    dvVisaIssueDate.InnerText = DrCandidateDocumentInfo["VisaIssueDate"].ToString();

                    dvVisaNumber.InnerText = DrCandidateDocumentInfo["VisaNumber"].ToString();

                    dvVisaPlaceOfIssue.InnerText = DrCandidateDocumentInfo["VisaISsuePlace"].ToString();

                    dvVisaUnifiedNumber.InnerText = DrCandidateDocumentInfo["UnifiedNumber"].ToString();



                    dvNationalCardNumber.InnerText = DrCandidateDocumentInfo["NationalCardNumber"].ToString();

                    dvCardNo.InnerText = DrCandidateDocumentInfo["CardNumber"].ToString();

                    dvCardNoExpiry.InnerText = DrCandidateDocumentInfo["NationalCardExpiryDate"].ToString(); 
 




                }


                if (ds.Tables[3].Rows.Count > 0)
                {
                    DrCandidateOtherInfo = ds.Tables[3].Rows[0];

                    dvOtherPermanentAddress.InnerText = DrCandidateOtherInfo["PermanentAddress"].ToString();

                    dvOtherHomeStreet.InnerText = DrCandidateOtherInfo["HomeStreet"].ToString();

                    dvOtherCity.InnerText = DrCandidateOtherInfo["City"].ToString();

                    dvOtherHomeTelephone.InnerText = DrCandidateOtherInfo["HomeTelephone"].ToString();

                    dvOtherBusinessAddress.InnerText = DrCandidateOtherInfo["BusinessAddress"].ToString();

                    dvOtherBusinessStreet.InnerText = DrCandidateOtherInfo["BusinessStreet"].ToString();

                    dvOtherArea.InnerText = DrCandidateOtherInfo["Area"].ToString();

                    dvOtherPOBox.InnerText = DrCandidateOtherInfo["POBOX"].ToString();

                    dvOtherFax.InnerText = DrCandidateOtherInfo["Fax"].ToString();

                    dvOtherTelephone.InnerText = DrCandidateOtherInfo["TelephoneNumber"].ToString();

                    dvOtherMob.InnerText = DrCandidateOtherInfo["MobileNumber"].ToString();

                    dvOtherSignUpDate.InnerText = DrCandidateOtherInfo["SignUpdate"].ToString();

                    //dvOtherLoginPassword.InnerText = DrCandidateOtherInfo[""].ToString();

                    dvOtherInsurance.InnerText = DrCandidateOtherInfo["InsuranceNumber"].ToString();

                    //dvOtherPlaceOfBusiness.InnerText = DrCandidateOtherInfo["PlaceOfBusinessPair"].ToString();

                    //dvOtherAddress.InnerText = DrCandidateOtherInfo["BusinessPairAddress"].ToString();

                    //dvOtherCompanyType.InnerText = DrCandidateOtherInfo["CompanyType"].ToString();


                }





            }


        }
    }

}
