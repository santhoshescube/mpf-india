﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DegreeReference.ascx.cs"
    Inherits="controls_DegreeReference" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<script type ="text/javascript">
function ValidateReferenceControl()
{
alert("fdfdf");
return false;
}
--%>

<script src="../js/HRPower.js" type="text/javascript"></script>

<style type="text/css">
    .style1
    {
        height: 33px;
    }
    #headername
    {
        width: 545px;
    }
    #close1
    {
        width: 37px;
    }
</style>
<div style="display: none">
    <asp:HiddenField ID="hfId" runat="server" />
    <asp:HiddenField ID="hfDataTextField" runat="server" />
    <asp:HiddenField ID="hfDataValueField" runat="server" />
    <asp:HiddenField ID="hfDataTextValue" runat="server" />
    <asp:HiddenField ID="hfDataNoValue" runat="server" />
    <asp:HiddenField ID="hfPredefinedField" runat="server" />
    <asp:HiddenField ID="hfTableName" runat="server" />
    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
    <asp:HiddenField ID="hfFunctionName" runat="server" />
    <asp:HiddenField ID="hfReferenceField" runat="server" />
    <asp:HiddenField ID="hfReferenceId" runat="server" />
    <asp:HiddenField ID="hfDisplayName" runat="server" />
</div>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 652px !important;">
        <div id="divHeaderMove" runat="server">
            <div id="header11">
                <div id="hbox1">
                    <div id="headername">
                        <%--Qualification--%>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Qualification1"></asp:Literal>
                    </div>
                </div>
                <div id="hbox2">
                    <div id="close1">
                        <asp:ImageButton ID="imgPopupClose" runat="server" OnClick="ibtnClose_Click" ImageUrl="~/Controls/images/icon-close.png"
                            CausesValidation="False" />
                    </div>
                </div>
            </div>
        </div>
        <%-- <div id="toolbar" style="height: 10px;">
            <div id="imgbuttons">
                
            </div>
        </div>--%>
        <div id="contentwrap">
            <div id="content">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%;">
                            <div style="width: 100%;">
                                <fieldset>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 20%; height: 30px; float: left;">
                                            <%--Level--%>
                                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Level"></asp:Literal>
                                        </div>
                                        <div style="width: 80%; height: 30px;">
                                            <asp:DropDownList ID="ddlQualificationType" runat="server" ValidationGroup="Save"
                                                AutoPostBack="true" DataTextField="QualificationType" DataValueField="QualificationTypeId"
                                                CssClass="dropdownlist_mandatory" Width="205px" Height="25px" OnSelectedIndexChanged="ddlQualificationType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 20%; height: 30px; float: left;">
                                            <%--Qualification--%>
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Qualification"></asp:Literal>
                                        </div>
                                        <div style="width: 37%; height: 30px; float: left;">
                                            <asp:TextBox ID="txtDegreeName" runat="server" BorderStyle="Solid" ValidationGroup="Save"
                                                MaxLength="50" BorderWidth="1px" Height="8px" Width="200px"></asp:TextBox>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                Display="Dynamic" ValidationGroup="Save" ControlToValidate="txtDegreeName" meta:resourcekey="Fieldrequired"></asp:RequiredFieldValidator><br />--%><%--ErrorMessage="Field required" CssClass="error"--%>
                                        </div>
                                        <div style="width: 20%; height: 30px; float: left;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                ValidationGroup="Save" ControlToValidate="txtDegreeName" meta:resourcekey="Fieldrequired"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div style="width: 100%; height: 30px; margin-top: 10px;">
                                        <div style="width: 20%; height: 30px; float: left;">
                                            <%--QualificationArb--%>
                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="QualificationArb"></asp:Literal>
                                        </div>
                                        <div style="width: 37%; height: 30px; float: left;">
                                            <asp:TextBox ID="txtDegreeNameArb" runat="server" BorderStyle="Solid" ValidationGroup="Save"
                                                MaxLength="50" BorderWidth="1px" Height="8px" Width="200px"></asp:TextBox>
                                        </div>
                                        <div style="width: 20%; height: 30px; float: left;">
                                            <asp:RequiredFieldValidator ID="rfvtxtDegreeName" runat="server" Display="Dynamic"
                                                ValidationGroup="Save" ControlToValidate="txtDegreeNameArb" meta:resourcekey="FieldrequiredArb"></asp:RequiredFieldValidator>
                                            <asp:ImageButton ID="btnSave" runat="server" vertical-align="top" Style="margin-right: 3px;
                                                margin-left: 3px; width: 14px; height: 16px;" OnClick="btnSave_Click" Width="16px"
                                                meta:resourcekey="Save" ValidationGroup="Save" ImageUrl="~/images/save_icon.jpg" /><%-- ToolTip="Save" OnClientClick="return ValidateSaveQualification(this);" CausesValidation="false" --%>
                                            <asp:ImageButton ID="imgClear" runat="server" vertical-align="top" Style="margin-right: 3px;
                                                margin-left: 3px; width: 14px; height: 16px;" CausesValidation="false" Width="16px"
                                                OnClick="imgClear_Click" meta:resourcekey="Clear" ImageUrl="~/images/new_icon_reference.PNG" />
                                            <%--ToolTip="Clear"--%>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div style="width: 100%;">
                            <fieldset>
                                <div style="max-height: 215px; overflow: auto; width: 100%; height: 111px;">
                                    <asp:GridView ID="gvDegreeReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                        Width="600px" DataKeyNames="DegreeID" OnRowCommand="gvDegreeReference_RowCommand">
                                        <HeaderStyle CssClass="datalistheader" />
                                        <Columns>
                                            <asp:TemplateField meta:resourcekey="Degree" ItemStyle-CssClass="datalistTrLeft"
                                                ItemStyle-VerticalAlign="Top">
                                                <%--HeaderText="Degree"--%>
                                                <ItemTemplate>
                                                    <div style="width: 150px; overflow: hidden">
                                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft" Width="25px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField meta:resourcekey="Type" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                <%--HeaderText="Type"--%>
                                                <ItemTemplate>
                                                    <div style="width: 125px; overflow: hidden">
                                                        <asp:Label ID="lblQualificationType" runat="server" Text='<%# Eval("QualificationType") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField meta:resourcekey="DegreeArb" ItemStyle-CssClass="datalistTrLeft"
                                                ItemStyle-VerticalAlign="Top">
                                                <%--HeaderText="Degree"--%>
                                                <ItemTemplate>
                                                    <div style="width: 150px; overflow: hidden">
                                                        <asp:Label ID="lblDescriptionArb" runat="server" Text='<%# Eval("DescriptionArb") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" CssClass="datalistTrLeft" Width="25px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField meta:resourcekey="TypeArb" ItemStyle-CssClass="datalistTrLeft"
                                                ItemStyle-VerticalAlign="Top">
                                                <%--HeaderText="TypeArb"--%>
                                                <ItemTemplate>
                                                    <div style="width: 125px; overflow: hidden">
                                                        <asp:Label ID="lblQualificationTypeArb" runat="server" Text='<%# Eval("QualificationTypeArb") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px"></ItemStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("DegreeID") %>'
                                                        CommandName="_ALTER" OnClientClick="return true" meta:resourcekey="edit" ImageUrl="~/images/edit_popup.png" />
                                                    <%--ToolTip="edit"--%>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("DegreeID") %>'
                                                        OnClientClick="return confirm(&quot;Are you sure you want to delete this?&quot;);"
                                                        CommandName="_REMOVE" meta:resourcekey="edit" ImageUrl="~/images/delete_popup.png" />
                                                    <%--ToolTip="Delete" --%>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </fieldset>
                        </div>
                       
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11" style="height: 30px;">
                <asp:UpdatePanel ID="upfooter" runat="server">
                    <ContentTemplate>
                        <%--       <div id="buttons">--%>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="false"></asp:Label>
                        <%--  </div>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</div>
