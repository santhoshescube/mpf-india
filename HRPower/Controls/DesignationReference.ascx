﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DesignationReference.ascx.cs"
    Inherits="Controls_DesignationReference" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<link href="../css/HRPower.css" rel="stylesheet" type="text/css" disabled="disabled" />
    type="text/css" disabled="disabled" />--%>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<style type="text/css">
   #popupmain
   {
   	width: 500px !important;
   }
</style>
<script src="../js/HRPower.js" type="text/javascript"></script>

<script type="text/javascript">

    function closThis(elementID) {
        var e = document.getElementById(elementID);

        if (e) {
            e.style.display = 'none';
            return false;
        }
    }

</script>

<style type="text/css">
    .style1
    {
        width: 230px;
    }
</style>

<div id="popupmainwrap">
    <div id="popupmain" style="width: 500px !important;">
        <div id="header11">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="HRPower" ></asp:Label><%--Text="HR Power"--%>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset>
                        <div id="content1">
                            <table runat="server" width="450px;">
                                <tr>
                                    <td align="center" style="width:150px;">
                                        <%--Parent Designation--%>
                                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ParentDesignation"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:DropDownList ID="ddlParent" runat="server" DataTextField="Designation" DataValueField="DesignationID"
                                            Width="237px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  style="width:150px;">
                                        <%--Designation--%>
                                         <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Designation"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtDesignation" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Width="229px" MaxLength="50"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="rfvtxtDesignation" runat="server" meta:resourcekey="PleaseenterDesignation" 
                                            ControlToValidate="txtDesignation"  ValidationGroup="Save1" Display="Dynamic" CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Designation"--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center"  style="width:150px;">
                                        <%--Designation--%>
                                         <asp:Literal ID="Literal3" runat="server" meta:resourcekey="DesignationArb"></asp:Literal>
                                    </td>
                                    <td align="center">
                                        <asp:TextBox ID="txtDesignationArb" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Width="229px" MaxLength="50"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" meta:resourcekey="PleaseenterDesignationArb" 
                                            ControlToValidate="txtDesignationArb" Display="Dynamic"  ValidationGroup="Save1" CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Designation"--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                    </td>
                                    <td>
                                        <%--OnClientClick="return ValidateDesignationControl(this);"--%>
                                        <asp:ImageButton ID="btnSave" runat="server" vertical-align="top" Style="margin-right: 3px; 
                                            margin-left: 3px; width: 14px; height: 16px;" OnClick="btnAdd_Click" Width="16px" ValidationGroup="Save1"
                                           meta:resourcekey="Save" ImageUrl="~/images/save_icon.jpg" />&nbsp;<%--ToolTip="Save"--%>
                                        <asp:ImageButton ID="imgClear" runat="server" vertical-align="top" Style="margin-right: 3px;
                                            margin-left: 3px; width: 14px; height: 16px;" CausesValidation="false" Width="16px"
                                            OnClick="imgClear_Click" meta:resourcekey="AddNewItem" ImageUrl="~/images/new_icon_reference.PNG" /><%--ToolTip="Add New Item"--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <div id="content" style="max-height: 226px; overflow: auto;">
                            <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                Width="100%">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:TemplateField meta:resourcekey="Particular" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"><%--HeaderText="Particular"--%>
                                        <ItemTemplate>
                                            <div style="width: 180px; overflow: hidden">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Designation") %>'></asp:Label><%--DataText--%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="ParticularArb" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"><%--HeaderText="Particular"--%>
                                        <ItemTemplate>
                                            <div style="width: 180px; overflow: hidden">
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("DesignationArb") %>'></asp:Label><%--DataText--%>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("DesignationID") %>'
                                                OnClick="ImgEdit_Click1" CausesValidation="false" OnClientClick="return true" meta:resourcekey="Edit" ImageUrl="~/images/edit_popup.png" />  <%--ToolTip="edit"--%>
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div id="footerwrapper">
                    <div id="footer11" style="height: 30px;">
                        <%--       <div id="buttons">--%>
                        <asp:Label ID="lblStatusMessage" Text="" CssClass="error" runat="server" Visible="False"></asp:Label>
                        <%--  </div>--%>
                    </div>
                </div>
                <div style="display: none">
                    <asp:HiddenField ID="hfId" runat="server" />
                    <asp:HiddenField ID="hfDataTextField" runat="server" />
                    <asp:HiddenField ID="hfDataValueField" runat="server" />
                    <asp:HiddenField ID="hfDataTextValue" runat="server" />
                    <asp:HiddenField ID="hfDataNoValue" runat="server" />
                    <asp:HiddenField ID="hfPredefinedField" runat="server" />
                    <asp:HiddenField ID="hfTableName" runat="server" />
                    <asp:HiddenField ID="hfCurrentIndex" runat="server" />
                    <asp:HiddenField ID="hfFunctionName" runat="server" />
                    <asp:HiddenField ID="hfReferenceField" runat="server" />
                    <asp:HiddenField ID="hfReferenceId" runat="server" />
                    <asp:HiddenField ID="hfDisplayName" runat="server" />
                    <asp:HiddenField ID="hfPreviousID" runat="server" Value="0" />
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfVaidationGroup" runat="server" Value="" />
                    <asp:HiddenField ID="hfParentElementId" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeFiled" runat="server" Value="" />
                    <asp:HiddenField ID="hfExcludeValues" runat="server" Value="" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
