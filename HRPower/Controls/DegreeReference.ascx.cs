﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data;
using System.Data.SqlClient;

public partial class controls_DegreeReference : System.Web.UI.UserControl
{
    #region Declarations

    clsDegreeReference objclsDegreeReference;

    public delegate void OnUpdate();
    public event OnUpdate Update;
    public delegate void SaveDegree(int DegreeID);
    public event SaveDegree OnSave;

    public string SelectedValue { get; set; }
    string sModalPopupID;
    public string ModalPopupID
    {
        set
        {
            sModalPopupID = value;
        }
    }

    #endregion Declarations

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {        
        if (!IsPostBack)
        {
            lblStatusMessage.Text = "";
            lblStatusMessage.Visible = false;            
        }
        if (clsGlobalization.IsArabicCulture())
        {
            rfvtxtDegreeName.ValidationGroup = RequiredFieldValidator1.ValidationGroup = "Save1";
        }
        else
        {
            rfvtxtDegreeName.ValidationGroup = "";
            RequiredFieldValidator1.ValidationGroup = "Save1";
            rfvtxtDegreeName.ErrorMessage = "";
        }      
                
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);

        if (Update != null)
        {
            Update();
        }
        //if (OnSave != null)
        //{
        //    OnSave(SelectedValue.ToInt32());
        //}
        
        mpe.Hide();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        int resId = Save();
        SelectedValue =Convert.ToString(resId);
        if (resId > 0)
        {
            ClearControls();
            BindGrid();

            if (Update != null)
            {
                Update();
            }
            if (OnSave != null)
            {
                OnSave(resId);
            }
        }
    }

    protected void gvDegreeReference_RowCommand(object sender, GridViewCommandEventArgs e)
    {      
        objclsDegreeReference = new clsDegreeReference();
        
        switch (e.CommandName)
        {
            case "_ALTER":

                lblStatusMessage.Text = "";

                objclsDegreeReference.DegreeID = Convert.ToInt32(e.CommandArgument);
                ViewState["DegreeID"] = Convert.ToInt32(e.CommandArgument);

                DataTable dt = objclsDegreeReference.GetDegreeDetail();

                if (dt.Rows.Count > 0)
                {
                    txtDegreeName.Text = dt.Rows[0]["Description"].ToString();
                    txtDegreeNameArb.Text = dt.Rows[0]["DescriptionArb"].ToString();
                    ddlQualificationType.SelectedIndex = ddlQualificationType.Items.IndexOf(ddlQualificationType.Items.FindByValue(dt.Rows[0]["QualificationTypeID"].ToString()));
                }

                break;

            case "_REMOVE":

                lblStatusMessage.Text = "";

                GridViewRow gr = (GridViewRow)((ImageButton)e.CommandSource).Parent.Parent;
                ImageButton ImgDelete = (ImageButton)gr.FindControl("ImgDelete");                
                try
                {
                    SelectedValue = Convert.ToString(e.CommandArgument);
                    objclsDegreeReference.DegreeID = Convert.ToInt32(e.CommandArgument);
                    objclsDegreeReference.Delete();
                    lblStatusMessage.Visible = true;
                    lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حذف بنجاح") : ("Deleted Successfully"); 
                    BindGrid();
                    ClearControls();

                    if (Update != null)
                    {
                        Update();
                    }
                    if (OnSave  != null)
                    {
                        OnSave(0);
                    }
                }
                catch (SqlException ex)
                {
                    Trace.Write(ex.Message);

                    ScriptManager.RegisterClientScriptBlock(ImgDelete, ImgDelete.GetType(), "DeleteDegree", "alert('Sorry, Unable to delete.This degree details exists in the system.')", true);
                }

                break;
        }
    }

    protected void imgClear_Click(object sender, EventArgs e)
    {
        ClearControls();
        lblStatusMessage.Text = "";
        lblStatusMessage.Visible = false;
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        AjaxControlToolkit.ModalPopupExtender mpe = (AjaxControlToolkit.ModalPopupExtender)this.Parent.NamingContainer.FindControl(sModalPopupID);
        mpe.PopupDragHandleControlID = divHeaderMove.ClientID;
    }

    protected void ddlQualificationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblStatusMessage.Text = "";
        lblStatusMessage.Visible = false;
        BindGrid();
    }

    #endregion Events

    #region Methods

    public void FillList()
    {
        objclsDegreeReference = new clsDegreeReference();
        ddlQualificationType.DataSource = objclsDegreeReference.FillQualificationTypes();
        ddlQualificationType.DataBind();

        BindGrid();
        if (SelectedValue != "")
        {
            objclsDegreeReference.DegreeID = SelectedValue.ToInt32();
            ViewState["DegreeID"] = SelectedValue.ToInt32();

            DataTable dt = objclsDegreeReference.GetDegreeDetail();

            if (dt.Rows.Count > 0)
            {
                txtDegreeName.Text = dt.Rows[0]["Description"].ToString();
                txtDegreeNameArb.Text = dt.Rows[0]["DescriptionArb"].ToString();
                ddlQualificationType.SelectedIndex = ddlQualificationType.Items.IndexOf(ddlQualificationType.Items.FindByValue(dt.Rows[0]["QualificationTypeID"].ToString()));
            }
        }
    }

    private void BindGrid()
    {
        DataSet ds = null;
        objclsDegreeReference = new clsDegreeReference();

        if (ddlQualificationType.SelectedValue.Trim() != string.Empty && ddlQualificationType.SelectedValue.Trim() != "-1")
        {
            objclsDegreeReference.QualificationTypeID = int.Parse(ddlQualificationType.SelectedValue);
            ds = objclsDegreeReference.BindDegreesByQualType();
            gvDegreeReference.DataSource = ds;
        }
        else
        {
            gvDegreeReference.DataSource = objclsDegreeReference.BindAllDegreeReferences();
        }
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvDegreeReference.DataBind();
        }
        else
        {
            gvDegreeReference.DataBind();
            lblStatusMessage.Visible = true;
            lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يوجد مثل هذا السجل!!") : ("No Such Record !!"); //"No Such Record !!";
        }
    }

    private int Save()
    {
        objclsDegreeReference = new clsDegreeReference();

        objclsDegreeReference.Description = txtDegreeName.Text;
        objclsDegreeReference.DescriptionArb = txtDegreeNameArb.Text;
        objclsDegreeReference.QualificationTypeID = Convert.ToInt32(ddlQualificationType.SelectedValue);
        int res = 0;
        lblStatusMessage.Visible = true;
        if (Convert.ToInt32(ViewState["DegreeID"]) > 0 && ViewState["DegreeID"] != null)
        {
            objclsDegreeReference.DegreeID = Convert.ToInt32(ViewState["DegreeID"]);
            res = objclsDegreeReference.UpdateDegreeReference();
            if (res < 0)
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("اسم درجة موجود بالفعل.") : ("Degree name already exists."); //"Degree name already exists.";
            else
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث التفاصيل بنجاح درجة.") : ("Successfully updated degree details."); //"Successfully updated degree details.";
        }
        else
        {
            res = objclsDegreeReference.InsertDegreeReference();
            if (res < 0)
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("اسم درجة موجود بالفعل.") : ("Degree name already exists."); //"Degree name already exists.";
            else
                lblStatusMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("وأضاف بنجاح التفاصيل درجة.") : ("Successfully added degree details."); //"Successfully added degree details.";            
        }

        return res;
    }

    private void ClearControls()
    {
        txtDegreeName.Text = string.Empty;
        txtDegreeNameArb.Text = string.Empty;
        ddlQualificationType.SelectedIndex = 0;

        ViewState["DegreeID"] = null;
    }

    #endregion Methods
}
