﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AgencyReference.ascx.cs"
    Inherits="Controls_AgencyReference" %>
<link href="../css/popup.css" rel="stylesheet" type="text/css" />
<link href="../css/default_common.css" rel="stylesheet" type="text/css" />

<script src="../js/HRPower.js" type="text/javascript"></script>

<script type="text/javascript">

    function closThis(elementID) {
        var e = document.getElementById(elementID);

        if (e) {
            e.style.display = 'none';
            return false;
        }
    }

</script>

<style type="text/css">
    .style15
    {
        width: 291px;
        height: 47px;
    }
    .style16
    {
        height: 47px;
    }
    .style17
    {
        width: 84px;
    }
    </style>
<div id="popupmainwrap">
    <div id="popupmain" style="width: 621px;">
        <div id="header11" style="width: 100%">
            <div id="hbox1">
                <div id="headername">
                    <asp:Label ID="lblHeading" runat="server" meta:resourcekey="HRPower" ></asp:Label><%--Text="HR Power"--%>
                </div>
            </div>
            <div id="hbox2">
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                    </a>
                </div>
            </div>
        </div>
        <div id="toolbar">
            <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div style="float: left; width: 100%; margin-right: 0px;">
                        <div id="imgbuttonss">
                            <asp:ImageButton ID="btnAddNew" CssClass="imgbuttons" runat="server" meta:resourcekey="AddNew" 
                                CausesValidation="False" ImageUrl="~/images/Plus.png" OnClick="btnAddNew_Click" /><%--ToolTip="Add New"--%>
                            <asp:ImageButton ID="btnSaveData" CssClass="imgbuttons" runat="server" meta:resourcekey="SaveData"
                                ValidationGroup="Submit" ImageUrl="~/images/Save%20Blue.png" OnClick="btnSaveData_Click" />  <%--ToolTip="Save Data"--%>
                            <asp:ImageButton ID="btnClear" CssClass="imgbuttons" runat="server" CausesValidation="False"
                               meta:resourcekey="Clear" ImageUrl="~/images/clear.png" OnClick="btnClear_Click" /> <%--ToolTip="Clear"--%>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlReference" runat="server">
            <ContentTemplate>
                <div id="contentwrap">
                    <fieldset style="width: 590px">
                        <table cellpadding="2" cellspacing="0" border="0" class="labeltext">
                            <tr>
                                <td>
                                    <%--Name--%>
                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Name"></asp:Literal>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAgencyName" runat="server" Width="186px" MaxLength="50" onchange="RestrictMulilineLength(this, 50);"
                                        onkeyup="RestrictMulilineLength(this, 50);"></asp:TextBox>
                                </td>
                                <td>
                                    <%--Name--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="NameArb"></asp:Literal>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAgencyNameArb" runat="server" Width="186px" MaxLength="50" onchange="RestrictMulilineLength(this, 50);"
                                        onkeyup="RestrictMulilineLength(this, 50);"></asp:TextBox>
                                </td>
                                </tr>
                                
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvEng" runat="server" ControlToValidate="txtAgencyName"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterAgencyName" ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage=" Enter Agency Name"--%>
                                </td>
                                <td class="style17">
                                </td>
                                <td>
                                <asp:RequiredFieldValidator ID="rfvArb" runat="server" ControlToValidate="txtAgencyNameArb"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterAgencyNameArb" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAddress"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterAddress" ValidationGroup="Submit"></asp:RequiredFieldValidator> <%--ErrorMessage=" Enter Address"--%>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <%--Email--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Email"></asp:Literal>
                                </td>
                                <td class="style16">
                                    <asp:TextBox ID="txtEmail" runat="server" Width="185px" MaxLength="50" onchange="RestrictMulilineLength(this, 50);"
                                        onkeyup="RestrictMulilineLength(this, 50);"></asp:TextBox>
                                    <br />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterEmail" ValidationGroup="Submit"></asp:RequiredFieldValidator> <%--ErrorMessage=" Enter Email"--%>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="InvalidEmail" SetFocusOnError="False"
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Submit"></asp:RegularExpressionValidator><%-- ErrorMessage="Invalid Email"--%>
                                </td>
                                <td valign="top">
                                    <%--Telephone--%>
                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Telephone"></asp:Literal>
                                </td>
                                <td class="style15">
                                    <asp:TextBox ID="txtTelephone" runat="server" MaxLength="20" onchange="RestrictMulilineLength(this, 20);"
                                        onkeyup="RestrictMulilineLength(this, 20);" Width="185px"></asp:TextBox>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvTelephone" runat="server" ControlToValidate="txtTelephone"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterTelephoneNumber" ValidationGroup="Submit"></asp:RequiredFieldValidator> <%--ErrorMessage=" Enter Telephone Number"--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="style17">
                                    <%--Address--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Address"></asp:Literal>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtAddress" runat="server" Rows="5" TextMode="MultiLine" MaxLength="250"
                                        onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLength(this, 250);"
                                        Width="238px" Height="48px"></asp:TextBox><br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtAddress"
                                        CssClass="error" Display="Dynamic" meta:resourcekey="EnterAddress" ValidationGroup="Submit"></asp:RequiredFieldValidator> <%--ErrorMessage=" Enter Address"--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style="margin-top: -10px;">
                                    &nbsp;
                                </td>
                                <td class="style17">
                                </td>
                                <td style="padding-left: 134px;">
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        FilterMode="ValidChars" FilterType="Numbers" InvalidChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ."
                                        TargetControlID="txtTelephone" ValidChars="0-9">
                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btnSave" runat="server" CssClass="popupbutton" OnClick="btnSave_Click"
                                       meta:resourcekey="Save"  ValidationGroup="Submit" /> <%--Text="Save"--%>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
                <div>
                    <fieldset>
                        <div id="content" style="max-height: 226px; overflow: auto; width: 100%;">
                            <asp:GridView ID="dgvReference" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                Width="99%" CssClass="labeltext" Height="135px">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:TemplateField  meta:resourcekey="Agency" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="20%"><%--HeaderText="Name"--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAgencyName" runat="server" Text='<%# Eval("Agency") %>' Style='word-break: break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="AgencyArb"  ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="20%"> <%--HeaderText="Name"--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAgencyNameArb" runat="server" Text='<%# Eval("AgencyArb") %>' Style='word-break: break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="30%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="Address1"  ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="55%" ItemStyle-Wrap="true"> <%--HeaderText="Address"--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>' Style='word-break: break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="50%" Wrap="True" />
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="Email1" HeaderText="Email" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="55%" ItemStyle-Wrap="true"> <%--HeaderText="Email"--%> 
                                        <ItemTemplate>
                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' Style='word-break: break-all'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="50%" Wrap="True" />
                                    </asp:TemplateField>
                                    <asp:TemplateField meta:resourcekey="Telephone1"  ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="55%" ItemStyle-Wrap="true"> <%--HeaderText="Telephone"--%>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTelephone" runat="server" Text='<%# Eval("Telephone") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="datalistTrLeft" VerticalAlign="Top" Width="50%" Wrap="True" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgEdit" runat="server" CommandArgument='<%# Eval("AgencyID") %>'
                                                OnClientClick="return true" OnClick="ImgEdit_Click" meta:resourcekey="edit"  ImageUrl="~/images/edit_popup.png" />
                                        </ItemTemplate><%--ToolTip="edit"--%>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="datalistTrLeft" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton Style="padding-right: 5px;" ID="ImgDelete" runat="server" CommandArgument='<%# Eval("AgencyID") %>'
                                             CausesValidation="false" meta:resourcekey="Delete" ImageUrl="~/images/delete_popup.png" OnClick="ImgDelete_Click" /> <%--ToolTip="Delete" --%>
                                            <AjaxControlToolkit:ConfirmButtonExtender ID="CBE1" runat="server" TargetControlID="ImgDelete"
                                                ConfirmText="Are you sure to delete this Agency?">
                                            </AjaxControlToolkit:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </fieldset>
                </div>
                <div id="footerwrapper">
                    <div id="footer11" style="height: 30px;">
                        <div id="buttons">
                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: right; height: 30px; width: 126px;" align="left">
                                        <asp:Button ID="btnOK" runat="server" CssClass="popupbutton" OnClick="btnOK_Click" ValidationGroup="Submit" 
                                           meta:resourcekey="Ok"  Width="60"  />&nbsp; <%--Text="Ok"--%>
                                        <asp:Button ID="btnCancel" runat="server" CssClass="popupbutton" Width="60" OnClick="btnCancel_Click"
                                          Text='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="False" />  <%--Text="Cancel"--%>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCancel" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnAddNew" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ibtnClose" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="btnClear" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <asp:Label ID="lblStatusMessage" runat="server" CssClass="error" Text="" 
                            Visible="false"></asp:Label>
                    </div>
                </div>
                <div style="display: none">
                    <asp:HiddenField ID="hfModalPopupID" runat="server" Value="" />
                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
