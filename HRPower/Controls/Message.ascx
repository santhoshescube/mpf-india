﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Message.ascx.cs" Inherits="controls_Message" %>
<link href="../css/Controls.css" rel="stylesheet" type="text/css" />
<link href="../css/HRPower.css" rel="stylesheet" type="text/css" />
<link href="../css/popup.css" rel="stylesheet" type="text/css" />

<div  id="popupmainwrap" >
    <div id="popupmain" style="min-height:100px;">
        <div id="header11">
            <div id="hbox1">
                <%--style="width:80%; float:left;padding:0;" --%>
                <div id="headername">
                   <%-- HRPower--%><asp:Literal ID="lit1" runat ="server" Text ='<%$Resources:ControlsCommon,HRPower%>'></asp:Literal>
                </div>
            </div>
            <div id="hbox2">
                <%--style="width:16%; float:right;padding:0;"--%>
                <div id="close1">
                    <a href="">
                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                            OnClick="ibtnClose_Click" CausesValidation="False" /></a>
                </div>
            </div>
        </div>
        <div id="contentwrap">
            <div id="content" >
                <div style="margin: 5px;">
                    <div style ="width:97%; min-height:50px; margin-top:5px;">
                        <asp:Label ID="lblMessage" runat="server" Font-Size="12px"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div id="footerwrapper">
            <div id="footer11" style ="height:35px;" >
                <div id="buttons" style ="padding-top:3px;" >
                    <asp:Button ID="btnOk" runat="server" CssClass="popupbutton" Width="40px" Text='<%$Resources:ControlsCommon,Ok%>'  OnClick="btnOk_Click"
                        CausesValidation="False"  />
                </div>
            </div>
        </div>
    </div>
</div>

