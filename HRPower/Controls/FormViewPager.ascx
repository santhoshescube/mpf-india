﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FormViewPager.ascx.cs" Inherits="FormViewPager" %>

<link id="cssMaroon" runat="server" href="../App_Themes/Maroon/css/Maroon.css" rel="stylesheet" type="text/css" disabled="disabled" />
<%--<link href="../css/Controls.css" rel="stylesheet" type="text/css" disabled="disabled" />--%>

<table cellpadding="0" cellspacing="0" style="font-family: Verdana; font-size: 11px;margin-right:8px" id="FormViewPager">
    <tr>
        <td width="20">
            <asp:ImageButton ID="btnFirst" runat="server" AlternateText="First" 
                SkinID="FirstButton" onclick="btnFirst_Click" 
                CausesValidation="False" ImageUrl="~/images/First.png" />
        </td>
        <td width="20">
            <asp:ImageButton ID="btnPrevious" runat="server" AlternateText="Previous" 
               SkinID="PreviousButton " onclick="btnPrevious_Click" 
                CausesValidation="False" ImageUrl="~/images/Previous.png" />
        </td>
        <td align="center" width="60">
            <asp:TextBox ID="txtPageIndex" runat="server" Style="border: 1px solid #bdc7d8; font-size: 11px;
                padding: 3px;" Width="50px" AutoPostBack="True" 
                OnTextChanged="txtPageIndex_TextChanged" MaxLength="9"></asp:TextBox>
            <div style="display: none;">
                <asp:HiddenField ID="hdPageIndex" runat="server" Value="0" />
                <asp:HiddenField ID="hdPageCount" runat="server" Value="0" />
            </div>
        </td>
        <td>
            of <asp:Label ID="lblPageCount" runat="server"></asp:Label></td>
        <td width="20" align="right">
            <asp:ImageButton ID="btnNext" runat="server" AlternateText="Next" 
               SkinID="NextButton"  onclick="btnNext_Click" 
                CausesValidation="False" ImageUrl="~/images/Next.png" />
        </td>
        <td width="20" align="right">
            <asp:ImageButton ID="btnLast" runat="server" AlternateText="Last" 
                SkinID="LastButton"  onclick="btnLast_Click" 
                CausesValidation="False" ImageUrl="~/images/Last.png" />
        </td>
    </tr>
</table>
