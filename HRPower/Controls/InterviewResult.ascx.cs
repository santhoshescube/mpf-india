﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class Controls_InterviewResult : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    public void SetGrid(Int64 InterviewProcessID,long CandidateID ,int JobID)
    {

        ViewState["CandidateID"] = CandidateID;
        ViewState["InterviewProcessID"] = InterviewProcessID;
        ViewState["JobID"] = JobID ;

        int Status = clsInterviewSchedule.IsCandidateScheduled(CandidateID,JobID);
        if (Status == (int)eCandidateResult.recommende_To_OtherJobs)  // Recommend to Other and Scheduled for other job
        {
            lblMessage.Text = clsGlobalization.IsArabicCulture() ? "لا يمكن تحديث نتائج ؛ المجدولة عن وظيفة أخرى ." : "Cannot update candidate;Scheduled for another job.";
            rdbAccept.Enabled =rdbRecommended.Enabled=rdbRejected.Enabled =rdbWaitingList.Enabled  = btnSave.Enabled = false;
        }
        else if (Status == (int)eCandidateResult.OfferSent) 
        {
            lblMessage.Text = clsGlobalization.IsArabicCulture() ? "لا يمكن تحديث نتائج ؛ العرض المرسلة" : "Cannot update candidate;Offer Sent";
            rdbAccept.Enabled = rdbRecommended.Enabled = rdbRejected.Enabled = rdbWaitingList.Enabled = btnSave.Enabled = false;
        }
        else        
        {
            lblMessage.Text ="";
            rdbAccept.Enabled = rdbRecommended.Enabled = rdbRejected.Enabled = rdbWaitingList.Enabled = btnSave.Enabled = true;
        }

        rdbAccept.Checked = rdbRecommended.Checked = rdbRejected.Checked = rdbWaitingList.Checked = false;

        rpLevels.DataSource = clsInterviewProcess.GetAllInterviewersEvaluationResultLevels(InterviewProcessID,CandidateID,JobID);
        rpLevels.DataBind();

        upd.Update();
    }

    protected void rpLevels_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        int PendingEvaluatorsCount = 0;
        string Name = "";

        LinkButton lnkLevels = (LinkButton)e.Item.FindControl("lnkLevels");
        HiddenField hfdLevelID = (HiddenField)e.Item.FindControl("hfdLevelID");
            
        Label lblTotalMarks = (Label)e.Item.FindControl("lblTotalMarks");
        Label lblPassPercentage = (Label)e.Item.FindControl("lblPassPercentage");
        Label lblNoOfInterviewers = (Label)e.Item.FindControl("lblNoOfInterviewers");
        Label lblTotalWeightedMarks = (Label)e.Item.FindControl("lblTotalWeightedMarks");
        Label lblTotalPercentage = (Label)e.Item.FindControl("lblTotalPercentage");
        Label lblResult = (Label)e.Item.FindControl("lblResult");


        DataGrid dgv = (DataGrid)e.Item.FindControl("dgv");

        DataSet ds = clsInterviewProcess.GetAllInterviewersEvaluationResult(ViewState["InterviewProcessID"].ToInt64(), ViewState["CandidateID"].ToInt64(), ViewState["JobID"].ToInt32(), hfdLevelID.Value.ToInt32());

        dgv.DataSource = ds.Tables[0];
        dgv.DataBind();


      
        if (ds.Tables[1].Rows.Count > 0)
        {
            lblTotalMarks.Text = ds.Tables[1].Rows[0]["TotalMarks"].ToString();
            lblPassPercentage.Text = ds.Tables[1].Rows[0]["PassPercentage"].ToString();
            lblNoOfInterviewers.Text = ds.Tables[1].Rows[0]["NoOfInterviewers"].ToString();


            lblTotalWeightedMarks.Text = ds.Tables[1].Rows[0]["TotalWeightedMark"].ToString();
            lblTotalPercentage.Text = ds.Tables[1].Rows[0]["MarkPercentage"].ToString();
            lblResult.Text = ds.Tables[1].Rows[0]["Result"].ToString();
            Name = ds.Tables[1].Rows[0]["Name"].ToString();

            lblName.Text = "     (Candidate :" + (Name.Length > 50 ? Name.Substring(0, 60) + "..." : Name) + ")";

            //ibtnSendOffer.Enabled = false ;
            //ibtnSendOffer.ToolTip = "";

            ViewState["CandStatusID"] = ds.Tables[1].Rows[0]["CandidateStatusID"];

            if (ds.Tables[1].Rows[0]["ResultTypeID"] != DBNull.Value)
            {
                rdbAccept.Checked = rdbRecommended.Checked = rdbRejected.Checked = rdbWaitingList.Checked = false;

                eResult Result = ((eResult)ds.Tables[1].Rows[0]["ResultTypeID"].ToInt32());

                if (Result == eResult.acceptable)
                {
                    //ibtnSendOffer.Enabled = true;
                    rdbAccept.Checked = true;
                    //ibtnSendOffer.ToolTip = "Send Offer";
                }
                else if (Result == eResult.recommende_To_OtherJobs)
                    rdbRecommended.Checked = true;
                else if (Result == eResult.rejected)
                    rdbRejected.Checked = true;
                else
                    rdbWaitingList.Checked = true;
            }



            PendingEvaluatorsCount = ds.Tables[1].Rows[0]["PendingEvaluators"].ToInt32();
            if (PendingEvaluatorsCount > 0)
            {
                hfPendingCount.Value = PendingEvaluatorsCount.ToString();
                lblPendingEvaluatorCount.Text = "Evaluation not completed interviewers count :" + PendingEvaluatorsCount;
                // lblPendingEvaluatorCount.Visible = true;
            }
            else
                lblPendingEvaluatorCount.Visible = false;

        }

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lnkJob = e.Item.FindControl("lnkJob") as LinkButton;
            HtmlGenericControl divJobDetails = e.Item.FindControl("divJobDetails") as HtmlGenericControl;

            if (lnkJob != null)
            {
                lnkJob.OnClientClick = "ToggleDiv(" + divJobDetails.ClientID + "); return false;";
            }
        }
    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string Message = string.Empty;
        try
        {
            //if (rdbAccept.Checked)
            //{
            //    if (ViewState["CandStatusID"].ToInt32() >= 5)
            //    {
            //        ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('This candidate is already selected for another job.');", true);

            //    }
            //}


            DataTable dt = clsInterviewProcess.CheckInterviewStatus(ViewState["InterviewProcessID"].ToInt32());
            int ResultID = 0;
            int CandidateStatusID = 0;

            if ((rdbAccept.Checked || rdbRecommended.Checked || rdbRejected.Checked || rdbWaitingList.Checked))
            {
                if (dt.Rows.Count > 0)
                {



                    if (dt.Rows[0]["IsBoard"].ToInt32() > 0 && dt.Rows[0]["PendingEvaluators"].ToInt32() > 0)
                    {
                        ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('Please wait till other evaluators finish their evaluation');", true);
                        return;
                    }
                    //else if (dt.Rows[0]["ResultTypeID"] != DBNull.Value)
                    //{

                    //    if (dt.Rows[0]["ResultTypeID"].ToInt32() == (int)eResult.acceptable)
                    //        ResultID = 1;
                    //    else if (dt.Rows[0]["ResultTypeID"].ToInt32() == (int)eResult.waitingList)
                    //        ResultID = 2;
                    //    else if (dt.Rows[0]["ResultTypeID"].ToInt32() == (int)eResult.rejected)
                    //        ResultID = 3;
                    //    else if (dt.Rows[0]["ResultTypeID"].ToInt32() == (int)eResult.recommende_To_OtherJobs)
                    //        ResultID = 4;


                    //    string Message = "Candidate status has been to ";


                    //    Message = Message + ((eResult)dt.Rows[0]["ResultTypeID"].ToInt32()).ToString().Replace("_", " ") + " by other interviewer  Are you sure you want to change the status ?";

                    //    ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + Message + " ');", true);

                    //}
                    // ScriptManager.RegisterClientScriptBlock(upd, upd.GetType(), new Guid().ToString(), "alert('This interview ca wait till other evaluators finish their evaluation');", true);
                }


                if (rdbAccept.Checked)
                {
                    ResultID = 1;
                    CandidateStatusID = (int)CandidateStatus.Selected;
                }
                else if (rdbRejected.Checked)
                {
                    ResultID = 3;
                    CandidateStatusID = (int)CandidateStatus.Rejected;
                }
                else if (rdbWaitingList.Checked)
                {
                    ResultID = 2;
                    CandidateStatusID = (int)CandidateStatus.WaitingList;
                }
                else if (rdbRecommended.Checked)
                {
                    ResultID = 4;
                    CandidateStatusID = (int)CandidateStatus.RecommendToOther;

                }

                if (new clsInterviewProcess().UpdateInterviewStatus(ViewState["InterviewProcessID"].ToInt32(), ResultID, false, CandidateStatusID))
                {
                    Message = clsGlobalization.IsArabicCulture() ? "نتيجة حفظها بنجاح" : "Updated successfully";
                    ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + Message + " ');", true);
                    //if (rdbAccept.Checked)
                    //{
                    //    ibtnSendOffer.Enabled = true;
                    //    ibtnSendOffer.ToolTip = "Send Offerletter";
                    //    upd.Update();
                    //}
                    //else
                    //{
                    //    ibtnSendOffer.Enabled = false ;
                    //    ibtnSendOffer.ToolTip = "Selected Candidates can send offer letter";
                    //    upd.Update();
                    //}

                    //send offer letter
                    //if (chk.Checked && ResultID == 1)
                    //{
                    //    SendOfferLetter();
                    //}
                    //else if (chk.Checked)
                    //{
                    //    ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('Offer letter will be send for acceptable status only');", true);

                    //}



                }
                else
                {
                    Message = "Updation failed please try again!!!";

                    ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
                }
            }
            else
            {
                Message = "Please select any status";
                ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
            }

        }
        catch (Exception ex)
        {

        }
    }



    private void SendOfferLetter()
    {

        string OfferLetter = GetOfferLetter();
        if (OfferLetter != "")
        {
            string FromMailID = "";
            string ToMailID = "";

            if (ViewState["CandidateID"] == null || ViewState["CandidateID"].ToInt32() == 0)
            {
                ViewState["CandidateID"] = clsInterviewProcess.GetCurrentInterviewCount(new clsUserMaster().GetUserId());  
            }

            clsInterviewProcess.UpdateOfferLetterStatus(ViewState["CandidateID"].ToInt64());

            DataTable dt = clsInterviewProcess.GetFromToMailIDs(ViewState["CandidateID"].ToInt64());
            if (dt.Rows.Count > 0)
            {
                FromMailID = dt.Rows[0]["FromMailID"].ToString();
                ToMailID = dt.Rows[0]["ToMailID"].ToString();


                if (FromMailID != "" && ToMailID != "")
                {
                    System.Threading.Thread t = new System.Threading.Thread(() => new clsMailSettings().SendMail(dt.Rows[0]["FromMailID"].ToString(), dt.Rows[0]["ToMailID"].ToString(), "Offer Letter", OfferLetter, clsMailSettings.AccountType.Email, false));
                    t.Start();
                }
                else
                    if (FromMailID == "")
                        ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('Mail cannot be send since there is no user name set in the mail settings');", true);
                    else if (ToMailID == "")
                        ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('Mail cannot be send since there is no email id set for the candidate ');", true);
            }
        }
    }






    private string GetOfferLetter()
    {

        if (clsInterviewProcess.GetCurrentInterviewCount(ViewState["InterviewerID"].ToInt32()) == 0)
        {
            //ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Wait for new candidate to come');", true);

        }
        else if (ViewState["InterviewProcessID"] == null || ViewState["InterviewProcessID"].ToInt64() == 0)
        {
            ViewState["InterviewProcessID"] = clsInterviewProcess.GetInterviewProcessID(ViewState["InterviewerID"].ToInt32(), ViewState["CandidateID"].ToInt32(),ViewState["JobID"].ToInt32());
        }

        if (ViewState["InterviewProcessID"] != null && ViewState["InterviewProcessID"].ToInt64() > 0)
        {
            clsCandidateHome objHome = new clsCandidateHome();
            DataTable dtOfferLetter = clsInterviewProcess.GetCandidateOfferLetter(ViewState["InterviewProcessID"].ToInt64());
            if (dtOfferLetter != null && dtOfferLetter.Rows.Count > 0)
            {
                StreamReader reader = new StreamReader(Server.MapPath("~/ETHome/OfferLetterMail.htm"));
                string readFile = reader.ReadToEnd();
                string offerLetter = "";
                offerLetter = readFile;

                offerLetter = offerLetter.Replace("$$OfferLetterNumber$$", Convert.ToString(dtOfferLetter.Rows[0]["OfferLetterNumber"]));//
                offerLetter = offerLetter.Replace("$$Date$$", dtOfferLetter.Rows[0]["SentDate"].ToDateTime().ToString("dd MMM yyyy"));
                offerLetter = offerLetter.Replace("$$ApplicationReference$$", Convert.ToString(dtOfferLetter.Rows[0]["ApplicationReference"]));//or+canno
                offerLetter = offerLetter.Replace("$$ApplicantName$$", Convert.ToString(dtOfferLetter.Rows[0]["ApplicantName"]));

               // divPrint.InnerHtml = offerLetter;


                return offerLetter;
            }
        }
        return "";
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
    }
    //protected void ibtnSendOffer_Click(object sender, ImageClickEventArgs e)
    //{
    //    if (rdbAccept.Checked)
    //        Response.Redirect(string.Format("~/public/OfferLetter.aspx?CandidateID={0}&JobID={1}", ViewState["CandidateID"].ToInt64(), ViewState["JobID"].ToInt32()));
    //    else
    //    {
    //        string script = string.Format("setTimeout(\"showAlert('Please select the candidate.')\", 100);");
    //        ScriptManager.RegisterClientScriptBlock(ibtnSendOffer, ibtnSendOffer.GetType(), Guid.NewGuid().ToString(), script, true);
    //    }

    //}
}


public enum eResult
{
    acceptable = 1,
    waitingList = 2,
    rejected = 3,
    recommende_To_OtherJobs = 4
}

public enum eCandidateResult
{
    recommende_To_OtherJobs = 1,
    OfferSent = 2
 
}