﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AssignEvaluators.ascx.cs"
    Inherits="Controls_AssignEvaluators" %>
<div id="popupmain1" style="width: 100%; height: auto; overflow: auto; background-color: White">
    <div id="header11" style="width: 100%">
        <div id="hbox1">
            <div id="headername">
                <asp:Label ID="lblHeading" runat="server" meta:resourcekey="AssignEmployees"></asp:Label>
            </div>
        </div>
        <div id="hbox2">
            <div id="close1">
                <a href="">
                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                        OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                </a>
            </div>
        </div>
    </div>
    <div style="width: 100%; overflow: hidden;">
        <div style="display: block; border: solid 1px black">
            <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                   
                    <div class="firstTrLeft" style="float: left; width: 7%; height: 29px">
                                                  <asp:Label ID="Label2" runat="server" meta:resourcekey="Template" CssClass="labeltext"></asp:Label>

                    </div>
                    <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                        <asp:DropDownList ID="ddlTemplate" Width="200px" runat="server" CssClass="textbox_mandatory"
                            MaxLength="25">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvTemplate" runat="server" ControlToValidate="ddlTemplate"
                            CssClass="error" Display="Dynamic" InitialValue="-1" meta:resourcekey="PleaseSelectATemplate"
                            SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="min-width: 100%; height: 50%; overflow: auto">
                        <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dl" Width="100%"
                            runat="server" RepeatColumns="4" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                            BorderWidth="1px" CellPadding="3" GridLines="Both">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <ItemStyle ForeColor="#000066" />
                            <SeparatorStyle BackColor="AliceBlue" />
                            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <HeaderTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%; overflow: hidden; vertical-align: top">
                                            <asp:CheckBox CssClass="labeltext" ForeColor="White" AutoPostBack="true" ID="chkSelectAll"
                                              Text ='<%$Resources:ControlsCommon,SelectAll%>'  Height="15px" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                        </td>
                                        <td style="width: 90%; overflow: hidden; vertical-align: top; padding-left: 320px">
                                            <asp:Label ID="lblHeader" meta:resourcekey="EmployeeList" ForeColor="White" CssClass="labeltext"
                                                runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 20px; height: 15px; overflow: hidden; vertical-align: bottom">
                                            <asp:CheckBox AutoPostBack="true" ID="chkSelect" Height="15px" runat="server" OnCheckedChanged="chkSelect_CheckedChanged" />
                                        </td>
                                        <td style="width: 200px; height: 15px; overflow: hidden; vertical-align: top">
                                            <asp:Label ID="lblEmployee" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EmployeeFullName") %>'
                                                Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                            <asp:HiddenField ID="hfEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                            </asp:HiddenField>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <div id="dvNoEmployee" runat="server" visible="false">
                            <asp:Label ID="lblNoEmployee" runat="server" Text="" CssClass="error"></asp:Label>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="updEvaluator" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="margin-top: 15px; background-color: White; width: 100%; height: 46px">
                        <div style="float: left; width: 80px">
                              <asp:Label ID="Label1" runat="server" meta:resourcekey="Evaluators" CssClass="labeltext"></asp:Label>

                        </div>
                        <div style="float: left; width: 200px">
                            <asp:DropDownList ID="ddlEvaluator" runat="server" Width="200px">
                            </asp:DropDownList>
                        </div>
                        <div style="margin-left: 10px; float: left; height: 24px;">
                            <asp:Button ID="btnAddEvaluator" Height="24px"  CssClass="btnsubmit" runat="server"
                                meta:resourcekey="AddEvaluator"  Width="100px" OnClick="btnAddEvaluator_Click"></asp:Button>
                        </div>
                         <div style="clear:both">
                        <asp:CheckBox ID="chkIncludeAllCompany" CssClass="labeltext" AutoPostBack ="true"
                                Text ='<%$Resources:ControlsCommon,SelectEvaluatorfromothercompanies%>'  runat ="server" 
                                oncheckedchanged="chkIncludeAllCompany_CheckedChanged" />
                        </div>
                    </div>
                    <div style="min-width: 100%; height: 50%; overflow: auto">
                        <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlEvaluator"
                            Width="100%" runat="server" RepeatColumns="3" BackColor="White" BorderColor="#CCCCCC"
                            BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <ItemStyle ForeColor="#000066" />
                            <SeparatorStyle BackColor="AliceBlue" />
                            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <HeaderTemplate>
                                                              
                                
                                <asp:Literal id="Literal1" runat="server"   meta:resourcekey="EvaluatorsList"></asp:Literal>
                            </HeaderTemplate>
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 250px; height: 15px; overflow: hidden">
                                            <asp:Label CssClass="labeltext" ID="lblEmployee" Height="15px" Width="250px" ToolTip='<%# Eval("EvaluatorName") %>'
                                                runat="server" Text='<%# Eval("EvaluatorName") %>'></asp:Label>
                                        </td>
                                        <td style="width: 20px">
                                            <asp:ImageButton  meta:resourcekey="RemoveEvaluator" ID="imgRemoveEvaluator" runat="server"
                                                ImageUrl="~/images/delete_popup.png" 
                                                CommandArgument='<%# Eval("EvaluatorID") %>' 
                                                onclick="imgRemoveEvaluator_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            
        </div>
    </div>
    <div style="clear: both;" id="footerwrapper">
        <div id="footer11" style="width: 100%">
        <asp:UpdatePanel ID="updEvaluatorSubmit" runat ="server" UpdateMode ="Conditional" >
        <ContentTemplate>
            <div style="float: right">
                <asp:Button ID="btnsave" 
                    CssClass="btnsubmit" runat="server"  meta:resourcekey="Add" OnClick="save_Click" />
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        </div>
    </div>
</div>
