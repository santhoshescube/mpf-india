﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true"
    CodeFile="RptInterviewProcess.aspx.cs" Inherits="reporting_RptInterviewProcess"
    Title="Interview Process Report" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    
        $(document).ready(function() {
             var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {
                
                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium'); 
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                 this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

                
            });
        });
        
       
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,InterviewProcess%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="filters" width="100%">
                    <tr>
                        <td valign="bottom">
                            <table id="tblEmployee" runat="server" class="labeltext" width="100%">
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" Width="200px" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                       <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,JobTitle%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlJobTitle" runat="server" Width="200px" OnSelectedIndexChanged="ddlJobTitle_SelectedIndexChanged"
                                            AutoPostBack="True" />
                                    </td>
                                    <td>
                                       <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Period%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtFromDate" runat="server" Width="80px" Style="padding-left: 3px;
                                                        padding-right: 3px;"></asp:TextBox>
                                                    <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" DefaultView="Days"
                                                        Format="dd MMM yyyy" PopupPosition="BottomLeft" PopupButtonID="txtFromDate" TargetControlID="txtFromDate" />
                                                </td>
                                                <td>
                                                    &nbsp;-&nbsp;
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtToDate" runat="server" Width="80px" Style="padding-left: 3px;
                                                        padding-right: 3px;"></asp:TextBox>
                                                    <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" DefaultView="Days"
                                                        Format="dd MMM yyyy" PopupPosition="BottomLeft" PopupButtonID="txtToDate" TargetControlID="txtToDate" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                       <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" Width="200px" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                       <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,Candidate%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCandidate" runat="server" Width="200px" />
                                    </td>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>' AutoPostBack="True"
                                            OnCheckedChanged="chkIncludeCompany_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                             Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%;overflow:auto;">
        <rsweb:ReportViewer ID="rvInterviewProcess" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true"  
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="rvInterviewProcess_PageNavigation">
         
        </rsweb:ReportViewer>
    </div>
</asp:Content>
