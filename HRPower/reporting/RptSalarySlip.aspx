﻿<%@ Page Title="Salary Slip Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptSalarySlip.aspx.cs" Inherits="reporting_RptSalarySlip" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,SLIP%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                 <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="filters" width="100%">
                    <tr>
                        <td valign="bottom">
                            <%-- <asp:UpdatePanel ID="upd1" runat="server">
                                <ContentTemplate>--%>
                            <table id="tblEmployee" runat="server" class="labeltext" width="100%">
                                <tr>
                                    <td>
                                        <%--Company--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="160px"
                                            AutoPostBack="True" 
                                            onselectedindexchanged="ddlCompany_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%--Department--%><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Designation%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="dropdownlist" 
                                            Width="160px" onselectedindexchanged="ddlDesignation_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%--Employee--%><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist" Width="180px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="dropdownlist" 
                                            Width="160px" onselectedindexchanged="ddlDepartment_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%--WorkStatus--%>Work Status</td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" CssClass="dropdownlist" AutoPostBack="true"
                                            Width="160px" 
                                            onselectedindexchanged="ddlDesignation_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                       <%-- Month-Year--%><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,MonthYear%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrentMonth" runat="server" CssClass="textbox" Width="85px"
                                            MaxLength="11" OnTextChanged="txtToDate_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CausesValidation="False" />
                                        <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                            Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtCurrentMonth" />
                                    </td>
                                </tr>
                            </table>
                            <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                            Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" Enabled="False" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <%-- <div style="clear: both; height: auto; width: 100%">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server"  Width="100%" ProcessingMode="Local" 
            SizeToReportContent="True" ShowPrintButton="False" ShowRefreshButton="False"
            Font-Names="Verdana" Font-Size="8pt" Height="400px">
            <LocalReport ReportPath="App_Code\rptEmployeeProfile.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsEmployee_dtEmployee" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>--%>
    <div style="clear: both; height: auto; width: 100%">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px">
            <%--  <LocalReport ReportPath="App_Code\RptVacationSummary.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsVacationSettlement_dtVacationSummary" />
                </DataSources>
            </LocalReport>--%>
        </rsweb:ReportViewer>
    </div>
</asp:Content>
