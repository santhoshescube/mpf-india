﻿<%@ Page Title="Attendance Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptMISAttendance.aspx.cs" Inherits="reporting_RptMISAttendance" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
  <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,AttendanceDetail%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                           <%-- <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                              <%--  Designation--%>
                                              
                                              <asp:Literal ID="Literal" runat ="server" Text ='<%$Resources:ReportsCommon,Designation %>'>
                                              </asp:Literal>
                                                
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" 
                                                    OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                           
                                             <td >
                                             <%--   Type--%>
                                             <asp:Literal ID="Literal3" runat ="server" Text ='<%$Resources:ReportsCommon,Type %>'>
                                              </asp:Literal>
                                              
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlType" runat="server"  
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                    <asp:ListItem Value="-1" Text='<%$Resources:ReportsCommon,Any %>'></asp:ListItem>
                                                    <asp:ListItem Value="1" Text='<%$Resources:ReportsCommon,EarlyOut %>'></asp:ListItem>
                                                    <asp:ListItem Value="2" Text='<%$Resources:ReportsCommon,LateIn %>'></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                           
                                             
                                            
                                           
                                        </tr>
                                        <tr>
                                            <td>
                                               <%-- <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>--%>
                                               
                                                <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                               
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                               <%-- Work status--%>
                                                
                                                <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ReportsCommon,WorkStatus %>'>
                                              </asp:Literal>
                                              
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlworkStatus" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" 
                                                    OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                             <td>
                                                <%--<asp:Label ID="lblMonth" runat="server" Text="Month"></asp:Label>--%>
                                                
                                                <asp:Label ID="lblMonth" runat="server" Text='<%$Resources:ReportsCommon,Month %>'></asp:Label>
                                                <asp:CheckBox ID="ChkMonth" 
                                                     Style="padding: 0px; margin: 4px -2px 0px 0px; height: 20px" runat="server"
                                                             AutoPostBack="True" OnCheckedChanged="ChkMonth_CheckedChanged" 
                                                     Text="" Width="10px" />
                                                
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlMonth" runat="server"  AutoPostBack ="true" 
                                                    CssClass="dropdownlist_mandatory" Width="180px" 
                                                    onselectedindexchanged="ddlMonth_SelectedIndexChanged" Enabled="False">
                                                </asp:DropDownList>
                                            </td>
                                       
                                        </tr>
                                        <tr>
                                            <td>
                                              <%--  Department--%>
                                                
                                             <asp:Literal ID="Literal2" runat ="server" Text ='<%$Resources:ReportsCommon,Department %>'>
                                              </asp:Literal>
                                                 
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" 
                                                    OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                               <%-- <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>--%>
                                               
                                               <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'></asp:Label>
                                               
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                          
                                            
                                            
                                             <td>
                                               <%-- <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>--%>
                                                 <div>
                                                     <div style="float: left; width:58px">
                                                         <asp:CheckBox ID="ChkDate" 
                                                             Style="padding: 0px; float:right; margin: 4px 0px 0px 0px; height: 20px" runat="server"
                                                             AutoPostBack="True" OnCheckedChanged="ChkDate_CheckedChanged" Text="" 
                                                             Width="16px" />
                                                             
                                                             <div style ="padding-left:5px;float:left">
                                                         <asp:Label ID="lblDate" runat="server" Text='<%$Resources:ReportsCommon,Date %>'></asp:Label>
                                                         </div>
                                                     </div>
                                                    <%-- <div style="float: left; width: 100px; padding-left: 7px">
                                                     </div>--%>
                                                 </div>
                                             </td>
                                            <td style ="width:170px" colspan ="2">
                                            
                                          <%--  <div style ="float:left;width:20px;height: 20px; overflow:hidden;vertical-align: bottom">
                                            
                                                 </div>
                                          --%>
                                                                                        
                                            <div style ="padding :0px; float:left;width:85px;height: 23px; overflow:hidden;vertical-align: bottom">
                                            
                                                <asp:TextBox style="padding :0px;margin:0px;height:20px" ID="txtDate" runat="server"  MaxLength="11" 
                                                    Width="80px"  ontextchanged="txtDate_TextChanged" AutoPostBack="true" onkeypress="return false;"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="txtDate_CalendarExtender" 
                                                    runat="server" Animated="true" Format="dd-MMM-yyyy" PopupButtonID="btnDate" 
                                                    TargetControlID="txtDate" />
                                                    
                                                    
                                                   </div> 
                                                    
                                                                                                
                                            <div style ="float:left;width:20px;height: 20px">
                                            
                                                <asp:ImageButton ID="btnDate" runat="server" CausesValidation="False" 
                                                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                    
                                                    </div> 
                                            </td>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                          
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td>
                                                           <%-- <asp:CheckBox ID="chkIncludeCompany" runat="server" Text="Include Company" OnCheckedChanged="chkIncludeCompany_CheckedChanged"
                                                                AutoPostBack="true" />--%>
                                                                 <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>' OnCheckedChanged="chkIncludeCompany_CheckedChanged"
                                                                AutoPostBack="true" />   
                                                                
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="ChkSummary" runat="server" Text='<%$Resources:ReportsCommon,Summary %>' 
                                                    Width="120px" AutoPostBack="True" oncheckedchanged="ChkSummary_CheckedChanged" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                           
                                              <asp:Literal ID="Literal4" Visible ="true" runat ="server" Text ='<%$Resources:ReportsCommon,DisplayFormat %>' >
                                              </asp:Literal>
                                            </td>
                                          
                                            <td>
                                            <%-- <asp:DropDownList Visible ="false"  ID="ddlProject" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                </asp:DropDownList>--%>
                                                  <asp:DropDownList ID="ddlFillFormat" runat="server" CssClass="dropdownlist_mandatory"
                                            AutoPostBack="true" EnableViewState="true" Width="180px">
                                            <asp:ListItem Selected="True" Value="0" Text='<%$Resources:ReportsCommon,GraphicFormat %>'></asp:ListItem>
                                            <asp:ListItem Value="1" Text='<%$Resources:ReportsCommon,GridFormat %>'></asp:ListItem>
                                        </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                                
                                             <td>
                                            
                                           
                                          <div style ="padding-left:5px;float:left">
                                                         <asp:Label ID="Label2" runat="server" Text='<%$Resources:ReportsCommon,ToDate %>'></asp:Label>
                                                         </div>       
                                                
                                            </td>
                                            <td style ="width:170px" colspan ="2">
                                            
                                            
                                                                                        
                                            <div style ="padding :0px; float:left;width:85px;height: 23px; overflow:hidden;vertical-align: bottom">
                                            
                                                <asp:TextBox style="padding :0px;margin:0px;height:20px" Visible="true" ID="txtToDate" runat="server"  MaxLength="11" 
                                                    Width="80px"  ontextchanged="txtDate_TextChanged" AutoPostBack="true" onkeypress="return false;"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" 
                                                    runat="server" Animated="true" Format="dd-MMM-yyyy" PopupButtonID="imgToDate" 
                                                    TargetControlID="txtToDate" />
                                                    </div> 
                                                    
                                                                                                
                                            <div style ="float:left;width:20px;height: 20px">
                                            
                                                <asp:ImageButton ID="imgToDate" runat="server" CausesValidation="False"  Visible="true"
                         
                                                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                    
                                                    </div> 
                                            </td>
                                            
                                        </tr>
                                    </table>
                                <%--</ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <%--<td style="width: 38px; margin: 0px; vertical-align: middle">
                         
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text="Go" OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip="Print" Text="Print" OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>--%>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" class="btnsubmit" CausesValidation="false"
                                            Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnSearch_Click1" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip="Print" Text="Print" OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
   
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" SizeToReportContent="True"
            ShowPrintButton="False" ShowRefreshButton="False" Font-Names="Verdana" Font-Size="8pt"
            AsyncRendering="false" Height="700px" 
            onpagenavigation="ReportViewer1_PageNavigation">
            
        </rsweb:ReportViewer>
     
    </div>
      <div class="report-filter">
    <div style="margin-left:858px;"><asp:ImageButton ID="ibtnExcel" runat="server" 
            ImageUrl="~/images/excel_icon.png" onclick="ibtnExcel_Click" CausesValidation="false" /></div>
        <div id="divData" style="display: block; max-height: 680px; overflow:auto; " runat="server">       
        
            <asp:GridView ID="gvPunchings" runat="server" AllowPaging="True" CellPadding="4"
                AutoGenerateColumns="true" ForeColor="#333333" GridLines="Both"
                OnRowCreated="gvPunchings_RowCreated" PageSize="10" OnPageIndexChanging="gvPunchings_PageIndexChanging">
                <RowStyle HorizontalAlign="Left" Font-Size="9px" Font-Names="Verdana"  />
                <PagerStyle BackColor="#59b6fe" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Font-Size="12px"
                    Font-Names="Verdana" />
                <HeaderStyle BackColor="#59b6fe" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" Height="60px" />
                <AlternatingRowStyle BackColor="White" Font-Size="9px" Font-Names="Verdana" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
