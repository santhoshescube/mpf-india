﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using Microsoft.Reporting.WebForms;



public partial class reporting_RptGeneralExpense : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "GeneralExpenseReport").ToString();
        ReportViewer1.Visible = false;
      
        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;

            //Getting the month start date
            txtExpenseFromDate.Text = clsReportCommon.GetMonthStartDate();
            //Getting the month end date
            txtExpenseToDate.Text = Convert.ToDateTime(txtExpenseFromDate.Text.Trim()).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
         
            FillCombos();
            ddlBranch.SelectedValue = "0";
            EnableDisableIncludeCompany();
        }
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

    }

    private void FillCombos()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataSource = clsRptEmployees.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();

        if (ddlCompany.Items.Count > 1)
            ddlCompany.Items.RemoveAt(0);

        FillAllBranches();
        FillAllExpenseCategories();
        ddlExpenseCategory_SelectedIndexChanged(new object(), new EventArgs()); 
    }

    private void FillAllBranches()
    {

        int CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();


        ddlBranch_SelectedIndexChanged(new object(), new EventArgs()); 
    }


    private void FillAllExpenseCategories()
    {
        ddlExpenseCategory.DataSource = clsReportCommon.GetAllExpenseCategories();
        ddlExpenseCategory.DataTextField = "ExpenseCategory";
        ddlExpenseCategory.DataValueField = "ExpenseCategoryID";
        ddlExpenseCategory.DataBind();
    }

    private void FillAllExpenseType()
    {
        ddlExpenseType.DataSource = clsReportCommon.GetAllExpenseTypeByExpenseCategory(ddlExpenseCategory.SelectedValue.ToInt32());
        ddlExpenseType.DataTextField = "ExpenseType";
        ddlExpenseType.DataValueField = "ExpenseTypeID";
        ddlExpenseType.DataBind();
    }
    
    private void FillAllExpenseNumber()
    {
        ddlExpenseNumber.DataSource = clsReportCommon.GetAllExpenseNumbers(ddlExpenseCategory.SelectedValue.ToInt32(),ddlExpenseType.SelectedValue.ToInt32());
        ddlExpenseNumber.DataTextField = "ExpenseNo";
        ddlExpenseNumber.DataValueField = "ExpenseID";
        ddlExpenseNumber.DataBind();
    }

    
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
    }

    //private void EnableDisableIncludeCompany()
    //{
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Enabled = false;
    //        chkIncludeCompany.Checked = true;
    //    }
    //    else
    //        chkIncludeCompany.Enabled = true;
    //}

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    protected void ddlEssType_SelectedIndexChanged(object sender, EventArgs e)
    {
      
    }

   

    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }


    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtExpenseFromDate.Text == string.Empty)
        {
            strMessage = Convert.ToString( GetGlobalResourceObject("ReportsCommon","PleaseSelectFromDate"));
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtExpenseFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtExpenseToDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtExpenseToDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }



        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            DataTable dtCompany = null;
            DataTable dt = null;
            string path = "";


            if (txtExpenseFromDate.Text != "")
                FromDate = Convert.ToDateTime(txtExpenseFromDate.Text, System.Globalization.CultureInfo.InvariantCulture);

            if (txtExpenseToDate.Text != "")
                ToDate = Convert.ToDateTime(txtExpenseToDate.Text, System.Globalization.CultureInfo.InvariantCulture);


            if (ToDate < FromDate)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" +Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate"))  + "');", true);
                return;
            }

            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));


            path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptGeneralExpenseArb.rdlc" : "reports/RptGeneralExpense.rdlc");
            dt = clsRptCandidates.GetGeneralExpenseReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlExpenseCategory.SelectedValue.ToInt32(), ddlExpenseType.SelectedValue.ToInt32(), ddlExpenseNumber.SelectedValue.ToInt32(), Convert.ToDateTime(txtExpenseFromDate.Text.Trim()), Convert.ToDateTime(txtExpenseToDate.Text.Trim()));   
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dtSetGeneralExpense_GeneralExpense", dt));
            
            ViewState["RecordCount"] = dt.Rows.Count;
            if (dt.Rows.Count > 0)
            {
                ReportViewer1.LocalReport.ReportPath = path;

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
				{
					new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter),false),
                    new ReportParameter("Company",ddlCompany.SelectedItem.Text),
                    new ReportParameter("Branch", ddlBranch.SelectedItem.Text),
                    new ReportParameter("ExpenseCategory",ddlExpenseCategory.SelectedItem.Text),
                    new ReportParameter("ExpenseType", ddlExpenseType.SelectedItem.Text),
                    new ReportParameter("ExpenseNumber",ddlExpenseNumber.SelectedItem.Text  ),
                    new ReportParameter("Period", txtExpenseFromDate.Text +"-"+txtExpenseToDate.Text) 
				});
                btnPrint.Enabled = true;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            else
            {
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);

            }

        }
        catch
        {
        }

    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true; 
    }
    protected void ddlExpenseCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllExpenseType();
        ddlExpenseType_SelectedIndexChanged(new object(), new EventArgs());
    }
    protected void ddlExpenseType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllExpenseNumber(); 
    }
}