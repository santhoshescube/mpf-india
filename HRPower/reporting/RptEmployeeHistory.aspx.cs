﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;

public partial class reporting_RptEmployeeHistory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "EmployeeProfileReport").ToString();

        rvEmployeeProfile.Visible = false;
        if (!IsPostBack)
        {
            chk.Checked = true;
            LoadCombos();
        }

        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }

    private void FillAllBranches()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        ddlBranch.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllDeparments()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }

    private void FillAllDesignation()
    {
        ddlDesignation.DataSource = clsReportCommon.GetAllDesignation();
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationID";
        ddlDesignation.DataBind();
    }

    private void FillAllEmployees()
    {
        DataTable dt = new DataTable();
        dt = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
            chk.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), 0, ddlWorkStatus.SelectedValue.ToInt32(), 0);
        dt.Rows.RemoveAt(0);
        dt.AcceptChanges();
        ddlEmployee.DataSource = dt; 
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
    }

    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllDesignation();
        FillWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
    }

    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    protected void btnShow_Click(object sender, EventArgs e)
    {
        ShowReport();
    }

    public void ShowReport()
    {
        try
        {
            DataTable dtCompany = null;
            DataTable dtJobSalaryDetails = null;
            string strMReportPath = "";
            this.rvEmployeeProfile.Reset();

            //Clear Report Datasources
            this.rvEmployeeProfile.LocalReport.DataSources.Clear();
            strMReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptEmployeeHistoryArb.rdlc" : "reports/RptEmployeeHistory.rdlc");
            DataSet dtEmployeeHistory = null;

            //Set Reportpath
            this.rvEmployeeProfile.LocalReport.ReportPath = strMReportPath;

            //Company Header
            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chk.Checked);

            //Summary report
            dtEmployeeHistory = clsRptEmployeeProfile.GetEmployeeHistory(ddlEmployee.SelectedValue.ToInt32());

            if (dtEmployeeHistory != null)
            {
                DataTable EmployeeInfo = dtEmployeeHistory.Tables[0];
                DataTable EmployeeDocuments = dtEmployeeHistory.Tables[6];
                DataTable EmployeeLeaveSummry = dtEmployeeHistory.Tables[4];
                DataTable EmployeeVacation = dtEmployeeHistory.Tables[5];
                DataTable Loan = dtEmployeeHistory.Tables[2];
                DataTable EmployeeSalaryStructure = dtEmployeeHistory.Tables[3];
                DataTable EmployeeWorkPolicy = dtEmployeeHistory.Tables[1];
                DataTable EmployeeTransfer = dtEmployeeHistory.Tables[7];
                DataTable EmployeePayment = dtEmployeeHistory.Tables[8];

                ViewState["RecordCount"] = dtEmployeeHistory.Tables[0].Rows.Count;

                if (dtEmployeeHistory.Tables[0].Rows.Count > 0)
                {
                    rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] {new ReportParameter("GeneratedBy", 
                        new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))});
                    rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] {new ReportParameter("IsDateSelected", "0")});

                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeInfo", EmployeeInfo));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeDocuments", EmployeeDocuments));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeLeaveSummry", EmployeeLeaveSummry));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeVacation", EmployeeVacation));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_Loan", Loan));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeWorkPolicy", EmployeeWorkPolicy));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeTransfer", EmployeeTransfer));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeePayment", EmployeePayment));
                    this.rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("rptDtSetEmployeeInformation_EmployeeSalaryStructure", EmployeeSalaryStructure));
                }
            }

            if (dtEmployeeHistory.Tables[0].Rows.Count > 0)
            {
                btnPrint1.Enabled = true;
                rvEmployeeProfile.LocalReport.Refresh();
                rvEmployeeProfile.Visible = true;
            }
            else
            {
                string msg = string.Empty;
                rvEmployeeProfile.Visible = false;
                btnPrint1.Enabled = false;
                msg = clsGlobalization.IsArabicCulture() ? "العثور على أي معلومات" : "No information found";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
            }
        }
        catch { }
    }

    private DataTable GetEmployees()
    {
        clsEmployeeReports objEmployeeReport = new clsEmployeeReports();
        objEmployeeReport.CompanyID = int.Parse(ddlCompany.Items.Count == 0 ? "0" : ddlCompany.SelectedValue);
        return objEmployeeReport.GetAllEmployees();
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        ShowReport();
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }

    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();
    }
    
    private void EnableDisableIncludeCompany()
    {
        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
            ddlBranch.Enabled = true;

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
            chk.Checked = chk.Enabled = false;
        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chk.Checked = true;
                chk.Enabled = false;
            }
            else
                chk.Enabled = chk.Checked = true;
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        ShowReport();
    }

    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(rvEmployeeProfile, this);
        ShowReport();
    }

    protected void chk_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    protected void rvEmployeeProfile_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        rvEmployeeProfile.Visible = true;
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
}