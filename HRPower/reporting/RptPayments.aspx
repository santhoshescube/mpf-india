﻿<%@ Page Title="Payment Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptPayments.aspx.cs" Inherits="reporting_RptPayments" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
    
   <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,Payments%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                 <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%" rowspan="2">
                          <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompnay" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDepartment" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Department %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                                </asp:DropDownList>
                                                
                                                <td>
                                               <%-- Type--%>
                                               <asp:Literal ID="Literal3" runat ="server" Text='<%$Resources:ReportsCommon,Type %>'>
                                             
                                             </asp:Literal>
                                                </td>
                                                
                                                <td>
                                                 <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="140px" onselectedindexchanged="ddlType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFromDate" runat="server" Text='<%$Resources:ReportsCommon,FromDate %>' Font-Bold="False"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFromDate" onkeypress="return false" runat="server" CssClass="textbox" MaxLength="10" OnTextChanged="txtFromDate_TextChanged"
                                                        Width="85px" ></asp:TextBox>
                                                    <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                        Format="dd-MMM-yyyy" PopupButtonID="txtFromDate" PopupPosition="BottomLeft" TargetControlID="txtFromDate" />
                                                </td>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBranch" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDesignation" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Designation %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                        <%--    Month--%>
                                        <asp:Literal ID="Literal2" runat ="server" Text='<%$Resources:ReportsCommon,Month %>'>
                                             
                                             </asp:Literal>
                                            </td>
                                            <td>
                                            <asp:DropDownList ID="ddlMonthAndYear" runat ="server"  Width="140px" ></asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%$Resources:ReportsCommon,ToDate %>' Font-Bold="False"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtToDate" onkeypress="return false" runat="server" CssClass="textbox" Width="85px" MaxLength="10"
                                                    OnTextChanged="txtToDate_TextChanged" ></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                    Format="dd-MMM-yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate" PopupPosition="BottomLeft" />
                                            </td>
                                            <td>
                                            </td>
                                          
                                        </tr>
                                        <tr>
                                            
                                           
                                            <td colspan ="2">
                                             <asp:CheckBox ID="chkIncludeCompany" runat="server" AutoPostBack="True" Checked="True"
                                                    Font-Bold="False" OnCheckedChanged="chkIncludeCompany_CheckedChanged" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                                    Width="150px" Font-Size="Small" />
                                            </td>
                                            <td>
                                            
                                          <%--  Status--%>
                                          <asp:Literal ID="Literal1" runat ="server" Text='<%$Resources:ReportsCommon,Status %>'>
                                             
                                             </asp:Literal>
                                            </td>
                                            <td>
                                              <asp:DropDownList ID="ddlWorkStatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                             <%--  Employee--%>
                                             <asp:Literal ID="Literal" runat ="server" Text='<%$Resources:ReportsCommon,Employee %>'>
                                             
                                             </asp:Literal>
                                            </td>
                                            <td>
                                             <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" Width="140px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                          <td colspan ="6">
                                              <div style="float: left">
                                                  <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,TransactionType %>'>
                                             
                                                  </asp:Literal>
                                              </div>
                                         
                                           <div style="float: left;padding-left:10px">
                                              <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="dropdownlist_mandatory"
                                                     Width="130px">
                                                </asp:DropDownList>
                                                </div> 
                                             
                                            </td>
                                          <%--  <td>
                                              <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="dropdownlist_mandatory"
                                                     Width="130px">
                                                </asp:DropDownList>
                                            </td>--%>
                                        </tr>
                                    </table>
                               <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>' OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print %>'  OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <%--<div style="clear: both; height: auto; width: 100%">
        <rsweb:ReportViewer ID="rvPayments" runat="server" Width="100%" 
            SizeToReportContent="True" ShowPrintButton="False" ShowRefreshButton="False"
            Font-Names="Verdana" Font-Size="8pt" Height="700px">
           
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>--%>
    
    
    
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="ReportViewer_PageNavigation">
           <%-- <LocalReport ReportPath="App_Code\rptEmployeeProfile.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsEmployee_dtEmployee" />
                </DataSources>
            </LocalReport>--%>
        </rsweb:ReportViewer>
       <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>--%>
    </div>
    
</asp:Content>
