﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Reporting.WebForms;
using System.Globalization;

public partial class reporting_RptPayments : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
     protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "PaymentReport").ToString();
        btnPrint1.Enabled = false;
        ReportViewer.Visible = false;
        if (!IsPostBack)
        {
            //Fill all the combos
            FillCombos();
            //Enable or disable the month combo ,from date and to date combo based on the type value
            EnableDisableDates();
            chkIncludeCompany.Checked = true;
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");

        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

    }



    private void EnableDisableDates()
    {
        if (ddlType.SelectedIndex == 0)
        {
            lblFromDate.Enabled = false;
            lblToDate.Enabled = false;
            txtFromDate.Enabled = false;
            txtToDate.Enabled = false;
            //lblMonthAndYear.Enabled = true;
          ddlMonthAndYear.Enabled = true;

        }
        else
        {
            lblFromDate.Enabled = true;
            lblToDate.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
           // lblMonthAndYear.Enabled = false;
            ddlMonthAndYear.Enabled = false;

        }
    }

    
    private void FillCombos()
    {
        FillAllCompanies();
        FillAllDepartments();
        FillPaymentTypes();
        FillDesignation();
        FillWorkStatus();
        FillAllTransactionTypes(); 

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        //FillAllEmployees();
       // FillPaymentMonthYear();

    }

    private void FillAllCompanies()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }



    private void FillAllTransactionTypes()
    {
        ddlTransactionType.DataTextField = "TransactionType";
        ddlTransactionType.DataValueField = "TransactionTypeID";
        ddlTransactionType.DataSource = clsReportCommon.GetAllTransactionTypes();
        ddlTransactionType.DataBind();
    }

    private void FillAllBranch()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        ddlBranch.SelectedValue = "0";

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllDepartments()
    {
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataBind();
    }

    private void FillAllEmployees()
    {
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                    ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), -1, ddlWorkStatus.SelectedValue.ToInt32(), -1);

        ddlEmployee.DataBind();

        ddlEmployee_SelectedIndexChanged(new object(), new EventArgs()); 
      
    }

    private void FillPaymentMonthYear()
    {

        ddlMonthAndYear.DataValueField = "Value";
        ddlMonthAndYear.DataTextField = "Description";
        ddlMonthAndYear.DataSource = clsReportCommon.GetPaymentMonthYear(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                            chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32());

        ddlMonthAndYear.DataBind();
        if (ddlMonthAndYear.Items.Count > 0)
            ddlMonthAndYear.SelectedIndex = 0;
        //else
        //    ddlMonthAndYear.Text = "";

     

    }

    private void FillPaymentTypes()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("TypeID");
        dt.Columns.Add("Type");

        dr = dt.NewRow();
        dr["TypeID"] = 0;
        dr["Type"] = "Monthly";
        dt.Rows.InsertAt(dr, 0);


        dr = dt.NewRow();
        dr["TypeID"] = 1;
        dr["Type"] = "Summary";
        dt.Rows.InsertAt(dr, 1);



        ddlType .DataSource = dt;
        ddlType.DataTextField = "Type";
        ddlType.DataValueField= "TypeID";
        ddlType.DataBind();


    }

    //private void EnableDisableIncludeCompany()
    //{
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }
    //}

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0"; 
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    private void FillDesignation()
    {
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationID";
        ddlDesignation.DataSource = clsReportCommon.GetAllDesignation();
        ddlDesignation.DataBind();
    }

    private void FillWorkStatus()
    {
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString(); 
    }














    #region"Filling Drop Down List"
   
  
    private bool ValidateDate()
    {
        //DateTime dtFromDate = System.DateTime.MinValue;
        //DateTime dtToDate = System.DateTime.MinValue;

        //DateTimeFormatInfo df = new DateTimeFormatInfo();

        //df.ShortDatePattern = "dd MMM yyyy";

        //bool blnIsValid = true;
        //string strMessage = string.Empty;


        //if (rdBtnSummary.Checked)
        //{
        //    if (txtFromDate.Text == string.Empty)
        //    {
        //        strMessage = "Please Select From Date";
        //        blnIsValid = false;
        //    }
        //    else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        //    {
        //        strMessage = "Please Select Valid Date";
        //        blnIsValid = false;
        //    }
        //    else if (txtToDate.Text == string.Empty)
        //    {
        //        strMessage = "Please Select To Date";
        //        blnIsValid = false;
        //    }
        //    else if (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        //    {
        //        strMessage = "Please Select Valid Date";
        //        blnIsValid = false;
        //    }

        //    if (!blnIsValid)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
        //        return false;
        //    }
        //}

        return true;
    }






    private void ShowMessage(string message)
    {
        ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + message + "');", true );
   
    }

    private bool ValidateReport()
    {
         
        DateTime dtDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();
        df.ShortDatePattern = "dd-MMM-yyyy";



        if (ddlType.SelectedIndex == 0 && ddlMonthAndYear.Items.Count == 0)
        {
             //ShowMessage("Please select a month");

            ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectAMonth").ToString());
            return false;
        }
        else if (ddlType.SelectedIndex == 1)
        {

            if (txtFromDate.Text == "")
            {
                //ShowMessage("Please enter from date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate").ToString());
                return false;
            }
            else if (txtToDate.Text == "")
            {
               // ShowMessage("Please enter to date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate").ToString());
                return false;
            }

            else if (!(DateTime.TryParse(txtFromDate.Text,df, DateTimeStyles.None , out dtDate)))
            {
                //ShowMessage("Invalid from date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidFromDate").ToString());
                return false;
            }
            else if (!(DateTime.TryParse(txtToDate.Text, out dtDate)))
            {
                //ShowMessage("Invalid to date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidToDate").ToString());
                return false;
            }
        }
        return true;
    }


    private void ShowReport()
    {
        try
        {
            ReportViewer.Visible = false;
            int Month = 0;
            int Year = 0;
            DataTable DtPayments = null;
            string strMReportPath = "";

            //setting the report path based on the payment type value

            strMReportPath = ddlType.SelectedIndex == 0 ? ( clsGlobalization.IsArabicCulture()? Server.MapPath("reports/RptEmployeeMonthlyPaymentArb.rdlc") :   Server.MapPath("reports/RptEmployeeMonthlyPayment.rdlc"))
            : ( clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptEmployeeYearlyPaymentArb.rdlc"): Server.MapPath("reports/RptEmployeeYearlyPayment.rdlc"));

            //Setting the report header 

            ReportViewer.Reset();
            ReportViewer.LocalReport.DataSources.Clear();
            ReportViewer.LocalReport.ReportPath = strMReportPath;

            //Monthly Report
            if (ddlType.SelectedIndex == 0)
            {
                Month = ddlMonthAndYear.SelectedValue.ToString().Split('@')[0].ToInt16();
                Year = ddlMonthAndYear.SelectedValue.ToString().Split('@')[1].ToInt16();

                ReportViewer.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false),
                    new ReportParameter("CompanyName", ddlCompany.SelectedItem.Text , false),
                    new ReportParameter("BranchName",ddlBranch.SelectedItem.Text , false),
                    new ReportParameter("Department", ddlDepartment.SelectedItem.Text , false),
                    new ReportParameter("EmployeeName",ddlEmployee.SelectedItem.Text, false),
                    new ReportParameter("MonthAndYear",  ddlMonthAndYear.SelectedItem.Text , false),
                     new ReportParameter("Designation",ddlDesignation.SelectedItem.Text, false),
                    new ReportParameter("WorkStatus",  ddlWorkStatus.SelectedItem.Text , false)

                });



                DtPayments = clsRptPayments.GetMonthlyPayments(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), Month, Year, chkIncludeCompany.Checked,ddlTransactionType.SelectedValue.ToInt32());
                if (DtPayments.Rows.Count > 0)
                {
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeSalarySummary", DtPayments));
                    btnPrint1.Enabled = true;
                }
            }

            else if (ddlType.SelectedIndex == 1)
            {

                DtPayments = clsRptPayments.GetSummaryPayments(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), txtFromDate.Text.ToDateTime(), txtToDate.Text.ToDateTime(), chkIncludeCompany.Checked,ddlTransactionType.SelectedValue.ToInt32());

                DataTable dtCompanies = DtPayments.DefaultView.ToTable(true, "CompanyName");


                ReportViewer.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false),
                    new ReportParameter("CompanyName", ddlCompany.SelectedItem.Text , false),
                    new ReportParameter("BranchName",ddlBranch.SelectedItem.Text , false),
                    new ReportParameter("Department", ddlDepartment.SelectedItem.Text , false),
                    new ReportParameter("EmployeeName",ddlEmployee.SelectedItem.Text, false),
                    new ReportParameter("FromDate",  txtFromDate.Text, false),
                    new ReportParameter("ToDate", txtToDate.Text  , false),
                       new ReportParameter("Designation",ddlDesignation.SelectedItem.Text, false),
                    new ReportParameter("WorkStatus",  ddlWorkStatus.SelectedItem.Text , false),
                    new ReportParameter("Count",  dtCompanies.Rows.Count.ToString() , false),

                });


                if (DtPayments.Rows.Count > 0)
                {
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeYearlyPayment", DtPayments));
                    btnPrint1.Enabled = true;
                }

            }

            ViewState["RecordCount"] = DtPayments.Rows.Count;
            //Show no record found
            if (DtPayments.Rows.Count == 0)
            {
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('NoInformationFound');", true);
                return;
            }

            ReportViewer.LocalReport.Refresh();

            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;

            ReportViewer.Visible = true;
            //Set the report display mode and zoom percentage
            // SetReportDisplay(ReportViewer);

        }
        catch (Exception ex)
        {

        }
       // upd.Update();
    }




  
    #endregion
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();
    }
    private void EnableDisable()
    {
        //if (rdBtnSummary.Checked == true && ddlEmployee.SelectedValue.ToInt32() != -1)
        //{
        //    FillFinancialYear();

        //}

        //if (ddlEmployee.SelectedValue.ToInt32() != -1 && rdBtnSummary.Checked == true)
        //{
        //    ChkFinYear.Enabled = true;
        //    ddlFinYear.Enabled = true;
        //    lblPeriod.Enabled = true;

        //}
        //else
        //{
        //    ChkFinYear.Enabled = false;
        //    ddlFinYear.Enabled = false;
        //    lblPeriod.Enabled = false;

        //}
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillPaymentMonthYear();
    }

    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {

        if (ValidateReport())
            ShowReport();

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer, this);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void rdBtnMonthly_CheckedChanged(object sender, EventArgs e)
    {
        //if (rdBtnMonthly.Checked == true)
        //{
        //    lblFromDate.Enabled = false;
        //    lblToDate.Enabled = false;
        //    txtFromDate.Enabled = false;
        //    txtToDate.Enabled = false;
        //    lblMonth.Enabled = true;
        //    ddlMonth.Enabled = true;
        //    lblPeriod.Enabled = false;
        //    ddlFinYear.Enabled = false;
        //    ChkFinYear.Enabled = false;
        //}
        //else
        //{
        //    lblFromDate.Enabled = true;
        //    lblToDate.Enabled = true;
        //    txtFromDate.Enabled = true;
        //    txtToDate.Enabled = true;
        //    lblMonth.Enabled = true;
        //    ddlMonth.Enabled = true;
        //    lblPeriod.Enabled = true;
        //    ddlFinYear.Enabled = true;
        //    ChkFinYear.Enabled = true;
        //}
        //FillBranchDropDown();
    }
    protected void rdBtnSummary_CheckedChanged(object sender, EventArgs e)
    {
        //if (rdBtnSummary.Checked == true)
        //{
        //    lblFromDate.Enabled = true;
        //    lblToDate.Enabled = true;
        //    txtFromDate.Enabled = true;
        //    txtToDate.Enabled = true;
        //    lblMonth.Enabled = false;
        //    ddlMonth.Enabled = false;
        //}
        //else
        //{
        //    lblFromDate.Enabled = false;
        //    lblToDate.Enabled = false;
        //    txtFromDate.Enabled = false;
        //    txtToDate.Enabled = false;
        //    lblMonth.Enabled = true;
        //    ddlMonth.Enabled = true;
        //}
        //FillBranchDropDown();
    }
    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        //if (rdBtnSummary.Checked == true && ddlEmployee.SelectedValue.ToInt32() != -1)
        //{
        //    FillFinancialYear();

        //}
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        //if (rdBtnSummary.Checked == true && ddlEmployee.SelectedValue.ToInt32() != -1)
        //{
        //    FillFinancialYear();

        //}
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableDates(); 
    }
    protected void ReportViewer_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer.Visible = true;
    }
}
