﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true" CodeFile="RptProjectCost.aspx.cs" Inherits="reporting_RptProjectCost" Title="Project cost Report" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {

            // add click event handler
            $('#span-toggle').click(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");            

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
         function Validate()
        {        
         var hidControl = document.getElementById("ctl00_hidCulture");      
         var StartDate = document.getElementById('<%= txtStartdate.ClientID%>');
         var EndDate = document.getElementById('<%=txtEndDate.ClientID%>');
         
         if(StartDate.value == '')
         {
            alert(hidControl.value == 'ar-AE' ? 'من فضلك ادخل من تاريخ' : 'Please enter from date');//alert('Please enter from date');
            return false;
          }
          else if(EndDate.value == '')
          {
          alert(hidControl.value == 'ar-AE' ? 'الرجاء إدخال حتى الآن' : 'Please enter to date');//alert('Please enter to date');
          return false;
          }
         
         
         return true;
        }
    </script>

    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <%--Hide Filter--%>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal>
                </span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                     <%--       <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <%--Designation--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Designation%>'></asp:Literal>
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" OnSelectedIndexChanged="LoadEmployee" 
                                                    Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblFromDate" runat="server" Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Label></td>
                                            <td>
                                                <asp:TextBox ID="txtStartdate" runat="server" BorderStyle="Solid" 
                                                    BorderWidth="1px" Height="8px" ValidationGroup="Submit" Width="85px"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                    Format="dd MMM yyyy" TargetControlID="txtStartdate">
                                                </AjaxControlToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                             <td>
                                                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Project%>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlProject" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="180px" onselectedindexchanged="ddlProject_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                          
                                            
                                            
                                            <td>
                                                <asp:Label ID="lblToDate" runat="server" Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Label></td>
                                            <td >
                                                <asp:TextBox ID="txtEndDate" runat="server" BorderStyle="Solid" 
                                                    BorderWidth="1px" Height="8px" ValidationGroup="Submit" Width="85px"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                    Format="dd MMM yyyy" TargetControlID="txtEndDate">
                                                </AjaxControlToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Literal></td>
                                            <td>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" 
                                                    OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                           
                                              
                                            <td>
                                                <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="style1">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>' OnCheckedChanged="chkIncludeCompany_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                               <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                     
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" OnClientClick ="return Validate();" runat="server" class="btnsubmit" CausesValidation="false" ValidationGroup ="Submit"
                                           Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnSearch_Click1" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
   
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="ReportViewer1_PageNavigation">
            <LocalReport ReportPath="App_Code\RptVacationSummary.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsVacationSettlement_dtVacationSummary" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
       <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>
</asp:Content>

