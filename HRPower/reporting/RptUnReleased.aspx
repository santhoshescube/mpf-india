﻿<%@ Page Title="Processed Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptUnReleased.aspx.cs" Inherits="reporting_RptUnReleased" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>

    <div style="padding-left: 350px;">
        <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,Processed%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span>
            </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%" rowspan="2">
                            <%--  <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                            <table style="width: 100%" class="labeltext">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCompnay" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDepartment" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Department %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                        </asp:DropDownList>
                                        <td>
                                            <%-- Type--%>
                                            <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,FromDate %>'>
                                             
                                            </asp:Literal>
                                        </td>
                                        <td>
                                                 <asp:TextBox ID="txtFromDate" onkeypress="return false" runat="server" CssClass="textbox"
                                                MaxLength="10" Width="85px"></asp:TextBox>
                                            <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                Format="dd-MMM-yyyy" PopupButtonID="txtFromDate" PopupPosition="BottomLeft" TargetControlID="txtFromDate" />
                                        </td>
                                        <td colspan ="2">
                                            <asp:CheckBox ID="chkIsPayStructureOnly" runat="server" 
                                            Font-Bold="False"  Text='<%$Resources:ReportsCommon,PayStructureOnly %>'
                                            Width="150px" Font-Size="Small" />
                                        </td>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="150px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDesignation" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Designation %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <%--    Month--%>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,ToDate %>'>
                                             
                                        </asp:Literal>
                                    </td>
                                    <td>
                                       <asp:TextBox ID="txtToDate" onkeypress="return false" runat="server" CssClass="textbox"
                                            Width="85px" MaxLength="10" ></asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                            Format="dd-MMM-yyyy" PopupButtonID="txtToDate" TargetControlID="txtToDate" PopupPosition="BottomLeft" />
                                    </td>
                                    <td colspan ="2">
                                           <asp:CheckBox ID="chkTransfer" runat="server" 
                                            Font-Bold="False"  Text='<%$Resources:ReportsCommon,IncludeTransferPayments %>' 
                                            Width="150px" Font-Size="Small" />
                                    </td>
                                                                      
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" AutoPostBack="True" Checked="True"
                                            Font-Bold="False" OnCheckedChanged="chkIncludeCompany_CheckedChanged" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                            Width="150px" Font-Size="Small" />
                                    </td>
                                    <td>
                                        <%--  Status--%>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,Status %>'>
                                             
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="130px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <%--  Employee--%>
                                        <asp:Literal ID="Literal" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'>
                                             
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="140px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <div style="float: left">
                                            <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,TransactionType %>'>
                                             
                                            </asp:Literal>
                                        </div>
                                        <div style="float: left; padding-left: 10px">
                                            <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="dropdownlist_mandatory"
                                                Width="130px">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                  
                                </tr>
                            </table>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>'
                                OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print %>' OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
        <rsweb:ReportViewer ID="ReportViewer" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" OnPageNavigation="ReportViewer_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
