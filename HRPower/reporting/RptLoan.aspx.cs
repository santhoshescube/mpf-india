﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Globalization;

public partial class reporting_RptLoan : System.Web.UI.Page
{
    #region Declarations
    clsCommon objCommon;
    clsUserMaster objUser;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "LoanAdvance").ToString();
        ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            LoadInitial();
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }

    #region Methods
    private void LoadInitial()
    {
        txtStartMonth.Text = clsReportCommon.GetMonthStartDate();
        txtEndMonth.Text = System.DateTime.Today.ToString("dd MMM yyyy");

        FillCompany();
        FillBranch();
        FillEmployees();
        FillDepartment();
        FillWorkStatus();
        FillType();
    }

    private void FillCompany()
    {
        objUser = new clsUserMaster();
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();
    }

    private void FillEmployees()
    {
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                         chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(),-1,-1,ddlWorkStatus.SelectedValue.ToInt32());
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();

    }
    /// <summary>
    /// Fill all types
    /// 0->Monthly 
    /// 1->Financial year
    /// </summary>
    private void FillType()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Type");
        dt.Columns.Add("TypeID");

        dr = dt.NewRow();
        dr["Type"] = "Salary Advance";
        dr["TypeID"] = 1;
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Type"] = "Loan";
        dr["TypeID"] = 2;
        dt.Rows.Add(dr);

        ddlType.DataSource = dt;
        ddlType.DataTextField = "Type";
        ddlType.DataValueField = "TypeID";
        ddlType.DataBind();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = "6";
    }
    private void FillDepartment()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }
    private void FillBranch()
    {
        int CompanyID = ddlCompany.SelectedValue.ToInt32();
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillEmployees();
    }
    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtStartMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate"));
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtStartMonth.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtEndMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtEndMonth.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }


        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DataTable dtAdvanceOrLoan;
            DataTable dtLoanDetails = new DataTable();

            //Set Company Header For Report
            DataTable dtCompany = null;

            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
            
            //Set Report Path
            if (clsGlobalization.IsArabicCulture())
                ReportViewer1.LocalReport.ReportPath = (ddlType.SelectedIndex == 0) ? Server.MapPath("reports/RptAdvanceArb.rdlc") : Server.MapPath("reports/RptLoanDetailsArb.rdlc");
            else
                ReportViewer1.LocalReport.ReportPath = (ddlType.SelectedIndex == 0) ? Server.MapPath("reports/RptAdvance.rdlc") : Server.MapPath("reports/RptLoanDetails.rdlc");

            //Get Report Data
            dtAdvanceOrLoan = clsRptLoan.GetSalaryAdvanceOrLoanSummary(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                                                        ddlDepartment.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(),
                                                                        ddlEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, 
                                                                        clsCommon.Convert2DateTime(txtStartMonth.Text), 
                                                                        clsCommon.Convert2DateTime(txtEndMonth.Text), ddlType.SelectedIndex.ToInt32());

            //Detailed Report of Loan for a specific employee
            if((ddlEmployee.SelectedValue.ToInt32()>0)&& (ddlType.SelectedIndex==1))
                dtLoanDetails = clsRptLoan.GetLoanDetails(ddlEmployee.SelectedValue.ToInt32(), clsCommon.Convert2DateTime(txtStartMonth.Text), 
                                                          clsCommon.Convert2DateTime(txtEndMonth.Text));
            if (dtAdvanceOrLoan != null)
            {
                if (dtAdvanceOrLoan.Rows.Count > 0)
                {
                    //SALARY ADVANCE REPORT
                    if (ddlType.SelectedIndex == 0)
                    {
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsLoanAdvance_SalaryAdvanceReport", dtAdvanceOrLoan));
                    }
                    //LOAN REPORT
                    else
                    {
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsLoanAdvance_LoanDetails", dtAdvanceOrLoan));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsLoanAdvance_LoanInstallment", dtLoanDetails));
                    }

                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                    {
                        new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                        new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                        new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                        new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                        new ReportParameter("WorkStatus",ddlWorkStatus.SelectedItem.Text.Trim()),
                        new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),                                
                        new ReportParameter("Period",clsCommon.Convert2DateTime(txtStartMonth.Text).ToString("dd MMM yyyy")+"-"+clsCommon.Convert2DateTime(txtEndMonth.Text).ToString("dd MMM yyyy"))
                    });
                    
                    btnPrint.Enabled = true;
                    ReportViewer1.LocalReport.Refresh();
                    ReportViewer1.Visible = true;
                }
            }
            else
            { // Show 'No Records Found' Message to User
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
            }
        }
        catch { }
    }


    #endregion
}
