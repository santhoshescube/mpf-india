﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;

public partial class reporting_RptRecruitmentProcess : System.Web.UI.Page
{
    clsRptCandidates MobjclsRptCandidates;

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "JobReport").ToString();
        ReportViewer1.Visible = false;
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
    }

    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadJobs(new object(), new EventArgs());
    }

    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }

    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;
    }

    private void FillAllDeparments()
    {
        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }

    private void FillAllBranches()
    {
        int intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);

        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));

        ddlBranch_SelectedIndexChanged(null, null);
    }

    protected void LoadJobs(object sender, EventArgs e)
    {
        FillAllJobs();
    }

    private void FillAllJobs()
    {
        this.BindDropDown(ddlJob, "JobId", "JobTitle", clsRptCandidates.FillJobs(ddlCompany.SelectedValue.ToInt32(), 
            ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32()));
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }

    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllJobs();
    }

    private void EnableDisableIncludeCompany()
    {
        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
            ddlBranch.Enabled = true;

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
        }
    }

    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllJobs();
    }

    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        ShowReport();
    }

    private void ShowReport()
    {
        try
        {
            DataTable dtCompany = null;
            DataTable dtJobSalaryDetails = null;
            string strMReportPath = "";
            this.ReportViewer1.Reset();

            //Clear Report Datasources
            this.ReportViewer1.LocalReport.DataSources.Clear();
            strMReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptRecruitmentProcessArb.rdlc" : "reports/RptRecruitmentProcess.rdlc");
            DataSet dtRecruitmentProcess = null;

            //Set Reportpath
            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;

            //Company Header
            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), 
                chkIncludeCompany.Checked);

            //Summary report
            dtRecruitmentProcess = clsRptCandidates.GetRecruitmentProcess(ddlJob.SelectedValue.ToInt32());

            if (dtRecruitmentProcess != null)
            {
                ViewState["RecordCount"] = dtRecruitmentProcess.Tables[0].Rows.Count;

                if (dtRecruitmentProcess.Tables[0].Rows.Count > 0)
                {
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] {new ReportParameter("GeneratedBy", 
                        new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))});

                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsJob_RecruitmentProcess", dtRecruitmentProcess.Tables[0]));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsJob_RecruitmentProcessElapsedTime", dtRecruitmentProcess.Tables[1]));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsJob_RecruitmentProcessSummary", dtRecruitmentProcess.Tables[2]));
                }
            }

            if (dtRecruitmentProcess.Tables[0].Rows.Count > 0)
            {
                btnPrint.Enabled = true;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            else
            {
                string msg = string.Empty;
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                msg = clsGlobalization.IsArabicCulture() ? "العثور على أي معلومات" : "No information found";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
}