﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using Microsoft.Reporting.WebForms;
using System.Globalization;  

public partial class reporting_RptMISLeaveSummary : System.Web.UI.Page
{
   
    private int intCompany;
    private int intBranch;
    private int intEmployee;
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Title = GetGlobalResourceObject("ReportsCommon", "LeaveSummaryReport").ToString();
        ReportViewer1.Visible = false;
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
        if (!IsPostBack)
        {
            ddlFinYear.Enabled = false;
            
            chkIncludeCompany.Checked = true;
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
    }

    protected void OnChange()
    {
        if (chkFinSummary.Checked)
        {
            lblFromDate.Enabled =lblToDate.Enabled= false;
            txtFromDate.Enabled =txtToDate.Enabled= false;
            txtFromDate.Text =  txtToDate.Text= new clsCommon().GetSysDate();
            btnFromDate.Enabled =btnToDate.Enabled= false;
            lblPeriod.Enabled = true;
            ddlFinYear.Enabled = true;
        }
        else
        {
            lblFromDate.Enabled =lblToDate.Enabled= true;
            txtFromDate.Enabled =txtToDate.Enabled= true;
            btnFromDate.Enabled = btnToDate.Enabled= true;
            lblPeriod.Enabled = false;
            ddlFinYear.Enabled = false;
        }
    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
        FillFinancialYears();
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    //private void EnableDisableIncludeCompany()
    //{
    //    //No branch selected
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }

    //}
    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }

    private void FillAllWorkStatus()
    {

        this.BindDropDown(ddlWorkstatus, "WorkStatusID", "WorkStatus", clsReportCommon.GetAllWorkStatus());

    }
    private void FillAllEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue==""?"0":ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), -1, -1, ddlWorkstatus.SelectedValue.ToInt32(), -1
                                ));


    }
    private void FillFinancialYears()
    {
       
        this.BindDropDown(ddlFinYear, "FinYearStartDate", "FinYearPeriod", clsReportCommon.GetAllFinancialYears(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                            chkIncludeCompany.Checked));

    }
    #endregion
  
   
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ReportViewer1.Visible = true;
            ShowReport();
        }
    }
    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;

       
        if (txtFromDate.Text == string.Empty)
        {
            strMessage =GetGlobalResourceObject("ReportsCommon","PleaseSelectFromDate").ToString();
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = GetGlobalResourceObject("ReportsCommon","PleaseSelectValidDate").ToString();
            blnIsValid = false;
        } 
        else if (txtToDate.Text == string.Empty)
        {
            strMessage = GetGlobalResourceObject("ReportsCommon","PleaseSelectToDate").ToString();
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = GetGlobalResourceObject("ReportsCommon","PleaseSelectValidDate").ToString();
            blnIsValid = false;
        }
        


        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }

    protected void chkSummary_CheckedChanged(object sender, EventArgs e)
    {
        OnChange();
    }
    private void ShowReport()
    {
        try
        {
            DataTable dtCompany = null;
            this.ReportViewer1.Reset();
            //Clear DataSources
            this.ReportViewer1.LocalReport.DataSources.Clear();
            //Set Report Path
            this.ReportViewer1.LocalReport.ReportPath = chkFinSummary.Checked ? (clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptLeaveFinancialYearSummaryArb.rdlc") : Server.MapPath("reports/RptLeaveFinancialYearSummary.rdlc")) : (clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RpLeaveSummaryArb.rdlc") : Server.MapPath("reports/RpLeaveSummary.rdlc"));


            //Set Company Header For Report
            //Company Header

            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            //Leave Summary Report
            if (chkFinSummary.Checked == true)
            {
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                    new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                    new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                    new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                    new ReportParameter("WorkStatus",ddlWorkstatus.SelectedItem.Text.Trim()),
                    new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),
                    new ReportParameter("Period",ddlFinYear.SelectedItem.Text.Trim())

                });

                string strFinYearStartDate = string.Empty;


                if (ddlFinYear.SelectedIndex != -1 && ddlFinYear.Text.ToStringCustom() != string.Empty)
                {
                    strFinYearStartDate = ddlFinYear.Text.ToStringCustom().Split('-')[0].Trim();
                }

                DataTable dtLeaveSummary = clsMISEmployeeReport.GetLeaveSummaryReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlWorkstatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), strFinYearStartDate);
                ViewState["RecordCount"] = dtLeaveSummary.Rows.Count;
                if (dtLeaveSummary.Rows.Count > 0)
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeaveSummary", dtLeaveSummary));


                    btnPrint1.Enabled = true;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('"+GetGlobalResourceObject("ReportsCommon","NoInformationFound").ToString()+"');", true);
                    ReportViewer1.Visible = false;
                    btnPrint1.Enabled = false;
                    return;

                }

            }
            else // Get Leave Detail Report
            {

                DateTime dtFromDate = txtFromDate.Text.ToDateTime();
                DateTime dtToDate = txtToDate.Text.ToDateTime();

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                    new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                    new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                    new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                    new ReportParameter("WorkStatus",ddlWorkstatus.SelectedItem.Text.Trim()),
                    new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),
                    new ReportParameter("Period", txtFromDate.Text+" - "+txtToDate.Text)

                });

                DataTable dtLeaveReport = clsMISEmployeeReport.GetLeaveReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlWorkstatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), dtFromDate, dtToDate);
                ViewState["RecordCount"] = dtLeaveReport.Rows.Count;
                if (dtLeaveReport.Rows.Count > 0)
                {
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_dtLeave", dtLeaveReport));
                    btnPrint1.Enabled = true;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                    ReportViewer1.Visible = false;
                    btnPrint1.Enabled = false;
                    return;
                }

            }
        }
        catch (Exception ex)
        {
        }
    }
    //private void ShowReport()
    //{
    //    ReportViewer1.Visible = false;
    //    this.ReportViewer1.Reset();
    //    int intCompanyID = 0;
    //    int intBranchID = 0;
    //    int intDepartmentID = 0;
    //    int intIncludeComp = 0;
    //    int intEmployeeID = 0;
    //    int intMonth = 0;
    //    int intYear = 0;
    //    string strFromDate = "";
    //    string strToDate = "";
    //    string strFinYearStartDate = "";
    //    string[] MonthYear = new string[2];
    //    string[] FinYears = new string[2];
    //    DataTable dtCompanyInfo;
    //    string strMReportPath = "";

    //    intCompanyID = ddlCompany.SelectedValue.ToInt32();
    //    intBranchID = ddlBranch.SelectedValue.ToInt32();
    //    intEmployeeID = ddlEmployee.SelectedValue.ToInt32();


    //    objEmployee = new clsMISEmployeeReport();

    //    if (ddlBranch.SelectedValue.ToInt32() == -2)
    //        intBranchID = 0;
    //    else
    //        intBranchID = ddlBranch.SelectedValue.ToInt32();

    //    if (chkIncludeCompany.Checked)
    //    {
    //        intIncludeComp = 1;
    //    }
    //    else
    //    {
    //        intIncludeComp = 0;
    //    }

    //    if (chkFinSummary.Checked == true)
    //    {
    //        strMReportPath = Server.MapPath("reports/RptLeaveFinancialYearSummary.rdlc");
    //    }
    //    else
    //    {
    //        strMReportPath = Server.MapPath("reports/RpLeaveSummary.rdlc");
    //    }


    //    if (chkIncludeCompany.Checked)
    //    {
    //        dtCompanyInfo = objEmployee.DisplayCompanyReportHeader(intCompanyID);
    //    }
    //    else
    //    {
    //        if (intBranchID > 0)
    //        {
    //            dtCompanyInfo = objEmployee.DisplayCompanyReportHeader(intBranchID);
    //        }
    //        else
    //        {
    //            dtCompanyInfo = objEmployee.DisplayCompanyReportHeader(intCompanyID);
    //        }
    //    }

    //    this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
    //    this.ReportViewer1.LocalReport.ReportPath = strMReportPath;

    //    if (chkFinSummary.Checked == true)
    //    {
    //        this.ReportViewer1.LocalReport.DataSources.Clear();

    //        if (ddlFinYear.SelectedIndex != -1)
    //        {
    //            if (ddlFinYear.Text.ToStringCustom() != string.Empty)
    //            {
    //                FinYears = ddlFinYear.SelectedItem.Text.ToStringCustom().Split('-');
    //                strFinYearStartDate = FinYears[0].ToStringCustom().TrimStart().TrimEnd().ToString();
    //            }

    //        }

    //        this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
    //        DataTable dtLeaveFinYearSummary = objEmployee.GetLeaveFinYearSummary(intCompanyID, intBranchID, intEmployeeID, strFinYearStartDate, intIncludeComp);

    //        if (dtLeaveFinYearSummary.Rows.Count > 0)
    //        {
    //            btnPrint1.Enabled = true;
    //            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyInfo));
    //            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_EmployeeLeaveSummary", dtLeaveFinYearSummary));
    //            ReportViewer1.Visible = true;
    //        }
    //        else
    //        {
    //            btnPrint1.Enabled = false;
    //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
    //            //UserMessage.ShowMessage(9707, null, 2);
    //            return;

    //        }
    //        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
    //            {
    //              new ReportParameter("GeneratedBy",  "HRPOWER")
                
    //            });

    //    }
    //    else
    //    {

    //        strFromDate = txtFromDate.Text.ToDateTime().ToString("dd-MMM-yyyy");
    //        strToDate = txtToDate.Text.ToDateTime().ToString("dd-MMM-yyyy");

    //        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
    //            {  new ReportParameter("GeneratedBy",  "HRPOWER") ,
    //                  new ReportParameter("FromDate",  strFromDate),
    //                    new ReportParameter("ToDate",  strToDate)
              
                
    //            });


    //        this.ReportViewer1.LocalReport.DataSources.Clear();

    //        this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
    //        DataTable dtLeaveSummary = objEmployee.GetLeaveSummary(intCompanyID, intBranchID, intEmployeeID, strFromDate, strToDate, intIncludeComp);
    //        if (dtLeaveSummary.Rows.Count > 0)
    //        {
    //            btnPrint1.Enabled = true;
    //            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyInfo));
    //            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetLeaveSummary_EmployeeLeaveDetails", dtLeaveSummary));
    //            ReportViewer1.Visible = true;
    //        }
    //        else
    //        {
    //            btnPrint1.Enabled = false;
    //            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
    //            //UserMessage.ShowMessage(9707, null, 2);
    //            return;


    //        }
    //    }

    //}
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
}
