﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using Microsoft.Reporting.WebForms;

public partial class reporting_RptMISSalaryStructure : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (clsGlobalization.IsArabicCulture())
            {
                ddlType.Items.Add(new ListItem() { Text = " راتب", Value = "0" });
                ddlType.Items.Add(new ListItem() { Text = "تاريخ", Value = "1" });
            }
            else
            {
                ddlType.Items.Add(new ListItem() { Text = "Salary", Value = "0" });
                ddlType.Items.Add(new ListItem() { Text = "History", Value = "1" });
            }
        }


        this.Title = GetGlobalResourceObject("ReportsCommon", "SalaryStructureReport").ToString();
        ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

    }
    private void LoadCombos()
    {
        FillCompany();

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        FillDepartment();
        FillAllDesignations();
        FillWorkStatus();
        FillEmploymentTypes();
        ddlType.SelectedIndex =0;


        LoadEmployee(new object(),new EventArgs ());
    }



    /// <summary>
    /// Fill All Companies
    /// </summary>
    private void FillCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();

    }
    /// <summary>
    /// Fill All Branches based on the selected company
    /// </summary>
    private void FillBraches()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        ddlBranch.SelectedValue = "0";


        ddlBranch_SelectedIndexChanged(new object(), new EventArgs()); 
    }

    /// <summary>
    /// Fill All Departments
    /// </summary>
    private void FillDepartment()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }
    private void FillAllDesignations()
    {
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationID";
        ddlDesignation.DataSource = clsReportCommon.GetAllDesignation();
        ddlDesignation.DataBind();
    }
    /// <summary>
    /// Fill All WorkStatus
    /// </summary>
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();

        ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString(); 
    }

    /// <summary>
    /// Fill All EmploymentTypes
    /// </summary>
    private void FillEmploymentTypes()
    {
        ddlEmploymentType .DataSource = clsReportCommon.GetAllEmploymentTypes();
        ddlEmploymentType.DataTextField = "EmploymentType";
        ddlEmploymentType.DataValueField = "EmploymentTypeID";
        ddlEmploymentType.DataBind();
    }
    private void FillAllEmployees()
    {
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployeesInCurrentCompany(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                            chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlEmploymentType.SelectedValue.ToInt32(),
                            ddlWorkStatus.SelectedValue.ToInt32()
                            );
        ddlEmployee.DataBind();
    }
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBraches();
        FillAllEmployees();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();
    }
    //private void EnableDisableIncludeCompany()
    //{
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }
    //}

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    public void SetDefaultBranch(DropDownList  cb)
    {
        cb.SelectedValue = "0";
    }

    public void SetDefaultWorkStatus(DropDownList  cb)
    {
        cb.SelectedValue = "6";
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        ShowReport();

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }


    private void ShowReport()
    {
        try
        {
            ReportViewer1.Visible = false;
            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            string strMReportPath = "";
            DataTable dtSalary;

            strMReportPath = ddlType.SelectedIndex == 0 ? (clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptMISSalaryStructureSummaryArb.rdlc") : Server.MapPath("reports/RptMISSalaryStructureSummary.rdlc")) :
                (clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptMISSalaryStructureHistorySummaryArb.rdlc") : Server.MapPath("reports/RptMISSalaryStructureHistorySummary.rdlc"));

            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;

            //IF CBOTYPE SELECTED INDEX = 0 THEN SHOW CURRENT SALARY STRUCTURE REPORT
            //ELSE SHOW HISTORY REPORT

            if (ddlType.SelectedIndex == 0)
            {
                this.ReportViewer1.LocalReport.SetParameters(
                    new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",  new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                        new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                        new ReportParameter("Branch",ddlBranch .SelectedItem .Text.Trim()),
                        new ReportParameter("Department",ddlDepartment.SelectedItem .Text.Trim()),
                        new ReportParameter("WorkStatus",ddlWorkStatus.SelectedItem .Text.Trim()),
                        new ReportParameter("EmploymentType",ddlEmploymentType.SelectedItem .Text.Trim())
                    });

                dtSalary = clsRptSalaryStructure.GetSalaryStructureSummary(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmploymentType.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlEmployee.SelectedValue.ToInt32());

                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalary));

            }
            else
            {
                this.ReportViewer1.LocalReport.SetParameters(
                    new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",  new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                        new ReportParameter("Company",ddlCompany.SelectedItem .Text.Trim()),
                        new ReportParameter("Branch",ddlBranch.SelectedItem .Text.Trim()),
                        new ReportParameter("Department",ddlDepartment.SelectedItem .Text.Trim()),
                        new ReportParameter("WorkStatus",ddlWorkStatus.SelectedItem .Text.Trim()),
                        new ReportParameter("EmploymentType",ddlEmploymentType.SelectedItem .Text.Trim())
                    });

                dtSalary = clsRptSalaryStructure.GetSalaryStructureHistorySummary(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmploymentType.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlEmployee.SelectedValue.ToInt32(),false );


                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayroll_SalaryStructureSummary", dtSalary));
            }



            ViewState["RecordCount"] = dtSalary.Rows.Count;

            //If no records found show error message(No records found)
            if (dtSalary.Rows.Count == 0)
            {
                // UserMessage.ShowMessage(9700);
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnShow, btnShow.GetType(), new Guid().ToString(), "alert('"+GetGlobalResourceObject("ReportsCommon","NoInformationFound").ToString()+"');", true);
                return;
            }
            else
            {
                btnPrint.Enabled = true;
                ReportViewer1.Visible = true;
                // this.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                this.ReportViewer1.ZoomMode = ZoomMode.Percent;
                this.ReportViewer1.ZoomPercent = 100;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true; 
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
}
