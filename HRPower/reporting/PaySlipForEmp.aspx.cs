﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;

public partial class reporting_PaySlipForEmp : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private clsCommon objCommon;
    public DataTable MdatLeaveDed;
    public DataTable MdatOTDed;
    public DataTable MdatHourDetail;
    public DataTable MdatConsequenceList;
    public DataTable MdatSalarySlipWorkInfo;
    public DataTable MdatMaster;
    public DataTable datDetailDed;
    public DataTable datDetail;
    public DataTable datMaster;
    public DataTable datVatcation;

    protected void Page_Load(object sender, EventArgs e)
    {
        ReportViewer1.Visible = false;
        btnPrint1.Enabled = false;
        this.Title = GetGlobalResourceObject("ReportsCommon","PaySlipReport").ToString();
        if (!IsPostBack)
        {
            FillCombos();
           
            objCommon = new clsCommon();
            txtCurrentMonth.Text = objCommon.GetSysDate();
            txtToDate_TextChanged(null, null);
        }

        if (Request.QueryString["EmpId"] != null)
        {
            int EmployeeID = Request.QueryString["EmpId"].ToInt32();
            int CompanyID = clsRptEmployees.GetCompanyIDByEmployee(EmployeeID);

           // ddlCompany.SelectedValue = CompanyID.ToString();
            FillAllEmployees();

            ddlEmployee.SelectedValue = EmployeeID.ToString();

            ddlEmployee.Enabled = false;   
        }
    }
    private void FillCombos()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataSource = clsRptEmployees.GetCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();

        FillAllEmployees();
    }

    #region BindDropDown
    private void BindDropDown(DropDownList ddl, string sDataTextField, string sDataValueField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    #endregion


    private void FillAllEmployees()
    {
        int CompanyID = Convert.ToInt32(new clsUserMaster().GetCompanyId()); //Convert.ToInt32(ddlCompany.SelectedValue);

        ddlEmployee.DataSource = clsRptEmployees.GetEmployees(CompanyID);
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        int iMonth = txtCurrentMonth.Text.ToDateTime().Date.Month;
        int iYear = txtCurrentMonth.Text.ToDateTime().Date.Year;
        
        txtCurrentMonth.Text = GetCurrentMonthStr(iMonth) + ' ' + Convert.ToString(iYear);
    }
    private string GetCurrentMonthStr(int MonthVal)
    {
        string Months;
        Months = "";

        switch (MonthVal)
        {
            case 1:
                Months = "Jan";
                break;
            case 2:
                Months = "Feb";
                break;
            case 3:
                Months = "Mar";
                break;
            case 4:
                Months = "Apr";
                break;
            case 5:
                Months = "May";
                break;
            case 6:
                Months = "Jun";
                break;
            case 7:
                Months = "Jul";
                break;
            case 8:
                Months = "Aug";
                break;
            case 9:
                Months = "Sep";
                break;
            case 10:
                Months = "Oct";
                break;
            case 11:
                Months = "Nov";
                break;
            case 12:
                Months = "Dec";
                break;

        }


        return Months;
    }

    protected void btnPrint_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
            ShowReport();
    }
    public void OneSubReportProcessingEventHandlerOT(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", (DataTable)ViewState["MdatOTDed"]));
    }
    public void OneSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", (DataTable)ViewState["MdatLeaveDed"]));
    }
    public void SubReportProcessingEventHandlerVacation(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDtls", (DataTable)ViewState["datVatcation"]));
    }
    //public void TwoSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    //{
    //    e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", (DataTable)ViewState["MdatHourDetail"]));
    //}
    public void ThreeSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", (DataTable)ViewState["MdatSalarySlipWorkInfo"]));
    }
    public void SubReportCompanyDisplayEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", (DataTable)ViewState["MdatMaster"]));
    }

    private bool ValidateDate()
    {
        DateTime dtDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;

        if (txtCurrentMonth.Text == string.Empty)
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectDate").ToString();
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtCurrentMonth.Text, df, DateTimeStyles.None, out dtDate))
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate").ToString();
            blnIsValid = false;
        }

        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }

        return true;
    }

    private void ShowReport()
    {
        this.ReportViewer1.Reset();
        ReportViewer1.Visible = true;

        this.ReportViewer1.LocalReport.DataSources.Clear();
        string MsReportPath = "";
        objEmployee = new clsMISEmployeeReport();

        int intYearInt = 0;
        int intMonthInt = 0;
        int intCompanyID = 0;
        int intDepartmentID = 0;
        int intDesignationID = 0;
        int intEmployeeID = 0;

        MsReportPath = clsGlobalization.IsArabicCulture()? Server.MapPath("reports/RptSalarySlipArb.rdlc"): Server.MapPath("reports/RptSalarySlip.rdlc");

        this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;

        intYearInt = Convert.ToInt32(txtCurrentMonth.Text.ToDateTime().Date.Year);
        intMonthInt = Convert.ToInt32(txtCurrentMonth.Text.ToDateTime().Date.Month);
        intCompanyID = Convert.ToInt32(new clsUserMaster().GetCompanyId());// Convert.ToInt32(ddlCompany.SelectedValue);

        if (ddlEmployee.SelectedIndex != -1)
        {
            intEmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
        }

        string pPayID = objEmployee.GetPaymentIDList(intYearInt, intMonthInt, intCompanyID, intDepartmentID, intDesignationID, intEmployeeID,-1);

        string strPayID = pPayID;
        int intCompanyPayFlag = 1;
        int intDivisionID = 0;
        intMonthInt = txtCurrentMonth.Text.ToDateTime().Date.Month;
        intYearInt = txtCurrentMonth.Text.ToDateTime().Date.Year;
        intCompanyID = Convert.ToInt32(new clsUserMaster().GetCompanyId()); //Convert.ToInt32(ddlCompany.SelectedValue);

        DataTable MdatMaster = objEmployee.datMaster(strPayID, intCompanyPayFlag, intDivisionID);
        if (MdatMaster == null)
        {
            return;
        }
        if (MdatMaster.Rows.Count <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
            ReportViewer1.Visible = false;
            return;
        }
        Cursor.Current = Cursors.WaitCursor;
        ViewState["MdatMaster"] = datMaster = objEmployee.datMaster(strPayID, intCompanyPayFlag, intDivisionID);
        ViewState["datDetail"] = datDetail = objEmployee.datDetail(strPayID, intMonthInt, intYearInt);
        ViewState["MdatLeaveDed"] = MdatLeaveDed = objEmployee.MdatLeaveDed(strPayID);
        ViewState["datDetailDed"] = datDetailDed = objEmployee.datDetailDed(strPayID, intMonthInt, intYearInt);
        ViewState["MdatOTDed"] = MdatOTDed = objEmployee.MdatOTDed(strPayID);
        //ViewState["MdatHourDetail"] = MdatHourDetail = objEmployee.MdatHourDetail(strPayID, intMonthInt, intYearInt);
        ViewState["MdatSalarySlipWorkInfo"] = MdatSalarySlipWorkInfo = objEmployee.MdatSalarySlipWorkInfo(strPayID);
        ViewState["datVatcation"] = datVatcation = objEmployee.MdatVacationDtls(strPayID);
        bool GlDisplayLeaveSummaryInSalarySlip = true;
        bool PbSummary = GlDisplayLeaveSummaryInSalarySlip;

        //this.ReportViewer1.LocalReport.DataSources.Clear();
        this.ReportViewer1.LocalReport.ReportPath = MsReportPath;

        if (datMaster.Rows.Count > 0)
        {
            btnPrint1.Enabled = true;
        }
        else
        {
            btnPrint1.Enabled = false;
        }
        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
        //this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
        //this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);
        this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandlerVacation);


        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
        //this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
        //this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);
        this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandlerVacation);


        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", datMaster));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetail", datDetail));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetailDed", datDetailDed));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDtls", datVatcation));
        //this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
        //this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));

        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                {
                  new ReportParameter("GeneratedBy",  new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                new ReportParameter("SelRowCount",  PbSummary.ToString()),
                new ReportParameter("bSummary", Convert.ToString(new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.DisplayLeaveSummaryInSalarySlip)).ToUpper().Trim()=="NO" ?"false":"true"),
                new ReportParameter("bMonthly",  "HRPOWER")
                });
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        Response.Redirect("../Public/EmployeeProfile.aspx?EmpId=" + ViewState["EmployeeID"].ToInt32());
    }
}