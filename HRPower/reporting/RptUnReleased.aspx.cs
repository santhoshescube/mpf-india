﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Reporting.WebForms;
using System.Globalization;

public partial class reporting_RptUnReleased : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "PaymentReport").ToString();
        btnPrint1.Enabled = false;
        ReportViewer.Visible = false;
        if (!IsPostBack)
        {
            //Fill all the combos
            FillCombos();
         
            chkIncludeCompany.Checked = true;
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");

        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

    }

    private void FillCombos()
    {
        FillAllCompanies();
        FillAllDepartments();
        FillPaymentTypes();
        FillDesignation();
        FillWorkStatus();
        FillAllTransactionTypes();

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        //FillAllEmployees();
        // FillPaymentMonthYear();

    }

    private void FillAllCompanies()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }



    private void FillAllTransactionTypes()
    {
        ddlTransactionType.DataTextField = "TransactionType";
        ddlTransactionType.DataValueField = "TransactionTypeID";
        ddlTransactionType.DataSource = clsReportCommon.GetAllTransactionTypes();
        ddlTransactionType.DataBind();
    }

    private void FillAllBranch()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        ddlBranch.SelectedValue = "0";

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllDepartments()
    {
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataBind();
    }

    private void FillAllEmployees()
    {
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                                    ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), -1, ddlWorkStatus.SelectedValue.ToInt32(), -1);

        ddlEmployee.DataBind();

    
    }

    private void FillPaymentTypes()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("TypeID");
        dt.Columns.Add("Type");

        dr = dt.NewRow();
        dr["TypeID"] = 0;
        dr["Type"] = "Monthly";
        dt.Rows.InsertAt(dr, 0);


        dr = dt.NewRow();
        dr["TypeID"] = 1;
        dr["Type"] = "Summary";
        dt.Rows.InsertAt(dr, 1);

    }

  
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    private void FillDesignation()
    {
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationID";
        ddlDesignation.DataSource = clsReportCommon.GetAllDesignation();
        ddlDesignation.DataBind();
    }

    private void FillWorkStatus()
    {
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString();
    }


    #region"Filling Drop Down List"


    private bool ValidateDate()
    {
      
        return true;
    }


    private void ShowMessage(string message)
    {
        ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + message + "');", true);

    }

    private bool ValidateReport()
    {

        DateTime dtDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();
        df.ShortDatePattern = "dd-MMM-yyyy";



        //if (ddlType.SelectedIndex == 0 && ddlMonthAndYear.Items.Count == 0)
        //{
        //    //ShowMessage("Please select a month");

        //    ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectAMonth").ToString());
        //    return false;
        //}
        //else if (ddlType.SelectedIndex == 1)
        //{

        if (txtFromDate.Text == "")
        {
            //ShowMessage("Please enter from date");
            ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate").ToString());
            return false;
        }
        else if (txtToDate.Text == "")
        {
            // ShowMessage("Please enter to date");
            ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate").ToString());
            return false;
        }

        else if (!(DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtDate)))
        {
            //ShowMessage("Invalid from date");
            ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidFromDate").ToString());
            return false;
        }
        else if (!(DateTime.TryParse(txtToDate.Text, out dtDate)))
        {
            //ShowMessage("Invalid to date");
            ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidToDate").ToString());
            return false;
        }
        //}
        return true;
    }


    private void ShowReport()
    {
        try
        {
            ReportViewer.Visible = false;
          
            DataTable DtPayments = null;
            string strMReportPath = "";

            //setting the report path based on the payment type value

            strMReportPath = (clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptEmployeeProcessPaymentArb.rdlc") : Server.MapPath("reports/RptEmployeeProcessPayment.rdlc"));

            //Setting the report header 

            ReportViewer.Reset();
            ReportViewer.LocalReport.DataSources.Clear();
            ReportViewer.LocalReport.ReportPath = strMReportPath;

            //Monthly Report
           

                ReportViewer.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false),
                    new ReportParameter("CompanyName", ddlCompany.SelectedItem.Text , false),
                    new ReportParameter("BranchName",ddlBranch.SelectedItem.Text , false),
                    new ReportParameter("Department", ddlDepartment.SelectedItem.Text , false),
                    new ReportParameter("EmployeeName",ddlEmployee.SelectedItem.Text, false),
                    new ReportParameter("MonthAndYear",  "" , false),
                    new ReportParameter("Designation",ddlDesignation.SelectedItem.Text, false),
                    new ReportParameter("WorkStatus",  ddlWorkStatus.SelectedItem.Text , false)

                });

                DtPayments = clsRptPayments.GetMonthlyPaymentsProcessed(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), 0, 0, chkIncludeCompany.Checked, ddlTransactionType.SelectedValue.ToInt32(), chkIsPayStructureOnly.Checked, txtFromDate.Text, txtToDate.Text, chkTransfer.Checked ? 1:0);
                
            
                if (DtPayments.Rows.Count > 0)
                {
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetPayments_EmployeeSalarySummary", DtPayments));
                    btnPrint1.Enabled = true;
                }
           

            ViewState["RecordCount"] = DtPayments.Rows.Count;
            //Show no record found
            if (DtPayments.Rows.Count == 0)
            {
                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لعثور على أي معلومات") : ("No Information Found");             
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
                return;
            }

            ReportViewer.LocalReport.Refresh();

            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;

            ReportViewer.Visible = true;
            //Set the report display mode and zoom percentage
            // SetReportDisplay(ReportViewer);

        }
        catch (Exception ex)
        {

        }
        // upd.Update();
    }





    #endregion
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();
    }
  
   
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {

        if (ValidateReport())
            ShowReport();

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer, this);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    

    protected void ReportViewer_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer.Visible = true;
    }
}
