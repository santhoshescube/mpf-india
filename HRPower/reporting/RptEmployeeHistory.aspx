﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true"
    CodeFile="RptEmployeeHistory.aspx.cs" Inherits="reporting_RptEmployeeHistory"
    Title="Employee History" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    
        $(document).ready(function() {
             var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {
                
                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium'); 
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');
            });
        });
    </script>

    <div style="padding-left: 350px;">
        <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,EmployeeHistory%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span>
            </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="filters" width="100%">
                    <tr>
                        <td valign="bottom">
                            <table id="tblEmployee" runat="server" class="labeltext" width="100%">
                                <tr>
                                    <td class="style1">
                                        <%-- Company--%>
                                        <asp:Literal ID="Lit" runat="server" Text='<%$Resources:ReportsCommon,Company %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="150px"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%-- Department--%>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Designation %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style2">
                                        <%-- Branch--%>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'>
                                        </asp:Literal>
                                    </td>
                                    <td class="style3">
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" />
                                    </td>
                                    <td class="style3">
                                        <%--Designation--%>
                                        Work Status</td>
                                    <td class="style3">
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" CssClass="dropdownlist" 
                                            Width="150px" AutoPostBack="true"  onselectedindexchanged="ddlWorkStatus_SelectedIndexChanged" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style1">
                                        <%-- Include Company --%>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,Department %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>                                        
                                        <asp:DropDownList AutoPostBack="true" ID="ddlDepartment" runat="server" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                    <td>
                                        <%--   Employee--%>
                                        <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist" Width="150px" />
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="style1">
                                        <asp:CheckBox ID="chk" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                            AutoPostBack="true" OnCheckedChanged="chk_CheckedChanged" />
                                    </td>
                                    <td>                                        
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>                                
                            </table>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                            Text='<%$Resources:ReportsCommon,Go %>' OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ControlsCommon,Print %>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
        <rsweb:ReportViewer ID="rvEmployeeProfile" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" OnPageNavigation="rvEmployeeProfile_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="head">

    <style type="text/css">
        .style1
        {
            width: 182px;
        }
        .style2
        {
            width: 182px;
            height: 29px;
        }
        .style3
        {
            height: 29px;
        }
    </style>

</asp:Content>

