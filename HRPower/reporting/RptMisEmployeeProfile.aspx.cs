﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class reporting_RptMisEmployeeProfile : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private int intCompanyID;
    private int intBranchID;
    protected void Page_init(object sender, EventArgs e)
    {
       
     
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.FillCompanyDropDown();
            this.chkIncludeCompany.Checked = true;
            //CrystalReportViewer1.Visible = false;
        }
     

    }
    private void SetIframeSource()
    {
        this.iframeReport.Attributes.Add("src", string.Format("RptEmployeeProfileSummary.aspx?cid={0}&bid={1}&eid={2}&ic={3}",
                                                    ddlCompany.Items.Count == 0 ? "0" : ddlCompany.SelectedValue,
                                                    ddlBranch.Items.Count == 0 ? "0" : ddlBranch.SelectedValue,
                                                    ddlEmployee.Items.Count == 0 ? "0" : ddlEmployee.SelectedValue,
                                                    chkIncludeCompany.Checked
                                                   ));
    }

    private void FillReportDataSource()
    {
        //try
        //{
        //DataSet ds;
        //int intIncludeCompany = 0;
        //if (chkIncludeCompany.Checked == true)
        //{
        //    intIncludeCompany = 1;
        //}
        //objEmployee = new clsMISEmployeeReport();
        //using (ds = new DataSet())
        //{
        //    DataTable dtEmployee = new DataTable();
        //    dtEmployee = objEmployee.GetEmployeeProfile(Convert.ToInt32(ddlCompany.SelectedValue), Convert.ToInt32(ddlBranch.SelectedValue), Convert.ToInt32(ddlEmployee.SelectedValue), intIncludeCompany);
        //    dtEmployee.TableName = "DtEmployeeProfile";

        //    DataTable dtCompany = new DataTable();
        //    dtCompany = objEmployee.GetCompanyHeader(Convert.ToInt32(ddlCompany.SelectedValue));
        //    dtCompany.TableName = "ReportHeader";
        //    ds.Tables.Add(dtEmployee.Copy());
        //    ds.Tables.Add(dtCompany.Copy());
        //}

        //    ViewState["dsEmployee"] = ds;
        //    rptDoc.SetDataSource(ds);

        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        CrystalReportViewer1.Visible = true;
        //        CrystalReportViewer1.ShowFirstPage();
        //    }
        //    else
        //    {
        //        CrystalReportViewer1.Visible = false;// Show No record s found error message
        //        this.Page.ClientScript.RegisterClientScriptBlock(this.Page.GetType(), Guid.NewGuid().ToString(), "alert(\"No Information found\")", true);
        //    }

        //}
        //finally
        //{
        //    objEmployee = null;
        //}
      
    }


    #region Fill DropDown Lists
    private void FillCompanyDropDown()
    {
        this.BindDropDown(ddlCompany, "CompanyID", "Name", new clsMISEmployeeReport().FillCompany());
        FillBranchDropDown();
    }
    private void FillBranchDropDown()
    {
        intCompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        if (intCompanyID > 0)
            this.BindDropDown(ddlBranch, "CompanyID", "Name", new clsMISEmployeeReport().FillBranchs(intCompanyID, 42));

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs()); 
        FillEmployeeDropDown();
    }
    private void FillEmployeeDropDown()
    {
        intCompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        if (ddlBranch.Items.Count > 0)
            intBranchID = Convert.ToInt32(ddlBranch.SelectedValue);


        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", new clsMISEmployeeReport().FillEmployee(intCompanyID, intBranchID, 43));
    }

    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    #endregion

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBranchDropDown();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (Convert.ToInt32(ddlBranch.SelectedValue) != 0)
        //{
        //    chkIncludeCompany.Enabled = true;
        //}
        //else
        //{
        //    chkIncludeCompany.Checked = true;
        //    chkIncludeCompany.Enabled = false;
        //}
        FillEmployeeDropDown();

        EnableDisableIncludeCompany();
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
       // FillReportDataSource();
        SetIframeSource();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //FillReportDataSource();
        SetIframeSource();
    }
}
