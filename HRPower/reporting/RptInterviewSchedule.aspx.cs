﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using Microsoft.Reporting.WebForms;

/* Created By   : Sruthy K
 * Created Date : Oct 17 2013
 * Purpose      : Interview Schedule Report
 * */

public partial class reporting_RptInterviewSchedule : System.Web.UI.Page
{
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "InterviewScheduleReport").ToString();

        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            chkIncludeCompany.Enabled = false;
            FillDropDown(0);
            FillDropDown(1);
            ddlBranch.SelectedValue = "0";
            FillDropDown(2);
            FillDropDown(3);
            rvInterviewSchedule.Visible = false;
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
        }
    }
    #endregion

    #region Functions
    private void FillDropDown(int intType)
    {
        DataTable dtCombo;
        clsUserMaster objUser = new clsUserMaster();
        if (intType == 0)
        {
            dtCombo = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
            this.BindDropDown(dtCombo, "Company", "CompanyID", ddlCompany);
        }
        else if (intType == 1)
        {
            dtCombo = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
            this.BindDropDown(dtCombo, "Branch", "BranchID", ddlBranch);
            ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        }
        else if (intType == 2)
        {
            dtCombo = clsRptCandidates.FillcandidateJobs(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, -1);
            this.BindDropDown(dtCombo, "JobTitle", "JobID", ddlJobTitle);
        }
        else if (intType == 3)
        {
            dtCombo = clsRptInterviewSchedule.FillJobLevel(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlJobTitle.SelectedValue.ToInt32());
            this.BindDropDown(dtCombo, "JobLevel", "JobLevelId", ddlJobLevel);
        }

    }

    private void BindDropDown(DataTable datResult, string strTextField, string strValueField, DropDownList ddlTarget)
    {
        ddlTarget.DataSource = datResult;
        ddlTarget.DataTextField = strTextField;
        ddlTarget.DataValueField = strValueField;
        ddlTarget.DataBind();
    }

    private bool ValidateDate()
    {
        DateTime dtpFromDate = System.DateTime.MinValue;
        DateTime dtpToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = false;

        string strMessage = string.Empty;

        if (txtFromDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate")); //"Please Select From Date";
        }
        else if (txtToDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
        }
        else if (txtFromDate.Text != string.Empty && !DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtpFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
        }
        else if (txtToDate.Text != string.Empty && !DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtpToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
        }
        else if (txtFromDate.Text.ToDateTime() > txtToDate.Text.ToDateTime())
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
        }
        else
            blnIsValid = true;

        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('"+strMessage+"');", true);
            return false;
        }
        return true;
    }

    private void ShowReport()
    {
        try
        {
            DataTable dtReport = clsRptInterviewSchedule.GetInterviewScheduleReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlJobTitle.SelectedValue.ToInt32(),ddlJobLevel.SelectedValue.ToInt32(), clsCommon.Convert2DateTime(txtFromDate.Text), clsCommon.Convert2DateTime(txtToDate.Text));
            DataTable dtDetails = new DataTable();

            ViewState["RecordCount"] = dtReport.Rows.Count;
            if (dtReport.Rows.Count > 0)
            {
                btnPrint.Enabled = true;
                rvInterviewSchedule.Visible = true;
            }
            else
            {
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
                rvInterviewSchedule.Visible = false;
            }


            rvInterviewSchedule.LocalReport.ReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/rptInterviewScheduleArb.rdlc" : "reports/rptInterviewSchedule.rdlc");

            if (ddlJobTitle.SelectedValue.ToInt32() > 0)
            {
                dtDetails = clsRptInterviewSchedule.GetDetailedReport(ddlJobTitle.SelectedValue.ToInt32(), ddlJobLevel.SelectedValue.ToInt32(), clsCommon.Convert2DateTime(txtFromDate.Text), clsCommon.Convert2DateTime(txtToDate.Text));
            }

            rvInterviewSchedule.LocalReport.DataSources.Clear();

            rvInterviewSchedule.LocalReport.SetParameters(new ReportParameter[] { new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),

                new ReportParameter("JobID",ddlJobTitle.SelectedValue) });
            rvInterviewSchedule.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
            rvInterviewSchedule.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsCandidate_dtInterviewSchedule", dtReport));
            rvInterviewSchedule.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsCandidate_dtInterviewScheduleDetails", dtDetails));

            this.rvInterviewSchedule.ZoomMode = ZoomMode.Percent;
            this.rvInterviewSchedule.ZoomPercent = 100;
        }
        catch
        {
        }
    }

    #endregion

    #region Events
    protected void btnGo_Click(object sender, EventArgs e)
    {
        rvInterviewSchedule.Reset();

        if (ValidateDate())
        {
            ShowReport();
        }
    }

    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(rvInterviewSchedule, this);
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDropDown(1);
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDropDown(2);
        FillDropDown(3);
        //if (ddlBranch.SelectedValue == "0")
        //{
        //    chkIncludeCompany.Checked = true;
        //    chkIncludeCompany.Enabled = false;
        //}
        //else
        //{
        //    chkIncludeCompany.Enabled = true;
        //}

        EnableDisableIncludeCompany();
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void ddlJobTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillDropDown(3);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillDropDown(2);
        FillDropDown(3);
    }
    #endregion
    protected void rvInterviewSchedule_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        rvInterviewSchedule.Visible = true;
    }
}
