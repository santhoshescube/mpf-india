﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using Microsoft.Reporting.WebForms;

public partial class reporting_RptCommonEss : System.Web.UI.Page
{
    private int intCompany = 0;
    private int intBranch = 0;
    private int intEmployee = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "ESSReport").ToString();
        ReportViewer1.Visible = false;
    

        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            txtStartMonth.Text = clsReportCommon.GetMonthStartDate();
            txtEndMonth.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            FillCombos();
            ddlBranch.SelectedValue = "-1";
            EnableDisableIncludeCompany();
        }
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
       
        //ltrlEmployee.Visible =  ddlEmployee.Visible = ddlEssType.SelectedValue.ToInt32() == 7;
     
    }

    private void FillCombos()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());// clsRptEmployees.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();


        //if (ddlCompany.Items.Count > 1)
        //    ddlCompany.Items.RemoveAt(0);
        FillEssType();
        FillTypesByEssID();
        FillAllStatus();
        FillAllBranches();
        FillEmployees();
        FillWorkStatus();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
    }
    private void FillEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue == "" ? "0" : ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked,-1,-1, -1, ddlWorkStatus.SelectedValue.ToInt32(), -1
                                ));
    }

    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllBranches()
    {
        int CompanyID = ddlCompany.SelectedValue.ToInt32();
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllStatus()
    {
        ddlStatus.DataSource = clsRptEmployees.GetAllStatus();
        ddlStatus.DataTextField = "Status";
        ddlStatus.DataValueField = "StatusID";
        ddlStatus.DataBind();
    }

    private void FillEssType()
    {
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Leave")), Value = "1" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "SalaryAdvance")), Value = "2" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Loan")), Value = "3" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Transfer")), Value = "4" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Document")), Value = "5" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Expense")), Value = "6" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Travel")), Value = "7" });
    }

  
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }

    //private void EnableDisableIncludeCompany()
    //{
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Enabled = false;
    //        chkIncludeCompany.Checked = true;
    //    }
    //    else
    //        chkIncludeCompany.Enabled = true;
    //}



    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    protected void ddlEssType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTypesByEssID();
    }


    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }


    private void FillTypesByEssID()
    {
        int EssTypeID = ddlEssType.SelectedValue.ToInt32();

        DataTable dt = clsRptEmployees.GetEssType(EssTypeID);
        DataRow dr = dt.NewRow();
        dr["TypeID"] = "-2";
        dr["Type"] = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Vacation"));
        dt.Rows.InsertAt(dr, 0);
        dr = dt.NewRow();
        dr["TypeID"] = "-1";
        dr["Type"] = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Any"));
        dt.Rows.InsertAt(dr, 0);
       
        ddlType.DataSource = dt;
        ddlType.DataBind();

        ddlType.Enabled = true;
        switch (EssTypeID)
        {
            case 1:
                lblType.Text =Convert.ToString( GetGlobalResourceObject("ReportsCommon", "LeaveType"));
                break;

            case 3:

                lblType.Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Loan")); //"Loan";
                break;

            case 4:
                lblType.Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "TransferType"));//"Transfer Type";
                break;

            case 5:
                lblType.Text =Convert.ToString( GetGlobalResourceObject("ReportsCommon", "DocumentType"));//"Document Type";
                break;
            //case 7:
            //    lblType.Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Travel"));//"Document Type";
               
            //    break;

            default:
                lblType.Text =Convert.ToString( GetGlobalResourceObject("ReportsCommon", "Type"));//"Type";
                ddlType.Enabled = false;
                break;
        }

    }

    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }


    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtStartMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString( GetGlobalResourceObject("ReportsCommon","PleaseSelectFromDate"));
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtStartMonth.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtEndMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtEndMonth.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }       


        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            DataTable dtCompany = null;
            DataTable dt = null;
            string path = "";

            if (txtStartMonth.Text != "")
                FromDate = Convert.ToDateTime(txtStartMonth.Text, System.Globalization.CultureInfo.InvariantCulture);

            if (txtEndMonth.Text != "")
                ToDate = Convert.ToDateTime(txtEndMonth.Text, System.Globalization.CultureInfo.InvariantCulture);

            if (ToDate < FromDate)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" +Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate"))  + "');", true);
                return;
            }
            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }
            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

            switch (ddlEssType.SelectedValue.ToInt32())
            {
                case 1:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSLeaveArb.rdlc" : "reports/RptESSLeave.rdlc");
                    dt = clsEmployeeESS.GetEmployeeLeaveRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlType.SelectedValue.ToInt32(), ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtEssLeave", dt));
                    break;
                case 2:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSSalaryArb.rdlc" : "reports/RptESSSalary.rdlc");
                    dt = clsEmployeeESS.GetSalaryRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate,ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtSalaryAdvance", dt));
                    break;
                case 3:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSLoanArb.rdlc" : "reports/RptESSLoan.rdlc");
                    dt = clsEmployeeESS.GetLoanRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlType.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtEssLoan", dt));
                    break;
                case 4:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSTransferArb.rdlc" : "reports/RptESSTransfer.rdlc");
                    dt = clsEmployeeESS.GetTransferRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlType.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtEssTransfer", dt));
                    break;
                case 5:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSDocumentsArb.rdlc" : "reports/RptESSDocuments.rdlc");
                    dt = clsEmployeeESS.GetDocumentRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlType.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtEssDocument", dt));
                    break;
                case 6:
                    path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSexpenseArb.rdlc" : "reports/RptESSexpense.rdlc");
                    dt = clsEmployeeESS.GetExpenseRequestESS(ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtExpense", dt));
                    break;
                case 7:
                    if (ddlEmployee.SelectedValue.ToInt64() < 0)
                        path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSTravelArb.rdlc" : "reports/RptESSTravelAll.rdlc");
                    
                    else
                        path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptESSTravelArb.rdlc" : "reports/RptESSTravel.rdlc");
                    
                    dt = clsEmployeeESS.GetTravelRequestESS(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlWorkStatus.SelectedValue.ToInt32());
                    ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEmployeeEss_dtEssTravel", dt));
                    break;
                default:
                    break;
            }
            ViewState["RecordCount"] = dt.Rows.Count;
            if (dt.Rows.Count > 0)
            {
                ReportViewer1.LocalReport.ReportPath = path;

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
				{
					new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter),false)
				});
                btnPrint.Enabled = true;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            else
            {
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
            }
        }
        catch(Exception ex)
        {
        }
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true; 
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
}