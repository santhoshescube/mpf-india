﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Reporting.WebForms;
using System.Globalization;  

public partial class reporting_RptSalaryAdvance : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private int intCompany;
    private int intBranch;
    protected void Page_Load(object sender, EventArgs e)
    {
        ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            FillCompanyDropDown();
            FillLocationDropDown();
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            chkIncludeCompany.Checked = true;
        }
    }
    #region "Fill DropDown List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillCompanyDropDown()
    {
        this.BindDropDown(ddlCompany, "CompanyID", "Name", new clsMISEmployeeReport().FillCompany());
        ddlCompany.SelectedIndex = 0;
        FillBranchDropDown();
    }
    private void FillBranchDropDown()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "CompanyID", "Name", new clsMISEmployeeReport().FillBranchs(intCompany, 42));
        FillEmployeeDropDown();
        ddlBranch.SelectedIndex = 1;


        ddlBranch_SelectedIndexChanged(new object(), new EventArgs()); 
    }
    private void FillEmployeeDropDown()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", new clsMISEmployeeReport().FillEmployee(intCompany, intBranch, 30));
        // ddlEmployee.SelectedValue = "-1";
    }
    private void FillLocationDropDown()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue);
        this.BindDropDown(ddlLocation, "WorkLocationID", "LocationName", (clsRptEmployees.GetALLLocation()));
        // ddlEmployee.SelectedValue = "-1";
    }
    #endregion
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBranchDropDown();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        //chkIncludeCompany.Checked = true;
        //chkIncludeCompany.Enabled = Convert.ToInt32(ddlBranch.SelectedValue) != -2;
        FillEmployeeDropDown();

        EnableDisableIncludeCompany();
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowSalaryAdvanceReport();
        }
    }

    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;

        if (txtFromDate.Text == string.Empty )
        {
            strMessage = "Please Select From Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = "Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtToDate.Text == string.Empty)
        {
            strMessage = "Please Select To Date";
            blnIsValid = false;
        }
        else if(!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = "Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtFromDate.Text.ToDateTime() > txtToDate.Text.ToDateTime())
        {
            strMessage = "Please Select Valid Date";
            blnIsValid = false;
        }

        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('"+strMessage+"');", true);
            return false;
        }

        return true;
    }
    private bool ShowSalaryAdvanceReport()
    {
        try
        {
            ReportViewer1.Visible = true;
            bool blnRetValue = false;
            int intCompanyID = ddlCompany.SelectedValue.ToInt32();
            int intEmployeeID = ddlEmployee.SelectedValue.ToInt32();
            int intBranchID = 0;
            int intIncludeCompany = chkIncludeCompany.Checked ? 1 : 0;

            string strMReportPath = "";
            objEmployee = new clsMISEmployeeReport();

            if (ddlBranch.SelectedValue.ToInt32() == -2)
                intBranchID = 0;
            else
                intBranchID = ddlBranch.SelectedValue.ToInt32();

            string strFromDate = txtFromDate.Text.ToDateTime().ToString("dd-MMM-yyyy");
            string strToDate = txtToDate.Text.ToDateTime().ToString("dd-MMM-yyyy");
            int intLocationID = ddlLocation.SelectedValue.ToInt32();

            DataTable dtSalaryAdvance = null;
            DataTable dtCompanyDetails = null;
            this.ReportViewer1.Reset();
            if (ddlType.SelectedIndex == 0)//SalaryAdvance Report 
            {

                dtSalaryAdvance = objEmployee.GetSalaryAdvanceSummary(intCompanyID, intBranchID, intEmployeeID, intIncludeCompany, strFromDate, strToDate, intLocationID);
                strMReportPath = Server.MapPath("reports/RptSalaryAdvanceSummary.rdlc");
                this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
                this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
                this.ReportViewer1.LocalReport.DataSources.Clear();

            }

            if (chkIncludeCompany.Checked)
            {
                dtCompanyDetails = objEmployee.GetCompanyHeader(intCompanyID);
            }
            else
            {
                if (intBranchID > 0)
                {
                    dtCompanyDetails = objEmployee.GetCompanyHeader(intBranchID);
                }
                else
                {
                    dtCompanyDetails = objEmployee.GetCompanyHeader(intCompanyID);
                }
            }


            if (dtSalaryAdvance != null)
            {
                if (dtSalaryAdvance.Rows.Count > 0)
                {
                    btnPrint1.Enabled = true;
                    ReportViewer1.Visible = true;
                    blnRetValue = true;
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_SalaryAdvanceReport", dtSalaryAdvance));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetSalaryAdvanceSummary_CompanyDetails", dtCompanyDetails));

                }
                else
                {
                    btnPrint1.Enabled = false;
                    ReportViewer1.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
                }

            }
            else
            {

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
                //return;

            }
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                {
                  new ReportParameter("GeneratedBy",  "HRPOWER")
                
                });
            return blnRetValue;
        }
        catch (Exception ex)
        {
            return false;
        }


        
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
}
