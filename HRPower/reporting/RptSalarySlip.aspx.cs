﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;

public partial class reporting_RptSalarySlip : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private int intCompany = 0;
    private int intBranch = 0;
    private int intEmployee = 0;
    private clsCommon objCommon;
    public DataTable MdatLeaveDed;
    public DataTable MdatOTDed;
    public DataTable MdatHourDetail;
    public DataTable MdatConsequenceList;
    public DataTable MdatSalarySlipWorkInfo;
    public DataTable MdatMaster;
    public DataTable datDetailDed;
    public DataTable datDetail;
    public DataTable datMaster;
    public DataTable datVatcation;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "PaySlipReport").ToString();
        ReportViewer1.Visible = false;
        btnPrint1.Enabled = false;
        if (!IsPostBack)
        {
            FillCombos();
            objCommon = new clsCommon();
            txtCurrentMonth.Text = objCommon.GetSysDate();
            txtToDate_TextChanged(null, null);
        }
        if (Request.QueryString["EmpId"] != null)
        {
            int EmployeeID = Request.QueryString["EmpId"].ToInt32();
            int CompanyID = clsRptEmployees.GetCompanyIDByEmployee(EmployeeID);
            ddlCompany.SelectedValue = CompanyID.ToString();
            //FillAllEmployees();
            ddlEmployee.SelectedValue = EmployeeID.ToString();
            ddlCompany.Enabled = ddlDesignation.Enabled = ddlDepartment.Enabled = ddlEmployee.Enabled = false;   
        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
    }
    private void FillCombos()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataSource = clsRptEmployees.GetCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs()); 
        ddlDepartment.DataSource = clsRptEmployees.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();

        ddlDesignation.DataSource = clsRptEmployees.GetAllDesignation();
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationID";
        ddlDesignation.DataBind();

        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();

    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        int iMonth = txtCurrentMonth.Text.ToDateTime().Date.Month;
        int iYear = txtCurrentMonth.Text.ToDateTime().Date.Year;
        // txtCurrentMonth.Text = Convert.ToDateTime("01-" + Convert.ToString(GetCurrentMonthStr(iMonth)) + "-" + Convert.ToString(iYear) + "").ToString();
        txtCurrentMonth.Text = GetCurrentMonthStr(iMonth) + ' ' + Convert.ToString(iYear);
    }
    private string GetCurrentMonthStr(int MonthVal)
    {
        string Months;
        Months = "";

        switch (MonthVal)
        {
            case 1:
                Months = "Jan";
                break;
            case 2:
                Months = "Feb";
                break;
            case 3:
                Months = "Mar";
                break;
            case 4:
                Months = "Apr";
                break;
            case 5:
                Months = "May";
                break;
            case 6:
                Months = "Jun";
                break;
            case 7:
                Months = "Jul";
                break;
            case 8:
                Months = "Aug";
                break;
            case 9:
                Months = "Sep";
                break;
            case 10:
                Months = "Oct";
                break;
            case 11:
                Months = "Nov";
                break;
            case 12:
                Months = "Dec";
                break;
        }
        return Months;
    }
    protected void btnPrint_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
            ShowReport();
    }
    public void OneSubReportProcessingEventHandlerOT(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", (DataTable)ViewState["MdatOTDed"]));
    }
    public void OneSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", (DataTable)ViewState["MdatLeaveDed"]));
    }
    public void SubReportProcessingEventHandlerVacation(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDtls", (DataTable)ViewState["datVatcation"]));
    }
    //public void TwoSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    //{
    //    e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", (DataTable)ViewState["MdatHourDetail"]));
    //}
    public void ThreeSubReportProcessingEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", (DataTable)ViewState["MdatSalarySlipWorkInfo"]));
    }
    public void SubReportCompanyDisplayEventHandler(Object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", (DataTable)ViewState["MdatMaster"]));
    }
    private bool ValidateDate()
    {
        DateTime dtDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();
        df.ShortDatePattern = "dd MMM yyyy";
        bool blnIsValid = true;
        string strMessage = string.Empty;
        if (txtCurrentMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectDate")); //"Please Select Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtCurrentMonth.Text, df, DateTimeStyles.None, out dtDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));  //"Please Select Valid Date";
            blnIsValid = false;
        }
        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            this.ReportViewer1.Reset();
            ReportViewer1.Visible = true;

            this.ReportViewer1.LocalReport.DataSources.Clear();
            string MsReportPath = "";
            objEmployee = new clsMISEmployeeReport();

            int intYearInt = 0;
            int intMonthInt = 0;
            int intCompanyID = 0;
            int intDepartmentID = 0;
            int intDesignationID = 0;
            int intEmployeeID = 0;
            int intWorkStatusID = 0;

            MsReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptSalarySlipArb.rdlc" : "reports/RptSalarySlip.rdlc");
            this.ReportViewer1.ProcessingMode = ProcessingMode.Local;
            this.ReportViewer1.LocalReport.ReportPath = MsReportPath;

            intYearInt = Convert.ToInt32(txtCurrentMonth.Text.ToDateTime().Date.Year);
            intMonthInt = Convert.ToInt32(txtCurrentMonth.Text.ToDateTime().Date.Month);
            intCompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            intWorkStatusID = Convert.ToInt32(ddlWorkStatus.SelectedValue);

            if (ddlDepartment.SelectedIndex != -1)
            {
                intDepartmentID = Convert.ToInt32(ddlDepartment.SelectedValue);
            }
            if (ddlDesignation.SelectedIndex != -1)
            {
                intDesignationID = Convert.ToInt32(ddlDesignation.SelectedValue);
            }
            if (ddlEmployee.SelectedIndex != -1)
            {
                intEmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            }
            if (ddlWorkStatus.SelectedIndex != -1)
            {
                intWorkStatusID = Convert.ToInt32(ddlWorkStatus.SelectedValue);
            }
            string pPayID = objEmployee.GetPaymentIDList(intYearInt, intMonthInt, intCompanyID, intDepartmentID, intDesignationID, intEmployeeID,intWorkStatusID);
            string strPayID = pPayID;
            int intCompanyPayFlag = 1;
            int intDivisionID = 0;
            intMonthInt = txtCurrentMonth.Text.ToDateTime().Date.Month;
            intYearInt = txtCurrentMonth.Text.ToDateTime().Date.Year;
            intCompanyID = Convert.ToInt32(ddlCompany.SelectedValue);

            DataTable MdatMaster = objEmployee.datMaster(strPayID, intCompanyPayFlag, intDivisionID);
            if (MdatMaster == null)
            {
                return;
            }
            ViewState["RecordCount"] = MdatMaster.Rows.Count;
            //Show no record found
            if (MdatMaster.Rows.Count <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
                ReportViewer1.Visible = false;
                return;
            }
            Cursor.Current = Cursors.WaitCursor;
            ViewState["MdatMaster"] = datMaster = objEmployee.datMaster(strPayID, intCompanyPayFlag, intDivisionID);
            ViewState["datDetail"] = datDetail = objEmployee.datDetail(strPayID, intMonthInt, intYearInt);
            ViewState["MdatLeaveDed"] = MdatLeaveDed = objEmployee.MdatLeaveDed(strPayID);
            ViewState["datDetailDed"] = datDetailDed = objEmployee.datDetailDed(strPayID, intMonthInt, intYearInt);
            ViewState["MdatOTDed"] = MdatOTDed = objEmployee.MdatOTDed(strPayID);
            ViewState["MdatHourDetail"] = MdatHourDetail = objEmployee.MdatHourDetail(strPayID, intMonthInt, intYearInt);
            ViewState["MdatSalarySlipWorkInfo"] = MdatSalarySlipWorkInfo = objEmployee.MdatSalarySlipWorkInfo(strPayID);
            ViewState["datVatcation"] = datVatcation = objEmployee.MdatVacationDtls(strPayID);
            bool GlDisplayLeaveSummaryInSalarySlip = true;
            bool PbSummary = GlDisplayLeaveSummaryInSalarySlip;
            //this.ReportViewer1.LocalReport.DataSources.Clear();
            this.ReportViewer1.LocalReport.ReportPath = MsReportPath;
            if (datMaster.Rows.Count > 0)
            {
                btnPrint1.Enabled = true;
            }
            else
            {
                btnPrint1.Enabled = false;
            }
            this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            //this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            //this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);
            this.ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(SubReportProcessingEventHandlerVacation);

            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandler);
            //this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(TwoSubReportProcessingEventHandler);
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(ThreeSubReportProcessingEventHandler);
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportCompanyDisplayEventHandler);
            //this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(OneSubReportProcessingEventHandlerOT);
            this.ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubReportProcessingEventHandlerVacation);


            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipMaster", datMaster));
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetail", datDetail));
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_EmployeePaymentSalarySlipDetailDed", datDetailDed));
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetLeaveDetailSalarySlip", MdatLeaveDed));
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeVacationProcess_VacationDtls", datVatcation));
            //this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_OTPolicyDetailList", MdatOTDed));
            //this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetHourlyDetail", MdatHourDetail));
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetSalarySlip_GetSalarySlipWorkInfo", MdatSalarySlipWorkInfo));

            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                {
                  new ReportParameter("GeneratedBy",  new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                new ReportParameter("SelRowCount",  PbSummary.ToString()),
                new ReportParameter("bSummary", Convert.ToString(new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.DisplayLeaveSummaryInSalarySlip)).ToUpper().Trim()=="NO" ?"false":"true"),
                new ReportParameter("bMonthly",  "HRPOWER")
                });
            ReportViewer1.LocalReport.Refresh(); 
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    private void FillAllEmployees()
    {
        int CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), 0, true, -1, -1, -1, ddlWorkStatus.SelectedValue.ToInt32(), -1);
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    private void BindDropDown(DropDownList ddl, string sDataTextField, string sDataValueField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
}
