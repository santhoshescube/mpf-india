﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"  AutoEventWireup="true" CodeFile="RptVacation.aspx.cs" Inherits="reporting_RptVacation" Title="Vacation report" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
        var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
            this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,Vacation%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                           <%-- <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                               <%-- Work status--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,WorkStatus%>'></asp:Literal>
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlworkStatus" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" OnSelectedIndexChanged="LoadEmployee" 
                                                    Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <%--From Date--%><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Literal>
                                                </td>
                                            <td>
                                                <asp:TextBox ID="txtStartdate" runat="server" BorderStyle="Solid" 
                                                    BorderWidth="1px" Height="8px"  ValidationGroup ="Search" Width="85px"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                    Format="dd MMM yyyy" TargetControlID="txtStartdate">
                                                </AjaxControlToolkit:CalendarExtender>
                                                
                                                
                                              <%--  <asp:CompareValidator  ValidationGroup ="Search" ErrorMessage ="Please enter a valid from date"  ID="cmpFromDate"  runat ="server" ControlToValidate ="txtStartdate" Display="Dynamic" Type="Date"  Operator="DataTypeCheck" >
                                                
                                                </asp:CompareValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <%--To Date--%><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Literal>
                                                </td>
                                            <td >
                                                <asp:TextBox ID="txtEndDate" runat="server" BorderStyle="Solid" 
                                                    BorderWidth="1px" Height="8px" ValidationGroup="Submit" Width="85px"></asp:TextBox>
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                    Format="dd MMM yyyy" TargetControlID="txtEndDate">
                                                </AjaxControlToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               <%-- Department--%><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Literal>
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" 
                                                    OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                               <%-- Type--%><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,Type%>'></asp:Literal>
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="180px">
                                                    <asp:ListItem Text='<%$Resources:ReportsCommon,VacationInfo%>'></asp:ListItem>
                                                    <asp:ListItem Text='<%$Resources:ReportsCommon,VacationRejoining%>'></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>' OnCheckedChanged="chkIncludeCompany_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </td>
                                                        <td>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                               <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <%--<td style="width: 38px; margin: 0px; vertical-align: middle">
                         
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text="Go" OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip="Print" Text="Print" OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>--%>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch" runat="server" class="btnsubmit" CausesValidation="true" ValidationGroup ="Search"
                                            Text='<%$Resources:ReportsCommon,Go%>'  OnClick="btnSearch_Click1" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
   
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="ReportViewer1_PageNavigation">
            <LocalReport ReportPath="App_Code\RptVacationSummary.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsVacationSettlement_dtVacationSummary" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
       <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>
</asp:Content>

