﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Globalization;
public partial class reporting_RptVacation : System.Web.UI.Page
{
    private int intCompany = 0;
    private int intBranch = 0;
    int SearchType = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "VacationReport").ToString();
        ReportViewer1.Visible = false;
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            txtStartdate.Text = clsReportCommon.GetMonthStartDate();
            txtEndDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllWorkStatus();

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());

    }
    //private void EnableDisableIncludeCompany()
    //{
    //    //No branch selected
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }

    //}

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue==""?"0":ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }
   
    private void FillAllWorkStatus()
    {

        this.BindDropDown(ddlworkStatus, "WorkStatusID", "WorkStatus", clsReportCommon.GetAllWorkStatus());

    }
    private void FillAllEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue==""?"0":ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(),-1, -1, ddlworkStatus.SelectedValue.ToInt32(), -1
                                ));
     

    }
    #endregion
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ReportViewer1.Visible = true;
            ShowReport();
        }
    }
    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtStartdate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate")); //"Please Select From Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtStartdate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtEndDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate")); //"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtEndDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }



        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            DataTable dtCompany = null;

            string strMReportPath = "";

            this.ReportViewer1.Reset();
            //Clear Report Datasources
            this.ReportViewer1.LocalReport.DataSources.Clear();

            if (txtStartdate.Text != "")
                FromDate = Convert.ToDateTime(txtStartdate.Text, System.Globalization.CultureInfo.InvariantCulture);

            if (txtEndDate.Text != "")
                ToDate = Convert.ToDateTime(txtEndDate.Text, System.Globalization.CultureInfo.InvariantCulture);


            if (ToDate < FromDate)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate")) + "');", true);
                return;
            }
            if (ddlType.SelectedIndex == 0)
            {
                SearchType = 1;
                strMReportPath = ddlEmployee.SelectedValue.ToInt32() == -1 ? (Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptVacationSummaryArb.rdlc" : "reports/RptVacationSummary.rdlc")) :
                    (Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptVacationDetailArb.rdlc" : "reports/RptVacationDetail.rdlc"));
            }
            else if (ddlType.SelectedIndex == 1)
            {
                SearchType = 2;
                strMReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptVacationRejoiningSummaryArb.rdlc" : "reports/RptVacationRejoiningSummary.rdlc");

            }
            DataTable dtVacation = null;
            DataTable dtVacationDetails = null;

            //Set Reportpath
            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
            //Company Header

            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Summary report
            dtVacation = clsRptVactionAndSettlement.Getvacation(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, FromDate, ToDate, SearchType);
            //Detailed Report for a specific employee
            if ((ddlEmployee.SelectedValue.ToInt32() > 0) & SearchType == 1)
                dtVacationDetails = clsRptVactionAndSettlement.GetvacationDetails(ddlEmployee.SelectedValue.ToInt32(), FromDate, ToDate);

            if (dtVacation != null)
            {
                ViewState["RecordCount"] = dtVacation.Rows.Count;
                if (dtVacation.Rows.Count > 0)
                {
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                                 new ReportParameter("Company", ddlCompany.SelectedItem.Text),
                                 new ReportParameter("Branch", ddlBranch.SelectedItem.Text),
                                 new ReportParameter("Department", ddlDepartment.SelectedItem.Text),
                                 new ReportParameter("Workstatus",  ddlworkStatus.SelectedItem.Text),
                                 new ReportParameter("Employee",ddlEmployee.SelectedItem.Text),
                                 new ReportParameter("Type",ddlType.SelectedItem.Text),
                                 new ReportParameter("FromDate",txtStartdate.Text),
                                 new ReportParameter("ToDate",txtEndDate.Text)
                               });

                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsVacationSettlement_dtVacationSummary", dtVacation));
                    if (ddlEmployee.SelectedValue.ToInt32() > 0)
                    {
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsVacationSettlement_dtVacationDetails", dtVacationDetails));
                    }
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    btnPrint.Enabled = true;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
                    ReportViewer1.Visible = false;
                    btnPrint.Enabled = false;
                    return;
                }
            }

            this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.ReportViewer1.ZoomPercent = 100;
        }
        catch (Exception ex)
        {

        }
    }

    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
}
