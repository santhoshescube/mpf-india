﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true"
    CodeFile="RptEmployeeNationalization.aspx.cs" Inherits="reporting_RptEmployeeNationalization"
    Title="Employee Profile" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    
        $(document).ready(function() {
             var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {
                
                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium'); 
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

                
            });
            
        });
        
        
        
        
        
        
    </script>

    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span>
            </legend>
            <%--<div style="text-align:right;" >
                
            </div>--%>
            <div id="filter-cotainer" style="display: block;">
            <asp:UpdatePanel ID="upFilter" runat="server">
                <ContentTemplate>
                     <table id="filters" width="100%">
                    <tr>
                        <td valign="bottom">
                            <%--  <asp:UpdatePanel ID="upd1" runat="server">
                                <ContentTemplate>--%>
                            <table id="tblEmployee" runat="server" class="labeltext" width="100%">
                                <tr>
                                    <td>
                                        <%-- Company--%>
                                        <asp:Literal ID="Lit" runat="server" Text='<%$Resources:ReportsCommon,Company %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="150px"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%-- Department--%>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,Department %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList AutoPostBack="true" ID="ddlDepartment" runat="server" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                    <td style="width: 118px">
                                        <%--   Employment Type--%>
                                        <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,EmploymentType %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList AutoPostBack="true" ID="ddlEmploymentType" runat="server" CssClass="dropdownlist"
                                            Width="180px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <%-- Branch--%>
                                        <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%--Designation--%>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,Designation %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                    <td style="width: 114px">
                                        <%--  Work Status--%>
                                        <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,WorkStatus %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" AutoPostBack="true" runat="server" CssClass="dropdownlist"
                                            Width="180px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chk" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                            AutoPostBack="true" OnCheckedChanged="chk_CheckedChanged" />
                                    </td>
                                    <td>
                                        <%--Work Location--%>
                                        <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,WorkLocation %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkLocation" AutoPostBack="true" runat="server" CssClass="dropdownlist"
                                            Width="150px" OnSelectedIndexChanged="LoadEmployee" />
                                    </td>
                                    <td>
                                        <%--   Employee--%>
                                        <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist" Width="180px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkDate" runat="server"  Text='<%$Resources:ControlsCommon,DateOfJoining %>'
                                            AutoPostBack="true" oncheckedchanged="chkDate_CheckedChanged" />
                                    </td>
                                    <td>
                                        <asp:TextBox Style="padding: 0px; margin: 0px; height: 20px" ID="txtDOJ" runat="server"
                                            MaxLength="11" Width="100px" AutoPostBack="true" onkeypress="return false;" Height="20px" Enabled="false"></asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="txtDOJ_CalendarExtender" runat="server"
                                            Animated="true" Format="dd-MMM-yyyy" PopupButtonID="btnDate" TargetControlID="txtDOJ" />
                                        <div style="float: left; width: 20px; height: 20px">
                                            <asp:ImageButton ID="btnDate" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" Enabled="false" />
                                        </div>
                                    </td>
                                    <td>
                                        <%--Work Location--%>
                                        <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:DocumentsCommon,Country %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="dropdownlist" Width="150px" />
                                    </td>
                                    <td>
                                        <%--Employee--%>
                                        <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,Religion %>'>
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlReligion" runat="server" CssClass="dropdownlist" Width="180px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                    </td>
                                </tr>
                            </table>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                            Text='<%$Resources:ReportsCommon,Go %>' OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ControlsCommon,Print %>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
            </asp:UpdatePanel>
               
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
        <rsweb:ReportViewer ID="rvEmployeeProfile" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" OnPageNavigation="rvEmployeeProfile_PageNavigation">
        </rsweb:ReportViewer>
        </ContentTemplate> 
        </asp:UpdatePanel> 
        <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>--%>
    </div>
</asp:Content>
