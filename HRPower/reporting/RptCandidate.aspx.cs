﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Globalization;
public partial class reporting_RptCandidate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ReportViewer1.Visible = false;
        //btnPrint.Enabled = false;
        this.Title = GetGlobalResourceObject("ReportsCommon","CandidateReport").ToString();
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {
            LoadCombos();

            chkIncludeCompany.Checked = true;

            SetDefaultBranch(ddlBranch);
        }

    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllCandidateStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadJobs(new object(), new EventArgs());
        ddlJob_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }


    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadJobs(object sender, EventArgs e)
    {
        FillAllJobs();
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        int intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }


    private void FillAllJobs()
    {

        this.BindDropDown(ddlJob, "JobId", "JobTitle", clsRptCandidates.FillcandidateJobs(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32())
                                );
        ddlJob_SelectedIndexChanged(null, null);

    }
    private void FillAllCandidates()
    {

        this.BindDropDown(ddlCandidate, "CandidateID", "EngName", clsRptCandidates.FillCandidates(ddlJob.SelectedValue.ToInt32())
                                );


    }
    private void FillAllCandidateStatus()
    {

        this.BindDropDown(ddlStatus, "CandidateStatusId", "CandidateStatus", clsRptCandidates.CandidateStatus()
                                );


    }
    #endregion
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllJobs();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllJobs();
    }
    protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllCandidates();
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        ShowReport();
    }
    private void ShowReport()
    {
        try
        {
            DataTable dtCompany = null;

            string strMReportPath = "";

            this.ReportViewer1.Reset();
            //Clear Report Datasources
            this.ReportViewer1.LocalReport.DataSources.Clear();



           
            strMReportPath =  clsGlobalization.IsArabicCulture() ? ddlCandidate.SelectedValue.ToInt32() == -1 ? Server.MapPath("reports/RptCandidateListArb.rdlc") :
                                                                             Server.MapPath("reports/RptCandidateDetailsArb.rdlc")
: ddlCandidate.SelectedValue.ToInt32() == -1 ? Server.MapPath("reports/RptCandidateList.rdlc") :
                                                                             Server.MapPath("reports/RptCandidateDetails.rdlc");

            DataTable dtCandidateList = null;
            DataTable dtCandidateMaster = null;
            DataTable dtCandidateScheduledetails = null;



            //Set Reportpath

            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
            //Company Header

            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Summary report
            if (ddlCandidate.SelectedValue.ToInt32() > 0)
            {
                dtCandidateMaster = clsRptCandidates.ShowCandidatemasterReport(ddlCandidate.SelectedValue.ToInt32());
                ViewState["RecordCount"] = dtCandidateMaster.Rows.Count;
                if (dtCandidateMaster.Rows.Count > 0)
                    dtCandidateScheduledetails = clsRptCandidates.ShowCandidatescheduleReport(ddlCandidate.SelectedValue.ToInt32());
                if (dtCandidateMaster != null)
                {
                    if (dtCandidateMaster.Rows.Count > 0)
                    {
                        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))
                                
                               });

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCandidate_dtCandidateMaster", dtCandidateMaster));

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCandidate_dtinterviewScheduled", dtCandidateScheduledetails));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        btnPrint.Enabled = true;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
                        ReportViewer1.Visible = false;
                        btnPrint.Enabled = false;
                        return;
                    }
                }
            }
            else
            {
                dtCandidateList = clsRptCandidates.ShowCandidateList(ddlJob.SelectedValue.ToInt32(), ddlStatus.SelectedValue.ToInt32());
                ViewState["RecordCount"] = dtCandidateList.Rows.Count;
                if (dtCandidateList != null)
                {
                    if (dtCandidateList.Rows.Count > 0)
                    {
                        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))
                                
                               });

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCandidate_dtCandidateList", dtCandidateList));

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        btnPrint.Enabled = true;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" +GetGlobalResourceObject("ReportsCommon","NoInformationFound").ToString()+"');", true);
                        ReportViewer1.Visible = false;
                        btnPrint.Enabled = false;
                        return;
                    }
                }
            }
            //Detailed Report for a specific employee
            //if (dtJobmaster.Rows.Count > 0)
            //{
            //    dtJobDetails = clsRptCandidates.FillJobDetailsReport(ddlJob.SelectedValue.ToInt32());
            //    dtJobScheduledetails = clsRptCandidates.FillJobScheduleDetailsReport(ddlJob.SelectedValue.ToInt32());
            //}


            this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.ReportViewer1.ZoomPercent = 100;
            ReportViewer1.Visible = true;
        }
        catch
        {

        }
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
}
