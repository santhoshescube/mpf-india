﻿<%@ Page Title="Employee Profile" Language="C#" MasterPageFile="~/Master/ReportmasterPage.master" AutoEventWireup="true"
    CodeFile="RptMisEmployeeProfile.aspx.cs" Inherits="reporting_RptMisEmployeeProfile"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
    <h4>
        <asp:Label ID="lblReportHeading" runat="server" Font-Size="Medium">Employee 
        Profile</asp:Label></h4>
            <script type="text/javascript">

        $(document).ready(function() {

            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');

            });

        });
        
    </script>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                 <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblEmpProfile" runat="server" style="margin-top: 2px;">
                    <tr>
                        <td>
                            <%--<asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table>
                                        <tr>
                                            <td class="bold" style="width: 61px">
                                                Company
                                            </td>
                                            <td style="width: 179px">
                                                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist_mandatory"
                                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="bold" style="width: 49px">
                                                Branch
                                            </td>
                                            <td style="width: 180px">
                                                <asp:DropDownList ID="ddlBranch" runat="server" CssClass="dropdownlist_mandatory"
                                                    Width="180px" AutoPostBack="True" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="bold" style="width: 62px">
                                                Employee
                                            </td>
                                            <td style="width: 177px">
                                                <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist_mandatory"
                                                    Width="180px" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkIncludeCompany" Text="Include Company" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                               <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td>
                            <%--<asp:ImageButton SkinID="GoButton" ID="btnSearch" runat="server" OnClick="btnSearch_Click" />--%>
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text="Go" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <%--<fieldset>
        <div style="min-height: 500px;">
            <div style="margin-left: 65px;">
                <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
                    DisplayGroupTree="False" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
                    HasToggleGroupTreeButton="False" Height="50px" Width="350px" />
            </div>
        </div>
    </fieldset>--%>
      <fieldset class="report-fs">
            <iframe id="iframeReport" runat="server" class="iframe" height="1150" scrolling="auto"
                border="0" frameborder="0"></iframe>
        </fieldset>

</asp:Content>
