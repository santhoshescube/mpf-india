﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using Microsoft.Reporting.WebForms;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

public partial class reporting_RptEmployeePunching : System.Web.UI.Page
{

    private int intCompany;
    private int intBranch;
    private int intEmployee;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "AttendanceReport").ToString();
        rvEmployeePunching.Visible = ibtnExcel.Visible = false; divData.Style["display"] = "none";
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            txtFromDate.Text = new clsCommon().GetSysDate().ToString();
            LoadCombos();
            SetDefaultBranch(ddlBranch);
            txtFromDate_TextChanged(sender, e);
        }
    }


    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }

    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
        FillAllEmployees();
        FillDesignation();     
    }
    
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }


    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }

    private void FillAllWorkStatus()
    {

        this.BindDropDown(ddlWorkstatus, "WorkStatusID", "WorkStatus", clsReportCommon.GetAllWorkStatus());

    }
    private void FillDesignation()
    {
        this.BindDropDown(ddlDesignation, "DesignationID", "Designation", clsReportCommon.GetAllDesignation());
    }
    private void FillAllEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue == "" ? "0" : ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), -1, -1, ddlWorkstatus.SelectedValue.ToInt32(), -1
                                ));


    }

    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";
        bool blnIsValid = true;
        string strMessage = string.Empty;

        if (txtFromDate.Text == string.Empty)
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate").ToString();
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate").ToString();
            blnIsValid = false;
        }
        else if (txtToDate.Text == string.Empty)
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate").ToString();
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate").ToString();
            blnIsValid = false;
        }

        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }

    private void ShowReport()
    {
        try
        {
            DataTable dtCompany = null;
            this.rvEmployeePunching.Reset();
            //Clear DataSources
            this.rvEmployeePunching.LocalReport.DataSources.Clear();
            //Set Report Path
            this.rvEmployeePunching.LocalReport.ReportPath = Server.MapPath("reports/RptAttendanceLogDailyPunching.rdlc");

            //Set Company Header For Report
            //Company Header

            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);

            rvEmployeePunching.LocalReport.SetParameters(new ReportParameter[]
                {
                    new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                   
                });

            DataTable dtDailyPunching = null;
            if (ddlEmployee.SelectedValue.ToInt32() > -1)
            {
                dtDailyPunching = clsMISEmployeeReport.GetPunchingLogReport(ddlEmployee.SelectedValue.ToInt32(), txtFromDate.Text.ToString(), txtToDate.Text.ToString(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32()
                   , chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkstatus.SelectedValue.ToInt32()).Tables[0];
            }
            else
            {
                dtDailyPunching = clsMISEmployeeReport.GetPunchingLogReport(txtFromDate.Text.ToString(), txtToDate.Text.ToString(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
                    ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlWorkstatus.SelectedValue.ToInt32()).Tables[0];
            }
            ViewState["RecordCount"] = dtDailyPunching.Rows.Count;
            if (dtDailyPunching.Rows.Count > 0)
            {
                rvEmployeePunching.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                rvEmployeePunching.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_DtPunchingLog", dtDailyPunching));

                btnPrint1.Enabled = true;
                divData.Style["display"] = "none";
                divData.Style["height"] = "550px";
                rvEmployeePunching.Visible = true;
                ibtnExcel.Visible = false;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                rvEmployeePunching.Visible = false;
                btnPrint1.Enabled = false;
                return;
            }
        }

        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
            rvEmployeePunching.Visible = false;
            rvEmployeePunching.LocalReport.DataSources.Clear();
            btnPrint1.Enabled = false;
            return;
        }
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            txtFromDate_TextChanged(sender, e);
        }
        catch
        { }
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            if (ddlFillFormat.SelectedIndex == 0)
            {
                ShowReport();

            }
            else
            {
                ShowGridReport();               

            }
        }
    }

    private void ShowGridReport()
    {
        try
        {
            DataTable datTemp = new DataTable();

            if (ddlEmployee.SelectedIndex != 0)
            {
                datTemp = clsMISEmployeeReport.GetPunchingLogReportGrid(ddlEmployee.SelectedValue.ToInt32(),
                        txtFromDate.Text, txtToDate.Text, ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                        chkIncludeCompany.Checked.ToBoolean(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(),
                        ddlWorkstatus.SelectedValue.ToInt32()).Tables[0];
            }
            else
            {
                datTemp = clsMISEmployeeReport.GetPunchingLogReportGrid(txtFromDate.Text, txtToDate.Text, ddlCompany.SelectedValue.ToInt32(),
                           ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked.ToBoolean(), ddlDepartment.SelectedValue.ToInt32(),
                           ddlDesignation.SelectedValue.ToInt32(),
                           ddlWorkstatus.SelectedValue.ToInt32()).Tables[0];
            }
            if (datTemp.Rows.Count > 0)
            {
                datTemp.Columns.RemoveAt(0);
                datTemp.AcceptChanges();

                gvPunchings.DataSource = datTemp;
                gvPunchings.DataBind();

                DataTable dt = new DataTable();
                int ColCount = gvPunchings.Rows[0].Cells.Count;
                for (int i = 0; i < ColCount-1; i++)
                {
                    dt.Columns.Add(gvPunchings.HeaderRow.Cells[i].Text.ToString());
                }                
                foreach (DataRow row in datTemp.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < ColCount - 1; j++)
                    {
                        dr[gvPunchings.HeaderRow.Cells[j].Text.ToString()] = row[j].ToString();
                    }
                    dt.Rows.Add(dr);
                }
                ViewState["datTemp"] = dt;

                divData.Style["display"] = "block";
                divData.Style["height"] = "550px";
                rvEmployeePunching.Visible = false;
                ibtnExcel.Visible = true;               
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
            rvEmployeePunching.Visible = false;
            rvEmployeePunching.LocalReport.DataSources.Clear();
            btnPrint1.Enabled = false;
            return;
        }
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(rvEmployeePunching, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, Microsoft.Reporting.WebForms.PageNavigationEventArgs e)
    {
        rvEmployeePunching.Visible = true;
    }
    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        try
        {
            txtToDate.Enabled = btnToDate.Enabled = ddlEmployee.SelectedValue.ToInt32() > -1;
            if (ddlEmployee.SelectedIndex == 0 || ddlEmployee.SelectedIndex == -1)
            {
                txtToDate.Text = txtFromDate.Text.ToDateTime().AddDays(7).AddDays(-1).ToString("dd MMM yyyy");
            }
            else
            {
                txtToDate.Text = txtFromDate.Text.ToDateTime().AddMonths(1).AddDays(-1).ToString("dd MMM yyyy");
            }
        }
        catch
        {
        }
    }

    protected void gvPunchings_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            int j = 1, k=1;
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                if ((e.Row.Cells[i].Text).Contains("Entry"))
                {
                    e.Row.Cells[i].Text = "Punch In " + j.ToString();
                    j++;
                }
                if ((e.Row.Cells[i].Text).Contains("Exit"))
                {
                    e.Row.Cells[i].Text = "Punch Out " + k.ToString();
                    k++;
                }
                if (e.Row.Cells[i].Text == "EmployeeNumber")
                    e.Row.Cells[i].Text = "Employee Number";
                if (e.Row.Cells[i].Text == "EmployeeName")
                    e.Row.Cells[i].Text = "Employee Name";
                if (e.Row.Cells[i].Text == "NormalWorkTime")
                    e.Row.Cells[i].Text = "Normal Work Time";
                if (e.Row.Cells[i].Text == "ActualWorktime")
                    e.Row.Cells[i].Text = "Actual Work Time";
                if (e.Row.Cells[i].Text == "Late")
                    e.Row.Cells[i].Text = "Late Time";
                if (e.Row.Cells[i].Text == "Early")
                    e.Row.Cells[i].Text = "Early Going";
            }
        }
    }
   
    protected void gvPunchings_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPunchings.PageIndex = e.NewPageIndex;
        ShowGridReport();
    }
    protected void ibtnExcel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dtTemp = (DataTable)ViewState["datTemp"];
            if (dtTemp == null) return;
            if (dtTemp.Rows.Count == 0) return;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=LiveAttendanceExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            GridView gridView = new GridView();
            gridView.DataSource = dtTemp;
            gridView.DataBind();
           
            gridView.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        catch
        {
        }

    }
   
}
