﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
using System.Drawing;

public partial class reporting_RptEmployeeNationalization : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "EmployeeProfileReport").ToString();
     
        rvEmployeeProfile.Visible = false;
        if (!IsPostBack)
        {
            chk.Checked = true;
            LoadCombos();

        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

    }

	public void SetDefaultBranch(DropDownList ddl)
	{
		ddl.SelectedValue = "0";
	}

	/// <summary>
	/// Fill all companies
	/// </summary>
	private void FillAllCompany()
	{
        clsUserMaster objUser = new clsUserMaster();

		ddlCompany.DataTextField = "Company";
		ddlCompany.DataValueField = "CompanyID";
		ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
		ddlCompany.DataBind();
	}

	/// <summary>
	/// Fill all companies based on the company id
	/// </summary>
	private void FillAllBranches()
	{
		ddlBranch.DataTextField = "Branch";
		ddlBranch.DataValueField = "BranchID";
		ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
		ddlBranch.DataBind();
		ddlBranch.SelectedValue = "0";

	}

	/// <summary>
	/// Fill all departments
	/// </summary>
	private void FillAllDeparments()
	{
		ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
		ddlDepartment.DataTextField = "Department";
		ddlDepartment.DataValueField = "DepartmentID";
		ddlDepartment.DataBind();
	}

	/// <summary>
	/// Fill all designation
	/// </summary>
	private void FillAllDesignation()
	{
		ddlDesignation.DataSource = clsReportCommon.GetAllDesignation();
		ddlDesignation.DataTextField = "Designation";
		ddlDesignation.DataValueField = "DesignationID";
		ddlDesignation.DataBind();
	}

	/// <summary>
	/// Fill all employment types
	/// </summary>
	private void FillAllEmploymentType()
	{
		ddlEmploymentType.DataSource = clsReportCommon.GetAllEmploymentTypes();
		ddlEmploymentType.DataTextField = "EmploymentType";
		ddlEmploymentType.DataValueField = "EmploymentTypeID";
		ddlEmploymentType.DataBind();
	}

	/// <summary>
	/// Fill all work status
	/// </summary>
	private void FillAllWorkStatus()
	{
		ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
		ddlWorkStatus.DataTextField = "WorkStatus";
		ddlWorkStatus.DataValueField = "WorkStatusID";
		ddlWorkStatus.DataBind();
		ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString();
	}

	/// <summary>
	/// Fill all work locations based on the company
	/// </summary>
	private void FillAllWorkLocation()
	{
		ddlWorkLocation.DataSource = clsReportCommon.GetAllWorkLocations(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
										chk.Checked);
		ddlWorkLocation.DataTextField = "WorkLocation";
		ddlWorkLocation.DataValueField = "WorkLocationID";
		ddlWorkLocation.DataBind();
	}

	/// <summary>
	/// Fill all employees based on the company ,branch,departmnt,desigantion,employment type,work status and worklocation
	/// </summary>
	private void FillAllEmployees()
	{
		ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
										chk.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(),
										ddlEmploymentType.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlWorkLocation.SelectedValue.ToInt32());
		ddlEmployee.DataTextField = "EmployeeFullName";
		ddlEmployee.DataValueField = "EmployeeID";
		ddlEmployee.DataBind();
	}

    /// <summary>
    /// Fill all country.
    /// </summary>
    private void FillAllCoutry()
    {
        ddlCountry.DataSource = clsReportCommon.GetAllCountry();
        ddlCountry.DataValueField = "CountryId";
        ddlCountry.DataTextField = "Country";
        ddlCountry.DataBind();
    }

    /// <summary>
    /// Fill All religion
    /// </summary>
    private void FillAllReligion()
    {
        ddlReligion.DataSource = clsReportCommon.GetAllReligion();
        ddlReligion.DataValueField = "ReligionID";
        ddlReligion.DataTextField = "Religion";
        ddlReligion.DataBind();
    }
	/// <summary>
	/// Loads all the combos 
	/// </summary>
	private void LoadCombos()
	{
		FillAllCompany();
		FillAllDeparments();
		FillAllDesignation();
		FillAllEmploymentType();
		FillAllWorkStatus();
		FillAllWorkLocation();
        FillAllCoutry();
        FillAllReligion();
		ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
		ddlBranch_SelectedIndexChanged(new object(), new EventArgs());

		LoadEmployee(new object(), new EventArgs());

	}


	/// <summary>
	/// Load employee function
	/// </summary>
	protected void LoadEmployee(object sender, EventArgs e)
	{
		FillAllEmployees();
	}


	protected void btnShow_Click(object sender, EventArgs e)
	{
		ShowReport();
	}



	public void ShowReport()
	{
        try
        {
            DataTable dtEmployee;
            string strMReportPath = "";


            strMReportPath = Server.MapPath("reports/RptNationalisationRatioReport.rdlc");
            this.rvEmployeeProfile.Reset();
            this.rvEmployeeProfile.LocalReport.DataSources.Clear();



           

            //setting the report path
            this.rvEmployeeProfile.LocalReport.ReportPath = strMReportPath;




            dtEmployee = clsRptEmployeeProfile.GetNationalisationRatioReport(0, ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlEmploymentType.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), ddlWorkLocation.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32());  

              
                this.rvEmployeeProfile.LocalReport.ReportPath = strMReportPath;
                this.rvEmployeeProfile.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter),false),
                    new ReportParameter("Company", ddlCompany.SelectedItem.Text, false),
                    new ReportParameter("Branch", ddlBranch.SelectedItem.Text, false),
                    new ReportParameter("Department", ddlDepartment.SelectedItem.Text, false),
                    new ReportParameter("Designation", ddlDesignation.SelectedItem.Text, false),
                    new ReportParameter("EmployeementType", ddlEmploymentType.SelectedItem.Text, false),
                    new ReportParameter("Employee",ddlEmployee.SelectedItem.Text, false),
                    new ReportParameter("Location", ddlWorkLocation.SelectedItem.Text, false),
                    new ReportParameter("WorkStatus", ddlWorkStatus.SelectedItem.Text, false)
                });
                string  dtDoj = chkDate.Checked ? txtDOJ.Text : null;
               

              
                if (dtEmployee.Rows.Count > 0)
                {
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chk.Checked)));
                    rvEmployeeProfile.LocalReport.DataSources.Add(new ReportDataSource("dsRptNationalisationRatioReport_NationalisationRatioReport", dtEmployee));
                    btnPrint1.Enabled = true;
                }


            ViewState["RecordCount"] = dtEmployee.Rows.Count;
            //No records found message
            if (dtEmployee.Rows.Count == 0)
            {
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnGo, btnGo.GetType(), new Guid().ToString(), "alert('"+GetGlobalResourceObject("ReportsCommon","NoInformationFound").ToString()+"');", true);

                return;

            }
            else
            {
                rvEmployeeProfile.LocalReport.Refresh();
                rvEmployeeProfile.Visible = true;
                btnPrint1.Enabled = true;
            }

            
            //this.rvEmployeeProfile.SetDisplayMode(DisplayMode.PrintLayout);
            this.rvEmployeeProfile.ZoomMode = ZoomMode.Percent;
            this.rvEmployeeProfile.ZoomPercent = 100;
        }
        catch
        {
        }

	}
	#region GetEmployees
	private DataTable GetEmployees()
	{
		clsEmployeeReports objEmployeeReport = new clsEmployeeReports();
		objEmployeeReport.CompanyID = int.Parse(ddlCompany.Items.Count == 0 ? "0" : ddlCompany.SelectedValue);
		return objEmployeeReport.GetAllEmployees();
	}
	#endregion

	protected void btnGo_Click(object sender, EventArgs e)
	{
		ShowReport();
	}
	protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
	{
		FillAllBranches();
	}
	protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
	{
		EnableDisableIncludeCompany();
		FillAllWorkLocation();
		FillAllEmployees();

	}


    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue =  "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chk.Checked = chk.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chk.Checked = true;
                chk.Enabled = false;
            }
            else
            {
                chk.Enabled = chk.Checked = true;
            }
        }

    }

	protected void Button1_Click1(object sender, EventArgs e)
	{
		ShowReport();
	}
	protected void btnPrint_Click(object sender, ImageClickEventArgs e)
	{
		clsCommon.PrintReport(rvEmployeeProfile, this);
	}


	protected void chk_CheckedChanged(object sender, EventArgs e)
	{
		FillAllWorkLocation();
		FillAllEmployees();

	}
    protected void rvEmployeeProfile_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        rvEmployeeProfile.Visible = true;
    }

    protected void chkDate_CheckedChanged(object sender, EventArgs e)
    {
        txtDOJ.Enabled = btnDate.Enabled = chkDate.Checked;
    }
}


