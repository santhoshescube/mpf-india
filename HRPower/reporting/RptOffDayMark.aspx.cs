﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Reporting.WebForms;
using System.Globalization;

public partial class reporting_RptOffDayMark : System.Web.UI.Page
{
  
    private clsCommon objCommon;
    private int intCompany = 0;
    private int intBranch = 0;
    private int intEmployee = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "OffDayReport").ToString();
        btnPrint1.Enabled = false;
        ReportViewer.Visible = false;
        if (!IsPostBack)
        {
            //Fill all the combos
            FillCombos();
            objCommon = new clsCommon();

            txtMonthYear.Text = objCommon.GetSysMonthYear();

            chkIncludeCompany.Checked = true;
        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
    }
    private void FillCombos()
    {
        FillAllCompanies();
        FillAllDepartments();
        FillWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
    }
    private void FillAllCompanies()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }

    private void FillAllBranch()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        ddlBranch.SelectedValue = "0";

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllDepartments()
    {
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataBind();
    }

    //private void FillAllEmployees()
    //{
    //    ddlEmployee.DataTextField = "EmployeeFullName";
    //    ddlEmployee.DataValueField = "EmployeeID";
    //    ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,
    //                                ddlDepartment.SelectedValue.ToInt32(), -1, -1, -1, -1);
    //    ddlEmployee.DataBind();
    //}
    private void FillAllWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }
    }

    #region"Filling Drop Down List"


    private void ShowMessage(string message)
    {
        ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + message + "');", true);

    }

    private bool ValidateReport()
    {
        return true;
    }


    private void ShowReport()
    {
        try
        {
            ReportViewer.Visible = false;

            DataTable DtOffDay = null;
            string strMReportPath = "";

            //setting the report path based on the payment type value

            strMReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptOffDayDetailsArb.rdlc" : "reports/RptOffDayDetails.rdlc");


            //Setting the report header 

            ReportViewer.Reset();
            ReportViewer.LocalReport.DataSources.Clear();
            ReportViewer.LocalReport.ReportPath = strMReportPath;

            //Monthly Report

            ReportViewer.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false),
                    new ReportParameter("IsDateSelected",  "0", false),

                });
            DtOffDay = clsRptPayroll.GetOffDayDetails(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32() <= 0 ? 0 : ddlDepartment.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32() <= 0 ? 0 : ddlEmployee.SelectedValue.ToInt32(), clsCommon.Convert2DateTime(txtMonthYear.Text), chkIncludeCompany.Checked,ddlWorkStatus.SelectedValue.ToInt32());


            if (DtOffDay.Rows.Count > 0)
            {
                this.ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_OffDayDetail", DtOffDay));
                ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                btnPrint1.Enabled = true;
            }



            ViewState["RecordCount"] = DtOffDay.Rows.Count;
            //Show no record found
            if (DtOffDay.Rows.Count == 0)
            {
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('NoInformationFound');", true);
                return;
            }

            ReportViewer.LocalReport.Refresh();

            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;

            ReportViewer.Visible = true;
            //Set the report display mode and zoom percentage
            // SetReportDisplay(ReportViewer);

        }
        catch (Exception ex)
        {

        }
        // upd.Update();
    }

    #endregion
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
        FillAllWorkStatus();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {

        if (ValidateReport())
            ShowReport();

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer, this);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }

    protected void ReportViewer_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer.Visible = true;
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    private void FillEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue == "" ? "0" : ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, -1, -1, -1, ddlWorkStatus.SelectedValue.ToInt32(), -1
                                ));
    }
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }

}
