﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class reporting_Print : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Session["PrintData"] != null)
        {

            byte[] bytes = ((byte[])HttpContext.Current.Session["PrintData"]);

            if (bytes != null && bytes.Length > 0)
            {
                Response.ContentType = "application/pdf";

                Response.AddHeader("content-length", bytes.Length.ToString());

                Response.BinaryWrite(bytes);
            }
        }
    }
}
