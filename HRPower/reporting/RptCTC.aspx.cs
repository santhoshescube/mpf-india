﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;


public partial class reporting_RptCTC : System.Web.UI.Page
{
    #region Declarations
    clsCommon objCommon;
    clsUserMaster objUser;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "CTCReport").ToString();
        ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            LoadInitial();
        }
    }


    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlType.SelectedIndex == 0)
        {
            ddlFinYear.Enabled = false;
            txtMonth.Enabled = true;
        }
        else
        {
            ddlFinYear.Enabled = true;
            txtMonth.Enabled = false;
            FillFinYear();
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }

    #region Methods
    private void LoadInitial()
    {
        objCommon = new clsCommon();
        txtMonth.Text = objCommon.GetSysMonthYear();
        ddlFinYear.Enabled = false;

        FillCompany();
        FillBranch();
        FillEmployees();
        FillDepartment();
        FillWorkStatus();
        FillType();
        FillFinYear();
    }

    private void FillCompany()
    {
        objUser = new clsUserMaster();
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();
    }
    private void FillFinYear()
    {
        ddlFinYear.DataSource = clsReportCommon.GetAllFinancialYears(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
        ddlFinYear.DataTextField = "FinYearPeriod";
        ddlFinYear.DataValueField = "FinYearStartDate";
        ddlFinYear.DataBind();
    }
    private void FillEmployees()
    {
        DataTable dt = new DataTable();
        dt = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                         chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), -1, -1, ddlWorkStatus.SelectedValue.ToInt32());
        dt.Rows.RemoveAt(0);
        dt.AcceptChanges();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    /// <summary>
    /// Fill all types
    /// 0->Monthly 
    /// 1->Financial year
    /// </summary>
    private void FillType()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Type");
        dt.Columns.Add("TypeID");

        dr = dt.NewRow();
        dr["Type"] = "Month";
        dr["TypeID"] = 1;
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Type"] = "Financial Year";
        dr["TypeID"] = 2;
        dt.Rows.Add(dr);

        ddlType.DataSource = dt;
        ddlType.DataTextField = "Type";
        ddlType.DataValueField = "TypeID";
        ddlType.DataBind();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = "6";
    }
    private void FillDepartment()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }
    private void FillBranch()
    {
        int CompanyID = ddlCompany.SelectedValue.ToInt32();
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillEmployees();
    }
    private bool ValidateDate()
    {
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DataSet dsEmployeeCTC;
            DataTable dtCompany = null;
            DateTime? Date;

            if (ddlType.SelectedIndex == 1)
                Date = null;
            else
                Date = clsCommon.Convert2DateTime(txtMonth.Text).Date.AddMonths(1).AddDays(-clsCommon.Convert2DateTime(txtMonth.Text).Date.Day);

            //Set Company Header For Report 
            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
            //Get Report Data
            dsEmployeeCTC = clsRptCTC.GetEmployeeCTCReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlEmployee.SelectedValue.ToInt32(), Date, (ddlType.SelectedIndex == 0 ? "" : ddlFinYear.SelectedItem.Text),ddlWorkStatus.SelectedValue.ToInt32());

            //Set Report Path
            ReportViewer1.LocalReport.ReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptEmployeeCTCArb.rdlc" : "reports/RptEmployeeCTC.rdlc");

            //Set Report DataSource if Record Exists
            if (dsEmployeeCTC.Tables[0].Rows.Count > 0)
            {
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCTC_dtCTC", dsEmployeeCTC.Tables[0]));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsCTC_dtAssetDetails", dsEmployeeCTC.Tables[1]));

                //Set Report Parameters
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
            {
                new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),               
                new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),                
                new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),               
                new ReportParameter("Period",ddlType.SelectedIndex == 0?"Month":"Financial Year", false),
                new ReportParameter("PeriodValue",ddlType.SelectedIndex == 0?txtMonth.Text:ddlFinYear.SelectedItem.Text, false)
            });
                btnPrint.Enabled = true;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            else
            { // Show 'No Records Found' Message to User
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
            }
        }
        catch { }
    }

    #endregion
    protected void Button1_Command(object sender, CommandEventArgs e)
    {

    }
}
