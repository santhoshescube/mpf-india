﻿<%@ Page Title="Employee Salary Structure" Language="C#" MasterPageFile="~/Master/ReportmasterPage.master"
    AutoEventWireup="true" CodeFile="RptMISSalaryStructure.aspx.cs" Inherits="reporting_RptMISSalaryStructure" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
  
    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,EmployeeSalaryStructure%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                 <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="filters" runat="server">
                    <tr>
                        <td valign="bottom">
                           <%-- <asp:UpdatePanel ID="upd" runat="server">
                                <ContentTemplate>--%>
                                    <table class="labeltext" width="100%">
                                        <tr>
                                            <td style ="width:70px">
                                               <%-- <asp:Label ID="lblCompnay" runat="server" Text="Company"></asp:Label>--%>
                                                 <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="150px" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td style ="padding-left  :10px;width:85px">
                                              <%--  <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>--%>
                                              
                                             <asp:Label ID="lblDepartment" runat="server" Text='<%$Resources:ReportsCommon,Department %>'></asp:Label>
           
                                              
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="true" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="LoadEmployee" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                            <td  style ="padding-left  :10px;width:120px">
                                            
                                               <%-- <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type"></asp:Label>--%>
                                                    <asp:Label ID="lblEmploymentType" runat="server" Text='<%$Resources:ReportsCommon,EmploymentType %>'></asp:Label>
                                               
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmploymentType" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="165px" OnSelectedIndexChanged="LoadEmployee">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style ="width:70px">
                                               <%-- <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>--%>
                                               
                                                  <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                               
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="150px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td  style ="padding-left  :10px">
                                                <%--<asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>--%>
                                                   <asp:Label ID="lblDesignation" runat="server" Text='<%$Resources:ReportsCommon,Designation %>'></asp:Label>
                                                
                                                
                                            </td>
                                            <td >
                                                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="true" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="LoadEmployee" Width="150px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style ="padding-left  :10px">
                                             <%--   <asp:Label ID="lblType" runat="server" Text="Type"></asp:Label>--%>
                                                  <asp:Label ID="lblType" runat="server" Text='<%$Resources:ReportsCommon,Type %>'></asp:Label>
                                             
                                             
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlType" runat="server" CssClass="dropdownlist_mandatory"   Width="165px">
                                                   <%-- <asp:ListItem Selected="True" Value="0">Salary</asp:ListItem>
                                                    <asp:ListItem Value="1">History</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>' AutoPostBack="True"
                                                    Checked="True" oncheckedchanged="LoadEmployee" />
                                            </td>
                                           <td  style ="padding-left  :10px">
                                         <%--  Work Status--%>
                                         
                                         
                                         <asp:Literal ID ="LitWorkStatus" runat ="server" Text ='<%$Resources:ReportsCommon,WorkStatus %>'>
                                         </asp:Literal>
                                         
                                           </td>
                                           <td>
                                               <asp:DropDownList ID="ddlWorkStatus" Width="150px" runat="server" 
                                                   CssClass="dropdownlist_mandatory" AutoPostBack="True"
                                                   OnSelectedIndexChanged ="ddlWorkStatus_SelectedIndexChanged" > 
                                               </asp:DropDownList>

                                           </td>
                                           
                                            <td style ="padding-left  :10px">
                                        <%--   Employee--%>
                                        
                                        <asp:Literal ID ="Literal1" runat ="server" Text ='<%$Resources:ReportsCommon,Employee %>'>
                                         </asp:Literal>
                                           </td>
                                           <td>
                                               <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist_mandatory" Width="165px" >
                                               </asp:DropDownList>

                                           </td>
                                            
                                        </tr>
                                    </table>
                              <%--  </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td style="width: 14px">
                            <asp:Button ID="btnShow" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>'
                                onclick="btnShow_Click"  />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print%>' Text="Print" OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
   
   
    
    
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="ReportViewer1_PageNavigation">
        </rsweb:ReportViewer>
       <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>--%>
    </div>
    
    
    
    
</asp:Content>
