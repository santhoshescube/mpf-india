﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Microsoft.Reporting.WebForms;
using System.Globalization;

public partial class reporting_RptAsset : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    DataTable dtCompanyAsset = null;
    DataSet datTemp = null;
    public DataTable dtCompany { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "AssetReport").ToString();
        btnPrint1.Enabled = false;
        ReportViewer.Visible = false;
        if (!IsPostBack)
        {
            //Fill all the combos
            FillCombos();
            //Enable or disable the month combo ,from date and to date combo based on the type value
            //EnableDisableDates();
           
            chkIncludeCompany.Checked = true;
            ddlType.SelectedIndex = 0;
            ddlCompany_SelectedIndexChanged(null, null);
            SetDefaultBranch(ddlBranch);
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");

        }
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
    }
    private void EnableDisableDates()
    {
        if (ddlType.SelectedIndex == 0)
        {
            lblFromDate.Enabled = false;
            lblToDate.Enabled = false;
            txtFromDate.Enabled = false;
            txtToDate.Enabled = false;
        }
        else
        {
            lblFromDate.Enabled = true;
            lblToDate.Enabled = true;
            txtFromDate.Enabled = true;
            txtToDate.Enabled = true;
        }
    }
    private void FillCombos()
    {
        FillAllCompanies();
        FillWorkStatus();
        SetDefaultWorkStatus(ddlWorkStatus);
        FillAllEmployees();
        FillAssetTypes();
        FillAssets();
        FillAssetStatus();

        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        //ddlBranch_SelectedIndexChanged(new object(), new EventArgs());

    }
    private void FillAllCompanies()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }
    private void FillAllBranch()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();
        //ddlBranch.SelectedValue = "0";

        //ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString();
    }
    //private void FillAllEmployees()
    //{
    //    ddlEmployee.DataTextField = "EmployeeFullName";
    //    ddlEmployee.DataValueField = "EmployeeID";
    //    ddlEmployee.DataSource = clsReportCommon.LoadEmployeesIncurrentCompany(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked,-1, -1, -1, ddlWorkStatus.SelectedValue.ToInt32());

    //    ddlEmployee.DataBind();
    //}
    private void FillAllEmployees()
    {
        int CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), 0, true, -1, -1, -1, ddlWorkStatus.SelectedValue.ToInt32(), -1);
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    private void FillAssetTypes()
    {
        ddlAssetType.DataTextField = "BenefitTypeName";
        ddlAssetType.DataValueField = "BenefitTypeID";
        ddlAssetType.DataSource = clsReportCommon.GetAllAssetTypes();
        ddlAssetType.DataBind();
        ddlAssetType_SelectedIndexChanged(null, null);
    }
    private void FillAssets()
    {
        ddlAsset.DataTextField = "Description";
        ddlAsset.DataValueField = "CompanyAssetID";
        ddlAsset.DataSource = clsReportCommon.GetAllAssets(ddlAssetType.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
        ddlAsset.DataBind();
    }
    private void FillAssetStatus()
    {
        ddlAssetStatus.DataTextField = "AssetStatus";
        ddlAssetStatus.DataValueField = "AssetStatusID";
        ddlAssetStatus.DataSource = clsReportCommon.GetAllAssetStatus(ddlType.SelectedIndex);
        ddlAssetStatus.DataBind();
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAssetStatus();
        if (ddlType.SelectedIndex == 0)
        {
            ddlWorkStatus.Enabled = false;
            ddlEmployee.Enabled = false;
        }
        else
        {
            ddlWorkStatus.Enabled = true;
            ddlEmployee.Enabled = true;
        }
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    #region"Filling Drop Down List"

    private bool ValidateDate()
    {
        //DateTime dtFromDate = System.DateTime.MinValue;
        //DateTime dtToDate = System.DateTime.MinValue;

        //DateTimeFormatInfo df = new DateTimeFormatInfo();

        //df.ShortDatePattern = "dd MMM yyyy";

        //bool blnIsValid = true;
        //string strMessage = string.Empty;


        //if (rdBtnSummary.Checked)
        //{
        //    if (txtFromDate.Text == string.Empty)
        //    {
        //        strMessage = "Please Select From Date";
        //        blnIsValid = false;
        //    }
        //    else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        //    {
        //        strMessage = "Please Select Valid Date";
        //        blnIsValid = false;
        //    }
        //    else if (txtToDate.Text == string.Empty)
        //    {
        //        strMessage = "Please Select To Date";
        //        blnIsValid = false;
        //    }
        //    else if (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        //    {
        //        strMessage = "Please Select Valid Date";
        //        blnIsValid = false;
        //    }

        //    if (!blnIsValid)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
        //        return false;
        //    }
        //}

        return true;
    }

    private void ShowMessage(string message)
    {
        ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + message + "');", true);
    }
    private bool ValidateReport()
    {

        DateTime dtDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();
        df.ShortDatePattern = "dd-MMM-yyyy";

        if (ddlType.SelectedIndex == 1)
        {

            if (txtFromDate.Text == "")
            {
                //ShowMessage("Please enter from date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate").ToString());
                return false;
            }
            else if (txtToDate.Text == "")
            {
                // ShowMessage("Please enter to date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate").ToString());
                return false;
            }

            else if (!(DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtDate)))
            {
                //ShowMessage("Invalid from date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidFromDate").ToString());
                return false;
            }
            else if (!(DateTime.TryParse(txtToDate.Text, out dtDate)))
            {
                //ShowMessage("Invalid to date");
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "InvalidToDate").ToString());
                return false;
            }
        }
        return true;
    }
    private void ShowAssetReport()
    {
        string strMReportPath = "";

        ReportViewer.LocalReport.DataSources.Clear();

        this.ReportViewer.Reset();


        if (ddlCompany.SelectedIndex >= 0)
        {

            DateTime FromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime ToDate = Convert.ToDateTime(txtToDate.Text);

            if (ddlType.SelectedIndex == 0)
            {
                if (clsGlobalization.IsArabicCulture())
                {
                    strMReportPath = Server.MapPath("reports/RptCompanyAssetDetailsArb.rdlc");

                }
                else
                {
                    strMReportPath = Server.MapPath("reports/RptCompanyAssetDetails.rdlc");

                }

                dtCompanyAsset = clsRptAsset.GetAllCompanyAssets(ddlAssetType.SelectedValue.ToInt32(), ddlAsset.SelectedValue.ToInt32(), ddlAssetStatus.SelectedValue.ToInt32(), FromDate, ToDate, ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            }
            else
            {
                if (clsGlobalization.IsArabicCulture())
                {
                    if ((ddlEmployee.SelectedValue.ToInt32() != -1) && (ddlAsset.SelectedValue.ToInt32() == -1))
                        strMReportPath = Server.MapPath("reports/RptAssetHistorySingleEmployArb.rdlc");
                    else if ((ddlEmployee.SelectedValue.ToInt32() == -1) && (ddlAsset.SelectedValue.ToInt32() == -1))
                        strMReportPath = Server.MapPath("reports/RptCompanyAssetHandOverArb.rdlc");
                    else if (ddlAsset.SelectedValue.ToInt32() != -1)
                        strMReportPath = Server.MapPath("reports/RptCompanyAssetRetailsArb.rdlc");
                }
                else
                {
                    if ((ddlEmployee.SelectedValue.ToInt32() != -1) && (ddlAsset.SelectedValue.ToInt32() == -1))
                        strMReportPath = Server.MapPath("reports/RptAssetHistorySingleEmploy.rdlc");
                    else if ((ddlEmployee.SelectedValue.ToInt32() == -1) && (ddlAsset.SelectedValue.ToInt32() == -1))
                        strMReportPath = Server.MapPath("reports/RptCompanyAssetHandOver.rdlc");
                    else if (ddlAsset.SelectedValue.ToInt32() != -1)
                        strMReportPath = Server.MapPath("reports/RptCompanyAssetRetails.rdlc");
                }
                datTemp = clsRptAsset.DisplayCompanyAssetReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlType.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), ddlAssetType.SelectedValue.ToInt32(), ddlAsset.SelectedValue.ToInt32(), ddlAssetStatus.SelectedValue.ToInt32(), FromDate, ToDate, chkIncludeCompany.Checked);
            }
            ReportViewer.LocalReport.ReportPath = strMReportPath;

            

            ReportViewer.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false),
                                 new ReportParameter("Company", ddlCompany.SelectedItem.Text, false),
                                 new ReportParameter("Branch", ddlBranch.SelectedItem.Text, false),
                                 new ReportParameter("Department", ddlType.SelectedItem.Text, false),
                                 new ReportParameter("Workstatus", ddlWorkStatus.SelectedItem.Text, false),
                                 new ReportParameter("Employee", ddlEmployee.SelectedItem.Text, false),
                                 new ReportParameter("AssetType",ddlAssetType.SelectedItem.Text, false),
                                 new ReportParameter("CompanyAsset",ddlAsset.SelectedItem.Text, false),
                                 new ReportParameter("Status",ddlAssetStatus.SelectedItem.Text, false),
                                 new ReportParameter("FromDate",txtFromDate.Text, false),
                                 new ReportParameter("ToDate",txtToDate.Text, false)
                               });

           // SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
            if (ddlType.SelectedIndex == 0)
            {
                if (dtCompanyAsset.Rows.Count > 0)
                {
                    ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetInfo", dtCompanyAsset));
                    btnPrint1.Enabled = true;
                    ViewState["RecordCount"] = dtCompanyAsset.Rows.Count;
                }
                else
                {
                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لعثور على أي معلومات") : ("No Information Found");
                    btnPrint1.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
                    return;
                }

            }
            else
            {
                if (datTemp.Tables[0].Rows.Count > 0)
                {
                    if (ddlAsset.SelectedValue.ToInt32() > 0)
                    {
                        ReportViewer.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                        ReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                        ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetDetails", datTemp.Tables[0]));
                        ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAssetInfo", datTemp.Tables[1]));
                        ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_AssetUsageDamageInfo", datTemp.Tables[2]));
                        btnPrint1.Enabled = true;
                        ViewState["RecordCount"] = datTemp.Tables[0].Rows.Count;
                    }
                    else
                    {
                        ReportViewer.LocalReport.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_CompanyAsset", datTemp.Tables[0]));
                        btnPrint1.Enabled = true;
                        ViewState["RecordCount"] = datTemp.Tables[0].Rows.Count;
                    }
                }
                else
                {
                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لعثور على أي معلومات") : ("No Information Found");
                    btnPrint1.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(btnSearch, btnSearch.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
                    return;
                }

            }
            ReportViewer.LocalReport.Refresh();
            this.ReportViewer.ZoomMode = ZoomMode.Percent;
            this.ReportViewer.ZoomPercent = 100;
            ReportViewer.Visible = true;
        }
    }
    /// <summary>
    /// Sub report event
    /// </summary>
    void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
    {
        e.DataSources.Add(new ReportDataSource("dtSetCompanyAsset_AssetUsageDamageInfo", datTemp.Tables[2]));
    }
    #endregion

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();
        FillAssets();
    }
    protected void ddlAssetType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAssets();
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
    }
   
    protected void btnSearch_Click1(object sender, EventArgs e)
    {

        if (ValidateReport())
            ShowAssetReport();

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer, this);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
        FillAssets();
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "-1";
    }

    public void SetDefaultWorkStatus(DropDownList ddl)
    {
        ddl.SelectedValue = "6";
    }
   
    protected void ReportViewer_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer.Visible = true;
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
}
