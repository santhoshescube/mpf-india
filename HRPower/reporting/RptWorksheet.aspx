﻿<%@ Page Title="Worksheet" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptWorksheet.aspx.cs" Inherits="reporting_RptWorksheet" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
    <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,WorkSheet%>'></asp:Label>
    </div>
   
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span>
            </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%" rowspan="2">
                            <table style="width: 100%" class="labeltext">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCompnay" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                    </td>
                                    <td>

                                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                     <td>
                                       <%-- Month-Year--%><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,MonthYear%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtMonthYear" runat ="server" AutoPostBack="True" Width="104px" onkeypress="return false;"></asp:TextBox>
                                        <asp:ImageButton ID ="imgMOnthYaer" runat ="server" ImageUrl="~/images/Calendar_scheduleHS.png" 
                                            CausesValidation="False"  />
                                        <AjaxControlToolkit:CalendarExtender ID ="ajxMonthYear" runat="server" TargetControlID ="txtMonthYear"  PopupButtonID ="imgMOnthYaer" Format ="MMM yyyy"></AjaxControlToolkit:CalendarExtender>
                                    </td>
                                    <td>
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                     <td>
                                        <%--  Work Status--%>
                                        <asp:Literal ID="lblWorkStatus" runat="server" Text="Work Status">
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="250px" onselectedindexchanged="ddlWorkStatus_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                      <td>
                                        <asp:Label ID="lblDepartment" runat="server" Font-Bold="False" Text='<%$Resources:ReportsCommon,Department %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <%--  Employee--%>
                                        <asp:Literal ID="Literal" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'>
                                             
                                        </asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="250px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" AutoPostBack="True" Checked="True"
                                            Font-Bold="False" OnCheckedChanged="chkIncludeCompany_CheckedChanged" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                            Width="150px" Font-Size="Small" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>'
                                OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print %>' OnClick="btnPrint_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
        <rsweb:ReportViewer ID="ReportViewer" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" OnPageNavigation="ReportViewer_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
