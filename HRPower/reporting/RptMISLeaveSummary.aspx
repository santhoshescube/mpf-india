﻿<%@ Page Title="Leave Summary Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptMISLeaveSummary.aspx.cs" Inherits="reporting_RptMISLeaveSummary" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
function test(sender,arg)
{
    var chkSummary = document.getElementById("ctl00_ctl00_content_report_content_chkSummary")
    var lblFromDate = document.getElementById("ctl00_ctl00_content_report_content_lblFromDate") 
    var lblToDate = document.getElementById("ctl00_ctl00_content_report_content_lblToDate") 
    var txtFromDate = document.getElementById("ctl00_ctl00_content_report_content_txtFromDate") 
    var txtToDate = document.getElementById("ctl00_ctl00_content_report_content_txtToDate") 
    var btnFromDate = document.getElementById("ctl00_ctl00_content_report_content_btnFromDate") 
    var btnToDate = document.getElementById("ctl00_ctl00_content_report_content_btnToDate") 
    var lblPeriod = document.getElementById("ctl00_ctl00_content_report_content_btnToDate") 
    var ddlPeriod = document.getElementById("ctl00_ctl00_content_report_content_ddlPeriod") 

if(lblFromDate != null && lblToDate != null && txtFromDate != null && txtToDate != null && btnFromDate != null
 && btnToDate != null && lblPeriod != null && ddlPeriod != null)
{

if(chkSummary.checked)
{
    lblFromDate.disabled = true
    lblToDate.disabled= true
    txtFromDate.disabled= true
    txtToDate.disabled= true
    btnFromDate.disabled= true
    btnToDate.disabled= true
    lblPeriod.disabled= false
    ddlPeriod.disabled= false
}
else
{ 
    lblFromDate.disabled = false
    lblToDate.disabled= false
    txtFromDate.disabled= false
    txtToDate.disabled= false
    btnFromDate.disabled= false
    btnToDate.disabled= false
    lblPeriod.disabled= true
    ddlPeriod.disabled= true

}
}
}


        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,LeaveSummary%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                 <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 770px">
                            <%--  <asp:UpdatePanel ID="upd" runat="server">
                                <ContentTemplate>--%>
                            <table style="width: 770px" class="labeltext">
                                <tr>
                                    <td style="width: 66px; padding-left: 4px">
                                        <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                    <asp:Literal id="Literal" runat ="server" Text ='<%$Resources:ReportsCommon,WorkStatus %>'>
                                    </asp:Literal>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlWorkstatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblFromDate" runat="server" Text='<%$Resources:ReportsCommon,FromDate %>'></asp:Label>
                                    </td>
                                    <td style="width: 118px;" valign="middle">
                                        <div id="divFromDate" runat="server" style="width: 118px">
                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="btnFromDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                CausesValidation="False" />
                                            <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                Format="dd-MMM-yyyy" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 66px; padding-left: 4px">
                                        <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblToDate" runat="server" Text='<%$Resources:ReportsCommon,ToDate %>'></asp:Label>
                                    </td>
                                    <td style="width: 118px;" valign="middle">
                                        <div id="divToDate" runat="server" style="width: 118px">
                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"></asp:TextBox>
                                            <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                CausesValidation="False" />
                                            <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtToDate" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 66px; padding-left: 4px">
                                       <%-- Department--%>
                                       <asp:Literal id="Literal1" runat ="server" Text ='<%$Resources:ReportsCommon,Department %>'>
                                    </asp:Literal>
                                    
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblPeriod" runat="server" Text='<%$Resources:ReportsCommon,Period %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlFinYear" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        &nbsp;
                                    </td>
                                    <td style="width: 118px;" valign="middle">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>' AutoPostBack="True"
                                            OnCheckedChanged="chkIncludeCompany_CheckedChanged" />
                                        <asp:CheckBox ID="chkFinSummary" runat="server" Text='<%$Resources:ReportsCommon,Summary %>' OnClick="test()" AutoPostBack="True"
                                            OnCheckedChanged="chkSummary_CheckedChanged" />
                                    </td>
                                </tr>
                            </table>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td style="width: 38px; margin: 0px; padding-left: 10px; vertical-align: middle">
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>' OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print %>' OnClick="btnPrint_Click" Enabled="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%">
    
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" SizeToReportContent="True"
            ShowPrintButton="False" ShowRefreshButton="False" Font-Names="Verdana" 
            Font-Size="8pt" AsyncRendering="false"
            Height="700px" onpagenavigation="ReportViewer1_PageNavigation">
            <%--   <LocalReport ReportPath="App_Code\rptEmployeeProfile.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsEmployee_dtEmployee" />
                </DataSources>
            </LocalReport>--%>
        </rsweb:ReportViewer>
        <%--<asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>--%>
    </div>
</asp:Content>
