﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Globalization;

public partial class reporting_RptDocument : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private clsCommon objCommon;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon","DocumentReport").ToString();
        if (!IsPostBack)
        {
            Initialize();
        }

        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
        txtFromDate.Text = clsReportCommon.GetMonthStartDate();
        txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
    }


    private void Initialize()
    {
        chkIncludeCompany.Checked = true;
        LoadCombos();
        chkExpiryDate.Checked = false;
        chkExpiryDate_CheckedChanged(new object(), new EventArgs());
    }


    private void LoadCombos()
    {

        FillAllCompany();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
        FillAllDocumentTypes();
        ddlDocumentType_SelectedIndexChanged(new object(), new EventArgs());
        ddlDocumentNo_SelectedIndexChanged(new object(), new EventArgs());
        FillDocumentStatus();

    }

    public void SetDefaultBranch(DropDownList cb)
    {
        cb.SelectedValue = "0";
    }



    /// <summary>
    /// Load all the companies
    /// </summary>
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataBind();
    }

    /// <summary>
    /// Load all the branches based on the company
    /// </summary>
    private void FillAllBranches()
    {
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(ddlCompany.SelectedValue.ToInt32());
        ddlBranch.DataBind();

        ddlBranch.SelectedValue = "0";
    }

    /// <summary>
    /// Load all document types
    /// </summary>
    private void FillAllDocumentTypes()
    {
        ddlDocumentType.DataTextField = "DocumentType";
        ddlDocumentType.DataValueField = "DocumentTypeID";
        ddlDocumentType.DataSource = clsReportCommon.GetAllDocumentTypes();
        ddlDocumentType.DataBind();

    }

    /// <summary>
    /// Load all document status
    /// </summary>
    private void FillDocumentStatus()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("DocumentStatusID");
        dt.Columns.Add("DocumentStatus");

        DataRow dr = null;
        dr = dt.NewRow();
        dr["DocumentStatusID"] = -1;
        dr["DocumentStatus"] = GetGlobalResourceObject("ReportsCommon", "Any").ToString();

        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["DocumentStatusID"] = 2;
        dr["DocumentStatus"] = GetGlobalResourceObject("ReportsCommon", "New").ToString();

        dt.Rows.Add(dr);



        dr = dt.NewRow();
        dr["DocumentStatusID"] = 1;
        dr["DocumentStatus"] = GetGlobalResourceObject("ReportsCommon", "Received").ToString();

        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["DocumentStatusID"] = 0;
        dr["DocumentStatus"] = GetGlobalResourceObject("ReportsCommon", "Issued").ToString();

        dt.Rows.Add(dr);


        ddlDocumentStatus.DataSource = dt;
        ddlDocumentStatus.DataTextField = "DocumentStatus";
        ddlDocumentStatus.DataValueField = "DocumentStatusID";
        ddlDocumentStatus.DataBind();
    }

    /// <summary>
    /// Load all document numbers based on the document typeid
    /// </summary>
    private void FillAllDocumentNumbers()
    {

        DataTable dtDistinctDocumentNumber = clsReportCommon.GetAllDocumentNumbers(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDocumentType.SelectedValue.ToInt32()).DefaultView.ToTable(true, "DocumentNumber");
        DataRow dr = dtDistinctDocumentNumber.NewRow();
        //dr[0] = -1;
        dr[0] = GetGlobalResourceObject("ReportsCommon", "Any").ToString();
        //dr[2] = -1;
        //dr[3] = DateTime.Now;
        //dr[4] = 1;
        //dr[5] = 200;

        dtDistinctDocumentNumber.Rows.InsertAt(dr, 0);

        ddlDocumentNo.DataTextField = "DocumentNumber";
        // ddlDocumentNo.DataValueField = "DocumentID";
        ddlDocumentNo.DataSource = dtDistinctDocumentNumber;
        ddlDocumentNo.DataBind();

    }

    /// <summary>
    /// Enable or disable include company check box based on the company ,branch and chkinclude company values
    /// </summary>
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    /// <summary>
    /// LOad all employees
    /// </summary>
    private void FillAllEmployees()
    {
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
            chkIncludeCompany.Checked, ddlDocumentType.SelectedValue.ToInt32(), ddlDocumentNo.SelectedValue.ToInt32() > 0 ? ddlDocumentNo.Text : "-1");
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";

        ddlEmployee.DataBind();


    }






























    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();

        FillAllDocumentNumbers();
    }


    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillAllEmployees();

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateReport())
            ShowReport();
    }

    protected void chkExpiryDate_CheckedChanged(object sender, EventArgs e)
    {
        EnableDisableDate();
    }

    /// <summary>
    /// Enabled or disable the date controls based on the checkbox expiry date value
    /// </summary>
    private void EnableDisableDate()
    {
        if (chkExpiryDate.Checked)
        {
            txtFromDate.Enabled = txtToDate.Enabled = true;

        }
        else
        {
            txtFromDate.Enabled = txtToDate.Enabled = false;
            txtFromDate.Text = txtToDate.Text = "";
        }
    }



    private void ShowMessage(string Message)
    {
        ScriptManager.RegisterClientScriptBlock(btnGo, btnGo.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
    }


    private bool ValidateReport()
    {
        //if (cboCompany.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(17612);
        //    return false;
        //}
        //else if (cboBranch.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(17613);
        //    return false;
        //}
        //else if (CboDocumentTypeID.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(10251);
        //    return false;
        //}
        //else if (CboDocumentNumber.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(17615);
        //    return false;
        //}
        //else if (CboEmployee.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(10250);
        //    return false;
        //}
        //else if (CboDocumentStatus.SelectedIndex < 0)
        //{
        //    UserMessage.ShowMessage(17617);
        //    return false;
        //}







        DateTime dtDate = System.DateTime.MinValue;
        DateTimeFormatInfo df = new DateTimeFormatInfo();
        df.ShortDatePattern = "dd-MMM-yyyy";



        if (chkExpiryDate.Checked)
        {
            if (txtFromDate.Text == "")
            {
                ShowMessage(GetGlobalResourceObject("ReportsCommon","PleaseSelectFromDate").ToString() );
                return false;
            }
            else if (!(DateTime.TryParse(txtFromDate.Text, out dtDate)))
            {
                ShowMessage(GetGlobalResourceObject("ReportsCommon","PleaseSelectValidDate").ToString());
                return false;
            }

            else if (txtToDate.Text != "" && (!(DateTime.TryParse(txtFromDate.Text, out dtDate))))
            {
                ShowMessage(GetGlobalResourceObject("ReportsCommon","PleaseSelectValidDate").ToString() );
                return false;
            }

            else if (txtFromDate.Text != "" && txtToDate.Text != "" && txtToDate.Text.ToDateTime().Date < txtFromDate.Text.ToDateTime().Date)
            {
                ShowMessage(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate").ToString());
                return false;
            }
        }

        return true;
    }



    public void ShowReport()
    {
        try
        {
            btnPrint1.Enabled = true;
            DateTime? FromDate, ToDate;
            DataSet DsDocuments = null;
            string strMReportPath = string.Empty;
            int Count = 0;
            //Setting the company header logo

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.Reset();

            FromDate = ToDate = null;
            //setting the from date and to date based on the expiry check box
            if (chkExpiryDate.Checked)
            {
                if (txtFromDate.Text != "")
                    FromDate = txtFromDate.Text.ToDateTime();

                if (txtToDate.Text != "")
                    ToDate = txtToDate.Text.ToDateTime();
            }


            if (ddlEmployee.SelectedValue.ToInt32() == -1 && ddlDocumentNo.SelectedIndex == 0)
            {
                strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptDocumentAllArb.rdlc") : Server.MapPath("reports/RptDocumentAll.rdlc");

                // Reports based on the document status value             
                if (ddlDocumentStatus.SelectedIndex > 0)
                {
                    if (ddlDocumentStatus.SelectedValue.ToInt32() == 1)
                        strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptDocumentsReceivedDetailsArb.rdlc"):Server.MapPath("reports/RptDocumentsReceivedDetails.rdlc");

                    else if (ddlDocumentStatus.SelectedValue.ToInt32() == 0)
                        strMReportPath =  clsGlobalization.IsArabicCulture() ?  Server.MapPath("reports/RptDocumentsIssuedDetailsArb.rdlc"): Server.MapPath("reports/RptDocumentsIssuedDetails.rdlc");
                }


                DsDocuments = clsDocumentReport.AllDocuments(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDocumentStatus.SelectedValue.ToInt32(), ddlDocumentType.SelectedValue.ToInt32(),
                   FromDate, ToDate
                   );



                if (DsDocuments.Tables[0].Rows.Count > 0)
                {   Count = 1;
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAllDocuments", DsDocuments.Tables[0]));
                   
                }

            }
            else if (ddlEmployee.SelectedValue.ToInt32() > 0)
            {
                strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptDocumentreportArb.rdlc") : Server.MapPath("reports/RptDocumentreport.rdlc");

                DataTable dtTemp = new DataTable();
                DsDocuments = clsDocumentReport.GetAllDocumentsByEmployee(ddlEmployee.SelectedValue.ToInt32(), ddlDocumentType.SelectedValue.ToInt32(), ddlDocumentNo.SelectedIndex == 0 ? "" : ddlDocumentNo.Text, FromDate, ToDate);
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));



                foreach (DataTable dt in DsDocuments.Tables)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Count = 1;
                        break;
                    }
                }


              



                if (DsDocuments.Tables[0].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeePassport", dtTemp));//Passport
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeePassport", DsDocuments.Tables[0]));//Passport



               


                if (DsDocuments.Tables[1].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeVisaDetails", dtTemp));//Visa
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeVisaDetails", DsDocuments.Tables[1]));//Visa




                if (DsDocuments.Tables[2].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtHealthCard", dtTemp));//Health Card
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtHealthCard", DsDocuments.Tables[2]));//Health Card



                if (DsDocuments.Tables[3].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmiratesHealthCard", dtTemp));//Emirated Card
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmiratesHealthCard", DsDocuments.Tables[3]));//Emirated Card



                if (DsDocuments.Tables[4].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLabourCard", dtTemp));//Labour Card
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtLabourCard", DsDocuments.Tables[4]));//Labour Card






                if (DsDocuments.Tables[5].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceCard", dtTemp));//Insurance Card
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtInsuranceCard", DsDocuments.Tables[5]));//Insurance Card



                if (DsDocuments.Tables[6].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeLicenceDetails", dtTemp));//License Details

                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeLicenceDetails", DsDocuments.Tables[6]));//License Details


                if (DsDocuments.Tables[7].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_Qualification", dtTemp));//Qualification Details
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_Qualification", DsDocuments.Tables[7]));//Qualification Details


                if (DsDocuments.Tables[8].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtOtherDocs", dtTemp));//Other Documents
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtOtherDocs", DsDocuments.Tables[8]));//Other Documents



                if (DsDocuments.Tables[9].Columns.Count == 1)
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeDet", dtTemp));//Other Documents
                else
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtEmployeeDet", DsDocuments.Tables[9]));//Other Documents
            }

            if (ddlDocumentNo.SelectedIndex > 0)
            {

                strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptSingleDocumentArb.rdlc"): Server.MapPath("reports/RptSingleDocument.rdlc");
                DsDocuments = clsDocumentReport.GetSingleDocumentDetails(ddlCompany.SelectedValue.ToInt16(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDocumentType.SelectedValue.ToInt32(), ddlDocumentNo.SelectedIndex > 0 ? ddlDocumentNo.Text : "-1", FromDate, ToDate);

                if (DsDocuments.Tables[0].Rows.Count > 0)
                {
                    Count = 1;
                    ViewState["Document"] = DsDocuments.Tables[1];
                    //ReportViewer1.LocalReport.SubreportProcessing -= new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.SetReportHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtSingleDocument", DsDocuments.Tables[0]));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dtDocuments_dtAllReceiptIssues", DsDocuments.Tables[1]));

                }

            }



           // ViewState["RecordCount"] = DsDocuments.Tables[0].Rows.Count;

            //No record found message 
            if (Count == 0)
            {
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(btnGo, btnGo.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
               
                // UserMessage.ShowMessage(17620);
                return;
            }
            else
                ViewState["RecordCount"] = Count;


            //Setting the report parameter for all the reports since it s common for all reports
            ReportViewer1.LocalReport.ReportPath = strMReportPath;
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                {
                    new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter), false)
                });

            ReportViewer1.LocalReport.Refresh();

            //Setting the report display

            // SetReportDisplay(ReportViewer1);
        }
        catch (Exception ex)
        {

        }
    }

    void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
    {
        DataTable dt = null;

        dt = ((DataTable)ViewState["Document"]);
        e.DataSources.Add(new ReportDataSource("dtDocuments_dtAllReceiptIssues", dt));

    }

    protected void ddlDocumentNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();


        if (ddlDocumentNo.SelectedIndex > 0)
        {
            ddlEmployee.SelectedIndex = 0;
            ddlDocumentStatus.SelectedIndex = 0;

            ddlEmployee.Enabled = ddlDocumentStatus.Enabled = false;
            chkExpiryDate.Checked = false;
            chkExpiryDate.Enabled = false;
        }
        else
        {
            ddlEmployee.Enabled = ddlDocumentStatus.Enabled = true;
            chkExpiryDate.Enabled = true;
        }
    }
    protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllDocumentNumbers();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllDocumentNumbers();
        FillAllEmployees();
    }
    protected void btnPrint1_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);

    }
}
