﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Globalization;

public partial class reporting_RptMISAttendance : System.Web.UI.Page
{
    private clsMISEmployeeReport objEmployee;
    private int intCompany = 0;
    private int intBranch = 0;
    private int intEmployee = 0;
    private string strStartDate = "";
    private string strEndDate = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "AttendanceReport").ToString();
        ReportViewer1.Visible = ibtnExcel.Visible = false; divData.Style["display"] = "none";
        //btnPrint1.Enabled = false;
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {

            
            chkIncludeCompany.Checked = true;
           
            txtDate.Text = new clsCommon().GetSysDate();
            txtDate_TextChanged(null, null);
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
         ddlMonth.Enabled = false;
    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllDesignation();

        FillAllWorkStatus();


        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
        FillMonthDropDown();

    }
    //private void EnableDisableIncludeCompany()
    //{
    //    //No branch selected
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }

    //}
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }

    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();

    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillMonthDropDown();
    }




    private void FillProjects()
    {


        string MonthYear = ddlMonth.SelectedValue;

        if (ddlMonth.SelectedValue != "")
        {
            string Month = MonthYear.Split('@')[0].ToString();
            string Year = MonthYear.Split('@')[1].ToString();
            int? MonthI = 0;
            DateTime? ProjectDate;

            if (ChkDate.Checked)
            {
                ProjectDate = txtDate.Text.ToDateTime();
                MonthI = null;
            }
            else
            {
                ProjectDate = null;
                MonthI = Month.ToInt32();
            }



            //ddlProject.DataSource = clsReportCommon.GetProjects(ddlEmployee.SelectedValue.ToInt16(), ProjectDate, MonthI, Year.ToInt32());
            //ddlProject.DataTextField = "ProjectName";
            //ddlProject.DataValueField = "ProjectID";
            //ddlProject.DataBind();
        }


    }
    //protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlType.SelectedIndex == 0)
    //    {
    //        lblMonth.Enabled = true;
    //        ddlMonth.Enabled = true;
    //        lblDate.Enabled = true;
    //        ChkDate.Enabled = true;
    //        btnDate.Enabled = true;
    //        ChkSummary.Enabled = true;
    //    }
    //}
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());


    }
    private void FillAllDesignation()
    {

        this.BindDropDown(ddlDesignation, "DesignationID", "Designation", clsReportCommon.GetAllDesignation());


    }
    private void FillAllWorkStatus()
    {

        this.BindDropDown(ddlworkStatus, "WorkStatusID", "WorkStatus", clsReportCommon.GetAllWorkStatus());


    }
    private void FillAllEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue == "" ? "0" : ddlBranch.SelectedValue);
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), -1, ddlworkStatus.SelectedValue.ToInt32(), -1
                                ));
        FillMonthDropDown();

    }
    public void FillMonthDropDown()
    {


        this.BindDropDown(ddlMonth, "MDate", "Month", clsReportCommon.GetMonthForAttendance(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32()
                                ));

        FillProjects();
    }
    #endregion

    private string GetCurrentMonth(int intMonth)
    {
        string strMonth = "";
        switch (intMonth)
        {
            case 1:
                strMonth = "Jan";
                break;
            case 2:
                strMonth = "Feb";
                break;
            case 3:
                strMonth = "Mar";
                break;
            case 4:
                strMonth = "Apr";
                break;
            case 5:
                strMonth = "May";
                break;
            case 6:
                strMonth = "Jun";
                break;
            case 7:
                strMonth = "Jul";
                break;
            case 8:
                strMonth = "Aug";
                break;
            case 9:
                strMonth = "Sep";
                break;
            case 10:
                strMonth = "Oct";
                break;
            case 11:
                strMonth = "Nov";
                break;
            case 12:
                strMonth = "Dec";
                break;
        }
        return strMonth;
    }
    private void GetStartEndDate()
    {
        string[] strMonthYear = null;
        DateTime dteDate;
        dteDate = Convert.ToDateTime(txtDate.Text);
        if (ddlMonth.SelectedIndex != -1)
        {
            strMonthYear = ddlMonth.SelectedValue.ToString().Split(Convert.ToChar("@"));
        }
        DateTime dteFromDate;
        if (ChkDate.Checked)
        {
            strStartDate = "01-" + GetCurrentMonth(dteDate.Month) + "-" + dteDate.Year.ToString();
            dteFromDate = Convert.ToDateTime(strStartDate);
            strEndDate = dteFromDate.AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
        }
        else
        {
            strStartDate = "01-" + ddlMonth.SelectedItem.Text;
            dteFromDate = Convert.ToDateTime(strStartDate);
            strStartDate = dteFromDate.ToString("dd-MMM-yyyy");
            strEndDate = dteFromDate.AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");
        }
    }






    private bool ValidateDate()
    {

        DateTime dtDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;

        if (ChkDate.Checked)
        {
            if (txtDate.Text == string.Empty)
            {
                strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate").ToString();
                blnIsValid = false;
            }
            else if (!DateTime.TryParse(txtDate.Text, df, DateTimeStyles.None, out dtDate))
            {
                strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate").ToString();
                blnIsValid = false;
            }
            else if (txtToDate.Text != "" && (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtDate)))
            {
                strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate").ToString();
                blnIsValid = false;
            }
        }

        if (ChkMonth.Checked == false && ChkDate.Checked == false)
        {
            if (txtToDate.Text == "")
            {
                strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectValid ToDate").ToString();
                blnIsValid = false;
            }

        }

        if (ChkDate.Checked == false && ddlMonth.SelectedIndex == -1)
        {
            strMessage = GetGlobalResourceObject("ReportsCommon", "PleaseSelectamonth").ToString();
            blnIsValid = false;
        }

        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport()
    {
        try
        {
            string strMReportPath = "";
            bool blnIsMonth = true;
            if (ChkDate.Checked)
                blnIsMonth = false;
            int intMonth = 0;
            int intYear = 0;
            DataTable dtCompany = null;
            // Check whether Month is Selected
            string MonthYear = ddlMonth.SelectedValue;
            // Check whether Month is Selected
            if (ddlMonth.SelectedValue != "")
            {
                intMonth = (MonthYear.Split('@')[0].ToString()).ToInt32();
                intYear = (MonthYear.Split('@')[1].ToString()).ToInt32();
            }
            this.ReportViewer1.Reset();
            //Clear Report Datasources
            this.ReportViewer1.LocalReport.DataSources.Clear();
            //Set Company Header For Report
            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);



            if (ChkSummary.Checked) // Attendance Summary Report
            {
               string  iToDate;

                if (txtToDate.Text == "")
                    iToDate = txtDate.Text.ToDateTime().ToString("dd-MMM-yyyy");
                else
                    iToDate = txtToDate.Text.ToDateTime().ToString("dd-MMM-yyyy");

                DataTable dtReport = clsMISEmployeeReport.GetSummaryReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), intMonth, intYear, txtDate.Text.ToDateTime().ToString("dd-MMM-yyyy"), iToDate);

                DataTable dt = clsMISEmployeeReport.GetProjectSummaryAttendance(intMonth, intYear);

                var ProjectAttendanceCount = dt.AsEnumerable().Select(t => t.Field<int>("Type").ToInt32() == 1).Count();

                if (ProjectAttendanceCount > 0)
                    ViewState["dtProjectAttendanceSummary"] = dt;
                else
                    ViewState["dtProjectAttendanceSummary"] = null;

                strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptAttendanceSummaryForaMonthArb.rdlc") : Server.MapPath("reports/RptAttendanceSummaryForaMonth.rdlc");
                this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                        new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                        new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                        new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                        new ReportParameter("Designation",ddlDesignation.SelectedItem.Text.Trim()),
                        new ReportParameter("WorkStatus",ddlworkStatus.SelectedItem.Text.Trim()),
                        new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),
                        new ReportParameter("Month",ddlMonth.SelectedItem.Text.Trim())
                        
                    });
                ViewState["RecordCount"] = dtReport.Rows.Count;
                if (dtReport.Rows.Count > 0)
                {

                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtSummary", dtReport));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                    btnPrint1.Enabled = true;
                }
                else
                {

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                    ReportViewer1.Visible = false;
                    btnPrint1.Enabled = false;
                    return;

                }



                ReportViewer1.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(LocalReport_SubreportProcessing);

            }
            else
            {
                if (ddlEmployee.SelectedValue.ToInt32() > 0 && ChkDate.Checked) // Punching Report
                {
                    DateTime? ToDate;

                    if (txtToDate.Text == "")
                        ToDate = null;
                    else
                        ToDate = txtToDate.Text.ToDateTime();

                    DataSet dsReport = clsMISEmployeeReport.GetPunchingReport(ddlEmployee.SelectedValue.ToInt32(), txtDate.Text.ToDateTime(), ToDate);


                    strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptAttendanceDailyPunchingArb.rdlc") : Server.MapPath("reports/RptAttendanceDailyPunching.rdlc");
                    this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))
                    });
                    ViewState["RecordCount"] = dsReport.Tables[1].Rows.Count;
                    if (dsReport.Tables[1].Rows.Count > 0)
                    {

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingGeneral", dsReport.Tables[0]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunching", dsReport.Tables[1]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingConsequences", dsReport.Tables[2]));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        btnPrint1.Enabled = true;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                        ReportViewer1.Visible = false;
                        btnPrint1.Enabled = false;
                        return;
                    }
                }
                else // Attendance Month Wise Report
                {
                    DateTime? ToDate;

                    if (txtToDate.Text == "")
                        ToDate = null;
                    else
                        ToDate = txtToDate.Text.ToDateTime();
                    DataTable dtReport = clsMISEmployeeReport.GetReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), ddlType.SelectedValue.ToInt32(), blnIsMonth, txtDate.Text.ToDateTime(), ToDate, intMonth, intYear, -1);//ddlProject.SelectedValue.ToInt32()
                    ViewState["RecordCount"] = dtReport.Rows.Count;
                    strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptAttendanceSummaryArb.rdlc") : Server.MapPath("reports/RptAttendanceSummary1.rdlc");
                    this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
                    ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                        new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                        new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                        new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                        new ReportParameter("Designation",ddlDesignation.SelectedItem.Text.Trim()),
                        new ReportParameter("WorkStatus",ddlworkStatus.SelectedItem.Text.Trim()),
                        new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),
                        new ReportParameter("IsMonth",blnIsMonth ? "1" : "0"),
                        new ReportParameter("Period",ChkDate.Checked ? txtDate.Text.ToDateTime().ToString("dd MMM yyyy") : ddlMonth.SelectedItem.Text.Trim()),
                        new ReportParameter("Reporton", ChkDate.Checked ? txtDate.Text.ToDateTime().ToString("dd MMM yyyy") : ddlMonth.SelectedItem.Text.Trim(), false)
                    });

                    if (dtReport.Rows.Count > 0)
                    {

                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtAttendance", dtReport));
                        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                        btnPrint1.Enabled = true;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                        ReportViewer1.Visible = false;
                        btnPrint1.Enabled = false;
                        return;
                    }
                }
            }
            this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.ReportViewer1.ZoomPercent = 100;
        }
        catch (Exception ex)
        {
        }
        // SetReportDisplay(ReportViewer1);
    }

    void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
    {
        DataTable dt = ((DataTable)ViewState["dtProjectAttendanceSummary"]);
        e.DataSources.Add(new ReportDataSource("DtsetAttendance_dtProjectAttendanceSummary", dt));
    }
    private void ShowAttendanceSummaryReport()
    {
        string strMReportPath = "";
        objEmployee = new clsMISEmployeeReport();

        ReportViewer1.Reset();
        decimal decTotalAmt = 0;
        int sChecked = 1;


        string strDate = "";
        string strToDate = "";
        string strPerid = "";
        string strEndDate = "";
        string[] MonthYear = new string[2];

        DataTable dtEmployeeAttendance = null; ;
        DataTable dtCompanyDetails = null;
        DataTable dtEntryExitDetails = null;

        int iMonth = 0;
        int iYear = 0;
        int intIncludeComp = chkIncludeCompany.Checked ? 1 : 0;
        int intBranchID = 0;
        int intCompanyID = ddlCompany.SelectedValue.ToInt32();
        int intEmployeeID = ddlEmployee.SelectedValue.ToInt32();

        int intLocationID = 0;
        string strTotalWorkTime = "00:00:00";
        string strTotalBreakTime = "00:00:00";
        string strTotalOt = "00:00:00";
        string strTotalAbsentTime = "00:00:00";
        string strTotalFoodBreakTime = "00:00:00";


        if (ddlBranch.SelectedValue.ToInt32() == -2)
            intBranchID = 0;
        else
            intBranchID = ddlBranch.SelectedValue.ToInt32();
        bool blnIsMonth = true;
        if (ChkDate.Checked)
            blnIsMonth = false;

        if (ChkDate.Checked)
        {
            iMonth = txtDate.Text.ToDateTime().Month;
            iYear = txtDate.Text.ToDateTime().Year;
            strDate = txtDate.Text.ToDateTime().ToString("dd-MMM-yyyy");
            strPerid = strDate;

        }
        else
        {
            if (ddlMonth.Text.ToStringCustom() != string.Empty && ddlMonth.SelectedValue.ToStringCustom() != string.Empty)
            {
                int iDaycount =0;
                MonthYear = ddlMonth.SelectedValue.ToStringCustom().Split('@');
                DateTime  istrDate = Convert.ToDateTime(txtDate.Text.ToDateTime().ToString("dd-MMM-yyyy"));
                iMonth = MonthYear[0].ToStringCustom().TrimStart().TrimEnd().ToInt32();
                iYear = MonthYear[1].ToStringCustom().TrimStart().TrimEnd().ToInt32();
                strDate = "01-" + DateAndTime.MonthName(iMonth, true) + "-" + iYear.ToString();
                //iDaycount = (istrDate.AddMonths(1) - istrDate).TotalDays.ToInt32();
                iDaycount = System.DateTime.DaysInMonth(iYear,iMonth);
                strToDate = iDaycount+"-"+ DateAndTime.MonthName(iMonth, true) + "-" + iYear.ToString();
                strPerid = strDate;
            }
            else
            {
                return;
            }

        }
        int intDayID = (int)Convert.ToDateTime(strDate).DayOfWeek + 1;


        if (ddlEmployee.SelectedItem.Text != "ALL")
        {
            if (ChkDate.Checked == false)
                sChecked = 0;

        }


        this.ReportViewer1.LocalReport.DataSources.Clear();


        if (chkIncludeCompany.Checked)
        {
            dtCompanyDetails = objEmployee.GetCompanyHeader(intCompanyID);
        }
        else
        {
            if (intBranchID > 0)
            {
                dtCompanyDetails = objEmployee.GetCompanyHeader(intBranchID);
            }
            else
            {
                dtCompanyDetails = objEmployee.GetCompanyHeader(intCompanyID);
            }
        }


        if (ChkSummary.Checked)
        {
            strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptAttendanceSummaryForaMonthArb.rdlc") : Server.MapPath("reports/RptAttendanceSummaryForaMonth.rdlc");

            if (ChkDate.Checked)
                strEndDate = strDate;
            else
                strEndDate = Convert.ToDateTime(strDate).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");

            dtEmployeeAttendance = clsMISEmployeeReport.GetSummaryReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), iMonth, iYear, strDate,strEndDate);
            if (dtEmployeeAttendance.Rows.Count > 0)
            {
                btnPrint1.Enabled = true;
            }
            else
            {
                btnPrint1.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);

            }

            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_AttendanceForaMonth", dtEmployeeAttendance));


            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                {                   
                    new ReportParameter("GeneratedBy",  "HRPOWER"), 
                    new ReportParameter("Reporton", strPerid, false)                   
                });

        }
        else
        {
            DateTime? ToDate;

            if (txtToDate.Text == "")
                ToDate = null;
            else
                ToDate = txtToDate.Text.ToDateTime();

            if (ddlEmployee.SelectedValue.ToInt32() > 0 && ChkDate.Checked)//punching report
            {

                DataSet dsReport = clsMISEmployeeReport.GetPunchingReport(ddlEmployee.SelectedValue.ToInt32(), txtDate.Text.ToDateTime(), ToDate);

                strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptAttendanceDailyPunchingArb.rdlc") : Server.MapPath("reports/RptAttendanceDailyPunching.rdlc");

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[]
                    {
                        new ReportParameter("GeneratedBy","HRPower")
                        //new ReportParameter("Date", strDate, false)
                        
                    });
                if (dsReport.Tables[1].Rows.Count > 0)
                {
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingGeneral", dsReport.Tables[0]));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunching", dsReport.Tables[1]));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtPunchingConsequences", dsReport.Tables[2]));
                    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompanyDetails));
                }
                else
                {
                    btnPrint1.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);

                }
            }
            else// Attendance Month Wise Report
            {

                int iMode = ChkDate.Checked ? 9 : 5;
                strMReportPath = Server.MapPath("reports/RptAttendanceSummaryNew.rdlc");

                string strIsOTShown = "1";

                if (ddlEmployee.SelectedItem.Text == "ALL" && ChkDate.Checked && !ChkSummary.Checked)
                    strIsOTShown = "0";

                dtEmployeeAttendance = clsMISEmployeeReport.GetEmployeeAttendnaceAllEmployee(iMode, intCompanyID, intBranchID, intEmployeeID, strDate,strToDate, intIncludeComp, 0);

                if (dtEmployeeAttendance.Rows.Count > 0)
                {
                    btnPrint1.Enabled = true;
                }
                else
                {
                    btnPrint1.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);

                }

                ReportViewer1.LocalReport.DataSources.Clear();
                this.ReportViewer1.LocalReport.ReportPath = strMReportPath;
                this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_EmployeeAttendance", dtEmployeeAttendance));

                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                { 
                  new ReportParameter("GeneratedBy",  "HRPOWER"),
                  new ReportParameter("CompanyID", Convert.ToString(intCompanyID), false),
                  new ReportParameter("Month", Convert.ToString(iMonth), false),
                  new ReportParameter("Year", Convert.ToString(iYear), false),
                  new ReportParameter("Companyname", ddlCompany.SelectedItem.Text.Trim(), false),
                  new ReportParameter("EmployeeID", intEmployeeID.ToString(), false),
                  new ReportParameter("Totalworktime", strTotalWorkTime, false),
                  new ReportParameter("Totalbreaktime", strTotalBreakTime, false),
                  new ReportParameter("Date", strDate, false),
                  new ReportParameter("totalfbreaktime", strTotalFoodBreakTime, false),
                  new ReportParameter("totalot", strTotalOt, false),
                  new ReportParameter("checked", Convert.ToString(sChecked), false),
                  new ReportParameter("sMonth", Convert.ToString(decTotalAmt), false),
                  new ReportParameter("Employee", ddlEmployee.SelectedItem.Text, true),
                  new ReportParameter("Reporton", strPerid, false),
                  new ReportParameter("TotalShortage", strTotalAbsentTime, false),
                  new ReportParameter("IsOTShown", strIsOTShown, false),
                  new ReportParameter("Company", ddlCompany.SelectedItem.Text.Trim(), false)           
                });
            }
        }
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_dtAttendance", dtEmployeeAttendance));
        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtsetAttendance_CompanyDetails", dtCompanyDetails));
        if (dtEmployeeAttendance.Rows.Count <= 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('No information found');", true);
            ReportViewer1.Visible = false;
            return;
        }
        else
            ReportViewer1.Height = 700;

        if (!File.Exists(strMReportPath))
        {
            return;
        }
        this.ReportViewer1.LocalReport.Refresh();
    }

    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        ReportViewer1.Visible = false;

        if (ChkSummary.Checked == true && ChkDate.Checked == true && ChkMonth.Checked == false)
        {
            if (ValidateDate())
            {
                
                if (ddlFillFormat.SelectedIndex == 0)
                {
                    ReportViewer1.Height = 700;
                    ShowReport();
                    ReportViewer1.Visible = true; 
                    ibtnExcel.Visible = false;
                }
                else
                {
                    ShowGridReport();
                    ReportViewer1.Visible = false;                   
                    ibtnExcel.Visible = true;
                }
            }
        }
        else if (ChkMonth.Checked == true)
        {
            if (ValidateDate())
            {
                ReportViewer1.Visible = true;
                if (ddlFillFormat.SelectedIndex == 0)
                {
                    ReportViewer1.Height = 700;
                    ShowAttendanceSummaryReport();
                    ReportViewer1.Visible = true;
                    ibtnExcel.Visible = false;
                }
                else
                {
                    ShowGridReport();
                    //ReportViewer1.Visible = false;
                    //ibtnExcel.Visible = true;
                }
            }
        }
        else if(ChkSummary.Checked == false || ChkDate.Checked == false || ChkMonth.Checked==false)
        {

            if (ValidateDate())
            {

                if (ddlFillFormat.SelectedIndex == 0)
                {
                    ReportViewer1.Height = 700;
                    ShowReport();
                    ReportViewer1.Visible = true;
                    ibtnExcel.Visible = false;
                }
                else
                {
                    ShowGridReport();
                    ReportViewer1.Visible = false;
                    ibtnExcel.Visible = true;
                }
            }

        }
    }

    protected void ChkDate_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkDate.Checked == true)
        {
            ddlMonth.Enabled = false;
            txtDate.Enabled =  true;
            ChkSummary.Enabled = true;
            ChkMonth.Checked = false;
            txtToDate.Enabled = false;
            txtToDate.Text = "";
        }
        else
        {
            ddlMonth.Enabled = false;
            txtDate.Enabled = txtToDate.Enabled = true;
            txtToDate.Enabled = true;
        }

        FillProjects();
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();


    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        if (ChkDate.Checked)
        {
            ddlMonth.Enabled = false;
            ChkSummary.Checked = false;
            ChkSummary.Enabled = false;
        }
        else
        {
            ddlMonth.Enabled = true;
            ChkSummary.Enabled = true;
        }
    }
    protected void ChkSummary_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkSummary.Checked)
            ddlType.Enabled = false;
        else
            ddlType.Enabled = true;

        //FillProjects();
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillProjects();
    }



    protected void gvPunchings_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {          
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {               
                if (e.Row.Cells[i].Text == "EmployeeNumber")
                    e.Row.Cells[i].Text = "Employee Number";
                if (e.Row.Cells[i].Text == "EmployeeName")
                    e.Row.Cells[i].Text = "Employee Name";
                if (e.Row.Cells[i].Text == "NormalWorkTime")
                    e.Row.Cells[i].Text = "Normal Work Time";
                if (e.Row.Cells[i].Text == "ActualWorkTime")
                    e.Row.Cells[i].Text = "Actual Work Time";
                if (e.Row.Cells[i].Text == "FirstPunchIn")
                    e.Row.Cells[i].Text = "First Punch In";
                if (e.Row.Cells[i].Text == "LastPunchOut")
                    e.Row.Cells[i].Text = "Last Punch Out";
                if (e.Row.Cells[i].Text == "BreakTime")
                    e.Row.Cells[i].Text = "Break Time";
                if (e.Row.Cells[i].Text == "OT")
                    e.Row.Cells[i].Text = "OverTime";
            }
        }
    }

    protected void gvPunchings_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPunchings.PageIndex = e.NewPageIndex;
        ShowGridReport();
    }

    private void ShowGridReport()
    {
        try
        {
            bool blnIsMonth = true;
            if (ChkDate.Checked==true || ChkMonth.Checked==false)
                blnIsMonth = false;


            DateTime? ToDate;

            if (txtToDate.Text == "")
                ToDate = null;
            else
                ToDate = txtToDate.Text.ToDateTime();

            int intMonth = 0;
            int intYear = 0;
            string MonthYear = ddlMonth.SelectedValue;
            // Check whether Month is Selected
            if (ddlMonth.SelectedValue != "")
            {
                intMonth = (MonthYear.Split('@')[0].ToString()).ToInt32();
                intYear = (MonthYear.Split('@')[1].ToString()).ToInt32();
            }

            DataTable datTemp = new DataTable();
            datTemp = clsMISEmployeeReport.GetReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlworkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), ddlType.SelectedValue.ToInt32(), blnIsMonth, txtDate.Text.ToDateTime(), ToDate, intMonth, intYear, -1);

            if (datTemp.Rows.Count > 0)
            {
                for (int i = 0; i < 7; i++)
                {
                    datTemp.Columns.RemoveAt(12);
                    datTemp.AcceptChanges();
                }
                if (ChkSummary.Checked)
                {
                    for (int i = 0; i < 6; i++)
                    {
                        datTemp.Columns.RemoveAt(2);
                        datTemp.AcceptChanges();
                    }
                     DataTable dtClone = datTemp.Clone();
                     DataRow dtNewRow;

                    double awt = 0, bt = 0, st = 0, ot = 0;
                    string empNo = "", empName = "";
                    var x = (from r in datTemp.AsEnumerable() select r["EmployeeNumber"]).Distinct().ToList();

                    foreach (var eno in x)
                    {
                        DataTable dttemp = new DataTable();
                        dttemp = datTemp.Clone();
                         
                        DataRow[] rowsToCopy;
                        rowsToCopy = datTemp.Select("EmployeeNumber='" + eno + "'");

                        foreach (DataRow temp in rowsToCopy)
                        {
                            dttemp.ImportRow(temp);
                        }

                        for (int iCounter = 0; iCounter < dttemp.Rows.Count; iCounter++)
                        {
                            empNo = dttemp.Rows[iCounter]["EmployeeNumber"].ToString();
                            empName = dttemp.Rows[iCounter]["EmployeeName"].ToString();
                            awt = awt + (dttemp.Rows[iCounter]["ActualWorkTime"].ToString().Replace(":", ".")).ToDouble();
                            st = st + (dttemp.Rows[iCounter]["Shortage"].ToString().Replace(":", ".")).ToDouble();
                            bt = bt + (dttemp.Rows[iCounter]["BreakTime"].ToString().Substring(0, 5).Replace(":", ".")).ToDouble();
                            ot = bt + (dttemp.Rows[iCounter]["OT"].ToString().Substring(0, 5).Replace(":", ".")).ToDouble();
                        }

                        dtNewRow = dtClone.Rows.Add();
                        dtNewRow.SetField("EmployeeNumber", empNo);
                        dtNewRow.SetField("EmployeeName", empName);
                        dtNewRow.SetField("ActualWorkTime", awt);
                        dtNewRow.SetField("BreakTime", bt);
                        dtNewRow.SetField("Shortage", st);
                        dtNewRow.SetField("OT", ot);
                    }
                    //dtClone.Rows.Add(dtNewRow);
                    datTemp = dtClone;
                }                
                    gvPunchings.DataSource = datTemp;
                    gvPunchings.DataBind();                

                DataTable dt = new DataTable();
                int ColCount = gvPunchings.Rows[0].Cells.Count;
                for (int i = 0; i < ColCount - 1; i++)
                {
                    dt.Columns.Add(gvPunchings.HeaderRow.Cells[i].Text.ToString());
                }
                foreach (DataRow row in datTemp.Rows)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < ColCount - 1; j++)
                    {
                        dr[gvPunchings.HeaderRow.Cells[j].Text.ToString()] = row[j].ToString();
                    }
                    dt.Rows.Add(dr);
                }
                ViewState["datTemp"] = dt;

                divData.Style["display"] = "block";

                divData.Style["max-height"] = (datTemp.Rows.Count > 5) ? "680px" : "auto";
                divData.Style["height"] = "";
                ReportViewer1.Visible = false;
                ibtnExcel.Visible = true;
            }
            else {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
                ReportViewer1.Visible = false;
                ReportViewer1.LocalReport.DataSources.Clear();
                btnPrint1.Enabled = ibtnExcel.Visible = false;
                return;
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + GetGlobalResourceObject("ReportsCommon", "NoInformationFound").ToString() + "');", true);
            ReportViewer1.Visible = false;
            ReportViewer1.LocalReport.DataSources.Clear();
            btnPrint1.Enabled = false;
            return;
        }
    }

    protected void ibtnExcel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dtTemp = (DataTable)ViewState["datTemp"];
            if (dtTemp == null) return;
            if (dtTemp.Rows.Count == 0) return;

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=LiveAttendanceExport.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            GridView gridView = new GridView();
            gridView.DataSource = dtTemp;
            gridView.DataBind();

            gridView.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
        catch
        {
        }

    }
    protected void ChkMonth_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkMonth.Checked == true)
        {
            ddlMonth.Enabled = true;
            txtDate.Enabled = txtToDate.Enabled = false;
            ChkSummary.Enabled = false;
            ChkSummary.Checked = false;
            ChkDate.Checked = false;
        }
        else
        {
            ddlMonth.Enabled = false;
            txtDate.Enabled = txtToDate.Enabled = true;
            
           
           
           
        }

       

    }
}
