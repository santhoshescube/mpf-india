﻿<%@ Page Title="Document Report" Language="C#" MasterPageFile="~/Master/ReportmasterPage.master" AutoEventWireup="true" CodeFile="RptDocument.aspx.cs" Inherits="reporting_RptDocument" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" Runat="Server">

     <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
     <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,Documents%>'></asp:Label>
    </div>
       <div class="report-filter">
        <fieldset class="report-fs">
             <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                  <asp:Literal ID="Literal8" runat="server"  Text='<%$Resources:ReportsCommon,HideFilter%>'>
                </asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
    <table id="filters" width="100%">
                    <tr>
                        <td  style="width: 80%">
                           
                                    <table id="tblEmployee" style="width :100%" runat="server" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal57" runat="server"  Text='<%$Resources:ReportsCommon,Company%>'></asp:Literal></td>
                                            <td style="width: 150px">
                                                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="150px"
                                                    AutoPostBack="True" 
                                                    />
                                            </td>
                                            <td style="width: 98px">
                                               <asp:Literal ID="Literal1" runat="server"  Text='<%$Resources:ReportsCommon,DocType%>'></asp:Literal></td>
                                            <td>
                                                <asp:DropDownList ID="ddlDocumentType" runat="server" CssClass="dropdownlist" 
                                                    Width="125px" AutoPostBack ="true" 
                                                    onselectedindexchanged="ddlDocumentType_SelectedIndexChanged" />
                                            </td>
                                            <td>
                                            <asp:Literal ID="Literal2" runat="server"  Text='<%$Resources:ReportsCommon,Employee%>'></asp:Literal>
                                            </td>
                                            
                                            <td>
                                            <asp:DropDownList ID="ddlEmployee" runat ="server"  >
                                            </asp:DropDownList> 
                                            </td>
                                          
                                            
                                   
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal3" runat="server"  Text='<%$Resources:ReportsCommon,Branch%>'></asp:Literal></td>
                                            <td style="width: 153px">
                    
                                              <asp:DropDownList ID="ddlBranch" Width ="150px" runat ="server"  AutoPostBack ="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" 
                                                 >
                                              </asp:DropDownList> 
                                              
                                               </td>
                                            
                                            <td>
                                            <asp:Literal ID="Literal4" runat="server"  Text='<%$Resources:ReportsCommon,DocNo%>'></asp:Literal>
                                            </td>
                                            
                                            <td>
                                            <asp:DropDownList ID="ddlDocumentNo" Width="125px"  runat ="server"  AutoPostBack ="true" 
                                                    onselectedindexchanged="ddlDocumentNo_SelectedIndexChanged" ></asp:DropDownList>
                                            </td>
                                            
                                            <td>
                                            <asp:Literal ID="Literal5" runat="server"  Text='<%$Resources:ReportsCommon,Status%>'></asp:Literal>
                                            </td>
                                            
                                            <td>
                                            <asp:DropDownList ID="ddlDocumentStatus" runat ="server" ></asp:DropDownList>
                                            </td>
                                           
                                        </tr>
                                        <tr>
                                        <td colspan ="2"> 
                                        <asp:CheckBox ID="chkIncludeCompany" Text ='<%$Resources:ReportsCommon,IncludeCompany%>' runat ="server" 
                                                oncheckedchanged="chkIncludeCompany_CheckedChanged" AutoPostBack ="true"  />
                                        
                                        </td>
                                        </tr>
                                    </table>
                               
                        </td>
                        
                        
                        <td rowspan ="3">
                       <%-- <asp:UpdatePanel ID="updExpiry" runat ="server" >
                        <ContentTemplate >--%>
                             <div class ="labeltext" style ="border :solid 1px;width:165px;height:88px">
                                                <table>
                                                    <tr>
                                                        <td colspan ="2">
                                                       <asp:CheckBox ID="chkExpiryDate" runat="server" Text ='<%$Resources:ReportsCommon,DocumentExpiry%>'
                                                                oncheckedchanged="chkExpiryDate_CheckedChanged" AutoPostBack ="true"  />
                                                       
                                                        </td>
                                                       
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                       <asp:Literal ID="Literal6" runat="server"  Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Literal>
                                                        </td>
                                                        <td>
                                                        <AjaxControlToolkit:CalendarExtender ID="FromDate" TargetControlID ="txtFromDate" runat ="server" Format ="dd-MMM-yyyy"></AjaxControlToolkit:CalendarExtender>
                                                        <asp:TextBox ID="txtFromDate" Width ="82px" runat="server" ></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td>
                                                        <asp:Literal ID="Literal7" runat="server"  Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Literal>
                                                        </td>
                                                        <td>
                                                          <AjaxControlToolkit:CalendarExtender ID="ToDate" TargetControlID ="txtToDate" runat ="server" Format ="dd-MMM-yyyy"></AjaxControlToolkit:CalendarExtender>

                                                        <asp:TextBox ID="txtToDate" Width ="82px" runat="server" ></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                            <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                        
                        </td> 
                        
                        
                        <td >
                           <table>
                           <%--     <tr>
                                    <td>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                              Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnGo_Click" />
                                     
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" 
                                                    ImageUrl ="~/images/print_IconNew.png" ToolTip='<%$Resources:ReportsCommon,print%>' 
                                                    Text='<%$Resources:ReportsCommon,print%>'  Enabled="true" onclick="btnPrint1_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                  
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                               </div>
        </fieldset>
    </div>
      
       
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode ="Percent" ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px">
           <%-- <LocalReport ReportPath="App_Code\rptEmployeeProfile.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsEmployee_dtEmployee" />
                </DataSources>
            </LocalReport>--%>
        </rsweb:ReportViewer>
       <%-- <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>--%>
    </div>
</asp:Content>

