﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Globalization;
public partial class reporting_RptSettlement : System.Web.UI.Page
{
    private int intCompany = 0;
    private int intBranch = 0;
    string strMReportPath = "";

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "SettlementReport").ToString();
        ReportViewer1.Visible = false;
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);
        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
           // txtStartdate.Text = clsReportCommon.GetMonthStartDate();
            //txtEndDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillAllDesignation();
        FillAllWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
    }
    private void FillAllWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
    }

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }

    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }

    private void FillAllDesignation()
    {

        this.BindDropDown(ddlDesignation, "DesignationID", "Designation", clsReportCommon.GetAllDesignation());


    }
    //private void FillAllEmployees()
    //{
    //    intCompany = Convert.ToInt32(ddlCompany.SelectedValue==""?"0":ddlCompany.SelectedValue);
    //    intBranch = Convert.ToInt32(ddlBranch.SelectedValue==""? "0" : ddlBranch.SelectedValue );
    //    DataTable dt = clsReportCommon.GetAllEmployees(intCompany, intBranch,
    //                            chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32()
    //                            );

    //      ddlEmployee.DataSource = dt;
    //      ddlEmployee.DataTextField = "EmployeeFullName";
    //      ddlEmployee.DataValueField = "EmployeeID";
    //      ddlEmployee.DataBind();
    //}
    private void FillAllEmployees()
    {
        int CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        DataTable dt = new DataTable();
        dt = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), true, -1, -1, -1, ddlWorkStatus.SelectedValue.ToInt32());
        dt.Rows.RemoveAt(0);
        dt.AcceptChanges();
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    #endregion
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        //if (ValidateDate())
        //{
            ReportViewer1.Visible = true;
            ShowReport();
        //}
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    //private bool ValidateDate()
    //{
    //    DateTime dtFromDate = System.DateTime.MinValue;
    //    DateTime dtToDate = System.DateTime.MinValue;

    //    DateTimeFormatInfo df = new DateTimeFormatInfo();

    //    df.ShortDatePattern = "dd MMM yyyy";

    //    bool blnIsValid = true;
    //    string strMessage = string.Empty;


    //    if (txtStartdate.Text == string.Empty)
    //    {
    //        strMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى تحديد من التسجيل") : ("Please Select From Date"); //"Please Select From Date";
    //        blnIsValid = false;
    //    }
    //    else if (!DateTime.TryParse(txtStartdate.Text, df, DateTimeStyles.None, out dtFromDate))
    //    {
    //        strMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء الإختيار تاريخ صالح") : ("Please Select Valid Date"); //"Please Select Valid Date";
    //        blnIsValid = false;
    //    }
    //    else if (txtEndDate.Text == string.Empty)
    //    {
    //        strMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء الإختيار إلى تاريخ") : ("Please Select To Date"); //"Please Select To Date";
    //        blnIsValid = false;
    //    }
    //    else if (!DateTime.TryParse(txtEndDate.Text, df, DateTimeStyles.None, out dtToDate))
    //    {
    //        strMessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء الإختيار تاريخ صالح") : ("Please Select Valid Date"); //"Please Select Valid Date";
    //        blnIsValid = false;
    //    }



    //    if (!blnIsValid)
    //    {
    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
    //        return false;
    //    }
    //    return true;
    //}
    private void ShowReport()
    {
        try
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            //DataTable dtCompany = null;
            //DataTable dtsettlement = null;
            //DataTable dtsettlementDetails = new DataTable();

            DataTable datSettDtls;
            DataTable datLeaveDtls;
            DataTable datGLADtls;
            DataTable datPayEmployeeSettlementGratuity;
            DataTable datLastDtls;
            DataSet DTSet;

            int HasSalary;
            int Pscale;

            this.ReportViewer1.Reset();
            this.ReportViewer1.LocalReport.DataSources.Clear();

            //strMReportPath = ddlEmployee.SelectedValue.ToInt32() == -1 ? Server.MapPath("reports/RptSettlement.rdlc") :
                                                                        //Server.MapPath("reports/RptEmployeeSettlement.rdlc");
            strMReportPath = clsGlobalization.IsArabicCulture() ? Server.MapPath("reports/RptGratuityReportArb.rdlc") :
                                                                  Server.MapPath("reports/RptGratuityReport.rdlc");
            //Setting ReportPath
            this.ReportViewer1.LocalReport.ReportPath = strMReportPath;

            DTSet = clsRptVactionAndSettlement.EmpGratuityReport(ddlEmployee.SelectedValue.ToInt32());

            HasSalary = 0;

            datSettDtls = DTSet.Tables[0];
            datLeaveDtls = DTSet.Tables[1];
            datGLADtls = DTSet.Tables[2];
            datPayEmployeeSettlementGratuity = DTSet.Tables[3];
            datLastDtls = DTSet.Tables[4];



            ReportParameter[] RepParam = new ReportParameter[1];
            RepParam[0] = new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter));
            ReportViewer1.LocalReport.SetParameters(RepParam);

            //DataTable dtCompany = Report.GetReportHeader(datSettDtls.Rows[0]["CompanyID"].ToInt32(), 0, true);

             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_SettDtls", datSettDtls));
             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LeaveDtls", datLeaveDtls));
             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_GLADtls", datGLADtls));
             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_PayEmployeeSettlementGratuity", datPayEmployeeSettlementGratuity));
             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetGrauityRpt_LastDtls", datLastDtls));
             this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader",clsReportCommon.GetCompanyHeader( ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));

             ViewState["RecordCount"] = DTSet.Tables[0].Rows.Count;
             if (DTSet.Tables[0].Rows.Count > 0)
             {
                 btnPrint.Enabled = true;
             }
             else
             {
                 string msg = clsGlobalization.IsArabicCulture() ? "العثور على أي معلومات" : "No information found";
                 ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);
                 ReportViewer1.Visible = false;
                 btnPrint.Enabled = false;
                 return;
             }

            //if (ddlEmployee.SelectedIndex > 0)
            //{

            //    dtsettlementDetails = clsRptVactionAndSettlement.GetEmpSettlementDetails(ddlEmployee.SelectedValue.ToInt32());
            //    dtsettlement = clsRptVactionAndSettlement.GetEmpSettlement(ddlEmployee.SelectedValue.ToInt32());

            //    HasSalary = 0;
            //    if (dtsettlement.Rows[0]["Scale"] == System.DBNull.Value)
            //    {
            //        Pscale = 2;
            //    }
            //    else
            //    {
            //        Pscale = Convert.ToInt32(dtsettlement.Rows[0]["Scale"]);
            //    }

            //    ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
            //                    {
            //                     new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
            //                     new ReportParameter("Scale", Pscale.ToString(),false ),
            //                     new ReportParameter("HasSalary", HasSalary.ToString(),false ),
            //                     new ReportParameter("bComDisplay", "true",false  )
                                 
            //                   });


            //    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlement", dtsettlement));
            //    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptDtSetEmployeeSettlement_EmployeeSettlementDetail", dtsettlementDetails));
            //    this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", clsReportCommon.GetCompanyHeader(dtsettlement.Rows[0]["CompanyID"].ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked)));

            //    this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            //    this.ReportViewer1.ZoomPercent = 100;
            //    return;


            //}

            //if (txtStartdate.Text != "")
            //    FromDate = Convert.ToDateTime(txtStartdate.Text, System.Globalization.CultureInfo.InvariantCulture);

            //if (txtEndDate.Text != "")
            //    ToDate = Convert.ToDateTime(txtEndDate.Text, System.Globalization.CultureInfo.InvariantCulture);


            //if (ToDate < FromDate)
            //{
            //    string msg = clsGlobalization.IsArabicCulture() ? "حتى الآن يجب أن تكون أكبر من من تاريخ" : "To date must be greater than from date";
            //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);                
            //    return;
            //}

            //Company Header
            //dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
            //Main Report
            //dtsettlement = clsRptVactionAndSettlement.GetSettlement(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), ddlDesignation.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), chkIncludeCompany.Checked, FromDate, ToDate, ddlType.SelectedValue.ToInt32());
            ////Detailed Report for a specific employee
            //if (ddlEmployee.SelectedValue.ToInt32() > 0)
            //    dtsettlementDetails = clsRptVactionAndSettlement.GetSettlementDetails(ddlEmployee.SelectedValue.ToInt32(), FromDate, ToDate, ddlType.SelectedValue.ToInt32());
            //if (dtsettlement != null)
            //{
            //    ViewState["RecordCount"] = dtsettlement.Rows.Count;
            //    if (dtsettlement.Rows.Count > 0)
            //    {

            //        ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
            //                    {
            //                     new ReportParameter("GeneratedBy",new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
            //                     new ReportParameter("Company", ddlCompany.SelectedItem.Text),
            //                     new ReportParameter("Branch", ddlBranch.SelectedItem.Text),
            //                     new ReportParameter("Department", ddlDepartment.SelectedItem.Text),
            //                     new ReportParameter("Designation", ddlDesignation.SelectedItem.Text),
            //                     new ReportParameter("Employee",ddlEmployee.SelectedItem.Text),
            //                     new ReportParameter("Type",ddlType.SelectedItem.Text),
            //                     new ReportParameter("FromDate",txtStartdate.Text),
            //                     new ReportParameter("ToDate",txtEndDate.Text)
            //                   });

            //        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsVacationSettlement_Settlement", dtsettlement));
            //        if (ddlEmployee.SelectedValue.ToInt32() > 0)
            //        {
            //            this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsVacationSettlement_dtSettlementDetails", dtsettlementDetails));
            //        }
            //        this.ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
            //        btnPrint.Enabled = true;
            //    }
            //    else
            //    {
            //        string msg = clsGlobalization.IsArabicCulture() ? "العثور على أي معلومات" : "No information found";
            //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + msg + "');", true);                    
            //        ReportViewer1.Visible = false;
            //        btnPrint.Enabled = false;
            //        return;
            //    }
            //}
            this.ReportViewer1.ZoomMode = ZoomMode.Percent;
            this.ReportViewer1.ZoomPercent = 100;
        }
        catch (Exception ex)
        {
        }
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
}
