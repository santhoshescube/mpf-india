﻿<%@ Page Title="Salary Slip Report" Language="C#" MasterPageFile="~/Master/ESSMaster.master"
    AutoEventWireup="true" CodeFile="~/reporting/PaySlipForEmp.aspx.cs" Inherits="reporting_PaySlipForEmp" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
        var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');
            });

        });
        
    </script>
    
    <div class="report-filter">
        <fieldset class="report-fs grid">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="filters" width="100%">
                    <tr>
                        <td valign="bottom">
                            <asp:UpdatePanel ID="upd1" runat="server">
                                <ContentTemplate>
                                    <table id="tblEmployee" runat="server" class="labeltext" width="100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" Width="200px"
                                                    AutoPostBack="True" Visible="False" />
                                            </td>
                                            <td>
                                                 <asp:Literal ID="Literal14" runat ="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>   
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px"/>
                                            </td>
                                            <td>
                                                 <asp:Literal ID="Literal1" runat ="server" Text='<%$Resources:ReportsCommon,MonthYear%>'></asp:Literal>   
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCurrentMonth" runat="server" Width="85px"
                                                    MaxLength="11" OnTextChanged="txtToDate_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CausesValidation="False" />
                                                <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                    Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtCurrentMonth" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td valign="bottom">
                            <table>
                                <tr>
                                    <td style="width: 50px">
                                        <asp:Button ID="btnGo" runat="server" class="btnsubmit" CausesValidation="false"
                                             Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnGo_Click" />                                        
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="upd" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>'  Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" Enabled="False" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>   
    
    <div style="clear: both; height:auto; width: 100%">
              <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" Runat="Server">
 <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           <div id="sidebarsettings">
                    <div class="sideboxImage">                  
                        <asp:ImageButton ID="imgEdit" ImageUrl="~/images/New_Employee_Big.PNG" 
                            runat="server" />
                        </div>
                    <div class="name">
                        <h5> <asp:LinkButton ID="lnkEdit" runat="server" onclick="lnkEdit_Click" Text='<%$Resources:ReportsCommon,ViewProfile%>'>
                            </asp:LinkButton>   </h5>                    
                    </div>
                </div>
        </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>