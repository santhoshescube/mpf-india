<%@ Page Title="Salary Advance Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptSalaryAdvance.aspx.cs" Inherits="reporting_RptSalaryAdvance" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {

            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');

            });

        });
        
    </script>

    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                Hide Filter</span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl" style="width:100%">
                    <tr>
                        <td style="width: 100%">
                            <asp:UpdatePanel ID="upd" runat="server">
                                <ContentTemplate>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompnay" runat="server" Text="Company"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="170px" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="170px" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="170px">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td>
                                                <div id="divFromDate" runat="server">
                                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"></asp:TextBox>
                                                    <asp:ImageButton ID="btnFromDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CausesValidation="False" />
                                                    <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                        Format="dd-MMM-yyyy" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" />
                                                </div>
                                            </td>
                                         
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    Width="170px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblType" runat="server" Text="Type"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="dropdownlist_mandatory"
                                                    Width="170px" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" Enabled="False">
                                                    <asp:ListItem Selected="True" Value="0">Salary Advance Summary</asp:ListItem>
                                                    <asp:ListItem Value="1">Loan Details</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td>
                                                <div id="divToDate" runat="server">
                                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"></asp:TextBox>
                                                    <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CausesValidation="False" />
                                                    <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                        Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtToDate" />
                                                </div>
                                             </td>
                                        </tr>
                                        <tr>
                                           <td colspan="8">
                                                <asp:CheckBox ID="chkIncludeCompany" runat="server" Text="Include Company" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td style="vertical-align: middle">
                    
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text="Go" OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip="Print" Text="Print" OnClick="btnPrint_Click" Enabled="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" ZoomMode="PageWidth"
            SizeToReportContent="True" ShowPrintButton="False" ShowRefreshButton="False"
            Font-Names="Verdana" Font-Size="8pt" Height="400px">
            <LocalReport ReportPath="App_Code\rptEmployeeProfile.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsEmployee_dtEmployee" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>
</asp:Content>
