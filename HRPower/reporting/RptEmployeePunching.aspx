﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true"
    CodeFile="RptEmployeePunching.aspx.cs" Inherits="reporting_RptEmployeePunching"
    Title="Employee Punching report" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية' : 'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي' : 'Hide Filter' : hidControl.value == 'ar-AE' ? 'عرض تصفية' : 'Show Filter');

            });

        });
        
    </script>

    <div style="padding-left: 350px;">
        <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,LiveAttendance%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span>
            </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 770px">
                            <%--  <asp:UpdatePanel ID="upd" runat="server">
                                <ContentTemplate>--%>
                            <table style="width: 770px" class="labeltext">
                                <tr>
                                    <td style="width: 66px; padding-left: 4px">
                                        <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Literal ID="Literal" runat="server" Text='<%$Resources:ReportsCommon,WorkStatus %>'>
                                        </asp:Literal>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlWorkstatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblFromDate" runat="server" Text='<%$Resources:ReportsCommon,FromDate %>'></asp:Label>
                                    </td>
                                    <td style="width: 118px;" valign="middle">
                                        <div id="divFromDate" runat="server" style="width: 118px">
                                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"
                                                AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                                            <asp:ImageButton ID="btnFromDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                CausesValidation="False" />
                                            <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                Format="dd-MMM-yyyy" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 66px; padding-left: 4px">
                                        <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblToDate" runat="server" Text='<%$Resources:ReportsCommon,ToDate %>'></asp:Label>
                                    </td>
                                    <td style="width: 118px;" valign="middle">
                                        <div id="divToDate" runat="server" style="width: 118px">
                                            <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox" Width="85px" MaxLength="11"
                                                Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                CausesValidation="False" Enabled="False" />
                                            <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtToDate" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,Department %>'>
                                        </asp:Literal>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 77px; padding-left: 4px">
                                        <asp:Label ID="lblPeriod" runat="server" Text='<%$Resources:ReportsCommon,Designation %>'></asp:Label>
                                    </td>
                                    <td style="width: 160px;">
                                        <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="padding-left: 4px" colspan="2">
                                      
                                    </td>
                                    <td valign="middle">
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany %>'
                                            AutoPostBack="True" OnCheckedChanged="chkIncludeCompany_CheckedChanged" />
                                    </td>
                                    <td style="width:100px; padding-left: 4px;">
                                           <asp:Literal ID="Literal2" Visible ="true" runat ="server" Text ='<%$Resources:ReportsCommon,DisplayFormat %>' >
                                              </asp:Literal>
                                    </td>
                                    <td colspan="3">
                                         <asp:DropDownList ID="ddlFillFormat" runat="server" CssClass="dropdownlist_mandatory"
                                            AutoPostBack="true" EnableViewState="true" Width="180px">
                                            <asp:ListItem Selected="True" Value="0" Text='<%$Resources:ReportsCommon,GraphicFormat %>'></asp:ListItem>
                                            <asp:ListItem Value="1" Text='<%$Resources:ReportsCommon,GridFormat %>'></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    
                                  
                                           
                                </tr>
                            </table>
                            <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                        <td style="width: 38px; margin: 0px; padding-left: 10px; vertical-align: middle">
                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" Text='<%$Resources:ReportsCommon,Go %>'
                                OnClick="btnSearch_Click1" />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                        ToolTip='<%$Resources:ReportsCommon,Print %>' OnClick="btnPrint_Click" Enabled="False" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%">
        <rsweb:ReportViewer ID="rvEmployeePunching" runat="server" Width="100%" SizeToReportContent="True"
            ShowPrintButton="False" ShowRefreshButton="False" Font-Names="Verdana" Font-Size="8pt"
            AsyncRendering="false" Height="700px" OnPageNavigation="ReportViewer1_PageNavigation">
        </rsweb:ReportViewer>
    </div>
    <div class="report-filter">
        <div style="margin-left: 858px;">
            <asp:ImageButton ID="ibtnExcel" runat="server" ImageUrl="~/images/excel_icon.png"
                OnClick="ibtnExcel_Click" CausesValidation="false" /></div>
        <div id="divData" style="display: block; height: 560px; overflow: auto;" runat="server">
            <asp:GridView ID="gvPunchings" runat="server" AllowPaging="True" CellPadding="4"
                AutoGenerateColumns="true" ForeColor="#333333" GridLines="Both" Height="530px"
                OnRowCreated="gvPunchings_RowCreated" PageSize="10" OnPageIndexChanging="gvPunchings_PageIndexChanging">
                <RowStyle HorizontalAlign="Left" Font-Size="9px" Font-Names="Verdana" />
                <PagerStyle BackColor="#59b6fe" ForeColor="White" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" Font-Size="12px"
                    Font-Names="Verdana" />
                <HeaderStyle BackColor="#59b6fe" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                <AlternatingRowStyle BackColor="White" Font-Size="9px" Font-Names="Verdana" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
