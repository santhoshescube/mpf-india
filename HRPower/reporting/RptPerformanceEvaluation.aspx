﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true" CodeFile="RptPerformanceEvaluation.aspx.cs" Inherits="reporting_RptPerformanceEvaluation" Title="Performance Evaluation" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
        var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
<div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal></span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 100%" class="labeltext">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <%--Department--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="LoadEmployee" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblFromDate" runat="server" Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox" MaxLength="11" Width="85px"></asp:TextBox>
                                        <asp:ImageButton ID="btnFromDate" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                        <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                            Format="dd-MMM-yyyy" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Work Status</td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px" onselectedindexchanged="ddlWorkStatus_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblToDate" runat="server" Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox" MaxLength="11" Width="85px"></asp:TextBox>
                                        <asp:ImageButton ID="btnToDate" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                        <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                            Format="dd-MMM-yyyy" PopupButtonID="btnToDate" TargetControlID="txtToDate" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                                    <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>'  OnCheckedChanged="chkIncludeCompany_CheckedChanged"
                                                        AutoPostBack="true" />
                                    </td>
                                    <td>
                                        Employee</td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        &nbsp;</td>
                                </tr>
                            </table>
                              </td>
                            <td valign="top" style="width: 40px">
                                <table>
                                    <tr>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" class="btnsubmit" CausesValidation="false"
                                               Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnSearch_Click1" />
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    <asp:ImageButton class="print" ID="btnPrint1" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                        ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" onpagenavigation="ReportViewer1_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>

