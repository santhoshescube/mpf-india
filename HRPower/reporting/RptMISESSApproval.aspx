﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptMISESSApproval.aspx.cs"
    MasterPageFile="~/Master/ReportMasterPage.master" Inherits="reporting_RptMISESSApproval" %>
    
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text

                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية' : 'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي' : 'Hide Filter' : hidControl.value == 'ar-AE' ? 'عرض تصفية' : 'Show Filter');

            });

        });



        function Validate() {
            var StartDate = document.getElementById('<%= txtStartMonth.ClientID%>');
            var EndDate = document.getElementById('<%=txtEndMonth.ClientID%>');

            if (StartDate.value == '') {
                alert('Please enter from date');
                return false;
            }
            else if (EndDate.value == '') {
                alert('Please enter to date');
                return false;
            }
            return true;
        }
        
        
    </script>

    <div style="padding-left: 350px;">
        <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,EssApproval%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <%--Hide Filter--%><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal>
            </span></legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 100%" class="labeltext">
                                <tr>
                                    <td>
                                        <%-- Company--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="190px"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <%-- Ess Type--%><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,EssType%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEssType" runat="server" CssClass="dropdownlist"
                                            Width="180px" />
                                    </td>
                                    <td>
                                        <%--From Date--%><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtStartMonth" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="8px" ValidationGroup="Submit" Width="100px">
                                        </asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd MMM yyyy"
                                            TargetControlID="txtStartMonth">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="190px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        Work Status</td>
                                    <td>
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                            Width="180px" 
                                            onselectedindexchanged="ddlWorkStatus_SelectedIndexChanged" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtEndMonth" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="8px" ValidationGroup="Submit" Width="100px">
                                        </asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender TargetControlID="txtEndMonth" Format="dd MMM yyyy"
                                            ID="CalendarExtender2" runat="server">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblType" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="dropdownlist" DataTextField="Department" OnSelectedIndexChanged = "ddlDepartment_SelectedIndexChanged" AutoPostBack="true"
                                            DataValueField="DepartmentID" Width="180px" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltrlEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>'  AutoPostBack="True" OnCheckedChanged="chkIncludeCompany_CheckedChanged" />
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button1" OnClientClick="return Validate();" runat="server" class="btnsubmit"
                                            CausesValidation="false" Text='<%$Resources:ReportsCommon,Go%>' ValidationGroup="Submit"
                                            OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>'
                                                    OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div class="report-filter">
        <%--<fieldset class="report-fs">--%>
            <div id="Div1" style="display: block; height:170px; overflow:auto;">
                 <asp:DataList ID="dlRequests" runat="server" BorderColor="#307296" RepeatDirection="Vertical"
                    Style="width: 100%; float: left; padding-top: 20px;" Width="100%" DataKeyField="requestId"
                    CssClass="labeltext" OnItemCommand="dlRequests_ItemCommand">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table width="100%" class="datalistheader">
                            <tr>                              
                               
                                <td style="width:21%;">
                                    <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Literal>
                                </td>                               
                                <td style="width:22%;">
                                    <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ReportsCommon,Type%>'></asp:Literal>                                    
                                </td>
                                <td style="width:21%;">
                                    <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ReportsCommon,Date%>'></asp:Literal>
                                </td>
                                 <td style="width:22%;">
                                    <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ReportsCommon,Reason%>'></asp:Literal>
                                </td>
                                <td style="width:10%;">
                                    <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ReportsCommon,Status%>'></asp:Literal>
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                    <table width="100%" style="border-color:Black; border:1px;   border-style:solid;">
                            <tr>  
                                <td style="width:21%;">
                                     <%# Eval("EmployeeFullName") %>
                                </td>
                                <td style="width:22%;">
                                     <%# Eval("Type") %>
                                </td>
                                <td style="width:21%;">
                                     <%# Eval("Date") %>
                                </td>
                                <td style="width:22%;">
                                     <%# Eval("Reason") %>
                                </td>
                                <td style="width:10%;">
                                    <%# Eval("Status") %>
                                </td>
                                <td style="width:5%;">
                                    <asp:LinkButton ID="lnkView" runat="server" CommandName="VIEW" CommandArgument='<%# Eval("RequestID") %>' Text='<%$Resources:ReportsCommon,View%>' ></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </div>
    <%--    </fieldset>--%>
    </div>
    <div style="clear: both;  width: 100%; height:700px; overflow: auto">
         <rsweb:ReportViewer ID="rptApproval" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="500px" onpagenavigation="rptApproval_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
