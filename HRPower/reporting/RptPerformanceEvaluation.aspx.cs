﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.Reporting.WebForms;
using System.Globalization; 
public partial class reporting_RptPerformanceEvaluation : System.Web.UI.Page
{
    private int intCompany;
    private int intBranch;
    private int intEmployee;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "PerformanceEvaluationReport").ToString();

        ReportViewer1.Visible = false;
        //btnPrint1.Enabled = false;
        btnPrint1.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);

        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            txtFromDate.Text = clsReportCommon.GetMonthStartDate();
            txtToDate.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            LoadCombos();
            SetDefaultBranch(ddlBranch);
        }
    }
    private void LoadCombos()
    {
        FillAllCompany();
        FillAllDeparments();
        FillWorkStatus();
        ddlCompany_SelectedIndexChanged(new object(), new EventArgs());
        LoadEmployee(new object(), new EventArgs());
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataBind();
    }
    //private void EnableDisableIncludeCompany()
    //{
    //    //No branch selected
    //    if (ddlBranch.SelectedValue.ToInt32() == 0)
    //    {
    //        chkIncludeCompany.Checked = true;
    //        chkIncludeCompany.Enabled = false;
    //    }
    //    else
    //    {
    //        chkIncludeCompany.Enabled = true;
    //    }

    //}

    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    /// <summary>
    /// Load employee function
    /// </summary>
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    public void SetDefaultBranch(DropDownList ddl)
    {
        ddl.SelectedValue = "0";
        ddlBranch_SelectedIndexChanged(null, null);
    }
    #region"Filling Drop Down List"
    private void BindDropDown(DropDownList ddl, string sDataValueField, string sDataTextField, DataTable dt)
    {
        ddl.DataTextField = sDataTextField;
        ddl.DataValueField = sDataValueField;
        ddl.DataSource = dt;
        ddl.DataBind();
    }
    private void FillAllCompany()
    {
        clsUserMaster objUser = new clsUserMaster();
        this.BindDropDown(ddlCompany, "CompanyID", "Company", clsReportCommon.GetAllCompanies(objUser.GetCompanyId()));
        ddlCompany.SelectedIndex = 0;

    }
    private void FillAllBranches()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue == "" ? "0" : ddlCompany.SelectedValue);
        if (intCompany > 0)
            this.BindDropDown(ddlBranch, "BranchID", "Branch", clsReportCommon.GetAllBranchesByCompany(intCompany));
        ddlBranch_SelectedIndexChanged(null, null);
    }
    private void FillAllDeparments()
    {

        this.BindDropDown(ddlDepartment, "DepartmentID", "Department", clsReportCommon.GetAllDepartments());

    }

    private void FillAllEmployees()
    {
        intCompany = Convert.ToInt32(ddlCompany.SelectedValue==""?"0":ddlCompany.SelectedValue);
        intBranch = Convert.ToInt32(ddlBranch.SelectedValue =="" ?"0": ddlBranch.SelectedValue );
        this.BindDropDown(ddlEmployee, "EmployeeID", "EmployeeFullName", clsReportCommon.GetAllEmployees(intCompany, intBranch,
                                chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), -1, -1,ddlWorkStatus.SelectedValue.ToInt32(), -1
                                ));
    }
    #endregion
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();

        FillAllEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtFromDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate"));//"Please Select From Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtFromDate.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtToDate.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtToDate.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }



        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    protected void btnSearch_Click1(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    private void ShowReport()
    {
        try
        {
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            DataTable dtCompany = null;
            if (txtFromDate.Text != "")
                FromDate = Convert.ToDateTime(txtFromDate.Text, System.Globalization.CultureInfo.InvariantCulture);

            if (txtToDate.Text != "")
                ToDate = Convert.ToDateTime(txtToDate.Text, System.Globalization.CultureInfo.InvariantCulture);


            if (ToDate < FromDate)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate")) + "');", true);
                return;
            }
            this.ReportViewer1.Reset();
            //Clear DataSources
            this.ReportViewer1.LocalReport.DataSources.Clear();
            //Set Report Path
            this.ReportViewer1.LocalReport.ReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptEvaluationArb.rdlc" : "reports/RptEvaluation.rdlc");


            //Set Company Header For Report
            //Company Header

            dtCompany = clsReportCommon.GetCompanyHeader(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);



            DataTable dtEvaluationMaster = clsRptPerformanceInitiation.GetEvaluationMaster(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlEmployee.SelectedValue.ToInt32(), FromDate, ToDate);
            DataTable dtEvaluationDetails = clsRptPerformanceInitiation.GetEvaluationDetails();
            DataTable dtEvaluationGoals = clsRptPerformanceInitiation.GetEvaluationGoals();
            ViewState["dtEvaluationGoals"] = dtEvaluationGoals;
            ViewState["dtEvaluationDetail"] = dtEvaluationDetails;

            ViewState["RecordCount"] = dtEvaluationMaster.Rows.Count;
            if (dtEvaluationMaster.Rows.Count > 0)
            {
                ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
                                {
                                 new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))
                                
                               });
                ReportViewer1.LocalReport.SubreportProcessing += new Microsoft.Reporting.WebForms.SubreportProcessingEventHandler(LocalReport_SubreportProcessing);
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsPerformance_dtEvaluationMaster", dtEvaluationMaster));

                ReportViewer1.Visible = true;
                btnPrint1.Enabled = true;

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
                ReportViewer1.Visible = false;
                btnPrint1.Enabled = false;
                return;

            }



        }
        catch (Exception ex)
        {
        }
    }
    void LocalReport_SubreportProcessing(object sender, Microsoft.Reporting.WebForms.SubreportProcessingEventArgs e)
    {

       e.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsPerformance_dtEvaluationDetail", ((DataTable)ViewState["dtEvaluationDetail"])));
        e.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsPerformance_dtGoal", ((DataTable)ViewState["dtEvaluationGoals"])));

    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees();
    }
}
