﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using Microsoft.Reporting.WebForms;

public partial class reporting_RptMISESSApproval : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "EssApproval").ToString();
        //ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            chkIncludeCompany.Checked = true;
            txtStartMonth.Text = clsReportCommon.GetMonthStartDate();
            txtEndMonth.Text = System.DateTime.Today.ToString("dd MMM yyyy");
            FillCombos();
            ddlBranch.SelectedValue = "-1";
            EnableDisableIncludeCompany();
        }
        btnPrint.Enabled = (ViewState["RecordCount"] != null && Convert.ToInt32(ViewState["RecordCount"]) > 0);


    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    private void FillCombos()
    {
        clsUserMaster objUser = new clsUserMaster();
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();

        FillEmployees();

        FillEssType();
        FillAllBranches();
        FillAllDeparments();
        FillWorkStatus();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = EmployeeWorkStatus.InService.ToString();
    }
    private void FillEmployees()
    {
        DataTable dt = new DataTable();
        dt = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                       chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), -1, -1, ddlWorkStatus.SelectedValue.ToInt32());
        dt.Rows.RemoveAt(0);
        dt.AcceptChanges();
        ddlEmployee.DataSource = dt;
        
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    private void FillEssType()
    {
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Leave")), Value = "1" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "LeaveExtension")), Value = "2" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "SalaryAdvance")), Value = "3" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Expense")), Value = "4" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Loan")), Value = "5" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Travel")), Value = "6" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Transfer")), Value = "7" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Document")), Value = "8" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "General")), Value = "9" });
        ddlEssType.Items.Add(new ListItem() { Text = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Vacation")), Value = "10" });



    }
    private void FillAllBranches()
    {

        int CompanyID = ddlCompany.SelectedValue.ToInt32();
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }

    private void FillAllDeparments()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllBranches();
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindDataList();
    }

    private void BindDataList()
    {
        DateTime FromDate = DateTime.MinValue;
        DateTime ToDate = DateTime.MinValue;

        if (txtStartMonth.Text != "")
            FromDate = Convert.ToDateTime(txtStartMonth.Text, System.Globalization.CultureInfo.InvariantCulture);

        if (txtEndMonth.Text != "")
            ToDate = Convert.ToDateTime(txtEndMonth.Text, System.Globalization.CultureInfo.InvariantCulture);

        if (ToDate < FromDate)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Todatemustbegreaterthanfromdate")) + "');", true);
            return;
        }

        DataTable dt = clsRptMISESSApproval.GetRequests(ddlEssType.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), FromDate, ToDate);
        if (dt.Rows.Count > 0)
        {
            dlRequests.DataSource = dt;
            dlRequests.DataBind();
        }
        else
        {
            dlRequests.DataSource = null;
            dlRequests.DataBind();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
        }
        rptApproval.Reset();
        rptApproval.LocalReport.DataSources.Clear();
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(rptApproval, this);
    }
    protected void dlRequests_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "VIEW":
                    if (ValidateDate())
                    {
                        ShowReport(e.CommandArgument.ToInt32());
                    }
                    break;
            }
        }
        catch { }
    }

    private bool ValidateDate()
    {
        DateTime dtFromDate = System.DateTime.MinValue;
        DateTime dtToDate = System.DateTime.MinValue;

        DateTimeFormatInfo df = new DateTimeFormatInfo();

        df.ShortDatePattern = "dd MMM yyyy";

        bool blnIsValid = true;
        string strMessage = string.Empty;


        if (txtStartMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectFromDate"));
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtStartMonth.Text, df, DateTimeStyles.None, out dtFromDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate"));//"Please Select Valid Date";
            blnIsValid = false;
        }
        else if (txtEndMonth.Text == string.Empty)
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectToDate"));//"Please Select To Date";
            blnIsValid = false;
        }
        else if (!DateTime.TryParse(txtEndMonth.Text, df, DateTimeStyles.None, out dtToDate))
        {
            strMessage = Convert.ToString(GetGlobalResourceObject("ReportsCommon", "PleaseSelectValidDate")); //"Please Select Valid Date";
            blnIsValid = false;
        }


        if (!blnIsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + strMessage + "');", true);
            return false;
        }
        return true;
    }
    private void ShowReport(int RequestID)
    {
        try
        {
            DataTable dtCompany = null;            
            DataSet ds = null;
            string path = "";

            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }

            rptApproval.Reset();
            rptApproval.LocalReport.DataSources.Clear();
            rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));

            path = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptMISESSApprovalArb.rdlc" : "reports/RptMISESSApproval.rdlc");

            int FormID = 0;
            switch (ddlEssType.SelectedValue.ToInt32())
            {
                case 1:  FormID = (int)essReferenceTypes.Leave; break;
                case 2:  FormID = (int)essReferenceTypes.Extension; break;
                case 3:  FormID = (int)essReferenceTypes.SalaryAdvance; break;
                case 4:  FormID = (int)essReferenceTypes.Expense; break;
                case 5:  FormID = (int)essReferenceTypes.Loan; break;
                case 6:  FormID = (int)essReferenceTypes.Travel; break;
                case 7:  FormID = (int)essReferenceTypes.Transfer; break;
                case 8:  FormID = (int)essReferenceTypes.Document; break;
                case 9:  FormID = (int)essReferenceTypes.General; break;
                case 10: FormID = (int)essReferenceTypes.Vacation; break;
            }
            ds = clsRptMISESSApproval.GetRequestDetails(RequestID, FormID, ddlEmployee.SelectedValue.ToInt32());
            if (ds.Tables.Count > 0)
            {

                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtLeaveRequest", ds.Tables[0]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtExtensionRequest", ds.Tables[1]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtSalaryRequest", ds.Tables[2]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtExpenseRequest", ds.Tables[3]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtLoanRequest", ds.Tables[4]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtTravelRequest", ds.Tables[5]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtTransferRequest", ds.Tables[6]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtDocumentRequest", ds.Tables[7]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtGeneralRequest", ds.Tables[8]));

                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtApprovalDetails", ds.Tables[9]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtEmployee", ds.Tables[10]));
                rptApproval.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("dsEssApproval_dtVacationRequest", ds.Tables[11]));

                string flag = string.Empty, fval = string.Empty;
                switch (FormID)
                {
                    case 1: flag = "LeaveFlag"; break;
                    case 2: flag = "ExtFlag";  break;
                    case 3: flag = "SalFlag"; break;
                    case 4: flag = "ExpFlag"; break;
                    case 5: flag = "LoanFlag";  break;
                    case 6: flag = "travelFlag";  break;
                    case 7: flag = "transFlag"; break;
                    case 8: flag = "DocFlag"; break;
                    case 9: flag = "GenFlag"; break;
                    case 10:flag = "VacFlag"; break;

                }

                rptApproval.LocalReport.ReportPath = path;
                rptApproval.LocalReport.SetParameters(new ReportParameter[] 
                        {
                            new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter))
                             , new ReportParameter(flag, FormID.ToString())
                        });

                btnPrint.Enabled = true;
                rptApproval.LocalReport.Refresh();
                rptApproval.Visible = true;
            }
            else
            {
                rptApproval.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void rptApproval_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        rptApproval.Visible = true;
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
}
