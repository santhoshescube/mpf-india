﻿<%@ Page Language="C#" MasterPageFile="~/Master/ReportMasterPage.master" AutoEventWireup="true" CodeFile="RptSettlement.aspx.cs" Inherits="reporting_RptSettlement" Title="Settlement Report" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" Runat="Server">

    <script type="text/javascript">

        $(document).ready(function() {

            // add click event handler
            $('#span-toggle').click(function() {
            var hidControl = document.getElementById("ctl00_hidCulture");            

                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium');
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
                //this.innerHTML = (this.innerHTML == 'Show Filter' ? 'Hide Filter' : 'Show Filter');
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });

        });
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label1" runat="server" class="labeltext" Text='<%$Resources:MasterPageCommon,Settlement%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <%--Hide Filter--%>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal>
                </span> </legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 73%">
                     <%--       <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>--%>
                                    <table style="width: 100%" class="labeltext">
                                        <tr>
                                            <td style ="width:80px">
                                                <asp:Label ID="lblCompnay" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Label>
                                            </td>
                                            <td style ="width:220px">
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                            
                                            
                                              <td style ="width:80px">
                                                  <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Designation%>'></asp:Literal>
                                            </td>
                                            <td style ="width:220px">
                                                <asp:DropDownList ID="ddlDesignation" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" OnSelectedIndexChanged="LoadEmployee" 
                                                    Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                            
                                            
                                           
                                           
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                            </td>
                                            <td style ="width:220px">
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                             <td>
                                                <%--Designation--%>Work Status</td>
                                            <td>
                                                <asp:DropDownList ID="ddlWorkStatus" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" OnSelectedIndexChanged="LoadEmployee" 
                                                    Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,Department%>'></asp:Literal>
                                            </td>
                                            <td style ="width:220px">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                                    OnSelectedIndexChanged="LoadEmployee" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                             <td>
                                                <asp:Label ID="lblEmployee" runat="server" Text='<%$Resources:ReportsCommon,Employee%>'></asp:Label>
                                                </td>
                                            <td>
                                                <asp:DropDownList ID="ddlEmployee" runat="server" AutoPostBack="True" 
                                                    CssClass="dropdownlist_mandatory" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                           
                                           
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>'
                                                    OnCheckedChanged="chkIncludeCompany_CheckedChanged" AutoPostBack="true" />
                                            </td>
                                            
                                             
                                            <td>
                                                &nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                            
                                           
                                        </tr>
                                       
                                    </table>
                               <%-- </ContentTemplate>
                            </asp:UpdatePanel>--%>
                        </td>
                     
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="btnSearch"  runat="server" class="btnsubmit" CausesValidation="false" ValidationGroup ="Submit"
                                           Text='<%$Resources:ReportsCommon,Go%>' OnClick="btnSearch_Click1" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,print%>' Text='<%$Resources:ReportsCommon,Print%>' OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
   
    <div style="clear: both; height: auto; width: 100%;overflow:auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode ="Percent" 
            ZoomPercent ="93" Width="100%" AsyncRendering="false" 
            SizeToReportContent="True" ShowPrintButton="False" 
            ShowRefreshButton="False" ShowZoomControl ="true" 
            Font-Names="Verdana" Font-Size="8pt" Height="380px" 
            onpagenavigation="ReportViewer1_PageNavigation">
            <LocalReport ReportPath="App_Code\RptVacationSummary.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="dsVacationSettlement_dtVacationSummary" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
       <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData"
            TypeName="dsEmployeeTableAdapters."></asp:ObjectDataSource>
    </div>
</asp:Content>

