﻿<%@ Page Title="ESS Report" Language="C#" MasterPageFile="~/Master/ReportMasterPage.master"
    AutoEventWireup="true" CodeFile="RptGeneralExpense.aspx.cs" Inherits="reporting_RptGeneralExpense" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    
        $(document).ready(function() {
               var hidControl = document.getElementById("ctl00_hidCulture");
            // add click event handler
            $('#span-toggle').click(function() {
                
                // apply animation
                SlideUpSlideDown('filter-cotainer', 'medium'); 
                // change css class
                this.className = (this.innerHTML == 'Hide Filter' ? 'filter-show' : 'filter-hide');
                // change display text
               
                this.innerHTML = (this.innerHTML == hidControl.value == 'ar-AE' ? 'عرض تصفية':'Show Filter' ? hidControl.value == 'ar-AE' ? 'اخفاء تصفي': 'Hide Filter' : hidControl.value == 'ar-AE' ?'عرض تصفية':'Show Filter');

            });
            
        });
        
        
        
        function Validate()
        {
             var StartDate = document.getElementById('<%= txtExpenseFromDate.ClientID%>');
             var EndDate = document.getElementById('<%=txtExpenseToDate.ClientID%>');
             
             if(StartDate.value == '')
             {
                alert('Please enter from date');
                return false;
             }
             else if(EndDate.value == '')
             {
                alert('Please enter to date');
                return false;
             }
             return true;
        }
        
        
    </script>
 <div style="padding-left:350px;">
     <asp:Label ID="Label2" runat="server" class="labeltext" Text='<%$Resources:ReportsCommon,GeneralExpenseReport%>'></asp:Label>
    </div>
    <div class="report-filter">
        <fieldset class="report-fs">
            <legend class="filter-legend"><span id="span-toggle" style="cursor: pointer;" class="filter-hide">
                <%--Hide Filter--%><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ReportsCommon,HideFilter%>'></asp:Literal>
            </span></legend>
            <div id="filter-cotainer" style="display: block;">
                <table id="tblLeave" runat="server" class="filtertbl">
                    <tr>
                        <td style="width: 100%">
                            <table style="width: 100%" class="labeltext">
                                <tr>
                                    <td>
                                        <%-- Company--%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ReportsCommon,Company%>'></asp:Literal>
                                    </td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" Width="190px"
                                            AutoPostBack="True" />
                                    </td>
                                    <td>
                                        <%-- Ess Type--%><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ReportsCommon,ExpenseCategory%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExpenseCategory" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                            Width="180px" 
                                            OnSelectedIndexChanged="ddlExpenseCategory_SelectedIndexChanged" />
                                    </td>
                                    
                                      <td>
                                        <%--From Date--%><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ReportsCommon,FromDate%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtExpenseFromDate" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="8px" ValidationGroup="Submit" Width="100px">
                                        </asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd MMM yyyy"
                                            TargetControlID="txtExpenseFromDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                    
                                    
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBranch" runat="server" Text='<%$Resources:ReportsCommon,Branch%>'></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="True" CssClass="dropdownlist_mandatory"
                                            OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged" Width="190px">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <%--<asp:Label ID="lblType" runat="server"></asp:Label>--%>
                                        <asp:Label ID="Label1" runat="server" Text='<%$Resources:ReportsCommon,ExpenseType%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExpenseType" runat="server" AutoPostBack ="true"  CssClass="dropdownlist" DataTextField="Type"
                                            DataValueField="TypeID" Width="180px" 
                                            onselectedindexchanged="ddlExpenseType_SelectedIndexChanged" />
                                    </td>
                                    
                                    
                                    
                                     <td>
                                        <%-- To Date--%><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ReportsCommon,ToDate%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtExpenseToDate" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                            Height="8px" ValidationGroup="Submit" Width="100px">
                                        </asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender TargetControlID="txtExpenseToDate" Format="dd MMM yyyy"
                                            ID="CalendarExtender2" runat="server">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:CheckBox ID="chkIncludeCompany" runat="server" Text='<%$Resources:ReportsCommon,IncludeCompany%>' />
                                    </td>
                                     <td>
                                        <%--Status--%><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ReportsCommon,ExpenseNumber%>'></asp:Literal>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExpenseNumber" runat="server" CssClass="dropdownlist" Width="180px" />
                                    </td>
                                   
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="width: 40px">
                            <table>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button1" OnClientClick="return Validate();" runat="server" class="btnsubmit"
                                            CausesValidation="false" Text='<%$Resources:ReportsCommon,Go%>' ValidationGroup="Submit"
                                            OnClick="btnGo_Click" />
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:ImageButton class="print" ID="btnPrint" runat="server" ImageUrl="~/images/print_IconNew.png"
                                                    ToolTip='<%$Resources:ReportsCommon,Print%>' Text='<%$Resources:ReportsCommon,Print%>'
                                                    OnClick="btnPrint_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div style="clear: both; height: auto; width: 100%; overflow: auto">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" ZoomMode="Percent" ZoomPercent="93"
            Width="100%" AsyncRendering="false" SizeToReportContent="True" ShowPrintButton="False"
            ShowRefreshButton="False" ShowZoomControl="true" Font-Names="Verdana" Font-Size="8pt"
            Height="380px" OnPageNavigation="ReportViewer1_PageNavigation">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
