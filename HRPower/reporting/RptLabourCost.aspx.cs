﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

public partial class reporting_RptLabourCost : System.Web.UI.Page
{
    #region Declarations
    clsCommon objCommon;
    clsUserMaster objUser;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ReportsCommon", "LabourCost").ToString();
        ReportViewer1.Visible = false;
        if (!IsPostBack)
        {
            LoadInitial();
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (ValidateDate())
        {
            ShowReport();
        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillBranch();
    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableDisableIncludeCompany();
        FillEmployees();
    }
    protected void chkIncludeCompany_CheckedChanged(object sender, EventArgs e)
    {
        FillEmployees();
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlType.SelectedIndex == 0)
        {
            ddlFinYear.Enabled = false;
            txtMonth.Enabled = true;
        }
        else
        {
            ddlFinYear.Enabled = true;
            txtMonth.Enabled = false;
            FillFinYear();
        }
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {      
        FillEmployees();
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        clsCommon.PrintReport(ReportViewer1, this);
    }
    protected void ReportViewer1_PageNavigation(object sender, PageNavigationEventArgs e)
    {
        ReportViewer1.Visible = true;
    }

    #region Methods
    private void LoadInitial()
    {
        objCommon = new clsCommon();
        txtMonth.Text = objCommon.GetSysMonthYear();
        ddlFinYear.Enabled = false;

        FillCompany();
        FillBranch();
        FillEmployees();
        FillDepartment();
        FillWorkStatus();
        FillType();
        FillFinYear();
    }

    private void FillCompany()
    {
        objUser = new clsUserMaster();
        ddlCompany.DataSource = clsReportCommon.GetAllCompanies(objUser.GetCompanyId());
        ddlCompany.DataTextField = "Company";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();
    }
    private void FillFinYear()
    {
        ddlFinYear.DataSource = clsReportCommon.GetAllFinancialYears(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked);
        ddlFinYear.DataTextField = "FinYearPeriod";
        ddlFinYear.DataValueField = "FinYearStartDate";
        ddlFinYear.DataBind();
    }
    private void FillEmployees()
    {
        ddlEmployee.DataSource = clsReportCommon.GetAllEmployees(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(),
                                         chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(),-1,-1,ddlWorkStatus.SelectedValue.ToInt32());
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }
    /// <summary>
    /// Fill all types
    /// 0->Monthly 
    /// 1->Financial year
    /// </summary>
    private void FillType()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Type");
        dt.Columns.Add("TypeID");

        dr = dt.NewRow();
        dr["Type"] = "Month";
        dr["TypeID"] = 1;
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Type"] = "Financial Year";
        dr["TypeID"] = 2;
        dt.Rows.Add(dr);

        ddlType.DataSource = dt;
        ddlType.DataTextField = "Type";
        ddlType.DataValueField = "TypeID";
        ddlType.DataBind();
    }
    private void FillWorkStatus()
    {
        ddlWorkStatus.DataSource = clsReportCommon.GetAllWorkStatus();
        ddlWorkStatus.DataTextField = "WorkStatus";
        ddlWorkStatus.DataValueField = "WorkStatusID";
        ddlWorkStatus.DataBind();
        ddlWorkStatus.SelectedValue = "6";
    }
    private void FillDepartment()
    {
        ddlDepartment.DataSource = clsReportCommon.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }
    private void FillBranch()
    {
        int CompanyID = ddlCompany.SelectedValue.ToInt32();
        ddlBranch.DataSource = clsReportCommon.GetAllBranchesByCompany(CompanyID);
        ddlBranch.DataTextField = "Branch";
        ddlBranch.DataValueField = "BranchID";
        ddlBranch.DataBind();

        ddlBranch_SelectedIndexChanged(new object(), new EventArgs());
    }
    private void EnableDisableIncludeCompany()
    {

        //Checking if the user is having access to any branch
        if (ddlBranch.Items.Count == 2)
        {
            ddlBranch.SelectedValue = "0";
            ddlBranch.Enabled = false;
        }
        else
        {
            ddlBranch.Enabled = true;
        }

        //Checking if the users is having access to the selected company
        if (!(clsReportCommon.IsCompanyPermissionExists(ddlCompany.SelectedValue.ToInt32())))
        {
            chkIncludeCompany.Checked = chkIncludeCompany.Enabled = false;
        }

        else
        {
            //No branch selected
            if (ddlBranch.SelectedValue.ToInt32() == 0 || ddlBranch.Items.Count == 2)
            {
                chkIncludeCompany.Checked = true;
                chkIncludeCompany.Enabled = false;
            }
            else
            {
                chkIncludeCompany.Enabled = chkIncludeCompany.Checked = true;
            }
        }

    }
    protected void LoadEmployee(object sender, EventArgs e)
    {
        FillEmployees();
    }
    private bool ValidateDate()
    {
        return true;
    }
    private void ShowReport()
    {
        try
        {
            DateTime dtFinYearStartDate = DateTime.Today;
            DateTime dtFinYearEndDate = DateTime.Today;
            bool blnIsMonth = ddlType.SelectedIndex == 0 ? true : false;
            int intMonth = 0;
            int intYear = 0;
            DateTime dtMonthEndDate = DateTime.Today;

            // Check whether Month is Selected
            if (blnIsMonth)
            {
                intMonth = clsCommon.Convert2DateTime(txtMonth.Text).Date.Month;
                intYear = clsCommon.Convert2DateTime(txtMonth.Text).Date.Year;
                dtMonthEndDate = new DateTime(intYear, intMonth, DateTime.DaysInMonth(intYear, intMonth)).Date;
            }
            else if (ddlFinYear.SelectedValue != null && ddlFinYear.Text != string.Empty)
            {
                dtFinYearStartDate = ddlFinYear.SelectedItem.Text.Split('-')[0].ToDateTime();// ddlFinYear.Text.Split('-')[0].ToDateTime();
                dtFinYearEndDate = ddlFinYear.SelectedItem.Text.Split('-')[1].ToDateTime(); //ddlFinYear.Text.Split('-')[1].ToDateTime();
            }

            //Set Company Header For Report

            DataTable dtCompany = null;
            DataSet ds = null;

            if (chkIncludeCompany.Checked)
            {
                dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
            }
            else
            {
                if (ddlBranch.SelectedValue.ToInt32() > 0)
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlBranch.SelectedValue.ToInt32());
                }
                else
                {
                    dtCompany = new clsMISEmployeeReport().DisplayCompanyReportHeader(ddlCompany.SelectedValue.ToInt32());
                }
            }

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DtSetCompanyFormReport_CompanyHeader", dtCompany));
            //Get Report Data
            DataSet dsReport = clsRptLabourCost.GetReport(ddlCompany.SelectedValue.ToInt32(), ddlBranch.SelectedValue.ToInt32(), chkIncludeCompany.Checked, ddlDepartment.SelectedValue.ToInt32(), ddlWorkStatus.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), blnIsMonth, intMonth, intYear, dtFinYearStartDate, dtFinYearEndDate, dtMonthEndDate);

            //Set Report Path
            ReportViewer1.LocalReport.ReportPath = Server.MapPath(clsGlobalization.IsArabicCulture() ? "reports/RptLabourCostArb.rdlc" : "reports/RptLabourCost.rdlc");

            //Set Report DataSource if Record Exists
            if (dsReport.Tables[0].Rows.Count > 0 || dsReport.Tables[1].Rows.Count > 0)
            {
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsLabourCost_dtLabourCost", dsReport.Tables[0]));
                ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dsLabourCost_dtSettlement", dsReport.Tables[1]));

            //Set Report Parameters
            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] 
            {
                new ReportParameter("GeneratedBy", new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ReportFooter)),
                new ReportParameter("Company",ddlCompany.SelectedItem.Text.Trim()),
                new ReportParameter("Branch",ddlBranch.SelectedItem.Text.Trim()),
                new ReportParameter("Department",ddlDepartment.SelectedItem.Text.Trim()),
                new ReportParameter("WorkStatus",ddlWorkStatus.SelectedItem.Text.Trim()),
                new ReportParameter("Employee",ddlEmployee.SelectedItem.Text.Trim()),
                new ReportParameter("IsMonth",blnIsMonth ? "1" : "0"),
                new ReportParameter("Period",blnIsMonth ? clsCommon.Convert2DateTime(txtMonth.Text).ToString("MMM yyyy") : ddlFinYear.SelectedItem.Text.Trim()),
                new ReportParameter("CFlag", (dsReport.Tables[0].Rows.Count > 0)? "1" : "0"),
                new ReportParameter("SFlag", (dsReport.Tables[1].Rows.Count > 0)? "1" : "0")
            });

                btnPrint.Enabled = true;
                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.Visible = true;
            }
            else
            { // Show 'No Records Found' Message to User
                ReportViewer1.Visible = false;
                btnPrint.Enabled = false;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + Convert.ToString(GetGlobalResourceObject("ReportsCommon", "Noinformationfound")) + "');", true);
            }
        }
        catch { }
    }


    #endregion
}
