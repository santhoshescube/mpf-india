﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class ESSMaster : System.Web.UI.MasterPage
{

    //protected void Page_Init(object sender, EventArgs e)
    ////{
    //    LtAdvance.Text = GetGlobalResourceObject("MasterPageCommon", "Advance").ToString();
    //    LtDocument.Text = GetGlobalResourceObject("MasterPageCommon", "Document").ToString();
    //    LtExpense.Text = GetGlobalResourceObject("MasterPageCommon", "Expense").ToString();
    //    LtExtension.Text = GetGlobalResourceObject("MasterPageCommon", "Extension").ToString();
    //    LtLeave.Text = GetGlobalResourceObject("MasterPageCommon", "Leave").ToString();
    //    LtLoan.Text = GetGlobalResourceObject("MasterPageCommon", "Loan").ToString();
    //    LtProfile.Text = GetGlobalResourceObject("MasterPageCommon", "Profile").ToString();
    //    LtTransfer.Text = GetGlobalResourceObject("MasterPageCommon", "Transfer").ToString();
    //    LtTravel.Text = GetGlobalResourceObject("MasterPageCommon", "Travel").ToString();
    //    LtGeneral.Text = GetGlobalResourceObject("MasterPageCommon", "General").ToString();
    //  //  LtAttendance.Text = GetGlobalResourceObject("MasterPageCommon", "Attendance").ToString();
    //}

    //private void SetTopMenus()
    //{
    //    LtAdvance.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Advance").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Advance").ToString();
    //    LtDocument.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Document").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Document").ToString();
    //    LtExpense.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Expense").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Expense").ToString();
    //    LtExtension.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Extension").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Extension").ToString();
    //    LtLeave.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Leave").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Leave").ToString();
    //    LtLoan.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Loan").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Loan").ToString();
    //    LtProfile.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Profile").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Profile").ToString();
    //    LtTransfer.Text = clsGlobalization.IsArabicCulture() ? GetGlobalResourceObject("MasterPageCommonAr", "Transfer").ToString() : GetGlobalResourceObject("MasterPageCommonEng", "Transfer").ToString();

    //}

    protected void Page_Load(object sender, EventArgs e)
    {

        imgLogo.Src = GetGlobalResourceObject("MasterPageCommon", "Logo").ToString();
        clsGlobalization.SetCulture(Page);

        clsUserMaster objUserMaster;
        clsEmployee objEmployee;
        clsRoleSettings objRoleSettings;

        int RoleID = 0;
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objEmployee = new clsEmployee();
            objRoleSettings = new clsRoleSettings();

            hidCulture.Value = clsUserMaster.GetCulture();
            objEmployee.EmployeeID = objUserMaster.GetEmployeeId();
            lblWelcomeText.Text = objEmployee.GetWelcomeText();

            RoleID = objUserMaster.GetRoleId();

            if (RoleID > 3)
            {

                DataTable dtPermissions = objRoleSettings.GetModulePermissions(RoleID, (int)eModuleID.HomePage);
                if (dtPermissions.Rows.Count == 0)
                {

                    ancSettings.Disabled = true;
                    ancSettings.HRef = "";
                }
                else
                {
                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HRSettings).ToString(); // Settings
                    ancSettings.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancSettings.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/RequestSettings.aspx" : "";
                }
            }


            if (RoleID < 5) ancManager.Visible = true;
            if (RoleID == 5) ancManager.Visible = false;

            if (RoleID > 5)
            {
                if (objRoleSettings.GetModulePermissions(RoleID, 0).Rows.Count > 0)
                {
                    ancManager.Visible = true;
                }
                else
                {
                    ancManager.Visible = false;
                }
            }
            GetCompanyDetails();
            objUserMaster.UserID = objUserMaster.GetUserId();
            imgChangeCompany.Visible = objUserMaster.CheckEmployeeRole();

           
        }
    }


    private void GetCompanyDetails()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        objUserMaster.CmpID = objUserMaster.GetCompanyId();
        objUserMaster.UserID = objUserMaster.GetUserId();
        DataTable dt = objUserMaster.GetUserCompanyDetails();
        if (dt.Rows.Count > 0)
        {
            imgCompanyLogo.ImageUrl = GetLogo(90);
            lblCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
        }
    }
    public string GetLogo(int width)
    {
        clsUserMaster objUser = new clsUserMaster();
        return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + objUser.GetCompanyId() + "&t=" + DateTime.Now.Ticks;
    }
    protected void imgChangeCompany_Click(object sender, ImageClickEventArgs e)
    {

    }
}
