﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Master_PublicmasterPage : System.Web.UI.MasterPage
{

      clsUserMaster objUserMaster;
        clsEmployee objEmployee;
        clsRoleSettings objRoleSettings;

    private void SetTopMenus()
    {
        LtDocuments.Text = GetGlobalResourceObject("MasterPageCommon", "Documents").ToString();
        LtEmployee.Text = GetGlobalResourceObject("MasterPageCommon", "Employee").ToString();
        LtHome.Text = GetGlobalResourceObject("MasterPageCommon", "Home").ToString();
        LtPerformanceAndTraining.Text = GetGlobalResourceObject("MasterPageCommon", "Performance").ToString();
        // LtRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();
        LtReports.Text = GetGlobalResourceObject("MasterPageCommon", "Reports").ToString();
        LitPlanningAndRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        imgLogo.Src = GetGlobalResourceObject("MasterPageCommon", "Logo").ToString();
       
        SetTopMenus();
      
        hidCulture.Value = clsUserMaster.GetCulture();
        int RoleID = 0;
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objEmployee = new clsEmployee();
            objRoleSettings = new clsRoleSettings();

            objEmployee.EmployeeID = objUserMaster.GetEmployeeId();
            lblWelcomeText.Text = objEmployee.GetWelcomeText();
            RoleID = objUserMaster.GetRoleId();

            //  ancSettings.Visible = (objRoleSettings.IsMenuEnabled(RoleID, clsRoleSettings._MENU.Manager_View));
            if (RoleID > 3)
            {

                DataTable dtPermissions = objRoleSettings.GetModulePermissions(RoleID, (int)eModuleID.HomePage);
                if (dtPermissions.Rows.Count == 0)
                {

                    ancHome.Disabled = ancDocument.Disabled = ancEmployee.Disabled = ancPerformance.Disabled =
                        // ancRecruitment.Disabled = 
                        ancSettings.Disabled = ancreports.Disabled = true;
                    ancHome.HRef = ancDocument.HRef = ancEmployee.HRef = ancPerformance.HRef =
                        //ancRecruitment.HRef =
                        ancSettings.HRef = ancreports.HRef = "";
                }
                else
                {
                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HomePage).ToString(); // Home
                    ancHome.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancHome.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/HomeDetailView.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Recruitment).ToString(); // Recruitment
                    //ancRecruitment.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    //ancRecruitment.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Job.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Employee).ToString(); // Employee
                    ancEmployee.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancEmployee.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Employee.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Documents).ToString(); // Documents
                    ancDocument.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancDocument.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Passport.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Performance).ToString(); // Performance
                    ancPerformance.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancPerformance.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/PerformanceInitiation.aspx" : "";

                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Reports).ToString(); // Reports
                    ancreports.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancreports.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/reporting/RptCommonEss.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HRSettings).ToString(); // Settings
                    ancSettings.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancSettings.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/RequestSettings.aspx" : "";

                }

            }

            if (RoleID == 4) // interviewer
            {
                //ancRecruitment.Disabled = false;
                //ancRecruitment.HRef = "~/Public/Job.aspx";
                ancHome.Disabled = false;
                ancHome.HRef = "~/Public/HomeDetailView.aspx";
                // Response.Redirect("~/public/Job.aspx");
            }
            GetCompanyDetails();

            if (!clsGlobalization.LicenseStatus)
                hrSts.Text = "You are using cracked HR software of Escube Technovation.";
        }

     
    }
    private void GetCompanyDetails()
    {
        objUserMaster = new clsUserMaster();
        objUserMaster.CmpID = objUserMaster.GetCompanyId();
      
        DataTable dt = objUserMaster.GetUserCompanyDetails();
        if (dt.Rows.Count > 0)
        {
            imgCompanyLogo.ImageUrl = GetLogo(90);
            lblCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
        }
    }
     public string GetLogo(int width)
     {
         clsUserMaster objUser = new clsUserMaster();
         return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + objUser.GetCompanyId()+ "&t=" + DateTime.Now.Ticks;
     }


     protected void imgChangeCompany_Click(object sender, ImageClickEventArgs e)
     {
         //mdChangeCompany.Show();
        // updChangeCompany.Update();
     }
}
