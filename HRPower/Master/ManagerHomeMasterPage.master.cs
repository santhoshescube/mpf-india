﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Master_ManagerHomeMasterPage : System.Web.UI.MasterPage
{

    private void SetTopMenus()
    {
        LtDocuments.Text = GetGlobalResourceObject("MasterPageCommon", "Documents").ToString();
        LtEmployee.Text = GetGlobalResourceObject("MasterPageCommon", "Employee").ToString();
        LtHome.Text = GetGlobalResourceObject("MasterPageCommon", "Home").ToString();
       // LtPerformance.Text = GetGlobalResourceObject("MasterPageCommon", "Performance").ToString();
        LtTrainingAndPerformance.Text = GetGlobalResourceObject("MasterPageCommon", "PerformanceAndTraining").ToString();
        LitPlanningAndRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "PlanningAndRecruitment").ToString();
       // LtRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();
        LtReports.Text = GetGlobalResourceObject("MasterPageCommon", "Reports").ToString();
//        LtReports.Text = GetGlobalResourceObject("MasterPageCommon", "Task").ToString();

      
    }
    protected void Page_Load(object sender, EventArgs e)
   
    {
        imgLogo.Src = GetGlobalResourceObject("MasterPageCommon", "Logo").ToString();
        SetTopMenus();
        clsUserMaster objUserMaster;
        clsEmployee objEmployee;
        clsRoleSettings objRoleSettings;

        int RoleID = 0;
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objEmployee = new clsEmployee();
            objRoleSettings = new clsRoleSettings();

            objEmployee.EmployeeID = objUserMaster.GetEmployeeId();
            lblWelcomeText.Text = objEmployee.GetWelcomeText();

            RoleID = objUserMaster.GetRoleId();

            if (RoleID > 3)
            {

                DataTable dtPermissions = objRoleSettings.GetModulePermissions(RoleID, (int)eModuleID.HomePage);
                if (dtPermissions.Rows.Count == 0)
                {

                    ancHome.Disabled = ancDocument.Disabled = ancEmployee.Disabled = ancPerformance.Disabled = 
                        ancPlanning.Disabled = 
                        ancSettings.Disabled = ancreports.Disabled = true;
                    ancHome.HRef = ancDocument.HRef = ancEmployee.HRef = ancPerformance.HRef =
                        ancPlanning.HRef = 
                        ancSettings.HRef = ancreports.HRef = "";
                }
                else
                {
                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HomePage).ToString(); // Home
                    ancHome.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancHome.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/HomeDetailView.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Recruitment).ToString(); // Recruitment
                    ancPlanning.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancPlanning.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Vacancy.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Employee).ToString(); // Employee
                    ancEmployee.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancEmployee.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Employee.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Documents).ToString(); // Documents
                    ancDocument.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancDocument.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Passport.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Performance).ToString(); // Performance
                    ancPerformance.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancPerformance.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/PerformanceInitiation.aspx" : "";

                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Reports).ToString(); // Reports
                    ancreports.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancreports.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/reporting/RptCommonEss.aspx" : "";


                    dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HRSettings).ToString(); // Settings
                    ancSettings.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    ancSettings.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/RequestSettings.aspx" : "";

                }

            }

            if (RoleID == 4) // interviewer
            {
                ancPlanning.Disabled = false;
                ancPlanning.HRef = "~/Public/Vacancy.aspx";
                ancHome.Disabled = false;
                ancHome.HRef = "~/Public/HomeDetailView.aspx";
                // Response.Redirect("~/public/Job.aspx");
            }



            if (RoleID < 5) ancManager.Visible = true;
            if (RoleID == 5) ancManager.Visible = false;

            if (RoleID > 5)
            {
                if (objRoleSettings.GetModulePermissions(RoleID, 0).Rows.Count > 0)
                {
                    ancManager.Visible = true;
                }
                else
                {
                    ancManager.Visible = false;
                }
            }

            if (!clsGlobalization.LicenseStatus)
                hrSts.Text = "You are using cracked HR software of ES3 Technovations";
        }
        GetCompanyDetails();
    }
    private void GetCompanyDetails()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        objUserMaster.CmpID = objUserMaster.GetCompanyId();
        DataTable dt = objUserMaster.GetUserCompanyDetails();
        if (dt.Rows.Count > 0)
        {
            imgCompanyLogo.ImageUrl = GetLogo(90);
            lblCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
        }
    }
    public string GetLogo(int width)
    {
        clsUserMaster objUser = new clsUserMaster();
        return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + objUser.GetCompanyId() + "&t=" + DateTime.Now.Ticks;
    }

    protected void imgChangeCompany_Click(object sender, ImageClickEventArgs e)
    {
        //mdChangeCompany.Show();
        //updChangeCompany.Update();
    }
}
