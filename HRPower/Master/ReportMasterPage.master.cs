﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class Master_ReportMasterPage : System.Web.UI.MasterPage
{

    private void SetTopMenus()
    {
        LtDocuments.Text = GetGlobalResourceObject("MasterPageCommon", "Documents").ToString();
        LtEmployee.Text = GetGlobalResourceObject("MasterPageCommon", "Employee").ToString();
        LtHome.Text = GetGlobalResourceObject("MasterPageCommon", "Home").ToString();
        //LtPerformance.Text = GetGlobalResourceObject("MasterPageCommon", "Performance").ToString();
        LTPerformanceAndTraining.Text = GetGlobalResourceObject("MasterPageCommon", "Performance").ToString();
        LTPlanningAndRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();

        //LtRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();
        LtReports.Text = GetGlobalResourceObject("MasterPageCommon", "Reports").ToString();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        hidCulture.Value = clsUserMaster.GetCulture().ToString();
        imgLogo.Src = GetGlobalResourceObject("MasterPageCommon", "Logo").ToString();
        SetTopMenus();
        if (!IsPostBack)
        {
            SetWelcomeText();
            SetPermissions();
            GetCompanyDetails();
            if (!clsGlobalization.LicenseStatus)               
                hrSts.Text = "You are using cracked HR software of ES3 Technovations LLP.";
        }
    }
  
    private void SetWelcomeText()
    {
        lblWelcomeText.Text = new clsEmployee(){EmployeeID = new clsUserMaster().GetEmployeeId()}.GetWelcomeText();
    }

    private void SetPermissions()
    {
        int RoleID = new clsUserMaster().GetRoleId();

        if (RoleID > 3)
        {
            DataTable dtPermissions = clsReportCommon.GetPermissions(RoleID);

            if (dtPermissions.Rows.Count == 0)
            {
                ancAttendance.Disabled =ancLiveAttendance.Disabled =ancOffday.Disabled =ancWorksheet.Disabled =  ancCandidate.Disabled = ancDocument.Disabled = ancEmployeeProfile.Disabled = ancInterviewProcess.Disabled = ancInterviewSchedule.Disabled = ancJobOpening.Disabled = ancLeaveSummary.Disabled = ancPayments.Disabled =  ancPaySlip.Disabled = ancAsset.Disabled = ancSalaryStructure.Disabled = ancSettlement.Disabled = ancVacation.Disabled = ancESS.Disabled = true;
                ancAttendance.HRef = ancLiveAttendance.HRef = ancOffday.HRef = ancWorksheet.HRef = ancCandidate.HRef = ancDocument.HRef = ancEmployeeProfile.HRef = ancInterviewProcess.HRef = ancInterviewSchedule.HRef = ancJobOpening.HRef = ancLeaveSummary.HRef = ancPayments.HRef = ancPaySlip.HRef = ancAsset.HRef = ancSalaryStructure.HRef = ancSettlement.HRef = ancVacation.HRef = ancESS.HRef = "";
            }
            else
            {
                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.ESSReport;
                ancESS.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancESS.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptCommonEss.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.AttendanceReport;
                ancAttendance.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancAttendance.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptMISAttendance.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.CandidateReport;
                ancCandidate.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancCandidate.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptCandidate.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.DocumentReport;
                ancDocument.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancDocument.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptDocument.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.EmployeeProfileReport;
                ancEmployeeProfile.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancEmployeeProfile.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptEmployeeProfile.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.InterviewProcessReport;
                ancInterviewProcess.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancInterviewProcess.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptInterviewProcess.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.InterviewScheduleReport;
                ancInterviewSchedule.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancInterviewSchedule.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptInterviewSchedule.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.JobOpeningReport;
                ancJobOpening.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancJobOpening.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptJobOpening.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.LeaveSummaryReport;
                ancLeaveSummary.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancLeaveSummary.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptMISLeaveSummary.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.PaymentReport;
                ancPayments.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPayments.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptPayments.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.PaySlip;
                ancPaySlip.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPaySlip.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptSalarySlip.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.SalaryStructureReport;
                ancSalaryStructure.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancSalaryStructure.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptMISSalaryStructure.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.SettlementReport;
                ancSettlement.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancSettlement.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptSettlement.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.VacationReport;
                ancVacation.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancVacation.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptVacation.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.PerformanceInitiationReport;
                ancPerformanceInitation.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPerformanceInitation.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptPerformanceInitiation.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.PerformanceEvaluationReport;
                ancPerformanceEvaluation.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPerformanceEvaluation.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptPerformanceEvaluation.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.PerformanceActionReport;
                ancPerformanceAction.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPerformanceAction.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptPerformanceAction.aspx" : "";






                //Project cost report

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.ProjectCostReport;

                //commented for SSP -----------------
                    //ancProjectCost.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                    //ancProjectCost.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptProjectCost.aspx" : "";

                //General Expense Report

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.GeneralExpenseReport ;
                ancGeneralExpense.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancGeneralExpense.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptGeneralExpense.aspx" : "";

                //Asset Report

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.AssetReport;
                ancAsset.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancAsset.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptAsset.aspx" : "";


                //LiveAttendanceReport
                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.LiveAttendanceReport;
                ancLiveAttendance.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancLiveAttendance.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptEmployeePunching.aspx" : "";

                //WorkSheetReport

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.WorksheetReport;
                ancWorksheet.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancWorksheet.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptWorksheet.aspx" : "";

                //OffDayReport

                dtPermissions.DefaultView.RowFilter = "MenuID = " + (int)eMenuID.OffdayMarkReport;
                ancOffday.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancOffday.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "../reporting/RptOffDayMark.aspx" : "";

            }
        }
    }

    private void GetCompanyDetails()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        objUserMaster.CmpID = objUserMaster.GetCompanyId();
        DataTable dt = objUserMaster.GetUserCompanyDetails();
        if (dt.Rows.Count > 0)
        {
            imgCompanyLogo.ImageUrl = GetLogo(90);
            lblCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
        }
    }
    public string GetLogo(int width)
    {
        clsUserMaster objUser = new clsUserMaster();
        return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + objUser.GetCompanyId() + "&t=" + DateTime.Now.Ticks;
    }


    protected void imgChangeCompany_Click(object sender, ImageClickEventArgs e)
    {
        //mdChangeCompany.Show();
        //updChangeCompany.Update();
    }
}
