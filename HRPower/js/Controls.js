﻿
function calcDuration(ctl)
{

    var ddlShiftType = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "ddlShiftType");
    var txtFromTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtFromTime");
    var txtToTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtToTime");
    var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDuration");    
    var txtMinWorkingHrs = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtMinWorkingHrs");  
//    var txtNoOfTimings = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtNoOfTimings");  
    var txtLateAfter = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtLateAfter");  
    var txtEarlyBefore = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtEarlyBefore"); 
    var txtAllowedBreakTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtAllowedBreakTime"); 
    
    var FromHour = parseFloat(txtFromTime.value.substr(0, 2));
    var FromMin = parseFloat(txtFromTime.value.substr(3, 2));
    var isAMFromTime = (txtFromTime.value.lastIndexOf('AM') != -1);
    
    var ToHour = parseFloat(txtToTime.value.substr(0, 2));
    var ToMin = parseFloat(txtToTime.value.substr(3, 2));
    var isAMToTime = (txtToTime.value.lastIndexOf('AM') != -1);
    
    txtDuration.value = TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
    txtMinWorkingHrs.value = TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
    
    if (isNaN(parseFloat(txtDuration.value)) == true) txtDuration.value=0;
    if (isNaN(parseFloat(txtMinWorkingHrs.value)) == true) txtMinWorkingHrs.value=0;
    
    if(ddlShiftType.value == "2")    // Flexi
    {
         // txtDuration.disabled = false;
//        txtNoOfTimings.disabled=true;
//        txtNoOfTimings.value="";
//        txtNoOfTimings.className = "textbox_disabled";
        
        txtLateAfter.disabled=true;
        txtLateAfter.value="";
        txtLateAfter.className = "textbox_disabled";
        
        txtEarlyBefore.disabled=true;
        txtEarlyBefore.value="";
        txtEarlyBefore.className = "textbox_disabled";
        
        txtAllowedBreakTime.disabled=true;
        txtAllowedBreakTime.className = "textbox_disabled"; 
        
        txtMinWorkingHrs.disabled=false;
        txtMinWorkingHrs.className = "textbox";    
    }
    
//    else if(ddlShiftType.value == "3")   //dynamic
//    {
//       txtNoOfTimings.disabled=false;
//       txtNoOfTimings.className = "textbox_mandatory";
//       txtAllowedBreakTime.disabled=true;
//       txtAllowedBreakTime.className = "textbox_disabled";
//       
//        txtLateAfter.disabled=false;
//        txtLateAfter.value="";
//        txtLateAfter.className = "textbox";
//        
//        txtEarlyBefore.disabled=false;
//        txtEarlyBefore.value="";
//        txtEarlyBefore.className = "textbox";
//        
//        txtMinWorkingHrs.disabled=true;
//        txtMinWorkingHrs.className = "textbox_disabled";
//        
//       SetDynamicPolicyTab('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_pnlpTab2')
//       
//       SplitDuration(ctl);
//       
//       if(txtNoOfTimings.value!=0 ||  txtNoOfTimings.value!="")
//       {
//             var dlDynamicShiftDetails = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "dlDynamicShiftDetails");
//             if(dlDynamicShiftDetails!=null)
//             {
//                 for(var i = 0; i < dlDynamicShiftDetails.rows.length; i++)
//                 {
//                    var txtDynamicFromTime=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[0];
//                    var txtDynamicToTime=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[1];
//                    var txtDynamicDuration=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[2];
//                    var txtDynamicMinWorkingHrs=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[4];
//                    
//                    txtDynamicDuration.value=txtMinWorkingHrs.value;
//                    txtDynamicMinWorkingHrs.value=txtMinWorkingHrs.value;
//                    
//                    if(i==0)
//                    {
//                       txtDynamicFromTime.value = txtFromTime.value;
//                       
//                       txtDynamicToTime.value=GetToTime(txtDynamicFromTime.value,txtMinWorkingHrs.value);
//                      
//                    }
//                    else
//                    {
//                        
//                        txtDynamicFromTime.value=dlDynamicShiftDetails.rows[i-1].getElementsByTagName("INPUT")[1].value;
//                        
//                        txtDynamicToTime.value = GetToTime(txtDynamicFromTime.value,txtMinWorkingHrs.value);
//                        
//                    }
//                 }
//             }
//       }
//       
//    }
//    else if(ddlShiftType.value == "4")
//    {
//       txtNoOfTimings.disabled=false;
//       txtNoOfTimings.className = "textbox_mandatory";
//       
//       txtAllowedBreakTime.disabled=false;
//       txtAllowedBreakTime.className = "textbox";
//       
//         txtMinWorkingHrs.disabled=false;
//        txtMinWorkingHrs.className = "textbox";    
//       
//       SetDynamicPolicyTab('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_pnlpTab3')
//       
//       SplitDuration(ctl);
//       
//        if(txtNoOfTimings.value!=0 ||  txtNoOfTimings.value!="")
//       {
//             var dlSplitShiftDetails = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "dlSplitShiftDetails");
//             if(dlSplitShiftDetails!=null)
//             {
//                 for(var i = 0; i < dlSplitShiftDetails.rows.length; i++)
//                 {
//                    var txtSplitFromTime=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[0];
//                    var txtSplitToTime=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[1];
//                    var txtSplitDuration=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[2];
//                  
//                    txtSplitDuration.value=txtMinWorkingHrs.value;
//                    
//                    if(i==0)
//                    {
//                       txtSplitFromTime.value = txtFromTime.value;
//                       
//                       txtSplitToTime.value=GetToTime(txtSplitFromTime.value,txtMinWorkingHrs.value);
//                      
//                    }
//                    else
//                    {  
//                        txtSplitFromTime.value=dlSplitShiftDetails.rows[i-1].getElementsByTagName("INPUT")[1].value;
//                        
//                        txtSplitToTime.value = GetToTime(txtSplitFromTime.value,txtMinWorkingHrs.value); 
//                    }
//                 }
//             }
//             txtMinWorkingHrs.value=txtDuration.value;
//       }
//       
//    }
    else
    {
//        txtNoOfTimings.disabled=true;
//        txtNoOfTimings.value="";
//        txtNoOfTimings.className = "textbox_disabled";
        
        txtAllowedBreakTime.disabled=false;
        txtAllowedBreakTime.className = "textbox";
       
        txtLateAfter.disabled=false;
        txtLateAfter.value="";
        txtLateAfter.className = "textbox";
        
        txtEarlyBefore.disabled=false;
        txtEarlyBefore.value="";
        txtEarlyBefore.className = "textbox";
        
        txtMinWorkingHrs.disabled=false;
        txtMinWorkingHrs.className = "textbox";  
        
//        SplitDuration(ctl);  
     }  
     
     if(ddlShiftType.value == "2" || ddlShiftType.value == "1")
     {
        SetDynamicPolicyTab('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_pnlpTab1')
        var div =document.getElementById('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_divpTab2') ; 
        div.style.display="none";
        var tab = document.getElementById('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_pnlpTab2');
        tab.style.display="none";
        
        var div3=document.getElementById('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_divpTab3') ; 
        div3.style.display="none";
        var tab3 = document.getElementById('ctl00_ctl00_content_menu_content_FormView3_ucShiftPolicy_pnlpTab3');
        tab3.style.display="none";
     }
       
        //setTimeout("__doPostBack(\'" + ctl.id + "\',\'\')", 0);
       
}
function SetAbsentPolicyTab(id)     // AbsentPolicy.ascx
{
    var div1,div2,tab1,tab2,div3,tab3;
  
    if(id.lastIndexOf('pnlpTab1')!= -1)
    {
        div1 = document.getElementById(id.replace(/pnlpTab1/,'divpTab1'));
        div2 = document.getElementById(div1.id.replace(/divpTab1/,'divpTab2'));
        tab1 = document.getElementById(id);
        tab2 = document.getElementById(id.replace(/pnlpTab1/,'pnlpTab2'));
        div1.style.display = tab1.style.display = "block";
        tab1.className = "TabbedPanelsTabSelected";
        div2.style.display = "none";
        tab2.className =    "TabbedPanelsTab";
    }
    else
    {
        var chkAbsentPolicyOnly=document.getElementById(id.replace(/pnlpTab2/,'chkAbsentPolicyOnly'));
       
        div2 = document.getElementById(id.replace(/pnlpTab2/,'divpTab2'));
        div1 = document.getElementById(div2.id.replace(/divpTab2/,'divpTab1'));
        tab2 = document.getElementById(id);
        tab1 = document.getElementById(id.replace(/pnlpTab2/,'pnlpTab1'));
         if(chkAbsentPolicyOnly.checked)
        {
            div1.style.display = tab1.style.display = "block";
            tab1.className =  "TabbedPanelsTabSelected";
            div2.style.display = "none";
            tab2.className = "TabbedPanelsTab";
        }
        else
        {
            div2.style.display = tab2.style.display = "block";
            tab2.className =  "TabbedPanelsTabSelected";
            div1.style.display = "none";
            tab1.className = "TabbedPanelsTab";
        }
        
    }
}

function SetAbsentPolicyOnly(ctl)
{
    var chkMergeBoth=document.getElementById(ctl.id.replace("chkAbsentPolicyOnly","chkMergeBoth"));

     var div1,div2,tab1,tab2;
     div2 = document.getElementById(ctl.id.replace(/chkAbsentPolicyOnly/,'divpTab2'));
     
    // div1 = document.getElementById(div2.id.replace(/divpTab2/,'divpTab1'));
     tab2 = document.getElementById(ctl.id.replace(/chkAbsentPolicyOnly/,'pnlpTab2'));
     tab1 = document.getElementById(ctl.id.replace(/chkAbsentPolicyOnly/,'pnlpTab1'));
     div1 = document.getElementById(ctl.id.replace(/chkAbsentPolicyOnly/,'divpTab1'));
    
    if(ctl.checked)
    {
        chkMergeBoth.checked=false;
        chkMergeBoth.disabled=true;
        
          div1.style.display = tab1.style.display = "block";
          tab1.className =  "TabbedPanelsTabSelected";
          div2.style.display = "none";
          tab2.className = "TabbedPanelsTab";
    }
    else
    {
        chkMergeBoth.disabled=false;       
    }
}
function SetRatePerDay(ctl)
{
    var rblCompanyBased=document.getElementById(ctl.id.replace(/chkRatePerDay/,"rblCompanyBased"));
    var ddlCalculationBasedOn=document.getElementById(ctl.id.replace(/chkRatePerDay/,"ddlCalculationBasedOn"));
    var cblParticulars=document.getElementById(ctl.id.replace(/chkRatePerDay/,"cblParticulars"));
    var txtHoursPerDay=document.getElementById(ctl.id.replace(/chkRatePerDay/,"txtHoursPerDay"));
    var txtAbsentRate=document.getElementById(ctl.id.replace(/chkRatePerDay/,"txtAbsentRate"));
    var chkRateOnly=document.getElementById(ctl.id.replace(/chkRatePerDay/,"chkRateOnly"));
    
    if(ctl.checked)
    {
        rblCompanyBased.disabled = false;
        ddlCalculationBasedOn.disabled =false;
        ddlCalculationBasedOn.className = "dropdownlist_mandatory";
       cblParticulars.disabled = (ddlCalculationBasedOn.value == "1" ? true : false);

//        cblParticulars.disabled = true;
        
        txtHoursPerDay.disabled = false;
        txtHoursPerDay.className = "textbox_mandatory";
        txtAbsentRate.disabled = true;
        txtAbsentRate.value="";
        txtAbsentRate.className = "textbox_disabled";
        chkRateOnly.checked = false;
        ddlCalculationBasedOn.className = "dropdownlist_mandatory";
       
    }
    else
    {
        
       // divParticular.Attributes["disabled"] = "true";
        txtHoursPerDay.disabled = true;
        txtHoursPerDay.value="";
        txtHoursPerDay.className="textbox_disabled";
        //txtAbsentRate.disabled = true;
        //txtAbsentRate.className="textbox_disabled";
        //cblParticulars.disabled = (ddlCalculationBasedOn.value == "1" ? true : false);
        
       
       
       
    }
}

function SetRateOnly(ctl)
{
    var rblCompanyBased=document.getElementById(ctl.id.replace(/chkRateOnly/,"rblCompanyBased"));
    var ddlCalculationBasedOn=document.getElementById(ctl.id.replace(/chkRateOnly/,"ddlCalculationBasedOn"));
    var cblParticulars=document.getElementById(ctl.id.replace(/chkRateOnly/,"cblParticulars"));
    var txtHoursPerDay=document.getElementById(ctl.id.replace(/chkRateOnly/,"txtHoursPerDay"));
    var txtAbsentRate=document.getElementById(ctl.id.replace(/chkRateOnly/,"txtAbsentRate"));
    var chkRatePerDay=document.getElementById(ctl.id.replace(/chkRateOnly/,"chkRatePerDay"));
    
    if(ctl.checked)
    {
        rblCompanyBased.disabled = true;
        ddlCalculationBasedOn.disabled = true;
        ddlCalculationBasedOn.className = "dropdownlist_disabled";

        cblParticulars.disabled = true;
        txtHoursPerDay.disabled = true;
        txtHoursPerDay.value="";
        txtHoursPerDay.className = "textbox_disabled";
       // divParticular.Attributes["disabled"] = "true";

        txtAbsentRate.disabled = false;
        txtAbsentRate.className = "textbox_mandatory";
        
        chkRatePerDay.checked = false;
        
    }
    else
    {
        txtAbsentRate.disabled = true;
        txtAbsentRate.className = "textbox_disabled";
        txtAbsentRate.value="";
        rblCompanyBased.disabled = false;
        ddlCalculationBasedOn.disabled = false;
 
        cblParticulars.disabled = (ddlCalculationBasedOn.value == "1" ? true : false);

        ddlCalculationBasedOn.className = "dropdownlist_mandatory";
    }
}

function SetRateOnlyS(ctl)
{
    var txtRatePerHourS=document.getElementById(ctl.id.replace(/chkRateOnlyS/,"txtRatePerHourS"));
    var txtHoursPerDayS=document.getElementById(ctl.id.replace(/chkRateOnlyS/,"txtHoursPerDayS"));
    var cblParticularsS=document.getElementById(ctl.id.replace(/chkRateOnlyS/,"cblParticularsS"));
    var ddlCalculationBasedOnS=document.getElementById(ctl.id.replace(/chkRateOnlyS/,"ddlCalculationBasedOnS"));
    var rblCompanyBasedS=document.getElementById(ctl.id.replace(/chkRateOnlyS/,"rblCompanyBasedS"));
    
    if(ctl.checked)
    {
        txtRatePerHourS.disabled = false;
        txtRatePerHourS.className = "textbox_mandatory";

        rblCompanyBasedS.disabled = true;
        ddlCalculationBasedOnS.disabled = true;
        ddlCalculationBasedOnS.className = "dropdownlist_disabled";

        cblParticularsS.disabled = true;
        txtHoursPerDayS.disabled = true;
        txtHoursPerDayS.className = "textbox_disabled";
        txtHoursPerDayS.value="";

    }
    else
    {
        txtRatePerHourS.disabled = true;
        txtRatePerHourS.className = "textbox_disabled";

        rblCompanyBasedS.disabled = false;
        ddlCalculationBasedOnS.disabled = false;
        ddlCalculationBasedOnS.className = "dropdownlist_mandatory";

        cblParticularsS.disabled = false;
        txtHoursPerDayS.disabled = false;
        txtHoursPerDayS.className = "textbox_mandatory";
    }
}
function GetToTime(FromTime,MinWorkingHrs)
{
    var date = new Date('January 1,2010 ' + FromTime.replace(' ', ':00 '));
    var customTime = new CustomDate(date);
    
   // new Date('January 1, 2010 ' + datalist.rows[0].getElementsByTagName("input")[0].value.replace(' ', ':00 '));
   
   // var date2= new Date('January 1,2010 03:22:00');
     var date2=new Date('January 1,2010 ' + MinWorkingHrs.replace(' ', ':00 '));
    var customTime2 = new CustomDate(date2);
    
    var hrs=0;
    var IsAM=false;
     var mins=customTime.Minutes + customTime2.Minutes;
     if(mins>=60)
     {
        hrs=hrs+1;
        mins=mins-60;
     }
    hrs = hrs + customTime.Hours + customTime2.Hours;
    
    if(hrs>12)
    {
        if(hrs>24)
        {
            hrs=hrs-24;
            IsAM=true;
        }
        else
        {
            hrs=hrs-12;
            IsAM=false;
        }
        
    }    
    else
    {
        IsAM=true;
    }
    if(IsAM==true)
    {
       var resTime = (hrs < 10 ? "0" + hrs : hrs).toString() + ":" + (mins < 10 ? "0" + mins : mins).toString() + " AM";
    }
    else
    {
         var resTime = (hrs < 10 ? "0" + hrs : hrs).toString() + ":" + (mins < 10 ? "0" + mins : mins).toString() + " PM";
    }
    return resTime;
   
}

function SetDynamicPolicyTab(id)     // shift form
{
    var div1,div2,tab1,tab2,div3,tab3;
  
    if(id.lastIndexOf('pnlpTab2')!= -1)
    {
        div2 = document.getElementById(id.replace(/pnlpTab2/,'divpTab2'));
        div1 = document.getElementById(div2.id.replace(/divpTab2/,'divpTab1'));
        tab2 = document.getElementById(id);
        tab1 = document.getElementById(id.replace(/pnlpTab2/,'pnlpTab1'));
        div2.style.display = tab2.style.display = "block";
        tab2.className = "TabbedPanelsTabSelected";  
        div1.style.display = "none";
        tab1.className = "TabbedPanelsTab";
        
        div3=document.getElementById(div2.id.replace(/divpTab2/,'divpTab3'));
        tab3 = document.getElementById(id.replace(/pnlpTab2/,'pnlpTab3'));
        div3.style.display = "none";
        //tab3.className = "TabbedPanelsTab";
        tab3.style.display="none";
    }
    else if(id.lastIndexOf('pnlpTab3')!= -1)
    {
        div3 = document.getElementById(id.replace(/pnlpTab3/,'divpTab3'));
        div1 = document.getElementById(div3.id.replace(/divpTab3/,'divpTab1'));
        tab3 = document.getElementById(id);
        tab1 = document.getElementById(id.replace(/pnlpTab3/,'pnlpTab1'));
        div3.style.display = tab3.style.display = "block";
        tab3.className = "TabbedPanelsTabSelected";  
        div1.style.display = "none";
        tab1.className = "TabbedPanelsTab";
        
        div2=document.getElementById(div3.id.replace(/divpTab3/,'divpTab2'));
        tab2 = document.getElementById(id.replace(/pnlpTab3/,'pnlpTab2'));
        div2.style.display = "none";
        tab2.style.display="none";
    }
    else
    {
        div1 = document.getElementById(id.replace(/pnlpTab1/,'divpTab1'));
        div2 = document.getElementById(div1.id.replace(/divpTab1/,'divpTab2'));
        tab1 = document.getElementById(id);
        tab2 = document.getElementById(id.replace(/pnlpTab1/,'pnlpTab2'));
        div1.style.display = tab1.style.display = "block";
        tab1.className = "TabbedPanelsTabSelected";  
        div2.style.display = "none";
        tab2.className = "TabbedPanelsTab"; 
        
        div3= document.getElementById(div1.id.replace(/divpTab1/,'divpTab3'));
        tab3 = document.getElementById(id.replace(/pnlpTab1/,'pnlpTab3'));
        div3.style.display = "none";
        tab3.className = "TabbedPanelsTab"; 
    }
 
} 

var CustomDate = function(objDate) {

 if(!objDate)
    objDate = new Date();
    
  var date = objDate.copy();
  this.Hours = date.getHours();
  this.Minutes = date.getMinutes();
  this.Date = date;
  this.AddHours = function(hours) {
        date = objDate.setHours(this.Hours + hours);
  };
  this.AddTimeSpan = function(objEmtptyTimeSpan) {
        date.setHours(date.getHours() + objEmtptyTimeSpan.Hours);
        date.setMinutes(date.getMinutes() + objEmtptyTimeSpan.Minutes);
  };
};




var TimeSpan = function(smallerTime, biggerTime) {
    var minTimeMeridien = smallerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    var maxTimeMeridien = biggerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    smallerTime = smallerTime.replace(minTimeMeridien, ':00 '+ minTimeMeridien);
    biggerTime = maxTime.replace(maxTimeMeridien, ':00 '+ maxTimeMeridien);
    var tempDate1 = new Date('January 1,2010 '+ smallerTime);
    var tempDate2 = new Date('January 1,2010 '+ biggerTime);
    var day = ( (minTimeMeridien == 'PM' &&  maxTimeMeridien == 'AM') || (minTimeMeridien != maxTimeMeridien && tempDate1 > tempDate2 ) || ( (minTimeMeridien == maxTimeMeridien) && ( tempDate1 > tempDate2 ) )) ? 2 : 1; 
    var date1 = new Date('January 1,2010 '+ smallerTime);
    var date2 = new Date('January ' + day + ',2010 '+ biggerTime);
    this.Seconds = (date2 - date1)/(1000);
    this.Minutes = (this.Seconds/ 60 ) % 60 ;
    this.Hour = parseInt(this.Seconds / (60 * 60));
    this.Days = parseInt(this.Seconds/(60 * 60 * 24)); 
};

function SplitDuration(ctl)
{
    var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDuration");  
    var txtNoOfTimings = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtNoOfTimings");  
    var txtMinWorkingHrs = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtMinWorkingHrs"); 
    var ddlShiftType = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "ddlShiftType");  

     if(txtNoOfTimings.value != 0 || txtNoOfTimings.value != "")
     {
//        if(ddlShiftType.value=="3")
//        {
            var GetHours = parseFloat(txtDuration.value.substr(0, 2));
            var ConvertedMins=parseFloat(GetHours*60);
            
            var GetMins = parseFloat(txtDuration.value.substr(3, 2));
            
            var TotalMinutes=parseFloat(ConvertedMins+GetMins);
            
            var ResMinutes=TotalMinutes/(parseInt(txtNoOfTimings.value));
            
            var ResHours=parseInt(ResMinutes/60);
            var ResMin=parseInt(ResMinutes-(ResHours*60));
            
            txtMinWorkingHrs.value=(ResHours <10 ? "0" + ResHours : ResHours).toString() + ":" + (ResMin <10 ? "0" + ResMin:ResMin).toString();  
       // }
//        else
//        {
//             txtMinWorkingHrs.value=txtDuration.value;
//        }
        
     } 
     
    function SetMinWorkingHr(ctl)   // to set min working hours in split shift
    {
         var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDuration");  
        
         var txtMinWorkingHrs = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtMinWorkingHrs");   
         
         txtMinWorkingHrs.value=txtDuration.value;
    }
     
    var date = new Date('January 1,2010 05:22:00 PM');
    var customTime = new CustomDate(date);
    
    var date2= new Date('January 1,2010 03:22:00');
    var customTime2 = new CustomDate(date2);
    
    var hrs=0;
    var IsAM=false;
    var mins=customTime.Minutes + customTime2.Minutes;
     if(mins.value>=60)
     {
        hrs=hrs+1;
        mins=mins.value-60;
     }
    hrs+=customTime.Hours + customTime2.Hours;
    
    if(hrs>12)
    {
        hrs=hrs-12;
        IsAM=false;
    }    
    else
    {
        IsAM=true;
    }
    if(IsAM==true)
    {
       var resTime = (hrs < 10 ? "0" + hrs : hrs).toString() + ":" + (mins < 10 ? "0" + mins : mins).toString() + " AM";
    }
    else
    {
         var resTime = (hrs < 10 ? "0" + hrs : hrs).toString() + ":" + (mins < 10 ? "0" + mins : mins).toString() + " PM";
    }
     customTime.Date;
}



function checkTemplateContent(source,args)
    {
       var content= document.getElementById("ctl00_ctl00_content_public_content_fvTemplates_txtBody_ctl02_ctl01");
       if(content!=null) 
       {      
            if(content.value=='')
            {
                args.IsValid = false;
                
            }
            else
                args.IsValid = true;
       }
          
       
    }

function TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime)
{
    if(isAMFromTime == false && FromHour < 12) FromHour = FromHour + 12;
    if(isAMToTime == false && ToHour < 12) ToHour = ToHour + 12;
    if(isAMFromTime == true && FromHour == 12) FromHour = 24;
    if(isAMToTime == true && ToHour == 12) ToHour = 24;
    
    if(isAMFromTime == false && isAMToTime == true && ToHour < 24) ToHour = ToHour + 24;
    if(isAMFromTime == isAMToTime && FromHour > ToHour) ToHour = ToHour + 24;
    if(ToMin < FromMin)
    {
        if(FromHour != ToHour)
        {
            ToMin = ToMin + 60;
            ToHour = ToHour - 1;
        }
        else
        {
            ToMin = ToMin + 60;
            FromHour = FromHour + 23;
        }
    }
    
    var MinDiff = ToMin - FromMin;
    var HourDiff = ToHour - FromHour;
    
    if(HourDiff < 0) HourDiff = -(HourDiff);
    
    // *****added newly
    if(HourDiff == 0 && MinDiff == 0)
    {
        HourDiff=24;
    }
    
    //******
    
    return (HourDiff < 10 ? "0" + HourDiff : HourDiff).toString() + ":" + (MinDiff < 10 ? "0" + MinDiff : MinDiff).toString();
}

function ValidateTimes(source, args)
{
    var txtFromTime = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_') + 1) + "txtFromTime");
    var txtToTime = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_') + 1) + "txtToTime");
    
    if(txtFromTime.value == txtToTime.value)
    {
        args.IsValid = false;
    }
}

function ValidateMinHours(source, args)
{
    var txtMinWorkingHrs = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_') + 1) + "txtMinWorkingHrs");
     if (isNaN(parseFloat(txtMinWorkingHrs.value)) == false ) 
     {
         if (txtMinWorkingHrs.value=="00:00")
         {
            args.IsValid = false;
         }
     }
}

function ChangeShiftIndex(id)
{
    var btnDescription = document.getElementById(id.substr(0, id.lastIndexOf('_') + 1) + "btnDescription");
    
    btnDescription.click();
}

function confirmDelete(btnId, valueType)
{
    var ddlList = document.getElementById(btnId.replace(/btnDelete/, "ddlList"));
    
    if(ddlList.value != "-1")
    {
        return confirm('Are you sure to delete this ' + valueType.toLowerCase() + '?');
    }
    else { return false; }
}

function validateEdit(btnId)
{
    var ddlList = document.getElementById(btnId.replace(/btnEdit/, "ddlList"));
    
    if(ddlList.value == "-1") { return false; }
    else { return true; }
}

function ChangeCurrencyIndex(id)
{    
    var lnkRequestType = document.getElementById(id.substr(0, id.lastIndexOf('_') + 1) + "lnkRequestType");
    
    lnkRequestType.click();
}

function ApplyToAll(anc)
{
    var ctlPrefix = document.getElementById(anc.id.replace(/ancContext/, "hdSender")).value.substring(0, document.getElementById(anc.id.replace(/ancContext/, "hdSender")).value.lastIndexOf('_') + 1);
    
    var chkWorkingDay, txtBreakDuration, txtFoodBreakTimeOut, txtFoodBreakTimeIn, txtFoodBreakVariableTime, txtConsiderLateAfter, txtLeavingEarlyBefore;
    
    chkWorkingDay = document.getElementById(ctlPrefix + 'chkWorkingDay');
    txtBreakDuration = document.getElementById(ctlPrefix + 'txtBreakDuration');
    txtFoodBreakTimeOut = document.getElementById(ctlPrefix + 'txtFoodBreakTimeOut');
    txtFoodBreakTimeIn = document.getElementById(ctlPrefix + 'txtFoodBreakTimeIn');
    txtFoodBreakVariableTime = document.getElementById(ctlPrefix + 'txtFoodBreakVariableTime');
    txtConsiderLateAfter = document.getElementById(ctlPrefix + 'txtConsiderLateAfter');
    txtLeavingEarlyBefore = document.getElementById(ctlPrefix + 'txtLeavingEarlyBefore');
    
    var chkWorkingDays = document.getElementsByTagName("INPUT");
    
    for(var i = 0; i < chkWorkingDays.length; i++)
    {
        if(chkWorkingDays[i].type == "checkbox")
        {
            if(chkWorkingDays[i].id.lastIndexOf("chkWorkingDay") != -1)
            {
                if(chkWorkingDays[i].checked)
                {
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtBreakDuration")).value = txtBreakDuration.value;
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtFoodBreakTimeOut")).value = txtFoodBreakTimeOut.value;
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtFoodBreakTimeIn")).value = txtFoodBreakTimeIn.value;
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtFoodBreakVariableTime")).value = txtFoodBreakVariableTime.value;
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtConsiderLateAfter")).value = txtConsiderLateAfter.value;
                    document.getElementById(chkWorkingDays[i].id.replace(/chkWorkingDay/, "txtLeavingEarlyBefore")).value = txtLeavingEarlyBefore.value;
                }
            }
        }        
    }
}

function valProjectDateInsert(source, args)
{
    if(!isValidDate(args.Value))
    {
        args.IsValid = false;
    }
    else
    {
        var startDate = Convert2Date(args.Value);
        
        if(startDate < GetSysDate())
        {
            args.IsValid = false;
        }
    }
}

function valProjectDateUpdate(source, args)
{
    if(!isValidDate(args.Value))
    {
        args.IsValid = false;
    }
}


function validateRate(source,args)
{

    var rate= (isNaN(parseInt(args.Value)) ? 0 : parseInt(args.Value));                       
    
//    var rate=args.Value;
    if (rate ==0)
    {
        args.IsValid = false;
    }
}
function confirmSave(validationGroup)
{
   var hidControl = document.getElementById("ctl00_hidCulture");
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    {
        return confirm(hidControl.value == "ar-AE" ?" هل أنت متأكد من حفظ هذه المعلومات؟":  "Are you sure to save this information?");
    }
}

function setupReferences(chkDefaultPolicy)
{
    var rcDepartment = document.getElementById(chkDefaultPolicy.id.substr(0, chkDefaultPolicy.id.lastIndexOf('_')) + '_rcDepartment_pnlReference');
    var rcDesignation = document.getElementById(chkDefaultPolicy.id.substr(0, chkDefaultPolicy.id.lastIndexOf('_')) + '_rcDesignation_pnlReference');
    var rcEmploymentType = document.getElementById(chkDefaultPolicy.id.substr(0, chkDefaultPolicy.id.lastIndexOf('_')) + '_rcEmploymentType_pnlReference');
    var ddlProjects = document.getElementById(chkDefaultPolicy.id.substr(0, chkDefaultPolicy.id.lastIndexOf('_')) + '_ddlProjects');
    
    if(chkDefaultPolicy.checked)
    {
        SetupPanelReference(rcDepartment, false);
        SetupPanelReference(rcDesignation, false);
        SetupPanelReference(rcEmploymentType, false);
        ddlProjects.disabled = "disabled";
        ddlProjects.className = "dropdownlist_disabled";
        ddlProjects.selectedIndex = 0;
    }
    else
    {
        SetupPanelReference(rcDepartment, true);
        SetupPanelReference(rcDesignation, true);
        SetupPanelReference(rcEmploymentType, true);
        ddlProjects.disabled = false;
        ddlProjects.className = "dropdownlist";
    }    
}

function AbsentPolicychkRateReference(chkRateOnly)
{
    var txtAbsentRate=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtAbsentRate');
    var txtHoursPerDay=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtHoursPerDay');
    var ddlcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_ddlcalc');
    var chkRateBasedOnhr=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_chkRateBasedOnhr');
    var rblcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_rblcalc');
    var cblParticulars=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_cblParticulars');
    
    if (chkRateOnly.checked)
    {
        txtAbsentRate.disabled=false;
        ddlcalc.disabled="disabled";
        ddlcalc.selectedIndex = -1;
        txtHoursPerDay.disabled="disabled";
        chkRateBasedOnhr.checked=false;
        txtHoursPerDay.value="";
        rblcalc.disabled="disabled";
        txtAbsentRate.className="textbox_mandatory";
        txtHoursPerDay.clasName="textbox_disabled";
        ddlcalc.className="dropdownlist_disabled";
        cblParticulars.disabled="disabled";
                
    }
    else
    {
        txtAbsentRate.disabled="disabled";
        txtHoursPerDay.disabled=false;
        txtAbsentRate.value="";
        ddlcalc.disabled=false ;
        rblcalc.disabled=false ;
        txtAbsentRate.className="textbox_disabled";
        txtHoursPerDay.clasName="textbox_mandatory";
        ddlcalc.className="dropdownlist_mandatory";
        cblParticulars.disabled=false;
        
    }
}

function EncashPolicychkRateReference(chkRateOnly)
{
    var txtEncashtRate=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtEncashtRate');
    var ddlcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_ddlcalc');
    var txtcalcPercentage=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtcalcPercentage');
    var rblCalcDay = document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_rblCalcDay');
    if (chkRateOnly.checked)
    {
        txtEncashtRate.disabled=false;
        ddlcalc.disabled="disabled";
        ddlcalc.selectedIndex = -1;
        ddlcalc.className="dropdownlist_disabled";
        txtcalcPercentage.disabled="disabled";
        txtEncashtRate.className="textbox_mandatory";  
        txtcalcPercentage.className="textbox_disabled";  
        txtcalcPercentage.value=0;  
        rblCalcDay.disabled="disabled";
    }
    else
    {
        txtEncashtRate.disabled="disabled";
        txtEncashtRate.value="";
        txtEncashtRate.className="textbox_disabled";
        txtcalcPercentage.disabled=false;
        txtcalcPercentage.className="textbox_mandatory";
        ddlcalc.disabled=false ;
        ddlcalc.className="dropdownlist_mandatory";
        rblCalcDay.disabled=false ;
      
    }
}

function AbsentPolicychkRateBasedOnhrReference(chkRateBasedOnhr)
{
     var txtHoursPerDay=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_txtHoursPerDay');
     var txtAbsentRate=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_txtAbsentRate');
     var chkRateOnly=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_chkRateOnly');
     var ddlcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_ddlcalc');
     
    if (chkRateBasedOnhr.checked)
    {
        txtHoursPerDay.disabled=false;
        txtAbsentRate.disabled="disabled";
        chkRateOnly.checked=false;
        txtAbsentRate.value="";
        txtAbsentRate.className="textbox_disabled";
        txtHoursPerDay.className="textbox_mandatory";
//        ddlcalc.disabled=false;
//        ddlcalc.className="dropdownlist_mandatory";
        
    }
    else
    {
        txtHoursPerDay.value="";
        txtHoursPerDay.disabled="disabled";
        txtHoursPerDay.value="";
        txtAbsentRate.disabled="false";
        txtAbsentRate.className="textbox_mandatory";
        txtHoursPerDay.className="textbox_disabled";
//        ddlcalc.disabled="disabled";
//        ddlcalc.className="dropdownlist";
    }

}
function HolidayPolicychkRateReference(chkRateOnly)
{
    var txtHolidayRate=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtHolidayRate');
    var txtHoursPerDay=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtHoursPerDay');
    var ddlcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_ddlcalc');
    var chkRateBasedOnhr=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_chkRateBasedOnhr');
    var txtcalcPercentage=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtcalcPercentage');
    
    if (chkRateOnly.checked)
    {
        txtHolidayRate.disabled=false;
        txtHolidayRate.className = "textbox_mandatory";
        ddlcalc.disabled="disabled";
        ddlcalc.selectedIndex = -1;
        ddlcalc.className="dropdownlist_disabled";
        txtHoursPerDay.disabled="disabled";
        txtHoursPerDay.className="textbox_disabled";
        chkRateBasedOnhr.checked=false;
        txtcalcPercentage.disabled="disabled";
        txtcalcPercentage.className="textbox_disabled";
    }
    else
    {   
        txtHolidayRate.disabled="disabled";    
        txtHolidayRate.className = "textbox_disabled";
        txtHoursPerDay.disabled=false;
        txtHoursPerDay.className="textbox";
        txtHolidayRate.value="";
        ddlcalc.disabled=false ;
        ddlcalc.className="dropdownlist_mandatory";
        txtcalcPercentage.disabled=false;
        txtcalcPercentage.className="textbox_mandatory";
    }
}
function HolidayPolicychkRateBasedOnhrReference(chkRateBasedOnhr)
{
     var txtHoursPerDay=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_txtHoursPerDay');
     var txtHolidayRate=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_txtHolidayRate');
     var chkRateOnly=document.getElementById(chkRateBasedOnhr.id.substr(0, chkRateBasedOnhr.id.lastIndexOf('_')) + '_chkRateOnly');
     var ddlcalc=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_ddlcalc');
      var txtcalcPercentage=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_')) + '_txtcalcPercentage');
      
    if (chkRateBasedOnhr.checked)
    {
        txtHoursPerDay.disabled=false;
        txtHolidayRate.disabled="disabled";
        chkRateOnly.checked=false;
        txtHolidayRate.className = "textbox_disabled";
        txtHoursPerDay.className="textbox_mandatory";
        txtHolidayRate.value="";
        ddlcalc.disabled=false ;
        ddlcalc.className="dropdownlist_mandatory";
        txtcalcPercentage.disabled=false;
        txtcalcPercentage.className="textbox_mandatory";
    }
    else
    {
        txtHoursPerDay.disabled="disabled";
        txtHoursPerDay.value=0;
        txtHolidayRate.disabled="false";
        txtHolidayRate.className = "textbox_disabled";
        txtHoursPerDay.className="textbox_disabled";
        txtHoursPerDay.value="";
        ddlcalc.selectedIndex = -1;
        ddlcalc.className="dropdownlist_mandatory";
        txtcalcPercentage.disabled=false;
        txtcalcPercentage.className="textbox_mandatory";
    }

}


function SetupPanelReference(refControl, enable)
{
    var elements = refControl.getElementsByTagName("INPUT");
    
    for(var i = 0; i < elements.length; i++)
    {
        if(enable)
        {
            elements[i].disabled = false;
        }
        else
        {
            elements[i].disabled = "disabled";
        }
    }
    
    elements = refControl.getElementsByTagName("SELECT");
    
    for(var i = 0; i < elements.length; i++)
    {
        if(enable)
        {
            elements[i].disabled = false;
            elements[i].className = "dropdownlist";
        }
        else
        {
            elements[i].disabled = "disabled";
            elements[i].className = "dropdownlist_disabled";
            elements[i].selectedIndex = 0;
        }
    }
}

function valPolicy(source, args)
{
    var chkDefaultPolicy = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_chkDefaultPolicy');
    var rcDepartment = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_rcDepartment_pnlReference');
    var rcDesignation = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_rcDesignation_pnlReference');
    var rcEmploymentType = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_rcEmploymentType_pnlReference');
    var ddlProjects = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_ddlProjects');
    
    if(chkDefaultPolicy.checked)
    {
        args.IsValid = true;
    }
    else
    {
        var refStatus = false;
        
        elements = rcDepartment.getElementsByTagName("SELECT");
    
        if(elements[0].value != "-1") { refStatus = true; }
        
        elements = rcDesignation.getElementsByTagName("SELECT");
    
        if(elements[0].value != "-1") { refStatus = true; }
        
        elements = rcEmploymentType.getElementsByTagName("SELECT");
    
        if(elements[0].value != "-1") { refStatus = true; }
        
        if(ddlProjects.value != "-1") { refStatus = true; }
        
        args.IsValid = refStatus;
    }
}

function setupLeaveDetailEntry(ddlLeaveType)
{
     //var txtMonthly = document.getElementById(rcLeaveType.id.replace(/rcLeaveType/, "txtMonthly"));
     var txtMonthly = document.getElementById(GetReferenceControlPrefix(ddlLeaveType.id) + "txtMonthly");
    
    
    if(ddlLeaveType.value == "1" || ddlLeaveType.value == "4")
    {   
        txtMonthly.disabled = "disabled";
        txtMonthly.className = "textbox_disabled";
        txtMonthly.value = "";
    }
    else
    {
        txtMonthly.disabled = false;
        txtMonthly.className = "textbox";
    }
}

function validateCarryForward(source, args)
{
    var dlLeaveInformation = document.getElementById(source.id.replace(/cvCarryForward/, "dlLeaveInformation"));
    
    if(dlLeaveInformation != null)
    {
        var controls = dlLeaveInformation.getElementsByTagName("INPUT");
        
        var txtDays, txtCarryForward, txtEncashableDays;
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtDays') != -1)
            {
                txtDays = controls[i];
                txtCarryForward = document.getElementById(txtDays.id.replace(/txtDays/, "txtCarryForward"));
                txtEncashableDays = document.getElementById(txtDays.id.replace(/txtDays/, "txtEncashableDays"));
                
                var Days, Monthly, CarryForward, EncashableDays;
                
                Days = (isNaN(parseInt(txtDays.value)) ? 0 : parseInt(txtDays.value));
                CarryForward = (isNaN(parseInt(txtCarryForward.value)) ? 0 : parseInt(txtCarryForward.value));
                EncashableDays = (isNaN(parseInt(txtEncashableDays.value)) ? 0 : parseInt(txtEncashableDays.value));
                
                if(Days < (CarryForward + EncashableDays))
                {
                    args.IsValid = false;
                    txtCarryForward.focus();
                }
            }
        }
    }
}

function validateMonthly(source, args)
{
    var dlLeaveInformation = document.getElementById(source.id.replace(/cvMonthly/, "dlLeaveInformation"));
    
    if(dlLeaveInformation != null)
    {
        var controls = dlLeaveInformation.getElementsByTagName("INPUT");
        
        var txtDays, txtMonthly;
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtDays') != -1)
            {
                txtDays = controls[i];
                txtMonthly = document.getElementById(txtDays.id.replace(/txtDays/, "txtMonthly"));
                
                var Days, Monthly;
                
                Days = (isNaN(parseInt(txtDays.value)) ? 0 : parseInt(txtDays.value));
                Monthly = (isNaN(parseInt(txtMonthly.value)) ? 0 : parseInt(txtMonthly.value));
                
                if(Monthly > 31 )
                {                
                  source.errormessage = "Monthly leave cannot be greater than 31.";   
                  args.IsValid = false;
                    txtMonthly.focus();
                }
                else if(Days < (Monthly))
                {
                
                 source.errormessage = "Monthly leave cannot be greater than Days.";  
                 args.IsValid = false;
                    txtMonthly.focus();
                }
               
                
            }
        }
    }
}
function setLabel(chkId)
{   
    var chk = document.getElementById(chkId);
    if(chk != null)
    {
        var lblshortname=document.getElementById(chk.id.replace(/chkAcconts/, "lblShortName"));
        var lblgrouphead=document.getElementById(chk.id.replace(/chkAcconts/, "lblGroupHead"));
        var trOpeningBalance=document.getElementById(chk.id.replace(/chkAcconts/, "trOpeningBalance"));
        if(chk.checked)
        {
            trOpeningBalance.style.display="none";
            if(lblshortname.innerText == undefined) { lblshortname.textContent = "Short Name"; }
            else { lblshortname.innerText = "Short Name"; } 
            if(lblgrouphead.innerText == undefined) { lblgrouphead.textContent = "Group Head"; }
            else { lblgrouphead.innerText = "Group Head"; } 
        }
        else
        {
            trOpeningBalance.style.display=trDisplay();
            if(lblshortname.innerText == undefined) { lblshortname.textContent = "Code"; }
            else { lblshortname.innerText = "Code"; } 
            if(lblgrouphead.innerText == undefined) { lblgrouphead.textContent = "Account Head"; }
            else { lblgrouphead.innerText = "Account Head"; } 
        }        
    }   
}

function validateLeaveDays(source, args)
{
    var dlLeaveInformation = document.getElementById(source.id.replace(/cvDays/, "dlLeaveInformation"));
    
    if(dlLeaveInformation != null)
    {
        var controls = dlLeaveInformation.getElementsByTagName("INPUT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtDays') != -1)
            {
                if((isNaN(parseInt(controls[i].value)) ? 0 : parseInt(controls[i].value)) == 0) { args.IsValid = false; }
                //controls[i].focus();}
            }
        }
    }
}

function validatePayPolicyDays(source, args)
{
    var dlPayPolicyInformation = document.getElementById(source.id.replace(/cvDays/, "dlPayPolicyInformation"));
    
    if(dlPayPolicyInformation != null)
    {
        var controls = dlPayPolicyInformation.getElementsByTagName("INPUT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtLimit') != -1)
            {
                if((isNaN(parseInt(controls[i].value)) ? 0 : parseInt(controls[i].value)) == 0) { args.IsValid = false; controls[i].focus();}
            }
        }
    }
}
function validatePayPolicyNRate(source, args)
{
    var dlPayPolicyInformation = document.getElementById(source.id.replace(/cvNRate/, "dlPayPolicyInformation"));
    
    if(dlPayPolicyInformation != null)
    {
        var controls = dlPayPolicyInformation.getElementsByTagName("INPUT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtNormalRate') != -1)
            {
                if((isNaN(parseInt(controls[i].value)) ? 0 : parseInt(controls[i].value)) == 0) { args.IsValid = false; controls[i].focus();}
            }
        }
    }
}
function validatePayPolicyGrid(source, args)
{
    var dlPayPolicyInformation = document.getElementById(source.id.replace(/cvGridCount/, "dlPayPolicyInformation"));
    var hf=document.getElementById(source.id.replace(/cvGridCount/, "hfGridCount"));
    
    if(hf.value!='1')
    {
     args.IsValid = true;
    }
    else
    {
        if(dlPayPolicyInformation == null)
        {
            args.IsValid = false; 
               
        }
    }
}
function validateotherdocumentAttachment(source,args)
{
//     var fuDocument=document.getElementById('ctl00_ctl00_content_public_content_fuDocument_ctl02');
    var fuDocument=document.getElementById('ctl00_public_content_fuDocument_ctl02');
    
     if (fuDocument.value == "")
     {
        args.IsValid = false;
     }         
}
function validateDuplicateLeave(source, args)
{
    var dlLeaveInformation = document.getElementById(source.id.replace(/cvDuplicate/, "dlLeaveInformation"));
    
    if(dlLeaveInformation != null)
    {
        var controls = dlLeaveInformation.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            for(var j = (i + 1); j < controls.length; j++)
            {
                if(controls[i].value == controls[j].value)
                {
                    args.IsValid = false;
                }
            }
        }
    }
}
function validateotherdocnameduplicate(source, args)
{
    var dlOtherDocDoc = document.getElementById(source.id.replace(/cvotherdocnameduplicate/, "dlOtherDoc"));
    
    if(dlOtherDocDoc != null)
    {
        var controls = dlOtherDocDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvotherdocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validatePassportdocnameduplicate(source, args)
{
    var dlPassportDoc = document.getElementById(source.id.replace(/cvPassportdocnameduplicate/, "dlPassportDoc"));
    
    if(dlPassportDoc != null)
    {
        var controls = dlPassportDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvPassportdocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validateLeaseAgreementdocnameduplicate(source, args)
{
    var dlDoc = document.getElementById(source.id.replace(/cvLeaseAgreementdocnameduplicate/, "dlListDoc"));
    
    if(dlDoc != null)
    {
        var controls = dlDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvLeaseAgreementdocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validateInsurancedocnameduplicate(source, args)
{
    var dlDoc = document.getElementById(source.id.replace(/cvInsurancedocnameduplicate/, "dlListDoc"));
    
    if(dlDoc != null)
    {
        var controls = dlDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvInsurancedocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validatevisadocnameduplicate(source, args)
{
    var dlVisaDoc = document.getElementById(source.id.replace(/cvVisadocnameduplicate/, "dlVisaDoc"));
    
    if(dlVisaDoc != null)
    {
        var controls = dlVisaDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvVisadocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validateTradeLicensedocnameduplicate(source, args)
{
    var dlTradeLicenseDoc = document.getElementById(source.id.replace(/cvTradeLicensedocnameduplicate/, "dlTradeLicenseDoc"));
    
    if(dlTradeLicenseDoc != null)
    {
        var controls = dlTradeLicenseDoc.getElementsByTagName("SPAN");
        var txtDocname= document.getElementById(source.id.replace(/cvTradeLicensedocnameduplicate/, "txtDocname"));
        
        for(var i = 0; i < controls.length; i++)
        {
           
                if(controls[i].innerText == txtDocname.value)
                {
                    args.IsValid = false;
                }
       }
    }
}
function validateShiftDetail(source, args)
{

 var dlPolicyDetails = document.getElementById(source.id.replace(/cvShiftDetail/, "dlPolicyDetails"));
 
  
    if(dlPolicyDetails != null)
    {
        for(var j = 1; j < dlPolicyDetails.rows.length; j++)
        {
            var chk = dlPolicyDetails.rows[j].getElementsByTagName("input")[0];
            
            if(chk.checked)
            {
                var ddl = document.getElementById(chk.id.replace(/chkWorkingDay/,"ddlShift"));
                
                if(ddl.value == "-1")
                {
                
                  source.errormessage = "Please select shift for working days.";
                  args.IsValid = false;
                }
            }
            else
            {
                var ddl = document.getElementById(chk.id.replace(/chkWorkingDay/,"ddlShift"));

                if(ddl.value != "-1" )
                {
              
                  source.errormessage = "No need to assign shift/durations for Offdays."; 
                  args.IsValid = false;
                }
            }
                                                                         
            }
        }
        
    }

function validateAllowedDays(source, args)
{
 var dlPolicyConsequence = document.getElementById(source.id.replace(/cvConsequence/, "dlPolicyConsequences"));
 
  
    if(dlPolicyConsequence != null)
    {
        for(var j = 1; j < dlPolicyConsequence.rows.length; j++)
        {
        
            var txtAllowedDays = dlPolicyConsequence.rows[j].getElementsByTagName("input")[1].value;
            
            if(txtAllowedDays.value!="")
            {
               
               
                if(txtAllowedDays.value > "31")
                {
                
                  source.errormessage = "Allowed days cannot be greater than 31";
                  args.IsValid = false;
                }
            }
           
                                                                         
            }
        }
        
    }

function validateDuplicateJob(source, args)
{
    var dlPayPolicyInformation = document.getElementById(source.id.replace(/cvDuplicate/, "dlPayPolicyInformation"));
    
    if(dlPayPolicyInformation != null)
    {
        var controls = dlPayPolicyInformation.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            for(var j = (i + 1); j < controls.length; j++)
            {
                if(controls[i].value == controls[j].value)
                {
                    args.IsValid = false;
                }
            }
        }
    }
}
function hasDefaultLeavePolicy(source, args)
{
    var hdHasDefaultPolicy = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdHasDefaultPolicy");
    var chkDefaultPolicy = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkDefaultPolicy");
        
    if(hdHasDefaultPolicy.value == "True" && chkDefaultPolicy.checked)
    {
        args.IsValid = false;
    }
}
function hasDefaultWorkPolicy(source, args)
{
    var hdHasDefaultPolicy = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdHasDefaultPolicy");
    var chkDefaultPolicy = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkDefaultPolicy");
    var chkActive = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkActive");
        
    if(hdHasDefaultPolicy.value == "True" && chkDefaultPolicy.checked && chkActive.checked)
    {
        args.IsValid = false;
    }
}
function invokeDelete(tblLeaveDetail, e)
{
    if(e.keyCode == 46)
    {
        var btnDeleteDetail = document.getElementById(tblLeaveDetail.id.substring(0, tblLeaveDetail.id.lastIndexOf('_') + 1) + "btnDeleteDetail");
        
        if(btnDeleteDetail != null)
        {
            btnDeleteDetail.click();
        }
    }
}

function showMenu(TextBox, e)
{
    var divContext = document.getElementById(TextBox.id.substring(0,TextBox.id.lastIndexOf('_dlPolicyDetails')+1) + 'divContext');
    
    divContext.style.display = "block";
    divContext.style.left = e.x - 260;
    divContext.style.top = e.y - 25;   
    
    document.getElementById(divContext.id.replace(/divContext/, "hdSender")).value = TextBox.id;
    
    return false; 
}


function compareDurations(duration1, duration2)
{
    var firstHour = ParseInt(duration1.substring(0, 2));
    var firstMin = ParseInt(duration1.substring(3, 5));
    
    var secondHour = ParseInt(duration2.substring(0, 2));
    var secondMin = ParseInt(duration2.substring(3, 5));
    
    if(firstHour > secondHour)
    {
        return 1;
    }
    else if(firstHour == secondHour)
    {
        if(firstMin > secondMin) return 1;
        else if(firstMin == secondMin) return 0;
        else return -1;
    }
    else
    {
        return -1;
    }
}

function setupPolicyConsequence(control)
{
    var ddlLOP, ddlCasual, txtAmount;
    
    ddlLOP = document.getElementById(control.id.substring(0, control.id.lastIndexOf('_') + 1) + "ddlLOP");
    ddlCasual = document.getElementById(control.id.substring(0, control.id.lastIndexOf('_') + 1) + "ddlCasual");
    txtAmount = document.getElementById(control.id.substring(0, control.id.lastIndexOf('_') + 1) + "txtAmount");
    
    if(control == txtAmount && (isNaN(parseFloat(txtAmount.value)) == true ? 0 : parseFloat(txtAmount.value)) != 0)
    {
  
        ddlLOP.selectedIndex = ddlCasual.selectedIndex = 0;
    }
    
    if(control == ddlLOP)
    {
   
        ddlCasual.selectedIndex = 0;
        txtAmount.value = "0";
    }
    
    if(control == ddlCasual)
    {
    
        ddlLOP.selectedIndex = 0;
        txtAmount.value = "0";
    }
}


function validateBreakDuration(txtBreakDuration, errormessage)
{

    var breakTime = parseFloat(txtBreakDuration.value);
    
    var txtShiftTime = document.getElementById(txtBreakDuration.id.substring(0, txtBreakDuration.id.lastIndexOf('dlPolicy')) + 'txtShiftTime');
    var shiftDuration = txtShiftTime.value.substring(txtShiftTime.value.lastIndexOf(' : ') + 3, txtShiftTime.value.length - 1);
    
    var shiftHour = ParseInt(shiftDuration.substring(0, 2));
    var shiftMin = ParseInt(shiftDuration.substring(3, 5));
    
    var shiftTime = (shiftHour * 60) + shiftMin;
    
    if(breakTime > shiftTime)
    {
        alert(errormessage);
        txtBreakDuration.value = "";
    }
}
 function CheckSelectioninCheckBoxList(source, args)
    {
       
        var checkAssociates = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_cblEmployees');
        if(checkAssociates != null)
        {
            var chkList = checkAssociates.getElementsByTagName("input");
            for(var i=0;i<chkList.length ;i++)
            {
                if(chkList[i].checked)
                {
                    args.IsValid = true;
                    return;
                }
            }
        }
        args.IsValid = false;
    }
    function checkLeaveFromToDate(source,args)
    {
        
       
        var ctl2=document.getElementById(source.controltovalidate);

        var ctl1=document.getElementById(ctl2.id.replace(/txtLeaveTodate/,"txtLeaveFromDate"));
        if((ctl1.value!="") && (ctl2.value!=""))
        {
            var sdate = new Date(Convert2Date(ctl1 .value));
          
            var edate=new Date(Convert2Date(ctl2.value));
          
            if(sdate.format("MM/dd/yy")<=edate.format("MM/dd/yy"))
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
}
function setPayPolicysubmitValidation(validationGroup,hfId)
{   
    document.getElementById(hfId).value='1';
    if(typeof(Page_ClientValidate) == "function")
    {
     
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    { 

        return confirm("Are you sure to save this information?");
    }
    else
    {
         document.getElementById(hfId).value='0';
    }
    
   
}

function validateAllowanceDeduction(source, args)  //salary structure
{
    
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvAllowances/, "dlDesignationAllowances"));
    var isValid=false;
    
    if(dlDesignationAllowances != null)
    {
        var controls = dlDesignationAllowances.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('ddlParticulars') != -1)
            {
                if(controls[i].value != "-1")
                {
                   isValid=true;               
                }
                else
                {
                    isValid = false;
                }
            }
        }
        args.IsValid=isValid;
    }

}
function validateEvaluation(datalistId)  //Performance Evaluation
{

    var datalist=document.getElementById(datalistId);
    if(datalist != null)
    {
                
        var controls = document.getElementById(datalistId).getElementsByTagName("TEXTAREA");//check if control is multiline textbox
        
        for(var i = 0; i < controls.length; i++)
        {
              
          if(controls[i].value != "")
                {
               return true;          
                }
        }
        controls =document.getElementById(datalistId).getElementsByTagName("INPUT");
        for(var i = 0; i < controls.length; i++)
        {
       
          if (controls[i].type =="radio")  //check if input is radio button
          {  
              
                if(controls[i].checked ==true)
                {
                    return true;          
                }
          }
        }          
    
   }
   alert("Please enter evaluation for atleast one question.")
   return false ;
}
function validateAddAllowanceDeduction(source, args)  //salary structure
{

    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvAddAllowances/, "dlDesignationAllowances"));
    var isValid = false;

    if (dlDesignationAllowances == null) {
        isValid = false;
        args.IsValid = isValid;
    }
    else {

       
    }
}
function validateRemuneration(source, args)  //salary structure
{

    var dlOtherRemuneration = document.getElementById(source.id.replace(/cvRemuneration/, "dlOtherRemuneration"));
    var isValid = false;

    if (dlOtherRemuneration != null) {
        var controls = dlOtherRemuneration.getElementsByTagName("SELECT");

        for (var i = 0; i < controls.length; i++) {
            if (controls[i].id.lastIndexOf('ddlRemunerationParticulars') != -1) {
                if (controls[i].value != "-1") {
                    isValid = true;
                }
                else {
                    isValid = false;
                }
            }
        }
        args.IsValid = isValid;
    }
}


function validateEmployeeAllowanceDeduction(source, args)  //salary structure
{
    
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvAllowancespnlTab6/, "dlDesignationAllowances"));
    var isValid=false;
    
    if(dlDesignationAllowances != null)
    {
        var controls = dlDesignationAllowances.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('ddlParticulars') != -1)
            {
                if(controls[i].value != "-1")
                {
                   isValid=true;               
                }
                else
                {
                    isValid = false;
                }
            }
        }
        args.IsValid=isValid;
    }
}

function validateEmployeeAmount(source, args)         //salary structure
{
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvAmountpnlTab6/, "dlDesignationAllowances"));
    var bSelected=true;
    
    if(dlDesignationAllowances != null)
    {
        for(var i = 0; i < dlDesignationAllowances.rows.length; i++)
        {
            var ddlpayment = dlDesignationAllowances.rows[i].getElementsByTagName("SELECT")[1];
            var txtamount=dlDesignationAllowances.rows[i].getElementsByTagName("INPUT")[1];
                                  
                    if(ddlpayment.disabled && txtamount.disabled )                      
                    {     
                        args.IsValid = true;                       
                    }
                    else
                    {
                     var famt=(isNaN(parseFloat(txtamount.value))?0 :parseFloat(txtamount.value));
                        if(famt == 0 && ddlpayment.value =="-1") 
                        {
                            args.IsValid = false; 
                        }
                    }   
            
        }
    }       
}

function validateAmount(source, args)         //salary structure
{
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvAmount/, "dlDesignationAllowances"));
    var bSelected=true;
    
    if(dlDesignationAllowances != null)
    {
        for(var i = 0; i < dlDesignationAllowances.rows.length; i++)
        {
            var ddlpayment = dlDesignationAllowances.rows[i].getElementsByTagName("SELECT")[1];
            var txtamount=dlDesignationAllowances.rows[i].getElementsByTagName("INPUT")[1];
                
                    
                    if(ddlpayment.disabled && txtamount.disabled )                      
                    {     
                        args.IsValid = true;                       
                    }
                    else
                    {
                     var famt=(isNaN(parseFloat(txtamount.value))?0 :parseFloat(txtamount.value));
                        if(famt == 0 && ddlpayment.value =="-1") 
                        {
                            args.IsValid = false; 
                        }
                    }   
            
        }
    }       
}

function validateRemunerationAmount(source, args)         //salary structure
{
    var dlOtherRemuneration = document.getElementById(source.id.replace(/cvRemunerationAmount/, "dlOtherRemuneration"));
    var bSelected = true;

    if (dlOtherRemuneration != null) {
        for (var i = 0; i < dlOtherRemuneration.rows.length; i++) {
            var txtRemunerationAmount = dlOtherRemuneration.rows[i].getElementsByTagName("INPUT")[1];

            if (txtRemunerationAmount.disabled) {
                args.IsValid = true;
            }
            else {
                var famt = (isNaN(parseFloat(txtRemunerationAmount.value)) ? 0 : parseFloat(txtRemunerationAmount.value));
                if (famt == 0 ) {
                    args.IsValid = false;
                }
            }

        }
    }
}

function validateDuplicateParticulars(source, args)        //salary structure no need
{
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvDuplicateParticulars/, "dlDesignationAllowances"));
    
    if(dlDesignationAllowances != null)
    {
        var controls = dlDesignationAllowances.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('ddlParticulars') != -1)
            {
                for(var j = (i + 1); j < controls.length; j++)
                {
                    if(controls[j].id.lastIndexOf('ddlParticulars') != -1)
                    {
                        if(controls[i].value == controls[j].value)
                        {
                            args.IsValid = false;
                        }
                    }
                }
            }
        }
    }
}

function validateDuplicatePaymentPolicy(source, args)        //salary structure no need
{
    var dlDesignationAllowances = document.getElementById(source.id.replace(/cvDuplicateParticulars/, "dlDesignationAllowances"));
    
    if(dlDesignationAllowances != null)
    {
        var controls = dlDesignationAllowances.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('ddlPaymentPolicy') != -1)
            {
                for(var j = (i + 1); j < controls.length; j++)
                {
                    if(controls[j].id.lastIndexOf('ddlPaymentPolicy') != -1)
                    {
                        if(controls[i].value == controls[j].value)
                        {
                            args.IsValid = false;
                        }
                    }
                }
            }
        }
    }
}

function chkWorkstatus(ddlWorkStatus)
{
      
    if(ddlWorkStatus.value <=5)
    {
        alert('This workstatus can change through employee separation.');
        ddlWorkStatus.selectedIndex = -1;
        return false;
    }
    else
        return true ;
    
}

//function chkDuplicateParticulars(ddlAllowanceDeduction)        //salary structure 
//{
//    var dlAllowanceDeductions = document.getElementById(ddlAllowanceDeduction.id.substring(0,ddlAllowanceDeduction.id.lastIndexOf('_ctl')));
//    
//    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");    
//   
//    var bDuplicate = false;    
//    
//    for(var i = 0; i < ddlAllowanceDeduction.length; i++)
//    {
//        if(ddlAllowanceDeduction[i].selected)
//        {
//            for(j = 0; j < controls.length; j++)
//            {
//                if((controls[j] != ddlAllowanceDeduction) && (controls[j].id.lastIndexOf('ddlParticulars') != -1))
//                {
//                    if(ddlAllowanceDeduction[i].value == controls[j].value)
//                    {
//                        bDuplicate = true; 
//                                            
//                    }
//                }
//            }
//        }
//    }           
//    
//    if(bDuplicate)
//    {
//        alert('Duplicate value in Particulars.');
//        ddlAllowanceDeduction.selectedIndex = 0;
//        return false;
//    }
//    else
//    {
//        setTimeout("__doPostBack(\'" + ddlAllowanceDeduction.id + "\',\'\')", 0);
//        return true;
//    }
//}
function chkDuplicateParticulars(ddlAllowanceDeduction)        //salary structure 
{
    var dlAllowanceDeductions = document.getElementById(ddlAllowanceDeduction.id.substring(0,ddlAllowanceDeduction.id.lastIndexOf('_ctl')));
    
    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");

    var bDuplicate = false;
    var bBasicCheck = false;  
     
    
    for(var i = 0; i < ddlAllowanceDeduction.length; i++)
    {
        if(ddlAllowanceDeduction[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if((controls[j] != ddlAllowanceDeduction) && (controls[j].id.lastIndexOf('ddlParticulars') != -1))
                {
                    if(ddlAllowanceDeduction[i].value == controls[j].value)
                    {
                        bDuplicate = true; 
                                            
                    }
                }
            }
        }
    }
    
    if(bDuplicate)
    {
        ddlAllowanceDeduction.selectedIndex = 0;
    }
        
    var txtNetAmount=document.getElementById(dlAllowanceDeductions.id.substr(0,dlAllowanceDeductions.id.lastIndexOf('_')) + '_txtNetAmount');
    var txtAdditionAmt=document.getElementById(dlAllowanceDeductions.id.substr(0,dlAllowanceDeductions.id.lastIndexOf('_')) + '_txtAdditionAmt');
    var txtDeductionAmt=document.getElementById(dlAllowanceDeductions.id.substr(0,dlAllowanceDeductions.id.lastIndexOf('_')) + '_txtDeductionAmt');  
    
    var AdditionAmt=0;
    var DeductionAmt=0;
    var NetAmt=0;
 
    var ddlfirstParticualr = dlAllowanceDeductions.rows[0].getElementsByTagName("select")[0];
    if (ddlfirstParticualr.value != "1") {
        ddlfirstParticualr.value = 1;
        bBasicCheck = true;
    }
     
    if(dlAllowanceDeductions!=null)
    {
        for(var i=0;i<dlAllowanceDeductions.rows.length;i++)
        {
           var txtAmount = dlAllowanceDeductions.rows[i].getElementsByTagName("input")[2];
           var ddlParticulars = dlAllowanceDeductions.rows[i].getElementsByTagName("select")[0];
            var hdnDeductionAmount = dlAllowanceDeductions.rows[i].getElementsByTagName("input")[3];
           if(ddlParticulars.value != "-1")
           {
               if(ddlParticulars.options[ddlParticulars.selectedIndex].text.indexOf('+') != "-1")
               {
                  if(txtAmount.value !="")
                  {
                      AdditionAmt=parseFloat(AdditionAmt) + parseFloat(txtAmount.value);
                      NetAmt=parseFloat(NetAmt) + parseFloat(txtAmount.value);
                  }        
               }
               else {
                    if (txtAmount.value != "" && parseFloat(txtAmount.value) != 0) {
                        DeductionAmt = parseFloat(DeductionAmt) + parseFloat(txtAmount.value);
                    }
                    else if(hdnDeductionAmount.value != "")
                    {
                         DeductionAmt = parseFloat(DeductionAmt) + parseFloat(hdnDeductionAmount.value);
                    }
                    
                }
           }
           
        }
    
    NetAmt = parseFloat(NetAmt) - DeductionAmt;
    txtAdditionAmt.value=ConvertToMoney(AdditionAmt);
    txtDeductionAmt.value=ConvertToMoney(DeductionAmt);
    txtNetAmount.value=ConvertToMoney(NetAmt);
    }

    if (bBasicCheck) {
        alert('Please select basic pay');

        return false;
    }
    else if (bDuplicate) {
        alert('Duplicate value in Particulars.');

        return false;
    }
    else {
        setTimeout("__doPostBack(\'" + ddlAllowanceDeduction.id + "\',\'\')", 0);
        return true;
    }
}

function chkDuplicateRemunerationParticulars(ddlRemunerationParticulars)        //salary structure 
{
    var dlOtherRemuneration = document.getElementById(ddlRemunerationParticulars.id.substring(0, ddlRemunerationParticulars.id.lastIndexOf('_ctl')));

    var controls = dlOtherRemuneration.getElementsByTagName("SELECT");

    var bDuplicate = false;

    for (var i = 0; i < ddlRemunerationParticulars.length; i++) {
        if (ddlRemunerationParticulars[i].selected) {
            for (j = 0; j < controls.length; j++) {
                if ((controls[j] != ddlRemunerationParticulars) && (controls[j].id.lastIndexOf('ddlRemunerationParticulars') != -1)) {
                    if (ddlRemunerationParticulars[i].value == controls[j].value) {
                        bDuplicate = true;

                    }
                }
            }
        }
    }

    if (bDuplicate) {
        ddlRemunerationParticulars.selectedIndex = 0;
    }

   if (bDuplicate) {
        alert('Duplicate value in Particulars.');

        return false;
    }
    else {
        setTimeout("__doPostBack(\'" + ddlRemunerationParticulars.id + "\',\'\')", 0);
        return true;
    }
}

function chkDuplicateParticulars1(ddlAllowanceDeduction)        //performance action
{
    var dlAllowanceDeductions = document.getElementById(ddlAllowanceDeduction.id.substring(0,ddlAllowanceDeduction.id.lastIndexOf('_ctl')));
    
    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");    
   
    var bDuplicate = false;    
    
    for(var i = 0; i < ddlAllowanceDeduction.length; i++)
    {
        if(ddlAllowanceDeduction[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if((controls[j] != ddlAllowanceDeduction) && (controls[j].id.lastIndexOf('ddlParticulars') != -1))
                {
                    if(ddlAllowanceDeduction[i].value == controls[j].value)
                    {
                        bDuplicate = true; 
                                            
                    }
                }
            }
        }
    }           
    
    if(bDuplicate)
    {
        alert('Duplicate value in Particulars.');
        ddlAllowanceDeduction.selectedIndex = 0;
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + ddlAllowanceDeduction.id + "\',\'\')", 0);
        return true;
    }
}


function chkDuplicatePaymentpolicy(ddlPaymentPolicy)        //salary structure 
{
    var dlAllowanceDeductions = document.getElementById(ddlPaymentPolicy.id.substring(0,ddlPaymentPolicy.id.lastIndexOf('_ctl')));
    
    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");    
   
    var bDuplicate = false;    
    
    for(var i = 0; i < ddlPaymentPolicy.length; i++)
    {
        if(ddlPaymentPolicy[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if((controls[j] != ddlPaymentPolicy) && (controls[j].id.lastIndexOf('ddlPaymentPolicy') != -1))
                {
                    if(controls[j].value !="-1")
                    {
                        bDuplicate = true;                     
                    }
                }
            }
        }
    }           
    
    if(bDuplicate)
    {
        alert('Cannot assign more than one payment policy for a designation.');
        ddlPaymentPolicy.selectedIndex = 0;
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + ddlPaymentPolicy.id + "\',\'\')", 0);
        return true;
    }
}

function ChangeAdditionDeductionIndex(id)         //      Addition / Deduction
{    
    var lnkDescription = document.getElementById(id.substr(0, id.lastIndexOf('_') + 1) + "lnkDescription");
    
    lnkDescription.click();
}

      
// ---------------------------------Sendmail page------------------------------------------
function chkRecepients(source,args)
{    
    var toMailId = document.getElementById(source.controltovalidate);
    
    if(toMailId.value=="")
    {
        if(source.innerText==undefined){source.textContent = "Please enter recipients.";}
        else{source.innerText ="Please enter recipients."; }           
        
        args.IsValid = false;
    }
    else
    {
        var validMailId = toMailId.value.replace(/;/g, ',').split(/,/);
        
        var inValidId = false;
        
        for(var i=0;i<validMailId.length;i++)
        {
            if(!isEmail(validMailId[i]))
            {
                inValidId = true;
            }
        }
        if(inValidId)
        {
            if(source.innerText == undefined) { source.textContent = "Please enter valid recipients"; }
            else { source.innerText = "Please enter valid recipients"; }
            
            args.IsValid = false;
        }
        else
            args.IsValid = true;
    }
}
function chkMails(source, args)
{    
    var txt = document.getElementById(source.controltovalidate);
    
    if(txt.value != "")
    {
        var validMailId = txt.value.replace(/;/g, ',').split(/,/);
        
        var inValidId = false;
        
        for(var i=0;i<validMailId.length;i++)
        {
            if(!isEmail(validMailId[i]))
            {
                inValidId = true;
            }
        }
        if(inValidId)
        {
            if(source.innerText == undefined) { source.textContent = "Please enter valid emails"; }
            else { source.innerText = "Please enter valid emails"; }
            
            args.IsValid = false;
        }
        else
            args.IsValid = true;        
    }
}






function calcDynamicDuration(ctl)
{

    var txtFromTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDynamicFromTime");
    
    var txtToTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDynamicToTime");
    var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDynamicDuration");    
    var txtMinWorkingHrs=document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDynamicMinWorkingHrs");    
    
    var FromHour = parseFloat(txtFromTime.value.substr(0, 2));
    var FromMin = parseFloat(txtFromTime.value.substr(3, 2));
    var isAMFromTime = (txtFromTime.value.lastIndexOf('AM') != -1);
    
    var ToHour = parseFloat(txtToTime.value.substr(0, 2));
    var ToMin = parseFloat(txtToTime.value.substr(3, 2));
    var isAMToTime = (txtToTime.value.lastIndexOf('AM') != -1);
    
    txtDuration.value=TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
    txtMinWorkingHrs.value=TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
    
    //if (isNaN(parseFloat(txtDuration.value)) == true) txtDuration.value=0;
         
}

function calcSplitDuration(ctl)
{
    var txtFromTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtSplitFromTime");
    
    var txtToTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtSplitToTime");
    var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtSplitDuration");    
    
    var FromHour = parseFloat(txtFromTime.value.substr(0, 2));
    var FromMin = parseFloat(txtFromTime.value.substr(3, 2));
    var isAMFromTime = (txtFromTime.value.lastIndexOf('AM') != -1);
    
    var ToHour = parseFloat(txtToTime.value.substr(0, 2));
    var ToMin = parseFloat(txtToTime.value.substr(3, 2));
    var isAMToTime = (txtToTime.value.lastIndexOf('AM') != -1);
    
    txtDuration.value=TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
    
    var dlSplitShiftDetails = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('dlSplitShiftDetails')) + "dlSplitShiftDetails");
    
    
     if(dlSplitShiftDetails!=null)
     {
        var TotalWorkingHoursMins=0;
         for(var i = 0; i < dlSplitShiftDetails.rows.length; i++)
         {
            var txtSplitDuration=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[2];
            
            TotalWorkingHoursMins = parseFloat(parseFloat(TotalWorkingHoursMins) + parseFloat(txtSplitDuration.value.substr(0,2))*60 + parseFloat(txtSplitDuration.value.substr(3,2)));
            
         }
     }
     
      var ResHours=parseInt(TotalWorkingHoursMins/60);
      var ResMin=parseInt(TotalWorkingHoursMins-(ResHours*60));
        
      var MinWorkingHrs=(ResHours <10 ? "0" + ResHours : ResHours).toString() + ":" + (ResMin <10 ? "0" + ResMin:ResMin).toString();  
        
      var txtMinWorkingHrs=document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('dlSplitShiftDetails')) + "txtMinWorkingHrs");
      txtMinWorkingHrs.value=MinWorkingHrs;
    
    //if (isNaN(parseFloat(txtDuration.value)) == true) txtDuration.value=0;
         
}

function ValidateNoOfTimings(source, args)
{
    var txtNoOfTimings = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_') + 1) + "txtNoOfTimings");
    var ddlShiftType = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_') + 1) + "ddlShiftType");
    
    var IsValid=false;
    
    if(ddlShiftType.value==3 || ddlShiftType.value==4)
    {
        if(txtNoOfTimings.value == "")
        {
            IsValid=false;
        }
        else if (isNaN(parseInt(txtNoOfTimings.value)) == false ) 
        {
            if (txtNoOfTimings.value=="0")
            {
               IsValid=false;
            }
            else
            {
                IsValid=true;
            }
        }
        else
        {
            IsValid=true;
        }
     }
     else
     {
        IsValid=true;
     }
     if(IsValid==true)
     {
        args.IsValid=true;
     }
     else
     {
        args.IsValid=false;
     }
}

function ValidateSlotDetails(source,args)  //overtime policy.ascx
{

    var dlSlotDetails = document.getElementById(source.id.replace(/cvSlotDetails/, "dlSlotDetails"));
    var bValid=true;
    
     if(dlSlotDetails != null)
    {
        for(var i = 0; i < dlSlotDetails.rows.length; i++)
        {
            var txtHourSlot=dlSlotDetails.rows[i].getElementsByTagName("INPUT")[1];
            var txtHourSlotValue=dlSlotDetails.rows[i].getElementsByTagName("INPUT")[1];
            
            if(txtHourSlot.value=="" || txtHourSlotValue=="")
            {
                bValid=false;
            }
        }
     }
     
     if(bValid==true)
     {
        args.IsValid=true;
     }
     else
     {
        args.IsValid=false;
     }
}

function ValidatetxtRate(source,args)   //overtime policy.ascx
{

  var chkRateOnly= document.getElementById(source.id.replace(/cvRate/, "chkRateonly"));
  var txtOvertimeRate=document.getElementById(source.id.replace(/cvRate/, "txtOvertimeRate"));
  if(chkRateOnly.checked)
  {
        if(txtOvertimeRate.value=="")
        {
            args.IsValid=false;
        }
        else
        {
            args.IsValid=true;
        }
  }
  else
  {
    args.IsValid=true;
  }
}

function validateHolidayRate(source,args)
{
     var txtHolidayRate=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtHolidayRate");
     var chkRateOnly=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateOnly");
     var ddlcalc=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlcalc");
     var txtcalcPercentage=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtcalcPercentage");
        
     if (chkRateOnly.checked && (txtHolidayRate.value==0 || txtHolidayRate.value==""))
     {
         source.errormessage="Please enter Holiday Rate.";      
         args.IsValid = false;
     }
     else
     {
        if (chkRateOnly.checked  == false && (ddlcalc.value==""))
         {
             source.errormessage="Please enter Calculation Based On";     
             args.IsValid = false;
         }
         else if (chkRateOnly.checked  == false && (txtcalcPercentage.value=="" || txtcalcPercentage.value==0))
         {
             source.errormessage="Please enter Calculation percentage.";      
             args.IsValid = false;
         }
     }
}

function validateAbsentRate(source,args)
{
     var txtAbsentRate=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtAbsentRate");
     var chkRateOnly=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateOnly");
     var ddlcalc=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlCalculationBasedOn");
     
     if (chkRateOnly.checked && (txtAbsentRate.value==0 || txtAbsentRate.value==""))
     {
          source.errormessage="Please enter Absent Rate.";      
         args.IsValid = false;
     }
     else
     {
        if (chkRateOnly.checked  == false && (ddlcalc.value=="")&& (ddlcalc.value == null))
         {
             source.errormessage="Please enter Calculation Based On";     
             args.IsValid = false;
         }
     }
}
function validateEncashRate(source,args)
{
     var txtEncashtRate=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtEncashtRate");
     var txtcalcPercentage=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtcalcPercentage");
     var chkRateOnly=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateOnly");
     var ddlcalc=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlcalc");
     
     if (chkRateOnly.checked && (txtEncashtRate.value==0 || txtEncashtRate.value==""))
     {
         source.errormessage="Please enter Encash Rate.";      
         args.IsValid = false;
     }
     else 
     {
        if (chkRateOnly.checked  == false && (ddlcalc.value==""))
         {
             source.errormessage="Please enter Calculation Based On";     
             args.IsValid = false;
         }
         else if  (chkRateOnly.checked  == false && (txtcalcPercentage.value==0 || txtcalcPercentage.value==""))
         {
             source.errormessage="Please enter Calculation percentage";     
             args.IsValid = false;
         }
     }
   
}


function validateHrsperdayAbsent(source,args)
{
     var txtHoursPerDay=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtHoursPerDay");
     var chkRateBasedOnhr=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRatePerDay");
     if (chkRateBasedOnhr.checked && (txtHoursPerDay.value==0 || txtHoursPerDay.value==""))
     {
        
         args.IsValid = false;
           
     }
}
function validateHrsperdayShortage(source,args)
{
     var MinHrsPerDay=1,MaxHrsPerDay=24;
     var txtHoursPerDayS=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtHoursPerDayS");
     var chkRateOnlyS=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateOnlyS");
     var chkAbsentPolicyOnly=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkAbsentPolicyOnly");
     if(!chkAbsentPolicyOnly.checked)
     {
         if (!(chkRateOnlyS.checked) && (txtHoursPerDayS.value==0 || txtHoursPerDayS.value==""))
         {
             args.IsValid = false;
         }
         else
         {
            if(!(chkRateOnlyS.checked) && (txtHoursPerDayS.value < 1 || txtHoursPerDayS.value>24))
            {
                  args.IsValid = false;
            }
         }
     }
     else
     {
        args.IsValid = true;
     }
}
function validateAbsentRateShortage(source,args)
{
     var txtRatePerHourS=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtRatePerHourS");
     var chkRateOnlyS=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateOnlyS");
     var ddlcalc=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlCalculationBasedOnS");
     
     if (chkRateOnlyS.checked && (txtRatePerHourS.value==0 || txtRatePerHourS.value==""))
     {
          source.errormessage="Please enter valid Rate for shortage policy.";      
         args.IsValid = false;
     }
     else
     {
        if (chkRateOnlyS.checked  == false && (ddlcalc.value==""))
         {
             source.errormessage="Please enter Calculation Based On";     
             args.IsValid = false;
         }
     }
}

function validateHrsperdayHoliday(source,args)
{
     var txtHoursPerDay=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtHoursPerDay");
     var chkRateBasedOnhr=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkRateBasedOnhr");
     if (chkRateBasedOnhr.checked && (txtHoursPerDay.value==0 || txtHoursPerDay.value==""))
     {
         args.IsValid = false;
     }
}


function ValidateCalcPercentage(source,args)  //overtime policy.ascx
{

  var chkCalcPercentage= document.getElementById(source.id.replace(/cvCalcPercentage/, "chkCalcPercentage"));
  var txtCalcPercentage=document.getElementById(source.id.replace(/cvCalcPercentage/, "txtCalcPercentage"));
 // var txtTotalWorkingHours=document.getElementById(source.id.replace(/cvCalcPercentage/, "txtTotalWorkingHours"));
 // var ddlCalculationBasedOn=document.getElementById(source.id.replace(/cvCalcPercentage/, "ddlCalculationBasedOn"));
  if(chkCalcPercentage.checked)
  {
      if(txtCalcPercentage.value=="")
        {
          
            args.IsValid=false;
        }
        
  }
  else
  {
    args.IsValid=true;
  }
  
}
function CheckValidPercent(source,args)
    {
        var chkCalcPercentage=document.getElementById(source.id.replace(/cvValidPercent/, "chkCalcPercentage"));
        var txtCalcPercentage=document.getElementById(source.id.replace(/cvValidPercent/, "txtCalcPercentage"));
         
        if(chkCalcPercentage.checked)
        {
             var fPercent=(isNaN(parseInt(txtCalcPercentage.value))?0 :parseInt(txtCalcPercentage.value)); 
             if(fPercent==0 || fPercent>100)
             {
                args.IsValid=false;
             }
           
        }
        else
        {
                args.IsValid=true;
        }
    }
    
function ValidateddlCalculation(source,args)   //overtime policy.ascx
{

   var chkRateonly= document.getElementById(source.id.replace(/cvCalculationBased/, "chkRateonly"));
   var ddlCalculationBasedOn=document.getElementById(source.id.replace(/cvCalculationBased/, "ddlCalculationBasedOn"));
   
   if(chkRateonly.checked)
   {
        args.IsValid=true;
   }
   else
   {
        if(ddlCalculationBasedOn.value==-1)
        {
            args.IsValid=false;
        }
        else
        {
             args.IsValid=true;
        }
   }
}
function ValidateTotalWorkingHours(source,args)
{
    var chkCalcPercentage= document.getElementById(source.id.replace(/cvWorkingHours/, "chkCalcPercentage"));
    var txtTotalWorkingHours=document.getElementById(source.id.replace(/cvWorkingHours/, "txtTotalWorkingHours"));
    var chkRateonly= document.getElementById(source.id.replace(/cvWorkingHours/, "chkRateonly"));
    
    if(chkRateonly.checked)
    {
        args.IsValid=true;
    }
    else
    {
        if(txtTotalWorkingHours.value=="")
        {
            args.IsValid=false;
        }
    }
    
}

//deduction policy.ascx

function SetEnableAccountable(ctl)
{
    var chkIsAccountable=document.getElementById(ctl);
    var ddlExpenseAc=document.getElementById(chkIsAccountable.id.substr(0, chkIsAccountable.id.lastIndexOf('_') + 1) + "ddlExpenseAc");  
    var ddlPaymentAccount=document.getElementById(chkIsAccountable.id.substr(0, chkIsAccountable.id.lastIndexOf('_') + 1) + "ddlPaymentAccount"); 
    
    if(chkIsAccountable.checked)
    {
        ddlExpenseAc.disabled = false;
        ddlExpenseAc.className = "dropdownlist_mandatory";
        
        ddlPaymentAccount.disabled = false;
        ddlPaymentAccount.className = "dropdownlist_mandatory";
    }
    else
    {
         ddlExpenseAc.disabled=true;
         ddlExpenseAc.className = "dropdownlist_disabled";
        
        ddlPaymentAccount.disabled=true;
        ddlPaymentAccount.className = "dropdownlist_disabled";
    }
}

function ChangelblRate(ctl)
{
     var chkRateOnly=document.getElementById(ctl);
     var lblEmployerContribution=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_') + 1) + "lblEmployerContribution");  
     var lblEmployeeContribution=document.getElementById(chkRateOnly.id.substr(0, chkRateOnly.id.lastIndexOf('_') + 1) + "lblEmployeeContribution"); 
     var ddlParameterName = document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"ddlParameterName"));
     var ddlParameterType = document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"ddlParameterType"));
     var txtAmount = document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"txtAmount"));
     var rfvParametername =document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"rfvParametername"));
     var rfvParameterType =document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"rfvParameterType"));
     var cvAmount =document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"cvAmount"));
     var cvRelatedParticulars = document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"cvRelatedParticulars"));
     var cblParticulars = document.getElementById(chkRateOnly.id.replace(/chkRateOnly/,"cblParticulars"));
     if(chkRateOnly.checked) 
     {
        lblEmployeeContribution.innerText="Employee Contribution";
        lblEmployerContribution.innerText="Employer Contribution";
        ddlParameterName.disabled  = true;
        ddlParameterType.disabled  = true;
        txtAmount.disabled  = true;
        ddlParameterName.value = "-1";
        ddlParameterType.value = "-1";
        txtAmount.value = "";
        rfvParametername.enabled  = false;
        rfvParameterType.enabled  = false;
        cvAmount.enabled  = false;
        cvRelatedParticulars.enabled  = false;
        cblParticulars.disabled  = true;
        
     }
     else
     {
        lblEmployeeContribution.innerText="Employee Contribution(%)";
        lblEmployerContribution.innerText="Employer Contribution(%)";
        ddlParameterName.disabled  = false;
        ddlParameterType.disabled  = false;
        txtAmount.disabled  = false;
        rfvParametername.enabled  = true;
        rfvParameterType.enabled  = true;
        cvAmount.enabled  = true;
        cvRelatedParticulars.enabled  = true;
        cblParticulars.disabled  = false;
     } 
}

function ValidateddlPayAccount(source,args)
{
    var chkIsAccountable=document.getElementById(source.id.replace(/cvPayAccount/, "chkIsAccountable"));
    var ddlPaymentAccount=document.getElementById(source.id.replace(/cvPayAccount/, "ddlPaymentAccount"));
    if(chkIsAccountable.checked)
    {
        if(ddlPaymentAccount.value==-1)
        {
            args.IsValid=false;
        }
    }
    else
    {
        args.IsValid=true;
    }
}

function ValidateddlExpenseAc(source,args)
{
    var chkIsAccountable=document.getElementById(source.id.replace(/cvExpenseAc/, "chkIsAccountable"));
    var ddlExpenseAc=document.getElementById(source.id.replace(/cvExpenseAc/, "ddlExpenseAc"));
    if(chkIsAccountable.checked)
    {
        if(ddlExpenseAc.value==-1)
        {
            args.IsValid=false;
        }
    }
    else
    {
        args.IsValid=true;
    }
}

function ChangeCompanyIndex(id)
{    
    var lnkCompany = document.getElementById(id.substr(0, id.lastIndexOf('_') + 1) + "lnkCompany");
    
    lnkCompany.click();
}


//  Job post.aspx

 function SetClearOption(source,args)
  {
   var btnClear=document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "btnClearAttachment"));
    btnClear.style.display = "block"; 
     
  }
  
  function validateEditDegree(btnId)  
{
    var ddlList = document.getElementById(btnId.replace(/btnEditDegree/, "ddlDegree"));
    
    if(ddlList.value == "-1") { return false; }
    else { return true; }
}


function confirmDeleteDegree(btnId, valueType)
{
    var ddlList = document.getElementById(btnId.replace(/btnDelete/, "ddlDegree"));
    
    if(ddlList.value != "-1")
    {
        return confirm('Are you sure to delete this ' + valueType.toLowerCase() + '?');
    }
    else { return false; }
}


function confirmSaveLeaveEntry(btnApproveId)
{
    var hdConfirm=document.getElementById(btnApproveId.replace(/btnApprove/, "hdConfirm"));
    var ddlEditStatus=document.getElementById(btnApproveId.replace(/btnApprove/, "ddlEditStatus"));
    var txtBalanceLeave=document.getElementById(btnApproveId.replace(/btnApprove/, "txtBalanceLeave"));
    var txtNoOfHolidays = document.getElementById(btnApproveId.replace(/btnApprove/, "txtNoOfHolidays"));
    
    
     var hidControl = document.getElementById("ctl00_hidCulture");

  
    
    
    if(ddlEditStatus.value == 5 && txtBalanceLeave.value > 0 && Number(txtNoOfHolidays.value) > 0)
    {
        //var result=confirm('Holiday(s) exist in between the period.Do you wish to consider those as holiday(s)?');
        
                var result =confirm(hidControl.value == "ar-AE" ?' عطلة (ق) موجودة في بين الفترة. هل ترغب في الاستمرار؟':'Holiday(s) exist in between the period.Do you wish to Continue?');

        
        if(result)
        {
           hdConfirm.value=1;
           return true;
        }
        else
        {
           hdConfirm.value=0;
           return true;  
        }
     }
     else
     {
         hdConfirm.value=0;
           return true;  
     }
}


function ChangeVisibility(ddlEditStatus)
{
    rfvddlDebitHeadID=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "rfvddlDebitHeadID"));
    rfvddlCreditHeadID=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "rfvddlCreditHeadID"));
    
    ddlCreditHeadID=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "ddlCreditHeadID"));
    ddlDebitHeadID=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "ddlDebitHeadID"));
    
    if(ddlEditStatus.value==5)
    {
        rfvddlDebitHeadID.validationGroup="ApproveRequest";
        rfvddlCreditHeadID.validationGroup="ApproveRequest";
        
        ddlCreditHeadID.disabled=false;
        ddlCreditHeadID.className="dropdownlist_mandatory";
        
        ddlDebitHeadID.disabled=false;
        ddlDebitHeadID.className="dropdownlist_mandatory";
        
    }
   
}


function ChangeVisibilityTravelRequest(ddlEditStatus)
{
    rfvddlCreditAcc=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "rfvddlCreditAcc"));
    rfvddlDebitAcc=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "rfvddlDebitAcc"));
    cvAccountDate=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "cvAccountDate"));
    
    ddlCreditAcc=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "ddlCreditAcc"));
    ddlDebitAcc=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "ddlDebitAcc"));
    txtAccountDate=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "txtAccountDate"));
    
    ibtnAccountDate=document.getElementById(ddlEditStatus.id.replace(/ddlEditStatus/, "ibtnAccountDate"));
    
    if(ddlEditStatus.value==5)
    {
        rfvddlCreditAcc.validationGroup="Approve";
        rfvddlDebitAcc.validationGroup="Approve";
        
        cvAccountDate.validationGroup="Approve";
        
        ddlCreditAcc.disabled=false;
        ddlCreditAcc.className="dropdownlist_mandatory";
        
        ddlDebitAcc.disabled=false;
        ddlDebitAcc.className="dropdownlist_mandatory";
        
        txtAccountDate.disabled=false;
        txtAccountDate.className="textbox_mandatory";
        
        ibtnAccountDate.disabled=false;
        
    }
    else
    {
        rfvddlCreditAcc.validationGroup="";
        rfvddlDebitAcc.validationGroup="";
        cvAccountDate.validationGroup="";
        
        rfvddlCreditAcc.innerHTML="";
        rfvddlDebitAcc.innerHTML="";
        cvAccountDate.innerHTML="";
        
        rfvddlCreditAcc.className="";
        rfvddlDebitAcc.className="";
        cvAccountDate.className="";        
        
        ddlCreditAcc.disabled=true;
        ddlCreditAcc.className="dropdownlist_disabled";
        
        ddlDebitAcc.disabled=true;
        ddlDebitAcc.className="dropdownlist_disabled";
        
        txtAccountDate.disabled=true;
        txtAccountDate.className="textbox_disabled";
        
        ibtnAccountDate.disabled=true;
        
    }
   
}


function valAccountDate(source,args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) { source.textContent = "Please enter Account Date." ; }
        else { source.innerText = "Please enter Account date."; }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }   
}


function CheckDefaultValueExtensionRequest(Control)
{
    ddlExtensionType=document.getElementById(Control.id.replace(/txtDuration/, "ddlExtensionType"));
    
    if(ddlExtensionType.value=="1")
    {
        if(Control.value == '')
        {
            Control.value = '0.00';
            Control.focus();
        }
        else if(Control.value == '0.00' || Control.value == '0')
        {
             alert('Please enter a valid number of days');
              Control.focus();
        }
        else
        {
            var value = ConvertToMoney(Control.value);
            if (!isNaN(value)) Control.value = value.toString();
            else
                {
                    alert('Invalid number');
                    Control.focus();
                }
        }
    }
}

function ValidateShiftDuration(source,args)
{
    var ddlShiftType=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "ddlShiftType"));
    var txtDuration=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "txtDuration"));
    var txtMinWorkingHrs=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "txtMinWorkingHrs"));
    var txtAllowedBreakTime=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "txtAllowedBreakTime"));
    var txtLateAfter=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "txtLateAfter"));
    var txtEarlyBefore=document.getElementById(source.id.replace(/cvValidateShiftDuration/, "txtEarlyBefore"));
    
    if(ddlShiftType.value == "1")
    {
        var DurationNMins=parseFloat(parseFloat(txtDuration.value.substr(0,2)) * 60 + parseFloat(txtDuration.value.substr(3,2)));
        if(parseFloat(parseFloat(txtMinWorkingHrs.value.substr(0,2)) * 60 + parseFloat(txtMinWorkingHrs.value.substr(3,2)) + (isNaN(parseFloat(txtAllowedBreakTime.value)) ? 0 : parseFloat(txtAllowedBreakTime.value)) + (isNaN(parseFloat(txtLateAfter.value)) ? 0 : parseFloat(txtLateAfter.value))+ (isNaN(parseFloat(txtEarlyBefore.value)) ? 0 : parseFloat(txtEarlyBefore.value))) == DurationNMins)
        {
            args.IsValid = true;
        }
        else
        {
           // alert('Sum of Min Working Hours,Allowed Break Time,Late After,Early Before should be equal to Duration');
            args.IsValid = false;
        }
    }
    
    
}



function ValidateSplitShiftOverlap(source,args)
{
    var dlSplitShiftDetails=document.getElementById(source.id.replace(/cvValidateSplitShiftOverlap/, "dlSplitShiftDetails"));
    var k;
    
    if(dlSplitShiftDetails!=null)
    {
        for(var i=0;i<dlSplitShiftDetails.rows.length-1;i++)
        {
                var txtSplitFromTime=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[0];
                var txtSplitToTime=dlSplitShiftDetails.rows[i].getElementsByTagName("INPUT")[1];
                
                 var dtDate1 = new Date('January 1, 2010 ' + txtSplitFromTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
                 var dtDate2 = new Date('January 1, 2010 ' + txtSplitToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
                 
                 if(dtDate2<dtDate1)
                 {
                         dtDate2=new Date('January 2, 2010 ' + txtSplitToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
                 }           
                 
                 k=i+1;
                 
                 var txtcmpSplitFromTime=dlSplitShiftDetails.rows[k].getElementsByTagName("INPUT")[0];
                 var dtDate3 = new Date('January 1, 2010 ' + txtcmpSplitFromTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
                 
                 if(dtDate3<dtDate2)
                  {
                        if(source.innerText == undefined) { source.textContent = "shift overlap is not allowed in split shift." ; }
                         else { source.innerText = "shift overlap is not allowed in split shift."; }
                        args.IsValid=false;
                        return;
                        //dtDate3=new Date('January 2, 2010 ' + txtcmpSplitFromTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
                  }            
        }
    }
    args.IsValid=true;
}



function ValidateMinWorkingHrs(source,args)
{
    var txtMinWorkingHrs=document.getElementById(source.id.replace(/cvValidateMinWorkingHrs/, "txtMinWorkingHrs"));
    var txtDuration=document.getElementById(source.id.replace(/cvValidateMinWorkingHrs/, "txtDuration"));
    
    if(parseFloat(txtMinWorkingHrs.value.replace(":", ".")) > parseFloat(txtDuration.value.replace(":", ".")))
    {
        args.IsValid=false;  
    }
    else
    {
        args.IsValid=true;  
    }
}

function ValidateBufferTime(source,args)
{
    var dlDynamicShiftDetails=document.getElementById(source.id.replace(/cvBufferTimeValidation/, "dlDynamicShiftDetails"));
    if(dlDynamicShiftDetails!=null)
    {
          for(var i = 0; i < dlDynamicShiftDetails.rows.length; i++)
           {
                 var txtDynamicMinWorkingHrs=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[4];
                 var txtBufferTime=dlDynamicShiftDetails.rows[i].getElementsByTagName("INPUT")[5];
                 
                if(parseFloat(txtBufferTime.value) > parseFloat(parseFloat(txtDynamicMinWorkingHrs.value.substr(0,2)) * 60 + parseFloat(txtDynamicMinWorkingHrs.value.substr(3,2))))
                {
                     args.IsValid=false;  
                     return;
                } 
                 
                 
           }
            args.IsValid=true;  
     }
   
}

function CalculateNetAmount(ctlId) {

    var ctl=document.getElementById(ctlId);
    
    //var txtBasicPay=document.getElementById(ctl.id.substr(0,ctl.id.lastIndexOf('_')) + '_txtBasicPay');
    var txtNetAmount=document.getElementById(ctl.id.substr(0,ctl.id.lastIndexOf('_')) + '_txtNetAmount');
    var txtAdditionAmt=document.getElementById(ctl.id.substr(0,ctl.id.lastIndexOf('_')) + '_txtAdditionAmt');
    var txtDeductionAmt=document.getElementById(ctl.id.substr(0,ctl.id.lastIndexOf('_')) + '_txtDeductionAmt'); 
    
   // var value = ConvertToMoney(txtBasicPay.value);
     
     var AdditionAmt=0;
     var DeductionAmt=0;
     var NetAmt=0;
     
//     if (!isNaN(value))
//     {
//        AdditionAmt=parseFloat(txtBasicPay.value);
//        txtNetAmount.value=parseFloat(txtBasicPay.value);
//        NetAmt=parseFloat(txtBasicPay.value);
//     }
        
    var dlDesignationAllowances=document.getElementById(ctl.id.substr(0,ctl.id.lastIndexOf('_')) + '_dlDesignationAllowances');
    if(dlDesignationAllowances!=null)
    {
        for(var i=0;i<dlDesignationAllowances.rows.length;i++)
        {
           var txtAmount = dlDesignationAllowances.rows[i].getElementsByTagName("input")[1];
           var ddlParticulars = dlDesignationAllowances.rows[i].getElementsByTagName("select")[0];
           if(ddlParticulars.value != "-1")
           {
               if(ddlParticulars.options[ddlParticulars.selectedIndex].text.indexOf('+') != "-1")
               {
                  if(txtAmount.value !="")
                  {
                      AdditionAmt=parseFloat(AdditionAmt) + parseFloat(txtAmount.value);
                      NetAmt=parseFloat(NetAmt) + parseFloat(txtAmount.value);
                  }        
               }
               else
               {
                    if(txtAmount.value != "")
                    {
                         DeductionAmt=parseFloat(DeductionAmt) + parseFloat(txtAmount.value);
                         NetAmt=parseFloat(NetAmt) - parseFloat(txtAmount.value);
                    }
               }
           }          
        }
    }   
     txtAdditionAmt.value=ConvertToMoney(AdditionAmt);
     txtDeductionAmt.value=ConvertToMoney(DeductionAmt);
     txtNetAmount.value=ConvertToMoney(NetAmt);  
}

function CalculateSalaryStructureForAction(ctl) 
{
    
    var dlDesignationAllowances = document.getElementById(ctl.substr(0, ctl.lastIndexOf('_ctl')));
    var txtNetAmount = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtNetAmount');
    var txtAdditionAmt = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtAdditionAmt');
    var txtDeductionAmt = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtDeductionAmt');

    var AdditionAmt = 0;
    var DeductionAmt = 0;
    var NetAmt = 0;

    if (dlDesignationAllowances != null) {
        for (var i = 0; i < dlDesignationAllowances.rows.length; i++) {
            var txtAmount = dlDesignationAllowances.rows[i].getElementsByTagName("input")[2];
            var ddlParticulars = dlDesignationAllowances.rows[i].getElementsByTagName("select")[0];
            var hdnDeductionAmount = dlDesignationAllowances.rows[i].getElementsByTagName("input")[3];

            if (ddlParticulars.value != "-1") {
                if (ddlParticulars.options[ddlParticulars.selectedIndex].text.indexOf('+') != "-1") {
                    if (txtAmount.value != "") {
                        AdditionAmt = parseFloat(AdditionAmt) + parseFloat(txtAmount.value);
                        NetAmt = parseFloat(NetAmt) + parseFloat(txtAmount.value);
                    }
                }
                else {
                    if (txtAmount.value != "" && parseFloat(txtAmount.value) != 0) {
                        DeductionAmt = parseFloat(DeductionAmt) + parseFloat(txtAmount.value);
                    }
                    else if(hdnDeductionAmount.value != "")
                    {
                         DeductionAmt = parseFloat(DeductionAmt) + parseFloat(hdnDeductionAmount.value);
                    }
                }
            }
        }
    }
    NetAmt = parseFloat(NetAmt) - DeductionAmt;
    txtAdditionAmt.value = ConvertToMoney(AdditionAmt);
    txtDeductionAmt.value = ConvertToMoney(DeductionAmt);
    txtNetAmount.value = ConvertToMoney(NetAmt);
}

function CalculateSalaryStructure(ctl) {


    var dlDesignationAllowances = document.getElementById(ctl.substr(0, ctl.lastIndexOf('_ctl')));
    // var txtBasicPay=document.getElementById(dlDesignationAllowances.id.substr(0,dlDesignationAllowances.id.lastIndexOf('_')) + '_txtBasicPay');
    var txtNetAmount = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtNetAmount');
    var txtAdditionAmt = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtAdditionAmt');
    var txtDeductionAmt = document.getElementById(dlDesignationAllowances.id.substr(0, dlDesignationAllowances.id.lastIndexOf('_')) + '_txtDeductionAmt');

    // var value = ConvertToMoney(txtBasicPay.value);

    var AdditionAmt = 0;
    var DeductionAmt = 0;
    var NetAmt = 0;

    //     if (!isNaN(value))
    //     {
    //        AdditionAmt=parseFloat(txtBasicPay.value);
    //        txtNetAmount.value=parseFloat(txtBasicPay.value);
    //        NetAmt=parseFloat(txtBasicPay.value);
    //     }

    if (dlDesignationAllowances != null) {
        for (var i = 0; i < dlDesignationAllowances.rows.length; i++) {
            var txtAmount = dlDesignationAllowances.rows[i].getElementsByTagName("input")[2];
            var ddlParticulars = dlDesignationAllowances.rows[i].getElementsByTagName("select")[0];

            if (ddlParticulars.value != "-1") {
                if (ddlParticulars.options[ddlParticulars.selectedIndex].text.indexOf('+') != "-1") {
                    if (txtAmount.value != "") {
                        AdditionAmt = parseFloat(AdditionAmt) + parseFloat(txtAmount.value);
                        NetAmt = parseFloat(NetAmt) + parseFloat(txtAmount.value);
                    }
                }
                else {
                    if (txtAmount.value != "") {
                        DeductionAmt = parseFloat(DeductionAmt) + parseFloat(txtAmount.value);
                        NetAmt = parseFloat(NetAmt) - parseFloat(txtAmount.value);
                    }
                }
            }
        }
    }

    txtAdditionAmt.value = ConvertToMoney(AdditionAmt);
    txtDeductionAmt.value = ConvertToMoney(DeductionAmt);
    txtNetAmount.value = ConvertToMoney(NetAmt);
}

function SetAdditionDeductionPolicyTab(id)     // SalaryScale form
{
    var div1,div2,tab1,tab2,div3,tab3;
  
    if(id.lastIndexOf('pnlpTab1')!= -1)
    {
        div1 = document.getElementById(id.replace(/pnlpTab1/,'divpTab1'));
        div2 = document.getElementById(div1.id.replace(/divpTab1/,'divpTab2'));
        tab1 = document.getElementById(id);
        tab2 = document.getElementById(id.replace(/pnlpTab1/,'pnlpTab2'));
        div1.style.display = tab1.style.display = "block";
        tab1.className = "TabbedPanelsTabSelected";
        div2.style.display = "none";
        tab2.className =    "TabbedPanelsTab";
    }
    else
    {
        div2 = document.getElementById(id.replace(/pnlpTab2/,'divpTab2'));
        div1 = document.getElementById(div2.id.replace(/divpTab2/,'divpTab1'));
        tab2 = document.getElementById(id);
        tab1 = document.getElementById(id.replace(/pnlpTab2/,'pnlpTab1'));
        div2.style.display = tab2.style.display = "block";
        tab2.className =  "TabbedPanelsTabSelected";
        div1.style.display = "none";
        tab1.className = "TabbedPanelsTab";
        
    }
}
function valEffectFromDate(source, args)
{
    if(!isValidDate(args.Value))
    {
        args.IsValid = false;
    }
}

function CheckSelectioninRequestedTo(source, args) {

    var checkAssociates = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_chkRequestedTo');
    if (checkAssociates != null) {
        var chkList = checkAssociates.getElementsByTagName("input");
        for (var i = 0; i < chkList.length; i++) {
            if (chkList[i].checked) {
                args.IsValid = true;
                return;
            }
        }
    }
    args.IsValid = false;
}

function validatePettyCashDocumentNameDuplicate(source, args)
{
    var dlPettyCashDocument = document.getElementById(source.id.replace(/cvPettyCashDocumentNameDuplicate/, "dlPettyCashDocument"));
    
    if(dlPettyCashDocument != null)
    {
        var controls = dlPettyCashDocument.getElementsByTagName("SPAN");
        var txtPettyCashDocumentName= document.getElementById(source.id.replace(/cvPettyCashDocumentNameDuplicate/, "txtPettyCashDocumentName"));
        
        for(var i = 0; i < controls.length; i++)
        {           
            if(controls[i].innerText == txtPettyCashDocumentName.value)
            {
                args.IsValid = false;
            }
        }
    }
}
 function ValidateIssueDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
     var hidControl = document.getElementById("ctl00_hidCulture");
    if(ctrl.offsetWidth	> 0 || ctrl.offsetHeight > 0)
    {
        var currentDate=new Date();
        currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        
        if(ctrl.value=='')
        {
            if(source.innerText == undefined) 
            { 
                 source.textContent = hidControl.value == "ar-AE" ? 'الرجاء الإختيار التسجيل.' : 'Please Select Date.'; 
            }
            else 
            { 
                source.innerText =  hidControl.value == "ar-AE" ? 'الرجاء الإختيار التسجيل.' : 'Please Select Date.'; 
            }
             args.IsValid= false;
        }
        else if(!isDate(ctrl.value,source))
        {
            args.IsValid = false;
        }
         else if ((currentDate) < Convert2Date(ctrl.value))
            {
            if(source.innerText == undefined)
             { 
            source.textContent = hidControl.value == "ar-AE" ? 'من تاريخ يجب ألا تكون أكبر من التاريخ الحالي.' : 'From Date Should Not Be Greater than Current Date.'; 
            }
                else 
                { 
                source.innerText = hidControl.value == "ar-AE" ? 'من تاريخ يجب ألا تكون أكبر من التاريخ الحالي.' : 'From Date Should Not Be Greater than Current Date.';  
                }
                
            args.IsValid= false;
            }    
        else
        {
            args.IsValid=true;
        } 
    } 
    else
      args.IsValid=true;
 }
 
 function ValidateDocumentDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    
    var hidControl = document.getElementById("ctl00_hidCulture");
     
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    
    args.IsValid = true;
    
    if(!ctrl.disabled)
    {
        if(ctrl.value=='')
        {
            if(source.innerText == undefined) 
            { 
                 source.textContent = hidControl.value == "ar-AE"  ? 'الرجاء الإختيار التسجيل.': 'Please Select Date.'; 
            }
            else 
            { 
                source.innerText = hidControl.value == "ar-AE"  ? 'الرجاء الإختيار التسجيل.': 'Please Select Date.'; 
            }
             args.IsValid= false;
        }
        else if(!isDate(ctrl.value,source))
        {
            args.IsValid = false;
        }
        else if ((currentDate) > Convert2Date(ctrl.value))
        {
            if(source.innerText == undefined) 
                { 
                    source.textContent = hidControl.value == "ar-AE" ? 'من تاريخ لا ينبغي أن يكون في وقت سابق من التاريخ الحالي.' : 'From date should not be earlier than current date.'; 
                }
                else 
                {
                    source.innerText = hidControl.value == "ar-AE" ? 'من تاريخ لا ينبغي أن يكون في وقت سابق من التاريخ الحالي.' : 'From date should not be earlier than current date.'; 
                 }
                
            args.IsValid= false;
        }    
    }   
 }
 
 function ValidateReturnDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var IssueDate=document.getElementById(ctrl.id.replace(/txtExpectedReturnDate/,"txtIssueDate"));
     var hidControl = document.getElementById("ctl00_hidCulture");
    args.IsValid=true;
    if(ctrl.offsetWidth	> 0 || ctrl.offsetHeight > 0)
    {
        var currentDate=new Date();
        currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        if(ctrl.value=='')
        {
             args.IsValid = true;
        }
        else if(!isDate(ctrl.value,source))
        {
            args.IsValid = false;
        }
        else if (Convert2Date(IssueDate.value) > Convert2Date(ctrl.value))
        {
            if(source.innerText == undefined) 
            { 
            source.textContent = hidControl.value == "ar-AE" ?'المتوقع تاريخ العودة يجب أن تكون أكبر من أو يساوي العدد التسجيل.' : 'Expected Return Date Should be Greater than or Equal to Issue Date.'; 
            }
                else
                 { 
                 source.innerText =  hidControl.value == "ar-AE" ?'المتوقع تاريخ العودة يجب أن تكون أكبر من أو يساوي العدد التسجيل.' : 'Expected Return Date Should be Greater than or Equal to Issue Date.'; 
                 }
                
            args.IsValid= false;
        }    
    } 
 }
 
 function ValidateIssueReason(source,args)
 {
     var ctrl = document.getElementById(source.controltovalidate);
     var hidControl = document.getElementById("ctl00_hidCulture");
     args.IsValid = true;
     if(ctrl.offsetWidth	> 0 || ctrl.offsetHeight > 0)
     {
        if(ctrl.value == "-1")
        {
             if(source.innerText == undefined)
              {
              source.textContent = hidControl.value == "ar-AE" ? 'يرجى تحديد سبب المشكلة' :'Please Select Issue Reason'; 
              }
                        else 
                        { 
                        source.innerText = hidControl.value == "ar-AE" ? 'يرجى تحديد سبب المشكلة' :'Please Select Issue Reason'; 
                        }
             args.IsValid= false;
        }
     }
    
 }
 
 function ValidateIssueReason(source,args)
 {
     var ctrl = document.getElementById(source.controltovalidate);
     var hidControl = document.getElementById("ctl00_hidCulture");
     args.IsValid = true;
     if(ctrl.offsetWidth	> 0 || ctrl.offsetHeight > 0)
     {
        if(ctrl.value == "-1")
        {
             if(source.innerText == undefined) { 
             source.textContent = hidControl.value == "ar-AE" ? 'يرجى تحديد سبب المشكلة' :'Please Select Issue Reason'; 
             }
                        else { 
                        source.innerText = hidControl.value == "ar-AE" ? 'يرجى تحديد سبب المشكلة' :'Please Select Issue Reason'; 
                        }
             args.IsValid= false;
        }
     }
 }
 
function calDuration(ctl)
{
    var pnl;
   
    var txtFromTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtFromTime");
    var txtToTime = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtToTime");
    var txtDuration = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "txtDuration");    
  
    
    var FromHour = parseFloat(txtFromTime.value.substr(0, 2));
    var FromMin = parseFloat(txtFromTime.value.substr(3, 2));
    var isAMFromTime = (txtFromTime.value.lastIndexOf('AM') != -1);
    
    var ToHour = parseFloat(txtToTime.value.substr(0, 2));
    var ToMin = parseFloat(txtToTime.value.substr(3, 2));
    var isAMToTime = (txtToTime.value.lastIndexOf('AM') != -1);
    
    txtDuration.value = TimeDuration(FromHour, FromMin, isAMFromTime, ToHour, ToMin, isAMToTime);
   
    if (isNaN(parseFloat(txtDuration.value)) == true) txtDuration.value=0;
     
}