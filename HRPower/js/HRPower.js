﻿var OverTime = "00:00"; /* For time sheet overtime calculation*/

Date.prototype.getMonthName = function()
{
    return ['January','February','March','April','May','June','July','August','September','October','November','December'][this.getMonth()];
}
Date.prototype.copy = function() {
    return new Date(this.getTime());
}

Date.prototype.addDays = function(days) {
    return this.setTime(this.getTime() + (24 * 60 * 60 * 1000)); 
}

function closeReference(elementID)
{
    var element =  document.getElementById(elementID);
    if(element)
    {
        if(typeof(element.hide) === "function")
        {
            element.hide();
            return false;
        }
    }
}

function GetReferenceControlPrefix(id)
{
    return id.substr(0, id.lastIndexOf('_')).substr(0, id.substr(0, id.lastIndexOf('_')).lastIndexOf('_') + 1);
}
function ValidateReferenceControl()
{
alert("sdsdsd");
}
var TimeSpan = function(smallerTime, biggerTime) {
    var minTimeMeridien = smallerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    var maxTimeMeridien = biggerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    smallerTime = smallerTime.replace(minTimeMeridien, ':00 '+ minTimeMeridien);
    biggerTime = maxTime.replace(maxTimeMeridien, ':00 '+ maxTimeMeridien);
    var tempDate1 = new Date('January 1,2010 '+ smallerTime);
    var tempDate2 = new Date('January 1,2010 '+ biggerTime);
    var day = ( (minTimeMeridien == 'PM' &&  maxTimeMeridien == 'AM') || (minTimeMeridien != maxTimeMeridien && tempDate1 > tempDate2 ) || (                       (minTimeMeridien == maxTimeMeridien) && ( tempDate1 > tempDate2 ) )) ? 2 : 1; 
    var date1 = new Date('January 1,2010 '+ smallerTime);
    var date2 = new Date('January ' + day + ',2010 '+ biggerTime);
    this.Seconds = (date2 - date1)/(1000);
    this.Minutes = (this.Seconds/ 60 ) % 60 ;
    this.Hour = parseInt(this.Seconds / (60 * 60));
    this.Days = parseInt(this.Seconds/(60 * 60 * 24)); 
};

var EmptyTimeSpan = function() {
    this.Seconds = 0;
    this.Minutes = 0;
    this.Hours = 0;
    this.Days = 0;
};

var CustomDate = function(objDate) {

 if(!objDate)
    objDate = new Date();
    
  var date = objDate.copy();
  this.Hours = date.getHours();
  this.Minutes = date.getMinutes();
  this.Date = date;
  this.AddHours = function(hours) {
        date = objDate.setHours(this.Hours + hours);
  };
  this.AddTimeSpan = function(objEmtptyTimeSpan) {
        date.setHours(date.getHours() + objEmtptyTimeSpan.Hours);
        date.setMinutes(date.getMinutes() + objEmtptyTimeSpan.Minutes);
  };
};

function DateDiff(objDate1, objDate2) {
    var objTimespan = new EmptyTimeSpan();
    var tempDate = objDate2.copy();
    if(objDate2 < objDate1) tempDate.setTime( tempDate.getTime() + (24 * 60 * 60 * 1000) );
    objTimespan.Seconds = (tempDate - objDate1)/(1000);
    objTimespan.Minutes = (objTimespan.Seconds/ 60 ) % 60 ;
    objTimespan.Hours = parseInt(objTimespan.Seconds / (60 * 60));
    objTimespan.Days = parseInt(objTimespan.Seconds/(60 * 60 * 24)); 
    return objTimespan;
}

function DateDiff2(objBiggerDate, objSmallerDate2) {
    var objTimespan = new EmptyTimeSpan();
    objTimespan.Seconds = (objBiggerDate - objSmallerDate2)/(1000);
    objTimespan.Minutes = (objTimespan.Seconds/ 60 ) % 60 ;
    objTimespan.Hours = parseInt(objTimespan.Seconds / (60 * 60));
    objTimespan.Days = parseInt(objTimespan.Seconds/(60 * 60 * 24)); 
    return objTimespan;
}

//Function for choosing a company or branch

function toggle(rbtnId)
{
  
    var radioCompany;
    var radioBranch;
    
    radioCompany = document.getElementById(rbtnId.id+'_0');
    radioBranch = document.getElementById(rbtnId.id+'_1');
    
    
//    var fvCompany = document.getElementById(rbtnId.substring(0,rbtnId.lastIndexOf("_") - 17));    
//    
//    var rfvCompany = document.getElementById(fvCompany.id + "_rfvCompany");
//    var rfvBranch = document.getElementById(fvCompany.id + "_rfvBranch");     
//    
//    rfvBranch.style.display = "none";
//    rfvCompany.style.display = "none";
    
//    if(IsCompany == 1)
//    {
//        rfvCompany.validationGroup = "Submit";
//        rfvBranch.validationGroup = "";
//    }
//    else
//    {
//        rfvCompany.validationGroup = "";
//        rfvBranch.validationGroup = "Submit";
//    }
    
    document.getElementById(rbtnId.id.replace(/rbtnCreationMode/,'divCompany')).style.display = (radioCompany.checked == true ? trDisplay() : "none");
    document.getElementById(rbtnId.id.replace(/rbtnCreationMode/,'divBranch')).style.display = (radioBranch.checked == true ? trDisplay() : "none");
    document.getElementById(rbtnId.id.replace(/rbtnCreationMode/,'trAddBranch')).style.display = (radioBranch.checked == true ? trDisplay() : "none");
    
    var txt = document.getElementById(rbtnId.id.replace(/rbtnCreationMode/,'txtBranchName'));
    
    txt.value = "";    
            
    txt.disabled = (radioBranch.checked == true ? false : true);
}

function CheckCreationMode(source,args)
{
    var radioCompany = document.getElementById('ctl00_ctl00_content_public_content_fvCompany_rbtnCreationMode_0');
    var radioBranch = document.getElementById('ctl00_ctl00_content_public_content_fvCompany_rbtnCreationMode_1');     
    //var validator =     
    
    if(radioCompany.checked == true)
    {
        var txtCompany = document.getElementById('ctl00_ctl00_content_public_content_fvCompany_txtCompanyName');
        
        if(txtCompany.value == "")
        {
            if (source.innerText == undefined){source.textContent = "Please enter a company name.";}
            else { source.innerText = "Please enter a company name."; }                
            
            args.IsValid = false;
        }
        else
            args.IsValid = true;
    }
    else if(radioBranch.checked == true)
    {
        var txtBranch = document.getElementById('ctl00_ctl00_content_public_content_fvCompany_txtBranchName');
        
        if(txtBranch.value == "")
        {
            if (source.innerText == undefined){source.textContent = "Please enter a branch name.";}
            else { source.innerText = "Please enter a branch name."; }                
            
            args.IsValid = false;
        }
        else
            args.IsValid = true;
     }    
}

function selectHeaderAll(chkAllId, chkId)
{    
    var datalistId, startIndex;
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
            
    startIndex = parseInt(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 5, chkAllId.lastIndexOf('_ctl') + 6)) + 1;
    
    var chkAllCtl = document.getElementById(chkAllId);
    
    var chk = document.getElementById(datalistId + "_ctl" + "0" + startIndex + "_" + chkId);      
    
    while(chk != null)
    {
        if(chkAllCtl.checked)
            chk.checked = true;
        else
            chk.checked = false;       
            
        startIndex++;
        
        if(startIndex < 10)
            chk = document.getElementById(datalistId + "_ctl" + "0" + startIndex + "_" + chkId);
        else
            chk = document.getElementById(datalistId + "_ctl" + startIndex + "_" + chkId);
    }     
    
    if(startIndex > 9)
        var chkFooter = document.getElementById(chkAllId.replace(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 4),startIndex  + '_chk_FooterAll'));
    else
        var chkFooter = document.getElementById(chkAllId.replace(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 5), startIndex + '_chk_FooterAll'));
    if(chkAllCtl.checked)  
        chkFooter.checked = true;
    else
        chkFooter.checked = false;    
}

function selectFooterAll(chkAllId, chkId)
{   
    var datalistId, startIndex;
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
            
    startIndex = parseInt(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 6, chkAllId.lastIndexOf('_ctl') + 5)) -parseInt(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 6, chkAllId.lastIndexOf('_ctl') + 5)) + 1;
    
    var chkAllCtl = document.getElementById(chkAllId);   
    
    var chkHeader = document.getElementById(chkAllId.replace(chkAllId.substring(chkAllId.lastIndexOf('_ctl') + 4),'00_chk_All'));
    
     var chk = document.getElementById(datalistId + "_ctl" + "0" + startIndex + "_" + chkId);   
    
    while(chk != null)
    {
        if(chkAllCtl.checked)
        {
            chk.checked = true;
            chkHeader.checked = true;
        }
        else
        {
            chk.checked = false; 
            chkHeader.checked = false;
        }
        
        startIndex++;
        
        if(startIndex < 10)
            chk = document.getElementById(datalistId + "_ctl" + "0" + startIndex + "_" + chkId);
        else
            chk = document.getElementById(datalistId + "_ctl" + startIndex + "_" + chkId);
    }   
}

function valAddInterviewer(source,args)
{
   
    var isValid=false;
    var arrCheckBox = document.getElementsByTagName("input");
    
    for(var i = 0; i < arrCheckBox.length; i++)
    {
        if(arrCheckBox[i].type == "checkbox" && arrCheckBox[i].id.lastIndexOf("chlInterviewer") != -1)
        {
            if(arrCheckBox[i].checked)
            {
               isValid = true;
            }
        }
    }
    
     args.IsValid = isValid;
}

function selectAll(chkAllId, chkId)
{
    var datalistId, startIndex;
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
            
    var chks = document.getElementById(datalistId).getElementsByTagName("INPUT");
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
        {
            chks[i].checked = document.getElementById(chkAllId).checked;
        }
    }
}

function valRecipients(source, args)
{
    var txtTo = document.getElementById(source.id.replace(/cvTo/, "txtTo"));
    
    if(isEmpty(txtTo)) 
    { 
        if(source.innerText == undefined) { source.textContent = "Please enter recipients"; }
        else { source.innerText = "Please enter recipients"; }
        
        args.IsValid = false; 
    }
    else
    {
        var addresses = txtTo.value.replace(/;/g, ',').split(/,/);
        var hasError = false;
        
        for(var i = 0; i < addresses.length; i++)
        {
            if(!isEmail(addresses[i]))
            {
                hasError = true;   
            }
        }
        if(hasError)
        {
            if(source.innerText == undefined) { source.textContent = "Please enter valid recipients"; }
            else { source.innerText = "Please enter valid recipients"; }
            
            args.IsValid = false;
        }
    }
}

function addAttachment()
{
    var divFiles = document.getElementById("divFiles");
       
    var filAttachment = document.getElementById("filAttachment");

    var newFilAttachment = filAttachment.cloneNode(true);
    
    newFilAttachment.id = filAttachment.id + (new Date());
    
    if(navigator.appName != 'Microsoft Internet Explorer')
    {
        newFilAttachment.value = ""; 
    }
    
    var delNewFilAttachment = document.createElement("a");
    
    delNewFilAttachment.id = "del" + newFilAttachment.id;
    
    if(delNewFilAttachment.innerText == undefined) { delNewFilAttachment.textContent = "Remove"; }
    else { delNewFilAttachment.innerText = "Remove"; }
    
    delNewFilAttachment.href = "javascript:removeAttachment('" + delNewFilAttachment.id + "')";
    
    divFiles.appendChild(newFilAttachment);
    divFiles.appendChild(delNewFilAttachment);
    
    divFiles.style.maxHeight = "50px";
    divFiles.style.overflow= "auto";
}

function removeAttachment(id)
{
    var delNewFilAttachment = document.getElementById(id);
    var newFilAttachment = document.getElementById(id.replace(/del/, ''));
    
    var divFiles = delNewFilAttachment.parentNode;
    
    divFiles.removeChild(delNewFilAttachment);
    divFiles.removeChild(newFilAttachment);
    
    divFiles.style.maxHeight = "50px";
    divFiles.style.overflow= "auto";
}

function valAttachment(source, args)
{
    var elements = document.getElementsByTagName("INPUT");
    
    var hasFile = false;
    
    for(var i = 0; i < elements.length; i++)
    {
        if(elements[i].type == "file")
        {
            if(elements[i].value != "")
            {
                hasFile = true;
            }
        }
    }
    
    if(!hasFile) { args.IsValid = false; }
}

function validateJobLevel(source,args)
{


 var ctrl = document.getElementById(source.controltovalidate);
 var hfdIsBorad=document.getElementById(ctrl.id.replace(/ddlJobLevel/,"hfdIsBorad"));
  var hidControl = document.getElementById("ctl00_hidCulture");
 if (hfdIsBorad.value =="0" && ctrl.value =="-1")
 {
   if(source.innerText == undefined) { 
   source.textContent = (hidControl.value == "ar-AE" ? 'حدد مستوى الوظيفة':  'Select Joblevel'); 
   }
        else { 
        source.innerText = (hidControl.value == "ar-AE" ? 'حدد مستوى الوظيفة':  'Select Joblevel'); 
        }
         args.IsValid= false;
 }
 else
 {
  args.IsValid=true;
 }
 
}

function validateInterviewDate(source,args)
{

    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
     var hidControl = document.getElementById("ctl00_hidCulture");
     
    if(ctrl.value=='')
    {
     if(source.innerText == undefined) { 
     source.textContent = (hidControl.value == "ar-AE" ? 'من فضلك ادخل تاريخ مقابلة':  'Please enter Interview date.'); 
     }
        else { 
        source.innerText = (hidControl.value == "ar-AE" ? 'من فضلك ادخل تاريخ مقابلة':  'Please enter Interview date.'); 
        }
         args.IsValid= false;
         
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }
    else if(Convert2Date(ctrl.value)<currentDate)
    {
        if(source.innerText == undefined) { source.textContent =(source.controltovalidate.lastIndexOf('Interview')!=-1 ? "Interview" :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "From" : "To" )) + " date should not earlier than today."; }
        else { source.innerText = (source.controltovalidate.lastIndexOf('Interview')!=-1 ? "Interview" :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "From" : "To" )) + " date should not earlier than today."; }
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }
}

function valInterviewStartDateEdit(source,args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    hfInterviewDate = document.getElementById(ctrl.id.replace(/txtInterviewDate/,'hfInterviewDate'));
    if(ctrl.value=='')
    {
     if(source.innerText == undefined) { source.textContent = "Please enter " +(source.controltovalidate.lastIndexOf('Interview')!=-1 ? "interview" :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "from" : "to" ))+  "date."; }
        else { source.innerText = "Please enter " +(source.controltovalidate.lastIndexOf('Interview')!=-1 ? "interview " :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "from " : "to " ))+  "date."; }
         args.IsValid= false;
         
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }    
     else if(Convert2Date(ctrl.value)<currentDate && hfInterviewDate.value!=ctrl.value)
    {
        if(source.innerText == undefined) { source.textContent =(source.controltovalidate.lastIndexOf('Interview')!=-1 ? "Interview" :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "From" : "To" )) + " date should not earlier than today."; }
        else { source.innerText = (source.controltovalidate.lastIndexOf('Interview')!=-1 ? "Interview" :(source.controltovalidate.lastIndexOf('Start')!=-1 ? "From" : "To" )) + " date should not earlier than today."; }
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }    
}

function ValidateDate(source, args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(!isValidDate(ctrl.value))
    {
        source.innerText = (hidControl.value == "ar-AE" ? 'الرجاء إدخال تاريخ صالح' : 'Invalid Date.'); 
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }   
}
function valRequestFromToDate(source,args)
{
   
    var ctrl = document.getElementById(source.controltovalidate);    
    var ctr2=document.getElementById(ctrl.id.replace(/txtToDate/,"txtFromDate"));
    var hidControl = document.getElementById("ctl00_hidCulture");
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
            if (source.controltovalidate.lastIndexOf('From')!=-1)
            {
               source.innerText = (hidControl.value == "ar-AE" ? ' الرجاء من تاريخ دخول' : 'Please enter from date.'); 
            }
            else
            {
                source.innerText = (hidControl.value == "ar-AE" ? 'الرجاء إدخال حتى الآن' :'Please enter to date.'); 
            }
       
        }
        else 
        { 
            if (source.controltovalidate.lastIndexOf('From')!=-1)
            {
                source.innerText = (hidControl.value == "ar-AE" ? ' الرجاء من تاريخ دخول' : 'Please enter from date.'); 
            }
            else
            {
                source.innerText = (hidControl.value == "ar-AE" ? 'الرجاء إدخال حتى الآن' : 'Please enter to date.'); 
            }
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }    
    else
    {
        args.IsValid=true;
    }  
   
    if((ctrl.value!="") && (ctr2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctr2 .value));
        var edate=new Date(Convert2Date(ctrl.value));
       
        if(edate.getFullYear()< sdate.getFullYear())
        {
            args.IsValid = false;
            source.innerText = hidControl.value == "ar-AE" ?"من تاريخ أكبر من تاريخ ل":"From date is greater than to date.";// "From date is greater than to date."; 
        }
        else if(sdate > edate)
        {
            args.IsValid = false;
            source.innerText = hidControl.value == "ar-AE" ?"من تاريخ أكبر من تاريخ ل":"From date is greater than to date.";// "From date is greater than to date."; 
        }
    }   
}

function valRequestLeaveFromToDate(source,args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) { source.textContent = "Please enter leave " +((source.controltovalidate.lastIndexOf('From')!=-1 ? "from " : "to " ))+  "date."; }
        else { source.innerText = "Please enter leave " +((source.controltovalidate.lastIndexOf('From')!=-1 ? "from " : "to " ))+  "date."; }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }   
}
function valRequestTravelFromToDate(source,args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) { source.textContent = "Please enter travel " +((source.controltovalidate.lastIndexOf('From')!=-1 ? "from " : "to " ))+  "date."; }
        else { source.innerText = "Please enter travel " +((source.controltovalidate.lastIndexOf('From')!=-1 ? "from " : "to " ))+  "date."; }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }   
}

/*For Loan request*/
function SetDivVisibility(source)
{
    var ddlStatus=document.getElementById(source.id);
    var divHigher=document.getElementById(ddlStatus.id.replace(/ddlEditStatus/,"divHigher"));
    var divReportingTo=document.getElementById(ddlStatus.id.replace(/ddlEditStatus/,"divReportingTo"));
    var hdIsHigher=document.getElementById(ddlStatus.id.replace(/ddlEditStatus/,"hdIsHigher"));
    var btnApprove=document.getElementById(ddlStatus.id.replace(/ddlEditStatus/,"btnApprove"));
  //  var RequiredFieldValidator2=document.getElementById(ddlStatus.id.replace(/ddlEditStatus/,"RequiredFieldValidator2"));

    if(hdIsHigher.value=="1")//ddlStatus[ddlStatus.selectedIndex].value== "5" && 
    {
    divHigher.style.display="table-row";
    btnApprove.validationGroup ="FinalApproval";
    //RequiredFieldValidator2.disabled=false;
    }
    else
    {
    divHigher.style.display="none";
    btnApprove.validationGroup ="";
   // RequiredFieldValidator2.disabled="disabled";
    }
}

function SetInterestRate(source)
{
    var ddlInterestType=document.getElementById(source.id);
    var txtInterestRate=document.getElementById(ddlInterestType.id.replace(/ddlInterestType/,"txtInterestRate"));
    if(ddlInterestType[ddlInterestType.selectedIndex].value== "1")//none
    {
    txtInterestRate.value="0";
    txtInterestRate.disabled="disabled";
    }
    else
    {
    txtInterestRate.disabled=false;
    }
}

function valLoanDate(source,args)
{
    var txtLoanDate=document.getElementById(source.controltovalidate);
    var hdStartDate=document.getElementById(txtLoanDate.id.replace(/txtLoanDate/,"hdStartDate"));
     if(txtLoanDate.offsetWidth	> 0 || txtLoanDate.offsetHeight > 0)
     {
    if(txtLoanDate.value!="" && hdStartDate.value!="")
    {
    var lDate=new Date(Convert2Date(txtLoanDate.value));
    var sDate=new Date(Convert2Date(hdStartDate.value));
    if(lDate >sDate)
        {
            args.IsValid = false;
        }
   
    }
    }
    else
    {
    args.IsValid = true;
    }
}

function valInstallmentDate(source,args)
{
    var txtPaymentAmount=document.getElementById(source.controltovalidate);
    var dlInstallments=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"dlInstallments"));
    var txtLoanAmount=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"txtLoanAmount"));
    var hdLoanAmount=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"hdLoanAmount"));
    var txtLoanNumber=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"txtLoanNumber"));
    var txtInterestRate=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"txtInterestRate"));
    var txtNoOfInstallments=document.getElementById(txtPaymentAmount.id.replace(/txtPaymentAmount/,"txtNoOfInstallments"));
    var hidControl = document.getElementById("ctl00_hidCulture");
    
     if(txtLoanNumber.offsetWidth	> 0 || txtLoanNumber.offsetHeight > 0)
     {
    
    var hasFile = false;
    if(txtLoanNumber==null || txtLoanNumber.value=="")
    {
         source.errormessage = hidControl.value == "ar-AE" ?"يرجى إدخال رقم القرض":"Please enter loan number";//"Please enter loan number";
         hasFile=true;
    }
     if(txtInterestRate==null || txtInterestRate.value=="")
    {
         source.errormessage= hidControl.value == "ar-AE" ?"الرجاء إدخال سعر الفائدة":"Please enter interest rate";//"Please enter interest rate";
         hasFile=true;
    }
      if(txtNoOfInstallments==null || txtNoOfInstallments.value=="")
    {
         source.errormessage= hidControl.value == "ar-AE" ?"يرجى إدخال رقم الأقساط":"Please enter number of installments";//"Please enter number of installments";
         hasFile=true;
    }
     if(txtLoanAmount==null || txtLoanAmount.value=="")
    {
         source.errormessage= hidControl.value == "ar-AE" ?"الرجاء إدخال مبلغ القرض":"Please enter loan amount";//"Please enter loan amount";
         hasFile=true;
    }
    //Checking for loan amount and payment amount
    if(txtLoanAmount.value!="" && txtPaymentAmount.value!="" && hdLoanAmount.value!=null)
    {
    if(new Number(txtPaymentAmount.value)<new Number(txtLoanAmount.value))
    {
         source.errormessage= hidControl.value == "ar-AE" ?"دفع المبلغ لا يمكن أن يكون أقل من مبلغ القرض":"Payment amount cannot be less than loan amount";//"Payment amount cannot be less than loan amount ";
         hasFile=true;
    }
    if(new Number(hdLoanAmount.value)<new Number(txtLoanAmount.value))
    {
         source.errormessage= hidControl.value == "ar-AE" ?"مبلغ القرض لا يمكن أن يكون أكبر من المبلغ المطلوب":"Loan amount cannot be greater than requested amount";//"Loan amount cannot be greater than requested amount";
         hasFile=true;
    }
    }
    //Checking for installment date,installment amount
    if(dlInstallments!=null && !hasFile)
        {
            var controls = dlInstallments.getElementsByTagName("INPUT");
            var txtDate1;
            var txtDate2;
            var txtAmount;
            for(var i = 1; i < controls.length; i++)
                         {
                             if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtDate') != -1 && !hasFile)
                              {
                                txtDate1 = controls[i];
                                if(txtDate1.value=="" && !hasFile)
                                {
                                source.errormessage= hidControl.value == "ar-AE" ?"من فضلك ادخل تاريخ القسط":"Please enter Installment date";//"Please enter Installment date";
                                hasFile=true;
                                }
                                if(isNaN(new Date(Convert2Date(txtDate1.value))))
                                {
                                 source.errormessage= hidControl.value == "ar-AE" ?"يرجى إدخال تاريخ القسط صالحة":"Please enter a valid Installment date";//"Please enter a valid Installment date";
                                hasFile=true;
                                }
                                if(txtDate2!=null && txtDate1.value!="" && txtDate2.value!="")
                                {
                                    var Date1=new Date(Convert2Date(txtDate1.value));
                                    var Date2=new Date(Convert2Date(txtDate2.value));
                                    if( Date1< Date2)
                                    {
                                        hasFile=true;
                                    }
                                    
                                }
                                txtDate2 = controls[i];
                              }
                              if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtAmount') != -1  && !hasFile)
                              {
                              txtAmount=controls[i];
                              if(txtAmount.value== null || isNaN(new Number(txtAmount.value)) ||new Number(txtAmount.value)==0 )
                              {
                              source.errormessage= hidControl.value == "ar-AE" ?"الرجاء إدخال مبلغ الدفعة":"Please enter installment amount";//"Please enter installment amount";
                              hasFile=true;
                              }
                              }
                         }
            if(!hasFile){
            if(dlInstallments.rows.length==1)
            {source.errormessage= hidControl.value == "ar-AE" ?"الرجاء إدخال الاقل دفعة واحدة":"Please enter atleast one installment";//"Please enter atleast one installment";
            hasFile=true;}
            
            }
        }
        if(hasFile) { args.IsValid = false; }
        }
        else
        args.IsValid=true;
}

function checkFromToDate(source,args)
{
    var ctl2=document.getElementById(source.controltovalidate);

    var ctl1=document.getElementById(ctl2.id.replace(/txtToDate/,"txtFromDate"));
    if((ctl1.value!="") && (ctl2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctl1 .value));
        var edate=new Date(Convert2Date(ctl2.value));
       
        if(edate.getFullYear()>sdate.getFullYear())
        {
            args.IsValid = true;
        }
        else if(sdate.format("MM/dd/yy")<=edate.format("MM/dd/yy"))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
}
 function CheckFutureAndFromToDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var ctr2=document.getElementById(ctrl.id.replace(/txtToDate/,"txtFromDate"));
    var hidControl = document.getElementById("ctl00_hidCulture");
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
  
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";// "Please enter  date."; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
//    else if ((currentDate) < Convert2Date(ctrl.value))
//    {
//        if(source.innerText == undefined) 
//        { 
//        source.textContent = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحالي":"Requested date should not be greater than the current date.";// "Requested date should not be greater than the current date."; 
//        }
//         else 
//         { 
//         source.innerText = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحالي":"Requested date should not be greater than the current date.";// "Requested date should not be greater than the current date."; 
//         }
//            
//        args.IsValid= false;
//     }
    else
    {
        args.IsValid=true;
    }  
   
    if((ctrl.value!="") && (ctr2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctr2 .value));
        var edate=new Date(Convert2Date(ctrl.value));
       
        if(edate.getFullYear()< sdate.getFullYear())
        {
            args.IsValid = false;
            source.innerText = hidControl.value == "ar-AE" ?"من تاريخ أكبر من تاريخ ل":"From date is greater than to date.";// "From date is greater than to date."; 
        }
        else if(sdate >edate)
        {
            args.IsValid = false;
            source.innerText = hidControl.value == "ar-AE" ?"من تاريخ أكبر من تاريخ ل":"From date is greater than to date.";// "From date is greater than to date."; 
        }
    }  
}
 function CheckValidDateAndFromToDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var ctr2=document.getElementById(ctrl.id.replace(/txtToDate/,"txtFromDate"));
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";// "Please enter  date."; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    else
    {
        args.IsValid=true;
    }  
   
    if((ctrl.value!="") && (ctr2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctr2 .value));
        var edate=new Date(Convert2Date(ctrl.value));
       
        if(edate.getFullYear()< sdate.getFullYear())
        {
             args.IsValid = false;
             source.innerText = hidControl.value == "ar-AE" ?"يجب أن يكون تاريخ نهاية أكبر من تاريخ البدء.":"End date must be greater than  start date."  ; 
        }
        else if(sdate > edate)
        {
            args.IsValid = false;
             source.innerText = hidControl.value == "ar-AE" ?"يجب أن يكون تاريخ نهاية أكبر من تاريخ البدء.":"End date must be greater than  start date."  ; 
        }
    }  
}
function CheckValidDateAndExpiry(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var ctr2=document.getElementById(ctrl.id.replace(/txtExpirydate/,"txtIssuedate"));
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(ctrl.value=='')
    {
       if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";// "Please enter  date."; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    else
    {
        args.IsValid=true;
    }  
   
    if((ctrl.value!="") && (ctr2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctr2 .value));
        var edate=new Date(Convert2Date(ctrl.value));
       
        if(edate.getFullYear()< sdate.getFullYear())
        {
            args.IsValid = false;
            source.innerText = hidControl.value == "ar-AE" ?"تاريخ انتهاء الصلاحية هو أقل من تاريخ الإصدار.":"Expiry date is less than Issue date";//"Expiry date is less than Issue date."; 
        }
        else if(sdate.format("MM/dd/yy")>edate.format("MM/dd/yy"))
        {
            args.IsValid = false;
            source.innerText =hidControl.value == "ar-AE" ?"تاريخ انتهاء الصلاحية هو أقل من تاريخ الإصدار.":"Expiry date is less than Issue date";// "Expiry date is less than Issue date."; 
        }
    }  
}
function checkFromToDateApproval(source,args)
{
    var ctl2=document.getElementById(source.controltovalidate.replace(/txtApprovalToDate/,"txtApprovalFromDate"));//FromDate

    var ctl1=document.getElementById(ctl2.id.replace(/txtApprovalFromDate/,"txtApprovalToDate"));//ToDate
    var ctlFromLabel=document.getElementById(ctl2.id.replace(/txtApprovalFromDate/,"lblFromPeriod"));
    var ctlToLabel=document.getElementById(ctl2.id.replace(/txtApprovalFromDate/,"lblToPeriod"));
    var hidControl = document.getElementById("ctl00_hidCulture");

 
    
    
    
    
    if((ctl1.value!="") && (ctl2.value!="")&& (ctlFromLabel.value!="") && (ctlToLabel.value!=""))
    {
        var sdate = new Date(Convert2Date(ctl2 .value));
        var edate=new Date(Convert2Date(ctl1.value));
        var sldate=new Date(Convert2Date(ctlFromLabel.innerText)); 
        var eldate=new Date(Convert2Date(ctlToLabel.innerText));
       
        if(edate.getFullYear()>sdate.getFullYear())
        {
            args.IsValid = true;
        }
//         else if(edate.format("MM/dd/yy")>eldate.format("MM/dd/yy") || sdate.format("MM/dd/yy")<sldate.format("MM/dd/yy"))
//        {
//           source.innerText= hidControl.value == "ar-AE" ?"من التاريخ وحتى الآن وينبغي أن تكون بين تواريخ المطلوبة":"From date and To date should be between requested dates";
//           args.IsValid = false;
//        }
        else if(sdate.format("MM/dd/yy")<=edate.format("MM/dd/yy"))
        {
            args.IsValid = true;
        }
       
        else
        {
            source.innerText=hidControl.value == "ar-AE" ?"ترك حتى الآن ينبغي أن يكون في وقت لاحق من ترك من تاريخ":"Leave to date should be later than leave from date";
            args.IsValid = false;
        }
    }
}

function checkFromToDateValidation(source, args) {
    var ctl2 = document.getElementById(source.controltovalidate);

    var rd = document.getElementById('ctl00_ctl00_content_public_content_fvLeaveRequest_rblhalfDay_0');
    if (rd.checked) {
        args.IsValid = false;
    }
    else {

        var ctl1 = document.getElementById(ctl2.id.replace(/txtToDate/, "txtFromDate"));
        if ((ctl1.value != "") && (ctl2.value != "")) {
            var sdate = new Date(Convert2Date(ctl1.value));
            var edate = new Date(Convert2Date(ctl2.value));

            if (edate.getFullYear() > sdate.getFullYear()) {
                args.IsValid = true;
            }
            else if (sdate.format("MM/dd/yy") <= edate.format("MM/dd/yy")) {
                args.IsValid = true;
            }
            else {
                args.IsValid = false;
            }
    }


    }
}

function selectCurrency(source,args)
{  
    var ddlCurrency = document.getElementById(source.controltovalidate);    
   
    if(ddlCurrency[ddlCurrency.selectedIndex].value== "-1")
    {
        args.IsValid = false;
    }
    else
    {
        args.IsValid = true;
    } 
}

// Function for displaying the default working days according to year and month selection
function DisplayWorkingDays(btn)
{
     var rbtnDailyPayMode = document.getElementById(btn); 
     var txtWorkingDays = document.getElementById(btn.replace(/rbtnDailyPayMode/,"txtWorkingDays")); 
        if(rbtnDailyPayMode.getElementsByTagName("input")[0].checked)
         {
            txtWorkingDays.value = 365;
         }        
         else
         {
            txtWorkingDays.value = 30;
         }
     return txtWorkingDays.value;
}

function DailyPayMode(source,args)
{
    var rbtnDailyPayMode = document.getElementById(source.id.replace(/cvDailyPayMode/,"rbtnDailyPayMode"));
    var txtWorkingDays = document.getElementById(source.id.replace(/cvDailyPayMode/,"txtWorkingDays"));    
    if(txtWorkingDays.value == "")
    {
       if (source.innerText == undefined){source.textContent = "Please enter number of working days.";}
        else { source.innerText = "Please enter number of working days."; }    
       args.IsValid = false;
    }
    else
    {
      if(rbtnDailyPayMode.getElementsByTagName("input")[0].checked)
        {
            if(txtWorkingDays.value > 365 || txtWorkingDays.value < 100 )
            {     
                if (source.innerText == undefined){source.textContent = "The number of working days must be within the range (100-365).";}
                else { source.innerText = "The number of working days must be within the range (100-365)."; }       
                
                args.IsValid = false;           
            }        
            else
                args.IsValid = true;           
        }
        else
        {
            if(txtWorkingDays.value > 31)
            {
                if (source.innerText == undefined){source.textContent = "The number of working days must be less than or equal to 31 when selecting month." ;}
                else { source.innerText = "The number of working days must be less than or equal to 31 when selecting month." ; }     
                   
                args.IsValid = false; 
            }
            else if(txtWorkingDays.value == 0)
            {
                 if (source.innerText == undefined){source.textContent = "Working days cannot be zero."  ;}
                else { source.innerText = "Working days cannot be zero."  ; }    
                  
                args.IsValid = false; 
            }
            else
                args.IsValid = true;
        }
    }
}
function validatePaypolicy(source, args)
{
    var dlPayPolicyInformation = document.getElementById(source.id.replace(/cvLimit/, "dlPayPolicyInformation"));
    if(dlPayPolicyInformation != null)
    {
        var controls = dlPayPolicyInformation.getElementsByTagName("INPUT");
         var txtDays;
         var ddlPayClassification = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlPayClassification");
         var ddlPayCalculation = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlPayCalculation");
           for(var i = 0; i < controls.length; i++)
             {
              if(controls[i].type == "text" && controls[i].id.lastIndexOf('txtLimit') != -1)
              {
                txtDays = controls[i];
                var Days;
                Days = (isNaN(parseInt(txtDays.value)) ? 0 : parseInt(txtDays.value));
                switch(ddlPayClassification.value)
                {
                    case "1": // Daily 
                          switch(ddlPayCalculation .value)
                          {
                           case "2": // day  
                           if(Days > 1)
                            {
                              source.errormessage = "Days must be less than or equal to 1.";                  
                              args.IsValid = false;
                            }
                            break;
                           case "1": // Hour 
                            if(Days > 24)
                            {
                              source.errormessage = "Working hours must be less than or equal to 24.";                  
                              args.IsValid = false;
                            } 
                             break;
                           }                         
                    break;
                    case "2":// Weekly
                          switch(ddlPayCalculation .value)
                           {
                              case "2": // day  
                               if(Days > 7)
                                {
                                  source.errormessage = "Days must be less than or equal to 7.";                  
                                  args.IsValid = false;
                                }
                              break;
                              case "1": // Hour  
                               if(Days > 168)
                                {
                                  source.errormessage = "Working hours must be less than or equal to 168.";                  
                                  args.IsValid = false;
                                } 
                              break;
                     
                          }
                    break;
                    case "3"://Fortnight
                       switch(ddlPayCalculation .value)
                       {
                           case "2": // day  
                           if(Days > 14)
                            {
                              source.errormessage = "Days must be less than or equal to 14.";                  
                              args.IsValid = false;
                            }
                          break;
                          case "1": // Hour  
                             if(Days > 336)
                            {
                              source.errormessage = "Working hours must be less than or equal to 336.";                  
                              args.IsValid = false;
                            } 
                          break;                  
                      }
                   break;
                   case "4"://Monthly
                   switch(ddlPayCalculation .value)
                       {
                           case "2": // day  
                           if(Days > 30)
                            {
                              source.errormessage = "Days must be less than or equal to 30.";                  
                              args.IsValid = false;
                            }
                           break;
                           case "1": // Hour  
                           if(Days > 720)
                            {
                              source.errormessage = "Working hours must be less than or equal to 720.";                  
                              args.IsValid = false;
                            } 
                           break;
                          case "4": // Month  
                           if(Days > 30)
                            {
                              source.errormessage = "Days must be less than or equal to 30.";                  
                              args.IsValid = false;
                            }
                           break;
                        }
                      break;  
                  }
             }
    }
}
}

function BindTimeEntry(ancId)
{
    var grid = document.getElementById(ancId.replace(/anc/,"gv"));
    if(grid == null)
    {
        return true;
    }
    else
    {
        toggleAnimate(ancId);
        return false;
    }
}

function selectAllSettings(chkAllId, chkId,datalistId)
{
    var  startIndex;
    var datalist = document.getElementById(chkAllId.replace(/chkAllEnabled/,datalistId));;
    if(datalist != null)
    {     
        startIndex = 0;
    
        var chkAllCtl = document.getElementById(chkAllId);
    
        var chk = document.getElementById(datalist.id + "_ctl" + "0" + startIndex + "_" + chkId);   
    
        while(chk != null)
        {
            if(chkAllCtl.checked)
                chk.checked = true;
            else
                chk.checked = false;       
            
            startIndex++;
        
            if(startIndex < 10)
                chk = document.getElementById(datalist.id + "_ctl" + "0" + startIndex + "_" + chkId);
            else
                chk = document.getElementById(datalist.id + "_ctl" + startIndex + "_" + chkId);
        }   
    }
}

function valInTextBox(btnId)
{
    var btn = document.getElementById(btnId);
    var grid = document.getElementById(btnId.substring(0,btnId.lastIndexOf('_ctl02')));
    
    var flag = false;
    
    for(var i = 0; i < grid.rows[1].cells.length; i++)
    {
        if(grid.getElementsByTagName("input")[i].type == "text")
        {
            var txtBox = grid.getElementsByTagName("input")[i];
            
            if(grid.getElementsByTagName("input")[i].value == "")
                flag = true;
        }        
    }
    
    if(!flag)
        return true;
    else
    {
        alert("Please enter time");        
        return false;
    }
}

function getHour(Time) {
    return new Date('January 1,2010 '+ Time).getHours();
}

function getMinute(Time) {
    return new Date('January 1,2010 '+ Time).getMinutes();
}

function valCandidateDatalist(datalistId,isEdit,btnSubmitId)
{
    var datalist = document.getElementById(datalistId);

    var count = 0;
    var checkedcount=0;  
    var chk;
    var txt;  
    var checked = false;
    if(datalist!=null)
    {
       
       for(var i =0 ; i < datalist.rows.length-1; i++)
       {
            chk = datalist.rows[i].getElementsByTagName("INPUT")[0];       
            txt = datalist.rows[i].getElementsByTagName("INPUT")[1];
            
                if(chk.checked) 
                { 
                    checked = true;
                    checkedcount++;
                     if(txt.value==''){count++;}
                }
        }
     }    
    if(checked)
    { 
       if(isEdit)
       {
            if((count!=0) && (count!=checkedcount))
            {
                alert('Please enter the schedule time for the selected candidates');
        
                return false;
            }
            else if(count==checkedcount)
            {
               var txtTime=btnSubmitId.replace(/btnSubmit/,'txtInterviewTime');
               var txtDuration=btnSubmitId.replace(/btnSubmit/,'txtDuration');
               
               setTime(datalistId,txtTime,txtDuration); 
               return true;
            }
           
            var result=confirm('Do you want to maintain history for the schedule?');
            var hfconfirm=document.getElementById("ctl00_ctl00_content_public_content_hfConfirm");
            if(result)
            {
               hfconfirm.value=1;
               return true;
            }
            else
            {
               hfconfirm.value=0;
               return true;  
            }
       }
       else
       {
            return true;
        }
    }
    else
    {
        alert('Please select at least a candidate for scheduling the interview');
        
        return false;
    }
}

function setupEmployeeTransaction(ddlTransactionType)
{
    var ddlCompanyAccount, ddlEmployeeBankName;
  
    ddlCompanyAccount = document.getElementById(ddlTransactionType.id.substring(0, ddlTransactionType.id.lastIndexOf('_') + 1) + "ddlCompanyAccount");
    ddlEmployeeBankName = document.getElementById(ddlTransactionType.id.substring(0, ddlTransactionType.id.lastIndexOf('_') + 1) + "ddlEmployeeBankName");
    
    switch(ddlTransactionType.value)
    {
        case "1"://Bank
            ddlEmployeeBankName.disabled = "disabled";            
            ddlCompanyAccount.disabled = false;
            break;
            
        case "5"://WPS
            ddlCompanyAccount.disabled = ddlEmployeeBankName.disabled = false;
            
            break;
            
        default://Cash, DD and Check
            ddlCompanyAccount.disabled = ddlEmployeeBankName.disabled = "disabled";
//            cvEmployerBankNamepnlTab4.style.display = "none";
//            cvCompanyAccountpnlTab4.style.display = "none";
            break;
    }
    
    if(ddlCompanyAccount.disabled) { ddlCompanyAccount.className = "dropdownlist_disabled"; } else { ddlCompanyAccount.className = "dropdownlist" }
    if(ddlEmployeeBankName.disabled) { ddlEmployeeBankName.className = "dropdownlist_disabled"; } else { ddlEmployeeBankName.className = "dropdownlist" }
}

function setupEmploymentType(ddlEmploymentType)
{
    var ddlVendor = document.getElementById(GetReferenceControlPrefix(ddlEmploymentType.id) + "ddlVendor");
    
    if(ddlEmploymentType.value == "2")
    {
        ddlVendor.disabled = false;
        ddlVendor.className = "dropdownlist_mandatory";
    }
    else
    {
        ddlVendor.SelectedIndex = -1;
        ddlVendor.disabled = "disabled";
        ddlVendor.className = "dropdownlist_disabled";      
    }
}

function setupProbationDates(txtDateofJoining)
{
    if(isValidDate(txtDateofJoining.value))
    {
    
        var yearOfJoining, monthOfJoining, dateOfJoining;
        
        yearOfJoining = parseFloat(txtDateofJoining.value.substring(txtDateofJoining.value.lastIndexOf('/') + 1, txtDateofJoining.value.length));
        monthOfJoining = parseFloat(txtDateofJoining.value.substr(txtDateofJoining.value.indexOf('/') + 1, 2));
        dateOfJoining = parseFloat(txtDateofJoining.value.substr(0, 2));
        
        var txtProbationEndDate, txtPerformanceReviewDate;
        var hdDefaultProbationPeriod, hdDefaultperformancereview;
        
        txtProbationEndDate = document.getElementById(txtDateofJoining.id.substring(0, txtDateofJoining.id.lastIndexOf('_') + 1) + "txtProbationEndDate");
       // txtPerformanceReviewDate = document.getElementById(txtDateofJoining.id.substring(0, txtDateofJoining.id.lastIndexOf('_') + 1) + "txtPerformanceReviewDate");
        hdDefaultProbationPeriod = document.getElementById(txtDateofJoining.id.substring(0, txtDateofJoining.id.lastIndexOf('_') + 1) + "hdDefaultProbationPeriod");
        //hdDefaultperformancereview = document.getElementById(txtDateofJoining.id.substring(0, txtDateofJoining.id.lastIndexOf('_') + 1) + "hdDefaultperformancereview");
        
        var DateofJoining = new Date(yearOfJoining, monthOfJoining - 1, dateOfJoining);
        
        DateofJoining.setDate(parseInt(DateofJoining.getDate()) + parseFloat(hdDefaultProbationPeriod.value));
        txtProbationEndDate.value = DateofJoining.localeFormat("dd/MM/yyyy");
        
        //DateofJoining = new Date(yearOfJoining, monthOfJoining - 1, dateOfJoining);
        //DateofJoining.setDate(parseInt(DateofJoining.getDate()) + parseFloat(hdDefaultperformancereview.value));
        //txtPerformanceReviewDate.value = DateofJoining.localeFormat("dd/MM/yyyy");
    }
}
function ValidateFromDate(source, args)
{
   var txtFromDate = document.getElementById(source.controltovalidate);
   if (txtFromDate.value != "")
            {
                if(!isValidDate(txtFromDate.value))
    {
       if(source.innerText == undefined) { source.textContent =  'Please enter valid date'; }
            else { source.innerText =  'Please enter valid date'; }
   
        args.IsValid = false;
   }  
   else
   {
        var ddlFinyear=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlFinyear");
        var finyearvalue=ddlFinyear.value.substring(0,12);
        var finyearendvalue=ddlFinyear.value.substring(13,25);
        var fromdate=txtFromDate.value;
        if (Convert2Date( fromdate) < new Date(finyearvalue) || Convert2Date( fromdate) > new Date(finyearendvalue))
        {
             if(source.innerText == undefined) { source.textContent =  'Date should be with in FinYeardate'; }
                else { source.innerText =  'Date should be with in FinYeardate'; }
       
              args.IsValid = false;
        }
    }
   }
}
function ConfirmCancel(imgCancel)
{
    var hidControl = document.getElementById("ctl00_hidCulture");
    var message = "";
    message = hidControl.value == 'ar-AE' ? 'هل تريد إلغاء هذا الطلب؟' : 'Do you want to cancel this request?';
    return confirm(message);
}
function CheckRequestdate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var txtOldDueDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hfLoanDate");
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    var hidControl = document.getElementById("ctl00_hidCulture");
    var message = "";
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
                    if(hidControl.value == "ar-AE" )
                    {
                        source.innerText = "الطلب تفاصيل أضاف بنجاح";
                    }
                    else
                    {
                        source.innerText = "Please enter date.";
                    }
        
//             message = hidControl.value == "ar-AE" ? "الطلب تفاصيل أضاف بنجاح." : "Please enter date.";
//             source.textContent = message; 

                //source.textContent = "Please enter date.";
        }
        else 
        { 
        
                   if(hidControl.value == "ar-AE" )
                    {
                        source.innerText = "الطلب تفاصيل أضاف بنجاح";
                    }
                    else
                    {
                        source.innerText = "Please enter date.";
                    }
                    
//             message = hidControl.value == "ar-AE" ? "الطلب تفاصيل أضاف بنجاح." : "Please enter date.";
//             source.textContent = message; 

              // source.textContent = "Please enter date.";
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    
   // else if (Convert2Date(txtOldDueDate.value) > Convert2Date(ctrl.value))
        else if ((currentDate) > Convert2Date(ctrl.value))
        {
        if(source.innerText == undefined) 
        {   
                  if(hidControl.value == "ar-AE" )
                    {
                        source.innerText = "الطلب تفاصيل أضاف بنجاح";
                    }
                    else
                    {
                        source.innerText = "Requested date should not earlier than current date.";
                    }
                
                
//                message = hidControl.value == "ar-AE" ? "الطلب تفاصيل أضاف بنجاح." : "Requested date should not earlier than current date.";
//        
//                source.textContent = message; 
        }
        else 
        { 
                    if(hidControl.value == "ar-AE" )
                    {
                        source.innerText = "الطلب تفاصيل أضاف بنجاح";
                    }
                    else
                    {
                        source.innerText = "Requested date should not earlier than current date.";
                    }
                    
//                   message = hidControl.value == "ar-AE" ? "الطلب تفاصيل أضاف بنجاح." : "Requested date should not earlier than current date.";
//                    source.textContent = message; 
                //source.innerText = "Requested date should not earlier than current date.";
        
         }
            
        args.IsValid= false;
        }
    else
    {
        args.IsValid=true;
    }    
 }
 function CheckFuturedate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var txtOldDueDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hfLoanDate");
    var currentDate=new Date();
    var hidControl = document.getElementById("ctl00_hidCulture");
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
  
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";// "Please enter  date."; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    
   // else if (Convert2Date(txtOldDueDate.value) > Convert2Date(ctrl.value))
//    else if ((currentDate) < Convert2Date(ctrl.value))
//        {
//            if(source.innerText == undefined) 
//            { 
//            source.textContent = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحالي":"Requested date should not be greater than the current date.";
//            }//"Requested date should not be greater than the current date."; 
//            else 
//            { 
//            source.innerText = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحاليs":"Requested date should not be greater than the current date.";
//            }// "Requested date should not be greater than the current date."; 
//            
//        args.IsValid= false;
//        }
    else
    {
        args.IsValid=true;
    }    
 }
 function CheckFuturedate2(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var txtOldDueDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hfLoanDate");
    var currentDate=new Date();
    var hidControl = document.getElementById("ctl00_hidCulture");
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
  
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";// "Please enter  date."; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    
   // else if (Convert2Date(txtOldDueDate.value) > Convert2Date(ctrl.value))
    else if ((currentDate) < Convert2Date(ctrl.value))
        {
        if(source.innerText == undefined) 
            { 
            source.textContent = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحالي":"Requested date should not be greater than the current date.";
            }
            else 
            { 
            source.innerText = hidControl.value == "ar-AE" ?"تاريخ الطلب لا ينبغي ان يكون أكبر من التاريخ الحاليs":"Requested date should not be greater than the current date.";
            }
            
        args.IsValid= false;
        }
    else
    {
        args.IsValid=true;
    }    
 }
 function CheckValidDate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
     var hidControl = document.getElementById("ctl00_hidCulture");
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ? 'الطلب تفاصيل أضاف بنجاح' : 'Please enter  date.'; 
        }
        else 
        { 
            source.innerText = hidControl.value == "ar-AE" ? 'الطلب تفاصيل أضاف بنجاح' : 'Please enter  date.'; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
    else
    {
        args.IsValid=true;
    }    
 }
 function Checkdate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) 
        { 
             source.textContent = hidControl.value == "ar-AE" ?"يرجى إدخال تاريخ":"Please enter date."; 
        }
        else 
        { 
            source.innerText =  hidControl.value == "ar-AE" ?"يرجى إدخال تاريخ":"Please enter date."; 
        }
         args.IsValid= false;
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid = false;
    }
     else if ((currentDate) > Convert2Date(ctrl.value))
        {
        if(source.innerText == undefined) { source.textContent =  hidControl.value == "ar-AE" ?"من تاريخ لا ينبغي أن يكون في وقت سابق من التاريخ الحالي":"From date should not be earlier than current date."; }
            else { source.innerText =  hidControl.value == "ar-AE" ?"من تاريخ لا ينبغي أن يكون في وقت سابق من التاريخ الحالي":"From date should not be earlier than current date."; }
            
        args.IsValid= false;
        }    
    else
    {
        args.IsValid=true;
    }    
 }
 
 function CheckResumedate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    
    if(ctrl.value != "")
    {
        if(!isDate(ctrl.value,source))
        {
            args.IsValid = false;
        }
     
        else
        {
                var DateofBirth = Convert2Date(ctrl.value);
                var validDateofBirth = GetSysDate();
                validDateofBirth.setFullYear(parseInt(validDateofBirth.getFullYear()) - 18);
                if(DateofBirth > validDateofBirth)
                {
                    if(source.innerText == undefined) { source.textContent =  'Age should be greater than or equal to 18 years'; }
                    else { source.innerText =  'Age should be greater than or equal to 18 years'; }
                    args.IsValid = false;
                }
           
        }    
    }
   
 }
 
 function CheckResumePExpirydate(source,args)
 {
    var ctrl = document.getElementById(source.controltovalidate);
    
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    
    if(ctrl.value != "")
    {
        if(!isDate(ctrl.value,source))
        {
            args.IsValid = false;
        }
     
//        else
//        {
//                var DateofBirth = Convert2Date(ctrl.value);
//                var validDateofBirth = GetSysDate();
//                validDateofBirth.setFullYear(parseInt(validDateofBirth.getFullYear()) - 18);
//                if(DateofBirth > validDateofBirth)
//                {
//                    if(source.innerText == undefined) { source.textContent =  'Age should be greater than or equal to 18 years'; }
//                    else { source.innerText =  'Age should be greater than or equal to 18 years'; }
//                    args.IsValid = false;
//                }
//           
//        }    
    }
   
 }
 
function ValidateDuedate(source, args)
{
  var txtDueDate = document.getElementById(source.controltovalidate);
  var txtOldDueDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hfDuedate");
  
  var currentDate=new Date();
  currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
  
  
   if(!isValidDate(txtDueDate.value))
    {
       if(source.innerText == undefined) { source.textContent =  'Please enter valid date'; }
            else { source.innerText =  'Please enter valid date'; }
   
        args.IsValid = false;
   }  
//   else if (Convert2Date(txtOldDueDate.value) > Convert2Date(txtDueDate.value))
//   {
//       if(source.innerText == undefined) { source.textContent ="Due date should not earlier than today."; }
//            else { source.innerText = "Due date should not earlier than today."; }
//            
//        args.IsValid= false;
//   }
   else
   {
        var txtToDate=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtToDate");
         if (txtToDate.value != "")
            {
            var Duedate=Convert2Date(txtDueDate.value);
            var Todate=Convert2Date(txtToDate.value);
            
             if (Duedate < Todate)
                {
                     if(source.innerText == undefined) { source.textContent =  'Due date should be greater than To date'; }
                        else { source.innerText =  'Due date should be greater than To date'; }
               
                      args.IsValid = false;
                }
            }
            else
            {
                var ddlFinyear=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlFinyear");
                var finyearvalue=ddlFinyear.value.substring(0,12);
                var duedate=txtDueDate.value;
                
                
                if (Convert2Date( duedate) < new Date(finyearvalue))
                {
                     if(source.innerText == undefined) { source.textContent =  'Due date should be greater than FinYeardate'; }
                        else { source.innerText =  'Due date should be greater than FinYeardate'; }
               
                      args.IsValid = false;
                }
            }
   }   
}
function ValidateInitiateTodate(source, args)
{
    var txtToDate = document.getElementById(source.controltovalidate);
    
    var txtFromDate=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtFromDate");
    
     if (txtFromDate.value != "" && txtToDate.value != "" )
     {
        if (Convert2Date(txtToDate.value) < Convert2Date(txtFromDate.value))
        {
             source.innerHTML =  'To date should be smaller than From date'; 
             args.IsValid = false;
        }
    }
}

function ValidateOtherDocExpiryDate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
      var hidControl = document.getElementById("ctl00_hidCulture");
    if (!isValidDate(txtExpiryDate.value))
    {
       if(hidControl.value == "en-US")
            source.textContent =  'Please enter valid date'; 
          else
            source.textContent =  'الرجاء إدخال تاريخ صالح';
            
        args.IsValid = false;
    }
    else
    {
         var Expirydate = Convert2Date(txtExpiryDate.value);
         var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtIssuedate");
         var Issuedate = Convert2Date(txtIssuedate.value);
         
         if(Issuedate > Expirydate)
         {
               if(hidControl.value == "en-US")
                 source.textContent = "Expiry date should be greater than Issue date"; 
              else
                 source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"
                   
                   
            args.IsValid = false;
         }        
    }
}

function validatepassportExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");

    if (!isValidDate(txtExpiryDate.value))
    {
          if(hidControl.value == "en-US")
            source.textContent =  'Please enter valid date'; 
          else
            source.textContent =  'الرجاء إدخال تاريخ صالح';
           args.IsValid = false;
    }
    else
    {
     var Expirydate = Convert2Date(txtExpiryDate.value);
     var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtIssuedate");
     var Issuedate = Convert2Date(txtIssuedate.value);
     
       if(Issuedate >= Expirydate)
        {
           if(hidControl.value == "en-US")
             source.textContent = "Expiry date should be greater than Issue date"; 
          else
             source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"
                   
            args.IsValid = false;
        }        
    }
}

function validatevisaExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    if (!isValidDate(txtExpiryDate.value))
    {
        if (source.innerText == undefined) 
        {
        source.textContent  = (hidControl.value == "en-US" ?  'Please enter valid date' : 'الرجاء إدخال تاريخ صالح');
        }
        else 
        {
        source.innerText= (hidControl.value == "en-US" ?  'Please enter valid date' : 'الرجاء إدخال تاريخ صالح');
        }
           args.IsValid = false;
    }
    else
    {
     var Expirydate = Convert2Date(txtExpiryDate.value);
     var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtIssuedate");
     var Issuedate = Convert2Date(txtIssuedate.value);
     
       if(Issuedate >= Expirydate)
        {
        
           if(source.innerText == undefined) 
           { 
           source.textContent = (hidControl.value == "en-US" ?  '"Expiry date should be greater than Issue date"' : 'يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار');  
           }
            else 
            { 
            source.innerText =  (hidControl.value == "en-US" ?  '"Expiry date should be greater than Issue date"' : 'يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار');  
            }
                   
            args.IsValid = false;
        }
    }
}
function validateTradeLicenseExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    if (!isValidDate(txtExpiryDate.value))
    {
        if(source.innerText == undefined)
        { 
                if(hidControl.value == "en-US")
                    source.textContent =  'Please enter valid date'; 
                else
                    source.textContent =  'الرجاء إدخال تاريخ صالح';        
        }
        else 
        { 
            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';                
       }
           args.IsValid = false;
    }
    else
    {
     var Expirydate = Convert2Date(txtExpiryDate.value);
     var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtIssuedate");
     var Issuedate = Convert2Date(txtIssuedate.value);
     
       if(Issuedate >= Expirydate)
        {        
            if(source.innerText == undefined) 
           { 
           source.textContent = (hidControl.value == "en-US" ?  '"Expiry date should be greater than Issue date"' : 'يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار');  
           }
            else 
            { 
            source.innerText =  (hidControl.value == "en-US" ?  '"Expiry date should be greater than Issue date"' : 'يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار');  
            }                   
            args.IsValid = false;
        }
    }
}
function validatepassportIssuedate(source,args)
{
    var txtIssuedate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");

    if (!isValidDate(txtIssuedate.value))
    {
        if(source.innerText == undefined)
        { 
                if(hidControl.value == "en-US")
                    source.textContent =  'Please enter valid date'; 
                else
                    source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
        else 
        { 
        

             if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
             
       }
        args.IsValid = false;
    }
    else
    {
        var currentDate=new Date();
        currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        var Issuedate = Convert2Date(txtIssuedate.value);

        if(Issuedate > currentDate)
        {
             if(source.innerText == undefined)
                { 

                    if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';
                
                }
                else 
                { 
                

                     if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';   
                     
               }
          
            args.IsValid = false;
        }
    }
}
function validatevisaIssuedate(source,args)
{
    var txtIssuedate=document.getElementById(source.controltovalidate);
      var hidControl = document.getElementById("ctl00_hidCulture");
    if (!isValidDate(txtIssuedate.value))
    {
        if(source.innerText == undefined)
        { 
                if(hidControl.value == "en-US")
                    source.textContent =  'Please enter valid date'; 
                else
                    source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
        else 
        { 
        

             if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
             
       }
           args.IsValid = false;
    }
    else
    {
        var currentDate=new Date();
        currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        var Issuedate = Convert2Date(txtIssuedate.value);

        if(Issuedate > currentDate)
        {
            if(source.innerText == undefined)
                { 

                    if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';
                
                }
                else 
                { 
                

                     if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';   
                     
               }

            args.IsValid = false;
        }
    }
}
function validateTradeLicenseIssuedate(source,args)
{
    var txtIssuedate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    if (!isValidDate(txtIssuedate.value))
    {        
        if(source.innerText == undefined)
        { 
                if(hidControl.value == "en-US")
                    source.textContent =  'Please enter valid date'; 
                else
                    source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
        else 
        { 
        

             if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
          }   
           args.IsValid = false;
    }
    else
    {
        var currentDate=new Date();
        currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
        var Issuedate = Convert2Date(txtIssuedate.value);

        if(Issuedate > currentDate)
        {
        if(source.innerText == undefined)
                { 

                    if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';
                
                }
                else 
                { 
                

                     if(hidControl.value == "en-US")
                        source.textContent =  'Issue date should not be greater than current date'; 
                    else
                        source.textContent =  'لا ينبغي أن يكون تاريخ الاصدار أكبر من التاريخ الحالي ';   
                     
               }
            args.IsValid = false;
        }
    }
}
function validateQualificationPeriod(source,args)
{
    var txtToDate=document.getElementById(source.controltovalidate);
    var txtFromDate=document.getElementById(txtToDate.id.replace(/txtToDate/,"txtFromDate"));
    var hidControl = document.getElementById("ctl00_hidCulture");
     
    if (!isValidDate(txtFromDate.value) || !isValidDate(txtToDate.value))
    {
           source.errormessage= (hidControl.value == "en-US" ?'Please enter valid date' : 'الرجاء إدخال تاريخ صالح');
           args.IsValid = false;
    }
    else
    {
     var FromDate = Convert2Date(txtFromDate.value);
     var ToDate = Convert2Date(txtToDate.value);
     var currentDate=new Date();
     currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
       if(FromDate >= ToDate)
        {
            source.errormessage= (hidControl.value == "en-US" ?'To date should be greater than From date' : 'حتى الآن يجب أن يكون أكبر من من تاريخ');//"To date should be greater than From date";                  
            args.IsValid = false;
        }
        else if(FromDate > currentDate)
        {
            source.errormessage=  (hidControl.value == "en-US" ?'From date should not be greater than current date' : ' من تاريخ لا ينبغي أن يكون أكبر من التاريخ الحالي'); //"From date should not be greater than current date";                  
            args.IsValid = false;
        }
    }
}



function validateDateofJoining(source, args)
{

    var txtDateofJoining = document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");

    
    if(!isValidDate(txtDateofJoining.value))
    {
       if(source.innerText == undefined)
        { 

                if(hidControl.value == "en-US")
                    source.textContent =  'Please enter valid date'; 
                else
                    source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
        else 
        { 
        

             if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
             
       }
   
        args.IsValid = false;
    }
    else
    {
        var DateofJoining = Convert2Date(txtDateofJoining.value);
        
        var hdFinYearStartDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdFinYearStartDate");
        
        var hdCurrentDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdCurrentDate");

        var CurrentDate = Convert2Date(hdCurrentDate.value)
        
        var hdCandidateID=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdCandidateID");
        
         var hdcandidateDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hdcandidateDate");
          
        var FinYearStartDate = Convert2Date(hdFinYearStartDate.value);
       
        var CandidateID=(isNaN(parseInt(hdCandidateID.value)) ? 0 : parseInt(hdCandidateID.value)); ;
        
        var Candidatedate = Convert2Date(hdcandidateDate.value);
        
        
        
        if(DateofJoining >CurrentDate )
        {
          if(source.innerText == undefined)
           { 
//            source.textContent =  "Date of joining must not be a future date."; 
              if(hidControl.value == "en-US")
                  source.textContent =  'Date of joining must not be a future date.'; 
              else
                  source.textContent =  'تاريخ الانضمام يجب ألا يكون تاريخ مستقبلي. ';
          
           }
           else {
//                     source.innerText =  "Date of joining must not be a future date."; 
                     if(hidControl.value == "en-US")
                         source.innerText =  'Date of joining must not be a future date.'; 
                     else
                         source.innerText =  'تاريخ الانضمام يجب ألا يكون تاريخ مستقبلي. ';
                }
        
           args.IsValid = false;
            return ;
        }
        
        if(CandidateID > 0)
        {
        if(DateofJoining < Candidatedate)
        {
        
           if(source.innerText == undefined) 
            { 
                //source.textContent =  "Joining date should be greater than or equal to candidate hire date.";
                  if(hidControl.value == "en-US")
                     source.textContent =  'Please enter valid date'; 
                  else
                     source.textContent =  'الرجاء إدخال تاريخ صالح';
            }
            else { 
                   // source.innerText =  "Joining date should be greater than or equal to candidate hire date."; 
                     if(hidControl.value == "en-US")
                        source.innerText =  'Please enter valid date'; 
                    else
                        source.innerText =  'الرجاء إدخال تاريخ صالح';
                    
                 }
        
           args.IsValid = false;
        }
        }
        
        if(FinYearStartDate > DateofJoining)
        {
        
           if(source.innerText == undefined) 
           {
            //source.textContent =  "Date of joining should be greater than company's financial year date"; 
              if(hidControl.value == "en-US")
                   source.textContent =  "Date of joining should be greater than company's financial year date"; 
              else
                   source.textContent =  'يجب أن يكون تاريخ الانضمام أكبر من تاريخ السنة المالية  للشركة';
            
           }
            else {
                     //source.innerText =  "Date of joining should be greater than company's financial year date";
                    if(hidControl.value == "en-US")
                        source.innerText =  "Date of joining should be greater than company's financial year date"; 
                    else
                        source.innerText =  'يجب أن يكون تاريخ الانضمام أكبر من تاريخ السنة المالية  للشركة';
                 }
                   
            args.IsValid = false;
        }
        else
        {
            var txtDateofBirth = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtDateofBirth");
            
            var DateofBirth = Convert2Date(txtDateofBirth.value);
        
            if(DateofBirth > DateofJoining)
            {
              if(source.innerText == undefined) 
              {
                 //source.textContent =  "Date of joining should be greater than date of birth"; 
                   if(hidControl.value == "en-US")
                    source.textContent =  'Date of joining should be greater than date of birth'; 
                else
                    source.textContent =  'يجب أن يكون تاريخ الانضمام أكبر من تاريخ الميلاد ';
              }
              else {
                     //source.innerText = "Date of joining should be greater than date of birth";
                       if(hidControl.value == "en-US")
                           source.innerText =  'Date of joining should be greater than date of birth'; 
                       else
                           source.innerText =  'يجب أن يكون تاريخ الانضمام أكبر من تاريخ الميلاد ';
                     
                   }
                         
                args.IsValid = false;
            }
        }        
    }
}

function validateInitiateEmployee(source, args)
{
    var lstEmployee=document.getElementById(source.controltovalidate);
    var lstEvaluator=document.getElementById(source.controltovalidate);
        if (lstEmployee.length ==0)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
         if (lstEvaluator.length ==0)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
        
}
function validateDesignation(source, args)
{
    var ddlDesignation=document.getElementById(source.controltovalidate);
    var chkJobPromotion=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkJobPromotion");
    var txtDesigWithEffetFrom=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtDesigWithEffetFrom");
    var currentDate = new Date();
    currentDate = new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());

    if (chkJobPromotion.checked)
    {
        if (!isValidDate(txtDesigWithEffetFrom.value))
        {
            source.innerHTML = "Please enter effet from"
            args.IsValid = false;
        }
        else if( Convert2Date(txtDesigWithEffetFrom.value) < currentDate)
        {
            source.innerHTML = "With effect from date must be a future date";
            args.IsValid = false;
        }
        else
            args.IsValid = true;
    }    
}

function ValidateWithEffectFrom(source, args)
{
     var txtWEF = document.getElementById(source.controltovalidate);
     var chkAlterSalary = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkAlterSalary");
     var hfServerDate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "hfServerDate");

     if(chkAlterSalary != null && chkAlterSalary.checked && hfServerDate != null)
     {
     
     var CurrentDate = Convert2Date(hfServerDate.value)
     var FromDate    = Convert2Date(txtWEF.value)
     if(txtWEF != null && txtWEF.value == "")
     {
        if(source.innerText == undefined) 
            {
                 source.textContent =  "Please enter with effect from"; 
            }
            else
                 {
                     source.innerText = "Please enter with effet from";
                 }
        args.IsValid = false;
     }
     else if(!isValidDate(txtWEF.value))
     {
        if(source.innerText == undefined) { source.textContent =  "Please enter valid date"; }
                          else { source.innerText = "Please enter valid date";}
                                     
                            args.IsValid = false;
     }
     
     else if(FromDate < CurrentDate)
     {
      if(source.innerText == undefined) { source.textContent =  "with effect date should be greater than today"; }
                          else { source.innerText = "with effect date should be greater than today";}
                                     
                            args.IsValid = false;
     }
     else
        args.IsValid = true;
     
     }
     else
     args.IsValid = true;
     
 }
 
  function ValidateAlterSalary(id)
  {
   var div = document.getElementById(id.id);
   if(div != null)
   {
   
   var rfvBasicpay = document.getElementById(div.id.replace(/chkAlterSalary/,"rfvBasicpay"));
   
   var upEmployees = document.getElementById(div.id.replace(/chkAlterSalary/,"upEmployees"));

   if(rfvBasicpay != null && upEmployees != null)
   {
//      if(div.checked)
//      {
//        rfvBasicpay.enabled = true;
//      }
//      else
//      {
//        rfvBasicpay.enabled = false;
//      }
//     
   }
   
 
   
//   if(div.checked)
//   {
//    var anc = document.getElementById(div.id.replace(/chkAlterSalary/,"ancParticulars"));
//    toggleDiv(anc)
//   }
   toggleDiv(div)
   }
  }
 
function validateTravelFare(source, args)
{
   var txtTravelFare=document.getElementById(source.controltovalidate);
   if ( txtTravelFare.value == "0"  || txtTravelFare.value == "")
    {
        txtTravelFare.value = '0.00'
    }
    var value = ConvertToMoney(txtTravelFare.value);
    
    if (txtTravelFare.value == "0"  || txtTravelFare.value == "" || txtTravelFare.value =='0.00')
    {
        if(source.innerText == undefined) { source.textContent =  "Please enter Travel fare."; }
                        else { source.innerText = "Please enter Travel fare.";}
                         
                        args.IsValid = false;
    }
    else if (isNaN(value))
    {
        if(source.innerText == undefined) { source.textContent =  "Invalid number"; }
            else { source.innerText = "Invalid number";}
             
            args.IsValid = false;
    }
}

function validateGradeIncrease(source, args)
{
      var txtGradeIncrease=document.getElementById(source.controltovalidate);
      var chkGradeIncrease=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkGradeIncrease");
      var txtGradeWithEffetFrom=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtGradeWithEffetFrom");
      
        if (chkGradeIncrease.checked)
        {
         
            if ( txtGradeIncrease.value == "0" || txtGradeIncrease.value == "")
            {
            
                 if(source.innerText == undefined) { source.textContent =  "Please enter Grade Increase."; }
                      else { source.innerText = "Please enter Grade Increase.";}
                                 
                        args.IsValid = false;
            }
            else if (!isValidDate(txtGradeWithEffetFrom.value))
            {
                 if(source.innerText == undefined) { source.textContent =  "Please enter effet from"; }
                      else { source.innerText = "Please enter effet from";}
                                 
                        args.IsValid = false;
            }
            
            else
            {
                args.IsValid = true ;
            }
        }    
   }

function validateSalaryIncrease(source, args)
{
   var txtsalaryIncrease=document.getElementById(source.controltovalidate);
   var chksalaryIncrease=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkSalaryIncrease");
   var txtSalaryWithEffetFrom=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtSalaryWithEffetFrom");
   
    if (chksalaryIncrease.checked)
    {
       if ( txtsalaryIncrease.value == "0"  || txtsalaryIncrease.value == "")
        {
            txtsalaryIncrease.value = '0.00'
        }
        var value = ConvertToMoney(txtsalaryIncrease.value);
        
        if (txtsalaryIncrease.value == "0"  || txtsalaryIncrease.value == "" || txtsalaryIncrease.value =='0.00')
        {
            if(source.innerText == undefined) { source.textContent =  "Please enter salary increase."; }
                            else { source.innerText = "Please enter salary increase.";}
                             
                            args.IsValid = false;
        }
       
        else if (isNaN(value))
                
                    {
                        if(source.innerText == undefined) { source.textContent =  "Invalid number"; }
                            else { source.innerText = "Invalid number";}
                             
                            args.IsValid = false;
                    }
        
         else if (!isValidDate(txtSalaryWithEffetFrom.value))
                {
                     if(source.innerText == undefined) { source.textContent =  "Please enter effet from"; }
                          else { source.innerText = "Please enter effet from";}
                                     
                            args.IsValid = false;
                }
                
          else
                {
                    args.IsValid = true ;
                }
    }   
}

function validateAllowanceIncrease(source, args)
{
   var txtAllowanceIncrease=document.getElementById(source.controltovalidate);
   var chkAllowanceIncrease=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkAllowanceIncrease");
    var txtAllowanceWithEffetFrom=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtAllowanceWithEffetFrom");
    if (chkAllowanceIncrease.checked)
    {
     
        if ( txtAllowanceIncrease.value == "0"  || txtAllowanceIncrease.value == "")
        {
            txtAllowanceIncrease.value = '0.00'
        }
        var value = ConvertToMoney(txtAllowanceIncrease.value);
        
        if (txtAllowanceIncrease.value == "0"  || txtAllowanceIncrease.value == "" || txtAllowanceIncrease.value =='0.00')
        {
            if(source.innerText == undefined) { source.textContent =  "Please enter allowance increase."; }
                            else { source.innerText = "Please enter allowance increase.";}
                             
                            args.IsValid = false;
        }
       
        else if (isNaN(value))
                
                    {
                        if(source.innerText == undefined) { source.textContent =  "Invalid number"; }
                            else { source.innerText = "Invalid number";}
                             
                            args.IsValid = false;
                    }
       
         else if (!isValidDate(txtAllowanceWithEffetFrom.value))
                {
                     if(source.innerText == undefined) { source.textContent =  "Please enter effet from"; }
                          else { source.innerText = "Please enter effet from";}
                                     
                            args.IsValid = false;
                }
        else
        {
         args.IsValid = true ;
        }
    }
}

function validateBonusIncrease(source, args)
{
   var txtBonusIncrease=document.getElementById(source.controltovalidate);
   var chkBonusIncrease=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "chkBonusIncrease");
    var txtBonusWithEffetFrom=document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtBonusWithEffetFrom");
    if (chkBonusIncrease.checked)
    {
     
        if ( txtBonusIncrease.value == "0"  || txtBonusIncrease.value == "")
        {
            txtBonusIncrease.value = '0.00'
        }
        var value = ConvertToMoney(txtBonusIncrease.value);
        
        if (txtBonusIncrease.value == "0"  || txtBonusIncrease.value == "" || txtBonusIncrease.value =='0.00')
        {
            if(source.innerText == undefined) { source.textContent =  "Please enter bonus increase."; }
                            else { source.innerText = "Please enter bonus increase.";}
                             
                            args.IsValid = false;
        }
       
        else if (isNaN(value))
                
                    {
                        if(source.innerText == undefined) { source.textContent =  "Invalid number"; }
                            else { source.innerText = "Invalid number";}
                             
                            args.IsValid = false;
                    }
         else if (!isValidDate(txtBonusWithEffetFrom.value))
                {
                     if(source.innerText == undefined) { source.textContent =  "Please enter effet from"; }
                          else { source.innerText = "Please enter effet from";}
                                     
                            args.IsValid = false;
                }
        else
        {
         args.IsValid = true ;
        }
    }
}

function validateActionVisibility(ctl)
{
 
   var chkJobPromotion = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "chkJobPromotion");
   var chkGradeIncrease = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "chkGradeIncrease");
   var chkSalaryIncrease = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "chkSalaryIncrease");
   var chkAllowanceIncrease = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "chkAllowanceIncrease");
   var chkBonusIncrease = document.getElementById(ctl.id.substr(0, ctl.id.lastIndexOf('_') + 1) + "chkBonusIncrease");
   
   
   var ddlDesignation = document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"ddlDesignation"));
   var txtGradeIncrease = document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"txtGradeIncrease"));
   var txtsalaryIncrease = document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"txtsalaryIncrease"));
   var txtAllowanceIncrease = document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"txtAllowanceIncrease"));
   var txtBonusIncrease = document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"txtBonusIncrease"));

    var cvtxtGradeIncrease=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"cvtxtGradeIncrease"));
    var cvtxtsalaryIncrease=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"cvtxtsalaryIncrease")); 
    var cvAllowanceIncrease=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"cvAllowanceIncrease")); 
    var cvtxtBonusIncrease=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"cvtxtBonusIncrease")); 
        
    var tableDesig=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"tableDesig"));   
    var tableGrade=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"tableGrade"));  
    var tableSalaryWithEffetFrom=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"tableSalaryWithEffetFrom"));  
    var tableAllowanceWithEffetFrom=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"tableAllowanceWithEffetFrom"));  
    var tableBonusWithEffetFrom=document.getElementById(chkJobPromotion.id.replace(/chkJobPromotion/,"tableBonusWithEffetFrom"));  
    
    if (chkJobPromotion.checked)
    {
         tableDesig.style.display = "block";
    }
    else
    {
        tableDesig.style.display = "none";
    }
//    if (chkGradeIncrease.checked)
//    {
//           tableGrade.style.display = "block";
//    }
//    else
//    {
//         tableGrade.style.display = "none";
//         cvtxtGradeIncrease.style.display = "none";
//    }
//    if (chkSalaryIncrease.checked)
//    {
//           tableSalaryWithEffetFrom.style.display = "block";
//    }
//    else
//    {
//         tableSalaryWithEffetFrom.style.display = "none";
//         cvtxtsalaryIncrease.style.display = "none";
//    }
//    if (chkAllowanceIncrease.checked)
//    {
//       tableAllowanceWithEffetFrom.style.display = "block";
//    }
//    else
//    {
//      tableAllowanceWithEffetFrom.style.display = "none";
//      cvAllowanceIncrease.style.display = "none";
//    }
//    if (chkBonusIncrease.checked)
//    {
//        tableBonusWithEffetFrom.style.display = "block";
//    }
//    else
//    {
//       tableBonusWithEffetFrom.style.display = "none";
//       cvtxtBonusIncrease.style.display = "none";
//    }
}
function validateProbationEndDate(source, args)
{
    var ddlWorkStatus = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlWorkStatus");
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    if (ddlWorkStatus.value == "7")
    {
        var txtProbationEndDate = document.getElementById(source.controltovalidate);
    
         if ( txtProbationEndDate.value != "")
         {
            if(!isValidDate(txtProbationEndDate.value))
            {
                if(source.innerText == undefined) 
                  { 
                    if(hidControl.value == "en-US")
                        source.textContent =  'Please enter valid date'; 
                    else
                        source.textContent =  'الرجاء إدخال تاريخ صالح ';
                  }
                else {
                        if(hidControl.value == "en-US")
                            source.innerText =  'Please enter valid date'; 
                        else
                            source.innerText =  'الرجاء إدخال تاريخ صالح ';
                         
                      }
                
                args.IsValid = false;
            }
            else
            {
                var txtDateofJoining = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtDateofJoining");
    
                var ProbationEndDate, DateofJoining;

                ProbationEndDate = Convert2Date(txtProbationEndDate.value);

                DateofJoining = Convert2Date(txtDateofJoining.value);
        
                if(DateofJoining > ProbationEndDate)
                {
                        if(source.innerText == undefined)
                        { 
                            if(hidControl.value == "en-US")
                                source.textContent =  'Probation end date should be greater than date of joining'; 
                            else
                                source.textContent =  'يجب أن يكون تاريخ نهاية الاختبار أكبر من تاريخ التحاقه ';
                        }
                        else {  
                                if(hidControl.value == "en-US")
                                    source.innerText =  'Probation end date should be greater than date of joining'; 
                                else
                                    source.innerText =  'يجب أن يكون تاريخ نهاية الاختبار أكبر من تاريخ التحاقه ';
                             }
                        args.IsValid = false;
                }
            }
        } 
        else
        { 
            if(hidControl.value == "en-US")
                source.innerText =  'Please enter valid date'; 
            else
                source.innerText =  'الرجاء إدخال تاريخ صالح ';
            args.IsValid = false;
        } 
    }    
}
function validatePerformanceReviewDate(source, args)
{
    var txtPerformanceReviewDate = document.getElementById(source.controltovalidate);
    
     if ( txtPerformanceReviewDate.value != "")
        {
         if(!isValidDate(txtPerformanceReviewDate.value))
        {
            if(source.innerText == undefined) { source.textContent =  'Please enter valid date'; }
                else { source.innerText =  'Please enter valid date'; }
            args.IsValid = false;
        }
        else
        {
             var txtDateofJoining = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtDateofJoining");
    
                var PerformanceReviewDate, DateofJoining;
                
                PerformanceReviewDate = Convert2Date(txtPerformanceReviewDate.value);
                
                DateofJoining = Convert2Date(txtDateofJoining.value);
                
                if(DateofJoining > PerformanceReviewDate)
                {
                 if(source.innerText == undefined) { source.textContent =  '  Performance review date should be greater than date of joining'; }
                    else { source.innerText =  '  Performance review date should be greater than date of joining'; }
                args.IsValid = false;
                }
        }
   }
}

function validateDateofBirth(source, args)
{
   var txtDateofBirth = document.getElementById(source.controltovalidate);
   var hidControl = document.getElementById("ctl00_hidCulture");
   
   if ( txtDateofBirth.value != "")
   {
        if(!isValidDate(txtDateofBirth.value))
        {
            if(hidControl != null)
            {
                if(source.innerText == undefined)
                 {
                    if(hidControl.value == "en-US")
                        source.textContent =  'Please enter valid date'; 
                    else
                        source.textContent =  'الرجاء إدخال تاريخ صالح';
                 }
                else
                 {
                     if(hidControl.value == "en-US")
                        source.innerText =  'Please enter valid date'; 
                      else
                        source.innerText =  'الرجاء إدخال تاريخ صالح';
                  }
              }
              else
              {
                 source.innerText =  'Please enter valid date'; 
              }
              
            args.IsValid = false;
        }
        else
        {
            var DateofBirth = Convert2Date(txtDateofBirth.value);
            var validDateofBirth = new Date();         
            validDateofBirth.setFullYear(parseInt(validDateofBirth.getFullYear()) - 18);
            if(DateofBirth > validDateofBirth)
            {
                if(hidControl != null)
                {
                    if(source.innerText == undefined) 
                    {
                         if(hidControl.value == "en-US")
                            source.textContent ='Age should be greater than or equal to 18 years';
                         else
                            source.textContent ='يجب أن يكون عمر أكبر من أو يساوي 18 سنة';
                    }
                    else 
                    { 
                         if(hidControl.value == "en-US")
                            source.innerText =  'Age should be greater than or equal to 18 years'; 
                         else
                            source.innerText ='يجب أن يكون عمر أكبر من أو يساوي 18 سنة';
                       
                     }
                 }
                 else
                 {
                     source.innerText =  'Age should be greater than or equal to 18 years'; 
                 }
                args.IsValid = false;
            }
        }
    }
}

function validateTransactionType(source, args)
{
    var control = document.getElementById(source.controltovalidate);
    
    var ddlTransactionType = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "ddlTransactionType");
   
    var isEmpty = false;
    
    if(control.type == "text" && control.value == "") { isEmpty = true; }
    if(control.type == "select-one" && control.value == "-1") { isEmpty = true; }
    
    var bankMandatory = new Array('ddlEmployerBankName', 'ddlCompanyAccount', 'txtAccountNumber','ddlEmployeeBankName');
    var WPSMandatory = new Array('ddlEmployerBankName', 'ddlCompanyAccount', 'txtAccountNumber', 'ddlEmployeeBankName');
    
    if(isEmpty)
    {
        switch(ddlTransactionType.value)
        {
            case "1":
                for(var i = 0; i < bankMandatory.length; i++)
                {
                    if(control.id.lastIndexOf(bankMandatory[i]) != -1)
                    {
                        args.IsValid = false;
                    }
                }
                break;
                
            case "3":
                for(var i = 0; i < WPSMandatory.length; i++)
                {
                    if(control.id.lastIndexOf(WPSMandatory[i]) != -1)
                    {
                        args.IsValid = false;
                    }
                }
                break;
        }
    }
}

function valCancelScheduleCandidate (containerId, chkId, listingType, action, requireConfirm)
{

   if( confirmAction(containerId, chkId, listingType, action, requireConfirm))
    {
        var chks = document.getElementById(containerId).getElementsByTagName("INPUT");
    
        var isAllchecked = true;
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
            {
                if(!chks[i].checked)
                {
                    isAllchecked = false;
                }               
                
            }
        }
        if(isAllchecked)
         {
            return confirm('All candidates are selected for cancel.This action will cause interview schedule cancellation, do you want to proceed?');
         }        
    }
    else
    {
      return false;
    }   
}

/* Function for checking whether interview performance entered for any candidate in interview process*/

function chkPerformanceEntry(btnId,validationGroup)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(!Page_IsValid)
    {        
        return false;
    }
    else
    {
        var datalist = document.getElementById(btnId.replace(/btnSubmit/,"dlProcess"));
        
        if(datalist == null)
            return false;
        
        var ValEntered = true;
        
        for(var i = 1; i < datalist.rows.length; i++)
        {   
            var txtPeformance = datalist.rows[i].getElementsByTagName("INPUT")[1];
            var Status = datalist.rows[i].getElementsByTagName("SELECT")[0];
            var txtRemarks = datalist.rows[i].getElementsByTagName("INPUT")[3];
            
            if(txtPeformance.value == "")
            {
                for(var j = 1; j < Status.length; j++)
                {
                    if(Status.options[j].selected)
                    {   
                        ValEntered=false;                    
                    }
                }                                   
            }       
        }
        if(!ValEntered)
        {
            alert('Please enter performance details for the candidates');
            return false; 
        }
        else
        {
        return true;
        }
    }
}

function confirmSaveSchedule(validationGroup,datalistId,IsEdit,IsWalkin,btnsubmitId)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    {    
       if(!IsWalkin)
        {
            return valCandidateDatalist(datalistId,IsEdit,btnsubmitId);
        }
        else
        {
            if(IsEdit)
            {
                var result=confirm('Do you want to maintain history for the schedule?');
                var hfconfirm=document.getElementById("ctl00_ctl00_content_public_content_hfConfirm");
                if(result)
                {
                   hfconfirm.value=1;
                   return true;
                }
                else
                {
                   hfconfirm.value=0;
                   return true;  
                }
            }
           else
           {
                return true;
            }
        }        
    }
}
function valCancelSchedule(validationGroup)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    {    
       return confirm("Are you sure to cancel this interview");
    }
}

function validateSubordinates(btnSubmit)
{
    var chkEmployees = document.getElementById(btnSubmit.id.substring(0, btnSubmit.id.lastIndexOf('_') + 1) + 'chkEmployees');
    
    var chks = document.getElementsByTagName('INPUT');
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            if(chks[i].checked)
            {
                checked = true;
            }
        }
    }
    if(checked)
    {
        return confirm('Are you sure to add these employees to the hierarchy?');
    }
    else
    {
        alert('Please select atleast an employee');
        return false;
    }
}
function checkStartEndDate(source,args)
{
    var ctl2=document.getElementById(source.controltovalidate);

    var ctl1=document.getElementById(ctl2.id.replace(/txtEndDate/,"txtStartDate"));
    
    if((ctl1.value!="") && (ctl2.value!=""))
    {
        var sdate = new Date(Convert2Date(ctl1 .value));
        
        var edate=new Date(Convert2Date(ctl2.value));
        
         if(edate.getFullYear()>sdate.getFullYear())
         {
          args.IsValid = true;
         }
        else if(sdate.format("MM/dd/yy")<=edate.format("MM/dd/yy"))
        {
            args.IsValid = true;
        }
        else
        {
            args.IsValid = false;
        }
    }
}
function checkStartEndTime(source,args)
{

    var time1=document.getElementById(source.controltovalidate.replace(/txtEndTime/,"txtStartTime")).value;

    var time2=document.getElementById(source.controltovalidate).value;
    
    var minTimeMeridien = time1.indexOf('PM') > 0 ? 'PM' : 'AM';
    var maxTimeMeridien = time2.indexOf('PM') > 0 ? 'PM' : 'AM';
    
    minTime = time1.replace(minTimeMeridien, ':00 '+ minTimeMeridien);
    maxTime = time2.replace(maxTimeMeridien, ':00 '+ maxTimeMeridien);
    
    var tempDate1 = new Date('January 1,2010 '+ minTime);
    var tempDate2 = new Date('January 1,2010 '+ maxTime);
    
    var objTimespan =new DateDiff2(tempDate2,tempDate1);
    
    var flag=true; 
   if(objTimespan.Hours<0)
        flag=false;
   else if(objTimespan.Minutes<0)
        flag=false;
   else if(objTimespan.Hours==0 &&  objTimespan.Minutes==0)
       flag=false;
   else
        flag=true;
        
    args.IsValid=flag;     
}

function validateOfficialEmailPassword(source, args)
{
    var txtOfficialEmailPassword = document.getElementById(source.controltovalidate);
    
    var txtOfficialEmail = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtOfficialEmail");
    
    if(txtOfficialEmail.value != "")
    {
        if(txtOfficialEmailPassword.value == "")
        {
            args.IsValid = false;
        }
    }
}

function setToMailId(chk)
{
  var lbl=document.getElementById(chk.id.replace(/chkSelect/,"lblEmail")); 
  var txtTime=document.getElementById(chk.id.replace(/chkSelect/,"txtTime"));        
  var txt= document.getElementById(lbl.id.substring(0, lbl.id.lastIndexOf('_dlCandidates')).replace(/fvVacancyDetails/,"txtTo"));
  if(chk.checked)
  {       
        if(txt.value=='')
        {
            txt.value = (lbl.innerText == undefined ? lbl.textContent : lbl.innerText);
        }
        else
        {
           txt.value=txt.value +','+(lbl.innerText == undefined ? lbl.textContent : lbl.innerText); 
        }
        if(txtTime!=null)
        {
            txtTime.disabled = false;
            txtTime.className = "textbox_mandatory";
        }
  }
  else
  {
       var str=(lbl.innerText == undefined ? lbl.textContent : lbl.innerText);
        if(txt.value != "")
        {
            if(txt.value.lastIndexOf(str) != -1)
            {
                var regExp = new RegExp("," + str, "g");
                txt.value = txt.value.replace(regExp, "");
                regExp = new RegExp(str + ",", "g");
                txt.value = txt.value.replace(regExp, "");
                regExp = new RegExp(str, "g");
                txt.value = txt.value.replace(regExp, "");
            }
        }
        txtTime.disabled="disabled";
        txtTime.value="";
        txtTime.className="textbox_disabled";  
    }
}
function showEdit(tbl)
{
    var btnEdit = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnEdit");
//    var btnEdit = ctl00_ctl00_content_public_content_dlTermination_ctl01_btnEdit;
    if(btnEdit) btnEdit.style.display = "block";
}
function hideEdit(tbl)
{
    var btnEdit = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnEdit");
    
    
    if(btnEdit) btnEdit.style.display = "none";
}

function showCancel(tbl)
{   
    var btnCancel = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnCancel")
    var btnEdit   = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnEdit")

    if(btnCancel) btnCancel.style.display = "inline-block";
    if(btnEdit) btnEdit.style.display = "inline-block";
}
function hideCancel(tbl)
{
    var btnCancel = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnCancel")
    var btnEdit   = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + "btnEdit")
    
    if(btnCancel) btnCancel.style.display = "none";
    if(btnEdit) btnEdit.style.display = "none";
}

function showEditButton(tbl, editBtnID)
{
    var btnEdit = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + editBtnID);
    
    if(btnEdit) btnEdit.style.display = "block";
}
function hideEditButton(tbl, editBtnID)
{
    var btnEdit = document.getElementById(tbl.id.substring(0, tbl.id.lastIndexOf('_') + 1) + editBtnID);
    
    if(btnEdit) btnEdit.style.display = "none";
}

//Function for calculating salary offered with basicpay, allowance and deductions

function calSalaryOffered(ctrl,FromBasicPay)
{
    var txtBasicPay;
    var txtAmt;
    var ddlAllowanceDeduction;
    var hfAddition;
    var dlAllowance;
    var Allowances = 0;
    var Deductions = 0;
    var Amount = 0;
    var SalaryOffered = 0;
    var txtSalary; 
    var ddlPaymentMode;
    var selectedModeValue;
    
    if(FromBasicPay == 1)
    {
        txtBasicPay = document.getElementById(ctrl);
        
        dlAllowance = document.getElementById(txtBasicPay.id.replace(/txtBasicPay/,"dlAllowanceDeduction")); 
    }
    else if(FromBasicPay == 0)
    {         
         dlAllowance = document.getElementById(ctrl.substring(0,ctrl.lastIndexOf("_ctl")));
         
         txtBasicPay = document.getElementById(dlAllowance.id.replace(/dlAllowanceDeduction/,"txtBasicPay"));
    } 
    else
    {
        txtBasicPay = document.getElementById(ctrl.replace(/ddlPaymentMode/,"txtBasicPay"));
        
        dlAllowance = dlAllowance = document.getElementById(ctrl.replace(/ddlPaymentMode/,"dlAllowanceDeduction")); 
    }   
    
    ddlPaymentMode = document.getElementById(txtBasicPay.id.replace(/txtBasicPay/,"ddlPaymentMode"));
    
    txtSalary = document.getElementById(txtBasicPay.id.replace(/txtBasicPay/,"txtSalary"));    
    
    for(var j = 0; j < ddlPaymentMode.length; j++)
    {
        if(ddlPaymentMode[j].selected)
        {
            selectedModeValue = ddlPaymentMode[j].value;
        }
    }
    
    for(var i = 1; i < dlAllowance.rows.length; i++)
    {            
        var k = 0;
        
        while (k < dlAllowance.rows[i].getElementsByTagName("INPUT").length)
        {
            var element = dlAllowance.rows[i].getElementsByTagName("INPUT")[k];
            
            if(element.type == "hidden")
            {
                hfAddition = element; 
            }   
            if(element.type == "text")
            {
                txtAmt = element;
            } 
            k++;
        }
        
        if(hfAddition.value == "False")
        {
            Deductions = Deductions + parseFloat(txtAmt.value == "" ? 0 : txtAmt.value);
        }         
        else if(hfAddition.value == "True")
        {
            Allowances = Allowances + parseFloat(txtAmt.value == "" ? 0 : txtAmt.value);
        }
    }
    
    txtBasicPay.value = txtBasicPay.value.replace(/,/,""); 
    
//    switch(selectedModeValue)/* based on selection of payment mode*/
//    {
//        case "1":        
//            SalaryOffered = ((txtBasicPay.value != ""?  parseFloat(txtBasicPay.value) + parseFloat(Allowances) - parseFloat(Deductions): 0 + parseFloat(Allowances))) * 30;
//                
//            break;
//        case "2":        
//            SalaryOffered = ((txtBasicPay.value != ""?  parseFloat(txtBasicPay.value) + parseFloat(Allowances) - parseFloat(Deductions): 0 + parseFloat(Allowances))) * 4;

//            break;
//        case "3":
//            SalaryOffered = ((txtBasicPay.value != ""?  parseFloat(txtBasicPay.value) + parseFloat(Allowances) - parseFloat(Deductions): 0 + parseFloat(Allowances))) * 2;
//        
//            break;        
//        default:        
            SalaryOffered = ((txtBasicPay.value != ""?  parseFloat(txtBasicPay.value) + parseFloat(Allowances) - parseFloat(Deductions): 0 + parseFloat(Allowances))) * 12;
            
//            break;
//    }   
  //  SalaryOffered = SalaryOffered * 12; // Calculating salary per annum
    
    txtSalary.value = SalaryOffered;
    
    return true;
}

/* Function for confirmation of keeping history of the offer letter*/

function confirmOfferLetter(validationGroup,hfConfirmHistory,IsEdit)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
         Page_ClientValidate('common');
    }
    if(Page_IsValid)
    {  
        var fv = document.getElementById(hfConfirmHistory.substring(0,hfConfirmHistory.lastIndexOf("_")));
        
        if(chkAmount(fv.id,IsEdit,false))
        {
            if(IsEdit)
            {
                var result = confirm('Do you want to maintain history of the offer letter?');
                
                var hfConfirmHistory = document.getElementById(hfConfirmHistory);
                
                if(result)
                {
                   hfConfirmHistory.value = 1;
                   return true;
                }
                else
                {
                   hfConfirmHistory.value = 0;
                   return true;  
                }
            }
            else
            {
                return true;
            }  
        }   
        else
        {
            return false;
        }   
    }
}


function confirmLeave(btnSave) 
{


    var status = false;
    Page_ClientValidate('Submit')
    if (Page_IsValid)
        status = true;
    var hfConfirm = document.getElementById(btnSave.id.replace(/btnSubmit/, "hfConfirm"));
    var hfdHoliday = document.getElementById(btnSave.id.replace(/btnSubmit/, "hfdHoliday"));
   var hidControl = document.getElementById("ctl00_hidCulture");

    if (hfdHoliday.value == "1") {

//        var result = confirm('Holiday(s) exist in between the period.Do you wish to Continue?')


        var result =confirm(hidControl.value == "ar-AE" ?' عطلة (ق) موجودة في بين الفترة. هل ترغب في الاستمرار؟':'Holiday(s) exist in between the period.Do you wish to Continue?');
            
        if (result) {
            hfConfirm.value = 1;
            return true;
        }
        else {
            hfConfirm.value = 0;
            return true;
        }
     
     
//         if (confirm('Holiday(s) exist in between the period.Do you wish to consider those as holiday(s)?')) {

//             status= true;
//         }
//         else {
//             status= false;
//         }
    }
    else
        return status ;

}

function confirmLeaveHolidays(btnApprove) {

    var hfChkHoliday = document.getElementById(btnApprove.id.replace(/btnApprove/, "hfChkHoliday"));
    var hfConfirmHoliday = document.getElementById(btnApprove.id.replace(/btnApprove/, "hfConfirmHoliday"));
    var hidControl = document.getElementById("ctl00_hidCulture");

    if (hfChkHoliday.value == "1") {

        var result = confirm(hidControl.value == "ar-AE" ?' عطلة (ق) موجودة في بين الفترة. هل ترغب في الاستمرار؟':'Holiday(s) exist in between the period.Do you wish to Continue?');
        if (result) {
            hfConfirmHoliday.value = 1;
            return true;
        }
        else {
            hfConfirmHoliday.value = 0;
            return true;
        }
    }

}




function checkAmount(fv, HasSent, IsSave)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate('Save');
        Page_ClientValidate('common');
        }
        
    if(Page_IsValid)
        return chkAmount(fv,HasSent,IsSave);
}

function chkAmount(fv,HasSent,IsSave)
{
    var datalist = document.getElementById(fv + "_dlAllowanceDeduction");
    
    var bFlag = true;
        
    for(var i = 1; i < datalist.rows.length; i++)
    {
        var hfDeductionPolicy = datalist.rows[i].getElementsByTagName("INPUT")[0];
        var txtAmt = datalist.rows[i].getElementsByTagName("INPUT")[1];
        
        if(hfDeductionPolicy.value == "" || hfDeductionPolicy.value == 'False')
        {   
            if(txtAmt.style.display == "block")
            {
                if(txtAmt.value == "" || parseFloat(txtAmt.value) == 0)
                {
                    txtAmt.value = "";
                    
                    bFlag = false;
                }
            }                
        }
    }
    
    if(!bFlag)
    {
        alert('Please enter amount.');
                
        return false;
    }
    else
    {
        if(HasSent,IsSave)
        {
            var result = confirm('Do you want to maintain history of the offer letter?');
                
            var hfConfirmHistory = document.getElementById(datalist.id.replace(/dlAllowanceDeduction/,"hfConfirmHistory"));
            
            if(result)
            {
               hfConfirmHistory.value = 1;
               return true;
            }
            else
            {
               hfConfirmHistory.value = 0;
               return true;  
            } 
        } 
        else
        {
            return true;
        }                  
    }
}

function setupNavigation()
{
   
    var nav = document.getElementById("nav");
                        
    var list = document.getElementsByTagName("li");
    
    for(var i = 0; i < list.length; i++)
    {
        list[i].id = null;
    }
    
    var links = nav.getElementsByTagName("a");
    
    for(var i = 0; i < links.length; i++)
    {
        if(document.location.toString().lastIndexOf(links[i].href) != -1)
        {
            document.cookie = "header=" + links[i].href;
        }
    }
    
    var header = document.cookie.split(';');
    var href = "";
    
    for(var i = 0; i < header.length; i++)
    {
        if(header[i].split('=')[0] == "header")
        {
            href = header[i].split('=')[1];
        }   
    }
    
    for(var i = 0; i < links.length; i++)
    {
        if(href.lastIndexOf(links[i].href) != -1)
        {
            links[i].parentNode.id = "current";
        }
    }
}
/* Function addMailAttachments() and removeSendMailAttachments(id) added by Megha */
function addMailAttachments()
{       
    var divFiles = document.getElementById("divFiles");
    
    var filAttachment = document.getElementById('ctl00_public_content_filAttachment');
    
    var btnAttachFiles = document.getElementById(filAttachment.id.replace(/filAttachment/, "btnAttachFiles"));   
     
    var attachCount = document.getElementById(filAttachment.id.replace(/filAttachment/, "hdAttachCount"));    
    
    if(filAttachment.value != "")
    {            
            lnkAddMoreAttachments.Enabled = true;            
           
            attachCount.value = parseInt(attachCount.value) + 1;
            
            var newFilAttachment = filAttachment.cloneNode(true);
               
            newFilAttachment.id = filAttachment.id + (new Date());
            
            if(parseInt(attachCount.value) <= 5)
            {
                if(navigator.appName != 'Microsoft Internet Explorer')
                {
                    newFilAttachment.value = ""; 
                }
                    
                var delNewFilAttachment = document.createElement("a");
                    
                delNewFilAttachment.id = "del" + newFilAttachment.id;
                    
                if(delNewFilAttachment.innerText == undefined) { delNewFilAttachment.textContent = "Remove"; }
                else { delNewFilAttachment.innerText = "Remove"; }                
                  
                delNewFilAttachment.href = "javascript:removeSendMailAttachments('" + delNewFilAttachment.id + "')";            
                
                divFiles.appendChild(newFilAttachment);
                divFiles.appendChild(delNewFilAttachment);
            }
    }
    else
    {
        lnkAddMoreAttachments.Enabled = false;           
    }
}

function removeSendMailAttachments(id)
{
    var delNewFilAttachment = document.getElementById(id);
    var newFilAttachment = document.getElementById(id.replace(/del/, ''));
    var delCount = document.getElementById('ctl00_public_content_hdAttachCount');
    var divFiles = delNewFilAttachment.parentNode;
    
    divFiles.removeChild(delNewFilAttachment);
    divFiles.removeChild(newFilAttachment);
    if(delCount.value > 5)
    {
        delCount.value = 5;
    }    
    delCount.value = parseInt(delCount.value) - 1;       
}

function addAttachments(ancAdd)
{    
    
    var divFiles = document.getElementById("divFiles");
    
    var ancAdd = document.getElementById(ancAdd);
    
    var filAttachment = document.getElementById(ancAdd.id.replace(/ancAddAttachment/,"filAttachment"));
    
    var newFilAttachment = filAttachment.cloneNode(true);
    
    newFilAttachment.id = filAttachment.id + (new Date());
    
    var delNewFilAttachment = document.createElement("a");
    
    delNewFilAttachment.id = "del" + newFilAttachment.id;
    
    if(delNewFilAttachment.innerText == undefined) { delNewFilAttachment.textContent = "Remove"; }
    else { delNewFilAttachment.innerText = "Remove"; }
    
    delNewFilAttachment.href = "javascript:removeAttachments('" + delNewFilAttachment.id + "')";
    
    divFiles.appendChild(newFilAttachment);
    divFiles.appendChild(delNewFilAttachment);
}

function removeAttachments(id)
{
    var delNewFilAttachment = document.getElementById(id);
    var newFilAttachment = document.getElementById(id.replace(/del/, ''));
    
    var divFiles = delNewFilAttachment.parentNode;
    
    divFiles.removeChild(delNewFilAttachment);
    divFiles.removeChild(newFilAttachment);
}
/* Function for checking allowance deduction duplication*/

function chkDuplicateAllowanceDeduction(ddlAllowanceDeduction)
{
    var dlAllowanceDeductions = document.getElementById(ddlAllowanceDeduction.id.substring(0,ddlAllowanceDeduction.id.lastIndexOf('_ctl')));
    var hidControl = document.getElementById("ctl00_hidCulture");
    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");    
   
    var bDuplicate = false;    
    
    for(var i = 0; i < ddlAllowanceDeduction.length; i++)
    {
        if(ddlAllowanceDeduction[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if(controls[j] != ddlAllowanceDeduction)
                {
                    if(ddlAllowanceDeduction[i].value == controls[j].value)
                    {
                        bDuplicate = true;                     
                    }
                }
            }
        }
    }           
    if(bDuplicate)
    {
        alert(hidControl.value == 'ar-AE' ? 'يوجد قيمة مكررة.' : 'Duplicate value exists.');
        ddlAllowanceDeduction.selectedIndex = 0;
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + ddlAllowanceDeduction.id + "\',\'\')", 0);
        return true;
    }
}

/*------------------------------------------------------------------------------------------------------------------------------*/
//Function for setting the initial tab selected on validation error (company.aspx)

function setTabResumeSelected(validationGroup)
{
     var id;
     if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
     if(Page_IsValid)        
    { 
        return true;
    }
    else
    {
         id = "pnlTab1"
         SetResumeTab(id);
   }
    
}
function ValEmail(source,args)
{
    var secEmail = document.getElementById(source.controltovalidate);
    var priEmail = document.getElementById(secEmail.id.replace(/txtSecondaryEmail/,"txtPrimaryEmail"));
    
    if(secEmail.value == priEmail.value)
        args.IsValid = false;
    else
        args.IsValid = true;
}

function SetCompanyTab(id)
{
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
    var divTab4 = document.getElementById('divTab4');
   
    divTab1.style.display = divTab2.style.display = divTab3.style.display = divTab4.style.display = "none" ;
    
    var pnlTab1 = document.getElementById('pnlTab1');
    var pnlTab2 = document.getElementById('pnlTab2');
    var pnlTab3 = document.getElementById('pnlTab3');
    var pnlTab4 = document.getElementById('pnlTab4');
    
    pnlTab1.className = pnlTab2.className = pnlTab3.className = pnlTab4.className = "TabbedPanelsTab";  
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
}

function SetJobTab(id)
{
   var div = document.getElementById(id.substring(id.length,id.lastIndexOf("_") + 1).replace(/pnl/,"div"));
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
    var divTab4 = document.getElementById('divTab4');
    var divTab5 = document.getElementById('divTab5');
   
      var table = document.getElementById('tblSubmit');
    
    if(tab.id.substring(tab.id.lastIndexOf("_") + 1) == "pnlTab6")
        { table.style.display = "none";}
       else
        { table.style.display = trDisplay();}        
     
    divTab1.style.display = divTab2.style.display = divTab3.style.display = divTab4.style.display = divTab5.style.display = "none" ;
    
    var pnlSelected = id.substring(0,id.lastIndexOf("_") + 7);
    for(var i = 1; i <= 5; i++)
    {
        var pnlTab = document.getElementById(id.substring(0,id.lastIndexOf("_") + 7) + i);
        
        pnlTab.className = "TabbedPanelsTab";  
    }  
       
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
}

function SetVacancyTab(id)
{
   var div = document.getElementById(id.substring(id.length,id.lastIndexOf("_") + 1).replace(/pnl/,"div"));
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
       
      var table = document.getElementById('tblSubmit');
    
    if(tab.id.substring(tab.id.lastIndexOf("_") + 1) == "pnlTab3")
        { table.style.display = "none";}
       else
        { table.style.display = trDisplay();}        
     
    divTab1.style.display = divTab2.style.display = "none" ;
    
    var pnlSelected = id.substring(0,id.lastIndexOf("_") + 7);
    for(var i = 1; i <= 2; i++)
    {
        var pnlTab = document.getElementById(id.substring(0,id.lastIndexOf("_") + 7) + i);
        
        pnlTab.className = "TabbedPanelsTab";  
    }  
       
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
}

function SetResumeTab(id)
{
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
   
    divTab1.style.display = divTab2.style.display = divTab3.style.display = "none" ;
    
    var pnlTab1 = document.getElementById('pnlTab1');
    var pnlTab2 = document.getElementById('pnlTab2');
    var pnlTab3 = document.getElementById('pnlTab3');
    
    pnlTab1.className = pnlTab2.className = pnlTab3.className = "TabbedPanelsTab";  
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
}

function setVacancyPostTab(id)
 {
    var div = document.getElementById(id.substring(id.length,id.lastIndexOf("_") + 1).replace(/pnl/,"div"));
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');    
    
    var table = document.getElementById('tblSubmit');
    
    if(tab.id.substring(tab.id.lastIndexOf("_") + 1) == "pnlTab3")
        { table.style.display = "none";}
       else
        { table.style.display = trDisplay();}        
     
    divTab1.style.display = divTab2.style.display = "none" ;
    
    var pnlSelected = id.substring(0,id.lastIndexOf("_") + 7);
    for(var i = 1; i <= 2; i++)
    {
        var pnlTab = document.getElementById(id.substring(0,id.lastIndexOf("_") + 7) + i);
        
        pnlTab.className = "TabbedPanelsTab";  
    }  
       
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";   
 }

 function setJobPostTab(id)
 {
    var div = document.getElementById(id.substring(id.length,id.lastIndexOf("_") + 1).replace(/pnl/,"div"));
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
    var divTab4 = document.getElementById('divTab4');
    var divTab5 = document.getElementById('divTab5');
    var divTab6 = document.getElementById('divTab6');
    
    var table = document.getElementById('tblSubmit');
    
    if(tab.id.substring(tab.id.lastIndexOf("_") + 1) == "pnlTab7")
        { table.style.display = "none";}
       else
        { table.style.display = trDisplay();}        
     
    divTab1.style.display = divTab2.style.display = divTab3.style.display =
    divTab4.style.display = divTab5.style.display = divTab6.style.display = "none" ;
    
    var pnlSelected = id.substring(0,id.lastIndexOf("_") + 7);
    for(var i = 1; i <= 6; i++)
    {
        var pnlTab = document.getElementById(id.substring(0,id.lastIndexOf("_") + 7) + i);
        
        pnlTab.className = "TabbedPanelsTab";  
    }  
       
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
} 

function setVisaTicketTab(id)
{
     var div = document.getElementById(id.substring(id.length,id.lastIndexOf("_") + 1).replace(/pnl/,"div"));
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
        
    var table = document.getElementById('tblSubmit');
    
    if(tab.id.substring(tab.id.lastIndexOf("_") + 1) == "pnlTab3")
        { table.style.display = "none";}
       else
        { table.style.display = trDisplay();}        
     
    divTab1.style.display = divTab2.style.display = "none" ;
    
    var pnlSelected = id.substring(0,id.lastIndexOf("_") + 7);
    for(var i = 1; i <= 2; i++)
    {
        var pnlTab = document.getElementById(id.substring(0,id.lastIndexOf("_") + 7) + i);
        
        pnlTab.className = "TabbedPanelsTab";  
    }  
       
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
}

// Function for toggle div in interview process candidate level tab
function toggleTabDiv(divId)
{
    var hfToggleDiv = document.getElementById(divId.replace(/div/,"hf"));
    
    if(hfToggleDiv.value == "0")
    {        
        hfToggleDiv.value = "1";
        return true;
    }
    else
    {
        hfToggleDiv.value = "0";
        
        SlideUpSlideDown(divId, 500);
        return false;
    }
}
function dispSearchDiv()
{ 
    var rbtnBasicSearch= document.getElementById('rbtnBasicSearch');
    
    var trDepartment = document.getElementById('trDepartment');
    
    var  trVacancy = document.getElementById('trVacancy');
    
    var trAdvacnceSearch = document.getElementById('trAdvacnceSearch');
    
    var hfSearch=document.getElementById('ctl00_ctl00_content_public_content_hfSearch');
     
    var divSearchResult =document.getElementById('divSearchResult');
  
    if(divSearchResult!=null){divSearchResult.style.display='none';}
    
    if(rbtnBasicSearch.checked)
    {
         trDepartment.style.display=trVacancy.style.display = trDisplay();
         trAdvacnceSearch.style.display="none";             
         
         hfSearch.value='1'; // 1 for basic search
    }
    else
    {
        trDepartment.style.display=trVacancy.style.display="none";            
        trAdvacnceSearch.style.display = trDisplay();
      
        hfSearch.value='2'; // 2 for advance search
    }  
}

// SElect nodes and ites child nodes
function SelectAllChildNodes(e)
{
    var obj = (e.srcElement == undefined ? e.target : e.srcElement);
    var treeNodeFound = false;

    var checkedState;
    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
        var treeNode = obj;
        checkedState = treeNode.checked;
        do
        {
            obj = obj.parentNode;
        } while (obj.tagName != "TABLE")
        
        var parentTreeLevel = obj.rows[0].cells.length;            
        var parentTreeNode = obj.rows[0].cells[0];
        var tables = obj.parentNode.getElementsByTagName("TABLE");
        var numTables = tables.length;
        if (numTables >= 1)
        {
            for (iCount=0; iCount < numTables; iCount++)
            {
                if (tables[iCount] == obj)
                {
                    treeNodeFound = true;
                    iCount++;
                    if (iCount == numTables)
                    {
                        return;
                    }
                }
                if (treeNodeFound == true)
                {
                    var childTreeLevel = tables[iCount].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel)
                    {
                        var cell = tables[iCount].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }
}
function valDelHierarchy()
{
    var arrCheckBox = document.getElementsByTagName("input");
    
    var isChecked = false;
    
    for(var i = 0; i < arrCheckBox.length; i++)
    {
        if(arrCheckBox[i].type == "checkbox" && arrCheckBox[i].id.lastIndexOf("tvOrganizationalHierarchy") != -1)
        {
            if(arrCheckBox[i].checked)
            {
                isChecked = true;
            }
        }
    }
    
    if(isChecked)
    {
        return confirm('All the child hierarchies of the selected designations will be deleted.\rAre you sure to delete the selected designations?');
    }
    else
    {
        alert('Please select a designation to remove');
        return false;
    }
}
function ValCandidatePage(validationGroup)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    { 
        if((!isDoc('ctl00_ctl00_content_public_content_fuResumeAttach_ctl02')) || (!isImage('ctl00_ctl00_content_public_content_fuRecentPhoto_ctl02')))
        {
            alert('Invalid file type');  
            return false;                          
        }
        else
        {
            return true; 
        }  
    }
    else
    {
         for (i = 0; i < Page_Validators.length; i++) 
         {
             if(!Page_Validators[i].isvalid)
             {
               if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
                  SetCandidateTab("pnlTab2");                
               else
                 SetCandidateTab("pnlTab1");
                
                // (Page_Validators[i].id!='ctl00_ctl00_content_public_content_fvCandidate_rfvddlQualification' ? SetCandidateTab('pnlTab1'):SetCandidateTab('pnlTab2'));
                 break;
             }          
         }
    }
}

function SetPolicyTab(id)
{
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divpTab1');
    var divTab2 = document.getElementById('divpTab2');
  
    divTab1.style.display = divTab2.style.display  = "none" ;
    
    var pnlTab1 = document.getElementById('pnlpTab1');
    var pnlTab2 = document.getElementById('pnlpTab2');
     
    pnlTab1.className = pnlTab2.className = "TabbedPanelsTab";  
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";
}

function SetSalaryTab(id) {
    var div = document.getElementById(id.replace(/pnl/, 'div'));
    var tab = document.getElementById(id);

    var divTab1 = document.getElementById('divpTab1');
    var divTab2 = document.getElementById('divpTab2');

    divTab1.style.display = divTab2.style.display = "none";

    var pnlTab1 = document.getElementById('pnlpTab1');
    var pnlTab2 = document.getElementById('pnlpTab2');

    pnlTab1.className = pnlTab2.className = "TabbedPanelsTab";

    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";
} 

function SetLeavePolicyTab(id)
{
    var div1,div2,tab1,tab2;
  
    if(id.lastIndexOf('pnllTab2')!= -1)
    {
        div2 = document.getElementById(id.replace(/pnllTab2/,'divlTab2'));
        div1 = document.getElementById(div2.id.replace(/divlTab2/,'divlTab1'));
        tab2 = document.getElementById(id);
        tab1 = document.getElementById(id.replace(/pnllTab2/,'pnllTab1'));
        div1.style.display= div2.style.display="none";
        div2.style.display = tab2.style.display = "block";
        tab2.className = "TabbedPanelsTabSelected";  
        tab1.className = "TabbedPanelsTab";     
    }
    else
    {
        div1 = document.getElementById(id.replace(/pnllTab1/,'divlTab1'));
        div2 = document.getElementById(div1.id.replace(/divlTab1/,'divlTab2'));
        tab1 = document.getElementById(id);
        tab2 = document.getElementById(id.replace(/pnllTab1/,'pnllTab2'));
        div1.style.display= div2.style.display="none";
        div1.style.display = tab1.style.display = "block";
        tab1.className = "TabbedPanelsTabSelected";  
        tab2.className = "TabbedPanelsTab";     
    }
} 

function collapseEllapse(ancid)
{
 var anc= document.getElementById(ancid);
 var div = document.getElementById(anc.id.replace(/anc/, "div"));
 var img = document.getElementById(anc.id.replace(/anc/, "img"));
 
 img.src=(div.style.display =="block" ? '../images/Hide.png':'../images/Show.png');  
 SlideUpSlideDown(div.id,500);
 
}

function valRelatedDepartments(source,args)
{

    var isValid=false;
    var arrCheckBox = document.getElementsByTagName("input");
    
    for(var i = 0; i < arrCheckBox.length; i++)
    {
        if(arrCheckBox[i].type == "checkbox" && arrCheckBox[i].id.lastIndexOf("chkRelatedDepartments") != -1)
        {
            if(arrCheckBox[i].checked)
            {
               isValid = true;
            }
        }
    }
     args.IsValid = isValid;
}

function Display(btn)
{
     var rbtn = document.getElementById(btn);  
     var divfirst,divsecond;
     if(rbtn.id.lastIndexOf("rblAccommodation") != -1)
     {
        divfirst = document.getElementById('divfirst');
        divsecond = document.getElementById('divsecond');
     }
    else
    {
        divfirst = document.getElementById('divthird');
        divsecond = document.getElementById('divfourth');
    }  
     var div = document.getElementById(rbtn.id.replace(/rbl/,"div"));
     
     if(rbtn.cells[0].childNodes[0].checked)
     {
        div.style.display = "block";
        divfirst.style.height='89px';
        divsecond.style.height='89px';
     }        
     else
     {
        div.style.display = "none";
        divfirst.style.height='29px';
        divsecond.style.height='29px';
     }
     //return true;
}
function DisplayTextbox(btn,mode)
{
        var rbtn = document.getElementById(btn);  
        var divfirst,divsecond;
        var div = document.getElementById(rbtn.id.replace(/rbl/,"div"));
        var radioAccomodationYes;
        var radioAccomodationNo;
        if(mode==1)
        {
            var radioAccomodationYes=document.getElementById('ctl00_ctl00_content_public_content_fvPostJobs_rblAccommodation_0');
            var radioAccomodationNo =document.getElementById('ctl00_ctl00_content_public_content_fvPostJobs_rblAccommodation_1');
        }
        else
        {
            var radioAccomodationYes=document.getElementById('ctl00_ctl00_content_public_content_fvPostJobs_rblVisaProcessingDetails_0');
            var radioAccomodationNo =document.getElementById('ctl00_ctl00_content_public_content_fvPostJobs_rblVisaProcessingDetails_1');
        }

        if (radioAccomodationYes.checked)
        {
            div.style.display = "block";
        }
        else if (radioAccomodationNo.checked)
        {
            div.style.display = "none";
        }
     //return true;
}
function validateLastDate(source,args)
{    
    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    if(!isDate(ctrl.value,source))
    {
         args.IsValid= false;      
    }
    else
    {
        if(Convert2Date(ctrl.value)<currentDate)
        {
            if(source.innerText == undefined) { source.textContent = "Last date should not earlier than today."; }
            else { source.innerText = "Last date should not earlier than today"; }
            args.IsValid= false;
        }
        else
        {
            args.IsValid=true;
        }
    }
}

function valDatalistQualification(source, args)
{
    var a = args;
    
    var lblMinQualification = document.getElementById(source.id.replace('cvdlHighestQualificationpnlTab2', 'lblMinQualification'));
    
    if(lblMinQualification)
        args.IsValid = lblMinQualification.innerHTML.length > 0;
    else
        args.IsValid = true;
}
       
function valAssignedTo(source,args)
{
    var isValid=false;
    var arrCheckBox = document.getElementsByTagName("input");

    for(var i = 0; i < arrCheckBox.length; i++)
    {
        if(arrCheckBox[i].type == "checkbox" && arrCheckBox[i].id.lastIndexOf("chkAssignedTo") != -1)
        {
            if(arrCheckBox[i].checked)
            {
                isValid = true;
            }
        }
    }
    args.IsValid = isValid;
}

function validateStartDate(source,args)
{    
    var hf=document.getElementById(source.id.replace(/cv/,'hf'));
    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString()); 
    if(!isDate(ctrl.value,source))
    {
         args.IsValid= false;      
    }
    else
    {
        if(hf.value=='New')
        {
            if(Convert2Date(ctrl.value)<currentDate)
            {
                if(source.innerText == undefined) { source.textContent = "Start date should not earlier than today."; }
                else { source.innerText = "Start date should not earlier than today"; }
                args.IsValid= false;
            }
            else
            {
                args.IsValid=true;
            }
        }
        else
        {
            args.IsValid=true;
        }
    }
}
function validateEndDate(source,args)
{   
    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());   
    if(!isDate(ctrl.value,source))
    {
         args.IsValid= false;      
    }
    else
    {
        if(Convert2Date(ctrl.value)<currentDate)
        {
            if(source.innerText == undefined) { source.textContent = "End date should not earlier than today."; }
            else { source.innerText = "End date should not earlier than today"; }
            args.IsValid= false;
        }
        else
        {
            args.IsValid=true;
        }  
    }   
}        
function valJobPostPage(validationGroup,button)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    {  
        return true;
    }
    else
    {
     for (i = 0; i < Page_Validators.length; i++) 
     {
        if(!Page_Validators[i].isvalid)
        {
            if(Page_Validators[i].id.lastIndexOf("pnlTab1")!= -1)
            {
                setJobPostTab("ctl00_ctl00_content_public_content_fvPostJobs_pnlTab1");
            }
            else if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
            {
                setJobPostTab("ctl00_ctl00_content_public_content_fvPostJobs_pnlTab2");
            }
            else if(Page_Validators[i].id.lastIndexOf("pnlTab3")!= -1)
            {
                setJobPostTab("ctl00_ctl00_content_public_content_fvPostJobs_pnlTab3");
            }
            else  
            {
                setJobPostTab("ctl00_ctl00_content_public_content_fvPostJobs_pnlTab1");  
            }
            break;
        }
     }            
    }
}
function SetEmployeeTab(id)
{
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
    var divTab4 = document.getElementById('divTab4');
    var divTab5 = document.getElementById('divTab5');
    
    //var divTab6 = document.getElementById('divTab6');
    
    divTab1.style.display = divTab2.style.display = divTab3.style.display = divTab4.style.display = divTab5.style.display = "none" ;//divTab6.style.display=
    
    var pnlTab1 = document.getElementById('pnlTab1');
    var pnlTab2 = document.getElementById('pnlTab2');
    var pnlTab3 = document.getElementById('pnlTab3');
    var pnlTab4 = document.getElementById('pnlTab4');
    var pnlTab5 = document.getElementById('pnlTab5');
      //var pnlTab6 = document.getElementById('pnlTab6');
    
    
    pnlTab1.className = pnlTab2.className = pnlTab3.className = pnlTab4.className = pnlTab5.className = "TabbedPanelsTab";  //pnlTab6.className= 
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
} 



function SetPlanningTab(id,dataListID)
{
    var chk   = dataListID.getElementsByTagName("INPUT");
   
   if(id == "pnlTab3")
   {
        var IsContain = false;
        for(var i = 0; i < chk.length; i++)
        {
           if(chk[i].type == "checkbox" && chk[i].checked == true )
           {
            IsContain = true;
            break;
           }
        }
  
    if(IsContain == false)
    {
        alert("Please select atlease one designation");
        return;
    }

  }
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');
 
 
    
    //var divTab6 = document.getElementById('divTab6');
    
    divTab1.style.display = divTab2.style.display = divTab3.style.display = "none" ;
    
    var pnlTab1 = document.getElementById('pnlTab1');
    var pnlTab2 = document.getElementById('pnlTab2');
    var pnlTab3 = document.getElementById('pnlTab3');
 
    
    //var pnlTab6 = document.getElementById(id.replace(/id.id/,"pnlTab6")) //document.getElementById(id.replace(/divTab1/, "pnlTab6"));
    
    pnlTab1.className = pnlTab2.className =  pnlTab3.className = "TabbedPanelsTab";  
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
} 






function SetEmpSalTab(id,objli)
{

   var div = document.getElementById(id.id.replace(/pnl/,'div')) ; 
   var tab = document.getElementById(objli.id);
 
   var divTab1 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "divTab1");
   var divTab2 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "divTab2");
   var divTab3 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "divTab3");
   var divTab4 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "divTab4");
   var divTab5 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "divTab5");
   
   var pnlTab1 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "pnlTab1");
   var pnlTab2 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "pnlTab2");
   var pnlTab3 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "pnlTab3");
   var pnlTab4 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "pnlTab4");
   var pnlTab5 = document.getElementById(id.id.substr(0, id.id.lastIndexOf('_') + 1) + "pnlTab5");
   
   divTab1.style.display = divTab2.style.display = divTab3.style.display = divTab4.style.display = divTab5.style.display =  "none" ;
     
   pnlTab1.className = pnlTab2.className = pnlTab3.className = pnlTab4.className = pnlTab5.className =  "TabbedPanelsTab";
      
  // objli.className =  "TabbedPanelsTab";  
   div.style.display = "block";
   tab.className = "TabbedPanelsTabSelected";  
}
function setTabSelected(validationGroup,id)
{

    var id;
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)        
    { 
        if(!isImage('ctl00_public_content_fuRecentPhoto_ctl02'))
        {
            alert('Invalid file type');
            return false;
        }
        else
        {        
            return true;
        }
    }
    else
    {
         for (i = 0; i < Page_Validators.length; i++) 
         {
            if(!Page_Validators[i].isvalid)
            {               
                if(Page_Validators[i].id.lastIndexOf("pnlTab3")!= -1)
                {
                    id = "pnlTab3"
                    SetCompanyTab(id);
                }
                else if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
                {
                    id = "pnlTab2"
                    SetCompanyTab(id);
                }
//                else if(Page_Validators[i].id.lastIndexOf("pnlTab1")!= -1 || Page_Validators[i].id == "ctl00_ctl00_content_public_content_fvCompany_rcCountry_req" || Page_Validators[i].id == "ctl00_ctl00_content_public_content_fvCompany_rcCompanyIndustry_req")
//                {
//                    id = "pnlTab1"
//                    SetCompanyTab(id);
//                }
                else 
                {
                    id = "pnlTab1"
                    SetCompanyTab(id);
                }
                break;
            }
        }    
    }
}

function setJobTabSelected(validationGroup,id)
{

    var id;
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)        
    { 
       
    }
    else
    {
         for (i = 0; i < Page_Validators.length; i++) 
         {
            if(!Page_Validators[i].isvalid)
            {               
                if(Page_Validators[i].id.lastIndexOf("pnlTab6")!= -1)
                {
                    //id = "pnlTab6"
                    SetJobTab("ctl00_public_content_pnlTab6");
                }
                else if(Page_Validators[i].id.lastIndexOf("pnlTab5")!= -1)
                {
                    //id = "pnlTab5"
                    SetJobTab("ctl00_public_content_pnlTab5");
                } 
                else if(Page_Validators[i].id.lastIndexOf("pnlTab4")!= -1)
                {
                    //id = "pnlTab4"
                    SetJobTab("ctl00_public_content_pnlTab4");
                }           
                else if(Page_Validators[i].id.lastIndexOf("pnlTab3")!= -1)
                {
                    //id = "pnlTab3"
                    SetJobTab("ctl00_public_content_pnlTab3");
                }
                else if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
                {
                    //id = "pnlTab2"
                    SetJobTab("ctl00_public_content_pnlTab2");
                }

                else 
                {
                    //id = "pnlTab1"
                    SetJobTab("ctl00_public_content_pnlTab1");
                }
                break;
            }
        }    
    }
}

function setVacancyTabSelected(validationGroup,id)
{

    var id;
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)        
    { 
       
    }
    else
    {
         for (i = 0; i < Page_Validators.length; i++) 
         {
            if(!Page_Validators[i].isvalid)
            {               
                if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
                {
                    //id = "pnlTab2"
                    SetVacancyTab("ctl00_public_content_pnlTab2");
                }            
                else 
                {
                    //id = "pnlTab1"
                    SetVacancyTab("ctl00_public_content_pnlTab1");
                }
                break;
            }
        }    
    }
}
function VisaTicketTabSelected(ValidationGroup, id)
{
     var id;
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)        
    { 
       
    }
    else
    {
         for (i = 0; i < Page_Validators.length; i++) 
         {
            if(!Page_Validators[i].isvalid)
            {               
                if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1)
                {
                    //id = "pnlTab2"
                    setVisaTicketTab("ctl00_public_content_pnlTab2");
                }               

                else 
                {
                    //id = "pnlTab1"
                    setVisaTicketTab("ctl00_public_content_pnlTab1");
                }
                break;
            }
        }    
    }
}

function ValidateEmployeePage(validationGroup,id)
{
    var CurrentTabIndex ;
    
    CurrentTabIndex = 5;
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    { 
        if(!isImage('ctl00_public_content_filRecentPhoto_ctl02'))
        {
            alert('Invalid file type');
            return false;
        }
     
        else
        {        
            return true;
        }
    }
    else
    {
     for (i = 0; i < Page_Validators.length; i++) 
     {
        if(!Page_Validators[i].isvalid)
        {
            if(Page_Validators[i].id.lastIndexOf("pnlTab1")!= -1)
            {
               if(1 < CurrentTabIndex)
                    CurrentTabIndex = 1; 
            }
            else if(Page_Validators[i].id.lastIndexOf("pnlTab2")!= -1 )
            {
                 if(2 < CurrentTabIndex)
                     CurrentTabIndex = 2;
             }
              
            else if(Page_Validators[i].id.lastIndexOf("pnlTab3")!= -1 )
            {
                 if(3 < CurrentTabIndex)
                    CurrentTabIndex = 3;
             }
              
            else if(Page_Validators[i].id.lastIndexOf("pnlTab4")!= -1)
            {
                 if(4 < CurrentTabIndex)
                    CurrentTabIndex = 4;
             }
               
            else if(Page_Validators[i].id.lastIndexOf("pnlTab5")!= -1)
            {
                 if(5 < CurrentTabIndex)
                    CurrentTabIndex = 5;
             }
//               else if(Page_Validators[i].id.lastIndexOf("pnlTab6")!= -1)
//            {
//                 if(6 < CurrentTabIndex)
//                    CurrentTabIndex = 6;
//             }
         
            //break;
        }
     }/// nd of for each loop
     
      if(CurrentTabIndex == 1)
        SetEmployeeTab("pnlTab1");
      else if(CurrentTabIndex == 2)
        SetEmployeeTab("pnlTab2");
      else if(CurrentTabIndex == 3)
        SetEmployeeTab("pnlTab3");
      else if(CurrentTabIndex == 4)
        SetEmployeeTab("pnlTab4");
      else if(CurrentTabIndex == 5)
        SetEmployeeTab("pnlTab5");
//       else if(CurrentTabIndex == 6)
//        SetEmployeeTab("pnlTab6");
        
   }
}

/* Calculates difference between two dates in days in project page */
function CalculateWorkDuration(fromDateID, toDateID, targetCtrlID, errorCtrlID)
{
    var txtfromDate = document.getElementById(fromDateID);
    var txttoDate  = document.getElementById(toDateID);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    if(typeof(txtfromDate) != 'undefined' && typeof(txttoDate) != 'undefined')
    {
        if(txtfromDate.value != '' && txttoDate.value != '')
        {
            var fromDateValid = isValidDate(txtfromDate.value);
            var toDateValid = isValidDate(txttoDate.value);
            var fromDate, toDate;
            var errorCtrl = document.getElementById(errorCtrlID);
            
            errorCtrl.style.color = 'Red';
            
            if(fromDateValid) fromDate = Convert2Date(txtfromDate.value);
            else errorCtrl.innerHTML = 'Invalid work start date';
                      
            if(toDateValid) toDate = Convert2Date(txttoDate.value);
            else
            { 
                if(fromDateValid) errorCtrl.innerHTML = 'Invalid work finish date';
            }
            
            var type = typeof(fromDate);
            
            if(typeof(fromDate) != 'undefined' && typeof(toDate) != 'undefined')
            {
                errorCtrl.innerHTML = '';
                
               // MilliSeconds * Seconds * Minutes * Hours
               var one_day = 1000 * 60 * 60 * 24; // total time of day
               var days = Math.ceil( (toDate.getTime() - fromDate.getTime()) / (one_day) );
               
               if(days < 0) // ie negative
                    errorCtrl.innerHTML =   hidControl.value == 'ar-AE' ? 'يجب أن يكون تاريخ بدء العمل مبشرة من تاريخ بدء المشروع!':'Work finish date must be greater than work start date';
               
               document.getElementById(targetCtrlID).value = (days == 0) ? 1 : ((days > 0) ? days + 1 : days);
            }
        }
        else
        {
            document.getElementById(targetCtrlID).value = '';
        }    
    }    
}

function valApproveEntry(datalistId)
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {           
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        return true;
    }
    else
    {
        alert('Please select an entry to approve.');
        return false;
    }
}
// function for job post activity assign
 function valGridAssignJobs(datalistId)    
    {
        var datalist = document.getElementById(datalistId);
        
        var chks = document.getElementsByTagName("INPUT");
       
        var count = 0;
        
        var checked = false;
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox")
            {
                count++;
                
                if(count == 1) { continue; }
                
                if(chks[i].checked && (chks[i].id.lastIndexOf('chkSelect') != -1)) { checked = true; }
            }
        }
        
        if(!checked)
        {
             alert('Please select a task');
            
            return false;
        }
    }
    
    //function for job post activity assign 
function validateAssignedEmployees(btnSubmit)
{
    var chkEmployees = document.getElementById(btnSubmit.id.substring(0, btnSubmit.id.lastIndexOf('_') + 1) + 'chkEmployees');
    
    var chks = document.getElementsByTagName('INPUT');
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            if(chks[i].checked && (chks[i].id.lastIndexOf('chkEmployees') != -1))
            {
                checked = true;
            }
        }
    }
    
    if(checked)
    {
        return confirm('Are you sure to assign these employees to the selected jobs?');
    }
    else
    {
        alert('Please select atleast an employee');
        return false;
    }
}

/* Function for checking financial year start date*/
function CheckFinancialStartDate(source,args)
{
    var radioBranch = document.getElementById('ctl00_ctl00_content_public_content_fvCompany_rbtnCreationMode_0');
    var fDate = document.getElementById(source.controltovalidate);
    
    if(radioBranch.checked)
    {
        var hfFinYear = document.getElementById(source.controltovalidate.replace(/txtFinancialStartDate/,"hfCompanyFinYear"));        
        
        if( Convert2Date(fDate.value) < Convert2Date(hfFinYear.value))
        {
            if(source.innerText == undefined) { source.textContent = "Branch financial year must be greater than or equal to company financial year.";                 }
            else { source.innerText = "Branch financial year must be greater than or equal to company financial year."; } 
        
            args.IsValid = false;
        }
        else
        {
            if(fDate.value == "")
            {
                if(source.innerText == undefined) { source.textContent = "Please enter financial start date."; }
                else { source.innerText = "Please enter financial start date."; }         
                        
                args.IsValid = false;
            }    
            else
            {
                var chkDate = Convert2Date(fDate.value).getDate();
                
                if(chkDate > 1)
                {
                    if(source.innerText == undefined) { source.textContent = "The selected date must be starting date of the month."; }
                    else { source.innerText = "The selected date must be starting date of the month."; }    
                    
                    args.IsValid = false;
                }    
                else
                {
                    if(!isDate(fDate.value,source))
                    {
                        args.IsValid = false;
                    }
                    else
                    {
                        args.IsValid = true;
                    }            
                }
            }           
        }     
    }
    else
    {
        if(fDate.value == "")
        {
            if(source.innerText == undefined) { source.textContent = "Please enter financial start date."; }
            else { source.innerText = "Please enter financial start date."; }         
                    
            args.IsValid = false;
        }    
        else
        {
            var chkDate = Convert2Date(fDate.value).getDate();
            
            if(chkDate > 1)
            {
                if(source.innerText == undefined) { source.textContent = "The selected date must be starting date of the month."; }
                else { source.innerText = "The selected date must be starting date of the month."; }    
                
                args.IsValid = false;
            }    
            else
            {
                if(!isDate(fDate.value,source))
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }            
            }
        } 
    }     
     return args.IsValid;
}

/* Function for checking book start date*/
function CheckBookStartDate(source,args)
{
    var bDate = document.getElementById(source.controltovalidate);
    
    if(bDate.value == "")
    {
        if(source.innerText == undefined) { source.textContent = "Please enter book start date."; }
        else { source.innerText = "Please enter book start date."; } 
               
        args.IsValid = false;
    }    
    else
    {
        var fDate = document.getElementById(bDate.id.replace(/txtBookStartDate/,"txtFinancialStartDate"));
        
        if(Convert2Date(bDate.value) < Convert2Date(fDate.value))
        {
            if(source.innerText == undefined) { source.textContent = "The book start date must be greater than or equal to financial start date."; }
            else { source.innerText = "The book start date must be greater than or equal to financial start date."; } 
            
            args.IsValid = false;
        }    
        else
        {
            if(!isDate(bDate.value,source))
            {
                args.IsValid = false;
            } 
            else
            {
                args.IsValid = true;
            }            
        }
    }
    return args.IsValid;
}

/* Function for checking bank duplication*/

function chkDuplicateBank(ddlBank,Fromddl)
{
    var dlBank,txtAccount,ddlBanks;
    
    dlBank = document.getElementById(ddlBank.id.substring(0,ddlBank.id.lastIndexOf('_ctl')));
    
    if(Fromddl == 1)
    {
        ddlBanks = ddlBank;
        txtAccount = document.getElementById(ddlBanks.id.replace(/ddlBanks/,"txtAccountNo"));
    }
    else
    {
        ddlBanks = document.getElementById(ddlBank.id.replace(/txtAccountNo/,"ddlBanks"));
        txtAccount = ddlBank;
    }
    
    var ddl = dlBank.getElementsByTagName("SELECT");   
    var txt = dlBank.getElementsByTagName("INPUT");
       
    var bDuplicate = false;    
    
    for(var i = 0; i < ddlBanks.length; i++)
    {
        if(ddlBanks[i].selected)
        {
            for(var j = 0; j < ddl.length; j++)
            {                
                if(ddl[j] != ddlBanks)
                {
                    for(var k = 0; k < txt.length; k++)
                    {
                        if(txt[k].type == "text" && txtAccount.value != "" && txt[k] != txtAccount)
                        {
                            if(ddlBanks[i].value == ddl[j].value && txt[k].value == txtAccount.value)
                            {
                                bDuplicate = true; 
                                
                                break;  
                            } 
                        }
                    }                
                }
            }
        }
    }           
    if(bDuplicate)
    {
        alert('Duplicate bank exists.');
        
        ddlBanks.selectedIndex = 0;
        txtAccount.value = ""
        
        return false;
    }
    else
    {
       return true;
    }
}

function ChkSelectioninCheckBoxList(source, args)
{
    var checkAssociates = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_chklstRequestTo');
    
    if(checkAssociates != null)
    {
        var chkList = checkAssociates.getElementsByTagName("input");
        
        for(var i=0;i<chkList.length ;i++)
        {
            if(chkList[i].checked)
            {
                args.IsValid = true;
                return;
            }
        }
    }
    args.IsValid = false;
}

function checkDropdownSelection(ddlID, errorControlID)
{
    
    var ddl = document.getElementById(ddlID);
    var lblErrorControl = document.getElementById(errorControlID);
    if(ddl.options.length > 0)
    {
        if(ddl.options[ddl.selectedIndex].value == "-1")
            lblErrorControl.style.display = 'block';
        else
            lblErrorControl.style.display = 'none';
    }
    else
         lblErrorControl.style.display = 'none';
         
    return (ddl.selectedIndex > 0);
    
}

function ChangeSearchBy(ddlSearchByID, ddlYearID, ddlMonthID, divDateID, txtSearchDateID)
{
    var ddlSearchBy = document.getElementById(ddlSearchByID);
    
    if(ddlSearchBy != null && typeof(ddlSearchBy) != 'undefined')
    {
        var ddlYear = document.getElementById(ddlYearID);
        var ddlMonth = document.getElementById(ddlMonthID);
        var txtSearchDate = document.getElementById(txtSearchDateID);
        var divDate = document.getElementById(divDateID);
        
        txtSearchDate.value = '';

       switch(ddlSearchBy.options[ddlSearchBy.selectedIndex].value) 
       {
            case 's': // search by none.
                ddlYear.style.display = 'none';
                ddlMonth.style.display = 'none';
                divDate.style.display = 'none';
                break;
                
            case 'y': // search by year
                ddlMonth.style.display = 'none';
                divDate.style.display = 'none';
                ddlYear.style.display = 'block';
                break;
                
            case 'm' : // search by month
                divDate.style.display = 'none';
                ddlMonth.style.display = 'block';
                ddlYear.style.display = 'block';
                break;
                
            case 'd': // search by date
                ddlYear.style.display = 'none';
                ddlMonth.style.display = 'none';
                divDate.style.display = 'block';
                break;
            
            default:
                break;
       }
    }
    else
        return true; // allow postback
        
   return false; // block postback
}

function CheckSelection(ddlID, strMessage)
{
    var ddl = document.getElementById(ddlID);
    if(ddl != null && typeof(ddl) != 'undefined')
    {
        if(ddl.selectedIndex == 0)
        {
             alert(strMessage);
             return false;
        }
        else
            return true;   
    }
    else 
        return true;
}

function ShowHidePanels(ddlSourceID, tbloneID, tbltwoID, btnID)
{
    var ddlSource = document.getElementById(ddlSourceID);
    if(ddlSourceID != null && typeof(ddlSourceID) != 'undefined')
    {
        var tblone = document.getElementById(tbloneID);
        var tbltwo = document.getElementById(tbltwoID);
        var btn = document.getElementById(btnID);
        
        if(ddlSource.selectedIndex == 0)
        {
            tblone.style.display = 'block';
            tbltwo.style.display = 'block';
            btn.style.display = 'none';
        }
        else
        {
            tblone.style.display = 'none';
            tbltwo.style.display = 'none';
            btn.style.display = 'block';
        }
    }
}

function setInterviewtypeEnability(ddlActivityType)       //for job post assign job interview type enability
{   
    var txtTotal=document.getElementById(ddlActivityType.id.replace('ddlActivityType','txtTotal'));
     var txtintrwType=document.getElementById(ddlActivityType.id.replace('ddlActivityType','rcInterviewType'));
    var fv = document.getElementById(ddlActivityType.id.substring(0,ddlActivityType.id.lastIndexOf('_')));
     var hf=document.getElementById("hfInterviewType");
    for(var i = 0; i < ddlActivityType.length; i++)
    {
        if(ddlActivityType[i].selected)            
        {
            var rcControl = fv.getElementsByTagName("div")[1];
            var txtActivityName = fv.getElementsByTagName("input")[8];
           
            if(ddlActivityType[i].value == "4")
            {
                hf.value="1";
                SetupReference(rcControl,true); 
                txtTotal.disabled=false;
                txtTotal.className="textbox";
                txtintrwType.disabled = false;
                txtintrwType.className = "dropdownlist_mandatory";
            }
            else
            {
                hf.value="0";
                SetupReference(rcControl,false);
                txtTotal.disabled="disabled";
                txtTotal.value="";
                txtTotal.className="textbox_disabled";
                txtintrwType.disabled = "disabled";
                txtintrwType.className = "dropdownlist_disabled";
                txtintrwType.selectedIndex = 0;

            }
            txtActivityName.value = ddlActivityType[i].text;
            break;
        }
    }
}
function SetupReference(refControl, enable)         //for job post assign job interview type enability
{
    var elements = refControl.getElementsByTagName("INPUT");
    
    for(var i = 0; i < elements.length; i++)
    {
        if(enable)
        {
            elements[i].disabled = false;
        }
        else
        {
            elements[i].disabled = "disabled";
        }
    }
    
    elements = refControl.getElementsByTagName("SELECT");
    
    for(var i = 0; i < elements.length; i++)
    {
        if(enable)
        {
            elements[i].disabled = false;
            elements[i].className = "dropdownlist_mandatory";
        }
        else
        {
            elements[i].disabled = "disabled";
            elements[i].className = "dropdownlist_disabled";
            elements[i].selectedIndex = 0;
        }
    }
}

function SetReferenceControlEnability(fvActivity,enable)
{
     var fv = document.getElementById(fvActivity);
     var rcControl = fv.getElementsByTagName("div")[1];
     
      SetupReference(rcControl, enable);    
}

function setType(ddl)         //for job post assign job 
{  
 var hf=document.getElementById("hfInterviewType");
 var txt=ddl.options[ddl.selectedIndex].text;

 if(ddl.value =="-1")
 {
    hf.value="1";
 }
 else
 {
    hf.value="0";
 }
}
function validateInterviewType(source,args)    
{
//document.getElementById(source.controltovalidate.substring(0,source.controltovalidate.lastIndexOf('_'))).getElementsByTagName("div")[1];.id
  var ddlActivityType= document.getElementById(source.id.replace(/cvInterviewType/,"ddlActivityType"));
  var hf = document.getElementById("hfInterviewType");
  if(hf.value=="0")
  {
     args.IsValid = true;
  }
   else
  {  
    var ddl=document.getElementById(source.controltovalidate.substring(0,source.controltovalidate.lastIndexOf('_'))).getElementsByTagName("div")[1].getElementsByTagName("SELECT")[0];
    if(ddl!=null && ddl.value =="-1")
    {
        if(ddlActivityType.value==4)
        {
            if(source.innerText == undefined) { source.textContent = "Please select interview type"; }
            else { source.innerText = "Please select interview type"; } 
            args.IsValid = false; 
        } 
        else
        {
            args.IsValid = true; 
        }
    }   
     else
     {
        args.IsValid = true;
     }
  }
}

function CheckCompanySelected(imageId)   //job post page
{

  var ibtn =  document.getElementById(imageId);
  //var ddl = document.getElementById(ibtn.id.replace(/ibtnDesignation/, "ddlCompany"));
  
  var ddl = document.getElementById("ctl00_ctl00_content_public_content_fvPostJobs_ddlCompany")
  if(ddl.value != "-1")
   {
        return true;
   }
   else
   {
         alert('Please select a company');
         return false;
   }
}

function calPerformanceScore(rbl)
{
    var score = 0;
    
    var datalist = document.getElementById(rbl.substring(0,rbl.lastIndexOf('dlEvaluation') + 12));
    
    var lblScore = document.getElementById(datalist.id.replace(/dlEvaluation/,"lblScore"));    
    
    for(var k = 0; k < datalist.getElementsByTagName("input").length; k++)
    {
       if(datalist.getElementsByTagName("input")[k].type == "radio")
        {
            if(datalist.getElementsByTagName("input")[k].checked)
            {                        
               score = score + parseInt(datalist.getElementsByTagName("input")[k].value.split("@")[1]);    
            }
        }  
    }                
    if(lblScore.innerText == undefined) { lblScore.textContent = score; }
    else { lblScore.innerText = score; }
}

function SetPercentageEnability(radio)
{
  var txt=document.getElementById(radio.id.substring(0,radio.id.lastIndexOf('_rblSponership'))+'_txtCostPercentage');
  txtRegFee=document.getElementById(txt.id.replace(/txtCostPercentage/,"txtRegFee"));
  var hf=document.getElementById(txt.id.replace(/txtCostPercentage/,"hfSponsership"));
  hf.value=radio.value;
  if(radio.value=="1")
  {
      txt.disabled = false;
      txt.className = "textbox_mandatory";
     
  }
  else
  {
      txt.disabled = "disabled";
      txt.className = "textbox_disabled";
      txt.value = "";
      txtRegFee.disabled = false;
      txtRegFee.className = "textbox";
  }
}
function valPercentage(source,args)
{
   // var hf=document.getElementById("hfSponsership");
    var hf=document.getElementById(source.id.replace(/cvPercentage/,"hfSponsership"));

    if((hf.value=="1") && (document.getElementById(source.controltovalidate).value==""))
    {
         args.IsValid = false;
    }
    else
    {
         args.IsValid = true;
    }
}
function isPercentage(source,args)
{
    
   if(parseFloat(document.getElementById(source.controltovalidate).value)>100)
    {
         args.IsValid = false;
    }
    else
    {
         args.IsValid = true;
    }
}
function disbableRegFee(txt)
{
    txtRegFee=document.getElementById(txt.id.replace(/txtCostPercentage/,"txtRegFee"));
    if(txt.value!='')
    {
      if(parseInt(txt.value)=='100')
      {
          txtRegFee.disabled = "disabled";
          txtRegFee.className = "textbox_disabled";
          txtRegFee.value = "";
      }
      else
      {
         txtRegFee.disabled = false;
         txtRegFee.className = "textbox";
      }
    }
}
function validateTrainingFrom(source,args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    var currentDate=new Date();
    currentDate=new Date(currentDate.getFullYear().toString(),currentDate.getMonth().toString(),currentDate.getDate().toString());
    
    if(ctrl.value=='')
    {
        if(source.innerText == undefined) { source.textContent ="Please enter date."; } else {source.innerText ="Please enter date." ;} 
         args.IsValid= false;
         
    }
    else if(!isDate(ctrl.value,source))
    {
        args.IsValid= false;
    }
    else if(Convert2Date(ctrl.value)<currentDate)
    {
        if(source.innerText == undefined) { source.textContent ="date should not earlier than today."; }
        else { source.innerText = "date should not earlier than today."; }
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }
}
function slide(span)
{
      // change css class
      span.className = (document.getElementById(span.id.replace(/span/,"div")).style.display == 'block' ? 'filter-show' : 'filter-hide');

      SlideUpSlideDown(span.id.replace(/span/,"div"), 'medium'); 
}  

function CheckDefaultValue(Control)
{
    if(Control.value == '') Control.value = '0.00';
    else
    {
        var value = ConvertToMoney(Control.value);
        if (!isNaN(value)) Control.value = value.toString();
        else
            {
                alert('Invalid number');
                Control.focus();
               
            }
    }
}

function ConvertToMoney(sValue)
{
    // first make sure that input string is a valid number.
    if(!isNaN(sValue))
    {
        var number = parseFloat(sValue);
        
        var Rounded = number.toFixed(2);
        var RoundedString = Rounded.toString();
        var length = RoundedString.length;
        var index = RoundedString.indexOf('.');
        if(index == -1)
            RoundedString += '.00';
        else if(index == length - 2)
            RoundedString += '0';
        return RoundedString;
    }
    return sValue;
}

function valPrintEmailCandidateDatalist(datalistId)         //candidate page
{
    var datalist = document.getElementById(datalistId);
    var hidControl = document.getElementById("ctl00_hidCulture");
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        return true;
    }
    else
    {
        var message = hidControl.value == 'ar-AE' ? 'يرجى اختيار مرشح ' : 'Please select atleast a candidate';
        alert(message);
        
        return false;
    }
}

function valDeleteCandidateDatalist(datalistId)
{
    var datalist = document.getElementById(datalistId);
    var hidControl = document.getElementById("ctl00_hidCulture");
    var chks = document.getElementsByTagName("INPUT");
    var message;
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        message = hidControl.value == 'ar-AE' ? 'هل أنت متأكد من حذف المرشحين المختارين؟' : 'Are you sure to delete the selected candidates?';
        return confirm(message);
    }
    else
    {
        message = hidControl.value == 'ar-AE' ? 'يرجى اختيار مرشح ' : 'Please select atleast a candidate to delete';
        alert(message);
        return false;
    }
}
function valDeleteJobDatalist(datalistId)
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    var message;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
       message = hidControl.value  == "ar-AE" ? 'هل أنت متأكد من حذف وظائف المختارة؟ ' :'Are you sure to delete the selected Jobs?';
       return confirm(message);
       // return confirm('Are you sure to delete the selected Jobs?');
    }
    else
    {
        //alert('Please select atleast a Job to delete');
        message = hidControl.value  == "ar-AE" ? 'يرجى اختيار وظيفة لحذفs' :'Please select atleast a Job to delete';
        alert(message);
        return false;
    }
}

/* Function to insert text in editor at cursorposition*/
function CursorPosition(btn)
{
    var ddlTemplates = document.getElementById(btn.replace(/btnAddFields/,"ddlFields"));    
    var fv = document.getElementById(btn.substring(0,btn.lastIndexOf("_")));
    
    if(ddlTemplates.value != "-1")
    {
        var txt = ddlTemplates.value;
        var editPanel = Sys.Extended.UI.HTMLEditor.LastFocusedEditPanel; 
        
        // if the current mode is 'Design'
        if (editPanel != null && editPanel.get_activeMode() == Sys.Extended.UI.HTMLEditor.ActiveModeType.Design) 
        {
            // get the DesignPanel's object
            var designPanel = editPanel.get_activePanel();
            // For 'Undo'
            designPanel._saveContent();
            // insert drop down value at current selection    

            designPanel.insertHTML(txt);

            // Notify Editor about content changed and update toolbars linked to the edit panel
            setTimeout(function() { designPanel.onContentChanged(); editPanel.updateToolbar(); }, 0);
            // Ensure focus in design panel
            designPanel.focusEditor();
        }
    }  
    return false;  
}

 function validateParser(source,args)
{
    if(!isDoc('ctl00_ctl00_content_menu_content_fuParse_ctl02'))
    {
        alert('Invalid file type');  
        return false;                          
    }
    else
    {
       tbl=document.getElementById('tblparse');
       if(tbl!=null)
        tbl.style.display="block";
    }
}
 function validateResumeBank(source,args)
{
   var inputArray=  document.getElementsByTagName("input");
   var txt;
   for(var i=0;i<=inputArray.length;i++)
   {
       if(inputArray[i].id.indexOf("ucResume_fuParse_ctl02")!=-1)
       {
        txt=inputArray[i];      
        break; 
       }
   }
//'ctl00_ctl00_content_menu_content_ucResume_fuParse_ctl02'
    if(!isDoc(txt.id))
    {
        alert('Invalid file type');  
        return false;                          
    }
    
    else
    {    
       var div=document.getElementById('divParse');
       var btn=div.getElementsByTagName("input")[0];
       btn.disabled=false;
    }
}

function enableParse(ddl)
{
    var btn=document.getElementById(ddl.id.replace(/ddlCountry/,'btnParse'));
    btn.disabled=(ddl.value=='-1');
    
}
function displayParse()
{ 
    
    SlideUpSlideDown('divParse','medium');
   
    document.getElementById('ctl00_ctl00_content_menu_content_fuParse_ctl02').style.width='150px';
   // alert(document.getElementById('ctl00_ctl00_content_menu_content_fuParse_ctl02').style.width); 
    return false;
    
}

function validatepassportAttachment(source,args)
{
     //var fuPassport1=document.getElementById('ctl00_ctl00_content_public_content_fuPassport_ctl02');
     var fuPassport1=document.getElementById('ctl00_public_content_fuPassport_ctl02');
    
     if (fuPassport1.value == "")
     {
        args.IsValid = false;
     }         
}
function validateLeaseAgreementAttachment(source,args)
{
     //var fuPassport1=document.getElementById('ctl00_ctl00_content_public_content_fuLeaseAgreement_ctl02');
     var fuLeaseAgreement1=document.getElementById('ctl00_public_content_fuLeaseAgreement_ctl02');
    
     if (fuLeaseAgreement1.value == "")
     {
        args.IsValid = false;
     }         
}
function validateInsuranceAttachment(source,args)
{
     //var fuPassport1=document.getElementById('ctl00_ctl00_content_public_content_fuLeaseAgreement_ctl02');
     var fuLeaseAgreement1=document.getElementById('ctl00_public_content_fuInsurance_ctl02');
    
     if (fuLeaseAgreement1.value == "")
     {
        args.IsValid = false;
     }         
}
function validatevisaAttachment(source,args)
{
//    var fuvisa1=document.getElementById('ctl00_ctl00_content_public_content_fuVisa_ctl02');
    var fuvisa1=document.getElementById('ctl00_public_content_fuVisa_ctl02');
    
     if (fuvisa1.value == "")
     {
        args.IsValid = false;
     }        
}
function validateTradeLicenseAttachment(source,args)
{
    var fufuTradeLicense1=document.getElementById('ctl00_public_content_fuTradeLicense_ctl02');
    
     if (fufuTradeLicense1.value == "")
     {
        args.IsValid = false;
     }        
}

function HideHolidayPopup()
{  

    var trHolidayType = document.getElementById("ctl00_ctl00_content_public_content_trHolidayType"); 
    var trEvent = document.getElementById("ctl00_ctl00_content_public_content_trEventTitle"); 
    var rbEvent = document.getElementById("ctl00_ctl00_content_public_content_rdbEvents"); 
    var  rbHoliday= document.getElementById("ctl00_ctl00_content_public_content_rdbHoliday"); 
    
    rbEvent.checked =  false;
    rbHoliday.checked = true ;
   
    trHolidayType.style.display =  trDisplay(); 
    trEvent.style.display =  "none"; 

    setTimeout("__doPostBack(\'" + ctl00_ctl00_content_public_content_Calendar1.id + "\',\'\')", 0);
   
    $find('mpeBHoliday').hide();
}

function ChangeName(source,args)
{
    var lbl = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "lblFileName"));
    
    var file = source._element.getElementsByTagName("input")[1].value;
    
    var fileName = file.substring(file.lastIndexOf("\\")+1,file.length);
    
    if(lbl.innerText == undefined) { lbl.textContent = fileName; }
    else { lbl.innerText = fileName ; }
}

function ShowDetails(chkBox, detailDivID, ddlFinancianYearID)
{
   var showDetail = false;
   
   if(chkBox.id.indexOf('Detail') > 0)
       showDetail = true;
   
   document.getElementById(detailDivID).style.display = (showDetail) ? 'block' : 'none';
   document.getElementById(ddlFinancianYearID).disabled = showDetail;
   
   var prefix = detailDivID.substring(0, detailDivID.lastIndexOf('_') + 1);
   var txtFromDate = document.getElementById(prefix + 'txtFromDate');
   var txtToDate = document.getElementById(prefix + 'txtToDate');
   
   if(txtFromDate != null) txtFromDate.value = '';
   if(txtToDate != null) txtToDate.value = '';
}

function chkMailID(source,args)
{    
   var txtEmail = document.getElementById(source.id.replace(/cvEmail/,"txtEmail"));
    
    if(txtEmail.value=="")
    {
        if(source.innerText==undefined){source.textContent = "Please enter email.";}
        else{source.innerText ="Please enter email."; }           
        
        args.IsValid = false;
    }
    else
    {
        var validMailId = txtEmail.value.replace(/;/g, ',').split(/,/);
        
        var inValidId = false;
        
        for(var i=0;i<validMailId.length;i++)
        {
            if(!isEmail(validMailId[i]))
            {
                inValidId = true;
            }
        }
        if(inValidId)
        {
            if(source.innerText == undefined) { source.textContent = "Please enter valid email"; }
            else { source.innerText = "Please enter valid email"; }
            
            args.IsValid = false;
        }
        else
            args.IsValid = true;
    }
}
function CheckDuplicateOption(source,args)                  //performance questionaire
{
    var dlOption=source.id.replace(/cvOption/,'dlOption');
    var controls=document.getElementsByTagName("INPUT");
    var bDuplicate=false;
    var txtOption;
    
    for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('txtOption') != -1)
            {
                txtOption=controls[i];
               
               if(controls[i].value != "")
               {
                    for(var j = (i + 1); j < controls.length; j++)
                    {
                        if(controls[j].id.lastIndexOf('txtOption') != -1)
                        {
                            if(controls[i].value == controls[j].value)
                            {
                            args.IsValid=false;
                            }
                        }
                    }
                }
            }
        }
}

function ToggleFilter(divID, ddlSearch)
{
    var divDateRange = document.getElementById(divID);
    if(divDateRange != null)
    {
        var ddlSearch = document.getElementById(ddlSearch);
        if(ddlSearch != null)
        {
            divDateRange.style.display = ddlSearch.options[ddlSearch.selectedIndex].value == 'DR' ? 'block' : 'none';
            var txtFromDate = document.getElementById(divID.replace('divDateRange', 'txtFromDate'));
            var txtToDate = document.getElementById(divID.replace('divDateRange', 'txtToDate'));
            if(txtFromDate != null) txtFromDate.value = '';
            if(txtToDate != null) txtToDate.value = '';
        }
    }
    
}

function SplitterToggle(currentDivId)
{
    HideIfRequired(currentDivId,'divJobTray');
    HideIfRequired(currentDivId,'divAnnouncement');
   
    
    var currdiv = document.getElementById(currentDivId);
    
    if(currdiv != null)
    {
        var img = document.getElementById(currentDivId.replace(/div/, "img"));
        
        if(currdiv.style.display == 'none')
             img.src = '../images/Show.png' 
        else
             img.src = '../images/Hide.png' 
            
       SlideUpSlideDown(currentDivId, 'medium');    
    }
}

function HideIfRequired(currDivId, toggleDivID)
{
    if(currDivId != toggleDivID)
    {
        HideElement(toggleDivID);
        ChangeImage(toggleDivID.replace(/div/, 'anc'));
    }
}

function HideElement(ctrlID, currentElement)
{
    var element = document.getElementById(ctrlID);
    
    if(element != null)  element.style.display = 'none';
}

function ShowElement(ctrlID)
{
    var element = document.getElementById(ctrlID);
    
    if(element != null)  element.style.display = 'block';
}

function ChangeImage(anchID)
{
    var anc= document.getElementById(anchID);
    var div = document.getElementById(anc.id.replace(/anc/, "div"));
    var img = document.getElementById(anc.id.replace(/anc/, "img"));
 
    img.src=(div.style.display == 'none') ? '../images/Hide.png':'../images/Show.png';  
}

 function valDeleteDatalistSettings(datalistId)                    //user settings hrpower
    {
        var datalist = document.getElementById(datalistId);
        var hidControl = document.getElementById("ctl00_hidCulture");
        var chks = document.getElementsByTagName("INPUT");
        var message;
        var count = 0;
        
        var checked = false;
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox")
            {
                count++;
                
                if(count == 1) { continue; }
                
                if(chks[i].checked && (chks[i].id.lastIndexOf('chkUserSettings') != -1)) { checked = true; }
            }
        }
        
        if(checked)
        {
             message = hidControl.value  == "ar-AE" ? 'هل أنت متأكد من حذف العناصر المحددة؟ ' :'Are you sure to delete the selected items?';
             return confirm(message);
        }
        else
        {
            message = hidControl.value  == "ar-AE" ? 'يرجى تحديد العنصر المراد ازالته ' :'Please select an item to remove';
            alert(message);
            
            return false;
        }
    }

function Test()
{
    var myTimeSpan = new TimeSpan('5:00:00PM', '5:30:00AM');
    
    alert(myTimeSpan.Hour);
}

function GetTimeDifference(date1, date2)
{
}

function checkCandidatePercentage(source,args)
{
 var txt =document.getElementById(source.controltovalidate);
 if (parseFloat(txt.value) > 100)
 {
 args.IsValid=false;
 }
 else
 args.IsValid=true ;
 
}

function chekPerformance(source,args)
{
    var hdnType = document.getElementById('hdnType');
    var hdnMark = document.getElementById('hdnMark');
     
    var _value = 100;
    var mark = 0;
    if(hdnType) {
        if(hdnType.value == '2') // Mark
         {
            if(! (isNaN(hdnMark.value)) ) {
                mark = parseInt(hdnMark.value);
                
                source.innerHTML = 'Mark should not exceed maximum mark :' + mark;

                if(mark > 0)
                    _value = mark;
            }
         }
         else
             source.innerHTML = 'Percentage should not exceed 100';
    }
    var txt=document.getElementById(source.controltovalidate);
//    var lbl= document.getElementById(source.controltovalidate.replace(/txtPerformance/,'lblPerformanceType'));
//    if(lbl==null)
//        args.IsValid=true;
    if(parseFloat(txt.value) > _value)
    {
       args.IsValid=false;
    }
    else
         args.IsValid=true;
}
function toggleMenu(divId)
{
    var img = document.getElementById(divId.replace(/div/, "img"));

    var div = document.getElementById(divId);       

    img.src = (div.style.display == "block" ? "../images/down.png" : "../images/up.png");

    var tbl= document.getElementById(divId.replace(/div/, "tbl"));     

    var tr=tbl.parentNode.parentNode;

    tr.className=(tr.className=="activerow"?"inactiverow":"activerow");

    SlideUpSlideDown(div.id, 'medium');    
}
function selectAllModules(chkAllId, chkId)
{
    var datalistId, startIndex;
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
            
    var chks = document.getElementById(datalistId).getElementsByTagName("INPUT");
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
        {
            chks[i].checked = document.getElementById(chkAllId).checked;
            selectAllMenus(chks[i].id,'chkIsEnabled');
        }
    }
}
function selectAllMenus(chkModuleId, chkId)
{
    var datalistId, startIndex;
    var datalistId=chkModuleId.replace(/chkModules/,"dlMenus");    
  var datalist= document.getElementById(datalistId)
      if(datalist!=null)
      {      
        var chks =datalist.getElementsByTagName("INPUT");
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
            {
                chks[i].checked = document.getElementById(chkModuleId).checked;
            }
        }
    }
}

function selectAllModules2(chkAllId, chkId)
{
    var datalistId, startIndex;
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
            
    var chks = document.getElementById(datalistId).getElementsByTagName("INPUT");
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
        {
            chks[i].checked = document.getElementById(chkAllId).checked;
            selectAllMenus2(chks[i].id);
        }
    }
}
function selectAllMenus2(chkModuleId)
{
    var datalistId, startIndex;
    var datalistId=chkModuleId.replace(/chkModules/,"dlMenus");    
  var datalist= document.getElementById(datalistId)
      if(datalist!=null)
      {      
        var chks =datalist.getElementsByTagName("INPUT");
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox")
            {
                chks[i].checked = document.getElementById(chkModuleId).checked;
            }
        }
    }
}
function selectAllPhases(chkAllId, chkId)
{
    var datalistId, startIndex;
    
    //datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('_ctl'));
    
    datalistId = chkAllId.substring(0, chkAllId.lastIndexOf('chkSelectAll')) + "dlPhases";
            
    var chks = document.getElementById(datalistId).getElementsByTagName("INPUT");
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
        {
            chks[i].checked = document.getElementById(chkAllId).checked;
            selectAllQuestions(chks[i].id,'chkIsEnabled');
        }
    }
}
function selectAllQuestions(chkModuleId, chkId)
{
    var datalistId, startIndex;
    var datalistId=chkModuleId.replace(/chkPhases/,"dlQuestions");    
    var datalist= document.getElementById(datalistId)
      if(datalist!=null)
      {      
        var chks =datalist.getElementsByTagName("INPUT");
        
        for(var i = 0; i < chks.length; i++)
        {
            if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
            {
                chks[i].checked = document.getElementById(chkModuleId).checked;
            }
        }
    }
}

function ProjectValidation() {
    var d = document;
    var errFound = false;
    var tbl = d.getElementById('tblTeamAdd');
    if(tbl != null) {
        var ddlEmployee = tbl.getElementsByTagName('SELECT')[0];
        if(ddlEmployee != null) {
            var lblEmployeeErr = d.getElementById(ddlEmployee.id.replace('ddlEmployee', 'lblEmployeeNotSelected'));
            var txtStartDate = d.getElementById(ddlEmployee.id.replace('ddlEmployee', 'txtEmployeeStartDate'));
            var txtEndDate = d.getElementById(ddlEmployee.id.replace('ddlEmployee', 'txtEmployeeFinishDate'));
            var lblStartDateErr = d.getElementById(ddlEmployee.id.replace('ddlEmployee', 'lblStartDateErr'));
            var lblFinishDateErr = d.getElementById(ddlEmployee.id.replace('ddlEmployee', 'lblFinishDateErr'));
            lblEmployeeErr.style.display = lblStartDateErr.style.display = lblFinishDateErr.style.display = 'none';
            if(ddlEmployee.options.length > 0 && ddlEmployee.selectedIndex == 0) {
                lblEmployeeErr.style.display = 'block';
                errFound = true;        
            }
            if(txtStartDate.value == ''){
                lblStartDateErr.style.display = 'block';
                errFound = true;               
            }
            if(txtEndDate.value == ''){
                lblFinishDateErr.style.display = 'block';
                errFound = true;               
            }
        }
    }
    return (errFound) ? false : true;
}
function chklstDesignation(source,args)
{    
    var lst1 = document.getElementById(source.controltovalidate);
    
    if(lst1.selectedIndex==-1)
    {
        args.IsValid = false;
    }
    else
          args.IsValid = true;
}
function valCofirmHiearchyInsert(validationGroup)
{
    if(typeof(Page_ClientValidate) == "function")
    {
        Page_ClientValidate(validationGroup);
    }
    if(Page_IsValid)
    {    
       return confirm('Are you sure to add these designations to the hierarchy?');
    }
}

  function Changediv(ddl)
        {
            var trCurrentEmployer = document.getElementById(GetReferenceControlPrefix(ddl.id)+"txtCurrentEmployer");
            var trExperience =  document.getElementById(GetReferenceControlPrefix(ddl.id)+"ddlYear");
            var trMonth = document.getElementById(GetReferenceControlPrefix(ddl.id)+"ddlMonth");
            var trCurrentSalary =  document.getElementById(GetReferenceControlPrefix(ddl.id)+"txtCurrentSalary");
            
            if(ddl.value == "1")
            {
                trCurrentEmployer.disabled = true;
                trCurrentEmployer.className="textbox_disabled";
                
                trCurrentSalary.disabled = true;
                trCurrentSalary.className="textbox_disabled";
                
                trExperience.disabled = true;
                trExperience.className="dropdownlist_disabled";
                
                trMonth.disabled = true; 
                trMonth.className="dropdownlist_disabled";
            }
            else
            {
                trCurrentEmployer.disabled = false;
                trCurrentEmployer.className="textbox";
                
                trCurrentSalary.disabled = false;
                trCurrentSalary.className="textbox";
                
                trExperience.disabled = false;
                trExperience.className="dropdownlist";
                
                trMonth.disabled = false;
                trMonth.className="dropdownlist";
            }    
        }

 function ClearFields(ancId)
 {
     toggleAnimate(ancId);
     
      var txtskills = document.getElementById(ancId.replace(/ancAdvanceSearch/, "txtSrchSkillSet"));                   
             var ddlSrchQualification = document.getElementById(ancId.replace(/ancAdvanceSearch/, "ddlSrchQualification"));
             var txtSrchExp = document.getElementById(ancId.replace(/ancAdvanceSearch/, "txtSrchExp"));
             var txtCode = document.getElementById(ancId.replace(/ancAdvanceSearch/, "txtCode"));
             if (txtCode != null && txtskills != null && txtSrchExp != null && ddlSrchQualification != null )
             {
             txtCode.value="";
             txtskills.value = "";
             txtSrchExp.value="";           
             ddlSrchQualification.selectedIndex = 0; 
             }
  }
  
 function ClearAcceptance(ancId)
 {
     toggleAnimate(ancId);
     
      var txtRejectRemarks = document.getElementById(ancId.replace(/ancOfferAcceptance/, "txtRejectRemarks"));                   
           
             if (txtRejectRemarks != null)
             {
            
            txtRejectRemarks.value = "";
           
             }
  }
  
  function ClearAttachDocuments(Id)
 {
     toggleAnimate(Id);     
      var txtCandidateDocumentName = document.getElementById(Id.replace(/btnAttach/, "txtCandidateDocumentName"));                   
           
             if (txtCandidateDocumentName != null)
             {            
                txtCandidateDocumentName.value = "";           
             }
  }
  
  function ValidateFileExtension(source,args)
  {
   var btnClear=document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "btnClearAttachment"));
    btnClear.style.display = "block"; 
    if(!isDoc('ctl00_ctl00_content_public_content_fuResumeAttach_ctl02'))
    {
        alert('Invalid file type');  
        return false;                          
    }
  }
  
   function SetCandidateTab(id)
   {
        var div = document.getElementById(id.replace(/pnl/,'div')) ; 
        var tab = document.getElementById(id);
        
        var divTab1 = document.getElementById('divTab1');
        var divTab2 = document.getElementById('divTab2');
        var divTab3 = document.getElementById('divTab3');
        var divTab4 = document.getElementById('divTab4');
       
        divTab1.style.display = divTab2.style.display = divTab3.style.display = divTab4.style.display = "none" ;
        
        var pnlTab1 = document.getElementById('pnlTab1');
        var pnlTab2 = document.getElementById('pnlTab2');
        var pnlTab3 = document.getElementById('pnlTab3');
        var pnlTab4 = document.getElementById('pnlTab4');
        
        pnlTab1.className = pnlTab2.className = pnlTab3.className =  pnlTab4.className = "TabbedPanelsTab";  
       
        div.style.display = "block";
        tab.className = "TabbedPanelsTabSelected";  
  } 
  
   function ShowPreview(source,args)
   {   
   
        if(!isImage('ctl00_ctl00_content_public_content_fuRecentPhoto_ctl02'))
        {
            alert('Invalid file type');            
        }
        else
        {
            var divImage = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "divImage"));
            divImage.style.display = "block"; 
            
            WidthPreview(source,args,100,'imgPreview');
        }
    }  
    
function validateEmployer(source, args)      
{
    var dlJobHistory = document.getElementById(source.id.replace(/cvEmployer/, "dlJobHistory"));

    if(dlJobHistory != null)
    {
        for(var i = 0; i < dlJobHistory.rows.length; i++)
        {
            var txtEmployer=dlJobHistory.rows[i].getElementsByTagName("INPUT")[7].id;
            if(txtEmployer.value=="")    
            {
                args.IsValid=false;
            }
            else
            {
                 args.IsValid=true;
            }
        }
    }       
}  

function validateJobTitle(source, args)  
{
    var dlJobHistory = document.getElementById(source.id.replace(/cvJobTitle/, "dlJobHistory"));
    var isValid=false;
    
    if(dlJobHistory != null)
    {
        var controls = dlJobHistory.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('rcJobTitle') != -1)
            {
                if(controls[i].value != "-1")
                {
                   isValid=true;               
                }
                else
                {
                    isValid = false;
                }
            }
        }
        args.IsValid=isValid;
    }
}     

// Javascript For Employee Seperation Page Starts Here //
function  ValidateSeperatedReason(source,args)
{
    var ErroMessage;
    var IsValid =true;
    var ddl = document.getElementById('ctl00_ctl00_content_public_content_fvTerminationRequest_SeperationReasonReferenceControl_ddlList');
    var ddlValidator = document.getElementById(ddl.id.replace('ddlList','req'));
    var ddlDateOfSepearation = document.getElementById(source.id.replace(/cvValidateSepearatedReason/,"ddlWorkStatus"));
           
    if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "1")
    {
      if(ddl.selectedIndex == 0)
      {
         ErroMessage="Please select reason for absconding";   
         if (source.innerText == undefined)
          {  
            source.textContent = ErroMessage; 
          }
         else
            source.innerText = ErroMessage; 
         IsValid =false;
         
      }  
    }
    else if( ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "5")
    {
      if(ddl.selectedIndex == 0)
      {
         ErroMessage="Please select reason for termination";
         
        if (source.innerText == undefined)
          {  
            source.textContent = ErroMessage; 
          }
         else
            source.innerText = ErroMessage; 
         IsValid=false;  
      }
    }
    args.IsValid=IsValid;
         
}

function ValidateDateOfSeperation(source,args)
{
    var ErrorMessage;
    var txtDateOfSeperation  = document.getElementById(source.id.replace(/cvDateOfSeperation/,"txtDateofSepearation"));
    var hfCurrentServerDate  = document.getElementById(source.id.replace(/cvDateOfSeperation/,"hfCurrentDate"));
    var txtDateOfJoining     = document.getElementById(source.id.replace(/cvDateOfSeperation/,"txtDateofjoining"));
    var ddlDateOfSepearation = document.getElementById(source.id.replace(/cvDateOfSeperation/,"ddlWorkStatus"));
    
    if(txtDateOfSeperation.value == "" && ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value != "0" )
    {
       if (source.innerText == undefined)
       {
           if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "1")
                   ErrorMessage = "Please enter date of absconding";
                  else if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "2")
                    ErrorMessage = "Please enter expired date";
                  else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "3")
                    ErrorMessage = "Please enter retired date";
                  else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "4")
                    ErrorMessage = "Please enter resignation date";
                  else   if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "5")
                     ErrorMessage = "Please enter terminated date";
                  source.textContent = ErrorMessage;          
       }
       else 
        {  
             if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "1")
                ErrorMessage = "Please enter date of absconding";
              else if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "2")
                ErrorMessage = "Please enter expired date";
              else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "3")
                ErrorMessage = "Please enter retired date";
              else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "4")
                ErrorMessage = "Please enter resignation date";
              else   if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "5")
                 ErrorMessage = "Please enter terminated date";
              else if(!isValidDate(txtDateOfSeperation.value))
                  ErrorMessage = "Please enter valid date";
            
           source.innerText = ErrorMessage; 
       }    
       args.IsValid = false;
    }
    else
    {
      if (source.innerText == undefined)
       { 
          var DateofJoining     = Convert2Date(txtDateOfJoining.value);
          var DateofSeperation  = Convert2Date(txtDateOfSeperation.value);
          var CurrentDate       = Convert2Date(hfCurrentServerDate.value)
          var SelectedIndex     = ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value;
          var Message1;
          
          if(!isValidDate(txtDateOfSeperation.value))
          {
             ErrorMessage="Please enter a valid date";
             source.textContent = ErrorMessage; 
             args.IsValid = false;
          }
          else
          {
              if(SelectedIndex == "1")
                Message1 = "Date of absconding";
              else if(SelectedIndex =="2")
                Message1 ="Date of expiry";
              else if(SelectedIndex =="3")
                Message1 ="Retired date";
              else if(SelectedIndex =="4")
                Message1 ="Resignation date";
              else if(SelectedIndex =="5")
                Message1 ="Terminated date";
             
             
             if(DateofSeperation > CurrentDate)
             {
                ErrorMessage = Message1 +" should not be future date";
                source.textContent = ErrorMessage; 
                args.IsValid = false;
             }
             else 
             if(DateofSeperation < DateofJoining)
              {
                ErrorMessage = Message1 +" should be greater than date of joining";
               source.textContent = ErrorMessage; 
                args.IsValid = false;
              }
             else
                args.IsValid = true; 
          }
          
       }
       else
       {
          var DateofJoining    = Convert2Date(txtDateOfJoining.value);
          var DateofSeperation = Convert2Date(txtDateOfSeperation.value);
          var CurrentDate      = Convert2Date(hfCurrentServerDate.value)
          var SelectedIndex    = ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value;
          var Message1;
          
          if(!isValidDate(txtDateOfSeperation.value))
          {
             ErrorMessage="Please enter a valid date";
             source.innerText = ErrorMessage; 
             args.IsValid = false;
          }
          else
          {
              if(SelectedIndex == "1")
                Message1 = "Date of absconding";
              else if(SelectedIndex =="2")
                Message1 ="Date of expiry";
              else if(SelectedIndex =="3")
                Message1 ="Retired date";
              else if(SelectedIndex =="4")
                Message1 ="Resignation date";
              else if(SelectedIndex =="5")
                Message1 ="Terminated date";
             
             if(DateofSeperation > CurrentDate)
             {
                ErrorMessage = Message1 +" should not be future date";
                source.innerText = ErrorMessage; 
                args.IsValid = false;
             }
             else 
             if(DateofSeperation < DateofJoining)
              {
                ErrorMessage = Message1 +" should be greater than date of joining";
                source.innerText = ErrorMessage; 
                args.IsValid = false;
              }
             else
                args.IsValid = true; 
          }
       }
    }
       
}

// Javascript for employee termination page ends here //

function ValidateTrainingSchedule(sender, args)
{
    try
    {
        var rbtnDepartmentWise = document.getElementById(sender.id.replace('cvEmployee', 'rbtnDepartmentWise'));
        var rbtnEmployeeWise = document.getElementById(sender.id.replace('cvEmployee', 'rbtnEmployeeWise'));
        var chklstDepartments = document.getElementById(sender.id.replace('cvEmployee', 'chklstDepartments'));
        var chklstEmployees = document.getElementById(sender.id.replace('cvEmployee', 'chklstEmployees'));
        var chkSelectAll = document.getElementById(sender.id.replace('cvEmployee', 'chkAll'));
        var EmployeeSelected = false;
        
        if(chkSelectAll != null)
            EmployeeSelected = chkSelectAll.checked;
        
        if(!EmployeeSelected)
        {
            if(rbtnDepartmentWise.checked)
            {   
                // Training is scheduled by DepartmentWise
                if(chklstDepartments.cells.length > 0) 
                {
                    var CheckBoxes = chklstDepartments.getElementsByTagName('INPUT');
                    if(CheckBoxes != null)
                    {
                        for(var i = 0 ; i < CheckBoxes.length; i++)
                        {
                            if(CheckBoxes[i].checked)
                            {
                                EmployeeSelected = true;
                                break;
                            }
                        }
                        if(!EmployeeSelected)
                        {
                            sender.innerHTML = 'Please select atleast a department';
                            args.IsValid = false;
                            return;
                        }
                        else
                            args.IsValid = true;
                    }
                }
            }
            else 
            {
                // EmployeeWise
                if(chklstEmployees.cells.length > 0 ) 
                {
                    var CheckBoxes = chklstEmployees.getElementsByTagName('INPUT');
                    if(CheckBoxes != null)
                    {
                        for(var i = 0 ; i < CheckBoxes.length; i++)
                        {
                            if(CheckBoxes[i].checked)
                            {
                                EmployeeSelected = true;
                                break;
                            }
                        }
                        if(!EmployeeSelected)
                        {
                            sender.innerHTML = 'Please select atleast an employee';
                            args.IsValid = false;
                            
                            return;
                        }
                        else
                            args.IsValid = true;
                    }
                }
            }
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }

        if(typeof(rbtnDepartmentWise) !== 'undefined' && typeof(rbtnEmployeeWise) !== 'undefined')
        {
            if(rbtnDepartmentWise.checked)
            {
                // Departmentwise
            }
        }
        args.IsValid = false;
    }
    catch(e)
    {
       args.IsValid = true;
    }
}
  
  function SetTotalMarks(ctl)
  {
        var ddlPerformanceType=document.getElementById(ctl.id);
        var trTotalmarks=document.getElementById(ctl.id.replace('ddlPerformanceType','trTotalmarks'));
        
        if(ddlPerformanceType.value=="2")
        {
            trTotalmarks.style.display=trDisplay();
        }
        else
        {
            trTotalmarks.style.display="none";
            trTotalmarks.value="";
        }
  }
  
 function validateAnnouncementFromDate(source, args) {
    var ctrl = document.getElementById(source.controltovalidate);
    if(ctrl.disabled) {
        args.IsValid = true;
        return;
    }
    var currentDate = new Date();
    currentDate = new Date(currentDate.getFullYear().toString(), currentDate.getMonth().toString(), currentDate.getDate().toString());
    if(ctrl.value == '') {
        source.innerHTML = "Please enter date.";
        args.IsValid = false;
    }
    else if(!isDate(ctrl.value, source))
        args.IsValid = false;
        
    else if(Convert2Date(ctrl.value) < currentDate) {
        source.innerHTML = "date should not be earlier than today."; 
        args.IsValid = false;
    }
    else
        args.IsValid = true;
}
  
//function calAssessmentScore()
//{
//    try
//    {
//           //var txtGiveMarks=document.getElementById(ctl);
//           var dlEvaluator=document.getElementById('ctl00_ctl00_content_public_content_repEvaluation_ctl00_repEvaluator');
//           
//           var score = 0,  netScore= 0, phaseTotal = 0;
//           for(var j=0;j<dlEvaluator.rows.length; j++)
//           {
//                netScore = 0;
//                
//                 var dlrepPhases=dlEvaluator.rows[j].getElementsByTagName('table')[2];
//                 var lblTotalScore=dlEvaluator.rows[j].getElementsByTagName('span')[0];
//                 // document.getElementById('ctl00_ctl00_content_public_content_repEvaluation_ctl00_repEvaluator_ctl00_lblTotalScore');
//                 
//                for(var i=0;i< dlrepPhases.rows.length; i++)
//                {
//                     phaseTotal = 0;
//                     var dlrepAssessment = dlrepPhases.rows[i].getElementsByTagName('table')[1];   
//                                      
//                     
//                    for(var k = 0; k < dlrepAssessment.getElementsByTagName("input").length; k++)
//                    {
//                        if(dlrepAssessment.getElementsByTagName("input")[k].type == "text" && 
//                                dlrepAssessment.getElementsByTagName("input")[k].id.lastIndexOf("txtGiveMarks") != -1)
//                        {
//                            if(dlrepAssessment.getElementsByTagName("input")[k].value!="")
//                            {
//                            
//                                var maxScore = parseInt(dlrepAssessment.getElementsByTagName("INPUT")[3].value);
//                            
//                                score = parseInt(dlrepAssessment.getElementsByTagName("input")[k].value); 
//                                
//                                if(score > maxScore)
//                                {
//                                    if(dlrepAssessment.parentElement)
//                                        dlrepAssessment.parentElement.style.display = 'block';
//                                        
//                                    alert('Score cannot be greater than max score (' + maxScore.toString() + ')');
//                                    dlrepAssessment.getElementsByTagName("input")[k].focus();
//                                    dlrepAssessment.getElementsByTagName("input")[k].style.border = 'solid 1px Red';
//                                    return;
//                                }
//                                else
//                                {
//                                    dlrepAssessment.getElementsByTagName("input")[k].style.border = 'solid 1px Green'
//                                }
//                                dlrepAssessment.getElementsByTagName("input")[k].style.height = '15px';
//                                
//                                phaseTotal = phaseTotal + score;
//                                var strScore =  score.toString();
//                                var strPhaseTotal =phaseTotal.toString();
//                            }
//                        }
//                    }
//                    netScore = netScore + phaseTotal;
//                    var strNetScore = netScore.toString();
//                    
//                    dlrepPhases.rows[i].getElementsByTagName("span")[0].innerHTML = phaseTotal;
//                }
//                
//                if(typeof lblTotalScore === 'object')
//                    lblTotalScore.innerHTML = netScore.toString();
//            }
//      }
//      catch(e)
//      {
//      }
//}

function valSeparation(source,args)
{
    var ddlEmp = document.getElementById(source.controltovalidate);
    var ddlcomp=document.getElementById(ddlEmp.id.replace(/ddlEmployee/, "ddlCompany"));
    
    if(ddlcomp.selectedIndex == 0)
    {
        alert('Please select any company');
        args.IsValid = false;
    }
    else
    {
        if(ddlEmp.selectedIndex == 0)
        {
        
            var ddl = document.getElementById(ddlEmp.id.replace(/ddlEmployee/, "ddlSearchType"));
            if(ddl== null)
            {
                  args.IsValid = true; 
                  return;
            }
            else
            {
            if(ddl.selectedIndex == 0)
            {
                var fdate = document.getElementById(ddlEmp.id.replace(/ddlEmployee/, "txtFromDate"));
                var tdate = document.getElementById(ddlEmp.id.replace(/ddlEmployee/, "txtToDate"));
                if(fdate.value == '' && tdate.value=='')
                {
                     alert('Please enter from date and to date.');
                     args.IsValid = false;
                }
               else if(fdate.value=='')
                {
                     alert('Please enter from date .');
                     args.IsValid = false;
                }
                else if(tdate.value=='')
                {
                     alert('Please enter to date .');
                     args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }
            
            else
            {
                 var fdate=document.getElementById(ddlEmp.id.replace(/ddlEmployee/, "txtFromDate"));
                 if(fdate.value=='')
                {
                    alert('Please enter date .');
                     args.IsValid = false;
                }
                else
                    args.IsValid = true;
            }   
           } 
        }
        else
            args.IsValid = true;
      }
}

function SetCurrentAddress(ctl)
{
    var chkAddress=document.getElementById(ctl);
    var txtPermanentAddress=document.getElementById(ctl.id.replace(/chkAddress/,"txtPermanentAddress"));
    var txtCurrentAddress=document.getElementById(ctl.id.replace(/chkAddress/,"txtCurrentAddress"));
    
    if(ctl.checked)
    {
        txtCurrentAddress.value=txtPermanentAddress.value;
    }
    else
    {
        txtCurrentAddress.value="";
    }
}

function ShowCandidatePreview(source,args)
   {    
        var divImage = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "divImage"));     
        if(!isImage('ctl00_public_content_fuRecentPhoto_ctl02'))
        {
            var btnClear=document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "btnClearRecentPhoto"));
            btnClear.style.display = "block";       
            alert('Invalid file type');  
            
        }
        else
        {
            divImage.style.display = "block"; 
            
            WidthPreview(source,args,100,'imgPreview');
            
            var btnRemove=document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "btnRemoveRecentPhoto"));
            btnRemove.style.display = "block";     
        }
      
    } 
    
//     function WidthPreview(source, args, width)  
//        {
//        
//            var imgPreview = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "imgPreview"));
//            
//            imgPreview.style.display = "block";
//            imgPreview.src = "../thumbnail.aspx?folder=Temp&file=" + '<%= Session.SessionID %>' + ".jpg&width=" + width + "&t=" + new Date();
//        }  
    
    function SetRemarksField(ctl)
    {
    
         var ddlVacancyStatus=document.getElementById(ctl.id);
         var trRemarks=document.getElementById(ctl.id.replace(/ddlVacancyStatus/,"trRemarks"));
         var txtRemarks=document.getElementById(ctl.id.replace(/ddlVacancyStatus/,"txtRemarks"));
         
         if(ddlVacancyStatus.value==3 || ddlVacancyStatus.value==4)
         {
            trRemarks.style.display="block";
         }
         else
         {
             trRemarks.style.display="none";
         }    
    }
    
function Displaytextbox(btn)
{

     var rbtn = document.getElementById(btn);  
     
     var div = document.getElementById(rbtn.id.replace(/rbl/,"div"));
     
     if(rbtn.cells[0].childNodes[0].checked)
     {
        div.style.display = "block";
     }        
     else
     {
        div.style.display = "none";
     }
     //return true;
}
function CheckDefaultValueBasicPay(Control)
{
    var bIsValid = false;   
    
   // txtNetAmount 
  // var txtNetAmount = document.getElementById(Control.id.replace(/txtBasicPay/,"txtNetAmount"));
    
    if(Control.value == '') 
    {
        Control.value = '0.00';   
       // txtNetAmount.value = '0.00';   
        bIsValid = true;     
    }
    else
    {
        var value = ConvertToMoney(Control.value);
        if (!isNaN(value))
        {
            Control.value = value.toString();
            bIsValid = true;     
        }
        else
            {
                //alert('Invalid number');
                //Control.focus();
                bIsValid = false;    
            }
    }
    
    if(!bIsValid)
    {
        alert('Invalid number.');
        Control.focus();
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + Control.id + "\',\'\')", 0);
        return true;
    }
}

function chkDuplicateAllowanceDeduction(ddlAllowanceDeduction)
{
    var dlAllowanceDeductions = document.getElementById(ddlAllowanceDeduction.id.substring(0,ddlAllowanceDeduction.id.lastIndexOf('_ctl')));
    
    var controls = dlAllowanceDeductions.getElementsByTagName("SELECT");    
     var hidControl = document.getElementById("ctl00_hidCulture");
    var bDuplicate = false;    
    
    for(var i = 0; i < ddlAllowanceDeduction.length; i++)
    {
        if(ddlAllowanceDeduction[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if(controls[j] != ddlAllowanceDeduction)
                {
                    if(ddlAllowanceDeduction[i].value == controls[j].value)
                    {
                        bDuplicate = true;                     
                    }
                }
            }
        }
    }           
    if(bDuplicate)
    {
        alert(hidControl.value == 'ar-AE' ? 'يوجد قيمة مكررة.' : 'Duplicate value exists.');
        ddlAllowanceDeduction.selectedIndex = 0;
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + ddlAllowanceDeduction.id + "\',\'\')", 0);
        return true;
    }
}

function CheckDefaultValueRequest(Control)
{
    if(Control.value == '')
    {
        Control.value = '0.00';
        Control.focus();
    }
    else if(Control.value == '0.00' || Control.value == '0')
    {
         alert('Please enter a valid number of days');
          Control.focus();
    }
    else
    {
        var value = ConvertToMoney(Control.value);
        if (!isNaN(value)) Control.value = value.toString();
        else
            {
                alert('Invalid number');
                Control.focus();
            }
    }
}

function ShowOrHide(disable, ctrlID)
{
    var id = ctrlID.substring(0, ctrlID.lastIndexOf('_') + 1) + 'divCombination'
    var ctrl = document.getElementById(id);
    
    if(ctrl)
    {
        ctrl.disabled = disable;
        
        var texboxes = ctrl.getElementsByTagName("INPUT");
        var dropdowns = ctrl.getElementsByTagName("SELECT");
        
        for(var i = 0; i < texboxes.length; i++)
        {
            texboxes[i].disabled = disable;
            texboxes[i].value = '';
        }
        
        for(var i = 0; i < dropdowns.length; i++)
        {
            dropdowns[i].disabled = disable;
            dropdowns[i].selectedIndex = 0;
        }
    }
}
var eTimeSpan = function() {
    this.Seconds = 0;
    this.Minutes = 0;
    this.Hours = 0;
    this.Days = 0;
    this.Meridian='AM';
    
    this.Add = function(timespan){
        this.Seconds = this.Seconds + (timespan.Seconds > 0 ? timespan.Seconds : 0);
        this.Minutes = this.Minutes + (timespan.Minutes > 0 ? timespan.Minutes : 0);
        this.Hours = this.Hours + (timespan.Hours > 0 ? timespan.Hours : 0);
        this.Days = this.Days + (timespan.Days > 0 ? timespan.Days : 0);
        
        if(this.Minutes >= 60) {
            this.Hours += 1;
            this.Minutes -= 60;
        }
        
        if(this.Hours >= 24) {
            this.Days += 1;
            this.Hours -= 24;
        }
    };
    
    this.addMinutes = function(minutes){
        this.Minutes = this.Minutes +parseInt((minutes > 0 ? minutes : 0));
        
        if(this.Minutes >= 60) {
            this.Hours += parseInt(this.Minutes / 60);
            this.Minutes = this.Minutes % 60;
        }
        
        if(this.Hours >= 24) {
            this.Days += 1;
            this.Hours -= 24;
        }
    };
};


function setTime(datalistId,txtTime,txtDuration)
{
    var datalist = document.getElementById(datalistId);  
     
    if(datalist!=null)
    {
        var InterviewTime=document.getElementById(txtTime).value;
        var Duration=document.getElementById(txtDuration).value;
        var t = new eTimeSpan();     
            
        var span;
        var d;
        if(InterviewTime != '')
        {
            InterviewTime=InterviewTime.replace(' ','');
            InterviewTime=InterviewTime.replace('AM',' AM');
            InterviewTime=InterviewTime.replace('PM',' PM');
            t.Hours=eval(InterviewTime.split(':')[0]);
            t.Minutes=eval(InterviewTime.split(':')[1].split(' ')[0]);
            t.Meridian=InterviewTime.lastIndexOf('AM')>0?'AM':'PM';
            t.addMinutes(0);      
        }
        else
        {
            alert('You must set the interview time first');
            return false;
        }
        if(Duration == '' || Duration == 'hh:mm' )
        {
           alert('You must set the duration');
            return false;
        }
        else
        {
             d=parseInt(Duration.split(':')[1]) + (parseInt(Duration.split(':')[0]) * 60);
        }
        for(var i =0 ; i < datalist.rows.length-1; i++)
        {   
            var Time;
           
            var chk = datalist.rows[i].getElementsByTagName("INPUT")[0];
            if(chk!=null && chk.checked)
            {
                Time = datalist.rows[i].getElementsByTagName("INPUT")[1]; 
                if(t.Meridian=='AM')
                {
                    if(t.Hours<12)
                        t.Meridian="AM";
                    else
                        t.Meridian="PM";
                 }
                    
                Time.value=(t.Hours<10?'0':'')+ t.Hours.toString()+ ':' +(t.Minutes<10?'0':'')+ t.Minutes.toString()+ ' ' + t.Meridian;              
                t.addMinutes(d);               
            }
        }
    }  
    return false;  
}
function clearTime(datalistId)
{
    var hidControl = document.getElementById("ctl00_hidCulture");
 
    
    if(confirm(hidControl.value == "en-US"  ? 'Are you sure to clear the timings' : 'هل أنت متأكد لإلغاء توقيت'))
    {
        var datalist = document.getElementById(datalistId); 
        for(var i =0 ; i < datalist.rows.length-1; i++)
        {   
            var Time;
           
            var chk = datalist.rows[i].getElementsByTagName("INPUT")[0];
            if(chk!=null && chk.checked)
            {
                Time = datalist.rows[i].getElementsByTagName("INPUT")[1];            
                Time.value='';    
            }
        }
  }
  return false;
}

function ValidateReferenceControl1(ctrl)
{
   var txtBox = document.getElementById(ctrl.id.replace('btnSave', 'txtReferenceName'));
   
   if(txtBox)
   {
        if(txtBox.value == '') {
            alert('field required');
            txtBox.focus();
            return false;
        }
        else {
            return true;
        }
   }
   return false;
}

function ValidateDesignationControl(ctrl)
{
   var txtBox = document.getElementById(ctrl.id.replace('btnSave', 'txtDesignationName'));
   
   if(txtBox)
   {
        if(txtBox.value == '') {
            alert('field required');
            txtBox.focus();
            return false;
        }
        else {
            return true;
        }
   }
   return false;
}

function ValidateSaveQualification(ctrl)
{
   var txtBox = document.getElementById(ctrl.id.replace('btnSave', 'txtDegreeName'));
   
   if(txtBox)
   {
        if(txtBox.value == '') {
            alert('field required');
            txtBox.focus();
            return false;
        }
        else {
            return true;
        }
   }
   return false;
}
function ValidateSaveDegree(ctrl)
{
   var txtBox = document.getElementById(ctrl.id.replace('btnSave', 'txtSpecialization'));
   
   if(txtBox)
   {
        if(txtBox.value == '') {
            alert('field required');
            txtBox.focus();
            return false;
        }
        else {
            return true;
        }
   }
   return false;
}
function closePopupReference(element)
{
    alert('Hai from hr js file');
    return false;
}

//function ValidateMoreQualification(source,args)
//{
//    var dlMoreQualification=document.getElementById(source.id.replace(/cvValidatedlQualification/,"dlAddMoreQualifications"));
//    
//    for(var i=1; i<dlMoreQualification.rows.length; i++)
//    {
//         var degree = dlMoreQualification.rows[i].getElementsByTagName("SELECT")[0];
//         
//    }
//    args.isValid=true;
//}



function ValidateMoreQualificationDuplicate(ddlDegree)        // Candidate page Add More Qualifications
{
    var dlAddMoreQualifications = document.getElementById(ddlDegree.id.substring(0,ddlDegree.id.lastIndexOf('_ctl')));
    
    var controls = dlAddMoreQualifications.getElementsByTagName("SELECT");    
   
    var bDuplicate = false; 
    for(var i = 0; i < ddlDegree.length; i++)
    {
        if(ddlDegree[i].selected)
        {
            for(j = 0; j < controls.length; j++)
            {
                if((controls[j].id.lastIndexOf('ddlDegree') != -1) && controls[j] != ddlDegree) 
                {
                    if(ddlDegree[i].value == controls[j].value)
                    {
                        bDuplicate = true;                     
                    }
                }
            }
        }
    }           
    
    if(bDuplicate)
    {
        alert('Duplicate value in Degree.');
        ddlDegree.selectedIndex = 0;
        return false;
    }
    else
    {
        setTimeout("__doPostBack(\'" + ddlDegree.id + "\',\'\')", 0);
        return true;
    }
}

function ValidateDegreeSelection(source,args)
{
    var dlAddMoreQualifications=document.getElementById(source.id.replace(/cvValidateMoreQualification/, "dlAddMoreQualifications"));
    var isValid=false;
    
    if(dlAddMoreQualifications!=null)
    {
        var controls = dlAddMoreQualifications.getElementsByTagName("SELECT");
        
        for(var i = 0; i < controls.length; i++)
        {
            if(controls[i].id.lastIndexOf('ddlDegree') != -1)
            {
                if(controls[i].value != "-1")
                {
                   isValid=true;               
                }
                else
                {
                    isValid = false;
                }
            }
        }
        args.IsValid=isValid;
    }
    
}

// if(txtDateOfSeperation.value == "" && ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value != "0" )
//    {
//       if (source.innerText == undefined)
//       {
//           if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "1")
//                   ErrorMessage = "Please enter date of absconding";
//                  else if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "2")
//                    ErrorMessage = "Please enter expired date";
//                  else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "3")
//                    ErrorMessage = "Please enter retired date";
//                  else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "4")
//                    ErrorMessage = "Please enter resignation date";
//                  else   if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "5")
//                     ErrorMessage = "Please enter terminated date";
//                  source.textContent = ErrorMessage;          
//       }
//       else 
//        {  
//             if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "1")
//                ErrorMessage = "Please enter date of absconding";
//              else if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "2")
//                ErrorMessage = "Please enter expired date";
//              else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "3")
//              else  if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "4")
//                ErrorMessage = "Please enter resignation date";
//              else   if(ddlDateOfSepearation.options[ddlDateOfSepearation.selectedIndex].value == "5")
//                 ErrorMessage = "Please enter terminated date";
//              else if(!isValidDate(txtDateOfSeperation.value))
//                  ErrorMessage = "Please enter valid date";
//            
//           source.innerText = ErrorMessage; 
//       }    
//       args.IsValid = false;
function ValidateNoOfVacancies(sender,args)
{
  var ErrorMessage ;
  var txtNoOfVacancies  = document.getElementById(sender.id.replace(/cvNoOfVacancies/, "txtNoofVacancies")); 
  var hfHiredStatus  = document.getElementById(sender.id.replace(/cvNoOfVacancies/, "hfHiredStatus")); 
  
  if(txtNoOfVacancies != null && hfHiredStatus != null)
  {
    args.IsValid = false;
    if (sender.innerText == undefined)
    {
       if(parseInt(txtNoOfVacancies.value) == 0)
       {
         ErrorMessage = "No of vacancies should be greater than zero";
         sender.textContent = ErrorMessage;  
       }
       
       else if(parseInt(txtNoOfVacancies.value) < parseInt(hfHiredStatus.value))
       {
         ErrorMessage = "No of vacancies should not be less than no of hired candidates";
         sender.textContent = ErrorMessage;  
       
       }
       
       
       else
          args.IsValid = true;
    }
    else
    {
      if(parseInt(txtNoOfVacancies.value) == 0)
      {
        ErrorMessage = "No of vacancies should be greater than zero";
        sender.innerText = ErrorMessage; 
      }
      
       else if(parseInt(txtNoOfVacancies.value) < parseInt(hfHiredStatus.value))
       {
         ErrorMessage = "No of vacancies should not be less than no of hired candidates";
         sender.innerText = ErrorMessage;  
       
       }
      else
        args.IsValid = true;
    }
   }
  }

function ValidateWorkingDays(sender, args)
{   
    var txtWeeklyWorkingDays = document.getElementById(sender.id.replace('cvWeeklyWorkingDays', 'txtWeeklyWorkingDays'));  
    var MinValue = 1, MaxValue = 7;
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(txtWeeklyWorkingDays.value != 0) {
        var value = parseInt(txtWeeklyWorkingDays.value);
        
        if(value < MinValue || value > MaxValue) {
            sender.innerHTML = (hidControl.value == "ar-AE" ? ' الرجاء إدخال قيمة بين 1 و' : 'Please enter value between 1 and ') + MaxValue.toString(); // 'Please enter value between ' + MinValue.toString()+' and ' + MaxValue.toString(); 
            args.IsValid = false;   
        }
        else
            args.IsValid = true;
    }
}

function ValidateDailyWorkingHours(sender, args)
{   
    var txtDailyWorkingHours = document.getElementById(sender.id.replace('cvDailyWorkingHours', 'txtDailyWorkingHours'));  
    var MinValue = 1, MaxValue = 24;
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(txtDailyWorkingHours.value != 0) {
        var value = parseInt(txtDailyWorkingHours.value);
        
        if(value < MinValue || value > MaxValue) {
            sender.innerHTML =  (hidControl.value == "ar-AE" ? ' الرجاء إدخال قيمة بين 1 و' : 'Please enter value between 1 and ') + MaxValue.toString(); //'Please enter value between ' + MinValue.toString()+' and ' + MaxValue.toString(); 
            args.IsValid = false;   
        }
        else
            args.IsValid = true;
    }
}

function ValidateAnnualLeave(sender, args)
{   
    var txtAnnualLeave = document.getElementById(sender.id.replace('cvAnnualLeave', 'txtAnnualLeave'));  
    var MinValue = 0, MaxValue = 365;
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(txtAnnualLeave.value != 0) {
        var value = parseInt(txtAnnualLeave.value);
        
        if(value < MinValue || value > MaxValue) {
            sender.innerHTML =  (hidControl.value == "ar-AE" ? ' الرجاء إدخال قيمة بين 0 و' : 'Please enter value between 0 and ') + MaxValue.toString(); //'Please enter value between ' + MinValue.toString()+' and ' + MaxValue.toString(); 
            args.IsValid = false;   
        }
        else
            args.IsValid = true;
    }
}

function ValidateDuration(sender, args)
{
    var ddlExtensionType = document.getElementById(sender.id.replace('rgvDuration', 'ddlExtensionType'));  
    var txtDuration = document.getElementById(sender.id.replace('rgvDuration', 'txtDuration'));  
    var MinValue = 0.0, MaxValue = 0.0;
    
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    switch(ddlExtensionType.value)
    {
        // Leave Extension
        case "1":
            MaxValue = 365.00
            break;
        
        // Time extension day
        case "2":
            MaxValue = 1440.00;
            break;
        
        // Time extension month
        case "3":
            MaxValue = 44640.00;
            break;
        
        default :
            MaxValue = 44640.00
            break;
    }
    
    if(txtDuration.value != '') {
        var value = parseFloat(txtDuration.value);
        
        if(value < MinValue || value > MaxValue) {
            sender.innerHTML = (hidControl.value == "ar-AE" ? ' الرجاء إدخال قيمة بين 0 و' : 'Please enter value between 0 and ') + MaxValue.toString(); 
            args.IsValid = false;   
        }
        else
            args.IsValid = true;
    }
}

function ValidateleaveType(sender, args)
{
    var ddlExtensionType = document.getElementById(sender.id.replace('CvLeaveType', 'ddlExtensionType'));  
    var ddlLeaveType = document.getElementById(sender.id.replace('CvLeaveType', 'ddlLeaveType'));  
    var hidControl = document.getElementById("ctl00_hidCulture");
     
    if(ddlExtensionType.value=="1")
    {
        if(ddlLeaveType.value =="")
        {
                sender.innerHTML =hidControl.value == "ar-AE" ?' يرجى تحديد نوع الإجازة' : 'Please select a Leave Type'; 
                args.IsValid = false;   
                 
        }
        else
            args.IsValid = true;
            
                   
    }
    else
    {
       args.IsValid = true;   
    }
}


function calAssessmentScore()
{
    try
    {
           //var txtGiveMarks=document.getElementById(ctl);
           var dlEvaluator=document.getElementById('ctl00_ctl00_content_public_content_repEvaluation_ctl00_repEvaluator');
           
           var score = 0,  netScore= 0, phaseTotal = 0;
           for(var j=0;j<dlEvaluator.rows.length; j++)
           {
                netScore = 0;
                
                 var dlrepPhases=dlEvaluator.rows[j].getElementsByTagName('table')[2];
                 var lblTotalScore=dlEvaluator.rows[j].getElementsByTagName('span')[0];
                 // document.getElementById('ctl00_ctl00_content_public_content_repEvaluation_ctl00_repEvaluator_ctl00_lblTotalScore');
                 
                for(var i=0;i< dlrepPhases.rows.length; i++)
                {
                     phaseTotal = 0;
                     var dlrepAssessment = dlrepPhases.rows[i].getElementsByTagName('table')[1];   
                                      
                     for(var j=0;j<dlrepAssessment.rows.length;j++)
                     {
                        for(var k = 0; k < dlrepAssessment.rows[j].getElementsByTagName("input").length; k++)
                        {
                            if(dlrepAssessment.rows[j].getElementsByTagName("input")[k].type == "text" && 
                                    dlrepAssessment.rows[j].getElementsByTagName("input")[k].id.lastIndexOf("txtGiveMarks") != -1)
                            {
                                if(dlrepAssessment.rows[j].getElementsByTagName("input")[k].value!="")
                                {
                                
                                    var maxScore = parseFloat(dlrepAssessment.rows[j].getElementsByTagName("INPUT")[3].value);
                                    var Control = dlrepAssessment.rows[j].getElementsByTagName("input")[k];
                                    if(Control.value == '') Control.value = '0.00';
                                    else
                                    {
                                    var value = ConvertToMoney(Control.value);
                                    if (!isNaN(value)) Control.value = value.toString();
                                    else
                                    {
                                    alert('Invalid number');
                                    Control.focus();
                                    return ;
                                    }
                                    }
                                
                                    score = parseFloat(dlrepAssessment.rows[j].getElementsByTagName("input")[k].value); 
                                    
                                    if(score > maxScore)
                                    {
                                        if(dlrepAssessment.parentElement)
                                            dlrepAssessment.parentElement.style.display = 'block';
                                            
                                        alert('Score cannot be greater than max score (' + maxScore.toString() + ')');
                                        dlrepAssessment.rows[j].getElementsByTagName("input")[k].focus();
                                        dlrepAssessment.rows[j].getElementsByTagName("input")[k].style.border = 'solid 1px Red';
                                        return;
                                    }
                                    else
                                    {
                                        dlrepAssessment.rows[j].getElementsByTagName("input")[k].style.border = 'solid 1px Green'
                                    }
                                    dlrepAssessment.rows[j].getElementsByTagName("input")[k].style.height = '15px';
                                    
                                    phaseTotal = phaseTotal + score;
                                    var strScore =  score.toString();
                                    var strPhaseTotal =phaseTotal.toString();
                                }
                            }
                        }
                       }
                    netScore = netScore + phaseTotal;
                    var strNetScore = netScore.toString();
                    
                    dlrepPhases.rows[i].getElementsByTagName("span")[0].innerHTML = phaseTotal;
                    
                }
                
                if(typeof lblTotalScore === 'object')
                    lblTotalScore.innerHTML = netScore.toString();
            }
      }
      catch(e)
      {
      }
}
function valRelatedParticulars(source,args)
{

    var isValid=false;
    var arrCheckBox = document.getElementsByTagName("input");
    
    for(var i = 0; i < arrCheckBox.length; i++)
    {
        if(arrCheckBox[i].type == "checkbox" && arrCheckBox[i].id.lastIndexOf("cblParticulars") != -1)
        {
            if(arrCheckBox[i].checked)
            {
               isValid = true;
            }
        }
    }
     args.IsValid = isValid;
}

function CheckAssessment(id)
{
   
    var dlrepAssessment=document.getElementById(id.substring(0,id.lastIndexOf("repAssessment")+13));
    var IsValid=false;
    
     for(var j=0;j<dlrepAssessment.rows.length;j++)
     {
        for(var k = 0; k < dlrepAssessment.rows[j].getElementsByTagName("input").length; k++)
        {
            if(dlrepAssessment.rows[j].getElementsByTagName("input")[k].type == "text" && 
                    dlrepAssessment.rows[j].getElementsByTagName("input")[k].id.lastIndexOf("txtGiveMarks") != -1)
            {
                if(dlrepAssessment.rows[j].getElementsByTagName("input")[k].value!="" && parseInt(dlrepAssessment.rows[j].getElementsByTagName("input")[k].value) != 0)
                {
                    IsValid=true;
                   // var maxScore = parseInt(dlrepAssessment.rows[j].getElementsByTagName("INPUT")[3].value);
                   // score = parseInt(dlrepAssessment.rows[j].getElementsByTagName("input")[k].value);            
                    
                }
            }
        }
       }
       if(!IsValid)
       {
            alert('Please enter the score details to save performance assessment.');
       }
       
       return IsValid;
    
    
}

function validatePettyCashAttachment(source,args)
{
     var fuPettyCash1=document.getElementById('ctl00_ctl00_content_public_content_fuPettyCash_ctl02');
    
     if (fuPettyCash1.value == "")
     {
        args.IsValid = false;
     }         
}
function imgNotImage(source)
{
    var img1="../images/reports1.png";
    if(img1!=source.src)
    {
        source.src=img1;
    }
}
function ValidateEmployeeProfilePage(validationGroup,id)
    {
        if(typeof(Page_ClientValidate) == "function")
        {
            Page_ClientValidate(validationGroup);
        }
        if(Page_IsValid)
        { 
                return true; 
        }
        else
        {
          return false; 
     
        }
    }
    
function ValidateMark(sender, args)
{

    var rdbMark = document.getElementById(sender.id.replace('CvMark', 'rdMark'));  
    var txtmark = document.getElementById(sender.id.replace('CvMark', 'txtMaxMark'));  
    if(rdbMark.checked)
    {
     if(txtmark.value =="")
     {
          sender.innerHTML = 'Please enter a mark'; 
            args.IsValid = false;   
     }
     
     
    }
    else
    
    {
      
            args.IsValid = true;   
    }
} 


function confirmOfferFromCandidate(hfConfirmOffer,IsEdit)
{
//    if(typeof(Page_ClientValidate) == "function")
//    {
//         Page_ClientValidate('common');
//    }
//    if(Page_IsValid)
//    {  
        var fv = document.getElementById(hfConfirmOffer.substring(0,hfConfirmOffer.lastIndexOf("_")));

            if(IsEdit)
            {
                var result = confirm('Do you wish to accept this offer?');
                
                var hfConfirmOffer = document.getElementById(hfConfirmOffer);
                
                if(result)
                {
                   hfConfirmOffer.value = 1;
                   return true;
                }
                else
                {
                   hfConfirmOffer.value = 0;
                   return true;  
                }
            }
            else
            {
                return true;
            }  
        
//    }
}


function ValidateOtherDocExpiryDate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    if (!isValidDate(txtExpiryDate.value))
    {
        if(source.innerText == undefined) 
           {source.textContent = 'Please enter valid date';}
        else {source.innerText='Please enter valid date';}
        args.IsValid = false;
    }
    else
    {
         var Expirydate = Convert2Date(txtExpiryDate.value);
         var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtIssuedate");
         var Issuedate = Convert2Date(txtIssuedate.value);
         
         if(Issuedate > Expirydate)
         {
            if(source.innerText == undefined) { source.textContent =  "Expiry date should be greater than Issue date"; }
            else { source.innerText =  "Expiry date should be greater than Issue date";}
                   
            args.IsValid = false;
         }        
    }
}

function ValidateCandidatepassportExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    
    var hidControl = document.getElementById("ctl00_hidCulture");

    
   
    
    if (!isValidDate(txtExpiryDate.value))
    {
        if(source.innerText == undefined)
        { 

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
           
        else 
        { 
            

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
        }
        
        args.IsValid = false;
        
    }
    else
    {
         var Expirydate = Convert2Date(txtExpiryDate.value);
         var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtPassportReleaseDate");
         var Issuedate = Convert2Date(txtIssuedate.value);
         
           if(Issuedate >= Expirydate)
            {
               if(source.innerText == undefined) 
               { 
                      if(hidControl.value == "en-US")
                        source.textContent = "Expiry date should be greater than Issue date"; 
                      else
                         source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"

               }
               else 
               {
                    if(hidControl.value == "en-US")
                        source.textContent = "Expiry date should be greater than Issue date"; 
                      else
                         source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"

               }
                
                args.IsValid = false;
               
            }        
    }
}

function ValidateCandidatevisaExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");

    
   
    
    if (!isValidDate(txtExpiryDate.value))
    {
        if(source.innerText == undefined)
        { 

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
           
        else 
        { 
            

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
        }
        
        args.IsValid = false;
        
    }
    else
    {
         var Expirydate = Convert2Date(txtExpiryDate.value);
         var txtIssuedate = document.getElementById(source.id.substring(0, source.id.lastIndexOf('_') + 1) + "txtVisaReleaseDate");
         var Issuedate = Convert2Date(txtIssuedate.value);
         
         if(Issuedate >= Expirydate)
            {
               if(source.innerText == undefined) 
               { 
                      if(hidControl.value == "en-US")
                        source.textContent = "Expiry date should be greater than Issue date"; 
                      else
                         source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"

               }
               else 
               {
                    if(hidControl.value == "en-US")
                        source.textContent = "Expiry date should be greater than Issue date"; 
                      else
                         source.textContent ="يجب أن يكون تاريخ الانتهاء أكبر من تاريخ الإصدار"

               }
                
                args.IsValid = false;
               
            }        
    }
}


function ValidateCandidateNationalIDExpirydate(source, args)
{
    var txtExpiryDate=document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
   
    
    if (!isValidDate(txtExpiryDate.value))
    {
        if(source.innerText == undefined)
        { 

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';
        
        }
           
        else 
        { 
            

            if(hidControl.value == "en-US")
                source.textContent =  'Please enter valid date'; 
            else
                source.textContent =  'الرجاء إدخال تاريخ صالح';   
        }
        
        args.IsValid = false;
        
    }
    
}
function ValidateDecimal(e,value)
{
    if(value != "")
    {
        var code = e.keyCode;
        if(code != 46 && code > 31 && (code < 48 || code > 57))
        return false;
        
        else if(code == 46)
        {
            if(value.indexOf('.') != -1)
            {
                var Split = value.split('.');
                if(Split.length >= 2)
                return false;
            }
        }
    }
    return true;
}

function SplitterToggle(currentDivId)
{
    HideIfRequired(currentDivId,'divJobTray');
   
    
    var currdiv = document.getElementById(currentDivId);
    
    if(currdiv != null)
    {
        var img = document.getElementById(currentDivId.replace(/div/, "img"));
        
        if(currdiv.style.display == 'none')
             img.src = '../images/Show.png' 
        else
             img.src = '../images/Hide.png' 
            
       SlideUpSlideDown(currentDivId, 'medium');    
    }
}

function ValidateProjectStartDate(source, args)
{
    var ctrl = document.getElementById(source.controltovalidate);
    var hidControl = document.getElementById("ctl00_hidCulture");
    if(!isValidDate(ctrl.value))
    {
        source.innerText = hidControl.value == "ar-AE" ?"الطلب تفاصيل أضاف بنجاح":"Please enter  date.";//"Please enter date."; 
        args.IsValid= false;
    }
    else
    {
        args.IsValid=true;
    }   
}

function valAttendanceDatalist(datalistId)
{

    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var hidControl = document.getElementById("ctl00_hidCulture");
      
    
    var count = 0;
    
    var checked = false;
    
   
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        //if(IsArabic == "False")
        
          if(hidControl.value == "en-US")
            return confirm('Are you sure to update the selected items?');
        else
            return confirm ('هل أنت متأكد لتحديث العناصر المحددة؟');
        
        
    }
    else
    { 
       // if(IsArabic == "False")
       if(hidControl.value == "en-US")
            alert('Please select an item ');
        else
            alert('يرجى تحديد عنصر');
        
        
        return false;
    }
}

 function ValidateResume(source, args)
  {
      
       if(!isDoc('fuResumeAttach_ctl02'))
        {
            alert('Invalid file type');  
            return false;                          
        }  
  }
function SetTrainingTab(id)
{
    var div = document.getElementById(id.replace(/pnl/,'div')) ; 
    var tab = document.getElementById(id);
    
    var divTab1 = document.getElementById('divTab1');
    var divTab2 = document.getElementById('divTab2');
    var divTab3 = document.getElementById('divTab3');

    
    //var divTab6 = document.getElementById('divTab6');
    
    divTab1.style.display = divTab2.style.display = divTab3.style.display  = "none" ;
    
    var pnlTab1 = document.getElementById('pnlTab1');
    var pnlTab2 = document.getElementById('pnlTab2');
    var pnlTab3 = document.getElementById('pnlTab3');
   
    
    //var pnlTab6 = document.getElementById(id.replace(/id.id/,"pnlTab6")) //document.getElementById(id.replace(/divTab1/, "pnlTab6"));
    
    pnlTab1.className = pnlTab2.className = pnlTab3.className =  "TabbedPanelsTab";  
   
    div.style.display = "block";
    tab.className = "TabbedPanelsTabSelected";  
} 

