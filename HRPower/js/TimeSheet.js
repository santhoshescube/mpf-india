﻿
var TimeSpan = function(smallerTime, biggerTime) {
    var minTimeMeridien = smallerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    var maxTimeMeridien = biggerTime.indexOf('PM') > 0 ? 'PM' : 'AM';
    smallerTime = smallerTime.replace(minTimeMeridien, ':00 '+ minTimeMeridien);
    biggerTime = maxTime.replace(maxTimeMeridien, ':00 '+ maxTimeMeridien);
    var tempDate1 = new Date('January 1,2010 '+ smallerTime);
    var tempDate2 = new Date('January 1,2010 '+ biggerTime);
    var day = ( (minTimeMeridien == 'PM' &&  maxTimeMeridien == 'AM') || (minTimeMeridien != maxTimeMeridien && tempDate1 > tempDate2 ) || ( (minTimeMeridien == maxTimeMeridien) && ( tempDate1 > tempDate2 ) )) ? 2 : 1; 
    var date1 = new Date('January 1,2010 '+ smallerTime);
    var date2 = new Date('January ' + day + ',2010 '+ biggerTime);
    this.Seconds = (date2 - date1)/(1000);
    this.Minutes = (this.Seconds/ 60 ) % 60 ;
    this.Hour = parseInt(this.Seconds / (60 * 60));
    this.Days = parseInt(this.Seconds/(60 * 60 * 24)); 
};

var EmptyTimeSpan = function() {
    this.Seconds = 0;
    this.Minutes = 0;
    this.Hours = 0;
    this.Days = 0;
    
    this.Add = function(timespan){
        this.Seconds = this.Seconds + (timespan.Seconds > 0 ? timespan.Seconds : 0);
        this.Minutes = this.Minutes + (timespan.Minutes > 0 ? timespan.Minutes : 0);
        this.Hours = this.Hours + (timespan.Hours > 0 ? timespan.Hours : 0);
        this.Days = this.Days + (timespan.Days > 0 ? timespan.Days : 0);
        
        if(this.Minutes >= 60) {
            this.Hours += 1;
            this.Minutes -= 60;
        }
        
        if(this.Hours >= 24) {
            this.Days += 1;
            this.Hours -= 24;
        }
    };
};

var CustomDate = function(objDate) {

 if(!objDate)
    objDate = new Date();
    
  var date = objDate.copy();
  this.Hours = date.getHours();
  this.Minutes = date.getMinutes();
  this.Date = date;
  this.AddHours = function(hours) {
        date = objDate.setHours(this.Hours + hours);
  };
  this.AddTimeSpan = function(objEmtptyTimeSpan) {
        date.setHours(date.getHours() + objEmtptyTimeSpan.Hours);
        date.setMinutes(date.getMinutes() + objEmtptyTimeSpan.Minutes);
  };
};

function DateDiff(objDate1, objDate2) {
    var objTimespan = new EmptyTimeSpan();
    var tempDate = objDate2.copy();
    if(objDate2 < objDate1) tempDate.setTime( tempDate.getTime() + (24 * 60 * 60 * 1000) );
    
    if(tempDate < objDate1)
        tempDate.addDays(1);
        
    objTimespan.Seconds = (tempDate - objDate1)/(1000);
    objTimespan.Minutes = (objTimespan.Seconds/ 60 ) % 60 ;
    objTimespan.Hours = parseInt(objTimespan.Seconds / (60 * 60));
    objTimespan.Days = parseInt(objTimespan.Seconds/(60 * 60 * 24)); 
    return objTimespan;
}

function DateDiff2(objBiggerDate, objSmallerDate2) {
    var objTimespan = new EmptyTimeSpan();
    objTimespan.Seconds = (objBiggerDate - objSmallerDate2)/(1000);
    objTimespan.Minutes = (objTimespan.Seconds/ 60 ) % 60 ;
    objTimespan.Hours = parseInt(objTimespan.Seconds / (60 * 60));
    objTimespan.Days = parseInt(objTimespan.Seconds/(60 * 60 * 24)); 
    return objTimespan;
}

var Shift = function () {
    this.OrderNo = -1;
    this.ShiftID = -1;
    this.FromTime = new Date();
    this.ToTime = new Date();
    this.MinWorkingHours = -1;
    this.Duration = '00:00';
    this.BufferTime = 0;
    this.TimeMatch = new CustomDate(new Date());
    this.FallInShift = false;
    this.DayAdded = false;
};

function CalculateTimeSheet(ctrlID, isTextbox) {
    var datalist;
    if(isTextbox) datalist = document.getElementById(ctrlID.substring(0,ctrlID.lastIndexOf('_ctl')));   
    else 
        datalist = document.getElementById(ctrlID.replace(/ibtnNewEntry/,"dlEntry"));
    var lblTotalWorkTime = document.getElementById(datalist.id.replace(/dlEntry/,"lblTotalWorktime")); 
    return CalculateTimeSheetEntries(datalist, lblTotalWorkTime);
}

var GetShift = function(table, dateTime) {
    var myShift = new Shift();
    
    var shiftList = [];
    if(typeof(table) === 'object')
    {
        for(var i = 1; i < table.rows.length; i++)
        {
            myShift = new Shift();
            myShift.OrderNo = parseInt(table.rows[i].cells[0].innerHTML);
            myShift.ShiftID = parseInt(table.rows[i].cells[1].innerHTML);
            myShift.FromTime = new Date('January 1, 2010 ' + table.rows[i].cells[2].innerHTML);
            myShift.ToTime = new Date('January 1, 2010 ' + table.rows[i].cells[3].innerHTML);
            
            if(myShift.ToTime < myShift.FromTime) {
                myShift.ToTime.addDays(1);
                myShift.DayAdded = true;
                var tempDate = dateTime.copy();
                
                var diff = DateDiff2(myShift.FromTime, dateTime.copy());
                
                if(Math.abs(diff.Hours) > 12)
                    tempDate.addDays(1);
                
                                
                //tempDate = tempDate.addDays(1); // commented case : shift 11:00 PM - 07:00 AM ---> first time 11:30 PM 
                myShift.TimeMatch = DateDiff2(myShift.FromTime, tempDate);
            }
            else
                myShift.TimeMatch = DateDiff2(myShift.FromTime, dateTime.copy());
                
            myShift.MinWorkingHours = table.rows[i].cells[4].innerHTML;
            myShift.Duration = table.rows[i].cells[5].innerHTML;
            myShift.BufferTime = table.rows[i].cells[6].innerHTML;
            myShift.FallInShift = (dateTime >= myShift.FromTime && dateTime <= myShift.ToTime);
            
            shiftList.push(myShift);
        }
    }
    if(shiftList.length > 0)
    {
        var index = -1;
        var lowestValue = 0;
        var secondLowest = 0;
        var secondLowestIndex = -1;
        for(var j = 0 ; j < shiftList.length; j ++)
        {
            var shift = shiftList[j];
            if(j == 0) {
                lowestValue = Math.abs(shift.TimeMatch.Seconds);
                secondLowest = Math.abs(shift.TimeMatch.Seconds);
                index = j;        
            }
            else {
                if(lowestValue > Math.abs(shift.TimeMatch.Seconds)) {
                    var temp = lowestValue;
                    if(temp < secondLowest) {
                        secondLowest = temp;
                        secondLowestIndex = index;
                    } 
                    lowestValue = Math.abs(shift.TimeMatch.Seconds);
                    index = j;
                }
                else if( secondLowest > Math.abs(shift.TimeMatch.Seconds)) {
                    secondLowest = Math.abs(shift.TimeMatch.Seconds);
                    secondLowestIndex = j;
                }
            }    
        }
        if(index > -1 && shiftList.length > 0) {
               myShift = shiftList[index]; 
               
           if((lowestValue/60) <= parseInt(shiftList[index].BufferTime))
                myShift = shiftList[index];
           else {
                var prevDiff = myShift.TimeMatch.Seconds;
                var FallInShiftFound = false;
                for(var i = 0; i < shiftList.length; i++)
                {
                    if(shiftList[i].FallInShift == true) {
                        FallInShiftFound = true;
                        break;
                    }
                }
                if(FallInShiftFound == false){
                    dateTime = dateTime.addDays(1);
                    
                    for(var i = 0; i < shiftList.length; i++)
                    {
                        if(shiftList[i].FallInShift == false) {
                            shiftList[i].FallInShift = (dateTime >= shiftList[i].FromTime && dateTime <= shiftList[i].ToTime);                 
                        }
                    }
                }
                if(myShift.FallInShift == false) {
                   var index2 = -1;
                   var assigned = false;
                   for(var j = 0 ; j < shiftList.length; j++) {
                       if(j != index) {
                           if(shiftList[j].FallInShift) {
                                if(!assigned) {
                                   myShift = shiftList[j];
                                   assigned = true;
                                }
                                else if(assigned && shiftList[j].FromTime > myShift.FromTime)
                                     myShift = shiftList[j];
                           }
                       } 
                   }
                    if(Math.abs(myShift.TimeMatch.Seconds) > Math.abs(shiftList[index].TimeMatch.Seconds))
                        myShift = shiftList[index];
               }
               else
               {
                    if(Math.abs(myShift.TimeMatch.Seconds) > Math.abs(shiftList[index].TimeMatch.Seconds))
                        myShift = shiftList[index];
               }
           }
       }
    }
    return myShift;
};

function GetSplitShifts(table)
{
    var myShift = new Shift();
    
    var shiftList = [];
    try
    {
        if(typeof(table) === 'object')
        {
            for(var i = 1; i < table.rows.length; i++)
            {
                myShift = new Shift();
                myShift.OrderNo = parseInt(table.rows[i].cells[0].innerHTML);
                myShift.ShiftID = parseInt(table.rows[i].cells[1].innerHTML);
                myShift.FromTime = new Date('January 1, 2010 ' + table.rows[i].cells[2].innerHTML);
                myShift.ToTime = new Date('January 1, 2010 ' + table.rows[i].cells[3].innerHTML);
                    
                myShift.MinWorkingHours = table.rows[i].cells[4].innerHTML;
                myShift.Duration = table.rows[i].cells[5].innerHTML;
                myShift.BufferTime = table.rows[i].cells[6].innerHTML;
                myShift.FallInShift = false;// (dateTime >= myShift.FromTime && dateTime <= myShift.ToTime);
                
                shiftList.push(myShift);
            }
        }
    }
    catch(ex) { }    
    return shiftList;
}

function CalculateTimeSheetEntries(datalist, lblTotalWorkTime)
{
    var lblOverTime = document.getElementById(datalist.id.replace(/dlEntry/,"lblOverTime"));  
    var hfDuration = document.getElementById(datalist.id.replace(/dlEntry/,"hfDuration"));
    var hfFromTime = document.getElementById(datalist.id.replace(/dlEntry/,"hfFromTime"));
    var hfToTime = document.getElementById(datalist.id.replace(/dlEntry/,"hfToTime"));
    var lblAllowedBreakTime = document.getElementById(datalist.id.replace(/dlEntry/,"lblAllowedBreakTime"));
    var lblExtraBreak = document.getElementById(datalist.id.replace(/dlEntry/,"lblExtraBreak"));
    var lblIsEarly = document.getElementById(datalist.id.replace(/dlEntry/,"lblIsEarly"));
    var lblIsLate = document.getElementById(datalist.id.replace(/dlEntry/,"lblIsLate"));
    var lblEmployee = document.getElementById(datalist.id.replace(/dlEntry/,"lblEmployee"));
    var hfFlexi = document.getElementById(datalist.id.replace(/dlEntry/,"hfFlexi"));
    var flexi = (hfFlexi.value == 'Flexi' || hfFlexi.value == 'Dynamic') ? true : false;
    var shiftType = hfFlexi.value;
    var fixed = (hfFlexi.value == 'Fixed') ? true : false;
    var EmployeeeName = lblEmployee.innerHTML;
    var txtLastExitTime, txtLastExit, dtLastEntry, totalOverTimeHrs = 0, totalOverTimeMins = 0, DataEntryFound = false;
    var totalMin = 0, totalHr = 0; // total working hours and minutes
    var totalBreakHrs = 0, totalBreakMins = 0, allowedBreakMins = 0, alllowedBreakHrs = 0, durationMins = 0, durationHrs = 0;
    var stopWorkTimeCalculation = false, overTimeAdded = false, earlyHrs = 0, earlyMins = 0;
    var dtTime, dtEntry;
    var lblShiftDesc = document.getElementById(datalist.id.replace(/dlEntry/,"lblShift"));  
    var tblShifts = document.getElementById(datalist.id.replace(/dlEntry/,"hiddenGridShifts"));  
    var lblBackupShiftDesc = document.getElementById(datalist.id.replace(/dlEntry/,"lblBackupShiftDesc"));  
    var breakInShift = new EmptyTimeSpan();
    var hfIsMinWorkOT = document.getElementById(datalist.id.replace(/dlEntry/,"hfIsMinWorkOT"));
    var CalculateOTBasedOnMinWorkTime = hfIsMinWorkOT.value == '1' ? true : false;
    var tsOverTime = new EmptyTimeSpan();
    var overTimeAdd = false;
    var hfEarlyBefore = document.getElementById(datalist.id.replace(/dlEntry/,"hfEarlyBefore"));
    var hfLateAfter = document.getElementById(datalist.id.replace(/dlEntry/,"hfLateAfter"));
    
    var EarlyBeforeInMinutes = parseInt(hfEarlyBefore.value);
    var LateAfterInMinutes = parseInt(hfLateAfter.value);
    
    if( shiftType == 'Flexi') { // if flexi timig, working hours greater than duration (time set in work policy) is considered as overtime
        durationHrs = parseInt(eval(hfDuration.value.split(':')[0]));
        durationMins = parseInt(eval(hfDuration.value.split(':')[1]));
    }
    else if( shiftType == 'Split')
    {
        return HandleSplitShift(datalist, lblTotalWorkTime);
    }
   
    if(datalist != null && typeof(datalist) !== 'undefined')
    {
        var dtShiftTo = new Date('January 1, 2010 ' + hfToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        var dtShiftFrom = new Date('January 1, 2010 ' + hfFromTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        var dtFirstPunchingTime ;
        if(dtShiftFrom > dtShiftTo) 
            // increment date dtShiftTo by 1
            dtShiftTo =  new Date('January 2, 2010 ' + hfToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        
        if(datalist.rows.length > 0) {
            for (var i = 0; i < datalist.rows.length ;  i++) {
                overTimeAdded = false;
                txtLastExitTime = datalist.rows[i].getElementsByTagName("input")[0];
                if(!DataEntryFound) {
                    if( (txtLastExitTime.value.replace(' ', '').length) > 0) DataEntryFound = true;
                }
                if(datalist.rows.length == 1 && txtLastExitTime.value.replace(' ', '').length == 0)
                {
                    lblShiftDesc.innerHTML = lblBackupShiftDesc.innerHTML;
                    return;
                }
                dtLastEntry = new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 '));
                if(!dtTime){ 
                    dtTime = new CustomDate(dtLastEntry); 
                    dtFirstPunchingTime = new Date('January 1, 2010 ' + datalist.rows[0].getElementsByTagName("input")[0].value.replace(' ', ':00 '));
                }
                if(!dtEntry) 
                    dtEntry = new CustomDate(new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 ')));
                
               if( shiftType == 'Dynamic') {
               
                    if(i == 0) {
                        var shift = GetShift(tblShifts, dtFirstPunchingTime.copy());

                        if(typeof(shift) !== 'undefined') {
                            dtShiftFrom = shift.FromTime;
                            dtShiftTo = shift.ToTime;
                            
                            var diffShift = DateDiff2(dtShiftTo.copy(), dtShiftFrom.copy());
                            var d = new EmptyTimeSpan();
                            if(dtShiftFrom > dtLastEntry)
                                d = DateDiff2(dtShiftFrom, dtLastEntry);
                            else
                                d = DateDiff2(dtLastEntry, dtShiftFrom);
                                
                            var FromHr = dtShiftFrom.getHours();
                            
                            if(d.Hours > 12 && dtShiftFrom.getHours() >  dtShiftTo.getHours())
                            {
                                dtTime.Date.addDays(1);
                                dtFirstPunchingTime = dtTime.Date.copy();
                                dtLastEntry = dtTime.Date.copy();
                            }
                            if(typeof(lblShiftDesc) === 'object'){
                                var custDateFrom = new CustomDate(shift.FromTime);
                                var custDateTo = new CustomDate(shift.ToTime);

                                lblShiftDesc.innerHTML = custDateFrom.Date.format('hh:mm tt') + ' - ' + custDateTo.Date.format('hh:mm tt') + ' [Duration - ' + shift.MinWorkingHours + ' hrs]';
                                durationHrs = parseInt(eval(shift.Duration.split(':')[0]));
                                durationMins = parseInt(eval(shift.Duration.split(':')[1]));
                            }
                        }
                    }
               }
               
               if( i == 0 && shiftType != 'Flexi' && DataEntryFound) 
                { 
                    lblIsLate.innerHTML = '';
                    // check if employee is late
                    if(dtTime.Date > dtShiftFrom)
                    {
                        var d = DateDiff2(dtTime.Date.copy(), dtShiftFrom);
                        d.Minutes += d.Hours > 0 ? d.Hours * 60 : 0;
                        
                        lblIsLate.innerHTML = d.Minutes > LateAfterInMinutes ? (EmployeeeName +' came late') : '';
                    }
                }
                var tsDayCheck = DateDiff2(dtTime.Date, dtFirstPunchingTime);
                if(tsDayCheck.Days > 0)  {
                    alert('Time sheet entry cannot exceed 24 hours');
                    return false;
                    break;
                }
                if( (i + 1) < datalist.rows.length) {
                    txtLastExit = datalist.rows[i + 1].getElementsByTagName("input")[0];
                    if(txtLastExit.value.replace(' ', '') != '') {
                        var lblWorkTime = datalist.rows[i + 1].getElementsByTagName("span")[3];
                        dtExitTime = new Date('January 1, 2010 ' + txtLastExit.value.replace(' ', ':00 '));
                        
                        var exitTime = dtExitTime.copy();
                        
                        var diff = DateDiff(dtLastEntry, dtExitTime.copy());
                        dtExitTime = new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 '));
                        
                        var Entry = dtTime.Date.copy();
                        
                        dtTime.AddTimeSpan(diff);
                         var ExitTime = dtTime.Date.copy();
                         
                        var lblBreaktime = datalist.rows[i + 1].getElementsByTagName("span")[4];
                        if( (i > 0) && (i  % 2 == 1) ) {// assign break time
                            lblBreaktime.innerHTML = (diff.Hours > 9 ? '' : '0') + diff.Hours.toString() +  ':' + (diff.Minutes > 9 ? '' : '0') + diff.Minutes.toString(); 
                            if (Entry >= dtShiftFrom && Entry <= dtShiftTo)
                            {
                                if (dtExitTime >= dtShiftFrom && dtExitTime <= dtShiftTo)
                                    breakInShift.Add(DateDiff (Entry.copy(), ExitTime.copy()));
                            }
                            if(Entry > dtShiftFrom) { 
                                totalBreakHrs += diff.Hours > 0 ? diff.Hours : 0;
                                totalBreakMins += diff.Minutes > 0 ? diff.Minutes : 0;
                            }
                        }
                        if( ( (i + 1) % 2 == 1) ) // assign work time
                        {
                            var wt = new EmptyTimeSpan();
                            
                            if(Entry <= dtShiftFrom && ExitTime >= dtShiftTo){
                                wt.Hours = durationHrs > 0 ? durationHrs : wt.Hours;
                                wt.Minutes = durationMins > 0 ? durationMins : wt.Minutes;
                                
                                if(wt.Hours == 0 && wt.Minutes == 0)
                                    wt = DateDiff2(dtShiftTo, dtShiftFrom);
                            }
                            else if(Entry >= dtShiftFrom && ExitTime <= dtShiftTo){
                                wt = DateDiff2(ExitTime, Entry);
                            }
                            else if(Entry < dtShiftFrom && (ExitTime >= dtShiftFrom && ExitTime <= dtShiftTo))
                                wt = DateDiff2(ExitTime, dtShiftFrom);
                            else if(Entry >= dtShiftFrom && Entry <= dtShiftTo)
                            {
                                if(ExitTime > dtShiftTo)
                                    wt = DateDiff2(dtShiftTo, Entry);
                                else
                                    wt = DateDiff2(ExitTime, Entry);
                            }
                                
                            totalHr += wt.Hours;
                            totalMin += wt.Minutes;
                        
                            lblWorkTime.innerHTML = (diff.Hours > 9 ? '' : '0') + diff.Hours.toString() +  ':' + (diff.Minutes > 9 ? '' : '0') + diff.Minutes.toString(); 
                            if(ExitTime > dtShiftTo)
                            {
                                if(Entry < dtShiftTo){
                                    if(!overTimeAdd)
                                    {
                                        tsOverTime.Add(DateDiff2(ExitTime, dtShiftTo));
                                    }
                                }
                                else
                                    tsOverTime.Add(DateDiff2(ExitTime, Entry));
                                    
                                overTimeAdd = true;
                            } 
                        }
                        if(shiftType == 'Flexi')  { // in flexi timing, work time befor "shift from" is not considered as over time.calculation of time to be excluded from over time
                            if(dtEntry.Date < dtShiftFrom && ( (i + 1) % 2 == 1)) {
                                var tsEarly;
                                if(dtTime.Date < dtShiftFrom) {
                                    var s = dtExitTime;
                                    tsEarly = DateDiff2(dtTime.Date, dtExitTime);
                                    earlyHrs += tsEarly.Hours > 0 ? tsEarly.Hours : 0;
                                    earlyMins += tsEarly.Minutes > 0 ? tsEarly.Minutes : 0;
                                }
                                else if(dtTime.Date >= dtShiftFrom) {
                                    tsEarly = DateDiff2(dtShiftFrom, dtEntry.Date);
                                    earlyHrs += tsEarly.Hours > 0 ? tsEarly.Hours : 0;
                                    earlyMins += tsEarly.Minutes > 0 ? tsEarly.Minutes : 0;
                                }
                            }
                        }
                        if(dtTime.Date > dtShiftTo && !stopWorkTimeCalculation && shiftType != 'Flexi') {// stop futher work time calculation
                            stopWorkTimeCalculation = true;
                            var diff2 = DateDiff(dtEntry.Date, dtShiftTo);
                           
                            if(dtLastEntry <= dtShiftTo)  {
                                var diff3 = DateDiff(dtShiftTo, dtTime.Date);
                                totalOverTimeHrs += diff3.Hours > 0 ? diff3.Hours : 0 ;
                                totalOverTimeMins += diff3.Minutes > 0 ? diff3.Minutes : 0;
                                overTimeAdded = true; 
                                
                                // if total work time exceeds duration, adjust work time
                                if(totalHr > durationHrs || (totalHr == durationHrs && totalMin > durationMins))
                                {
                                    // worktime exceeded shift time
                                    totalHr = durationHrs > 0 ? durationHrs : totalHr;
                                    totalMin = durationMins > 0 ? durationMins : totalBreakHrs;
                                }
                            }
                        }
                        if(stopWorkTimeCalculation && !overTimeAdded && (i % 2 == 0)) {
                            totalOverTimeHrs += diff.Hours + (diff.Days > 0 ? (diff.Days * 24) : 0);
                            totalOverTimeMins += diff.Minutes;
                        }
                        dtEntry.AddTimeSpan(diff);
                    }
                } 
            }
            if(shiftType == 'Flexi' || shiftType == 'Dynamic') {
                 if(totalMin >= 60) {
                      totalHr += parseInt(totalMin / 60);
                      totalMin = totalMin % 60;
                 }
                 if( (durationHrs < totalHr) || (durationHrs == totalHr && durationMins < totalMin)) {
                      totalOverTimeHrs += totalHr - durationHrs;
                      totalOverTimeMins += totalMin - durationMins;
                      totalHr = durationHrs;
                      totalMin = durationMins;
                 }
            }
           
            var arrBreakTime = lblAllowedBreakTime.innerHTML.split(':');
            if(arrBreakTime.length == 2) {
                alllowedBreakHrs = parseInt(eval(arrBreakTime[0]));
                allowedBreakMins = parseInt(eval(arrBreakTime[1]));
               
                lblExtraBreak.innerHTML = '00:00';

                if(breakInShift.Hours > alllowedBreakHrs || (breakInShift.Hours == alllowedBreakHrs && breakInShift.Minutes > allowedBreakMins)) {
                    // calculate extra break time
                    var excessHrs = breakInShift.Hours - alllowedBreakHrs;
                    var excessMins = 0;
                    if(breakInShift.Minutes < allowedBreakMins)
                    {
                        excessHrs -= 1;
                        excessMins = (breakInShift.Minutes + 60) - allowedBreakMins;
                    } 
                    else
                        excessMins = breakInShift.Minutes - allowedBreakMins;
                        
                    lblExtraBreak.innerHTML = (excessHrs < 10 ? '0' : '') + excessHrs.toString() + ':' + (excessMins < 10 ? '0' : '') + excessMins.toString();
                } 
            }
            if(totalMin >= 60) {
                totalHr += parseInt(totalMin / 60);
                totalMin = totalMin % 60;
            }
            if(totalOverTimeMins >= 60) {
                totalOverTimeHrs += parseInt(totalOverTimeMins / 60);
                totalOverTimeMins = totalOverTimeMins % 60;
            }
            if(earlyMins >= 60) {
                earlyHrs += parseInt(earlyMins / 60);
                earlyMins = earlyMins % 60;
            }
            
            if(tsOverTime.Hours > 0 || tsOverTime.Minutes > 0)
            {
                var overtimeHrs = tsOverTime.Hours;
                if(tsOverTime.Days > 0)
                    overtimeHrs += tsOverTime.Days * 24;
                    
                lblOverTime.innerHTML =  (overtimeHrs < 10 ? '0' : '') + overtimeHrs.toString() + ':' + (tsOverTime.Minutes < 10 ? '0' : '') + tsOverTime.Minutes.toString();
            }
            else
                lblOverTime.innerHTML = '00:00';

            lblTotalWorkTime.innerHTML = (totalHr < 10 ? '0' : '') + totalHr.toString() + ':' + (totalMin < 10 ? '0' : '') + totalMin.toString();
            
            lblIsEarly.innerHTML = '';
            
            if( datalist.rows.length >= 2  && shiftType != 'Flexi' && DataEntryFound) 
            {
                if(dtTime.Date < dtShiftTo)
                {
                    var d = DateDiff2(dtShiftTo, dtTime.Date);
                    d.Minutes += d.Hours > 0 ? d.Hours * 60 : 0;
                    
                    lblIsEarly.innerHTML =  d.Minutes > LateAfterInMinutes ? (EmployeeeName + ' went early') : '';
                }
            } 
            else lblIsEarly.innerHTML = '';
        }
    }
    return true;
}

function FormatTimeSpan(timeSpan)
{
   return (timeSpan.Hour < 10 ? '0' : '') + timeSpan.Hour.toString() + ':' +  (timeSpan.Minutes < 10 ? '0' : '') + timeSpan.Minutes.toString()
}

function GetLastShift(table)
{
    var shift = new Shift();
    var shifts = GetSplitShifts(table);
    
    if(shifts.length > 0)
    {
        shift = shifts[0];
        var OrderNo = shifts[0].OrderNo;
        for(var i = 0; i < shifts.length; i++)
        {
            if(shifts[i].OrderNo > OrderNo)
            {
                shift = shifts[i];
                OrderNo = shifts[i].OrderNo;
            }
        }
    }
    return shift;
}

function ProcessTime(dtEntryTime, dtExitTime, dtShiftTo, CalculateOTBasedOnMinWorkTime, table, dtFirstPunchingTime, dtLastExit)
{
    var timing = { "WorkTime": new EmptyTimeSpan(), "OverTime": new EmptyTimeSpan() };
    
    var ts = new EmptyTimeSpan ();
    var tsOverTime = new EmptyTimeSpan();

    var shifts = GetSplitShifts(table);
    var overTimeCalculated = false;

    try
    {
        if (shifts.length > 0)
        {
            var LastShift = GetLastShift(table);
         
            for (var i = 0; i < shifts.length; i++)
            {
                var shift = shifts[i];
                
                ts = new EmptyTimeSpan();
                
                if(dtEntryTime >= shift.FromTime && dtEntryTime <= shift.ToTime)
                {
                    // entry time between current shift
                    if(dtExitTime <= shift.ToTime)
                        ts = DateDiff2(dtExitTime, dtEntryTime);
                    else
                        ts = DateDiff2(shift.ToTime, dtEntryTime);
                }
                else if(dtExitTime >= shift.FromTime && dtExitTime <= shift.ToTime)
                {
                    if(dtEntryTime < shift.FromTime)
                        ts = DateDiff2(dtExitTime, shift.FromTime);
                }
                else if (dtEntryTime < shift.FromTime)
                {
                    if(dtExitTime > shift.ToTime)
                    {
                        if(dtExitTime < shift.ToTime)
                            ts = DateDiff2(dtExitTime, shift.FromTime);
                        else
                            ts = DateDiff2(shift.ToTime, shift.FromTime);
                    }
                }
               timing.WorkTime.Add(ts);
            }
            
            if (CalculateOTBasedOnMinWorkTime)
            {
                if(dtExitTime > LastShift.ToTime)
                {
                    if (dtExitTime > LastShift.ToTime && dtEntryTime < dtShiftTo)
                    {
                        // calculate overtime
                        if (dtEntryTime >= LastShift.FromTime && dtEntryTime <= LastShift.ToTime)
                        {
                            if (dtExitTime < dtShiftTo)
                                timing.OverTime.Add( DateDiff2(dtExitTime, LastShift.ToTime));
                            else
                                timing.OverTime.Add( DateDiff2(dtShiftTo, LastShift.ToTime));
                        }
                        else //if(dtEntryTime >= LastShift.FromTime && dtExitTime > LastShift.ToTime)
                        {
                            if (dtExitTime < dtShiftTo)
                                timing.OverTime.Add( DateDiff2(dtExitTime, dtShiftTo));
                            else
                            {
                                if(dtEntryTime > LastShift.ToTime)
                                {
                                    if(dtEntryTime > dtShiftTo)
                                        timing.OverTime.Add( DateDiff2(dtExitTime, dtEntryTime));
                                    else
                                    {
                                         if(dtExitTime < dtShiftTo)
                                            timing.OverTime.Add( DateDiff2(dtExitTime, dtEntryTime));    
                                         else
                                            timing.OverTime.Add( DateDiff2(dtShiftTo, dtEntryTime)); 
                                    }
                                }
                                else
                                    timing.OverTime.Add( DateDiff2(dtShiftTo, LastShift.ToTime));
                            }
                        } // else if
                    } // if
                } // if
            }
            
            if (dtExitTime > dtShiftTo && dtExitTime > LastShift.ToTime)
            {
                if (dtEntryTime > dtShiftTo)
                    timing.OverTime.Add( DateDiff2(dtExitTime, dtEntryTime));
                else
                    timing.OverTime.Add( DateDiff2(dtExitTime, dtShiftTo));
            }
            else
            {
                if(dtLastExit.getDate() > dtFirstPunchingTime.getDate())
                {
                    // working time continuing to next day
                    if(dtEntryTime > dtShiftTo)
                        timing.OverTime.Add( DateDiff2(dtLastExit, dtEntryTime));
                    else
                        timing.OverTime.Add( DateDiff2(dtLastExit, dtShiftTo));
                }
            }
        }
    }
    catch(ex) { }
    
    return timing
}

function HandleSplitShift(datalist, lblTotalWorkTime)
{
    var lblOverTime = document.getElementById(datalist.id.replace(/dlEntry/,"lblOverTime"));  
    var hfDuration = document.getElementById(datalist.id.replace(/dlEntry/,"hfDuration"));
    var hfFromTime = document.getElementById(datalist.id.replace(/dlEntry/,"hfFromTime"));
    var hfToTime = document.getElementById(datalist.id.replace(/dlEntry/,"hfToTime"));
    var lblAllowedBreakTime = document.getElementById(datalist.id.replace(/dlEntry/,"lblAllowedBreakTime"));
    var lblExtraBreak = document.getElementById(datalist.id.replace(/dlEntry/,"lblExtraBreak"));
    var lblIsEarly = document.getElementById(datalist.id.replace(/dlEntry/,"lblIsEarly"));
    var lblIsLate = document.getElementById(datalist.id.replace(/dlEntry/,"lblIsLate"));
    var lblEmployee = document.getElementById(datalist.id.replace(/dlEntry/,"lblEmployee"));
    var hfFlexi = document.getElementById(datalist.id.replace(/dlEntry/,"hfFlexi"));
    var shiftType = hfFlexi.value;
    var EmployeeeName = lblEmployee.innerHTML;
    var txtLastExitTime, txtLastExit, dtLastEntry, totalOverTimeHrs = 0, totalOverTimeMins = 0, DataEntryFound = false;
    var totalMin = 0, totalHr = 0; // total working hours and minutes
    var totalBreakHrs = 0, totalBreakMins = 0, allowedBreakMins = 0, alllowedBreakHrs = 0, durationMins = 0, durationHrs = 0;
    var stopWorkTimeCalculation = false, overTimeAdded = false, earlyHrs = 0, earlyMins = 0;
    var dtTime, dtEntry, dtEntryLatest;
    var lblShiftDesc = document.getElementById(datalist.id.replace(/dlEntry/,"lblShift"));  
    var tblShifts = document.getElementById(datalist.id.replace(/dlEntry/,"hiddenGridShifts"));  
    var lblBackupShiftDesc = document.getElementById(datalist.id.replace(/dlEntry/,"lblBackupShiftDesc"));
    var hfIsMinWorkOT = document.getElementById(datalist.id.replace(/dlEntry/,"hfIsMinWorkOT"));
    var CalculateOTBasedOnMinWorkTime = hfIsMinWorkOT.value == '1' ? true : false;
    var breakInShift = new EmptyTimeSpan();
    
    var hfEarlyBefore = document.getElementById(datalist.id.replace(/dlEntry/,"hfEarlyBefore"));
    var hfLateAfter = document.getElementById(datalist.id.replace(/dlEntry/,"hfLateAfter"));
    
    var EarlyBeforeInMinutes = parseInt(hfEarlyBefore.value);
    var LateAfterInMinutes = parseInt(hfLateAfter.value);

    if(datalist != null && typeof(datalist) !== 'undefined')
    {
        var dtShiftTo = new Date('January 1, 2010 ' + hfToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        var dtShiftFrom = new Date('January 1, 2010 ' + hfFromTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        var dtFirstPunchingTime ;
        if(dtShiftFrom > dtShiftTo) dtShiftTo =  new Date('January 2, 2010 ' + hfToTime.value.replace('AM', ':00 AM').replace('PM', ':00 PM'));
        
        if(datalist.rows.length > 0) {
            for (var i = 0; i < datalist.rows.length ;  i++) {
                overTimeAdded = false;
                txtLastExitTime = datalist.rows[i].getElementsByTagName("input")[0];
                if(!DataEntryFound) {
                    if( (txtLastExitTime.value.replace(' ', '').length) > 0) DataEntryFound = true;
                }
                if(datalist.rows.length == 1 && txtLastExitTime.value.replace(' ', '').length == 0)
                {
                    lblShiftDesc.innerHTML = lblBackupShiftDesc.innerHTML;
                    return;
                }
                dtLastEntry = new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 '));
                if(!dtTime){ 
                    dtTime = new CustomDate(dtLastEntry); 
                    dtEntryLatest = new CustomDate(dtLastEntry);
                    dtFirstPunchingTime = new Date('January 1, 2010 ' + datalist.rows[0].getElementsByTagName("input")[0].value.replace(' ', ':00 '));
                }
                if(!dtEntry) 
                    dtEntry = new CustomDate(new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 ')));
                if( i == 0 && DataEntryFound) { // check if employee is late
                    var d = DateDiff2(dtTime.Date.copy(), dtShiftFrom);
                        d.Minutes += d.Hours > 0 ? d.Hours * 60 : 0;
                        
                    lblIsLate.innerHTML = d.Minutes > LateAfterInMinutes ? (EmployeeeName +' came late') : '';
                }
                var tsDayCheck = DateDiff2(dtTime.Date, dtFirstPunchingTime);
                if(tsDayCheck.Days > 0)  {
                    alert('Time sheet entry cannot exceed 24 hours');
                    return false;
                    break;
                }
                if( (i + 1) < datalist.rows.length) {
                    txtLastExit = datalist.rows[i + 1].getElementsByTagName("input")[0];
                    if(txtLastExit.value.replace(' ', '') != '') {
                        var lblWorkTime = datalist.rows[i + 1].getElementsByTagName("span")[3];
                        dtExitTime = new Date('January 1, 2010 ' + txtLastExit.value.replace(' ', ':00 '));
                        var ExitTime = dtExitTime.copy();
                        var diff = DateDiff(dtLastEntry.copy(), dtExitTime.copy());
                        dtExitTime = new Date('January 1, 2010 ' + txtLastExitTime.value.replace(' ', ':00 '));
                        dtTime.AddTimeSpan(diff);
                        if( (i + 1) % 2 == 0)
                            dtEntryLatest.Date = dtTime.Date.copy();
                            
                        var lblBreaktime = datalist.rows[i + 1].getElementsByTagName("span")[4];
                        if( (i > 0) && (i  % 2 == 1) ) {// assign break time
                        
                            if (dtLastEntry >= dtShiftFrom && dtLastEntry <= dtShiftTo)
                            {
                                if (dtExitTime >= dtShiftFrom && dtExitTime <= dtShiftTo)
                                    breakInShift.Add(DateDiff (dtLastEntry.copy(), ExitTime.copy()));
                            }
                            lblBreaktime.innerHTML = (diff.Hours > 9 ? '' : '0') + diff.Hours.toString() +  ':' + (diff.Minutes > 9 ? '' : '0') + diff.Minutes.toString(); 
                            if(dtLastEntry > dtShiftFrom) { 
                                totalBreakHrs += diff.Hours > 0 ? diff.Hours : 0;
                                totalBreakMins += diff.Minutes > 0 ? diff.Minutes : 0;
                            }
                        }
                        if( ( (i + 1) % 2 == 1) ) // assign work time
                        {
                            lblWorkTime.innerHTML = (diff.Hours > 9 ? '' : '0') + diff.Hours.toString() +  ':' + (diff.Minutes > 9 ? '' : '0') + diff.Minutes.toString(); 
                            var Timing = ProcessTime(dtEntryLatest.Date.copy(), dtTime.Date.copy(), dtShiftTo.copy(), CalculateOTBasedOnMinWorkTime, tblShifts, dtFirstPunchingTime, dtTime.Date.copy());

                            if(typeof(Timing) === 'object')
                            {
                                totalHr += Timing.WorkTime.Hours > 0 ? Timing.WorkTime.Hours : 0;
                                totalMin += Timing.WorkTime.Minutes > 0 ? Timing.WorkTime.Minutes : 0;
                                
                                totalOverTimeHrs += Timing.OverTime.Hours > 0 ? Timing.OverTime.Hours : 0;
                                totalOverTimeMins += Timing.OverTime.Minutes > 0 ? Timing.OverTime.Minutes : 0;
                            }
                        }
                        if(stopWorkTimeCalculation && !overTimeAdded && (i % 2 == 0)) {
                            totalOverTimeHrs += diff.Hours + (diff.Days > 0 ? (diff.Days * 24) : 0);
                            totalOverTimeMins += diff.Minutes;
                        }
                        dtEntry.AddTimeSpan(diff);
                    }
                } 
            }
            var arrBreakTime = lblAllowedBreakTime.innerHTML.split(':');
            if(arrBreakTime.length == 2) {
                alllowedBreakHrs = parseInt(eval(arrBreakTime[0]));
                allowedBreakMins = parseInt(eval(arrBreakTime[1]));

                lblExtraBreak.innerHTML = '00:00';

                if(breakInShift.Hours > alllowedBreakHrs || (breakInShift.Hours == alllowedBreakHrs && breakInShift.Minutes > allowedBreakMins)) {
                    // calculate extra break time
                    var excessHrs = breakInShift.Hours - alllowedBreakHrs;
                    var excessMins = 0;
                    if(breakInShift.Minutes < allowedBreakMins)
                    {
                        excessHrs -= 1;
                        excessMins = (breakInShift.Minutes + 60) - allowedBreakMins;
                    } 
                    else
                        excessMins = breakInShift.Minutes - allowedBreakMins;
                        
                    lblExtraBreak.innerHTML = (excessHrs < 10 ? '0' : '') + excessHrs.toString() + ':' + (excessMins < 10 ? '0' : '') + excessMins.toString();
                }
            }
            if(totalMin >= 60) {
                totalHr += parseInt(totalMin / 60);
                totalMin = totalMin % 60;
            }
            if(totalOverTimeMins >= 60) {
                totalOverTimeHrs += parseInt(totalOverTimeMins / 60);
                totalOverTimeMins = totalOverTimeMins % 60;
            }
            if(earlyMins >= 60) {
                earlyHrs += parseInt(earlyMins / 60);
                earlyMins = earlyMins % 60;
            }
            if(totalOverTimeHrs > 0 || totalOverTimeMins > 0 ) lblOverTime.innerHTML =  (totalOverTimeHrs  < 10 ? '0' : '') + totalOverTimeHrs.toString() + ':' + (totalOverTimeMins < 10 ? '0' : '') + totalOverTimeMins.toString();
            else  lblOverTime.innerHTML = '00:00';
            lblTotalWorkTime.innerHTML = (totalHr < 10 ? '0' : '') + totalHr.toString() + ':' + (totalMin < 10 ? '0' : '') + totalMin.toString();
            if( datalist.rows.length >= 2  && shiftType != 'Flexi' && DataEntryFound) {
            
                if(dtTime.Date < dtShiftTo)
                {
                    var d = DateDiff2(dtShiftTo, dtTime.Date);
                    d.Minutes += d.Hours > 0 ? d.Hours * 60 : 0;
                    
                    lblIsEarly.innerHTML =  d.Minutes > LateAfterInMinutes ? (EmployeeeName + ' went early') : '';
                 }
            } 
            else lblIsEarly.innerHTML = '';
        }
    }
    return true;
}

function CheckOrder(datalist)
{    
    var bFlag = false;
    var entry = '', exit = '';
    var objTimeSpan;
    if(datalist.rows.length > 1) {
        for(var i = 0; i < datalist.rows.length; i++) {
            if(datalist.rows[i].getElementsByTagName("input")[0].type == "text") {
                if(datalist.rows[i].getElementsByTagName("input")[0].value != "") {
                    if((i + 1) < datalist.rows.length && datalist.rows[i + 1].getElementsByTagName("input")[0].value != "")
                         exit = datalist.rows[i + 1].getElementsByTagName("input")[0].value.replace(' ', '');
                }
                if(datalist.rows.length != 1) {
                    if(datalist.rows[i].getElementsByTagName("input")[0].value != "")
                        entry = datalist.rows[i].getElementsByTagName("input")[0].value.replace(' ', '');
                }
                if(entry != '' && exit != '') {
                    var objTimeSpan = new TimeSpan(exit, entry);
                    bFlag = (objTimeSpan.Hour < 0 || objTimeSpan.Minutes < 0) ? true : false;
                }
            }
        }
    }   
    if(bFlag) {
        alert('Exit time should be greater than entry time.');
        return false;
    }
    else
        return true;            
}

function CheckHasTimeEntry(btn,validationGroup)
{
    var datalist = document.getElementById(btn.replace(/btnSave/,"dlEntry"));
    var hfRequest = document.getElementById(btn.replace(/btnSave/,"hfRequest"));
    if(typeof(Page_ClientValidate) == "function") {
        Page_ClientValidate(validationGroup);
    }
    if(!Page_IsValid)  return false;
    else {
        for(var i = 0; i < datalist.rows.length; i++) {    
            Entry = datalist.rows[i].getElementsByTagName("input")[0];
            if(Entry.value == "") {
                alert("Entry time is empty.");
                return false;
            }
            else {
                if(CalculateTimeSheet(btn.replace(/btnSave/,"ibtnNewEntry"), false)) {//TimeCalculations(btn.replace(/btnSave/,"ibtnNewEntry"),true)
                    var result = confirm('Do you want to send as request?');
                    hfRequest.value = (result) ? 1 : 0;
                    return true;
                }
                else {
                    return false;
                }
            }
        }
    }  
}

function CheckTimeOrder(btn)
{   
    var grid = document.getElementById(btn.replace(/btnSave/, "gvNewEntry"))
    for(var i = 0; i < grid.rows[1].cells.length; i++) {   
        if(grid.getElementsByTagName("input")[i].type == "text") {               
            var txtBox = grid.getElementsByTagName("input")[i];
            if(CheckOrder(txtBox.id))
                return true;
            else
                return false;
        }
    }  
}
