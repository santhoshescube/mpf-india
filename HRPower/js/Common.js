﻿function trimNumber(number)
{
    while(number.substring(0, 1) == '0' && number.length > 1)
    {
        number = number.substring(1, 9999);
    }
    
    return (number == "" ? 0 : parseInt(number));
}

function ParseInt(string)
{
    return trimNumber(string.toString());
}

function trim(val)
{
    return val.replace(/^\s+/, '').replace(/\s+$/, '');
}

function isEmpty(ctl)
{
    if(trim(ctl.value) == "")
    {
        return true;
    }
    else
    {
        return false;
    }
}

function EnableProjectDate()
{
    
}

function isEmail(value)
{
    var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
    
    if(pattern.test(trim(value)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

function isImage(ctl)
{
    var validExtensions = new Array("bmp", "dib", "jpg", "jpeg", "jpe", "jfif", "gif", "tif", "tiff", "png");
    
    var img = document.getElementById(ctl).value;
    if(img!='')
    {
    var extension = img.substring(img.lastIndexOf('.') + 1, img.length);
    
    extension = extension.toLowerCase();
    
    for(var i = 0; i < validExtensions.length; i++)
    {
        if(validExtensions[i] == extension)
        {
            return true;
        }
    }
    
    return false;
    }
    else
    {
        return true;
    }
}
function isDoc(ctl)
{
    var validExtensions = new Array("doc", "pdf", "txt", "rtf", "docx");
    
    var img = document.getElementById(ctl).value;
    if(img!='')
    {
        var extension = img.substring(img.lastIndexOf('.') + 1, img.length);
        
        extension = extension.toLowerCase();
        
        for(var i = 0; i < validExtensions.length; i++)
        {
            if(validExtensions[i] == extension)
            {
                return true;
            }
        }
        
        return false;
    }
    else
    {
        return true;
    }
}

function isDocument(fileName)
{
    var validExtensions = new Array("doc", "pdf", "txt", "rtf", "docx");

    var extension = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length);
        
    extension = extension.toLowerCase();
    
    for(var i = 0; i < validExtensions.length; i++)
    {
        if(validExtensions[i] == extension)
        {
            return true;
        }
    }
    
    return false;
}

function toggleDiv(ancId)
{
    var div = document.getElementById(ancId.replace(/anc/, "div"));
    
    if(div != null)
    {
        div.style.display = (div.style.display == "block" ? "none" : "block");
        div.style.visibility = (div.style.visibility == "visible" ? "hidden" : "visible");
        
        var img = document.getElementById(ancId.replace(/anc/, "img"));                
        if(img != null) { img.src = (img.nameProp == "collapsed.gif" ? "/images/expanded.gif" : "/images/collapsed.gif"); }
    }
}

function toggleAnimate(ancId)
{
    var div = document.getElementById(ancId.replace(/anc/, "div"));
    
    var divArr = document.getElementsByTagName("div");
    
    if(div != null)
    {
        for(var i = 0; i < divArr.length; i++)
        {
            if(divArr[i].id.lastIndexOf(div.id.substring(div.id.lastIndexOf('_') + 1, div.id.length)) != -1)
            {
                if(divArr[i] != div)
                {
                    divArr[i].style.display = "none";
                }
            }
        }
        
        SlideUpSlideDown(div.id, 'medium');
    }
}

function toggleImgAnimate(imgId)
{
    var div = document.getElementById(imgId.replace(/img/, "div"));
    
    var img = document.getElementById(imgId);
        
    img.src = (div.style.display == "block" ? "../images/Hide.GIF" : "../images/Show.GIF");
    
    var divArr = document.getElementsByTagName("div");
    
    if(div != null)
    {
        for(var i = 0; i < divArr.length; i++)
        {
            if(divArr[i].id.lastIndexOf(div.id.substring(div.id.lastIndexOf('_') + 1, div.id.length)) != -1)
            {
                if(divArr[i] != div)
                {
                    divArr[i].style.display = "none";
                }
            }
        }
        SlideUpSlideDown(div.id, 'medium');
    }
}

function SelectAllChildNodes(e)
{
    var obj = (e.srcElement == undefined ? e.target : e.srcElement);
    var treeNodeFound = false;

    var checkedState;
    if (obj.tagName == "INPUT" && obj.type == "checkbox")
    {
        var treeNode = obj;
        checkedState = treeNode.checked;
        do
        {
            obj = obj.parentNode;
        } while (obj.tagName != "TABLE")
        
        var parentTreeLevel = obj.rows[0].cells.length;            
        var parentTreeNode = obj.rows[0].cells[0];
        var tables = obj.parentNode.getElementsByTagName("TABLE");
        var numTables = tables.length;
        if (numTables >= 1)
        {
            for (iCount=0; iCount < numTables; iCount++)
            {
                if (tables[iCount] == obj)
                {
                    treeNodeFound = true;
                    iCount++;
                    if (iCount == numTables)
                    {
                        return;
                    }
                }
                if (treeNodeFound == true)
                {
                    var childTreeLevel = tables[iCount].rows[0].cells.length;
                    if (childTreeLevel > parentTreeLevel)
                    {
                        var cell = tables[iCount].rows[0].cells[childTreeLevel - 1];
                        var inputs = cell.getElementsByTagName("INPUT");
                        inputs[0].checked = checkedState;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }
}

function openModalDialog(url, width, height)
{
    var settings = "dialogHeight: " + height + "px; dialogWidth: " + width + "px; dialogTop: px; dialogLeft: px; edge: Raised; center: Yes; resizable: No; status: No;";   
    
    var myArgs = null;
    
    window.showModalDialog(url, myArgs, settings);
}

function openWindow(url, width, height)
{
    var settings = "height = " + height + "px, width = " + width + "px, resizable = 0, status = 0, toolbar = 0, menubar = 0, top = 30px";   
    window.open(url, "Compose", settings);
}


var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
	    this[i] = 31
	    if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
	    if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr, source){
    if(dtStr == "") { 
        return false; 
    }
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strDay=dtStr.substring(0,pos1)
    var strMonth=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
	    if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        if(source.innerText == undefined) { source.textContent = "The date format should be : dd/mm/yyyy"; }
        else { source.innerText = "The date format should be : dd/mm/yyyy"; }
	    return false
    }
    if (strMonth.length<1 || month<1 || month>12){
        if(source.innerText == undefined) { source.textContent = "Please enter a valid month"; }
        else { source.innerText = "Please enter a valid month"; }
	    return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        if (month == 2 && day > daysInFebruary(year)) {
            if(source.innerText == undefined) { source.textContent = year + " is not leap year"; }
            else { source.innerText = year + " is not leap year"; }
	        return false
        }
        else{
            if(source.innerText == undefined) { source.textContent = "Please enter a valid day"; }
            else { source.innerText = "Please enter a valid day"; }
	        return false
        }
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        if(source.innerText == undefined) { source.textContent = "Please enter a valid 4 digit year between "+minYear+" and "+maxYear; }
        else { source.innerText = "Please enter a valid 4 digit year between "+minYear+" and "+maxYear; }
	    return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        if(source.innerText == undefined) { source.textContent = "Please enter a valid date"; }
        else { source.innerText = "Please enter a valid date"; }
	    return false
    }
return true
}

function isValidDate(dtStr){
    if(dtStr == "") { 
        return false; 
    }
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strDay=dtStr.substring(0,pos1)
    var strMonth=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
	    if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        return false
    }
    if (strMonth.length<1 || month<1 || month>12){
        return false
    }
    if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        if (month == 2 && day > daysInFebruary(year)) {
            return false
        }
        else{
            return false
        }
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        return false
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        return false
    }
return true
}

function Convert2Date(dtStr)
{
    var pos1 = dtStr.indexOf('/');
    var pos2 = dtStr.indexOf('/',pos1+1);
    var strDay = dtStr.substring(0,pos1);
    var strMonth = dtStr.substring(pos1+1,pos2);
    if (strDay.charAt(0) == "0" && strDay.length>1) strDay = strDay.substring(1);
    if (strMonth.charAt(0) == "0" && strMonth.length>1) strMonth = strMonth.substring(1);
    strMonth = strMonth - 1;
    var strYear = dtStr.substring(pos2+1);
    
    var date = new Date(strYear, strMonth, strDay);
    
    return date;
}
function changeBackground(ctrl_id)
{
    var chk = document.getElementById(ctrl_id);
    var lnkMail = document.getElementById("ctl00_ctl00_content_lnkMail");
    
    if(chk.checked==true)
    {
        chk.parentNode.parentNode.className='rowSelect';
    }
    else
    {
        chk.parentNode.parentNode.className='row';
    }
    
    var grid = document.getElementById(ctrl_id.substring(0, ctrl_id.lastIndexOf('_ctl')));
        
    var elements = grid.getElementsByTagName("INPUT");
    
    var count = 0;
    
    for(var i = 0; i < elements.length; i++)
    {
        if(elements[i].type == "checkbox")
        {
            count++;
        }
    }
    
    if(count > 0)
    {
        var chks = new Array(count);
        
        var index = 0;
        
        for(var i = 0; i < elements.length; i++)
        {
            if(elements[i].type == "checkbox")
            {
                chks[index] = elements[i];
                index++;
            }
        }
        
        var chkAll = chks[0];
        
        var checked = false;
        var allchecked = true;
        
        var LeadId = "";
        
        for(var i = 1; i < chks.length; i++)
        {
            if(chks[i].checked)
            {
                checked = true;
                var hdLeadId = document.getElementById(chks[i].id.replace(/chk/, "hdLeadId"));
                if(hdLeadId != null)
                {
                    var ancMail = document.getElementById(hdLeadId.id.replace(/hdLeadId/,"ancMail"));
                    var email = (ancMail.innerText == undefined ? ancMail.textContent : ancMail.innerText);
                    if(email != "")
                    {
                        LeadId += "," + hdLeadId.value;
                    }
                }
            }
            else
            {
                allchecked = false;
            }
        }
        
        if(LeadId != "")
        {
            LeadId = LeadId.substring(1, LeadId.length);
            lnkMail.href = "Mail.aspx?LeadId=" + LeadId;
        }
        else
        {
            lnkMail.href = "#";
        }
        
        if(checked)
        {
            if(allchecked)
            {
                chkAll.checked = true;
            }
            else
            {
                chkAll.checked = false;
            }
        }
        else
        {
            chkAll.checked = false;
        }
    }
}

function isChecked(gridId)
{
    var grid = document.getElementById(gridId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        return true;
    }
    else
    {
        alert('Please select at least an item');
        
        return false;
    }
}


function Print(ctrlId)
{          
    var printContent = document.getElementById(ctrlId);
    if(printContent.innerHTML =="<TBODY></TBODY>")
    {
        alert("Nothing to print.");
        return;
    }
    var windowUrl = 'about:blank';
    var uniqueName = new Date();
    var windowName = 'Print' + uniqueName.getTime();
//    var printWindow = window.open(windowUrl, windowName, 'left=' + (this.screen.availWidth + 100) + ',top=' + (this.screen.availHeight + 100) + ',width=0,height=0');
    var printWindow = window.open(windowUrl, windowName, 'resizable=1,scrollbars=1,left=' + (100) + ',top=' + (100) + ',width=595,height=' + (this.screen.availHeight - 200));

    printWindow.document.write('<html><head><style type="text/css">body{font-family: Tahoma;font-size: 11px;color: Black;background-color: White;margin: 5px;width: 100%;}.style1{width: 100%;}</style>');
    printWindow.document.write('<script type="text/javascript">function doPrint(ctl) { ctl.style.display = "none"; this.print(); this.close(); } </script></head><body><div style="float: right;"><a href="#" onclick="doPrint(this);"><img src="../images/Printer.png" alt="Print" border="0" /></a></div>');
    printWindow.document.write(printContent.innerHTML);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.focus();
}


function valPrintEmailDatalist(datalistId)
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var hidControl = document.getElementById("ctl00_hidCulture");
     
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        return true;
    }
    else
    {
         message = hidControl.value == 'ar-AE' ? 'الرجاء اختيار ما لا يقل عن عنصر ' : 'Please select at least an item';
         alert(message);
        
         return false;
    }
}

function valDeleteDatalist(datalistId)
{

    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var hidControl = document.getElementById("ctl00_hidCulture");
      
    
    var count = 0;
    
    var checked = false;
    
   
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        //if(IsArabic == "False")
        
          if(hidControl.value == "ar-AE")
            return confirm ('هل أنت متأكد أنك تريد حذف العناصر المحددة؟');
        else
            return confirm('Are you sure you want to delete the selected items?');
       
        
    }
    else
    { 
       // if(IsArabic == "False")
//       if(hidControl.value == "en-US")
//            alert('Please select an item to remove');
//        else
//            alert('يرجى تحديد العنصر المراد ازالته');
        
         if(hidControl.value == "ar-AE")
             alert('يرجى تحديد العنصر المراد ازالته');
        else
           alert('Please select an item to remove');
        
        return false;
    }
}

function valDeleteDatalistNew(datalistId)
{


    var datalist = document.getElementById(datalistId);
    
    var chks = datalist.getElementsByTagName("INPUT");
    
    
   var hidControl = document.getElementById("ctl00_hidCulture");
      
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
       // if(IsArabic =="False")
         if(hidControl.value == "en-US")
            return confirm('Are you sure to delete the selected items?');
        else
            return confirm('هل أنت متأكد أنك تريد حذف العناصر المحددة؟');
    }
    else
    {
        //if(IsArabic =="False")
          if(hidControl.value == "en-US")
            alert('Please select an item to remove');
        else
            alert('يرجى تحديد عنصر لإزالة');
             
        return false;
    }
}



function valDeleteVacancyDatalist(datalistId)     //job post
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked && chks[i].id.lastIndexOf("chkSelect")!=-1) { checked = true; }
        }
    }
    
    if(checked)
    {
        return confirm('Are you sure to cancel the selected vacancies?');
    }
    else
    {
        alert('Please select a vacany to cancel');
        
        return false;
    }
}

function valPrintEmailVacancyDatalist(datalistId)   //job post
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked && chks[i].id.lastIndexOf("chkSelect")!=-1) { checked = true; }
        }
    }
    
    if(checked)
    {
        return true;
    }
    else
    {
        alert('Please select atleast a vacancy');
        
        return false;
    }
}



function showMailDialog(query)
{

    openWindow("ComposeMail.aspx?" + query, 750, 610);
}

function addToTextBox(chk, txt)
{
    var td = chk.parentNode;
    
    var str = (td.innerText == undefined ? td.textContent : td.innerText);
    
    if(chk.checked)            
    {
        if(txt.value == "")
        {
            txt.value = str;
        }
        else
        {
            txt.value = txt.value + "," + str;
        }
    }
    else
    {
        if(txt.value != "")
        {
            if(txt.value.lastIndexOf(str) != -1)
            {
                var regExp = new RegExp("," + str, "g");
                txt.value = txt.value.replace(regExp, "");
                regExp = new RegExp(str + ",", "g");
                txt.value = txt.value.replace(regExp, "");
                regExp = new RegExp(str, "g");
                txt.value = txt.value.replace(regExp, "");
            }
        }
    }
}

function GetSysDate()
{
    return Convert2Date(document.getElementById('ctl00_ctl00_hdCurrentDate').value);
}

function isContainerChecked(containerId, chkId)
{
    var chks = document.getElementById(containerId).getElementsByTagName("INPUT");
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox" && chks[i].id.lastIndexOf(chkId) != -1)
        {
            if(chks[i].checked)
            {
                checked = true;
            }
        }
    }
    
    return checked;
}

function confirmAction(containerId, chkId, listingType, action, requireConfirm)
{

    if(listingType.substring(listingType.length - 1, listingType.length) == 's')
    {
        listingType = listingType.substring(0, listingType.length - 1);
    }
     
    var container = document.getElementById(containerId);
    // check to see if container element exists in the page., if not return false.
    if(container == null || typeof(container) === 'undefined') return false; 
        
    if(isContainerChecked(containerId, chkId))
    {
        if(requireConfirm) { return confirm('Are you sure to ' + action + ' the selected ' + listingType + 's'); }
        else { return true; }
    }
    else
    {
        var article = 'a';
        
        var vowels = new Array ( 'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U' );
        
        for(var i = 0; i < vowels.length; i++)
        {
            if(vowels[i] == listingType.substr(0, 1))
            {
                article = 'an';
            }
        }
        
        alert('Please select at least ' + article + ' ' + listingType + ' to ' + action);
        return false;
    }
}
function valMakeHotDatalist(datalistId)  //Make candidate hot in candidate page
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    var IsAllHot = true;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked)
             { 
                checked = true; 
                var img = document.getElementById(chks[i].id.replace(/chkSelect/, "imgCandidateRating"));
                
                if(img.src.lastIndexOf("images/SavedStar.png")==-1)
                {
                    IsAllHot=false;    
                }
             }
        }
    }
    if(IsAllHot == true && checked==true)
    {
        //return confirm('Are you sure to make hot the selected items?');
        alert('The selected candidate(s) is/are already hot');
        return false;
    }
    else if(checked==true)
    {
        return confirm('Are you sure to make hot the selected candidate(s)?');
    }
    else
    {
        alert('Please select atleast a candidate');
        
        return false;
    }
}

function valAssociateDatalist(datalistId)  //Associate candidates in candidate page
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(checked)
    {
        return confirm('Are you sure to associate the selected candidates with the vacancy ?');
    }
    else
    {
        alert('Please select atleast a candidate');
        
        return false;
    }
}

function valMakeHotVacancyDatalist(datalistId)  //Make vacancy hot in Job post page
{
    var datalist = document.getElementById(datalistId);
    
    var chks = document.getElementsByTagName("INPUT");
    
    var count = 0;
    
    var checked = false;
    
    var IsAllHot = true;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked && chks[i].id.lastIndexOf("chkSelect")!=-1)
            {
                checked = true;
                
                var img = document.getElementById(chks[i].id.replace(/chkSelect/, "imgVacancyRating")); 
                
                if(img.src.lastIndexOf("images/SavedStar.png")==-1)
                {
                    IsAllHot=false;    
                }
            }
        }
    }
    
    if(IsAllHot == true && checked == true)
    {
        //return confirm('Are you sure to make hot the selected vacancies?');
        alert('The selected vacancy(s) is/are already hot');
        return false;
    }
    else if(checked==true)
    {
        return confirm('Are you sure to make hot the selected vacancy(s)?');
    }
    else
    {
        alert('Please select atleast a vacancy');
        
        return false;
    }
}

// Function for table row display - browser compatible function
function trDisplay()
{
    if(navigator.appName == 'Microsoft Internet Explorer')
    {
        return "block";
    }
    else
    {
        return "block";//table-row
    }
}

// Slides up or slides down an element according to it's display property
//
// Parameters
//      an_element_id : ID of the element to be animated.
//      speed         : speed for the animation - options  (string value: 'slow', 'medium', 'fast' or  numeric values ( in milliseconds) )
//
// eg: 1. SlideUpSlideDown('ctl_100_txt', 'slow')       2. SlideUpSlideDown('ctl_100_txt', 1000) 
function SlideUpSlideDown(an_element_id, speed)
{


    var AnimationSpeed = (speed == 'default') ? 'slow' : speed;
    if(document.getElementById(an_element_id).style.display == 'none')
         $('#'+an_element_id).slideDown(AnimationSpeed);
    else
        $('#'+an_element_id).slideUp(AnimationSpeed);
}


var NumericValidChars = '1234567890.';
var StringValidChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,-()[]=+&%$@!?/\"*';

/*
Summary
    Restrict user's inupt characters according to the DataType

e:
    Event (keypress)

DataType:
    Input DataType :- options (Number, String)

AdditionalCharacters:
    Additional characters to be allowed apart from valid characters of DataType

eg: txtElement.Attributes["onkeypress"] = "return KeyRestrict(event, 'String', '-&*()');
*/

function KeyRestrict(e, DataType, AdditionalCharacters)
{
    var KeyCode = (e.keyCode) ? e.keyCode : e.which;
    
    var ValidCharacters;
    
    switch(DataType)
    {
        case 'Number':
            ValidCharacters = NumericValidChars + AdditionalCharacters;
            break;
        
        case 'String':
            ValidCharacters = StringValidChars + NumericValidChars + AdditionalCharacters;
            break;
            
        default:
            ValidCharacters = StringValidChars + NumericValidChars + AdditionalCharacters;
            break;
    }
    
    // 13 - enterkey, 8 - back space, 37 - left arrow, 39 - right arrow  
    if(KeyCode == 13 || KeyCode == 8 || KeyCode == 37 || KeyCode == 39) // enter key
        return true;
        
    if(KeyCode == null)
        return true;
    
    // get character from keyCode.
    var KeyChar = String.fromCharCode(KeyCode);
           
    if(ValidCharacters.indexOf(KeyChar) != -1)
        return true;
    else 
        return false;
    
}

/*Function for checking any offers selected for sending*/

function chkOffers(datalistId)
{   
    var datalist = document.getElementById(datalistId);
    
    var chks = datalist.getElementsByTagName("input");
    var bFlag = false;
    
    var hidControl = document.getElementById("ctl00_hidCulture");
      
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            if(chks[i].checked)
            {
                 bFlag = true;           
            }
        }
    }        
    if(bFlag)
    {
        return true;
    }
    else
    {
     if(hidControl.value == "en-US")
     {
       alert('Please select an item');
        return false;
     }
     else
     {
       alert(' يرجى تحديد عنصر');
        return false;
     }
      
    }
}

function checkOfferLetterCancel(datalistId)
{
    var selected = chkOffers(datalistId);
    
     var hidControl = document.getElementById("ctl00_hidCulture");
    
    if(selected)
    {
        if(hidControl.value == "en-US")
         {
            return confirm('Are you sure to cancel selected offer-letter(s)');
         }
        else
        {
            
            return confirm('هل أنت متأكد لإلغاء خطاب العرض مختارة');
        }
    }
    else 
        return false;
}


function showMenu(id)
{
    document.getElementById(id).style.display = 'block';
}
function hideMenu(id)
{
    document.getElementById(id).style.display = 'none';
}

function MakeParentMenuActive(anchID)
{
    var nav = document.getElementById("nav");
                        
    var list = nav.getElementsByTagName("li");
    
    for(var i = 0; i < list.length; i++)
    {
        list[i].id = null;
    }
    
    var element = document.getElementById(anchID);
    
    if(element != null)
    {
        element.parentNode.id = 'current';
    }
}

function RestrictLength(e, maxLength, ctrl)
{
    var KeyCode = (e.keyCode) ? e.keyCode : e.which;
    
   if(ctrl.value.selected) return true;
    
     // 13 - enterkey, 8 - back space, 37 - left arrow, 39 - right arrow , 46 - Delete key
   if(KeyCode == 13 || KeyCode == 8 || KeyCode == 37 || KeyCode == 39 || KeyCode == 46) // enter key
        return true;
        
    return (ctrl.value.length <= maxLength);
}

function RestrictMulilineLength(ctrl, maxLength)
{
 var hidControl = document.getElementById("ctl00_hidCulture");
 if(ctrl.value.length > maxLength)
   {
        ctrl.value = ctrl.value.substring(0, maxLength);
        if(hidControl.value == "en-US")
            alert( 'Only ' + maxLength + ' characters allowed.' );
        else
            alert(+ maxLength +' فقط الأحرف المسموح بها ');
    }
}

function closePopup(popupControlID)
{
    var ctrl = document.getElementById(popupControlID);
    
    if(ctrl)
    {
        ctrl.style.display = 'none';
        return false;
    }
}

function checkPercentage(source,args)
{  

    var dlAddMoreQualifications=document.getElementById(source.id.replace(/cvCheckPercentage/, "dlAddMoreQualifications"));
    var isValid=false;
    var _value = 100; 
    
    if(dlAddMoreQualifications!=null)
    {
        var controls = dlAddMoreQualifications.getElementsByTagName("INPUT");
             
        for(var i = 0; i < controls.length; i++)
        {
             if(controls[i].id.lastIndexOf('txtPercentage') != -1)
             {
                if(controls[i].value >  _value)
                {
                    aisValid=false;
                    source.innerHTML = 'Percentage should not exceed 100';
                }
                else
                    isValid=true;
             }
                 
        }
        args.IsValid=isValid;
        
    }
          
}


function RestrictMulilineLengthCandidate(ctrl, maxLength)
{   
    var chkAddress=document.getElementById(ctrl.id.substr(0,ctrl.id.lastIndexOf('_')+1) + "chkAddress");
    var txtCurrentAddress=document.getElementById(ctrl.id.substr(0, ctrl.id.lastIndexOf('_') + 1) + "txtCurrentAddress");
    var txtPermanentAddress=document.getElementById(ctrl.id.substr(0, ctrl.id.lastIndexOf('_') + 1) + "txtPermanentAddress");
    
//    if(chkAddress.checked)
//    {
//        txtCurrentAddress.value=ctrl.value;
//        txtPermanentAddress.value=ctrl.value;
//    }

    if(ctrl.value.length > maxLength)
    {
        ctrl.value = ctrl.value.substring(0, maxLength);
        
        alert( 'Only ' + maxLength + ' characters allowed.' );
    }
    
    
}


function setVisibility(ctrl)
{
    var txtAmount = document.getElementById(ctrl.id.substr(0,ctrl.id.lastIndexOf('_')+1) + "txtAmount");
    var ddlDeductionPolicy =   document.getElementById(ctrl.id.substr(0,ctrl.id.lastIndexOf('_')+1) + "ddlDeductionPolicy");
    if(txtAmount.value!="")
    {
        ddlDeductionPolicy.selectedIndex=0;
    }
   if(ddlDeductionPolicy.selectedvalue>0)
    {
        txtAmount.value="";
    }
}

function setVisibility2(ctrl)
{
    var txtAmount = document.getElementById(ctrl.id.substr(0,ctrl.id.lastIndexOf('_')+1) + "txtAmount");
    var ddlDeductionPolicy =   document.getElementById(ctrl.id.substr(0,ctrl.id.lastIndexOf('_')+1) + "ddlDeductionPolicy");
    if(ddlDeductionPolicy.selectedIndex==0)
    {
        txtAmount.value="";
    }
   else
    {
        txtAmount.value="";
    }
}

function valRenewDatalist(datalistId)
{

    var datalist = document.getElementById(datalistId);
    var hidControl = document.getElementById("ctl00_hidCulture");
    
    var chks = document.getElementsByTagName("INPUT");
    var message="";
    
    var count = 0;
    
    var checked = false;
    
    for(var i = 0; i < chks.length; i++)
    {
        if(chks[i].type == "checkbox")
        {
            count++;
            
            if(count == 1) { continue; }
            
            if(chks[i].checked) { checked = true; }
        }
    }
    
    if(!checked)
    {
        message = hidControl.value == 'ar-AE' ? 'هل أنت متأكد من تجديد العناصر المحددة؟':'Are you sure to renew the selected items?'
        return confirm(message);
    }
}
// Projects 
  function CheckSelectionindatalistList(source,args)
    {
    
       var dlEmployees = document.getElementById(source.id.substr(0, source.id.lastIndexOf('_')) + '_dlEmployeesList');
       if(dlEmployees !=null)
       {
         
            var chks = dlEmployees.getElementsByTagName("INPUT");
            
            var hidControl = document.getElementById("ctl00_hidCulture");              
            
            var count = 0;
            
            var checked = false;
            
           
            
            for(var i = 0; i < chks.length; i++)
            {
                if(chks[i].type == "checkbox")
                {
                    count++;
                    
                    if(count == 1) { continue; }
                    
                    if(chks[i].checked) { checked = true; }
                }
            }
            
             if(checked)
             {
                args.IsValid = true;
                return;
             }
             else
             {
                args.IsValid = false;               
            }
        }
    }
    
    
    
    
    function isValidFile(ctl)
{
    var validExtensions = new Array("bmp", "dib", "jpg", "jpeg", "jpe", "jfif", "gif", "tif", "tiff", "png","pdf","doc","docx","xls","xlsx","txt");
    
    var img = document.getElementById(ctl).value;
    if(img!='')
    {
    var extension = img.substring(img.lastIndexOf('.') + 1, img.length);
    
    extension = extension.toLowerCase();
    
    for(var i = 0; i < validExtensions.length; i++)
    {
        if(validExtensions[i] == extension)
        {
            return true;
        }
    }
    
    return false;
    }
    else
    {
        return true;
    }
}