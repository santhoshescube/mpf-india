﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="PerformanceAction.aspx.cs" Inherits="Public_PerformanceAction" Title="Performance Action" %>

<%@ Register Src="../Controls/AssignEvaluators.ascx" TagName="AssignEvaluators" TagPrefix="uc1" %>
<%@ Register src="../Controls/PerformanceView.ascx" tagname="PerformanceView" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <%-- <ul>
            <li><a href="SalesPerformance.aspx">Target Performance</a></li>
            <li><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
            <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li class='selected'><a href="PerformanceAction.aspx">Performance Action</a></li>
        </ul>--%>
        
             <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
        
        
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    
    function Save()
    {
        Page_ClientValidate();
        
        if(Page_IsValid)
            return confirm('Are you sure you want to save performance initiation ?');
        else
        return false;
    }
    </script>


<div id="dvHavePermission" runat ="server" >
    <asp:UpdatePanel ID="updEmployees" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 100%">
                <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
                   <%-- Department--%>
                   
                   <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,Department %>'></asp:Literal>
                </div>
                <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                    <asp:DropDownList ID="ddlDepartment" Width="200px" AutoPostBack ="true"  runat="server" CssClass="textbox_mandatory"
                        MaxLength="25" onselectedindexchanged="ddlDepartment_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="ddlDepartment"
                        CssClass="error" Display="Dynamic" InitialValue="-1" ErrorMessage="Please select a department"
                        SetFocusOnError="False" ValidationGroup="SaveInitiation"></asp:RequiredFieldValidator>
                </div>
                <div class="firstTrLeft" style="float: left; width: 20%; height: 29px">
                   <%-- Emloyee--%>
                   <asp:Literal ID="Literal2" runat ="server" Text ='<%$Resources:ControlsCommon,Employee %>'></asp:Literal>
                </div>
                <div class="firstTrRight" style="float: left; width: 73%; height: 29px; padding-bottom: 5px;">
                    <asp:DropDownList ID="ddlEmployee" Width="200px" runat="server" AutoPostBack ="true"  CssClass="textbox_mandatory"
                        MaxLength="25" onselectedindexchanged="ddlEmployee_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                        CssClass="error" Display="Dynamic"  ErrorMessage='<%$Resources:ControlsCommon,PleaseSelectEmployee %>'
                        SetFocusOnError="False" ValidationGroup="SaveInitiation"></asp:RequiredFieldValidator>
                </div>
             
                <div>
                    <div style="margin-top: 10px; min-width: 100%; height: 550px; overflow: scroll">
                        <div id="dvEmployeeList" visible = "false" runat="server">
                            <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeeList" Width="100%"
                                 runat="server" BackColor="White"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Table"
                                CellPadding="3" GridLines="Both" >
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                                <SeparatorStyle BackColor="AliceBlue" />
                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 45px; vertical-align: top">
                                                <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                    Text='<%$Resources:ControlsCommon,SLNO %>'></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                   Text='<%$Resources:ControlsCommon,FromDate %>'></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="Label3" CssClass="labeltext" runat="server" ForeColor="White"  Text='<%$Resources:ControlsCommon,ToDate %>'></asp:Label>
                                            </td>
                                            <td style="width: 80px; vertical-align: top">
                                                <asp:Label ID="Label4" CssClass="labeltext" runat="server" ForeColor="White"  meta:resourcekey='AvgMark'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label1" CssClass="labeltext" runat="server" ForeColor="White"  meta:resourcekey='AvgGrade'></asp:Label>
                                            </td>
                                             <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label2" CssClass="labeltext" runat="server" ForeColor="White"  meta:resourcekey='MaxGrade'></asp:Label>
                                            </td>
                                              <td style="width: 80px; vertical-align: top">
                                                <asp:Label ID="Label5" CssClass="labeltext" runat="server" ForeColor="White"  meta:resourcekey='MaxMark'></asp:Label>
                                            </td>
                                             <td style="width: 20px; vertical-align: top">
                                                                                  

                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 45px; vertical-align: top">
                                                <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" 
                                                    Text='<%# Eval("SLNO") %>' ></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="lblFromDate" CssClass="labeltext" runat="server"  Text='<%# Eval("FromDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="Label3" CssClass="labeltext" runat="server"  Text='<%# Eval("ToDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 80px; vertical-align: top">
                                                <asp:Label ID="Label4" CssClass="labeltext" runat="server"  Text='<%# Eval("AvgMark") %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label1" CssClass="labeltext" runat="server" Text='<%# Eval("AvgGrade") %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label2" CssClass="labeltext" runat="server"  Text='<%# Eval("MaxGrade") %>'></asp:Label>
                                            </td>
                                             <td style="width: 80px; vertical-align: top">
                                                <asp:Label ID="Label6" CssClass="labeltext" runat="server"  Text='<%# Eval("Total") %>'></asp:Label>
                                            </td>
                                            
                                             <td style="width: 20px; vertical-align: top">
                                                         
                                                     <asp:ImageButton Height ="20px"  CommandArgument='<%# Eval("PerformanceInitiationID") %>'   onclick="btnDetails_Click"  ID="Button1" runat ="server"  ImageUrl="~/images/View visa request.png" ToolTip='<%$Resources:ControlsCommon,ViewDetails %>'  />         
                                                                                  

                                               
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <div style="margin-top:10px; float: right; width: 15%">
                                <asp:Button ID="btnAction" CssClass="btnsubmit" runat="server"  meta:resourcekey="Action" 
                                    onclick="btnAction_Click" />
                                   <asp:Button ID="btnActionCancel" CssClass="btnsubmit" runat="server" 
                                     Text ='<%$Resources:ControlsCommon,Cancel %>' onclick="btnActionCancel_Click" />
                            </div>
                        </div>
                        <div id="dvNoRecord" runat="server"  style="text-align: center">
                            <asp:Label ID="lblNoRecord" CssClass="error " runat="server" Text='<%$Resources:ControlsCommon,NoRecordFound %>'></asp:Label>
                        </div>
                    </div>
                </div>
                
            </div>
            
            
           
            
        </ContentTemplate>
    </asp:UpdatePanel>
    
    
      
    
     <asp:UpdatePanel ID="updEvaluationDetails" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
           
         <AjaxControlToolkit:ModalPopupExtender Drag="true" 
                PopupControlID="dvEvaluationDetails" PopupDragHandleControlID="dvEvaluationDetails" TargetControlID="btn" ID="mdEvaluationDetails"
                runat="server">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btn" runat="server" Style="display: none" />
            <div id="dvEvaluationDetails" style="width:85%;height:80%;overflow:scroll" >
              <uc2:PerformanceView ID="EvaluationDetails" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    </div>
     <div id="dvNoPermission" runat ="server"  style="margin-top:10px;display :none;text-align :center ; width: 100%">
    <asp:Label  ID="lblNoPermission" runat ="server" CssClass ="error" Text ="You have no permission to view this page">
    </asp:Label>
    </div> 
    
    
    
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_action.png" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkAdd" runat="server"  Text ='<%$Resources:ControlsCommon,AddAction %>' CausesValidation="false" OnClick="lnkAdd_Click"> 
		
	<%--Add Action--%>
	
	</asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/view_performance_action.png" />
                </div>
                <div class="name">
                 <h5>   <asp:LinkButton ID="lnkViewInitiation" runat="server" Text ='<%$Resources:ControlsCommon,ViewAction %>'  CausesValidation="false" OnClick="lnkViewInitiation_Click"> 
		
	<%--View Action--%>
	
	</asp:LinkButton></h5>
                </div>
                </a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
