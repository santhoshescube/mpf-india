﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="EmployeeSalaryCertificate.aspx.cs" Inherits="Public_EmployeeSalaryCertificate"
    Title="Salary Certificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Employee.aspx'><span>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
            <li class="selected"><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeDutyRoaster.aspx">
                <%-- View Policies--%>
               <asp:Literal ID="Literal56" runat="server" Text='<%$Resources:MasterPageCommon,DutyRoaster %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upAdd" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="divhead">
                <div class="divleft">
                    <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,Department%>'></asp:Literal>
                </div>
                <div class="divright">
                    <asp:DropDownList ID="ddldepartment" runat="server" Width="200px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddldepartment_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="divleft">
                    <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ControlsCommon,Employee%>'>
                    </asp:Literal>
                </div>
                <div class="divright">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                DataValueField="EmployeeID" DataTextField="Employee" AutoPostBack="true" BackColor="Info"
                                OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                         Display="Dynamic" ErrorMessage='*' ValidationGroup="Submit" CssClass="error"
                        InitialValue="-1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="divhead">
                <div class="divleft">
                    <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'>
                    </asp:Literal>
                </div>
                <div class="divright">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlTemplates" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                AutoPostBack="true" BackColor="Info" OnSelectedIndexChanged="ddlTemplates_SelectedIndexChanged">
                            </asp:DropDownList>                          
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTemplates"
                       Display="Dynamic" ErrorMessage='*' ValidationGroup="Submit" CssClass="error"
                        InitialValue="-1"></asp:RequiredFieldValidator>
                </div>
                <div class="divleft">
                    <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ControlsCommon,Email%>'>
                    </asp:Literal>
                </div>
                <div class="divright">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEmail" runat="server" Enabled="false" Width="200px"></asp:TextBox>
                             
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="divhead" style="height: 300px">
                <div class="divleft">
                    <asp:Literal ID="Literal2" runat="server">
                    </asp:Literal>
                </div>
                <div class="divright" style="height: 300px">
                    <asp:UpdatePanel ID="upContent" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <AjaxControlToolkit:Editor ID="ajcSalaryCertificate" runat="server" Height="300px"
                                Width="600px" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlTemplates" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="divhead">
                <div class="divleft">
                </div>
                <div class="divright" style="width: 400px">
                  <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                    <asp:Button ID="btnPreview" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Preview%>'
                        Text='<%$Resources:ControlsCommon,Preview%>' ValidationGroup="Submit" Width="75px"
                        OnClick="btnPreview_Click" />&nbsp;
                    <asp:Button ID="btnPrint" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Print%>'
                        CausesValidation="true" ValidationGroup="Submit" Text='<%$Resources:ControlsCommon,Print%>'
                        Width="75px" OnClick="btnPrint_Click" />
                    &nbsp;
                    <asp:Button ID="btnSend" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Send%>'
                        CausesValidation="true" ValidationGroup="Submit" Text='<%$Resources:ControlsCommon,Send%>'
                        Width="75px" OnClick="btnSend_Click" />
                        </ContentTemplate>
                        </asp:UpdatePanel>
                </div>
            </div>
            <div style="width: 100%; height: 50px; float: left; margin-top: 20px;">
                <asp:Panel ID="pnlPrint" runat="server" Style="display: none;">
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none">
        <input type="button" runat="server" id="btn" />
    </div>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" meta:resourcekey="Submit" /><%--Text="submit"--%>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
       
    </asp:UpdatePanel>
    <AjaxControlToolkit:ModalPopupExtender ID="mpePreview" runat="server" PopupControlID="pnlPreview"
        TargetControlID="btn" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlPreview"
        CancelControlID="ibtnClose">
    </AjaxControlToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlPreview" runat="server" Width="600px">
        <%--Style="display: none;"--%>
        <div id="popupmainwrap">
            <div id="popupmain" style="width: 700px;">
                <div id="header11">
                    <div id="hbox1">
                        <div id="headername" runat="server" style="color: White;">
                            <%--Preview Offer Letter--%>
                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="PreviewOfferLetter"></asp:Literal>
                        </div>
                    </div>
                    <div id="hbox2">
                        <div id="close1">
                            <a href="">
                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png" />
                            </a>
                        </div>
                    </div>
                </div>
                <div id="contentwrap">
                    <div id="content">
                        <div style="max-height: 400px; overflow: auto;">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="100%">
                                        <asp:UpdatePanel ID="upPreview" runat="server">
                                            <ContentTemplate>
                                                <div style="width: 100%; float: left; margin-top: 5px;">
                                                    <div class="trRight" style="width: 95%; height: 500px; float: left;">
                                                        <asp:Literal ID="literalContent" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                
    
                <div id="footerwrapper">
                    <div id="footer11">
                        <div id="buttons">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
</asp:Content>
