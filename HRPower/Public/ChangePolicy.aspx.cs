﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_ChangePolicy : System.Web.UI.Page
{
    clsChangePolicy objclsChangePolicy;
    protected void Page_Load(object sender, EventArgs e)
    {
        ChangePolicyPager.Fill += new controls_Pager.FillPager(BindGrid);

        if (!IsPostBack)
        {

            LoadCombo();
        }
    }

    private void LoadCombo()
    {
        objclsChangePolicy = new clsChangePolicy();
        DataSet dtsData = objclsChangePolicy.GetCombo();
        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyID";
                ddlCompany.DataSource = dtsData.Tables[0];
                ddlCompany.DataBind();
            }
            if (dtsData.Tables[1].Rows.Count > 0)
            {
                DataRow dr = dtsData.Tables[1].NewRow();
                dr["EmployeeFullName"] = "Any";
                dr["EmployeeID"] = "0";
                dtsData.Tables[1].Rows.InsertAt(dr, 0);
                ddlEmployee.DataTextField = "EmployeeFullName";
                ddlEmployee.DataValueField = "EmployeeID";
                ddlEmployee.DataSource = dtsData.Tables[1];
                ddlEmployee.DataBind();
            }
            if (dtsData.Tables[2].Rows.Count > 0)
            {
                ddlDepartment.DataTextField = "Department";
                ddlDepartment.DataValueField = "DepartmentID";
                ddlDepartment.DataSource = dtsData.Tables[2];
                ddlDepartment.DataBind();
            }
            if (dtsData.Tables[3].Rows.Count > 0)
            {
                DataRow dr = dtsData.Tables[3].NewRow();
                dr["Designation"] = "Any";
                dr["DesignationID"] = "0";
                dtsData.Tables[3].Rows.InsertAt(dr, 0);
                ddlDesignation.DataTextField = "Designation";
                ddlDesignation.DataValueField = "DesignationID";
                ddlDesignation.DataSource = dtsData.Tables[3];
                ddlDesignation.DataBind();
            }
        }

    }

    protected void gvChangePolicy_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow gr;
        gr = ((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent);
        DropDownList ddlPayStructure = (DropDownList)gr.FindControl("ddlPayStructure");
        DropDownList ddlWorkPolicy = (DropDownList)gr.FindControl("ddlWorkPolicy");
        DropDownList ddlLeavePolicy = (DropDownList)gr.FindControl("ddlLeavePolicy");
        DropDownList ddlAbsentPolicy = (DropDownList)gr.FindControl("ddlAbsentPolicy");
        DropDownList ddlEncashPolicy = (DropDownList)gr.FindControl("ddlEncashPolicy");
        DropDownList ddlHolidayPolicy = (DropDownList)gr.FindControl("ddlHolidayPolicy");
        DropDownList ddlOvertimePolicy = (DropDownList)gr.FindControl("ddlOvertimePolicy");

         switch (e.CommandName)
        {
            case "Add":

                objclsChangePolicy = new clsChangePolicy();
                objclsChangePolicy.intEmployeeID = e.CommandArgument.ToInt32();
                objclsChangePolicy.intWorkPolicyID = ddlWorkPolicy.SelectedValue.ToInt32();
                objclsChangePolicy.intLeavePolicyID= ddlLeavePolicy.SelectedValue.ToInt32();
                objclsChangePolicy.intAbsentPolicyID= ddlAbsentPolicy.SelectedValue.ToInt32();
                //objclsChangePolicy.intEncashPolicyID= ddlEncashPolicy.SelectedValue.ToInt32();
                objclsChangePolicy.intHolidayPolicyID = ddlHolidayPolicy.SelectedValue.ToInt32();
                objclsChangePolicy.intOvertimePolicyID = ddlOvertimePolicy.SelectedValue.ToInt32();
                objclsChangePolicy.intPayStructureID= ddlPayStructure.SelectedValue.ToInt32();
                objclsChangePolicy.AddPolicy();

                break;

            case "Resume":

                //gvUserSettings.EditIndex = -1;

                //BindGrid();

                break;

            case "Alter":

                //int RoleID;
                //RoleID = objUserMaster.GetRoleId();
                //if (RoleID != 1 && RoleID != 2 && RoleID != 3)
                //{
                //    bool IsUpdate = false;
                //    DataTable dtm = (DataTable)ViewState["Permission"];
                //    if (dtm.Rows.Count > 0)
                //    {
                //        IsUpdate = dtm.Rows[0]["IsUpdate"].ToBoolean();

                //    }
                //    if (IsUpdate == false)
                //    {

                //        break;
                //    }
                //}

                gvChangePolicy.EditIndex = gr.RowIndex;

                BindGrid();

                upGridView.Update();

                break;
        }
    }

    protected void gvChangePolicy_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //objUserMaster = new clsUserMaster();

        if (e.Row.RowIndex == gvChangePolicy.EditIndex)
        {
            DropDownList ddlWorkPolicy = (DropDownList)e.Row.FindControl("ddlWorkPolicy");
            DropDownList ddlLeavePolicy = (DropDownList)e.Row.FindControl("ddlLeavePolicy");
            DropDownList ddlAbsentPolicy = (DropDownList)e.Row.FindControl("ddlAbsentPolicy");
            DropDownList ddlEncashPolicy = (DropDownList)e.Row.FindControl("ddlEncashPolicy");
            DropDownList ddlHolidayPolicy = (DropDownList)e.Row.FindControl("ddlHolidayPolicy");
            DropDownList ddlOvertimePolicy = (DropDownList)e.Row.FindControl("ddlOvertimePolicy");
            DropDownList ddlPayStructure = (DropDownList)e.Row.FindControl("ddlPayStructure");

            objclsChangePolicy = new clsChangePolicy();
            DataSet dtsData = objclsChangePolicy.GetGridCombo();
            if (dtsData.Tables.Count > 0)
            {
                if (dtsData.Tables[0].Rows.Count > 0)
                {
                    if (ddlWorkPolicy != null)
                    {
                        ddlWorkPolicy.DataTextField = "WorkPolicy";
                        ddlWorkPolicy.DataValueField = "WorkPolicyID";
                        ddlWorkPolicy.DataSource = dtsData.Tables[0];
                        ddlWorkPolicy.DataBind();
                    }
                }
                if (dtsData.Tables[1].Rows.Count > 0)
                {
                    if (ddlLeavePolicy != null)
                    {
                        ddlLeavePolicy.DataTextField = "LeavePolicyName";
                        ddlLeavePolicy.DataValueField = "LeavePolicyID";
                        ddlLeavePolicy.DataSource = dtsData.Tables[1];
                        ddlLeavePolicy.DataBind();
                    }
                }
                if (dtsData.Tables[2].Rows.Count > 0)
                {
                    if (ddlAbsentPolicy != null)
                    {
                        ddlAbsentPolicy.DataTextField = "AbsentPolicy";
                        ddlAbsentPolicy.DataValueField = "AbsentPolicyID";
                        ddlAbsentPolicy.DataSource = dtsData.Tables[2];
                        ddlAbsentPolicy.DataBind();
                    }
                }
                if (dtsData.Tables[3].Rows.Count > 0)
                {
                    if (ddlEncashPolicy != null)
                    {
                        ddlEncashPolicy.DataTextField = "EncashPolicy";
                        ddlEncashPolicy.DataValueField = "EncashPolicyID";
                        ddlEncashPolicy.DataSource = dtsData.Tables[3];
                        ddlEncashPolicy.DataBind();
                    }
                }
                if (dtsData.Tables[4].Rows.Count > 0)
                {
                    if (ddlHolidayPolicy != null)
                    {
                        ddlHolidayPolicy.DataTextField = "HolidayPolicy";
                        ddlHolidayPolicy.DataValueField = "HolidayPolicyID";
                        ddlHolidayPolicy.DataSource = dtsData.Tables[4];
                        ddlHolidayPolicy.DataBind();
                    }
                }
                if (dtsData.Tables[5].Rows.Count > 0)
                {
                    if (ddlOvertimePolicy != null)
                    {
                        ddlOvertimePolicy.DataTextField = "OTPolicy";
                        ddlOvertimePolicy.DataValueField = "OTPolicyID";
                        ddlOvertimePolicy.DataSource = dtsData.Tables[5];
                        ddlOvertimePolicy.DataBind();
                    }
                }
                if (dtsData.Tables[6].Rows.Count > 0)
                {
                    if (ddlPayStructure != null)
                    {
                        ddlPayStructure.DataTextField = "Paystructure";
                        ddlPayStructure.DataValueField = "JobSalaryID";
                        ddlPayStructure.DataSource = dtsData.Tables[6];
                        ddlPayStructure.DataBind();
                    }
                }
            }
        }
    }

    protected void btnShow_Click(object sender, EventArgs e)
    { BindGrid(); }
    private void BindGrid()
    {

        objclsChangePolicy = new clsChangePolicy();
        objclsChangePolicy.intPageIndex = ChangePolicyPager.CurrentPage + 1;
        objclsChangePolicy.intPageSize = ChangePolicyPager.PageSize;
        objclsChangePolicy.intEmployeeID = ddlEmployee.SelectedValue.ToInt32();
        objclsChangePolicy.intWorkPolicyID = ddlCompany.SelectedValue.ToInt32();
        objclsChangePolicy.intLeavePolicyID = ddlDepartment.SelectedValue.ToInt32();
        objclsChangePolicy.intAbsentPolicyID = ddlDesignation.SelectedValue.ToInt32();
        DataSet dtsData = objclsChangePolicy.GetEmployeesPolicies();

        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                gvChangePolicy.DataSource = dtsData.Tables[0];
                gvChangePolicy.DataBind();
                if (dtsData.Tables[0].Rows.Count > 0)
                {

                    ChangePolicyPager.Total = dtsData.Tables[1].Rows[0]["Count"].ToInt32();
                }
                else
                {
                    ChangePolicyPager.Total = 0;
                }
                ChangePolicyPager.Visible = true;
            }
            else
            {
                gvChangePolicy.DataSource = null;
                gvChangePolicy.DataBind();

                ChangePolicyPager.Visible = false;
            }
        }
        else
        {
            gvChangePolicy.DataSource = null;
            gvChangePolicy.DataBind();

            ChangePolicyPager.Visible = false;
        }

    }
    protected void imgAddPayStructure_Click(object sender, ImageClickEventArgs e)
    {
        fvPayStructure.ChangeMode(FormViewMode.Insert);
        Controls_PayStructure ucPayStructure = (Controls_PayStructure)fvPayStructure.FindControl("ucPayStructure");
        AjaxControlToolkit.ModalPopupExtender mpePayStructure = (AjaxControlToolkit.ModalPopupExtender)fvPayStructure.FindControl("mpePayStructure");

        if (ucPayStructure != null && mpePayStructure != null)
        {
            mpePayStructure.Show();
            upnlPayStructure.Update();
        }
    }
    protected void btnAddPayStructure_Click(object sender, EventArgs e)
    {
        fvPayStructure.ChangeMode(FormViewMode.Insert);
        Controls_PayStructure ucPayStructure = (Controls_PayStructure)fvPayStructure.FindControl("ucPayStructure");
        AjaxControlToolkit.ModalPopupExtender mpePayStructure = (AjaxControlToolkit.ModalPopupExtender)fvPayStructure.FindControl("mpePayStructure");

        if (ucPayStructure != null && mpePayStructure != null)
        {
            mpePayStructure.Show();
            upnlPayStructure.Update();
        }
    }
}
