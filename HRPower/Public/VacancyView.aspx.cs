﻿using System;
using System.Collections;
using System.Configuration;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;

public partial class Public_VacancyView : System.Web.UI.Page
{
    #region Declarations
    clsVacancy objVacancy = new clsVacancy();
    clsUserMaster objUserMaster = new clsUserMaster();
    #endregion

    #region Events
    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            EnableMenus();
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        int JobID = Convert.ToInt32(Server.UrlDecode(Request.QueryString["JobID"].ToString()));
        ViewState["JobID"] = JobID;
        DisplayVacancyDetails(JobID);
       
    }

    public void EnableMenus()
    {
        objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        if (RoleID > 4)//RoleID based checking
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.JobOpening);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = lnkNewJob.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = lnkViewjob.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
              
                if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = true;
                else
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = false;
            }
        }
        else
        {
            lnkNewJob.Enabled = lnkViewjob.Enabled = lnkDelete.Enabled = true;
           
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }
       
        if (ViewState["IsCreate"].ToBoolean() || ViewState["IsUpdate"].ToBoolean())
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = true;
        else
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = false;

        updJobView.Update();

    }

    #region RightSideMenuEvents
    protected void lnkDeleteJob_Click(object sender, EventArgs e)
    {
        Delete();
    }
    protected void lnkAddCriteria_Click(object sender, EventArgs e)
    {
        if (CriteriaReferenceControl != null && mdlPopUpReference != null)
        {
            CriteriaReferenceControl.BindDataList();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void lnkAddparticular_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Insert);
        controls_AdditionDeduction ucAdditionDeduction = (controls_AdditionDeduction)FormView1.FindControl("ucAdditionDeduction");

        AjaxControlToolkit.ModalPopupExtender mpeAdditionDeduction = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeAdditionDeduction");
        if (ucAdditionDeduction != null && mpeAdditionDeduction != null)
        {
            ucAdditionDeduction.BindDataList();
            mpeAdditionDeduction.Show();
            upnlAdditionDeduction.Update();
        }
    }
    //protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    //{
    //    //divNoData.Visible = false;
    //    //pgrJobs.CurrentPage = 0;
    //    //BindDetails();
    //    //this.RegisterAutoComplete();
    //}
    #endregion
    #endregion

    #region Methods
    private void DisplayVacancyDetails(int JobID)
    {
        DataSet ds = clsVacancyView.GetVacancyDetails(JobID);
        dlJobDetails.DataSource = ds.Tables[0];
        dlJobDetails.DataBind();

        lbViewTotalWeightage.Text = ds.Tables[0].Rows[0]["Totalmarks"].ToString();
        lbViewPassMark.Text = ds.Tables[0].Rows[0]["PassPercentage"].ToString();
        lblPay.Text = ds.Tables[0].Rows[0]["Paystructure"].ToString();

        if (ds.Tables.Count > 1)
        {
            gvCriteria.DataSource = ds.Tables[1];
            gvCriteria.DataBind();

            if (gvCriteria.Rows.Count > 0)
                lblTotal.Text = "100";
            else
                lblTotal.Text = "0";

            gvSalaryInfo.DataSource = ds.Tables[2];
            gvSalaryInfo.DataBind();

            if (ds.Tables[4].Rows.Count > 0)
            {
                gvOffer.DataSource = ds.Tables[4];
                gvOffer.DataBind();
                trOffer.Style["display"] = "block";
            }
            else
            {
                gvOffer.DataSource = null;
                gvOffer.DataBind();
                trOffer.Style["display"] = "none";
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                lbViewAdiitions.Text = ds.Tables[2].Compute("Sum(Amount)", "IsAddition=1").ToString() == "" ? "0" : ds.Tables[2].Compute("Sum(Amount)", "IsAddition=1").ToString();
            }

            //if (ds.Tables[6].Rows.Count > 0)
            //{ 
            //    gvKPI.DataSource = ds.Tables[6];
            //    gvKPI.DataBind();
            //    trKPIKRA.Style["display"] = divKPI.Style["display"] = "block";
            //}
            //else
            //{
            //    gvKPI.DataSource = null;
            //    gvKPI.DataBind();
            //    trKPIKRA.Style["display"] = divKPI.Style["display"] =  "none";
            //}
            //if (ds.Tables[7].Rows.Count > 0)
            //{                
            //    gvKRA.DataSource = ds.Tables[7];
            //    gvKRA.DataBind();
            //    trKPIKRA.Style["display"] = divKRA.Style["display"] = "block";
            //}
            //else
            //{
            //    gvKRA.DataSource = null;
            //    gvKRA.DataBind();
            //    trKPIKRA.Style["display"] = divKRA.Style["display"] = "none";
            //    if (ds.Tables[6].Rows.Count > 0)
            //        trKPIKRA.Style["display"] = divKPI.Style["display"] = "block";
            //}
        }
        if (lnkDelete.Enabled)
        {
            lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد من حذف هذه الوظيفة ؟');") : ("return confirm('Are you sure to delete this Job?');");
        }
       
        updJobView.Update();
    }
    private void Delete()
    {
        string msg = string.Empty;
        if (clsVacancy.IsVacancyScheduled(ViewState["JobID"].ToInt32()) > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
        else if (clsVacancy.IsCandidateAssigned(ViewState["JobID"].ToInt32()) > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
        else
        {
            DeleteVacancy(ViewState["JobID"].ToInt32());
        }
    }
    private void DeleteVacancy(int JobID)
    {
        string msg = string.Empty;
        if (clsVacancy.IsUsedInGE(JobID) > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة موجود المرجعي") : ("Cannot Delete Vacancy as Reference Exists");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        else
        {
            if (objVacancy.DeleteVacancy(JobID))
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("حذف المهمة بنجاح") : ("Vacancy Deleted successfully");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                clsCommonMessage.DeleteMessages(JobID, eReferenceTypes.JobApproval);
                Response.Redirect("Vacancy.aspx");
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل الحذف") : ("Deletion Failed");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }
    }
    #endregion
}
