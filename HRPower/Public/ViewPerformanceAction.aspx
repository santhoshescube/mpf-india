﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="ViewPerformanceAction.aspx.cs" Inherits="Public_ViewPerformanceAction"
    Title="View Performance Action" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <%--     <ul>
                <li><a href="SalesPerformance.aspx">Target Performance</a></li>
                <li ><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
                <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
                <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
                <li><a href="PerformanceEvaluation.aspx"><span>Performance Evaluation</span></a></li>
                <li class='selected'><a href="PerformanceAction.aspx">Performance Action</a></li>
            </ul>--%>
            
            
            
              <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
            
            
            
            
            
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="dvHavePermission" runat="server">
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <div style="width: 100%">
                    <%--<div style="float: left; width: 80%; margin: 5px; margin-bottom: 0px">
                        <h4 id="header" runat="server" style="color: #30a5d6">
                            Evaluation Completed List :</h4>
                    </div>--%>
                    <div style="clear: both">
                    </div>
                    <div style="margin-top: 10px; min-width: 100%; height: 550px; overflow: scroll">
                        <div id="dvEmployeeList" runat="server">
                            <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeeList" Width="100%"
                                DataKeyField="PerformanceActionID" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Table" CellPadding="3" GridLines="Both"
                                OnItemCommand="dlEmployeeList_ItemCommand" OnItemDataBound="dlEmployeeList_ItemDataBound"
                                OnSelectedIndexChanged="dlEmployeeList_SelectedIndexChanged">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                                <SeparatorStyle BackColor="AliceBlue" />
                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 40px; vertical-align: top">
                                                <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                    Text='<%$Resources:ControlsCommon,SLNo %>'></asp:Label>
                                            </td>
                                            <td style="width: 270px; vertical-align: top">
                                                <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                    Text='<%$Resources:ControlsCommon,Employee %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label8" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="ActionDate"></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label3" CssClass="labeltext" runat="server" ForeColor="White"  Text='<%$Resources:ControlsCommon,FromDate %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label4" CssClass="labeltext" runat="server" ForeColor="White"  Text='<%$Resources:ControlsCommon,ToDate %>'></asp:Label>
                                            </td>
                                            <td style="width: 18px">
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 40px; vertical-align: top">
                                                <asp:Label ID="lblSlNo" CssClass="labeltext" runat="server" ToolTip='<%# Eval("SLNo") %>'
                                                    Text='<%# Eval("SLNo") %>'></asp:Label>
                                            </td>
                                            <td style="width: 270px; vertical-align: top">
                                                <asp:Label ID="lblEmployee" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EmployeeFullName") %>'
                                                    Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                <asp:HiddenField ID="hfEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                                <asp:HiddenField ID="hfIsActionDone" runat="server" Value='<%# Eval("IsActionDone") %>' />
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label9" CssClass="labeltext" runat="server" ToolTip='<%# Eval("ActionDate") %>'
                                                    Text='<%# Eval("ActionDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="lblFromDate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("FromDate") %>'
                                                    Text='<%# Eval("FromDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="lblToDate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("ToDate") %>'
                                                    Text='<%# Eval("ToDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 18px">
                                                <asp:ImageButton ID="imgEdit" runat="server" ToolTip='<%$Resources:ControlsCommon,Edit %>' ImageUrl="~/images/edit.png"
                                                    CommandArgument='<%# Eval("PerformanceActionID") %>' OnClick="imgEdit_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td colspan="4">
                                                <asp:LinkButton ID="lnkShowDetails" CssClass="labeltext" ForeColor="ActiveBorder"
                                                    Text='<%$Resources:ControlsCommon,ShowDetails %>' runat="server"></asp:LinkButton>
                                                <div style="clear: both">
                                                </div>
                                                <asp:HiddenField ID="hfShowDetails" runat="server" Value="0" />
                                                <div id="divShowDetails" style="display: none" runat="server">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label2" CssClass="labeltext" runat="server" meta:resourcekey="AnyJobPromotion"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="IsJobPromotion" Text='<%# Eval("IsJobPromotion") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label6" CssClass="labeltext" runat="server" meta:resourcekey="AlteredDesignation"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAlteredDesignation" Text='<%# Eval("AlteredDesignation") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="Label1" CssClass="labeltext" runat="server" meta:resourcekey="IsSalaryAltered"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td style="width: 40%">
                                                                <asp:Label ID="lblISSalaryAltered" Text='<%# Eval("IsSalaryAltered") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label5" CssClass="labeltext" runat="server" meta:resourcekey="AlteredSalary"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblAlteredSalary" Text='<%# Eval("AlteredSalary") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Remarks" CssClass="labeltext" runat="server"  meta:resourcekey="Remarks"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblRemarks" Text='<%# Eval("Remarks") %>' CssClass="labeltext" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label7" CssClass="labeltext" runat="server" meta:resourcekey="EmployeeFeedback"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblEmployeeFeedback" Text='<%# Eval("FeedBack") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                        
                                                          <tr>
                                                            <td>
                                                                <asp:Label ID="Label10" CssClass="labeltext" runat="server" meta:resourcekey="ApprovedBy"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label11" Text='<%# Eval("ApprovedBy") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                         
                                                        
                                                          <tr>
                                                            <td>
                                                                <asp:Label ID="Label12" CssClass="labeltext" runat="server" meta:resourcekey="ApprovedDate"></asp:Label>
                                                            </td>
                                                            <td style="width: 3%">
                                                                :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label13" Text='<%# Eval("ApprovedDate") %>' CssClass="labeltext"
                                                                    runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                        
                                                        
                                                        
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div id="dvNoRecord" runat="server" style="text-align: center">
                            <asp:Label ID="lblNoRecord" CssClass="error " runat="server" Text='<%$Resources:ControlsCommon,NoRecordFound %>'></asp:Label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="dvNoPermission" runat="server" style="margin-top: 10px; text-align: center;
        width: 100%">
        <asp:Label ID="NoPermission" runat="server" Text='<%$Resources:ControlsCommon,NoPermission %>'
            CssClass="error"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_action.png" />
                </div>
                <div class="name">
                 <h5>   <asp:LinkButton ID="lnkAdd" runat="server" Text ='<%$Resources:ControlsCommon,AddAction %>' CausesValidation="false" OnClick="lnkAdd_Click"> 
		
	<%--Add Action--%>
	
	</asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/view_performance_action.png" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkViewInitiation" Text ='<%$Resources:ControlsCommon,ViewAction %>' runat="server" CausesValidation="false" OnClick="lnkViewInitiation_Click"> 
		
	<%--View Action--%>
</asp:LinkButton>	</h5>
                </div>
                </a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
