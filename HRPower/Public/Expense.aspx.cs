﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Reflection;
using System.IO;
/// <summary> 
/// Created By:    Sanju
/// Created Date:  19-Sept-2013
/// Description:   Expense
/// </summary>
public partial class Public_Expense : System.Web.UI.Page
{

    #region EVENTS
    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        ExpensePager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {

            ViewState["PrevPage"] = Request.UrlReferrer;

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;
            string msg = string.Empty;
            clsExpense objclsExpense = new clsExpense();
            DataTable datData;

            loadCombo(1);
            int RequestID = 0;
            if (Request.QueryString["RequestId"] != null && Request.QueryString["Type"] == "Cancel")
            {
                ViewState["ExpenseID"] = RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["RequestForCancel"] = true;
                loadCombo(2);
                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;

                setDivVisibility(3);
                txtEditRemarks.Visible = true;
                lblRemarks.Visible = true;

            }
            else if (Request.QueryString["RequestId"] != null)
            {
                ViewState["ExpenseID"] = RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["Approve"] = true;



                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;


            }
            else
            {
                BindDataList();
            }
        }
    }
    private void BindForActions(int RequestID)
    {

        int EmployeeID = new clsUserMaster().GetEmployeeId();


        if (Convert.ToBoolean(ViewState["Approve"]) == true) // Approval
        {

            divApproveExpense.Style["display"] = "block";
            divCancelTheRequest.Style["display"] = "none";
            ddlApproveStatus.DataTextField = "Description";
            ddlApproveStatus.DataValueField = "StatusId";
            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("5"));

            if (!clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.Expense), Convert.ToInt64(EmployeeID), RequestID))
            {
                ddlApproveStatus.Items.Remove(new ListItem("Pending", "4"));
                divAppSettings.Style["display"] = "none";
            }
            else
            {
                divAppSettings.Style["display"] = "block";
            }

        }
        else if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            divApproveExpense.Style["display"] = "none";
            divCancelTheRequest.Style["display"] = "block";

            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("6"));
            ddlApproveStatus.Enabled = false;
        }
        clsUserMaster objUser = new clsUserMaster();
        clsExpense objExpense = new clsExpense();
        objExpense.CompanyID = objUser.GetCompanyId();
        objExpense.intExpenseRequestID = RequestID;
        DataTable dt2 = objExpense.GetApprovalHierarchy();
        if (dt2.Rows.Count > 0)
        {
            dl1.DataSource = dt2;
            dl1.DataBind();
        }
        setDivVisibility(4);

        GetRequestDetails(RequestID);
    }
    private void GetRequestDetails(int RequestId)
    {
        clsExpense objclsExpense = new clsExpense();
        objclsExpense.lngEmployeeID = 0;
        objclsExpense.intExpenseRequestID = RequestId;
        DataSet ds = objclsExpense.getExpenseRequest();
        DataTable datData = ds.Tables[0];
        string msg = string.Empty;
        if (datData.Rows.Count > 0)
        {

            lblApproveRequestedBy.Text = lblRequestedBy.Text = datData.Rows[0]["EmployeeName"].ToStringCustom();
            lblApproveType.Text = lblExpenseType.Text = datData.Rows[0]["ExpenseType"].ToStringCustom();
            ViewState["ExpenseTypeID"] = datData.Rows[0]["ExpenseHeadID"].ToInt32();
            lblApproveDate.Text = lblDate.Text = datData.Rows[0]["Date"].ToStringCustom();
            txtApproveAmount.Text = lblAmount.Text = datData.Rows[0]["Amount"].ToStringCustom();
            if (datData.Rows[0]["ApprovedAmount"].ToDecimal() > 0)
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المبلغ المعتمد") : ("Approved Amount");
                lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ApprovedAmount"].ToStringCustom() + ")";
            }
            if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 2 || datData.Rows[0]["ExpenseTransID"].ToInt32() == 3)
            {
                if (datData.Rows[0]["ActualAmount"].ToDecimal() > 0)
                {
                    if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 2)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("قيمة السداد") : ("Reimbursement Amount");
                        lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ActualAmount"].ToStringCustom() + ")";
                    }
                    else if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 3)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("خصم المبلغ") : ("Deduction Amount");
                        lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ActualAmount"].ToStringCustom() + ")";
                    }
                }
            }
            lblRequestedToNames.Text = datData.Rows[0]["RequestedToNames"].ToStringCustom();
            lblCurrentStatus.Text = lblCurrentStatus.Text = lblStatus.Text = datData.Rows[0]["Status"].ToStringCustom();

            lblRemarks.Text = lblApproveRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();//  txtEditRemarks.Text = txtApproveRemarks.Text 
            //lblRemarks.Text = ":" + datData.Rows[0]["Remarks"].ToStringCustom();

            if (ds.Tables[1].Rows.Count > 0)
            {
                dlDocumentsApproval.DataSource = ds.Tables[1];
                dlDocumentsApproval.DataBind();


                dlApprovedDocuments.DataSource = ds.Tables[1];
                dlApprovedDocuments.DataBind();
            }
        }




        txtExpenseDate.Text = DateTime.Now.Date.ToString("dd/MM/yyy");


        rblAccountability.SelectedValue = "1";//None
        txtAccountabilityAmount.Text = "0";
        txtAccountabilityAmount.Enabled = false;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        string msg = string.Empty;
        clsUserMaster objUser = new clsUserMaster();
        clsExpense objclsExpense = new clsExpense();

        //Fill Parameter
        objclsExpense.intExpenseTypeID = ddlType.SelectedValue.ToInt32();
        objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
        objclsExpense.dteFromDate = clsCommon.Convert2DateTime(txtFromDate.Text.Trim());
        objclsExpense.dteToDate = clsCommon.Convert2DateTime(txtToDate.Text.Trim());
        objclsExpense.decAmount = txtAmount.Text.Trim().ToDecimal();
        objclsExpense.strRequestTo = clsLeaveRequest.GetRequestedTo(0, objclsExpense.lngEmployeeID, (int)RequestType.Expense);
        objclsExpense.strRemarks = txtRemarks.Text.Trim();

        //Validations
        if (objclsExpense.isSalaryStructureNotExist())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مصروف غير ممكن  الحين، لم يتم إنشاء هيكل الرواتب حتى الآن") : ("Expense request not possible,Salary structure is not yet created.");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.intExpenseTypeID <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء اختيار نوع المصروف") : ("Please select expense type");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.lngEmployeeID <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك الآن") : ("Your request cannot process this time");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.dteFromDate > objclsExpense.dteToDate)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("من اكبر تاريخ الى تاريخ اليوم") : ("From date greater than to date");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.decAmount <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء ادخال مبلغ صالح") : ("Please enter a valid amount");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.strRequestTo == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك الآن . يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        if (objclsExpense.strRemarks == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الملاحظات") : ("Please enter remarks.");
            msgs.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }



        DataTable datData;
        if (hfExpenseRequestID.Value.ToInt32() > 0)
        {
            //Expense request update
            objclsExpense.intExpenseRequestID = hfExpenseRequestID.Value.ToInt32();
            datData = objclsExpense.updateExpenseRequest();
            if (datData.Rows.Count > 0)
            {
                clsCommonMessage.SendMessage(objclsExpense.lngEmployeeID.ToInt32(), hfExpenseRequestID.Value.ToInt32(), eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Request, eAction.Applied, "");
                clsCommonMail.SendMail(datData.Rows[0]["ID"].ToInt32(), MailRequestType.Expense, eAction.Applied);
                SaveExpenseDocuments(hfExpenseRequestID.Value.ToInt32());
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم التحديث بنجاح") : ("Successfully updated");

            }
        }
        else
        {
            //Expense request save
            datData = objclsExpense.saveExpenseRequest();
            if (datData.Rows.Count > 0)
            {

                clsCommonMessage.SendMessage(objclsExpense.lngEmployeeID.ToInt32(), datData.Rows[0]["ID"].ToInt32(), eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Request, eAction.Applied, "");
                clsCommonMail.SendMail(datData.Rows[0]["ID"].ToInt32(), MailRequestType.Expense, eAction.Applied);
                if (dlExpDocument.Items.Count > 0)
                    SaveExpenseDocuments(datData.Rows[0]["ID"].ToInt32());
                ViewState["ExpenseID"] = datData.Rows[0]["ID"].ToInt32();
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم الحفظ بنجاح") : ("Successfully saved");
            }

        }
        if (message != string.Empty)
        {
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
        lnkView_Click(null, null);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

        lnkView_Click(null, null);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ddlType.SelectedIndex = 0;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtAmount.Text = "";
        txtRemarks.Text = "";
        txtExpDocumentName.Text = "";
        if (dlExpDocument.Items.Count > 0)
        {
            dlExpDocument.DataSource = null;
            dlExpDocument.DataBind();

            ViewState["Documents"] = null;
        }
        hfExpenseRequestID.Value = "";
        setDivVisibility(1);
        enableDeleteButton(false);
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        ExpensePager.CurrentPage = 0;
        BindDataList();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        string msg = string.Empty;
        clsExpense objclsExpense = new clsExpense();


        if (hfExpenseRequestID.Value.ToInt32() > 0)
        {

            objclsExpense.intExpenseRequestID = hfExpenseRequestID.Value.ToInt32();

            if (objclsExpense.isExpenseDeletePossible())
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم بدأ المعالجة. لا يمكن الحذف") : ("Processing started request cannot be deleted.");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
            else
            {
                objclsExpense.DeleteExpenseRequest();
                clsCommonMessage.DeleteMessages(objclsExpense.intExpenseRequestID, eReferenceTypes.ExpenseRequest);
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم حذف الطلب (ات) بنجاح") : ("Request(s) deleted successfully");
            }

        }
        else
        {
            foreach (DataListItem item in dlExpenseRequest.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkExpense");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {
                    objclsExpense.intExpenseRequestID = dlExpenseRequest.DataKeys[item.ItemIndex].ToInt32();
                    if (objclsExpense.isExpenseDeletePossible())
                    {

                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم بدأ معالجة بعض الطلبات. لا يمكن الحذف") : ("Processing started some request(s) cannot be deleted.");
                    }
                    else
                    {
                        objclsExpense.DeleteExpenseRequest();
                        clsCommonMessage.DeleteMessages(objclsExpense.intExpenseRequestID, eReferenceTypes.ExpenseRequest);

                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم حذف الطلب (ات) بنجاح") : ("Request(s) deleted successfully");
                    }
                }
            }
        }
        if (message != string.Empty)
        {
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
        lnkView_Click(null, null);
    }
    protected void dlExpenseRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        clsExpense objclsExpense = new clsExpense();
        clsUserMaster objUser = new clsUserMaster();
        DataTable datData;
        switch (e.CommandName)
        {
            case "_View":
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
                trSingleReason.Style["display"] = "none";
                hfApproveLink.Value = "0";
               
                upSingleViewExpense.Update();
                upApproveExpense.Update();
                singleView(objUser.GetEmployeeId(), Convert.ToInt32(e.CommandArgument));
                enableDeleteButton(true);
                break;

            case "_Edit":

                objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
                objclsExpense.intExpenseRequestID = Convert.ToInt32(e.CommandArgument);
                ViewState["ExpenseID"] = Convert.ToInt32(e.CommandArgument);
                hfExpenseRequestID.Value = objclsExpense.intExpenseRequestID.ToStringCustom();
                DataSet ds = objclsExpense.getExpenseRequest();
                datData = ds.Tables[0];
                if (datData.Rows.Count > 0)
                {
                    ddlType.SelectedValue = datData.Rows[0]["ExpenseHeadID"].ToStringCustom();
                    txtFromDate.Text = datData.Rows[0]["FromDate"].ToDateTime().ToString("dd/MM/yyy");
                    txtToDate.Text = datData.Rows[0]["ToDate"].ToDateTime().ToString("dd/MM/yyy");
                    txtAmount.Text = datData.Rows[0]["Amount"].ToStringCustom();
                    txtRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        dlExpDocument.DataSource = ds.Tables[1].DefaultView;
                        dlExpDocument.DataBind();

                        ViewState["Documents"] = ds.Tables[1];
                        dlExpDocument.Visible = true;
                    }
                    setDivVisibility(1);
                }

                enableDeleteButton(false);
                break;

            case "_Cancel":

                objclsExpense.intExpenseRequestID = Convert.ToInt32(e.CommandArgument);
                objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
                hfExpenseRequestID.Value = objclsExpense.intExpenseRequestID.ToStringCustom();
                objclsExpense.strRemarks = "Expense Request for Cancellation";
                objclsExpense.requestForCancelExpenseRequest();

                clsCommonMessage.SendMessage(null, objclsExpense.intExpenseRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Cancel_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(objclsExpense.intExpenseRequestID, MailRequestType.Expense, eAction.RequestForCancel);

                string msg = string.Empty;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب الالغاء تم بنجاح") : ("Successfully requested for cancel");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                lnkView_Click(null, null);

                break;

        }
    }
    protected void btnApproveExpense_Click(object sender, EventArgs e)
    {
        //Approve Expense
        string message = string.Empty;
        string msg = string.Empty;
        Int32 intRequestID = Request.QueryString["Requestid"].ToInt32();
        if (intRequestID > 0)
        {
            clsUserMaster objUser = new clsUserMaster();
            clsExpense objclsExpense = new clsExpense();
            objclsExpense.intExpenseRequestID = intRequestID;
            DataTable datOldData = objclsExpense.getExpenseRequest().Tables[0];
            if (datOldData.Rows.Count > 0)
            {
                if (datOldData.Rows[0]["StatusID"].ToInt32() == (int)RequestStatus.Approved)
                {

                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تمت الموافقة مسبقا") : ("Already Approved.");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                    return;
                }
            }
            objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
            objclsExpense.dteExpenseDate = clsCommon.Convert2DateTime(txtExpenseDate.Text.Trim());
            objclsExpense.decAmount = txtApproveAmount.Text.Trim().ToDecimal();
            objclsExpense.intAccountabilityType = rblAccountability.SelectedValue.ToInt32();
            objclsExpense.decActualAmount = txtAccountabilityAmount.Text.Trim().ToDecimal();
            objclsExpense.intStatusID = ddlApproveStatus.SelectedValue.ToInt32();
            objclsExpense.strRemarks = txtApproveRemarks.Text.Trim();

            if (objclsExpense.lngEmployeeID <= 0)
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معاجة طلبك الحين") : ("Your request cannot process this time");
                msgs.InformationalMessage(msg);

                mpeMessage.Show();
                return;
            }
            if (objclsExpense.decAmount <= 0)
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء ادخال مبلغ صالح") : ("Please enter a valid amount");
                msgs.InformationalMessage(msg);

                mpeMessage.Show();
                return;
            }
            if ((rblAccountability.SelectedValue.ToInt32() == 2 || rblAccountability.SelectedValue.ToInt32() == 3) && txtAccountabilityAmount.Text.Trim().ToDecimal() <= 0)
            {
                if (rblAccountability.SelectedValue.ToInt32() == 2)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء ادخال مبلغ التعويض") : ("Please enter Reimbursement amount");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                }
                else if (rblAccountability.SelectedValue.ToInt32() == 3)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال كمية خصم") : ("Please enter Deduction amount");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                }
                return;
            }
            if ((rblAccountability.SelectedValue.ToInt32() == 2 || rblAccountability.SelectedValue.ToInt32() == 3) && txtAccountabilityAmount.Text.Trim().ToDecimal() > 0)
            {
                if (rblAccountability.SelectedValue.ToInt32() == 2 && txtAccountabilityAmount.Text.Trim().ToDecimal() > objclsExpense.decAmount)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء ادخال مبلغ  التعويض اقل من المبلغ المصروف") : ("Please enter a reimbursement amount less than the expense amount");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                    return;
                }
                if (rblAccountability.SelectedValue.ToInt32() == 3 && txtAccountabilityAmount.Text.Trim().ToDecimal() > objclsExpense.decAmount)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء ادخال مبلغ الخصم اقل من المبلغ المصروف") : ("Please enter a deduction amount less than the expense amount");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                    return;
                }

            }
            if (ddlApproveStatus.SelectedValue.ToInt32() <= 0)
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال حالة صالحة") : ("Please enter a valid status");
                msgs.InformationalMessage(msg);

                mpeMessage.Show();
                return;
            }
            if (objclsExpense.strRemarks == "")
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء إدخال الملاحظات") : ("Please enter remarks");
                msgs.InformationalMessage(msg);

                mpeMessage.Show();
                return;
            }
            if (ddlApproveStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved)
            {
                if (objclsExpense.isEmployeeNotInService(Convert.ToInt64(objUser.GetEmployeeId())))
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الموظف ليس في الخدمة") : ("Employee not in service");
                    msgs.InformationalMessage(msg);

                    mpeMessage.Show();
                    return;
                }

                if (objclsExpense.isSalaryExist(objclsExpense.dteExpenseDate, intRequestID))
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تتم معالجة الراتب بالفعل في تاريخ هذا المصروف ") : ("Salary is already processed for this expense date");
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;
                }
            }
            DataTable datData;


            if (clsLeaveRequest.IsHigherAuthority((int)RequestType.Expense, Convert.ToInt64(objUser.GetEmployeeId()), intRequestID))
            {

                datData = objclsExpense.ApproveExpenseRequest();
                if (datData.Rows.Count > 0)
                {
                    if (datData.Rows[0]["ID"].ToInt32() > 0)
                    {
                        new clsExportToCSV().ExportToCSV(objclsExpense.dteExpenseDate);
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("اجراءات الموافقة تمت بنجاح") : ("Successfully completed approval procedure");
                        divPrint.Style["display"] = "block";
                        upMenu.Update();
                        if (objclsExpense.intStatusID == (int)RequestStatus.Approved)
                        {
                            string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages/";
                            int i = 0;
                            foreach (DataListItem item in dlApprovedDocuments.Items)
                            {
                                i++;
                                Label lblFileName = (Label)item.FindControl("lblFileName");

                                objclsExpense.UpdateExpenseDocumentsToTreemaster(lblFileName.Text, "Expense Document " + i.ToString());

                                if (File.Exists(Server.MapPath("~/documents/ExpenseDocuments/" + lblFileName.Text.Trim().ToString())))
                                {
                                    if (!Directory.Exists(Path))
                                        Directory.CreateDirectory(Path);
                                    clsCommon.WriteToLog("Directory is created");
                                    File.Copy(Server.MapPath("~/documents/ExpenseDocuments/" + lblFileName.Text.Trim().ToString()), Path + lblFileName.Text.Trim().ToString());
                                    clsCommon.WriteToLog("File moved successfully");
                                }
                            }


                            clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Approval, eAction.Approved, "");
                            clsCommonMail.SendMail(intRequestID, MailRequestType.Expense, eAction.Approved);

                        }
                        else if (objclsExpense.intStatusID == (int)RequestStatus.Pending)
                        {
                            clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Approval, eAction.Pending, "");
                        }
                        else if (objclsExpense.intStatusID == (int)RequestStatus.Rejected)
                        {
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مرفوض بنجاح") : ("Request rejected successfully.");
                            clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Rejection, eAction.Reject, "");
                            clsCommonMail.SendMail(intRequestID, MailRequestType.Expense, eAction.Reject);
                        }
                        singleView(0, intRequestID);
                    }
                    else
                    {
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("الموافقة غير ممكنة") : ("Approval not possible");
                    }

                }
                else
                {
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("الموافقة غير ممكنة") : ("Approval not possible");
                }
            }
            else
            {
                divPrint.Style["display"] = "none";
                upMenu.Update();
                objclsExpense.intStatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue);
                objclsExpense.strRemarks = txtApproveRemarks.Text.Trim();

                datData = objclsExpense.ForwardExpenseRequest();
                if (datData.Rows.Count > 0)
                {


                    if (datData.Rows[0]["ID"].ToInt32() > 0)
                    {
                        if (objclsExpense.intStatusID == (int)RequestStatus.Approved)
                        {
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                            clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Approval, eAction.Approved, "");
                            clsCommonMail.SendMail(intRequestID, MailRequestType.Expense, eAction.Approved);


                        }

                        else if (objclsExpense.intStatusID == (int)RequestStatus.Rejected)
                        {
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مرفوض بنجاح") : ("Request rejected successfully.");
                            clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Rejection, eAction.Reject, "");
                            clsCommonMail.SendMail(intRequestID, MailRequestType.Expense, eAction.Reject);
                        }

                    }
                    singleView(0, intRequestID);
                }
                else
                {
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("الموافقة غير ممكنة") : ("Approval not possible");
                }

            }


            ViewState["Approve"] = false;

            if (message != string.Empty)
            {
                msgs.InformationalMessage(message);
                mpeMessage.Show();
            }
        }
    }
    protected void btnApproveCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }
    protected void rblAccountability_SelectedIndexChanged(object sender, EventArgs e)
    {
        string msg = string.Empty;
        //1:None ,//2:Reimbursement,//3:Deduction        
        if (rblAccountability.SelectedValue == "2")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("قيمة السداد") : ("Reimbursement Amount");
            lblAccountabilityHeader.Text = msg;
            txtAccountabilityAmount.Text = "0";
            txtAccountabilityAmount.Enabled = true;
        }
        else if (rblAccountability.SelectedValue == "3")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("قيمة الخصم") : ("Deduction Amount");
            lblAccountabilityHeader.Text = msg;
            txtAccountabilityAmount.Text = "0";
            txtAccountabilityAmount.Enabled = true;
        }
        else
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المبلغ الفعلي") : ("Actual Amount");
            lblAccountabilityHeader.Text = msg;
            txtAccountabilityAmount.Text = "0";
            txtAccountabilityAmount.Enabled = false;
        }
        upApproveExpense.Update();
    }
    protected void dlExpenseRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hfForwarded");
            HiddenField hdStatusID = (HiddenField)e.Item.FindControl("hfStatusID");
            Int32 intStatusID = hdStatusID.Value.ToInt32();
            bool blnForwared = hdForwarded.Value == "True" ? true : false;

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            if (intStatusID != (int)RequestStatus.Applied || blnForwared)
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton btnCancel = (ImageButton)e.Item.FindControl("btnCancel");
            btnCancel.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm(' هل تريد إلغاء هذا الطلب؟');") : ("return confirm('Do you want to cancel this request?');");
            if (intStatusID == (int)RequestStatus.Cancelled || intStatusID == (int)RequestStatus.RequestForCancel || intStatusID == (int)RequestStatus.Rejected || (intStatusID == (int)RequestStatus.Applied && !blnForwared))
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }

        }
    }
    protected void btnCancelExpense_Click(object sender, EventArgs e)
    {
        string msg = string.Empty;
        //cancel expense request
        string message = string.Empty;
        Int32 intRequestID = Request.QueryString["Requestid"].ToInt32();
        clsUserMaster objUser = new clsUserMaster();
        clsExpense objclsExpense = new clsExpense();
        if (intRequestID > 0)
        {
            if (objclsExpense.isSalaryExistToCancel(intRequestID))
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تتم معالجة المرتب , الالغاء غير ممكن") : ("Salary is processed,cancellation not possible");
                msgs.InformationalMessage(msg);

                mpeMessage.Show();
                return;
            }
            objclsExpense.intExpenseRequestID = intRequestID;
            objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
            objclsExpense.intExpenseTypeID = ViewState["ExpenseTypeID"].ToInt32();
            objclsExpense.intStatusID = (int)RequestStatus.Cancelled;

            DataTable datData;
            string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";
            objclsExpense.strRemarks = txtEditRemarks.Text;
            datData = objclsExpense.CancelExpenseRequest();
            if (datData.Rows.Count > 0)
            {

                foreach (DataListItem item in dlDocumentsApproval.Items)
                {
                    Label lblFileName = item.FindControl("lblFileName") as Label;

                    string sFilename = lblFileName.Text;

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }


                message = clsUserMaster.GetCulture() == "ar-AE" ? ("ألغي بنجاح") : ("Successfully Cancelled");

                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.ExpenseRequest, eMessageTypes.Expense_Cancellation, eAction.Cancelled, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Expense, eAction.Cancelled);
                singleView(0, intRequestID);
                divCancelTheRequest.Style["display"] = "none";
            }
            else
            {

                message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الالغاء") : ("Cancellation not possible");
            }
        }

        if (message != string.Empty)
        {
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
    }

    protected void btnCancelCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }

    #endregion
    #region USER FUNCTIONS
    private void loadCombo(int intMode)
    {
        clsExpense objclsExpense = new clsExpense();
        DataTable datData;
        if (intMode == 1)
        {
            datData = objclsExpense.getExpenseType();
            if (datData.Rows.Count > 0)
            {
                ddlType.DataTextField = "Description";
                ddlType.DataValueField = "ExpenseHeadID";
                ddlType.DataSource = datData;
                ddlType.DataBind();
            }
        }

        if (intMode == 3)
        {
            datData = objclsExpense.getExpenseApproveStatus();
            if (datData.Rows.Count > 0)
            {
                ddlApproveStatus.DataTextField = "Description";
                ddlApproveStatus.DataValueField = "StatusId";
                ddlApproveStatus.DataSource = datData;
                ddlApproveStatus.DataBind();
                ddlApproveStatus.SelectedValue = ((int)RequestStatus.Approved).ToString();

                if (!clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.Expense), Convert.ToInt64(new clsUserMaster().GetEmployeeId()), Convert.ToInt32(Request.QueryString["RequestId"])))
                {
                    ddlApproveStatus.Items.Remove(new ListItem("Pending", "4"));
                }
            }
        }
    }
    private void enableDeleteButton(bool IsEnable)
    {
        if (IsEnable)
        {
            lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد أنك تريد حذف?');") : ("return confirm('Are you sure you want to delete?');"); ;
            lnkDelete.Enabled = true;
        }
        else
        {
            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
        }
        upMenu.Update();
    }
    private void setDivVisibility(int intMode)
    {
        //Mode 1:-divAddExpense //Mode 2:-divViewExpense  //Mode 3:-divSingleViewExpense  //Mode 4:-divApproveExpense

        if (intMode == 1)
        {
            divAddExpense.Style["display"] = "block";
            divViewExpense.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "none";
            divApproveExpense.Style["display"] = "none";
        }
        else if (intMode == 2)
        {
            divAddExpense.Style["display"] = "none";
            divViewExpense.Style["display"] = "block";
            divSingleViewExpense.Style["display"] = "none";
            divApproveExpense.Style["display"] = "none";
        }
        else if (intMode == 3)
        {
            divAddExpense.Style["display"] = "none";
            divViewExpense.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "block";
            divApproveExpense.Style["display"] = "none";
        }
        else if (intMode == 4)
        {
            divAddExpense.Style["display"] = "none";
            divViewExpense.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "none";
            divApproveExpense.Style["display"] = "block";
        }
        upViewExpense.Update();
        upAddExpense.Update();
        upSingleViewExpense.Update();
        upApproveExpense.Update();

    }
    private void BindDataList()
    {
        clsExpense objclsExpense = new clsExpense();
        clsUserMaster objUser = new clsUserMaster();
        objclsExpense.lngEmployeeID = objUser.GetEmployeeId();
        objclsExpense.intPageIndex = ExpensePager.CurrentPage + 1;
        objclsExpense.intPageSize = ExpensePager.PageSize;
        DataSet dtsData = objclsExpense.getExpenseRequestToBind();
        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                dlExpenseRequest.DataSource = dtsData.Tables[0];
                dlExpenseRequest.DataBind();
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        ExpensePager.Total = dtsData.Tables[1].Rows[0]["Count"].ToInt32();
                    }
                    else
                    {
                        ExpensePager.Total = 0;
                    }
                }
                else
                {
                    ExpensePager.Total = 0;
                }
                setDivVisibility(2);
                hfExpenseRequestID.Value = "";
                lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlExpenseRequest.ClientID + "');";
                lnkDelete.Enabled = true;
                upMenu.Update();
            }

            else
            {
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        if (dtsData.Tables[1].Rows[0]["Count"].ToInt32() > 0)
                        {
                            if (ExpensePager.CurrentPage > 0)
                            {
                                ExpensePager.CurrentPage = ExpensePager.CurrentPage - 1;
                                BindDataList();
                            }
                            else
                            {
                                lnkAdd_Click(null, null);
                            }
                        }
                        else
                        {
                            lnkAdd_Click(null, null);
                        }
                    }
                    else
                    {
                        lnkAdd_Click(null, null);

                    }
                }
                else
                {
                    lnkAdd_Click(null, null);

                }
            }
        }
        else
        {
            lnkAdd_Click(null, null);
        }

    }
    private void singleView(Int64 lngEmployeeID, Int32 intExpenseRequestID)
    {
        clsExpense objclsExpense = new clsExpense();
        DataTable datData;
        objclsExpense.lngEmployeeID = lngEmployeeID;
        objclsExpense.intExpenseRequestID = intExpenseRequestID;
        hfExpenseRequestID.Value = intExpenseRequestID.ToStringCustom();
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["ExpenseID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
        }
        else
        {
            ViewState["ExpenseID"] = objclsExpense.intExpenseRequestID;
        }
        DataSet ds = objclsExpense.getExpenseRequest();
        datData = ds.Tables[0];
        if (datData.Rows.Count > 0)
        {
            string msg = string.Empty;

            lblRequestedBy.Text = datData.Rows[0]["EmployeeName"].ToStringCustom();
            lblExpenseType.Text = datData.Rows[0]["ExpenseType"].ToStringCustom();
            lblDate.Text = datData.Rows[0]["Date"].ToStringCustom();
            lblAmount.Text = datData.Rows[0]["Amount"].ToStringCustom();
            if (datData.Rows[0]["ApprovedAmount"].ToDecimal() > 0)
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المبلغ المعتمد") : ("Approved Amount");
                lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ApprovedAmount"].ToStringCustom() + ")";

            }
            if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 2 || datData.Rows[0]["ExpenseTransID"].ToInt32() == 3)
            {
                if (datData.Rows[0]["ActualAmount"].ToDecimal() > 0)
                {
                    if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 2)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("قيمة السداد") : ("Reimbursement Amount");
                        lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ActualAmount"].ToStringCustom() + ")";
                    }
                    else if (datData.Rows[0]["ExpenseTransID"].ToInt32() == 3)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("قيمة الخصم") : ("Deduction Amount");
                        lblAmount.Text = lblAmount.Text + " (" + msg + ": " + datData.Rows[0]["ActualAmount"].ToStringCustom() + ")";
                    }
                }
            }
            lblRequestedToNames.Text = datData.Rows[0]["RequestedToNames"].ToStringCustom();
            lblStatus.Text = datData.Rows[0]["Status"].ToStringCustom();
            lblRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();
            setDivVisibility(3);
            txtEditRemarks.Visible = false;
            lblRemarks.Visible = true;

            objclsExpense.CompanyID = objclsExpense.GetCompanyID();
            DataTable dt2 = objclsExpense.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                //dl1.DataSource = dt2;
                //dl1.DataBind();
                dl2.DataSource = dt2;
                dl2.DataBind();
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                dlDocumentsApproval.DataSource = ds.Tables[1];
                dlDocumentsApproval.DataBind();
            }
        }
    }
    #endregion
    protected void lnkAttch_Click(object sender, EventArgs e)
    {
        try
        {

            if (fuExpense.HasFile)
                fuExpense.ClearAllFilesFromPersistedStore();
            else
            {
                string sMsg = clsGlobalization.IsArabicCulture() ? "يرجى إرفاق مستند" : "Please attach a document";
                msgs.InformationalMessage(sMsg);
                this.mpeMessage.Show();
                return;
            }

            DataTable dt = null;


            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);
            else
            {
                dt = new DataTable();
                dt.Columns.Add("FileName");
                dt.Columns.Add("DocumentName");
                dt.Columns.Add("Location");

            }

            DataRow dr = dt.NewRow();
            dr["FileName"] = Session["FileName"];
            dr["DocumentName"] = txtExpDocumentName.Text;
            dr["Location"] = "../Documents/ExpenseDocuments/" + Session["FileName"].ToString();

            dt.Rows.Add(dr);



            dlExpDocument.DataSource = dt;
            dlExpDocument.DataBind();
            dlExpDocument.Visible = dlExpDocument.Items.Count > 0 ? true : false;
            ViewState["Documents"] = dt;


            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;


            txtExpDocumentName.Text = "";



        }
        catch (Exception)
        {

        }
    }
    protected void fuExpense_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            string FileName = string.Empty;
            string ActualFileName = string.Empty;


            if (!Directory.Exists(Server.MapPath("~/Documents/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/"));

            if (!Directory.Exists(Server.MapPath("~/Documents/ExpenseDocuments/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/ExpenseDocuments/"));

            if (fuExpense.HasFile)
            {
                ActualFileName = fuExpense.FileName;
                FileName = DateTime.Now.Ticks.ToString() + new Random().Next(0, 1000).ToString() + Path.GetExtension(fuExpense.FileName);
                Session["FileName"] = FileName;
                fuExpense.SaveAs(Server.MapPath("~/Documents/ExpenseDocuments/") + FileName);
            }

        }
        catch (Exception)
        {
        }
    }
    protected void img_Click(object sender, ImageClickEventArgs e)
    {
        string FileName = ((ImageButton)sender).CommandArgument.ToString();

        if (IsValidImage(FileName))
        {
            string Page = "Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OPen", "window.open('" + Page + "');", true);
        }
        else
        {
            string Page = "Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OPen", "window.open('" + Page + "','','width=400,height=600');", true);
        }

    }
    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dt = null;
            string FileName = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            clsExpense objExpense = new clsExpense();
            objExpense.intExpenseRequestID = Convert.ToInt32(ViewState["ExpenseID"]);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FileName"].ToString() == FileName)
                {
                    if (File.Exists(Server.MapPath(dt.Rows[i]["Location"].ToString())))
                        File.Delete(Server.MapPath(dt.Rows[i]["Location"].ToString()));
                    dt.Rows.Remove(dt.Rows[i]);

                    objExpense.DeleteExpenseAttachedDocuments(FileName);

                }
            }

            dlExpDocument.DataSource = dt;
            dlExpDocument.DataBind();

            dlExpDocument.Visible = dlExpDocument.Items.Count > 0 ? true : false;

            ViewState["Documents"] = dt;

        }
        catch (Exception)
        {
        }

    }
    private void SaveExpenseDocuments(int ExpenseID)
    {
        try
        {
            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            clsExpense objExpens = new clsExpense();
            objExpens.intExpenseRequestID = ExpenseID;
            if (dt != null && dt.Rows.Count > 0)
            {
                objExpens.DeleteExpenseAttachedDocuments(string.Empty);

                foreach (DataRow dr in dt.Rows)
                {
                    objExpens.SaveExpenseDocuments(dr["FileName"].ToString(), dr["DocumentName"].ToString());
                }
            }

        }
        catch (Exception)
        {
        }
    }

    protected void dlExpDocument_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            if (img != null)
            {

                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }
        }
    }
    protected void dlApprovedDocuments_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            if (img != null)
            {
                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }
        }
    }
    protected void dlDocumentsApproval_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            if (img != null)
            {
                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }
        }
    }

    protected void lbReason_OnClick(object source, EventArgs e)
    {
        clsExpense objExpense = new clsExpense();

        if (Convert.ToInt32(ViewState["ExpenseID"]) > 0)
        {
            objExpense.intExpenseRequestID = ViewState["ExpenseID"].ToInt32();

            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                trSingleReason.Style["display"] = "block";
                DataTable dt = objExpense.DisplayReasons(objExpense.intExpenseRequestID);
                if (dt.Rows.Count > 0)
                {
                    gvSingleReason.DataSource = dt;
                    gvSingleReason.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                trSingleReason.Style["display"] = "none";
            }
        }
    }

    protected void lbApproveReason_OnClick(object source, EventArgs e)
    {
        clsExpense objExpense = new clsExpense();
        clsUserMaster objUser = new clsUserMaster();
        if (Convert.ToInt32(ViewState["ExpenseID"]) > 0)
        {
            objExpense.intExpenseRequestID = ViewState["ExpenseID"].ToInt32();
            objExpense.CompanyID = objExpense.GetCompanyID();
            DataTable dt2 = objExpense.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                dl1.DataSource = dt2;
                dl1.DataBind();
            }
            if (Convert.ToInt32(hfApproveLink.Value) == 0)
            {
                hfApproveLink.Value = "1";
                trReasons.Style["display"] = "block";
                DataTable dt = objExpense.DisplayReasons(objExpense.intExpenseRequestID);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfApproveLink.Value = "0";
                trReasons.Style["display"] = "none";
            }
        }
    }
    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
}
