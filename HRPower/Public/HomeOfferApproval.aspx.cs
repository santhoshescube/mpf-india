﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_HomeOfferApproval : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            FillCombo();

            ddlStatus.SelectedValue = "0";

            if (Request.QueryString["RequestType"] != null)
            {
                ddlStatus.SelectedValue = Convert.ToString(Convert.ToString(Session["StatusID"]));

            }

            dvMain.Style["display"] = "none";
            dvViewOffers.Style["display"] = "block";
            BindOfferApprovals();
           
        }
    }

    private void FillCombo()
    {
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();

        DataSet dsVacancy = objHomePage.GetAllVacancies(-1, -1);

        ddlHireVacancy.DataSource = dsVacancy;
        ddlHireVacancy.DataBind();
        ddlHireVacancy.Items.Insert(0, new ListItem("Any Job", "-1"));


        ddlStatus.DataSource = clsHomePage.FillOfferStatus();
        ddlStatus.DataBind();
        string sStatus = clsGlobalization.IsArabicCulture() ? "أي وضع" : "Any Status";
        ddlStatus.Items.Insert(0, new ListItem(sStatus, "-1"));

    }
   
    protected void btnNewHires_Click(object sender, EventArgs e)
    {
        ClearLists();

        BindOfferApprovals();
    }
    private void ClearLists()
    {
        dlNewHires.DataSource = null;
        dlNewHires.DataBind();
        ddlHireVacancy.SelectedIndex = 0;

    }


    protected void ddlHireVacancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrOffer.CurrentPage = 0;
        BindOfferApprovals();
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindOfferApprovals();
    }

    protected void BindOfferApprovals()
    {
     

        objHomePage = new clsHomePage();
        objRoleSettings = new clsRoleSettings();
        objUserMaster = new clsUserMaster();

        DataTable dt = clsHomePage.FillOfferApprovals(pgrOffer.CurrentPage + 1, pgrOffer.PageSize, "", "desc", "OfferID", objUserMaster.GetEmployeeId(),ddlStatus.SelectedValue.ToInt32(),ddlHireVacancy.SelectedValue.ToInt32());

        if (dt.Rows.Count == 0)
        {
            pgrOffer.Visible = false;
            dlNewHires.ShowHeader = false;
            pgrOffer.Total = 0;
        }
        else
        {
            pgrOffer.Visible = true;
            dlNewHires.ShowHeader = true;
            pgrOffer.Total = Convert.ToInt32(dt.Rows[0]["TotalCount"]);
        }

        dlNewHires.DataSource = dt;
        dlNewHires.DataBind();

        if (dlNewHires.Items.Count == 0)
        {
            lblMessage.Style["display"] = "block";
            lblMessage.Text = "No pending offers";
        }
        else
            lblMessage.Style["display"] = "none";
        //}
        //else
        //{
        //    lblMessage.Text = "You have no permission to view new hires";
        //}
    }

    public bool SetVisibility()
    {
        //objRoleSettings = new clsRoleSettings();
        //objUserMaster = new clsUserMaster();
        //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Make_AsEmployee))
        return true;
        //else
        //    return false;
    }

    protected string GetRequestLink(object RequestId)
    {
        string url = string.Empty;
       
        url = "OfferLetter.aspx?OfferID=" + RequestId ;

        return url;
    }

    public bool IsApproveVisible(object oStatusId)
    {
        if ((int)oStatusId ==(int) OfferStatus.Waitingforapproval) return true; else return false;

    }

    public string SetMessage( object Remarks, object RejectedBy,object OfferStatusID)
    {
        string Message = string.Empty;
        if ((int)OfferStatusID == (int)OfferStatus.Rejected)
        {

            Message = (clsGlobalization.IsArabicCulture() ? "  رفض بواسطة : " + RejectedBy + "  سبب : " : "Rejected By :" + RejectedBy + " Reason :")  + Remarks;
            return Message;
        }
        else
            return Message;

    }

    protected void dlNewHires_ItemCommand(object source, DataListCommandEventArgs e)
    {

        switch (e.CommandName)
        {
            case "_ViewCandidate":
                dvMain.Style["display"] = "block";
                dvViewOffers.Style["display"] = "none";

                CandidateHistory.GetHistory(null, dlNewHires.DataKeys[e.Item.ItemIndex].ToInt32());
                 updOffer.Update();
                break;
           
        }
    }

    protected void btnGoBack_Click(object sender, EventArgs e)
    {
        BindOfferApprovals();
        dvMain.Style["display"] = "none";
        dvViewOffers.Style["display"] = "block";
      
        updOffer.Update();


    }
    #region RightMenuEvents
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {

        TableCell cell = (TableCell)e.Cell;
        cell.Width = Unit.Pixel(30);
        cell.Height = Unit.Pixel(30);
        if (e.Day.Date.ToString("dd MMM yyyy") == System.DateTime.Now.Date.ToString("dd MMM yyyy"))
        {
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Red;
            //cell.CssClass = "Currendate";
        }
        //cell.BorderStyle = BorderStyle.Solid;
        //cell.BorderWidth = Unit.Pixel(1);
        //cell.BorderColor = System.Drawing.Color.Black;
        cell.Style["font-size"] = "10px";
        //cell.Style["padding"] = "0";
        cell.Attributes.Add("title", "");
        if (e.Day.IsOtherMonth)
        {
            cell.Text = "";
            cell.CssClass = "tblBackground1";

        }
        else
        {

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();

            DataSet ds = objHomePage.GetCalendarDetails(objUserMaster.GetEmployeeId(), e.Day.Date.ToString("dd MMM yyyy"));
            cell.CssClass = "tblBackground1";
            string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : " Holiday:");
            if (ds.Tables[0].Rows.Count > 0)
            {
                sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                cell.ForeColor = System.Drawing.Color.Red;
            }
            string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : " Events: \n");
            int icount = 1;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                icount++;
            }
            if (sEvent != string.Empty)
                cell.ForeColor = System.Drawing.Color.Green;
            if (sEvent != string.Empty && sHoliday != string.Empty)
                cell.ForeColor = System.Drawing.Color.FromArgb(244, 166, 29);
            //cell.ForeColor = System.Drawing.Color.Magenta;
            string sTitle = (sHoliday == string.Empty ? string.Empty : sHoliday) + (sEvent == string.Empty ? string.Empty : sEvent);
            if (sTitle != string.Empty)
                cell.Attributes.Add("title", sTitle);

        }
    }
    #endregion RightMenuEvents
}
