﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Projects.aspx.cs" Inherits="Public_Projects" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
  <%--      <ul>
            <li><a href='Company.aspx'>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'>
                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>
            <li><a href="Bank.aspx">
                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx">
                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx">
                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx">
                <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx">
                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx">
                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx">
                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx">
                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx">
                <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            <li class="selected"><a href="Projects.aspx">
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:SettingsCommon,Projects%>'></asp:Literal></a></li>
        </ul>--%>
   
   
    <ul>
            <li class="selected"><a href='Projects.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Project %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='ManpowerPlanning.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,Plan %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,Expense %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
            
              <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,Calender %>'>
                </asp:Literal>
            </a></li>
          
            
            
        </ul>
   
   
   
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div runat="server" id="projects" style="width: 100%; float: left;">
        <div style="width: 100%; float: left;">
            <div style="display: none">
                <asp:Button ID="btnSubmit" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <div style="width: 100%; float: left;">
            <asp:UpdatePanel ID="upProjects" runat="server">
                <ContentTemplate>
                    <asp:FormView ID="fvProjects" runat="server" Width="100%" CellSpacing="0" OnDataBound="fvProjects_DataBound"
                        OnItemCommand="fvProjects_ItemCommand" DataKeyNames="ProjectId">
                        <EditItemTemplate>
                            <div style="width: 100%; float: left;">
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="projectcode"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtProjectCode" runat="server" Text='<%# Eval("ProjectCode")%>'
                                            MaxLength="50" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProjectCode"
                                            ValidationGroup="submit" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtProjectName" runat="server" Text='<%# Eval("Description")%>'
                                            MaxLength="50" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtProjectName"
                                            ValidationGroup="submit" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="StartDate"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                            Text='<%# Eval("StartDate") %>' AutoPostBack="true" OnTextChanged="BindEmployees"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnStartDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CssClass="imagebutton" CausesValidation="False" />
                                        <AjaxControlToolkit:CalendarExtender ID="extenderSalaryDate" runat="server" TargetControlID="txtFromDate"
                                            PopupButtonID="ibtnStartDate" Format="dd/MM/yyyy">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtFromDate"
                                            Display="Dynamic" ClientValidationFunction="ValidateProjectStartDate" ValidationGroup="submit"
                                            CssClass="error"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="EstimatedDate"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                            Text='<%# Eval("EndDate") %>' AutoPostBack="true" OnTextChanged="BindEmployees"></asp:TextBox>
                                        <asp:ImageButton ID="ibtnEndDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CssClass="imagebutton" CausesValidation="False" />
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate"
                                            PopupButtonID="ibtnEndDate" Format="dd/MM/yyyy">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ValidateEmptyText="true"
                                            ControlToValidate="txtToDate" Display="Dynamic" ClientValidationFunction="CheckValidDateAndFromToDate"
                                            ValidationGroup="submit" CssClass="error"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Budget"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtBudget" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Budget") %>'
                                            MaxLength="15" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtBudget"
                                            ValidationGroup="submit" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtBudgetExtender" runat="server"
                                            FilterType="Numbers,Custom" TargetControlID="txtBudget" ValidChars=".">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Remarks") %>'
                                            TextMode="MultiLine" Rows="5" Width="250px" Height="60px" onchange="RestrictMulilineLength(this, 500);"
                                            onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;" id="divstatus">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:ReportsCommon,Status%>'></asp:Literal>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:RadioButtonList ID="rdbStatus" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" meta:Resourcekey="Active" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="0" meta:Resourcekey="InActive"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="AssignEmployees"></asp:Literal>
                                    </div>
                                    <div class="trRight" style="width: auto; float: left; vertical-align: top">
                                        <div style="height: 350px; overflow: auto">
                                            <asp:UpdatePanel ID="upEmployees" runat="server">
                                                <ContentTemplate>
                                                    <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeesList"
                                                        DataKeyField="EmployeeId" Width="100%" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                                        BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <ItemStyle ForeColor="#000066" />
                                                        <SeparatorStyle BackColor="AliceBlue" />
                                                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <HeaderTemplate>
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 20px; overflow: hidden;">
                                                                        <asp:CheckBox CssClass="labeltext" ForeColor="White" AutoPostBack="true" ID="chkSelectAll"
                                                                            OnCheckedChanged="chkSelectAll_CheckedChanged" Height="15px" runat="server" />
                                                                    </td>
                                                                    <td style="width: 150px; overflow: hidden; vertical-align: top;"  align="left">
                                                                        <asp:Label ID="lblHeader" Text='<%$Resources:ControlsCommon,Employee%>' ForeColor="White"
                                                                            CssClass="labeltext" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 120px; vertical-align: top;" align="left">
                                                                        <asp:Label ID="Label5" meta:ResourceKey="StartDate" ForeColor="White" CssClass="labeltext"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 120px; overflow: hidden; vertical-align: top;" align="left">
                                                                        <asp:Label ID="Label6" meta:ResourceKey="EndDate" ForeColor="White" CssClass="labeltext"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                                        <asp:Label ID="Label7" meta:ResourceKey="Duration" ForeColor="White" CssClass="labeltext"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                                        <asp:Label ID="Label14" Text='<%$Resources:ReportsCommon,Status%>' ForeColor="White"
                                                                            CssClass="labeltext" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                                        <HeaderStyle CssClass="datalistheader" />
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td width="20px">
                                                                        <asp:CheckBox ID="chkSelect" Height="15px" runat="server" AutoPostBack="true" OnCheckedChanged="EnableDates" />
                                                                    </td>
                                                                    <td width="150px" align="left">
                                                                        <asp:Label ID="lblEmployee" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("Employee") %>'
                                                                            Text='<%# Eval("Employee") %>'></asp:Label>
                                                                        <asp:HiddenField ID="hfEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                                                        </asp:HiddenField>
                                                                    </td>
                                                                    <td width="120px" align="left" id="tdSDate" runat="server">
                                                                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox_mandatory" Width="75px"
                                                                            Value='<%# Eval("WorkStartDate") %>'></asp:TextBox>
                                                                        <asp:ImageButton ID="ibtnEmpStartDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                            CssClass="imagebutton" CausesValidation="false" />
                                                                        <AjaxControlToolkit:CalendarExtender ID="extenderSalaryDate" runat="server" TargetControlID="txtStartDate"
                                                                            PopupButtonID="ibtnEmpStartDate" Format="dd/MM/yyyy">
                                                                        </AjaxControlToolkit:CalendarExtender>
                                                                        <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtStartDate"
                                                                            Display="Dynamic" ClientValidationFunction="ValidateProjectStartDate" CssClass="error"></asp:CustomValidator>
                                                                    </td>
                                                                    <td width="120px" align="left" id="tdEDate" runat="server">
                                                                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="textbox_mandatory" Width="75px"
                                                                            Value='<%# Eval("FinishDate") %>'></asp:TextBox>
                                                                        <asp:ImageButton ID="ibtnEmpendDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                            CssClass="imagebutton" CausesValidation="false" />
                                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtEndDate"
                                                                            PopupButtonID="ibtnEmpendDate" Format="dd/MM/yyyy">
                                                                        </AjaxControlToolkit:CalendarExtender>
                                                                        <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtEndDate"
                                                                            Display="Dynamic" ClientValidationFunction="ValidateProjectStartDate" CssClass="error"></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                                        <asp:TextBox ID="txtDuration" runat="server" Width="30px" Enabled="false" CssClass="textbox_mandatory"
                                                                            Value='<%# Eval("Duration") %>'></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                                        <asp:Label ID="Label14" Text='<%# Eval("ProjectStatus") %>' CssClass="labeltext"
                                                                            runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4">
                                                                        <asp:Label ID="lblInfoEmployeeAdd" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <asp:CustomValidator ID="cvValidateAssociates" runat="server" ClientValidationFunction="CheckSelectionindatalistList"
                                            Display="Dynamic" CssClass="error" ValidationGroup="submit" meta:ResourceKey="EmployeeValidation"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 450px; float: left;" class="trLeft">
                                        &nbsp;
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" CommandName="Add"
                                            ValidationGroup="submit" Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                            Width="75px" />
                                        <asp:Button ID="btnCancelSubmit" runat="server" CssClass="btnsubmit" CommandName="CancelProject"
                                            Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                            CausesValidation="false" Width="75px" />
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="ProjectCode"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left;" class="trRight">
                                    <asp:Label ID="lbProjectCode" runat="server" Text='<%# Eval("ProjectCode") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Description"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left;" class="trRight">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Description") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="StartDate"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left;" class="trRight">
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("StartDate") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="EstimatedDate"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left;" class="trRight">
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("EndDate") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Budget"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left;" class="trRight">
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("Budget") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                </div>
                                <div style="width: auto; float: left; word-break: break-all" class="trRight">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Remarks") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ReportsCommon,Status%>'></asp:Literal>
                                </div>
                                <div style="width: auto; float: left; word-break: break-all" class="trRight">
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div style="width: 150px; float: left;" class="trLeft">
                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="TeamMembers"></asp:Literal>
                                </div>
                                <div class="trRight" style="width: auto; float: left; vertical-align: top">
                                    <div style="height: 350px; overflow: auto">
                                        <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeesViewList"
                                            DataKeyField="EmployeeId" Width="100%" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                            BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <ItemStyle ForeColor="#000066" />
                                            <SeparatorStyle BackColor="AliceBlue" />
                                            <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <HeaderTemplate>
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 200px; overflow: hidden; vertical-align: top;" align="left">
                                                            <asp:Label ID="lblHeader" Text='<%$Resources:ControlsCommon,Employee%>' ForeColor="White"
                                                                CssClass="labeltext" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 200px; overflow: hidden; vertical-align: top;" align="left">
                                                            <asp:Label ID="Label10" meta:ResourceKey="StartDate" ForeColor="White" CssClass="labeltext"
                                                                runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 200px; overflow: hidden; vertical-align: top;" align="left">
                                                            <asp:Label ID="Label11" meta:ResourceKey="EndDate" ForeColor="White" CssClass="labeltext"
                                                                runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                            <asp:Label ID="Label7" meta:ResourceKey="Duration" ForeColor="White" CssClass="labeltext"
                                                                runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="datalistheader" />
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="vertical-align: top; width: 200px;" align="left">
                                                            <asp:Label ID="lblemp" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("Employee") %>'
                                                                Text='<%# Eval("Employee") %>'></asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top; width: 200px;" align="left">
                                                            <asp:Label ID="Label8" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("WorkStartDate") %>'
                                                                Text='<%# Eval("WorkStartDate") %>'></asp:Label>
                                                        </td>
                                                        <td style="vertical-align: top; width: 200px;" align="left">
                                                            <asp:Label ID="Label9" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("FinishDate") %>'
                                                                Text='<%# Eval("FinishDate") %>'></asp:Label>
                                                        </td>
                                                        <td style="width: 50px; overflow: hidden; vertical-align: top;" align="left">
                                                            <asp:Label ID="Label7" Text='<%# Eval("Duration") %>' CssClass="labeltext" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DataList ID="dlProject" runat="server" Width="100%" OnItemCommand="dlProject_ItemCommand"
                        CssClass="labeltext" DataKeyField="ProjectID" OnItemDataBound="dlProject_ItemDataBound">
                        <ItemStyle CssClass="item" />
                        <HeaderTemplate>
                            <table width="100%" cellpadding="5">
                                <tr>
                                    <td width="25" valign="top" style="padding-left: 5px">
                                        <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkSelect')" />
                                    </td>
                                    <td style="padding-left: 5px">
                                        <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table width="100%" id="tblDetails" runat="server" border="0" cellpadding="0" cellspacing="0"
                                onmouseover="showEdit(this);" onmouseout="hideEdit(this);">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                            <tr>
                                                <td valign="top" rowspan="5" width="30" style="padding-left: 7px">
                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                </td>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <asp:HiddenField ID="hidStatus" runat="server" Value='<%# Eval("StatusId") %>' />
                                                                <asp:LinkButton ID="lnkProject" runat="server" CssClass="listHeader bold" CommandArgument='<%# Eval("ProjectID") %>'
                                                                    CommandName="_View" CausesValidation="False">
                                                                             <%# Eval("Description") + "  [" +  Eval("ProjectCode")%>] &nbsp;
                                                                </asp:LinkButton>
                                                                <span id="spanProject" runat="server" style="display: none;">
                                                                    <%# Eval("Description") + "  [" +  Eval("ProjectCode")%>] </span>
                                                            </td>
                                                            <td valign="top" align="right">
                                                                <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ProjectID") %>'
                                                                    CommandName="_Edit" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>' />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="item_title" width="150px">
                                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="StartDate"></asp:Literal>
                                                            </td>
                                                            <td width="5">
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("StartDate")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="item_title">
                                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="EndDate"></asp:Literal>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("EstimatedFinishDate")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="item_title">
                                                                <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Budget"></asp:Literal>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("Budget") %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="item_title">
                                                                <asp:Literal ID="Literal22" runat="server" meta:resourcekey="TeamMembers"></asp:Literal>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("NoOfStaff")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="item_title">
                                                                <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ReportsCommon,Status%>'></asp:Literal>
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("Status")%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <HeaderStyle CssClass="listItem" />
                    </asp:DataList>
                    <uc:Pager ID="projectPager" runat="server" />
                    <asp:Panel ID="pnlSelectedProjects" runat="server" Style="display: none;">
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlProject" EventName="ItemCommand" />
                    <asp:AsyncPostBackTrigger ControlID="fvProjects" EventName="ItemCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"
        Visible="False"></asp:Label>
    <asp:UpdatePanel ID="upPrint" runat="server">
        <ContentTemplate>
            <div id="divPrint" runat="server" style="display: none">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>'
                    WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="ImgSearch" ImageUrl="~/images/search.png " OnClick="btnSearch_Click"
                        runat="server" CausesValidation="false" />
                </div>
            </div>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add project.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" CausesValidation="false" runat="server" OnClick="lnkAdd_Click"
                                meta:resourcekey="Add"> </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/Vew Project.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" CausesValidation="false" runat="server" OnClick="lnkView_Click"
                                meta:resourcekey="View"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: block">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/E-Mail.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: block">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/printer.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" Text='<%$Resources:ControlsCommon,Print%>'
                                OnClick="lnkPrint_Click"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" CausesValidation="false" runat="server" OnClick="lnkDelete_Click"
                                Text='<%$Resources:ControlsCommon,Delete%>'></asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
