﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AllEmployeeDocuments.aspx.cs"
    Inherits="Public_AllEmployeeDocuments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List All Documents</title>

    <script src="../js/HRPower.js" type="text/javascript"></script>

    <script src="../js/Controls.js" type="text/javascript"></script>

    <script src="../js/Common.js" type="text/javascript"></script>

    <script src="../js/jquery/jquery.js?Math.Round()" type="text/javascript"></script>

    <script src="../js/jquery/jquery.timeentry.js?Math.Round()" type="text/javascript"></script>

    <%-- JQuery Auto Complete--%>
    <link href="../js/jquery/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="../js/jquery/jquery.autocomplete.js" type="text/javascript"></script>

    <script src="../js/jquery/jquery.bgiframe.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery/jquery.ui.widget.js"></script>

    <link href="../js/jquery/css/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../css/AjaxControlToolkit.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:UpdatePanel ID="upd" runat="server">
        <ContentTemplate>
            <div style="width: 90%; vertical-align: top; margin-left: 3px">
                <div style="float: left">
                    <asp:Label runat="server" ID="Type" Text='<%$Resources:ControlsCommon,Type%>'>
                    </asp:Label>
                    <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                        <asp:ListItem Value="1" Text='<%$Resources:DocumentsCommon,DocumentType%>'></asp:ListItem>
                        <asp:ListItem Value="2" Text='<%$Resources:DocumentsCommon,EmployeeName%>'></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label runat="server" ID="lblEMployee" Text="Employee">
                    </asp:Label>
                    <asp:TextBox ID="txtSearch" runat="server">
                    </asp:TextBox>
                    <asp:Label runat="server" ID="Status" Text='<%$Resources:Requests,Status%>'>
                    </asp:Label>
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Value="0" Text='<%$Resources:ControlsCommon,Any%>'></asp:ListItem>
                        <asp:ListItem Value="1" Text='<%$Resources:DocumentsCommon,Provided%>'></asp:ListItem>
                        <asp:ListItem Selected="True" Value="2" Text='<%$Resources:DocumentsCommon,NotProvided%>'></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float: left">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:ImageButton ID="btnSearch" ToolTip="Search Employee" runat="server" ImageUrl="~/images/search.png"
                        OnClick="btnSearch_Click" />
                </div>
            </div>
            <div style="float: right; width: 12%">
                <asp:ImageButton ID="btnPrint" ToolTip='<%$Resources:ControlsCommon,Print%>' runat="server" ImageUrl="~/images/print_IconNew.png"
                    OnClick="btnPrint_Click" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updDetails" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="dvNoRecord" runat="server" style="text-align: center; min-width: 100%; overflow: auto;padding-top:20px">
                <asp:Label ID="lblNoRecord" runat="server" Text="No record found" Style="color: Red"></asp:Label>
            </div>
            <div id="dvRecord" runat="server" style="clear:both; max-width: 90%; overflow: auto;">
                <asp:GridView ID="dgvAllEmployeeDocuments" runat="server" BackColor="White" Style="min-width: 100%;
                    overflow: auto" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                    OnRowDataBound="OnRowDataBound" OnItemDataBound="dgvAllEmployeeDocuments_ItemDataBound">
                    <RowStyle ForeColor="#000066" Font-Names="Tahoma" Font-Size="Small" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#45bbF3" Font-Bold="false" Font-Names="Tahoma" Font-Size="Small"
                        ForeColor="White" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
