﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Data.SqlClient;
using HRAutoComplete;


public partial class Public_Bank : System.Web.UI.Page
{
    clsBank objBank;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    private bool MblnUpdatePermission = true; 
    /// <summary>
    /// For reference control
    /// </summary>
    private string CurrentSelectedValue { get; set; }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
        // Auto complete
        this.RegisterAutoComplete();
        // 

        BankPager.Fill += new controls_Pager.FillPager(BindDatalist);

        if (!IsPostBack)
        {

            this.ReferenceControlNew1.ModalPopupID = this.mdlPopUpReference.ID;
            this.CurrentSelectedValue = "-1";

            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            DataTable dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.BankHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();
        }

       
        this.RegisterReferenceControlEvents();
    }

    private void RegisterReferenceControlEvents()
    {
        this.ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        this.ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type type = this.GetType();
        MethodInfo method = type.GetMethod(functionName);
        method.Invoke(this, null);
    }
    public void SetPermission()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();
        if (UserID != 1 && UserID != 2 && UserID != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                if (lnkView.Enabled)
                {
                    BindDatalist();
                }
            }
            else
            {
                BankPager.Visible = false;

                lblNoData.Text = GetGlobalResourceObject("CommonControls", "NoPermission").ToString();
                btnSearch.Enabled = false;
            }
        }
        else
        {
            lnkView.Enabled =MblnUpdatePermission = true;
            BindDatalist();
        }
    }
    public void EnableMenus()
    {
        //objUser = new clsUserMaster();
        //objRoleSettings = new clsRoleSettings();

        //lnkAdd.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Bank);
        lnkView.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank);
        //lnkEmail.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Email_Bank);
        //lnkPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Print_Bank);
        //lnkDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Delete_Bank);

        //if (lnkDelete.Enabled)
        //    lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlBankDetails.ClientID + "');";
        //else
        //    lnkDelete.OnClientClick = "return false;";

        //if (lnkPrint.Enabled)
        //    lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlBankDetails.ClientID + "');";
        //else
        //    lnkPrint.OnClientClick = "return false;";

        //if (lnkEmail.Enabled)
        //    lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlBankDetails.ClientID + "');";
        //else
        //    lnkEmail.OnClientClick = "return false;";
    }

    public void EnableFormViewMenus()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        //lnkAdd.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Bank);
        lnkView.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank);
        //lnkEmail.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Email_Bank);
        //lnkPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Print_Bank);
        //lnkDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Delete_Bank);

        //if (lnkDelete.Enabled)
        //    lnkDelete.OnClientClick = "return confirm('Are you sure to delete this bank details ?')";
        //else
        //    lnkDelete.OnClientClick = "return false;";

        //if (lnkPrint.Enabled)
        //    lnkPrint.OnClientClick = "return true;";
        //else
        //    lnkPrint.OnClientClick = "return false;";

        //if (lnkEmail.Enabled)
        //    lnkEmail.OnClientClick = "return showMailDialog('Type=Bank&BankNameID=" + Convert.ToString(fvBankDetails.DataKey["BankID"]) + "');";
        //else
        //    lnkEmail.OnClientClick = "return false;";
    }

    private void FillDropDown()
    {
        objBank = new clsBank();

        using (DataSet ds = objBank.FillCombos())
        {
            if (ds.Tables.Count > 0)
            {
                DropDownList ddlBank = (DropDownList)this.fvBankDetails.FindControl("rcBankName");
                this.BindDropDown(ddlBank, "Description", "BankID", ds.Tables[0], "Select");

                if (ds.Tables.Count > 1)
                {
                    DropDownList ddlCountry = (DropDownList)this.fvBankDetails.FindControl("rcCountry");
                    this.BindDropDown(ddlCountry, "CountryName", "CountryID", ds.Tables[1], "Select");
                }
            }
        }
    }

    public void FillBank()
    {
        DropDownList ddlBank = (DropDownList)this.fvBankDetails.FindControl("rcBankName");
        UpdatePanel upnlBank = (UpdatePanel)this.fvBankDetails.FindControl("upnlBank");

        if (ddlBank != null)
        {
            this.BindDropDown(ddlBank, "BankName", "BankID", new clsBindComboBox().GetBankBranches(), "Select");
            this.SetSelectedIndex(ddlBank);
            upnlBank.Update();
        }
    }

    public void FillCountry()
    {
        DropDownList ddlCountry = (DropDownList)this.fvBankDetails.FindControl("rcCountry");
        UpdatePanel upnlCountry = (UpdatePanel)this.fvBankDetails.FindControl("upnlCountry");

        if (ddlCountry != null)
        {
            this.BindDropDown(ddlCountry, "Countryname", "CountryID", new clsBindComboBox().GetCountryReference(), "Select");
            this.SetSelectedIndex(ddlCountry);
            upnlCountry.Update();
        }
    }

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource, string insertText)
    {
        if (ddwn != null)
        {
            ddwn.DataValueField = dataValueField;
            ddwn.DataTextField = dataTextField;
            ddwn.DataSource = dataSource;
            ddwn.DataBind();

            if (insertText != null && insertText.Trim() != string.Empty)
                ddwn.Items.Insert(0, new ListItem(insertText, "-1"));
        }
    }

    protected void fvBankDetails_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        int iBankID = 0;
        switch (e.CommandName)
        {

            case "Add":
                objBank = new clsBank();
                TextBox txtBranchName = (TextBox)fvBankDetails.FindControl("txtBranchName");
                TextBox txtBankCode = (TextBox)fvBankDetails.FindControl("txtBankCode");
                TextBox txtAddress = (TextBox)fvBankDetails.FindControl("txtAddress");
                TextBox txtTelephone = (TextBox)fvBankDetails.FindControl("txtTelephone");
                TextBox txtFax = (TextBox)fvBankDetails.FindControl("txtFax");

                DropDownList ddlCountry = (DropDownList)this.fvBankDetails.FindControl("rcCountry");
                DropDownList ddlBank = (DropDownList)this.fvBankDetails.FindControl("rcBankName");

                TextBox txtEmail = (TextBox)fvBankDetails.FindControl("txtEmail");
                TextBox txtWebsite = (TextBox)fvBankDetails.FindControl("txtWebsite");

                TextBox txtBankRoutingCode = (TextBox)fvBankDetails.FindControl("txtBankRoutingCode");
                Label lblMsg = (Label)fvBankDetails.FindControl("lblMsg");
                 
                objBank.BankNameID  = ddlBank.SelectedValue.ToInt32();
                objBank.Description = txtBranchName.Text;
                objBank.BankCode = txtBankCode.Text;
                objBank.Address = txtAddress.Text;
                objBank.Telephone = txtTelephone.Text;
                objBank.Fax = txtFax.Text;
                objBank.CountryID = ddlCountry.SelectedValue.ToInt32();
                objBank.Email = txtEmail.Text;
                objBank.Website = txtWebsite.Text;
                objBank.UAEBankCode = txtBankRoutingCode.Text == string.Empty ? "-1" : txtBankRoutingCode.Text;

                if (fvBankDetails.DataKey["BankBranchID"] == DBNull.Value)
                {
                    if (objBank.checkBankNameDuplication())
                    {
                        msgs.InformationalMessage("BranchName already exists");
                    }
                    else
                    {
                        iBankID = objBank.insertBankDetails();
                        if (iBankID != 0)
                        {
                            msgs.InformationalMessage("Bank details saved successfully");

                            BindFormView(iBankID, FormViewMode.ReadOnly);

                            EnableFormViewMenus();

                        }

                    }
                }
                else
                {
                    if (objBank.checkDuplication(Convert.ToInt32(ViewState["iBankNameID"])))
                    {
                        msgs.InformationalMessage("BranchName already exists");
                    }
                    else
                    {
                        iBankID = objBank.updateBankDetails(Convert.ToInt32(ViewState["iBankNameID"]));
                        if (iBankID > 0)
                        {
                            msgs.InformationalMessage("Bank details updated successfully");

                            BindFormView(iBankID, FormViewMode.ReadOnly);

                            EnableFormViewMenus();

                        }

                    }
                }

                mpeMessage.Show();
                break;

            case "Resume":
                if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank))
                {
                    BindDatalist();
                }

                break;

            case "Modify":              
                BindFormView(Convert.ToInt32(e.CommandArgument), FormViewMode.Edit);
                txtSearch.Text = null;
                ViewState["iBankNameID"] = Convert.ToInt32(e.CommandArgument);

                //lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
                //lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;


                break;

        }
    }

    protected void fvBankDetails_DataBound(object sender, EventArgs e)
    {
        //Modified by Ratheesh
        DropDownList ddlCountry = (DropDownList)this.fvBankDetails.FindControl("rcCountry");
        DropDownList ddlBank = (DropDownList)this.fvBankDetails.FindControl("rcBankName");


        this.FillDropDown();


        if (fvBankDetails.DataKey["BankID"] != DBNull.Value)
            this.SetIndex(ddlBank, fvBankDetails.DataKey["BankID"].ToInt32());


        if (fvBankDetails.DataKey["CountryID"] != DBNull.Value)
            this.SetIndex(ddlCountry, fvBankDetails.DataKey["CountryID"].ToInt32());
    }

    private void SetIndex(DropDownList ddl, int Value)
    {
        if (ddl != null)
        {
            if (ddl.Items.FindByValue(Value.ToString()) != null)
                ddl.Items.FindByValue(Value.ToString()).Selected = true;
        }
    }

    private void BindDatalist()
    {
        objBank = new clsBank();

        objBank.PageIndex = BankPager.CurrentPage + 1;
        objBank.PageSize = BankPager.PageSize;
        objBank.SortExpression = "BankID";
        objBank.SortOrder = "desc";
        objBank.SearchKey = txtSearch.Text;

        DataSet ds = objBank.FillDataList();

        if (ds.Tables[0].Rows.Count > 0)
        {
            BankPager.Total = objBank.GetRecordCount();

            dlBankDetails.DataSource = ds;
            dlBankDetails.DataBind();

            EnableMenus();

            divList.Visible = true;
            BankPager.Visible = true;
            lblBank.Attributes.Add("style", "display:none");
            lblBank.Text = string.Empty;
            lblNoData.Style["display"] = "none";
        }
        else
        {
            dlBankDetails.DataSource = null;
            dlBankDetails.DataBind();
            lblBank.Attributes.Add("style", "display:block");
            lblBank.Text = "No Bank Details added yet.";
            BankPager.Visible = false;
        }

        fvBankDetails.ChangeMode(FormViewMode.ReadOnly);
        fvBankDetails.DataSource = null;
        fvBankDetails.DataBind();
    }

    private void NewBank()
    {
        objBank = new clsBank();

        objBank.BankNameID = -1;
        DataTable dt = objBank.BindForm().Tables[0];

        DataRow dw = dt.NewRow();

        dt.Rows.Add(dw);

        fvBankDetails.DataSource = dt;
        fvBankDetails.DataBind();

        FillBank();
        divList.Visible = false;
    }

    protected void dlBankDetails_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "View":


                BindFormView(Convert.ToInt32(e.CommandArgument), FormViewMode.ReadOnly);

                //lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return true;";

                //lnkDelete.OnClientClick = "return confirm('Are you sure to delete this bank details ?')";

                //lnkEmail.OnClientClick = "return showMailDialog('Type=Bank&BankNameID=" + Convert.ToString(fvBankDetails.DataKey["BankID"]) + "');";


                break;

            //case "Modify":


            //    BindFormView(Convert.ToInt32(e.CommandArgument), FormViewMode.Edit);

            //    ViewState["iBankNameID"] = Convert.ToInt32(e.CommandArgument);

            //    lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
            //    lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;


            //    break;

            //    upDataList.Update();
            //    upFormView.Update();
        }
    }

    private void BindFormView(int iBankNameId, FormViewMode fvMode)
    {

        divList.Visible = false;

        objBank = new clsBank();

        fvBankDetails.ChangeMode(fvMode);

        objBank.BankBranchID = iBankNameId;

        fvBankDetails.DataSource = objBank.BindForm();
        fvBankDetails.DataBind();

        dlBankDetails.DataSource = null;
        dlBankDetails.DataBind();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        AddNewBank();

    }

    private void AddNewBank()
    {
        dlBankDetails.DataSource = null;
        dlBankDetails.DataBind();

        txtSearch.Text = string.Empty;

        fvBankDetails.ChangeMode(FormViewMode.Edit);
        NewBank();

        //lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
        //lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;

        upDataList.Update();
        upFormView.Update();
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        lblNoData.Style["display"] = "none";
        BindDatalist();

        upnlNoData.Update();
        upDataList.Update();
        upFormView.Update();
    }

    //protected void lnkPrint_Click(object sender, EventArgs e)
    //{
    //    objBank = new clsBank();
    //    string sBankNameIDs = string.Empty;

    //    Table tbl = new Table();

    //    if (dlBankDetails.Items.Count > 0)
    //    {
    //        foreach (DataListItem item in dlBankDetails.Items)
    //        {
    //            CheckBox chkBank = (CheckBox)item.FindControl("chkBank");
    //            if (chkBank == null)
    //                return;
    //            if (chkBank.Checked == true)
    //            {
    //                sBankNameIDs += "," + Convert.ToString(dlBankDetails.DataKeys[item.ItemIndex]);
    //            }
    //        }
    //        tbl = objBank.PrintBank(sBankNameIDs.Remove(0, 1));
    //    }
    //    else
    //    {
    //        if (fvBankDetails.CurrentMode == FormViewMode.ReadOnly)
    //        {
    //            tbl = objBank.PrintBank(Convert.ToString(fvBankDetails.DataKey["BankBranchID"]));
    //        }
    //    }
    //    tbl.Width = Unit.Percentage(100);
    //    tbl.Attributes.Add("Style", "Display:none;");

    //    pnlPrint.Controls.Add(tbl);

    //    ScriptManager.RegisterStartupScript(lnkPrint, this.GetType(), "Print", "Print('" + tbl.ClientID + "')", true);
    //}

    //protected void lnkEmail_Click(object sender, EventArgs e)
    //{
    //    string sBankNameIDs = string.Empty;

    //    if (dlBankDetails.Items.Count > 0)
    //    {
    //        foreach (DataListItem item in dlBankDetails.Items)
    //        {
    //            CheckBox chkBank = (CheckBox)item.FindControl("chkBank");
    //            if (chkBank == null)
    //                return;
    //            if (chkBank.Checked)
    //            {
    //                sBankNameIDs += "," + Convert.ToString(dlBankDetails.DataKeys[item.ItemIndex]);

    //            }
    //        }
    //        ScriptManager.RegisterStartupScript(lnkEmail, this.GetType(), "Email", "showMailDialog('Type=Bank&BankNameID=" + Convert.ToString(sBankNameIDs.Remove(0, 1)) + "');", true);
    //    }
    //    else
    //    {
    //        if (fvBankDetails.CurrentMode == FormViewMode.ReadOnly)
    //        {
    //            ScriptManager.RegisterStartupScript(lnkEmail, this.GetType(), "Email", "showMailDialog('Type=Bank&BankNameID=" + Convert.ToString(fvBankDetails.DataKey["BankBranchID"]) + "');", true);
    //        }
    //    }
    //}

    //protected void lnkDelete_Click(object sender, EventArgs e)
    //{
    //    objBank = new clsBank();
    //    objUser = new clsUserMaster();
    //    objRoleSettings = new clsRoleSettings();

    //    string sNoDelete = string.Empty;
    //    if (dlBankDetails.Items.Count > 0)
    //    {
    //        foreach (DataListItem item in dlBankDetails.Items)
    //        {
    //            CheckBox chkBank = (CheckBox)item.FindControl("chkBank");
    //            if (chkBank == null)
    //                return;
    //            if (chkBank.Checked == true)
    //            {
    //                objBank.BankNameID = Convert.ToInt32(dlBankDetails.DataKeys[item.ItemIndex]);
    //                try
    //                {
    //                    objBank.DeleteBankDetails();
    //                }
    //                catch (SqlException)
    //                {
    //                    if (sNoDelete == string.Empty)
    //                    {
    //                        sNoDelete += objBank.GetName();
    //                    }
    //                    else
    //                    {
    //                        sNoDelete += ", " + objBank.GetName();
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    else
    //    {
    //        if (fvBankDetails.CurrentMode == FormViewMode.ReadOnly)
    //        {
    //            objBank.BankNameID = Convert.ToInt32(fvBankDetails.DataKey["BankBranchID"]);

    //            try
    //            {
    //                objBank.DeleteBankDetails();
    //            }
    //            catch (SqlException)
    //            {
    //                if (sNoDelete == string.Empty)
    //                {
    //                    sNoDelete += objBank.GetName();
    //                }
    //                else
    //                {
    //                    sNoDelete += ", " + objBank.GetName();
    //                }
    //            }
    //        }
    //    }
    //    if (sNoDelete != string.Empty)
    //    {
    //        msgs.InformationalMessage("cannot delete bank");
    //        if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank))
    //        {
    //            BindDatalist();
    //        }
    //        else
    //        {
    //            AddNewBank();
    //        }

    //    }
    //    else
    //    {
    //        msgs.InformationalMessage("Bank details deleted successfully");
    //        txtSearch.Text = string.Empty;
    //        if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank))
    //        {
    //            BindDatalist();
    //        }
    //        else
    //        {
    //            AddNewBank();
    //        }
    //    }
    //    mpeMessage.Show();

    //    upDataList.Update();
    //    upFormView.Update();
    //}

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        BankPager.CurrentPage = 0;
        this.RegisterAutoComplete();

        if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Bank))
        {
            BindDatalist();

            upDataList.Update();
            upFormView.Update();
        }
    }

    protected void dlBankDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        //if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        //{

        //    LinkButton lnkBranchName = (LinkButton)e.Item.FindControl("lnkBranchName");

        //    if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Bank))
        //        lnkBranchName.Enabled = true;
        //    else
        //        lnkBranchName.Enabled = false;
        //}
        ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");
        if (imgEdit != null)
        {
            imgEdit.Enabled = ViewState["MblnUpdatePermission"] == null ? true : ((bool)ViewState["MblnUpdatePermission"]);
        }
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Bank,objUser.GetCompanyId());
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    protected void btnNewBank_Click(object sender, EventArgs e)
    {
        DropDownList ddlBank = (DropDownList)this.fvBankDetails.FindControl("rcBankName");

        this.ReferenceControlNew1.ClearAll();
        this.ReferenceControlNew1.TableName = "BankReference";
        this.ReferenceControlNew1.DataTextField = "BankName";
        this.ReferenceControlNew1.DataValueField = "BankID";
        this.ReferenceControlNew1.FunctionName = "FillBank";
        this.ReferenceControlNew1.SelectedValue = ddlBank.SelectedValue;
        this.ReferenceControlNew1.DisplayName = "Bank";
        this.ReferenceControlNew1.PopulateData();

        this.mdlPopUpReference.Show();
        this.updModalPopUp.Update();
    }

    protected void btnNewCountry_Click(object sender, EventArgs e)
    {
        DropDownList ddlCountry = (DropDownList)this.fvBankDetails.FindControl("rcCountry");

        this.ReferenceControlNew1.ClearAll();
        this.ReferenceControlNew1.TableName = "CountryReference";
        this.ReferenceControlNew1.DataTextField = "CountryName";
        this.ReferenceControlNew1.DataValueField = "CountryID";
        this.ReferenceControlNew1.FunctionName = "FillCountry";
        this.ReferenceControlNew1.SelectedValue = ddlCountry.SelectedValue;
        this.ReferenceControlNew1.DisplayName = "Country";
        this.ReferenceControlNew1.PopulateData();

        this.mdlPopUpReference.Show();
        this.updModalPopUp.Update();
    }
}
