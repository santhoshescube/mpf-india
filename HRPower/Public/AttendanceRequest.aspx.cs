﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using System.Reflection;
using System.Data.OleDb;
using System.ComponentModel;

using System.Collections.Generic;
using System.Drawing;

using Microsoft.VisualBasic;




public partial class Public_AttendanceRequest : System.Web.UI.Page
{
    clsAttendanceRequest objRequest;
    clsUserMaster objUser;
    clsLeaveRequest objLeaveRequest;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        SalaryRequestpager.Fill += new controls_Pager.FillPager(BindDataList);


        if (!IsPostBack)
        {
            int iRequestId = 0;
            if (Request.QueryString["RequestId"] != null && Request.QueryString["Type"] == "Cancel")
            {
                iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                fvAttendance.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;

            }
            else if (Request.QueryString["RequestId"] != null)
            {
                iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (iRequestId > 0)
                    ViewState["Approve"] = true;
                fvAttendance.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;

            }
            else
            {
                BindDataList();
            }
        } 
    }
    private void RemoveQueryString(string Query)
    {
        PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isReadOnly.SetValue(this.Request.QueryString, false, null);

        Request.QueryString.Remove(Query);

        Response.ClearContent();
    }
    private void BindDataList()
    {

        objRequest = new clsAttendanceRequest();
        objUser = new clsUserMaster();
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.Mode = "VIEW";
        objRequest.PageIndex = SalaryRequestpager.CurrentPage + 1;
        objRequest.PageSize = SalaryRequestpager.PageSize;
        DataTable oTable = objRequest.GetRequests();
        if (oTable.DefaultView.Count > 0)
        {
            lnkView.Enabled = true;
            dlAttendance.DataSource = oTable;
            dlAttendance.DataBind();

            SalaryRequestpager.Total = objRequest.GetRecordCount();
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlAttendance.ClientID + "');";

            lnkDelete.Enabled = true;

            fvAttendance.ChangeMode(FormViewMode.ReadOnly);
            fvAttendance.DataSource = null;
            fvAttendance.DataBind();

            SalaryRequestpager.Visible = true;
        }
        else
        {
            lnkView.Enabled = false;
            dlAttendance.DataSource = null;
            dlAttendance.DataBind();
            lnkDelete.Enabled = false;
            fvAttendance.ChangeMode(FormViewMode.Insert);
            SalaryRequestpager.Visible = false;
        }


    }
    private void BindRequestDetails(int iRequestId)
    {
        objRequest = new clsAttendanceRequest();
        objRequest.Mode = "EDT";
        objRequest.RequestId = iRequestId;

        fvAttendance.DataSource = objRequest.GetRequestDetails();
        fvAttendance.DataBind();

        dlAttendance.DataSource = null;
        dlAttendance.DataBind();

        SalaryRequestpager.Visible = false;

    }
    protected void dlAttendance_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_View":

                fvAttendance.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                if (clsUserMaster.GetCulture() == "ar-AE")
                    lnkDelete.OnClientClick = "return confirm('هل أنت متأكد أنك تريد حذف؟');";
                else
                    lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;
                break;

            case "_Edit":

                fvAttendance.ChangeMode(FormViewMode.Edit);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;

            case "_Cancel":
                Label lbRequested = (Label)e.Item.FindControl("lbRequested");
                RequestCancel(Convert.ToInt32(e.CommandArgument), lbRequested.Text);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;

        }
    }
    private void RequestCancel(int RequestId, string sRequestedIds)
    {
        objUser = new clsUserMaster();
        objRequest = new clsAttendanceRequest();
        int EmployeeID = objUser.GetEmployeeId();

        objRequest.RequestId = RequestId;
        objRequest.StatusId = Convert.ToInt32(RequestStatus.RequestForCancel);

        objRequest.UpdateCancelStatus();

        clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.AttendanceRequest , eMessageTypes.Attendance_Request , eAction.RequestForCancel, "");
        clsCommonMail.SendMail(RequestId, MailRequestType.AttendanceRequest, eAction.RequestForCancel);

        //--------------------------------------------------------------------------------------------------------------------------------------------------

        BindDataList();
        if (clsUserMaster.GetCulture() == "ar-AE")
            msgs.InformationalMessage("تم تحديث التفاصيل بنجاح.");
        else
            msgs.InformationalMessage("Successfully updated request details.");

        mpeMessage.Show();

    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ViewState["ApplyToAll"] = null;
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;

        ViewState["RequestForCancel"] = false;

        fvAttendance.ChangeMode(FormViewMode.Insert);

        dlAttendance.DataSource = null;
        dlAttendance.DataBind();

        lnkDelete.Enabled = false;

        lnkDelete.OnClientClick = "return false;";
        SalaryRequestpager.Visible = false;

        upForm.Update();
        upnlDatalist.Update();
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        BindDataList();

        upForm.Update();
        upnlDatalist.Update();
    }
   
    protected void fvAttendance_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            objRequest = new clsAttendanceRequest();
            objUser = new clsUserMaster();
            objLeaveRequest = new clsLeaveRequest();

            int EmployeeID = objUser.GetEmployeeId();

            string sToEmployees = string.Empty;
            DataList dlAttendanceDetails = (DataList)fvAttendance.FindControl("dlAttendanceDetails");
            DropDownList ddlStatus = (DropDownList)fvAttendance.FindControl("ddlEditStatus");
            TextBox txtRemarks = (TextBox)fvAttendance.FindControl("txtRemarks");
            DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");

            string sRequestedIds = string.Empty;
            string sToMailId = string.Empty;
            int iRequestedById, iRequestId = 0, CompanyID =0;

            TextBox txtVFromDate = (TextBox)fvAttendance.FindControl("txtVFromDate");
            TextBox txtVToDate = (TextBox)fvAttendance.FindControl("txtVToDate");

            switch (e.CommandName)
            {
                case "Add":

                    string RequestedTo = clsLeaveRequest.GetRequestedTo(e.CommandArgument.ToInt32(),  Convert.ToInt64(objUser.GetEmployeeId()),(int)RequestType.AttendanceRequest); 
                    if (RequestedTo == string.Empty)
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في الوقت الحالي.يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();
                    }
                    else
                    {
                        bool bIsInsert = true;

                        DropDownList ddlProject = (DropDownList)fvAttendance.FindControl("ddlProject");
                        TextBox txtFromdate = (TextBox)fvAttendance.FindControl("txtFromdate");
                        TextBox txtToDate = (TextBox)fvAttendance.FindControl("txtToDate");

                        if (Convert.ToString(e.CommandArgument) != "")
                        {
                            iRequestId = Convert.ToInt32(e.CommandArgument);

                        }
                        objRequest.EmployeeId = EmployeeID;
                       
                        if (iRequestId == 0)
                        {
                            //set values 
                            objRequest.Mode = "INS";
                        }
                        else
                        {
                            objRequest.Mode = "UPT";
                            objRequest.RequestId = iRequestId;
                            bIsInsert = false;
                        }

                        objRequest.ProjectID = ddlProject.SelectedValue.ToInt32();
                        objRequest.Fromdate = clsCommon.Convert2DateTime(txtFromdate.Text.Trim());
                        objRequest.ToDate = clsCommon.Convert2DateTime(txtToDate.Text.Trim());
                        // objRequest.Reason = txtRemarks.Text;
                        objRequest.RequestedTo = RequestedTo;

                        objRequest.StatusId = Convert.ToInt32(RequestStatus.Applied);
                        // Check for duplication.Check for period
                        if (objRequest.IsAttendanceApplied())
                        {

                            msgs.InformationalMessage(GetLocalResourceObject("RequestDetailsExist").ToString());
                            mpeMessage.Show();
                            return;
                        }
                        else if (new clsLeaveRequest().CheckSalaryProcessingOver(EmployeeID, clsCommon.Convert2DateTime(txtFromdate.Text), clsCommon.Convert2DateTime(txtToDate.Text)))
                        {
                            msgs.InformationalMessage(GetLocalResourceObject("CannotApproveRequest").ToString());
                            mpeMessage.Show();
                            return;
                        }

                        System.Collections.Generic.List<clsAttnDetails> AttnDetails = new System.Collections.Generic.List<clsAttnDetails>();

                        try
                        {


                            foreach (DataListItem dl in dlAttendanceDetails.Items)
                            {
                                if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
                                {
                                    Label lblDate = (Label)dl.FindControl("lblDate");
                                    TextBox txtFromTime = (TextBox)dl.FindControl("txtFromTime");
                                    TextBox txtToTime = (TextBox)dl.FindControl("txtToTime");
                                    TextBox txtDuration = (TextBox)dl.FindControl("txtDuration");

                                    if (lblDate != null && txtDuration.Text != "" && txtDuration.Text != "0")
                                    {
                                        AttnDetails.Add(new clsAttnDetails()
                                        {
                                            Date = clsCommon.Convert2DateTime(lblDate.Text),
                                            TimeFrom = Convert.ToDateTime(txtFromTime.Text).ToString("HH:mm"),
                                            TimeTo = Convert.ToDateTime(txtToTime.Text).ToString("HH:mm"),
                                            WorkTime  = Convert.ToDateTime(txtDuration.Text).ToString("HH:mm"),

                                        });

                                    }
                                }
                            }
                        }
                        catch 
                        {
                            msgs.InformationalMessage("Please Check Timings.");
                            mpeMessage.Show();
                            return;
                        }

                        objRequest.AttnDetails = AttnDetails;


                        iRequestId = objRequest.InsertRequest();
                        clsCommonMessage.SendMessage(EmployeeID, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Applied);

                        string message = "";
                        if (bIsInsert)
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم اضافة تفاصيل الطلب بنجاح") : ("Request details added successfully.");
                        else
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث تفاصيل الطلب بنجاح") : ("Request details updated successfully.");


                        upForm.Update();
                        upnlDatalist.Update();
                        BindDataList();
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();

                    }
                    break;

                case "CancelRequest":
                    upForm.Update();
                    upnlDatalist.Update();
                    BindDataList();
                    break;

                case "Approve":
                
                    iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);

                    objRequest.RequestId = iRequestId;
                    iRequestedById = objRequest.GetRequestedById();
                   
                    CompanyID = objRequest.GetCompanyID(iRequestId);
                    int WorkPolicyID = objRequest.GetEmpPolicyID(iRequestedById);

                    objRequest.RequestId = iRequestId;
                    objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                    objLeaveRequest.EmployeeId = EmployeeID;


                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 5) //Approved
                    { 
                        if (new clsLeaveRequest().CheckSalaryProcessingOver(iRequestedById, clsCommon.Convert2DateTime(txtVFromDate.Text), clsCommon.Convert2DateTime(txtVToDate.Text)))
                        {
                            msgs.InformationalMessage(GetLocalResourceObject("CannotApproveRequest").ToString());
                            mpeMessage.Show();
                            return;
                        }
                        if (objRequest.GetWorkStatusId() < 6)
                        {
                            string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Cannot approve the request. The requested employee is no longer in service.");
                            msgs.InformationalMessage(message);
                            mpeMessage.Show();

                        }
                        else
                        {

                            if (clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.AttendanceRequest), Convert.ToInt64(EmployeeID), objRequest.RequestId))
                            {
                                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5) // Approved
                                {
                                    if (EntryValidations(iRequestedById, iRequestId))
                                    {
                                        objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                        objRequest.UserId = objUser.GetUserId();
                                        objRequest.Reason = txtRemarks.Text;


                                        //Details
                                        System.Collections.Generic.List<clsAttnDetails> AttnDetails = new System.Collections.Generic.List<clsAttnDetails>();

                                        foreach (DataListItem dl in dlAttendanceForAction.Items)
                                        {
                                            if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
                                            {
                                                CheckBox chkSelect = (CheckBox)dl.FindControl("chkSelect");
                                                Label lblDate = (Label)dl.FindControl("lblDate");
                                                TextBox txtFromTime = (TextBox)dl.FindControl("txtFromTime");
                                                TextBox txtToTime = (TextBox)dl.FindControl("txtToTime");
                                                TextBox txtDuration = (TextBox)dl.FindControl("txtDuration");

                                                if (lblDate != null && chkSelect.Checked)
                                                {

                                                    int ShiftId = 0;
                                                    
                                                    string WorkTime = "00:00:00";
                                                    string BreakTime = "00:00:00";
                                                    string Late = "00:00:00";
                                                    string Early = "00:00:00";
                                                    string Excess = "00:00:00";
                                                    int AbsentTime = 0;

                                                    FillDateToGrid(CompanyID, iRequestedById, clsCommon.Convert2DateTime(lblDate.Text), txtFromTime.Text, txtToTime.Text, txtDuration.Text, ref ShiftId, ref WorkTime, ref BreakTime, ref Late, ref Early, ref Excess, ref AbsentTime);



                                                    objRequest.RequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                                                    objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                                                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                                    objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                                                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                                    objRequest.UserId = objUser.GetUserId();
                                                    objRequest.Reason = txtRemarks.Text;

                                                    AttnDetails.Add(new clsAttnDetails()
                                                    {
                                                        Date = clsCommon.Convert2DateTime(lblDate.Text),
                                                        TimeFrom = Convert.ToDateTime(txtFromTime.Text).ToString("hh:mm tt"),
                                                        TimeTo = Convert.ToDateTime(txtToTime.Text).ToString("hh:mm tt"),
                                                        WorkTime = WorkTime,
                                                        BreakTime =BreakTime,
                                                        Late =Late ,
                                                        Early =Early ,
                                                        Excess =Excess ,
                                                        ShiftID =ShiftId ,
                                                        WorkPolicyID =WorkPolicyID ,
                                                        CompanyID =CompanyID,
                                                        AbsentTime =AbsentTime 

                                                    });
                                                }
                                            }
                                        }

                                        objRequest.AttnDetails = AttnDetails;

                                      
                                        objRequest.UpdateAdvanceStatus(true);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                else
                                {
                                    objRequest.UpdateAdvanceStatus(true);
                                }

                                objRequest.RequestId = iRequestId;

                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 3 || Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                                {
                                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                                    msgs.InformationalMessage(message);
                                    mpeMessage.Show();



                                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                    {
                                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Approved, "");
                                        clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Approved);
                                        ////-----------------------------------------------------------------------------------------------------------
                                    }
                                    else if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                                    {
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Reject, "");
                                        clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Reject);
                                    }

                                    else if (Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                                    {
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Pending, "");
                                    }

                                }
                                else
                                {
                                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                                    msgs.InformationalMessage(message);
                                    mpeMessage.Show();
                               }
                            }

                            else
                            {


                                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue) == 5 ? 1 : Convert.ToInt32(ddlStatus.SelectedValue);
                                objRequest.UpdateAdvanceStatus(false);
                                string message = "";
                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                {
                                    clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Approved, "");

                                    clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Approved);
                                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم توجيه طلب الحصول على راتب مقدم بنجاح") : ("Request forwarded successfully.");


                                }
                                else
                                {
                                    clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Reject);
                                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم رفض طلب الحصول على راتب مسبق بنجاح") : ("Request rejected successfully.");

                                }

                                msgs.InformationalMessage(message);
                                mpeMessage.Show();

                            }
                        }


                        ViewState["Approve"] = false;
                        ViewState["RequestedAdvAmt"] = null;
                        fvAttendance.ChangeMode(FormViewMode.ReadOnly);
                        BindRequestDetails(iRequestId);
                        lnkDelete.OnClientClick = "return false;";
                        lnkDelete.Enabled = false;


                        RemoveQueryString("RequestId");


                    }
                    else
                    {
                        objRequest.RequestId = iRequestId;
                        iRequestedById = objRequest.GetRequestedById();
                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);

                        objRequest.EmployeeId = iRequestedById;
                        objRequest.UpdateAdvanceStatus(true);
                        string message = "";
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                        {
                            clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Reject, "");
                            clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Reject);
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب سلفة على المرتب رفض بنجاح.") : ("Request rejected successfully.");
                        }
                        else if (Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                        {
                            clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Pending, "");
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");

                        }
                        ViewState["Approve"] = false;
                        ViewState["RequestedAdvAmt"] = null;
                        fvAttendance.ChangeMode(FormViewMode.ReadOnly);
                        BindRequestDetails(iRequestId);
                        lnkDelete.OnClientClick = "return false;";
                        lnkDelete.Enabled = false;
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();

                        RemoveQueryString("RequestId");

                    }
                    break;
                case "Cancel":

                    this.Response.Redirect("Home.aspx");
                    break;

                case "RequestForCancel":

                    iRequestId = Convert.ToInt32(fvAttendance.DataKey["RequestId"]);
                    objRequest.RequestId = iRequestId;

                    if (new clsLeaveRequest().CheckSalaryProcessingOver(objRequest.GetRequestedById(), clsCommon.Convert2DateTime(txtVFromDate.Text), clsCommon.Convert2DateTime(txtVToDate.Text)))
                    {
                        msgs.InformationalMessage(GetLocalResourceObject("CannotApproveRequest").ToString());
                        mpeMessage.Show();
                        return;
                    }



                    string strmessage = "";
                    if (objRequest.GetWorkStatusId() < 6)
                    {
                        strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل في إلغاء الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Failed to cancel the request. The requested employee is no longer in service.");

                    }
                    else
                    {
                        objRequest.RequestId = iRequestId;
                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                        objRequest.EmployeeId = EmployeeID;

                        //Details
                        System.Collections.Generic.List<clsAttnDetails> AttnDetails = new System.Collections.Generic.List<clsAttnDetails>();

                        foreach (DataListItem dl in dlAttendanceForAction.Items)
                        {
                            if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
                            {
                                CheckBox chkSelect = (CheckBox)dl.FindControl("chkSelect");
                                Label lblDate = (Label)dl.FindControl("lblDate");
                                TextBox txtFromTime = (TextBox)dl.FindControl("txtFromTime");
                                TextBox txtToTime = (TextBox)dl.FindControl("txtToTime");
                                TextBox txtDuration = (TextBox)dl.FindControl("txtDuration");
                                HiddenField hfAttendanceID = (HiddenField)dl.FindControl("hfAttendanceID");

                                if (chkSelect.Checked)
                                {
                                    AttnDetails.Add(new clsAttnDetails()
                                    {
                                        Date = clsCommon.Convert2DateTime(lblDate.Text),
                                        TimeFrom = Convert.ToDateTime(txtFromTime.Text).ToString("hh:mm tt"),
                                        TimeTo = Convert.ToDateTime(txtToTime.Text).ToString("hh:mm tt"),
                                        WorkTime  = Convert.ToDateTime(txtDuration.Text).ToString("HH:mm"),
                                        AttendanceID = hfAttendanceID.Value.ToDecimal()

                                    });
                                }
                            }
                        }
                        objRequest.AttnDetails = AttnDetails;
                        objRequest.SalaryAdvanceRequestCancelled();
                        iRequestedById = objRequest.GetRequestedById();
                        clsCommonMail.SendMail(iRequestId, MailRequestType.AttendanceRequest, eAction.Cancelled);

                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------

                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.AttendanceRequest, eMessageTypes.Attendance_Request, eAction.Cancelled, "");

                        //-----------------------------------------------------------------------------------------------------------          

                        ViewState["RequestForCancel"] = false;
                        BindRequestDetails(iRequestId);
                        strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                    }
                    msgs.InformationalMessage(strmessage);
                    mpeMessage.Show();
                    break;

            }
        }
        catch
        {
        }

    }

    /// <summary>
    /// salary advance request validations
    /// </summary>
    private bool EntryValidations(int EmployeeID, int RequestID)
    {
        objRequest = new clsAttendanceRequest();
        Label lblDate = (Label)fvAttendance.FindControl("lblDate");

        objRequest.EmployeeId = EmployeeID;
        objRequest.RequestId = RequestID;
        string message = "";
       
        if (objRequest.GetWorkStatusId() < 6)
        {

            message = clsUserMaster.GetCulture() == "ar-AE" ? ("الموظف المطلوب لم يعد في الخدمة.") : ("The requested employee is no longer in service.");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;

        }
        return true;
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;

        if (dlAttendance.Items.Count > 0)
        {
            foreach (DataListItem item in dlAttendance.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkAdvance");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {

                    if (DeleteRequest(Convert.ToInt32(dlAttendance.DataKeys[item.ItemIndex])))
                        message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : ("Request(s) deleted successfully");
                    else
                        message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted.");

                }
            }

        }
        else
        {
            if (fvAttendance.CurrentMode == FormViewMode.ReadOnly)
            {
                if (DeleteRequest(Convert.ToInt32(fvAttendance.DataKey["RequestId"])))
                    message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : "Request deleted successfully";
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب لا يمكن حذفها.") : "Processing started request cannot be deleted.";
            }
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();

        upForm.Update();
        upnlDatalist.Update();
    }
    private bool DeleteRequest(int requestId)
    {
        string message = string.Empty;

        objRequest = new clsAttendanceRequest();
        objRequest.RequestId = requestId;

        if (objRequest.IsSalaryRequestApproved())
        {
            return false;
        }
        else
        {
            objRequest.Mode = "DEL";
            objRequest.DeleteRequest();
            clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.AttendanceRequest);
            return true;
        }
    }
    protected void ddlEditStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlEditStatus =(DropDownList) sender ;
           Button btnApprove = (Button)fvAttendance.FindControl("btnApprove");
           DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");


           if (ddlEditStatus.SelectedValue == "5")
               btnApprove.OnClientClick = "return valAttendanceDatalist('" + dlAttendanceForAction.ClientID + "');";
           else
               btnApprove.OnClientClick = "return true;";
    }   
    protected void fvAttendance_DataBound(object sender, EventArgs e)
    {

        objUser = new clsUserMaster();
        clsLoanRequest loanRequest = new clsLoanRequest();
        objLeaveRequest = new clsLeaveRequest();     
        HiddenField hfLoanDate = (HiddenField)fvAttendance.FindControl("hfLoanDate");

        int EmployeeID = objUser.GetEmployeeId();
        int RequestId = 0;

        if (Request.QueryString["RequestId"] != null)
        {
            RequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
        }

        if (fvAttendance.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == false)
        {
            HtmlTableRow trForwardedBy = (HtmlTableRow)fvAttendance.FindControl("trForwardedBy");
            if (trForwardedBy != null)
                trForwardedBy.Visible = false;

          

            HtmlTableRow trVRemarks = (HtmlTableRow)fvAttendance.FindControl("trVRemarks");
            if (trVRemarks != null && fvAttendance.DataKey["StatusId"].ToInt32() != 1)
                trVRemarks.Visible = true;

            HtmlTableRow trappAmount = (HtmlTableRow)fvAttendance.FindControl("trappAmount");
            if (trappAmount != null)
                trappAmount.Style["display"] = "table-row";

            HtmlTableRow trDetails = (HtmlTableRow)fvAttendance.FindControl("trDetails");
            if (trDetails == null) return;
            trDetails.Style["display"] = "table-row";
            DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");

            dlAttendanceForAction.DataSource = clsAttendanceRequest.FillAttnDetails(fvAttendance.DataKey["RequestId"].ToInt32());
            dlAttendanceForAction.DataBind(); 

        }

        if (fvAttendance.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == true)
        {

            Button btnApprove = (Button)fvAttendance.FindControl("btnApprove");
            DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");

            btnApprove.OnClientClick = "return valAttendanceDatalist('" + dlAttendanceForAction.ClientID + "');";

            DropDownList ddlEditStatus = (DropDownList)fvAttendance.FindControl("ddlEditStatus");

            ddlEditStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
            ddlEditStatus.DataBind();

            Label lbDate = (Label)fvAttendance.FindControl("lblDate");

            Label lblStatus = (Label)fvAttendance.FindControl("lblStatus");
        
            Button btCancel = (Button)fvAttendance.FindControl("btCancel");
          
            HtmlTableRow trRemarks = (HtmlTableRow)fvAttendance.FindControl("trRemarks");
            HtmlTableRow trDetails = (HtmlTableRow)fvAttendance.FindControl("trDetails");
            trDetails.Style["display"] = "table-row";
            dlAttendanceForAction.DataSource = clsAttendanceRequest.FillAttnDetails(RequestId);
            dlAttendanceForAction.DataBind(); 

            if (Request.QueryString["RequestId"] != null)
            {
                lblStatus.Visible = false;
                ddlEditStatus.Visible = true;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                btnApprove.Visible = true;
                btCancel.Visible = true;
            }
            else
            {
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                btnApprove.Visible = false;
                btCancel.Visible = false;
            }

            if (clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.AttendanceRequest), Convert.ToInt64(EmployeeID), objRequest.RequestId))
            {
               
                trRemarks.Style["display"] = "table-row";
                trDetails.Style["display"] = "table-row"; 
              
            }
            else
            {
                ddlEditStatus.Items.Remove(new ListItem("Pending", "4"));
               
            }

        }
        else if (fvAttendance.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestCancel"]) == true)
        {
          
            Label lbDate = (Label)fvAttendance.FindControl("lblDate");
          
            DropDownList ddlEditStatus = (DropDownList)fvAttendance.FindControl("ddlEditStatus");

            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvAttendance.FindControl("lblStatus");
            Button btnCancel = (Button)fvAttendance.FindControl("btnCancel");

            lblStatus.Visible = false;
            ddlEditStatus.Visible = true;
            ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("7"));
            ddlEditStatus.Enabled = false;
            btnCancel.Visible = true;

        }
        else if (fvAttendance.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            DropDownList ddlEditStatus = (DropDownList)fvAttendance.FindControl("ddlEditStatus");

         
            Label lbDate = (Label)fvAttendance.FindControl("lblDate");
            HtmlTableRow trDetails = (HtmlTableRow)fvAttendance.FindControl("trDetails");
            trDetails.Style["display"] = "table-row";

       
            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvAttendance.FindControl("lblStatus");
            Button btnRequestForCancel = (Button)fvAttendance.FindControl("btnRequestForCancel");

            lblStatus.Visible = false;
            ddlEditStatus.Visible = true;
            ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
            ddlEditStatus.Enabled = false;
            btnRequestForCancel.Visible = true;

            DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");

            dlAttendanceForAction.DataSource = clsAttendanceRequest.FillAttnDetails(RequestId);
            dlAttendanceForAction.DataBind();

            foreach (DataListItem itm in dlAttendanceForAction.Items)
            {
               // CheckBox chkAll = (CheckBox)itm.FindControl("chkAll");
                CheckBox chkSelect = (CheckBox)itm.FindControl("chkSelect");
                HiddenField hfAttendanceID = (HiddenField)itm.FindControl("hfAttendanceID");
                if (hfAttendanceID.Value.ToInt32() > 0) chkSelect.Checked = true;

                chkSelect.Enabled = false;
            }
        }

        if (fvAttendance.CurrentMode == FormViewMode.Insert | fvAttendance.CurrentMode == FormViewMode.Edit)
        {

            DropDownList ddlProject = (DropDownList)fvAttendance.FindControl("ddlProject");

            if (ddlProject == null) return;
            ddlProject.DataSource = clsAttendanceRequest.FillProjects(objUser.GetEmployeeId(), fvAttendance.DataKey["RequestId"].ToInt32());
            ddlProject.DataBind();

            ddlProject.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"), "-1"));

            DropDownList ddlStatus = (DropDownList)fvAttendance.FindControl("ddlStatus");
            ddlStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlStatus.DataBind();
      
            if (Convert.ToBoolean(ViewState["Approve"]) == false && (fvAttendance.CurrentMode == FormViewMode.Edit | fvAttendance.CurrentMode == FormViewMode.Insert))
            {
                ddlStatus.Enabled = false;
                ddlStatus.CssClass = "dropdownlist_disabled";
            }

         

            if (fvAttendance.CurrentMode == FormViewMode.Edit)
            {
                if (fvAttendance.DataKey["StatusId"] != DBNull.Value)
                {
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvAttendance.DataKey["StatusId"])));
                }
                if (fvAttendance.DataKey["ProjectID"] != DBNull.Value)
                {
                    ddlProject.SelectedIndex = ddlProject.Items.IndexOf(ddlProject.Items.FindByValue(Convert.ToString(fvAttendance.DataKey["ProjectID"])));
                }

            }

            DataList dlAttendanceDetails = (DataList)fvAttendance.FindControl("dlAttendanceDetails");
            dlAttendanceDetails.DataSource = clsAttendanceRequest.FillAttnDetails(fvAttendance.DataKey["RequestId"].ToInt32());

            dlAttendanceDetails.DataBind();
        }
    }
    protected void dlAttendanceForAction_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");
            CheckBox chkSelect = (CheckBox)e.Item.FindControl("chkSelect");
            HiddenField hfRequestedBy = (HiddenField)e.Item.FindControl("hfRequestedBy");
            if (objRequest.IsEmployeeOnLeave(hfRequestedBy.Value.ToInt64(), clsCommon.Convert2DateTime(lblDate.Text)))
            {
                chkSelect.Enabled = false;
                lblRemarks.Text = "[" + "Leave" + "]";
                
            }

        }
    }
    protected void dlAttendanceDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objRequest = new clsAttendanceRequest();

        DataList dlAttendanceDetails =(DataList)fvAttendance.FindControl("dlAttendanceDetails");

        TextBox txtFromTime = (TextBox)e.Item.FindControl ("txtFromTime");
        TextBox txtToTime = (TextBox)e.Item.FindControl("txtToTime");
        TextBox txtDuration = (TextBox)e.Item.FindControl("txtDuration");
        if (txtFromTime != null && txtToTime != null)
        {
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeIn" + txtFromTime.ClientID, "$(function () { $('#" + txtFromTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeOut" + txtToTime.ClientID, "$(function () { $('#" + txtToTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "Duration" + txtDuration.ClientID, "$(function () { $('#" + txtDuration.ClientID + "').timeEntry({show24Hours: false}); });", true);

            if (txtFromTime.Text != "" && txtToTime.Text != "")
            {
                txtFromTime.Text = Convert.ToDateTime(txtFromTime.Text).ToString("hh:mm tt");
                txtToTime.Text = Convert.ToDateTime(txtToTime.Text).ToString("hh:mm tt");
            }
        }

      

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblDate = (Label)e.Item.FindControl("lblDate");
            Label lblRemarks = (Label)e.Item.FindControl("lblRemarks");
            CheckBox chkApply = (CheckBox)e.Item.FindControl("chkApply");
            HtmlTableRow trAtndet = (HtmlTableRow)e.Item.FindControl("trAtndet");
          
            if (objRequest.IsEmployeeOnLeave(new clsUserMaster().GetEmployeeId(), clsCommon.Convert2DateTime(lblDate.Text)))
            {
                txtFromTime.Enabled = txtToTime.Enabled = false;
                lblRemarks.Text = "[" + "Leave" + "]";
                chkApply.Visible = false;
                ViewState["ApplyToAll"] = null;
            }

        }
    }
    public bool CheckApproved(object AttendanceID)
    {
            if (AttendanceID.ToInt64() > 0)
                return true;
            else
                return false;
    }
    public bool SetApply(object ApplyToAll)
    {
        if (ViewState["ApplyToAll"] == null)
        {
            ViewState["ApplyToAll"] = ApplyToAll;
            return true ;

        }
        else if (Convert.ToString(ViewState["ApplyToAll"]) == Convert.ToString(ApplyToAll))
        {
            return false ;
        }
        else
        {
            ViewState["ApplyToAll"] = ApplyToAll;
            return true ;
        }
    }
    protected void dlAttendance_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
            int iRequestId = 0;
            iRequestId = Convert.ToInt32(txtStatusId.Text);


            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
            if (iRequestId != 1 || Convert.ToBoolean(hdForwarded.Value) == true)
            {
                imgEdit.Enabled = false;
                imgEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton imgCancel = (ImageButton)e.Item.FindControl("imgCancel");
            if ((iRequestId == 1 && Convert.ToBoolean(hdForwarded.Value) == false) | iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
            {
                imgCancel.Enabled = false;
                imgCancel.ImageUrl = "~/images/cancel_disable.png";

            }

            Label lbDlCurrency = (Label)e.Item.FindControl("lblDlCurrency");
            clsLoanRequest request = new clsLoanRequest();
            if (objUser.GetEmployeeId() > 0)
            {
                request.EmployeeId = objUser.GetEmployeeId();
                if (lbDlCurrency != null)
                    lbDlCurrency.Text = request.GetCurrency();
            }
            else
            {
                if (lbDlCurrency != null)
                    lbDlCurrency.Text = "AED";
            }
        }
    }
    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    protected void txtFromDate_TextChanged(object sender, EventArgs e)
    {
        FillDetails();
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        FillDetails();
    } 
    private DataTable GetNewTable()
    {
        DataTable datDetails = new DataTable();
        datDetails.Columns.Add("Date", typeof(string));
        datDetails.Columns.Add("TimeFrom", typeof(string));
        datDetails.Columns.Add("TimeTo", typeof(string));
        datDetails.Columns.Add("Duration", typeof(string));

        return datDetails;
    }
    public void FillDetails()
    {

        ViewState["ApplyToAll"] = null;

        TextBox txtFromdate = (TextBox)fvAttendance.FindControl("txtFromdate");
        TextBox txtToDate = (TextBox)fvAttendance.FindControl("txtToDate");
        DataList dlAttendanceDetails = (DataList)fvAttendance.FindControl("dlAttendanceDetails");
        Label lblDate = (Label)dlAttendanceDetails.FindControl("lblDate");

        TextBox txtFromTime = (TextBox)dlAttendanceDetails.FindControl("txtFromTime");
        TextBox txtToTime = (TextBox)dlAttendanceDetails.FindControl("txtToTime");
        TextBox txtDuration = (TextBox)dlAttendanceDetails.FindControl("txtDuration");


       
        if (txtFromTime != null && txtToTime != null)
        {
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeIn" + txtFromTime.ClientID, "$(function () { $('#" + txtFromTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeOut" + txtToTime.ClientID, "$(function () { $('#" + txtToTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
            ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "Duration" + txtDuration.ClientID, "$(function () { $('#" + txtDuration.ClientID + "').timeEntry({show24Hours: false}); });", true);
        }



        DataTable datAttnDetails = GetNewTable();
        int iCounter = 0;
        DateTime  dtStartDate = clsCommon.Convert2DateTime(txtFromdate.Text);

        dlAttendanceDetails.DataSource = null;
        dlAttendanceDetails.DataBind();

        TimeSpan t = clsCommon.Convert2DateTime(txtToDate.Text) - clsCommon.Convert2DateTime(txtFromdate.Text);
        int Period = t.Days;
       
        for (iCounter = 0; iCounter <= Period; iCounter++)
        {
           
            if (iCounter == 0)
                dtStartDate = clsCommon.Convert2DateTime(txtFromdate.Text);
            else
                dtStartDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, dtStartDate);

            DataRow drRow = datAttnDetails.NewRow();
            drRow["Date"] = dtStartDate.ToString("dd-MMM-yyyy");
            drRow["TimeFrom"] = "";
            drRow["TimeTo"] = "";
            drRow["Duration"] = "";
            datAttnDetails.Rows.Add(drRow);
        }
        dlAttendanceDetails.DataSource = datAttnDetails;
        dlAttendanceDetails.DataBind();

    }
    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {

        if (Convert.ToBoolean(ViewState["Approve"]) == true)
        {

        DataList dlAttendanceForAction = (DataList)fvAttendance.FindControl("dlAttendanceForAction");
        foreach (DataListItem item in dlAttendanceForAction.Items)
        {

            CheckBox  chkSelect =(CheckBox ) item.FindControl("chkSelect");

            if (chkSelect != null && chkSelect.Enabled )
            {
                ((CheckBox)chkSelect).Checked = ((System.Web.UI.WebControls.CheckBox)(sender)).Checked;
            }

        }
        }
    }
    protected void chkApply_CheckedChanged(object sender, EventArgs e)
    {
        DataList dlAttendanceDetails = (DataList)fvAttendance.FindControl("dlAttendanceDetails");
        int i = 0;
        string FromTime = string.Empty;
        string ToTime = string.Empty;
        string Duration = string.Empty;
        CheckBox chkApply = (CheckBox)sender;
       
            foreach (DataListItem item in dlAttendanceDetails.Items)
            {
                TextBox txtFromTime = (TextBox)item.FindControl("txtFromTime");
                TextBox txtToTime = (TextBox)item.FindControl("txtToTime");
                TextBox txtDuration = (TextBox)item.FindControl("txtDuration");
                if (txtFromTime != null && txtToTime != null)
                {
                    ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeIn" + txtFromTime.ClientID, "$(function () { $('#" + txtFromTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
                    ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "TimeOut" + txtToTime.ClientID, "$(function () { $('#" + txtToTime.ClientID + "').timeEntry({show24Hours: false}); });", true);
                    ScriptManager.RegisterClientScriptBlock(dlAttendanceDetails, dlAttendanceDetails.GetType(), "Duration" + txtDuration.ClientID, "$(function () { $('#" + txtDuration.ClientID + "').timeEntry({show24Hours: false}); });", true);
                }


                if (txtFromTime.Enabled && FromTime =="")
                {
                    FromTime = txtFromTime.Text;
                    ToTime = txtToTime.Text;
                    Duration = txtDuration.Text;

                    continue;
                }

                if (FromTime != "")
                {
                    if (txtFromTime.Enabled)
                    {
                        txtFromTime.Text = FromTime;
                        txtToTime.Text = ToTime;
                        txtDuration.Text = Duration;
                    }
                }
                i++;
            }
    }



    // -------------------------------------------------------Attendance approval Crieteriasssssssssss


    private void FillDateToGrid(int CompID, int EmpID, DateTime AtnDate, string FromTime, string ToTime, string Duration, ref int ShiftId, ref string ColDgvWorkTime, ref string ColDgvBreakTime, ref string ColDgvLateComing, ref string ColDgvEarlyGoing, ref string ColDgvExcessTime,ref int ColDgvAbsentTime)
    {
        objRequest = new clsAttendanceRequest();

        try
        {
            //string ColDgvLateComing = string.Empty;
            //string ColDgvEarlyGoing = string.Empty;
            //string ColDgvWorkTime = string.Empty;
            //string ColDgvBreakTime = string.Empty;
            //string ColDgvExcessTime = string.Empty;
           // int ColDgvAbsentTime = 0;


            bool IsHoliday = false;
            bool IsLeave = false;
            string ColDgvDay = string.Empty;
            //int ShiftId = 0;
            int WorkPolicyID =0;
            //string BreakTime ="00:00:00";
            //string Late ="00:00:00";
            //string Early ="00:00:00";

          //  ---------------------------

            DateTime dpDate = AtnDate;
            int iDaycount = 30;
            bool bLeave = false;


            dpDate = AtnDate;
            iDaycount = System.Data.Linq.SqlClient.SqlMethods.DateDiffDay(dpDate, dpDate.AddMonths(1));
          
           
                bLeave = false;
                bool leaveflag = false;
                bool blnHoliday = false;
                bool blnOffDay = false;
                bool blnLeaveDay = false;

                blnOffDay = objRequest.CheckOffDay(EmpID, dpDate.DayOfWeek.ToString(), dpDate);
                blnHoliday = objRequest.CheckHoliday(CompID , dpDate.ToString("dd MMM yyyy"));
                if (blnOffDay != true && blnHoliday != true)//leave only handled if it is not holiday and off day
                {
                    blnLeaveDay = objRequest.CheckLeave(EmpID, dpDate.ToString("MM/dd/yyyy"), ref leaveflag);
                }

                if (blnLeaveDay == false && blnHoliday == false && blnOffDay == false)
                    bLeave = false;
                else
                    bLeave = true;

              
                //DgvAttendance.Rows[iRw].Cells["ColDgvAtnDate"].Value = Convert.ToString(dpDate.ToString("dd MMM yyyy"));
                //DgvAttendance.Rows[iRw].Cells["ColDgvDay"].Value = dpDate.DayOfWeek.ToString();
                //DgvAttendance.Rows[iRw].Cells["ColDgvValidFlag"].Value = 0;
                ColDgvDay = dpDate.DayOfWeek.ToString();

                if (bLeave == false)
                {
                    IsHoliday =IsLeave =false ;
                  
                }
                else
                {
                    if (leaveflag == false)//'leave entry 
                    {
                        IsHoliday =true ;
                        IsLeave =false ;
                      
                    }

                    else
                    {
                        IsHoliday =false ;
                        IsLeave =true  ;
                    }
                }

                string sFromtime = "";
                string sTotime = "";
                string sShiftName = "";
                string sDuration = "";
                int ishiftType = 0;
                int iNoOfTimings = 0;
                string sMinWorkHours = "";
                string sAllowedBreak = "0";
                int iLate = 0;
                int iEarly = 0;

                if (GetEmployeeShiftInfo(EmpID, CompID, AtnDate , ref ShiftId,
                       ref sShiftName, ref sFromtime, ref sTotime, ref sDuration, ref ishiftType, ref iNoOfTimings, ref sMinWorkHours, ref sAllowedBreak, ref iLate, ref iEarly))
                {
                    ShiftId  = ShiftId ;
                  
                }

                WorkTimeCalculation(CompID, EmpID, AtnDate, FromTime, ToTime, Duration, ShiftId, sAllowedBreak, ref ColDgvLateComing, ref ColDgvEarlyGoing, ref ColDgvWorkTime, ref ColDgvBreakTime, ref ColDgvExcessTime,ref ColDgvAbsentTime);

            }
        
        catch (Exception Ex)
        {
          
        }
    }

    private bool GetEmployeeShiftInfo(int intEmpID, int intCmpID, DateTime atnDate, ref int iShiftId, ref string sShiftName, ref string sFromtime,
                                   ref string sTotime, ref string sDuration, ref int ishiftType,
                                     ref int iNoOfTimings, ref string sMinWorkHours, ref string sAllowedBreak, ref int iLate, ref int iEarly)
    {
        DataTable dtEmployeeInfo = new DataTable();

        iShiftId = 0;
        sShiftName = "";
        sFromtime = "";
        sTotime = "";
        sDuration = "";
        ishiftType = 0;
        iNoOfTimings = 0;
        sMinWorkHours = "0";
        sAllowedBreak = "0";
        iLate = 0;
        iEarly = 0;

        int intDayID = 0;
        intDayID = (int)atnDate.DayOfWeek + 1;
        string sFromDate = atnDate.ToString("dd MMM yyyy");
        string sToDate = atnDate.ToString("dd MMM yyyy");

        try
        {
            dtEmployeeInfo = objRequest.GetEmployeeShiftInfo(intEmpID, intCmpID, sFromDate);//Shift Schedule fromDAte and toDAte are same
            if (dtEmployeeInfo.Rows.Count > 0)
            {

                ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["ShiftName"]);
                //sFromtime = Convert.ToString(dtEmployeeInfo.Rows[0]["FromTime"]);
                //sTotime = Convert.ToString(dtEmployeeInfo.Rows[0]["ToTime"]);
                sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
            }
            else
            {
                bool dailyShift = objRequest.CheckDay(intEmpID, intDayID, atnDate);

                if (dailyShift == true)
                {

                    dtEmployeeInfo = objRequest.GetDailyShift(intEmpID, intDayID, atnDate);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                        //sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                        //sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeOut1"])));
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                        iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                    }

                }
                else
                {
                    dtEmployeeInfo = objRequest.GetOffDayShift(intEmpID, intDayID, atnDate);
                    if (dtEmployeeInfo.Rows.Count > 0)
                    {
                        iShiftId = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftID"]);
                        sShiftName = Convert.ToString(dtEmployeeInfo.Rows[0]["Shift"]);
                        //sFromtime = Convert.ToString(ConvertDate24(Convert.ToDateTime(dtEmployeeInfo.Rows[0]["TimeIn1"])));
                        //sTotime = Convert.ToString(ConvertDate24(Convert.ToDateTime((dtEmployeeInfo.Rows[0]["TimeOut1"]))));
                        ishiftType = Convert.ToInt32(dtEmployeeInfo.Rows[0]["ShiftTypeID"]);
                        sDuration = Convert.ToString(dtEmployeeInfo.Rows[0]["Duration"]);
                        iNoOfTimings = Convert.ToInt32(dtEmployeeInfo.Rows[0]["NoOfTimings"]);
                        sMinWorkHours = Convert.ToString(dtEmployeeInfo.Rows[0]["MinWorkingHours"]);
                        sAllowedBreak = Convert.ToString(dtEmployeeInfo.Rows[0]["AllowedBreakTime"]);
                        iLate = Convert.ToInt32(dtEmployeeInfo.Rows[0]["LateAfter"]);
                        iEarly = Convert.ToInt32(dtEmployeeInfo.Rows[0]["EarlyBefore"]);
                    }

                }
            }
            return true;
        }
        catch (Exception Ex)
        {
            return false;
        }
    }



    private bool WorkTimeCalculation(int CompID, int EmpID, DateTime AtnDate, string FromTime, string ToTime, string Duration, int ShiftId, string AllowedBreak,
                            ref string ColDgvLateComing, ref string ColDgvEarlyGoing, ref string ColDgvWorkTime, ref string ColDgvBreakTime, ref string ColDgvExcessTime, ref int ColDgvAbsentTime)// binoUtonly= false
    {
        try
        {

            //string ColDgvLateComing = string.Empty;
            //string ColDgvEarlyGoing = string.Empty;
            //string ColDgvWorkTime = string.Empty;
            //string ColDgvBreakTime =string.Empty ;
            //string ColDgvExcessTime = string.Empty;
            //int ColDgvAbsentTime = 0;


            string sWorkTime = "0";
            string sBreakTime = "0";
            string sOTBreakTime = "0";
            int iAbsentTime = 0;
            string sOt = "0";
            string sPreviousTime = "";
            bool bShiftChangeTime = true;
            bool bShiftChangeTimeBrkOT = true;
            bool bShiftChangeTimeEarlyWRk = true;
            bool bShiftChangeTimeEarlyBRK = true;
            bool blnNightShiftFlag = false;
            int intDurationInMinutes = 0;
            int intMinWorkHrsInMinutes = 0;
            bool blnIsMinWrkHRsAsOT = false;
            //int iCountIndex = DgvAttendance.Columns["ColDgvAddMore"].Index - 1;
            //if (bInOutOnly)
            //    iCountIndex = 4;

            int k = 2;
            //for (int j = 3; j <= iCountIndex; j++)
            //{
            //    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() == "")
            //        break;
            //    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value).Trim() != "")
            //        k = k + 1;
            //}

            //if (k % 2 == 1)
            //{
            //    DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.IndianRed;
            //    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 0;
            //}
            //else if (k == 0)
            //{
            //    return false;
            //}
            //else
            //{
            //    DgvAttendance.Rows[iRowIndex].Cells["ColDgvValidFlag"].Value = 1;
            //    if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvHolidayFlag"].Value.ToInt32() == 1 || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1) //Then 'HOLIDAY CHEKINGG
            //    {
            //        if (DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
            //        {

            //            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
            //        }
            //        else
            //        {
            //            DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Red;
            //        }
            //    }
            //    else
            //    {
            //        DgvAttendance.Rows[iRowIndex].DefaultCellStyle.ForeColor = Color.Black;
            //    }
            //}

            DateTime atnDate = AtnDate;
            string strAtnDate = atnDate.ToString("dd MMM yyyy");
            int iEmpId = EmpID ;
            int iCmpId = CompID ;

            int iShiftId = ShiftId;//Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvShift"].Tag);

            string sAllowedBreakTime = AllowedBreak != null && AllowedBreak != string.Empty ? AllowedBreak : "0";
            int iDayID = Convert.ToInt32(atnDate.DayOfWeek) + 1;
            string sFromDate = "";
            string sToDate = "";
            string sFromTime = "";
            string sToTime = "";
            string sDuration = "";

            int iShiftType = 0;
            int iNoOfTimings = 0;
            string sMinWorkHours = "0";
            int iLate = 0;
            int iEarly = 0;
            bool bOffDay = true;
            int iShiftOrderNo = 0;
            DataTable dtShiftInfo;
            dtShiftInfo = objRequest.GetShiftInfo(4, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
            if (dtShiftInfo.Rows.Count > 0)
            {
                bOffDay = false;
                sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                iLate = Convert.ToInt32(dtShiftInfo.Rows[0]["LateAfter"]);
                iEarly = Convert.ToInt32(dtShiftInfo.Rows[0]["EarlyBefore"]);
                sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
            }
            if (bOffDay == true)
            {
                dtShiftInfo = objRequest.GetShiftInfo(5, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
                if (dtShiftInfo.Rows.Count > 0)
                {
                    sFromTime = Convert.ToString(dtShiftInfo.Rows[0]["FromTime"]);
                    sToTime = Convert.ToString(dtShiftInfo.Rows[0]["ToTime"]);
                    sDuration = Convert.ToString(dtShiftInfo.Rows[0]["Duration"]);
                    iShiftType = Convert.ToInt32(dtShiftInfo.Rows[0]["ShiftTypeID"]);
                    iNoOfTimings = Convert.ToInt32(dtShiftInfo.Rows[0]["NoOfTimings"]);
                    sMinWorkHours = Convert.ToString(dtShiftInfo.Rows[0]["MinWorkingHours"]);
                    sAllowedBreakTime = Convert.ToString(dtShiftInfo.Rows[0]["AllowedBreakTime"]);
                }
            }
            if (iShiftType == 3)// Then 'for Dynamic Shift
            {
                string dFirstTime = FromTime; //Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[3].Value);
                string sFirstTime = Convert.ToDateTime(dFirstTime).ToString("HH:mm");

               // iShiftOrderNo = GetShiftOrderNo(iShiftId, sFirstTime, ref sFromTime, ref  sToTime, ref sDuration);
                objRequest.GetDynamicShiftDuration(iShiftId, iShiftOrderNo, ref sMinWorkHours, ref sAllowedBreakTime);
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvShiftOrderNo"].Value = iShiftOrderNo;
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Tag = Convert.ToDouble(sAllowedBreakTime);
                string sAlldBreakT = sAllowedBreakTime;
                sAlldBreakT = Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sAlldBreakT), DateTime.Today).ToString();
                sAlldBreakT = Convert.ToDateTime(sAlldBreakT).ToString("HH:mm:ss");
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value = sAlldBreakT;

            }

            dtShiftInfo = objRequest.GetShiftInfo(3, iEmpId, iShiftId, iDayID, sFromDate, sToDate, iCmpId);
            blnIsMinWrkHRsAsOT = objRequest.isAfterMinWrkHrsAsOT(iShiftId);
            intDurationInMinutes = GetDurationInMinutes(sDuration);//Getting Duration In mintes
            int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(intDurationInMinutes), Convert.ToDateTime(sToTime));
            if (tmDurationDiff < 0) //For Night Shift
            {
                blnNightShiftFlag = true;
                sFromTime = (Convert.ToDateTime(sFromTime)).ToString();
                sToTime = Convert.ToString(Convert.ToDateTime(sToTime).AddDays(1));
            }

            int iAdditionalMinutes = 0;
            bool bEarly = true;
            bool bLate = true;
            iAdditionalMinutes = objRequest.GetLeaveExAddMinutes(iEmpId, Convert.ToDateTime(strAtnDate));
            if (iAdditionalMinutes > 0)
            {
                sAllowedBreakTime = Convert.ToString(Convert.ToInt32(sAllowedBreakTime) + Convert.ToInt32(iAdditionalMinutes));
                bEarly = false;
                bLate = false;
            }
            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
            //lblLateComing.Visible = false;
            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
            //lblEarlyGoing.Visible = false;

            ColDgvLateComing = "00:00:00";
            ColDgvEarlyGoing = "00:00:00";


            int iBufferForOT = 0;
            bool blnConsiderBufferForOT = false;
            int iMinOtMinutes = 0;
            blnConsiderBufferForOT = objRequest.IsConsiderBufferTimeForOT(iShiftId, ref iBufferForOT, ref iMinOtMinutes);
            intMinWorkHrsInMinutes = GetDurationInMinutes(sMinWorkHours);//Minwork hrs Setting

            if (iShiftType <= 3)
            {

                for (int j = 3; j <= k + 2; j++)
                {

                    DateTime dResult = j == 3 ? Convert.ToDateTime(FromTime) : Convert.ToDateTime(ToTime); //Convert.ToDateTime(DgvAttendance.Rows[iRowIndex].Cells[j].Value);
                   
                    string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");
                    if (iShiftType == 3)//'Modified for Dynamic Shift
                    {
                        if (blnNightShiftFlag == true)
                        {
                            if (Convert.ToDateTime(sTimeValue) >= Convert.ToDateTime("12:00 AM") &&
                                Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime("12:00 PM"))
                            { sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt"); }

                        }
                    }

                    if (j == 3)
                    {
                        string sFrm = Convert.ToDateTime(sFromTime).ToString("HH:mm tt");
                        string sto = Convert.ToDateTime(sToTime).ToString("HH:mm tt");
                        if (blnNightShiftFlag && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sto))
                        {
                            if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm) && Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sFrm).AddMinutes(-120))
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString("dd MMM yyyy HH:mm tt");
                        }
                        if (bLate)
                        {
                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate) && iShiftType != 2)
                            {

                                int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                if (il > 0)
                                {
                                    string sLate = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today));
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    ColDgvLateComing = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                }

                                //lblLateComing.Tag = 1;
                                //lblLateComing.Visible = true;
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes

                            }
                            else
                            {
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                //lblLateComing.Tag = 0;
                                //lblLateComing.Visible = false;
                                ColDgvLateComing = "00:00:00";
                            }
                        }
                    }
                    else
                    {

                        int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));

                        if (tmDiff < 0)
                        {
                            tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                            sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                        }
                        if (j % 2 == 0)
                        {
                            if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && iShiftType != 2 && bEarly == true)
                            {
                                int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                if (iE > 0)
                                {
                                    string sEarly = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today));
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    ColDgvEarlyGoing = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                }

                                //lblEarlyGoing.Visible = true;
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes

                            }

                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                            {

                                if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                {
                                    bShiftChangeTime = false;
                                    int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime));
                                    int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));

                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                    sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                }

                                else
                                {

                                    sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));

                                }
                            }
                            else
                            {
                                bool bflag = false;

                                if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime))
                                    bflag = true;

                                if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime))
                                {
                                    bShiftChangeTimeEarlyWRk = false;
                                    int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime), Convert.ToDateTime(sTimeValue));
                                    sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork.ToString());
                                    bflag = true;
                                }
                                if (bflag == false)
                                    sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));

                            }

                        }
                        else//brek time
                        {


                            if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                            {
                                //lblEarlyGoing.Visible = false;
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                ColDgvEarlyGoing = "00:00:00";
                                if (bShiftChangeTimeBrkOT = true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                {
                                    bShiftChangeTimeBrkOT = false;
                                    int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                    sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));
                                    sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                }
                                else
                                {
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff));
                                }
                            }
                            else
                            {

                                bool bflag = false;
                                if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                {
                                    bflag = true;
                                }
                                if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sFromTime).AddMinutes(iLate) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate))
                                {
                                    bShiftChangeTimeEarlyBRK = false;
                                    int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                    bflag = true;
                                }
                                if (bflag == false)
                                {
                                    sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                }

                                //lblEarlyGoing.Visible = false;
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                ColDgvEarlyGoing = "00:00:00";
                            }

                        }
                    }

                    sPreviousTime = sTimeValue;
                    //if (Glbln24HrFormat == false)
                    //{
                    //    sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                    //    DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                    //}
                    //else
                    //{
                    //    DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                    //}


                }//for
            }


            // -----------------------------------------------To be included----------------------------------------

            else if (iShiftType == 4)
            {
                int intMinWorkHrsInMinutesIf = 0;
                intDurationInMinutes = GetSplitShiftTotalDuration(iShiftId);
                sWorkTime = "0";
                sBreakTime = "0";
                sOt = "0";
                int intTotalSplitAbsentTm = 0;
                int intTotalAbsentTm = 0;
                WorktimeCalculationForSplitShift(ShiftId, k, sFromTime , sToTime  , iLate , iEarly, bLate,
                                                 bEarly, "", blnIsMinWrkHRsAsOT, blnConsiderBufferForOT, iBufferForOT, ref sWorkTime, ref sBreakTime,
                                                 ref sOt, ref intTotalSplitAbsentTm, ref intTotalAbsentTm,ref ColDgvLateComing, ref ColDgvEarlyGoing,FromTime,ToTime);


                if (intDurationInMinutes > intMinWorkHrsInMinutes)  //'Split shift Absent           
                {

                    //if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
                    //{
                    //    intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes / 2;
                    //}
                    //else
                    //{
                        intMinWorkHrsInMinutesIf = intMinWorkHrsInMinutes;
                   // }

                    if (iAdditionalMinutes > 0)
                        iAbsentTime = intMinWorkHrsInMinutesIf - (Convert.ToInt32(sWorkTime) + iAdditionalMinutes);
                    else
                        iAbsentTime = intMinWorkHrsInMinutesIf - Convert.ToInt32(sWorkTime);
                }
                else
                {
                    if (intTotalSplitAbsentTm > (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate))
                    {
                        iAbsentTime = intTotalAbsentTm + (intTotalSplitAbsentTm - (Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate));
                    }
                    else
                    {
                        iAbsentTime = intTotalAbsentTm;
                    }
                }
            }


            // -----------------------------------------------To be included End----------------------------------------


            //if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave)) //Then 'for halfday
            //{
            //    if (Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value) == "1")
            //    {
            //        if (Convert.ToDouble(sWorkTime) >= (intMinWorkHrsInMinutes / 2))
            //        {

            //            if (blnIsMinWrkHRsAsOT == true)// Then 'Checking whether calculation is based on minimum working hour 
            //            {
            //                int ihalfday = (intMinWorkHrsInMinutes / 2);
            //                int addOt = Convert.ToInt32(sWorkTime) - ihalfday;
            //                sWorkTime = ihalfday.ToString();
            //                sOt = Convert.ToString(Convert.ToInt32(sOt) + addOt);
            //            }
            //        }
            //        if (intDurationInMinutes > 0)
            //            intDurationInMinutes = intDurationInMinutes / 2;
            //        if (intMinWorkHrsInMinutes > 0)
            //            intMinWorkHrsInMinutes = intMinWorkHrsInMinutes / 2;
            //    }
            //}
            //else
            //{

                if (blnIsMinWrkHRsAsOT == true)
                {
                    if (Convert.ToDouble(sWorkTime) == Convert.ToDouble(intMinWorkHrsInMinutes))
                    { }//'
                    else if (Convert.ToDouble(sWorkTime) < Convert.ToDouble(intMinWorkHrsInMinutes))
                    { //'sOt = "0"
                        if (intDurationInMinutes > intMinWorkHrsInMinutes)
                        { sOt = "0"; }
                        else// 'duration and minWorkhours are same then Consider Convert.ToInt32(sAllowedBreak) + iEarly + iLate)
                        {
                            if (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate < intMinWorkHrsInMinutes)
                            { sOt = "0"; }
                        }
                    }
                    else if (Convert.ToDouble(sWorkTime) > Convert.ToDouble(intMinWorkHrsInMinutes))
                    {
                        int Ot = Convert.ToInt32(sWorkTime) - Convert.ToInt32(intMinWorkHrsInMinutes);
                        sOt = Convert.ToString(Convert.ToInt32(sOt) + Ot);
                        sWorkTime = intMinWorkHrsInMinutes.ToString();
                    }
                }

            //}
            if (Convert.ToInt32(sOt) < iMinOtMinutes)
                sOt = "0";
            if (sOt != "" && sOt != "0")//Ot Template
                sOt = GetOt(sOt);

            bool blnIsHoliday = false;
            blnIsHoliday = objRequest.IsHoliday(iEmpId, iCmpId, atnDate, iDayID); //'cheking current date is holiday

            if (iShiftType <= 3)
            {

                if (iAdditionalMinutes > 0)
                    intMinWorkHrsInMinutes = intMinWorkHrsInMinutes - iAdditionalMinutes;

                if (intDurationInMinutes > intMinWorkHrsInMinutes)
                {
                    iAbsentTime = intMinWorkHrsInMinutes - Convert.ToInt32(sWorkTime);
                }
                else
                {
                    iAbsentTime = intMinWorkHrsInMinutes - (Convert.ToInt32(sWorkTime) + Convert.ToInt32(sAllowedBreakTime) + iEarly + iLate);
                }
            }

            int iShortage = 0;
            if (iShiftType == 2 || iShiftType == 4)
            { }
            else
            {
                if (blnIsHoliday == false)
                {
                    iShortage = Convert.ToInt32(sBreakTime == "0" ? "0" : sBreakTime) - Convert.ToInt32(sAllowedBreakTime);
                    if (iShortage > iAbsentTime)
                    {
                        iAbsentTime = iShortage;
                    }
                }
            }
            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvAbsentTime"].Value = iAbsentTime > 0 ? iAbsentTime : 0;

            ColDgvAbsentTime = iAbsentTime > 0 ? iAbsentTime : 0;

            int iAbsent = iAbsentTime > 0 ? iAbsentTime : 0;
            //lblShortageTime.ForeColor = iAbsent > 0 ? Color.DarkRed : System.Drawing.SystemColors.ControlText;
            string sShortage = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iAbsent), DateTime.Today));
           // lblShortageTime.Text = Convert.ToDateTime(sShortage).ToString("HH:mm:ss");
            //------------------------------------

           // lblAllowedBreakTime.Text = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells["ColDgvAllowedBreakTime"].Value);

            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Tag = Convert.ToDouble(sWorkTime);

            //ColDgvWorkTime = Convert.ToDouble(sWorkTime);

            sWorkTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sWorkTime), DateTime.Today));
            sWorkTime = Convert.ToDateTime(sWorkTime).ToString("HH:mm:ss");
           // DgvAttendance.Rows[iRowIndex].Cells["ColDgvWorkTime"].Value = sWorkTime;
            ColDgvWorkTime = sWorkTime;
            //lblWorkTime.Text = sWorkTime;
            if (sBreakTime != "" && sBreakTime != "0")
            {
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = sBreakTime;
                sBreakTime = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sBreakTime), DateTime.Today));
                sBreakTime = Convert.ToDateTime(sBreakTime).ToString("HH:mm:ss");
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = sBreakTime;
                //lblBreakTime.Text = sBreakTime;

                ColDgvBreakTime = sBreakTime;
            }
            else
            {
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Tag = 0;
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvBreakTime"].Value = "";
                //lblBreakTime.Text = "00:00:00";
                ColDgvBreakTime = "";
            }

            if (Convert.ToString(ColDgvWorkTime) != "" && Convert.ToDouble(sOt) > 0)
            {
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = sOt;
                sOt = Convert.ToString(Microsoft.VisualBasic.DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(sOt), DateTime.Today));
                sOt = Convert.ToDateTime(sOt).ToString("HH:mm:ss");
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = sOt;
                //lblExcessTime.Text = sOt;
                ColDgvExcessTime = sOt;
            }
            else
            {
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Tag = 0;
                //DgvAttendance.Rows[iRowIndex].Cells["ColDgvExcessTime"].Value = "";
                //lblExcessTime.Text = "00:00:00";
                ColDgvExcessTime = "";
            }

            return true;
        }
        catch (Exception Ex)
        {

            //MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            return false;
        }

    }
    private string GetOt(string StrOt)
    {
        string StrOtReturn = "0";
        try
        {
            int iOt = 0;
            if (Int32.TryParse(StrOt, out iOt) == false)
                StrOt = "0";

            StrOtReturn = StrOt;
            string strStartMin = "";
            string strEndMin = "";
            string strOTMin = "";

            DataTable dtOtDetails = objRequest.GetOtDetails();
            if (dtOtDetails != null)
            {
                if (dtOtDetails.Rows.Count > 0)
                {
                    foreach (DataRow row in dtOtDetails.Rows)
                    {
                        strStartMin = (row["StartMin"] == DBNull.Value ? "0" : Convert.ToString(row["StartMin"]));
                        strEndMin = (row["EndMin"] == DBNull.Value ? "0" : Convert.ToString(row["EndMin"]));
                        strOTMin = (row["OTMin"] == DBNull.Value ? "0" : Convert.ToString(row["OTMin"]));
                        if (iOt >= Convert.ToInt32(strStartMin) && iOt <= Convert.ToInt32(strEndMin))
                        {
                            StrOtReturn = strOTMin;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            //MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);

        }
        return StrOtReturn;
    }
    private int GetDurationInMinutes(string sDuration)// 'Getting Duration In minutes
    {
        int iDurationInMinutes = 0;
        try
        {
            string[] sHourMinute = new string[2];

            if (sDuration.Contains('.'))
            {
                sHourMinute = sDuration.Split('.');
                iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);

            }
            else if (sDuration.Contains(':'))
            {
                sHourMinute = sDuration.Split(':');
                iDurationInMinutes = Convert.ToInt32(sHourMinute[0]) * 60 + Convert.ToInt32(sHourMinute[1]);
            }
            else if (sDuration != "")
            {
                iDurationInMinutes = Convert.ToInt32(sDuration) * 60;
            }
            return iDurationInMinutes;
        }
        catch (Exception Ex)
        {

           // MobjClsLogs.WriteLog("Error on :" + new System.Diagnostics.StackTrace().GetFrame(0).GetMethod().Name.ToString() + " " + this.Name.ToString() + " " + Ex.Message.ToString(), 1);
            return iDurationInMinutes;
        }
    }




    private int GetSplitShiftTotalDuration(int ishiftID)
    {
        //'in split Shift Total Duration is Sum of  details shift duration
        int intTotalSplitShiftDurInMinutes = 0;
        try
        {

            DataTable DtShiftDetails = objRequest.GetDynamicShiftDetails(ishiftID);
            if (DtShiftDetails.Rows.Count > 0)
            {
                for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                {
                    string strDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                    int intDurInMin = GetDurationInMinutes(strDuration);
                    intTotalSplitShiftDurInMinutes += intDurInMin;
                }
            }
            return intTotalSplitShiftDurInMinutes;
        }
        catch (Exception ex)
        {
           // MobjClsLogs.WriteLog("Error on GetSplitShiftTotalDuration  :" + this.Name + " --" + ex.Message.ToString(), 1);
            return 0;
        }
    }


    private bool WorktimeCalculationForSplitShift(int iShiftID, int k,  string sFromTime, string sToTime, int iLate, int iEarly,
                                              bool bLate, bool bEarly, string sEmpName, bool isAfterMinWrkHrsAsOT, bool blnConsiderBufferForOT, int iBufferForOT,
                                              ref string sWorkTime, ref  string sBreakTime, ref string sOt, ref int intTotalSplitAbsentTm, 
                                                ref int intTotalAbsentTm, ref string ColDgvLateComing, ref string ColDgvEarlyGoing, string FromTime,string ToTime)
    {//'Worktime Calculation for Split shift 
        try
        {

            //int ColDgvShiftOrderNo = 0;
            //string ColDgvLateComing = "00:00:00";
            //string ColDgvEarlyGoing = "00:00:00";


            sWorkTime = "0";
            sBreakTime = "0";
            sOt = "0";
            string sOvertime = "0";
            bool nightFlag = false;

            int intWorkTm = 0;
            int intBreakTm = 0;
            int intAbsentTm = 0;
            int intSplitAbsentTm = 0;
            intTotalSplitAbsentTm = 0;
            intTotalAbsentTm = 0;

            DataTable DtShiftDetails = objRequest.GetDynamicShiftDetails(iShiftID);

            if (DtShiftDetails.Rows.Count > 0)
            {
                for (int i = 0; i <= DtShiftDetails.Rows.Count - 1; i++)
                {

                    string sSplitFromTime = Convert.ToString(DtShiftDetails.Rows[i]["FromTime"]);
                    string sSplitToTime = Convert.ToString(DtShiftDetails.Rows[i]["ToTime"]);
                    string sDuration = Convert.ToString(DtShiftDetails.Rows[i]["Duration"]);
                    int iMinWorkingHrsMin = GetDurationInMinutes(sDuration);
                    //'Modified for Split Shift
                    if (nightFlag == true)
                    {
                        sSplitFromTime = Convert.ToDateTime(sSplitFromTime).AddDays(1).ToString();
                        sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                    }

                    int tmDurationDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime).AddMinutes(iMinWorkingHrsMin), Convert.ToDateTime(sSplitToTime));
                    if (tmDurationDiff < 0)
                    {
                        nightFlag = true;
                        sSplitFromTime = Convert.ToDateTime(sSplitFromTime).ToString();
                        sSplitToTime = Convert.ToDateTime(sSplitToTime).AddDays(1).ToString();
                    }

                    string sPreviousTime = "";
                    bool bShiftChangeTime = true;
                    bool bShiftChangeOvertime = true;
                    bool bShiftChangeTimeBrkOT = true;
                    bool bShiftChangeTimeEarlyWRk = true;
                    bool bShiftChangeTimeEarlyBRK = true;
                    string sOTBreakTime = "0";

                    for (int j = 3; j <= k + 2; j++)
                    {
                        //string dResult = Convert.ToString(DgvAttendance.Rows[iRowIndex].Cells[j].Value);
                        string dResult = j == 3 ? Convert.ToString(FromTime) : Convert.ToString(ToTime); 

                        string sTimeValue = Convert.ToDateTime(dResult).ToString("HH:mm");

                        if (j == 3)// Then ''first time
                        {
                            if (bLate)
                            {
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sFromTime).AddMinutes(iLate)) //'bFlexi = False
                                {
                                    int il = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sFromTime).AddMinutes(iLate), Convert.ToDateTime(sTimeValue));
                                    if (il > 0)
                                    {
                                        string sLate = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(il), DateTime.Today).ToString();
                                        //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                        ColDgvLateComing = Convert.ToDateTime(sLate).ToString("HH:mm:ss");
                                    }
                                    //lblLateComing.Tag = 1;
                                    //lblLateComing.Visible = true;
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 1;//'if 1 then LateComing=yes
                                }
                                else
                                {
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Value = "00:00:00";
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvLateComing"].Tag = 0;
                                    //lblLateComing.Tag = 0;
                                    //lblLateComing.Visible = false;
                                    ColDgvLateComing = "00:00:00";
                                }
                            }
                        }
                        else
                        {
                            int tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue));
                            //' If tmDiff < 0 Then tmDiff = DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1))
                            if (tmDiff < 0)//Then 'if date diff less than Zero then adding day to the StimeValue
                            {
                                tmDiff = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sTimeValue).AddDays(1));
                                sTimeValue = Convert.ToDateTime(sTimeValue).AddDays(1).ToString();
                            }
                            //'------------------------------------------------------------------------------------------------
                            //' workTime Time Calculation-
                            if (j % 2 == 0) //Then 'Mode=0 for workTime
                            { //'-------------------------------------Early Going validating 
                                if (Convert.ToDateTime(sTimeValue) < Convert.ToDateTime(sToTime).AddMinutes(-iEarly) && bEarly == true)//Then 'bFlexi = False
                                {
                                    int iE = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sTimeValue), Convert.ToDateTime(sToTime).AddMinutes(-iEarly));
                                    if (iE > 0)
                                    {
                                        string sEarly = DateAndTime.DateAdd(DateInterval.Minute, Convert.ToDouble(iE), DateTime.Today).ToString();
                                        //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                        ColDgvEarlyGoing = Convert.ToDateTime(sEarly).ToString("HH:mm:ss");
                                    }

                                    //lblEarlyGoing.Visible = true;
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 1; //'Earlygoing yes
                                } //'-------------------------------------Early Going 
                                //'---------------------------------------------Split Over Time-------------------------------------------

                                if (i == DtShiftDetails.Rows.Count - 1)
                                {
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime))
                                    {
                                        if (bShiftChangeOvertime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sToTime))
                                        {
                                            bShiftChangeOvertime = false;
                                            int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sToTime), Convert.ToDateTime(sTimeValue));
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + idiffCurtShiftOT);
                                            }
                                        }
                                        else
                                        {
                                            if (i == DtShiftDetails.Rows.Count - 1)
                                            {
                                                sOvertime = Convert.ToString(Convert.ToDouble(sOvertime) + Convert.ToDouble(tmDiff));//'WorkTime Calculation 
                                            }
                                        }
                                    }
                                }
                                //'--------------------------------------------------------------------------------------------------------
                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                { //' DgvAttendanceFetch.Rows(iRowIndex).Cells("EarlyGoing").Value = 0
                                    if (bShiftChangeTime && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                    {
                                        bShiftChangeTime = false;
                                        int idiffPrevShitWork = 0;
                                        if (Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime))// 'modified 28 dec
                                        {
                                            idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sSplitToTime));
                                        }
                                        else
                                        {
                                            idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));
                                        }
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));
                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            sOt = Convert.ToString(Convert.ToDouble(sOt) + idiffCurtShiftOT);
                                        }
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                    }
                                    else
                                    {
                                        if (i == DtShiftDetails.Rows.Count - 1)
                                        {
                                            sOt = Convert.ToString(Convert.ToDouble(sOt) + Convert.ToDouble(tmDiff));
                                        }
                                    }
                                }
                                else
                                {
                                    //'Modification -----------------------------12-11-2010
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                    {
                                        bflag = true;
                                    } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                    if (bShiftChangeTimeEarlyWRk && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                    {
                                        bShiftChangeTimeEarlyWRk = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + idiffPrevShitWork);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sWorkTime = Convert.ToString(Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff));
                                    }
                                    //'Modification -----------------------------End 12-11-2010
                                }
                                //'---------------------------------------------------------------------------------------------------------------------------------------------------------------------
                            }
                            else //'Break Time Calculation-where mode=1
                            {
                                if (i == DtShiftDetails.Rows.Count - 1)
                                {
                                    if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sToTime).AddMinutes(-iEarly))
                                    {
                                        if (bEarly)
                                        {

                                            //lblEarlyGoing.Visible = false;
                                            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                            //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                            ColDgvEarlyGoing = "00:00:00";
                                        }
                                    }
                                }

                                if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime))
                                {
                                    if (bShiftChangeTimeBrkOT == true && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitToTime))
                                    {
                                        bShiftChangeTimeBrkOT = false;
                                        int idiffPrevShitWork = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sPreviousTime), Convert.ToDateTime(sSplitToTime));//'break time= before ToTime- Early giong minutes
                                        int idiffCurtShiftOT = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitToTime), Convert.ToDateTime(sTimeValue));// 'Ot Break time = after To time
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(idiffPrevShitWork));//;'Break Time Calculation
                                        sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(idiffCurtShiftOT));
                                    }
                                    else
                                    {
                                        if (Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitToTime)) { sOTBreakTime = Convert.ToString(Convert.ToDouble(sOTBreakTime) + Convert.ToDouble(tmDiff)); } //'Break Time Calculation Ot
                                    }
                                }
                                else
                                {
                                    //'Modification -----------------------------12-11-2010
                                    bool bflag = false;
                                    if (Convert.ToDateTime(sTimeValue) <= Convert.ToDateTime(sSplitFromTime))
                                    {
                                        bflag = true;
                                    } //'sWorkTime = Convert.ToDouble(sWorkTime) + Convert.ToDouble(tmDiff)
                                    if (bShiftChangeTimeEarlyBRK && Convert.ToDateTime(sPreviousTime) < Convert.ToDateTime(sSplitFromTime) && Convert.ToDateTime(sTimeValue) > Convert.ToDateTime(sSplitFromTime))
                                    {
                                        bShiftChangeTimeEarlyBRK = false;
                                        int idiffPrevShitBreak = System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(Convert.ToDateTime(sSplitFromTime), Convert.ToDateTime(sTimeValue));
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + idiffPrevShitBreak);
                                        bflag = true;
                                    }
                                    if (bflag == false)
                                    {
                                        sBreakTime = Convert.ToString(Convert.ToDouble(sBreakTime) + Convert.ToDouble(tmDiff));
                                    }


                                    //lblEarlyGoing.Tag = 0;
                                    //lblEarlyGoing.Visible = false;
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Tag = 0;
                                    //DgvAttendance.Rows[iRowIndex].Cells["ColDgvEarlyGoing"].Value = "00:00:00";
                                    ColDgvEarlyGoing = "00:00:00";

                                }
                            }
                        }
                        sPreviousTime = sTimeValue;
                        //if (Glbln24HrFormat == false)
                        //{
                        //    sTimeValue = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                        //    DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("hh:mm tt");
                        //}
                        //else
                        //{
                        //    DgvAttendance.Rows[iRowIndex].Cells[j].Value = Convert.ToDateTime(dResult).ToString("HH:mm");
                        //}

                    } //'For j = 3 To k + 2 'Time selecting filter wit date and id 

                    //'------------------------------------Shoratge Calculation starts----------------------------------
                    intSplitAbsentTm = 0;

                    intAbsentTm = 0;
                    if (intWorkTm == Convert.ToInt32(sWorkTime) && intBreakTm == Convert.ToInt32(sBreakTime))
                    { intAbsentTm = iMinWorkingHrsMin; }
                    else
                    {
                        intSplitAbsentTm = iMinWorkingHrsMin - (Convert.ToInt32(sWorkTime) - intWorkTm);
                        if (intSplitAbsentTm > 0)
                        { intTotalSplitAbsentTm += intSplitAbsentTm; }
                    }
                    if (intAbsentTm > 0)
                    { intTotalAbsentTm += intAbsentTm; }


                    intWorkTm = Convert.ToInt32(sWorkTime);
                    intBreakTm = Convert.ToInt32(sBreakTime);
                    //'------------------------------------Shoratge Calculation Ends------------------------------------

                } 

                //if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave) && DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveInfo"].Value.ToInt32() == 1)
                //{
                //if (Convert.ToInt32(DgvAttendance.Rows[iRowIndex].Cells["ColDgvStatus"].Value) == Convert.ToInt32(AttendanceStatus.Leave) || DgvAttendance.Rows[iRowIndex].Cells["ColDgvLeaveFlag"].Value.ToInt32() == 1)
                //{
                //    intTotalSplitAbsentTm = 0;
                //    intTotalAbsentTm = 0;
                //}
                if (isAfterMinWrkHrsAsOT == false)
                {
                    sOt = sOvertime;
                }
            } 

            return true;
        }

        catch (Exception Ex)
        {
           // MobjClsLogs.WriteLog("Error on WorktimeCalculationForSplitShift:Attendance auto." + Ex.Message.ToString() + "", 3);
            return false;
        }
    }

}
