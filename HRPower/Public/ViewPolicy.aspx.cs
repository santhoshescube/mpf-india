﻿using System;
using System.Collections;
using System.Text;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;

/// <summary>
/// Created By:    Sanju
/// Created Date:  08-Oct-2013
/// Description:   View Policies
/// </summary>
public partial class Public_ViewPolicy : System.Web.UI.Page
{
    #region DECLARATIONS
    clsViewPolicies objclsViewPolicies;
    #endregion
    #region EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("ViewPolicy").ToString();
        if (!IsPostBack)
        {
            rbEmployeeMode.Checked = true;
            BindTree(1);
            divEmployeeMode.Visible = true;
            divPolicyMode.Visible = false;
        }
        clsUserMaster objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.ViewPolicies, objUser.GetCompanyId());
    }
    protected void tvEmployeePolicies_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (tvEmployeePolicies.SelectedNode != null)
        {
            LoadEmpPolicies();
            if (tvEmployeePolicies.SelectedNode.ChildNodes.Count > 0)
            {
                tvEmployeePolicies.SelectedNode.ExpandAll();
            }

        }
        //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), new Guid().ToString(), "var elem = document.getElementById('" + tvEmployeePolicies.ClientID + "_SelectedNode');var node = document.getElementById(elem.value);node.scrollIntoView(true);elem.scrollLeft=0;", true);
    }
    protected void tvAllPolicies_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (tvAllPolicies.SelectedNode != null)
        {
            LoadAllPolicies();
            if (tvAllPolicies.SelectedNode.ChildNodes.Count > 0)
            {
                tvAllPolicies.SelectedNode.ExpandAll();
            }
        }
    }
    protected void imgBtnSearch_Click(object sender, ImageClickEventArgs e)
    {
        TreeNode[] searchedNodes = tvEmployeePolicies.Nodes.Cast<TreeNode>().Where(r => r.Text == txtSearch.Text.Trim()).ToArray();
        if (searchedNodes.Count() > 0)
        {
            //tvEmployeePolicies.FindNode(searchedNodes[0].ValuePath).Select();
            //tvEmployeePolicies.FindNode(searchedNodes[0].ValuePath).SelectAction = TreeNodeSelectAction.Select;
            tvEmployeePolicies.CollapseAll();
            foreach (TreeNode CurrentNode in searchedNodes)
            {
                CurrentNode.Selected = true;
                tvEmployeePolicies_SelectedNodeChanged(null, null);
            }

        }
        else
        {
            txtSearch.Text = "";
        }
    }
    protected void rbEmployeeMode_CheckedChanged(object sender, EventArgs e)
    {
        setMode();
    }
    protected void rbPolicyMode_CheckedChanged(object sender, EventArgs e)
    {
        setMode();
    }
    #endregion
    #region METHODS
    private void setMode()
    {
        if (rbEmployeeMode.Checked)
        {
            BindTree(1);
            divEmployeeMode.Visible = true;
            divPolicyMode.Visible = false;
        }
        else
        {
            BindTree(2);
            divEmployeeMode.Visible = false;
            divPolicyMode.Visible = true;
        }
    }
    private void BindTree(int intMode)
    {
        DataTable datData;
        if (intMode == 1)
        {
            objclsViewPolicies = new clsViewPolicies();
            clsUserMaster objUser = new clsUserMaster();

            tvEmployeePolicies.Nodes.Clear();
            objclsViewPolicies.CompanyID = objUser.GetCompanyId();
            datData = objclsViewPolicies.GetAllEmployees();
            //ddlEmployee.DataValueField = "ID";
            //ddlEmployee.DataTextField = "Description";
            //ddlEmployee.DataSource = datData;
            //ddlEmployee.DataBind();
            PopulateNodes(datData, tvEmployeePolicies.Nodes);
            //tvEmployeePolicies.ExpandAll();
            if (tvEmployeePolicies.Nodes.Count > 0)
            {
                if (tvEmployeePolicies.Nodes[0].ChildNodes.Count > 0)
                {
                    tvEmployeePolicies.Nodes[0].ChildNodes[0].Selected = true;
                    LoadEmpPolicies();
                }
            }
            else
            {

                //ifDocument.Visible = false;
            }
        }
        else
        {
            datData = new DataTable();
            datData.Columns.Add("ID");
            datData.Columns.Add("Description");
            datData.Columns.Add("Type");
            DataRow dr1 = datData.NewRow();
            dr1["ID"] = "WorkPolicy";
            // dr1["Description"] = "Work Policy";
            dr1["Description"] = GetGlobalResourceObject("ControlsCommon", "WorkPolicy");
            dr1["Type"] = "Master";
            datData.Rows.Add(dr1);

            DataRow dr2 = datData.NewRow();
            dr2["ID"] = "ShiftPolicy";
            //dr2["Description"] = "Shift Policy";
            dr2["Description"] = GetGlobalResourceObject("ControlsCommon", "ShiftPolicy");
            dr2["Type"] = "Master";
            datData.Rows.Add(dr2);

            DataRow dr3 = datData.NewRow();
            dr3["ID"] = "LeavePolicy";
            //dr3["Description"] = "Leave Policy";
            dr3["Description"] = GetGlobalResourceObject("ControlsCommon", "LeavePolicy");
            dr3["Type"] = "Master";
            datData.Rows.Add(dr3);

            DataRow dr4 = datData.NewRow();
            dr4["ID"] = "VacationPolicy";
            //dr4["Description"] = "Vacation Policy";
            dr4["Description"] = GetGlobalResourceObject("ControlsCommon", "VacationPolicy");
            dr4["Type"] = "Master";
            datData.Rows.Add(dr4);

            DataRow dr5 = datData.NewRow();
            dr5["ID"] = "AbsentPolicy";
            //dr5["Description"] = "Absent Policy";
            dr5["Description"] = GetGlobalResourceObject("ControlsCommon", "AbsentPolicy");
            dr5["Type"] = "Master";
            datData.Rows.Add(dr5);

            DataRow dr6 = datData.NewRow();
            dr6["ID"] = "HolidayPolicy";
            //dr6["Description"] = "Holiday Policy";
            dr6["Description"] = GetGlobalResourceObject("ControlsCommon", "HolidayPolicy");
            dr6["Type"] = "Master";
            datData.Rows.Add(dr6);

            DataRow dr7 = datData.NewRow();
            dr7["ID"] = "OTPolicy";
            //dr7["Description"] = "OverTime Policy";
            dr7["Description"] = GetGlobalResourceObject("ControlsCommon", "OverTimePolicy");
            dr7["Type"] = "Master";
            datData.Rows.Add(dr7);

            DataRow dr8 = datData.NewRow();
            dr8["ID"] = "EncashPolicy";
            //dr8["Description"] = "Encash Policy";
            dr8["Description"] = GetGlobalResourceObject("ControlsCommon", "EncashPolicy");
            dr8["Type"] = "Master";
            datData.Rows.Add(dr8);

            DataRow dr9 = datData.NewRow();
            dr9["ID"] = "SettlementPolicy";
            //dr9["Description"] = "Settlement Policy";
            dr9["Description"] = GetGlobalResourceObject("ControlsCommon", "SettlementPolicy");
            dr9["Type"] = "Master";
            datData.Rows.Add(dr9);

            DataRow dr10 = datData.NewRow();
            dr10["ID"] = "UnearnedPolicy";
            //dr10["Description"] = "Unearned Policy";
            dr10["Description"] = GetGlobalResourceObject("ControlsCommon", "UnearnedPolicy");
            dr10["Type"] = "Master";
            datData.Rows.Add(dr10);

            tvAllPolicies.Nodes.Clear();
            PopulateAllNodes(datData, tvAllPolicies.Nodes);

            if (tvAllPolicies.Nodes.Count > 0)
            {
                if (tvAllPolicies.Nodes[0].ChildNodes.Count > 0)
                {
                    tvAllPolicies.Nodes[0].ChildNodes[0].Selected = true;
                    LoadEmpPolicies();
                }
            }
            else
            {

                //ifDocument.Visible = false;
            }

        }


    }
    private string GetPolicyHtml(DataSet datSource, string MstrCaption, int intPolicyMode, string strEmployee)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:20px;float:left;background-color:rgb(188, 205, 211);'";
        string strHeaderRowInnerLeftStyle = "style='width:30%;height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInnerRightStyle = "style='width:70%;height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner50Style = "style='width:50%;height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner25Style = "style='width:25%;height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner20Style = "style='width:20%;height:20px;float:left; font-size:14px; color:#000000''";


        string strRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:20px;float:left;background-color:rgb(232, 238, 240)'";
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:20px;float:left;background-color:#FFFFFF;'";

        string strRowInnerLeftStyle = "style='width:30%;height:20px;float:left;font-size:12px; color:#000000''";
        string strAltRowInnerLeftStyle = "style='width:30%;height:20px;float:left;font-size:12px; color:#000000'";

        string strRowInnerRightStyle = "style='width:70%;height:20px;float:left;font-size:12px; color:#000000''";
        string strAltRowInnerRightStyle = "style='width:70%;height:20px;float:left;font-size:12px; color:#000000'";

        string strRowInner50Style = "style='width:50%;height:20px;float:left;font-size:12px; color:#000000''";
        string strAltRowInner50Style = "style='width:50%;height:20px;float:left;font-size:12px; color:#000000'";
        string strRowInner25Style = "style='width:25%;height:20px;float:left;font-size:12px; color:#000000''";
        string strAltRowInner25Style = "style='width:25%;height:20px;float:left;font-size:12px; color:#000000'";
        string strRowInner20Style = "style='width:20%;height:20px;float:left;font-size:12px; color:#000000''";
        string strAltRowInner20Style = "style='width:20%;height:20px;float:left;font-size:12px; color:#000000'";
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");
        if (strEmployee != "")
            MsbHtml.Append("<div   " + strSubHeaderStyle3 + ">"+GetGlobalResourceObject("ControlsCommon","Employee").ToString()+" : " + strEmployee + "</div>");
        MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("GeneralInformation").ToString()+"</div>");

        if (datSource.Tables[0].Rows.Count <= 0)
        {
            return "";
        }
        switch (intPolicyMode)
        {
            case 1://Work Policy


                if (datSource.Tables[0].Rows.Count > 0)
                {
                    DataRow DrWebInfo = datSource.Tables[0].Rows[0];
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("PolicyName").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrWebInfo["PolicyName"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("Shift").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrWebInfo["OffDayshift"].ToStringCustom() + "</div></div>");
                    //MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + "></div><div " + strRowInnerRightStyle + "></div></div>");
                }
                if (datSource.Tables[1].Rows.Count > 0)
                {
                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("PolicyDetails").ToString ()+"</div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInnerLeftStyle + ">"+GetLocalResourceObject("WorkingDays").ToString()+"</div><div " + strHeaderRowInnerRightStyle + ">"+GetLocalResourceObject("Shift").ToString()+"</div></div>");
                    int intMode = 0;
                    foreach (DataRow drCurrentRow in datSource.Tables[1].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("ControlsCommon", drCurrentRow["DayName"].ToStringCustom()).ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + drCurrentRow["ShiftName"].ToStringCustom() + "</div></div>");

                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("ControlsCommon",drCurrentRow["DayName"].ToStringCustom()).ToString() + "</div><div " + strRowInnerRightStyle + ">" + drCurrentRow["ShiftName"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;

                    }
                }
                if (datSource.Tables[2].Rows.Count > 0)
                {


                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("PolicyConsequenceDetails").ToString()+"  </div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("Consequence").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("Lop").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("Casual").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("Amount").ToString() + "</div></div>");
                    int intMode = 0;
                    foreach (DataRow drCurrentRow in datSource.Tables[2].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner25Style + ">" + drCurrentRow["Conseq"].ToStringCustom() + "</div><div " + strAltRowInner25Style + ">" + drCurrentRow["LOP"].ToStringCustom() + "</div><div " + strAltRowInner25Style + ">" + drCurrentRow["Casual"].ToStringCustom() + "</div><div " + strAltRowInner25Style + ">" + drCurrentRow["Amount"].ToStringCustom() + "</div></div>");

                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInner25Style + ">" + drCurrentRow["Conseq"].ToStringCustom() + "</div><div " + strRowInner25Style + ">" + drCurrentRow["LOP"].ToStringCustom() + "</div><div " + strRowInner25Style + ">" + drCurrentRow["Casual"].ToStringCustom() + "</div><div " + strRowInner25Style + ">" + drCurrentRow["Amount"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;

                    }
                }
                break;

            case 2://Shift Policy

                DataRow DrShiftInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("ShiftName").ToString()+" </div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["Shiftname"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("FromTime").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrShiftInfo["Fromtime"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("ToTime").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["Totime"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("ShiftType").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrShiftInfo["Shifttype"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("Duration").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["Duration"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("MinWorkingHours").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrShiftInfo["MinWorkingHours"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("AllowedBreakTime").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["AllowedBreakTime"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("LateAfter").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrShiftInfo["LateAfter"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("EarlyBefore").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["EarlyBefore"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("Buffer").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrShiftInfo["Buffer"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ConsequenceApplicable").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrShiftInfo["IsConsequenceRequired"].ToStringCustom() + "</div></div>");

                break;

            case 3://Leave Policy
                if (datSource.Tables[0].Rows.Count > 0)
                {
                    DataRow DrLeaveInfo = datSource.Tables[0].Rows[0];
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("LeavePolicyName").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrLeaveInfo["LeavePolicyName"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("ApplicableFrom").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrLeaveInfo["ApplicableFrom"].ToStringCustom() + "</div></div>");
                }
                if (datSource.Tables[1].Rows.Count > 0)
                {
                    int intMode = 0;
                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("PolicyDetails").ToString()+"</div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("LeaveType").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("NoOfLeaves").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("MonthLeaves").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("CarryforwardedLeaves").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("EncashableDays").ToString()+"</div></div>");
                    foreach (DataRow drCurrentRow in datSource.Tables[1].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner20Style + ">" + drCurrentRow["lvtype"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["NoOfLeave"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["MonthLeave"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["CarryForwardLeave"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["EncashDays"].ToStringCustom() + "</div></div>");

                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInner20Style + ">" + drCurrentRow["lvtype"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["NoOfLeave"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["MonthLeave"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["CarryForwardLeave"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["EncashDays"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;
                    }
                }
                if (datSource.Tables[2].Rows.Count > 0)
                {
                    int intMode = 0;
                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("PolicyConsequenceDetails").ToString() + "</div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner20Style + ">" + GetLocalResourceObject("LeaveType").ToString() + "</div><div " + strHeaderRowInner20Style + ">" + GetLocalResourceObject("CalculationType").ToString() + "</div><div " + strHeaderRowInner20Style + ">" + GetLocalResourceObject("ApplicableDays").ToString() + "</div></div>");
                    foreach (DataRow drCurrentRow in datSource.Tables[2].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner20Style + ">" + drCurrentRow["leavetype"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["CalculationType"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["ApplicableDays"].ToStringCustom() + "</div></div>");

                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInner20Style + ">" + drCurrentRow["leavetype"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["CalculationType"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["ApplicableDays"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;
                    }
                }
                break;

            case 4: //Vacation Policy

                if (datSource.Tables[0].Rows.Count > 0)
                {
                    DataRow DrVacationInfo = datSource.Tables[0].Rows[0];
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("VacationPolicyName").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrVacationInfo["PolicyName"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("CalculationBasedOn").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrVacationInfo["BasedOn"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("ExcludeFromGross").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrVacationInfo["ExcludeFromGross"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("OnTimeRejoinBenefit").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + DrVacationInfo["OnTimeRejoinBenefit"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Calculationbasedonworkeddays").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrVacationInfo["CalBasedWKDays"].ToStringCustom() + "</div></div>");

                }
                if (datSource.Tables[1].Rows.Count > 0)
                {
                    int intMode = 0;
                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("PolicyDetails").ToString()+"</div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("Parameter").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("NoOfMonths").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("Leave/PayableDays").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("Tickets").ToString()+"</div><div " + strHeaderRowInner20Style + ">"+GetLocalResourceObject("TicketAmount").ToString()+"</div></div>");
                    foreach (DataRow drCurrentRow in datSource.Tables[1].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner20Style + ">" + drCurrentRow["ParameterName"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["NoOfMonths"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["PayableDays"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["Tickets"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["TicketAmount"].ToStringCustom() + "</div></div>");

                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInner20Style + ">" + drCurrentRow["ParameterName"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["NoOfMonths"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["PayableDays"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["Tickets"].ToStringCustom() + "</div><div " + strRowInner20Style + ">" + drCurrentRow["TicketAmount"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;
                    }
                }
                break;

            case 5://OverTime Policy

                DataRow DrOverTimeInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("OTPolicyName").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrOverTimeInfo["OTPolicy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">"+GetLocalResourceObject("CalculationbasedOn").ToString()+"</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "-" : DrOverTimeInfo["Calculation"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "-" : DrOverTimeInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "-" : DrOverTimeInfo["CalculationPercentage"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("CalculationDay").ToString()+" </div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "-" : DrOverTimeInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("TotalWorkingHour/Year").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "-" : DrOverTimeInfo["TotalWorkingHours"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("RateOnly")+"</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrOverTimeInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrOverTimeInfo["OTRate"].ToStringCustom() + "</div></div>");

                break;

            case 6://Encash Policy

                DataRow DrEncashInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("EncashPolicyName").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrEncashInfo["EncashPolicy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationbasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrEncashInfo["IsRateOnly"]) ? "-" : DrEncashInfo["Calculation"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrEncashInfo["IsRateOnly"]) ? "-" : DrEncashInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationPercentage").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrEncashInfo["IsRateOnly"]) ? "-" : DrEncashInfo["CalculationPercentage"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationDay").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrEncashInfo["IsRateOnly"]) ? "-" : DrEncashInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Hours Per Day</div><div " + strAltRowInnerRightStyle + ">" + DrEncashInfo["HoursPerDay"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RateOnly") + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrEncashInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrEncashInfo["EncashRate"].ToStringCustom() + "</div></div>");

                break;

            case 7://Holiday Policy

                DataRow DrHolidayInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("HolidayPolicyName").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrHolidayInfo["HolidayPolicy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationbasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "-" : DrHolidayInfo["Calculation"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "-" : DrHolidayInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationPercentage").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "-" : DrHolidayInfo["CalculationPercentage"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationDay").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "-" : DrHolidayInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("HoursPerDay").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "-" : DrHolidayInfo["HoursPerDay"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("RateOnly") + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrHolidayInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrHolidayInfo["HolidayRate"].ToStringCustom() + "</div></div>");

                break;

            case 8://Unearned Policy

                DataRow DrUnearnedInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("UnearnedPolicyName").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrUnearnedInfo["UnearnedPolicy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationbasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrUnearnedInfo["IsRateOnly"]) ? "-" : DrUnearnedInfo["Calculation"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrUnearnedInfo["IsRateOnly"]) ? "-" : DrUnearnedInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationDay").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrUnearnedInfo["IsRateOnly"]) ? "-" : DrUnearnedInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
               // MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("HoursPerDay").ToString()+"</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrUnearnedInfo["IsRateOnly"]) ? "-" : DrUnearnedInfo["HoursPerDay"].ToStringCustom()) + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RateOnly") + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrUnearnedInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
               // MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrUnearnedInfo["UnearnedRate"].ToStringCustom() + "</div></div>");

                break;

            case 9://Settlement Policy

                DataRow DrSettlementInfo = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("SettlementPolicyName").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrSettlementInfo["DescriptionPolicy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationbasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrSettlementInfo["Calculation"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("ExcludeFromGross").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrSettlementInfo["ExcludeFromGross"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("IncludeEligibleLeave").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrSettlementInfo["IsIncludeLeave"].ToStringCustom() + "</div></div>");   
                 //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Calculation Percentage</div><div " + strAltRowInnerRightStyle + ">" + DrSettlementInfo["CalculationPercentage"].ToStringCustom() + "</div></div>");
                //MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">Calculation Day</div><div " + strRowInnerRightStyle + ">" + DrSettlementInfo["IsCompanyBasedOn"].ToStringCustom() + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">Total Working Hour/Year</div><div " + strAltRowInnerRightStyle + ">" + DrSettlementInfo["TotalWorkingHours"].ToStringCustom() + "</div></div>");
                //MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">Rate Only</div><div " + strRowInnerRightStyle + ">" + DrSettlementInfo["IsRateOnly"].ToStringCustom() + "</div></div>");
                //MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">OverTime Rate</div><div " + strAltRowInnerRightStyle + ">" + DrSettlementInfo["UnearnedRate"].ToStringCustom() + "</div></div>");

                if (datSource.Tables[1].Rows.Count > 0)
                {
                    int intMode = 0;
                    MsbHtml.Append("<div   " + strSubHeaderStyle + ">"+GetLocalResourceObject("Criteria").ToString()+"</div>");
                    MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("Parameter").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("NoOfMonths").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("NoOfDays").ToString() + "</div><div " + strHeaderRowInner25Style + ">" + GetLocalResourceObject("TerminationDays").ToString() + "</div></div>");
                    foreach (DataRow drCurrentRow in datSource.Tables[1].Rows)
                    {
                        if (intMode % 2 == 0)
                        {
                            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner25Style + ">" + drCurrentRow["ParameterName"].ToStringCustom() + "</div><div " + strAltRowInner25Style + ">" + drCurrentRow["NoOfMonths"].ToStringCustom() + "</div><div " + strAltRowInner25Style + ">" + drCurrentRow["NoOfDays"].ToStringCustom() + "</div></div>");
                        }
                        else
                        {
                            MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInner25Style + ">" + drCurrentRow["ParameterName"].ToStringCustom() + "</div><div " + strRowInner25Style + ">" + drCurrentRow["NoOfMonths"].ToStringCustom() + "</div><div " + strRowInner25Style + ">" + drCurrentRow["NoOfDays"].ToStringCustom() + "</div></div>");
                        }
                        intMode++;
                    }
                }
                break;

            case 10://Absent/Shortage Policy

                if (datSource.Tables.Count > 0)
                {

                    DataRow DrAbsentInfo = datSource.Tables[0].Rows[0];
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">"+GetLocalResourceObject("AbsentPolicyName").ToString()+"</div><div " + strRowInnerRightStyle + ">" + DrAbsentInfo["AbsentPolicy"].ToStringCustom() + "</div></div>");
                    MsbHtml.Append("<div   " + strHeaderRowStyle + "><div " + strHeaderRowInner50Style + ">" + GetLocalResourceObject("AbsentPolicy").ToString() + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationBasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrAbsentInfo["IsRateOnly"]) ? "-" : DrAbsentInfo["Calculation"].ToStringCustom()) + "</div></div>");
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrAbsentInfo["IsRateOnly"]) ? "-" : DrAbsentInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationDay").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrAbsentInfo["IsRateOnly"]) ? "-" : DrAbsentInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("HoursPerDay").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrAbsentInfo["IsRateOnly"]) ? "-" : DrAbsentInfo["HoursPerDay"].ToStringCustom()) + "</div></div>");
                    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RateOnly").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrAbsentInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
                    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrAbsentInfo["AbsentRate"].ToStringCustom() + "</div></div>");
                }
                if (datSource.Tables.Count > 1)
                {
                    if (datSource.Tables[1].Rows.Count > 0)
                    {
                        DataRow DrShortageInfo = datSource.Tables[1].Rows[0];
                        MsbHtml.Append("<div   " + strHeaderRowStyle + "><div " + strHeaderRowInner50Style + ">" + GetLocalResourceObject("ShortagePolicy").ToString() + "</div></div>");
                        MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationbasedOn").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrShortageInfo["IsRateOnly"]) ? "-" : DrShortageInfo["Calculation"].ToStringCustom()) + "</div></div>");
                        MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("ExcludeFromGross").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrShortageInfo["IsRateOnly"]) ? "-" : DrShortageInfo["ExcludeFromGross"].ToStringCustom()) + "</div></div>");
                        MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CalculationDay").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrShortageInfo["IsRateOnly"]) ? "-" : DrShortageInfo["IsCompanyBasedOn"].ToStringCustom()) + "</div></div>");
                        MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("HoursPerDay").ToString() + "</div><div " + strRowInnerRightStyle + ">" + (Convert.ToBoolean(DrShortageInfo["IsRateOnly"]) ? "-" : DrShortageInfo["HoursPerDay"].ToStringCustom()) + "</div></div>");
                        MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RateOnly").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + (Convert.ToBoolean(DrShortageInfo["IsRateOnly"]) ? "Yes" : "No") + "</div></div>");
                        MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("OverTimeRate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrShortageInfo["AbsentRate"].ToStringCustom() + "</div></div>");

                    }
                }
                break;
        }
        return MsbHtml.ToString();
    }
    private void Populate(int iParentID, TreeNode parentNode)
    {
        if (parentNode.Target == "Employee")
        {
            objclsViewPolicies = new clsViewPolicies();

            objclsViewPolicies.EmployeeID = iParentID;

            DataTable datData = objclsViewPolicies.GetEmployeePolicies();
            if (datData.Rows.Count > 0)
            {
                PopulateNodes(datData, parentNode.ChildNodes);
            }
        }
    }
    private void PopulateAllPolicies(string strType, TreeNode parentNode)
    {
        if (parentNode.Target == "Master")
        {
            objclsViewPolicies = new clsViewPolicies();

            objclsViewPolicies.Type = strType;

            DataTable datData = objclsViewPolicies.GetAllPolicies();
            if (datData.Rows.Count > 0)
            {
                PopulateAllNodes(datData, parentNode.ChildNodes);
            }
        }
    }
    private void LoadEmpPolicies()
    {

        if (tvEmployeePolicies.SelectedNode.Target.ToStringCustom() == "Employee")
        {
            if (tvEmployeePolicies.SelectedNode.ChildNodes.Count <= 0)
            {
                Populate(tvEmployeePolicies.SelectedNode.Value.ToStringCustom().Split('@')[0].ToInt32(), tvEmployeePolicies.SelectedNode);
            }
        }
        else
        {
            string strHTMLSource = "";
            objclsViewPolicies = new clsViewPolicies();
            objclsViewPolicies.Type = tvEmployeePolicies.SelectedNode.Target.ToStringCustom();
            objclsViewPolicies.EmployeeID = tvEmployeePolicies.SelectedNode.Parent.Value.ToStringCustom().Split('@')[0].ToInt32();
            objclsViewPolicies.PolicyID = tvEmployeePolicies.SelectedNode.Value.ToStringCustom().Split('@')[0].ToInt32();
            string strEmployee = tvEmployeePolicies.SelectedNode.Parent.Text.ToStringCustom();
            DataSet dtsData = objclsViewPolicies.GetPolicyDetails();
            if (dtsData.Tables.Count > 0)
            {
                if (objclsViewPolicies.Type == "WorkPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData,GetGlobalResourceObject("ControlsCommon","WorkPolicy").ToString(), 1, strEmployee);
                }
                else if (objclsViewPolicies.Type == "ShiftPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "ShiftPolicy").ToString(), 2, strEmployee);
                }
                else if (objclsViewPolicies.Type == "LeavePolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "LeavePolicy").ToString(), 3, strEmployee);
                }
                else if (objclsViewPolicies.Type == "VacationPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "VacationPolicy").ToString(), 4, strEmployee);
                }
                else if (objclsViewPolicies.Type == "OTPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "OverTimePolicy").ToString(), 5, strEmployee);
                }
                else if (objclsViewPolicies.Type == "EncashPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "EncashPolicy").ToString(), 6, strEmployee);
                }
                else if (objclsViewPolicies.Type == "HolidayPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "HolidayPolicy").ToString(), 7, strEmployee);
                }
                else if (objclsViewPolicies.Type == "UnearnedPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "UnearnedPolicy").ToString(), 8, strEmployee);
                }
                else if (objclsViewPolicies.Type == "SettlementPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "SettlementPolicy").ToString(), 9, strEmployee);
                }
                else if (objclsViewPolicies.Type == "AbsentPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "AbsentPolicy").ToString(), 10, strEmployee);
                }
                divContent.InnerHtml = strHTMLSource;
                divContent.Style["background-color"] = "#FFFFFF";
                rbEmployeeMode.Focus();
            }
        }

    }
    private void LoadAllPolicies()
    {

        if (tvAllPolicies.SelectedNode.Target.ToStringCustom() == "Master")
        {
            if (tvAllPolicies.SelectedNode.ChildNodes.Count <= 0)
            {
                PopulateAllPolicies(tvAllPolicies.SelectedNode.Value.ToStringCustom(), tvAllPolicies.SelectedNode);
            }
        }
        else
        {
            string strHTMLSource = "";
            objclsViewPolicies = new clsViewPolicies();
            objclsViewPolicies.Type = tvAllPolicies.SelectedNode.Target.ToStringCustom();
            objclsViewPolicies.PolicyID = tvAllPolicies.SelectedNode.Value.ToStringCustom().Split('@')[0].ToInt32();
            DataSet dtsData = objclsViewPolicies.GetPolicyDetails();
            if (dtsData.Tables.Count > 0)
            {
                if (objclsViewPolicies.Type == "WorkPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "WorkPolicy").ToString(), 1, "");
                }
                else if (objclsViewPolicies.Type == "ShiftPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","ShiftPolicy").ToString(), 2, "");
                }
                else if (objclsViewPolicies.Type == "LeavePolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","LeavePolicy").ToString(), 3, "");
                }
                else if (objclsViewPolicies.Type == "VacationPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","VacationPolicy").ToString(), 4, "");
                }
                else if (objclsViewPolicies.Type == "OTPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","OverTimePolicy").ToString(), 5, "");
                }
                else if (objclsViewPolicies.Type == "EncashPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","EncashPolicy").ToString(), 6, "");
                }
                else if (objclsViewPolicies.Type == "HolidayPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","HolidayPolicy").ToString(), 7, "");
                }
                else if (objclsViewPolicies.Type == "UnearnedPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","UnearnedPolicy").ToString(), 8, "");
                }
                else if (objclsViewPolicies.Type == "SettlementPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","SettlementPolicy").ToString(), 9, "");
                }
                else if (objclsViewPolicies.Type == "AbsentPolicy")
                {
                    strHTMLSource = GetPolicyHtml(dtsData, GetGlobalResourceObject("ControlsCommon","AbsentPolicy").ToString(), 10, "");
                }
                divContent.InnerHtml = strHTMLSource;
                divContent.Style["background-color"] = "#FFFFFF";
                rbPolicyMode.Focus();
            }
        }

    }
    private void PopulateNodes(DataTable datData, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in datData.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dw["Description"].ToString();
            tn.Value = dw["ID"].ToString() + "@" + dw["Index"].ToString();
            tn.Target = dw["Type"].ToString();
            nodes.Add(tn);
            //Populate(tn.Value.ToInt32(), tn);
        }
    }
    private void PopulateAllNodes(DataTable datData, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in datData.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dw["Description"].ToString();
            tn.Value = dw["ID"].ToString();
            tn.Target = dw["Type"].ToString();
            nodes.Add(tn);
        }
    }
    #endregion
}