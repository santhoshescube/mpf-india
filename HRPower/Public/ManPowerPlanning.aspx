<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="ManPowerPlanning.aspx.cs" Inherits="Public_ManPowerPlanning" Title="Untitled Page" %>

<%@ Register Src="../Controls/HiringSchedule.ascx" TagName="HiringSchedule" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>

        <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

        <link href="../css/employee.css" rel="stylesheet" type="text/css" />
        <%--  Salutation--%>
        <%--Male--%>
        <%-- Female--%>
        <%--    commented--%>
        <ul>
            <li><a href='Projects.aspx'><span>
                <%--   Employee Number--%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Project %>'>
                </asp:Literal>
            </span></a></li>
            <li class="selected"><a href='ManPowerPlanning.aspx'><span>
                <%--   Employee Number--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,Plan %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="HiringSchedules.aspx">
                <%-- View Policies--%>
                <%--                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,Calender %>'>
                </asp:Literal>--%>
                <asp:Literal ID="Literal1" runat="server" Text="Hiring Schedules">
                </asp:Literal>
            </a></li>
            <li><a href="ViewPolicy.aspx">
                <%--Work Location--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,Expense %>'>
                </asp:Literal>
            </a></li>
            <li><a href="ViewPolicy.aspx">
                <%--Work Location--%>
                <asp:Literal ID="Literal19" runat="server" Text="Budject Vs Actual">
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%">
        <div>
            <div id="div1" class="TabbedPanels" style="width: 100%;">
                <ul class="TabbedPanelsTabGroup">
                    <li class="TabbedPanelsTabSelected" id="pnlTab1" onclick="SetPlanningTab(this.id)">
                        <%-- Employee Details--%>
                        <asp:Literal ID="Literal11" runat="server" Text="Plan Description"></asp:Literal>
                    </li>
                  
                    <li class="TabbedPanelsTab" id="pnlTab2" onclick="SetPlanningTab(this.id)">
                        <%--                    Work Profile--%>
                        <asp:Literal ID="Literal53" runat="server" Text="Plan Details"></asp:Literal>
                    </li>
                    
                    
                      <li class="TabbedPanelsTab" id="pnlTab3" onclick="SetPlanningTab(this.id)">
                        <%-- Employee Details--%>
                        <asp:Literal ID="Literal15" runat="server" Text="Recruitment Cost"></asp:Literal>
                    </li>
                </ul>
            </div>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
                    <div style="width: 100%">
                        <div class="trLeft" style="float: left; width: 18%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal4" runat="server" Text="Plan Code"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:TextBox ID="txtPlanCode" runat="server" CssClass="dropdownlist_mandatory" Text="Pl-01"
                                    Width="100px">
                                </asp:TextBox>
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                            </div>
                        </div>
                        <div class="trLeft" style="float: left; width: 18%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal16" runat="server" Text="Plan Title"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:TextBox ID="txtPlanTitle" runat="server" CssClass="dropdownlist_mandatory" Width="100px">
                                </asp:TextBox>
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                            </div>
                        </div>
                        <div style="min-height: 30px; height: auto">
                            <div class="trLeft" style="float: left; width: 18%; min-height: 40px; height: auto">
                                <%--   Employee Number--%>
                                <asp:Literal ID="Literal6" runat="server" Text="Description"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: auto; min-height: 40px">
                                <div style="float: left; width: 25%; height: auto; min-height: 40px">
                                    <asp:TextBox ID="txtPlanDescription" TextMode="MultiLine" Height="50px" runat="server"
                                        CssClass="dropdownlist_mandatory" Width="178px" Style="background-color: InfoBackground;">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div style="clear: both">
                            </div>
                        </div>
                        <div style="clear: both">
                        </div>
                        <div>
                            <div class="trLeft" style="float: left; width: 18%; height: 29px">
                                <%--   Employee Number--%>
                                <asp:Literal ID="Literal2" runat="server" Text="Company"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <div style="float: left; width: 25%; height: 29px">
                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="180px" Style="background-color: InfoBackground;">
                                    </asp:DropDownList>
                                </div>
                                <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                                </div>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 17%; height: 29px">
                                <%--  Salutation--%>
                                <asp:Literal ID="Literal3" runat="server" Text="Type"></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updProjectOrDepartment" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 50%; height: 29px">
                                            <asp:RadioButtonList AutoPostBack="true" ID="rdlDepartmentOrProject" runat="server"
                                                RepeatDirection="Horizontal" OnSelectedIndexChanged="rdlDepartmentOrProject_SelectedIndexChanged">
                                                <asp:ListItem Selected="True" Text="Department"></asp:ListItem>
                                                <asp:ListItem Text="Project"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div>
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 17%; height: 29px">
                                <%--  Salutation--%>
                                <asp:Literal ID="Literal20" runat="server" Text="Recruitment Type"></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 50%; height: 29px">
                                            <asp:RadioButtonList AutoPostBack="true" ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Selected="True" Text="Local"></asp:ListItem>
                                                <asp:ListItem Text="Overseas"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div>
                                            <asp:HiddenField ID="HiddenField3" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="width: 100%">
                            <asp:UpdatePanel ID="UpdProjectOrDeparmentDetails" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="trLeft" style="float: left; width: 18%; height: 29px">
                                        <%--   Employee Number--%>
                                        <asp:Literal ID="LitProjectOrDepartment" runat="server" Text=" Department"></asp:Literal>
                                    </div>
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                                        <div style="float: left; width: 25%; height: 29px">
                                            <asp:DropDownList ID="ddlProjectOrDepartment" runat="server" CssClass="dropdownlist_mandatory"
                                                Width="180px" Style="background-color: InfoBackground;">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="trLeft" style="float: left; width: 18%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal18" runat="server" Text="Closing Date"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:TextBox ID="TextBox1" runat="server" CssClass="dropdownlist_mandatory" Width="100px">
                                </asp:TextBox>
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 17%; height: 29px">
                                <%--  Salutation--%>
                                <asp:Literal ID="Literal17" runat="server" Text="Status"></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 50%; height: 29px">
                                            <asp:RadioButtonList AutoPostBack="true" ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                                OnSelectedIndexChanged="rdlDepartmentOrProject_SelectedIndexChanged">
                                                <asp:ListItem Selected="True" Text="Active"></asp:ListItem>
                                                <asp:ListItem Text="InActive"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <div>
                                            <asp:HiddenField ID="HiddenField2" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
              
                <div class="TabbedPanelsContent" id="divTab2" style="display: none;">
                    <asp:UpdatePanel ID="updDetails" runat="server">
                        <ContentTemplate>
                            <div style="width: 100%">
                                <div class="trRight" style="float: left; padding-top: 10px; width: 99%; height: auto;
                                    overflow: auto">
                                    <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlPlanDetails"
                                        Width="100%" runat="server" RepeatColumns="1" BackColor="White" BorderColor="#CCCCCC"
                                        BorderStyle="None" BorderWidth="1px" CellPadding="0" CellSpacing="0" 
                                        GridLines="Both" onitemdatabound="dlPlanDetails_ItemDataBound">
                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                        <ItemStyle ForeColor="#000066" />
                                        <SeparatorStyle BackColor="AliceBlue" />
                                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <HeaderTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td style ="width:20px">
                                                    </td>
                                                    <td style ="width:105px">
                                                        <asp:HiddenField ID="hfDesignationID" runat="server" Value='<%#Eval("DesignationID") %>'>
                                                        </asp:HiddenField>
                                                        <asp:Label ID="lblDesignation" runat="server" Text="Designation"></asp:Label>
                                                    </td>
                                                    <td style ="width:50px">
                                                        <asp:Label ID="lblNewHires" Text="New Hires" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td style ="width:105px">
                                                        <asp:Label ID="Label3" Text="Pay Structure" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td style ="width:60px">
                                                        <asp:Label ID="lblAvailable" Text="Available" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td style ="width:70px">
                                                        <asp:Label ID="Label4" Text="Amount" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td style ="width:20px">
                                                        <asp:Label ID="Label1" Text="" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td style ="width:20px">
                                                        <asp:Label ID="Label2" Text="" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    
                                                     <td style ="width:20px">
                                                  
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <table width="100%">
                                               <tr>
                                                    <td style ="width:20px">
                                                    
                                                    <asp:CheckBox ID="chkSelect" runat ="server" />
                                                    </td>
                                                    <td style ="width:105px">
                                                        <asp:HiddenField ID="hfDesignationID" runat="server" Value='<%#Eval("DesignationID") %>'>
                                                        </asp:HiddenField>
                                                      <asp:DropDownList Width="120px" ID="ddlDesignation" runat ="server" ></asp:DropDownList>
                                                    </td>
                                                    <td style ="width:50px">
                                                        <asp:Label ID="lblNewHires1" Text="10" runat="server">
                                                        </asp:Label>
                                                        
                                                        
                                                    </td>
                                                    <td style ="width:105px;margin:0px;padding:0px">
                                                      <asp:DropDownList ID="ddlPayStructure" runat ="server" Width ="120px"></asp:DropDownList>
                                                    </td>
                                                    <td style ="width:60px">
                                                      <asp:Label  ID="lblAvailable" runat ="server" Text ="10">
                                                      </asp:Label>
                                                    </td>
                                                    <td style ="width:70px">
                                                        <asp:TextBox ID="txtPayStructureAmount" Width ="70px" Text="" runat="server">
                                            
                                                        </asp:TextBox>
                                                    </td>
                                                   <td style="width: 20px">
                                                       <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/images/edit.png" OnClick="imgEdit_Click"
                                                           CommandArgument='<%# Eval("DesignationID") %>' />
                                                   </td>
                                                   <td style="width: 20px">
                                                       <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete_popup.png" />
                                                   </td>
                                                    <td style="width: 20px">
                                                          <asp:Button ID="btnAdd" runat ="server" Text ="+" />
                                                   </td>
                                                </tr>
                                            
                                            
                                            
                                            
                                            
                                              <%--  <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hfDesignationID" runat="server" Value='<%# Eval("DesignationID") %>' />
                                                        <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                    </td>
                                                    <td >
                                                        <asp:Label ID="lblNewHires" Text="10" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td >
                                                        <asp:DropDownList ID="ddlPayStructure" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td >
                                                        <asp:Label ID="lblAvailable" Text="5" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblPayStructureAmount" Text="100000" runat="server">
                                            
                                                        </asp:Label>
                                                    </td>
                                                    <td >
                                                        <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/images/edit.png" OnClick="imgEdit_Click"
                                                            CommandArgument='<%# Eval("DesignationID") %>' />
                                                    </td>
                                                    <td >
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/delete_popup.png" />
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td colspan="7">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblFromDateL" Visible="false" Text=" From Date :" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblFromDate" Text="" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblToDateL" Visible="false" Text=" To Date :" runat="server"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblToDate" Text="" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                
                
                  <div class="TabbedPanelsContent" id="divTab3" style="display: none;">
                    <div style="clear: both; width: 100%; height: auto">
                        <div class="trLeft" style="float: left; width: 50%; height: 29px; font-weight: bold">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal5" runat="server" Text="Select Designation"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 95%; padding-left: 11px; max-height: 400px;
                            overflow: scroll">
                            <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlDesignation"
                                        Width="100%" runat="server" RepeatColumns="3" BackColor="White" BorderColor="#CCCCCC"
                                        BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both" OnItemCommand="dlDesignation_ItemCommand"
                                        OnItemDataBound="dlDesignation_ItemDataBound">
                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                        <ItemStyle ForeColor="#000066" />
                                        <SeparatorStyle BackColor="AliceBlue" />
                                        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <HeaderTemplate>
                                            <asp:Literal ID="Literal1" runat="server" Text="Designation"></asp:Literal>
                                        </HeaderTemplate>
                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="width: 20px">
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="True" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField ID="hfDesignationID" runat="server" Value='<%# Eval("DesignationID") %>' />
                                                        <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both; height: auto">
            </div>
            <div style="float: right; margin-top: 6px; width: 18%">
                <asp:Button ID="btnSave" runat="server" Text='<%$Resources:ControlsCommon,Save %>'
                    CssClass="btnsubmit" OnClick="btnSave_Click" ValidationGroup="SaveInitiation"
                    OnClientClick="return Save(true);" />&nbsp
                <asp:Button Width="50px" ID="btnCancel" runat="server" Text='<%$Resources:ControlsCommon,Cancel %>'
                    CssClass="btnsubmit" OnClick="btnCancel_Click" />
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <AjaxControlToolkit:ModalPopupExtender ID="mdPopUp" runat="server" TargetControlID="btn"
                PopupControlID="popupmain1">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btn" runat="server" Style="display: none" />
            <div id="popupmain1" style="width: 40%; height: auto; overflow: auto; background-color: White">
                <div id="header11" style="width: 100%">
                    <div id="hbox1">
                        <div id="headername">
                            <asp:Label ID="lblHeading" runat="server" Text="Plan Details"></asp:Label>
                        </div>
                    </div>
                    <div id="hbox2">
                        <div id="close1">
                            <a href="">
                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                    OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                            </a>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; overflow: hidden;">
                    <asp:UpdatePanel ID="updMain" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="clear: both">
                            </div>
                            <div style="display: block; border: solid 1px black; padding-top: 10px; min-height: 170px">
                                <div style="margin-top: 15px">
                                    <div class="labeltext" style="float: left; width: 18%">
                                        <asp:Literal ID="Literal12" Text="From Date" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 30%">
                                        <asp:TextBox ID="txtFromDate" Width="108px" runat="server"></asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="CalFromDate" runat="server" TargetControlID="txtFromDate"
                                            Format="dd-MMM-yyyy" PopupButtonID="imgFromDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:ImageButton Style="padding-left: 5px" ID="imgFromDate" runat="server" ImageUrl="~/images/calendar.png" />
                                    </div>
                                    <div class="labeltext" style="float: left; width: 10%">
                                        <asp:Literal ID="Literal13" Text="To Date" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 30%">
                                        <asp:TextBox ID="txtToDate" Width="108px" runat="server"></asp:TextBox>
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate"
                                            Format="dd-MMM-yyyy" PopupButtonID="ImgToDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:ImageButton Style="padding-left: 5px" ID="ImgToDate" runat="server" ImageUrl="~/images/calendar.png" />
                                    </div>
                                    <div style="padding-top: 12px; clear: both">
                                    </div>
                                    <div class="labeltext" style="float: left; width: 18%">
                                        <asp:Literal ID="Literal14" Text="Budjet" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 28%">
                                        <asp:TextBox ID="txtBudjet" Width="108px" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 12%">
                                        <asp:Literal ID="Literal9" Text="New Hire" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 25%">
                                        <asp:TextBox ID="txtNewHire" Width="108px" runat="server"></asp:TextBox>
                                    </div>
                                    <div style="padding-top: 12px; clear: both">
                                    </div>
                                    <div class="labeltext" style="float: left; width: 18%">
                                        <asp:Literal ID="litTotal" Text="Required" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 28%">
                                        <asp:TextBox ID="txtRequired" Width="80px" runat="server"></asp:TextBox>
                                        <asp:UpdatePanel ID="updSearch" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="imgGetAvailableDetails" runat="server" ToolTip="Search available members"
                                                    ImageUrl="~/images/search.png" OnClick="imgGetAvailableDetails_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 12%">
                                        <asp:Literal ID="Literal10" Text="Available" runat="server">
                                        </asp:Literal>
                                    </div>
                                    <div class="labeltext" style="float: left; width: 25%">
                                        <asp:TextBox ID="txtAvailable" Width="108px" runat="server"></asp:TextBox>
                                    </div>
                                    <div style="padding-top: 12px; clear: both">
                                    </div>
                                    <div style="clear: both">
                                        <asp:UpdatePanel ID="updAvailableDetails" runat="server">
                                            <ContentTemplate>
                                                <asp:DataList ID="dlAvailableDetails" Width="100%" runat="server" CellPadding="4"
                                                    ForeColor="#333333">
                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                    <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                                    <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                    <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                    <HeaderTemplate>
                                                        <table>
                                                            <tr>
                                                                <td style="width: 20px">
                                                                </td>
                                                                <td style="width: 200px">
                                                                    <asp:Literal ID="litTotal" Text="Employee" runat="server">
                                                                    </asp:Literal>
                                                                </td>
                                                                <td style="width: 250px">
                                                                    <asp:Literal ID="Literal8" Text="Project Details" runat="server">
                                                                    </asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td style="width: 20px">
                                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                                </td>
                                                                <td style="width: 200px">
                                                                    <asp:Label ID="lblAvailableEmployee" CssClass="labeltext" runat="server" Text='<%#Eval("EmployeeName") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 250px">
                                                                    <asp:Label ID="lblEmployee" CssClass="labeltext" runat="server" Text='<%#Eval("Details") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="btnAddAvailableDetails" Text="Transfer selected employees" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:DataList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="clear: both;" id="footerwrapper">
                    <div id="footer11" style="width: 100%">
                        <asp:UpdatePanel ID="updSubmit" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: right">
                                    <asp:Button ID="Button1" CssClass="btnsubmit" runat="server" Text="Save" OnClick="Button1_Click" />
                                    <asp:Button ID="Button2" CssClass="btnsubmit" runat="server" Text="Cancel" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="dvHiringSchedule">
        <asp:UpdatePanel ID="updHiringSchedule" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AjaxControlToolkit:ModalPopupExtender ID="MdHiringSchedule" runat="server" TargetControlID="btnHiringScheduleTemp"
                    PopupControlID="dvHiringPopUP">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Button ID="btnHiringScheduleTemp" runat="server" Style="display: none" />
                <div id="dvHiringPopUP">
                    <uc1:HiringSchedule ID="HiringSchedule" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/New_Employee_Big.PNG" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnNew" runat="server" OnClick="btnNew_Click">
                            <%--  Add Employee--%>
                            <asp:Literal ID="Literal59" runat="server" Text="New Planning">
                            
                            </asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Lit1" runat="server" Text="View All Plans">
                            
                            </asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkSchedule" runat="server" OnClick="lnkSchedule_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Literal7" runat="server" Text="Hiring Schedule">
                            
                            </asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnDelete" Text='<%$Resources:ControlsCommon,Delete %>' runat="server"
                            CausesValidation="false" OnClick="btnDelete_Click"> 
                          <%-- Delete--%>
                           
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnPrint" Text='<%$Resources:ControlsCommon,Print %>' runat="server"
                            CausesValidation="false" OnClick="btnPrint_Click">
                         <%--Print--%>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnEmail" Text='<%$Resources:ControlsCommon,Email %>' runat="server"
                            CausesValidation="false" OnClick="btnEmail_Click">
              
             <%--   Email--%>  
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
