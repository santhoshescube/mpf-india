﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/*
    Created By  :    MEGHA
    Date        :    23 OCT 2013
    Description :    View/Approve Jobs
 */
public partial class Public_HomeActiveVacancies : System.Web.UI.Page
{
    #region Declarations
    clsJob objJob;
    clsUserMaster objUserMaster =new clsUserMaster();
    clsRoleSettings objRoleSettings;
    #endregion Declarations

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        pgrActiveVacancies.Fill += new controls_Pager.FillPager(BindVacancies);
        if (!IsPostBack)
        {
            //objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            int RoleID = objUserMaster.GetRoleId();
            //objJob = new clsJob();
            EnableMenus();

            if (ViewState["IsView"].ToBoolean() == true || ViewState["IsDelete"].ToBoolean() == true ||
                ViewState["IsCreate"].ToBoolean() == true || ViewState["IsUpdate"].ToBoolean() == true ||
                ViewState["IsApprove"].ToBoolean() == true || ViewState["IsReportingTo"].ToBoolean() == true)
            {
                FillCombo();
                BindVacancies();
            }
            else
            {
                lblMessage.Style["display"] = "block";
                lblMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("ليس لديك إذن ما يكفي لعرض هذه الصفحة.") : ("You dont have enough permission to view this page."); //"You dont have enough permission to view this page.";                
            }
        }
    }

    /// <summary>
    /// Displays Jobs based on Department selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlVacancyDepartments_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrActiveVacancies.CurrentPage = 0;
        BindVacancies();
    }

    /// <summary>
    /// Displays Jobs which are either Approved/Waiting for Approval
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlStatusFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrActiveVacancies.CurrentPage = 0;
        BindVacancies();
    }

    /// <summary>
    /// Displays Job Details or Redirects To Job for Approval
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void dlActiveVacancies_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlAnchor ancApprove = (HtmlAnchor)e.Item.FindControl("ancApprove");
            //HiddenField hfID = (HiddenField)e.Item.FindControl("hfID");
            HiddenField hfReportingTo = (HiddenField)e.Item.FindControl("hfReportingTo");

            if (Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "Approved" || Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "وافق"
                || Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "Closed" || Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "مغلق" ||
                Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "Rejected" || Convert.ToString(dlActiveVacancies.DataKeys[e.Item.ItemIndex]) == "مرفوض")
            {
                //ancApprove.Disabled = true;
                //ancApprove.Visible = false;
                //ancApprove.HRef = "#";
            }
            else
            {
                objRoleSettings = new clsRoleSettings();
                //objUserMaster = new clsUserMaster();
                //objJob = new clsJob();
                if (ViewState["IsApprove"].ToBoolean() == true)
                {
                    //ancApprove.Disabled = true;
                    //ancApprove.HRef = "Job.aspx?JobId=" + hfID.Value.ToInt32() + "&Approval=true" ;
                }
                //else if (ViewState["IsReportingTo"].ToBoolean() == true)
                //{                   
                //    if (objUserMaster.GetEmployeeId().ToInt32() == hfReportingTo.Value.ToInt32())
                //    {                                                         
                //        //ancApprove.Disabled = true;
                //        //ancApprove.HRef = "Job.aspx?JobId=" + hfID.Value.ToInt32() + "&Approval=true";                       
                //    }
                //    else
                //    {
                //        //ancApprove.Disabled = false;
                //        //ancApprove.HRef = "#";
                //    }
                //}
                else
                {
                    //ancApprove.Disabled = false;
                    //ancApprove.HRef = "#";
                }
            }
        }
    }

    protected void dlActiveVacancies_PreRender(object sender, EventArgs e)
    {
        if (dlActiveVacancies.Items.Count == 0)
            pgrActiveVacancies.Visible = false;
        else
            pgrActiveVacancies.Visible = true;

        if (dlActiveVacancies.DataSource == null)
            tblActiveVacancyHeader.Attributes.Add("style", "display:none");
        else
            tblActiveVacancyHeader.Attributes.Add("style", "display:block");

    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindVacancies();
    }

    #endregion Events

    #region Methods

    /// <summary>
    /// Binds dropdowns for Department and Job Status
    /// </summary>
    private void FillCombo()
    {
        //objJob = new clsJob();
        //DataSet dsDepartment = objJob.GetDepartmentList();
        ddlVacancyDepartments.DataSource = clsVacancy.GetDepartmentList();
        ddlVacancyDepartments.DataBind();
        string sDept = clsGlobalization.IsArabicCulture() ? "جميع الإدارات" : "All Departments";
        ddlVacancyDepartments.Items.Insert(0, new ListItem(sDept, "-1"));

       // DataSet dsStatus = clsVacancy.GetHomeVacancyStatus();
        ddlStatusFilter.DataSource = clsVacancy.GetHomeVacancyStatus();
        ddlStatusFilter.DataBind();

        clsHomePage objHomePage = new clsHomePage();

        ddlCompany.DataSource = clsVacancyAddEdit.GetActiveVacanciesCompany(objUserMaster.GetCompanyId());//objHomePage.GetAllCompany(objUserMaster.GetUserId());
        ddlCompany.DataBind();

    }

    /// <summary>
    /// Gets and Displays jobs satisfying conditions
    /// </summary>
    protected void BindVacancies()
    {
        lblMessage.Style["display"] = "none";
        //objJob = new clsJob();
        objRoleSettings = new clsRoleSettings();
        //objUserMaster = new clsUserMaster();

        //objJob.PageIndex = pgrActiveVacancies.CurrentPage + 1;
        //objJob.PageSize = pgrActiveVacancies.PageSize;
        //objJob.DepartmentId = ddlVacancyDepartments.SelectedValue.ToInt32();
        //objJob.JobStatusID = ddlStatusFilter.SelectedValue.ToInt32();

        DataTable dt = clsVacancy.GetRecentVacancies(pgrActiveVacancies.CurrentPage + 1,
                                                     pgrActiveVacancies.PageSize,
                                                     ddlVacancyDepartments.SelectedValue.ToInt32(),
                                                     ddlStatusFilter.SelectedValue.ToInt32(), objUserMaster.GetCompanyId());
        if (dt.Rows.Count == 0)
        {
            dlActiveVacancies.ShowHeader = false;
            pgrActiveVacancies.Total = 0;
        }
        else
        {
            dlActiveVacancies.ShowHeader = true;
            pgrActiveVacancies.Total = clsVacancy.GetRecordCount(ddlVacancyDepartments.SelectedValue.ToInt32(),
                                                                 ddlStatusFilter.SelectedValue.ToInt32(), objUserMaster.GetCompanyId());
        }

        dlActiveVacancies.DataSource = dt;
        dlActiveVacancies.DataBind();
        if (dlActiveVacancies.Items.Count == 0)
        {
            lblMessage.Style["display"] = "block";
            lblMessage.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يوجد سجل") : ("No Records Found"); //"No Vacancies";
        }
    }

    public void EnableMenus()
    {
        //objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        if (RoleID > 4)
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.ActiveVacancies);
            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }
      
    }
    #endregion Methods
}
