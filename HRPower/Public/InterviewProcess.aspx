﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="InterviewProcess.aspx.cs" Inherits="Public_InterviewProcess" Title="Interview Process" %>

<%@ Register Src="../Controls/CandidateView.ascx" TagName="CandidateView" TagPrefix="uc1" %>
<%@ Register Src="../Controls/InterviewResult.ascx" TagName="InterviewResult" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li class='selected'><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

function SetMark(id)
{debugger ;

 var buttonNo   = document.getElementById(id);
 var temp       = id.substr(0, id.lastIndexOf('_'))
 var textBox    = document.getElementById(temp+"_txtMark");
 var dlEvaluate = document.getElementById(temp.substr(0, temp.lastIndexOf('_')));
 var TotalMarks = 0   
 var textBox1   = dlEvaluate.getElementsByTagName("INPUT");
 var lblResult  =document.getElementById('<%=lblResult.ClientID%>');
 var mark
 
 var hidControl = document.getElementById("ctl00_hidCulture");
 
 if(textBox && buttonNo )
 {
    textBox.value = buttonNo.value;
 }
 
 for(var i = 0; i < textBox1.length; i++)
  {
     if(textBox1[i].type == "text")
     {
            if(textBox1[i].value == "")
                 mark = 0;
            else
               mark = parseInt(textBox1[i].value);
       TotalMarks  = TotalMarks + mark;
     }
  }
 
 
 if(lblResult)
 {
 if(hidControl.value == "en-US")
    lblResult.innerText ="Total marks :"+ TotalMarks;
 else
    lblResult.innerText ="مجموع علامات :"+ TotalMarks;
  
 }
 
return false;

}

function checkMark(id)
{

    var txtMark   = document.getElementById(id);
    var temp       = id.substr(0, id.lastIndexOf('_'))
    var txtMark1    = document.getElementById(id.replace('txtMark','txtMark1'  ));

    if (txtMark.value != "")
    {
        if (parseInt(txtMark.value) > parseInt(txtMark1.value))
        {
          alert("Please check mark");
          txtMark.value = txtMark1.value;
          return false;
        }  
    }
    
    
 var textBox   = document.getElementById(id);
 var temp       = id.substr(0, id.lastIndexOf('_'))
 var dlEvaluate = document.getElementById(temp.substr(0, temp.lastIndexOf('_')));
 var TotalMarks = 0   
 var textBox1   = dlEvaluate.getElementsByTagName("INPUT");
 var lblResult  =document.getElementById('<%=lblResult.ClientID%>');
 var mark
  var hidControl = document.getElementById("ctl00_hidCulture");
     
    for(var i = 0; i < textBox1.length; i++)
  {
     if(textBox1[i].type == "text")
     {
            if(textBox1[i].value == "")
                 mark = 0;
            else
               mark = parseInt(textBox1[i].value);
       TotalMarks  = TotalMarks + mark;
     }
  }
 
 
 if(lblResult)
 {
    if(hidControl.value == "en-US")
        lblResult.innerText ="Total marks :"+ TotalMarks;
     else
        lblResult.innerText ="مجموع علامات :"+ TotalMarks;
 }
 
}

function ClearAll()
{
debugger ;

 var dlEvaluate = document.getElementById('<%=dlEvaluate.ClientID%>');
 var textBox1   = dlEvaluate.getElementsByTagName("INPUT");
 
   
 if(dlEvaluate && textBox1 )
 {
  for(var i = 0; i < textBox1.length; i++)
  {
     if(textBox1[i].type == "text")
     {
     textBox1[i].value =""
     }
  }
 }
 return false;
}






function Validate()
{
debugger ;
 var dlEvaluate = document.getElementById('<%=dlEvaluate.ClientID%>');
 var textBox1   = dlEvaluate.getElementsByTagName("INPUT");
 
  var isValid = true; 
 if(dlEvaluate && textBox1 )
 {
  for(var i = 0; i < textBox1.length; i++)
  {
     if(textBox1[i].type == "text" && textBox1[i].value =="" )
     {
        isValid = false;
        break ;
     }
  }
  
  if(isValid == false)
  {
  alert("Please enter the marks for all criteria");
  return false;
  }
  else
  return true;
  
 }
 return false;
}
    </script>

    <div style="width: 100%">
        <asp:UpdatePanel ID="updMain" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Timer ID="timer" Enabled="false" runat="server" Interval="3000">
                </asp:Timer>
                <div style="display: none; float: left; width: 100%">
                    <div style="margin-left: 200px">
                        <div style="float: left; width: 70px; margin-top: 5px">
                            <asp:CheckBox ID="chkManual" runat="server" Checked="true" Visible="false" />
                        </div>
                        <div style="float: left; width: 300px; margin-top: 2px">
                            <asp:DropDownList ID="ddlSchedules" Width="300px" AutoPostBack="true" runat="server"
                                OnSelectedIndexChanged="ddlSchedules_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlCandidates" Width="300px" AutoPostBack="true" runat="server"
                                OnSelectedIndexChanged="ddlCandidates_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <div style="float: left; width: 40px">
                            <asp:Button ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click" />
                        </div>
                    </div>
                </div>
                <div style="clear: both">
                </div>
                <div id="dvMain" runat="server">
                    <div style="float: left;">
                        <h4 style="color: #30a5d6">
                        </h4>
                    </div>
                    <div style="float: right; margin-top: 5px;">
                        <asp:Button ID="btnGoBack" runat="server" CssClass="btnsubmit" OnClick="btnGoBack_Click"
                            Text='<%$Resources:ControlsCommon,Back%>' ToolTip="View Schedules" />
                    </div>
                    <uc1:CandidateView ID="CandidateView1" runat="server" />
                </div>
                <div id="divSchedules" runat="server">
                    <div style="width: 100%; padding-bottom: 10px;">
                        <div style="width: 100%; padding-bottom: 10px;">
                            <div id="Div1" style="float: left; color: #30a5d6">
                                <img src="../images/Interview_Process_DetailsBig.png" />
                                <b>
                                    <%--Interview Process--%>
                                    <asp:Literal ID="lit1" runat="server" meta:resourcekey="InterviewProcess"></asp:Literal>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%;" runat="server" id="divSearch">
                        <div style="padding-top: 20px; padding-bottom: 5px; padding-left: 100px;">
                            <asp:UpdatePanel ID="updInterviewer" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlInterviewer" runat="server" DataTextField="InterviewerName"
                                        DataValueField="InterviewerID" AutoPostBack="true" OnSelectedIndexChanged="ddlInterviewer_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="updJob" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlJob" runat="server" DataTextField="Job" DataValueField="JobID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="updCandidate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlCandidate" runat="server" DataTextField="Candidate" DataValueField="CandidateID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlCandidate_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ddlJob" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="updStatus" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlStatus" runat="server" DataTextField="CandidateStatus" DataValueField="CandidateStatusID"
                                        AutoPostBack="true" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 100%; margin-top: 30px;">
                        <asp:UpdatePanel ID="updViewProcess" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvInterviewProcess" runat="server" AutoGenerateColumns="False"
                                    GridLines="None" Width="100%" DataKeyNames="CandidateId,CandidateStatusId,JobID,InterviewerID"
                                    OnRowCommand="gvInterviewProcess_RowCommand" OnRowDataBound="gvInterviewProcess_RowDataBound"
                                    CellSpacing="0" CellPadding="0" Style="margin-right: 0px; margin-left: 0px;"
                                    CssClass="labeltext">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <div style="float: left; width: 98%;" class="datalistheader">
                                                    <table width="100%" cellspacing="0" border="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left" valign="top" width="6%">
                                                                <%-- Sl.No--%>
                                                                <asp:Literal ID="lit1" runat="server" meta:resourcekey="SlNo"></asp:Literal>
                                                            </td>
                                                            <td align="left" width="35%">
                                                                <%--Candidate--%>
                                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                                                            </td>
                                                            <td align="left" width="35%">
                                                                <%--Job Title--%>
                                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="JobTitle"></asp:Literal>
                                                            </td>
                                                            <td align="left" width="10%">
                                                                <%--Levels--%>
                                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Levels"></asp:Literal>
                                                            </td>
                                                            <td align="left" width="10%">
                                                                <%--Status--%>
                                                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                            </td>
                                                            <td align="left" width="10%">
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%" cellspacing="0" border="0" cellpadding="0" style="margin-left: 5px;">
                                                    <tr style="width: 100%;">
                                                        <td class="datalistTrLeft" width="6%" valign="top">
                                                            <%#Eval("RowNum")%>
                                                        </td>
                                                        <td align="left" class="datalistTrLeft" width="35%" valign="top">
                                                            <asp:LinkButton ID="lnkCandidate" CssClass="linkbutton" runat="server" CommandArgument='<%# Eval("CandidateId") %>'
                                                                ToolTip="View Candidate" CommandName="_ViewCandidate" CausesValidation="False"><%#Eval("Candidate")%> </asp:LinkButton>
                                                        </td>
                                                        <td align="left" class="datalistTrRight" width="35%" valign="top">
                                                            <%#Eval("JobSchedule")%>&nbsp;
                                                        </td>
                                                        <td align="left" class="datalistTrRight" width="10%" valign="top">
                                                            <asp:LinkButton ID="ancCandidateLevels" CssClass="linkbutton" runat="server" CommandArgument='<%# Eval("CandidateId") %>'
                                                                ToolTip="View Levels" CommandName="_ViewLevels" CausesValidation="False">(<%#Eval("Levels")%>)</asp:LinkButton>
                                                        </td>
                                                        <td align="left" class="datalistTrRight" width="10%" valign="top">
                                                            <%#Eval("CandidateStatus")%>
                                                            <asp:HiddenField ID="hfdStatus" runat="server" Value=' <%#Eval("CandidateStatus")%>' />
                                                        </td>
                                                        <td align="left" width="10%">
                                                            <asp:ImageButton ID="ibtnSendOffer" runat="server" CommandArgument='<%# Eval("CandidateId") %>'
                                                                ImageUrl="~/images/sent_icon_grey.png" CommandName="_SendOffer" CausesValidation="false"
                                                                ToolTip="Send Offer Letter" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hfCandidateLevels" runat="server" Value="0" />
                                                <div id="divCandidateLevels" runat="server" style="display: none;">
                                                    <fieldset style="padding-left: 25px">
                                                        <AjaxControlToolkit:TabContainer ID="tcPerformanceDetails" runat="server" ActiveTabIndex="0"
                                                            AutoPostBack="false">
                                                            <AjaxControlToolkit:TabPanel ID="tpPanelDefault" runat="server">
                                                                <ContentTemplate>
                                                                </ContentTemplate>
                                                            </AjaxControlToolkit:TabPanel>
                                                        </AjaxControlToolkit:TabContainer>
                                                    </fieldset>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <center>
                                    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: none;"></asp:Label>
                                </center>
                                <uc:Pager ID="InterviewProcessPager" runat="server" Visible="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div id="dvError" runat="server" style="display: block; width: 100%">
                    <div style="float: left; width: 400px">
                        <%-- <h4 class="listHeader">
                            Waiting for new candidate ........</h4>--%>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="updPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <AjaxControlToolkit:ModalPopupExtender Drag="true" RepositionMode="RepositionOnWindowScroll"
                PopupControlID="pnlEvaluationResult" TargetControlID="btn" ID="ModalPopupExtender3"
                runat="server">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btn" runat="server" Style="display: none" />
            <asp:Panel ID="pnlEvaluationResult" runat="server">
                <uc2:InterviewResult ID="InterviewResult1" runat="server" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updEvaluate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <AjaxControlToolkit:ModalPopupExtender Drag="true" RepositionMode="RepositionOnWindowScroll"
                PopupDragHandleControlID="popupmain" PopupControlID="popupmain11" TargetControlID="btnPopupEvaluate"
                ID="ModalPopupExtender2" runat="server" CancelControlID="ibtnClose">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btnPopupEvaluate" runat="server" Style="display: none" />
            <div id="popupmain11" style="width: 800px; height: auto; overflow: hidden">
                <div id="header11" style="width: 100%; overflow: hidden">
                    <div id="hbox1">
                        <div id="headername">
                            <asp:Label ID="lblHeading" runat="server" meta:resourcekey="InterviewResult"></asp:Label>
                            <asp:Label ID="lblName" runat="server" meta:resourcekey="InterviewResult"></asp:Label>
                        </div>
                    </div>
                    <div id="hbox2">
                        <div id="close1">
                            <a href="">
                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                    OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                            </a>
                        </div>
                    </div>
                </div>
                <asp:DataList ID="dlEvaluate" runat="server" BorderStyle="None" BorderWidth="1px"
                    Width="100%" BackColor="White" BorderColor="#CCCCCC" CellPadding="3" GridLines="Both">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <ItemStyle ForeColor="#000066" />
                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="max-width: 150px; height: 50px; vertical-align: middle" class="trLeft">
                                    <%-- Criteria--%>
                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Criteria"></asp:Literal>
                                </td>
                                <td style="padding-left: 18px; margin-left: 10px; width: 89px; height: 50px; vertical-align: middle"
                                    class="trLeft">
                                    <%--Weightage--%>
                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Weightage"></asp:Literal>
                                </td>
                                <td style="width: 150px; height: 50px; vertical-align: middle" class="trLeft" align="left">
                                    <%--Mark--%>
                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Mark"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td style="max-width: 150px; height: 50px; vertical-align: middle" class="trLeft">
                                    <%# Eval("Criteria") %>
                                </td>
                                <td style="padding-left: 18px; width: 89px; height: 50px; vertical-align: middle"
                                    class="trLeft">
                                    <%# Eval("WeightagePercentage")%>
                                </td>
                                <td style="width: 150px; height: 50px; vertical-align: middle" class="trLeft" align="left">
                                    <asp:HiddenField ID="hfCriteriaID" runat="server" Value='<%# Eval("CriteriaID") %> ' />
                                    <asp:HiddenField ID="hfJobID" runat="server" Value='<%# Eval("JobID") %> ' />
                                    <asp:HiddenField ID="hfJobLevelID" runat="server" Value='<%# Eval("JobLevelID") %> ' />
                                    <asp:HiddenField ID="hfCandidateID" runat="server" Value='<%# Eval("CandidateID") %> ' />
                                    <asp:HiddenField ID="hfJobScheduleDetailID" runat="server" Value='<%# Eval("JobScheduleDetailID") %> ' />
                                    <asp:TextBox ID="txtMark" runat="server" ValidationGroup="Save" Width="50px" MaxLength="5"
                                        onblur="checkMark(this.id)" Text='<%#Eval("AssignedMarks").ToDecimal() == -1 ? "": Eval("AssignedMarks") %>'></asp:TextBox>
                                    &nbsp;/<%# Eval("Marks")%>
                                    <%--  <asp:TextBox ID="txtMark1" runat ="server" Text =' <%# Eval("Marks")%>'  Visible ="false" ></asp:TextBox>  --%>
                                    <asp:HiddenField ID="txtMark1" runat="server" Value=' <%# Eval("Marks")%>'></asp:HiddenField>
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteMark" runat="server" TargetControlID="txtMark"
                                        FilterMode="ValidChars" ValidChars="." FilterType="Numbers,custom">
                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <div id="div9" class="trRight" style="padding: 0px; margin: 0px; padding-top: 0px;
                    padding-left: 10px; padding-bottom: 5px; float: left; width: 98.7%; background-color: White;
                    height: 20px" runat="server">
                    <asp:Label ID="lblResult" Text="" runat="server"></asp:Label>
                </div>
                <div style="clear: both; height: 100%" id="footerwrapper">
                    <div id="footer11" style="width: 100%">
                        <div id="buttons" style="clear: both; width: 100%">
                            <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="clear: both; float: left; text-align: left; padding-left: 10px;">
                                        <asp:Label ID="lblWarning" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div style="float: right;" align="left">
                                        <asp:Button ID="btnSaveEvaluation" OnClientClick="return Validate()" Text='<%$Resources:ControlsCommon,SaveEvaluation%>'
                                            CssClass="popupbutton" ValidationGroup="Save" runat="server" OnClick="btnSaveEvaluation_Click" />
                                        <asp:Button ID="btnClearAll" Text='<%$Resources:ControlsCommon,Clear%>' OnClientClick="return ClearAll()"
                                            ValidationGroup="Clear" CssClass="popupbutton" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/evaluateinterview.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkEvaluate" runat="server" OnClick="lnkEvaluate_Click1">
                            <%-- Evaluate--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Evaluate"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/interview_results.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkInterviewREsult" runat="server" OnClick="lnkInterviewREsult_Click">
                            <%--  Interview Result--%>
                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="InterviewResult"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPrint" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divPrint" runat="server" style="display: none">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
