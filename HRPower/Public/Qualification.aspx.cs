﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Text;

/*******************************************
    *  Created By    : Siny
    *  Created Date  : 23 Oct 2013
    *  Purpose       : Add/update/delete qualification
    ********************************************/

public partial class Public_Qualification : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsQualification objQualification;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button

    private string CurrentSelectedValue
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("QualificationTitle.Title").ToString();
        this.RegisterAutoComplete();

        if (!IsPostBack)
        {
            objQualification = new clsQualification();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            CurrentSelectedValue = "-1";
            BindCombos();
            SetPermission();
            EnableMenus();
            divIssueReceipt.Style["display"] = "none";
            //pgrEmployees.Visible = false;
            //lblNoData.Visible = false;

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

            // btnList_Click(sender, e);
        }

        // ReferenceControlNew1.OnUpdate += new controls_DegreeReference.OnUpdate(ReferenceControlNew1_OnUpdate);
        rnUniversity.OnSave += new controls_ReferenceControlNew.saveHandler(rnUniversity_OnSave);

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);

        DegreeReference1.OnSave += new controls_DegreeReference.SaveDegree(DegreeReference1_OnSave);
    }

    void DegreeReference1_OnSave(int DegreeID)
    {
        FillComboDegree();

        if (DegreeID > 0)
        {
            rcDegree.SelectedValue = DegreeID.ToString();
            updDegree.Update();
        }

    }

    #region Issue Receipt
    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>" + Result + "</h5>";
        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        //lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        //lblCurrentStatus.Text = "Issued";
        //    }

        //    upMenu.Update();
        //}
    }

    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hfMode.Value == "Edit")
        {
            int intOperationTypeID = (int)OperationType.Employee, intDocType;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdQualificationID.Value.ToInt32();
            intDocType = (int)DocumentType.Qualification;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtReferenceNumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            btnList_Click(sender, e);
        }
        else
            return;
    }
    #endregion Issue Receipt

    public void FillComboDegree()
    {
        if (rcDegree != null && updDegree != null)
        {
            rcDegree.DataSource = new clsQualification().FillDegree();
            rcDegree.DataTextField = "Degree";
            rcDegree.DataValueField = "DegreeId";
            rcDegree.DataBind();
            rcDegree.SelectedValue = CurrentSelectedValue;
            updDegree.Update();
        }
    }

    public void FillUniversity()
    {
        if (ddlUniversity != null && upnlUniversity != null)
        {
            ddlUniversity.DataSource = new clsQualification().FillUniversity();
            ddlUniversity.DataTextField = "Description";
            ddlUniversity.DataValueField = "UniversityID";
            ddlUniversity.DataBind();
            ddlUniversity.SelectedValue = CurrentSelectedValue;
            upnlUniversity.Update();
        }
    }

    protected void Bind()
    {
        objQualification = new clsQualification();
        objUser = new clsUserMaster();

        objQualification.PageIndex = pgrEmployees.CurrentPage + 1;
        objQualification.PageSize = pgrEmployees.PageSize;
        objQualification.SearchKey = txtSearch.Text;
        objQualification.UserId = objUser.GetUserId();
        objQualification.CompanyID = objUser.GetCompanyId();
        pgrEmployees.Total = objQualification.GetCount();
        DataSet ds = objQualification.GetAllEmployeeDrivingLicenses();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlQualification.DataSource = ds;
            dlQualification.DataBind();
            pgrEmployees.Visible = true;
            lblNoData.Visible = false;
        }
        else
        {
            dlQualification.DataSource = null;
            dlQualification.DataBind();
            pgrEmployees.Visible = false;
            btnPrint.Enabled = btnEmail.Enabled = btnDelete.Enabled = false;
            btnPrint.OnClientClick = btnEmail.OnClientClick = btnDelete.OnClientClick = "return false;";
            lblNoData.Visible = true;

            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetLocalResourceObject("NoQualificationsareaddedyet.Text").ToString(); //"No Qualifications are added yet";
            else
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();  //"No Search results found";
        }

        fvEmployees.Visible = false;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('Employee');");
        upnlNoData.Update();
    }

    protected void NewLicense()
    {
        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        divPassDoc.Style["display"] = "none";
        fvQualification.Visible = false;
        hdQualificationID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        pgrEmployees.Visible = false;
        objQualification = new clsQualification();
        fvEmployees.Visible = true;
        txtReferenceNumber.Text = string.Empty;
        txtSchoolOrCollege.Text = string.Empty;
        txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtToDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        objQualification.QualificationID = -1;
        objQualification.EmployeeID = -1;
        txtOtherInfo.Text = string.Empty;
        // DataTable dtvisadoc = objQualification.GetQualificationDocuments().Tables[0];
        dlVisaDoc.DataSource = null;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = null;
        txtDocname.Text = "";
        btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
        btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
        FillEmployees(0);
        dlQualification.DataSource = null;
        dlQualification.DataBind();
        upnlNoData.Update();
        divIssueReceipt.Style["display"] = "none";
        ControlEnableDisble(true);
        lblCurrentStatus.Text = Convert.ToString(GetGlobalResourceObject("DocumentsCommon", "New")); // "New";
        upMenu.Update();
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        else
            return string.Empty;
    }

    private void BindCombos()
    {
        //objEmployee = new clsEmployee();
        //objRoleSettings = new clsRoleSettings();
        //ddlCompany.DataSource = objEmployee.GetAllCompanies();
        //ddlCompany.DataBind();

        //if (ddlCompany.Items.Count == 0)
        //    ddlCompany.Items.Add(new ListItem("Select", "-1"));

        //ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);

        //if (!(ddlCompany.Enabled))
        //    ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

        FillEmployees(0);
        FillComboDegree();
        FillUniversity();
    }

    private void FillEmployees(long lngEmployeeID)
    {
        objUser = new clsUserMaster();
        if (objQualification == null)
            objQualification = new clsQualification();
        objQualification.CompanyID = objUser.GetCompanyId();
        ddlEmployee.DataSource = objQualification.FillEmployees(lngEmployeeID);
        ddlEmployee.DataBind();

        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem(GetGlobalResourceObject("DocumentsCommon", "Select").ToString(), "-1"));
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Qualification);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
        //DataTable dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.DrivingLicense);
        //ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];
        if (dtm.Rows.Count > 0)
        {
            btnNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            btnList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            btnEmail.Enabled = btnPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            btnDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dtm.Rows[0]["IsUpdate"].ToBoolean();
        }
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();

        //if (UserID > 3)
        //{




        if (btnList.Enabled)
        {
            lblNoData.Visible = false;
            txtSearch.ReadOnly = false;
            Bind();
        }
        else if (btnNew.Enabled)
        {
            lblNoData.Visible = false;
            txtSearch.ReadOnly = false;
            NewLicense();
        }
        else if (UserID > 3)
        {
            pgrEmployees.Visible = false;
            dlQualification.DataSource = null;
            dlQualification.DataBind();
            fvEmployees.Visible = false;
            lblNoData.Visible = true;
            lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString(); //"You dont have enough permission to view this page.";
            ImgSearch.Enabled = false;
            txtSearch.ReadOnly = true;
        }
        // }

        if (btnDelete.Enabled)
            btnDelete.OnClientClick = "return valDeleteDatalist('" + dlQualification.ClientID + "');";
        else
            btnDelete.OnClientClick = "return false;";

        if (btnPrint.Enabled)
            btnPrint.OnClientClick = "return valPrintEmailDatalist('" + dlQualification.ClientID + "');";
        else
            btnPrint.OnClientClick = "return false;";

        if (btnEmail.Enabled)
            btnEmail.OnClientClick = "return valPrintEmailDatalist('" + dlQualification.ClientID + "');";
        else
            btnEmail.OnClientClick = "return false;";

    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Qualification,objUser.GetCompanyId());
    }

    protected void dlQualification_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        objQualification = new clsQualification();
        objUser = new clsUserMaster();

        switch (e.CommandName)
        {
            case "ALTER":
                hfMode.Value = "Edit";
                objQualification.QualificationID = Convert.ToInt32(e.CommandArgument);
                hdQualificationID.Value = Convert.ToString(e.CommandArgument);
                btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
                btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
                BindFormView(Convert.ToInt32(e.CommandArgument));
                txtSearch.Text = string.Empty;
                upnlIssueReceipt.Update();
                upEmployeeVisa.Update();
                break;
            case "VIEW":
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                divIssueReceipt.Style["display"] = "none";
                break;
        }
    }

    private void BindViewMode(int QualificationID)
    {
        dlQualification.DataSource = null;
        dlQualification.DataBind();
        pgrEmployees.Visible = false;
        fvEmployees.Visible = false;
        fvQualification.Visible = true;
        hfMode.Value = "List";
        objQualification.QualificationID = QualificationID;
        hdQualificationID.Value = Convert.ToString(QualificationID);
        fvQualification.DataSource = objQualification.GetEmpDrivingLicense();
        fvQualification.DataBind();
        divPrint.InnerHtml = GetPrintText(QualificationID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if (btnPrint.Enabled )
            btnPrint.OnClientClick = sPrinttbl;
            if(btnEmail.Enabled)
            btnEmail.OnClientClick = "return showMailDialog('Type=Qualification&QualificationID=" + Convert.ToString(fvQualification.DataKey["QualificationID"]) + "');";
            if(btnDelete.Enabled )
            btnDelete.OnClientClick = "return confirm(' " + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + " ');";
        }
    }

    private void BindFormView(int QualificationID)
    {
        fvEmployees.Visible = true;
        objQualification.QualificationID = QualificationID;
        DataTable dt = objQualification.GetEmployeeLicense().Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrEmployees.Visible = false;
        BindQualificationDetails(dt);
        dlQualification.DataSource = null;
        dlQualification.DataBind();
        divPrint.InnerHtml = objQualification.GetPrintText(QualificationID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            btnPrint.OnClientClick = sPrinttbl;
            btnEmail.OnClientClick = "return showMailDialog('Type=Qualification&QualificationID=" + Convert.ToString(fvQualification.DataKey["QualificationID"]) + "');";
            btnDelete.OnClientClick = "return true;";
        }

        if (Convert.ToInt32(dt.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";
        upMenu.Update();
    }

    private void BindQualificationDetails(DataTable dt)
    {
        upMenu.Update();

        if (dt != null && dt.Rows.Count > 0)
        {
            // ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            FillEmployees(dt.Rows[0]["EmployeeID"].ToInt64());
            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dt.Rows[0]["EmployeeID"])));

            if (dt.Rows[0]["DegreeID"] != DBNull.Value)
                rcDegree.SelectedValue = Convert.ToString(dt.Rows[0]["DegreeID"]);

            txtReferenceNumber.Text = Convert.ToString(dt.Rows[0]["ReferenceNumber"]);
            txtSchoolOrCollege.Text = Convert.ToString(dt.Rows[0]["SchoolOrCollege"]);
            ddlUniversity.SelectedValue = Convert.ToString(dt.Rows[0]["UniversityID"]);
            txtFromDate.Text = Convert.ToDateTime(dt.Rows[0]["FromDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtToDate.Text = Convert.ToDateTime(dt.Rows[0]["ToDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtOtherInfo.Text = dt.Rows[0]["Remarks"].ToStringCustom();
            txtCertificateTitle.Text = dt.Rows[0]["CertificateTitle"].ToStringCustom();
            txtGrade.Text = dt.Rows[0]["GradeorPercentage"].ToStringCustom();
            chkAttested.Checked = Convert.ToBoolean(dt.Rows[0]["CertificatesAttested"]);
            chkVerified.Checked = Convert.ToBoolean(dt.Rows[0]["CertificatesVerified"]);
            chkClearanceRequired.Checked = Convert.ToBoolean(dt.Rows[0]["UniversityClearenceRequired"]);
            chkClearanceCompleted.Checked = Convert.ToBoolean(dt.Rows[0]["ClearenceCompleted"]);
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToStringCustom();
            objQualification.QualificationID = Convert.ToInt32(dt.Rows[0]["QualificationID"]);
            objQualification.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"]);

            DataTable dtvisadoc = objQualification.GetQualificationDocuments().Tables[0];

            dlVisaDoc.DataSource = dtvisadoc;
            dlVisaDoc.DataBind();
            ViewState["PasDoc"] = dtvisadoc;

            if (dlVisaDoc.Items.Count > 0)
                divPassDoc.Style["display"] = "block";
        }
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        ddlEmployee.Enabled = rcDegree.Enabled = btnDegree.Enabled = blnTemp;//ddlCompany.Enabled = 
        txtReferenceNumber.Enabled = blnTemp;
        txtFromDate.Enabled = txtToDate.Enabled = txtOtherInfo.Enabled = btnFromDate.Enabled = btnToDate.Enabled = blnTemp;
        txtSchoolOrCollege.Enabled = ddlUniversity.Enabled = btnUniversity.Enabled = blnTemp;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objQualification = new clsQualification();
        CheckBox chkEmployee;
        LinkButton btnEmployee;
        string message = string.Empty;
        bool blnRenewed = false;
        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

        if (dlQualification.Items.Count > 0)
        {
            foreach (DataListItem item in dlQualification.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                if (objQualification.CheckIfDocumentIssued(Convert.ToInt32(dlQualification.DataKeys[item.ItemIndex])))
                {
                    blnRenewed = true;
                }
                else
                {
                    objQualification.QualificationID = Convert.ToInt32(dlQualification.DataKeys[item.ItemIndex]);
                    DataTable dt = objQualification.GetLicenseFilenames();
                    objQualification.Delete();
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                }
            }
            if (!blnRenewed)
                message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString();//"Qualification(s) deleted successfully";
            else
                message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString(); //"Once issued Qualification(s) cannot be deleted";
        }
        else
        {
            if (hfMode.Value == "List")
            {
                if (objQualification.CheckIfDocumentIssued(Convert.ToInt32(fvQualification.DataKey["QualificationID"])))
                {
                    blnRenewed = true;
                }
                else
                {
                    objQualification.QualificationID = Convert.ToInt32(fvQualification.DataKey["QualificationID"]);
                    DataTable dt = objQualification.GetLicenseFilenames();
                    objQualification.Delete();
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                }
                if (!blnRenewed)
                    message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString();//"Qualification(s) deleted successfully";
                else
                    message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();//"Once issued Qualification(s) cannot be deleted";
            }
        }

        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        btnList_Click(sender, e);
        upEmployeeVisa.Update();
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        //divRenew.Style["display"] = "none";
        txtSearch.Text = string.Empty;
        pgrEmployees.CurrentPage = 0;
        fvQualification.Visible = false;
        fvQualification.DataSource = null;
        fvQualification.DataBind();
        EnableMenus();
        Bind();
        hfMode.Value = "List";
        upEmployeeVisa.Update();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnList_Click(sender, e);
        upMenu.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int id = 0;
        try
        {
            objQualification = new clsQualification();
            objAlert = new clsDocumentAlert();

            objQualification.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objQualification.ReferenceNumber = txtReferenceNumber.Text.Trim();
            objQualification.SchoolOrCollege = txtSchoolOrCollege.Text.Trim();
            objQualification.UniversityID = ddlUniversity.SelectedValue.ToInt32();
            objQualification.FromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
            objQualification.ToDate = clsCommon.Convert2DateTime(txtToDate.Text);
            objQualification.DegreeID = Convert.ToInt32(rcDegree.SelectedValue);
            objQualification.Remarks = txtOtherInfo.Text.Trim();
            objQualification.CertificateTitle = txtCertificateTitle.Text.Trim();
            objQualification.GradeorPercentage = txtGrade.Text.Trim();
            objQualification.CertificatesAttested = chkAttested.Checked;
            objQualification.CertificatesVerified = chkVerified.Checked;
            objQualification.UniversityClearenceRequired = chkClearanceRequired.Checked;
            objQualification.ClearenceCompleted = chkClearanceCompleted.Checked;

            if (hfMode.Value == "Insert")
            {
                objQualification.QualificationID = 0;

                if (objQualification.IsReferenceNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("ReferenceNumberexists.Text").ToString());
                    mpeMessage.Show();
                    return;
                }
                else if (objQualification.CheckIfDegreeExists(0, rcDegree.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32()))
                {
                    mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "التفاصيل موجودة بنفس الدرجة لهذا الموظف " : "Same degree details exist for this employee ");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objQualification.BeginEmp();
                        id = objQualification.InsertEmployeeQualification();
                        objQualification.QualificationID = id;
                        //objAlert.AlertMessage(Convert.ToInt32(DocumentType.Qualification), "Qualification", "ReferenceNumber", id, txtReferenceNumber.Text, clsCommon.Convert2DateTime(txtToDate.Text), "Emp", Convert.ToInt32(ddlEmployee.SelectedValue), ddlEmployee.SelectedItem.Text, false);
                        objQualification.Commit();
                    }
                    catch (Exception ex)
                    {
                        objQualification.RollBack();
                    }
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objQualification.QualificationID = Convert.ToInt32(hdQualificationID.Value);

                if (objQualification.IsReferenceNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("ReferenceNumberexists.Text").ToString());
                    mpeMessage.Show();
                    return;
                }
                else if (objQualification.CheckIfDegreeExists(hdQualificationID.Value.ToInt32(), rcDegree.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32()))
                {
                    mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "التفاصيل موجودة بنفس الدرجة لهذا الموظف " : "Same degree details exist for this employee ");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objQualification.BeginEmp();
                        id = objQualification.UpdateEmployeeQualification();
                        objQualification.QualificationID = id;
                        objQualification.Commit();
                    }
                    catch (Exception ex)
                    {
                        objQualification.RollBack();
                    }
                }
            }

            if (id == 0) return;
            //insert into treemaster and treedetails
            Label lblDocname, lblActualfilename, lblFilename;

            foreach (DataListItem item in dlVisaDoc.Items)
            {
                lblDocname = (Label)item.FindControl("lblDocname");
                lblFilename = (Label)item.FindControl("lblFilename");
                lblActualfilename = (Label)item.FindControl("lblActualfilename");
                objQualification.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objQualification.QualificationID = id;
                objQualification.ReferenceNumber = Convert.ToString(txtReferenceNumber.Text.Trim());
                objQualification.Docname = lblDocname.Text.Trim();
                objQualification.Filename = lblFilename.Text.Trim();

                if (dlVisaDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                {
                    objQualification.InsertEmployeeLicenseTreemaster();

                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                    {
                        if (!Directory.Exists(Path))
                            Directory.CreateDirectory(Path);

                        File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                    }
                }
            }

            objQualification.Commit();

            if (hfMode.Value == "Insert")
            {
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString());
            }
            else
            {
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString());
            }

            mpeMessage.Show();
            upEmployeeVisa.Update();
            EnableMenus();
            divIssueReceipt.Style["display"] = "block";
            //divRenew.Style["display"] = "block";
            hfMode.Value = "Edit";
            hdQualificationID.Value = objQualification.QualificationID.ToStringCustom();
            upMenu.Update();
            upEmployeeVisa.Update();
            btnList_Click(sender, e);
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        pgrEmployees.CurrentPage = 0;
        divIssueReceipt.Style["display"] = "none";
        fvQualification.Visible = false;
        fvQualification.DataSource = null;
        fvQualification.DataBind();
        EnableMenus();
        Bind();
        hfMode.Value = "List";
        upEmployeeVisa.Update();
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        NewLicense();
        upEmployeeVisa.Update();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlQualification.Items.Count > 0)
        {
            objQualification = new clsQualification();

            if (fvQualification.DataKey["QualificationID"] == DBNull.Value || fvQualification.DataKey["QualificationID"] == null)
            {
                CheckBox chkEmployee;
                string sQualificationIDs = string.Empty;

                foreach (DataListItem item in dlQualification.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sQualificationIDs += "," + dlQualification.DataKeys[item.ItemIndex];
                }

                sQualificationIDs = sQualificationIDs.Remove(0, 1);

                if (sQualificationIDs.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sQualificationIDs);
                else
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sQualificationIDs));
            }
            else
                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvQualification.DataKey["QualificationID"]));

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }
    }


    public string GetPrintText(int iVisaID)
    {
        objQualification = new clsQualification();
        StringBuilder sb = new StringBuilder();
        DataTable dt = objQualification.GetPrintSingle(iVisaID);


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("QualificationInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("RefNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["ReferenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("University.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["University"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("SchoolCollege.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SchoolOrCollege"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Qualification.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Degree"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("FromPeriod.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["FromDate"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("ToPeriod.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ToDate"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificateTitle.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificateTitle"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("GradePercentage.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["GradeorPercentage"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificatesAttested.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesAttested"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificatesVerified.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesVerified"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("UniversityClearenceRequired.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["UniversityClearenceRequired"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("ClearenceCompleted.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ClearenceCompleted"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public string CreateSelectedEmployeesContent(string sVisaIds)
    {
        objQualification = new clsQualification();

        StringBuilder sb = new StringBuilder();

        DataTable dt = objQualification.GetPrintAll(sVisaIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("QualificationInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("RefNumber.Text").ToString() + "</td>");
        sb.Append("<td width='90px'>" + GetLocalResourceObject("University.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("SchoolCollege.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("Qualification.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("FromPeriod.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("ToPeriod.Text").ToString() + "</td>");
        sb.Append("<td width='90px'>" + GetLocalResourceObject("CertificateTitle.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("GradePercentage.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("CertificatesAttested.Text").ToString() + "</td>");
        sb.Append("<td width='50px'>" + GetLocalResourceObject("CertificatesVerified.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("UniversityClearenceRequired.Text").ToString() + "</td>");
        sb.Append("<td width='50px'>" + GetLocalResourceObject("ClearenceCompleted.Text").ToString() + "</td><td width='200px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='15'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ReferenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["University"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SchoolOrCollege"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Degree"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["FromDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ToDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificateTitle"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["GradeorPercentage"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesAttested"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesVerified"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["UniversityClearenceRequired"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ClearenceCompleted"]) + "</td>");
            sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        if (dlQualification.Items.Count > 0)
        {
            objQualification = new clsQualification();
            string sQualificationIDs = string.Empty;

            if (fvQualification.DataKey["QualificationID"] == DBNull.Value || fvQualification.DataKey["QualificationID"] == null)
            {
                CheckBox chkEmployee;

                foreach (DataListItem item in dlQualification.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sQualificationIDs += "," + dlQualification.DataKeys[item.ItemIndex];
                }

                sQualificationIDs = sQualificationIDs.Remove(0, 1);
            }
            else
                sQualificationIDs = fvQualification.DataKey["QualificationID"].ToString();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Qualification&QualificationID=" + sQualificationIDs + "', 835, 585);", true);
        }
    }

    protected void fuVisa_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuVisa.HasFile)
        {
            actualfilename = fuVisa.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuVisa.FileName);
            Session["Filename"] = filename;
            fuVisa.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }

    protected void btnattachvisadoc_Click(object sender, EventArgs e)
    {
        divPassDoc.Style["display"] = "block";
        DataTable dt;
        DataRow dw;
        objQualification = new clsQualification();
        objQualification.QualificationID = -1;
        objQualification.EmployeeID = -1;

        if (ViewState["PasDoc"] == null)
            dt = objQualification.GetQualificationDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["PasDoc"];
            dt = dtDoc;
        }

        if (hfMode.Value == "Insert" || hfMode.Value == "Edit")
        {
            dw = dt.NewRow();
            dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
            dw["Filename"] = Session["Filename"];
            dt.Rows.Add(dw);
        }

        dlVisaDoc.DataSource = dt;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = dt;
        txtDocname.Text = string.Empty;
        updldlPassportDoc.Update();
    }

    protected void dlVisaDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objQualification = new clsQualification();

                if (e.CommandArgument != string.Empty)
                {
                    objQualification.Node = Convert.ToInt32(e.CommandArgument);
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objQualification.DeleteLicenseDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }

                objQualification.QualificationID = -1;
                objQualification.EmployeeID = -1;
                DataTable dt = objQualification.GetQualificationDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlVisaDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    dw["Node"] = dlVisaDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlVisaDoc.DataSource = dt;
                dlVisaDoc.DataBind();
                ViewState["PasDoc"] = dt;
                break;
        }
    }

    protected void btnDegree_Click(object sender, EventArgs e)
    {
        DegreeReference1.SelectedValue = rcDegree.SelectedValue;
        DegreeReference1.FillList();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnUniversity_Click(object sender, EventArgs e)
    {
        rnUniversity.ClearAll();
        rnUniversity.TableName = "UniversityReference";
        rnUniversity.DataTextField = "Description";
        rnUniversity.DataValueField = "UniversityID";
        rnUniversity.FunctionName = "FillUniversity";
        rnUniversity.SelectedValue = ddlUniversity.SelectedValue;
        rnUniversity.DisplayName = GetLocalResourceObject("University.Text").ToString();
        rnUniversity.DataTextFieldArabic = "DescriptionArb";
        rnUniversity.PopulateData();
        mpeLicenceType.Show();
        upnlPopup.Update();
    }


    void rnUniversity_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }


    protected void dlQualification_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }
}
