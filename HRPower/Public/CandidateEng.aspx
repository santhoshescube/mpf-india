﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="CandidateEng.aspx.cs" Inherits="Public_CandidateEng" Title="Candidates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">

<style type="text/css">
select
{
	margin-left:5px;
}
</style>
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li class='selected'><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
                   <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal95" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <style type="text/css">
        .background
        {
            background-color: Transparent;
            filter: alpha(opacity=10);
            opacity: 0.1;
        }
        .updateprogress
        {
            background-position: center center;
            position: absolute;
            width: 31px;
            height: 31px;
            background-image: url(        '../images/loading.gif' );
            background-repeat: no-repeat;
            display: none;
        }
    </style>

    <script type="text/javascript">
        function WidthPreview(source, args, width)  
        {
            var imgPreview = document.getElementById('ctl00_public_content_imgPreview');
            
            imgPreview.style.display = "block";
            imgPreview.src = "../thumbnail.aspx?FromDB=false&isCandidate=true&folder=Temp&file=" + '<%= Session.SessionID %>' + ".jpg&width=" + width + "&t=" + new Date();
        }
   var _updateprogress, _background;        
        function pageLoad(sender, args)
        {
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            if(args._isPartialLoad == false)
            {
                _updateprogress = $get('updateprogress');
                
                _background = document.createElement('div');
                _background.style.display = 'none';
                _background.style.zIndex = 10000;
                _background.className = 'background';
                document.getElementById('ctl00_public_content_container').appendChild(_background);
            }
        }

        function beginRequest(sender, args)
        {
            _updateprogress.style.display = 'block';
            _background.style.display = 'block';

            var bounds = Sys.UI.DomElement.getBounds(document.getElementById('ctl00_public_content_container'));
            var progressbounds = Sys.UI.DomElement.getBounds(_updateprogress);

            var x = bounds.x + Math.round(bounds.width / 2) - Math.round(progressbounds.width / 2);
            var y = bounds.y + Math.round(bounds.height / 2) - Math.round(progressbounds.height / 2);

            _background.style.width = bounds.width + "px";
            _background.style.height = bounds.height + "px";

            Sys.UI.DomElement.setLocation(_updateprogress, x, y);            
            Sys.UI.DomElement.setLocation(_background, bounds.x, bounds.y);
        }

        function endRequest(sender, args)
        {
            _updateprogress.style.display = 'none';
            _background.style.display = 'none';
        }
        
    </script>

    <div id="updateprogress" class="updateprogress">
        &nbsp;
    </div>
    <div>
        <div style="display: none">
            <asp:Button ID="btnSubmitMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:HiddenField ID="hfCandidateID" runat="server" />
    <asp:UpdatePanel ID="upfvCandidate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="container" runat="server">
                <div id="divCandidate" class="labeltext" runat="server">
                     <%--   <div>
                    <asp:Label ID="lbl" runat="server" CssClass="error"></asp:Label>
                    </div>--%>
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="100%">
                                <div id="div1" class="TabbedPanels" style="width: 100%; padding: 0px;">
                                    <ul class="TabbedPanelsTabGroup">
                                        <li id="pnlTab1" class="TabbedPanelsTabSelected" onclick="SetCandidateTab(this.id)">
                                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="BasicInfo"></asp:Literal>
                                        </li>
                                        <li id="pnlTab2" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">
                                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ProfessionalInfo"></asp:Literal>
                                        </li>
                                        <li id="pnlTab3" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">
                                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="DocumentInfo"></asp:Literal>
                                        </li>
                                        <li id="pnlTab4" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="OtherInfo"></asp:Literal>
                                        </li>
                                    </ul>
                                    <div class="TabbedPanelsContentGroup">
                                        <div id="divTab1" class="TabbedPanelsContent" style="display: block; height: 500px;">
                                            <div style="width: 100%">
                                                <div style="float: left; width: 94%;">
                                                    <div style="float: left; width: 45%;">
                                                        <div class="trLeft" style="float: left; width: 36.5%; height: 29px">
                                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="JobName"></asp:Literal>
                                                        </div>
                                                        <div class="trRight" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                                            <asp:UpdatePanel ID="updJob" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="ddlJob" runat="server" Width="90%">
                                                                    </asp:DropDownList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <%--  <asp:RequiredFieldValidator ID="rfvJob" runat="server" ControlToValidate="ddlJob"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"
                                                            InitialValue="-1"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="trRight" style="float: left; width: 4%; height: 29px; padding-bottom: 5px;">
                                                        </div>
                                                        <div class="trLeft" style="float: left; width: 36.5%; height: 29px; display: none">
                                                            <asp:Literal ID="Literal5" runat="server">Candidate Code</asp:Literal>
                                                           <%-- <font color="Red">*</font>--%>
                                                        </div>
                                                        <div class="trRight" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;
                                                            display: none">
                                                            <asp:TextBox ID="txtCandidateCode" runat="server" Enabled="false" MaxLength="25"
                                                                Width="85%" BackColor="Info"></asp:TextBox>
                                                        </div>
                                                        <div class="trLeft" style="float: left; width: 36.5%; height: 29px">
                                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Salutation"></asp:Literal>
                                                           <%-- <font color="Red">*</font>--%>
                                                        </div>
                                                        <div class="trRight" style="float: left; width: 230px; height: 29px">
                                                            <asp:UpdatePanel ID="updSalutation" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                                <ContentTemplate>
                                                                    <asp:DropDownList ID="rcSalutation" runat="server" CssClass="dropdownlist" Width="75%"
                                                                        BackColor="Info">
                                                                    </asp:DropDownList>
                                                                    <asp:Button ID="btnSalutation" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                        Text="..." OnClick="btnSalutation_Click" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <asp:RequiredFieldValidator ID="rfvSalutation" runat="server" ControlToValidate="rcSalutation"
                                                                ErrorMessage="*" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div style="float: left; width: 45%;">
                                                        <div class="trLeft" style="height: auto">
                                                            <div class="trLeft" style="float: left; width: 24%">
                                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Photo"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 73%; margin: 0px; padding-bottom: 5px;">
                                                                <AjaxControlToolkit:AsyncFileUpload ID="fuRecentPhoto" runat="server" CompleteBackColor="White"
                                                                    OnClientUploadComplete="ShowCandidatePreview" OnUploadedComplete="fuRecentPhoto_UploadedComplete"
                                                                    PersistFile="true" />
                                                                <asp:LinkButton ID="btnClearRecentPhoto" runat="server" CausesValidation="False"
                                                                    CssClass="linkbutton" OnClick="btnClearRecentPhoto_Click" Style="display: none;">Clear</asp:LinkButton>
                                                                <div id="divImage" runat="server" style="display: none;">
                                                                    <img id="imgPreview" runat="server" alt="" src="" />
                                                                </div>
                                                                <asp:LinkButton ID="btnRemoveRecentPhoto" runat="server" CausesValidation="False"
                                                                    CssClass="linkbutton" OnClick="btnRemoveRecentPhoto_Click" Style="display: none;"
                                                                    meta:resourcekey="Remove"></asp:LinkButton>
                                                            </div>
                                                            <div style="clear: both">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div style="float: left; width: 48%; color: #1e8abe; padding-left: 10px;">
                                                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="NameInEnglish"></asp:Literal>
                                                    </div>
                                                    <div style="float: left; width: 48%; color: #1e8abe; padding-left: 13px;">
                                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="NameinArabic"></asp:Literal>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Firstname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:RequiredFieldValidator ID="rfvEnglishFirstName" runat="server" ControlToValidate="txtEnglishFirstName"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtEnglishFirstName" runat="server" MaxLength="25" Width="82%" BackColor="Info"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Thirdname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtArabicThirdName" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead" style="padding-bottom: 5px">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Secondname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtEnglishSecondName" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%" BackColor="Info">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEnglishSecondName"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Secondname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtArabicSecondName" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 98%; border-bottom: 1px dashed #cdcece; padding-bottom: 8px;">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Thirdname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtEnglishThirdName"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtEnglishThirdName" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%" BackColor="Info"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Firstname"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:RequiredFieldValidator ID="rfvArbFirstName" runat="server" ControlToValidate="txtArabicFirstName"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False">
                                                        </asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtArabicFirstName" runat="server" MaxLength="25" Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Citizenship"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtCitizenship" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal18" runat="server" meta:resourcekey="PlaceofBirth"></asp:Literal>
                                                       <%-- <font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPlaceOfBirth" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%" BackColor="Info"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvPlaceofBirth" runat="server" ControlToValidate="txtPlaceOfBirth"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Gender"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Selected="True" Value="0" meta:resourcekey="Male"></asp:ListItem>
                                                            <asp:ListItem Value="1" meta:resourcekey="Female"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="DateofBirth"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtDateofBirth" runat="server" MaxLength="10" Width="55%" BackColor="Info"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnDateofBirth" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnDateofBirth" TargetControlID="txtDateofBirth" />
                                                        <asp:RequiredFieldValidator ID="RfvDOB" runat="server" ControlToValidate="txtDateofBirth"
                                                            Display="Dynamic" ErrorMessage="*" ForeColor="Red" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                        <font color="Red"></font>
                                                        <br />
                                                    </div>
                                                </div>
                                                <div style="height: auto">
                                                    <div style="width: 50%; float: right">
                                                        <font color="Red">
                                                            <asp:CustomValidator ID="CustomValidator9" runat="server" ClientValidationFunction="validateDateofBirth"
                                                                ControlToValidate="txtDateofBirth" Display="Dynamic" ErrorMessage="Invalid Age"
                                                                ForeColor="" ValidateEmptyText="True" ValidationGroup="submit"></asp:CustomValidator>
                                                        </font>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="MaritalStatus"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updMaritalStatus" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlMaritalStatus" runat="server" Width="87%" BackColor="Info">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="rfvMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="NumberofSon"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtNoOfSon" runat="server" CssClass="textbox" MaxLength="2" Width="82%"></asp:TextBox>
                                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteSon" runat="server" TargetControlID="txtNoOfSon"
                                                            FilterType="Numbers" FilterMode="ValidChars">
                                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Religion"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updReligion" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="rcReligion" runat="server" Width="75%" BackColor="Info">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnReligion" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnReligion_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="rfvReligion" runat="server" ControlToValidate="rcReligion"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal24" runat="server" meta:resourcekey="MotherName"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtMotherName" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal25" runat="server" meta:resourcekey="CurrentNationality"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updCurrentNationality" runat="server" UpdateMode="Conditional"
                                                            RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlCurrentNationality" runat="server" Width="75%" BackColor="Info">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnCurrentNationality" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnCurrentNationality_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCurrentNationality"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"
                                                            InitialValue="-1">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal26" runat="server" meta:resourcekey="PreviousNationality"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updPreviousNationality" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlPreviousNationality" runat="server" Width="75%">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnPreviousNationality" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnPreviousNationality_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Doctrine"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtDoctrine" runat="server" CssClass="textbox" MaxLength="25" Width="83%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Telephone"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtCurrentTelephoneNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                            Width="83%" BackColor="Info"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvCurrentTelephoneNumber" runat="server" ControlToValidate="txtCurrentTelephoneNumber"
                                                            ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal29" runat="server" meta:resourcekey="Address"></asp:Literal>
                                                       <%-- <font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright" style="float: left; width: 225px; height: 60px; padding-bottom: 5px;">
                                                        <asp:TextBox ID="txtCurrentAddress" runat="server" MaxLength="250" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLengthCandidate(this, 250);"
                                                            TextMode="MultiLine" Width="85%" BackColor="Info"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvCurrentAddress" runat="server" ControlToValidate="txtCurrentAddress"
                                                            ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal30" runat="server" meta:resourcekey="POBox"></asp:Literal>
                                                    </div>
                                                    <div class="divright"style="padding-bottom: 11px;">
                                                        <asp:TextBox ID="txtCurrentPOBox" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="83%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal31" runat="server" meta:resourcekey="Country"></asp:Literal>
                                                       <%-- <font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="rcCountry" runat="server" Width="75%" BackColor="Info">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnCountry_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="rcCountry"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divhead">
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal32" runat="server" meta:resourcekey="PlaceofBusinessPair"></asp:Literal>
                                                </div>
                                                <div class="divright">
                                                    <asp:TextBox ID="txtPlaceofBusinessPair" runat="server" CssClass="textbox" MaxLength="25"
                                                        Width="83%"></asp:TextBox>
                                                </div>
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal33" runat="server" meta:resourcekey="CompanyType"></asp:Literal>
                                                </div>
                                                <div class="divright">
                                                    <asp:UpdatePanel ID="updCompanyType" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlCompanyType" runat="server" Width="75%">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnCompanyType" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                Text="..." OnClick="btnCompanyType_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                            <div class="divhead">
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal34" runat="server" meta:resourcekey="Address"></asp:Literal>
                                                </div>
                                                <div class="divright">
                                                    <asp:TextBox ID="txtBasicBusinessPairAddress" runat="server" MaxLength="250" Height="100%"
                                                        onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLengthCandidate(this, 250);"
                                                        TextMode="MultiLine" Width="85%">
                                                    </asp:TextBox>
                                                </div>
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal35" runat="server" meta:resourcekey="Email"></asp:Literal>
                                                    <%--<font color="Red">*</font>--%>
                                                </div>
                                                <div class="divright">
                                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" MaxLength="30" Width="84%"
                                                        BackColor="Info"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvEmailID" runat="server" ControlToValidate="txtEmail"
                                                        ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revEmailpnlTab1" runat="server" ControlToValidate="txtEmail"
                                                        CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterValidEmail %>'
                                                        SetFocusOnError="False" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                        ValidationGroup="submit"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="divhead">
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal36" runat="server" meta:resourcekey="ReferralType"></asp:Literal>
                                                </div>
                                                <div class="divright" style="margin-top:5px;">
                                                    <asp:UpdatePanel ID="updReferralType" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlReferraltype" runat="server" CssClass="dropdownlist" Width="77%"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlReferraltype_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnReferralType" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                Text="..." OnClick="btnReferralType_Click" Visible="false" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="divleft">
                                                    <asp:Literal ID="Literal37" runat="server" meta:resourcekey="ReferredBy"></asp:Literal>
                                                </div>
                                                <div class="divright">
                                                    <asp:UpdatePanel ID="updReferredBy" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlReferredBy" runat="server" CssClass="dropdownlist" Width="77%">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnReferredBy" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                Text="..." OnClick="btnReferredBy_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab2" class="TabbedPanelsContent" style="display: none; height: 500px;">
                                            <div style="width: 100%">
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal38" runat="server" meta:resourcekey="Qualification"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="uprcDegree" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="rcDegree" runat="server" Width="77%" AutoPostBack="true" ValidationGroup="submit"
                                                                    BackColor="Info">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnDegree" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnDegree_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="rfvQualificationpnlTab2" runat="server" ControlToValidate="rcDegree"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal39" runat="server" meta:resourcekey="Category"></asp:Literal><%--<font
                                                            color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updQualificationCategory" runat="server" UpdateMode="Conditional"
                                                            RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlQualificationCategory" runat="server" Width="74%" BackColor="Info">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnQualificationCategory" runat="server" CausesValidation="False"
                                                                    CssClass="referencebutton" Text="..." OnClick="btnQualificationCategory_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                        <asp:RequiredFieldValidator ID="rfvQualificationCategorypnlTab2" runat="server" ControlToValidate="ddlQualificationCategory"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <%--<div class="divleft">
                                                        Graduation
                                                    </div>
                                                    <div class="divright" >
                                                        <asp:TextBox ID="txtGraduation" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="85%"></asp:TextBox>
                                                    </div>--%>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal40" runat="server" meta:resourcekey="GraduationYear"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:DropDownList ID="ddlDegreeCompletionYear" runat="server" CssClass="dropdownlist"
                                                            Width="45%">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal41" runat="server" meta:resourcekey="PlaceofGraduation"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPlaceofGraduation" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal42" runat="server" meta:resourcekey="College"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtCollegeSchool" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal43" runat="server" meta:resourcekey="PreviousJob"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPreviousJob" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal44" runat="server" meta:resourcekey="Sector"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtSector" runat="server" CssClass="textbox" MaxLength="25" Width="85%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal45" runat="server" meta:resourcekey="Position"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPosition" runat="server" CssClass="textbox" MaxLength="25" Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal46" runat="server" meta:resourcekey="TypeofExperience"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtTypeofExperience" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal47" runat="server" meta:resourcekey="YearofExperience"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <table border="0" cellpadding="2" cellspacing="5">
                                                            <tr>
                                                                <td valign="top">
                                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList ID="ddlYear" runat="server" CssClass="dropdownlist" Width="45px">
                                                                                <asp:ListItem Value="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6"></asp:ListItem>
                                                                                <asp:ListItem Value="7"></asp:ListItem>
                                                                                <asp:ListItem Value="8"></asp:ListItem>
                                                                                <asp:ListItem Value="9"></asp:ListItem>
                                                                                <asp:ListItem Value="10"></asp:ListItem>
                                                                                <asp:ListItem Value="11"></asp:ListItem>
                                                                                <asp:ListItem Value="12"></asp:ListItem>
                                                                                <asp:ListItem Value="13"></asp:ListItem>
                                                                                <asp:ListItem Value="14"></asp:ListItem>
                                                                                <asp:ListItem Value="15"></asp:ListItem>
                                                                                <asp:ListItem Value="16"></asp:ListItem>
                                                                                <asp:ListItem Value="17"></asp:ListItem>
                                                                                <asp:ListItem Value="18"></asp:ListItem>
                                                                                <asp:ListItem Value="19"></asp:ListItem>
                                                                                <asp:ListItem Value="20"></asp:ListItem>
                                                                                <asp:ListItem Value="21"></asp:ListItem>
                                                                                <asp:ListItem Value="22"></asp:ListItem>
                                                                                <asp:ListItem Value="23"></asp:ListItem>
                                                                                <asp:ListItem Value="24"></asp:ListItem>
                                                                                <asp:ListItem Value="25"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            &nbsp
                                                                            <asp:Literal ID="Literal48" runat="server" meta:resourcekey="Years"></asp:Literal>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                                <td valign="top">
                                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="dropdownlist" Width="50px">
                                                                                <asp:ListItem Value="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6"></asp:ListItem>
                                                                                <asp:ListItem Value="7"></asp:ListItem>
                                                                                <asp:ListItem Value="8"></asp:ListItem>
                                                                                <asp:ListItem Value="9"></asp:ListItem>
                                                                                <asp:ListItem Value="10"></asp:ListItem>
                                                                                <asp:ListItem Value="11"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            &nbsp
                                                                            <asp:Literal ID="Literal49" runat="server" meta:resourcekey="Months"></asp:Literal>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal50" runat="server" meta:resourcekey="Compensation"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtCompensation" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal51" runat="server" meta:resourcekey="Specialization"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtSpecialization" runat="server" CssClass="textbox" MaxLength="25"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal52" runat="server" meta:resourcekey="DateofTransaction"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtDateofTransactionEntry" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnDateofTransactionEntry" runat="server" CausesValidation="False"
                                                            ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnDateofTransactionEntry" TargetControlID="txtDateofTransactionEntry" />
                                                        <asp:CustomValidator ID="CustomValidator7" runat="server" ClientValidationFunction="ValidateDate"
                                                            ControlToValidate="txtDateofTransactionEntry" CssClass="error" Display="Dynamic"
                                                            SetFocusOnError="False" ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal53" runat="server" meta:resourcekey="ExpectedJoinDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtExpectedJoinDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnExpectedJoinDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnExpectedJoinDate" TargetControlID="txtExpectedJoinDate" />
                                                        <asp:CustomValidator ID="CustomValidator6" runat="server" ClientValidationFunction="ValidateDate"
                                                            ControlToValidate="txtExpectedJoinDate" CssClass="error" Display="Dynamic" SetFocusOnError="False"
                                                            ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead" style="height: 50px; padding-top: 30px;">
                                                    <div class="divleft" style="color: #1e8abe;">
                                                        <asp:Literal ID="Literal54" runat="server" meta:resourcekey="Skills"></asp:Literal>
                                                    </div>
                                                    <div class="divright" style="width: 500px;">
                                                        <asp:TextBox ID="txtSkills" runat="server" TextMode="MultiLine" Width="85%" onchange="RestrictMulilineLength(this, 500);"
                                                            onkeyup="RestrictMulilineLengthCandidate(this, 500);"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div style="float: left; width: 98%; padding-top: 20px;">
                                                    <div style="float: left; width: 100%;">
                                                        <div class="divleft" style="color: #1e8abe;">
                                                            <asp:Literal ID="Literal55" runat="server" meta:resourcekey="Languages"></asp:Literal>
                                                        </div>
                                                        <div class="divleft" style="float: left; width: 225px; height: 29px; padding-top: 10px;">
                                                            <asp:Literal ID="Literal56" runat="server" meta:resourcekey="Arabic"></asp:Literal>
                                                        </div>
                                                        <div class="divright" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                                            <asp:CheckBoxList ID="chlArabic" runat="server" RepeatColumns="2">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    <div style="float: left; width: 100%;">
                                                        <div class="divleft" style="float: left; width: 16%; height: 29px">
                                                            &nbsp
                                                        </div>
                                                        <div class="divleft" style="float: left; width: 225px; height: 29px; padding-top: 12px;">
                                                            <asp:Literal ID="Literal57" runat="server" meta:resourcekey="English"></asp:Literal>
                                                        </div>
                                                        <div class="divright" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                                            <asp:CheckBoxList ID="chlEnglish" runat="server" RepeatColumns="2">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    <div style="float: left; width: 100%;">
                                                        <div class="divleft" style="float: left; width: 16%; height: 29px">
                                                            &nbsp
                                                        </div>
                                                        <div class="divright" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;">
                                                            <asp:TextBox ID="txtOtherLanguages" runat="server" CssClass="textbox" MaxLength="25"
                                                                Width="82%"></asp:TextBox>
                                                        </div>
                                                        <div class="divright" style="float: left; width: 225px; height: 29px; padding-bottom: 5px;
                                                            padding-left: 7px">
                                                            <asp:CheckBoxList ID="chlOtherLanguages" runat="server" RepeatColumns="2">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab3" class="TabbedPanelsContent" style="display: none; height: 500px;">
                                            <div style="width: 100%">
                                                <div class="divhead">
                                                    <div style="float: left; width: 43%; color: #1e8abe;">
                                                        <asp:Literal ID="Literal58" runat="server" meta:resourcekey="Passport"></asp:Literal>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal59" runat="server" meta:resourcekey="Number"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPassportNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal60" runat="server" meta:resourcekey="Country"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updPassportCountry" RenderMode="Inline" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlPassportCountry" runat="server" Width="77%">
                                                                    <%--BackColor="Info"--%>
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnPassportCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnPassportCountry_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal61" runat="server" meta:resourcekey="PlaceofIssue"></asp:Literal>
                                                        <%--<font color="Red">*</font>--%>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPlaceofPassportIssue" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal62" runat="server" meta:resourcekey="ExpiryDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPassportExpiryDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp<%-- BackColor="Info"--%>
                                                        <asp:ImageButton ID="btnPassportExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnPassportExpiryDate" TargetControlID="txtPassportExpiryDate" />
                                                        <asp:CustomValidator ID="CustomValidator5" runat="server" ClientValidationFunction="ValidateCandidatepassportExpirydate"
                                                            ControlToValidate="txtPassportExpiryDate" CssClass="error" Display="Dynamic"
                                                            SetFocusOnError="False" ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal63" runat="server" meta:resourcekey="IssueDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPassportReleaseDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp<%--BackColor="Info"--%>
                                                        <asp:ImageButton ID="btnPassportReleaseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnPassportReleaseDate" TargetControlID="txtPassportReleaseDate" />
                                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ControlToValidate="txtPassportReleaseDate"
                                                            CssClass="error" Display="Dynamic" ClientValidationFunction="validatepassportIssuedate"
                                                            SetFocusOnError="False" ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div style="float: left; width: 43%; color: #1e8abe;">
                                                        <asp:Literal ID="Literal64" runat="server" meta:resourcekey="Visa"></asp:Literal>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal65" runat="server" meta:resourcekey="Number"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtVisaNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal66" runat="server" meta:resourcekey="UnifiedNumber"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtUnifiedNumber" runat="server" CssClass="textbox" MaxLength="28"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal67" runat="server" meta:resourcekey="Country"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:UpdatePanel ID="updVisaCountry" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlVisaCountry" runat="server" Width="77%">
                                                                </asp:DropDownList>
                                                                <asp:Button ID="btnVisaCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    Text="..." OnClick="btnVisaCountry_Click" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal68" runat="server" meta:resourcekey="PlaceofIssue"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtVisaPlaceofIssue" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal69" runat="server" meta:resourcekey="ExpiryDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtVisaExpiryDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnVisaExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnVisaExpiryDate" TargetControlID="txtVisaExpiryDate" />
                                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ClientValidationFunction="ValidateCandidatevisaExpirydate"
                                                            ControlToValidate="txtVisaExpiryDate" CssClass="error" Display="Dynamic" c SetFocusOnError="False"
                                                            ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal70" runat="server" meta:resourcekey="IssueDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtVisaReleaseDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnVisaReleaseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnVisaReleaseDate" TargetControlID="txtVisaReleaseDate" />
                                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="validatevisaIssuedate"
                                                            ControlToValidate="txtVisaReleaseDate" CssClass="error" Display="Dynamic" SetFocusOnError="False"
                                                            ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div style="float: left; width: 43%; color: #1e8abe;">
                                                        <asp:Literal ID="Literal71" runat="server" meta:resourcekey="NationalCard"></asp:Literal>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal72" runat="server" meta:resourcekey="Number"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtNationalCardNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal73" runat="server" meta:resourcekey="CardNumber"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal74" runat="server" meta:resourcekey="ExpiryDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtNationalCardExpiryDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnNationalCardExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnNationalCardExpiryDate" TargetControlID="txtNationalCardExpiryDate" />
                                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ValidateDate"
                                                            ControlToValidate="txtNationalCardExpiryDate" CssClass="error" Display="Dynamic"
                                                            SetFocusOnError="False" ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                                <div style="clear: both">
                                                </div>
                                                <div style="float: left; width: 43%; color: #1e8abe;">
                                                    <asp:Literal ID="Literal75" runat="server" meta:resourcekey="AttachDocuments"></asp:Literal>
                                                </div>
                                                <div style="clear: both">
                                                </div>
                                                <div style="width: 100%; height: 150px;">
                                                    <asp:UpdatePanel ID="upCandidateDocument" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <div style="float: left; width: 17%">
                                                                <asp:Label ID="lblDocumentCaption" runat="server" meta:resourcekey="DocumentName"></asp:Label>
                                                            </div>
                                                            <div style="float: left; width: 70%">
                                                                <asp:TextBox ID="txtCandidateDocumentName" runat="server" CssClass="textbox" MaxLength="30"
                                                                    Width="150px" Style="margin-right: 20px;"></asp:TextBox>
                                                                <AjaxControlToolkit:AsyncFileUpload ID="fuCandidate" runat="server" ErrorBackColor="White"
                                                                    OnUploadedComplete="fuCandidate_UploadedComplete" PersistFile="True" CompleteBackColor="Green"
                                                                    Visible="true" Width="250px" />
                                                                <asp:LinkButton ID="LinkButton1" runat="server" Enabled="true" OnClick="btnAttachCandidateDocument_Click"
                                                                    ValidationGroup="AttachedDocument" meta:resourcekey="Addtolist" ForeColor="#33a3d5">
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div style="float: left; width: 70%">
                                                                <asp:RequiredFieldValidator ID="rfvDocumentName" Text="*" ValidationGroup="AttachedDocument"
                                                                    runat="server" ControlToValidate="txtCandidateDocumentName">
                                        
                                                                </asp:RequiredFieldValidator>
                                                            </div>
                                                            <div style="clear: both">
                                                            </div>
                                                            <div style="width: 70%; height: 200px; overflow: auto" id="dvAttachedDocuments" runat="server">
                                                                <asp:DataList ID="dlCandidateDocument" runat="server" GridLines="Horizontal" OnItemCommand="dlCandidateDocument_ItemCommand"
                                                                    Width="90%" BorderColor="#33a3d5" 
                                                                    onitemdatabound="dlCandidateDocument_ItemDataBound">
                                                                    <HeaderStyle CssClass="datalistheader" />
                                                                    <HeaderTemplate>
                                                                        <div style="float: left; width: 100%;">
                                                                            <div style="float: left; width: 20%;" class="trLeft">
                                                                                <asp:Literal ID="Literal75" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                                                                            </div>
                                                                            <div style="float: left; width: 40%;" class="trLeft">
                                                                                <asp:Literal ID="Literal76" runat="server" meta:resourcekey="FileName"></asp:Literal>
                                                                            </div>
                                                                            <div style="float: left; width: 18%;" class="trRight">
                                                                                <asp:Literal ID="Literal77" runat="server" meta:resourcekey="Image" ></asp:Literal>
                                                                            </div>
                                                                            <div style="float: left; width: 02%;" class="trRight">
                                                                            </div>
                                                                        </div>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <div style="float: left; width: 100%;">
                                                                        <asp:HiddenField ID="hfIsResume" runat ="server" Value ='<%#Eval("Type") %>' />
                                                                            <div style="float: left; width: 20%; word-break: break-all" class="trLeft">
                                                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                                                    Width="120px" Style="word-break: break-all"></asp:Label>
                                                                            </div>
                                                                            <div style="float: left; width: 40%;" class="trLeft">
                                                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="200px"></asp:Label>
                                                                            </div>
                                                                            <div style="float:LEFT; width: 18%;" class="trRight">
                                                                               <asp:ImageButton ID="img"  Width="30px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                     CommandArgument='<%# Eval("Location") %>' OnClick="img_Click" CausesValidation="false" CommandName="imgView"/>
                                                                            </div>
                                                                            <div style="float: left; width: 2%;" class="trRight">
                                                                                <asp:ImageButton ID="imgDelete" ImageUrl="~/images/Delete18x18.png" runat="server"
                                                                                    CommandArgument='<%# Eval("FileName") %>' OnClick="imgDelete_Click" CausesValidation="false"  />
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab4" class="TabbedPanelsContent" style="display: none; height: 500px;">
                                            <div style="width: 100%">
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal76" runat="server" meta:resourcekey="PermanentAddress"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPermenantAddress" runat="server" MaxLength="250" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLengthCandidate(this, 250);"
                                                            TextMode="MultiLine" Width="89%">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal78" runat="server" meta:resourcekey="HomeStreet"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtHomeStreet" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal79" runat="server" meta:resourcekey="City"></asp:Literal>
                                                    </div>
                                                    <div class="divright" style="margin-top: 10px;">
                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="textbox" MaxLength="28" Width="87%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal80" runat="server" meta:resourcekey="HomeTelephone"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtHomeTelephone" runat="server" CssClass="textbox" MaxLength="20"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal81" runat="server" meta:resourcekey="BusinessAddress"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtBusinessAddress" runat="server" MaxLength="250" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLengthCandidate(this, 250);"
                                                            TextMode="MultiLine" Width="89%">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal82" runat="server" meta:resourcekey="BusinessStreet"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtBusinessStreet" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal83" runat="server" meta:resourcekey="Area"></asp:Literal>
                                                    </div>
                                                    <div class="divright" style="margin-top: 10px;">
                                                        <asp:TextBox ID="txtArea" runat="server" CssClass="textbox" MaxLength="28" Width="87%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal84" runat="server" meta:resourcekey="POBox"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPermenantPOBox" runat="server" CssClass="textbox" MaxLength="20"
                                                            Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal85" runat="server" meta:resourcekey="Telephone"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="textbox" MaxLength="20"
                                                            Width="87%"></asp:TextBox>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal86" runat="server" meta:resourcekey="Fax"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtFax" runat="server" CssClass="textbox" MaxLength="28" Width="85%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal87" runat="server" meta:resourcekey="MobileNumber"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="textbox" MaxLength="50"
                                                            Width="87%"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="divhead">
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal88" runat="server" meta:resourcekey="SignUpDate"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        <asp:TextBox ID="txtmyDate" runat="server" MaxLength="10" Width="55%"></asp:TextBox>&nbsp
                                                        <asp:ImageButton ID="btnSUDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy"
                                                            PopupButtonID="btnSUDate" TargetControlID="txtmyDate" />
                                                        <asp:CustomValidator ID="cvtxtSUDate" runat="server" ClientValidationFunction="ValidateDate"
                                                            ControlToValidate="txtmyDate" CssClass="error" Display="Dynamic" SetFocusOnError="False"
                                                            ValidationGroup="submit"></asp:CustomValidator>
                                                    </div>
                                                    <div class="divleft">
                                                        <asp:Literal ID="Literal89" runat="server" meta:resourcekey="InsuranceNumber"></asp:Literal>
                                                    </div>
                                                    <div class="divright">
                                                        &nbsp;
                                                        <asp:TextBox ID="txtInsuranceNumber" runat="server" CssClass="textbox" MaxLength="28"
                                                            Width="82%"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellpadding="5" cellspacing="0" width="96%">
                        <tr align="right">
                            <td class="trRight" style="text-align: right">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" OnClientClick="return setTabSelected('submit','divTab1');"
                                    ValidationGroup="submit" Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                    OnClick="btnSubmit_Click" Width="75px" />
                                <asp:Button ID="btnCancelSubmit" runat="server" OnClick="btnCancelSubmit_Click" CssClass="btnsubmit"
                                    Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                    CausesValidation="false" Width="75px" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <center>
                <asp:Label ID="lblCandidates" runat="server" CssClass="error" Style="display: none;"
                    Width="100%"></asp:Label>
            </center>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn2" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn3" runat="server" />
            </div>
            <asp:UpdatePanel ID="upDegreeReference" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none;">
                        <input type="button" id="btnDegreeReference" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeDegreeReference" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlDegreeReference" TargetControlID="btnDegreeReference">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlDegreeReference" runat="server" Style="display: none;">
                        <uc:DegreeReference ID="ucDegreeReference" runat="server" ModalPopupID="mpeDegreeReference" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updAgency" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button2" runat="server" />
            </div>
            <div id="divModalPopoUpAgency" runat="server" style="display: none;">
                <uc:AgencyReference ID="AgencyReference" runat="server" ModalPopupID="ModalPopoUpAgency">
                </uc:AgencyReference>
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="ModalPopoUpAgency" runat="server" TargetControlID="Button2"
                PopupControlID="divModalPopoUpAgency" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddCandidate" ImageUrl="~/images/Add candidatesBig.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddCandidate" runat="server" OnClick="lnkAddCandidate_Click"
                            CausesValidation="false" meta:resourcekey="AddCandidate"></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListCandidate" ImageUrl="~/images/Candidate_Details.png"
                        CausesValidation="false" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkListCandidate" runat="server" OnClick="lnkListCandidate_Click"
                            CausesValidation="false" meta:resourcekey="ListCandidate"></asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
