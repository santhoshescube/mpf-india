﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="EmployeeDutyRoaster.aspx.cs" Inherits="Public_EmployeeDutyRoaster" Title="Duty Roaster" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #rightsidebar
        {
        	display: none!important;
        }
        #forms
        {
        	width: 969px!important;
        }
      
        .col {
        	height: 30px;
        	padding :5px 10px;
        	float:left;
        }
        .lbl {
        	line-height: 25px;
        	font-size: 14px;
        	height: 30px;
        	padding :5px 10px;
        	float:left;
        	color: #333;
        } 
        select {
          border: 1px solid #0FABEC;
          border-radius: 3px 3px 3px 3px;
          box-shadow: 1px 1px 5px #CCCCCC;
          float: left;
          font-family: Verdana;
          font-size: 12px;
          font-weight: 200;
          height: 20px;
          padding: 0;
          width: 150px;
          margin-bottom: 0px;
          margin-right: 0px;
}        
         .date {
        
          height:20px!important;
       
         }
         .header-menu{ 	
  	      width:170px;
  	      float:left;
                     }  
          .grid-frame
          {
          	width: 950px;
          	overflow: auto;
          	padding: 10px;
          	height: 700px;
          }  
        
        
        .modal-Loading
{
position:fixed;
z-index:999;
height:100%;
top=0;
width:100%;
backgrond-color:Black;
<%--filter:alpha(opacity=60);--%>
opacity:0.6;
-moz-opacity:0.8;

}
.center
{
z-index=1000;
margin:200px 450px;
padding:10px;
width:60px;
background:#fff;
border-radius:10px;


}
.center img-Loading
{
height:128px;
width:128px;

}

        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
<div id='cssmenu'>

        <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

        <link href="../css/employee.css" rel="stylesheet" type="text/css" />
        <%-- <ul>
            <li class="selected"><a href='Employee.aspx'><span>Employee </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="ViewPolicy.aspx">View Policies</a></li>--%>
        <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        <%--</ul> --%>
        <ul>
            <li class="selected"><a href='Employee.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
             <li><a href="EmployeeDutyRoaster.aspx">
                <%-- View Policies--%>
              <asp:Literal ID="Literal56" runat="server" Text='<%$Resources:MasterPageCommon,DutyRoaster %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        </ul>
    </div>
</asp:Content>

             
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">

<asp:UpdateProgress ID="DataLoading" runat="server" AssociatedUpdatePanelID="PanelSave">
<ProgressTemplate>
<div class="modal-Loading" >
<div class="center">
<img alt="" src="../images/HR power loader.gif"/>
</div>
</div>
</ProgressTemplate>
</asp:UpdateProgress>
<%--<asp:UpdatePanel ID="UpdatePanel-Loading" runat="server">
<ContentTemplate>
<div style="display: none">
<asp:Button ID="ButtonLoading" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
</div>
</ContentTemplate>
</asp:UpdatePanel>--%>


<div class="header-menu" id="TopHeader">
      <div id="Div1">
<div class="col">
<asp:UpdatePanel ID="PanelSave" runat="server">
<ContentTemplate>
    <asp:ImageButton ID="ButtonSave" runat="server" Height="18px" 
        ImageUrl="~/images/Save Blue.png" onclick="ButtonSave_Click"  CausesValidation="false" />
        </ContentTemplate>
        </asp:UpdatePanel>
         </div>
        
<div class="col">
    <asp:ImageButton ID="DeleteButton" runat="server" Height="17px" 
        ImageUrl="~/images/delete_icon.png" onclick="DeleteButton_Click" />
    </div>
<div class="col">
    <asp:ImageButton ID="ClearButton" runat="server" Height="18px"  
        ImageUrl="~/images/clear.png" onclick="ClearButton_Click"  />
    </div>
<div class="col">
    <asp:ImageButton ID="PrintButton" runat="server" Height="18px" 
        ImageUrl="~/images/print_IconNew.png" onclick="PrintButton_Click"  />
    </div>
   
</div>
   
   </div>

    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>

   


<div id="header" style="height:50px;width:969px;float:left">
  
   <div class="row">   
            
            <div class="lbl">
                <asp:Label ID="lblCompany" runat="server" Text="Company"></asp:Label>
            </div>
            <div class="col">
                <asp:DropDownList ID="drpCompany" runat="server" 
                    onselectedindexchanged="drpCompany_SelectedIndexChanged" 
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
           
            <div class="lbl">
                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
           </div>
            <div class="col">
                <asp:DropDownList ID="drpDepartment" runat="server">
                </asp:DropDownList>
            </div>
           
            <div class="lbl">
                <asp:Label ID="lblMonth" runat="server" Text="Month"></asp:Label>
            </div>
         


            <div class="col">
      
              <asp:TextBox ID="txtMonth" runat="server" CssClass="textbox_mandatory" Width="100px"
                                   ontextchanged="txtMonth_TextChanged" MaxLength="10" Style="background-color: InfoBackground;"></asp:TextBox>
            
                    <asp:ImageButton ID="ibtnLeaveTodate" 
                    runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                  
                    Style="margin-left: 5px" />
                    
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtMonth"
                                                    PopupButtonID="ibtnLeaveTodate" Format="MM/dd/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                         <%--       <asp:CustomValidator ID="RoastDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtMonth"
                                                    Display="Dynamic" ClientValidationFunction="valRequestFromToDate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>--%>
                                               <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtMonth"
                                                    Display="Dynamic" ClientValidationFunction="checkFromToDate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                              <%--  <asp:CustomValidator ID="cvCompare" runat="server" ValidationGroup="Submit" ControlToValidate="txtToDate"
                                                    ClientValidationFunction="confirmLeaveHolidays" CssClass="error" Display="Dynamic"></asp:CustomValidator>--%>
                                       
                                               
          <asp:Label ID="lblrequired" runat="server" Text="" Visible="false"></asp:Label >     
                                                    
                                                    
            </div>
              
           <div class="col lbl" style="cursor:pointer;">
              <asp:Button ID="btnShow" runat="server" Text="Show" Width="79px" AutoPostBack="false"
                    onclick="btnShow_Click" />
                    
                    
                    
                    
                    
           </div>
        </div>
    </div>    



<div id="data" class="grid-frame">

<asp:Label ID="lblEmpty" runat="server" Text="" Visible="false"></asp:Label >

  <asp:GridView ID="GrdRoaster" runat="server" AutoGenerateColumns="False" 
        Width="2703px" onrowdatabound="GrdRoaster_RowDataBound" >
      
        <Columns>
         <asp:TemplateField HeaderText=" " Visible="false">
                <HeaderTemplate>
                    <asp:Label ID="lblHdEmployeeID" runat="server" Text="EmployeeID"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="EmployeeID" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Employee ">
                <HeaderTemplate>
                    <asp:Label ID="lblEmployeeName" runat="server" Text="EmployeeName"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblEmployee" runat="server"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate  >
                    <asp:Label ID="lblShiftHeader1" runat="server" Text="1"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift1" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader2" runat="server" Text="2"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift2" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader3" runat="server" Text="3"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift3" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader4" runat="server" Text="4"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift4" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader5" runat="server" Text="5"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift5" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader6" runat="server" Text="6"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift6" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader7" runat="server" Text="7"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift7" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader8" runat="server" Text="8"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift8" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader9" runat="server" Text="9"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift9" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader10" runat="server" Text="10"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift10" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader11" runat="server" Text="11"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift11" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader12" runat="server" Text="12"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift12" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader13" runat="server" Text="13"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift13" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader14" runat="server" Text="14"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift14" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader15" runat="server" Text="15"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift15" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader16" runat="server" Text="16"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift16" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader17" runat="server" Text="17"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift17" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader18" runat="server" Text="18"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift18" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader19" runat="server" Text="19"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift19" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader20" runat="server" Text="20"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift20" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader21" runat="server" Text="21"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift21" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader22" runat="server" Text="22"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift22" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader23" runat="server" Text="23"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift23" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader24" runat="server" Text="24"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift24" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader25" runat="server" Text="25"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift25" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader26" runat="server" Text="26"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift26" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader27" runat="server" Text="27"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift27" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader28" runat="server" Text="28"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift28" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader29" runat="server" Text="29"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift29" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader30" runat="server" Text="30"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift30" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblShiftHeader31" runat="server" Text="31"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:DropDownList ID="drpShift31" runat="server" Height="30px" Width="80px">
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
        
        </Columns>
    </asp:GridView>
</div>

    
    </ContentTemplate>
    
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnShow" EventName="Click" />
            </Triggers>
    
  </asp:UpdatePanel>
  

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

