﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Organizationhierarchy.aspx.cs"
    Inherits="Organizationhierarchy" %>
<%@ Register Src="~/Controls/ChangeCompany.ascx" TagName="ChangeCompany" TagPrefix="uc1" %>

<%@ Register Src="../Controls/HierarchyViewControl.ascx" TagName="HierarchyViewControl"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>OrganisationHierarchy</title>
    <%--<link href="../css/managerview.css" rel="stylesheet" type="text/css" />
    <link href="../css/default_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/popup.css" rel="stylesheet" type="text/css" />--%>
    
    <link href="../css/ReplaceCls.css" rel="stylesheet" type="text/css" />
    <link href="../css/default_Cmn.css" rel="stylesheet" type="text/css" />
    <link href="../css/managerview.css" rel="stylesheet" type="text/css" />
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.js.css" rel="stylesheet" type="text/css" />
    <link href="../css/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
    <link href="../css/singleview.css" rel="stylesheet" type="text/css" />
    <link href="../css/Controls.css" rel="stylesheet" type="text/css" />
    <link href="../css/popup.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
     <AjaxControlToolkit:ToolkitScriptManager EnablePageMethods="true" ID="ajxManager"
        runat="server">
    </AjaxControlToolkit:ToolkitScriptManager>
    <div id="header-wrapper">
        <div id="header">
          <div id="companylogo1">
                    <div class="companyimg">
                        <asp:Image ID="imgCompanyLogo" runat="server" />
                    </div>
                    <div class="companyname">
                        <asp:Label id="lblCompanyName" runat="server" Text="company 1"></asp:Label></div>
            </div>
            <div id="logo">
                <img src="../images/logohr power.png" />
            </div>
            <div id="welcome">
                <asp:Label ID="lblWelcomeText" CssClass="welcomelabel" runat="server"></asp:Label>
            </div>
            <div id="logout">
                <a href="../logout.aspx" title="Logout">
                    <img src="../images/logout1.png" />
                </a>
            </div>
            <div class="linkhome">
                <a href="../Public/RequestSettings.aspx" title="Settings" id="ancSettings" runat="server">
                    <img src="../images/request_settings.png" />
                </a>
            </div>
            <div class="linkhome">
                <a href="~/Public/Home.aspx" title="Employee view" id="ancManager" runat="server"
                    style="display: block;">
                    <img src="../images/employeeview.png" />
                </a>
            </div>
            
            
             <div class="linkhome">
                <asp:UpdatePanel ID="upimgCompany" UpdateMode="Always" runat="server">
                    <ContentTemplate>
                        <asp:ImageButton ID="imgChangeCompany" runat="server" ImageUrl="../images/changecompany.png"
                           />
                        <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="dvChangeCompany"
                            RepositionMode="None" Drag="true" PopupControlID="dvChangeCompany" TargetControlID="imgChangeCompany"
                            ID="mdChangeCompany" runat="server" X="550" Y="100">
                        </AjaxControlToolkit:ModalPopupExtender>
                        <div id="dvChangeCompany" style="display:none; width: 70%; height: 80%; overflow: scroll">
                            <uc1:ChangeCompany ID="ChangeCompany1" runat="server" ModalPopupID="mdChangeCompany" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            
             
            </div>
        </div>
   
    </div>
    <asp:HiddenField ID="hdCurrentDate" runat="server" />
    <div>
        <div id="essfooter-wrapper">
            <div id="essfooter-content">
                <a href="~/Public/HomeDetailView.aspx" runat="server" id="ancHome">
                    <div class="homeBox">
                        <div class="one_fourth">
                            <div class="boxImage">
                                <img src="../images/3.png"></div>
                            <h5>
                                <asp:Literal ID="LtHome" runat ="server"  Text='<%$Resources:MasterPageCommon,Home %>'></asp:Literal>  </h5>
                        </div>
                </a><%--<a href="~/Public/Projects.aspx" runat ="server"  id ="ancPlanning" >
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/planning.png"></div>
                        <h5>
                         
                           <asp:Literal ID="Literal7" runat ="server" Text ="PLANNING"></asp:Literal>
                            
                            </h5>
                    </div>
                </a>--%>
               <%-- <a href="~/Public/Job.aspx" runat="server" id="ancRecruitment">
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/6.png"></div>
                        <h5>
                          <asp:Literal ID="Literal1" runat ="server"  Text='<%$Resources:MasterPageCommon,RECRUITMENT %>'></asp:Literal>    </h5>
                    </div>
                </a>--%>
                
                <a href="~/Public/Vacancy.aspx" runat ="server"  id ="ancPlanning" >
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/planning&recruitment.png"></div>
                        <h5>
                           <%-- PLANNING--%>
                           <asp:Literal ID="LitPlanningAndRecruitment" runat ="server" Text ="PLANNING & RECRUITMENT"></asp:Literal>
                            
                            </h5>
                    </div>
                </a>
                
                
                
                <a href="~/Public/Employee.aspx" runat="server" id="ancEmployee">
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/1.png"></div>
                        <h5>
                           <asp:Literal ID="Literal2" runat ="server"  Text='<%$Resources:MasterPageCommon, EMPLOYEE%>'></asp:Literal>   </h5>
                    </div>
                </a><a href="~/Public/Passport.aspx" runat="server" id="a1">
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/my-documents.png"></div>
                        <h5>
                            <asp:Literal ID="Literal3" runat ="server"  Text='<%$Resources:MasterPageCommon,DOCUMENTS%>'></asp:Literal>  </h5>
                    </div>
                </a>
                <%--
                <a href="../Public/PerformanceInitiation.aspx" runat="server" id="ancPerformance">
                    <div class="one_fourth last">
                        <div class="boxImage">
                            <img src="../images/performance2.png"></div>
                        <h5>
                           <asp:Literal ID="Literal4" runat ="server"  Text='<%$Resources:MasterPageCommon,PERFORMANCE %>'></asp:Literal>   </h5>
                    </div>
                </a>--%>
                <a href="../Public/PerformanceInitiation.aspx" runat ="server"  id ="ancPerformance">
                    <div class="one_fourth last">
                        <div class="boxImage">
                            <img src="../images/performance22.png"></div>
                        <h5>
                            <%--PERFORMANCE--%>
                              <asp:Literal ID="LtPerformanceAndTraining" runat ="server" Text ="PERFORMANCE & TRAINING"></asp:Literal>
                            </h5>
                    </div>
                </a>
                
                
                
                <%--<a href="~/Public/Job.aspx" runat ="server"  id ="ancTrainng" >
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/training.png"></div>
                        <h5>
                           
                           <asp:Literal ID="Literal8" runat ="server" Text ="TRAINING"></asp:Literal>
                            
                            </h5>
                    </div>
                </a>--%>
                <a href="~/Public/Vacancy.aspx" runat ="server"  id ="ancTask" visible="false" >
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/tasks.png"></div>
                        <h5>
                           <%-- TASK--%>
                           <asp:Literal ID="Literal9" runat ="server" Text ="TASK"></asp:Literal>
                            
                            </h5>
                    </div>
                </a><a href="../reporting/RptCommonEss.aspx?type=e" runat="server" id="ancreports">
                    <div class="one_fourth">
                        <div class="boxImage">
                            <img src="../images/4.png"></div>
                        <h5>
                           <asp:Literal ID="Literal5" runat ="server"  Text='<%$Resources:MasterPageCommon,REPORTS %>'></asp:Literal>   </h5>
                    </div>
                </a>
            </div>
        </div>
        <div id="footer-menucontent" style="width: 88%; margin: auto; padding: 0px;">
            <div id='cssmenu'>
               <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li class="selected"><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li ><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            
        </ul>
            </div>
              <div id="Rforms">
                <div id="dashboard1">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div style="width: 100%; float: left;" id="divMain" runat="server">
                                <div style="width: 60%; float: left;">
                                    <div style="width: 95%; float: left; margin-left: 5%; margin-top: 30px;">
                                        <div style="width: 30%; float: left;">
                                            Parent Designation
                                        </div>
                                        <div style="width: 70%; float: left;">
                                            <asp:DropDownList ID="ddlParentDesignation" DataTextField="Designation" DataValueField="DesignationId"
                                                runat="server" Width="216px" CssClass="dropdownlist_mandatory">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rqddlParentDesignation" runat="server" ControlToValidate="ddlParentDesignation"
                                                CssClass="error" Display="Dynamic" ErrorMessage="Please select any parent designation "
                                                InitialValue="-1" ForeColor="" SetFocusOnError="True" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div style="width: 95%; float: left; margin-left: 5%; margin-top: 30px;">
                                        <div style="width: 30%; float: left;">
                                            Designation
                                        </div>
                                        <div style="width: 70%; float: left;">
                                            <asp:ListBox ID="lstChildDesignations" DataTextField="Designation" DataValueField="DesignationId"
                                                runat="server" Width="292px" CssClass="textbox" Height="295px" SelectionMode="Multiple">
                                            </asp:ListBox>
                                            <asp:CustomValidator ID="cvDesignation" runat="server" ClientValidationFunction="chklstDesignation"
                                                Display="Dynamic" CssClass="error" ControlToValidate="lstChildDesignations" ValidationGroup="Submit"></asp:CustomValidator>
                                        </div>
                                    </div>
                                    <div style="width: 95%; float: left; margin-left: 5%; margin-top: 30px;">
                                        <div style="width: 20%; float: left; margin-left: 80%">
                                            <asp:Button ID="btnSubmit" Width="60" runat="server" CssClass="btnsubmit" ToolTip="Submit"
                                                Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return valCofirmHiearchyInsert('Submit');"
                                                ValidationGroup="Submit" />
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 40%; float: left;">
                                    <asp:UpdatePanel ID="upTree" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td valign="top" >
                                                        <table  border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="25px">
                                                                    <img src="../images/Organization hierarchy_small.png" />
                                                                </td>
                                                                <td width="50px">
                                                                    <h4>
                                                                        Hierarchy</h4>
                                                                </td>
                                                                <td align="right">
                                                                
                                                                <div style ="float:left ;margin-left:50px">
                                                                    <asp:UpdatePanel ID="updHierarchy" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                          <%--  <asp:LinkButton ID="lnkViewEmployeeHierarchy1" runat="server" Text="Employee Hierarchy"
                                                                               >
                                                                            </asp:LinkButton>--%>
                                                                            
                                                                            
                                                                            <asp:ImageButton Width ="32px" Style="margin-left: 10px;" Height ="32px" ID ="lnkViewEmployeeHierarchy"  OnClick="lnkViewEmployeeHierarchy_Click"  ToolTip ="View employee hierarchy" runat ="server" ImageUrl ="~/images/EmployeeHierarchy.png" />  
                                                                           <%-- <asp:LinkButton ID="lnkViewOrganizationHierarchy" runat="server" Text="Organization Hierarchy"
                                                                                ">
                                                                            </asp:LinkButton>--%>
                                                                            
                                                                            
                                                                          <asp:ImageButton Width ="32px" Style="margin-left: 20px;margin-right:10px;" Height ="32px" ID ="lnkViewOrganizationHierarchy" ToolTip="View organisation hierarchy" runat ="server" OnClick="lnkViewOrganisationHierarchy_Click1" ImageUrl ="~/images/OrganizationHierarchy.png" />  

                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    </div> 
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <div style="height: 250px; max-width: 70%; overflow: auto">
                                                            <asp:TreeView ID="tvOrganizationalHierarchy" runat="server" onclick="SelectAllChildNodes(event);"
                                                                ShowCheckBoxes="All" NodeStyle-CssClass="labeltext" ShowLines="True" OnTreeNodePopulate="tvOrganizationalHierarchy_TreeNodePopulate">
                                                            </asp:TreeView>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trRemove">
                                                    <td align="center" style="margin-top: 5px">
                                                        <a>
                                                            <img src="../images/Delete team member.png" />
                                                            <asp:LinkButton ID="btnDelete" runat="server" CausesValidation="False" ToolTip="Remove Selected Employees"
                                                                OnClientClick="return valDelHierarchy();" OnClick="btnDelete_Click">Remove Designation</asp:LinkButton></a>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="tr1" style="margin-top: 10px; height: 50px;">
                                                
                                                <td>
                                                </td>
                                                   <%-- <td align="right" style="padding: 10px; font-size: small; font-weight: bold; color: #FF3300;
                                                        text-align: justify;">
                                                        * If you delete a designation ,the designation and all its sub designations will
                                                        be deleted from the Organization Hierarchy.So you must have to reassign the employee
                                                        designation according to the designations in the Organization Hierarchy.
                                                    </td>--%>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    
                                    
                                    <div style="clear :both ;float:left;max-width:92%;overflow:auto; font-size: small; font-weight: bold; color: #FF3300; text-align: justify">
                                      
                                                        * If you delete a designation ,the designation and all its sub designations will
                                                        be deleted from the Organization Hierarchy.So you must have to reassign the employee
                                                        designation according to the designations in the Organization Hierarchy.
                                                  
                                        
                                </div>
                            </div>
                            </div> 
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <div style="width: 100%; float: left;" id="divError" runat="server">
                                <asp:Label ID="lblPermission" runat="server" CssClass="error " Visible="false"></asp:Label>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalHierarchy" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID ="dvHierarchy" RepositionMode ="RepositionOnWindowScroll"  Drag="true" PopupControlID="dvHierarchy" TargetControlID="btn"
                ID="mdHierarchy" runat="server">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btn" runat="server" Style="display: none" Text="Reporting To Hierarchy" />
            <div id="dvHierarchy" style="width: 99%; height: 99%; overflow: hidden">
                <uc1:HierarchyViewControl ID="HierarchyViewControl1" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
    <div id="footer">
        <p>
            HRPower © 2021 Privacy policy All rights reserved. Powered by <a href="http://www.escubetech.com/">
                ES3 Technovations LLP</a>.</p>
    </div>
</body>
</html>
