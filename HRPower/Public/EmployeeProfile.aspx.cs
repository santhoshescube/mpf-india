﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class Public_EmployeeProfile : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsUserMaster objUser;
    clsJobPost objJobPost;
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            CurrentSelectedValue = "-1";
            GetEmployeeDetails();
            if (new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.ViewMyDocumentsEnabled).ToUpper() == "YES")
                imgListAllDocuments.Visible = lnkListAllDocuments.Visible = true;
            else
                imgListAllDocuments.Visible = lnkListAllDocuments.Visible = false;
        }

        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);      
    }

    private void GetEmployeeDetails()
    {
        //objEmployee = new clsEmployee();
        //objUser = new clsUserMaster();
        //objEmployee.EmployeeID = objUser.GetEmployeeId();
        //ViewState["EmployeeID"] = objEmployee.EmployeeID;

        //DataSet ds = objEmployee.GetEmployee();
        //fvEmployee.DataSource = ds;
        //fvEmployee.DataBind();

        //try
        //{
        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
        //    {
        //        txtPhone.Text = ds.Tables[0].Rows[0]["Phone"].ToString();
        //        txtMobile.Text = ds.Tables[0].Rows[0]["Mobile"].ToString();
        //        txtEmail.Text = ds.Tables[0].Rows[0]["EmailID"].ToString();
        //        txtAddress.Text = ds.Tables[0].Rows[0]["AddressLine1"].ToString();
        //    }
        //}
        //catch { }

        objEmployee = new clsEmployee();
        objUser = new clsUserMaster();
        objEmployee.EmployeeID = objUser.GetEmployeeId();
        ViewState["EmployeeID"] = objEmployee.EmployeeID;
        DataTable dt = objEmployee.GetEmployee().Tables[0];
        if (dt.Rows.Count == 0) return;
        ViewState["Buffer"] = dt.Rows[0]["RecentPhoto"];

        fvEmployee.DataSource = dt;
        fvEmployee.DataBind();
    }

    protected string GetFilter(object sFilter)
    {
        if (sFilter != DBNull.Value)
        {
            if (sFilter.ToString().Length > 60)
            {
                sFilter = sFilter.ToString().Substring(0, 58);
                sFilter = sFilter + "............";
                return Convert.ToString(sFilter);
            }
            else
            {
                return Convert.ToString(sFilter);
            }
        }
        else
            return string.Empty;
    }
    protected string GetEmployeeRecentPhoto(object EmployeeID, object RecentPhoto, int width)
    {
        if (RecentPhoto == DBNull.Value)
            return GetEmployeePassportPhoto(EmployeeID, width);
        else
            return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }
    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
        else
            return string.Empty;

    }
    protected string GetEmployeeRecentPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeePassportPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }
    protected void btnPasswordSubmit_Click(object sender, EventArgs e)
    {
        objUser = new clsUserMaster();
        objUser.EmployeeID = objUser.GetEmployeeId();
        objUser.Password = txtPassword.Text;
        objUser.UpdatePassword();

        if (clsGlobalization.IsArabicCulture())
            msgs.InformationalMessage("تغيير كلمة المرور بنجاح");
        else
            msgs.InformationalMessage("Password succesfully changed.");

        mpeMessage.Show();

        fvEmployee.ChangeMode(FormViewMode.ReadOnly);
        GetEmployeeDetails();
        upEmployees.Update();

        ScriptManager.RegisterStartupScript(btnPasswordSubmit, btnPasswordSubmit.GetType(), "ShowHide", "ShowHide();", true);
    }
    protected void lnlEditProfile_Click(object sender, EventArgs e)
    {

        fvEmployee.ChangeMode(FormViewMode.Edit);

        objEmployee = new clsEmployee();

        objUser = new clsUserMaster();
        objEmployee.EmployeeID = objUser.GetEmployeeId();

        DataTable dt = objEmployee.GetEmployee().Tables[0];

        if (dt.DefaultView.Count > 0)
        {
            ViewState["Buffer"] = dt.Rows[0]["RecentPhoto"];
            ViewState["BufferP"] = dt.Rows[0]["PassportPhoto"];

            fvEmployee.DataSource = dt;
            fvEmployee.DataBind();

            upEmployees.Update();
        }

    }
    private void FillAllCombos()
    {
        using (DataSet ds = new clsUserMaster().FillAllCombos())
        {
            if (ds.Tables.Count == 4)
            {
                //DropDownList rcCountryOfOrigin = (DropDownList)fvEmployee.FindControl("rcCountryOfOrigin");
                DropDownList rcNationality = (DropDownList)fvEmployee.FindControl("rcNationality");
               // DropDownList rcEthnicOrigin = (DropDownList)fvEmployee.FindControl("rcEthnicOrigin");
                DropDownList rcReligion = (DropDownList)fvEmployee.FindControl("rcReligion");
                DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");

               // UpdatePanel updCountryOfOrigin = (UpdatePanel)fvEmployee.FindControl("updCountryOfOrigin");
                UpdatePanel updNationality = (UpdatePanel)fvEmployee.FindControl("updNationality");
              
                UpdatePanel updReligion = (UpdatePanel)fvEmployee.FindControl("updReligion");
                UpdatePanel upDegree = (UpdatePanel)fvEmployee.FindControl("upDegree");


                if (rcNationality != null && rcReligion != null 
                     && updNationality != null && updReligion != null 
                    )
                {
                    //rcCountryOfOrigin.DataSource = ds.Tables[0];
                    //rcCountryOfOrigin.DataTextField = "Description";
                    //rcCountryOfOrigin.DataValueField = "CountryId";
                    //rcCountryOfOrigin.DataBind();
                    //updCountryOfOrigin.Update();


                    rcNationality.DataSource = ds.Tables[1];
                    rcNationality.DataTextField = "NationalityDescription";
                    rcNationality.DataValueField = "NationalityId";
                    rcNationality.DataBind();
                    rcNationality.Items.Insert(0, new ListItem("--Select--", "-1"));
                    updNationality.Update();


                    //rcEthnicOrigin.DataSource = ds.Tables[2];
                    //rcEthnicOrigin.DataTextField = "EthnicOrigin";
                    //rcEthnicOrigin.DataValueField = "EthnicOriginId";
                    //rcEthnicOrigin.DataBind();
                    //updEthnicOrigin.Update();

                    rcReligion.DataSource = ds.Tables[2];
                    rcReligion.DataTextField = "Description";
                    rcReligion.DataValueField = "religionId";
                    rcReligion.DataBind();
                    rcReligion.Items.Insert(0, new ListItem("--Select--", "-1"));
                    updReligion.Update();

                    //rcDegree.DataSource = ds.Tables[3];
                    //rcDegree.DataTextField = "Description";
                    //rcDegree.DataValueField = "DegreeId";
                   
                    //rcDegree.DataBind();
                    //rcDegree.Items.Insert(0, new ListItem("--Select--", "-1"));
                    //upDegree.Update();


                   // FillComboSpecialization();

                }

            }

        }
    }

    protected void fvEmployee_DataBound(object sender, EventArgs e)
    {
        if (fvEmployee.CurrentMode == FormViewMode.Edit)
        {
            RadioButtonList rblGender = (RadioButtonList)fvEmployee.FindControl("rblGender");
            DropDownList ddlMaritalStatus = (DropDownList)fvEmployee.FindControl("ddlMaritalStatus");
            DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");
            DropDownList ddlList = (DropDownList)fvEmployee.FindControl("ddlList");
            DropDownList rcNationality = (DropDownList)fvEmployee.FindControl("rcNationality");          
            DropDownList rcReligion = (DropDownList)fvEmployee.FindControl("rcReligion");
            UpdatePanel updEthnicOrigin = (UpdatePanel)fvEmployee.FindControl("updEthnicOrigin");
            UpdatePanel updReligion = (UpdatePanel)fvEmployee.FindControl("updReligion");
            UpdatePanel updList = (UpdatePanel)fvEmployee.FindControl("updList");

            FillAllCombos();

            rblGender.SelectedIndex = (fvEmployee.DataKey["Gender"] == DBNull.Value ? 0 : (rblGender.Items.IndexOf(rblGender.Items.FindByValue(fvEmployee.DataKey["Gender"].ToString()))));
            rcNationality.SelectedValue = (fvEmployee.DataKey["NationalityID"] == DBNull.Value ? "-1" : (fvEmployee.DataKey["NationalityID"]).ToString());
            rcReligion.SelectedValue = (fvEmployee.DataKey["ReligionID"] == DBNull.Value ? "-1" : (fvEmployee.DataKey["ReligionID"]).ToString());
        }
    }

    protected void fvEmployee_ItemCommand(object sender, FormViewCommandEventArgs e)
    {        
    }

    private void UpdateProfile()
    {                
    }

    protected string GetEmployeeRecentPhotoNew(object EmployeeID, object RecentPhoto, int width)
    {
        if (fvEmployee.DataKey["EmployeeID"] != DBNull.Value)
        {
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }

    }
    protected string GetEmployeePassportPhotoNew(object EmployeeID, object PassportPhoto, int width)
    {
        if (fvEmployee.DataKey["EmployeeID"] != DBNull.Value)
        {
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }

    }


    public void FillComboSpecialization()
    {
        objEmployee = new clsEmployee();
        DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");
        DropDownList ddlList = (DropDownList)fvEmployee.FindControl("ddlList");
        UpdatePanel upDegree = (UpdatePanel)fvEmployee.FindControl("upDegree");
        UpdatePanel updList = (UpdatePanel)fvEmployee.FindControl("updList");

        if (rcDegree.SelectedValue != "")
        {
            objEmployee.DegreeId = Convert.ToInt32(rcDegree.SelectedValue);
            using (DataSet ds = objEmployee.FillComboSpecialization())
            {
                ddlList.DataSource = ds.Tables[0];
                ddlList.DataTextField = "Description";
                ddlList.DataValueField = "SpecializationId";
                ddlList.DataBind();
                ddlList.SelectedValue = CurrentSelectedValue;
                ddlList.Items.Insert(0, new ListItem("None", "-1"));
                updList.Update();
            }
        }

    }
    protected void rcDegree_IndexChanged(object sender, EventArgs e)
    {
       // FillComboSpecialization();
    }

    protected void btnSave_Click(object sender, ImageClickEventArgs e)
    {
        objEmployee = new clsEmployee();
        int ires = 0;

        ImageButton btnSave = (ImageButton)sender;

        DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");

        //if (hdMode.Value == "add")
        //{
        //    objEmployee.DegreeId = Convert.ToInt32(rcDegree.SelectedValue);
        //    if (Convert.ToInt32(rcDegree.SelectedValue) == -1)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(btnSave, btnSave.GetType(), string.Empty, "alert('Please select any Qualification.');", true);
        //        return;
        //    }
        //    objEmployee.Specialization = txtValue.Text;
        //    ires = objEmployee.InsertDegreeSpecializationReference();
        //    if (!(ires > 0))
        //    {
        //        ScriptManager.RegisterClientScriptBlock(btnSave, btnSave.GetType(), string.Empty, "alert('Insertion failed." + txtValue.Text + " already exists');", true);
        //        return;
        //    }

        //}
        //else if (hdMode.Value == "edit")
        //{
        //    objEmployee.DegreeId = Convert.ToInt32(rcDegree.SelectedValue);
        //    objEmployee.Specialization = txtValue.Text;
        //    objEmployee.SpecializationId = Convert.ToInt32(ddlList.SelectedValue);
        //    ires = objJobPost.UpdateDegreeSpecializationReference();
        //    if (!(ires > 0))
        //    {
        //        ScriptManager.RegisterClientScriptBlock(btnSave, btnSave.GetType(), string.Empty, "alert('Updation failed." + txtValue.Text + " already exists');", true);
        //        return;
        //    }
        //}

        //ddlList.DataSource = objEmployee.FillComboSpecialization();
        //ddlList.DataBind();
        //ddlList.Items.Insert(0, new ListItem("None", "-1"));
        //ddlList.SelectedIndex = ddlList.Items.IndexOf(ddlList.Items.FindByValue(ires.ToString()));
        //divBottom.Style.Add("display", "none");
        // divTop.Style.Add("display", "block");

        //b/tnEdit.OnClientClick = "return validateEdit('" + btnEdit.ClientID + "')";
        //btnDel.OnClientClick = "return confirmDelete('" + btnDel.ClientID + "', '" + "Specialization" + "');";
    }
    protected void btnResume_Click(object sender, ImageClickEventArgs e)
    {
        //divBottom.Style.Add("display", "none");
        // divTop.Style.Add("display", "block");
    }
    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {

    }
    //protected void btnCountry_Click(object sender, EventArgs e)
    //{
    //    ReferenceControlNew1.ClearAll();
    //    ReferenceControlNew1.TableName = "CountryReference";
    //    ReferenceControlNew1.DataTextField = "Description";
    //    ReferenceControlNew1.DataValueField = "CountryId";
    //    ReferenceControlNew1.FunctionName = "FillComboCountryOfOrigin";
    //    ReferenceControlNew1.SelectedValue = rcCountryOfOrigin.SelectedValue;
    //    ReferenceControlNew1.DisplayName = "Country";
    //    ReferenceControlNew1.PopulateData();

    //    mdlPopUpReference.Show();
    //    updModalPopUp.Update();
    //}

    protected void btnNatinality_Click(object sender, EventArgs e)
    {
        DropDownList rcNationality = (DropDownList)fvEmployee.FindControl("rcNationality");
        UpdatePanel updNationality = (UpdatePanel)fvEmployee.FindControl("updNationality");
        if (rcNationality != null && updNationality != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "NationalityReference";
            ReferenceControlNew1.DataTextField = "Nationality";
            ReferenceControlNew1.DataValueField = "NationalityId";
            ReferenceControlNew1.FunctionName = "FillComboNationality";
            ReferenceControlNew1.SelectedValue = rcNationality.SelectedValue;
            ReferenceControlNew1.DisplayName = "Nationality";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void btnEthnicOrigin_Click(object sender, EventArgs e)
    {
        DropDownList rcEthnicOrigin = (DropDownList)fvEmployee.FindControl("rcEthnicOrigin");
        UpdatePanel updEthnicOrigin = (UpdatePanel)fvEmployee.FindControl("updEthnicOrigin");
        if (rcEthnicOrigin != null && updEthnicOrigin != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "EthnicOrgin";
            ReferenceControlNew1.DataTextField = "EthnicOrigin";
            ReferenceControlNew1.DataValueField = "EthnicOriginId";
            ReferenceControlNew1.FunctionName = "FillComboEthnicOrigin";
            ReferenceControlNew1.SelectedValue = rcEthnicOrigin.SelectedValue;
            ReferenceControlNew1.DisplayName = "Ethnic Origin";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }

    }
    protected void btnReligion_Click(object sender, EventArgs e)
    {
        DropDownList rcReligion = (DropDownList)fvEmployee.FindControl("rcReligion");
        UpdatePanel updReligion = (UpdatePanel)fvEmployee.FindControl("updReligion");
        if (rcReligion != null && updReligion != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "ReligionReference";
            ReferenceControlNew1.DataTextField = "Religion";
            ReferenceControlNew1.DataValueField = "religionId";
            ReferenceControlNew1.FunctionName = "FillComboReligion";
            ReferenceControlNew1.SelectedValue = rcReligion.SelectedValue;
            ReferenceControlNew1.DisplayName = "Religion";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void btnDegree_Click(object sender, EventArgs e)
    {
        DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");
        UpdatePanel upDegree = (UpdatePanel)fvEmployee.FindControl("upDegree");
        if (rcDegree != null && upDegree != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "PayDegreeReference";
            ReferenceControlNew1.DataTextField = "Degree";
            ReferenceControlNew1.DataValueField = "DegreeId";
            ReferenceControlNew1.FunctionName = "FillComboDegree";
            ReferenceControlNew1.SelectedValue = rcDegree.SelectedValue;
            ReferenceControlNew1.DisplayName = "Degree";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void btnList_Click(object sender, EventArgs e)
    {
        DropDownList ddlList = (DropDownList)fvEmployee.FindControl("ddlList");
        DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");

        UpdatePanel updList = (UpdatePanel)fvEmployee.FindControl("updList");
        if (ddlList != null && updList != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "HRDegreeSpecializationReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataValueField = "SpecializationId";
            ReferenceControlNew1.FunctionName = "FillComboSpecialization";
            ReferenceControlNew1.FilterColumn = "DegreeId";
            ReferenceControlNew1.FilterId = Convert.ToInt32(rcDegree.SelectedValue);
            ReferenceControlNew1.SelectedValue = ddlList.SelectedValue;
            ReferenceControlNew1.DisplayName = "Specialization";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
    }

    protected void lnkChange_Click(object sender, EventArgs e)
    {
        //trProfile.Style["display"] = "none";
        //trSettings.Style["display"] = "block";
        //divEditProfileDetails.Style["display"] = "none";
    }

    public void FillComboNationality()
    {
        UpdatePanel updNationality = (UpdatePanel)fvEmployee.FindControl("updNationality");
        DropDownList rcNationality = (DropDownList)fvEmployee.FindControl("rcNationality");

        if (rcNationality != null && updNationality != null)
        {
            rcNationality.DataSource = new clsUserMaster().FillComboNationality();
            rcNationality.DataTextField = "Nationality";
            rcNationality.DataValueField = "NationalityId";
            rcNationality.DataBind();
            rcNationality.SelectedValue = CurrentSelectedValue;
            updNationality.Update();
        }
    }

    public void FillComboEthnicOrigin()
    {
        UpdatePanel updEthnicOrigin = (UpdatePanel)fvEmployee.FindControl("updEthnicOrigin");
        DropDownList rcEthnicOrigin = (DropDownList)fvEmployee.FindControl("rcEthnicOrigin");

        if (updEthnicOrigin != null && rcEthnicOrigin != null)
        {
            rcEthnicOrigin.DataSource = new clsUserMaster().FillComboEthnicOrigin();
            rcEthnicOrigin.DataTextField = "EthnicOrigin";
            rcEthnicOrigin.DataValueField = "EthnicOriginId";
            rcEthnicOrigin.DataBind();
            rcEthnicOrigin.SelectedValue = CurrentSelectedValue;
            updEthnicOrigin.Update();
        }
    }

    public void FillComboReligion()
    {
        UpdatePanel updReligion = (UpdatePanel)fvEmployee.FindControl("updReligion");
        DropDownList rcReligion = (DropDownList)fvEmployee.FindControl("rcReligion");

        if (rcReligion != null && updReligion != null)
        {
            rcReligion.DataSource = new clsUserMaster().FillComboReligion();
            rcReligion.DataTextField = "Religion";
            rcReligion.DataValueField = "religionId";
            rcReligion.DataBind();
            rcReligion.SelectedValue = CurrentSelectedValue;
            updReligion.Update();
        }
    }

    public void FillComboDegree()
    {
        UpdatePanel upDegree = (UpdatePanel)fvEmployee.FindControl("upDegree");
        DropDownList rcDegree = (DropDownList)fvEmployee.FindControl("rcDegree");

        if (upDegree != null && rcDegree != null)
        {
            rcDegree.DataSource = new clsUserMaster().FillComboDegree();
            rcDegree.DataTextField = "Degree";
            rcDegree.DataValueField = "DegreeId";
            rcDegree.DataBind();
            rcDegree.SelectedValue = CurrentSelectedValue;
            upDegree.Update();
        }
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    protected void lnkPaySlip_Click(object sender, EventArgs e)
    {
        if (ViewState["EmployeeID"].ToInt32() > 0)
            Response.Redirect("../reporting/PaySlipForEmp.aspx?EmpId=" + ViewState["EmployeeID"].ToInt32());
    }

    protected void lnkEditProfile_Click(object sender, EventArgs e)
    {        
    }

    protected void btnSubmitEditProfile_Click(object sender, EventArgs e)
    {
        objEmployee = new clsEmployee();
        objEmployee.LocalAddress = txtAddress.Text.Trim();
        objEmployee.LocalPhone = txtPhone.Text.Trim();
        objEmployee.LocalMobile = txtMobile.Text.Trim();
        objEmployee.Email = txtEmail.Text.Trim();

        objUser = new clsUserMaster();
        objEmployee.EmployeeID = objUser.GetEmployeeId();

        objEmployee.UpdateProfile();
        if (clsGlobalization.IsArabicCulture())
            msgs.InformationalMessage("تم تحديث التفاصيل بنجاح");
        else
            msgs.InformationalMessage("Details has been updated succesfully.");
        mpeMessage.Show();

        fvEmployee.ChangeMode(FormViewMode.ReadOnly);
        GetEmployeeDetails();
        upEmployees.Update();

        ScriptManager.RegisterStartupScript(btnSubmitEditProfile, btnSubmitEditProfile.GetType(), "ShowEditProfile", "ShowEditProfile();", true);
    }

    protected void btnCancelEditProfile_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(btnCancelEditProfile, btnCancelEditProfile.GetType(), "ShowEditProfile", "ShowEditProfile();", true);
    }
    /// <summary>
    /// displays selected employee documents
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkListAllDocuments_Click(object sender, EventArgs e)
    {
        if (ViewState["EmployeeID"].ToInt32() > 0)
        {
            string Page = "ViewEmployeeDocuments.aspx?EmpID=" + ViewState["EmployeeID"].ToInt32();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ListAllDocuments", "window.open('" + Page + "');", true);
        }
    }
}
