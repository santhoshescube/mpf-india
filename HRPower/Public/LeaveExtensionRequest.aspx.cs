﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using System.Resources;
using System.Web.Hosting;

/// <summary>
/// <created by>Megha</created>
/// <created on>2-AUG-2013</created>
/// Leave Extension Request
/// 
/// Code comprises of Leave Extension and Leave Extension Day of which currently used ones are of "Leave Extension Day"
/// </summary>
public partial class Public_LeaveExtensionRequest : System.Web.UI.Page
{
    clsLeaveExtensionRequest objRequest = new clsLeaveExtensionRequest();
    clsUserMaster objUser = new clsUserMaster();
    clsLeaveRequest objLeaveRequest;
    public bool bIsAdd = true;
    string strFailureMessage = string.Empty;

    protected void Page_PreInit(object sender, EventArgs e)
    {

        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("ExtensionTitle.Title").ToString();
        LeaveExtensionRequestPager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null)
                hfPostedUrl.Value = Request.UrlReferrer.ToString();

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;
            if (Request.QueryString["Requestid"] != null && Request.QueryString["Type"] == "Cancel")
            {
                //For binding cancelled Leave extension requests on to datalist
                int iRequestId = 0;
                ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = lnkView.Enabled = false;
            }
            else if (Request.QueryString["Requestid"] != null)
            {
                int iRequestId = 0;
                ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["Approve"] = true;
                fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = lnkView.Enabled = false;
            }
            else
            {
                //For binding all types of user Leave extension requests on to datalist
                BindDataList();
            }
            upForm.Update();
            upnlMenus.Update();
            upDetails.Update();
        }
        upDetails.Update();
        upForm.Update();
        upnlMenus.Update();
    }
    protected void GetBalanceLeave(object sender, EventArgs e)
    {
        GetLeaveCount();
    }
    protected void LeaveTypeVisibility(object sender, EventArgs e)
    {
        ChangeVisibility();
        upForm.Update();
    }
    private void ChangeVisibility()
    {
        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
        DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        Label lbDuration = (Label)fvLeaveRequest.FindControl("lbDuration");
        TextBox txtBalance = (TextBox)fvLeaveRequest.FindControl("txtBalance");
        Label lblBalanceCombo = (Label)fvLeaveRequest.FindControl("lblBalanceCombo");
        TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtDuration");
        TextBox txtReason = (TextBox)fvLeaveRequest.FindControl("txtReason");
        DropDownList ddlPeriod = (DropDownList)fvLeaveRequest.FindControl("ddlPeriod");
        Label lblPrevious = (Label)fvLeaveRequest.FindControl("lblPrevious");
        Label lblPreviousVacation = (Label)fvLeaveRequest.FindControl("lblPreviousVacation");
        HiddenField hfVacationFromDate = (HiddenField)fvLeaveRequest.FindControl("hfVacationFromDate");
        HiddenField hfVacationRejoinDate = (HiddenField)fvLeaveRequest.FindControl("hfVacationRejoinDate");
        TextBox txtVacationExtensionToDate = (TextBox)fvLeaveRequest.FindControl("txtVacationExtensionToDate");
        
        if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            txtDuration.Text = "";

        //  1--->leave extension ,2--->Time extension day, 3--->Compensatory-Off
        if (ddlExtensionType.SelectedValue == "2")
        {
            ddlLeaveType.Visible = false;
            //ddlLeaveType.CssClass = "dropdownlist_disabled";
            txtBalanceLeave.Text = "";
            ddlLeaveType.ClearSelection();
            lbDuration.Text = Convert.ToString(GetLocalResourceObject("BreakDuration.Text"));  //"Break Duration(Min)";
            chkCO.Checked = false;
            txtBalance.Visible = lblBalanceCombo.Visible = false;

            HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
            HtmlGenericControl divLeaveType = fvLeaveRequest.FindControl("divleaveType") as HtmlGenericControl;
            HtmlGenericControl divVacationChk = fvLeaveRequest.FindControl("divVacationChk") as HtmlGenericControl;
            HtmlGenericControl divLastVacation = fvLeaveRequest.FindControl("divLastVacation") as HtmlGenericControl;
            HtmlGenericControl divVacationType = fvLeaveRequest.FindControl("divVacationType") as HtmlGenericControl;
            divLeaveExtension.Style["display"] = "none";
            divTimeExtensionDay.Style["display"] = "block";
            divTimeExtensionMonth.Style["display"] = "none";
            divVacationChk.Style["display"] = "none";
            divLastVacation.Style["display"] = "none";
            divVacationType.Style["display"] = "none";
            divLeaveType.Style["display"] = "none";
            upForm.Update();
        }
        else if (ddlExtensionType.SelectedValue == "3")
        {
            ddlLeaveType.Visible = false;
            //ddlLeaveType.CssClass = "dropdownlist_disabled";
            txtBalanceLeave.Text = "";
            ddlLeaveType.ClearSelection();
            lbDuration.Text = Convert.ToString(GetLocalResourceObject("Days.Text"));  //"Break Duration(Min)";
            txtVacationExtensionToDate.Text = txtDuration.Text = string.Empty;
            HtmlGenericControl divLastVacation = fvLeaveRequest.FindControl("divLastVacation") as HtmlGenericControl;
            objRequest.EmployeeId = new clsUserMaster().GetEmployeeId();
            DataTable dt = objRequest.GetVacationID();
            if (dt.Rows.Count > 0)
            {
                ViewState["VID"] = dt.Rows[0]["VacationID"];
                lblPreviousVacation.Text = dt.Rows[0]["PreviousVacation"].ToString();
                hfVacationFromDate.Value = dt.Rows[0]["VacationFromDate"].ToString();
                hfVacationRejoinDate.Value = dt.Rows[0]["RejoiningDate"].ToString();
                divLastVacation.Style["display"] = "block";
            }
            else
                divLastVacation.Style["display"] = "none";

            HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
            HtmlGenericControl divLeaveType = fvLeaveRequest.FindControl("divleaveType") as HtmlGenericControl;
            HtmlGenericControl divVacationChk = fvLeaveRequest.FindControl("divVacationChk") as HtmlGenericControl;
            HtmlGenericControl divVacationType = fvLeaveRequest.FindControl("divVacationType") as HtmlGenericControl;

            divLeaveExtension.Style["display"] = "none";
            divTimeExtensionDay.Style["display"] = "none";
            divTimeExtensionMonth.Style["display"] = "none";
            divLeaveType.Style["display"] = "none";
            divVacationChk.Style["display"] = "block";
            divVacationType.Style["display"] = "block";
            upForm.Update();
        }
        else
        {
            ddlLeaveType.Enabled = ddlLeaveType.Visible = true;
            ddlLeaveType.CssClass = "dropdownlist_mandatory";
            GetLeaveCount();
            lbDuration.Text = Convert.ToString(GetLocalResourceObject("Days.Text"));  //"Days";
            //chkCO.Checked = false;
            txtBalance.Visible = lblBalanceCombo.Visible = false;

            HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
            HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
            HtmlGenericControl divLeaveType = fvLeaveRequest.FindControl("divleaveType") as HtmlGenericControl;
            HtmlGenericControl divVacationChk = fvLeaveRequest.FindControl("divVacationChk") as HtmlGenericControl;
            HtmlGenericControl divLastVacation = fvLeaveRequest.FindControl("divLastVacation") as HtmlGenericControl;
            HtmlGenericControl divVacationType = fvLeaveRequest.FindControl("divVacationType") as HtmlGenericControl;
            divLeaveExtension.Style["display"] = "block";
            divTimeExtensionDay.Style["display"] = "none";
            divTimeExtensionMonth.Style["display"] = "none";
            divLeaveType.Style["display"] = "block";
            divVacationChk.Style["display"] = "none";
            divLastVacation.Style["display"] = "none";
            divVacationType.Style["display"] = "none";
            upForm.Update();
        }
        GetLeaveCount();
    }

    private void GetLeaveCount()
    {
        TextBox txtExtensionFromDate = (TextBox)fvLeaveRequest.FindControl("txtExtensionFromDate");
        DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtDuration");
        objRequest = new clsLeaveExtensionRequest();
        objRequest.EmployeeId = new clsUserMaster().GetEmployeeId();
        objRequest.CurrentDate = new clsCommon().GetSysDate();
        
        if (ddlLeaveType.Items.Count > 0)
        {
            objRequest.LeaveType = Convert.ToInt32(ddlLeaveType.SelectedValue);
            if (ddlExtensionType.SelectedValue == "1")
            {
                DataTable ds = objRequest.GetBalanceLeave();
                if (ds.Rows.Count > 0)
                {
                    double iLeaveCount = (Convert.ToInt32(ds.Rows[0]["BalanceLeave"]));
                    txtBalanceLeave.Text = (iLeaveCount).ToString();
                }
            }
            else
            {
                txtBalanceLeave.Text = string.Empty;
            }
        }
        else
        {
            txtBalanceLeave.Text = "0";
        }
    }

    protected void fvLeaveRequest_DataBound(object sender, EventArgs e)
    {
        HiddenField hfLoanDate = (HiddenField)fvLeaveRequest.FindControl("hfLoanDate");
        
        if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
        {
            Label lblDay = (Label)fvLeaveRequest.FindControl("lblDay");
            HiddenField hdExtensionType = (HiddenField)fvLeaveRequest.FindControl("hdExtensionType");
            //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
            if (lblDay != null)
            {
                if (hdExtensionType.Value.ToInt32() == 1 || hdExtensionType.Value.ToInt32() == 3)
                    lblDay.Text = Convert.ToString(GetLocalResourceObject("NoofDays.Text"));
                else
                    lblDay.Text = Convert.ToString(GetLocalResourceObject("BreakDuration.Text"));
            }
            //if (imgBack != null)
            //    imgBack.Visible = false;
        }

        objUser = new clsUserMaster();
        objRequest = new clsLeaveExtensionRequest();
        objLeaveRequest = new clsLeaveRequest();
        if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == false)
        {
            HtmlTableRow trForwardedBy = (HtmlTableRow)fvLeaveRequest.FindControl("trForwardedBy");
            if (trForwardedBy != null)
                trForwardedBy.Visible = false;
        }
        if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == true)
        {
            DropDownList ddlEditStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
            ddlEditStatus.DataSource = objLeaveRequest.GetAllRequestStatus(1);
            ddlEditStatus.DataBind();
            Label lblStatus = (Label)fvLeaveRequest.FindControl("lblStatus");
            Button btnApprove = (Button)fvLeaveRequest.FindControl("btnApprove");
            Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");
            TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
            Button btCancel = (Button)fvLeaveRequest.FindControl("btCancel");
            if (Convert.ToBoolean(ViewState["Approve"]) == true)
            {
                lblStatus.Visible = false;
                txtEditreason.Visible = true;
                lblReason.Visible = false;
                ddlEditStatus.Visible = true;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                btnApprove.Visible = true;
                btCancel.Visible = true;
            }
            else
            {
                txtEditreason.Visible = false;
                lblReason.Visible = true;
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                btnApprove.Visible = false;
                btCancel.Visible = false;
            }
            HiddenField hdExtensionType = (HiddenField)fvLeaveRequest.FindControl("hdExtensionType");
            int TypeID = 0;
            if (hdExtensionType != null)
            {
                switch (hdExtensionType.Value.ToInt32())
                {
                    case 1:
                        TypeID = Convert.ToInt32(RequestType.LeaveExtension);
                        break;
                    case 2:
                        TypeID = Convert.ToInt32(RequestType.TimeExtensionRequest);
                        break;
                    case 3:
                        TypeID = Convert.ToInt32(RequestType.VacationExtensionRequest);
                        break;
                }
            }
            if (clsLeaveRequest.IsHigherAuthority(TypeID, objUser.GetEmployeeId(), fvLeaveRequest.DataKey["RequestId"].ToInt32()))
            {

            }
            else
            {
                ddlEditStatus.Items.Remove(new ListItem("Pending", "4"));
            }
            //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = true;
        }
        //CANCELLATION BY HIGHER AUTHORITY
        else if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            DropDownList ddlEditStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
            ddlEditStatus.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetAllStatus(1) : objRequest.GetAllStatus(0)); //objRequest.GetAllStatus();
            ddlEditStatus.DataBind();
            Label lblStatus = (Label)fvLeaveRequest.FindControl("lblStatus");
            Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");
            Button btnRequestForCancel = (Button)fvLeaveRequest.FindControl("btnRequestForCancel");
            TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");

            if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
            {
                lblStatus.Visible = lblReason.Visible = ddlEditStatus.Enabled = false;
                txtEditreason.Visible = ddlEditStatus.Visible = btnRequestForCancel.Visible = true;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
            }
            else
            {
                lblStatus.Visible = lblReason.Visible = true;
                ddlEditStatus.Visible = txtEditreason.Visible = btnRequestForCancel.Visible = false;
            }
            //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = true;
        }
        if (fvLeaveRequest.CurrentMode == FormViewMode.Insert | fvLeaveRequest.CurrentMode == FormViewMode.Edit)
        {
            TextBox txtExtensionFromDate = (TextBox)fvLeaveRequest.FindControl("txtExtensionFromDate");
            TextBox txtMonthYear = (TextBox)fvLeaveRequest.FindControl("txtMonthYear");
            
            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            {
                txtExtensionFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtMonthYear.Text = DateTime.Now.ToString("MMM-yyyy");
            }
            objRequest.Mode = "GLT";
            objRequest.EmployeeId = objUser.GetEmployeeId();

            DropDownList ddlPeriod = (DropDownList)fvLeaveRequest.FindControl("ddlPeriod");
            DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");

            ddlExtensionType.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetExtensionType(1) : objRequest.GetExtensionType(0));
            ddlExtensionType.DataBind();
            //ddlExtensionType.SelectedValue = 3;

            DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlStatus");
            ddlStatus.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetAllStatus(1) : objRequest.GetAllStatus(0));
            ddlStatus.DataBind();

            DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
            ddlLeaveType.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetLeaveTypes(1) : objRequest.GetLeaveTypes(0));  //objRequest.GetLeaveTypes();
            ddlLeaveType.DataBind();

            if (Convert.ToBoolean(ViewState["Approve"]) == false && (fvLeaveRequest.CurrentMode == FormViewMode.Edit | fvLeaveRequest.CurrentMode == FormViewMode.Insert))
            {
                ddlStatus.Enabled = false;
                ddlStatus.CssClass = "dropdownlist_disabled";
            }
            CheckBox chkHalfDay = (CheckBox)fvLeaveRequest.FindControl("chkHalfDay");
            if (fvLeaveRequest.CurrentMode == FormViewMode.Edit)
            {
                if (fvLeaveRequest.DataKey["ExtensionTypeId"] != DBNull.Value)
                {
                    ddlExtensionType.SelectedIndex = ddlExtensionType.Items.IndexOf(ddlExtensionType.Items.FindByValue(Convert.ToString(fvLeaveRequest.DataKey["ExtensionTypeId"])));
                    ddlExtensionType.Enabled = false;
                }
                if (fvLeaveRequest.DataKey["StatusId"] != DBNull.Value)
                {
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvLeaveRequest.DataKey["StatusId"])));
                }
                if (fvLeaveRequest.DataKey["LeaveTypeId"] != DBNull.Value)
                {
                    ddlLeaveType.SelectedValue = Convert.ToString(fvLeaveRequest.DataKey["LeaveTypeId"]);
                }
                clsLeaveRequest request = new clsLeaveRequest();
                request.EmployeeId = objUser.GetEmployeeId();
            }
            if (Convert.ToInt32(ddlExtensionType.SelectedValue) == 1)
            {
                objRequest.EmployeeId = objUser.GetEmployeeId();
                int IsAdd = 0;
                if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                    IsAdd = 1;
                else
                    IsAdd = 0;

                ddlPeriod.DataSource = objRequest.GetFinancialYears(IsAdd);
                ddlPeriod.DataBind();
                GetLeaveCount();
                if (fvLeaveRequest.DataKey["FromDate"] != DBNull.Value && fvLeaveRequest.DataKey["FromDate"] != null)
                {
                    ddlPeriod.SelectedValue = fvLeaveRequest.DataKey["FromDate"].ToString();
                }               
                HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
                divLeaveExtension.Style["display"] = "block";
                divTimeExtensionDay.Style["display"] = "none";
                divTimeExtensionMonth.Style["display"] = "none";
            }
            else if (Convert.ToInt32(ddlExtensionType.SelectedValue) == 2)
            {
                HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
                divLeaveExtension.Style["display"] = "none";
                divTimeExtensionDay.Style["display"] = "block";
                divTimeExtensionMonth.Style["display"] = "none";
            }
            else if (Convert.ToInt32(ddlExtensionType.SelectedValue) == 3)
            {
                objRequest.EmployeeId = objUser.GetEmployeeId();
                int IsAdd = 0;
                if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                    IsAdd = 1;
                else
                    IsAdd = 0;

                HtmlGenericControl divLeaveExtension = fvLeaveRequest.FindControl("divLeaveExtension") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionDay = fvLeaveRequest.FindControl("divTimeExtensionDay") as HtmlGenericControl;
                HtmlGenericControl divTimeExtensionMonth = fvLeaveRequest.FindControl("divTimeExtensionMonth") as HtmlGenericControl;
                HtmlGenericControl divVacationChk = fvLeaveRequest.FindControl("divVacationChk") as HtmlGenericControl;

                TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtDuration");
                TextBox txtBalance = (TextBox)fvLeaveRequest.FindControl("txtBalance");
                Label lblBalanceCombo = (Label)fvLeaveRequest.FindControl("lblBalanceCombo");
                RadioButtonList rblVacationType = (RadioButtonList)fvLeaveRequest.FindControl("rblVacationType");
                
                divLeaveExtension.Style["display"] = "none";
                divTimeExtensionDay.Style["display"] = "none";
                divTimeExtensionMonth.Style["display"] = "none";
                divVacationChk.Style["display"] = "block";

                objRequest.RequestId = ViewState["ID"].ToInt32();
                chkCO.Checked = objRequest.GetIsFromCompensatoryOff();
                if (objRequest.GetModeofExtension())
                    rblVacationType.SelectedValue = "1";
                else
                    rblVacationType.SelectedValue = "0";
                if (chkCO.Checked)
                {
                    chkCO_CheckedChanged(null, null);
                }
                else
                {
                    lblBalanceCombo.Visible = false;
                    txtBalance.Visible = false;
                    txtDuration.Visible = true;
                }
                GetVacationExtensionCount(null, null);
            }
            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                hfLoanDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            ChangeVisibility();
            //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = false;
        }
    }

    protected void fvLeaveRequest_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            objRequest = new clsLeaveExtensionRequest();
            objUser = new clsUserMaster();
            objLeaveRequest = new clsLeaveRequest();
            int iRequestId = 0;
            DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
            DataTable dtRequestedIds = new DataTable();
            string sRequestedTo = string.Empty;
            int iRequestedById = 0;
            int Mode;
            string ExtensionType = "";
            DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
            Label lblExtensionType = (Label)fvLeaveRequest.FindControl("lblExtensionType");
            Label lblExtensionFromDate = (Label)fvLeaveRequest.FindControl("lblExtensionFromDate");
            Label lblDuration = (Label)fvLeaveRequest.FindControl("lblDuration");
            Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");
            TextBox txtEditReason = (TextBox)fvLeaveRequest.FindControl("txtEditReason");
            int TypeID = 0;
            if (ddlExtensionType != null || Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) > 0)
            {
                int TID = 0;
                if (ddlExtensionType != null)
                    TID = ddlExtensionType.SelectedValue.ToInt32();
                else if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) > 0)
                    TID = Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]);

                switch (TID)
                {
                    case 1:
                        TypeID = Convert.ToInt32(RequestType.LeaveExtension);
                        break;
                    case 2:
                        TypeID = Convert.ToInt32(RequestType.TimeExtensionRequest);
                        break;
                    case 3:
                        TypeID = Convert.ToInt32(RequestType.VacationExtensionRequest);
                        break;
                }
            }
            switch (e.CommandName)
            {
                case "Add":

                    bIsAdd = true;

                    string RequestedTo = clsLeaveRequest.GetRequestedTo(e.CommandArgument.ToInt32(), Convert.ToInt64(objUser.GetEmployeeId()), TypeID);
                    if (RequestedTo == string.Empty)
                    {
                        mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن معالجة طلبك في هذا الوقت. يرجى الاتصال بقسم الموارد البشرية." : "Your request cannot be processed at this time. Please contact HR department.");
                        mpeMessage.Show();
                    }
                    else
                    {
                        //DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
                        DropDownList ddlPeriod = (DropDownList)fvLeaveRequest.FindControl("ddlPeriod");
                        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
                        TextBox txtExtensionFromDate = (TextBox)fvLeaveRequest.FindControl("txtExtensionFromDate");
                        TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtduration");

                        objRequest = new clsLeaveExtensionRequest();
                        if (Convert.ToString(e.CommandArgument) != "")
                        {
                            iRequestId = Convert.ToInt32(e.CommandArgument);
                        }
                        objRequest.EmployeeId = objUser.GetEmployeeId();
                        objRequest.ExtensionType = Convert.ToInt32(ddlExtensionType.SelectedValue);
                        objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(txtExtensionFromDate.Text);

                        if ((fvLeaveRequest.CurrentMode == FormViewMode.Insert) || (fvLeaveRequest.CurrentMode == FormViewMode.Edit))
                        {
                            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                                Mode = 1;
                            else
                                Mode = 2;
                            if (ddlExtensionType.SelectedValue == "1")
                            {
                                objRequest.LeaveType = Convert.ToInt32(ddlLeaveType.SelectedValue);
                                if (ddlPeriod.SelectedItem.Text != null)
                                    objRequest.MonthYear = ddlPeriod.SelectedItem.Text.Split('-')[1];
                            }
                            else
                            {
                                objRequest.MonthYear = clsCommon.Convert2DateTime(txtExtensionFromDate.Text).ToString("dd-MMM-yyyy");//MMM
                            }
                            if ((ddlExtensionType.SelectedValue == "1") && (ddlLeaveType.SelectedValue == "3") && (clsLeaveExtensionRequest.CheckMaleOrFemale(objRequest.EmployeeId) == true))
                            {
                                //mcMessage.InformationalMessage("Maternity Leave can be aplied only for Female employees");
                                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "إجازة الأمومة يمكن تطبيقها فقط على الموظفات" : "Maternity Leave can be aplied only for Female employees");
                                mpeMessage.Show();
                                return;
                            }
                            else if (objRequest.PaymentReleaseChecking(0))
                            {
                                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يجب أن يكون دخول أكبر من تاريخ الافراج عنه الراتب" : "Entry must be greater than salary release date");
                                mpeMessage.Show();
                                return;
                            }
                            else
                            {
                                if (ddlExtensionType.SelectedValue == "1")
                                {
                                    if (ddlPeriod.SelectedItem.Text != null)
                                        objRequest.MonthYear = ddlPeriod.SelectedItem.Text.Split('-')[0];
                                }
                                if ((ddlExtensionType.SelectedValue == "2") && (objRequest.DuplicateChecking(Mode)))
                                {
                                    mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "آسف، لا يسمح قيم مكررة." : "Sorry, duplicate values are not allowed.");
                                    mpeMessage.Show();
                                }
                                else
                                {
                                    if (txtDuration.Text.ToDouble() > 0)
                                    {
                                        AddUpdateLeaveRequest(iRequestId);
                                    }
                                    else
                                    {
                                        if (ddlExtensionType.SelectedValue == "1" || ddlExtensionType.SelectedValue == "3")
                                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا ينبغي أن تكون الأيام أكبر من 0" : "No of Days should be greater than 0");
                                        else
                                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "وينبغي أن تكون مدة أكبر من 0" : "Duration should be greater than 0");
                                        mpeMessage.Show();
                                    }
                                }
                            }
                        }
                    }
                    break;
                case "Cancel":

                    if (hfPostedUrl.Value != string.Empty)
                        this.Response.Redirect(hfPostedUrl.Value);
                    else
                        this.Response.Redirect("Home.aspx");
                    break;

                case "CancelRequest":
                    bIsAdd = false;

                    if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                    {
                        ddlExtensionType.SelectedValue = "1";
                        LeaveTypeVisibility(null, null);
                        TextBox txtReason = (TextBox)fvLeaveRequest.FindControl("txtReason");
                        txtReason.Text = string.Empty;
                    }
                    upForm.Update();
                    upDetails.Update();
                    BindDataList();

                    break;
                case "Approve":
                    bIsAdd = false;
                    this.strFailureMessage = string.Empty;
                    objRequest = new clsLeaveExtensionRequest();
                    objUser = new clsUserMaster();
                    iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.ApprovedBy = objUser.GetEmployeeId().ToString();
                    objLeaveRequest.EmployeeId = objUser.GetEmployeeId();
                    // Label lblExtensionFromDate = (Label)fvLeaveRequest.FindControl("lblExtensionFromDate");

                    iRequestedById = objRequest.GetRequestedById();
                    sRequestedTo = objRequest.GetRequestedEmployees();
                    if (clsLeaveRequest.IsHigherAuthority(TypeID, objLeaveRequest.EmployeeId, objRequest.RequestId))
                    {
                        if (LeaveExtensionEntryValidations(iRequestId))
                        {
                            objRequest.UserID = objUser.GetUserId();
                            objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                            objRequest.ApprovedBy = objUser.GetEmployeeId().ToString();
                            objRequest.RequestId = iRequestId;
                            objRequest.Reason = txtEditReason.Text;
                            objRequest.UpdateLeaveStatus(true);
                            divPrint.Style["display"] = "block";
                            upnlMenus.Update();
                        }
                        else
                        {
                            return;
                        }
                        mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يتم تحديث طلب التمديد بنجاح." : "Extension request is updated successfully.");
                    }
                    else
                    {
                        divPrint.Style["display"] = "none";
                        upnlMenus.Update();
                        objRequest.Reason = txtEditReason.Text;
                        objRequest.UpdateLeaveStatus(false);

                        if (objRequest.StatusId == (int)RequestStatus.Rejected)
                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يتم تحديث طلب التمديد بنجاح." : "Extension request is updated successfully.");
                        else
                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يتم توجيه طلب التمديد بنجاح." : "Extension request is forwarded successfully.");
                    }
                    if (objRequest.StatusId == (int)RequestStatus.Approved)
                    {
                        //Leave Extension
                        if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Leave)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.Approved, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.Approved);
                        }
                        //Vacation extension request
                        else if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Vacation)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.Approved, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.Approved);
                        }
                        else //Time Extension
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.Approved, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.TimeExtension, eAction.Approved);
                        }
                    }
                    else if (objRequest.StatusId == (int)RequestStatus.Rejected)
                    {
                        if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Leave)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.Reject, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.Reject);
                        }
                        else if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Vacation)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.Reject, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.Reject);
                        }
                        else
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.Reject, "");
                            clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.TimeExtension, eAction.Reject);
                        }
                    }
                    else if (objRequest.StatusId == (int)RequestStatus.Pending)
                    {
                        if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Leave)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.Pending, "");
                        }
                        else if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Vacation)
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.Pending, "");
                        }
                        else
                        {
                            clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.Pending, "");
                        }
                    }
                    ViewState["Approve"] = false;
                    fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(iRequestId);
                    lnkDelete.OnClientClick = "return false;";
                    lnkDelete.Enabled = false;
                    mpeMessage.Show();
                    break;
                //Cancellation by HIGHER AUTHORITY 
                case "RequestForCancel":

                    objRequest = new clsLeaveExtensionRequest();
                    objUser = new clsUserMaster();
                    objLeaveRequest = new clsLeaveRequest();

                    iRequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.EmployeeId = objUser.GetEmployeeId();

                    if (CancelExtensionRequestValidations(objRequest.RequestId))
                    {
                        objRequest.RequestId = iRequestId;
                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                        objRequest.EmployeeId = objUser.GetEmployeeId();
                        objRequest.Reason = txtEditReason.Text;
                        objRequest.LeaveExtensionRequestCancelled();
                    }
                    else
                    {
                        return;
                    }
                    if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Leave)
                    {
                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.Cancelled, "");
                        clsCommonMail.SendMail(iRequestId, MailRequestType.LeaveExtension, eAction.Cancelled);
                    }
                    else if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == (int)eExtensionType.Vacation)
                    {
                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.Cancelled, "");
                        clsCommonMail.SendMail(iRequestId, MailRequestType.LeaveExtension, eAction.Cancelled);
                    }
                    else
                    {
                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.Cancelled, "");
                        clsCommonMail.SendMail(iRequestId, MailRequestType.TimeExtension, eAction.Cancelled);
                    }

                    mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "الطلب تفاصيل تحديثها بنجاح." : "Request details updated successfully.");
                    mpeMessage.Show();
                    ViewState["RequestForCancel"] = false;
                    BindRequestDetails(iRequestId);
                    break;


            }
        }
        catch (Exception)
        { }
    }

    /// <summary>
    /// update or insert leave request.
    /// </summary>
    /// <param name="iRequestId"></param>
    private void AddUpdateLeaveRequest(int iRequestId)
    {
        objUser = new clsUserMaster();
        objRequest = new clsLeaveExtensionRequest();
        objLeaveRequest = new clsLeaveRequest();

        bool bIsInsert = true;

        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
        DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
        TextBox txtExtensionFromDate = (TextBox)fvLeaveRequest.FindControl("txtExtensionFromDate");
        Label lblDuration = (Label)this.fvLeaveRequest.FindControl("lbDuration");
        TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtBalance");
        TextBox txtBalance = (TextBox)fvLeaveRequest.FindControl("txtduration");
        TextBox txtReason = (TextBox)fvLeaveRequest.FindControl("txtReason");
        DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlStatus");
        TextBox txtMonthYear = (TextBox)fvLeaveRequest.FindControl("txtMonthYear");
        DropDownList ddlPeriod = (DropDownList)fvLeaveRequest.FindControl("ddlPeriod");
        RadioButtonList rblVacationType = (RadioButtonList)fvLeaveRequest.FindControl("rblVacationType");
        Label lblPreviousVacation = (Label)fvLeaveRequest.FindControl("lblPreviousVacation");

        string strLeave = string.Empty;

        if (ddlExtensionType != null && ddlExtensionType.Enabled)
            strLeave = (strLeave.Trim() == string.Empty) ? ddlExtensionType.SelectedItem.Text.Trim().ToLower() : (strLeave + " " + ddlExtensionType.SelectedItem.Text.Trim().ToLower());

        if (lblDuration != null)
            strLeave = strLeave + " of " + txtDuration.Text.Trim() + " " + lblDuration.Text.ToLower();


        if (iRequestId > 0)
        {
            objRequest.Mode = "UPD";
            objRequest.RequestId = iRequestId;
            bIsInsert = false;
        }
        else
        {
            objRequest.Mode = "INS";
            bIsInsert = true;
        }

        objRequest.EmployeeId = objUser.GetEmployeeId();
        if (ddlExtensionType.SelectedItem.Value == "1")
            objRequest.LeaveType = Convert.ToInt32(ddlLeaveType.SelectedValue);
        else
            objRequest.LeaveType = 0;

        objRequest.ExtensionType = Convert.ToInt32(ddlExtensionType.SelectedValue);
        objRequest.NOD = Convert.ToDouble(txtBalance.Text);
        objRequest.Reason = txtReason.Text;
        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
        int type=0;
        switch (ddlExtensionType.SelectedItem.Value)
        { 
            case "1":
                type = (int)RequestType.LeaveExtension;
                break;
            case"2":
                type = (int)RequestType.TimeExtensionRequest;
                break;
            case "3":
                type = (int)RequestType.VacationExtensionRequest;
                break;
        }
        objRequest.RequestedTo = clsLeaveRequest.GetRequestedTo(iRequestId, objRequest.EmployeeId.ToInt64(), type);

        if (ddlExtensionType.SelectedValue == "1")
            objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(ddlPeriod.SelectedValue);
        else if (ddlExtensionType.SelectedValue == "2")
            objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(txtExtensionFromDate.Text);
        else if (ddlExtensionType.SelectedValue == "3")
            objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(lblPreviousVacation.Text).AddDays(1);
        else
            objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(txtMonthYear.Text);

        Boolean isVal = true;
        if (chkCO.Checked && ddlExtensionType.SelectedValue == "3" && rblVacationType.SelectedValue == "1")
        {
            objRequest.IsCredit = chkCO.Checked;
            if (txtBalance.Text.ToDecimal() > txtDuration.Text.ToDecimal())
            {
                isVal = false;
                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى التحقق مع التوازن التعويضية أوف ") : ("Please check with the balance Compensatory-Off");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
        }
        else
            objRequest.IsCredit = chkCO.Checked;
        if (chkCO.Checked && ddlExtensionType.SelectedValue == "3" && rblVacationType.SelectedValue == "0")
        {
            objRequest.IsCredit = chkCO.Checked;
            int ExtDays = clsLeaveExtensionRequest.GetExtendedDaysCount(objUser.GetEmployeeId().ToInt32(), ViewState["VID"].ToInt32());
            if (txtBalance.Text.ToDecimal() > ExtDays)
            {
                isVal = false;
                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب الأيام هو أكبر من ملحقات المتاحة مصنوعة") : ("Request days is greater than available extensions made");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
        }
        objRequest.VacancyID = ViewState["VID"].ToInt32();
        if (lblPreviousVacation.Text == "" && ddlExtensionType.SelectedValue == "3")
        {
            isVal = false;
            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن توسيع / ​​تقصير عطلة كما لا يوجد طلب اجازة " : "Cannot Extend/Shorten Vacation as No Vacation Request exists");
            mpeMessage.Show();
            return;
        }
        if (lblPreviousVacation.Text != "" && ddlExtensionType.SelectedValue == "3")
        {
            if (objRequest.DoesVacationRequestExist() > 0)
            {
                isVal = false;
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن توسيع / ​​تقصير عطلة لا تتم معالجة طلب اجازة " : "Cannot Extend/Shorten Vacation as your previous Vacation Request is not Approved");
                mpeMessage.Show();
                return;
            }
        }
        if (ddlExtensionType.SelectedValue == "3")
        {
            if (rblVacationType.SelectedValue == "0")
            {
                bool ComboChk = chkCO.Checked;
                bool Ext = rblVacationType.SelectedValue.ToInt32() == 0 ? false : true;
                int Chk = clsLeaveExtensionRequest.DoesExtendedVacationExist(objUser.GetEmployeeId().ToInt32(), ComboChk, Ext, objRequest.ExtensionFromDate, objRequest.NOD.ToInt32(), objRequest.VacancyID.ToInt32());
                if (Chk == 0 || Chk == 1)
                {
                    isVal = false;
                    mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن تقصير عطلة كما عدد الأيام تقصير تتجاوز عطلة" : "Cannot Shorten Vacation as number of shortened days exceed vacation");
                    mpeMessage.Show();
                    return;
                }
            }

            if (objRequest.DoesVacationExists().ToInt32() == 2)
            {//Attendance exists or vacation not processed
                isVal = false;
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن توسيع / ​​تقصير عطلة لا تتم معالجة طلب اجازة " : "Cannot Extend/Shorten Vacation as Vacation Request is not confirmed");
                mpeMessage.Show();
                return;
            }
            else if (objRequest.DoesVacationExists().ToInt32() == 1 && rblVacationType.SelectedValue == "1")
            {
                isVal = false;
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن توسيع عطلة كما لا وجود لها الحضور " : "Cannot Extend Vacation as Attendance does not exist");
                mpeMessage.Show();
                return;
            }
            else
            { }
        }
        if (rblVacationType.SelectedValue == "1")
            objRequest.IsExtension = true;
        else
            objRequest.IsExtension = false;

        if (isVal)
        {
            iRequestId = objRequest.InsertUpdateLeaveRequest();

            if (ddlExtensionType.SelectedValue.ToInt32() == (int)eExtensionType.Leave)//"1"
            {
                clsCommonMessage.SendMessage(objRequest.EmployeeId, iRequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.Applied, "");
                clsCommonMail.SendMail(iRequestId, MailRequestType.LeaveExtension, eAction.Applied);
            }
            else if (ddlExtensionType.SelectedValue.ToInt32() == (int)eExtensionType.Vacation)
            {
                clsCommonMessage.SendMessage(objRequest.EmployeeId, iRequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.Applied, "");
                clsCommonMail.SendMail(iRequestId, MailRequestType.LeaveExtension, eAction.Applied);
            }
            else
            {
                clsCommonMessage.SendMessage(objRequest.EmployeeId, iRequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.Applied, "");
                clsCommonMail.SendMail(iRequestId, MailRequestType.TimeExtension, eAction.Applied);
            }
            string sMessage;
            sMessage = (bIsInsert ? (clsGlobalization.IsArabicCulture() ? "الطلب تفاصيل أضاف بنجاح." : "Request details added successfully.") : (clsGlobalization.IsArabicCulture() ? "????? ?????? ??????? ?????." : "Request details updated successfully."));

            mcMessage.InformationalMessage(sMessage);
            mpeMessage.Show();
            upForm.Update();
            upDetails.Update();
            BindDataList();
            fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
            fvLeaveRequest.DataSource = null;
            fvLeaveRequest.DataBind();
        }
    }

    /// <summary>
    /// bind datalist with requests.
    /// </summary>
    /// 
    private void BindDataList()
    {
        objRequest = new clsLeaveExtensionRequest();
        objUser = new clsUserMaster();

        objRequest.Mode = "VIEW";
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.PageIndex = LeaveExtensionRequestPager.CurrentPage + 1;
        objRequest.PageSize = LeaveExtensionRequestPager.PageSize;
        DataTable oTable = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GelAllRequests(1) : objRequest.GelAllRequests(0)); //objRequest.GelAllRequests();
        if (oTable.DefaultView.Count > 0)
        {
            bIsAdd = false;
            lnkView.Enabled = true;
            dlLeaveRequest.DataSource = oTable;
            dlLeaveRequest.DataBind();
            LeaveExtensionRequestPager.Total = objRequest.GetRecordCount();
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLeaveRequest.ClientID + "');";
            lnkDelete.Enabled = true;
            fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
            fvLeaveRequest.DataSource = null;
            fvLeaveRequest.DataBind();
            LeaveExtensionRequestPager.Visible = true;
        }
        else
        {
            lnkView.Enabled = false;
            dlLeaveRequest.DataSource = null;
            dlLeaveRequest.DataBind();
            lnkDelete.Enabled = false;
            fvLeaveRequest.ChangeMode(FormViewMode.Insert);
            LeaveExtensionRequestPager.Visible = false;
        }
        upDetails.Update();
        upForm.Update();
        upnlMenus.Update();
    }

    /// <summary>
    /// bind selected request details in formview 
    /// </summary>
    /// <param name="iRequestId"></param>
    private void BindRequestDetails(int iRequestId)
    {
        objRequest = new clsLeaveExtensionRequest();
        objRequest.Mode = "EDT";
        objRequest.RequestId = iRequestId;
        ViewState["ID"] = iRequestId;
        fvLeaveRequest.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetRequestDetails(1) : objRequest.GetRequestDetails(0)); //objRequest.GetRequestDetails();
        fvLeaveRequest.DataBind();

        HtmlTableRow trleaveType = (HtmlTableRow)fvLeaveRequest.FindControl("trleaveType");
        if (trleaveType != null)
        {
            if (Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]) == 1)
            {
                trleaveType.Style["display"] = "table-row";
            }
            else
            {
                trleaveType.Style["display"] = "none";
            }
        }

        HtmlTableRow trAuthority = (HtmlTableRow)fvLeaveRequest.FindControl("trAuthority");
        DataList dl1 = (DataList)fvLeaveRequest.FindControl("dl1");
        if (trAuthority != null)
        {
            objRequest.RequestId = iRequestId;
            int exType=Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]);
            if (exType == 1)
                objRequest.ExtensionType = (int)RequestType.LeaveExtension;
            else if (exType == 2)
                objRequest.ExtensionType = (int)RequestType.TimeExtensionRequest;
            else
                objRequest.ExtensionType = (int)RequestType.VacationExtensionRequest;
                 
            
            //int a= Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]);;
            //int b=Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
            //int c = Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]);
            //int d = Convert.ToInt32(fvLeaveRequest.DataKey["StatusId"]);
            //int e = Convert.ToInt32(fvLeaveRequest.DataKey["RequestedTo"]);
            //int f = Convert.ToInt32(fvLeaveRequest.DataKey["FromDate"]);
            //int g = Convert.ToInt32(fvLeaveRequest.DataKey["MonthYear"]);
            objRequest.CompanyID = objRequest.GetCompany();
            DataTable dt2 = objRequest.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                trAuthority.Style["display"] = "table-row";
                dl1.DataSource = dt2;
                dl1.DataBind();
            }
        }

        //link to display approval level remarks
        HtmlTableRow trlink = (HtmlTableRow)fvLeaveRequest.FindControl("trlink");
        //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
        if (trlink != null)
            trlink.Style["display"] = "table-row";
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            //if (imgBack != null)
            //imgBack.Visible = true;
        }
        else
        {
            ViewState["RequestID"] = iRequestId;
            //if (imgBack != null)
            //imgBack.Visible = false;
        }
       
        dlLeaveRequest.DataSource = null;
        dlLeaveRequest.DataBind();
        LeaveExtensionRequestPager.Visible = false;
        upDetails.Update();
        upForm.Update();
        upnlMenus.Update();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;
        DropDownList ddlExtensionType = (DropDownList)fvLeaveRequest.FindControl("ddlExtensionType");
        TextBox txtReason = (TextBox)fvLeaveRequest.FindControl("txtReason");
        if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
        {
            ddlExtensionType.SelectedValue = "1";
            LeaveTypeVisibility(null, null);
            txtReason.Text = string.Empty;
        }
        fvLeaveRequest.ChangeMode(FormViewMode.Insert);
        dlLeaveRequest.DataSource = null;
        dlLeaveRequest.DataBind();
        LeaveExtensionRequestPager.Visible = false;
        lnkDelete.Enabled = false;
        lnkDelete.OnClientClick = "return false;";
        upForm.Update();
        upnlMenus.Update();
        upDetails.Update();
    }
    protected void dlLeaveRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        HiddenField hdExtensionTypeID = (HiddenField)e.Item.FindControl("hdExtensionTypeId");
        switch (e.CommandName)
        {
            case "_View":
                HiddenField hfLink = (HiddenField)fvLeaveRequest.FindControl("hfLink");
                HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                if (hfLink != null && trReasons != null)
                {
                    hfLink.Value = "0";
                    trReasons.Style["display"] = "none";
                }
                fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد من حذف هذه ؟');") : ("return confirm('Are you sure to delete this ?');"); //"return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;
                LeaveExtensionRequestPager.Visible = false;
                break;

            case "_Edit":

                fvLeaveRequest.ChangeMode(FormViewMode.Edit);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                LeaveExtensionRequestPager.Visible = false;
                break;

            case "_Cancel":

                ViewState["RequestCancel"] = true;
                objRequest = new clsLeaveExtensionRequest();
                if (hdExtensionTypeID != null)
                {
                    objRequest.RequestId = Convert.ToInt32(e.CommandArgument);
                    objRequest.StatusId = (int)RequestStatus.RequestForCancel;
                    objRequest.Reason = clsUserMaster.GetCulture() == "ar-AE" ? ("ترك طلب التمديد للإلغاء") : ("Leave Extension Request For Cancellation"); //"Leave Extension Request For Cancellation";
                    objRequest.UpdateCancelStatus();
                    if (hdExtensionTypeID.Value.ToInt32() == (int)eExtensionType.Leave)
                    {
                        clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.LeaveExtensionRequest, eMessageTypes.Leave__Extension_Request, eAction.RequestForCancel, "");
                        clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.RequestForCancel);
                    }
                    else if (hdExtensionTypeID.Value.ToInt32() == (int)eExtensionType.Vacation)
                    {
                        clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.VacationExtensionRequest, eMessageTypes.Vacation__Extension_Request, eAction.RequestForCancel, "");
                        clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.LeaveExtension, eAction.RequestForCancel);
                    }
                    else
                    {
                        clsCommonMessage.SendMessage(null, objRequest.RequestId, eReferenceTypes.TimeExtensionRequest, eMessageTypes.Time__Extension_Request, eAction.RequestForCancel, "");
                        clsCommonMail.SendMail(objRequest.RequestId, MailRequestType.TimeExtension, eAction.RequestForCancel);
                    }
                    ViewState["RequestCancel"] = false;
                    BindDataList();
                    lnkDelete.Enabled = false;
                    lnkDelete.OnClientClick = "return false;";
                }
                break;
        }
    

    }
    protected void dlLeaveRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
            HtmlTableRow trLeaveType = (HtmlTableRow)e.Item.FindControl("trLeaveType");
            Label lbDuration = (Label)e.Item.FindControl("lbDlDuration");
            HiddenField hdExtensionTypeID = (HiddenField)e.Item.FindControl("hdExtensionTypeId");
            HtmlTableRow dltrleaveType = (HtmlTableRow)e.Item.FindControl("trLeaveTypedl");

            if (hdExtensionTypeID.Value.ToInt32() == 1)
            {
                lbDuration.Text = Convert.ToString(GetLocalResourceObject("NoofDays.Text"));
                dltrleaveType.Style["display"] = "table-row";
            }
            else if (hdExtensionTypeID.Value.ToInt32() == 3)
            {
                lbDuration.Text = Convert.ToString(GetLocalResourceObject("NoofDays.Text"));
                dltrleaveType.Style["display"] = "none";
            }
            else
            {
                lbDuration.Text = Convert.ToString(GetLocalResourceObject("BreakDuration.Text")); //"Break Duration(Min)";
                dltrleaveType.Style["display"] = "none";
            }
            int iRequestId = 0;
            iRequestId = Convert.ToInt32(txtStatusId.Text);
            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
            HiddenField hdLeaveTypeID = (HiddenField)e.Item.FindControl("hdLeaveTypeID");
            if (iRequestId == 2 || iRequestId == 3 || iRequestId == 4 || iRequestId == 5 || iRequestId == 6 || iRequestId == 7 || Convert.ToBoolean(hdForwarded.Value) == true)
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }
            ImageButton btnCancel = (ImageButton)e.Item.FindControl("btnCancel");
            btnCancel.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm(' هل تريد إلغاء هذا الطلب؟');") : ("return confirm('Do you want to cancel this request?');");
            if ((iRequestId == (int)RequestStatus.Applied && Convert.ToBoolean(hdForwarded.Value) == false) || iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;
        BindDataList();
        upForm.Update();
        upnlMenus.Update();
        upDetails.Update();
    }
    #region DELETE
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objRequest = new clsLeaveExtensionRequest();
        string message = string.Empty;
        objUser = new clsUserMaster();

        if (dlLeaveRequest.Items.Count > 0)
        {
            foreach (DataListItem item in dlLeaveRequest.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkLeave");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {
                    objRequest.RequestId = Convert.ToInt32(dlLeaveRequest.DataKeys[item.ItemIndex]);
                    HiddenField hdExtensionTypeID = (HiddenField)dlLeaveRequest.FindControl("hdExtensionTypeId");
                    HiddenField hdLeaveTypeID = (HiddenField)dlLeaveRequest.FindControl("hdLeaveTypeID");

                    if (objRequest.IsExtensionRequestApproved())
                    {
                        objRequest.Mode = "DEL";
                        objRequest.DeleteRequest();
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب (ق) حذف بنجاح") : ("Request(s) deleted successfully"); //"Request(s) deleted successfully";
                        clsCommonMessage.DeleteMessages(dlLeaveRequest.DataKeys[item.ItemIndex].ToInt32(), eReferenceTypes.LeaveExtensionRequest);
                    }
                    else
                    {
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted."); //"Processing started request(s) cannot be deleted.";
                    }
                }
            }
            mcMessage.InformationalMessage(message);
            mpeMessage.Show();
        }
        else
        {
            if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
            {
                objRequest.RequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
                if (objRequest.IsExtensionRequestApproved())
                {
                    objRequest.Mode = "DEL";
                    objRequest.DeleteRequest();
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب (ق) حذف بنجاح") : ("Request(s) deleted successfully"); //"Request(s) deleted successfully";
                    clsCommonMessage.DeleteMessages(fvLeaveRequest.DataKey["RequestId"].ToInt32(), eReferenceTypes.LeaveExtensionRequest);
                }
                else
                {
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted."); //"Processing started request(s) cannot be deleted.";
                }
            }
        }
        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        BindDataList();

        upForm.Update();
        upnlMenus.Update();
        upDetails.Update();
    }
    #endregion
    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// LeaveExtensionEntryValidations done in approval of request
    /// </summary>
    /// <returns></returns>
    private bool LeaveExtensionEntryValidations(int RequestID)
    {
        objRequest = new clsLeaveExtensionRequest();
        Label lblExtensionFromDate = (Label)fvLeaveRequest.FindControl("lblExtensionFromDate");
        HiddenField hdExtensionType = (HiddenField)fvLeaveRequest.FindControl("hdExtensionType");

        if (hdExtensionType.Value == "1")
        {
            objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text.Split('-')[1]).ToString("dd-MMM-yyyy");//MMM
        }
        else
        {
            if (hdExtensionType.Value == "2" || hdExtensionType.Value == "3")
                objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text).ToString("dd-MMM-yyyy");//MMM
            else
                objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text).ToString("dd-MMM-yyyy");//MMM
        }
        objRequest.RequestId = RequestID;
        objRequest.EmployeeId = objRequest.GetRequestedById();
        if (objRequest.PaymentReleaseChecking(0) && hdExtensionType.Value != "3")
        {
            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "الراتب الافراج عن هذا الموظف. الموافقة غير ممكن" : "Salary released for this employee.Approval not possible");
            mpeMessage.Show();
            return false;
        }
        if (hdExtensionType.Value == "1")
        {
            if (clsLeaveExtensionRequest.CheckleavePolicyChanged(objRequest.EmployeeId, objRequest.RequestId))
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "ترك السياسة تغيرت لهذا الموظف. الموافقة غير ممكن" : "Leave policy changed for this employee.Approval not possible");
                mpeMessage.Show();
                return false;
            }
        }
        bool IsExt = objRequest.GetModeofExtension();
        if (hdExtensionType.Value == "3" && IsExt)
        {
            if (objRequest.DoesVacationExists().ToInt32() == 1)
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن توسيع عطلة كما لا وجود لها الحضور. الموافقة غير ممكن" : "Cannot Extend Vacation as Attendance does not exist.Approval not possible");
                mpeMessage.Show();
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// get label based on Extension Type
    /// </summary>
    /// <param name="oType"></param>
    /// <returns></returns>

    public string GetLabel(object oType)
    {
        string lbl = string.Empty;
        System.Globalization.CultureInfo cultInfo = new System.Globalization.CultureInfo("ar-AE");

        object obj = GetLocalResourceObject("Period.Text");

        if (Convert.ToInt32(oType) == 2 || Convert.ToInt32(oType) == 3)
        {
            lbl = Convert.ToString(GetLocalResourceObject("ExtensionFromDate.Text"));
        }
        else if (Convert.ToInt32(oType) == 1)
        {
            lbl = Convert.ToString(GetLocalResourceObject("Period.Text"));
        }
        return lbl;
    }

    /// <summary>
    /// LeaveExtensionDeleteValidations done in cancel request
    /// </summary>
    /// <returns></returns>
    private bool CancelExtensionRequestValidations(int RequestID)
    {
        objRequest = new clsLeaveExtensionRequest();
        Label lblExtensionFromDate = (Label)fvLeaveRequest.FindControl("lblExtensionFromDate");
        HiddenField hdExtensionType = (HiddenField)fvLeaveRequest.FindControl("hdExtensionType");
        HiddenField hdFmViewleaveTypeID = (HiddenField)fvLeaveRequest.FindControl("hdFmViewleaveTypeID");
        HiddenField HdNOofDays = (HiddenField)fvLeaveRequest.FindControl("HdNOofDays");

        if (hdExtensionType.Value == "1")
        {
            objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text.Split('-')[1]).ToString("dd-MMM-yyyy");//MMM
        }
        else
        {
            if (hdExtensionType.Value == "2" || hdExtensionType.Value == "3")
                objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text).ToString("dd-MMM-yyyy");//MMM
            else
                objRequest.MonthYear = Convert.ToDateTime(lblExtensionFromDate.Text).ToString("dd-MMM-yyyy");//MMM
        }
        objRequest.RequestId = RequestID;
        objRequest.EmployeeId = objRequest.GetRequestedById();
        if (hdExtensionType.Value == "2")
        {
            if (objRequest.PaymentReleaseChecking(0))
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكنك تغيير هذا التمديد لأن راتب شهر معالجتها بالفعل." : "You can't change this  Extension because month salary already processed.");
                mpeMessage.Show();
                return false;
            }
        }
        if (hdExtensionType.Value == "3")
        {
            if (clsLeaveExtensionRequest.ValueCheckPriorCLR(objRequest.RequestId.ToInt32()) == 0)
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكنك تغيير هذا التمديد لأن راتب شهر معالجتها بالفعل." : "Cannot Cancel Request");
                mpeMessage.Show();
                return false;
            }
        }
        if (hdExtensionType.Value == "1")
        {
            if (clsLeaveExtensionRequest.CheckLeavesTakenForEdit(new clsCommon().GetSysDate(), objUser.GetEmployeeId(), Convert.ToInt32(hdExtensionType.Value), Convert.ToInt32(hdFmViewleaveTypeID.Value), HdNOofDays.Value.ToInt32()))
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكنك تغيير هذا التمديد بسبب إجازة معالجتها بالفعل." : "You can't change this  Extension because leave already processed.");
                mpeMessage.Show();
                return false;
            }
        }
        return true;
    }

    //code for Compensatory-Off
    protected void chkCO_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtDuration");
            Label lblBalanceCombo = (Label)fvLeaveRequest.FindControl("lblBalanceCombo");
            TextBox txtBalance = (TextBox)fvLeaveRequest.FindControl("txtBalance");
            Label lblPreviousVacation = (Label)fvLeaveRequest.FindControl("lblPreviousVacation");
            RadioButtonList rblVacationType = (RadioButtonList)fvLeaveRequest.FindControl("rblVacationType");
            //UpdatePanel upnlVacationType = (UpdatePanel)fvLeaveRequest.FindControl("upnlVacationType");upnlchk
            UpdatePanel upnlchk = (UpdatePanel)fvLeaveRequest.FindControl("upnlchk");
            if (chkCO.Checked && ddlExtensionType.SelectedValue == "3")
            {
                lblBalanceCombo.Visible = txtBalance.Visible = true;
                objRequest.EmployeeId = objUser.GetEmployeeId().ToInt32();
                objRequest.ExtensionFromDate = clsCommon.Convert2DateTime(lblPreviousVacation.Text);
                if (rblVacationType.SelectedValue == "1")
                    txtBalance.Text = Convert.ToString(objRequest.GetCompensatoryOffCount());
                else if (rblVacationType.SelectedValue == "0")
                {
                    if (ViewState["VID"] != null)
                        txtBalance.Text = (clsLeaveExtensionRequest.GetExtendedDaysCount(objUser.GetEmployeeId().ToInt32(), ViewState["VID"].ToInt32())).ToString();
                }
            }
            else
            {
                txtDuration.Text = "";
                lblBalanceCombo.Visible = txtBalance.Visible = false;
            }
            upnlchk.Update();
        }
        catch { }
    }

    protected void rblVacationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        chkCO_CheckedChanged(new object(), new EventArgs());
    }
    protected void GetVacationExtensionCount(object sender, EventArgs e)
    {
        if (ddlExtensionType.SelectedValue == "3")
        {
            HiddenField hfVacationFromDate = (HiddenField)fvLeaveRequest.FindControl("hfVacationFromDate");
            HiddenField hfVacationRejoinDate = (HiddenField)fvLeaveRequest.FindControl("hfVacationRejoinDate");
            TextBox txtVacationExtensionToDate = (TextBox)fvLeaveRequest.FindControl("txtVacationExtensionToDate");
            TextBox txtDuration = (TextBox)fvLeaveRequest.FindControl("txtDuration");
            RadioButtonList rblVacationType = (RadioButtonList)fvLeaveRequest.FindControl("rblVacationType");
            Label lblPreviousVacation = (Label)fvLeaveRequest.FindControl("lblPreviousVacation");

            objLeaveRequest = new clsLeaveRequest();
            objUser = new clsUserMaster();

            objLeaveRequest.EmployeeId = objUser.GetEmployeeId();
            objLeaveRequest.CompanyID = objUser.GetCompanyId();
            objLeaveRequest.LeaveFromDate = clsCommon.Convert2DateTime(hfVacationFromDate.Value);
            objLeaveRequest.LeaveToDate = clsCommon.Convert2DateTime(lblPreviousVacation.Text);

            objLeaveRequest.VacationID = 0;
            string eligibleDays = objLeaveRequest.GetEligibleLeavePayDays(false);
            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert || fvLeaveRequest.CurrentMode == FormViewMode.Edit)
            {
                if (txtDuration.Text != string.Empty && txtVacationExtensionToDate.Text.Trim() == string.Empty)
                {
                    if (rblVacationType.SelectedValue == "1")
                    {
                        DateTime dtTo = Convert.ToDateTime(clsCommon.Convert2DateTime(lblPreviousVacation.Text.Trim()).ToString("MM/dd/yyyy"));
                        //DateTime date = dtTo.AddDays(Convert.ToDouble(txtDuration.Text.Trim())).ToDateTime();
                        txtVacationExtensionToDate.Text = (dtTo).AddDays(Convert.ToDouble(txtDuration.Text.Trim())).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        DateTime dtTo1 = Convert.ToDateTime(clsCommon.Convert2DateTime(hfVacationFromDate.Value.Trim()).ToString("MM/dd/yyyy"));
                        //DateTime date1 = dtTo1.AddDays(Convert.ToDouble(txtDuration.Text.Trim())).ToDateTime();
                        txtVacationExtensionToDate.Text = (dtTo1).AddDays(Convert.ToDouble(txtDuration.Text.Trim())).ToString("dd/MM/yyyy");
                    }
                }
                else
                {
                    if (rblVacationType.SelectedValue == "1")
                    {
                        if ((txtVacationExtensionToDate.Text.Trim() != string.Empty && clsCommon.Convert2DateTime(txtVacationExtensionToDate.Text) <= clsCommon.Convert2DateTime(lblPreviousVacation.Text)))
                        {
                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "تاريخ التمديد لا يمكن أن يكون أقل من  " + lblPreviousVacation.Text : "Extension date cannot be lesser than " + lblPreviousVacation.Text);
                            mpeMessage.Show();
                        }
                        else
                        {
                            DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblPreviousVacation.Text.Trim()).ToString("MMM dd yyyy"));
                            DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtVacationExtensionToDate.Text.Trim()).ToString("MMM dd yyyy"));
                            if (dt2.ToString() != "1/1/1900 12:00:00 AM")
                            {
                                TimeSpan ts = dt2.Subtract(dt1);
                                txtDuration.Text = (ts.Days).ToString();
                            }                           
                        }
                    }
                    else
                    {
                        if ((txtVacationExtensionToDate.Text.Trim() != string.Empty &&
                            clsCommon.Convert2DateTime(txtVacationExtensionToDate.Text) >= clsCommon.Convert2DateTime(lblPreviousVacation.Text) &&
                            clsCommon.Convert2DateTime(txtVacationExtensionToDate.Text) <= clsCommon.Convert2DateTime(hfVacationFromDate.Value)))
                        {
                            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يجب أن يكون تاريخ التمديد تقصير بين  " + lblPreviousVacation.Text + "and" + hfVacationFromDate.Value : "Extension Shorten date must be between " + lblPreviousVacation.Text + "and" + hfVacationFromDate.Value);
                            mpeMessage.Show();
                        }
                        else
                        {
                            DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(hfVacationFromDate.Value.Trim()).ToString("MMM dd yyyy"));
                            DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblPreviousVacation.Text.Trim()).ToString("MMM dd yyyy"));
                            DateTime dt3 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtVacationExtensionToDate.Text.Trim()).ToString("MMM dd yyyy"));
                            TimeSpan ts = dt2.Subtract(dt1);
                            TimeSpan ts1 = dt3.Subtract(dt1);

                            if ((ts1.Days).ToInt32() == 0)
                            {
                                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "عدد الأيام تقصير لا يمكن أن يكون نفس اجازة تاريخ البدء  " : "Number of shortened days cannot be same as Vacation Start date");
                                mpeMessage.Show();
                            }

                            if ((ts1.Days).ToInt32() < (ts.Days + 1).ToInt32() && (ts1.Days).ToInt32() > 0)
                            {
                                txtDuration.Text = (ts1.Days).ToString();
                            }
                            else
                            {
                                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "عدد الأيام تقصير أكبر من عدد المستحقين (" + (ts.Days).ToInt32() + ")" : "Number of shortened days is greater than eligible count (" + (ts.Days).ToInt32() + ")");
                                mpeMessage.Show();
                            }
                        }
                    }
                }
            }
        }
    }

    protected void imgback_Click(object source, EventArgs e)
    {
        if (hfPostedUrl.Value != null)
            Response.Redirect(hfPostedUrl.Value.ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsLeaveExtensionRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            objRequest.ExtensionType = Convert.ToInt32(fvLeaveRequest.DataKey["ExtensionTypeId"]);
            LinkButton lbReason = (LinkButton)fvLeaveRequest.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)fvLeaveRequest.FindControl("hfLink");
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                GridView gvReasons = (GridView)fvLeaveRequest.FindControl("gvReasons");
                trReasons.Style["display"] = "table-row";

                DataTable dt = objRequest.DisplayReasons();
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                trReasons.Style["display"] = "none";
            }
        }
    }
    private enum eExtensionType
    {
        Leave = 1,
        Time = 2,
        Vacation = 3
    }
}
