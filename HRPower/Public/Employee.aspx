﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Employee.aspx.cs" Inherits="Public_Employee" Title="Employee" %>

<%@ Register Src="../Controls/CandidateToEmployeeHistory.ascx" TagName="CandidateToEmployeeHistory"
    TagPrefix="uc1" %>

<script runat="server">

   
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>

        <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

        <link href="../css/employee.css" rel="stylesheet" type="text/css" />
        <%-- <ul>
            <li class="selected"><a href='Employee.aspx'><span>Employee </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="ViewPolicy.aspx">View Policies</a></li>--%>
        <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        <%--</ul> --%>
        <ul>
            <li class="selected"><a href='Employee.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
             <li><a href="EmployeeDutyRoaster.aspx">
               <%-- Duty Roaster--%>
                <asp:Literal ID="Literal56" runat="server" Text='<%$Resources:MasterPageCommon,DutyRoaster %>'>
                </asp:Literal>
            </a></li>
<%--           <li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div>
        <div style="display: none">
            <asp:Button ID="btnProxy" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:UpdatePanel ID="upEmployees" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="fvEmployee" runat="server" BorderWidth="0px" CellPadding="10" DataKeyNames="EmployeeID,CompanyID"
                Width="100%" OnDataBound="fvEmployee_DataBound" CellSpacing="10">
                <ItemTemplate>
                    <div id="mainwrap">
                        <div id="main" style="width: 97%; height:160px; background-color:rgb(145, 208, 238)">
                            <div id="names" style="width: 100%">
                                <div style="float: left; width: 49%">
                                    <h3>
                                        <%# Eval("Salutation")%>
                                        <%# Eval("FirstName")%>
                                        <%# Eval("MiddleName")%>
                                        <%# Eval("LastName")%>
                                    </h3>
                                </div>
                                <div style="float: right; width: 49%; text-align: right;">
                                    <h3>
                                        <%# Eval("FirstNameArb")%>
                                        <%# Eval("MiddleNameArb")%>
                                        <%# Eval("LastNameArb")%>
                                    </h3>
                                </div>
                            </div>
                            <div class="t1" style="margin-top: 30px">
                                <div class="ta1">
                                    <div class="common">
                                        <div class="innerdivheader" style="color: Black">
                                            <%--  Employee Number--%>
                                            <asp:Literal ID="Literal111" runat="server" Text='<%$Resources:ControlsCommon,EmployeeNumber%>'>
                                            </asp:Literal>
                                        </div>
                                        <div class="innerdivheader" style="color: Black">
                                            <%-- Working At--%>
                                            <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,WorkingAt %>'></asp:Literal>
                                        </div>
                                        <div class="innerdivheader" style="color: Black">
                                            <%--  Date Of Joining--%>
                                            <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,DateOfJoining %>'></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div id="ta2">
                                    <div class="common">
                                        <div class="innerdivcolon">
                                            :</div>
                                        <div class="innerdivcolon">
                                            :</div>
                                        <div class="innerdivcolon">
                                            :
                                        </div>
                                    </div>
                                </div>
                                <div class="ta3" style="width: 40%">
                                    <div class="common">
                                        <div class="innerdivheaderValue">
                                            <%# Eval("EmployeeNumber")%></div>
                                        <div class="innerdivheaderValue">
                                            <%# Eval("CompanyName")%>
                                        </div>
                                        <div class="innerdivheaderValue">
                                            <%#   Convert.ToDateTime(Eval("DateofJoining")).ToString("dd-MMM-yyyy")%>
                                        </div>
                                    </div>
                                </div>
                                <div class="ta4">
                                    <div id="RecentPhoto" style="float:right; padding: 0px; margin: 0px; border: 1px solid #F0F0F0;">
                                        <img src='<%# GetLogo(Eval("EmployeeID"),120)%>' style="margin: 2px;"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%--Basic Info --%>
                                    <asp:Literal ID="Literal48" runat="server" Text='<%$Resources:ControlsCommon,BasicInfo %>'></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--Gender--%>
                                    <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,Gender %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Eval("Gender") == "" ? "-" : Eval("Gender")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Date of birth--%>
                                    <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,DateOfBirth %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Eval("DateOfBirth") == "" ? "-" :Convert.ToDateTime(Eval("DateOfBirth")).ToString("dd-MMM-yyyy") %>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--Country--%>
                                    <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Country %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("CountryName")) == "" ? "-" : Eval("CountryName")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Nationality--%>
                                    <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,Nationality %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Nationality")) == "" ? "-" : Eval("Nationality")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Religion--%>
                                    <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,Religion %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Religion")) == "" ? "-" : Eval("Religion")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Employment Type--%>
                                    <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,EmploymentType %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EmploymentType")) == "" ? "-" : Eval("EmploymentType")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Probation End Date--%>
                                    <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,ProbationEndDate %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("ProbationEndDate")) == "" ? "-" : (Eval("ProbationEndDate").ToDateTime()).ToString("dd-MMM-yyyy")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%-- Work Profile --%>
                                    <asp:Literal ID="Literal45" runat="server" meta:resourcekey="WorkProfile"></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--Department--%>
                                    <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,Department%>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Department")) == "" ? "-" : Eval("Department")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Desigation--%>
                                    <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Designation")) == "" ? "-" : Eval("Designation")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--   Work Policy--%>
                                    <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:ControlsCommon,WorkPolicy %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("WorkPolicy")) == "" ? "-" : Eval("WorkPolicy")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Leave Policy--%>
                                    <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:ControlsCommon,LeavePolicy %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("LeavePolicy")) == "" ? "-" : Eval("LeavePolicy")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Work Location--%>
                                    <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:ControlsCommon,WorkLocation %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("WorkLocation")) == "" ? "-" : Eval("WorkLocation")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--   Work Status--%>
                                    <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:ControlsCommon,WorkStatus %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("WorkStatus")) == "" ? "-" : Eval("WorkStatus")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Official Email--%>
                                    <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:ControlsCommon,OfficialEmail %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("OfficialEmailID")) == "" ? "-" : Eval("OfficialEmailID")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <asp:Literal ID="Literal46" runat="server" meta:resourcekey="PersonalFacts"></asp:Literal>
                                    <%--  Personal Facts--%>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="FathersName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("FathersName")) == "" ? "-" : Eval("FathersName")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Mother Name--%>
                                    <asp:Literal ID="Literal38" runat="server" meta:resourcekey="MothersName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("MothersName")) == "" ? "-" : Eval("MothersName")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--Spouse Name--%>
                                    <asp:Literal ID="Literal39" runat="server" meta:resourcekey="SpouseName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("SpouseName")) == "" ? "-" : Eval("SpouseName")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Languages--%>
                                    <asp:Literal ID="Literal40" runat="server" meta:resourcekey="Languages"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("LanguagesKnown")) == "" ? "-" : Eval("LanguagesKnown")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Mother Tongue--%>
                                    <asp:Literal ID="Literal41" runat="server" meta:resourcekey="MotherTongue"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("MotherTongue")) == "" ? "-" : Eval("MotherTongue")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Identification--%>
                                    <asp:Literal ID="Literal42" runat="server" meta:resourcekey="Identification"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("IdentificationMarks")) == "" ? "-" : Eval("IdentificationMarks")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Blood Group--%>
                                    <asp:Literal ID="Literal43" runat="server" meta:resourcekey="BloodGroup"></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("BloodGroup")) == "" ? "-" : Eval("BloodGroup")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%--  Accounts & Bank Identity --%>
                                    <asp:Literal ID="Literal44" runat="server" meta:resourcekey="AccountInfo"></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Transaction Type--%>
                                    <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:ControlsCommon,TransactionType %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("TransactionType")) == "" ? "-" : Eval("TransactionType")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Employer Bank Name--%>
                                    <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankName %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("EmployerBank")) == "" ? "-" : Eval("EmployerBank")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Employer Bank A/C--%>
                                    <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankAcc %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EmployerAccountNo")) == "" ? "-" : Eval("EmployerAccountNo")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Employee Bank Name--%>
                                    <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankName %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("EmployeeBank")) == "" ? "-" : Eval("EmployeeBank")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Employee Bank A/C--%>
                                    <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankAcc %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EmployeeAccountNo")) == "" ? "-" : Eval("EmployeeAccountNo")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%--  Local Contact--%>
                                    <asp:Literal ID="Literal211" runat="server" Text='<%$Resources:ControlsCommon,LocalInfo %>'></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Address--%>
                                    <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:ControlsCommon,LocalAddress %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("LocalAddress")) == "" ? "-" : Eval("LocalAddress")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Phone--%>
                                    <asp:Literal ID="Literal28" runat="server" Text='<%$Resources:ControlsCommon,LocalPhone %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("LocalPhone")) == "" ? "-" : Eval("LocalPhone")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Mobile--%>
                                    <asp:Literal ID="Literal29" runat="server" Text='<%$Resources:ControlsCommon,LocalMobile %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Mobile")) == "" ? "-" : Eval("Mobile")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Other Info--%>
                                    <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:ControlsCommon,OtherInfo %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("Remarks")) == "" ? "-" : Eval("Remarks")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%-- Emergency Contact--%>
                                    <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:ControlsCommon,EmergencyInfo %>'></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Emergency Address--%>
                                    <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:ControlsCommon,EmergencyAddress %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("EmergencyAddress")) == "" ? "-" : Eval("EmergencyAddress")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Emergency Phone--%>
                                    <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:ControlsCommon,EmergencyPhone %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EmergencyPhone")) == "" ? "-" : Eval("EmergencyPhone")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%--Permanent Contact--%>
                                    <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:ControlsCommon,PermanentInfo%>'></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Address--%>
                                    <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:ControlsCommon,AddressLine1 %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("AddressLine1")) == "" ? "-" : Eval("AddressLine1")%>
                                </div>
                            </div>
                              <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Address--%>
                                    <asp:Literal ID="Literal52" runat="server" Text='<%$Resources:ControlsCommon,AddressLine2 %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;word-break: break-all;">
                                    <%# Convert.ToString(Eval("AddressLine2")) == "" ? "-" : Eval("AddressLine2")%>
                                </div>
                            </div>
                              <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Address--%>
                                    <asp:Literal ID="Literal53" runat="server" Text='<%$Resources:ControlsCommon,City %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("City")) == "" ? "-" : Eval("City")%>
                                </div>
                            </div>
                              <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Address--%>
                                    <asp:Literal ID="Literal54" runat="server" Text='<%$Resources:ControlsCommon,State %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("StateProvinceRegion")) == "" ? "-" : Eval("StateProvinceRegion")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Address--%>
                                    <asp:Literal ID="Literal55" runat="server" Text='<%$Resources:ControlsCommon,Zip%>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("ZipCode")) == "" ? "-" : Eval("ZipCode")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Permanent Phone--%>
                                    <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:ControlsCommon,PermanentPhone %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("Phone")) == "" ? "-" : Eval("Phone")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%-- Permanent Email--%>
                                    <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:ControlsCommon,PermanentEmail %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EmailID")) == "" ? "-" : Eval("EmailID")%>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <%--Vacation Info--%>
                                    <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:ControlsCommon,VacationInfo%>'></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Eligible Leaves--%>
                                    <asp:Literal ID="Literal50" runat="server" Text='<%$Resources:ControlsCommon,EligibleLeaves %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("EligibleLeaves")) == "" ? "-" : Eval("EligibleLeaves")%>
                                </div>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div style="float: left; width: 20%;">
                                    <%--  Vacation2--%>
                                    <asp:Literal ID="Literal51" runat="server" Text='<%$Resources:ControlsCommon,TotalEligibleLeavePayDays %>'></asp:Literal>
                                </div>
                                <div style="float: left; width: 5%;">
                                    :
                                </div>
                                <div style="float: left; width: 70%;">
                                    <%# Convert.ToString(Eval("TotalEligibleLeavePayDays")) == "" ? "-" : Eval("TotalEligibleLeavePayDays")%>
                                </div>
                            </div>
                        </div>
                        <!-- Display Employee KPI/KRA Details !-->
                        <%-- <div style="width: 100%;" class="labeltext">
                            <div style="float: left; width: 100%">
                                <b style="padding-bottom: 5px;" class="listHeader">
                                    <asp:Literal ID="Literal52" runat="server" Text="KPI/KRA"></asp:Literal>
                                </b>
                            </div>
                            <div style="float: left; width: 100%; margin-left: 20px;">
                                <div id="divKPI" runat="server" style="float: left; width: 47%;">
                                    <asp:GridView ID="gvKPI" runat="server" AutoGenerateColumns="false" Width="100%"
                                        CellPadding="10" BorderColor="#307296" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="datalistheader" />
                                        <Columns>
                                            <asp:BoundField DataField="Kpi" HeaderText="KPI" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="KPIValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div id="divKRA" runat="server" style="float: left; width: 47%; padding-left: 10px;">
                                    <asp:GridView ID="gvKRA" runat="server" AutoGenerateColumns="false" Width="100%"
                                        CellPadding="10" BorderColor="#307296" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle CssClass="datalistheader" />
                                        <Columns>
                                            <asp:BoundField DataField="Kra" HeaderText="KRA" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="KRAValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center"/>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>--%>
                    </div>
                </ItemTemplate>
            </asp:FormView>
            <div id="divEmployees" runat="server" style="display: block">
                <div id="divSort" runat="server" style="padding-left: 376px;display: block">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblSort" runat="server" meta:resourcekey="SortBy"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlExpression" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlExpression_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Value="1" meta:resourcekey="EmployeeNumber"></asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="EmployeeName"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlOrder" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged"
                                    AutoPostBack="true">
                                    <asp:ListItem Value="1" meta:resourcekey="Ascending"></asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="Descending"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:DataList ID="dlEmployees" runat="server" BorderWidth="0px" CellPadding="0" Width="100%"
                    OnItemCommand="dlEmployees_ItemCommand" OnItemDataBound="dlEmployees_ItemDataBound"
                    DataKeyField="EmployeeID">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                            <tr>
                                <td align="left" height="30" width="30" style="padding-left: 3px">
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkEmployee');" />
                                </td>
                                <td style="padding-left: 5px;" class="trLeft">
                                    <%--  Select All--%>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hidWorkStatusID" runat="server" Value='<%# Eval("WorkStatusId")%>' />
                        <table cellpadding="0" cellspacing="0" style="width: 100%" id="tblDetails" runat="server">
                            <tr>
                                <td valign="top" width="30" style="padding-top: 4px">
                                    <asp:CheckBox ID="chkEmployee" runat="server" />
                                </td>
                                <td style="padding: 2px" valign="top" width="105" rowspan="5">
                                    <img alt="" src='<%# GetLogo(Eval("EmployeeID"),100)%>' style="border: 1px solid #F0F0F0;
                                        padding: 2px" />
                                </td>
                                <td valign="top">
                                    <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                    <tr>
                                                        <td style="width: 500px">
                                                            <asp:LinkButton ID="btnEmployee" CausesValidation="false" runat="server" CommandArgument='<%# Eval("EmployeeID")%>'
                                                                CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("Salutation"), Eval("EmployeeFullName")) %>'
                                                                CssClass="listHeader bold"></asp:LinkButton>
                                                            &nbsp;[<%# Eval("EmployeeNumber")%>]
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgConfirm" CausesValidation="false" runat="server" ToolTip='<%$Resources:ControlsCommon,ConfirmProbation%>'
                                                                ImageUrl="~/images/confirm1_20x20.png" CommandArgument='<%# Eval("EmployeeID")%>'
                                                                CommandName="CONFIRM" />
                                                        </td>
                                                        <td valign="top" align="center">
                                                            <asp:ImageButton ID="btnEdit" CausesValidation="false" runat="server" ToolTip='<%$Resources:ControlsCommon,Edit%>'
                                                                ImageUrl="~/images/edit.png" CommandArgument='<%# Eval("EmployeeID")%>' CommandName="ALTER" />
                                                        </td>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgViewDoc" ToolTip='<%$Resources:ControlsCommon,ViewDocuments%>'
                                                                runat="server" CommandName="VIEWDOC" Enabled='<%#Convert.ToBoolean( Eval("count"))%>'
                                                                CausesValidation="false" CommandArgument='<%# Eval("EmployeeID") %>' ImageUrl="~/images/view_documents.png" />
                                                        </td>
                                                        <td align="center">
                                                            <asp:ImageButton ID="imgCandidateHistory" ToolTip='<%$Resources:ControlsCommon,History%>'
                                                                runat="server" CommandName="CandidateHistory" CausesValidation="false" CommandArgument='<%# Eval("EmployeeID") %>'
                                                                ImageUrl="~/images/view_history.png" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%">
                                                    <tr>
                                                        <td class="innerdivheader">
                                                            <%-- Working At--%>
                                                            <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ControlsCommon,WorkingAt %>'>
                                                          
                                                            </asp:Literal>
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("CompanyName")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader" width="150px">
                                                            <%-- Work Status--%>
                                                            <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ControlsCommon,WorkStatus %>'>
                                                          
                                                            </asp:Literal>
                                                        </td>
                                                        <td width="5">
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("WorkStatus")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="innerdivheader">
                                                            <%--  Designation--%>
                                                            <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'>
                                                          
                                                            </asp:Literal>
                                                        </td>
                                                        <td>
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("Designation")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="pgrEmployees" runat="server" OnFill="Bind" />
            </div>
            <div id="divCandidateHistory" runat="server" style="float: left; width: 100%;">
                <uc:CandidateToEmployeeHistory ID="Candidatehistorycontrol" runat="server"></uc:CandidateToEmployeeHistory>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="divNoData" runat="server" style="float: left; width: 100%;">
        <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"></asp:Label>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="dlEmployees" EventName="ItemCommand" />
                <asp:AsyncPostBackTrigger ControlID="fvEmployee" EventName="ItemCommand" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divPintDetails" runat="server" style="float: left; width: 100%;">
        <asp:UpdatePanel ID="upPrint" runat="server">
            <ContentTemplate>
                <div id="divPrint" runat="server" style="display: none">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
        <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px; margin-top: 5px;">
            <tr>
                <td colspan="2">
                    <asp:DropDownList ID="ddlCompany" Width="200px" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 65%">
                    <asp:TextBox ID="txtSearch" Style="margin-top: 5px" runat="server" CssClass="WmCss"
                        MaxLength="50"></asp:TextBox>
                </td>
                <td style="width: 35%; text-align: left; padding-left: 8px">
                    <asp:ImageButton ID="btnSearch" Style="margin-top: 5px" runat="server" CausesValidation="false"
                        SkinID="GoButton" ImageUrl="~/images/search.png" ToolTip="Click here to search"
                        ImageAlign="AbsMiddle" OnClick="btnSearch_Click" />
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                        WatermarkText='<%$Resources:ControlsCommon,SearchBy%>' WatermarkCssClass="WmCss">
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/New_Employee_Big.PNG" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddEmp" runat="server" OnClick="lnkAddEmp_Click">
                            <%--  Add Employee--%>
                            <asp:Literal ID="Literal47" runat="server" Text='<%$Resources:ControlsCommon,AddEmployee %>'>
                            
                            </asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkListEmp" runat="server" OnClick="lnkListEmp_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:ControlsCommon,ViewEmployee %>'>
                            
                            </asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" Text='<%$Resources:ControlsCommon,Delete %>' runat="server"
                            OnClick="btnDelete_Click"> 
                        <%--   Delete--%>
                           
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkPrint" Text='<%$Resources:ControlsCommon,Print %>' runat="server"
                            OnClick="btnPrint_Click">
                          <%-- Print--%>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkEmail" runat="server" Text='<%$Resources:ControlsCommon,Email %>'
                            OnClick="btnEmail_Click">
                        
                          <%--  Email--%>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListAllDocuments" ImageUrl="~/images/Document_list.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkListAllDocuments" runat="server" Text='<%$Resources:ControlsCommon,ListAllDocuments %>'
                            OnClick="lnkListAllDocuments_Click">
                        
                          <%--  Email--%>
                        </asp:LinkButton>
                    </h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
