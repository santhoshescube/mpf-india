﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Public_ViewEmployeeDocuments : System.Web.UI.Page
{
    #region DECLARATIONS
    clsViewDocument objclsViewDocument;
    clsUserMaster objUserMaster;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("ViewDocument").ToString();

        if (!IsPostBack)
        {
            
                      
            if (Request.QueryString["EmpId"] != null)
            {
                int intEmployeeId = Request.QueryString["EmpId"].ToInt32();
                if (intEmployeeId > 0)
                {
                    BindTree(intEmployeeId); 
                    TreeNode[] searchedNodes = tvEmployeeDocument.Nodes.Cast<TreeNode>().Where(r => r.Value.ToStringCustom().Split('@')[0] == intEmployeeId.ToString()).ToArray();
                    if (searchedNodes.Count() > 0)
                    {
                        tvEmployeeDocument.CollapseAll();
                        foreach (TreeNode CurrentNode in searchedNodes)
                        {
                            CurrentNode.Selected = true;
                            tvEmployeeDocument_SelectedNodeChanged(null, null);
                        }

                    }
                }
            }
        }
        objUserMaster = new clsUserMaster();
    }
    protected void tvEmployeeDocument_SelectedNodeChanged(object sender, EventArgs e)
    {
        if (tvEmployeeDocument.SelectedNode != null)
        {
            LoadEmpDocument();
            if (tvEmployeeDocument.SelectedNode.ChildNodes.Count > 0)
            {
                tvEmployeeDocument.SelectedNode.ExpandAll();
            }
        }
    }
    protected void dlDoctmentsFiles_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Download":
                Response.Redirect(ResolveUrl(ConfigurationManager.AppSettings.Get("_PHY_PATH").ToString() + e.CommandArgument));
                break;
        }
    }

    private void LoadEmpDocument()
    {

        if (tvEmployeeDocument.SelectedNode.Target.ToStringCustom() == "Employee" || tvEmployeeDocument.SelectedNode.Target.ToStringCustom() == "عامل")
        {
            if (tvEmployeeDocument.SelectedNode.ChildNodes.Count <= 0)
            {
                SetDocumentTypes(tvEmployeeDocument.SelectedNode.Value.ToStringCustom().Split('@')[0].ToInt32(), tvEmployeeDocument.SelectedNode);
            }
        }
        else if (tvEmployeeDocument.SelectedNode.Target.ToStringCustom() == "DocumentType")
        {
            if (tvEmployeeDocument.SelectedNode.ChildNodes.Count <= 0)
            {
                SetEmployeeDocuments(tvEmployeeDocument.SelectedNode.Parent.Value.ToStringCustom().Split('@')[0].ToInt32(), tvEmployeeDocument.SelectedNode.Value.ToStringCustom().Split('@')[1], tvEmployeeDocument.SelectedNode);
            }
        }
        else
        {
            string strHTMLSource = "";
            objclsViewDocument = new clsViewDocument();
            objclsViewDocument.Type = tvEmployeeDocument.SelectedNode.Target.ToStringCustom();
            objclsViewDocument.EmployeeID = tvEmployeeDocument.SelectedNode.Parent.Value.ToStringCustom().Split('@')[0].ToInt32();
            objclsViewDocument.DocumentID = tvEmployeeDocument.SelectedNode.Value.ToStringCustom().Split('@')[0].ToInt32();
            string s1 = Convert.ToString(GetGlobalResourceObject("DocumentsCommon", "Employee"));
            string strEmployee = clsGlobalization.IsArabicCulture() ? Convert.ToString(tvEmployeeDocument.SelectedNode.Parent.Parent.Text.ToStringCustom()) : s1 + " " + Convert.ToString(tvEmployeeDocument.SelectedNode.Parent.Parent.Text.ToStringCustom());
            DataSet dtsData = objclsViewDocument.GetDocumentDetails(4);
            if (dtsData.Tables.Count > 0)
            {
                if (objclsViewDocument.Type == ((int)DocumentType.Passport).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "Passport").ToString(), (int)DocumentType.Passport, strEmployee);
                }
                else if (objclsViewDocument.Type == ((int)DocumentType.Visa).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "Visa").ToString(), (int)DocumentType.Visa, strEmployee);
                }
                else if (objclsViewDocument.Type == ((int)DocumentType.Driving_License).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "DrivingLicense").ToString(), (int)DocumentType.Driving_License, strEmployee);
                }
                else if (objclsViewDocument.Type == ((int)DocumentType.Qualification).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "Qualification").ToString(), (int)DocumentType.Qualification, strEmployee);
                }
                else if (objclsViewDocument.Type == ((int)DocumentType.National_ID_Card).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "EmiratesCard").ToString(), (int)DocumentType.National_ID_Card, strEmployee);
                }
                else if (objclsViewDocument.Type == ((int)DocumentType.Insurance_Card).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "InsuranceCard").ToString(), (int)DocumentType.Insurance_Card, strEmployee);
                }
                //else if (objclsViewDocument.Type == ((int)DocumentType.Labour_Card).ToString())
                //{
                //    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "LabourCard").ToString(), (int)DocumentType.Labour_Card, strEmployee);
                //}
                else if (objclsViewDocument.Type == ((int)DocumentType.Health_Card).ToString())
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "HealthCard").ToString(), (int)DocumentType.Health_Card, strEmployee);

                }
                else //if (objclsViewDocument.Type == "0")
                {
                    strHTMLSource = GetDocumentHtml(dtsData, GetGlobalResourceObject("ControlsCommon", "OtherDocuments").ToString(), 0, strEmployee);
                }
                divContent.InnerHtml = strHTMLSource;
                divContent.Style["background-color"] = "#FFFFFF";
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        dlDoctmentsFiles.DataSource = dtsData.Tables[1];
                        dlDoctmentsFiles.DataBind();
                        divDocumentFiles.Style["background-color"] = "#FFFFFF";
                    }
                    else
                    {
                        dlDoctmentsFiles.DataSource = null;
                        dlDoctmentsFiles.DataBind();
                        divDocumentFiles.Style["background-color"] = "transparent";
                    }
                }
                else
                {
                    dlDoctmentsFiles.DataSource = null;
                    dlDoctmentsFiles.DataBind();
                    divDocumentFiles.Style["background-color"] = "transparent";
                }
                updivContent.Update();

            }
        }

    }
    protected string GetViewLink(object oDocumentAttachment)
    {
        return ConfigurationManager.AppSettings.Get("_VIRP_PATH").ToString() + Convert.ToString(oDocumentAttachment);
    }
    private void BindTree(int EmployeeID)
    {
        DataTable datData;
        objclsViewDocument = new clsViewDocument();
        clsUserMaster objUser = new clsUserMaster();
        objclsViewDocument.CompanyID = objUser.GetCompanyId();
        objclsViewDocument.EmployeeID = EmployeeID;
        tvEmployeeDocument.Nodes.Clear();
        datData = objclsViewDocument.GetEmployee();
        PopulateNodes(datData, tvEmployeeDocument.Nodes);
        //tvEmployeeDocument.ExpandAll();
        if (tvEmployeeDocument.Nodes.Count > 0)
        {
            if (tvEmployeeDocument.Nodes[0].ChildNodes.Count > 0)
            {
                tvEmployeeDocument.Nodes[0].ChildNodes[0].Selected = true;
                LoadEmpDocument();
            }
        }
    }
    private void SetDocumentTypes(int iParentID, TreeNode parentNode)
    {
        if (parentNode.Target == "Employee" || parentNode.Target == "عامل")
        {
            objclsViewDocument = new clsViewDocument();
            objclsViewDocument.EmployeeID = iParentID;

            DataTable datData = objclsViewDocument.GetDocumentTypes(2);
            if (datData.Rows.Count > 0)
            {
                PopulateNodes(datData, parentNode.ChildNodes);
            }
        }        
    }
    private void SetEmployeeDocuments(int iParentID, string strType, TreeNode parentNode)
    {
        if (parentNode.Target == "DocumentType")
        {
            objclsViewDocument = new clsViewDocument();
            objclsViewDocument.Type = strType;
            objclsViewDocument.EmployeeID = iParentID;
            objclsViewDocument.DocumentID = 0;
            DataSet dtsData = objclsViewDocument.GetDocuments(3);
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                PopulateNodes(dtsData.Tables[0], parentNode.ChildNodes);
            }
        }
    }
    private void PopulateAllDocument(string strType, TreeNode parentNode)
    {
        if (parentNode.Target == "Master")
        {
            objclsViewDocument = new clsViewDocument();

            objclsViewDocument.Type = strType;

            DataTable datData = objclsViewDocument.GetAllDocument();
            if (datData.Rows.Count > 0)
            {
                PopulateAllNodes(datData, parentNode.ChildNodes);
            }
        }
    }

    private void PopulateNodes(DataTable datData, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in datData.Rows)
        {
            if (dw["Index"].ToInt32() != 5)
            {
                TreeNode tn = new TreeNode();
                tn.Text = dw["Description"].ToString();
                tn.Value = dw["ID"].ToString() + "@" + dw["Index"].ToString();
                tn.Target = dw["Type"].ToString();
                nodes.Add(tn);
            }
        }
    }
    private void PopulateAllNodes(DataTable datData, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in datData.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dw["Description"].ToString();
            tn.Value = dw["ID"].ToString();
            tn.Target = dw["Type"].ToString();
            nodes.Add(tn);
        }
    }
    private string GetDocumentHtml(DataSet datSource, string MstrCaption, int intPolicyMode, string strEmployee)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:rgb(188, 205, 211);'";
        string strHeaderRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";


        string strRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:rgb(232, 238, 240)'";
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";

        string strRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        string strRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        string strRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInner50Style = "style='width:50%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; olor:#000000''";
        string strAltRowInner25Style = "style='width:25%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000''";
        string strAltRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        if (datSource.Tables[0].Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");
        if (strEmployee != "")
            MsbHtml.Append("<div   " + strSubHeaderStyle3 + ">" + strEmployee + "</div>");
        //MsbHtml.Append("<div   " + strSubHeaderStyle + ">General Information</div>");
        DataRow DrData;
        switch (intPolicyMode)
        {
            case (int)DocumentType.Passport:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("PassportInfo.Text") + "</div>");
                //MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">Employee Name</div><div " + strRowInnerRightStyle + ">" + DrData["EmployeeName"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("PassportNumber.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["PassportNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("NameInPassport.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["NameInPassport"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Placeofissue") + "</div><div " + strRowInnerRightStyle + ">" + DrData["PlaceOfIssue"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Country") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Country"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("PlaceofBirth.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["PlaceOfBirth"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Pagesavailable.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PassportPagesAvailable"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Renewed") + "</div><div " + strRowInnerRightStyle + ">" + DrData["RenewStatus"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div   " + strSubHeaderStyle + "> " + GetGlobalResourceObject("DocumentsCommon", "Dates") + " </div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Residence.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["ResidencePermits"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("OldPassport.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["OldPassportNumbers"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("EntryRestrictions.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["EntryRestrictions"].ToStringCustom() + "</div></div>");
                break;

            case (int)DocumentType.Visa:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("VisaInformation.Text").ToString() + "</div>");
                //MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">Employee Name</div><div " + strRowInnerRightStyle + ">" + DrData["EmployeeName"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("VisaNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["VisaNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "PlaceOfIssue").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PlaceOfIssue"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["VisaIssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["VisaExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Country").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueCountry"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["RenewStatus"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;

            case (int)DocumentType.Driving_License:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("DrivingLicenseInformation.Text") + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("DrivingLicenseNumber.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["LicenceNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("NameinLicense.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LicenceHoldersName"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("LicenseType.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["LicenceType"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LicensedCountry.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LicencedCountry"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</div><div " + strRowInnerRightStyle + ">" + DrData["RenewStatus"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
                break;

            case (int)DocumentType.Qualification:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("QualificationInformation.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("University.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["University"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("SchoolCollege.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["SchoolOrCollege"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Qualification.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Degree"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RefNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ReferenceNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("FromPeriod.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["FromDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ToPeriod.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ToDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("CertificateTitle.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CertificateTitle"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("GradePercentage.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["GradeorPercentage"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("CertificatesAttested.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CertificatesAttested"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("CertificatesVerified.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["CertificatesVerified"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("UniversityClearenceRequired.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["UniversityClearenceRequired"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ClearenceCompleted.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ClearenceCompleted"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;

            case (int)DocumentType.National_ID_Card:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("CardDetails.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Cardnumber").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CardNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PersonalNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IDNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "otherInfo").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
                break;

            case (int)DocumentType.Insurance_Card:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("InsuranceCardDetails.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "CardNumber").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CardNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("InsuranceCompany.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["InsuranceCompany"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["PolicyNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyName.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyName"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssuedBy").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssuedBy"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;

            //case (int)DocumentType.Labour_Card:

            //    DrData = datSource.Tables[0].Rows[0];
            //    MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("LabourCardDetails.Text").ToString() + "</div>");
            //    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Cardnumber").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CardNumber"].ToStringCustom() + "</div></div>");
            //    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PersonalNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["CardPersonalNumber"].ToStringCustom() + "</div></div>");
            //    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
            //    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
            //    MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
            //    MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "otherInfo").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
            //    break;

            case (int)DocumentType.Health_Card:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("HealthCardDetails.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Cardnumber").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["CardNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PersonalNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["CardPersonalNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "otherInfo").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
                break;

            case (int)DocumentType.Trading_License:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Sponsor.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["Sponsor"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("SponserFee.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["SponserFee"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("LicenseNumber.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["LicenceNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("City.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["City"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Province.Text") + "</div><div " + strRowInnerRightStyle + ">" + DrData["ProvinceName"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Partner.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Partner"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</div><div " + strRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;

            case (int)DocumentType.Lease_Agreement:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("LeaseAgreementDetails.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("AgreementNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["AgreementNo"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Lessor.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Lessor"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Startdate.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["StartDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Enddate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["EndDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("DocumentDate.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["DocumentDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeasePeriod.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeasePeriod"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("RentalValue.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["RentalValue"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("DepositHeld.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["DepositHeld"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Tenant.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Tenant"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ContactNumbers.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ContactNumbers"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("Address.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["Address"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LandLord.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Landlord"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("UnitNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["UnitNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeaseUnitType.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeaseUnitType"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("BuildingNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["BuildingNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Location.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Location"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetLocalResourceObject("PlotNumber.Text").ToString() + "</div><div " + strRowInnerRightStyle + ">" + DrData["PlotNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Total.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Total"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</div><div " + strRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;


            case 0:

                DrData = datSource.Tables[0].Rows[0];
                MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("GeneralDetails.Text").ToString() + "</div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "DocumentType") + "</div><div " + strRowInnerRightStyle + ">" + DrData["DocumentType"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OperationType") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["OperationType"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "DocumentNumber") + "</div><div " + strRowInnerRightStyle + ">" + DrData["DocumentNumber"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IssuedDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
                MsbHtml.Append("<div " + strRowStyle + "><div " + strRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</div><div " + strRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

                break;


        }
        return MsbHtml.ToString();
    }
}
