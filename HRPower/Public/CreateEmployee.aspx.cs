﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using HRAutoComplete;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
public partial class Public_CreateEmployee : System.Web.UI.Page
{
    #region Declarations
    private clsBindComboBox objBindCombo;
    private clsEmployee objEmployee;
    private clsEmployeeNew objEmployeeNew;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaster = new clsUserMaster();
    int delcount = 0;
    int iLeavePolicyId;
    private int CompanyId;
    private int BankerId;
    private bool Fromdb = false;
    private bool MblnUpdatePermission = true;  // Edit Button Permission In Datalist
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    public string drpSelectedValue = "";
    List<int> lst = new List<int>();
    //List<clsKPI> EmpKpi = new List<clsKPI>();
    //List<clsKRA> EmpKra = new List<clsKRA>();

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        rfFirstNameArbpnlTab1.Enabled = clsGlobalization.IsArabicViewEnabled();
        this.Title = GetGlobalResourceObject("ControlsCommon", "Employee").ToString();
        this.RegisterAutoComplete();

        if (!IsPostBack)
        {
            FillAllCombos();
            Load();
            ViewState["EmployeeId"] = Request.QueryString["EmpId"];
            if (ViewState["EmployeeId"] == null)
            {
                ViewState["Candidate"] = false;
               // txtEmployeeNumber.Text = clsEmployeeNew.GetEmployeeCode();

            }
            else
            {
                ViewState["Candidate"] = true;
                Fromdb = true;
                FillData();
            }

            if (Request.QueryString["EmpId"] == null)
            {
                if (Request.QueryString["CandidateId"] == null)
                    NewEmployee();
                else
                {
                    ViewState["Candidate"] = true;
                    clsCandidate objCandidate = new clsCandidate();
                    objCandidate.CandidateID = Convert.ToInt32(Request.QueryString["CandidateId"]);

                    DataSet ds = objCandidate.getCandidateDetails();

                    DataRow dr = ds.Tables[0].Rows[0];
                    txtFirstName.Text = Convert.ToString(dr["FirstNameEng"]);
                    txtMiddleName.Text = Convert.ToString(dr["SecondNameEng"]);
                    txtLastName.Text = Convert.ToString(dr["ThirdNameEng"]);
                    txtDateofBirth.Text = Convert.ToString(dr["DateofBirth"]);

                    txtFirstNameArb.Text = Convert.ToString(dr["FirstNameArb"]);
                    txtMiddleNameArabic.Text = Convert.ToString(dr["SecondNameArb"]);
                    txtLastNameArb.Text = Convert.ToString(dr["ThirdNameArb"]);


                    ddlSalutation.SelectedValue = (dr["SalutationID"] != DBNull.Value ? Convert.ToInt32(dr["SalutationID"]) : -1).ToString();


                    rblGender.SelectedIndex = Convert.ToBoolean(dr["Gender"]) ? 0 : 1;

                    //rblGender.SelectedIndex = rblGender.Items.IndexOf(rblGender.Items.FindByText(dr["Gender"].ToString()));

                    if (dr["ReligionID"] != DBNull.Value)
                        ddlReligion.SelectedValue = Convert.ToInt32(dr["ReligionID"]).ToString();

                    if (dr["CountryID"] != DBNull.Value)
                        ddlCountry.SelectedValue = Convert.ToInt32(dr["CountryID"]).ToString();

                    if (dr["CurrentNationalityID"] != DBNull.Value)
                        ddlNationality.SelectedValue = Convert.ToInt32(dr["CurrentNationalityID"]).ToString();

                    try
                    {
                        dr = ds.Tables[5].Rows[0];

                        if (dr["DepartmentID"] != DBNull.Value && Convert.ToInt32(dr["DepartmentID"]) > 0)
                            ddlDepartment.SelectedValue = Convert.ToInt32(dr["DepartmentID"]).ToString();

                        if (dr["DesignationId"] != DBNull.Value && Convert.ToInt32(dr["DesignationId"]) > 0)
                            ddlDesignation.SelectedValue = Convert.ToInt32(dr["DesignationId"]).ToString();

                        if (dr["EmploymentTypeId"] != DBNull.Value && Convert.ToInt32(dr["EmploymentTypeId"]) > 0)
                            ddlEmployementType.SelectedValue = Convert.ToInt32(dr["EmploymentTypeId"]).ToString();

                        if (dr["CompanyID"] != DBNull.Value && Convert.ToInt32(dr["CompanyID"]) > 0)
                        {
                            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dr["CompanyID"])));
                            CompanyId = ddlCompany.SelectedValue.ToInt32();

                            FillCompanyRelatedCombos();
                        }
                    }
                    catch { }

                    hdCandidateID.Value = Convert.ToString(Request.QueryString["CandidateId"]);
                }
            }
            SetPermission();

            //KPI/KRA
            //ClearControls();
            //BindInitials();
        }
        DesignationReference.OnSave -= new Controls_DesignationReference.saveHandler(DesignationReference_OnSave);
        DesignationReference.OnSave += new Controls_DesignationReference.saveHandler(DesignationReference_OnSave);
        ReferenceNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }
    void DesignationReference_OnSave(string SelectedValue)
    {
        BindDesignations();
        ddlDesignation.SelectedValue = SelectedValue;
        updDesignation.Update();
    }
    private void BindDesignations()
    {
        string str = string.Empty;
        ddlDesignation.DataSource = clsVacancyAddEdit.GetAllDesignations(objUserMaster.GetCompanyId(), (ViewState["JobID"].ToInt32() > 0) ? ViewState["JobID"].ToInt32() : 0);
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, new ListItem(str, "-1"));
    }
    protected string GetDate(object date)
    {
        if (date != DBNull.Value || Convert.ToString(date) != string.Empty)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        else
            return string.Empty;
    }

    private void DisableMenus()
    {
        this.btnPrint.Enabled = this.btnEmail.Enabled = this.btnDelete.Enabled = false;
        this.btnPrint.Style["Cursor"] = this.btnEmail.Style["Cursor"] = this.btnDelete.Style["Cursor"] = "default";
        this.upMenu.Update();
    }

    protected string GetEmployeeRecentPhotoNew(object EmployeeID, object RecentPhoto, int width)
    {
        if (EmployeeID != null)
        {
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }
    }

    protected string GetEmployeePassportPhotoNew(object EmployeeID, object PassportPhoto, int width)
    {
        if (EmployeeID != null)
        {
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }
    }

    public void ShowpopUpErrorMessage(string message)
    {
        controls_Message ucMessage = (controls_Message)Page.LoadControl("~/controls/Message.ascx");
        if (ucMessage != null)
        {
            pnlMessage.Controls.Add(ucMessage);
            ucMessage.ModalPopupId = "mpeMessage";
            ucMessage.InformationalMessage(message);
            mpeMessage.Show();
            updMessage.Update();
        }
    }

    private void FillData()
    {
        objEmployee = new clsEmployee();

        objEmployee.EmployeeID = Convert.ToInt32(ViewState["EmployeeId"]);
        using (DataTable dt = objEmployee.GetEmployee().Tables[0])
        {
            DataRow dr = dt.Rows[0];
            if (dt.Rows.Count > 0)
            {
                EnableOrDiableUserCredentail(true);
                CompanyId = dt.Rows[0]["CompanyID"].ToInt32();

                if (dr["SalutationId"] != DBNull.Value)
                    ddlSalutation.SelectedValue = dr["SalutationId"].ToString();
                txtFirstName.Text = dr["FirstName"].ToString();
                txtFirstNameArb.Text = dr["FirstNameArb"].ToString();
                txtEmployeeNumber.Text = dr["EmployeeNumber"].ToString();
                if (Convert.ToString(dr["IsEmpNoEditable"]) != "" )
                txtEmployeeNumber.Enabled = !(Convert.ToBoolean(dr["IsEmpNoEditable"])); 
                txtMiddleName.Text = Convert.ToString(dr["MiddleName"]);
                txtMiddleNameArabic.Text = dr["MiddleNameArb"].ToString();

                txtLastName.Text = Convert.ToString(dr["LastName"]);
                txtLastNameArb.Text = dr["LastNameArb"].ToString();

                if (Convert.ToBoolean(dr["IsMale"]))
                    rblGender.Items[0].Selected = true;
                else
                    rblGender.Items[1].Selected = true;

                txtDateofBirth.Text = dr["DateOfBirth"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (dr["CompanyID"] != DBNull.Value)
                    ddlCompany.SelectedValue = Convert.ToString(dr["CompanyID"]);




                ddlCompany.Enabled = false;

                if (dr["CountryID"] != DBNull.Value)
                    ddlCountry.SelectedValue = dr["CountryID"].ToString();

                if (dr["NationalityId"] != DBNull.Value)
                    ddlNationality.SelectedValue = dr["NationalityId"].ToString();

                if (dr["ReligionId"] != DBNull.Value)
                    ddlReligion.SelectedValue = dr["ReligionId"].ToString();

                if (dr["EmploymentTypeID"] != DBNull.Value)
                    ddlEmployementType.SelectedValue = Convert.ToString(dr["EmploymentTypeID"]);

                if (dr["MothertongueID"] != DBNull.Value)
                    ddlMotherTongue.SelectedValue = Convert.ToString(dr["MothertongueID"]);

                txtDateofJoining.Text = dr["DateOfjoining"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                if (clsEmployeeNew.IsAttendanceExists(objEmployee.EmployeeID.ToInt32()))
                    txtDateofJoining.Enabled = false;
                else
                    txtDateofJoining.Enabled = true;

                if (dr["WorkStatusId"] != DBNull.Value)
                    ddlWorkStatus.SelectedValue = Convert.ToString(dr["WorkStatusId"]);


                txtProbationEndDate.Enabled = ddlWorkStatus.SelectedValue.ToInt32() == 7;
                updWorkStatus.Update();

                ViewState["IsSettled"] = Convert.ToString(dr["IsSettled"]);
                ViewState["WorkStatusId"] = Convert.ToString(dr["WorkStatusId"]);

                if (Request.QueryString["Action"] != null)
                    ddlWorkStatus.SelectedValue = "6";

                if (dr["WorkStatusId"].ToInt32() < 6)
                {
                    if (dr["IsSettled"].ToInt32() == 1)
                    {
                        ddlWorkStatus.Enabled = false;
                        btnWorkStatus.Enabled = false;
                    }
                    else
                    {
                        ddlWorkStatus.Enabled = true;
                        btnWorkStatus.Enabled = true;
                    }
                }
                else
                {
                    ddlWorkStatus.Enabled = true;
                    btnWorkStatus.Enabled = true;
                }


                if (ddlWorkStatus.SelectedValue.ToInt32() == 7)
                    txtProbationEndDate.Text = dr["ProbationEndDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                else
                    txtProbationEndDate.Text = "";

                if (dr["DepartmentId"] != DBNull.Value)
                    ddlDepartment.SelectedValue = Convert.ToString(dr["DepartmentId"]);

                FillCompanyRelatedCombos();


                if (dr["DesignationId"] != DBNull.Value)
                    ddlDesignation.SelectedValue = Convert.ToString(dr["DesignationId"]);


                if (dr["WorkLocationID"] != DBNull.Value)
                    ddlWorkLocation.SelectedValue = dr["WorkLocationID"].ToString();

                if (dr["WorkPolicyID"] != DBNull.Value)
                    ddlPolicy.SelectedValue = Convert.ToString(dr["WorkPolicyID"]);

                if (dr["LeavePolicyID"] != DBNull.Value)
                    ddlLeavePolicy.SelectedValue = Convert.ToString(dr["LeavePolicyID"]);

                GetReportingTo();
                if (dr["ReportingTo"] != DBNull.Value)
                    ddlReportingTo.SelectedValue = Convert.ToString(dr["ReportingTo"]);

                GetHOD();
                if(dr["HOD"] != DBNull.Value)
                    ddlHOD.SelectedValue = Convert.ToString(dr["HOD"]);
                if (dr["ProcessTypeID"] != DBNull.Value)
                    ddlProcessType.SelectedValue = Convert.ToString(dr["ProcessTypeID"]);
                txtDeviceUserID.Text = Convert.ToString(dr["DeviceUserID"]);
                txtOfficialEmail.Text = Convert.ToString(dr["OfficialEmailID"]);
                txtOfficialEmailPassword.Attributes["value"] = Convert.ToString(dr["OfficialEmailPassword"]);

                //if (dr["RoleID"] != DBNull.Value && dr["RoleID"].ToInt32() >0)
                //{
                //    ddlRole.SelectedValue = dr["RoleId"].ToString();
                //    txtusername.Text = Convert.ToString(dr["UserName"]);
                //    string DecryptionKey = ConfigurationManager.AppSettings["_ENCRYPTION"];
                //    txtpassword.Attributes["value"] = clsCommon.Decrypt(Convert.ToString(dr["Password"]).ToString(), DecryptionKey);
                //}

                txtFathersName.Text = dr["FathersName"].ToString();
                txtMothersName.Text = dr["MothersName"].ToString();
                txtSpouseName.Text = dr["SpouseName"].ToString();

                txtAppraisalDate.Text = dr["NextAppraisalDate"].ToString() == "" ? DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : dr["NextAppraisalDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture); 

                //if (dr["MotherTongueId"] != DBNull.Value)
                //    ddlMotherTongue.SelectedValue = dr["MotherTongueID"].ToString();

                txtIdentification.Text = dr["IdentificationMarks"].ToString();
                ddlBloodGroup.SelectedValue  = Convert.ToString(dr["BloodGroup"]);

                if (dr["TransactionType"] != DBNull.Value)
                    ddlTransactionType.SelectedValue = Convert.ToString(dr["TransactionTypeId"]);

                if (dr["EmployerBankID"] != DBNull.Value)
                    ddlEmployerBankName.SelectedValue = Convert.ToString(dr["EmployerBankID"]);

                OnEmployerBankChange();
                if (dr["EmployeeBankID"] != DBNull.Value)
                    ddlEmployeeBankName.SelectedValue = Convert.ToString(dr["EmployeeBankID"]);
                if (dr["EmployerAccountID"] != DBNull.Value)
                    ddlCompanyAccount.SelectedValue = Convert.ToString(dr["EmployerAccountID"]);

                txtAccountNumber.Text = Convert.ToString(dr["EmployeeAccountNo"]);


                txtAddressLine1.Text = Convert.ToString(dr["AddressLine1"]);
                txtAddressLine2.Text = Convert.ToString(dr["AddressLine2"]);
                txtCity.Text = Convert.ToString(dr["City"]);
                txtState.Text = Convert.ToString(dr["StateProvinceRegion"]);
                txtZip.Text = Convert.ToString(dr["ZipCode"]);




                txtPermanentPhone.Text = Convert.ToString(dr["Phone"]);

                txtLocalAddress.Text = Convert.ToString(dr["LocalAddress"]);
                txtLocalPhone.Text = Convert.ToString(dr["LocalPhone"]);


                txtEmergencyAddress.Text = Convert.ToString(dr["EmergencyAddress"]);
                txtEmergencyPhone.Text = Convert.ToString(dr["EmergencyPhone"]);



                txtLocalMobile.Text = Convert.ToString(dr["Mobile"]);

                txtEmail.Text = Convert.ToString(dr["EmailID"]);
                txtOtherInfo.Text = dr["Remarks"].ToString();

                //if (dr["UserID"] != DBNull.Value)
                //{
                //    if (Convert.ToInt32(dr["UserID"]) > 0)
                //    {
                //        chkUser.Enabled = false;
                //        chkUser.Checked = true;
                //        ddlRole.SelectedValue = Convert.ToString(dr["RoleID"]);
                //        txtusername.Text = Convert.ToString(dr["UserName"]);
                //        txtusername.Enabled = txtpassword.Enabled = ddlRole.Enabled = true;
                //    }
                //    else
                //    {
                //        chkUser.Enabled = true ;
                //        chkUser.Checked = false ;
                //        ddlRole.Enabled = txtusername.Enabled = txtpassword.Enabled = false;
                //    }
                //}

                if (dr["RecentPhoto"] != DBNull.Value)
                {
                    imgRecentPhotoPreview.ImageUrl = GetEmployeeRecentPhotoNew(ViewState["EmployeeId"], dr["RecentPhoto"], 120);
                    lnkremoveRecentPhoto.Visible = true;
                    ViewState["Buffer"] = dr["RecentPhoto"];
                }
                objEmployeeNew = new clsEmployeeNew();
                objEmployeeNew.EmployeeID = ViewState["EmployeeId"].ToInt32();
                DataTable dtLanguages = objEmployeeNew.GetEmployeeLanguages();
                if (dtLanguages.Rows.Count > 0)
                {
                    for (int i = 0; i < dtLanguages.Rows.Count; i++)
                    {
                        if (ddlLanguage.Items.Count > 0)
                        {
                            foreach (ListItem item in ddlLanguage.Items)
                            {
                                if (item.Value == dtLanguages.Rows[i].ItemArray[0].ToString())
                                    item.Selected = true;
                            }
                        }
                    }
                }
            }
        }
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        ViewState["Candidate"] = false;
        Response.Redirect("CreateEmployee.aspx");
    }

    private void SaveLeavepolicystructure(int iEmpID, bool bStatus)
    {
        if (bStatus == true)
            objEmployeeNew.Fromdate = clsCommon.Convert2DateTime(txtDateofJoining.Text.Trim()).ToString("dd-MMM-yyyy");
        else
            objEmployeeNew.Fromdate = DateTime.Now.Date.ToString("dd-MMM-yyyy");

        objEmployeeNew.SaveLeavepolicy();
    }

    protected void fvEmployee_DataBound(object sender, EventArgs e)
    {
        upMenu.Update();
        SetUserEnability();
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        CompanyId = ddlCompany.SelectedValue.ToInt32();
        DataTable dtSettings = clsEmployeeNew.GetEmployeeCode(CompanyId);
        txtEmployeeNumber.Text = string.Empty;
        txtEmployeeNumber.Enabled = true;
        if (dtSettings .Rows.Count > 0)
        {
            txtEmployeeNumber.Text = Convert.ToString(dtSettings.Rows[0]["Prefix"]);
            if (Convert.ToString(dtSettings.Rows[0]["IsAuto"]) != null || Convert.ToString(dtSettings.Rows[0]["IsAuto"]) != "")
                txtEmployeeNumber.Enabled = !(Convert.ToBoolean(dtSettings.Rows[0]["IsAuto"]));
        }
        updEmpNo.Update();
        FillCompanyRelatedCombos();
    }

    private void FillCompanyRelatedCombos()
    {
        try
        {
            FillComboWorkPolicy();
            FillComboLeavePolicy();
            FillComboEmployerBankersName();
            FillComboCompanyAccounts();
            FillComboWorkLocation();

            GetDesignation();

            GetCompanyWisePermission();


            GetReportingTo();
            GetHOD();

        }
        finally
        {
            objEmployee = null;
        }
    }

    private void FillComboRoles()
    {
        //ddlRole.DataSource = objEmployee.GetAllRoles();
        //ddlRole.DataBind();
        //ddlRole.Items.Insert(0, new ListItem("--Select--", "-1"));
    }

    private void Load()
    {
        try
        {
            objEmployee = new clsEmployee();
            hdCandidateID.Value = string.Empty;
            this.DeleteImage();
            filRecentPhoto.ClearFileFromPersistedStore();
            hdFinYearStartDate.Value = objEmployee.GetFinancialYearStartDate().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            hdCurrentDate.Value = clsEmployeeNew.GetCurrentDate().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtDateofJoining.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            hdDefaultProbationPeriod.Value = objEmployee.GetConfigurationValue("DefaultProbationPeriod");

            divDeviceUser.Style["display"] = objEmployee.GetConfigurationValue("EnableLiveAttendance").ToUpper() == "YES" ? "block" : "none";

            if (hdDefaultProbationPeriod.Value != string.Empty)
                txtProbationEndDate.Text = DateTime.Now.AddDays(Convert.ToDouble(hdDefaultProbationPeriod.Value)).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtAppraisalDate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            imgRecentPhotoPreview.ImageUrl = GetEmployeeRecentPhotoNew(null, null, 120);
            SetUserEnability();
            FillComboRoles();
            EnableOrDiableUserCredentail(true);
        }
        catch (Exception ex)
        {
        }
        finally
        {
            objEmployee = null;
        }
    }

    protected void ddlTransactionType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlEmployerBankName.Enabled = true;
        switch (ddlTransactionType.SelectedValue)
        {

            case "1": //Bank
                ddlEmployerBankName.CssClass = "dropdownlist_mandatory";
                ddlCompanyAccount.CssClass = "dropdownlist_mandatory";
                ddlEmployeeBankName.CssClass = "dropdownlist_mandatory";
                txtAccountNumber.CssClass = "textbox_mandatory";
                break;
            case "3": //WPS
                ddlEmployerBankName.CssClass = "dropdownlist_mandatory";
                ddlCompanyAccount.CssClass = "dropdownlist_mandatory";
                ddlEmployeeBankName.CssClass = "dropdownlist_mandatory";
                txtAccountNumber.CssClass = "textbox_mandatory";
                break;
            default:
                ddlEmployerBankName.CssClass = "dropdownlist";
                ddlCompanyAccount.CssClass = "dropdownlist";
                ddlEmployeeBankName.CssClass = "dropdownlist";
                txtAccountNumber.CssClass = "textbox";
                ddlEmployerBankName.SelectedIndex = -1;
                ddlEmployeeBankName.SelectedIndex = -1;
                ddlCompanyAccount.SelectedIndex = -1;
                txtAccountNumber.Text = string.Empty;
                ddlEmployerBankName.Enabled = false;
                OnEmployerBankChange();
                break;
        }
    }

    protected void ddlEmployerBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        OnEmployerBankChange();
    }

    private void OnEmployerBankChange()
    {
        FillComboCompanyAccounts();
    }

    protected string GetEmployeeRecentPhoto(object EmployeeID, object RecentPhoto, int width)
    {
        if (RecentPhoto == DBNull.Value)
            return GetEmployeePassportPhoto(EmployeeID, width);
        else
            return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeeRecentPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeePassportPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected void filRecentPhoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        AjaxControlToolkit.AsyncFileUpload filRecentPhoto = (AjaxControlToolkit.AsyncFileUpload)sender;

        if (filRecentPhoto.HasFile)
        {
            //if (filRecentPhoto.FileBytes.Length <= 307200)
            //{
            string ext = filRecentPhoto.ContentType;
            if (ext.Equals("image/gif") ||
                ext.Equals("image/png") ||
                ext.Equals("image/jpg") ||
                ext.Equals("image/bmp") ||
                ext.Equals("image/jpeg") ||
                ext.Equals("image/tif") ||
                ext.Equals("image/tiff"))
            {
                System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("../documents/temp/") + "Recent" + Session.SessionID + ".jpg");
                if (file.Exists)
                    file.Delete();

                filRecentPhoto.SaveAs(Server.MapPath("../documents/temp/") + "Recent" + Session.SessionID + ".jpg");
                this.hdnRecentPhotoURL.Value = "../documents/temp/Recent" + Session.SessionID + ".jpg";
                updRecentPhoto.Update();
                Response.Clear();
                Fromdb = false;
                lnkremoveRecentPhoto.Visible = true;
            }
            //}
        }
    }

    protected void filPassportPhoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        AjaxControlToolkit.AsyncFileUpload filPassportPhoto = (AjaxControlToolkit.AsyncFileUpload)sender;

        if (filPassportPhoto.HasFile)
        {
            //if (filPassportPhoto.FileBytes.Length <= 307200)
            //{
            System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("../documents/temp/") + "Passport" + Session.SessionID + ".jpg");
            if (file.Exists)
                file.Delete();

            Response.Clear();

            filPassportPhoto.SaveAs(Server.MapPath("../documents/temp/") + "Passport" + Session.SessionID + ".jpg");
            Fromdb = false;
            //}
        }
    }

    public void SetUserEnability()
    {
        //if (chkUser.Checked)// set employee as user
        //{
        //    ddlRole.Enabled = true;
        //    txtusername.Enabled = true;
        //    txtpassword.Enabled = true;
        //    txtusername.Text = txtOfficialEmail.Text;
        //    txtpassword.Attributes["value"] = txtOfficialEmailPassword.Text;
        //    txtOfficialEmailPassword.Attributes["value"] = txtOfficialEmailPassword.Text;
        //    txtusername.CssClass = "textbox_mandatory";
        //    txtpassword.CssClass = "textbox_mandatory";
        //    txtOfficialEmail.CssClass = "textbox_mandatory";
        //    txtOfficialEmailPassword.CssClass = "textbox_mandatory";
        //    ddlRole.CssClass = "dropdownlist_mandatory";
        //    rfvUsernamepnlTab2pnlTab2.Enabled = rfvpasswordpnlTab2pnlTab2.Enabled = rfvrolepnlTab2.Enabled = true;
        //}
        //else
        //{
        //    ddlRole.Enabled = false;
        //    txtusername.Enabled = false;
        //    txtpassword.Enabled = false;
        //    txtusername.Text = string.Empty;
        //    txtpassword.Attributes["value"] = string.Empty;
        //    ddlRole.SelectedIndex = -1;
        //    txtOfficialEmail.CssClass = "textbox";
        //    txtusername.CssClass = "textbox";
        //    txtpassword.CssClass = "textbox";
        //    txtOfficialEmail.CssClass = "textbox";
        //    txtOfficialEmailPassword.CssClass = "textbox";
        //    ddlRole.CssClass = "dropdownlist_disabled";
        //    rfvUsernamepnlTab2pnlTab2.Enabled = rfvpasswordpnlTab2pnlTab2.Enabled =  rfvrolepnlTab2.Enabled = false;
        //}

        // updRoles.Update();
    }

    protected void chkUser_CheckedChanged(object sender, EventArgs e)
    {
        SetUserEnability();
    }

    private void RegisterAutoComplete()
    {
        //clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Employee);
    }

    protected void btnSalaryStructure_Click(object sender, EventArgs e)
    {
        Response.Redirect("Salarystructure.aspx");
    }

    protected void btnPassport_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeePassport.aspx");
    }

    protected void btnVisa_Click(object sender, EventArgs e)
    {
        Response.Redirect("Visa.aspx");
    }

    protected void btnOtherDocs_Click(object sender, EventArgs e)
    {
        Response.Redirect("OtherDocument.aspx");
    }

    public void GetSalutationReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            ddlSalutation.DataSource = objBindCombo.GetSalutationReference();
            ddlSalutation.DataTextField = "Description ";
            ddlSalutation.DataValueField = "SalutationId";
            ddlSalutation.SelectedValue = CurrentSelectedValue;
            updSalutation.Update();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetDepartmentReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            ddlDepartment.DataSource = objBindCombo.GetDepartmentReference();
            ddlDepartment.DataTextField = "Description";
            ddlDepartment.DataValueField = "DepartmentId";
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetWorkStatusReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetDesignationReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetDegreeReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetCountryReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetNationalityReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetEthnicOrgin()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetReligionReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    public void GetEmploymentTypeReference()
    {
        try
        {
            objBindCombo = new clsBindComboBox();
            objBindCombo.GetSalutationReference();
        }
        finally
        {
            objBindCombo = null;
        }
    }

    protected void btnSalutation_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "SalutationReference";
        ReferenceNew1.DataTextField = "Salutation";
        ReferenceNew1.DataValueField = "SalutationId";
        ReferenceNew1.FunctionName = "FillComboSalutation";
        ReferenceNew1.SelectedValue = ddlSalutation.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Salutation").ToString();
        ReferenceNew1.DataTextFieldArabic = "SalutationArb";
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();





    }

    protected void btnEmployementType_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "EmploymentTypeReference";
        ReferenceNew1.DataTextField = "EmploymentType";
        ReferenceNew1.DataTextFieldArabic = "EmploymentTypeArb";
        ReferenceNew1.DataValueField = "EmploymentTypeId";
        ReferenceNew1.PredefinedField = "IsPredefined";
        ReferenceNew1.FunctionName = "FillComboEmployementType";
        ReferenceNew1.SelectedValue = ddlEmployementType.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "EmploymentType").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnWorkStatus_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "WorkStatusReference";
        ReferenceNew1.DataTextField = "WorkStatus";
        ReferenceNew1.DataTextFieldArabic = "WorkStatusArb";
        ReferenceNew1.DataValueField = "WorkStatusID";
        ReferenceNew1.PredefinedField = "IsPredifined";
        ReferenceNew1.FunctionName = "FillComboWorkStatus";
        ReferenceNew1.SelectedValue = ddlWorkStatus.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "WorkStatus").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnDepartment_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "DepartmentReference";
        ReferenceNew1.DataTextField = "Department";
        ReferenceNew1.DataTextFieldArabic = "DepartmentArb";
        ReferenceNew1.DataValueField = "DepartmentId";
        ReferenceNew1.FunctionName = "FillComboDepartment";
        ReferenceNew1.SelectedValue = ddlDepartment.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Department").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnNationality_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "NationalityReference";
        ReferenceNew1.DataTextField = "Nationality";
        ReferenceNew1.DataTextFieldArabic = "NationalityArb";
        ReferenceNew1.DataValueField = "NationalityId";
        ReferenceNew1.FunctionName = "FillComboNationality";
        ReferenceNew1.SelectedValue = ddlNationality.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Nationality").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnReligion_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "ReligionReference";
        ReferenceNew1.DataTextField = "Religion";
        ReferenceNew1.DataTextFieldArabic = "ReligionArb";
        ReferenceNew1.DataValueField = "ReligionId";
        ReferenceNew1.FunctionName = "FillComboReligion";
        ReferenceNew1.SelectedValue = ddlReligion.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Religion").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    public void FillComboSalutation()
    {
        this.BindDropDown(this.ddlSalutation, "Salutation", "SalutationId", new clsBindComboBox().GetSalutationReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlSalutation);
        this.updSalutation.Update();
    }

    public void FillComboEmployementType()
    {
        this.BindDropDown(ddlEmployementType, "EmploymentType", "EmploymentTypeID", new clsBindComboBox().GetEmploymentTypeReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlEmployementType);
        this.updEmployementType.Update();
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    public void FillComboWorkStatus()
    {
        this.BindDropDown(this.ddlWorkStatus, "WorkStatus", "WorkStatusID", new clsBindComboBox().GetWorkStatusReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlWorkStatus);
        this.updWorkStatus.Update();
    }

    public void FillComboDepartment()
    {
        this.BindDropDown(this.ddlDepartment, "Department", "DepartmentId", new clsBindComboBox().GetDepartmentReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlDepartment);
        this.updDepartment.Update();
    }

    public void FillComboDesignation()
    {
        this.BindDropDown(this.ddlDesignation, "Designation", "DesignationId", new clsBindComboBox().GetDesignationReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlDesignation);
        this.updDesignation.Update();
    }

    public void FillCompany()
    {
        objEmployee = new clsEmployee();
        objUser = new clsUserMaster();

        objEmployee.CompanyID = objUser.GetCompanyId();
        //this.BindDropDown(this.ddlCompany, "CompanyName", "CompanyID", objEmployee.GetAllCompanies().Tables[0], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

        this.BindDropDown(this.ddlCompany, "CompanyName", "CompanyID", new clsCompany().GetCompaniesbyUser(new clsUserMaster().GetUserId()), clsGlobalization.IsArabicCulture() ? "اختر" : "select");

        //ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue( objUser.GetCompanyId().ToString()));
        // ddlCompany.Enabled = false;
        this.updCompany.Update();


    }

    public void FillTransactionType()
    {
        try
        {
            objEmployee = new clsEmployee();
            ddlTransactionType.DataSource = objEmployee.GetAllTransactionTypes();
            ddlTransactionType.DataBind();
            ddlTransactionType.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "select", "-1"));
        }
        finally
        {
            objEmployee = null;
        }
    }

    private void FillComboWorkPolicy()
    {
        try
        {
            objEmployeeNew = new clsEmployeeNew();

            objEmployee = new clsEmployee();
            objEmployeeNew.CompanyID = CompanyId;
            DataTable dt = objEmployeeNew.GetAllPolicies();
            DataRow dr = dt.NewRow();
            dr[0] = "-1";
            dr[1] = clsGlobalization.IsArabicCulture() ? "اختر" : "select";
            dt.Rows.InsertAt(dr, 0);
            ddlPolicy.DataSource = dt;
            ddlPolicy.DataBind();
            updWorkPolicy.Update();
        }
        finally
        {
            objEmployee = null;
            objEmployeeNew = null;
        }
    }

    private void FillComboLeavePolicy()
    {
        try
        {
            objEmployeeNew = new clsEmployeeNew();
            objEmployeeNew.CompanyID = CompanyId;
            ddlLeavePolicy.DataSource = objEmployeeNew.GetAllLeavePolicies();
            ddlLeavePolicy.DataBind();
            ddlLeavePolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "select", "-1"));
            updLeavePolicy.Update();
        }
        finally
        {
            objEmployeeNew = null;
        }
    }

    private void FillComboWorkLocation()
    {
        try
        {
            ddlWorkLocation.DataSource = clsEmployeeNew.GetAllWorkLocation(CompanyId);
            ddlWorkLocation.DataValueField = "WorkLocationID";
            ddlWorkLocation.DataTextField = "WorkLocation";
            ddlWorkLocation.DataBind();
            ddlWorkLocation.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "select", "-1"));
            updWorkLocation.Update();
        }
        finally
        {
            objEmployee = null;
        }
    }

    public void FillComboEmployerBankersName()
    {
        try
        {
            objEmployeeNew = new clsEmployeeNew();
            objEmployeeNew.CompanyID = ddlCompany.SelectedValue.ToInt32();
            ddlEmployerBankName.DataSource = objEmployeeNew.GetEmployerBankNames();
            ddlEmployerBankName.DataBind();
            ddlEmployerBankName.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "select", "-1"));
        }
        finally
        {
            objEmployeeNew = null;
        }
    }

    public void FillComboCompanyAccounts()
    {
        try
        {
            objEmployeeNew = new clsEmployeeNew();
            objEmployeeNew.CompanyID = ddlCompany.SelectedValue.ToInt32();
            objEmployeeNew.BankBranchID = ddlEmployerBankName.SelectedValue.ToInt32();// BankerId;
            ddlCompanyAccount.DataSource = objEmployeeNew.GetAllCompanyAccounts();
            ddlCompanyAccount.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "select", "-1"));
            ddlCompanyAccount.DataBind();
        }
        finally
        {
            objEmployeeNew = null;
        }
    }

    public void FillComboNationality()
    {
        this.BindDropDown(this.ddlNationality, "Nationality", "NationalityId", new clsBindComboBox().GetNationalityReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlNationality);
        this.updNationality.Update();
    }

    public void FillComboCountry()
    {
        this.BindDropDown(this.ddlCountry, "CountryName", "CountryId", new clsBindComboBox().GetCountryReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlCountry);
        this.updCountry.Update();
    }

    public void FillComboMotherTongue()
    {
        this.BindDropDown(this.ddlMotherTongue, "MotherTongue","MotherTongueID" , new clsBindComboBox().GetMotherTongue(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        ddlLanguage.DataSource = new clsBindComboBox().GetMotherTongue();
        ddlLanguage.DataTextField = "MotherTongue";
        ddlLanguage.DataValueField = "MotherTongueID";
        ddlLanguage.DataBind();
      
        this.updMotherTongue.Update();
    }

    public void FillComboReligion()
    {
        this.BindDropDown(this.ddlReligion, "Religion", "ReligionId", new clsBindComboBox().GetReligionReference(), clsGlobalization.IsArabicCulture() ? "اختر" : "select");
        this.SetSelectedIndex(this.ddlReligion);
        this.updReligion.Update();
    }
     

    private void FillAllCombos()
    {
        FillCompany();
        FillComboWorkPolicy();
        FillComboLeavePolicy();
        FillComboEmployerBankersName();
        FillComboCompanyAccounts();
        FillComboWorkLocation();
        using (DataSet ds = new clsBindComboBox().GetAllData())
        {
            if (ds.Tables.Count > 0)
                this.BindDropDown(this.ddlSalutation, "Salutation", "SalutationID", ds.Tables[0], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            //if (ds.Tables.Count > 1)
            //    this.BindDropDown(this.ddlCompany, "CompanyName", "CompanyID", ds.Tables[1], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 2)
                this.BindDropDown(this.ddlNationality, "Nationality", "NationalityId", ds.Tables[2], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 3)
                this.BindDropDown(this.ddlReligion, "Religion", "ReligionID", ds.Tables[3], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 4)
                this.BindDropDown(this.ddlWorkStatus, "WorkStatus", "WorkStatusID", ds.Tables[4], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 5)
                this.BindDropDown(this.ddlWorkLocation, "WorkLocation", "WorkLocationID", ds.Tables[5], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 6)
                this.BindDropDown(this.ddlDepartment, "Department", "DepartmentID", ds.Tables[6], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            //if (ds.Tables.Count > 7)
            //    this.BindDropDown(this.ddlDesignation, "Designation", "DesignationID", ds.Tables[7], "select");

            if (ds.Tables.Count > 8)
                this.BindDropDown(this.ddlEmployementType, "EmploymentType", "EmploymentTypeID", ds.Tables[8], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 9)
                FillComboMotherTongue();

            if (ds.Tables.Count > 10)
                this.BindDropDown(this.ddlTransactionType, "TransactionType", "TransactionTypeID", ds.Tables[10], clsGlobalization.IsArabicCulture() ? "اختر" : "select");

            if (ds.Tables.Count > 11)
                this.BindDropDown(this.ddlEmployeeBankName, "Description", "BankBranchID", ds.Tables[11], clsGlobalization.IsArabicCulture() ? "اختر" : "select");
            if (ds.Tables.Count > 12)
                this.BindDropDown(this.ddlProcessType, "ProcessType", "ProcessTypeID", ds.Tables[12], "");

        }

        FillComboCountry();
        //GetDesignation();
        FillComboDesignation();
        GetReportingTo();
        GetHOD();

        //objEmployee = new clsEmployee();
        //this.BindDropDown(this.ddlReportingTo, "EmployeeName", "EmployeeID", objEmployee.GetReportingto(ViewState["EmployeeId"].ToInt32()), "select");
    }

    public void GetDesignation()
    {
        try
        {
            objEmployeeNew = new clsEmployeeNew();
            objEmployeeNew.CompanyID = ddlCompany.SelectedValue.ToInt32();
            DataTable dt = objEmployeeNew.GetDesignation();
            DataRow dr = dt.NewRow();
            dr["DesignationID"] = "-1";
            dr["Designation"] = clsGlobalization.IsArabicCulture() ? "اختر" : "Select";
            dt.Rows.InsertAt(dr, 0);
            ddlDesignation.DataTextField = "Designation";
            ddlDesignation.DataValueField = "DesignationID";
            ddlDesignation.DataSource = dt;
            ddlDesignation.DataBind();


            updDesignation.Update();
        }
        finally
        {
            objEmployeeNew = null;
        }
    }

    private void GetReportingTo()
    {
        try
        {
            if (ddlDesignation.SelectedIndex >= 0)
            {
                objUser = new clsUserMaster();
                objEmployee = new clsEmployee();

                if (chkEmployees.Checked)
                    objEmployee.CompanyID = -1;
                else
                    objEmployee.CompanyID = objUser.GetCompanyId();

                DataTable dt = objEmployee.GetReportingto(ViewState["EmployeeId"].ToInt32(), ddlDesignation.SelectedValue.ToInt32());
                DataRow dr = dt.NewRow();
                dr[0] = "-1";
                dr[1] = clsGlobalization.IsArabicCulture() ? "اختر" : "Select";
                dt.Rows.InsertAt(dr, 0);
                ddlReportingTo.DataSource = dt;
                ddlReportingTo.DataBind();
                updReportingTo.Update();
            }
        }
        finally
        {
            objEmployee = null;
        }
    }

    protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource)
    {
        this.BindDropDown(ddwn, dataTextField, dataValueField, dataSource, null);
    }

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource, string insertText)
    {
        ddwn.DataValueField = dataValueField;
        ddwn.DataTextField = dataTextField;
        ddwn.DataSource = dataSource;
        ddwn.DataBind();
        if (insertText != null && insertText.Trim() != string.Empty)
            ddwn.Items.Insert(0, new ListItem(insertText, "-1"));
    }

    protected void NewEmployee()
    {
        //txtSearch.Text = string.Empty;
        ViewState["Buffer"] = null;
        ViewState["BufferP"] = null;
        objEmployee = new clsEmployee();
        objEmployee.EmployeeID = -1;
        DataTable dt = objEmployee.GetEmployee().Tables[0];
        DataRow dw = dt.NewRow();
        dt.Rows.InsertAt(dw, 0);
        btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
        btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
    }

    private void SaveEmployee()
    {
        int iEmpID = 0;

        objEmployeeNew = new clsEmployeeNew();

        objEmployeeNew.FirstNameArb = txtFirstNameArb.Text.Trim();
        objEmployeeNew.MiddleNameArb = txtMiddleNameArabic.Text.Trim();
        objEmployeeNew.LastNameArb = txtLastNameArb.Text.Trim();
        objEmployeeNew.EmployeeID = Request.QueryString["EmpId"] == null ? 0 : Request.QueryString["EmpId"].ToInt32();
        objEmployeeNew.AddressLine1 = txtAddressLine1.Text.Trim();
        objEmployeeNew.AddressLine2 = txtAddressLine2.Text.Trim();
        objEmployeeNew.City = txtCity.Text.Trim();
        objEmployeeNew.StateProvinceRegion = txtState.Text.Trim();
        objEmployeeNew.ZipCode = txtZip.Text.Trim();
        objEmployeeNew.Phone = txtPermanentPhone.Text.Trim();
        objEmployeeNew.LocalAddress = txtLocalAddress.Text.Trim();
        objEmployeeNew.LocalPhone = txtLocalPhone.Text.Trim();
        objEmployeeNew.EmergencyAddress = txtEmergencyAddress.Text.Trim();
        objEmployeeNew.EmergencyPhone = txtEmergencyPhone.Text.Trim();
        objEmployeeNew.BloodGroup = ddlBloodGroup.SelectedValue.ToString().Trim();
        objEmployeeNew.CompanyID = ddlCompany.SelectedValue.ToInt32();
        objEmployeeNew.DateOfBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.ToString());
        objEmployeeNew.DateOfJoining = clsCommon.Convert2DateTime(txtDateofJoining.Text.ToString());
        objEmployeeNew.DepartmentID = ddlDepartment.SelectedValue.ToInt16();
        objEmployeeNew.DesignationID = ddlDesignation.SelectedValue.ToInt16();
        objEmployeeNew.Email = txtEmail.Text.Trim();
        objEmployeeNew.EmployeeBank = ddlEmployeeBankName.SelectedValue.ToInt32();
        objEmployeeNew.EmployeeACC = txtAccountNumber.Text.ToString();
        objEmployeeNew.EmployeeNumber = txtEmployeeNumber.Text.Trim();
        objEmployeeNew.EmployerACC = ddlCompanyAccount.SelectedValue.ToInt32();
        objEmployeeNew.EmployerBank = ddlEmployerBankName.SelectedValue.ToInt16();
        objEmployeeNew.EmploymentTypeID = ddlEmployementType.SelectedValue.ToInt32();
        objEmployeeNew.FatherName = txtFathersName.Text.Trim();
        objEmployeeNew.FirstName = txtFirstName.Text.Trim();
        objEmployeeNew.Identification = txtIdentification.Text.Trim();
        objEmployeeNew.IsMale = rblGender.SelectedIndex == 0 ? true : false;
        objEmployeeNew.MotherTongueID = ddlMotherTongue.SelectedValue.ToInt32();
        objEmployeeNew.LastName = txtLastName.Text.Trim();

        if (ddlLeavePolicy.SelectedIndex > 0)
            objEmployeeNew.LeavePolicyId = ddlLeavePolicy.SelectedValue.ToInt16();

        objEmployeeNew.MiddleName = txtMiddleName.Text.Trim();
        objEmployeeNew.Mobile = txtLocalMobile.Text.Trim();
        objEmployeeNew.MotherName = txtMothersName.Text.Trim();
        objEmployeeNew.MotherTongueID = ddlMotherTongue.SelectedValue.ToInt32();
        objEmployeeNew.CountryID = ddlCountry.SelectedValue.ToInt32();
        objEmployeeNew.NationalityID = ddlNationality.SelectedValue.ToInt32();
        objEmployeeNew.OfficialEmailID = txtOfficialEmail.Text.Trim();
        objEmployeeNew.OfficialEmailPassword = txtOfficialEmailPassword.Text.Trim();
        objEmployeeNew.OtherInfo = txtOtherInfo.Text.Trim();
        //objEmployeeNew.RoleID = ddlRole.SelectedValue.ToInt16();
        // objEmployeeNew.UserName =  txtusername.Text.Trim();
        // objEmployeeNew.Password = clsCommon.Encrypt(txtpassword.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);

        objEmployeeNew.ProbabtionEndDate = clsCommon.Convert2DateTime(txtProbationEndDate.Text.ToString());
        objEmployeeNew.ReligionID = ddlReligion.SelectedValue.ToInt32();
        objEmployeeNew.SalutationID = ddlSalutation.SelectedValue.ToInt16();
        objEmployeeNew.SpouseName = txtSpouseName.Text.Trim();
        objEmployeeNew.TransactionType = ddlTransactionType.SelectedValue.ToInt16();
        objEmployeeNew.ReportingTo = ddlReportingTo.SelectedValue.ToInt32();
        objEmployeeNew.HOD = ddlHOD.SelectedValue.ToInt32();
        objEmployeeNew.ProcessTypeID = ddlProcessType.SelectedValue.ToInt32();
         objEmployeeNew.DeviceUser = txtDeviceUserID.Text.Trim();
        if (txtAppraisalDate.Text != "")
            objEmployeeNew.NextAppraisalDate = clsCommon.Convert2DateTime(txtAppraisalDate.Text.ToString());
     

        if (ddlPolicy.SelectedIndex > 0)
            objEmployeeNew.WorkPolicyID = ddlPolicy.SelectedValue.ToInt16();

        objEmployeeNew.WorkLocationID = ddlWorkLocation.SelectedValue.ToInt32();

        objEmployeeNew.WorkStatusID = ddlWorkStatus.SelectedValue.ToInt32();

        if (ViewState["IsSettled"].ToInt32() == 0 && objEmployeeNew.WorkStatusID < 6)
        {
            if (ViewState["WorkStatusId"].ToInt32() == 0)
                ViewState["WorkStatusId"] = 6;

            objEmployeeNew.WorkStatusID = ViewState["WorkStatusId"].ToInt32();
        }

        objEmployeeNew.CandidateID = hdCandidateID.Value.ToInt32();

        try 
        {
            byte[] buffer = null;
            byte[] bufferThumbnail = null;
            if (filRecentPhoto.HasFile)
            {
                // Passport size photo
                System.Drawing.Image image;
                buffer = new byte[filRecentPhoto.PostedFile.ContentLength];
                var stream = filRecentPhoto.PostedFile.InputStream;
                stream.Seek(0, System.IO.SeekOrigin.Begin);
                stream.Read(buffer, 0, filRecentPhoto.PostedFile.ContentLength);
                filRecentPhoto.ClearFileFromPersistedStore();

                image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(buffer));
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image, 100, 125);

                System.Drawing.ImageConverter imgConverter = new System.Drawing.ImageConverter();
                objEmployeeNew.RecentPhoto = (byte[])imgConverter.ConvertTo(bmp, typeof(byte[]));
                ViewState["Buffer"] = objEmployeeNew.RecentPhoto;

                // End

                // Thumbnail (Resize image)
                System.Drawing.Image imageThumbnail;
                bufferThumbnail = new byte[filRecentPhoto.PostedFile.ContentLength];
                var streamThumbnail = filRecentPhoto.PostedFile.InputStream;
                streamThumbnail.Seek(0, System.IO.SeekOrigin.Begin);
                streamThumbnail.Read(bufferThumbnail, 0, filRecentPhoto.PostedFile.ContentLength);
                filRecentPhoto.ClearFileFromPersistedStore();

                imageThumbnail = System.Drawing.Image.FromStream(new System.IO.MemoryStream(bufferThumbnail));
                System.Drawing.Bitmap bmpThumbnail = new System.Drawing.Bitmap(imageThumbnail, 50, 60);

                System.Drawing.ImageConverter imgConverterThumbnail = new System.Drawing.ImageConverter();
                objEmployeeNew.Thumbnail = (byte[])imgConverterThumbnail.ConvertTo(bmpThumbnail, typeof(byte[]));
                // End
            }
            else
            {
                if (ViewState["Buffer"] != DBNull.Value)
                {
                    if (ViewState["Buffer"] != null)
                    {
                        // Passport size photo
                        System.Drawing.Image image;
                        buffer = (byte[])ViewState["Buffer"];
                        image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(buffer));
                        System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image, 100, 125);

                        System.Drawing.ImageConverter imgConverter = new System.Drawing.ImageConverter();
                        objEmployeeNew.RecentPhoto = (byte[])imgConverter.ConvertTo(bmp, typeof(byte[]));
                        // End

                        // Thumbnail (Resize image)
                        System.Drawing.Image imageThumbnail;
                        bufferThumbnail = (byte[])ViewState["Buffer"];
                        imageThumbnail = System.Drawing.Image.FromStream(new System.IO.MemoryStream(bufferThumbnail));
                        System.Drawing.Bitmap bmpThumbnail = new System.Drawing.Bitmap(imageThumbnail, 50, 60);

                        System.Drawing.ImageConverter imgConverterThumbnail = new System.Drawing.ImageConverter();
                        objEmployeeNew.Thumbnail = (byte[])imgConverterThumbnail.ConvertTo(bmpThumbnail, typeof(byte[]));
                        // End
                    }
                }
            }
        }
        catch(Exception ex)
        {
            //ShowpopUpErrorMessage("Photo size is too large.");
            //ShowpopUpErrorMessage(GetLocalResourceObject("PhotoSizeIsTooLarge.Text").ToString());
            return;
        }

        if (Request.QueryString["EmpId"] == null)
        {
            if (objEmployeeNew.IsEmployeeCodeExists())
            {
                // ShowpopUpErrorMessage("Employee Number exists");
                ShowpopUpErrorMessage(GetLocalResourceObject("EmployeeNumberExists.Text").ToString());
                return;
            }
            //else if ( txtusername.Text != ""  &&  objEmployeeNew.IsUsernameExists(0))
            //{
            //   // ShowpopUpErrorMessage("Username exists");
            //    ShowpopUpErrorMessage(GetLocalResourceObject("UsernameExists.Text").ToString()); ;
            //    return;
            //}
            else
            {                
                objEmployeeNew.EmployeeID = objEmployeeNew.SaveEmployee();

                // save alert if workstatus is probation
                if (ddlWorkStatus.SelectedValue.ToInt32() == 7)
                    new clsAlerts().AlertMessage((int)DocumentType.Probation, "Probation", "probation end date", objEmployeeNew.EmployeeID, txtEmployeeNumber.Text, clsCommon.Convert2DateTime(txtProbationEndDate.Text), "Employee", objEmployeeNew.EmployeeID, txtFirstName.Text.Trim() + " " + txtMiddleName.Text + " " + txtLastName.Text, false);

                //Calling the save leave policy structure twice 
                //Commenting will create problems in leave entry
                SaveLeavepolicystructure(objEmployeeNew.EmployeeID, true);

                SaveLeavepolicystructure(objEmployeeNew.EmployeeID, false);
                SaveEmployeeLanguage(objEmployeeNew.EmployeeID);

                //---------------Save employee KPI/KRA----------------------------
                //SaveKPIKRA(objEmployeeNew.EmployeeID);

                if (this.Request.QueryString["CandidateId"] != null)
                {
                    clsMessage objMessage = new clsMessage();
                    objMessage.ReferenceID = this.Request.QueryString["VacancyId"].ToInt32();
                    objMessage.ReferenceType = eReferenceType.Vacancy;
                    objMessage.AssociatedID = this.Request.QueryString["CandidateId"].ToInt32();
                    objMessage.AssociatedReference = eReferenceType.Candidate;
                    objMessage.Delete();
                }
            }
        }
        else
        {
            //if (txtusername.Text != "" && objEmployeeNew.IsUsernameExists(Request.QueryString["EmpId"].ToInt32()))
            //{
            //   // ShowpopUpErrorMessage("Username exists");
            //    ShowpopUpErrorMessage(GetLocalResourceObject("UsernameExists.Text").ToString());
            //    return;
            //}

            objEmployeeNew.EmployeeID = Convert.ToInt32(ViewState["EmployeeId"]);

            if (objEmployeeNew.IsEmployeeCodeExistsForUpdation())
            {
                //ShowpopUpErrorMessage("Employee Number exists");
                ShowpopUpErrorMessage(GetLocalResourceObject("EmployeeNumberExists.Text").ToString()); ;
                return;
            }
            else
            {
                objEmployeeNew.EmployeeID = objEmployeeNew.SaveEmployee();

                //---------------Save employee KPI/KRA----------------------------
                //SaveKPIKRA(objEmployeeNew.EmployeeID);

                // save alert if workstatus is probation
                if (ddlWorkStatus.SelectedValue.ToInt32() == 7)
                    new clsAlerts().AlertMessage((int)DocumentType.Probation, "Probation", "probation end date", objEmployeeNew.EmployeeID, txtEmployeeNumber.Text, clsCommon.Convert2DateTime(txtProbationEndDate.Text), "Employee", objEmployeeNew.EmployeeID, txtFirstName.Text.Trim() + " " + txtMiddleName.Text + " " + txtLastName.Text, false);


                SaveLeavepolicystructure(objEmployeeNew.EmployeeID, false);

                SaveEmployeeLanguage(Convert.ToInt32(ViewState["EmployeeId"]));

                if (Request.QueryString["Action"] == "Confirm")
                    Response.Redirect("SalaryStructure.aspx?EmpId=" + Convert.ToInt32(ViewState["EmployeeId"]));
            }
        }
        

        Response.Redirect("Employee.aspx?Empid=" + objEmployeeNew.EmployeeID + "");
    }
    private void SaveEmployeeLanguage(int EmployeeID)
    {
        objEmployeeNew = new clsEmployeeNew();
        foreach (ListItem item in ddlLanguage.Items)
        {
            if (item.Selected)
            {
                objEmployeeNew.EmployeeID = EmployeeID;
                objEmployeeNew.MotherTongueID = item.Value.ToInt32();
                objEmployeeNew.SaveEmployeeLanguages();
            }
               
        }
    }

    //private void SaveKPIKRA(int EmployeeID)
    //{
    //    //Save KPI        
    //    if (Validate())
    //    {
    //        objEmployee = new clsEmployee();
    //        objEmployee.EmployeeID = EmployeeID;

    //        if (objEmployee.DeleteKpiKra(objEmployee.EmployeeID) > 0)
    //        {
    //            int id = objEmployee.SaveKpi(EmpKpi, objEmployee.EmployeeID);
    //            int id2 = objEmployee.SaveKra(EmpKra, objEmployee.EmployeeID);
    //            if (id > 0 && id2 > 0)
    //            {
    //                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Saved Successfully") : ("Saved Successfully");
    //                mcMsg.InformationalMessage(msg);
    //                mpeMsg.Show();
    //            }
    //        }
    //    }

    //}

    //private bool Validate()
    //{
    //    bool val = true;
    //    string msg = string.Empty;
    //    bool gridKPI = true;
    //    bool gridKRA = true;
    //    if (gvKpi.Rows.Count > 0)
    //    {
    //        if (gvKpi.Rows.Count == 1)
    //        {
    //            TextBox txtKpiMarks = (TextBox)gvKpi.Rows[0].FindControl("txtKpiMarks");
    //            DropDownList ddlKpi = (DropDownList)gvKpi.Rows[0].FindControl("ddlKpi");
    //            if ((ddlKpi.SelectedValue == "-1" && txtKpiMarks.Text.ToDecimal() > 0) || (ddlKpi.SelectedValue.ToInt32() > 0 && txtKpiMarks.Text.ToDecimal() == 0))
    //            {
    //                val = gridKPI = false;
    //            }
    //            else if (ddlKpi.SelectedValue.ToInt32() == -1 && txtKpiMarks.Text.ToDecimal() == 0)
    //            {
    //                gridKPI = false;
    //            }
    //        }
    //        else
    //        {
    //            for (int i = 0; i < gvKpi.Rows.Count; i++)
    //            {
    //                TextBox txtKpiMarks = (TextBox)gvKpi.Rows[i].FindControl("txtKpiMarks");
    //                DropDownList ddlKpi1 = (DropDownList)gvKpi.Rows[i].FindControl("ddlKpi");
    //                for (int j = i + 1; j < gvKpi.Rows.Count; j++)
    //                {
    //                    DropDownList ddlKpi2 = (DropDownList)gvKpi.Rows[j].FindControl("ddlKpi");
    //                    if (ddlKpi1.SelectedValue == ddlKpi2.SelectedValue)
    //                    {
    //                        val = gridKPI = false;
    //                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Duplication in KPI's are not allowed") : ("Duplication in KPI's are not allowed");
    //                        mcMsg.InformationalMessage(msg);
    //                        mpeMsg.Show();
    //                        break;
    //                    }
    //                }
    //                if (ddlKpi1.SelectedValue == "-1")
    //                {
    //                    val = gridKPI = false;
    //                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Select a KPI") : ("Select a KPI");
    //                    mcMsg.InformationalMessage(msg);
    //                    mpeMsg.Show();
    //                    break;
    //                }
    //                if (txtKpiMarks.Text.Trim() == string.Empty || txtKpiMarks.Text.ToDecimal() == 0)
    //                {
    //                    val = gridKPI = false;
    //                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Enter KPI marks") : ("Enter KPI marks");
    //                    mcMsg.InformationalMessage(msg);
    //                    mpeMsg.Show();
    //                    break;
    //                }
    //            }
    //        }
    //        if (val && gridKPI)
    //        {
    //            foreach (GridViewRow gv in gvKpi.Rows)
    //            {
    //                TextBox txtKpiMarks = (TextBox)gv.FindControl("txtKpiMarks");
    //                DropDownList ddlKpi = (DropDownList)gv.FindControl("ddlKpi");
    //                EmpKpi.Add(new clsKPI()
    //                {
    //                    KPIId = ddlKpi.SelectedValue.ToInt32(),
    //                    KPIValue = txtKpiMarks.Text.Trim().ToDecimal()
    //                });
    //            }
    //        }
    //    }
    //    if (gvKra.Rows.Count > 0)
    //    {
    //        if (gvKra.Rows.Count == 1)
    //        {
    //            TextBox txtKraMarks = (TextBox)gvKra.Rows[0].FindControl("txtKraMarks");
    //            DropDownList ddlKra = (DropDownList)gvKra.Rows[0].FindControl("ddlKra");
    //            if ((ddlKra.SelectedValue == "-1" && txtKraMarks.Text.ToDecimal() > 0) || (ddlKra.SelectedValue.ToInt32() > 0 && txtKraMarks.Text.ToDecimal() == 0))
    //            {
    //                val = gridKRA = false;
    //            }
    //            else if (ddlKra.SelectedValue.ToInt32() == -1 && txtKraMarks.Text.ToDecimal() == 0)
    //            {
    //                gridKRA = false;
    //            }
    //        }
    //        else
    //        {
    //            for (int i = 0; i < gvKra.Rows.Count; i++)
    //            {
    //                TextBox txtKraMarks = (TextBox)gvKra.Rows[i].FindControl("txtKraMarks");
    //                DropDownList ddlKra1 = (DropDownList)gvKra.Rows[i].FindControl("ddlKra");
    //                for (int j = i + 1; j < gvKra.Rows.Count; j++)
    //                {
    //                    DropDownList ddlKra2 = (DropDownList)gvKra.Rows[j].FindControl("ddlKra");
    //                    if (ddlKra1.SelectedValue == ddlKra2.SelectedValue)
    //                    {
    //                        val = gridKRA = false;
    //                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Duplication in KRA's are not allowed") : ("Duplication in KRA's are not allowed");
    //                        mcMsg.InformationalMessage(msg);
    //                        mpeMsg.Show();
    //                        break;
    //                    }
    //                }
    //                if (ddlKra1.SelectedValue == "-1")
    //                {
    //                    val = gridKRA = false;
    //                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Select a KRA") : ("Select a KRA");
    //                    mcMsg.InformationalMessage(msg);
    //                    mpeMsg.Show();
    //                    break;
    //                }
    //                if (txtKraMarks.Text.Trim() == string.Empty || txtKraMarks.Text.ToDecimal() == 0)
    //                {
    //                    val = gridKRA = false;
    //                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Enter KRA marks") : ("Enter KRA marks");
    //                    mcMsg.InformationalMessage(msg);
    //                    mpeMsg.Show();
    //                    break;
    //                }
    //            }
    //        }
    //        if (val && gridKRA)
    //        {
    //            foreach (GridViewRow gv2 in gvKra.Rows)
    //            {
    //                TextBox txtKraMarks = (TextBox)gv2.FindControl("txtKraMarks");
    //                DropDownList ddlKra = (DropDownList)gv2.FindControl("ddlKra");
    //                EmpKra.Add(new clsKRA()
    //                {
    //                    KRAId = ddlKra.SelectedValue.ToInt32(),
    //                    KRAValue = txtKraMarks.Text.Trim().ToDecimal()
    //                });
    //            }
    //        }
    //    }
    //    return val;
    //}

    protected void btnList_Click(object sender, EventArgs e)
    {
        Response.Redirect("Employee.aspx");
    }

    private void DeleteImage()
    {
        System.IO.FileInfo file = new System.IO.FileInfo(Server.MapPath("~/documents/temp/") + Session.SessionID + ".jpg");
        if (file.Exists)
            file.Delete();
    }

    public void SetPermission()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();

        if (UserID != 1 && UserID != 2)
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Employee);
            if (dtm.Rows.Count > 0)
            {
                btnNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                btnList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                btnDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                btnEmail.Enabled = btnPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
                ViewState["MblnUpdatePermission"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }

            //GetCompanyWisePermission();
        }
        else
        {
            btnNew.Enabled = btnDelete.Enabled = btnEmail.Enabled = btnList.Enabled = btnPrint.Enabled = MblnUpdatePermission = true;
        }
    }


    private void GetCompanyWisePermission()
    {
        DataTable dt = clsRoleSettings.GetCompanyWisePermission(ddlCompany.SelectedValue.ToInt32(), new clsUserMaster().GetUserId());
        if (dt.Rows.Count > 0)
        {
            btnSubmit.Enabled = (Convert.ToBoolean(dt.Rows[0]["IsCreate"]) || Convert.ToBoolean(dt.Rows[0]["IsUpdate"]));
            btnDelete.Enabled = Convert.ToBoolean(dt.Rows[0]["IsDelete"]);
        }
        else
        {
            btnSubmit.Enabled = btnDelete.Enabled = false;
        }
    }


    public void EnableMenus()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        btnNew.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Employee);
        btnList.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Employee);
        btnEmail.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Email_Employee);
        btnPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Print_Employee);
        btnDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Delete_Employee);
        btnDelete.OnClientClick = "return false";
        btnPrint.OnClientClick = "return false;";
        btnEmail.OnClientClick = "return false;";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (new clsEmployeeNew().GetEmployeeCount() <= 1000)
        {
            SaveEmployee();
        }
    }

    private void EnableOrDiableUserCredentail(bool status)
    {
        //if (status)
        //{
        //    dvChkUser.Attributes.Add("style", "display:block");
        //    dvUserName.Attributes.Add("style", "display:block");
        //    dvPassword.Attributes.Add("style", "display:block");
        //    dvRole.Attributes.Add("style", "display:block");
        //}
        //else
        //{
        //    dvChkUser.Attributes.Add("style", "display:none");
        //    dvUserName.Attributes.Add("style", "display:none");
        //    dvPassword.Attributes.Add("style", "display:none");
        //    dvRole.Attributes.Add("style", "display:none");
        //}
    }

    protected void lnkremoveRecentPhoto_Click(object sender, EventArgs e)
    {
        ViewState["Buffer"] = null;
        objEmployee = new clsEmployee();
        objEmployee.EmployeeID = Convert.ToInt32(ViewState["EmployeeId"]);

        if (objEmployee.RemoveEmployeePhoto("RP") > 0)
        {
            imgRecentPhotoPreview.ImageUrl = GetEmployeeRecentPhoto(Convert.ToInt32(ViewState["EmployeeID"]), 120);
            lnkremoveRecentPhoto.Visible = false;
        }
    }
    protected void btnDesignation_Click(object sender, EventArgs e)
    {
        if (ddlDesignation != null)
        {
            DesignationReference.GetData();
            mdlPopDesignation.Show();
            updDesignation.Update();
        }
    }
    protected void lnkRemovePassportPhoto_Click(object sender, EventArgs e)
    {
        objEmployee = new clsEmployee();
        objEmployee.EmployeeID = Convert.ToInt32(ViewState["EmployeeId"]);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Employee.aspx");
    }

    protected void lnkBtnSalaryScale_Click(object sender, EventArgs e)
    {
        FormView3.ChangeMode(FormViewMode.Insert);
        controls_SalaryScale ucSalaryScale = (controls_SalaryScale)FormView3.FindControl("ucSalaryScale");
        AjaxControlToolkit.ModalPopupExtender mpeSalaryScale = (AjaxControlToolkit.ModalPopupExtender)FormView3.FindControl("mdlPopUpSalaryScale");

        if (ucSalaryScale != null && mpeSalaryScale != null)
        {
            updMdlSalaryScale.Update();
            mpeSalaryScale.Show();
        }
    }

    protected void btnMotherTongue_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "MotherTongueReference";
        ReferenceNew1.DataTextField = "MotherTongue";
        ReferenceNew1.DataTextFieldArabic = "MotherTongueArb";
        ReferenceNew1.DataValueField = "MotherTongueID";
        ReferenceNew1.FunctionName = "FillComboMotherTongue";
        ReferenceNew1.SelectedValue = ddlNationality.SelectedValue;
        ReferenceNew1.DisplayName = GetLocalResourceObject("MotherTongue.Text").ToString();
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }
    protected void btnCountry_Click(object sender, EventArgs e)
    {
        ReferenceNew1.ClearAll();
        ReferenceNew1.TableName = "CountryReference";
        ReferenceNew1.DataTextField = "CountryName";
        ReferenceNew1.DataValueField = "CountryId";
        ReferenceNew1.FunctionName = "FillComboCountry";
        ReferenceNew1.SelectedValue = ddlCountry.SelectedValue;
        ReferenceNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Country").ToString();
        ReferenceNew1.DataTextFieldArabic = "CountryNameArb";
        ReferenceNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }
    protected void ddlWorkStatus_SelectedIndexChanged(object sender, EventArgs e)
    {

        txtProbationEndDate.Enabled = ddlWorkStatus.SelectedValue.ToInt32() == 7;
        updWorkStatus.Update();


    }
    protected void chkEmployees_CheckedChanged(object sender, EventArgs e)
    {
        GetReportingTo();
    }
    protected void chkHOD_CheckedChanged(object sender, EventArgs e)
    {
        GetHOD();
    }

    private void GetHOD()
    {
        try
        {
            if (ddlDesignation.SelectedIndex >= 0)
            {
                objUser = new clsUserMaster();
                objEmployee = new clsEmployee();

                if (chkHOD.Checked)
                    objEmployee.CompanyID = -1;
                else
                    objEmployee.CompanyID = objUser.GetCompanyId();

                DataTable dt = objEmployee.GetHOD(ViewState["EmployeeId"].ToInt32(), ddlDesignation.SelectedValue.ToInt32());
                DataRow dr = dt.NewRow();
                dr[0] = "-1";
                dr[1] = clsGlobalization.IsArabicCulture() ? "اختر" : "Select";
                dt.Rows.InsertAt(dr, 0);
                ddlHOD.DataSource = dt;
                ddlHOD.DataBind();
                updHOD.Update();
            }
        }
        finally
        {
            objEmployee = null;
        }
    }

    //-------------------------------------KPI/KRA--------------------------------------------- 

    //private void ClearControls()
    //{
    //    ViewState["TableKPI"] = ViewState["TableKRA"] = null;
    //    EmpKpi.Clear();
    //    EmpKra.Clear();
    //    gvKpi.DataSource = null;
    //    gvKpi.DataBind();
    //    gvKra.DataSource = null;
    //    gvKra.DataBind();
    //}

    //private void BindInitials()
    //{
    //    DataSet ds = clsEmployee.GetKpiKra(ViewState["EmployeeId"].ToInt32());
    //    if (ds.Tables.Count > 0)
    //    {
    //        //KPI details
    //        DataTable dtKpi = ds.Tables[0];
    //        if (dtKpi.Rows.Count > 0)
    //        {
    //            ViewState["TableKPI"] = dtKpi;
    //            gvKpi.DataSource = dtKpi;
    //            gvKpi.DataBind();
    //            SetPreviousKpi();
    //        }
    //        else
    //            SetInitialRow();

    //        //KRA details
    //        DataTable dtKra = ds.Tables[1];
    //        if (dtKra.Rows.Count > 0)
    //        {
    //            ViewState["TableKRA"] = dtKra;
    //            gvKra.DataSource = dtKra;
    //            gvKra.DataBind();
    //            SetPreviousKra();
    //        }
    //        else
    //            SetInitialRowKRA();
    //    }
    //    else
    //    {
    //        SetInitialRow();
    //        SetInitialRowKRA();
    //    }
    //}

    #region KPI

    //private void SetInitialRow()
    //{
    //    EmpKpi.Add(new clsKPI() { KPIId = 0, KPIValue = 0, EmployeeKpiID = 0 });
    //    ViewState["TableKPI"] = EmpKpi;
    //    gvKpi.DataSource = EmpKpi;
    //    gvKpi.DataBind();
    //}

    //private void AddNewRowKPI()
    //{
    //    if (ViewState["TableKPI"] != null)
    //    {
    //        int rCount = 0;
    //        EmpKpi = new List<clsKPI>();
    //        foreach (GridViewRow dr in gvKpi.Rows)
    //        {
    //            TextBox txtKpiMarks = (TextBox)dr.FindControl("txtKpiMarks");
    //            DropDownList ddlKpi = (DropDownList)dr.FindControl("ddlKpi");
    //            if (txtKpiMarks.Text == "0" || txtKpiMarks.Text == string.Empty)
    //            {
    //                DispEmptyControlMessage("Enter Marks for KPI", "txt");
    //                break;
    //            }
    //            if (ddlKpi.SelectedIndex == -1 || ddlKpi.SelectedIndex == 0)
    //            {
    //                DispEmptyControlMessage("Select KPI", "ddl");
    //                break;
    //            }
    //            if (txtKpiMarks != null && ddlKpi != null)
    //            {
    //                EmpKpi.Add(new clsKPI()
    //                {
    //                    KPIId = ddlKpi.SelectedValue.ToInt32(),
    //                    KPIValue = txtKpiMarks.Text.Trim().ToDecimal(),
    //                    EmployeeKpiID = rCount
    //                });
    //            }
    //            rCount++;
    //        }
    //        EmpKpi.Add(new clsKPI() { KPIId = -1, KPIValue = 0, EmployeeKpiID = rCount });

    //        ViewState["TableKPI"] = EmpKpi;
    //        gvKpi.DataSource = EmpKpi;
    //        gvKpi.DataBind();
    //    }
    //    SetPreviousKpi();
    //    updKpiKra.Update();
    //    upnlKPI.Update();
    //}

    //private void DispEmptyControlMessage(string dispMsg, string val)
    //{
    //    string msg = string.Empty;
    //    switch (val)
    //    {
    //        case "txt":
    //            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : (dispMsg);
    //            ShowpopUpErrorMessage(msg);
    //            break;
    //        case "ddl":
    //            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : (dispMsg);
    //            ShowpopUpErrorMessage(msg);
    //            break;
    //    }
    //}

    //private void SetPreviousKpi()
    //{
    //    if (ViewState["TableKPI"] != null)
    //    {
    //        if (EmpKpi.Count > 0)
    //        {
    //            int i = 0;
    //            foreach (clsKPI jKpi in EmpKpi)
    //            {
    //                TextBox txtKpiMarks = (TextBox)gvKpi.Rows[i].FindControl("txtKpiMarks");
    //                DropDownList ddlKpi = (DropDownList)gvKpi.Rows[i].FindControl("ddlKpi");

    //                ddlKpi.SelectedValue = jKpi.KPIId.ToString();
    //                txtKpiMarks.Text = jKpi.KPIValue.ToString();
    //                i++;
    //            }
    //        }
    //        else if (((DataTable)ViewState["TableKPI"]).Rows.Count > 0)
    //        {
    //            DataTable dtKPI = (DataTable)ViewState["TableKPI"];
    //            for (int iCounter = 0; iCounter < gvKpi.Rows.Count; iCounter++)
    //            {
    //                TextBox txtKpiMarks = (TextBox)gvKpi.Rows[iCounter].FindControl("txtKpiMarks");
    //                DropDownList ddlKpi = (DropDownList)gvKpi.Rows[iCounter].FindControl("ddlKpi");

    //                ddlKpi.SelectedValue = dtKPI.Rows[iCounter]["KPIID"].ToString();
    //                txtKpiMarks.Text = dtKPI.Rows[iCounter]["KPIValue"].ToString();
    //            }
    //        }
    //    }
    //}

    //protected void imgKpiDel_Click(object sender, ImageClickEventArgs e)
    //{
    //    string msg = string.Empty;
    //    if (ViewState["TableKPI"] != null)
    //    {
    //        int Id = ((ImageButton)sender).CommandArgument.ToInt32();
    //        if (Id > 0)
    //        {
    //            //JobKpi.Remove(JobKpi.Find(t => t.Rowindex == Id));
    //            int rCount = 0;
    //            foreach (GridViewRow dr in gvKpi.Rows)
    //            {
    //                TextBox txtKpiMarks = (TextBox)dr.FindControl("txtKpiMarks");
    //                DropDownList ddlKpi = (DropDownList)dr.FindControl("ddlKpi");
    //                if (rCount != Id)
    //                {
    //                    if (txtKpiMarks != null && ddlKpi != null)
    //                    {
    //                        EmpKpi.Add(new clsKPI()
    //                        {
    //                            KPIId = ddlKpi.SelectedValue.ToInt32(),
    //                            KPIValue = txtKpiMarks.Text.Trim().ToDecimal(),
    //                            EmployeeKpiID = rCount
    //                        });
    //                    }
    //                }
    //                rCount++;
    //            }
    //            ViewState["TableKPI"] = EmpKpi;
    //            gvKpi.DataSource = EmpKpi;
    //            gvKpi.DataBind();
    //            SetPreviousKpi();
    //            upnlKPI.Update();
    //        }
    //        else
    //        {
    //            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
    //            ShowpopUpErrorMessage(msg);
    //        }
    //    }
    //}

    //protected void gvKpi_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowIndex >= 0)
    //    {
    //        DropDownList ddlKpi = (DropDownList)e.Row.FindControl("ddlKpi");
    //        ImageButton btnAddKpi = (ImageButton)e.Row.FindControl("btnAddKpi");

    //        if (ddlKpi != null && btnAddKpi != null)
    //        {
    //            ddlKpi.DataSource = clsEmployee.GetKpi();
    //            ddlKpi.DataBind();
    //        }
    //        if (EmpKpi.Count > 0)
    //            btnAddKpi.Visible = (e.Row.RowIndex == EmpKpi.Count - 1);
    //        else if (((DataTable)ViewState["TableKPI"]).Rows.Count > 0)
    //            btnAddKpi.Visible = (e.Row.RowIndex == ((DataTable)ViewState["TableKPI"]).Rows.Count - 1);
    //    }
    //}

    //protected void btnAddKpi_Click(object sender, ImageClickEventArgs e)
    //{
    //    AddNewRowKPI();
    //}

    #endregion

    #region KRA

    //private void SetInitialRowKRA()
    //{
    //    EmpKra.Add(new clsKRA() { KRAId = 0, KRAValue = 0, EmployeeKraID = 0 });
    //    ViewState["TableKRA"] = EmpKra;
    //    gvKra.DataSource = EmpKra;
    //    gvKra.DataBind();
    //}

    //private void AddNewRowKRA()
    //{
    //    if (ViewState["TableKRA"] != null)
    //    {
    //        int rCount = 0;
    //        EmpKra = new List<clsKRA>();
    //        foreach (GridViewRow dr in gvKra.Rows)
    //        {
    //            TextBox txtKraMarks = (TextBox)dr.FindControl("txtKraMarks");
    //            DropDownList ddlKra = (DropDownList)dr.FindControl("ddlKra");

    //            if (txtKraMarks.Text == "0" || txtKraMarks.Text == string.Empty)
    //            {
    //                DispEmptyControlMessage("Enter Marks for KRA", "txt");
    //                break;
    //            }
    //            if (ddlKra.SelectedIndex == -1 || ddlKra.SelectedIndex == 0)
    //            {
    //                DispEmptyControlMessage("Select KRA", "ddl");
    //                break;
    //            }

    //            if (txtKraMarks != null && ddlKra != null)
    //            {
    //                EmpKra.Add(new clsKRA()
    //                {
    //                    KRAId = ddlKra.SelectedValue.ToInt32(),
    //                    KRAValue = txtKraMarks.Text.Trim().ToDecimal(),
    //                    EmployeeKraID = rCount
    //                });
    //            }
    //            rCount++;
    //        }
    //        EmpKra.Add(new clsKRA() { KRAId = -1, KRAValue = 0, EmployeeKraID = rCount });

    //        ViewState["TableKRA"] = EmpKra;
    //        gvKra.DataSource = EmpKra;
    //        gvKra.DataBind();
    //    }
    //    SetPreviousKra();
    //    updKpiKra.Update();
    //}

    //private void SetPreviousKra()
    //{
    //    if (ViewState["TableKRA"] != null)
    //    {
    //        if (EmpKra.Count > 0)
    //        {
    //            int i = 0;
    //            foreach (clsKRA jKra in EmpKra)
    //            {
    //                TextBox txtKraMarks = (TextBox)gvKra.Rows[i].FindControl("txtKraMarks");
    //                DropDownList ddlKra = (DropDownList)gvKra.Rows[i].FindControl("ddlKra");

    //                ddlKra.SelectedValue = jKra.KRAId.ToString();
    //                txtKraMarks.Text = jKra.KRAValue.ToString();
    //                i++;
    //            }
    //        }
    //        else if (((DataTable)ViewState["TableKRA"]).Rows.Count > 0)
    //        {
    //            DataTable dtKRA = (DataTable)ViewState["TableKRA"];
    //            for (int iCounter = 0; iCounter < gvKra.Rows.Count; iCounter++)
    //            {
    //                TextBox txtKraMarks = (TextBox)gvKra.Rows[iCounter].FindControl("txtKraMarks");
    //                DropDownList ddlKra = (DropDownList)gvKra.Rows[iCounter].FindControl("ddlKra");

    //                ddlKra.SelectedValue = dtKRA.Rows[iCounter]["KRAID"].ToString();
    //                txtKraMarks.Text = dtKRA.Rows[iCounter]["KRAValue"].ToString();
    //            }
    //        }
    //    }
    //}

    //protected void btnAddKra_onClick(object sender, ImageClickEventArgs e)
    //{
    //    AddNewRowKRA();
    //}

    //protected void imgKraDel_Click(object sender, ImageClickEventArgs e)
    //{
    //    string msg = string.Empty;
    //    if (ViewState["TableKRA"] != null)
    //    {
    //        int Id = ((ImageButton)sender).CommandArgument.ToInt32();
    //        if (Id > 0)
    //        {
    //            //JobKpi.Remove(JobKpi.Find(t => t.Rowindex == Id));
    //            int rCount = 0;
    //            foreach (GridViewRow dr in gvKra.Rows)
    //            {
    //                TextBox txtKraMarks = (TextBox)dr.FindControl("txtKraMarks");
    //                DropDownList ddlKra = (DropDownList)dr.FindControl("ddlKra");
    //                if (rCount != Id)
    //                {
    //                    if (txtKraMarks != null && ddlKra != null)
    //                    {
    //                        EmpKra.Add(new clsKRA()
    //                        {
    //                            KRAId = ddlKra.SelectedValue.ToInt32(),
    //                            KRAValue = txtKraMarks.Text.Trim().ToDecimal(),
    //                            EmployeeKraID = rCount
    //                        });
    //                    }
    //                }
    //                rCount++;
    //            }
    //            ViewState["TableKRA"] = EmpKra;
    //            gvKra.DataSource = EmpKra;
    //            gvKra.DataBind();
    //            SetPreviousKra();
    //            upnlKRA.Update();
    //        }
    //        else
    //        {
    //            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
    //            ShowpopUpErrorMessage(msg);
    //        }
    //    }
    //}

    //protected void gvKra_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowIndex >= 0)
    //    {
    //        DropDownList ddlKra = (DropDownList)e.Row.FindControl("ddlKra");
    //        ImageButton btnAddKra = (ImageButton)e.Row.FindControl("btnAddKra");

    //        if (ddlKra != null && btnAddKra != null)
    //        {
    //            ddlKra.DataSource = clsEmployee.GetKra();
    //            ddlKra.DataBind();
    //        }
    //        if (EmpKra.Count > 0)
    //            btnAddKra.Visible = (e.Row.RowIndex == EmpKra.Count - 1);
    //        else if (((DataTable)ViewState["TableKRA"]).Rows.Count > 0)
    //            btnAddKra.Visible = (e.Row.RowIndex == ((DataTable)ViewState["TableKRA"]).Rows.Count - 1);
    //    }
    //}

    #endregion
}