﻿<%@ Page Language="C#" MasterPageFile="~/Master/ManagerHomeMasterPage.master" AutoEventWireup="true"
    CodeFile="HomeActiveVacancies.aspx.cs" Inherits="Public_HomeActiveVacancies"
    Title="Active Vacancies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
<div id='cssmenu'>
          <ul>
            <li ><a href='ManagerHomeNew.aspx'> <asp:Literal ID="Literal1" runat ="server" Text= '<%$Resources:MasterPageCommon,DashBoard%>'>
            </asp:Literal> </a></li>
             <li ><a href='HomeRecentActivity.aspx'><span> <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal> </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal> </span></a></li>
            <li  class="selected"><a href='HomeActiveVacancies.aspx'><span>  <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal> </span></a></li>
            <li><a href='HomeNewHires.aspx'><span> <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal></span></a></li>
            <li><a href='HomeAlerts.aspx'><span><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal></span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/active_vacancy.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b><asp:Literal ID="Literal7" runat ="server" meta:resourcekey="ActiveVacancies"></asp:Literal>  </b>
        </div>
    </div>
    <div style="float: left; width: 99%;">
        <asp:UpdatePanel ID="upActiveVacancies" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <table width="98%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="3" cellspacing="0" id="tblActiveVacancyHeader" runat="server"
                                align="right">
                                <tr>
                                    <td width="65px">
                                    </td>
                                     <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName"
                                            DataValueField="CompanyID" CssClass="dropdownlist" AutoPostBack="True"  onselectedindexchanged="ddlCompany_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlVacancyDepartments" runat="server" DataTextField="Department"
                                            DataValueField="DepartmentId" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlVacancyDepartments_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStatusFilter" runat="server" DataTextField="JobStatus"
                                            DataValueField="JobStatusID" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged"><%--OnSelectedIndexChanged="ddlStatusFilter_SelectedIndexChanged"--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <asp:DataList ID="dlActiveVacancies" runat="server" BorderWidth="0px" CellPadding="0"
                                Width="100%" OnPreRender="dlActiveVacancies_PreRender" OnItemDataBound="dlActiveVacancies_ItemDataBound"
                                DataKeyField="Status">
                                <HeaderTemplate>
                                    <div style="float: left; width: 98%;" class="datalistheader">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr style="text-align: left; width: 100%;">
                                                <td style="width: 50%;" align="left">
                                                   <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Vacancy"></asp:Literal> 
                                                </td>
                                                <td style="width: 12%;" align="left">
                                                   <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="LastDate"></asp:Literal>  
                                                </td>
                                                <td style="width: 29.5%;" align="left">
                                                    <asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Status"></asp:Literal>  
                                                </td>
                                                <%--<td style="width: 15%;" align="right">
                                                   <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="Action"></asp:Literal>   
                                                </td>--%>
                                            </tr>
                                        </table>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top: 5px;
                                        padding-left: 5px;">
                                        <tr class="Dashboradlink">
                                            <td class="datalistTrLeft" style="width: 50%;" align="left">
                                                <a href='<%# "VacancyView.aspx?JobID=" + Eval("JobId") %>' >
                                                    <%# Eval("JobTitle") %></a> <span class="item_title">( <%# Eval("CandidatesHired")%> 
                                                        
                                                         <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="hiredagainst"></asp:Literal>&nbsp;<%# Eval("RequiredQuota")%>
                                                    <asp:Literal ID="Literal11" runat ="server" meta:resourcekey="Vacancies"></asp:Literal>   ) </span>
                                            </td>
                                            <td class="datalistTrRight" style="width: 12%;" align="left">
                                                <%# Eval("LastDate")%>
                                            </td>
                                            <td class="datalistTrRight" style="width: 29.5%;">
                                                <%# Eval("Status")%>
                                            </td>
                                            <%--<td width="100px" class="datalistTrRight" align="right" style ="padding-right:5px;">
                                                <a id="ancApprove" class="actionbutton" runat="server" href='<%# "Job.aspx?JobId=" + Eval("JobId") + "&Approval=true" %>'
                                                   meta:resourcekey="Approve"  style="width:50px;text-align:center"></a>
                                                <asp:HiddenField ID="hfID" runat="server" Value='<%# Eval("JobId")%>' />
                                            </td>--%>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="pgrActiveVacancies" runat="server" OnFill="BindVacancies" PageSize="5" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: block;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

