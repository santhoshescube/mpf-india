﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="TradeLicense.aspx.cs" Inherits="Public_TradeLicense" Title="Trade License" %>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <div id='cssmenu'>
        <ul>
            <li><a href="Passport.aspx">
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx">
                <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx">
                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx">
                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1">
                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2">
                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3">
                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx">
                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li class="selected"><a href="TradeLicense.aspx">
                <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx">
                <asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx">
                <asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx">
                <asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx">
                <asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="server">
    <div style="width: 100%">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <asp:UpdatePanel ID="upTradeLicense" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="dvAddTradeLicense" runat="server" style="margin-left: 10px; margin-top: 10px">
                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                    <asp:HiddenField ID="hdTradeLicenseID" runat="server" Visible="false" />
                    <div style="width: 100%">
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Company--%>
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Company"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:DropDownList ID="ddlCompany" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                    DataValueField="CompanyID" DataTextField="CompanyName">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" CssClass="error" meta:resourcekey="Pleaseselectcompany"
                                    Display="Dynamic" ControlToValidate="ddlCompany" ValidationGroup="Employee" InitialValue="-1">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Sponsor--%>
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Sponsor"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtSponsor" CssClass="textbox_mandatory" MaxLength="50" runat="server"
                                    Width="182px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvSponsor" runat="server" ControlToValidate="txtSponsor"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseentersponsor" ValidationGroup="Employee">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Sponsor Fee--%>
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="SponsorFee"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtSponsorFee" CssClass="textbox_mandatory" MaxLength="12" runat="server"
                                    Width="182px"></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterMode="ValidChars"
                                    FilterType="Numbers,Custom" TargetControlID="txtSponsorFee" ValidChars=".">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                <asp:RequiredFieldValidator ID="rfvSponsorFee" runat="server" ControlToValidate="txtSponsorFee"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseentersponsorfee" ValidationGroup="Employee">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--License Number--%>
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="LicenseNumber"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtLicenseNumber" CssClass="textbox_mandatory" MaxLength="50" runat="server"
                                    Width="182px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvLicenseNumber" runat="server" ControlToValidate="txtLicenseNumber"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseenterlicensenumber"
                                    ValidationGroup="Employee">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--City--%>
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="City"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtCity" CssClass="textbox_mandatory" MaxLength="50" runat="server"
                                    Width="182px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="txtCity"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseentercity" ValidationGroup="Employee">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Province--%>
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Province"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:DropDownList ID="ddlProvince" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                    DataValueField="ProvinceID" DataTextField="Province">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Partner--%>
                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Partner"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtPartner" CssClass="textbox_mandatory" MaxLength="50" runat="server"
                                    Width="182px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvPartner" runat="server" ControlToValidate="txtPartner"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseenterpartner" ValidationGroup="Employee">
                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Issue Date--%>
                                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                    Text='<%# GetDate(Eval("IssueDate")) %>'>
                                </asp:TextBox>
                                <div class="firstTrRight" style="float: left; width: 5%; height: 29px; padding-top: 1px;">
                                    <asp:ImageButton ID="btnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                        CausesValidation="False" />
                                    <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                        Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                                </div>
                                <div class="firstTrRight" style="float: left; width: 60%; height: 29px">
                                    <asp:CustomValidator ID="cvTradeLicenseIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                        ClientValidationFunction="validateTradeLicenseIssuedate" CssClass="error" ValidateEmptyText="True"
                                        ValidationGroup="Employee" Display="Dynamic"></asp:CustomValidator>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Expiry Date--%>
                                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                    Text='<%# GetDate(Eval("ExpiryDate")) %>'>
                                </asp:TextBox>
                                <div class="firstTrRight" style="float: left; width: 5%; height: 29px; padding-top: 1px;">
                                    <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                        CausesValidation="False" />
                                    <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                        Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                </div>
                                <div class="firstTrRight" style="float: left; width: 60%; height: 29px">
                                    <asp:CustomValidator ID="cvTradeLicenseExpirydate" runat="server" ControlToValidate="txtExpiryDate"
                                        CssClass="error" ValidateEmptyText="True" ValidationGroup="Employee" ClientValidationFunction="validateTradeLicenseExpirydate"
                                        Display="Dynamic"></asp:CustomValidator>
                                </div>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Other Info--%>
                                <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: auto">
                                <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="textbox" Width="300px" TextMode="MultiLine"
                                    Height="76px" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <%--Current Status--%>
                                <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: auto">
                                <asp:Label ID="lblCurrentStatus" runat="server" Text="New" Width="30%"></asp:Label>
                            </div>
                        </div>
                        <div style="float: left; width: 100%">
                            <div id="divParse">
                                <fieldset style="padding: 10px 3px">
                                    <legend>
                                        <%--Add Documents--%>
                                        <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>
                                    </legend>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td valign="top">
                                                <%-- Document Name--%>
                                                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                            </td>
                                            <td style="width: 3px" valign="top">
                                            </td>
                                            <td valign="top">
                                                <%-- Choose file..--%>
                                                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:DocumentsCommon,ChooseFile%>'></asp:Literal>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" width="175px">
                                                <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="50" Width="150px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee1"></asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvTradeLicensedocnameduplicate" runat="server" ClientValidationFunction="validateTradeLicensedocnameduplicate"
                                                    ControlToValidate="txtDocname" CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'
                                                    ValidationGroup="Employee1"></asp:CustomValidator>
                                            </td>
                                            <td style="width: 2px">
                                            </td>
                                            <td valign="top" width="300px">
                                                <AjaxControlToolkit:AsyncFileUpload ID="fuTradeLicense" runat="server" OnUploadedComplete="fuTradeLicense_UploadedComplete"
                                                    PersistFile="True" Width="310px" />
                                                <asp:CustomValidator ID="cvTradeLicenseAttachment" runat="server" ClientValidationFunction="validateTradeLicenseAttachment"
                                                    CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                                    ValidateEmptyText="True" ValidationGroup="Employee1"></asp:CustomValidator>
                                            </td>
                                            <td style="padding-left: 15px" valign="top" width="200px">
                                                <asp:LinkButton ID="btnattachTradeLicensedoc" runat="server" Enabled="true" OnClick="btnattachTradeLicensedoc_Click"
                                                    Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="Employee1">
                                                </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            <div id="divTradeLicenseDoc" runat="server" class="container_content" style="display: none;
                                height: 150px; overflow: auto">
                                <asp:UpdatePanel ID="updTradeLicenseDoc" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DataList ID="dlTradeLicenseDoc" runat="server" Width="100%" DataKeyField="Node"
                                            GridLines="Horizontal" OnItemCommand="dlTradeLicenseDoc_ItemCommand">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <HeaderTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="170">
                                                            <%-- Document Name--%>
                                                            <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                        </td>
                                                        <td width="150" align="left">
                                                            <%--File Name--%>
                                                            <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td width="170">
                                                            <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="170px"></asp:Label>
                                                        </td>
                                                        <td width="150" align="left">
                                                            <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                                        </td>
                                                        <td width="25" align="left">
                                                            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"
                                                                CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%">
                        <div align="right">
                            <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                ToolTip='<%$Resources:ControlsCommon,Submit%>' CommandName="SUBMIT" ValidationGroup="Employee"
                                OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Cancel%>'
                                ToolTip='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="false" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>
                <asp:DataList ID="dlTradeLicense" runat="server" BorderWidth="0px" CellPadding="0"
                    CssClass="labeltext" Width="100%" OnItemCommand="dlTradeLicense_ItemCommand"
                    DataKeyField="TradeLicenseID" OnItemDataBound="dlTradeLicense_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; margin-left: 5px;">
                            <div class="firstTrLeft" style="float: left; width: 2%; height: 29px">
                                <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkCompany');" />
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <%--Select All--%>
                                 <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <HeaderStyle />
                    <ItemTemplate>
                        <div style="width: 100%; margin-right: 10px;" id="tblDetails" runat="server">
                            <div style="width: 100%;">
                                <div class="firstTrLeft" style="float: left; width: 2%; height: 29px">
                                    <asp:CheckBox ID="chkCompany" runat="server" />
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 85%; height: 29px">
                                    <asp:LinkButton ID="btnCompany" CausesValidation="false" runat="server" CommandArgument='<%# Eval("TradeLicenseID")%>'
                                        CommandName="VIEW" Text='<%# Eval("CompanyName")%>' CssClass="listHeader bold"></asp:LinkButton>
                                    [<%# Eval("ShortName")%>] &nbsp <%# Eval("LicenseNo")%>
                                    <%# Eval("LicenceNumber")%>
                                </div>
                                <div class="firstTrRight" style="float: left; width: 5%; height: auto">
                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                        display: block;" CommandArgument='<%# Eval("TradeLicenseID")%>' CommandName="ALTER"
                                        ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                                </div>
                                <div style="visibility: hidden">
                                    <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="innerdivheader" style="float: left; width: 15%; margin-left: 40px;">
                                    <%--Sponsor--%>
                                    <asp:Literal ID="Literal18" runat="server"  meta:resourcekey="Sponsor"></asp:Literal>
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 5%;">
                                    :
                                </div>
                                <div class="firstTrRight" style="float: left; width: 60%;">
                                    <%# Eval("Sponsor")%>
                                </div>
                                <div class="firstTrRight" style="color: #FF0000; font-weight: bold; float: left;
                                    width: 5%;">
                                    <%# Eval("IsRenewed")%>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="innerdivheader" style="float: left; width: 15%; margin-left: 40px;">
                                    <%--City--%>
                                    <asp:Literal ID="Literal19" runat="server"  meta:resourcekey="City"></asp:Literal>
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 5%;">
                                    :
                                </div>
                                <div class="firstTrRight" style="float: left; width: 70%;">
                                    <%# Eval("City")%>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="innerdivheader" style="float: left; width: 15%; margin-left: 40px;">
                                    <%--Issue Date--%>
                                    <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 5%;">
                                    :
                                </div>
                                <div class="firstTrRight" style="float: left; width: 70%;">
                                    <%# Eval("IssueDate")%>
                                </div>
                            </div>
                            <div style="width: 100%;">
                                <div class="innerdivheader" style="float: left; width: 15%; margin-left: 40px;">
                                    <%--Expiry Date--%>
                                    <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                </div>
                                <div class="firstTrLeft" style="float: left; width: 5%;">
                                    :
                                </div>
                                <div class="firstTrRight" style="float: left; width: 70%;">
                                    <%# Eval("ExpiryDate")%>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <asp:FormView ID="fvTradeLicense" runat="server" BorderWidth="0px" CellPadding="0"
                    Width="100%" CellSpacing="0" DataKeyNames="TradeLicenseID,IsDelete">
                    <ItemTemplate>
                        <div id="mainwrap">
                            <div id="main2" style="margin-top: 10px; margin-left: 10px">
                                <div class="t22">
                                    <div class="ta22">
                                        <div class="maindivheader">
                                            <div class="innerdivheader">
                                                <%--Company--%><asp:Literal ID="Literal18" runat="server" meta:resourcekey="Company"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Sponsor--%><asp:Literal ID="Literal21" runat="server" meta:resourcekey="Sponsor"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Sponsor Fee--%><asp:Literal ID="Literal22" runat="server" meta:resourcekey="SponsorFee"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--License Number--%><asp:Literal ID="Literal23" runat="server" meta:resourcekey="LicenseNumber"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--City--%><asp:Literal ID="Literal24" runat="server" meta:resourcekey="City"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Province--%><asp:Literal ID="Literal25" runat="server" meta:resourcekey="Province"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Partner--%><asp:Literal ID="Literal26" runat="server" meta:resourcekey="Partner"></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Issue Date--%>
                                                <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Expiry Date--%>
                                                <asp:Literal ID="Literal28" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                                <%--Is Renewed--%>
                                                        <asp:Literal ID="Literal29" runat="server" Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>
                                                </div>
                                            <div class="innerdivheader">
                                               <%--Other Info--%>
                                               <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                                                </div>
                                            <div class="innerdivheader" style="visibility: hidden">
                                                <%--Is Delete Possible--%><asp:Literal ID="Literal31" runat="server" meta:resourcekey="IsDeletePossible"></asp:Literal>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="ta3" style="width: 70%;">
                                        <div class="maindivheaderValue">
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("CompanyName")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("Sponsor")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("SponserFee")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("LicenceNumber")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("City")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("Province")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("Partner")%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# GetDate(Eval("IssueDate"))%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# GetDate(Eval("ExpiryDate"))%></div>
                                            <div class="innerdivheaderValue" style="width: 100%;">
                                                <%# Eval("IsRenewed")%></div>
                                            <div class="innerdivheaderValue" style="word-break: break-all; width: 100%;">
                                                <%# Eval("Remarks")%></div>
                                            <div class="innerdivheader" style="visibility: hidden; width: 100%;">
                                                <%# Eval("IsDelete")%></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
                <uc:Pager ID="pgrTradeLicense" runat="server" OnFill="Bind" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="fuTradeLicense" EventName="UploadedComplete" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="text-align: center; margin-top: 10px;">
            <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlTradeLicense" EventName="ItemCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upPrint" runat="server">
            <ContentTemplate>
                <div id="divPrint" runat="server" style="display: none">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="display: none">
                    <asp:Button ID="btnIssueReceipt" runat="server" />
                </div>
                <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                    <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                    </uc1:DocumentReceiptIssue>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                    PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                </AjaxControlToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:ControlsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="ImgSearch" OnClick="btnSearch_Click" ImageUrl="~/images/search.png"
                        runat="server" />
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/Add%20project.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnNew" runat="server" CausesValidation="False" OnClick="btnNew_Click"
                           meta:resourcekey="AddTradeLicense"></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/List project.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click" CausesValidation="False"
                            meta:resourcekey="ViewTradeLicense"></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'> 
                           <%--Delete--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnPrint" runat="server" OnClick="btnPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'>
                           <%--Print--%></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnEmail" runat="server" OnClick="btnEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'>
                         
                            <%--Email--%>  </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
