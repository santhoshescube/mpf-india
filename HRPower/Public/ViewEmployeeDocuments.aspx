﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="ViewEmployeeDocuments.aspx.cs"
    Inherits="Public_ViewEmployeeDocuments"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>View Documents</title>
<script src="../js/HRPower.js" type="text/javascript"></script>

    <link href="../css/default_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/managerview.css" rel="stylesheet" type="text/css" />
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.css" rel="stylesheet" type="text/css" />
    <link href="../css/inettuts.js.css" rel="stylesheet" type="text/css" />
    <link href="../css/SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css" />
    <link href="../css/singleview.css" rel="stylesheet" type="text/css" />
    <link href="../css/Controls.css" rel="stylesheet" type="text/css" />
    <link href="../css/popup.css" rel="stylesheet" type="text/css" />

    <script src="../js/HRPower.js" type="text/javascript"></script>

    <script src="../js/Controls.js" type="text/javascript"></script>

    <script src="../js/Common.js" type="text/javascript"></script>

    <script src="../js/jquery/jquery.js?Math.Round()" type="text/javascript"></script>

    <script src="../js/jquery/jquery.timeentry.js?Math.Round()" type="text/javascript"></script>

    <%-- JQuery Auto Complete--%>
    <link href="../js/jquery/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />

    <script src="../js/jquery/jquery.autocomplete.js" type="text/javascript"></script>

    <script src="../js/jquery/jquery.bgiframe.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../js/jquery/jquery.ui.widget.js"></script>

    <link href="../js/jquery/css/jquery.ui.all.css" rel="stylesheet" type="text/css" />
    <link href="../css/AjaxControlToolkit.css" rel="stylesheet" type="text/css" />
    
    <link href="../css/default_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/styless_common.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
  
   
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>   
        <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>        
            <div style="width: 100%; float: left; overflow: auto;">
            <div style="width: 30%; float: left; overflow: auto;">
                <asp:UpdatePanel ID="upEmployeeDocument" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="min-height: 800px; height: auto; display: block;">
                            <asp:TreeView ID="tvEmployeeDocument" runat="server" NodeStyle-CssClass="treeNode2"
                                SelectedNodeStyle-CssClass="treeNodeSelected2" ShowLines="True" OnSelectedNodeChanged="tvEmployeeDocument_SelectedNodeChanged">
                            </asp:TreeView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="width: 70%; float: left; overflow: auto;">
            <asp:UpdatePanel ID="updivContent" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 100%; margin-left: 2%; margin-right: 1%; float: right">
                        <div id="divContent" runat="server" style="width: 98%; padding-left: 1%; padding-right: 1%;
                            padding-bottom: 5%; float: left; border-radius: 5px;">
                        </div>
                        <div id="divDocumentFiles" runat="server" style="width: 98%; padding-left: 1%; padding-right: 1%;
                            padding-bottom: 5%; margin-top: 20px; float: left; border-radius: 5px;">
                            <asp:DataList ID="dlDoctmentsFiles" runat="server" OnItemCommand="dlDoctmentsFiles_ItemCommand"
                                DataKeyField="FileName" GridLines="Horizontal" Width="100%" BorderStyle="None">
                                <FooterStyle BackColor="Black" />
                                <AlternatingItemStyle BackColor="White" BorderStyle="None" />
                                <ItemStyle BackColor="#E8EEF0" BorderStyle="None" />
                                <HeaderTemplate>
                                    <div style="width: 100%; height: 20px; padding-top: 20px; font-size: medium; font-weight: bold;
                                        text-align: center; color: #000000;">
                                        <%-- Attached Documents--%><asp:Literal ID="Literal1" runat="server" meta:resourcekey="AttachedDocuments"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 100%;">
                                        <div style="float: left; width: 80%; font-weight: bold; color: #000000;" class="trLeft">
                                            <%--Document Name--%><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                            </td>
                                        </div>
                                        <div style="float: right; width: 20%;" class="trRight">
                                        </div>
                                </HeaderTemplate>
                                <HeaderStyle BorderStyle="None" />
                                <ItemTemplate>
                                    <div style="float: left; width: 100%;">
                                        <div style="float: left; width: 68%; padding-left: 2%;" class="trLeft">
                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                Width="150px"></asp:Label>
                                        </div>
                                        <div style="float: left; width: 5%;" class="trLeft">
                                            <a href='<%#GetViewLink(Eval("FileName")) %>' target="_blank" title="View">
                                                <asp:Image ID="imgThumbnail" runat="server" Height="20px" Width="20px" ImageUrl='<%#GetViewLink(Eval("FileName"))%>'
                                                    onerror="imgNotImage(this);" />
                                            </a>
                                        </div>
                                        <div style="float: left; width: 5%;" class="trLeft">
                                            <a href='<%#GetViewLink(Eval("FileName")) %>' download='<%#GetViewLink(Eval("FileName")) %>'
                                                title="Download">
                                                <%--Download--%><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,Download%>'></asp:Literal>
                                            </a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
