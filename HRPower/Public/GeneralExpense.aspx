﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="GeneralExpense.aspx.cs" Inherits="Public_GeneralExpense" Title="General Expense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<%-----------------Left Menu-----------------%>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <%--Job.aspx--%>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li class='selected'><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<%-----------------Page Contents-----------------%>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script src="../js/Common.js" type="text/javascript"></script>

    <%-----------------View for Adding New general Expense-----------------%>
    <asp:UpdatePanel ID="upnlAdd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAdd" runat="server" style="display: none; padding-top: 20px;">
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,Company%>'></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName" DataValueField="CompanyID"
                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Width="200px" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvddlCompanyName" runat="server" CssClass="error"
                            ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlCompany" ValidationGroup="submit"
                            InitialValue="-1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="ExpenseCategory"></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:DropDownList ID="ddlExpenseCategory" DataTextField="ExpenseCategory" DataValueField="ExpenseCategoryID"
                            OnSelectedIndexChanged="ddlExpenseCategory_SelectedIndexChanged" runat="server"
                            Width="200px" AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvddlExpenseCategory" runat="server" CssClass="error"
                            ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlExpenseCategory" ValidationGroup="submit"
                            InitialValue="-1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <asp:UpdatePanel ID="upnlVacancy" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Label ID="lblVacancy" runat="server"></asp:Label>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:DropDownList ID="ddlVacancy" runat="server" Width="200px" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvVacancy" runat="server" CssClass="error" Display="Dynamic"
                                    ControlToValidate="ddlVacancy" InitialValue="-1"></asp:RequiredFieldValidator><%--ValidationGroup="submit"--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="float: left; width: 96%;">
                    <asp:UpdatePanel ID="upnlExpenseType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="ExpenseType"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:DropDownList ID="ddlExpenseType" runat="server" Width="200px" AutoPostBack="true">
                                </asp:DropDownList>
                                <asp:Button ID="btnExpenseType" runat="server" CausesValidation="False" CommandName="ExpenseType"
                                    CssClass="referencebutton" OnClick="btnExpenseType_Click" Text="..." />
                                <asp:RequiredFieldValidator ID="rfvExpenseType" runat="server" CssClass="error" Display="Dynamic"
                                    ControlToValidate="ddlExpenseType" InitialValue="-1"></asp:RequiredFieldValidator><%--ValidationGroup="submit"--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="ExpenseNumber"></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:TextBox ID="txtExpenseNumber" MaxLength="30" runat="server" CssClass="textbox_mandatory"
                            Width="100px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvExpenseNumber" runat="server" ErrorMessage="*"
                            ControlToValidate="txtExpenseNumber" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="ExpenseDate"></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:TextBox ID="txtExpenseDate" runat="server" CssClass="textbox_mandatory" Width="100px"></asp:TextBox>&nbsp;
                        <asp:ImageButton ID="ibtnExpenseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                            CausesValidation="false" CssClass="imagebutton" />
                        <AjaxControlToolkit:CalendarExtender ID="cextExpenseDate" runat="server" TargetControlID="txtExpenseDate"
                            Format="dd/MM/yyyy" PopupButtonID="ibtnExpenseDate">
                        </AjaxControlToolkit:CalendarExtender>
                        <asp:CustomValidator ID="cvtxtExpenseDate" runat="server" CssClass="error" ControlToValidate="txtExpenseDate"
                            Display="Dynamic" ValidationGroup="submit" ClientValidationFunction="CheckFuturedate"></asp:CustomValidator>
                         <asp:RequiredFieldValidator ID="rfvtxtExpenseDate" runat="server" ErrorMessage="*"
                        ControlToValidate="txtExpenseDate" Display="Dynamic" ValidationGroup="submit"
                        CssClass="error"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="ExpenseAmount"></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:TextBox ID="txtExpenseAmount" runat="server" MaxLength="7" Width="10%" Text="0"></asp:TextBox>
                        &nbsp;
                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeExpenseAmount" runat="server"
                            FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789." TargetControlID="txtExpenseAmount">
                        </AjaxControlToolkit:FilteredTextBoxExtender>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                        <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                            onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                            TextMode="MultiLine" Height="50px"></asp:TextBox>
                    </div>
                </div>
                <div style="float: left; width: 96%;">
                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px;" margin-top="100px;">
                    </div>
                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                        <asp:HiddenField ID="hfmode" runat="server" />
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                            ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" ValidationGroup="submit"
                            CommandArgument='<%# Eval("RequestId") %>' OnClick="btnSubmit_Click" />&nbsp;
                        <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                            CausesValidation="false" OnClick="btnCancel_Click" ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                            Width="75px" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------Pop-Up for ErrorMessages-----------------%>
    <div style="height: auto">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" Text="" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceNew1" runat="server" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------View All general Expenses-----------------%>
    <asp:UpdatePanel ID="upnlViewAll" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewAll" runat="server" style="display: block;">
                <asp:DataList ID="dlGeneralExpense" runat="server" Width="100%" DataKeyField="ExpenseID"
                    CssClass="labeltext" OnItemCommand="dlGeneralExpense_ItemCommand" OnItemDataBound="dlGeneralExpense_ItemDataBound">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="selectAll(this.id, 'chkSelect');" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%-- Select All--%>
                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkSelect" runat="server" /></div>
                            <div style="width: 30%; height: 30px; float: left; margin-right: 50%">
                                <asp:LinkButton ID="lnkLeave" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("ExpenseID") %>'
                                    CommandName="VIEW" CausesValidation="False"><%# Eval("ExpenseNumber")%></asp:LinkButton>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ExpenseID") %>'
                                    CommandName="EDIT" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Company"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("CompanyName")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ExpenseCategory"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("ExpenseCategory")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="ExpenseType"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("ExpenseType")%>&nbsp; <font color="red">
                                <%# Eval("Status")%>
                            </font>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="CreatedBy"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("EmployeeFullName")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="CreatedDate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("CreatedDate")%>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="Pager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------View Single general Expense-----------------%>
    <asp:UpdatePanel ID="upnlSingleViewExpense" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleViewExpense" runat="server" style="width: 100%; height: 300px;
                float: left; padding-top: 2px;">
                <asp:DataList ID="dlGeneralExpenseView" runat="server" Width="100%">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table width="95%" border="0" cellpadding="3" class="datalistheader">
                            <tr>
                                <td valign="top" width="100%" style="padding-left: 5px;">
                                    <asp:Literal ID="Literal42" runat="server" meta:resourcekey="GeneralExpense"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Company"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("CompanyName")%>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="ExpenseCategory"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("ExpenseCategory")%>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="ExpenseType"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("ExpenseType")%>&nbsp; <font color="red">
                                <%# Eval("Status")%></font>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal16" runat="server" meta:resourcekey="ExpenseDate"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("ExpenseDate")%>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="ExpenseAmount"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("ExpenseAmount")%>&nbsp;[<%# Eval("Currency")%>]
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="CreatedBy"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("EmployeeFullName")%>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal19" runat="server" meta:resourcekey="CreatedDate"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px">
                            <%# Eval("CreatedDate")%>
                        </div>
                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                            <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                        </div>
                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                            :
                        </div>
                        <div class="trright" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                            <%# Eval("Remarks")%>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divNoData" class="error" runat="server" style="float: left; width: 96%;
                display: none;">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<%-----------------Right Menu-----------------%>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:Image ID="imgNewGereneralExpense" ImageUrl="~/images/add_general.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkNewGereneralExpense" runat="server" CausesValidation="False"
                            OnClick="lnkNewGereneralExpense_Click" meta:resourcekey="AddGereneralExpense">
                         
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgListGereneralExpense" CausesValidation="false" ImageUrl="~/images/view_general.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkViewGereneralExpense" runat="server" CausesValidation="False"
                            OnClick="lnkViewGereneralExpense_Click" meta:resourcekey="ViewGereneralExpense"> 
                       
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgDeleteGereneralExpense" ImageUrl="~/images/delete.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDeleteGereneralExpense" runat="server" CausesValidation="False"
                            OnClick="lnkDeleteGereneralExpense_Click" meta:resourcekey="Delete"> 
                        </asp:LinkButton></h5>
                </div>
                <%--  <div class="name">
                <h5><asp:LinkButton ID="ln" runat="server" CausesValidation="false" OnClick="ln_click" Text= "GE"></asp:LinkButton></h5>
                </div>
                --%>
            </div>
            <%-- ---------------------------test---------------------------------------- --%>
            <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="Div1" runat="server" style="display: none;">
                <uc:GeneralExpense ID="ge" runat="server" ModalPopupID="mdlPopUpReference1" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference1" runat="server" TargetControlID="Button1"
                PopupControlID="Div1" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
