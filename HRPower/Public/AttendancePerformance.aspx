﻿<%@ Page Language="C#" MasterPageFile="~/Master/ManagerHomeMasterPage.master" AutoEventWireup="true" CodeFile="AttendancePerformance.aspx.cs" 
Inherits="Public_AttendancePerformance" Title="Attendance Performance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
   <div id='cssmenu'>
     <%--   <ul>
            <li ><a href="SalesPerformance.aspx">Target Performance</a></li>
            <li class='selected'><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
            <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li><a href="PerformanceAction.aspx"><span>Performance Action</span></a></li>
        </ul>--%>
        
        
           <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li class='selected'><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
        
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
    <div id="dvHavePermission" runat="server">
        <div id="divMessagePopUp">
            <div style="display: none">
                <asp:Button ID="btnMessage" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="ucMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnMessage"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <div id="divFilter" runat="server" style="margin-top: 25px; width: 100%; height: 40px;
            font-family: Tahoma; font-size: 12px; color: Black;">
            <asp:UpdatePanel ID="upnlFilter" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 30%; float: left;">
                        <div class="trLeft" style="width: 35%; float: left; padding-left: 15px;">
                            <asp:Literal ID="literal1" runat="server" Text ='<%$Resources:ControlsCommon,Department %>'>
                            </asp:Literal>
                        </div>
                        <div style="width: 55%; float: left;">
                            <asp:UpdatePanel ID="upnlDepartment" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" Width="150px" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Height="25px" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 20%; float: left; padding: 0px; margin-left:10px;">
                        <div class="trLeft" style="width: 25%; float: left; padding: 0px; vertical-align: top;">
                          <%--  Month--%>
                          <asp:Literal ID="literal2" runat="server" Text ='<%$Resources:ControlsCommon,Month %>'>
                            </asp:Literal>
                        </div>
                        <div style="width: 64%; float: left; padding: 0px;">
                            <asp:TextBox ID="txtPeriod" runat="server" Style="margin-right: 4px;" Width="68px" />
                            <asp:ImageButton ID="ibtnPeriod" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="false" />
                            <AjaxControlToolkit:CalendarExtender ID="cePeriod" runat="server" Format="MMM-yyyy"
                                TargetControlID="txtPeriod" PopupButtonID="ibtnPeriod" />
                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftePeriod" TargetControlID="txtPeriod"
                                runat="server" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                ValidChars="-" />
                        </div>
                    </div>
                    <div style="width: 25%; float: left; padding: 0px;">
                        <div class="trLeft" style="width: 33%; float: left; vertical-align: top; padding: 0px;">
                           <%-- Employee--%>
                           <asp:Literal ID="literal3" runat="server" Text ='<%$Resources:ControlsCommon,Employee %>'>
                            </asp:Literal>
                        </div>
                        <div style="width: 60%; float: left;">
                            <asp:UpdatePanel ID="upnlEmployee" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlEmployee"  AutoPostBack="true"   OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged" runat="server" Width="150px" Height="25px" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                     <asp:UpdatePanel ID="updType" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                    <div id="divType" runat="server" style="display: none; width: 15%; float: left; padding: 0px;">
                        <div style="width: 25%; float: left;">
                           
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                        Height="25px" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                        <asp:ListItem Value="1" Text='<%$Resources:ControlsCommon,Attendance %>'></asp:ListItem>
                                        <asp:ListItem Value="2" Text ='<%$Resources:ControlsCommon,Leave %>'></asp:ListItem>
                                    </asp:DropDownList>
                                
                        </div>
                    </div>
                    </ContentTemplate>
                            </asp:UpdatePanel>
                    <div style=" width: 6%; float: right; margin-top: -5px; margin-right:20px;">
                        <asp:Button ID="btnShow" meta:resourcekey="show" runat="server" Width="51px" CssClass="btnsubmit"
                            OnClick="btnShow_Click" OnClientClick="return ValidateFilter();" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
              
                <%-- Added by Laxmi Attendance Chart--%>
                <div id="AttendanceChart" visible ="false" runat ="server" style="background-color: White; border: solid 1px grey; float: left; margin-top: 10px;
                 margin-left: 12px; font-size: 12px; overflow: auto; height: 550px;  max-width: 950px;">
                <%-- <asp:UpdatePanel ID="updJobStatus" runat="server" UpdateMode="Conditional">
                     <ContentTemplate>--%>
                         <asp:Chart ID="ChartAttendance" runat="server" Height="500px" Width="1250px" CssClass="divJobChart">
                             <Titles>
                                 <asp:Title Name="Title1"  Docking="Top" />
                             </Titles>
                             <Legends>
                                 <asp:Legend Alignment="Near" Docking="Top" IsTextAutoFit="true"   LegendStyle="Row"
                                     Enabled="false" />
                             </Legends>
                             <Series>
                                 <asp:Series Name="Default" IsVisibleInLegend="false"  />
                             </Series>
                             <ChartAreas>
                                 <asp:ChartArea Name="ChartArea1" BackColor="White"  />
                             </ChartAreas>
                         </asp:Chart>
                    <%-- </ContentTemplate>
                 </asp:UpdatePanel>--%>
             </div>
                
                
            </ContentTemplate>
        </asp:UpdatePanel>
       
    </div>
    <div id="dvNoPermission" style="display:none ; text-align: center; margin-top: 10px" runat="server">
        <asp:Label ID="lblNoPermission" runat="server" CssClass="error" Text="You have no permission">
        </asp:Label>
    </div>
</asp:Content>

