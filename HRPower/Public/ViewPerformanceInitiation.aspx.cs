﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;

public partial class Public_ViewPerformanceInitiation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        peformanceDetailsPager.Fill += new controls_Pager.FillPager(FillPeformanceInitiation);
        if (!IsPostBack)
        {
            SetPermission();  
            FillPeformanceInitiation();
        }

        EnableDisableMenus();
   }





    private void EnableDisableMenus()
    {
        if (ViewState["IsView"] != null && Convert.ToBoolean(ViewState["IsView"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }

        lnkAdd.Enabled =  Convert.ToBoolean(ViewState["IsCreate"]);
        lnkViewInitiation.Enabled = Convert.ToBoolean(ViewState["IsView"]);


    }


    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerfomanceInitiation);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }













    private void FillPeformanceInitiation()
    {
        int PageIndex = peformanceDetailsPager.CurrentPage + 1;
        int PageSize = peformanceDetailsPager.PageSize;
        DataTable dt = clsPerformanceInitiation.GetPerformanceInitiationViewDetails(PageIndex, PageSize);

        if (dt.Rows.Count > 0)
        {

            peformanceDetailsPager.Total = clsPerformanceInitiation.GetTotalRecordCount();
            peformanceDetailsPager.Visible = true;
            dlviewPeformance.DataSource = dt;
            dlviewPeformance.DataBind();
            lblNoperformance.Visible = false;
        }
        else
        {
            dlviewPeformance.DataSource = null;
            dlviewPeformance.DataBind();
            peformanceDetailsPager.Visible = false;
            lblNoperformance.Visible = true;
           
        }
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/PerformanceInitiation.aspx");

    }
    protected void lnkViewInitiation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/ViewPerformanceInitiation.aspx");
    }
    protected void dlviewPeformance_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_ViewDetails":
                Session["ResultList"] = clsPerformanceInitiation.GetPerformanceInitationDetails(Convert.ToInt32(e.CommandArgument));
                    StringBuilder EvaluatorName;
                    DataTable dtEmployeeList = new DataTable();
                    DataRow drEmployeeList = null;
                    int RowIndex = 0;
                    dtEmployeeList.Columns.Add("SLNo");
                    dtEmployeeList.Columns.Add("TemplateName");
                    dtEmployeeList.Columns.Add("EmployeeID");
                    dtEmployeeList.Columns.Add("EmployeeFullName");
                    dtEmployeeList.Columns.Add("EvaluatorFullName");
                    EmployeeInitiation c = ((EmployeeInitiation)Session["ResultList"]);
                    HtmlGenericControl divviewdetails = (HtmlGenericControl)e.Item.FindControl("divviewdetails");
                    Label lblNoRecord = (Label)e.Item.FindControl("lblNoRecord");
                    DataList dlEmployeeList = (DataList)e.Item.FindControl("dlEmployeeList");
                    foreach (InitiationEmployees employee in c.InitiationResult)
                    {
                        RowIndex = RowIndex + 1;

                        drEmployeeList = dtEmployeeList.NewRow();
                        drEmployeeList["SLNo"] = RowIndex.ToString();
                        drEmployeeList["TemplateName"] = employee.TemplateName;
                        drEmployeeList["EmployeeID"] = employee.EmployeeID;
                        drEmployeeList["EmployeeFullName"] = employee.EmployeeName;

                        EvaluatorName = new StringBuilder();
                        foreach (Evaluators evaluator in employee.InitiationEvaluators)
                        {
                            EvaluatorName.Append(evaluator.EvaluatorName);
                            EvaluatorName.Append(",");
                        }
                        drEmployeeList["EvaluatorFullName"] = EvaluatorName.Remove(EvaluatorName.Length - 1, 1);
                        dtEmployeeList.Rows.Add(drEmployeeList);
                    }
                    dlEmployeeList.DataSource = dtEmployeeList.Rows.Count == 0 ? null : dtEmployeeList;
                    dlEmployeeList.DataBind();
                    if (dtEmployeeList.Rows.Count > 0)
                        lblNoRecord.Visible = false;
                    else
                        lblNoRecord.Visible = true;

                string sTimeDelay = "var t = setTimeout(\"SlideUpSlideDown('" + divviewdetails.ClientID + "', 500)\",50)";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "toggle", sTimeDelay, true);
                break;
            case "_EditPerformance":
                Response.Redirect("~/public/PerformanceInitiation.aspx?PerformanceInitiationID="+Convert.ToInt32(e.CommandArgument));
                break;

        }
    }
    protected void dlviewPeformance_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if ((e.Item.ItemType == ListItemType.Item)||(e.Item.ItemType == ListItemType.AlternatingItem))
        {
            LinkButton lnkviewdetails = (LinkButton)e.Item.FindControl("lnkviewdetails");
            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");


            HtmlGenericControl divviewdetails = (HtmlGenericControl)e.Item.FindControl("divviewdetails");
            lnkviewdetails.OnClientClick = "return toggleTabDiv('" + divviewdetails.ClientID + "')";

            HiddenField hfIsEvaluationStarted = (HiddenField)e.Item.FindControl("hfIsEvaluationStarted");

            if (hfIsEvaluationStarted != null && imgEdit != null && hfIsEvaluationStarted.Value.ToInt32() == 1)
            {
                imgEdit.ImageUrl = "~/images/edit_disable.png";
                imgEdit.Enabled = false;
            }
            else
            {
                imgEdit.ImageUrl = "~/images/edit.png";
                imgEdit.Enabled = Convert.ToBoolean(ViewState["IsUpdate"]);

            }

           


        }
    }
    protected void lnkviewdetails_Click(object sender, EventArgs e)
    {

    }
}