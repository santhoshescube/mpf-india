﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="TravelRequest.aspx.cs" Inherits="Public_TravelRequest" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
  
    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=divSingleView.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Travel Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>
  <style type="text/css">
  label
  {
  	font-weight: normal;
  }
  #essnewforms
  {
  	min-height:1100px;
  }
  </style> 

    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            Width="22px" meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false"
            CausesValidation="false" />
    </div>
    <asp:UpdatePanel ID="upAddRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddRequest" runat="server">
                             
                <asp:HiddenField ID="hfRequestID" runat="server" />
                <div class="trLeft" style="width: 25%; padding-top: 15px; height: 30px; float: left;">
                    <asp:Literal ID="litDate" runat="server" meta:resourcekey="TravelDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; padding-top: 15px; height: 30px; float: left;">
                    <asp:TextBox ID="txtTravelDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                        Style="margin-right: 5px"></asp:TextBox>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CssClass="imagebutton" CausesValidation="false" style=" padding-top:8px" />
                    <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtTravelDate"
                        Display="Dynamic" ClientValidationFunction="ValidateDate" ValidationGroup="Submit"
                        CssClass="error"></asp:CustomValidator>
                    <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTravelDate"
                        PopupButtonID="ImageButton1" Format="dd/MM/yyyy">
                    </AjaxControlToolkit:CalendarExtender>
                </div>

                <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Placeoftravel" ></asp:Literal>
                </div>
                  <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtPlace" runat="server" CssClass="textbox" Width="150px" MaxLength="50"
                        Style="margin-left:6px;margin-bottom:15px;margin-right:12px;margin-top:12px" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPlace"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="NoofDays"></asp:Literal>
                </div>

                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtNoofDays" runat="server" CssClass="textbox" MaxLength="3" Width="25px"
                        Style="margin-right: 10px;margin-top: 8px;"></asp:TextBox>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                        FilterType="Numbers" TargetControlID="txtNoofDays">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoofDays"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="litRemarks" runat="server" meta:resourcekey="Purpose">Purpose</asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 100px; float: left;">
                
                    <asp:TextBox ID="txtPurpose" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="400px"
                        TextMode="MultiLine" Height="100px"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="TravelMode"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:DropDownList ID="ddlTravelMode" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="TravelArrangements"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:RadioButtonList ID="rdblArrangements" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" style="Margin-right:25px">
 
                        <asp:ListItem Value="1" meta:resourcekey="Made" Style="Margin-Right :10px;Margin-left:-4px"></asp:ListItem>
                        
                        <asp:ListItem Value="0" meta:resourcekey="Tobemade"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Route"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtRoute" runat="server" CssClass="textbox" Width="150px" Style="margin-right: 10px"
                        MaxLength="50"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 100px; float: left;display: none;">
                    <asp:CheckBox ID="chkVehicleRequired" runat="server" AutoPostBack="true" meta:resourcekey="RequireVehicle"
                        OnCheckedChanged="chkVehicleRequired_CheckedChanged" />
                </div>
                <div class="trRight" style="width: 70%; height: 100px; float: left;display: none;">
                    <asp:TextBox ID="txtvehcleReqRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="400px"
                        TextMode="MultiLine" Height="100px" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvVehRemarks" runat="server" ControlToValidate="txtvehcleReqRemarks"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ErrorMessage="*"></asp:RequiredFieldValidator>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;" >
                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Passport"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <div style="width: 30%; height: 30px; float: left;">
                        <asp:RadioButtonList ID="rblPassport" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                            <asp:ListItem Value="1" meta:resourcekey="Yes" Style="Margin-Right :10px;Margin-left:-4px"></asp:ListItem>
                            <asp:ListItem Value="0" meta:resourcekey="No"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div style="width: 50%; height: 30px; float: left;">
                        <asp:CheckBox ID="chkWithEmployee" runat="server" meta:resourcekey="WithEmployee" />
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;display: none;">
                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Advance"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;display: none;" >
                    <asp:RadioButtonList ID="rblAdvance" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                        AutoPostBack="true" OnSelectedIndexChanged="rblAdvance_SelectedIndexChanged">
                        <asp:ListItem Value="1" meta:resourcekey="Yes" Style="Margin-Right :10px;Margin-left:-4px"></asp:ListItem>
                        <asp:ListItem Value="0" meta:resourcekey="No"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;display: none;">
                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="AdvanceAmount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;display: none;" >
                    <asp:TextBox ID="txtAdvanceAmount" runat="server" CssClass="textbox" Width="150px"
                        Style="margin-right: 10px" Enabled="false" MaxLength="15"></asp:TextBox>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteAmount" runat="server" FilterType="Numbers,Custom"
                        ValidChars="." TargetControlID="txtAdvanceAmount">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="rfvAdvAmount" runat="server" ControlToValidate="txtAdvanceAmount"
                        Display="Dynamic" SetFocusOnError="True" CssClass="error" ErrorMessage="*"></asp:RequiredFieldValidator>
                </div>
                <div style="width: 100%; height: 30px; float: left; text-align: right;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                        Width="75px" ValidationGroup="Submit" OnClick="btnSubmit_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                        Width="75px" Style="margin-left: 10px; margin-right: 35%;" OnClick="btnCancel_Click"
                        CausesValidation="false" /><%--Text="Cancel"--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upSingleView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleView" runat="server">
                <div class="trLeft" style="width: 25%; padding-top: 7px; height: 20px; float: left;">
                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; padding-top: 7px; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblRequestedBy" runat="server"></asp:Label>
                </div>
                <%--<div class="trRight" style="width: 10%; padding-top: 15px; height: 20px; float: left;">
                    <asp:ImageButton ID="imgBack2" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" CausesValidation="false" />
                </div>--%>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="TravelDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblTravelDate" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="NoofDays"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblTravelDays" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Placeoftravel"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblPlace" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Route"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblRoute" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="TravelMode"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblTravelMode" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal37" runat="server" meta:resourcekey="TravelArrangements"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewArrangements" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal38" runat="server" meta:resourcekey="RequireVehicle"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewRequireVehicle" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal39" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewRemarks" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal40" runat="server" meta:resourcekey="Passport"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewPassport" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal43" runat="server" meta:resourcekey="WithEmployee"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblWithEmployee" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal41" runat="server" meta:resourcekey="Advance"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewAdvance" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal42" runat="server" meta:resourcekey="AdvanceAmount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblViewAdvAmount" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 100px; float: left;">
                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Purpose"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 100px; float: left;">
                    :
                    <asp:Label ID="lblPurpose" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 100%; height: 30px; float: left;" runat="server">
                    <asp:LinkButton ID="lbSingleReason" meta:resourcekey="ClicktoViewApprovalDetails"
                        runat="server" OnClick="lbSingleReason_OnClick" CausesValidation="false"></asp:LinkButton>
                    <asp:HiddenField ID="hfSingleLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 75%; float: left; display: none;" id="trSingleReason"
                    runat="server">
                    <asp:GridView ID="gvSingleReason" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 26%; height: 30px; float: left;margin-bottom:0px !important;">
                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="Div1" class="trRight" style="width: 50%; height: auto; float: left; overflow-y: scroll; margin-bottom:0px !important;
                    overflow-x: hidden; margin-bottom" runat="server">
                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td>
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upViewRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewRequest" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlRequest" runat="server" Width="100%" DataKeyField="requestId"
                    CssClass="labeltext" OnItemCommand="dlRequest_ItemCommand" OnItemDataBound="dlRequest_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkRequest')" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%-- Select All--%>
                                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkRequest" runat="server" /></div>
                            <div style="width: 30%; height: 30px; float: left; margin-right: 50%">
                                <asp:LinkButton ID="lnkRequest" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                    ToolTip="View" CommandName="_View" CausesValidation="False"><%# Eval("Date")%></asp:LinkButton>
                            </div>
                            <div style="width: 4%; height: 20px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    meta:resourcekey="Edit"></asp:ImageButton><%--ToolTip="Edit"--%>
                            </div>
                            <div style="width: 4%; height: 20px; float: left;">
                                <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Cancel" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/cancel.png"
                                    meta:resourcekey="RequestForCancel"></asp:ImageButton><%--ToolTip="Request For Cancel"--%>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left; margin-left: 10%">
                            <%--Type--%>
                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="TravelDate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("TravelDate")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left; margin-left: 10%">
                            <%--Type--%>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="TravelMode"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("TravelMode")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left;margin-top:-4px; margin-left: 10%">
                            <%--Date--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Placeoftravel"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("Place")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left; margin-left: 10%">
                            <%--Requested To--%>
                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="NoOfDays"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("TravelDays")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left; margin-left: 10%">
                            <%--Requested To--%>
                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("Requested")%>
                            <asp:Label ID="lbRequested" runat="server" Text=' <%# Eval("RequestedTo") %>' Visible="false"> </asp:Label>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 20px; float: left; margin-left: 10%">
                            <%--Status--%>
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 20px; float: left;">
                            :
                            <%# Eval("Status")%>
                            <asp:HiddenField ID="hfForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                            <asp:HiddenField ID="hfStatusID" runat="server" Value='<%# Eval("StatusID") %>' />
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="RequestPager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upApproveRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divApproveRequest" runat="server" style="width: 100%; height: 300px; float: left;
                padding-top: 20px;">
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%;  height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppRequestedBy" runat="server"></asp:Label>
                </div>
                <%-- <div class="trRight" style="width: 10%; padding-top: 15px; height: 20px; float: left;">
                    <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" CausesValidation="false" />
                </div>--%>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal29" runat="server" meta:resourcekey="RequestedDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppRequestedDate" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="TravelDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppTravelDate" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="NoofDays"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppNoofDays" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Placeoftravel"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppPlaceofTravel" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal26" runat="server" meta:resourcekey="Route"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppRoute" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="TravelMode"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppTravelMode" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal36" runat="server" meta:resourcekey="TravelArrangements"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppArrangements" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal34" runat="server" meta:resourcekey="RequireVehicle"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppRequireVehicle" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal35" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppRemarks" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal31" runat="server" meta:resourcekey="Passport"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppPassport" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal44" runat="server" meta:resourcekey="WithEmployee"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppWithEmployee" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal32" runat="server" meta:resourcekey="Advance"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lbAppAdvance" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal33" runat="server" meta:resourcekey="AdvanceAmount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppAmount" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 20px; float: left;">
                    <asp:Literal ID="Literal30" runat="server" meta:resourcekey="Purpose"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 20px; float: left;">
                    :
                    <asp:Label ID="lblAppPurpose" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Status--%>
                    <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblCurrentStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--New Status--%>
                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="NewStatus"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left; padding-left: 5px">
                    <asp:DropDownList ID="ddlApproveStatus" runat="server" Width="202px" DataTextField="Description"
                        DataValueField="StatusId">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvddlApproveStatus" runat="server" CssClass="error"
                        ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlApproveStatus" ValidationGroup="SubmitApprove"
                        InitialValue="-1"></asp:RequiredFieldValidator><%--ErrorMessage="Please select a Status"--%>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Remarks--%>
                    <asp:Literal ID="Literal25" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 55px; float: left; padding-left: 5px">
                    <asp:TextBox ID="txtApproveRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtApproveRemarks" runat="server" meta:resourcekey="Pleaseenterremarks"
                        ControlToValidate="txtApproveRemarks" Display="Dynamic" ValidationGroup="SubmitApprove"
                        CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter remarks."--%>
                </div>
                <div class="trLeft" style="width: 100%; height: 30px; float: left; display: none;"
                    id="trlink" runat="server">
                    <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                        CausesValidation="false" OnClick="lbReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 75%; float: left; display: none;" id="trReasons"
                    runat="server">
                    <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="100%">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 26%; height: 30px; float: left;">
                    <asp:Label ID="lbl2" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="div2" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl2" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td style="width:30%;">
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
                <div id="divApproveTheRequest" runat="server" style="display: none; width: 100%;
                    height: 30px; float: left; text-align: right;">
                    <asp:Button ID="btnApproveRequest" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                        Width="75px" ValidationGroup="SubmitApprove" OnClick="btnApproveRequest_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnApproveCancel" runat="server" CausesValidation="false" CssClass="btnsubmit"
                        Text='<%$Resources:ControlsCommon,Cancel%>' Width="75px" Style="margin-left: 10px;
                        margin-right: 35%;" OnClick="btnApproveCancel_Click" /><%--Text="Cancel"--%>
                </div>
                <div id="divCancelTheRequest" runat="server" style="display: none">
                    <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                        <asp:Button ID="btnCancelSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                            Width="75px" OnClick="btnCancelSubmit_Click" CausesValidation="false" /><%--Text="Submit"--%>
                        <asp:Button ID="btnCancelCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                            Width="75px" Style="margin-left: 10px; margin-right: 35%;" OnClick="btnCancelCancel_Click"
                            CausesValidation="false" />
                        <%--Text="Cancel"--%>
                    </div>
                </div>
            </div>
              
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" meta:resourcekey="Submit" /><%--Text="submit"--%>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgAdd" ImageUrl="~/images/Add travel request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" meta:resourcekey="AddRequest" OnClick="lnkAdd_Click"
                                CausesValidation="false">
                                <%--Request Expense--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgView" ImageUrl="~/images/View travel request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" meta:resourcekey="ViewRequest" OnClick="lnkView_Click"
                                CausesValidation="false">
                                <%--View Expense--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" meta:resourcekey="DeleteRequest" OnClick="lnkDelete_Click"
                                CausesValidation="false">
                                <%--Delete--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
    
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
