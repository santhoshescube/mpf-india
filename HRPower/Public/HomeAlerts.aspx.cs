﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Web.UI.DataVisualization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic;
using System.Threading;

public partial class Public_HomeAlerts : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillDocumentType();
            if (Request.QueryString["DocumentTypeID"] != null)
            {
                ddlDocType.SelectedValue = Convert.ToString(Request.QueryString["DocumentTypeID"]); 
            }
            if (Request.QueryString["IsExpiry"] != null)
            {
                hfdIsExpiry.Value = Convert.ToString(Request.QueryString["IsExpiry"]);
            }

            if (hfdIsExpiry.Value == "1")
            {
                rdbExpiryAlert.Checked = true;
                rdbReturnAlert.Checked = false;
                FillExpiryAlerts();
            }
            else
            {
                rdbExpiryAlert.Checked = false ;
                rdbReturnAlert.Checked = true ;
                FillAlerts();
            }
        }

     
    }
     

    private void FillDocumentType()
    {
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
        int iEmployeeId = objUserMaster.GetEmployeeId();

        ddlDocType.DataTextField = "Description";
        ddlDocType.DataValueField = "DocumentTypeId";
        ddlDocType.DataSource = objHomePage.FillDocumentType(objUserMaster.GetUserId());
        ddlDocType.DataBind();
      

    }
    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    private void FillAlerts()
    {
        
        objUserMaster = new clsUserMaster();
        objHomePage = new clsHomePage();
        dlAlerts.DataSource = null;
        DataTable datAlerts = null;

        DateTime? dtFromDate = null;
        DateTime? dtToDate = null;

        hfdIsExpiry.Value = "0";
        try
        {
            if (txtfrom.Text != "") dtFromDate = clsCommon.Convert2DateTime(txtfrom.Text);
            if (txtTo.Text != "") dtToDate = clsCommon.Convert2DateTime(txtTo.Text);


            datAlerts = clsAlerts.GetAlerts(dtFromDate, dtToDate, objUserMaster.GetUserId(),ddlDocType.SelectedValue.ToInt32() == 15? 1:0,ddlDocType.Items.Count > 0? ddlDocType.SelectedItem.Text : "", 100, 0);

            if (datAlerts == null || datAlerts.Rows.Count == 0)
            {
                dlAlerts.DataSource = null;
                dlAlerts.DataBind();
            }
            else
            {
                dlAlerts.DataSource = datAlerts;
                dlAlerts.DataBind();
            }
            upnlPopup.Update();
        }
        catch (Exception)
        {
        }

    }
    private void FillExpiryAlerts()
    {
        objUserMaster = new clsUserMaster();
        objHomePage = new clsHomePage();

        dlAlerts.DataSource = null;
        DataTable datAlerts = null;

        DateTime? dtFromDate = null;
        DateTime? dtToDate = null;

        hfdIsExpiry.Value = "1";

        if (txtfrom.Text != "") dtFromDate = clsCommon.Convert2DateTime(txtfrom.Text);
        if (txtTo.Text != "") dtToDate = clsCommon.Convert2DateTime(txtTo.Text);


        datAlerts = clsAlerts.GetAlerts(dtFromDate, dtToDate, objUserMaster.GetUserId(), 1, ddlDocType.Items.Count>0 ? ddlDocType.SelectedItem.Text : "", 100, 0);

        if (datAlerts == null || datAlerts.Rows.Count == 0)
        {
            dlAlerts.DataSource = null;
            dlAlerts.DataBind();
        }
        else
        {
            dlAlerts.DataSource = datAlerts;
            dlAlerts.DataBind();
        }
        upnlPopup.Update();

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManagerHomeNew.aspx");
    }

    protected void rdbExpiryAlert_CheckedChanged(object sender, EventArgs e)
    {
         FillExpiryAlerts(); 
       
    }

    protected void rdbReturnAlert_CheckedChanged(object sender, EventArgs e)
    {
        FillAlerts();

    }
    protected void btnAddEmployee_Click(object sender, EventArgs e)
    {
        try
        {
            objHomePage = new clsHomePage();
            List<AlertsDetails> Alerts = new List<AlertsDetails>();

            if (dlAlerts.Items.Count > 0)
            {
                DataRow dr;

                foreach (DataListItem item in dlAlerts.Items)
                {
                    CheckBox chkAlert = (CheckBox)item.FindControl("chkAlert");
                    HiddenField hfdDocTypeID = (HiddenField)item.FindControl("hfdDocTypeID");
                    HiddenField hfdCommonID = (HiddenField)item.FindControl("hfdCommonID");

                    if (chkAlert.Checked)
                    {
                        Alerts.Add(new AlertsDetails()
                        {
                            CommonId = hfdCommonID.Value.ToInt32(),
                            DocumentTypeID = hfdDocTypeID.Value.ToInt32()
                        }
                         );
                    }
                }
                if (new clsAlerts().DeleteAlerts(Alerts))
                {
                    if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();

                }
            }
            ////// Document return alert
            ////DataSet dsReturnAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GAR");
            ////rptAlert.DataSource = dsReturnAlert.Tables[1];
            ////rptAlert.DataBind();
            ////int RetCount = dsReturnAlert.Tables[0].Rows[0]["AlCount"].ToInt32();
            ////lblRetAlert.Text = RetCount > 0 ? "Document Return Alerts" + " ( " + RetCount + " )" : "Document Return Alerts";
            ////updAlert.Update();

            ////// Document expiry alert
            ////DataSet dsExpyAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GA");

            ////rptExpiryAlert.DataSource = dsExpyAlert.Tables[1];
            ////rptExpiryAlert.DataBind();
            ////int Count = dsExpyAlert.Tables[0].Rows[0]["AlCount"].ToInt32();

            ////lblAlert.Text = Count > 0 ? "Document Expiry Alerts" + " ( " + Count + " )" : "Document Expiry Alerts";
            ////updExpiryAlert.Update();
        }
        catch (Exception)
        {
        }
    }
    #region RightMenuEvents
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {

        TableCell cell = (TableCell)e.Cell;
        cell.Width = Unit.Pixel(30);
        cell.Height = Unit.Pixel(30);
        if (e.Day.Date.ToString("dd MMM yyyy") == System.DateTime.Now.Date.ToString("dd MMM yyyy"))
        {
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Red;
            //cell.CssClass = "Currendate";
        }
        //cell.BorderStyle = BorderStyle.Solid;
        //cell.BorderWidth = Unit.Pixel(1);
        //cell.BorderColor = System.Drawing.Color.Black;
        cell.Style["font-size"] = "10px";
        //cell.Style["padding"] = "0";
        cell.Attributes.Add("title", "");
        if (e.Day.IsOtherMonth)
        {
            cell.Text = "";
            cell.CssClass = "tblBackground1";

        }
        else
        {

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();

            DataSet ds = objHomePage.GetCalendarDetails(objUserMaster.GetEmployeeId(), e.Day.Date.ToString("dd MMM yyyy"));
            cell.CssClass = "tblBackground1";
            string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : " Holiday:");
            if (ds.Tables[0].Rows.Count > 0)
            {
                sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                cell.ForeColor = System.Drawing.Color.Red;
            }
            string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : " Events: \n");
            int icount = 1;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                icount++;
            }
            if (sEvent != string.Empty)
                cell.ForeColor = System.Drawing.Color.Green;
            if (sEvent != string.Empty && sHoliday != string.Empty)
                cell.ForeColor = System.Drawing.Color.FromArgb(244, 166, 29);
            //cell.ForeColor = System.Drawing.Color.Magenta;
            string sTitle = (sHoliday == string.Empty ? string.Empty : sHoliday) + (sEvent == string.Empty ? string.Empty : sEvent);
            if (sTitle != string.Empty)
                cell.Attributes.Add("title", sTitle);

        }
    }
    #endregion RightMenuEvents
    protected void dlAlerts_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (ddlDocType.SelectedItem.Value == "15")
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ImageButton imgConfirm = (ImageButton)e.Item.FindControl("imgConfirm");
                HiddenField hfdCommonID = (HiddenField)e.Item.FindControl("hfdCommonID");
                if (imgConfirm != null)
                {
                    imgConfirm.Visible = true;
                    imgConfirm.PostBackUrl = "CreateEmployee.aspx?EmpId=" + hfdCommonID.Value + "&Action=Confirm";
                }
            }
        }
    }

    
}
