﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="Expense.aspx.cs" Inherits="Public_Expense" Title="Expense" %>

<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <%--<script src="../js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>--%>
<style type="text/css">
.trRight, .firstTrLeft, .firstTrRight
{
	margin-bottom:0px;
}
	
</style>
    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=divSingleViewExpense.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Expense Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>

    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            Width="22px" meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <asp:UpdatePanel ID="upAddExpense" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddExpense" runat="server" style="width: 100%; height: 200px; float: left;
                padding-top: 20px;">
                <asp:HiddenField ID="hfExpenseRequestID" runat="server" />
                <div style="width: 100%; float: left;">
                    <div class="trLeft" style="width: 25%; float: left;">
                        <%--Type--%>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Type"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; float: left;">
                        <asp:DropDownList ID="ddlType" runat="server" Width="200px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvddlType" runat="server" CssClass="error" meta:resourcekey="Pleaseselectatype"
                            Display="Dynamic" ControlToValidate="ddlType" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; float: left;">
                    <%--From date--%>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Fromdate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%;  float: left;">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                        Style="margin-right: 10px"></asp:TextBox>
                    <asp:ImageButton ID="ibtnFromdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CausesValidation="false" CssClass="imagebutton" style="margin-top: 9px" />
                    <AjaxControlToolkit:CalendarExtender ID="extenderExpenseFromDate" runat="server"
                        TargetControlID="txtFromDate" PopupButtonID="ibtnFromdate" Format="dd/MM/yyyy"
                        Enabled="true">
                    </AjaxControlToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rfvtxtFromDate" runat="server" meta:resourcekey="PleaseenterExpensefromdate"
                        ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Submit" CssClass="error"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvStartDate" runat="server" ClientValidationFunction="CheckFuturedate"
                        CssClass="error" ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                </div>
                <div class="trLeft" style="width: 25%;  float: left;">
                    <%--To date--%>
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Todate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%;  float: left;">
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                        Style="margin-right: 10px"></asp:TextBox>
                    <asp:ImageButton ID="ibtnTodate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CssClass="imagebutton" CausesValidation="false" style="margin-top: 9px;" />
                    <AjaxControlToolkit:CalendarExtender ID="extenderExpenseToDate" runat="server" TargetControlID="txtToDate"
                        PopupButtonID="ibtnTodate" Format="dd/MM/yyyy">
                    </AjaxControlToolkit:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rfvtxtToDate" runat="server" meta:resourcekey="PleaseenterExpensetodate"
                        ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Submit" CssClass="error"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="cvEndDate1" runat="server" ClientValidationFunction="CheckFutureAndFromToDate"
                        CssClass="error" ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                    <%--<asp:CustomValidator ID="cvEndDate2" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                        Display="Dynamic" ClientValidationFunction="checkFromToDate" ValidationGroup="Submit"
                        ErrorMessage="From date is greater than to date." CssClass="error"></asp:CustomValidator>--%>
                </div>
                <%--        <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                Expense date
            </div>
            <div class="trRight" style="width: 70%; height: 30px; float: left;">
                <asp:TextBox ID="txtExpenseDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                    Style="margin-right: 10px"></asp:TextBox>
                <asp:ImageButton ID="ibtnExpenseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                    CausesValidation="false" CssClass="imagebutton" />
                <AjaxControlToolkit:CalendarExtender ID="extenderExpenseDate" runat="server" TargetControlID="txtExpenseDate"
                    Format="dd/MM/yyyy" PopupButtonID="ibtnExpenseDate">
                </AjaxControlToolkit:CalendarExtender>
                <asp:CustomValidator ID="cvtxtExpenseDate" runat="server" ClientValidationFunction="validateLastDate"
                    CssClass="error" ControlToValidate="txtExpenseDate" Display="Dynamic" ValidationGroup="submit"></asp:CustomValidator>
                <asp:RequiredFieldValidator ID="rfvtxtExpenseDatepnlTab1" runat="server" ErrorMessage="Please enter Expense date"
                    ControlToValidate="txtExpenseDate" Display="Dynamic" ValidationGroup="submit"
                    CssClass="error"></asp:RequiredFieldValidator>
            </div>--%>
                <div class="trLeft" style="width: 25%;  float: left;">
                    <%-- Amount--%>
                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Amount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%;  float: left;">
                    <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox_mandatory" MaxLength="14"
                        Width="195px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtAmount" runat="server" meta:resourcekey="Pleaseenteramount"
                        ControlToValidate="txtAmount" Display="Dynamic" ValidationGroup="Submit" CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter amount"--%>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmountExtender" runat="server"
                        FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                </div>
                <div class="trLeft" style="width: 25%;  float: left;">
                    <%--Remarks--%>
                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%;  float: left;">
                    <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" meta:resourcekey="Pleaseenterremarks"
                        ControlToValidate="txtRemarks" Display="Dynamic" ValidationGroup="Submit" CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter remarks."--%>
                </div>
                <div class="trLeft" style="width: 25%;  float: left;">
                    <asp:Literal ID="Literal26" runat="server" meta:resourcekey="AttachDocuments"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%;  float: left;">
                    <asp:UpdatePanel ID="upExpDocument" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <div style="float: left; width: 17%">
                                <asp:Label ID="lblDocumentCaption" runat="server" meta:resourcekey="DocumentName"></asp:Label>
                            </div>
                            <div style="float: left; width: 70%">
                                <asp:TextBox ID="txtExpDocumentName" runat="server" CssClass="textbox" MaxLength="30"
                                    Width="100px" Style="margin-right: 20px;"></asp:TextBox>
                                <AjaxControlToolkit:AsyncFileUpload ID="fuExpense" runat="server" ErrorBackColor="White"
                                    PersistFile="True" CompleteBackColor="Green" Visible="true" Width="250px" OnUploadedComplete="fuExpense_UploadedComplete" />
                                <asp:LinkButton ID="lnkAttch" runat="server" Enabled="true" ValidationGroup="AttachedDocument"
                                    meta:resourcekey="Addtolist" ForeColor="#33a3d5" OnClick="lnkAttch_Click">
                                </asp:LinkButton>
                            </div>
                            <div style="float: left; width: 70%">
                                <asp:RequiredFieldValidator ID="rfvDocumentName" Text="*" ValidationGroup="AttachedDocument"
                                    runat="server" ControlToValidate="txtExpDocumentName">
                                        
                                </asp:RequiredFieldValidator>
                            </div>
                            <div style="clear: both">
                            </div>
                            <div style="width: 80%; height: 150px; overflow: auto" id="dvAttachedDocuments" runat="server">
                                <asp:DataList ID="dlExpDocument" runat="server" GridLines="Horizontal" Width="98%"
                                    BorderColor="#33a3d5" OnItemDataBound="dlExpDocument_ItemDataBound">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <HeaderTemplate>
                                        <div style="float: left; width: 100%;">
                                            <div style="float: left; width: 20%;" class="trLeft">
                                                <asp:Literal ID="Literal75" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 40%;" class="trLeft">
                                                <asp:Literal ID="Literal76" runat="server" meta:resourcekey="FileName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 18%;" class="trRight">
                                                <asp:Literal ID="Literal77" runat="server" meta:resourcekey="Image"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 02%;" class="trRight">
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="float: left; width: 100%;">
                                            <div style="float: left; width: 20%; word-break: break-all" class="trLeft">
                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                    Width="120px" Style="word-break: break-all"></asp:Label>
                                            </div>
                                            <div style="float: left; width: 40%;" class="trLeft">
                                                <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="200px"></asp:Label>
                                            </div>
                                            <div style="float: LEFT; width: 18%;" class="trRight">
                                                <asp:ImageButton ID="img" Width="30px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                                    CommandArgument='<%# Eval("Location") %>' OnClick="img_Click" CausesValidation="false"
                                                    CommandName="imgView" />
                                            </div>
                                            <div style="float: left; width: 2%;" class="trRight">
                                                <asp:ImageButton ID="imgDelete" ImageUrl="~/images/Delete18x18.png" runat="server"
                                                    CommandArgument='<%# Eval("FileName") %>' OnClick="imgDelete_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                        Width="75px" ValidationGroup="Submit" OnClick="btnSubmit_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                        Width="75px" Style="margin-left: 10px; margin-right: 10%;" OnClick="btnCancel_Click" /><%--Text="Cancel"--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upViewExpense" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewExpense" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlExpenseRequest" runat="server" Width="100%" DataKeyField="requestId"
                    CssClass="labeltext" OnItemCommand="dlExpenseRequest_ItemCommand" OnItemDataBound="dlExpenseRequest_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkExpense')" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%-- Select All--%>
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5% !important; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkExpense" runat="server" /></div>
                            <div style="width: 30%; height: 30px; float: left; margin-right: 50%">
                                <asp:LinkButton ID="lnkLeave" runat="server"  CssClass="listHeader bold" CommandArgument='<%# Eval("RequestId") %>'
                                    ToolTip="View" CommandName="_View" CausesValidation="False"><%# Eval("RequestedDate").ToDateTime().ToString("dd MMM yyyy")%></asp:LinkButton>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    meta:resourcekey="Edit"></asp:ImageButton><%--ToolTip="Edit"--%>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Cancel" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/cancel.png"
                                    meta:resourcekey="RequestForCancel"></asp:ImageButton><%--ToolTip="Request For Cancel"--%>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 10%">
                            <%--Type--%>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Type"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("ExpenseType")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Date--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Date"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Date")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Amount--%>
                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Amount"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Amount")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Requested To--%>
                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("RequestedToNames")%>
                        </div>
                        <div class="trLeft" style="width: 25% !important; height: 30px; float: left; margin-left: 10%">
                            <%--Status--%>
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Status")%>
                            <asp:HiddenField ID="hfForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                            <asp:HiddenField ID="hfStatusID" runat="server" Value='<%# Eval("StatusID") %>' />
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="ExpensePager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upSingleViewExpense" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleViewExpense" runat="server" style="width: 100%; height: 300px;
                float: left; padding-top: 20px;">
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Requested By--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblRequestedBy" runat="server"></asp:Label>
                </div>
                <%--<div class="trRight" style="padding-top: 15px; height: 20px; float: left;">
                    <asp:ImageButton ID="imgBack2" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" CausesValidation="false" />
                </div>--%>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Type--%>
                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Type"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblExpenseType" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Date--%>
                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Date"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Amount--%>
                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Amount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Requested To--%>
                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblRequestedToNames" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Status--%>
                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25% !important;; height: 30px; float: left; margin-left: 5%">
                    <%--Remarks--%>
                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: auto; float: left; word-break: break-all;
                    overflow-y: scroll; overflow-x: hidden;">
                    :
                    <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                </div>
                <%--<div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                   
                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>--%>
                <div class="trRight" style="width: 60%; height: auto; float: left; word-break: break-all;
                    overflow-y: scroll; overflow-x: hidden; margin-left: 292px;">
                    <%-- <asp:Label ID="lblRemarks" runat="server"></asp:Label>--%>
                    <asp:TextBox ID="txtEditRemarks" runat="server" Visible="false" Width="200px" Height="50px"
                        TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLength(this, 500);"
                        MaxLength="500"></asp:TextBox>
                    <%--Text='<%# Eval("Reason") %>'--%>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <%--Status--%>
                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;">
                    <asp:DataList ID="dlDocumentsApproval" runat="server" GridLines="Horizontal" Width="98%"
                        BorderColor="#33a3d5" OnItemDataBound="dlDocumentsApproval_ItemDataBound">
                        <HeaderStyle CssClass="datalistheader" />
                        <HeaderTemplate>
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 20%;" class="trLeft">
                                    <asp:Literal ID="Literal75" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 40%;" class="trLeft">
                                    <asp:Literal ID="Literal76" runat="server" meta:resourcekey="FileName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 18%;" class="trRight">
                                    <asp:Literal ID="Literal77" runat="server" meta:resourcekey="Image"></asp:Literal>
                                </div>
                                <div style="float: left; width: 02%;" class="trRight">
                                </div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 20%; word-break: break-all" class="trLeft">
                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                        Width="120px" Style="word-break: break-all"></asp:Label>
                                </div>
                                <div style="float: left; width: 40%;" class="trLeft">
                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="200px"></asp:Label>
                                </div>
                                <div style="float: LEFT; width: 18%;" class="trRight">
                                    <asp:ImageButton ID="img" Width="30px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                        CommandArgument='<%# Eval("Location") %>' OnClick="img_Click" CausesValidation="false"
                                        CommandName="imgView" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div class="trLeft" style="width: 75%; float: left; margin-left: 5%">
                    <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                        OnClick="lbReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 90%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden; word-break: break-all; margin-left: 5%;"
                    id="trSingleReason" runat="server">
                    <asp:GridView ID="gvSingleReason" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ControlStyle-Width="200px" ItemStyle-Width="250px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" ItemStyle-Width="100px" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date1" ItemStyle-Width="120px"/>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 23%; height: 30px; float: left; margin-left: 5%;">
                    <asp:Label ID="Label1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="divAuthority2" class="trRight" style="width: 58%; height: auto; float: left;
                    overflow-y: scroll; margin-left: 1%; overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl2" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td>
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
                <div class="trLeft" style="width: 66%; float: left; margin-left: 5%">
                    <div id="divCancelTheRequest" runat="server" style="display: none">
                        <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                            <asp:Button ID="btnCancelSubmit" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                                Width="75px" OnClick="btnCancelExpense_Click" /><%--Text="Submit"--%>
                            <asp:Button ID="btnCancelCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                                Width="75px" Style="margin-left: 10px; margin-right: 35%;" OnClick="btnCancelCancel_Click" />
                            <%--Text="Cancel"--%>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upApproveExpense" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divApproveExpense" runat="server" style="width: 100%; height: 300px; float: left;
                padding-top: 20px;">
                <%--                <asp:HiddenField ID="hfApproveExpenseRequestID" runat="server" />
--%>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Requested By--%>
                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveRequestedBy" runat="server"></asp:Label>
                </div>
                <%--<div class="trRight" style="width: 10%; padding-top: 15px; height: 20px; float: left;">
                    <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                        ToolTip="Back to previous page" OnClick="imgback_Click" CausesValidation="false" />
                </div>--%>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Type--%>
                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Type"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveType" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Date--%>
                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Date"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveDate" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%-- Expense date--%>
                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Expensedate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtExpenseDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                        Style="margin-right: 10px"></asp:TextBox>
                    <asp:ImageButton ID="ibtnExpenseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CausesValidation="false" CssClass="imagebutton" />
                    <AjaxControlToolkit:CalendarExtender ID="extenderExpenseDate" runat="server" TargetControlID="txtExpenseDate"
                        Format="dd/MM/yyyy" PopupButtonID="ibtnExpenseDate">
                    </AjaxControlToolkit:CalendarExtender>
                    <asp:CustomValidator ID="cvtxtExpenseDate" runat="server" ClientValidationFunction="CheckFuturedate"
                        CssClass="error" ControlToValidate="txtExpenseDate" Display="Dynamic" ValidationGroup="SubmitApprove"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtExpenseDate" runat="server" meta:resourcekey="PleaseenterExpensedate"
                        ControlToValidate="txtExpenseDate" Display="Dynamic" ValidationGroup="SubmitApprove"
                        CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter Expense date"--%>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Amount--%>
                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Amount"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtApproveAmount" runat="server" CssClass="textbox_mandatory" MaxLength="14"
                        Width="195px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtApproveAmount" runat="server" meta:resourcekey="Pleaseenteramount"
                        ControlToValidate="txtApproveAmount" Display="Dynamic" ValidationGroup="SubmitApprove"
                        CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter amount"--%>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                        FilterType="Numbers,Custom" TargetControlID="txtApproveAmount" ValidChars=".">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                </div>
                <div id="divAppSettings" runat="server">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <%--Accountability--%>
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Accountability"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        <asp:RadioButtonList ID="rblAccountability" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                            OnSelectedIndexChanged="rblAccountability_SelectedIndexChanged">
                            <asp:ListItem meta:resourcekey="None" Value="1"></asp:ListItem>
                            <%--Text="None"--%>
                            <asp:ListItem meta:resourcekey="Reimbursement" Value="2"></asp:ListItem>
                            <%--Text="Reimbursement"--%>
                            <asp:ListItem meta:resourcekey="Deduction" Value="3"></asp:ListItem>
                            <%--Text="Deduction" --%>
                        </asp:RadioButtonList>
                    </div>
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Label ID="lblAccountabilityHeader" meta:resourcekey="ActualAmount" runat="server"></asp:Label><%--Text="Actual Amount"--%>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        <asp:TextBox ID="txtAccountabilityAmount" runat="server" MaxLength="14" Width="195px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtAccountabilityAmount" runat="server" meta:resourcekey="Pleaseenteramount"
                            ControlToValidate="txtAccountabilityAmount" Display="Dynamic" ValidationGroup="SubmitApprove"
                            CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter amount"--%>
                        <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                            FilterType="Numbers,Custom" TargetControlID="txtAccountabilityAmount" ValidChars=".">
                        </AjaxControlToolkit:FilteredTextBoxExtender>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Status--%>
                    <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblCurrentStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--New Status--%>
                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="NewStatus"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:DropDownList ID="ddlApproveStatus" runat="server" Width="202px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvddlApproveStatus" runat="server" CssClass="error"
                        meta:resourcekey="PleaseselectaStatus" Display="Dynamic" ControlToValidate="ddlApproveStatus"
                        ValidationGroup="SubmitApprove" InitialValue="-1"></asp:RequiredFieldValidator><%--ErrorMessage="Please select a Status"--%>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Remarks--%>
                    <asp:Literal ID="Literal25" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: auto; float: left; word-break: break-all;
                    overflow-y: scroll; overflow-x: hidden;">
                    <asp:Label ID="lblApproveRemarks" runat="server"></asp:Label>
                </div>
                <div class="trRight" style="width: 60%; height: auto; float: left; word-break: break-all;
                    margin-left: 245px; overflow-y: scroll; overflow-x: hidden;">
                    <asp:TextBox ID="txtApproveRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtApproveRemarks" runat="server" meta:resourcekey="Pleaseenterremarks"
                        ControlToValidate="txtApproveRemarks" Display="Dynamic" ValidationGroup="SubmitApprove"
                        CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter remarks."--%>
                </div>
                <div class="trLeft" style="width: 75%; height: 30px; float: left;">
                    <asp:LinkButton ID="lbApproveReason" meta:resourcekey="ClicktoViewApprovalDetails"
                        runat="server" CausesValidation="false" OnClick="lbApproveReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfApproveLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 90%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden; word-break: break-all;" id="trReasons" runat="server">
                    <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" RowStyle-Wrap="true"
                        RowStyle-Width="800px" BorderColor="#307296" Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" ItemStyle-Width="150px" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true" ItemStyle-Width="250px"/>
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" ItemStyle-Width="100px"/>
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date1" ItemStyle-Width="120px" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td>
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Status--%>
                    <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Documents"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;">
                    <asp:DataList ID="dlApprovedDocuments" runat="server" GridLines="Horizontal" Width="98%"
                        BorderColor="#33a3d5" OnItemDataBound="dlApprovedDocuments_ItemDataBound">
                        <HeaderStyle CssClass="datalistheader" />
                        <HeaderTemplate>
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 20%;" class="trLeft">
                                    <asp:Literal ID="Literal75" runat="server" meta:resourcekey="DocumentName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 40%;" class="trLeft">
                                    <asp:Literal ID="Literal76" runat="server" meta:resourcekey="FileName"></asp:Literal>
                                </div>
                                <div style="float: left; width: 18%;" class="trRight">
                                    <asp:Literal ID="Literal77" runat="server" meta:resourcekey="Image"></asp:Literal>
                                </div>
                                <div style="float: left; width: 02%;" class="trRight">
                                </div>
                            </div>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 20%; word-break: break-all" class="trLeft">
                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                        Width="120px" Style="word-break: break-all"></asp:Label>
                                </div>
                                <div style="float: left; width: 40%;" class="trLeft">
                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="200px"></asp:Label>
                                </div>
                                <div style="float: LEFT; width: 18%;" class="trRight">
                                    <asp:ImageButton ID="img" Width="30px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>'
                                        CommandArgument='<%# Eval("Location") %>' OnClick="img_Click" CausesValidation="false"
                                        CommandName="imgView" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                    <asp:Button ID="btnApproveExpense" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                        Width="75px" ValidationGroup="SubmitApprove" OnClick="btnApproveExpense_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnApproveCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                        Width="75px" Style="margin-left: 10px; margin-right: 10%;" OnClick="btnApproveCancel_Click" /><%--Text="Cancel"--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" meta:resourcekey="Submit" /><%--Text="submit"--%>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgAdd" ImageUrl="~/images/expense.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="RequestExpense">
                            
                                <%--Request Expense--%>
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgView" ImageUrl="~/images/view_expense.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="ViewExpense">
                           
                                <%--View Expense--%>
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="Delete">
                            
                                <%--Delete--%>
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
