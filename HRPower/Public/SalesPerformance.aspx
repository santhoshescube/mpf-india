﻿<%@ Page Language="C#" MasterPageFile="~/Master/ManagerHomeMasterPage.master" AutoEventWireup="true"
    CodeFile="SalesPerformance.aspx.cs" Inherits="Public_SalesPerformance" Title="Sales Performance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">

    <div id='cssmenu'>
        <%--Employee--%>
        
        
              <ul>
            <li class='selected'><a href="SalesPerformance.aspx">
                <%-- Projection--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%-- Actual--%>
                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li ><a href="PerformanceTemplate.aspx"><span>
                <%-- To--%>
                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function ValidateProjection(e,source)
        {
            var txtProjection = document.getElementById(source.id.replace(/txtActual/,'txtProjection'));
            if(txtProjection.value == null || txtProjection.value == '' || (txtProjection.value != null && parseFloat(txtProjection.value) == 0))
            {
                return false;
            }
            return ValidateDecimal(e,source.value);
        }
        
        function ValidateFilter()
        {
            var ddlDepartment = document.getElementById('<%= ddlDepartment.ClientID %>');
            var txtPeriod = document.getElementById('<%= txtPeriod.ClientID %>');
            var divView = document.getElementById('<%=divView.ClientID%>');
            var hfIsArabic = document.getElementById('<%=hfIsArabic.ClientID%>').value;

            if(divView != null)
              divView.style.display = "none";
            
            
            
            if(ddlDepartment.value == '')
            {
                if(hfIsArabic =='True')
                   alert('الرجاء اختر القسم');
                else
                   alert('Please Select Department');
                return false;
            }
            else if(txtPeriod.value == '')
            {  
                if(hfIsArabic =='True')
                   alert('الرجاء اختر الفترة');
                else
                     alert('Please Select Period');
                return false;
            }
            else 
            {
               return ValidateDate(txtPeriod);
            }
            return true;
        }
        
        function ValidateDate(sender)
        {
            var Months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var Month = sender.value.split('-');
            var hfIsArabic = document.getElementById('<%=hfIsArabic.ClientID%>').value;
            var IsValid = 0;
            for(var i = 0;i<Months.length;i++)
            {
                if(Months[i] == Month[0])
                {
                    IsValid = 1;
                }
            }
            if(IsValid == 1 && Month[1].length < 4)
            {
               IsValid = 0;
            }
            if(IsValid == 0)
            {
              if(hfIsArabic =='True')
                   alert('الرجاء الإختيار تاريخ صالح');
                else
                    alert('Please Select Valid Date');
                return false;
            }
            return true;
        }
    </script>

    <div id="dvHavePermission" runat="server">
        <div id="divMessagePopUp">
            <div style="display: none">
                <asp:Button ID="btnMessage" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="ucMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnMessage"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:HiddenField ID="hfIsArabic" runat="server" />
        </div>
        <div id="divFilter" runat="server" style="margin-top: 25px; width: 100%; height: 40px;
            font-family: Tahoma; font-size: 12px; color: Black;">
            <asp:UpdatePanel ID="upnlFilter" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 28%; float: left;">
                        <div class="trLeft" style="width: 35%; float: left; padding-left: 5px;">
                           <%-- Department--%>
                           <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,Department %>'>
                           </asp:Literal>
                        </div>
                        <div style="width: 55%; float: left;">
                            <asp:UpdatePanel ID="upnlDepartment" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" Width="130px" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" Height="25px" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 21%; float: left; padding: 0px;">
                        <div class="trLeft" style="width: 25%; float: left; padding: 0px; vertical-align: top;">
                           <%-- Month--%>
                           <asp:Literal ID="Literal2" runat ="server" Text ='<%$Resources:ControlsCommon,Month %>'>
                           </asp:Literal>
                        </div>
                        <div style="width: 64%; float: left; padding: 0px;">
                            <asp:TextBox ID="txtPeriod" runat="server" Style="margin-right: 4px; Width:79px ;margin-top: 0px;" />
                            <asp:ImageButton ID="ibtnPeriod" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="false" />
                            <AjaxControlToolkit:CalendarExtender ID="cePeriod" runat="server" Format="MMM-yyyy"
                                TargetControlID="txtPeriod" PopupButtonID="ibtnPeriod" />
                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftePeriod" TargetControlID="txtPeriod"
                                runat="server" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                ValidChars="-" />
                        </div>
                    </div>

                    <div style="width: 25%; float: left; padding: 0px;">
                        <div class="trLeft" style="width: 33%; float: left; vertical-align: top; padding: 0px;">
                           <%-- Employee--%>
                           <asp:Literal ID="Literal3" runat ="server" Text ='<%$Resources:ControlsCommon,Employee %>'>
                           </asp:Literal>
                        </div>
                        <div style="width: 60%; float: left;">
                            <asp:UpdatePanel ID="upnlEmployee" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlEmployee" runat="server" Width="130px" Height="25px" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 6%; float: left; margin-top: -5px;">
                        <asp:Button ID="btnShow" meta:resourcekey="show" runat="server" Width="50px" CssClass="btnsubmit"
                            OnClick="btnShow_Click" OnClientClick="return ValidateFilter();" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlView" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divView" runat="server" class="firstTrRight" style="float: left; width: 100%;
                    height: auto; padding-bottom: 5px; ">
                    <div style="padding-left: 5px; color: #307296; font-weight: bold; margin-bottom: 5px;">
                       <asp:Literal ID="Literal111" runat ="server" meta:resourcekey= 'EmployeePerformanceDetails'>
                       
                       </asp:Literal>
                    </div>
                    <asp:GridView ID="dgvPerformance" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" ShowFooter="true"
                        Width="100%" PagerSettings-Visible="false" OnRowCommand="dgvPerformance_RowCommand"
                        OnRowDataBound="dgvPerformance_RowDataBound">
                        <HeaderStyle />
                        <PagerSettings Visible="False" />
                        <RowStyle ForeColor="#000066" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <%--Employee--%>
                                    <asp:Literal ID="Literal111" runat ="server" Text ='<%$Resources:COntrolsCommon,Employee %>'>
                       
                       </asp:Literal>
                                    
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployee" runat="server" Text='<%# Eval("EmployeeName") %>' />
                                    <asp:HiddenField ID="hdnEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                    <asp:HiddenField ID="hdnPerformanceID" runat="server" Value="0" />
                                </ItemTemplate>
                                <ItemStyle Width="350px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   <%-- Projection--%>
                                   <asp:Literal ID="Literal111" runat ="server" meta:resourcekey= 'Projection'>
                       
                       </asp:Literal>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtProjection" runat="server" MaxLength="12" Width="175px" Style="text-align: right;
                                        padding-right: 3px;" onkeypress="return ValidateDecimal(event,this.value);" />
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteProjection" runat="server" FilterType="Custom,Numbers"
                                        ValidChars="." TargetControlID="txtProjection" />
                                </ItemTemplate>
                                <ItemStyle Width="60px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                   <%-- Actual--%>
                                   <asp:Literal ID="Literal111" runat ="server" meta:resourcekey= 'Actual'>
                       
                       </asp:Literal>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtActual" runat="server" MaxLength="12" Width="175px" Style="text-align: right;
                                        padding-right: 3px;" onkeypress="return ValidateProjection(event,this);" />
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteActual" runat="server" FilterType="Custom,Numbers"
                                        ValidChars="." TargetControlID="txtActual" />
                                </ItemTemplate>
                                <ItemStyle Width="60px" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnDelete" Width="18px" Height="18px" ToolTip='<%$Resources:ControlsCOmmon,Delete %>' ImageUrl="~/images/delete_popup.png"
                                        runat="server" CommandName="DeletePerformance"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit %>' OnClick="btnSubmit_Click"
                                        ValidationGroup="Submit" />
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <FooterStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgbtnViewChart" Width="18px" Height="18px" meta:resourcekey='ViewChart'
                                        ImageUrl="~/images/chart.png" runat="server" CommandName="ViewChart"></asp:ImageButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel %>' OnClick="btnCancel_Click" />
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <FooterStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    <uc:Pager ID="PerformancePager" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="divChart" runat="server" style="width: 500px; height: auto; min-height: 500px;
            display: none;">
            <div id="popupmainwrap">
                <div id="popupmain" style="width:450px!important;">
                    <div id="header11">
                        <div id="hbox1">
                            <div id="headername">
                                <asp:Label ID="lblHeader" runat="server" meta:Resourcekey ='PerformanceChart'></asp:Label>
                            </div>
                        </div>
                        <div id="hbox2">
                            <div id="close1">
                                <a href="">
                                    <asp:ImageButton ID="imgPopupClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                        CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="contentwrap">
                        <div id="content">
                            <table style="width: 100%;">
                                <tr>
                                    <td width="100%">
                                        <asp:UpdatePanel ID="upnlChart" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="trLeft" style="width: 100%; height: 30px;">
                                                    <div style="width:80%; float: left;">
                                                         <asp:Literal ID="Literal5" runat ="server" Text ='<%$Resources:ControlsCommon,Employee%>' >
                                                          </asp:Literal>
                                                          &nbsp 
                                                             <asp:Label ID="lblSelectedEmployee" runat="server" />
                                                    </div>
                                                  
                                                </div>
                                                <div style="width: 100%">
                                                    <div style="width: 40%; float: left">
                                                        <div class="trLeft" style="width: 28%; float: left">
                                                          <asp:Literal ID="Literal11" runat ="server" Text ='<%$Resources:ControlsCommon,FromDate%>' >
                                                          </asp:Literal>
                                                        </div>
                                                        <div class="trLeft" style="width: 80%; float: left">
                                                            <asp:TextBox ID="txtFromDate" runat="server" Style="width: 79px; margin-right: 5px;" />
                                                            <asp:ImageButton ID="ibtnFromDate" runat="server" style="margin-top: 5px;" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                CausesValidation="false" />
                                                            <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate"
                                                                PopupButtonID="ibtnFromDate" Format="MMM-yyyy">
                                                            </AjaxControlToolkit:CalendarExtender>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="fteFromDate" TargetControlID="txtFromDate"
                                                                runat="server" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                                                ValidChars="-" />
                                                            <asp:CustomValidator ID="cvFromDate" runat="server" ClientValidationFunction="ValidateDate(document.getElementById('ctl00_public_content_txtFromDate'));"
                                                                ErrorMessage="Please Select Valid From Date" ControlToValidate="txtFromDate"
                                                                ValidationGroup="Chart" Display="None" />
                                                        </div>
                                                    </div>
                                                    <div style="width: 40%; float: left">
                                                        <div class="trLeft" style="width: 20%; float: left">
                                                           <%-- To--%>
                                                           <asp:Literal ID="Literal4" runat ="server" Text ='<%$Resources:ControlsCommon,ToDate%>' >
                                                          </asp:Literal>
                                                        </div>
                                                        <div class="trLeft" style="width: 71%; float: left">
                                                            <asp:TextBox ID="txtToDate" runat="server" Style="width: 68px; margin-right: 5px;" />
                                                            <asp:ImageButton ID="ibtnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                CausesValidation="false" />
                                                            <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate"
                                                                PopupButtonID="ibtnToDate" Format="MMM-yyyy">
                                                            </AjaxControlToolkit:CalendarExtender>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="fteToDate" TargetControlID="txtToDate"
                                                                runat="server" FilterType="Custom,Numbers,UppercaseLetters,LowercaseLetters"
                                                                ValidChars="-" />
                                                            <asp:CustomValidator ID="cvToDate" runat="server" ClientValidationFunction="ValidateDate(document.getElementById('ctl00_public_content_txtToDate'));"
                                                                ErrorMessage="Please Select Valid To Date" ControlToValidate="txtToDate" ValidationGroup="Chart"
                                                                Display="None" />
                                                        </div>
                                                    </div>
                                                    <div style="width: 10%; float: left; margin-top: 22px;">
                                                        <asp:Button ID="btnViewChart" runat="server" CssClass="btnsubmit" Width="50px" Text='<%$Resources:ControlsCommon,Show %>'
                                                            OnClick="btnViewChart_Click" ValidationGroup="Chart" />
                                                    </div>
                                                    <div style="clear: both;">
                                                    </div>
                                                    <div id="divImage" runat="server" style="text-align: center; display: none;">
                                                        <img id="imgChart" runat="server" alt="" src="" height="400" width="400" style="margin-top: 20px;
                                                            margin-bottom: 10px;" />
                                                    </div>
                                                    <div id="divNoImage" runat="server" style="text-align: center; height: 300px; display: none;
                                                        margin-top: 20px;">
                                                        <asp:Label ID="lblNoImage" runat="server" Text='<%$Resources:ControlsCommon,NoRecordFound %>' class="error" />
                                                    </div>
                                                    <div>
                                                        <asp:HiddenField ID="hdnSelectedEmployee" runat="server" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeChart" CancelControlID="imgPopupClose"
                runat="server" BackgroundCssClass="modalBackground" PopupControlID="divChart"
                TargetControlID="btnChartPopup" PopupDragHandleControlID="divChart" Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btnChartPopup" runat="server" Style="display: none" />
        </div>
    </div>
    <div id="dvNoPermission" style="text-align: center; margin-top: 10px" runat="server">
        <asp:Label ID="lblNoPermission" runat="server" CssClass="error" Text='<%$Resources:ControlsCommon,NoPermission %>'>
        </asp:Label>
    </div>
</asp:Content>
