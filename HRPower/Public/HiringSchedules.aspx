<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="HiringSchedules.aspx.cs" Inherits="Public_HiringSchedules" Title="Untitled Page" %>

<%@ Register Src="../Controls/HiringSchedule.ascx" TagName="HiringSchedule" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>

        <script src="../js/jquery/jquery.ui.tabs.js" type="text/javascript"></script>

        <link href="../css/employee.css" rel="stylesheet" type="text/css" />
        <%--  Salutation--%>
        <%--Male--%>
        <%-- Female--%>
        <%--    commented--%>
        <ul>
            <li><a href='Projects.aspx'><span>
                <%--   Employee Number--%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Project %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href='ManPowerPlanning.aspx'><span>
                <%--   Employee Number--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,Plan %>'>
                </asp:Literal>
            </span></a></li>
               <li class="selected"><a href="HiringSchedules.aspx">
                <%-- View Policies--%>
<%--                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,Calender %>'>
                </asp:Literal>--%>
                
                
                                <asp:Literal ID="Literal1" runat="server" Text="Hiring Schedules">
                </asp:Literal>
            </a></li>
            
            <li><a href="HiringSchedules.aspx">
                <%--Work Location--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,Expense %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
         
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%">
    
    <div >
        <div style="padding-top: 10px; margin-left: 5px">
            <div class="trLeft" style="float: left; width: 8%; height: 29px">
                <%--   Employee Number--%>
                <asp:Literal ID="Literal4" runat="server" Text="Year"></asp:Literal>
            </div>
            <div class="trRight" style="float: left; width: 70%; height: 29px">
                <div style="float: left; width: 25%; height: 29px">
                
                <asp:DropDownList ID="ddlYear" runat ="server" CssClass ="dropdownlist_mandatory" >
                </asp:DropDownList>
                </div>
             
            </div>
            <div>
    <asp:DataGrid ID="dgHiringSchedules" runat ="server" BackColor="White" 
            BorderColor="#CCCCCC" BorderStyle="None"  Width ="98%" BorderWidth="1px" CellPadding="3" 
            style="margin-right: 0px" >
        <FooterStyle BackColor="White" ForeColor="#000066" />
        <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" 
            Mode="NumericPages" />
        <ItemStyle ForeColor="#000066" />
        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
    </asp:DataGrid>
    </div>
    </div>
    </div>
    
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/New_Employee_Big.PNG" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnNew" runat="server" OnClick="btnNew_Click">
                            <%--  Add Employee--%>
                            <asp:Literal ID="Literal59" runat="server" Text="New Planning">
                            
                            </asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Lit1" runat="server" Text="View All Plans">
                            
                            </asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkSchedule" runat="server" OnClick="lnkSchedule_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Literal7" runat="server" Text="Hiring Schedule">
                            
                            </asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnDelete" Text='<%$Resources:ControlsCommon,Delete %>' runat="server"
                            CausesValidation="false" OnClick="btnDelete_Click"> 
                          <%-- Delete--%>
                           
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnPrint" Text='<%$Resources:ControlsCommon,Print %>' runat="server"
                            CausesValidation="false" OnClick="btnPrint_Click">
                         <%--Print--%>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnEmail" Text='<%$Resources:ControlsCommon,Email %>' runat="server"
                            CausesValidation="false" OnClick="btnEmail_Click">
              
             <%--   Email--%>  
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
