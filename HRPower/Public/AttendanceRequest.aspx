﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="AttendanceRequest.aspx.cs" Inherits="Public_AttendanceRequest" Title="Attendance Request" %>

<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
    <table style="width: 100%" cellpadding="4" cellspacing="0">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnsubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FormView ID="fvAttendance" runat="server" Width="100%" OnItemCommand="fvAttendance_ItemCommand"
                            DataKeyNames="RequestId,RequestedTo,StatusId,Employee,ProjectID" OnDataBound="fvAttendance_DataBound"
                            CssClass="labeltext">
                            <EditItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="trLeft" width="20%">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Project"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:DropDownList ID="ddlProject" runat="server" DataTextField="Project" DataValueField="ProjectID"
                                                    Width="250px">
                                                </asp:DropDownList>
                                                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlProject" InitialValue ="-1"
                                                                Display="Dynamic" meta:resourcekey="PleaseSelectProject"  SetFocusOnError="True"
                                                                CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="20%">
                                                <asp:Literal ID="Literal14" runat="server" meta:resourcekey="FromDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"  OnTextChanged="txtFromDate_TextChanged"
                                                    Text='<%# Eval("FromDate") %>' AutoPostBack="true"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" />
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate"
                                                    PopupButtonID="ImageButton1" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                  <asp:CustomValidator ID="CustomValidator1" runat="server" ValidateEmptyText="true"
                                                    ControlToValidate="txtFromDate" Display="Dynamic" ClientValidationFunction="valRequestFromToDate"
                                                    ValidationGroup="Submit" CssClass="error"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="20%">
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="ToDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    OnTextChanged="txtToDate_TextChanged" Text='<%# Eval("ToDate") %>' AutoPostBack="true"></asp:TextBox>
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" />
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDate"
                                                    PopupButtonID="ImageButton2" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                  <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                                                    Display="Dynamic" ClientValidationFunction="valRequestFromToDate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                                  <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                                                    Display="Dynamic" ClientValidationFunction="checkFromToDate" ValidationGroup="Submit"
                                                   meta:resourcekey="ToDateMustBeGreaterThanFromDate" CssClass="error"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="20%">
                                                <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%" style="overflow: auto;">
                                                <asp:DataList ID="dlAttendanceDetails" runat="server" Width="100%" BackColor="White"
                                                    BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" OnItemDataBound="dlAttendanceDetails_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft ">
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Date"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="FromTime"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="ToTime"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Duration"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft" runat ="server"  id="trAtndet">
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                                     <asp:Label ID="lblRemarks" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtFromTime" runat="server" Text='<%# Eval("TimeFrom") %>' onblur="calDuration(this);"
                                                                        Width="100"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtToTime" runat="server" Text='<%# Eval("TimeTo") %>' Width="100"
                                                                        onblur="calDuration(this);"></asp:TextBox>
                                                                    <asp:CheckBox ID="chkApply" runat="server" ToolTip="Apply to All" OnCheckedChanged="chkApply_CheckedChanged"
                                                                        Visible='<%#SetApply("ApplyToAll") %>' AutoPostBack="true" />
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtDuration" runat="server" Text='<%# Eval("Duration") %>' Width="100"
                                                                        Enabled="false"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="trLeft" width="20%">
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    DataValueField="StatusId">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="20%">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" width="55%" align="right" style="padding-left: 550px;">
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" Width="75px" CommandName="Add"
                                                    CssClass="btnsubmit" UseSubmitBehavior="false" CommandArgument='<%# Eval("RequestId") %>'
                                                    meta:resourcekey="Submit" />&nbsp;
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                                                    CommandName="CancelRequest" Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="lbRequestedBY" runat="server" Text=' <%# Eval("RequestedBY") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Project"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="lblDate" runat="server" Text=' <%# Eval("Project") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Date"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="Label1" runat="server" Text=' <%# Eval("Period") %>'></asp:Label>
                                                <asp:TextBox ID="txtVFromDate" runat ="server" Text=' <%# Eval("FromDate") %>' Visible ="false" ></asp:TextBox>
                                                <asp:TextBox ID="txtVToDate" runat ="server" Text=' <%# Eval("ToDate") %>' Visible ="false" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" AutoPostBack="true"   OnSelectedIndexChanged="ddlEditStatus_SelectedIndexChanged" runat="server" DataTextField="Description"
                                                    Visible="false" DataValueField="StatusId">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblStatus" runat="server" Text=' <%# Eval("Status") %>'></asp:Label>
                                            </td>
                                        </tr>
                                         <tr id="trVRemarks" runat="server" visible ="false"  >
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%" style ="word-break:break-all;">
                                                <%# Eval("Remarks")%>
                                            </td>
                                        </tr>
                                        <tr id="trForwardedBy" runat="server" visible='<%# GetVisibility(Eval("ForwardedBy"))%>'>
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="ForwardedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <%# Eval("ForwardedBy")%>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trRemarks">
                                            <td class="innerdivheader" width="20%">
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Remarks") %>'
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                    Display="Dynamic" meta:resourcekey="EnterRemarks" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                         <tr runat="server" style="display: none;" id="trDetails">
                                            <td class="innerdivheader" width="20%">
                                                
                                            </td>
                                            <td class="trRight" width="70%">
                                              <asp:DataList ID="dlAttendanceForAction" runat="server" Width="100%" BackColor="White" 
                                                    BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"  OnItemDataBound="dlAttendanceForAction_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft ">
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:CheckBox ID="chkAll" runat ="server" ToolTip ="Select All" OnCheckedChanged="chkAll_CheckedChanged" AutoPostBack ="true" />
                                                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Date"></asp:Literal>
                                                                   
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="FromTime"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="ToTime"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Duration"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft">
                                                                <td style="width: 5%" class="trleft">
                                                                    <asp:CheckBox ID="chkSelect" runat="server"  Checked ='<%#  CheckApproved(Eval("AttendanceID")) %>'  />
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                
                                                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                                    <asp:Label ID="lblRemarks" runat="server" ></asp:Label>
                                                                     <asp:HiddenField ID="hfRequestedBy" runat ="server" Value ='<%# Eval("EmployeeId") %>' />
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtFromTime" runat="server" Text='<%# Eval("TimeFrom") %>' Enabled="false"
                                                                        Width="100"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtToTime" runat="server" Text='<%# Eval("TimeTo") %>' Width="100" Enabled="false"
                                                                        ></asp:TextBox>
                                                                   
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtDuration" runat="server" Text='<%# Eval("Duration") %>' Width="100"
                                                                        Enabled="false"></asp:TextBox>
                                                                     <asp:HiddenField ID="hfAttendanceID" runat ="server" Value = '<%# Eval("AttendanceID") %>'/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td class="innerdivheader" width="20%">
                                            </td>
                                            <td class="trRight" width="50%" style="padding-left: 470px;">
                                                <asp:Button ID="btnApprove" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                                                    CommandArgument=' <%# Eval("StatusId") %>' CommandName="Approve" Visible="false"
                                                    ValidationGroup="ApproveRequest" Width="75px" />
                                                <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" CommandName="Cancel"
                                                    meta:resourcekey="Cancel" Visible="false" Width="75px" />
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    meta:resourcekey="Submit" Width="75px" CommandName="RequestCancel" Visible="false" />
                                                <asp:Button ID="btnRequestForCancel" runat="server" CssClass="btnsubmit" CommandArgument='<%# Eval("StatusId") %>'
                                                    CommandName="RequestForCancel" Visible="false" meta:resourcekey="Submit" Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvAttendance" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="dlAttendance" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlAttendance" runat="server" Width="100%" DataKeyField="RequestId"
                            OnItemCommand="dlAttendance_ItemCommand" OnItemDataBound="dlAttendance_ItemDataBound"
                            CssClass="labeltext">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="25" valign="top" style="padding-left: 5px">
                                            <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkAdvance')" />
                                        </td>
                                        <td style="padding-left: 7px">
                                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server"
                                    onmouseover="showCancel(this);" onmouseout="hideCancel(this);">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td valign="top" rowspan="5" width="25" style="padding-left: 7px">
                                                        <asp:CheckBox ID="chkAdvance" runat="server" />
                                                    </td>
                                                    <td width="100%">
                                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkAdvance" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_View" CausesValidation="False">
                                                                             <%# Eval("Period")%>  </asp:LinkButton>
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Edit" ImageUrl="~/images/edit.png" meta:resourcekey="Edit" />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Cancel" ImageUrl="~/images/cancel.png" meta:resourcekey="Cancel"
                                                                        OnClientClick="return ConfirmCancel(this.id)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding-left: 25px">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("Date")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Project"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("Project")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("status") %>
                                                                    <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                                    <%--    <%# Eval("RequestStatusDate")%> --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="40%" valign="top">
                                                                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td class="innerdivheadervalue" valign="top" align="left">
                                                                    <%# Eval("Requested") %>
                                                                    <asp:Label ID="lbRequested" runat="server" Text=' <%# Eval("RequestedTo") %>' Visible="false"> </asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc:Pager ID="SalaryRequestpager" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvAttendance" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="dlAttendance" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add salary advance.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="Add"> </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/Salary_Advance Request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="View"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="Delete"></asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
