﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="LeaseAgreement.aspx.cs" Inherits="Public_LeaseAgreement" Title="Lease Agreement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <div id='cssmenu'>
        <ul>
            <li ><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li ><a href="Visa.aspx"><asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx"><asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx"><asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li class="selected"><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx"><asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx"><asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx"><asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upAddLeaseAgreement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddLeaseAgreement" runat="server" style="width: 100%; min-height: 1200px;
                height: auto; float: left; padding-top: 20px;">
                <asp:HiddenField ID="hfLeaseAgreementID" runat="server" />
                
                
                <div id="divAddLeaseAgreementDataPart" runat="server">
                    <div style="width: 99%; padding-left: 1%; height: 20px; float: left; border-bottom: 1px dashed #cdcece;
                        padding-bottom: 8px; font-weight: bold; color: #000000;">
                        <%--Contract Info--%>
                        <asp:Literal ID="lit1" runat ="server" meta:resourcekey="ContractInfo"></asp:Literal>
                    </div>
                    <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                             <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:DocumentsCommon,Company%>'></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:DropDownList ID="ddlCompany" runat="server" Width="207px" BackColor="#FFFFEC">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server"  ErrorMessage="*"
                                Display="Dynamic" ControlToValidate="ddlCompany" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                           <%-- Agreement Number--%>
                           <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="AgreementNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtAgreementNumber" runat="server" CssClass="textbox_mandatory"
                                Width="200px" Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtAgreementNumber" runat="server" ErrorMessage="*"
                                ControlToValidate="txtAgreementNumber" Display="Dynamic" ValidationGroup="Submit"
                                ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Lessor--%>
                             <asp:Literal ID="Literal3" runat ="server" meta:resourcekey="Lessor"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtLessor" runat="server" CssClass="textbox_mandatory" Width="200px"
                                Style="margin-right: 10px" BackColor="#FFFFEC"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtLessor" runat="server" ErrorMessage="*"
                                ControlToValidate="txtLessor" Display="Dynamic" ValidationGroup="Submit" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Rental Value--%><asp:Literal ID="Literal4" runat ="server" meta:resourcekey="RentalValue"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <div style="width: 100%;">
                                <div style="width: 50%;">
                                    <asp:TextBox ID="txtRentalValue" runat="server" CssClass="textbox" Width="100px"
                                        Style="margin-right: 10px"></asp:TextBox>
                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtRentalValueExtender" runat="server"
                                        FilterType="Numbers,Custom" TargetControlID="txtRentalValue" ValidChars=".">
                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                </div>
                                <div style="width: 50%;">
                                    <asp:CheckBox ID="chkDepositHeld" runat="server" Text="Deposit Held" meta:resourcekey="DepositHeld" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Tenant--%>
                            <asp:Literal ID="Literal5" runat ="server" meta:resourcekey="Tenant"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtTenant" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                           <%-- Contact Numbers--%>
                           <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="ContactNumbers"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtContactNumbers" runat="server" CssClass="textbox" Width="200px"
                                Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                          <%--  Start date--%> <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Startdate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px" BackColor="#FFFFEC"
                                Style="margin-right: 10px"></asp:TextBox>
                            <asp:ImageButton ID="ibtnFromdate" runat="server" style="margin-top:6px;" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="false" CssClass="imagebutton" />
                            <AjaxControlToolkit:CalendarExtender ID="extenderExpenseFromDate" runat="server"
                                TargetControlID="txtFromDate" PopupButtonID="ibtnFromdate" Format="dd/MM/yyyy"
                                Enabled="true">
                            </AjaxControlToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtFromDate" runat="server" ErrorMessage="*"
                                ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Submit" ></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvStartDate" runat="server" ClientValidationFunction="CheckFuturedate"
                                CssClass="error" ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--End date--%><asp:Literal ID="Literal8" runat ="server" meta:resourcekey="Enddate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px" BackColor="#FFFFEC"
                                Style="margin-right: 10px"></asp:TextBox>
                            <asp:ImageButton ID="ibtnTodate" runat="server" style="margin-top:6px;" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CssClass="imagebutton" CausesValidation="false" />
                            <AjaxControlToolkit:CalendarExtender ID="extenderExpenseToDate" runat="server" TargetControlID="txtToDate"
                                PopupButtonID="ibtnTodate" Format="dd/MM/yyyy">
                            </AjaxControlToolkit:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvtxtToDate" runat="server" ErrorMessage=""
                                ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvEndDate1" runat="server" ClientValidationFunction="CheckValidDateAndFromToDate"
                                CssClass="error" ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Lease Period--%><asp:Literal ID="Literal9" runat ="server" meta:resourcekey="LeasePeriod"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtLeasePeriod" runat="server" CssClass="textbox" Width="200px"
                                Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Document date--%><asp:Literal ID="Literal10" runat ="server" meta:resourcekey="Documentdate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtDocumentDate" runat="server" CssClass="textbox_mandatory" Width="100px" BackColor="#FFFFEC"
                                Style="margin-right: 10px"></asp:TextBox>
                            <asp:ImageButton ID="ibtnDocumentDate" runat="server" style="margin-top:6px;" ImageUrl="~/images/Calendar_scheduleHS.png"
                                CausesValidation="false" CssClass="imagebutton" />
                            <AjaxControlToolkit:CalendarExtender ID="extenderDocumentDate" runat="server" TargetControlID="txtDocumentDate"
                                Format="dd/MM/yyyy" PopupButtonID="ibtnDocumentDate">
                            </AjaxControlToolkit:CalendarExtender>
                            <asp:CustomValidator ID="cvtxtDocumentDate" runat="server" ClientValidationFunction="CheckValidDate"
                                CssClass="error" ControlToValidate="txtDocumentDate" Display="Dynamic" ValidationGroup="Submit"></asp:CustomValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtDocumentDate" runat="server" ErrorMessage="*"
                                ControlToValidate="txtDocumentDate" Display="Dynamic" ValidationGroup="Submit"
                                ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div style="width: 100%; height: 55px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                           <%-- Address--%><asp:Literal ID="Literal11" runat ="server" meta:resourcekey="Address"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 55px; float: left;">
                            <asp:TextBox ID="txtAddress" runat="server" onchange="RestrictMulilineLength(this, 500);"
                                onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="205px"
                                TextMode="MultiLine" Height="50px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Status--%><asp:Literal ID="Literal12" runat ="server" meta:resourcekey="Status"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:Label ID="lblStatusAdd" runat="server" Text='<%$Resources:DocumentsCommon,New%>'></asp:Label>
                        </div>
                    </div>
                    <div style="width: 99%; padding-left: 1%; height: 20px; margin-top: 30px; float: left;
                        border-bottom: 1px dashed #cdcece; padding-bottom: 8px; font-weight: bold; color: #000000;">
                        <%--Building Info--%><asp:Literal ID="Literal13" runat ="server" meta:resourcekey="BuildingInfo"></asp:Literal>
                    </div>
                    <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Land Lord--%><asp:Literal ID="Literal14" runat ="server" meta:resourcekey="LandLord"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtLandLord" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Unit Number--%><asp:Literal ID="Literal15" runat ="server" meta:resourcekey="UnitNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtUnitNumber" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Lease Unit Type--%><asp:Literal ID="Literal16" runat ="server" meta:resourcekey="LeaseUnitType"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:UpdatePanel ID="upLeaseUnitType" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlLeaseUnitType" runat="server" Width="207px">
                                    </asp:DropDownList>
                                    <asp:Button ID="imgLeaseUnitType" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        Text="..." OnClick="imgLeaseUnitType_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Building Number--%><asp:Literal ID="Literal17" runat ="server" meta:resourcekey="BuildingNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtBuildingNumber" runat="server" CssClass="textbox" Width="200px"
                                Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                           <%-- Location--%><asp:Literal ID="Literal18" runat ="server" meta:resourcekey="Location"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtLocation" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Plot Number--%><asp:Literal ID="Literal19" runat ="server" meta:resourcekey="PlotNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtPlotNumber" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="width: 100%; height: 40px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Total--%><asp:Literal ID="Literal20" runat ="server" meta:resourcekey="Total"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 40px; float: left;">
                            <asp:TextBox ID="txtTotal" runat="server" CssClass="textbox" Width="200px" Style="margin-right: 10px"></asp:TextBox>
                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtTotalExtender" runat="server"
                                FilterType="Numbers,Custom" TargetControlID="txtTotal" ValidChars=".">
                            </AjaxControlToolkit:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div style="width: 100%; height: 80px; float: left;">
                        <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                            <%--Remarks--%><asp:Literal ID="Literal21" runat ="server" Text ='<%$Resources:DocumentsCommon,OtherInfo%>' ></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 70%; height: 80px; float: left;">
                            <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                                onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="300px"
                                TextMode="MultiLine" Height="76px"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div id="divAddLeaseAgreementDocumentPart" runat="server" style="width: 100%; height: auto;
                    float: left; margin-top: 20px;">
                    <fieldset style="width: 98%; padding: 1%;">
                        <legend><asp:Literal ID="Literal61" runat ="server"  Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal></legend>
                        <div style="width: 100%; height: 30px; float: left; margin-top: 10px;">
                            <div style="width: 40%; height: 30px; float: left;">
                                <%--Document Name--%><asp:Literal ID="Literal22" runat ="server" Text ='<%$Resources:DocumentsCommon,DocumentName%>'  ></asp:Literal>
                            </div>
                            <div style="width: 40%; height: 30px; float: left;">
                               <%-- Choose file..--%><asp:Literal ID="Literal23" runat ="server" Text ='<%$Resources:DocumentsCommon,ChooseFile%>'  ></asp:Literal>
                            </div>
                            <div style="width: 15%; height: 30px; float: left;">
                            </div>
                        </div>
                        <div style="width: 100%; height: 40px; float: left;">
                            <div style="width: 40%; height: 40px; float: left;">
                                <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="30" Width="200px"></asp:TextBox>
                            </div>
                            <div style="width: 40%; height: 40px; float: left;">
                                <AjaxControlToolkit:AsyncFileUpload ID="fuLeaseAgreement" runat="server" CompleteBackColor="White"
                                    ErrorBackColor="White" OnUploadedComplete="fuLeaseAgreement_UploadedComplete"
                                    PersistFile="True" Visible="true" Width="250px" />
                            </div>
                            <div style="width: 15%; height: 40px; float: left;">
                                <asp:LinkButton ID="btnattachdoc" runat="server" Enabled="true" OnClick="btnattachdoc_Click"
                                    Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="VG1" ForeColor="#33a3d5">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div style="width: 100%; height: 20px; float: left; margin-top: 5px;">
                            <div style="width: 40%; height: 20px; float: left;">
                                <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                     Display="Dynamic" ErrorMessage="*"
                                    ValidationGroup="VG1"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvLeaseAgreementdocnameduplicate" runat="server" ClientValidationFunction="validateLeaseAgreementdocnameduplicate"
                                    ControlToValidate="txtDocname" Display="Dynamic" ErrorMessage="Duplicate document name"
                                    ValidationGroup="VG1"></asp:CustomValidator>
                            </div>
                            <div style="width: 40%; height: 20px; float: left;">
                                <asp:CustomValidator ID="cvAttachment" runat="server" ClientValidationFunction="validateLeaseAgreementAttachment"
                                    CssClass="error" ErrorMessage="Please attach file" ValidateEmptyText="True" ValidationGroup="VG1"></asp:CustomValidator>
                            </div>
                            <div style="width: 15%; height: 20px; float: left;">
                            </div>
                        </div>
                    </fieldset>
                    <div id="divListDoc" runat="server" class="container_content" style="display: inline;
                        width: 100%; margin-top: 20px; height: auto; overflow: auto">
                        <asp:UpdatePanel ID="upListDoc" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DataList ID="dlListDoc" runat="server" DataKeyField="Node" GridLines="Horizontal"
                                    OnItemCommand="dlListDoc_ItemCommand" Width="100%">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <HeaderTemplate>
                                        <div style="width: 100%; height: 30px; float: left;">
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <%--Document Name--%><asp:Literal ID="Literal22" runat ="server" Text ='<%$Resources:DocumentsCommon,DocumentName%>'  ></asp:Literal>
                                            </div>
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <%--File Name--%><asp:Literal ID="Literal14" runat ="server" Text ='<%$Resources:DocumentsCommon,FileName%>'  ></asp:Literal>
                                            </div>
                                            <div style="width: 15%; height: 30px; float: left;">
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100%; height: 30px; padding-top: 10px; float: left;">
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="150px"></asp:Label>
                                            </div>
                                            <div style="width: 40%; height: 30px; float: left;">
                                                <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                            </div>
                                            <div style="width: 15%; height: 30px; float: left;">
                                                <asp:ImageButton ID="btnRemove" runat="server" CommandArgument='<% # Eval("Node") %>'
                                                    CommandName="REMOVE_ATTACHMENT" SkinID="DeleteIconDatalist" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnattachdoc" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;
                    margin-bottom: 20px;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>' Width="75px"
                        ValidationGroup="Submit" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>' Width="75px"
                        Style="margin-left: 10px; margin-right: 35%;" OnClick="btnCancel_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upSingleViewLeaseAgreement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleViewLeaseAgreement" runat="server" style="width: 100%; min-height: 1000px;
                height: auto; float: left; padding-top: 20px;">
                <div style="width: 99%; padding-left: 1%; height: 20px; float: left; border-bottom: 1px dashed #cdcece;
                    padding-bottom: 8px; font-weight: bold; color: #000000;">
                    <%--Contract Info--%><asp:Literal ID="Literal24" runat ="server" meta:resourcekey="ContractInfo"></asp:Literal>
                </div>
                <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Company--%><asp:Literal ID="Literal25" runat ="server" Text ='<%$Resources:DocumentsCommon,Company%>'  ></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 50%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblCompany" runat="server"></asp:Label>
                    </div>
                    <div class="trRight" style="width: 20%; height: 40px; float: left; font-weight: bold;
                        color: #FF3300;">
                        <asp:Label ID="lblRenewed" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Agreement Number--%> <asp:Literal ID="Literal26" runat ="server" meta:resourcekey="AgreementNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblAgreementNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Lessor--%><asp:Literal ID="Literal27" runat ="server" meta:resourcekey="Lessor"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblLessor" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Rental Value--%><asp:Literal ID="Literal28" runat ="server" meta:resourcekey="RentalValue"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        <div style="width: 100%;">
                            :
                            <asp:Label ID="lblRentalValue" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Deposit Held--%><asp:Literal ID="Literal29" runat ="server" meta:resourcekey="DepositHeld"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        <div style="width: 100%;">
                            :
                            <asp:Label ID="lblDepositHeld" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Tenant--%> <asp:Literal ID="Literal42" runat ="server" meta:resourcekey="Tenant"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblTenant" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Contact Numbers--%> <asp:Literal ID="Literal43" runat ="server" meta:resourcekey="ContactNumbers"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblContactNumbers" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--From date--%><asp:Literal ID="Literal44" runat ="server" meta:resourcekey="Startdate"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblFromdate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--To date--%><asp:Literal ID="Literal45" runat ="server" meta:resourcekey="Enddate"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblTodate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Lease Period--%><asp:Literal ID="Literal46" runat ="server" meta:resourcekey="LeasePeriod"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblLeasePeriod" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Document date--%><asp:Literal ID="Literal47" runat ="server" meta:resourcekey="Documentdate"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblDocumentdate" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; min-height: 55px; height: auto; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Address--%><asp:Literal ID="Literal48" runat ="server" meta:resourcekey="Address"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; min-height: 55px; height: auto; float: left;
                        word-break: break-all;">
                        :
                        <asp:Label ID="lblAddress" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Status--%><asp:Literal ID="Literal49" runat ="server" meta:resourcekey="Status"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 99%; padding-left: 1%; height: 20px; margin-top: 10px; float: left;
                    border-bottom: 1px dashed #cdcece; padding-bottom: 8px; font-weight: bold; color: #000000;">
                    <%--Building Info--%><asp:Literal ID="Literal50" runat ="server" meta:resourcekey="BuildingInfo"></asp:Literal>
                </div>
                <div style="width: 100%; height: 40px; float: left; margin-top: 20px;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Land Lord--%><asp:Literal ID="Literal51" runat ="server" meta:resourcekey="LandLord"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblLandLord" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Unit Number--%><asp:Literal ID="Literal52" runat ="server" meta:resourcekey="UnitNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblUnitNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                       <%-- Lease Unit Type--%><asp:Literal ID="Literal53" runat ="server" meta:resourcekey="LeaseUnitType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblLeaseUnitType" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Building Number--%><asp:Literal ID="Literal54" runat ="server" meta:resourcekey="BuildingNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblBuildingNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Location--%><asp:Literal ID="Literal55" runat ="server" meta:resourcekey="Location"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Plot Number--%><asp:Literal ID="Literal56" runat ="server" meta:resourcekey="PlotNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblPlotNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; height: 40px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Total--%><asp:Literal ID="Literal57" runat ="server" meta:resourcekey="Total"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 40px; float: left;">
                        :
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                    </div>
                </div>
                <div style="width: 100%; min-height: 55px; height: auto; float: left;">
                    <div class="trLeft" style="width: 25%; height: 40px; float: left;">
                        <%--Remarks--%><asp:Literal ID="Literal58" runat ="server" Text ='<%$Resources:DocumentsCommon,OtherInfo%>' ></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; min-height: 55px; height: auto; float: left;
                        word-break: break-all;">
                        :
                        <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upViewLeaseAgreement" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewLeaseAgreement" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlLeaseAgreement" runat="server" Width="100%" DataKeyField="LeaseID"
                    CssClass="labeltext" OnItemCommand="dlLeaseAgreement_ItemCommand" OnItemDataBound="dlLeaseAgreement_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkLeaseAgreement')" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%--Select All--%><asp:Literal ID="Literal14" runat ="server" Text ='<%$Resources:ControlsCommon,SelectAll%>'  ></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkLeaseAgreement" runat="server" /></div>
                            <div style="width: 80%; height: 30px; float: left;">
                                <asp:LinkButton ID="lnkLeaseAgreement" runat="server" CssClass="listHeader bold"
                                    CommandArgument='<%# Eval("LeaseID") %>' ToolTip="View" CommandName="_View" Text='<%# Eval("CompanyName")%>'
                                    CausesValidation="False">
                                </asp:LinkButton>
                                <%# Eval("Header")%>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("LeaseID") %>'
                                    CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    ToolTip="Edit"></asp:ImageButton>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Start Date--%><asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Startdate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 48%; height: 30px; float: left;">
                            :
                            <%# Eval("StartDate")%>
                        </div>
                        <div class="trRight" style="width: 12%; height: 30px; float: left; font-weight: bold;
                            color: #FF3300;">
                            <%# Eval("Renewed")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--End Date--%><asp:Literal ID="Literal59" runat ="server" meta:resourcekey="Enddate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("EndDate")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Document Date--%><asp:Literal ID="Literal60" runat ="server" meta:resourcekey="DocumentDate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("DocumentDate")%>
                        </div>
                        <asp:HiddenField ID="hfIsRenewed" runat="server" Value='<%# Eval("IsRenewed")%>' />
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="LeaseAgreementPager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upPrint" runat="server">
        <ContentTemplate>
            <div id="divPrint" runat="server" style="display: none">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upNoInformationFound" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divNoInformationFound" runat="server" style="display: none; text-align: center;margin-top :5px">
                
                <asp:Label id="lblNoRecord" runat ="server"   CssClass="error"     Text='<%$Resources:ControlsCommon,NoRecordFound%>'></asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:ControlsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <a href="">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/search.png " OnClick="btnSearch_Click"
                            runat="server" />
                    </a>
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:Image ID="imgAdd" ImageUrl="~/images/expense.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="AddLeaseAgreement"> <h5>
		
	<%--Add Lease Agreement--%>
	</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgView" ImageUrl="~/images/view_expense.png" runat="server" />
                </div>
                </a>
                <div class="name">
                    <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click"  meta:resourcekey="ViewLeaseAgreement"> <h5>
		
	<%--View Lease Agreement--%></h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:Image ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text ='<%$Resources:ControlsCommon,Delete%>'> <h5>
		
	<%--Delete--%></h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" Text ='<%$Resources:ControlsCommon,Print%>'> <h5>
		
	              <%--Print--%></h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" Text ='<%$Resources:ControlsCommon,Email%>'> <h5> 
	            Email</h5></asp:LinkButton>
                </div>
                <%--<div id="divIssueReceipt" runat="server" style="display: none">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div class="name" runat="server">
                        <asp:LinkButton ID="lnkDocumentReceipt" runat="server" OnClick="lnkDocumentReceipt_Click"><h5>
            Receipt</h5>  </asp:LinkButton>
                    </div>
                </div>
                <div id="divRenew" runat="server" style="display: none">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div class="name" runat="server">
                        <asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click">
                    <h5>
                    Renew</h5>  </asp:LinkButton>
                    </div>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
