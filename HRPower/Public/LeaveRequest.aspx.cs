﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Resources;
using System.Globalization;


/*******************************************
    *  Purpose       : Summary description for clsLeaveRequest
    *  Modified By   : Siny
    *  Modified Date : 19 Sep 2013
    *  Modification  : Blocked 'requested to' from front-end,
                       Added vacation in leave type,
                       Enabled edit mode for final approval.
    ********************************************/

public partial class Public_LeaveRequest : System.Web.UI.Page
{
    clsLeaveRequest objRequest;
    clsUserMaster objUser;
    clsEmployee objEmployee;
    clsLeaveExtensionRequest objExtension;
    clsLeaveRequest objclsLeaveRequest;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        LeaveRequestPager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null)
                hfPostedUrl.Value = Request.UrlReferrer.ToString();
            if (Request.QueryString["Requestid"] != null && Request.QueryString["Type"] == "Cancel")
            {
                int iRequestId = 0;
                iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkAdd.OnClientClick = "return false;";
                lnkView.Enabled = false;
                lnkView.OnClientClick = "return false;";
            }
            else if (Request.QueryString["Requestid"] != null)
            {
                int iRequestId = 0;
                iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["Approve"] = true;

                fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkAdd.OnClientClick = "return false;";
                lnkView.Enabled = false;
                lnkView.OnClientClick = "return false;";
            }
            else
            {
                BindDataList();
            }
        }
    }

    protected void fvLeaveRequest_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {//test
            objRequest = new clsLeaveRequest();
            objUser = new clsUserMaster();
            clsMessageDetails objclsMessageDetails;

            int iRequestId = 0;
            TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
            Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");
            DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
            DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
            Label lblLeaveType = (Label)fvLeaveRequest.FindControl("lblLeaveType");
            Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
            Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
            Label lblHalfDay = (Label)fvLeaveRequest.FindControl("lblHalfDay");
            TextBox txtEncashComboEdit = (TextBox)fvLeaveRequest.FindControl("txtEncashComboEdit");
            CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
            Label lblFromComboOff = (Label)fvLeaveRequest.FindControl("lblFromComboOff");
            HiddenField hdLeaveType = null;
            //TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
            //TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");

    
   

            DateTime ts1;
            DateTime ts2;
            TimeSpan ts;

            int iRequestedById;
            string sRequestedTo = string.Empty;
            string[] SuperiorId;
            string[] RequestedEmployes;

            switch (e.CommandName)
            {
                case "Add":
                    if (Convert.ToString(e.CommandArgument) != "")
                    {
                        iRequestId = Convert.ToInt32(e.CommandArgument);
                    }
                    AddUpdateLeaveRequest(iRequestId);
                    break;

                case "CancelRequest":
                    if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                    {
                        ddlLeaveType.SelectedValue = "-1";
                        GetBalanceLeave(null, null);
                    }
                    upForm.Update();
                    upDetails.Update();

                    BindDataList();
                    break;

                case "Approve":
                  


                    hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
                    iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                    string str = string.Empty;
                    TextBox txtEditReason = (TextBox)fvLeaveRequest.FindControl("txtEditReason");
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.ApprovedBy = objUser.GetEmployeeId().ToString();
                    objRequest.EmployeeId = objUser.GetEmployeeId();
                    objRequest.ApprovedById = objUser.GetEmployeeId();
                    objRequest.LeaveType = hdLeaveType.Value.ToInt32();
                   
                    iRequestedById = objRequest.GetRequestedById();
                   
                    sRequestedTo = objRequest.GetRequestedEmployees();
                    int typeID = 0;
                    switch (hdLeaveType.Value.ToInt32())
                    {
                        case 5:
                            typeID = (int)RequestType.CompensatoryOffRequest;
                            break;
                        case -1:
                            typeID = (int)RequestType.VacationLeaveRequest;
                            break;
                        default:
                            typeID = (int)RequestType.Leave;
                            break;
                    }

                    if (clsLeaveRequest.IsHigherAuthority(typeID, objUser.GetEmployeeId(), objRequest.RequestId))
                    {
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                        {
                            TextBox txtNoOfHolidays = (TextBox)fvLeaveRequest.FindControl("txtNoOfHolidays");
                            TextBox txtApprovalFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
                            TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");

                            if (objRequest.CheckSalaryProcessingOver(objRequest.GetRequestedById(), clsCommon.Convert2DateTime(txtApprovalFromDate.Text), clsCommon.Convert2DateTime(txtApprovalToDate.Text)))
                            {
                                if (hdLeaveType.Value.ToInt32() != -1)
                                    //msgs.WarningMessage("Salary is already processed.Cannot approve leave.");
                                    msgs.WarningMessage(GetLocalResourceObject("CannotApproveLeave").ToString());
                                else
                                    // msgs.WarningMessage("Salary is already processed.Cannot approve vacation.");
                                    msgs.WarningMessage(GetLocalResourceObject("CannotApproveVacation").ToString());
                                mpeMessage.Show();
                                return;
                            }
                            if (objRequest.IsPolicyAvailable() == 0)
                            {
                                str = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة كما لا توجد سياسة اترك ") : ("Cannot Approve as no Leave Policy Exists");
                                msgs.WarningMessage(str);
                                //msgs.WarningMessage("Cannot Approve as no Leave Policy Exists");
                                mpeMessage.Show();
                                return;
                            }
                            decimal dNoOfHoliday = 0.0M;
                            decimal iholidays = 0.0M;
                            decimal iNoOfDays = 0.0M;
                            int iStatus = 0;

                            iholidays = Convert.ToDecimal(txtNoOfHolidays.Text);
                            if (lblHalfDay.Text == "No")
                            {
                                ts1 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtApprovalFromDate.Text.Trim()).ToString("MMM dd yyyy"));
                                ts2 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtApprovalToDate.Text.Trim()).ToString("MMM dd yyyy"));
                                ts = ts2.Subtract(ts1);
                                iNoOfDays = ts.Days + 1;
                            }
                            else
                            {
                                iNoOfDays = 0.5M;
                            }

                            if (Convert.ToInt32(txtNoOfHolidays.Text) > 0)
                            {
                                Button btnApprove = (Button)fvLeaveRequest.FindControl("btnApprove");
                                HiddenField hdConfirm = (HiddenField)fvLeaveRequest.FindControl("hdConfirm");

                                if (hdConfirm.Value == "1")
                                {
                                    iStatus = 1;
                                }
                            }

                            if (iStatus == 1)
                            {
                                iNoOfDays = iNoOfDays - iholidays;
                                dNoOfHoliday = iholidays;
                            }
                            //
                            objRequest.CompanyID = objRequest.GetCompanyID();
                            //

                            //EMERGENCY LEAVE TYPE

                            if (LeaveEntryValidations())
                            {
                                objRequest.RequestId = iRequestId;

                                objRequest.EmployeeId = objRequest.GetRequestedById();
                                objRequest.ApprovedById = objUser.GetEmployeeId();

                                objRequest.LeaveType = Convert.ToInt32(hdLeaveType.Value);
                                objRequest.HalfDay = (lblHalfDay.Text == "Yes" ? 1 : 0);
                                objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtApprovalFromDate.Text.Trim());
                                objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtApprovalToDate.Text.Trim());
                                objRequest.NoOfDays = Convert.ToDecimal(iNoOfDays);
                                objRequest.NoOfHolidays = Convert.ToDecimal(dNoOfHoliday);
                                CheckBox chkUnPaid = (CheckBox)fvLeaveRequest.FindControl("chkUnPaid");
                                objRequest.PaidLeave = (chkUnPaid.Checked ? false : true);
                                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                objRequest.Reason = txtEditreason.Text;
                                objRequest.IsCredit = (ViewState["IsCredit"].ToString() == "1" ? true : false);
                                objRequest.EncashComboOffDay = txtEncashComboEdit.Text.ToInt32();
                                objRequest.UpdateLeaveStatus(true);

                                divPrint.Style["display"] = "block";
                                upMenu.Update();
                                clsLeaveExtensionRequest obj = new clsLeaveExtensionRequest();
                                obj.ExtensionFromDate = objRequest.LeaveFromDate;
                                obj.EmployeeId = objRequest.EmployeeId.ToInt32();
                                
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            objRequest.Reason = txtEditreason.Text;

                            objRequest.UpdateLeaveStatus(clsLeaveRequest.IsHigherAuthority(typeID, objUser.GetEmployeeId(), objRequest.RequestId));
                        }

                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                        {
                            //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                            SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId, hdLeaveType.Value.ToInt32());
                            //-----------------------------------------------------------------------------------------------------------
                            if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                msgs.InformationalMessage(GetLocalResourceObject("RequestApprovedSuccessfully").ToString());
                            if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                                msgs.InformationalMessage(GetLocalResourceObject("RequestRejectedSuccessfully").ToString());
                        }
                        else
                        {
                            SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId, hdLeaveType.Value.ToInt32());
                            msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                        }
                    }
                    else
                    {
                        divPrint.Style["display"] = "none";
                        upMenu.Update();
                        objRequest.Reason = txtEditreason.Text;
                        objRequest.UpdateLeaveStatus(false);
                        if (ddlStatus.SelectedValue == "3")
                            msgs.InformationalMessage(GetLocalResourceObject("RequestRejectedSuccessfully").ToString());
                        else
                            msgs.InformationalMessage(GetLocalResourceObject("RequestForwardedSuccessfully").ToString());

                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId, hdLeaveType.Value.ToInt32());
                        //-----------------------------------------------------------------------------------------------------------
                    }

                    ViewState["Approve"] = false;
                    fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(iRequestId);

                    HtmlTableRow trPassportNo = (HtmlTableRow)fvLeaveRequest.FindControl("trPassportNo");
                    HtmlTableRow trPassportExpiryDate = (HtmlTableRow)fvLeaveRequest.FindControl("trPassportExpiryDate");
                    HtmlTableRow trLCExpiry = (HtmlTableRow)fvLeaveRequest.FindControl("trLCExpiry");
                    HtmlTableRow trVisaExpiry = (HtmlTableRow)fvLeaveRequest.FindControl("trVisaExpiry");
                    HtmlTableRow trEligibleLeavesView = (HtmlTableRow)fvLeaveRequest.FindControl("trEligibleLeavesView");
                    if (hdLeaveType.Value.ToInt32() == -1)
                    {
                        trPassportNo.Style["display"] = "table-row";
                        trPassportExpiryDate.Style["display"] = "table-row";
                        trLCExpiry.Style["display"] = "table-row";
                        trVisaExpiry.Style["display"] = "table-row";
                        trEligibleLeavesView.Style["display"] = "none";
                    }
                    lnkDelete.OnClientClick = "return false;";
                    lnkDelete.Enabled = false;

                    //Save attachment documents to tree master and tree details if exists any documents
                    //Save only when approve the request
                    if (ddlStatus.SelectedValue == "5")
                    {
                        if (Session["Documents"] != null)
                        {
                            DataTable dt = (DataTable)Session["Documents"];


                            if (dt != null && dt.Rows.Count > 0)
                            {
                                new clsLeaveRequest().SaveAttachmentsToTreeMaster(Server.MapPath("../"), iRequestId, iRequestedById, dt);
                            }
                        }
                    }
                    mpeMessage.Show();
                    break;

                case "RequestCancel":

                    hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
                    iRequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.Reason = txtEditreason.Text;

                    objRequest.UpdateCancelStatus();
                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId, hdLeaveType.Value.ToInt32());
                    //--------------------------------------------------------------------------------------------------------------------------------------------------                   

                    msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                    ViewState["Cancel"] = false;
                    BindDataList();
                    mpeMessage.Show();

                    break;
                case "Cancel":
                    //HiddenField hfPostedUrl = (HiddenField)fvLeaveRequest.FindControl("hfPostedUrl");
                    if (hfPostedUrl.Value != string.Empty)
                        this.Response.Redirect(hfPostedUrl.Value);
                    else
                        this.Response.Redirect("Home.aspx");
                    break;

                case "RequestForCancel":

                    hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
                    iRequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.EmployeeId = objUser.GetEmployeeId();

                    objRequest.LeaveType = Convert.ToInt32(hdLeaveType.Value);
                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 6)
                    {
                        if (DeleteLeaveEntryValidation())
                        {
                            if (!objRequest.CheckVacationProcessed(iRequestId))
                            {
                                objRequest.LeaveType = Convert.ToInt32(hdLeaveType.Value);
                                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                objRequest.EmployeeId = objUser.GetEmployeeId();
                                objRequest.Reason = txtEditreason.Text;
                                objRequest.UpdateLeaveCancellation();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, typeof(string), "validation", "alert('" + GetLocalResourceObject("VacationProcessedOrConfirmed") + "')", true);
                                return;
                            }
                        }
                        else
                        {
                            return;
                        }
                        if (objRequest.DoesVacationExtensionExist(iRequestId) > 0)
                        {
                            str = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل الإلغاء. المضي قدما أداء إلغاء ملحقات أدلى") : ("Cancellation failed. To proceed further perform cancellation of extensions made");
                            msgs.InformationalMessage(str);
                            mpeMessage.Show();
                            return;
                        }
                    }
                    else
                    {
                        objRequest.Reason = txtEditreason.Text;
                        objRequest.UpdateLeaveCancellation();
                    }

                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 6)
                    {
                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId, hdLeaveType.Value.ToInt32());
                        //-----------------------------------------------------------------------------------------------------------                                                               
                        msgs.InformationalMessage(GetLocalResourceObject("RequestCancelledSuccessfully").ToString());
                    }
                    else
                    {
                        msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                    }

                    ViewState["RequestForCancel"] = false;
                    fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(iRequestId);
                    mpeMessage.Show();

                    break;
            }
        }
        catch { }
    }

    ///<summary>
    ///leave entry validation in leave cancellation case
    ///</summary>
    private bool DeleteLeaveEntryValidation()
    {
        objRequest = new clsLeaveRequest();

        int iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
        objRequest.RequestId = iRequestId;
        int iRequestedById = objRequest.GetRequestedById();

        objRequest.EmployeeId = iRequestedById;
        Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim());
        return true;
    }

    /// <summary>
    /// leave entry validations in leave approval case
    /// </summary>
    /// <returns></returns>
    private bool LeaveEntryValidations()
    {
        objRequest = new clsLeaveRequest();
        DateTime ts1, ts2;
        TimeSpan ts;
        int NoOfDays = 0, HolidayCount;
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        TextBox txtNoOfHolidays = (TextBox)fvLeaveRequest.FindControl("txtNoOfHolidays");

        Label lblHalfDay = (Label)fvLeaveRequest.FindControl("lblHalfDay");
        Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
        Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
        TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
        TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
        HiddenField hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
        DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
        HiddenField hdConfirm = (HiddenField)fvLeaveRequest.FindControl("hdConfirm");
        TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
        TextBox txtBalanceCombo = (TextBox)fvLeaveRequest.FindControl("txtBalanceCombo");
        TextBox txtEncashCombo = (TextBox)fvLeaveRequest.FindControl("txtEncashCombo");
        TextBox txtEncashComboEdit = (TextBox)fvLeaveRequest.FindControl("txtEncashComboEdit");
        TextBox txtBalanceComboEdit = (TextBox)fvLeaveRequest.FindControl("txtBalanceComboEdit");
        Label lblFromComboOff = (Label)fvLeaveRequest.FindControl("lblFromComboOff");

        int iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
        objRequest.RequestId = iRequestId;
        int iRequestedById = objRequest.GetRequestedById();

        objRequest.EmployeeId = iRequestedById;
        objRequest.LeaveType = Convert.ToInt32(hdLeaveType.Value);
        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtFromDate.Text.Trim());
        objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtToDate.Text.Trim());
        objRequest.HalfDay = (lblHalfDay.Text == "Yes" ? 1 : 0);
        decimal DayDiff = 0.0M;
        if (objRequest.HalfDay == 1)
            DayDiff = 0.5M;
        else
        {
            ts1 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtFromDate.Text.Trim()).ToString("MMM dd yyyy"));
            ts2 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtToDate.Text.Trim()).ToString("MMM dd yyyy"));
            ts = ts2.Subtract(ts1);

            HolidayCount = txtNoOfHolidays.Text.ToInt32();
            NoOfDays = ts.Days + 1;
            if (hdConfirm.Value == "1")
                NoOfDays = NoOfDays - HolidayCount;
        }

        if (hdLeaveType.Value.ToInt32() == 6)
        {
            int NoOFAvailableDates = clsLeaveRequest.GetApplicableEmergencyLeave(iRequestedById, clsCommon.Convert2DateTime(txtFromDate.Text.Trim()).ToString("MMM dd yyyy"), clsCommon.Convert2DateTime(txtToDate.Text.Trim()).ToString("MMM dd yyyy"));
            if (NoOfDays > NoOFAvailableDates)
            {
                msgs.InformationalMessage("Please check the balance leave (Available balance leave :" + NoOFAvailableDates + ")");
                mpeMessage.Show();
                return false;
            }
            return true;
        }

        if (hdLeaveType.Value.ToInt32() != -1)
        {
            if ((txtBalanceLeave.Text.ToDecimal() <= 0 || ((txtBalanceLeave.Text.ToDecimal() - NoOfDays.ToDecimal()) < 0)) && (hdLeaveType.Value.ToInt32() != 5))
            {
                msgs.InformationalMessage(GetLocalResourceObject("CheckLeaveBalance").ToString());
                mpeMessage.Show();
                return false;
            }
            //If not maternity leave
            if (hdLeaveType.Value.ToInt32() != 3 && (hdLeaveType.Value.ToInt32() != 5) && objRequest.CheckMonthlyLeaveBalance())
            {
                msgs.InformationalMessage(GetLocalResourceObject("CheckMonthlyLeaveBalance").ToString());
                mpeMessage.Show();
                return false;
            }
            clsLeaveExtensionRequest obj = new clsLeaveExtensionRequest();
            obj.ExtensionFromDate = objRequest.LeaveFromDate;
            obj.EmployeeId = objRequest.EmployeeId.ToInt32();
            bool val = true;
            string str = string.Empty;
            if (ViewState["IsCredit"] != null)
            {
                if (lblFromComboOff != null)
                {
                    if ((ViewState["IsCredit"].ToInt32() != 1) && (hdLeaveType.Value.ToInt32() == 5) && (lblFromComboOff.Text != "No"))
                    {
                        if (obj.GetCompensatoryOffCount() <= 0)
                        {
                            str = clsUserMaster.GetCulture() == "ar-AE" ? (" إجازة التوازن هو " + obj.GetCompensatoryOffCount()) : ("Balance leave is " + obj.GetCompensatoryOffCount());
                            msgs.InformationalMessage(str);
                            //msgs.InformationalMessage("Balance leave is " + obj.GetCompensatoryOffCount());
                            mpeMessage.Show();
                            val = false;
                            return false;
                        }
                        if (val)
                        {
                            int iRequestedno = objRequest.GetLeaveRequestedCount();
                            int icompCount = obj.GetCompensatoryOffCount();
                            int diff = icompCount - iRequestedno;
                            if (diff < 0)
                            {
                                str = clsUserMaster.GetCulture() == "ar-AE" ? ("إجازة طلب أكبر من التوازن  ") : ("Requested leave is greater than available balance");
                                msgs.InformationalMessage(str);
                                mpeMessage.Show();
                                val = false;
                                return val;
                            }
                        }
                        if (!val)
                        {
                            if (objRequest.GetLeaveRequestedCount() > obj.GetCompensatoryOffCount())
                            {
                                str = clsUserMaster.GetCulture() == "ar-AE" ? ("إجازة طلب أكبر من التوازن ") : ("Requested leave is greater than balance");
                                msgs.InformationalMessage(str);
                                mpeMessage.Show();
                                return false;
                            }
                        }
                    }
                }
            }
        }

        if (hdLeaveType.Value.ToInt32() == -1)
        {
            if (Convert.ToBoolean(ViewState["Approve"]) == false)
            {
                if (txtEncashCombo.Text.ToInt32() > txtBalanceCombo.Text.ToInt32())
                {
                    string str = clsUserMaster.GetCulture() == "ar-AE" ? ("إجازة طلب أكبر من التوازن ") : ("Requested Encash days is greater than balance");
                    msgs.InformationalMessage(str);
                    mpeMessage.Show();
                    return false;
                }
            }
            else
            {
                if (txtEncashComboEdit.Text.ToInt32() > txtBalanceComboEdit.Text.ToInt32())
                {
                    string str = clsUserMaster.GetCulture() == "ar-AE" ? ("إجازة طلب أكبر من التوازن ") : ("Requested Encash days is greater than balance");
                    msgs.InformationalMessage(str);
                    mpeMessage.Show();
                    return false;
                }
            }
        }
        //if (objRequest.CheckSalaryProcessingOver())
        //{
        //    msgs.InformationalMessage("Cannot assign Leave in this date.This employee's processing over.");
        //    mpeMessage.Show();
        //    return false;
        //}
        if (objRequest.GetEmployeeWorkStatus() < 6)
        {
            msgs.InformationalMessage(GetLocalResourceObject("EmployeeNotInService").ToString());
            mpeMessage.Show();
            return false;
        }

        if (objRequest.CheckLeaveEntryExists() > 0)
        {
            //msgs.InformationalMessage("Sorry, leave has already been taken on the selected date.");
            msgs.InformationalMessage(GetLocalResourceObject("LeaveTakenSelectedDate").ToString());
            mpeMessage.Show();
            return false;
        }

        if (lblHalfDay.Text == "No")
        {
            if (objRequest.CheckAttendanceExists())
            {
                //msgs.InformationalMessage("Cannot assign Leave in this date.This employee's attendence exists");
                msgs.InformationalMessage(GetLocalResourceObject("LeaveTakenSelectedDate").ToString());
                mpeMessage.Show();
                return false;
            }
        }
        ///<summary>
        ///need to complete worksheetexists,....
        ///need to complete the selected day is a holiday ,,, from holiday table and day reference
        ///</summary>
        return true;
    }

    private void CheckEmployeeDOJ(int iEmployeeId)
    {
        objRequest = new clsLeaveRequest();

        SqlDataReader sdrEmpCon;
        int iTransfer = 0;
        DateTime dDOJ1;
        DateTime dDOJ;

        sdrEmpCon = objRequest.GetEmployeeIDInTransfer();
        if (sdrEmpCon.Read())
        {
            iTransfer = 1;
        }
        sdrEmpCon.Close();

        if (iTransfer == 1)
        {
            sdrEmpCon = objRequest.GetTransferDate();
            if (sdrEmpCon.Read())
            {
                dDOJ = Convert.ToDateTime(sdrEmpCon["DateofJoining"]);
            }
            sdrEmpCon.Close();
            //  dDOJ1 = Month(dDOJ) & "/" & "01" & "/" & Year(dDOJ);
        }
    }

    /// <summary>
    /// bind datalist with requests.
    /// </summary>
    private void BindDataList()
    {
        objRequest = new clsLeaveRequest();
        objUser = new clsUserMaster();

        objRequest.Mode = "VIEW";
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.PageIndex = LeaveRequestPager.CurrentPage + 1;
        objRequest.PageSize = LeaveRequestPager.PageSize;

        DataTable oTable = objRequest.GelAllRequests();
        if (oTable.DefaultView.Count > 0)
        {
            lnkView.Enabled = lnkDelete.Enabled = true;
            lnkDelete.OnClientClick = "return true;";
            dlLeaveRequest.DataSource = oTable;
            dlLeaveRequest.DataBind();

            LeaveRequestPager.Total = objRequest.GetRecordCount(objUser.GetEmployeeId());
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLeaveRequest.ClientID + "');";

            lnkDelete.Enabled = true;
            fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
            fvLeaveRequest.DataSource = null;
            fvLeaveRequest.DataBind();

            LeaveRequestPager.Visible = true;
        }
        else
        {
            lnkView.Enabled = lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
            dlLeaveRequest.DataSource = null;
            dlLeaveRequest.DataBind();

            fvLeaveRequest.ChangeMode(FormViewMode.Insert);
            LeaveRequestPager.Visible = false;
        }
        lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLeaveRequest.ClientID + "');";
        lnkDelete.Enabled = true;
        
        upMenu.Update();
    }
    /// <summary>
    /// update or insert leave request.
    /// </summary>
    /// <param name="iRequestId"></param>
    private void AddUpdateLeaveRequest(int iRequestId)
    {
        try
        {
            objUser = new clsUserMaster();
            objRequest = new clsLeaveRequest();
            bool bIsInsert = true;

            int days = 0;
            DateTime date;
            DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
            RadioButtonList rblHalfDay = (RadioButtonList)fvLeaveRequest.FindControl("rblhalfDay");
            RadioButtonList rblCreditLeave = (RadioButtonList)fvLeaveRequest.FindControl("rblCreditLeave");
            TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");
            TextBox txtLeaveFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
            TextBox txtLeaveTodate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
            TextBox txtReason = (TextBox)fvLeaveRequest.FindControl("txtReason");
            DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlStatus");
            HiddenField hfConfirm = (HiddenField)fvLeaveRequest.FindControl("hfConfirm");
            TextBox txtContactAddress = (TextBox)fvLeaveRequest.FindControl("txtContactAddress");
            TextBox txtTelephoneNo = (TextBox)fvLeaveRequest.FindControl("txtTelephoneNo");
            TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
            CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
            Controls_AttachDocument AttachDocument = (Controls_AttachDocument)fvLeaveRequest.FindControl("AttachDocument1");
            TextBox txtEncashCombo = (TextBox)fvLeaveRequest.FindControl("txtEncashCombo");
            //CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash");

            int intEmployeeID = objUser.GetEmployeeId();
            objRequest.HalfDay = Convert.ToInt32(rblHalfDay.SelectedItem.Value);
            if (objRequest.CheckSalaryProcessingOver(intEmployeeID, clsCommon.Convert2DateTime(txtLeaveFromDate.Text), clsCommon.Convert2DateTime(txtLeaveTodate.Text)))
            {
                if (ddlLeaveType.SelectedValue.ToInt32() != -1)
                    //msgs.WarningMessage("Salary is already released.Cannot request leave.");
                    msgs.WarningMessage(GetLocalResourceObject("CannotApproveLeave").ToString());
                else
                    //msgs.WarningMessage("Salary is already released.Cannot request vacation.");
                    msgs.WarningMessage(GetLocalResourceObject("CannotApproveVacation").ToString());
                mpeMessage.Show();
                return;
            }
            if (hfConfirm.Value.ToInt32() != 0 || hfConfirm.Value == "")
            {
                //EMERGENCY LEAVE TYPE
                if (ddlLeaveType.SelectedValue.ToInt32() == 6)
                {
                    if (!(clsLeaveRequest.CanRequestEmergencyLeave(intEmployeeID, clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim()))))
                    {
                        msgs.WarningMessage(GetLocalResourceObject("emergencyleavemsg").ToString());
                        mpeMessage.Show();
                        return;
                    }
                }
                objRequest.EmployeeId = intEmployeeID;
            
                objRequest.RequestType = (ddlLeaveType.SelectedValue == "-1") ? (int)RequestType.VacationLeaveRequest : (int)RequestType.Leave;

                if (ddlLeaveType.SelectedValue.ToInt32() == 1) ///Validation For Sick Leave
                {
                    TimeSpan t = clsCommon.Convert2DateTime(txtLeaveTodate.Text) - clsCommon.Convert2DateTime(txtLeaveFromDate.Text);
                    double nod = t.TotalDays + 1;
                    //if (nod > txtBalanceLeave.Text.ToInt32())
                    //{
                    //    string msg = "Please Check Balance Leave";
                    //    msgs.InformationalMessage(msg);
                    //    mpeMessage.Show();
                    //    return;
                    //}

                    if (!(clsCommon.Convert2DateTime(txtLeaveFromDate.Text) <= clsCommon.Convert2DateTime(clsUserMaster.GetcurrentDate())) || (clsCommon.Convert2DateTime(txtLeaveTodate.Text) > clsCommon.Convert2DateTime(clsUserMaster.GetcurrentDate())))
                    {
                            string msg = "Sick leave cannot be applied for future days.";
                            msgs.InformationalMessage(msg);
                            mpeMessage.Show();
                            return;
                    }
                    DataList dlAttachedDocuments = (DataList)AttachDocument.FindControl("dlAttachedDocuments");
                    if (dlAttachedDocuments.Items.Count == 0 && nod > 1 )
                    {
                        string Message = "Please add attachments";
                        msgs.InformationalMessage(Message);
                        mpeMessage.Show();
                        return;
                    }

                }

                if (ddlLeaveType.SelectedValue.ToInt32() == 5)
                {
                    objRequest.RequestType = (int)RequestType.CompensatoryOffRequest;
                }

                string sRequestedTo = "";
                sRequestedTo = clsLeaveRequest.GetRequestedTo(iRequestId, intEmployeeID, objRequest.RequestType);
                if (sRequestedTo == "")
                {
                    // msgs.WarningMessage("Your request cannot process this time. Please contact HR department.");
                    msgs.WarningMessage(GetLocalResourceObject("ContactHR").ToString());
                    mpeMessage.Show();
                    return;
                }

                int iHalfDay = Convert.ToInt32(rblHalfDay.SelectedItem.Value);
                
                if (iRequestId > 0)
                {
                    objRequest.Mode = "UPD";
                    objRequest.RequestId = iRequestId;
                    bIsInsert = false;
                }
                else
                {
                    objRequest.Mode = "INS";
                    bIsInsert = true;
                }

                objRequest.EmployeeId = intEmployeeID;
                objRequest.HalfDay = iHalfDay;
                objRequest.LeaveType = Convert.ToInt32(ddlLeaveType.SelectedValue);

                Boolean isVal = true;

                if (IsLeaveAlreadyExists(objRequest.RequestId, intEmployeeID))
                    return;
                if (clsLeaveRequest.GetRequestDetails(intEmployeeID, clsCommon.Convert2DateTime(txtLeaveFromDate.Text).ToString("dd-MMM-yyyy"), clsCommon.Convert2DateTime(txtLeaveTodate.Text).ToString("dd-MMM-yyyy"),objRequest.LeaveType.ToInt32()))
                {
                    string msg = "Request details exists for the dates.";
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;
                }

                if ((rblCreditLeave.SelectedValue == "1" || rblCreditLeave.SelectedValue == "0") && ddlLeaveType.SelectedValue == "5")
                {
                    if ((!chkCO.Checked && rblCreditLeave.SelectedValue == "0") || rblCreditLeave.SelectedValue == "1")
                    {
                        for (int j = 0; j < dlCODays.Items.Count; j++)
                        {
                            TextBox txtCODate = (TextBox)dlCODays.Items[j].FindControl("txtCODate");
                            RequiredFieldValidator rfvtxtCODate = (RequiredFieldValidator)dlCODays.Items[j].FindControl("rfvtxtCODate");
                            rfvtxtCODate.ValidationGroup = "AddnewP";

                            if (txtCODate.Text == "")
                            {
                                isVal = false;
                                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال تواريخ عمل") : ("Please enter worked dates");
                                msgs.InformationalMessage(msg);
                                mpeMessage.Show();
                                return;
                            }

                            if ((Convert.ToDateTime(txtCODate.Text, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }) >= Convert.ToDateTime(txtLeaveFromDate.Text, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }) || Convert.ToDateTime(txtCODate.Text, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" }) >= Convert.ToDateTime(txtLeaveTodate.Text, new DateTimeFormatInfo() { ShortDatePattern = "dd/MM/yyyy" })) && rblCreditLeave.SelectedValue == "0")
                            {
                                isVal = false;
                                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("أيام تعويضية أوف لا ينبغي أن يكون أكبر من ترك من تاريخ ") : ("Compensatory-Off days should not be greater than Leave From date ");
                                msgs.InformationalMessage(msg);
                                mpeMessage.Show();
                                return;
                            }
                            for (int i = j + 1; i < dlCODays.Items.Count; i++)
                            {
                                TextBox txtCODate2 = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                                if (txtCODate.Text == txtCODate2.Text)
                                {
                                    isVal = false;
                                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يسمح تواريخ مكررة") : ("Duplicate Dates are not allowed");
                                    msgs.InformationalMessage(msg);
                                    mpeMessage.Show();
                                    return;// break;
                                }
                            }

                        }
                    }
                    if (rblCreditLeave.SelectedValue == "1")
                        objRequest.IsCredit = true;
                    else
                    {
                        objRequest.IsCredit = false;
                        TimeSpan t = clsCommon.Convert2DateTime(txtLeaveTodate.Text) - clsCommon.Convert2DateTime(txtLeaveFromDate.Text);
                        double nod = t.TotalDays + 1;
                        if (!chkCO.Checked)
                        {
                            if (nod <= dlCODays.Items.Count)
                            { }
                            else
                            {
                                isVal = false;
                                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("عدد الأيام المطلوبة أكبر من أيام العمل ") : ("Number of requested days is greater than worked days");
                                msgs.InformationalMessage(msg);
                                mpeMessage.Show();
                                return;
                            }
                        }
                        else
                        {
                            if (txtBalanceLeave.Text.ToInt32() < nod)
                            {
                                isVal = false;
                                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حفظ الطلب كما عدد الأوراق المطلوبة أكبر من الأوراق الفضل ") : ("Cannot save request as No. of requested leaves is greater than credited leaves");
                                msgs.InformationalMessage(msg);
                                mpeMessage.Show();
                                return;
                            }
                        }
                    }
                }
                objRequest.IsFromComboOff = chkCO.Checked;
                //------------------------Travel------------------------------------------------------------------
                //objRequest.IsFlightRequired = rblFlightRequired.SelectedValue.ToInt32() == 1 ? true : false;
                //objRequest.IsSectorRequired = rblSectorRequired.SelectedValue.ToInt32() == 1 ? true : false;
                objRequest.ContactAddress = txtContactAddress.Text.Trim();
                objRequest.TelephoneNo = txtTelephoneNo.Text.Trim();
                //------------------------------------------------------------------------------------------------
                objRequest.Reason = txtReason.Text;
                objRequest.EncashComboOffDay = txtEncashCombo.Text.ToInt32();
                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                objRequest.RequestedTo = sRequestedTo;

                if (rblCreditLeave.SelectedValue == "1" && Convert.ToInt32(ddlLeaveType.SelectedValue) == 5)
                {
                    txtLeaveTodate.Text = "1/1/1900";
                    objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtLeaveTodate.Text.Trim());
                    txtLeaveFromDate.Text = "1/1/1900";
                    objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim());
                }
                else
                {

                    //if (chkEncash.Checked)                                                       
                    //{
                    //    txtLeaveTodate.Enabled = false;                                      

                    //    objRequest.IsEncash = true;
                    //}
                    //else
                    //{
                        txtLeaveTodate.Enabled = true;
                        objRequest.IsEncash = false;                                      
                    //}




                    if (txtLeaveTodate.Enabled)
                    {
                        objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtLeaveTodate.Text.Trim());
                        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim());
                    }
                    else
                    {
                        // objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim());

                        days = Convert.ToInt32(lblVacationDays.Text);
                        //DateTime todate = Convert.ToDateTime(txtLeaveTodate.Text.Trim());
                        //DateTime answer = todate.AddDays(days);
                        objRequest.LeaveToDate = (clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim())).AddDays(days - 1);
                        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim());
                        objRequest.NumberOfDays = days;

                    }
                }
                //Cannot request for a vacation unless existing request is approved
                if (ddlLeaveType.SelectedValue.ToInt32() == -1 && objRequest.IsVacationProcessing())
                {
                    // msgs.InformationalMessage("Approved vacation not confirmed.Please contact HR department.");
                    msgs.InformationalMessage(GetLocalResourceObject("VacationNotConfirmedContactHR").ToString());
                    mpeMessage.Show();
                    return;
                }

                if (isVal)
                {
                    iRequestId = objRequest.InsertUpdateLeaveRequest();
                    ViewState["ID"] = iRequestId;
                    if (iRequestId > 0)
                    {
                        if (ddlLeaveType.SelectedValue == "5" && !chkCO.Checked)
                        {
                            objRequest.RequestId = iRequestId;
                            if (!bIsInsert)
                            {
                                objRequest.DeleteCompensatoryDetails();
                            }
                            foreach (DataListItem item in dlCODays.Items)
                            {
                                TextBox txtCODate = (TextBox)item.FindControl("txtCODate");
                                objRequest.WorkedDay = clsCommon.Convert2DateTime(txtCODate.Text.Trim()).ToString("dd MMM yyyy"); //txtCODate.Text;

                                objRequest.InsertCompensatoryDetails();
                            }
                        }
                    }
                    if (!bIsInsert)
                    {

                        if (AttachDocument != null)
                        {
                            AttachDocument.SaveAttachedDocuments(iRequestId);
                        }
                        msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                        //-------------------------------------------- Message insertion into HRMessages--------------------------------------------------
                        SendMessage((int)RequestStatus.Applied, iRequestId, objRequest.LeaveType.ToInt32());
                        //--------------------------------------------------------------------------------------------------------------------------------
                    }
                    else
                    {
                        //Save the attached documents

                        if (AttachDocument != null)
                        {
                            AttachDocument.SaveAttachedDocuments(iRequestId);
                        }
                        msgs.InformationalMessage(GetLocalResourceObject("RequestAddedSuccessfully").ToString());
                        //-------------------------------------------- Message insertion into HRMessages--------------------------------------------------
                        SendMessage((int)RequestStatus.Applied, iRequestId, objRequest.LeaveType.ToInt32());
                        //---------------------------------------------------------------------------------------------------------------------------------  
                    }
                    upForm.Update();
                    upDetails.Update();
                    BindDataList();
                    fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                    fvLeaveRequest.DataSource = null;
                    fvLeaveRequest.DataBind();
                }
                mpeMessage.Show();
            }
            else
            {
                return;
            }
        }
        catch { }
    }

    private bool IsLeaveAlreadyExists(int RequestID, int EmployeeID)
    {
        if (dlCODays.Items.Count >= 1)
        {
            for (int i = 0; i < dlCODays.Items.Count; i++)
            {
                TextBox txtCODate1 = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                DateTime date;
                date = clsCommon.Convert2DateTime(txtCODate1.Text.Trim());
                int EID = objUser.GetEmployeeId().ToInt32();
                for (int j = i + 1; j < dlCODays.Items.Count; j++)
                {
                    TextBox txtCODate2 = (TextBox)dlCODays.Items[j].FindControl("txtCODate");
                    if (txtCODate1.Text == txtCODate2.Text)
                    {
                        string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يسمح تواريخ مكررة") : ("Duplicate Dates are not allowed");
                        msgs.InformationalMessage(msg);
                        mpeMessage.Show();
                        break;
                    }
                }

                if (objRequest.CheckComboExists(date, EmployeeID, ViewState["RequestId"].ToInt32()) > 0)
                {
                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تاريخ دخول") : ("Dates Exist");
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    return true;
                }
                ViewState["Table"] = dlCODays.DataSource;
            }
        }
        return false;
    }

    /// <summary>
    /// check Request FromDate is a holiday or a offday
    /// </summary>
    /// <returns></returns>
    private bool LeaveRequestValidations()
    {
        objRequest = new clsLeaveRequest();
        objUser = new clsUserMaster();

        TextBox txtLeaveFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");

        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim());

        if (objRequest.CheckCompanyHoliday())
        {
            //msgs.InformationalMessage("The selected LeaveFromdate is a holiday.");
            msgs.InformationalMessage(GetLocalResourceObject("SelectedLeaveHoliday").ToString());
            mpeMessage.Show();
            return false;
        }
        DataTable dt = objRequest.GetOffDayIds();
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int DayID = 0, CurDayID = 0, CurToDayID = 0;
                int Curday;

                DayID = Convert.ToInt32(dt.Rows[i]["DayID"]);
                Curday = GetDay(Convert.ToDateTime(clsCommon.Convert2DateTime(txtLeaveFromDate.Text.Trim()).ToString("MMM dd yyyy")).DayOfWeek.ToString());
                CurDayID = Curday + 1;
                if (CurDayID > 7)
                {
                    CurDayID = 1;
                }
                if (DayID == CurDayID)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return true;
    }

    private int GetDay(string Day)
    {
        int Curday = 0;
        switch (Day)
        {
            case "Sunday":
                Curday = 0;
                break;
            case "Monday":
                Curday = 1;
                break;
            case "Tuesday":
                Curday = 2;
                break;
            case "Wednesday":
                Curday = 3;
                break;
            case "Thursday":
                Curday = 4;
                break;
            case "Friday":
                Curday = 5;
                break;
            case "Saturday":
                Curday = 6;
                break;
        }
        return Curday;
    }

    protected void fvLeaveRequest_DataBound(object sender, EventArgs e)
    {
        try
        {
            objUser = new clsUserMaster();
            objRequest = new clsLeaveRequest();

            if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
            {
                HiddenField hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
                HtmlTableRow trTravelDocument = (HtmlTableRow)fvLeaveRequest.FindControl("trTravelDocument");
                HtmlTableRow trEncashView = (HtmlTableRow)fvLeaveRequest.FindControl("trEncashView");
                HtmlTableRow trHalfDayView = (HtmlTableRow)fvLeaveRequest.FindControl("trHalfDayView");
                GridView gvPreviousLoanDetails = (GridView)fvLeaveRequest.FindControl("gvPreviousLoanDetails");
                Label lblEncashEdit = (Label)fvLeaveRequest.FindControl("lblEncashEdit");
                HtmlTableRow trEligibleLeavesView = (HtmlTableRow)fvLeaveRequest.FindControl("trEligibleLeavesView");
                //CheckBox chkEncash1 = (CheckBox)fvLeaveRequest.FindControl("chkEncash1");
                //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
                if (gvPreviousLoanDetails != null)
                    gvPreviousLoanDetails.Visible = false;

                if (hdLeaveType != null)
                {
                    if (hdLeaveType.Value.ToInt32() == -1)
                    {
                        trEligibleLeavesView.Style["display"] = "none";
                        trTravelDocument.Style["display"] = "table-row";
                        trEncashView.Style["display"] = "table-row";
                        lblEncashEdit.Visible = true;
                        CalculateTotalNoofVacation(null, null);
                    }
                    else
                    {
                        trTravelDocument.Style["display"] = "none";
                        trEncashView.Style["display"] = "none";
                        lblEncashEdit.Visible = false;
                    }

                    if (hdLeaveType.Value.ToInt32() == -1 || hdLeaveType.Value.ToInt32() == 5 || hdLeaveType.Value.ToInt32() == 6)
                        trHalfDayView.Style["display"] = "none";
                    else
                        trHalfDayView.Style["display"] = "table-row";                   
                }               
                   // if (imgBack != null)
                ibtnBack.Visible = false;//imgBack.Visible = 
               
                upForm.Update();
            }

            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            {
                RadioButtonList rblHalfDay = (RadioButtonList)fvLeaveRequest.FindControl("rblhalfDay");
                rblHalfDay.Items[1].Selected = true;
            }

            if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == false)
            {
                HtmlTableRow trForwardedBy = (HtmlTableRow)fvLeaveRequest.FindControl("trForwardedBy");
                if (trForwardedBy != null)
                    trForwardedBy.Visible = false;
            }

            if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == true)
            {
                DropDownList ddlEditStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
                Button btnApprove = (Button)fvLeaveRequest.FindControl("btnApprove");
                HtmlTableRow trUnpaid = (HtmlTableRow)fvLeaveRequest.FindControl("trUnpaid");
                HtmlTableRow trPassportNo = (HtmlTableRow)fvLeaveRequest.FindControl("trPassportNo");
                HtmlTableRow trPassportExpiryDate = (HtmlTableRow)fvLeaveRequest.FindControl("trPassportExpiryDate");
                HtmlTableRow trLCExpiry = (HtmlTableRow)fvLeaveRequest.FindControl("trLCExpiry");
                HtmlTableRow trVisaExpiry = (HtmlTableRow)fvLeaveRequest.FindControl("trVisaExpiry");
                TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
                Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");
                Label lblEncashEdit = (Label)fvLeaveRequest.FindControl("lblEncashEdit");
                objRequest.EmployeeId = objUser.GetEmployeeId();
                HiddenField hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
                TextBox txtEncashComboEdit = (TextBox)fvLeaveRequest.FindControl("txtEncashComboEdit");
                TextBox txtBalanceComboEdit = (TextBox)fvLeaveRequest.FindControl("txtBalanceComboEdit");

                if (hdLeaveType.Value.ToInt32() == -1)
                {
                    lblEncashEdit.Visible = false;
                    txtEncashComboEdit.Visible = true;
                }
                else
                {
                    lblEncashEdit.Visible = false;
                    txtEncashComboEdit.Visible = false;
                }

                int typeID = 0;
                switch (hdLeaveType.Value.ToInt32())
                {
                    case 5:
                        typeID = (int)RequestType.CompensatoryOffRequest;
                        break;
                    case -1:
                        typeID = (int)RequestType.VacationLeaveRequest;
                        break;
                    default:
                        typeID = (int)RequestType.Leave;
                        break;
                }
                if (clsLeaveRequest.IsHigherAuthority(typeID, objUser.GetEmployeeId(), fvLeaveRequest.DataKey["RequestId"].ToInt32()))
                {
                    DateTime ts1, ts2;
                    TimeSpan ts;

                    TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
                    TextBox txtNoOfHolidays = (TextBox)fvLeaveRequest.FindControl("txtNoOfHolidays");
                    Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
                    Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
                    HtmlGenericControl divApprovalFromDate = (HtmlGenericControl)fvLeaveRequest.FindControl("divApprovalFromDate");
                    HtmlGenericControl divApprovalToDate = (HtmlGenericControl)fvLeaveRequest.FindControl("divApprovalToDate");
                    TextBox txtApprovalFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
                    TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
                    HtmlTableRow trComboOff = (HtmlTableRow)fvLeaveRequest.FindControl("trComboOff");
                    HtmlTableRow trCount = (HtmlTableRow)fvLeaveRequest.FindControl("trCount");
                    GridView gvPreviousLoanDetails = (GridView)fvLeaveRequest.FindControl("gvPreviousLoanDetails");

                    if (ddlEditStatus != null)
                    {
                        DataTable dt = objRequest.GetAllRequestStatus(1);
                        ddlEditStatus.DataSource = dt;
                        ddlEditStatus.DataBind();
                    }
                    objRequest.StatusId = Convert.ToInt32(ddlEditStatus.SelectedValue);
                    btnApprove.ValidationGroup = "Submit";
                    divApprovalFromDate.Attributes.Add("style", "display:table-row");
                    divApprovalToDate.Attributes.Add("style", "display:table-row");

                    lblFromPeriod.Attributes.Add("style", "display:none");
                    lblToPeriod.Attributes.Add("style", "display:none");

                    if (hdLeaveType.Value.ToInt32() == -1)
                    {
                        trUnpaid.Attributes.Add("style", "display:none");
                        trPassportNo.Style["display"] = "table-row";
                        trPassportExpiryDate.Style["display"] = "table-row";
                        trLCExpiry.Style["display"] = "table-row";
                        trVisaExpiry.Style["display"] = "table-row";
                        gvPreviousLoanDetails.Visible = true;
                        CalculateTotalNoofVacation(null, null);
                    }
                    else
                    {
                        if (hdLeaveType.Value.ToInt32() == 6)
                            trUnpaid.Attributes.Add("style", "display:none");
                        else
                            trUnpaid.Attributes.Add("style", "display:table-row");
                        trPassportNo.Style["display"] = "none";
                        trPassportExpiryDate.Style["display"] = "none";
                        trLCExpiry.Style["display"] = "none";
                        trVisaExpiry.Style["display"] = "none";
                        gvPreviousLoanDetails.Visible = false;
                    }

                    if (lblFromPeriod != null && txtApprovalFromDate != null && lblFromPeriod.Text != "")
                    {
                        txtApprovalFromDate.Text = lblFromPeriod.Text.ToDateTime().ToString("dd/MM/yyyy");
                        lblFromPeriod.Text = lblFromPeriod.Text.ToDateTime().ToString("dd/MM/yyyy");
                    }
                    if (lblToPeriod != null && txtApprovalToDate != null && lblToPeriod.Text != "")
                    {
                        txtApprovalToDate.Text = lblToPeriod.Text.ToDateTime().ToString("dd/MM/yyyy");
                        lblToPeriod.Text = lblToPeriod.Text.ToDateTime().ToString("dd/MM/yyyy");
                    }

                    if (txtNoOfHolidays == null) return;
                    ts1 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                    ts2 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblToPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                    ts = ts2.Subtract(ts1);

                    int idays = ts.Days + 1;
                    objRequest.RequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                    objRequest.LeaveFromDate = clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim());
                    objRequest.LeaveToDate = clsCommon.Convert2DateTime(lblToPeriod.Text.Trim());

                    int iRequestedById = objRequest.GetRequestedById();
                    objRequest.EmployeeId = iRequestedById;

                    int iTotalHolidays = objRequest.GetTotalHolidays();
                    txtNoOfHolidays.Text = Convert.ToString(iTotalHolidays);

                    objUser = new clsUserMaster();

                    objRequest.Mode = "GLC";

                    objRequest.LeaveType = Convert.ToInt32(hdLeaveType.Value);

                    if (hdLeaveType.Value == "5")
                    {
                        trUnpaid.Style["display"] = "none";
                        trComboOff.Style["display"] = "table-row";
                        trCount.Style["display"] = "table-row";
                    }
                    else
                    {
                        objRequest.month = clsCommon.Convert2DateTime(lblFromPeriod.Text).Month;
                        objRequest.year = clsCommon.Convert2DateTime(lblFromPeriod.Text).Year;
                        trComboOff.Style["display"] = "none";
                        trCount.Style["display"] = "none";

                        double iLeaveCount = objRequest.GetLeaveCount();
                        txtBalanceLeave.Text = (iLeaveCount).ToString();

                        decimal iNoOfDays = 0.0M;
                        Label lblHalfDay = (Label)fvLeaveRequest.FindControl("lblHalfDay");
                        if (lblHalfDay.Text == "No")
                        {
                            ts1 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                            ts2 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblToPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                            ts = ts2.Subtract(ts1);

                            iNoOfDays = ts.Days + 1;
                        }
                        else
                        {
                            iNoOfDays = 0.5M;
                        }
                        if (Convert.ToInt32(txtNoOfHolidays.Text) > 0)
                        {
                            btnApprove.OnClientClick = "return confirmSaveLeaveEntry('" + btnApprove.ClientID + "');";
                        }
                    }
                }
                else
                {
                    Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
                    Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
                    TextBox txtNoOfHolidays = (TextBox)fvLeaveRequest.FindControl("txtNoOfHolidays");
                    if (ddlEditStatus != null)
                    {
                        DataTable dt = objRequest.GetAllRequestStatus(1);
                        dt.DefaultView.RowFilter = "StatusID<>4";
                        ddlEditStatus.DataSource = dt.DefaultView.ToTable();
                        ddlEditStatus.DataBind();
                    }
                    objRequest.StatusId = Convert.ToInt32(ddlEditStatus.SelectedValue);

                    objRequest.RequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                    objRequest.LeaveFromDate = clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim());
                    objRequest.LeaveToDate = clsCommon.Convert2DateTime(lblToPeriod.Text.Trim());
                    int iTotalHolidays = objRequest.GetTotalHolidays();
                    trUnpaid.Style["display"] = "none";
                    trPassportNo.Style["display"] = "table-row";
                    trPassportExpiryDate.Style["display"] = "table-row";
                    trLCExpiry.Style["display"] = "table-row";
                    trVisaExpiry.Style["display"] = "table-row";
                }

                Label lblStatus = (Label)fvLeaveRequest.FindControl("lblStatus");
                Button btCancel = (Button)fvLeaveRequest.FindControl("btCancel");

                if (Convert.ToBoolean(ViewState["Approve"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    txtEditreason.Visible = true;
                    lblReason.Visible = false;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                    btnApprove.Visible = true;
                    btCancel.Visible = true;
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    txtEditreason.Visible = false;
                    lblReason.Visible = true;
                    btnApprove.Visible = false;
                    btCancel.Visible = false;
                }
                //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
                //if (imgBack != null)
                    ibtnBack.Visible = true;//imgBack.Visible = 
            }

           //cancel request
            else if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Cancel"]) == true)
            {
                DropDownList ddlEditStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
                TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
                Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");

                if (ddlEditStatus != null)
                {
                    ddlEditStatus.DataSource = objRequest.GetAllRequestStatus(2);
                    ddlEditStatus.DataBind();
                }

                Label lblStatus = (Label)fvLeaveRequest.FindControl("lblStatus");
                Button btnCancel = (Button)fvLeaveRequest.FindControl("btnCancel");

                if (Convert.ToBoolean(ViewState["Cancel"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    txtEditreason.Visible = true;
                    lblReason.Visible = false;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("7"));
                    ddlEditStatus.Enabled = false;
                    btnCancel.Visible = true;
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    txtEditreason.Visible = false;
                    lblReason.Visible = true;
                    btnCancel.Visible = false;
                }
                //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
                //if (imgBack != null)
                ibtnBack.Visible = true;//imgBack.Visible = 
            }
            //cancel request
            else if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
            {
                DropDownList ddlEditStatus = (DropDownList)fvLeaveRequest.FindControl("ddlEditStatus");
                TextBox txtEditreason = (TextBox)fvLeaveRequest.FindControl("txtEditreason");
                Label lblReason = (Label)fvLeaveRequest.FindControl("lblReason");

                if (ddlEditStatus != null)
                {
                    ddlEditStatus.DataSource = objRequest.GetAllStatus();
                    ddlEditStatus.DataBind();
                }

                Label lblStatus = (Label)fvLeaveRequest.FindControl("lblStatus");
                Button btnRequestForCancel = (Button)fvLeaveRequest.FindControl("btnRequestForCancel");

                if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    txtEditreason.Visible = true;
                    lblReason.Visible = false;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
                    ddlEditStatus.Enabled = false;
                    btnRequestForCancel.Visible = true;
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    txtEditreason.Visible = false;
                    lblReason.Visible = true;
                    btnRequestForCancel.Visible = false;
                }
                //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
                //if (imgBack != null)
                ibtnBack.Visible = true;//imgBack.Visible =
            }

            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert | fvLeaveRequest.CurrentMode == FormViewMode.Edit)
            {
                TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
                TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");

                if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
                    txtToDate.Text = txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

                objRequest.Mode = "GLT";
                objRequest.EmployeeId = objUser.GetEmployeeId();

                DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
                ddlLeaveType.DataSource = objRequest.GetLeaveTypes();
                ddlLeaveType.DataBind();

                DropDownList ddlStatus = (DropDownList)fvLeaveRequest.FindControl("ddlStatus");
                ddlStatus.DataSource = objRequest.GetAllStatus();
                ddlStatus.DataBind();
                if (Convert.ToBoolean(ViewState["Approve"]) == false && (fvLeaveRequest.CurrentMode == FormViewMode.Edit | fvLeaveRequest.CurrentMode == FormViewMode.Insert))
                {
                    ddlStatus.Enabled = false;
                    ddlStatus.CssClass = "dropdownlist_disabled";
                }

                RadioButtonList rblHalfDay = (RadioButtonList)fvLeaveRequest.FindControl("rblhalfDay");
                RadioButtonList rblCreditLeave = (RadioButtonList)fvLeaveRequest.FindControl("rblCreditLeave");
                CustomValidator cvToDate = (CustomValidator)fvLeaveRequest.FindControl("cvEndDate");
                UpdatePanel upnldlCODays = fvLeaveRequest.FindControl("upnldlCODays") as UpdatePanel;
                UpdatePanel upnlrblCreditLeave = fvLeaveRequest.FindControl("upnlrblCreditLeave") as UpdatePanel;

                if (rblHalfDay.SelectedValue == "1")
                    cvToDate.ValidationGroup = "";
                else
                    cvToDate.ValidationGroup = "Submit";
                //--------------------------------------------------Travel and Encash--------------------------------------------------------------------------------
                clsLeaveExtensionRequest obj = new clsLeaveExtensionRequest();
                HtmlTableRow trTravelDocumentAdd = (HtmlTableRow)fvLeaveRequest.FindControl("trTravelDocumentAdd");
                UpdatePanel upnlTravelDocumentAdd = (UpdatePanel)fvLeaveRequest.FindControl("upnlTravelDocumentAdd");
                HtmlTableRow trEncah = (HtmlTableRow)fvLeaveRequest.FindControl("trEncah");
                TextBox txtBalanceCombo = (TextBox)fvLeaveRequest.FindControl("txtBalanceCombo");

                if (ddlLeaveType.SelectedValue == "-1")
                {
                    CalculateTotalNoofVacation(null, null);
                    trTravelDocumentAdd.Style["display"] = "block";
                    trEncah.Style["display"] = "table-row";
                    obj.EmployeeId = objUser.GetEmployeeId().ToInt32();
                    if (txtFromDate.Text != "1/1/1900 12:00:00 AM")
                        obj.ExtensionFromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
                    else
                        obj.ExtensionFromDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                    txtBalanceCombo.Text = Convert.ToString(obj.GetCompensatoryOffCount());
                }
                else
                {
                    trTravelDocumentAdd.Style["display"] = "none";
                    trEncah.Style["display"] = "none";
                }
                upForm.Update();
                upnlTravelDocumentAdd.Update();
                //----------------------------------------------------------------------------------------------------------------------------------------------------
                if (fvLeaveRequest.CurrentMode == FormViewMode.Edit)
                {
                    if (fvLeaveRequest.DataKey["LeaveTypeId"] != DBNull.Value)
                    {
                        ddlLeaveType.SelectedIndex = ddlLeaveType.Items.IndexOf(ddlLeaveType.Items.FindByValue(Convert.ToString(fvLeaveRequest.DataKey["LeaveTypeId"])));
                        ddlLeaveType.Enabled = false;
                        if (ddlLeaveType.SelectedValue == "5")
                        {
                            objRequest.RequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
                            DataTable dtDetails = objRequest.GetCompensatoryOffDetails();

                            if (dtDetails.Rows.Count > 0)
                            {
                                dlCODays.DataSource = dtDetails;
                                dlCODays.DataBind();

                                if (objRequest.GetCreditDetails())
                                    rblCreditLeave.SelectedValue = "1";
                                else
                                    rblCreditLeave.SelectedValue = "0";
                                upnlrblCreditLeave.Update();

                                for (int i = 0; i < dlCODays.Items.Count; i++)
                                {
                                    TextBox txtCODate = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                                    txtCODate.Text = dtDetails.Rows[i]["WorkedDay"].ToString();

                                    ImageButton imgAddDays = (ImageButton)dlCODays.Items[i].FindControl("imgAddDays");
                                    if (imgAddDays != null)
                                    {
                                        if (dlCODays.Items[i].ItemIndex == dlCODays.Items.Count - 1)
                                            imgAddDays.Visible = true;
                                        else
                                            imgAddDays.Visible = false;
                                    }
                                }
                            }
                            else
                            { }
                            upnldlCODays.Update();
                            upForm.Update();
                        }
                    }
                    if (fvLeaveRequest.DataKey["StatusId"] != DBNull.Value)
                    {
                        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvLeaveRequest.DataKey["StatusId"])));
                    }
                    if (fvLeaveRequest.DataKey["Halfday"] != DBNull.Value)
                    {
                        if (Convert.ToString(fvLeaveRequest.DataKey["Halfday"]) == "Yes" || Convert.ToString(fvLeaveRequest.DataKey["Halfday"]) == "نعم")
                            rblHalfDay.Items[0].Selected = true;
                        else
                            rblHalfDay.Items[1].Selected = true;
                    }
                }
                GetLeaveCount();
                //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
                //if (imgBack != null)
                ibtnBack.Visible = false;//imgBack.Visible = 
            }
        }
        catch 
        {
        
        
        
        }
    }

    private void HalfDayValidation()
    {
        RadioButtonList rblHalfDay = (RadioButtonList)fvLeaveRequest.FindControl("rblhalfDay");
        TextBox txtTodate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
        ImageButton ibtnLeaveTodate = (ImageButton)fvLeaveRequest.FindControl("ibtnLeaveTodate");
        CustomValidator cvToDate = (CustomValidator)fvLeaveRequest.FindControl("cvEndDate");

        if (rblHalfDay.SelectedValue == "1")
        {
            txtTodate.Enabled = false;
            ibtnLeaveTodate.Enabled = false;
            txtTodate.CssClass = "textbox_disabled";
            cvToDate.ValidationGroup = "";
        }
        else
        {
            txtTodate.Enabled = true;
            ibtnLeaveTodate.Enabled = true;
            txtTodate.CssClass = "textbox_mandatory";
            cvToDate.ValidationGroup = "Submit";
        }
    }

    protected void CheckHalfDay(object sender, EventArgs e)
    {
        HalfDayValidation();
    }

    protected void GetBalanceLeave(object sender, EventArgs e)
    {
        GetLeaveCount();
    }

    protected void GetNewBalanceLeave(object sender, EventArgs e)
    {
        Label lblLeaveType = (Label)fvLeaveRequest.FindControl("lblLeaveType");
        Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
        Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        TextBox txtNoOfHolidays = (TextBox)fvLeaveRequest.FindControl("txtNoOfHolidays");
        TextBox txtApprovalFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
        TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
        HiddenField hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
        HiddenField hdEmployeeID = (HiddenField)fvLeaveRequest.FindControl("hdEmployeeID");
        TextBox lblTotalLeaves = (TextBox)fvLeaveRequest.FindControl("lblTotalLeaves");
        HtmlTableRow trTotalVacationView = (HtmlTableRow)fvLeaveRequest.FindControl("trTotalVacationView");

        objUser = new clsUserMaster();
        objRequest = new clsLeaveRequest();
        objRequest.Mode = "GLC";
        objRequest.EmployeeId = hdEmployeeID.Value.ToInt32();
        if (hdLeaveType.Value.ToInt32() == -1)
        {
            DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtApprovalFromDate.Text.Trim()).ToString("MMM dd yyyy")); //Convert.ToDateTime(txtApprovalFromDate.Text);
            DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtApprovalToDate.Text.Trim()).ToString("MMM dd yyyy")); //Convert.ToDateTime(txtApprovalToDate.Text);
            TimeSpan ts = dt2 - dt1;
            lblTotalLeaves.Text = (ts.Days + 1).ToString();
            trTotalVacationView.Style["display"] = "table-row";
        }
        if (hdLeaveType.Value.ToInt32() > 0)
        {
            objRequest.LeaveType = hdLeaveType.Value.ToInt32();
        }
        else
            return;

        objRequest.year = clsCommon.Convert2DateTime(txtApprovalFromDate.Text).Year;
        objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtApprovalFromDate.Text.Trim());
        objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtApprovalToDate.Text.Trim());

        int year1 = 0, year2 = 0;
        year1 = clsCommon.Convert2DateTime(txtApprovalToDate.Text).Year;
        year2 = clsCommon.Convert2DateTime(txtApprovalFromDate.Text).Year;
        if (year1 == year2)
            objRequest.year = clsCommon.Convert2DateTime(txtApprovalFromDate.Text).Year;
        else
        {
            objRequest.year = clsCommon.Convert2DateTime(txtApprovalFromDate.Text).Year;
            objRequest.yearTo = clsCommon.Convert2DateTime(txtApprovalToDate.Text).Year;
        }
        double iLeaveCount = objRequest.GetLeaveCount();
        txtBalanceLeave.Text = (iLeaveCount).ToString();
        txtNoOfHolidays.Text = Convert.ToString(objRequest.GetTotalHolidays());
        upForm.Update();
    }

    private void GetLeaveCount()
    {
        objUser = new clsUserMaster();
        objRequest = new clsLeaveRequest();

        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
        TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");
        HtmlTableRow trHalfDay = (HtmlTableRow)fvLeaveRequest.FindControl("trHalfDay");
        HtmlTableCell tdBalanceLabel = (HtmlTableCell)fvLeaveRequest.FindControl("tdBalanceLabel");
        HtmlTableRow trCreditLeave = (HtmlTableRow)fvLeaveRequest.FindControl("trCreditLeave");
        HtmlTableRow trWorkedDay = (HtmlTableRow)fvLeaveRequest.FindControl("trWorkedDay");
        HtmlTableRow trFromDate = (HtmlTableRow)fvLeaveRequest.FindControl("trFromDate");
        HtmlTableRow trToDate = (HtmlTableRow)fvLeaveRequest.FindControl("trToDate");
        HtmlTableRow trNoComboOff = (HtmlTableRow)fvLeaveRequest.FindControl("trNoComboOff");
        RadioButtonList rblCreditLeave = (RadioButtonList)fvLeaveRequest.FindControl("rblCreditLeave");
        HtmlTableRow trTravelDocumentAdd = (HtmlTableRow)fvLeaveRequest.FindControl("trTravelDocumentAdd");
        HtmlTableRow trFromComp = (HtmlTableRow)fvLeaveRequest.FindControl("trFromComp");
        HtmlTableRow trEncah = (HtmlTableRow)fvLeaveRequest.FindControl("trEncah");
        //CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash");
        //if (ddlLeaveType.SelectedValue.ToInt32() != -1)
        //{
        //    chkEncash.Visible = false;
        //}
        //else
        //{
        //    chkEncash.Visible = true;
        //}
        if (ddlLeaveType != null && (ddlLeaveType.SelectedValue.ToInt32() != -1 && ddlLeaveType.SelectedValue.ToInt32() != 5))
        {
            objRequest.Mode = "GLC";
            objRequest.EmployeeId = objUser.GetEmployeeId();
            if (ddlLeaveType.Items.Count > 0)
            {
                objRequest.LeaveType = Convert.ToInt32(ddlLeaveType.SelectedValue);
            }
            objRequest.month = clsCommon.Convert2DateTime(txtFromDate.Text).Month;

            int year1 = 0, year2 = 0;
            year1 = clsCommon.Convert2DateTime(txtToDate.Text).Year;
            year2 = clsCommon.Convert2DateTime(txtFromDate.Text).Year;
            if (year1 == year2)
                objRequest.year = clsCommon.Convert2DateTime(txtFromDate.Text).Year;
            else
            {
                objRequest.year = clsCommon.Convert2DateTime(txtFromDate.Text).Year;
                objRequest.yearTo = clsCommon.Convert2DateTime(txtToDate.Text).Year;
            }
            objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtFromDate.Text.Trim());
            double iLeaveCount = objRequest.GetLeaveCount();
            txtBalanceLeave.Text = (iLeaveCount).ToString();
            if (ddlLeaveType.SelectedValue.ToInt32() != 6)
            {
                trHalfDay.Style["display"] = "table-row";
                tdBalanceLabel.Style["display"] = "table-row";
                txtBalanceLeave.Style["display"] = "table-row";
            }
            else
            {
                trHalfDay.Style["display"] = "none";
                tdBalanceLabel.Style["display"] = "none";
                txtBalanceLeave.Style["display"] = "none";
            }
            trFromDate.Style["display"] = "table-row";
            trToDate.Style["display"] = "table-row";
            trCreditLeave.Style["display"] = "none";
            trWorkedDay.Style["display"] = "none";
            trNoComboOff.Style["display"] = "none";
            trTravelDocumentAdd.Style["display"] = "none";
            trFromComp.Style["display"] = "none";
            trEncah.Style["display"] = "none";
            txtNoComboOff.Text = "";


            HalfDayValidation();
        }
        else
        {
            RadioButtonList rblhalfDay = (RadioButtonList)fvLeaveRequest.FindControl("rblhalfDay");
            CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
            //CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash");
            Literal Literal3 = (Literal)fvLeaveRequest.FindControl("Literal3");
            //HtmlGenericControl trFlightRequired = (HtmlGenericControl)fvLeaveRequest.FindControl("trFlightRequired");
            //HtmlGenericControl trSectorRequired = (HtmlGenericControl)fvLeaveRequest.FindControl("trSectorRequired");
            HtmlGenericControl trContactAddress = (HtmlGenericControl)fvLeaveRequest.FindControl("trContactAddress");
            HtmlGenericControl trTelephoneNo = (HtmlGenericControl)fvLeaveRequest.FindControl("trTelephoneNo");
            //HtmlTableRow trEncah = (HtmlTableRow)fvLeaveRequest.FindControl("trEncah");
            rblhalfDay.SelectedValue = "0";
            CheckHalfDay(null, null);
            trCreditLeave.Style["display"] = "none";
            trWorkedDay.Style["display"] = "none";
            trNoComboOff.Style["display"] = "none";
            txtNoComboOff.Text = "";
            trFromDate.Style["display"] = "table-row";
            trToDate.Style["display"] = "table-row";
            //if (chkEncash.Checked)
            //{
            //    Literal3.Text = "Encash Date";
            //    trFlightRequired.Style["display"] = "none";
            //    trSectorRequired.Style["display"] = "none";
            //    trContactAddress.Style["display"] = "none";
            //    trTelephoneNo.Style["display"] = "none";
            //    trEncah.Style["display"] = "none";
            //}
            //else
            //{
                Literal3.Text = "Leave From Date";
                //trFlightRequired.Style["display"] = "block";
                //trFlightRequired.Style["width"] = "272%";
                //trSectorRequired.Style["display"] = "block";
                //trSectorRequired.Style["width"] = "272%";
                trContactAddress.Style["display"] = "block";
                trContactAddress.Style["width"] = "272%";
                trTelephoneNo.Style["display"] = "block";
                trTelephoneNo.Style["width"] = "272%";
                trEncah.Style["display"] = "block";
            //}
            if (ddlLeaveType.SelectedValue.ToInt32() == 5)
            {
                trCreditLeave.Style["display"] = "block";
                CreateTableWorkedDays();
                if (chkCO.Checked)
                {
                    trFromComp.Style["display"] = "table-row";
                    trWorkedDay.Style["display"] = "none";
                }
                else
                {
                    trFromComp.Style["display"] = "none";
                    trWorkedDay.Style["display"] = "table-row";
                }
                if (rblCreditLeave.SelectedValue == "1")
                {
                    trNoComboOff.Style["display"] = "table-row";
                    trFromDate.Style["display"] = "none";
                    trToDate.Style["display"] = "none";
                }
            }

            if (ddlLeaveType.SelectedValue == "-1" && fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            {
                trTravelDocumentAdd.Style["display"] = "block";
                trFromComp.Style["display"] = "none";
                trEncah.Style["display"] = "table-row";
                DataTable dtEmpDetails = clsLeaveRequest.GetEmployeeContactDetails(objUser.GetEmployeeId().ToInt32());
                if (dtEmpDetails.Rows.Count > 0)
                {
                    txtContactAddress.Text = dtEmpDetails.Rows[0]["Address"].ToString();
                    txtTelephoneNo.Text = dtEmpDetails.Rows[0]["Phone"].ToString();
                }
                CalculateTotalNoofVacation(null, null);
            }
            else if (ddlLeaveType.SelectedValue == "-1" && fvLeaveRequest.CurrentMode == FormViewMode.Edit)
            {
                trTravelDocumentAdd.Style["display"] = "block";
                trEncah.Style["display"] = "none";
                CalculateTotalNoofVacation(null, null);
            }
            else
            {
                trTravelDocumentAdd.Style["display"] = "none";
                trEncah.Style["display"] = "none";
                //chkEncash.Visible = false;
            }
            trHalfDay.Style["display"] = "none";
            tdBalanceLabel.Style["display"] = "none";
            txtBalanceLeave.Style["display"] = "none";
            if (fvLeaveRequest.CurrentMode == FormViewMode.Edit || fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            {

                if (rblCreditLeave.SelectedValue == "0" && chkCO.Checked)
                {
                    tdBalanceLabel.Style["display"] = "table-row";
                    txtBalanceLeave.Style["display"] = "table-row";
                }
                else
                {
                    tdBalanceLabel.Style["display"] = "none";
                    txtBalanceLeave.Style["display"] = "none";
                }
            }
            upForm.Update();
        }

    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["Approve"] = false;
            ViewState["Cancel"] = false;
            ViewState["RequestId"] = ViewState["IsCredit"] = null;
            fvLeaveRequest.ChangeMode(FormViewMode.Insert);
            HiddenField hfConfirm = (HiddenField)fvLeaveRequest.FindControl("hfConfirm");
            DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
            if (hfConfirm != null)
            {
                hfConfirm.Value = "";
            }
            if (ddlLeaveType.SelectedValue == "5" && fvLeaveRequest.CurrentMode == FormViewMode.Edit)
                CreateTableWorkedDays();
            if (fvLeaveRequest.CurrentMode == FormViewMode.Insert)
            {
                ddlLeaveType.SelectedValue = "-1";
                GetBalanceLeave(null, null);
            }
            dlLeaveRequest.DataSource = null;
            dlLeaveRequest.DataBind();
            LeaveRequestPager.Visible = false;
            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
            upForm.Update();
            upDetails.Update();
        }
        catch { }
    }

    public void CreateTableWorkedDays()
    {
        objRequest = new clsLeaveRequest();
        HtmlTableRow trWorkedDay = (HtmlTableRow)fvLeaveRequest.FindControl("trWorkedDay");
        DataList dlCODays = (DataList)trWorkedDay.FindControl("dlCODays");
        UpdatePanel upnldlCODays = (UpdatePanel)fvLeaveRequest.FindControl("upnldlCODays");
        UpdatePanel upnlrblCreditLeave = (UpdatePanel)fvLeaveRequest.FindControl("upnlrblCreditLeave");
        TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");

        DataTable dt = new DataTable();
        dt.Columns.Add("RequestId", typeof(int));
        dt.Columns.Add("WorkedDay", typeof(string));

        DataRow dr = dt.NewRow();
        dr["RequestId"] = 0;
        dr["WorkedDay"] = "";


        if (dlCODays.Items.Count == 0)
            dt.Rows.Add(dr);
        if (dlCODays != null)
        {
            for (int i = 0; i < dlCODays.Items.Count; i++)
            {
                TextBox txtCODate = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                DataRow dr1 = dt.NewRow();
                dr1["RequestId"] = i;
                dr1["WorkedDay"] = txtCODate.Text;
                if (txtCODate.Text != "")
                    txtNoComboOff.Text = (i + 1).ToString();
                dt.Rows.Add(dr1);
            }
        }
        dlCODays.DataSource = dt;
        dlCODays.DataBind();
        upnldlCODays.Update();

        trWorkedDay.Style["display"] = "table-row";

        if (fvLeaveRequest.CurrentMode == FormViewMode.Edit)
        {
            objRequest.RequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);
            DataTable dtDetails = objRequest.GetCompensatoryOffDetails();

            if (dtDetails.Rows.Count > 0)
            {
                dlCODays.DataSource = dtDetails;
                dlCODays.DataBind();

                for (int i = 0; i < dlCODays.Items.Count; i++)
                {
                    TextBox txtCODate = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                    txtCODate.Text = dtDetails.Rows[i]["WorkedDay"].ToString();

                    ImageButton imgAddDays = (ImageButton)dlCODays.Items[i].FindControl("imgAddDays");
                    if (imgAddDays != null)
                    {
                        if (dlCODays.Items[i].ItemIndex == dlCODays.Items.Count - 1)
                            imgAddDays.Visible = true;
                        else
                            imgAddDays.Visible = false;
                    }
                }

                upnlrblCreditLeave.Update();
                if (rblCreditLeave.SelectedValue == "1")
                    txtNoComboOff.Text = dtDetails.Rows.Count.ToString();
            }
            else
            { }
            upnldlCODays.Update();
            upForm.Update();
        }
    }

    /// <summary>
    /// bind selected request details in formview 
    /// </summary>
    /// <param name="iRequestId"></param>
    private void BindRequestDetails(int iRequestId)
    {
        objRequest = new clsLeaveRequest();
        clsLeaveExtensionRequest obj = new clsLeaveExtensionRequest();
        objRequest.Mode = "EDT";
        objRequest.RequestId = iRequestId;
        ViewState["RequestId"] = iRequestId;
        DataTable dt = objRequest.GetRequestDetails();
        fvLeaveRequest.DataSource = dt;
        fvLeaveRequest.DataBind();

        dlLeaveRequest.DataSource = null;
        dlLeaveRequest.DataBind();

        LeaveRequestPager.Visible = false;

        HtmlTableRow trAuthority = (HtmlTableRow)fvLeaveRequest.FindControl("trAuthority");
        DataList dl1 = (DataList)fvLeaveRequest.FindControl("dl1");
        if (trAuthority != null)
        {
            objRequest.RequestId = iRequestId;
            switch (Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]))
            {
                case 5:
                    objRequest.LeaveType = (int)RequestType.CompensatoryOffRequest;
                    break;
                case -1:
                    objRequest.LeaveType = (int)RequestType.VacationLeaveRequest;
                    break;
                default:
                    objRequest.LeaveType = (int)RequestType.Leave;
                    break;
            }
            objRequest.CompanyID = objRequest.GetCompany();
            DataTable dt2 = objRequest.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                trAuthority.Style["display"] = "table-row";
                dl1.DataSource = dt2;
                dl1.DataBind();
            }
        }


        //-------------------------------------------Compensatory-Offs----------------------------------------------------------------------
        HtmlTableRow trFromComp = (HtmlTableRow)fvLeaveRequest.FindControl("trFromComp");
        HtmlTableRow trWorkedDay = (HtmlTableRow)fvLeaveRequest.FindControl("trWorkedDay");
        CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
        RadioButtonList rblCreditLeave = (RadioButtonList)fvLeaveRequest.FindControl("rblCreditLeave");
        TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
        HtmlTableCell tdBalanceLabel = (HtmlTableCell)fvLeaveRequest.FindControl("tdBalanceLabel");
        TextBox txtEncashCombo = (TextBox)fvLeaveRequest.FindControl("txtEncashCombo");
        TextBox txtEncashComboEdit = (TextBox)fvLeaveRequest.FindControl("txtEncashComboEdit");
        HtmlTableRow trFromDate = (HtmlTableRow)fvLeaveRequest.FindControl("trFromDate");
        HtmlTableRow trToDate = (HtmlTableRow)fvLeaveRequest.FindControl("trToDate");
        HtmlTableRow trNoComboOff = (HtmlTableRow)fvLeaveRequest.FindControl("trNoComboOff");
        //CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash");
        ViewState["IsCredit"] = dt.Rows[0]["IsCredit"].ToString() == "True" ? "1" : "0";
        if (fvLeaveRequest.CurrentMode == FormViewMode.Edit)//Binding Compensatory details during Edit mode
        {
            rblCreditLeave.SelectedValue = dt.Rows[0]["IsCredit"].ToString() == "True" ? "1" : "0";
            //rblFlightRequired.SelectedValue = dt.Rows[0]["IsFlightRequiredBool"].ToString() == "True" ? "1" : "0";
            //rblSectorRequired.SelectedValue = dt.Rows[0]["IsSectorRequiredBool"].ToString() == "True" ? "1" : "0";
            txtContactAddress.Text = dt.Rows[0]["ContactAddress"].ToString();
            txtTelephoneNo.Text = dt.Rows[0]["TelephoneNo"].ToString();
            chkCO.Checked = dt.Rows[0]["IsFromComboOff"].ToBoolean();
            chkCO_CheckedChanged(new object(), new EventArgs());//To retrieve Compensatory balance based on credited leaves

            rblCreditLeave.SelectedValue = dt.Rows[0]["IsCredit"].ToString();

            if (rblCreditLeave.SelectedValue == "0" && chkCO.Checked)//If leave taken from Compensatory-Off
            {
                if (dt.Rows[0]["LeaveTypeId"].ToInt32() == 5)
                {
                    trFromComp.Style["display"] = "table-row";
                    if (trFromDate != null && trToDate != null)
                    {
                        trFromDate.Style["display"] = "table-row";
                        trToDate.Style["display"] = "table-row";
                        trNoComboOff.Style["display"] = "none";
                    }
                }
                trWorkedDay.Style["display"] = "none";
                tdBalanceLabel.Style["display"] = "table-row";
                txtBalanceLeave.Style["display"] = "table-row";
                dlCODays.Visible = false;
            }
            else//If leave taken by along with worked dates
            {
                if (dt.Rows[0]["LeaveTypeId"].ToInt32() == 5)
                    trFromComp.Style["display"] = "table-row";
                trWorkedDay.Style["display"] = "table-row";
                tdBalanceLabel.Style["display"] = "none";
                txtBalanceLeave.Style["display"] = "none";
                dlCODays.Visible = true;
            }

            if (dt.Rows[0]["LeaveTypeId"].ToInt32() == -1)
                txtEncashCombo.Text = dt.Rows[0]["EncashComboOffday"].ToString();
            upForm.Update();

            //-------Document Attach-------------------------------------------------------------------------------------------------
            Controls_AttachDocument AttachDocument1 = (Controls_AttachDocument)fvLeaveRequest.FindControl("AttachDocument1");
            if (AttachDocument1 != null)
            {
                AttachDocument1.LoadData(ViewState["RequestId"].ToInt32());
            }
        }
        if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)//During Approval
        {
            GridView gvComboOff = (GridView)fvLeaveRequest.FindControl("gvComboOff");
            GridView gvPreviousLoanDetails = (GridView)fvLeaveRequest.FindControl("gvPreviousLoanDetails");
            HtmlTableRow trComboOff = (HtmlTableRow)fvLeaveRequest.FindControl("trComboOff");
            HtmlTableRow trCount = (HtmlTableRow)fvLeaveRequest.FindControl("trCount");
            HtmlTableRow trFrom = (HtmlTableRow)fvLeaveRequest.FindControl("trFrom");
            HtmlTableRow trTo = (HtmlTableRow)fvLeaveRequest.FindControl("trTo");
            Label lblDuration = (Label)fvLeaveRequest.FindControl("lblDuration");
            HtmlTableRow trCompType = (HtmlTableRow)fvLeaveRequest.FindControl("trCompType");
            Label lblFromComboOff = (Label)fvLeaveRequest.FindControl("lblFromComboOff");
            //Label lblEligibleLeaves = (Label)fvLeaveRequest.FindControl("lblEligibleLeaves");
            HtmlTableCell tdBal = (HtmlTableCell)fvLeaveRequest.FindControl("tdBal");
            TextBox txtBal = (TextBox)fvLeaveRequest.FindControl("txtBal");
            TextBox txtBalanceComboEdit = (TextBox)fvLeaveRequest.FindControl("txtBalanceComboEdit");
            HtmlTable tdTravel = (HtmlTable)fvLeaveRequest.FindControl("tdTravel");
            TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
            TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
            TextBox txtApprovalFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
            TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
          //  CheckBox chkEncash1 = (CheckBox)fvLeaveRequest.FindControl("chkEncash1");
            TextBox lblTotalLeaves = (TextBox)fvLeaveRequest.FindControl("lblTotalLeaves");
            HtmlTableRow trTravelDocument = (HtmlTableRow)fvLeaveRequest.FindControl("trTravelDocument");
            //HtmlTableRow trFlightRequiredView = (HtmlTableRow)trTravelDocument.FindControl("trFlightRequiredView");
            //HtmlTableRow trSectorRequiredView = (HtmlTableRow)trTravelDocument.FindControl("trSectorRequiredView");
            HtmlTableRow trContactAddressView = (HtmlTableRow)trTravelDocument.FindControl("trContactAddressView");
            HtmlTableRow trTelephoneNoView = (HtmlTableRow)trTravelDocument.FindControl("trTelephoneNoView");
            HtmlTableRow trPassportNo = (HtmlTableRow)trTravelDocument.FindControl("trPassportNo");
            HtmlTableRow trPassportExpiryDate = (HtmlTableRow)trTravelDocument.FindControl("trPassportExpiryDate");
            HtmlTableRow trLCExpiry = (HtmlTableRow)trTravelDocument.FindControl("trLCExpiry");
            HtmlTableRow trVisaExpiry = (HtmlTableRow)trTravelDocument.FindControl("trVisaExpiry");
            HtmlTableRow trEncashView = (HtmlTableRow)trTravelDocument.FindControl("trEncashView");
            Literal Literal9 = (Literal)fvLeaveRequest.FindControl("Literal9");
            Label lblLeaveType = (Label)fvLeaveRequest.FindControl("lblLeaveType");
            //HtmlTableRow trEncashView = (HtmlTableRow)trTravelDocument.FindControl("trEncashView");

            //-------Document Attach-------------------------------------------------------------------------------------------------
            Controls_AttachDocument ViewAttachDocuments = (Controls_AttachDocument)fvLeaveRequest.FindControl("ViewAttachDocuments");
            if (ViewAttachDocuments != null)
            {
                ViewAttachDocuments.LoadData(ViewState["RequestId"].ToInt32());
            }




            if (lblLeaveType.Text != "Vacation") //----------check-------
            {
                //chkEncash1.Visible = false;
                trEncashView.Visible = false;
            }
            else
            {
                //chkEncash1.Visible = true;
                trEncashView.Visible = true;
               // chkEncash1.Enabled = false;
                lblTotalLeaves.Enabled = false;
            }
            //-----------------------------------------------------------------------------------------------------------------------
            //if (chkEncash1.Checked)                   // ----------check-------
            //{
            //    trFlightRequiredView.Style["display"] = "none";
            //    trSectorRequiredView.Style["display"] = "none";
            //    trContactAddressView.Style["display"] = "none";
            //    trTelephoneNoView.Style["display"] = "none";
            //    trPassportNo.Style["display"] = "none";
            //    trPassportExpiryDate.Style["display"] = "none";
            //    trLCExpiry.Style["display"] = "none";
            //    trVisaExpiry.Style["display"] = "none";
            //    trEncashView.Style["display"] = "none";
            //    Literal9.Text = "Encash Date";
            //}
            //else
            //{
                //trFlightRequiredView.Style["display"] = "table-row";
                //trSectorRequiredView.Style["display"] = "table-row";
                trContactAddressView.Style["display"] = "table-row";
                trTelephoneNoView.Style["display"] = "table-row";
                trPassportNo.Style["display"] = "table-row";
                trPassportExpiryDate.Style["display"] = "table-row";
                trLCExpiry.Style["display"] = "table-row";
                trVisaExpiry.Style["display"] = "table-row";
                trEncashView.Style["display"] = "table-row";
                Literal9.Text = "Leave From Date";
            //}
            objRequest.RequestId = iRequestId;
            if (trComboOff != null)//Displays Extra worked days if present
            {
                //In case of compensatory leaves not taken credited leaves
                if (Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]) == 5 && dt.Rows[0]["IsFromComboOff"].ToBoolean() == false)
                {
                    objRequest.IsCredit = (ViewState["IsCredit"].ToString() == "1" ? true : false);
                    DataTable dtDetails = objRequest.GetCompensatoryOffDetails();
                    if (dtDetails.Rows.Count > 0)
                    {
                        if (dtDetails.Rows.Count > 5)
                        {
                            trComboOff.Style["height"] = "249px";
                        }
                        gvComboOff.DataSource = dtDetails;
                        gvComboOff.DataBind();
                        lblDuration.Text = Convert.ToString(dtDetails.Rows.Count);
                        trComboOff.Style["display"] = "table-row";
                        trCount.Style["display"] = "block";

                        if (Convert.ToInt32(dt.Rows[0]["IsCredit"]) > 0)
                        {
                            trFrom.Style["display"] = "none";
                            trTo.Style["display"] = "none";
                            tdBal.Style["display"] = "none";
                            txtBal.Style["display"] = "none";
                        }
                        if (Convert.ToInt32(dt.Rows[0]["IsCredit"]) == 0)
                        {
                            trCompType.Style["display"] = "block";
                        }
                    }
                }
                else
                {
                    trComboOff.Style["display"] = "none";
                    trCount.Style["display"] = "none";
                    //compensatory leaves taken from credited leaves
                    obj.EmployeeId = dt.Rows[0]["EmployeeId"].ToInt32();
                    obj.ExtensionFromDate = clsCommon.Convert2DateTime(dt.Rows[0]["LeaveFromDate"]);
                    if (dt.Rows[0]["IsFromComboOff"].ToString() == "True" && Convert.ToInt32(dt.Rows[0]["IsCredit"]) == 0 && Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]) == 5)
                    {
                        tdBal.Style["display"] = "block";
                        txtBal.Style["display"] = "block";
                        txtBal.Text = Convert.ToString(obj.GetCompensatoryOffCount());
                    }
                    else if (Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]) == -1)
                    {
                        txtBalanceComboEdit.Text = Convert.ToString(obj.GetCompensatoryOffCount());
                        txtEncashComboEdit.Text = dt.Rows[0]["EncashComboOffDay"].ToString();

                        if ((fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Request.QueryString["Requestid"] != null && Request.QueryString["Type"] != "Cancel") || Convert.ToBoolean(ViewState["Approve"]) == true)
                        {
                            HtmlTableRow trEligibleLeavesView = (HtmlTableRow)fvLeaveRequest.FindControl("trEligibleLeavesView");
                            trEligibleLeavesView.Style["display"] = "table-row";
                        }
                        tdBal.Style["display"] = "none";
                        txtBal.Style["display"] = "none";


                    }
                    else
                    {
                        tdBal.Style["display"] = "none";
                        txtBal.Style["display"] = "none";
                    }
                }
            }
            else
            {
                trComboOff.Style["display"] = "none";
                trCount.Style["display"] = "none";
            }
            //Displays Previous loan Details                       
            if (Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]) == -1)
            {
                DataTable dtLoanDetails = objRequest.GetPreviousLoanDetails();
                if (dtLoanDetails.Rows.Count > 0)
                {
                    if (dtLoanDetails.Rows.Count > 5)
                    {
                        gvPreviousLoanDetails.Style["height"] = "249px";
                    }
                    gvPreviousLoanDetails.DataSource = dtLoanDetails;
                    gvPreviousLoanDetails.DataBind();
                }
            }
        }
        //link to display approval level remarks
        HtmlTableRow trlink = (HtmlTableRow)fvLeaveRequest.FindControl("trlink");
        //ImageButton imgBack = (ImageButton)fvLeaveRequest.FindControl("imgBack");
        if (trlink != null)
            trlink.Style["display"] = "table-row";
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            ibtnBack.Visible = true; //imgBack.Visible = 
        }
        else
        {
            ViewState["RequestID"] = objRequest.RequestId;
            ibtnBack.Visible = false; //imgBack.Visible = 
        }
    }
    protected void imgback_Click(object source, EventArgs e)
    {
        if (hfPostedUrl.Value != null)
            Response.Redirect(hfPostedUrl.Value.ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsLeaveRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            objRequest.LeaveType = Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]);
            LinkButton lbReason = (LinkButton)fvLeaveRequest.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)fvLeaveRequest.FindControl("hfLink");
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                GridView gvReasons = (GridView)fvLeaveRequest.FindControl("gvReasons");
                trReasons.Style["display"] = "table-row";

                DataTable dt = objRequest.DisplayReasons();
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                trReasons.Style["display"] = "none";
            }
        }
    }
    protected void dlLeaveRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "_View":
                    HiddenField hfLink = (HiddenField)fvLeaveRequest.FindControl("hfLink");
                    HtmlTableRow trReasons = (HtmlTableRow)fvLeaveRequest.FindControl("trReasons");
                   
                    if (hfLink != null && trReasons != null)
                    {
                        hfLink.Value = "0";
                        trReasons.Style["display"] = "none";
                    }
                    fvLeaveRequest.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                    enableDeleteButton(true);
                   
                    break;

                case "_Edit":

                    fvLeaveRequest.ChangeMode(FormViewMode.Edit);
                    BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                    GetBalanceLeave(null, null);
                    enableDeleteButton(false);
                    break;

                case "_Cancel":

                    HiddenField hdLeaveTypeID = (HiddenField)dlLeaveRequest.Items[e.Item.ItemIndex].FindControl("hdLeaveTypeID");
                    int iRequestId = e.CommandArgument.ToInt32();
                    objRequest = new clsLeaveRequest();
                    objUser = new clsUserMaster();
                    objRequest.StatusId = (int)RequestStatus.RequestForCancel;
                    objRequest.RequestId = iRequestId;
                    objRequest.EmployeeId = objUser.GetEmployeeId();
                    objRequest.LeaveType = Convert.ToInt32(hdLeaveTypeID.Value);
                    if ((clsLeaveRequest.CancelIsCreditLeaves(iRequestId).ToInt32() > 0) && (hdLeaveTypeID.Value == "5"))
                    {
                        msgs.InformationalMessage(GetLocalResourceObject("CannotCancel").ToString());
                        mpeMessage.Show();
                        return;
                    }
                    objRequest.Reason = "Request For Cancellation";
                    objRequest.UpdateCancelStatus();
                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    SendMessage((int)RequestStatus.RequestForCancel, iRequestId, hdLeaveTypeID.Value.ToInt32());
                    //--------------------------------------------------------------------------------------------------------------------------------------------------                   
                    msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                    BindDataList();
                    mpeMessage.Show();
                    break;
            }
        }
        catch { }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["Cancel"] = false;
        ViewState["RequestForCancel"] = false;
        BindDataList();
        upForm.Update();
        upDetails.Update();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string message = string.Empty;
            objRequest = new clsLeaveRequest();

            if (dlLeaveRequest.Items.Count > 0)
            {
                foreach (DataListItem item in dlLeaveRequest.Items)
                {
                    CheckBox chkRequest = (CheckBox)item.FindControl("chkLeave");
                    HiddenField LeaveType = (HiddenField)item.FindControl("LeaveType");
                    if (chkRequest == null)
                        continue;
                    if (chkRequest.Checked)
                    {
                        objRequest.RequestId = Convert.ToInt32(dlLeaveRequest.DataKeys[item.ItemIndex]);
                        if (objRequest.IsLeaveRequestApproved())
                        {
                            // message = "Approved leave request(s) could not be deleted.";
                            //previously working code
                            //message = "Processing started request(s) cannot be deleted.";
                            message = GetLocalResourceObject("ProcessStarted").ToString();
                            //message += "</ol>Please delete the existing details to proceed with employee deletion";
                        }
                        else
                        {

                            int LeaveTypeID = LeaveType.Value.ToInt32();
                            objRequest.Mode = "DEL";
                            objRequest.DeleteRequest();
                            if(LeaveTypeID != -1)
                                clsCommonMessage.DeleteMessages(dlLeaveRequest.DataKeys[item.ItemIndex].ToInt32(), (LeaveTypeID == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest));//eReferenceTypes.LeaveRequest
                            else
                                clsCommonMessage.DeleteMessages(dlLeaveRequest.DataKeys[item.ItemIndex].ToInt32(), eReferenceTypes.VacationLeaveRequest);
                            message = GetLocalResourceObject("RequestDeleteSuccessfully").ToString();
                        }
                    }
                }
            }
            else
            {
                if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
                {
                    objRequest.RequestId = Convert.ToInt32(fvLeaveRequest.DataKey["RequestId"]);

                    if (objRequest.IsLeaveRequestApproved())
                    {
                        // message = "Approved leave request could not be deleted.";
                        // message = "Processing started request(s) cannot be deleted.";
                        message = GetLocalResourceObject("ProcessStarted").ToString();
                        //message += "</ol>Please delete the existing details to proceed with employee deletion";
                    }
                    else
                    {
                        objRequest.Mode = "DEL";
                        objRequest.DeleteRequest();
                        int LeaveType = Convert.ToInt32(fvLeaveRequest.DataKey["LeaveTypeId"]);
                        if (LeaveType != -1)
                            clsCommonMessage.DeleteMessages(fvLeaveRequest.DataKey["RequestId"].ToInt32(), (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest));//eReferenceTypes.LeaveRequest
                        else
                            clsCommonMessage.DeleteMessages(fvLeaveRequest.DataKey["RequestId"].ToInt32(), eReferenceTypes.VacationLeaveRequest);
                        message = GetLocalResourceObject("RequestDeleteSuccessfully").ToString();
                    }
                }
            }
            msgs.InformationalMessage(message);
            mpeMessage.Show();

            BindDataList();

            upForm.Update();
            upDetails.Update();
        }
        catch { }
    }
    protected void dlLeaveRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
            HiddenField hdRequestID = (HiddenField)e.Item.FindControl("hdRequestID");
            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
            HiddenField hdIsVacation = (HiddenField)e.Item.FindControl("hdIsVacation");
            HtmlTableRow trApprovalDate = (HtmlTableRow)e.Item.FindControl("trApprovalDate");
            HtmlTableRow trRequestedDate = (HtmlTableRow)e.Item.FindControl("trRequestedDate");

            int iRequestId = txtStatusId.Text.ToInt32();

            if (iRequestId == 2 || iRequestId == 3 || iRequestId == 4 || iRequestId == 5 || iRequestId == 6 || iRequestId == 7 || Convert.ToBoolean(hdForwarded.Value) == true)
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton btnCancel = (ImageButton)e.Item.FindControl("btnCancel");
            if (iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }
            if (iRequestId == 1 && !Convert.ToBoolean(hdForwarded.Value))
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }
            if (iRequestId == 5)
            {
                objRequest = new clsLeaveRequest();
                trApprovalDate.Style["display"] = "table-row";
                if (Convert.ToString(hdIsVacation.Value).ToUpper() == "YES")
                    trRequestedDate.Style["display"] = "none";
                if ((objRequest.CheckVacationProcessed(hdRequestID.Value.ToInt32()) || objRequest.CheckSalaryProcessingCancel(hdRequestID.Value.ToInt32())))
                {
                    btnCancel.Enabled = false;
                    btnCancel.ImageUrl = "~/images/cancel_disable.png";
                }
            }
        }
    }

    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void SendMessage(int StatusID, int RequestID, int LeaveType)
    {
        switch (StatusID)
        {
            case (int)RequestStatus.Approved:
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(null, RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave_Approval, eAction.Approved, (LeaveType == 5 ? "Compensatory Off" : ""));
                    clsCommonMail.SendMail(RequestID, MailRequestType.Leave, eAction.Approved);
                }
                else//Vacation request
                {
                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Approval, eAction.Approved, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Vacation, eAction.Approved);
                }
                break;
            case (int)RequestStatus.Rejected:
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(null, RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave_Rejection, eAction.Reject, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Leave, eAction.Reject);
                }
                else
                {
                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Rejection, eAction.Reject, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Vacation, eAction.Reject);
                }
                break;
            case (int)RequestStatus.Applied:
                objUser = new clsUserMaster();
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(objUser.GetEmployeeId(), RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave_Request, eAction.Applied, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Leave, eAction.Applied);
                }
                else
                {
                    clsCommonMessage.SendMessage(objUser.GetEmployeeId(), RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Request, eAction.Applied, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Vacation, eAction.Applied);
                }
                break;
            case (int)RequestStatus.RequestForCancel:
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(null, RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave__Cancel_Request, eAction.RequestForCancel, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Leave, eAction.RequestForCancel);
                }
                else
                {
                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Cancel_Request, eAction.RequestForCancel, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Vacation, eAction.RequestForCancel);
                }
                break;
            case (int)RequestStatus.Cancelled:
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(null, RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave_Cancellation, eAction.Cancelled, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Leave, eAction.Cancelled);
                }
                else
                {
                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Cancellation, eAction.Cancelled, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.Vacation, eAction.Cancelled);
                }
                break;
            case (int)RequestStatus.Pending:
                if (LeaveType != -1)
                {
                    clsCommonMessage.SendMessage(null, RequestID, (LeaveType == 5 ? eReferenceTypes.CompensatoryOffRequest : eReferenceTypes.LeaveRequest), LeaveType == 5 ? eMessageTypes.Compensatory__Of_Request : eMessageTypes.Leave_Approval, eAction.Pending, "");
                }
                else
                {
                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VacationLeaveRequest, eMessageTypes.Vacation_Approval, eAction.Pending, "");
                }
                break;
        }
    }

    /// <summary>
    /// Crop the string if it exceeds the width of container 
    /// </summary>
    protected string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    private void enableDeleteButton(bool IsEnable)
    {
        if (IsEnable)
        {
            lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("ControlsCommon", "DeleteConfirmation") + "');";
            lnkDelete.Enabled = true;
        }
        else
        {
            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
        }
        upMenu.Update();
    }

    protected string ArrangeString(string value, int length)
    {
        int intlen = value.Length;
        int intPosition = 0;
        int intNewPosition = -3;
        string strCurrentstring = "";
        string strInsertString = "";
        if (intlen <= length)
            return value;
        else
        {
            int intCount = intlen / length;
            for (intPosition = 0; intPosition <= intCount; intPosition++)
            {
                intNewPosition += length;
                if (intNewPosition < intlen)
                {
                    strCurrentstring = value.Substring(intNewPosition - 1, 1);
                }

                if (strCurrentstring == " ")
                    strInsertString = "<p>";
                else
                    strInsertString = "-<p>";
                if (intNewPosition < intlen)
                    value = value.Insert(intNewPosition, strInsertString);
            }
            return value;
        }
    }
    //code for Compensatory-Off
    #region compensatory-Off
    protected void rblCreditLeave_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            HtmlTableRow trFromComp = (HtmlTableRow)fvLeaveRequest.FindControl("trFromComp");
            HtmlTableRow trWorkedDay = (HtmlTableRow)fvLeaveRequest.FindControl("trWorkedDay");
            HtmlTableRow trFromDate = (HtmlTableRow)fvLeaveRequest.FindControl("trFromDate");
            HtmlTableRow trToDate = (HtmlTableRow)fvLeaveRequest.FindControl("trToDate");
            HtmlTableRow trNoComboOff = (HtmlTableRow)fvLeaveRequest.FindControl("trNoComboOff");
            RadioButtonList rblCreditLeave = (RadioButtonList)fvLeaveRequest.FindControl("rblCreditLeave");
            TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");
            UpdatePanel upnldlCODays = fvLeaveRequest.FindControl("upnldlCODays") as UpdatePanel;
            UpdatePanel upnlrblCreditLeave = fvLeaveRequest.FindControl("upnlrblCreditLeave") as UpdatePanel;
            TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
            TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
            DataList dlCODays = (DataList)fvLeaveRequest.FindControl("dlCODays");
            CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
            HtmlTableCell tdBalanceLabel = (HtmlTableCell)fvLeaveRequest.FindControl("tdBalanceLabel");
            TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");

            trWorkedDay.Style["display"] = "table-row";
            chkCO.Checked = false;
            if (rblCreditLeave.SelectedValue == "0")
            {
                trFromComp.Style["display"] = "block";
                trFromDate.Style["display"] = "table-row";
                trToDate.Style["display"] = "table-row";
                trNoComboOff.Style["display"] = "none";
                txtBalanceLeave.Style["display"] = "none";
                tdBalanceLabel.Style["display"] = "none";
                if (fvLeaveRequest.CurrentMode == FormViewMode.Edit)
                    txtToDate.Text = txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                trFromComp.Style["display"] = "none";
                trFromDate.Style["display"] = "none";
                trToDate.Style["display"] = "none";
                trNoComboOff.Style["display"] = "table-row";
                txtBalanceLeave.Style["display"] = "none";
                tdBalanceLabel.Style["display"] = "none";
            }
            upnlrblCreditLeave.Update();
            upForm.Update();
        }
        catch { }
    }

    protected void dlCODays_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;
        int NoofDays = 0;
        if (e.CommandName == "Remove")
        {
            if (e.Item.ItemIndex > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("RequestId");
                dt.Columns.Add("WorkedDay");

                foreach (DataListItem item in dlCODays.Items)
                {
                    if (item != e.Item)
                    {
                        DataRow dr = dt.NewRow();
                        TextBox txtCODate = (TextBox)item.FindControl("txtCODate");

                        dr["RequestId"] = 0;
                        dr["WorkedDay"] = txtCODate.Text;
                        dt.Rows.Add(dr);
                    }
                }

                if (dlCODays != null)
                {
                    if (dt.Rows.Count == 0)
                        dt.Rows.Add(dt.NewRow());

                    dlCODays.DataSource = dt;
                    dlCODays.DataBind();
                }

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox txtCODate = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                        TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");

                        NoofDays = NoofDays + 1;
                        txtNoComboOff.Text = NoofDays.ToString();
                        if (dt.Rows[i]["RequestId"].ToInt32() != 0)
                        {
                            txtCODate.Text = dt.Rows[i]["WorkedDay"].ToString();
                        }

                        ImageButton imgAddDays = (ImageButton)dlCODays.Items[i].FindControl("imgAddDays");
                        if (imgAddDays != null)
                        {
                            if (dlCODays.Items[i].ItemIndex == dlCODays.Items.Count - 1)
                                imgAddDays.Visible = true;
                            else
                                imgAddDays.Visible = false;
                        }
                    }
                }
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                msgs.WarningMessage(msg);
                mpeMessage.Show();
            }
            upForm.Update();
        }
    }

    protected void imgAddDays_Click(object sender, ImageClickEventArgs e)
    {
        string msg = string.Empty;
        Boolean isVal = true;
        DataTable dt = new DataTable();
        dt.Columns.Add("RequestId");
        dt.Columns.Add("WorkedDay");

        int NoofDays = 0;
        TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");
        foreach (DataListItem item in dlCODays.Items)
        {
            DataRow dr = dt.NewRow();
            TextBox txtCODate = (TextBox)item.FindControl("txtCODate");
            if (txtCODate.Text != "")
            {
                NoofDays = NoofDays + 1;
            }
            dr["WorkedDay"] = txtCODate.Text;

            if (txtCODate.Text == "")
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("من فضلك ادخل التاريخ") : ("Please enter worked dates");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                return;
            }
            else
            {
                dr["WorkedDay"] = txtCODate.Text;
            }
            dt.Rows.Add(dr);
        }
        if (dlCODays != null)
        {
            if (dlCODays.Items.Count > 0)
            {
                for (int i = 0; i < dlCODays.Items.Count; i++)
                {
                    TextBox txtCODate1 = (TextBox)dlCODays.Items[i].FindControl("txtCODate");

                    for (int j = i + 1; j < dlCODays.Items.Count; j++)
                    {
                        TextBox txtCODate2 = (TextBox)dlCODays.Items[j].FindControl("txtCODate");
                        if (txtCODate1.Text == txtCODate2.Text)
                        {
                            isVal = false;
                            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يسمح تواريخ مكررة") : ("Duplicate Dates are not allowed");
                            msgs.InformationalMessage(msg);
                            mpeMessage.Show();
                            break;
                        }
                    }
                }
            }
            if (isVal)
            {
                txtNoComboOff.Text = NoofDays.ToString();
                dt.Rows.Add(dt.NewRow());
                dlCODays.DataSource = dt;
                dlCODays.DataBind();
            }
        }

        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TextBox txtCODate = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                ImageButton imgAddDays = (ImageButton)dlCODays.Items[i].FindControl("imgAddDays");
                if (imgAddDays != null)
                {
                    if (dlCODays.Items[i].ItemIndex == dlCODays.Items.Count - 1)
                    {
                        imgAddDays.Visible = true;
                    }
                    else
                    {
                        NoofDays = NoofDays + 1;
                        imgAddDays.Visible = false;
                    }
                }
            }
        }
    }

    protected void txtCODate_TextChanged(object sender, EventArgs e)
    {
        objRequest = new clsLeaveRequest();
        objUser = new clsUserMaster();
        int NoofDays = 0;
        TextBox txtNoComboOff = (TextBox)fvLeaveRequest.FindControl("txtNoComboOff");
        UpdatePanel upnldlCODays = (UpdatePanel)fvLeaveRequest.FindControl("upnldlCODays");

        if (dlCODays.Items.Count >= 1)
        {
            for (int i = 0; i < dlCODays.Items.Count; i++)
            {
                TextBox txtCODate1 = (TextBox)dlCODays.Items[i].FindControl("txtCODate");
                NoofDays = NoofDays + 1;
                DateTime date;
                date = clsCommon.Convert2DateTime(txtCODate1.Text.Trim());
                int EID = objUser.GetEmployeeId().ToInt32();
                for (int j = i + 1; j < dlCODays.Items.Count; j++)
                {
                    TextBox txtCODate2 = (TextBox)dlCODays.Items[j].FindControl("txtCODate");
                    if (txtCODate1.Text == txtCODate2.Text)
                    {
                        string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يسمح تواريخ مكررة") : ("Duplicate Dates are not allowed");
                        msgs.InformationalMessage(msg);
                        mpeMessage.Show();
                        return;// break;
                    }
                }

                if (objRequest.CheckComboExists(date, EID, ViewState["RequestId"].ToInt32()) > 0)
                {
                    string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تاريخ دخول") : ("Dates Exist");
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    return;// break;
                }
                ViewState["Table"] = dlCODays.DataSource;
            }
        }
        txtNoComboOff.Text = NoofDays.ToString();
        upnldlCODays.Update();
        upForm.Update();
    }

    protected void chkCO_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            clsLeaveExtensionRequest obj = new clsLeaveExtensionRequest();
            objUser = new clsUserMaster();
            HtmlTableRow trWorkedDay = (HtmlTableRow)fvLeaveRequest.FindControl("trWorkedDay");
            HtmlTableRow trFromComp = (HtmlTableRow)fvLeaveRequest.FindControl("trFromComp");
            CheckBox chkCO = (CheckBox)fvLeaveRequest.FindControl("chkCO");
            TextBox txtBalanceLeave = (TextBox)fvLeaveRequest.FindControl("txtBalanceLeave");
            HtmlTableCell tdBalanceLabel = (HtmlTableCell)fvLeaveRequest.FindControl("tdBalanceLabel");
            TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");

            if (chkCO.Checked)
            {
                trWorkedDay.Style["display"] = "none";
                tdBalanceLabel.Style["display"] = "table-row";
                txtBalanceLeave.Style["display"] = "table-row";
                obj.EmployeeId = objUser.GetEmployeeId();
                if (txtFromDate.Text != "1/1/1900 12:00:00 AM")
                    obj.ExtensionFromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
                else
                    obj.ExtensionFromDate = Convert.ToDateTime("1/1/1900 12:00:00 AM");
                txtBalanceLeave.Text = Convert.ToString(obj.GetCompensatoryOffCount());
            }
            else
            {
                trWorkedDay.Style["display"] = "table-row";
                tdBalanceLabel.Style["display"] = "none";
                txtBalanceLeave.Style["display"] = "none";
            }
            upForm.Update();
        }
        catch { }
    }

    protected void btnEligibleLeaves_Click(object sender, EventArgs e)
    {
        if (fvLeaveRequest.CurrentMode == FormViewMode.Insert | fvLeaveRequest.CurrentMode == FormViewMode.Edit)
        {
            CalculateEligibleVacationLeaves(1);

        }
        if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
        {
            CalculateEligibleVacationLeaves(2);
        }
        if ((fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly && Request.QueryString["Requestid"] != null) || Convert.ToBoolean(ViewState["Approve"]) == true)
        {
            CalculateEligibleVacationLeaves(2);
        }
    }

    private void CalculateEligibleVacationLeaves(int Type)
    {
        TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
        TextBox txtEligibleLeaves = (TextBox)fvLeaveRequest.FindControl("txtEligibleLeaves");
        TextBox txtApprovalFromDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
        TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
        TextBox lblEligibleLeaves = (TextBox)fvLeaveRequest.FindControl("lblEligibleLeaves");
        HtmlTableRow trEligibleLeavesView = (HtmlTableRow)fvLeaveRequest.FindControl("trEligibleLeavesView");

        Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
        Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
        //gets eligible leave days for vacation
        objExtension = new clsLeaveExtensionRequest();
        objUser = new clsUserMaster();
        objRequest = new clsLeaveRequest();
        switch (Type)
        {
            case 1:
                txtEligibleLeaves.Visible = true;

                objRequest.EmployeeId = objUser.GetEmployeeId();
                objRequest.CompanyID = objUser.GetCompanyId();
                objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
                objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtToDate.Text);
                objExtension.EmployeeId = objUser.GetEmployeeId();
                //DataTable dt = objExtension.GetVacationID();
                //if (dt.Rows.Count > 0)
                //    objRequest.VacationID = dt.Rows[0]["VacationID"].ToInt32();
                //else
                objRequest.VacationID = 0;
                txtEligibleLeaves.Text = objRequest.GetEligibleLeavePayDays(false);

                break;
            case 2:
                lblEligibleLeaves.Visible = true;
                trEligibleLeavesView.Style["display"] = "table-row";
                objRequest.RequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                objRequest.EmployeeId = objRequest.GetRequestedById();
                objRequest.CompanyID = objRequest.GetRequestedByCompanyId();
                objExtension.EmployeeId = objUser.GetEmployeeId();
                //DataTable dtVID = objExtension.GetVacationID();
                //if (dtVID.Rows.Count > 0)
                //    objRequest.VacationID = dtVID.Rows[0]["VacationID"].ToInt32();
                //else
                objRequest.VacationID = 0;
                if (lblFromPeriod != null && lblToPeriod != null && lblFromPeriod.Text != string.Empty && lblToPeriod.Text != string.Empty)
                {
                    objRequest.LeaveFromDate = clsCommon.Convert2DateTime(lblFromPeriod.Text);
                    objRequest.LeaveToDate = clsCommon.Convert2DateTime(lblToPeriod.Text);
                }
                else if (txtApprovalFromDate != null && txtApprovalToDate != null && txtApprovalFromDate.Text != string.Empty && txtApprovalToDate.Text != string.Empty)
                {
                    objRequest.LeaveFromDate = clsCommon.Convert2DateTime(txtApprovalFromDate.Text);
                    objRequest.LeaveToDate = clsCommon.Convert2DateTime(txtApprovalToDate.Text);
                }
                lblEligibleLeaves.Text = objRequest.GetEligibleLeavePayDays(false);
                break;
        }
    }

    protected void CalculateTotalNoofVacation(object sender, EventArgs e)
    {
        HiddenField hdLeaveType = (HiddenField)fvLeaveRequest.FindControl("hdLeaveType");
        DropDownList ddlLeaveType = (DropDownList)fvLeaveRequest.FindControl("ddlLeaveType");
        if (hdLeaveType != null)
        {
            if (hdLeaveType.Value.ToInt32() == -1)
            {
                CalculateVacation();
            }
        }
        if (ddlLeaveType != null)
        {
            if (ddlLeaveType.SelectedValue == "-1")
            {
                CalculateVacation();
            }
        }
    }


    private void CalculateVacation()
    {

        HtmlTableRow trToDate = (HtmlTableRow)fvLeaveRequest.FindControl("trToDate");
        HtmlTableRow trTo = (HtmlTableRow)fvLeaveRequest.FindControl("trTo");
        TextBox txtFromDate = (TextBox)fvLeaveRequest.FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
        TextBox lblVacationDays = (TextBox)fvLeaveRequest.FindControl("lblVacationDays");
        HtmlGenericControl divTotalVacation = (HtmlGenericControl)fvLeaveRequest.FindControl("divTotalVacation");
        HtmlGenericControl divApprovalToDate = (HtmlGenericControl)fvLeaveRequest.FindControl("divApprovalToDate");
        //CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash"); //******** Added n 24-7-2015***************///
        HtmlTableRow trTotalVacationView = (HtmlTableRow)fvLeaveRequest.FindControl("trTotalVacationView");
        Label lblFromPeriod = (Label)fvLeaveRequest.FindControl("lblFromPeriod");
        Label lblToPeriod = (Label)fvLeaveRequest.FindControl("lblToPeriod");
        TextBox lblTotalLeaves = (TextBox)fvLeaveRequest.FindControl("lblTotalLeaves");
        TextBox txtapprovalTodate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
        TextBox txtapprovalFromdate = (TextBox)fvLeaveRequest.FindControl("txtApprovalFromDate");
        //CheckBox chkEncash1 = (CheckBox)fvLeaveRequest.FindControl("chkEncash1");
        if (fvLeaveRequest.CurrentMode == FormViewMode.Insert || fvLeaveRequest.CurrentMode == FormViewMode.Edit)
        {
            //if (chkEncash.Checked)                                                        //******** Added n 9-6-2015***************///
            //{
            //    txtToDate.Enabled = false;                                          //******** Added n 9-6-2015***************///
            //    objRequest.IsEncash = true;
            //    lblVacationDays.Enabled = true;
            //    trToDate.Style["display"] = "none";
            //}
            //else
            //{
                txtToDate.Enabled = true;
                lblVacationDays.Enabled = false;//******** Added n 9-6-2015***************///

                divTotalVacation.Style["display"] = "block";

                DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtFromDate.Text.Trim()).ToString("MMM dd yyyy"));
                DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtToDate.Text.Trim()).ToString("MMM dd yyyy"));
                if (dt1 > dt2)
                {

                    txtFromDate.Text = dt1.ToString("dd/MM/yyyy");
                    txtToDate.Text = dt1.ToString("dd/MM/yyyy");
                    DateTime dt3 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtFromDate.Text.Trim()).ToString("MMM dd yyyy"));
                    DateTime dt4 = Convert.ToDateTime(clsCommon.Convert2DateTime(txtToDate.Text.Trim()).ToString("MMM dd yyyy"));
                    TimeSpan ts = dt4.Subtract(dt3);
                    lblVacationDays.Text = (ts.Days + 1).ToString();
                    Session["VacationDays"] = lblVacationDays.Text;
                }
                else
                {
                    TimeSpan ts = dt2.Subtract(dt1);
                    lblVacationDays.Text = (ts.Days + 1).ToString();
                    Session["VacationDays"] = lblVacationDays.Text;
                }
                // *******Added on 6/9/2015*********//
                // objRequest.NumberOfDays = Convert.ToInt32(lblVacationDays.Text);
            //}
        }
        else if (fvLeaveRequest.CurrentMode == FormViewMode.ReadOnly)
        {
            //if (chkEncash1.Checked)                                                        //******** Added n 9-6-2015***************///
            //{
            //    trTotalVacationView.Style["display"] = "table-row";
            //    txtapprovalTodate.Enabled = false;                                          //******** Added n 9-6-2015***************///
            //    objRequest.IsEncash = true;
            //    lblTotalLeaves.Enabled = true;
            //    trTo.Style["display"] = "none";


            //}
            //else
            //{
                txtapprovalTodate.Enabled = true;
                lblTotalLeaves.Enabled = false;//******** Added n 9-6-2015***************///

                trTotalVacationView.Style["display"] = "table-row";
                DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblToPeriod.Text.Trim()).ToString("MMM dd yyyy"));
                TimeSpan ts = dt2.Subtract(dt1);
                lblTotalLeaves.Text = (ts.Days + 1).ToString();
                Session["VacationDays"] = lblTotalLeaves.Text;     // *******Added on 6/9/2015*********//
                // objRequest.NumberOfDays = Convert.ToInt32(lblVacationDays.Text);
            //}
            //if (lblFromPeriod != null && lblToPeriod != null && lblFromPeriod.Text != string.Empty && lblToPeriod.Text != string.Empty)
            //{
            //    trTotalVacationView.Style["display"] = "table-row";
            //    DateTime dt1 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblFromPeriod.Text.Trim()).ToString("MMM dd yyyy"));
            //    DateTime dt2 = Convert.ToDateTime(clsCommon.Convert2DateTime(lblToPeriod.Text.Trim()).ToString("MMM dd yyyy"));
            //    TimeSpan ts = dt2.Subtract(dt1);
            //    lblTotalLeaves.Text = (ts.Days + 1).ToString();
            //    Session["VacationDays"] = lblTotalLeaves.Text;  // *******Added on 6/9/2015*********//
            //    //objRequest.NumberOfDays = Convert.ToInt32(lblVacationDays.Text);

            //}
        }

    }
    #endregion

   protected void chkEncash_CheckedChanged(object sender, EventArgs e)
    {
    //    TextBox txtToDate = (TextBox)fvLeaveRequest.FindControl("txtToDate");
    //    Literal Literal3 = (Literal)fvLeaveRequest.FindControl("Literal3");
    //    HtmlGenericControl trFlightRequired = (HtmlGenericControl)fvLeaveRequest.FindControl("trFlightRequired");
    //    HtmlGenericControl trSectorRequired = (HtmlGenericControl)fvLeaveRequest.FindControl("trSectorRequired");
    //    HtmlGenericControl trContactAddress = (HtmlGenericControl)fvLeaveRequest.FindControl("trContactAddress");
    //    HtmlGenericControl trTelephoneNo = (HtmlGenericControl)fvLeaveRequest.FindControl("trTelephoneNo");
    //    HtmlTableRow trEncah = (HtmlTableRow)fvLeaveRequest.FindControl("trEncah");
    //    UpdatePanel uppnlEncash = fvLeaveRequest.FindControl("pnlEncash") as UpdatePanel;
    //    HtmlTableRow trToDate = (HtmlTableRow)fvLeaveRequest.FindControl("trToDate");
    //    TextBox lblVacationDays = (TextBox)fvLeaveRequest.FindControl("lblVacationDays");
    //    CheckBox chkEncash = (CheckBox)fvLeaveRequest.FindControl("chkEncash");
    //    if (chkEncash.Checked)
    //    {
    //        trToDate.Style["display"] = "none";
    //        lblVacationDays.Text = "0";
    //        lblVacationDays.Enabled = true;
    //        Literal3.Text = "Encash Date";
    //        trFlightRequired.Style["display"] = "none";
    //        trSectorRequired.Style["display"] = "none";
    //        trContactAddress.Style["display"] = "none";
    //        trTelephoneNo.Style["display"] = "none";
    //        trEncah.Style["display"] = "none";
    //    }
    //    else
    //    {
    //        trToDate.Style["display"] = "inline-grid";
    //        //lblVacationDays.Text = Session["VacationDays"].ToString();
    //        lblVacationDays.Enabled = false;
    //        txtToDate.Enabled = true;
    //        lblVacationDays.Enabled = false;//******** Added n 9-6-2015***************///
    //        CalculateVacation();
    //        Literal3.Text = "Leave From Date";

    //        trFlightRequired.Style["display"] = "block";
    //        trFlightRequired.Style["width"] = "272%";
    //        trSectorRequired.Style["display"] = "block";
    //        trSectorRequired.Style["width"] = "272%";
    //        trContactAddress.Style["display"] = "block";
    //        trContactAddress.Style["width"] = "272%";
    //        trTelephoneNo.Style["display"] = "block";
    //        trTelephoneNo.Style["width"] = "272%";
    //        trEncah.Style["display"] = "table-row";
    //    }
    //    upForm.Update();

    }

   protected void chkEncash1_CheckedChanged(object sender, EventArgs e)
    {
    //    TextBox txtApprovalToDate = (TextBox)fvLeaveRequest.FindControl("txtApprovalToDate");
    //    UpdatePanel uppnlEncash = fvLeaveRequest.FindControl("pnlEncash") as UpdatePanel;
    //    HtmlTableRow trTo = (HtmlTableRow)fvLeaveRequest.FindControl("trTo");
    //    HtmlTableRow trTravelDocument = (HtmlTableRow)fvLeaveRequest.FindControl("trTravelDocument");
    //    HtmlTableRow trFlightRequiredView = (HtmlTableRow)trTravelDocument.FindControl("trFlightRequiredView");
    //    HtmlTableRow trSectorRequiredView = (HtmlTableRow)trTravelDocument.FindControl("trSectorRequiredView");
    //    HtmlTableRow trContactAddressView = (HtmlTableRow)trTravelDocument.FindControl("trContactAddressView");
    //    HtmlTableRow trTelephoneNoView = (HtmlTableRow)trTravelDocument.FindControl("trTelephoneNoView");
    //    HtmlTableRow trPassportNo = (HtmlTableRow)trTravelDocument.FindControl("trPassportNo");
    //    HtmlTableRow trPassportExpiryDate = (HtmlTableRow)trTravelDocument.FindControl("trPassportExpiryDate");
    //    HtmlTableRow trLCExpiry = (HtmlTableRow)trTravelDocument.FindControl("trLCExpiry");
    //    HtmlTableRow trVisaExpiry = (HtmlTableRow)trTravelDocument.FindControl("trVisaExpiry");
    //    HtmlTableRow trEncashView = (HtmlTableRow)trTravelDocument.FindControl("trEncashView");

    //    Literal Literal9 = (Literal)fvLeaveRequest.FindControl("Literal9");



    //    TextBox lblTotalLeaves = (TextBox)fvLeaveRequest.FindControl("lblTotalLeaves");
    //    CheckBox chkEncash1 = (CheckBox)fvLeaveRequest.FindControl("chkEncash1");
    //    if (chkEncash1.Checked)
    //    {
    //        trTo.Style["display"] = "none";

    //        lblTotalLeaves.Enabled = true;
    //        lblTotalLeaves.Text = Session["VacationDays"].ToString();
    //        //trTravelDocument.Style["display"] = "none";
    //        trFlightRequiredView.Style["display"] = "none";
    //        trSectorRequiredView.Style["display"] = "none";
    //        trContactAddressView.Style["display"] = "none";
    //        trTelephoneNoView.Style["display"] = "none";
    //        trPassportNo.Style["display"] = "none";
    //        trPassportExpiryDate.Style["display"] = "none";
    //        trLCExpiry.Style["display"] = "none";
    //        trVisaExpiry.Style["display"] = "none";
    //        trEncashView.Style["display"] = "none";
    //        Literal9.Text = "Encash Date";
    //    }
    //    else
    //    {
    //        trTo.Style["display"] = "inline-grid";
    //        //lblVacationDays.Text = Session["VacationDays"].ToString();
    //        lblTotalLeaves.Enabled = false;
    //        txtApprovalToDate.Enabled = true;
    //        lblTotalLeaves.Enabled = false;//******** Added n 9-6-2015***************///
    //        CalculateVacation();
    //        //trTravelDocument.Style["display"] = "block";
    //        trFlightRequiredView.Style["display"] = "table-row";
    //        trSectorRequiredView.Style["display"] = "table-row";
    //        trContactAddressView.Style["display"] = "table-row";
    //        trTelephoneNoView.Style["display"] = "table-row";
    //        trPassportNo.Style["display"] = "table-row";
    //        trPassportExpiryDate.Style["display"] = "table-row";
    //        trLCExpiry.Style["display"] = "table-row";
    //        trVisaExpiry.Style["display"] = "table-row";
    //        trEncashView.Style["display"] = "table-row";
    //        Literal9.Text = "Leave From Date";

    //    }
    //    //uppnlEncash.Update();
    //    upDetails.Update();
    }

}

