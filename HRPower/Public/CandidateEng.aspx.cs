﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Reflection;

public partial class Public_CandidateEng : System.Web.UI.Page
{
    clsCandidate objclsCandidate;
    clsUserMaster objclsUserMaster;
    clsRoleSettings objRoleSettings;

    private string CurrentSelectedValue
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            if (clsGlobalization.IsArabicViewEnabled())
            {
                txtArabicFirstName.BackColor = System.Drawing.SystemColors.Info;
                rfvArbFirstName.ValidationGroup = "submit";
            }
            objRoleSettings = new clsRoleSettings();
            objclsUserMaster = new clsUserMaster();
            //lbl.Text = clsGlobalization.IsArabicCulture() ? "الحقول التي تحمل علامة * إلزامية" : "Fields marked with * are mandatory";
            if (!objRoleSettings.IsMenuEnabledForInterviewer(objclsUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsCreate"))
            {
                 msgs.WarningMessage(GetGlobalResourceObject("ControlsCommon","NoPermission").ToString());
                 mpeMessage.Show();
                 lnkAddCandidate.Enabled = lnkListCandidate.Enabled = false ;
                 divCandidate.Style["display"] = "none";
            }
            else
            {
                divCandidate.Style["display"] = "block";
                lnkAddCandidate.Enabled = lnkListCandidate.Enabled = true;
                if (Request.QueryString["CandidateID"] == null)
                    AddNew();
                else
                {
                    long lngCandidateID = Request.QueryString["CandidateID"].ToInt64();

                    if (lngCandidateID > 0)
                        getCandidateDetails(Request.QueryString["CandidateID"].ToInt64());
                    else
                        AddNew();
                }
            }
            SetPermissions();
        }

        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        AgencyReference.OnSave += new Controls_AgencyReference.OnUpdate(AgencyReference_OnSave);
        ucDegreeReference.OnSave -= new controls_DegreeReference.SaveDegree(DegreeReference_OnSave);
        ucDegreeReference.OnSave += new controls_DegreeReference.SaveDegree(DegreeReference_OnSave);
      
    }
    void AgencyReference_OnSave(DataTable dt)
    {
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod("FillComboRefferedBy");
        theMethod.Invoke(this, null);
    }
   
    void DegreeReference_OnSave(int SelectedValue)
    {
        BindDegrees();
        if (SelectedValue >0)
        rcDegree.SelectedValue = SelectedValue.ToString();
        uprcDegree.Update();
    }

    private void BindDegrees()
    {
        objclsCandidate = new clsCandidate();
        objclsUserMaster = new clsUserMaster();
        objclsCandidate.CompanyID = objclsUserMaster.GetCompanyId();
        DataSet dsTemp = objclsCandidate.LoadCombos();
        string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (dsTemp.Tables[7] != null)
        {
            rcDegree.DataSource = dsTemp.Tables[7];
            rcDegree.DataTextField = "Qualification";
            rcDegree.DataValueField = "DegreeID";
            rcDegree.DataBind();
            rcDegree.Items.Insert(0, new ListItem(str, "-1"));
            uprcDegree.Update();
        }
    }
    private void SetPermissions()
    {
        lnkAddCandidate.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objclsUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsCreate");
        lnkListCandidate.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objclsUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsView");
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }
    protected void lnkAddCandidate_Click(object sender, EventArgs e)
    {
        ClearPhoto();
        AddNew();
    }

    private void AddNew()
    {
        RemoveQueryString("CandidateID");
        RemoveQueryString("type");
        objclsCandidate = new clsCandidate();
        objclsCandidate.CandidateID = 0;
        ViewState["CandidateID"] = "0";
       
        Session["Photo"] = null;
        RemovePhoto();
     
        getCandidateCode();
        LoadCombos();
        LoadGraduationYear();

        imgPreview.Src = "";

        // Basic Info
        rblGender.SelectedValue = "0";
       
        txtEnglishFirstName.Text = string.Empty;
        txtEnglishSecondName.Text = string.Empty;
        txtEnglishThirdName.Text = string.Empty;
        txtArabicFirstName.Text = string.Empty;
        txtArabicSecondName.Text = string.Empty;
        txtArabicThirdName.Text = string.Empty;
        txtCitizenship.Text = string.Empty;
        txtPlaceOfBirth.Text = string.Empty;
        txtDateofBirth.Text = string.Empty;
        txtNoOfSon.Text = string.Empty;
        txtMotherName.Text = string.Empty;
        txtDoctrine.Text = string.Empty;
        txtCurrentAddress.Text = string.Empty;
        txtCurrentPOBox.Text = string.Empty;
        txtCurrentTelephoneNumber.Text = string.Empty;
        txtBasicBusinessPairAddress.Text = string.Empty;
        txtPlaceofBusinessPair.Text = string.Empty;

        // Professional Info
        txtPreviousJob.Text = string.Empty;
        txtPlaceofGraduation.Text = string.Empty;
        txtCollegeSchool.Text = string.Empty;
        txtSector.Text = string.Empty;
        txtPosition.Text = string.Empty;
        txtTypeofExperience.Text = string.Empty;
        txtCompensation.Text = string.Empty;
        txtSpecialization.Text = string.Empty;
        txtDateofTransactionEntry.Text = string.Empty;
        txtExpectedJoinDate.Text = string.Empty;
        rcDegree.SelectedIndex = -1;
       // txtGraduation.Text = "";

        // Document Info
        txtPassportNumber.Text = string.Empty;
        txtPlaceofPassportIssue.Text = string.Empty;
        txtPassportExpiryDate.Text = string.Empty;
        txtPassportReleaseDate.Text = string.Empty;
        txtVisaNumber.Text = string.Empty;
        txtUnifiedNumber.Text = string.Empty;
        txtVisaPlaceofIssue.Text = string.Empty;
        txtVisaExpiryDate.Text = string.Empty;
        txtVisaReleaseDate.Text = string.Empty;
        txtNationalCardNumber.Text = string.Empty;
        txtNationalCardExpiryDate.Text = string.Empty;

        // Other Info
        txtPermenantAddress.Text = string.Empty;
        txtHomeStreet.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtHomeTelephone.Text = string.Empty;
        txtBusinessAddress.Text = string.Empty;
        txtArea.Text = string.Empty;
        txtPermenantPOBox.Text = string.Empty;
        txtFax.Text = string.Empty;
        txtPhoneNumber.Text = string.Empty;
        txtMobileNumber.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtInsuranceNumber.Text = string.Empty;
        //txtOtherBusinessPair.Text = string.Empty;
        txtmyDate.Text = string.Empty;
        //txtPassword.Text = string.Empty;
        txtBusinessStreet.Text = "";

        // Languages
        chlArabic.Items.Clear();
        string strWVal = clsGlobalization.IsArabicCulture() ? "كتابة" : "Writing";
        string strRVal = clsGlobalization.IsArabicCulture() ? "قراءة" : "Reading";
        chlArabic.Items.Add( new ListItem(strRVal, "0"));
        chlArabic.Items.Add(new ListItem(strWVal ,"1"));
        chlEnglish.Items.Clear();
        chlEnglish.Items.Add(new ListItem(strRVal, "0"));
        chlEnglish.Items.Add(new ListItem(strWVal, "1"));
        chlOtherLanguages.Items.Clear();
        chlOtherLanguages.Items.Add(new ListItem(strRVal, "0"));
        chlOtherLanguages.Items.Add(new ListItem(strWVal, "1"));
        txtOtherLanguages.Text = string.Empty;

        //txtPassword.Text = txtCandidateCode.Text;
        upfvCandidate.Update();


        ViewState["Documents"] = null;


        GetCandidateAttachedDocuments(0);
    }

    private void RemoveQueryString(string Query)
    {
        PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isReadOnly.SetValue(this.Request.QueryString, false, null);

        Request.QueryString.Remove(Query);

        Response.ClearContent();
    }

    private void getCandidateCode()
    {
        Random rd = new Random();
        txtCandidateCode.Text = rd.Next(1, 99999999).ToStringCustom();
    }

    public void FillComboSalutation()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[2] != null)
        {
            rcSalutation.DataSource = dsTemp.Tables[2];
            rcSalutation.DataTextField = "Salutation";
            rcSalutation.DataValueField = "SalutationID";
         
            rcSalutation.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            rcSalutation.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.rcSalutation);
            this.updSalutation.Update();
        }

    }
    public void FillComboReligion()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[4] != null)
        {

            rcReligion.DataSource = dsTemp.Tables[4];
            rcReligion.DataTextField = "Religion";
            rcReligion.DataValueField = "ReligionID";
            rcReligion.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            rcReligion.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.rcReligion);
            this.updReligion.Update();
        }
    }
    public void FillComboCountryOfOrigin()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[5] != null)
        {
            DataTable datTemp1 = dsTemp.Tables[5];
            rcCountry.DataSource = datTemp1;
            rcCountry.DataTextField = "CountryName";
            rcCountry.DataValueField = "CountryID";
            rcCountry.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            rcCountry.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.rcCountry);

            updCountry.Update();
        }
    }

    public void FillComboNationality()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[9] != null)
        {
            DataTable datTemp1 = dsTemp.Tables[9];

            ddlCurrentNationality.DataSource = datTemp1;
            ddlCurrentNationality.DataTextField = "Nationality";
            ddlCurrentNationality.DataValueField = "NationalityID";
            ddlCurrentNationality.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlCurrentNationality.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlCurrentNationality);
            this.updCurrentNationality.Update();
        }
    }

    public void FillComboQualificationCategory()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[10] != null)
        {
            if (dsTemp.Tables[10] != null)
            {
                ddlQualificationCategory.DataSource = dsTemp.Tables[10];
                ddlQualificationCategory.DataTextField = "QualificationCategory";
                ddlQualificationCategory.DataValueField = "QualificationCategoryID";
                ddlQualificationCategory.DataBind();
                string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                ddlQualificationCategory.Items.Insert(0, new ListItem(str, "-1"));

                this.SetSelectedIndex(this.ddlQualificationCategory);
                updQualificationCategory.Update();
            }
        }
    }

    public void FillComboCompanyType()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[6] != null)
        {
            DataTable datTemp1 = dsTemp.Tables[6];

            ddlCompanyType.DataSource = datTemp1;
            ddlCompanyType.DataTextField = "CompanyType";
            ddlCompanyType.DataValueField = "CompanyTypeID";
            ddlCompanyType.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlCompanyType.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlCompanyType);
            updCompanyType.Update();


        }

    }

    public void FillComboPNationality()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[9] != null)
        {
            DataTable datTemp2 = dsTemp.Tables[9];
            ddlPreviousNationality.DataSource = datTemp2;
            ddlPreviousNationality.DataTextField = "Nationality";
            ddlPreviousNationality.DataValueField = "NationalityID";
            ddlPreviousNationality.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlPreviousNationality.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlPreviousNationality);
            updPreviousNationality.Update();
        }
    }

    public void FillComboPassCountryOfOrigin()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[5] != null)
        {
            DataTable datTemp2 = dsTemp.Tables[5];
            ddlPassportCountry.DataSource = datTemp2;
            ddlPassportCountry.DataTextField = "CountryName";
            ddlPassportCountry.DataValueField = "CountryID";
            ddlPassportCountry.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlPassportCountry.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlPassportCountry);
            updPassportCountry.Update();
        }
    }

    public void FillComboVisaCountryOfOrigin()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[5] != null)
        {
            DataTable datTemp3 = dsTemp.Tables[5];
            ddlVisaCountry.DataSource = datTemp3;
            ddlVisaCountry.DataTextField = "CountryName";
            ddlVisaCountry.DataValueField = "CountryID";
            ddlVisaCountry.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlVisaCountry.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlVisaCountry);


            updVisaCountry.Update();
        }
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }
    public void FillComboRefferalType()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp.Tables[19] != null)
        {
            ddlReferraltype.DataSource = dsTemp.Tables[19];
            ddlReferraltype.DataTextField = "RefferalType";
            ddlReferraltype.DataValueField = "RefferalTypeID";
            ddlReferraltype.DataBind();
            string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlReferraltype.Items.Insert(0, new ListItem(str, "-1"));

            this.SetSelectedIndex(this.ddlReferraltype);

            
        }

    }

    private void LoadCombos()
    {
        objclsCandidate = new clsCandidate();
        objclsUserMaster = new clsUserMaster();
        objclsCandidate.CompanyID = objclsUserMaster.GetCompanyId();
        DataSet dsTemp = objclsCandidate.LoadCombos();
        string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (dsTemp != null)
        {
            if (dsTemp.Tables[0] != null)
            {
                ddlJob.DataSource = dsTemp.Tables[0];
                ddlJob.DataTextField = "JobCode";
                ddlJob.DataValueField = "JobId";
                ddlJob.DataBind();
                ;
                ddlJob.Items.Insert(0, new ListItem(str, "-1"));
                updJob.Update();
            }

            if (dsTemp.Tables[19] != null)
            {
                ddlReferraltype.DataSource = dsTemp.Tables[19];
                ddlReferraltype.DataTextField = "RefferalType";
                ddlReferraltype.DataValueField = "RefferalTypeID";
                ddlReferraltype.DataBind();
              
                ddlReferraltype.Items.Insert(0, new ListItem(str, "-1"));
                ddlReferredBy.Items.Clear();
                ddlReferredBy.Items.Insert(0, new ListItem(str, "-1"));
               
            }
            //if (dsTemp.Tables[1] != null)
            //{
            //    ddlReferredBy.DataSource = dsTemp.Tables[1];
            //    ddlReferredBy.DataTextField = "Agency";
            //    ddlReferredBy.DataValueField = "AgencyId";
            //    ddlReferredBy.DataBind();
            //    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));

            //}

            if (dsTemp.Tables[2] != null)
            {
                rcSalutation.DataSource = dsTemp.Tables[2];
                rcSalutation.DataTextField = "Salutation";
                rcSalutation.DataValueField = "SalutationID";
                rcSalutation.DataBind();
               
                rcSalutation.Items.Insert(0, new ListItem(str, "-1"));
                updSalutation.Update();
            }

            if (dsTemp.Tables[3] != null)
            {
                ddlMaritalStatus.DataSource = dsTemp.Tables[3];
                ddlMaritalStatus.DataTextField = "MaritalStatus";
                ddlMaritalStatus.DataValueField = "MaritalStatusID";
                ddlMaritalStatus.DataBind();
               
                ddlMaritalStatus.Items.Insert(0, new ListItem(str, "-1"));
                updMaritalStatus.Update();
            }

            if (dsTemp.Tables[4] != null)
            {
                rcReligion.DataSource = dsTemp.Tables[4];
                rcReligion.DataTextField = "Religion";
                rcReligion.DataValueField = "ReligionID";
                rcReligion.DataBind();
              
                rcReligion.Items.Insert(0, new ListItem(str, "-1"));
                updReligion.Update();
            }

            if (dsTemp.Tables[5] != null)
            {
                DataTable datTemp1 = dsTemp.Tables[5];
                rcCountry.DataSource = datTemp1;
                rcCountry.DataTextField = "CountryName";
                rcCountry.DataValueField = "CountryID";
                rcCountry.DataBind();
              
                rcCountry.Items.Insert(0, new ListItem(str, "-1"));
                updCountry.Update();

                DataTable datTemp2 = dsTemp.Tables[5];
                ddlPassportCountry.DataSource = datTemp2;
                ddlPassportCountry.DataTextField = "CountryName";
                ddlPassportCountry.DataValueField = "CountryID";
                ddlPassportCountry.DataBind();
                ddlPassportCountry.Items.Insert(0, new ListItem(str, "-1"));
                updPassportCountry.Update();

                DataTable datTemp3 = dsTemp.Tables[5];
                ddlVisaCountry.DataSource = datTemp3;
                ddlVisaCountry.DataTextField = "CountryName";
                ddlVisaCountry.DataValueField = "CountryID";
                ddlVisaCountry.DataBind();
                ddlVisaCountry.Items.Insert(0, new ListItem(str, "-1"));
                updVisaCountry.Update();
            }

            if (dsTemp.Tables[6] != null)
            {
                DataTable datTemp1 = dsTemp.Tables[6];

                ddlCompanyType.DataSource = datTemp1;
                ddlCompanyType.DataTextField = "CompanyType";
                ddlCompanyType.DataValueField = "CompanyTypeID";
                ddlCompanyType.DataBind();
                ddlCompanyType.Items.Insert(0, new ListItem(str, "-1"));
                updCompanyType.Update();

                //DataTable datTemp2 = dsTemp.Tables[6];
                //ddlOtherCompanyType.DataSource = datTemp2;
                //ddlOtherCompanyType.DataTextField = "CompanyType";
                //ddlOtherCompanyType.DataValueField = "CompanyTypeID";
                //ddlOtherCompanyType.DataBind();
                //ddlOtherCompanyType.Items.Insert(0, new ListItem("Select", "-1"));
                //updOtherCompanyType.Update();
            }

            if (dsTemp.Tables[7] != null)
            {
                rcDegree.DataSource = dsTemp.Tables[7];
                rcDegree.DataTextField = "Qualification";
                rcDegree.DataValueField = "DegreeID";
                rcDegree.DataBind();
                rcDegree.Items.Insert(0, new ListItem(str, "-1"));
                uprcDegree.Update();
            }

            

            if (dsTemp.Tables[9] != null)
            {
                DataTable datTemp1 = dsTemp.Tables[9];

                ddlCurrentNationality.DataSource = datTemp1;
                ddlCurrentNationality.DataTextField = "Nationality";
                ddlCurrentNationality.DataValueField = "NationalityID";
                ddlCurrentNationality.DataBind();
                ddlCurrentNationality.Items.Insert(0, new ListItem(str, "-1"));
                updCurrentNationality.Update();

                DataTable datTemp2 = dsTemp.Tables[9];
                ddlPreviousNationality.DataSource = datTemp2;
                ddlPreviousNationality.DataTextField = "Nationality";
                ddlPreviousNationality.DataValueField = "NationalityID";
                ddlPreviousNationality.DataBind();
                ddlPreviousNationality.Items.Insert(0, new ListItem(str, "-1"));
                updPreviousNationality.Update();
            }

            if (dsTemp.Tables[10] != null)
            {
                ddlQualificationCategory.DataSource = dsTemp.Tables[10];
                ddlQualificationCategory.DataTextField = "QualificationCategory";
                ddlQualificationCategory.DataValueField = "QualificationCategoryID";
                ddlQualificationCategory.DataBind();
                ddlQualificationCategory.Items.Insert(0, new ListItem(str, "-1"));
                updQualificationCategory.Update();
            }
        }
    }

    private void LoadGraduationYear()
    {
        string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlDegreeCompletionYear.Items.Clear();
        ddlDegreeCompletionYear.Items.Insert(0, new ListItem(str, "-1"));

        for (int iCounter = DateTime.Now.Year - 30; iCounter <= DateTime.Now.Year + 1; iCounter++)
            ddlDegreeCompletionYear.Items.Add(Convert.ToString(iCounter));
    }

    
    protected void btnCountry_Click(object sender, EventArgs e)
    {
        if (rcCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "FillComboCountryOfOrigin";
            ReferenceControlNew1.SelectedValue = rcCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Country.Text").ToString();
            //ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnPassportCountry_Click(object sender, EventArgs e)
    {
        if (ddlPassportCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "FillComboPassCountryOfOrigin";
            ReferenceControlNew1.SelectedValue = ddlPassportCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Country.Text").ToString();
           // ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnVisaCountry_Click(object sender, EventArgs e)
    {
        if (ddlVisaCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "FillComboVisaCountryOfOrigin";
            ReferenceControlNew1.SelectedValue = ddlVisaCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Country.Text").ToString();


            //ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnReligion_Click(object sender, EventArgs e)
    {
        if (rcReligion != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "ReligionReference";
            ReferenceControlNew1.DataTextField = "Religion";
            ReferenceControlNew1.DataTextFieldArabic = "ReligionArb";
            ReferenceControlNew1.DataValueField = "ReligionId";
            ReferenceControlNew1.FunctionName = "FillComboReligion";
            ReferenceControlNew1.SelectedValue = rcReligion.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Religion.Text").ToString();
            //ReferenceControlNew1.DataTextFieldArabic = "ReligionArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnSalutation_Click(object sender, EventArgs e)
    {
        if (rcSalutation != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "SalutationReference";
            ReferenceControlNew1.DataTextField = "Salutation";
            ReferenceControlNew1.DataTextFieldArabic = "SalutationArb";
            ReferenceControlNew1.DataValueField = "SalutationId";
            ReferenceControlNew1.FunctionName = "FillComboSalutation";
            ReferenceControlNew1.SelectedValue = rcSalutation.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Salutation.Text").ToString();
           // ReferenceControlNew1.DataTextFieldArabic = "SalutationArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }





    protected void btnDegree_Click(object sender, EventArgs e)
    {
        AddNewDegree();
    }

    public void AddNewDegree()
    {
        ucDegreeReference.FillList();
        mpeDegreeReference.Show();
        upDegreeReference.Update();       
       
    }

    protected void btnQualificationCategory_Click(object sender, EventArgs e)
    {
        if (ddlQualificationCategory != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "QualificationCategoryReference";
            ReferenceControlNew1.DataTextField = "QualificationCategory";
            ReferenceControlNew1.DataTextFieldArabic = "QualificationCategoryArb";
            ReferenceControlNew1.DataValueField = "QualificationCategoryID";
            ReferenceControlNew1.FunctionName = "FillComboQualificationCategory";
            ReferenceControlNew1.SelectedValue = ddlQualificationCategory.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("Category.Text").ToString();
           // ReferenceControlNew1.DataTextFieldArabic = "QualificationCategoryArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnCompanyType_Click(object sender, EventArgs e)
    {
        if (ddlCompanyType != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CompanyTypeReference";
            ReferenceControlNew1.DataTextField = "CompanyType";
            ReferenceControlNew1.DataTextFieldArabic = "CompanyTypeArb";
            ReferenceControlNew1.DataValueField = "CompanyTypeID";
            ReferenceControlNew1.FunctionName = "FillComboCompanyType";
            ReferenceControlNew1.SelectedValue = ddlCompanyType.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("CompanyType.Text").ToString();
            //ReferenceControlNew1.DataTextFieldArabic = "CompanyTypeArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnOtherCompanyType_Click(object sender, EventArgs e)
    {
        //if (ddlOtherCompanyType != null)
        //{
        //    ReferenceControlNew1.ClearAll();
        //    ReferenceControlNew1.TableName = "CompanyTypeReference";
        //    ReferenceControlNew1.DataTextField = "CompanyType";
        //    ReferenceControlNew1.DataValueField = "CompanyTypeID";
        //    ReferenceControlNew1.FunctionName = "FillComboCompanyType";
        //    ReferenceControlNew1.SelectedValue = ddlOtherCompanyType.SelectedValue;
        //    ReferenceControlNew1.DisplayName = "CompanyType";
        //    ReferenceControlNew1.PopulateData();

        //    mdlPopUpReference.Show();
        //    updModalPopUp.Update();
        //}
    }
    protected void btnCancelSubmit_Click(object sender, EventArgs e)
    {
        Response.Redirect("Candidates.aspx");
    }

    protected void btnRefferalType_Click(object sender, EventArgs e)
    {
        if (ddlReferraltype != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "RefferalTypeReference";
            ReferenceControlNew1.DataTextField = "RefferalType";
            ReferenceControlNew1.DataTextFieldArabic = "RefferalTypeArb";
            ReferenceControlNew1.DataValueField = "RefferalTypeID";
            ReferenceControlNew1.FunctionName = "FillComboRefferalType";
            ReferenceControlNew1.SelectedValue = ddlReferraltype.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("ReferralType.Text").ToString();
            //ReferenceControlNew1.DataTextFieldArabic = "RefferalTypeArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnCurrentNationality_Click(object sender, EventArgs e)
    {
        if (ddlCurrentNationality != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "NationalityReference";
            ReferenceControlNew1.DataTextField = "Nationality";
            ReferenceControlNew1.DataTextFieldArabic = "NationalityArb";
            ReferenceControlNew1.DataValueField = "NationalityID";
            ReferenceControlNew1.FunctionName = "FillComboNationality";
            ReferenceControlNew1.SelectedValue = ddlCurrentNationality.SelectedValue;
            ReferenceControlNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Nationality").ToString();
           // ReferenceControlNew1.DataTextFieldArabic = "NationalityArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnPreviousNationality_Click(object sender, EventArgs e)
    {
        if (ddlPreviousNationality != null)
        {
            ReferenceControlNew1.TableName = "NationalityReference";
            ReferenceControlNew1.DataTextField = "Nationality";
            ReferenceControlNew1.DataTextFieldArabic = "NationalityArb";
            ReferenceControlNew1.DataValueField = "NationalityID";
            ReferenceControlNew1.FunctionName = "FillComboPNationality";
            ReferenceControlNew1.SelectedValue = ddlPreviousNationality.SelectedValue;
            ReferenceControlNew1.DisplayName = GetGlobalResourceObject("ControlsCommon", "Nationality").ToString();
           // ReferenceControlNew1.DataTextFieldArabic = "NationalityArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (FormValidation())
        {
            SaveCandidate();
            RemovePhoto();
        }
    }

    private bool FormValidation()
    {
        bool isValidate = true;
        string Message = "";
        string strScript = "";
        DateTime T = DateTime.Now;

        objclsCandidate = new clsCandidate();
        objclsCandidate.CandidateID = ViewState["CandidateID"].ToInt64();
        objclsCandidate.DateofBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()).ToString("dd MMM yyyy");
        objclsCandidate.CandidateCode = txtCandidateCode.Text.Trim();
        objclsCandidate.PassportNumber = txtPassportNumber.Text.Trim();

        if (ddlJob.SelectedIndex == -1 || rcSalutation.SelectedIndex == -1 || ddlMaritalStatus.SelectedIndex == -1 || rcReligion.SelectedIndex == -1 ||
            ddlCurrentNationality.SelectedIndex == -1 || rcCountry.SelectedIndex == -1 ||
            txtCandidateCode.Text == string.Empty || txtEnglishFirstName.Text == string.Empty || txtEnglishSecondName.Text == string.Empty ||
            txtEnglishThirdName.Text == string.Empty || txtDateofBirth.Text == string.Empty ||
            txtPlaceOfBirth.Text == string.Empty || txtCurrentTelephoneNumber.Text == string.Empty ||
            txtCurrentAddress.Text == string.Empty)
        {
            isValidate = false;
            Message =  clsGlobalization.IsArabicCulture() ? "بعض الحقول الإلزامية مفقودة!": "Some mandatory fields are missing!!!";
            strScript = "pnlTab1";
        }


        if (!(DateTime.TryParseExact(txtDateofBirth.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        {
            isValidate = false;
            Message = clsGlobalization.IsArabicCulture() ? "(تاريخ الميلاد غير صالح (تنسيق صالحة: اليوم / الشهر / السنة" : "Invalid date of birth(valid format:dd/MM/yyyy)";
            strScript = "pnlTab1";

        }

        else if (clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()) > clsCommon.Convert2DateTime(DateTime.Now.Date.AddYears(-18).Date.ToString("dd/MM/yyyy")))
        {
            isValidate = false;
            Message =  clsGlobalization.IsArabicCulture() ? "يجب أن يكون العمر أكبر من أو يساوي 18 سنة" : "Age must be greater than or equal to 18 years!!!";
            strScript = "pnlTab1";
        }

        else if (txtNoOfSon.Text.Trim().ToInt32() < 0)
        {
            isValidate = false;
            Message = clsGlobalization.IsArabicCulture() ? "!!عدد الابن لا ينبغي أن يكون أقل من 0" :"No. of son should not be less than 0!!!";
            strScript = "pnlTab1";
        }

        else if (rcDegree.SelectedIndex == 0 || ddlQualificationCategory.SelectedIndex == 0)
        {
            isValidate = false;
            Message = clsGlobalization.IsArabicCulture() ? "بعض الحقول الإلزامية مفقودة!" : "Some mandatory fields are missing!!!";
            strScript = "pnlTab2";
        }

        //else if (ddlPassportCountry.SelectedIndex == -1 || txtPassportNumber.Text == string.Empty || txtPlaceofPassportIssue.Text == string.Empty ||
        //    txtPassportExpiryDate.Text == string.Empty || txtPassportReleaseDate.Text == string.Empty)
        //{
        //    isValidate = false;
        //    Message = "Some mandatory fields are missing!!!";
        //    strScript = "pnlTab3";
        //}

        //else if (objclsCandidate.CheckExistsCandidatePassport())
        //{
        //    isValidate = false;
        //    Message = "Passport Number is already exists!!!";
        //    strScript = "pnlTab3";
        //}

        //else if (!(DateTime.TryParseExact(txtPassportExpiryDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        //{
        //    isValidate = false;
        //    Message = "Invalid passport expiry date format(valid format:dd/MM/yyyy)";
        //    strScript = "pnlTab1";

        //}

        //else if (!(DateTime.TryParseExact(txtPassportReleaseDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        //{
        //    isValidate = false;
        //    Message = "Invalid passport issue date format(valid format:dd/MM/yyyy)";
        //    strScript = "pnlTab1";

        //}


        //else if (clsCommon.Convert2DateTime(txtPassportExpiryDate.Text.Trim()) < clsCommon.Convert2DateTime(txtPassportReleaseDate.Text.Trim()))
        //{
        //    isValidate = false;
        //    Message = "Passport expiry date must be greater than the issue date!!!";
        //    strScript = "pnlTab3";
        //}

        else if (txtEmail.Text == string.Empty)
        {
            isValidate = false;
            Message = clsGlobalization.IsArabicCulture() ? "بعض الحقول الإلزامية مفقودة!" : "Some mandatory fields are missing!!!";
            strScript = "pnlTab4";
        }

        if (!isValidate)
        {
            msgs.InformationalMessage(Message);
            this.mpeMessage.Show();
            ScriptManager.RegisterClientScriptBlock(btnSubmit, btnSubmit.GetType(), Guid.NewGuid().ToString(), "setTabSelected('" + strScript + "'); ", true);
            return false;
        }

        return true;
    }

    public void SaveCandidate()
    {
        //System.Threading.Thread.Sleep(1000);

        objclsCandidate = new clsCandidate();
        objclsUserMaster = new clsUserMaster();
        
        try
        {
            objclsCandidate.CandidateID = ViewState["CandidateID"].ToInt64();
            // Basic Info
            if(ddlJob.SelectedValue != "-1") 
                objclsCandidate.JobID = ddlJob.SelectedValue.ToInt32();
          
            if(ddlReferraltype.SelectedValue != "-1")
                objclsCandidate.ReferralType = ddlReferraltype.SelectedValue.ToInt32();
            if (ddlReferredBy.SelectedValue != "-1")
                objclsCandidate.ReferredBy = ddlReferredBy.SelectedValue.ToInt32();

            objclsCandidate.SalutationID = rcSalutation.SelectedValue.ToInt32();
            objclsCandidate.FirstNameEng = txtEnglishFirstName.Text.Trim();
            objclsCandidate.SecondNameEng = txtEnglishSecondName.Text.Trim();
            objclsCandidate.ThirdNameEng = txtEnglishThirdName.Text.Trim();
            objclsCandidate.FirstNameArb = txtArabicFirstName.Text.Trim();
            objclsCandidate.SecondNameArb = txtArabicSecondName.Text.Trim();
            objclsCandidate.ThirdNameArb = txtArabicThirdName.Text.Trim();
            objclsCandidate.Citizenship = txtCitizenship.Text.Trim();
            objclsCandidate.PlaceofBirth = txtPlaceOfBirth.Text.Trim();

            if (txtDateofBirth.Text.Trim() != "")
                objclsCandidate.DateofBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.DateofBirth = "";

            objclsCandidate.Gender = (rblGender.SelectedValue == "0" ? true : false);
            objclsCandidate.MaritalStatusID = ddlMaritalStatus.SelectedValue.ToInt32();
            objclsCandidate.NoofSon = txtNoOfSon.Text.ToInt32();
            objclsCandidate.MotherName = txtMotherName.Text.Trim();
            if(ddlCurrentNationality.SelectedValue != "-1") 
                objclsCandidate.CurrentNationalityID = ddlCurrentNationality.SelectedValue.ToInt32();
            if (ddlPreviousNationality.SelectedValue != "-1") 
                objclsCandidate.PreviousNationalityID = ddlPreviousNationality.SelectedValue.ToInt32();
            objclsCandidate.Doctrine = txtDoctrine.Text.Trim();
            if (rcReligion.SelectedValue != "-1") 
                objclsCandidate.ReligionID = rcReligion.SelectedValue.ToInt32();
            objclsCandidate.CurrentAddress = txtCurrentAddress.Text.Trim();
            objclsCandidate.CurrentPOBox = txtCurrentPOBox.Text.Trim();
            objclsCandidate.CountryID = rcCountry.SelectedValue.ToInt32();
            objclsCandidate.CurrentTelephone = txtCurrentTelephoneNumber.Text.Trim();
            objclsCandidate.BasicBusinessPair = txtPlaceofBusinessPair.Text.Trim();
            objclsCandidate.BasicBusinessPairAddress = txtBasicBusinessPairAddress.Text.Trim();
            if (ddlCompanyType.SelectedValue != "-1") 
                objclsCandidate.BasicCompanyTypeID = ddlCompanyType.SelectedValue.ToInt32();

           // if (txtPassword.Text.Trim() == "")
                objclsCandidate.Password = clsCommon.Encrypt(txtCandidateCode.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);
            //else
            //    objclsCandidate.Password = clsCommon.Encrypt(txtPassword.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);

            objclsCandidate.CreatedBy = objclsUserMaster.GetUserId();

            if (objclsCandidate.CreatedBy == 0)
                objclsCandidate.IsSelf = true;
            else
                objclsCandidate.IsSelf = false;

            // Professional Info
            objclsCandidate.PreviousJob = txtPreviousJob.Text.Trim();
            if (ddlQualificationCategory.SelectedValue != "-1") 
                objclsCandidate.QualificationCategoryID = ddlQualificationCategory.SelectedValue.ToInt32();
            if (rcDegree.SelectedValue != "-1") 
                objclsCandidate.QualificationID = rcDegree.SelectedValue.ToInt32();
           // objclsCandidate.Graduation = txtGraduation.Text.Trim();
            objclsCandidate.GraduationYear = ddlDegreeCompletionYear.SelectedValue.ToInt32();
            objclsCandidate.PlaceOfGraduation = txtPlaceofGraduation.Text.Trim();
            objclsCandidate.CollegeSchool = txtCollegeSchool.Text.Trim();
            objclsCandidate.Sector = txtSector.Text.Trim();
            objclsCandidate.Position = txtPosition.Text.Trim();
            objclsCandidate.TypeofExperience = txtTypeofExperience.Text.Trim();
            objclsCandidate.ExperienceYear = ddlYear.SelectedValue.ToInt32();
            objclsCandidate.ExperienceMonth = ddlMonth.SelectedValue.ToInt32();
            objclsCandidate.Compensation = txtCompensation.Text.Trim();
            objclsCandidate.Specialization = txtSpecialization.Text.Trim();
            objclsCandidate.Skill = txtSkills.Text;

            if (txtDateofTransactionEntry.Text.Trim() != "")
                objclsCandidate.TransactionEntryDate = clsCommon.Convert2DateTime(txtDateofTransactionEntry.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.TransactionEntryDate = "";

            if (txtExpectedJoinDate.Text.Trim() != "")
                objclsCandidate.ExpectedJoinDate = clsCommon.Convert2DateTime(txtExpectedJoinDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.ExpectedJoinDate = "";

            // Document Info
            objclsCandidate.PassportNumber = txtPassportNumber.Text.Trim();
            objclsCandidate.PassportCountryID = ddlPassportCountry.SelectedValue.ToInt32();
            objclsCandidate.PassportIssuePlace = txtPlaceofPassportIssue.Text.Trim();

            if (txtPassportExpiryDate.Text.Trim() != "")
                objclsCandidate.PassportExpiryDate = clsCommon.Convert2DateTime(txtPassportExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportExpiryDate = "";

            if (txtPassportReleaseDate.Text.Trim() != "")
                objclsCandidate.PassportReleaseDate = clsCommon.Convert2DateTime(txtPassportReleaseDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportReleaseDate = "";

            objclsCandidate.VisaNumber = txtVisaNumber.Text.Trim();
            objclsCandidate.UnifiedNumber = txtUnifiedNumber.Text.Trim();
            objclsCandidate.VisaCountryID = ddlVisaCountry.SelectedValue.ToInt32();
            objclsCandidate.VisaIssuePlace = txtVisaPlaceofIssue.Text.Trim();

            if (txtVisaExpiryDate.Text.Trim() != "")
                objclsCandidate.VisaExpiryDate = clsCommon.Convert2DateTime(txtVisaExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaExpiryDate = "";

            if (txtVisaReleaseDate.Text.Trim() != "")
                objclsCandidate.VisaReleaseDate = clsCommon.Convert2DateTime(txtVisaReleaseDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaReleaseDate = "";

            objclsCandidate.NationalCardNumber = txtNationalCardNumber.Text.Trim();
            objclsCandidate.CardNumber = txtCardNumber.Text.Trim();

            if (txtNationalCardExpiryDate.Text.Trim() != "")
                objclsCandidate.NationalCardExpiryDate = clsCommon.Convert2DateTime(txtNationalCardExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.NationalCardExpiryDate = "";

            if(ViewState["ResumePath"] != null)
                objclsCandidate.ResumeAttachment = ViewState["ResumePath"].ToString();

            // Other Info
            objclsCandidate.PermanentAddress = txtPermenantAddress.Text.Trim();
            objclsCandidate.HomeStreet = txtHomeStreet.Text.Trim();
            objclsCandidate.City = txtCity.Text.Trim();
            objclsCandidate.HomeTelephone = txtHomeTelephone.Text.Trim();
            objclsCandidate.BusinessAddress = txtBusinessAddress.Text.Trim();
            objclsCandidate.BusinessStreet = txtBusinessStreet.Text.Trim();
            objclsCandidate.Area = txtArea.Text.Trim();
            objclsCandidate.PermanentPOBox = txtPermenantPOBox.Text.Trim();
            objclsCandidate.Fax = txtFax.Text.Trim();
            objclsCandidate.PermanentTelephone = txtPhoneNumber.Text.Trim();
            objclsCandidate.MobileNumber = txtMobileNumber.Text.Trim();
            objclsCandidate.EmailID = txtEmail.Text.Trim();
            objclsCandidate.InsuranceNumber = txtInsuranceNumber.Text.Trim();
            //objclsCandidate.OtherBussinessPair = txtOtherBusinessPair.Text.Trim();
            //objclsCandidate.OtherCompanyTypeID = ddlOtherCompanyType.SelectedValue.ToInt32();
            //objclsCandidate.OtherBusinessPairAddress = txtOtherBusinessPairAddress.Text.Trim();

            if (txtmyDate.Text.Trim() != "")
                objclsCandidate.SignUpDate = clsCommon.Convert2DateTime(txtmyDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.SignUpDate = "";

            // Skills
            DataTable datTemp = new DataTable();
          

            // Languages
            foreach (ListItem item in chlArabic.Items)
            {
                if (item.Selected)
                {
                    if (item.Value == "0")
                        objclsCandidate.ReadingArb = true;

                    if (item.Value.ToString() == "1")
                        objclsCandidate.WritingArb = true;
                }
            }

            foreach (ListItem item in chlEnglish.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToString() == "0")
                        objclsCandidate.ReadingEng = true;

                    if (item.Value.ToString() == "1")
                        objclsCandidate.WritingEng = true;
                }
            }

            objclsCandidate.OtherLanguages = txtOtherLanguages.Text.Trim();

            foreach (ListItem item in chlOtherLanguages.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToString() == "0")
                        objclsCandidate.ReadingOtherLanguages = true;

                    if (item.Value.ToString() == "1")
                        objclsCandidate.WritingOtherLanguages = true;
                }
            }

            bool blnIsAddMode = true;
            if (objclsCandidate.CandidateID > 0)
                blnIsAddMode = false;

            if (objclsCandidate.CheckExistsCandidateCode())
                getCandidateCode();

          //  objclsCandidate.CandidateCode = txtCandidateCode.Text.Trim();
              string sMessage = string.Empty;  
           
            
                if (objclsCandidate.CheckCandidateExists())
                {
                    sMessage = clsGlobalization.IsArabicCulture()? "مرشح مسجل بالفعل. وجود معرف البريد الإلكتروني": "Candidate already registered. Email id exists";
                    msgs.InformationalMessage(sMessage);
                    this.mpeMessage.Show();
                    return;
                }
               
         
            objclsCandidate.Begin();

            long lngTempCID = objclsCandidate.InsertCandidateBasicInfoEng(datTemp, blnIsAddMode);

            if (lngTempCID > 0)
            {
                if (Session["Photo"] != null)
                {
                    System.IO.DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/documents/candidatephotos"));

                    foreach (System.IO.FileInfo file in dirInfo.GetFiles(lngTempCID + ".*"))
                    {
                        file.Delete();
                    }
                    File.Move(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()), Server.MapPath("~/documents/candidatephotos/" + lngTempCID + ".jpg"));
                }




              

                if (blnIsAddMode)
                {
                    sMessage = clsGlobalization.IsArabicCulture ()? " لا التطبيق: AN "  + lngTempCID + "التهاني، المرشح مسجلة بنجاح لتطبيق "  : "Application No: AN " + lngTempCID + " <br> Congratulations, Candidate successfully registered for the vacancy";
                    msgs.InformationalMessage(sMessage);
                    this.mpeMessage.Show();

                }
                else
                {
                    sMessage = clsGlobalization.IsArabicCulture ()? "معلومات مرشح تحديث بنجاح" : "Candidate Info  Updated Successfully";
                    msgs.InformationalMessage(sMessage);
                    this.mpeMessage.Show();
                }
                objclsCandidate.Commit();

                objclsCandidate.CandidateID = lngTempCID;
                SaveCandidateDocuments();

                AddNew();
            }

           
          

        }
        catch (Exception ex)
        {
            objclsCandidate.RollBack();
        }
    }

    protected void lnkListCandidate_Click(object sender, EventArgs e)
    {
        Response.Redirect("Candidates.aspx");
    }
    protected void lnkMore_Click(object sender, EventArgs e)
    {
        Response.Redirect("Candidate.aspx?CandidateID=" + ViewState["CandidateID"].ToInt64() + "");
    }
    private void getCandidateDetails(long CandidateID)
    {
        objclsCandidate = new clsCandidate();
        AddNew();
        objclsCandidate.CandidateID = CandidateID;
        ViewState["CandidateID"] = CandidateID.ToString();
        DataSet dsTemp = objclsCandidate.getCandidateDetails();

        if (dsTemp != null)
        {
            if (dsTemp.Tables.Count > 0)
            {
                if (dsTemp.Tables[0] != null) // Basic Info
                {
                    if (dsTemp.Tables[0].Rows.Count > 0)
                    {
                        ddlJob.SelectedValue = dsTemp.Tables[0].Rows[0]["JobID"].ToStringCustom();
                        ddlReferraltype.SelectedValue = dsTemp.Tables[0].Rows[0]["ReferralTypeId"].ToStringCustom();
                        FillComboRefferedBy();
                        ddlReferredBy.SelectedValue = dsTemp.Tables[0].Rows[0]["RefferedBy"].ToStringCustom();
                        txtCandidateCode.Text = dsTemp.Tables[0].Rows[0]["CandidateCode"].ToStringCustom();
                        rcSalutation.SelectedValue = dsTemp.Tables[0].Rows[0]["SalutationID"].ToStringCustom();
                        txtEnglishFirstName.Text = dsTemp.Tables[0].Rows[0]["FirstNameEng"].ToStringCustom();
                        txtEnglishSecondName.Text = dsTemp.Tables[0].Rows[0]["SecondNameEng"].ToStringCustom();
                        txtEnglishThirdName.Text = dsTemp.Tables[0].Rows[0]["ThirdNameEng"].ToStringCustom();
                        txtArabicFirstName.Text = dsTemp.Tables[0].Rows[0]["FirstNameArb"].ToStringCustom();
                        txtArabicSecondName.Text = dsTemp.Tables[0].Rows[0]["SecondNameArb"].ToStringCustom();
                        txtArabicThirdName.Text = dsTemp.Tables[0].Rows[0]["ThirdNameArb"].ToStringCustom();
                        txtCitizenship.Text = dsTemp.Tables[0].Rows[0]["Citizenship"].ToStringCustom();
                        txtPlaceOfBirth.Text = dsTemp.Tables[0].Rows[0]["PlaceofBirth"].ToStringCustom();
                        txtDateofBirth.Text = dsTemp.Tables[0].Rows[0]["DateofBirth"].ToStringCustom();
                        txtEmail.Text = dsTemp.Tables[0].Rows[0]["EmailID"].ToStringCustom();

                        foreach (DataRow row in dsTemp.Tables[0].Rows)
                        {
                            if (row["Gender"].ToBoolean())
                                rblGender.Items.FindByValue("0").Selected = true;
                            else
                                rblGender.Items.FindByValue("1").Selected = true;
                        }

                        ddlMaritalStatus.SelectedValue = dsTemp.Tables[0].Rows[0]["MaritalStatusID"].ToStringCustom();
                        txtNoOfSon.Text = dsTemp.Tables[0].Rows[0]["NoofSon"].ToStringCustom();
                        txtMotherName.Text = dsTemp.Tables[0].Rows[0]["MotherName"].ToStringCustom();
                        ddlCurrentNationality.SelectedValue = dsTemp.Tables[0].Rows[0]["CurrentNationalityID"].ToStringCustom();
                        ddlPreviousNationality.SelectedValue = dsTemp.Tables[0].Rows[0]["PreviousNationalityID"].ToStringCustom();
                        txtDoctrine.Text = dsTemp.Tables[0].Rows[0]["Doctrine"].ToStringCustom();
                        rcReligion.SelectedValue = dsTemp.Tables[0].Rows[0]["ReligionID"].ToStringCustom();
                        txtCurrentAddress.Text = dsTemp.Tables[0].Rows[0]["CurrentAddress"].ToStringCustom();
                        txtCurrentPOBox.Text = dsTemp.Tables[0].Rows[0]["CurrentPOBox"].ToStringCustom();
                        rcCountry.SelectedValue = dsTemp.Tables[0].Rows[0]["CountryID"].ToStringCustom();
                        txtCurrentTelephoneNumber.Text = dsTemp.Tables[0].Rows[0]["CurrentTelephone"].ToStringCustom();
                        txtPlaceofBusinessPair.Text = dsTemp.Tables[0].Rows[0]["BusinessPair"].ToStringCustom();
                        txtBasicBusinessPairAddress.Text = dsTemp.Tables[0].Rows[0]["BusinessPairAddress"].ToStringCustom();
                        ddlCompanyType.SelectedValue = dsTemp.Tables[0].Rows[0]["CompanyTypeID"].ToStringCustom();
                        //txtPassword.Text = "********";

                        if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + CandidateID.ToString() + ".jpg")))
                        {
                            divImage.Attributes.Add("style", "display:block");
                            btnRemoveRecentPhoto.Attributes.Add("style", "display:block");

                            string sFileName = Convert.ToString(CandidateID.ToString() + ".jpg");
                            imgPreview.Src = string.Format("../thumbnail.aspx?folder=candidatephotos&isCandidate=true&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
                        }
                    }
                }

                if (dsTemp.Tables[1] != null) // Professional Info
                {
                    if (dsTemp.Tables[1].Rows.Count > 0)
                    {
                        txtPreviousJob.Text = dsTemp.Tables[1].Rows[0]["PreviousJob"].ToStringCustom();
                        ddlQualificationCategory.SelectedValue = dsTemp.Tables[1].Rows[0]["QualificationCategoryID"].ToStringCustom();
                        rcDegree.SelectedValue = dsTemp.Tables[1].Rows[0]["DegreeID"].ToStringCustom();
                        //txtGraduation.Text = dsTemp.Tables[1].Rows[0]["Graduation"].ToStringCustom();
                        ddlDegreeCompletionYear.SelectedValue = dsTemp.Tables[1].Rows[0]["GraduationYear"].ToStringCustom();
                        txtPlaceofGraduation.Text = dsTemp.Tables[1].Rows[0]["PlaceOfGraduation"].ToStringCustom();
                        txtCollegeSchool.Text = dsTemp.Tables[1].Rows[0]["CollegeSchool"].ToStringCustom();
                        txtSector.Text = dsTemp.Tables[1].Rows[0]["Sector"].ToStringCustom();
                        txtPosition.Text = dsTemp.Tables[1].Rows[0]["Position"].ToStringCustom();
                        txtTypeofExperience.Text = dsTemp.Tables[1].Rows[0]["TypeofExperience"].ToStringCustom();
                        ddlYear.SelectedValue = dsTemp.Tables[1].Rows[0]["ExperienceYear"].ToStringCustom();
                        ddlMonth.SelectedValue = dsTemp.Tables[1].Rows[0]["ExperienceMonth"].ToStringCustom();
                        txtCompensation.Text = dsTemp.Tables[1].Rows[0]["Compensation"].ToStringCustom();
                        txtSpecialization.Text = dsTemp.Tables[1].Rows[0]["Specialization"].ToStringCustom();
                        txtDateofTransactionEntry.Text = dsTemp.Tables[1].Rows[0]["TransactionEntryDate"].ToStringCustom();
                        txtExpectedJoinDate.Text = dsTemp.Tables[1].Rows[0]["ExpectedJoinDate"].ToStringCustom();
                        txtSkills.Text = dsTemp.Tables[1].Rows[0]["Skills"].ToStringCustom();
                    }
                }

                if (dsTemp.Tables[2] != null) // Document Info
                {
                    if (dsTemp.Tables[2].Rows.Count > 0)
                    {
                        txtPassportNumber.Text = dsTemp.Tables[2].Rows[0]["PassportNumber"].ToStringCustom();
                        ddlPassportCountry.SelectedValue = dsTemp.Tables[2].Rows[0]["PassportCountryID"].ToStringCustom();
                        txtPlaceofPassportIssue.Text = dsTemp.Tables[2].Rows[0]["PassportIssuePlace"].ToStringCustom();
                        txtPassportExpiryDate.Text = dsTemp.Tables[2].Rows[0]["PassportExpiryDate"].ToStringCustom();
                        txtPassportReleaseDate.Text = dsTemp.Tables[2].Rows[0]["PassportReleaseDate"].ToStringCustom();
                        txtVisaNumber.Text = dsTemp.Tables[2].Rows[0]["VisaNumber"].ToStringCustom();
                        txtUnifiedNumber.Text = dsTemp.Tables[2].Rows[0]["UnifiedNumber"].ToStringCustom();
                        ddlVisaCountry.SelectedValue = dsTemp.Tables[2].Rows[0]["VisaCountryID"].ToStringCustom();
                        txtVisaPlaceofIssue.Text = dsTemp.Tables[2].Rows[0]["VisaIssuePlace"].ToStringCustom();
                        txtVisaExpiryDate.Text = dsTemp.Tables[2].Rows[0]["VisaExpiryDate"].ToStringCustom();
                        txtVisaReleaseDate.Text = dsTemp.Tables[2].Rows[0]["VisaReleaseDate"].ToStringCustom();
                        txtNationalCardNumber.Text = dsTemp.Tables[2].Rows[0]["NationalCardNumber"].ToStringCustom();
                        txtCardNumber.Text = dsTemp.Tables[2].Rows[0]["CardNumber"].ToStringCustom();
                        txtNationalCardExpiryDate.Text = dsTemp.Tables[2].Rows[0]["NationalCardExpiryDate"].ToStringCustom();
                        ViewState["ResumePath"] = dsTemp.Tables[2].Rows[0]["ResumePath"].ToStringCustom();
                    }
                }

                if (dsTemp.Tables[3] != null) // Other Info
                {
                    if (dsTemp.Tables[3].Rows.Count > 0)
                    {
                        txtPermenantAddress.Text = dsTemp.Tables[3].Rows[0]["PermanentAddress"].ToStringCustom();
                        txtHomeStreet.Text = dsTemp.Tables[3].Rows[0]["HomeStreet"].ToStringCustom();
                        txtCity.Text = dsTemp.Tables[3].Rows[0]["City"].ToStringCustom();
                        txtHomeTelephone.Text = dsTemp.Tables[3].Rows[0]["HomeTelephone"].ToStringCustom();
                        txtBusinessAddress.Text = dsTemp.Tables[3].Rows[0]["BusinessAddress"].ToStringCustom();
                        txtBusinessStreet.Text = dsTemp.Tables[3].Rows[0]["BusinessStreet"].ToStringCustom();
                        txtArea.Text = dsTemp.Tables[3].Rows[0]["Area"].ToStringCustom();
                        txtPermenantPOBox.Text = dsTemp.Tables[3].Rows[0]["PermanentPOBox"].ToStringCustom();
                        txtFax.Text = dsTemp.Tables[3].Rows[0]["Fax"].ToStringCustom();
                        txtPhoneNumber.Text = dsTemp.Tables[3].Rows[0]["PermanentTelephone"].ToStringCustom();
                        txtMobileNumber.Text = dsTemp.Tables[3].Rows[0]["MobileNumber"].ToStringCustom();                        
                        txtInsuranceNumber.Text = dsTemp.Tables[3].Rows[0]["InsuranceNumber"].ToStringCustom();
                        //txtOtherBusinessPair.Text = dsTemp.Tables[3].Rows[0]["BussinessPair"].ToStringCustom();
                        //ddlOtherCompanyType.SelectedValue = dsTemp.Tables[3].Rows[0]["CompanyTypeID"].ToStringCustom();
                        //txtOtherBusinessPairAddress.Text = dsTemp.Tables[3].Rows[0]["BusinessPairAddress"].ToStringCustom();
                        txtmyDate.Text = dsTemp.Tables[3].Rows[0]["SignUpDate"].ToStringCustom();
                    }
                }

                if (dsTemp.Tables[4] != null) // Languages
                {
                    if (dsTemp.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row in dsTemp.Tables[4].Rows)
                        {
                            if (row["ReadingArb"].ToBoolean())
                                chlArabic.Items.FindByValue("0").Selected = true;

                            if (row["WritingArb"].ToBoolean())
                                chlArabic.Items.FindByValue("1").Selected = true;

                            if (row["ReadingEng"].ToBoolean())
                                chlEnglish.Items.FindByValue("0").Selected = true;

                            if (row["WritingEng"].ToBoolean())
                                chlEnglish.Items.FindByValue("1").Selected = true;

                            if (row["ReadingOther"].ToBoolean())
                                chlOtherLanguages.Items.FindByValue("0").Selected = true;

                            if (row["WritingOther"].ToBoolean())
                                chlOtherLanguages.Items.FindByValue("1").Selected = true;
                        }

                        txtOtherLanguages.Text = dsTemp.Tables[4].Rows[0]["Other"].ToStringCustom();
                    }
                }

                
            }
        }

        GetCandidateAttachedDocuments(CandidateID);
    }


    private void GetCandidateAttachedDocuments(Int64 CandidateID)
    {
        try
        {
            DataTable dt = clsCandidate.GetCandidateAttachedDocuments(CandidateID);

            ViewState["Documents"] = dt;

            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();

            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;


        }
        catch (Exception ex)
        {
        }
    }

    protected void lnkAttachDocuments_Click(object sender, EventArgs e)
    {
        string strCandidateName = txtEnglishFirstName.Text.Trim() + " " + txtEnglishSecondName.Text.Trim() +
            " " + txtEnglishThirdName.Text.Trim();

        Response.Redirect("CandidateDoc.aspx?CandidateID=" + ViewState["CandidateID"].ToInt64() +
            "&CandidateName=" + strCandidateName + "&CandidateCode=" + txtCandidateCode.Text.Trim());
    }


    protected void fuRecentPhoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        btnClearRecentPhoto.Attributes.Add("style", "display:block");

        if (!Directory.Exists(Server.MapPath("~/Documents/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/"));

        if (!Directory.Exists(Server.MapPath("~/Documents/temp/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/temp/"));

        if (!Directory.Exists(Server.MapPath("~/Documents/candidatephotos/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/candidatephotos/"));


        if (fuRecentPhoto.HasFile)
        {

            filename = Session.SessionID + ".jpg";
            Session["Photo"] = filename;
            fuRecentPhoto.SaveAs(Server.MapPath("~/Documents/temp/") + filename);


        }
    }

    protected void btnClearRecentPhoto_Click(object sender, EventArgs e)
    {
        ClearPhoto();
    }

    protected void btnRemoveRecentPhoto_Click(object sender, EventArgs e)
    {
        RemovePhoto();
    }

    public void ClearPhoto()
    {
        if (fuRecentPhoto.HasFile)
        {
            if(Session["Photo"] != null)
                File.Delete(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()));
            fuRecentPhoto.ClearFileFromPersistedStore();
            Session["Photo"] = null;
        }

        ScriptManager.RegisterClientScriptBlock(btnClearRecentPhoto, btnClearRecentPhoto.GetType(), "Clear", "SetCandidateTab('pnlTab1')", true);
    }

    public void RemovePhoto()
    {
        if (ViewState["CandidateID"].ToString() != "" && ViewState["CandidateID"].ToString() != "0")
        {
            if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + ViewState["CandidateID"] + ".jpg")))
            {
                System.IO.DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/documents/candidatephotos"));

                foreach (System.IO.FileInfo file in dirInfo.GetFiles(ViewState["CandidateID"] + ".*"))
                {
                    file.Delete();
                }
            }
        }

        //if (fuRecentPhoto.HasFile)
        //{
        //    File.Delete(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()));
        //    fuRecentPhoto.ClearFileFromPersistedStore();
        //    Session["Photo"] = null;
        //}

        divImage.Attributes.Add("style", "display:none");
        btnRemoveRecentPhoto.Attributes.Add("style", "display:none");
        imgPreview.Src = "";

        ScriptManager.RegisterClientScriptBlock(btnRemoveRecentPhoto, btnRemoveRecentPhoto.GetType(), "Clear", "SetCandidateTab('pnlTab1')", true);
    }

    public string GetCandidatePhoto(object oCandidateId)
    {
        string sFileName = string.Empty;

        if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + oCandidateId.ToString() + ".jpg")))
        {
            sFileName = Convert.ToString(oCandidateId + ".jpg");
            return string.Format("../thumbnail.aspx?folder=candidatephotos&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
        }
        else
        {
            sFileName = "../images/candidate.jpg";
            return sFileName;
        }
    }
    protected void btnAttachCandidateDocument_Click(object sender, EventArgs e)
    {
        try
        {

            if (fuCandidate.HasFile)
                fuCandidate.ClearAllFilesFromPersistedStore();
            else
            {
                string sMsg = clsGlobalization.IsArabicCulture() ? "يرجى إرفاق مستند" : "Please attach a document";
                msgs.InformationalMessage(sMsg);
                this.mpeMessage.Show();
                return;
            }

            DataTable dt = null;


            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);
            else
            {
                dt = new DataTable();
                dt.Columns.Add("FileName");
                dt.Columns.Add("DocumentName");
                dt.Columns.Add("Location");
                dt.Columns.Add("Type");

            }

            DataRow dr = dt.NewRow();
            dr["FileName"] = Session["FileName"];
            dr["DocumentName"] = txtCandidateDocumentName.Text;
            dr["Location"] = "../Documents/Others/" + Session["FileName"].ToString();
            dr["Type"] = "";
            dt.Rows.Add(dr);



            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            dlCandidateDocument.Visible = dlCandidateDocument.Items.Count > 0 ? true : false;
            ViewState["Documents"] = dt;


            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;


            txtCandidateDocumentName.Text = "";



        }
        catch (Exception)
        {

        }

    }



    private void SaveCandidateDocuments()
    {
        try
        {
            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);


            if (dt != null && dt.Rows.Count > 0)
            {
                new clsCandidate().DeleteCandidateAttachedDocuments(objclsCandidate.CandidateID, string.Empty );

                foreach (DataRow dr in dt.Rows)
                {
                    if(dr["Type"].ToString() == "")
                     new clsCandidate().SaveCandidateDocuments(dr["FileName"].ToString(), dr["DocumentName"].ToString(), objclsCandidate.CandidateID);
                }
            }

        }
        catch (Exception)
        {

        }
    }

    protected void fuCandidate_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            string FileName = string.Empty;
            string ActualFileName = string.Empty;


            if (!Directory.Exists(Server.MapPath("~/Documents/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/"));

            if (!Directory.Exists(Server.MapPath("~/Documents/Others/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/Others/"));

            if (fuCandidate.HasFile)
            {
                ActualFileName = fuCandidate.FileName;
                FileName = DateTime.Now.Ticks.ToString() + new Random().Next(0, 1000).ToString() + Path.GetExtension(fuCandidate.FileName);
                Session["FileName"] = FileName;
                fuCandidate.SaveAs(Server.MapPath("~/Documents/Others/") + FileName);
            }




        }
        catch (Exception)
        {
        }


    }

    protected void dlCandidateDocument_ItemCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dt = null;
            string FileName = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;


           

            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FileName"].ToString() == FileName)
                {  
                    if (File.Exists(Server.MapPath(dt.Rows[i]["Location"].ToString())))
                        File.Delete(Server.MapPath(dt.Rows[i]["Location"].ToString()));

                    dt.Rows.Remove(dt.Rows[i]);
                    new clsCandidate().DeleteCandidateAttachedDocuments(ViewState["CandidateID"].ToInt64(), FileName);

                 
                }
            }


            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();

            dlCandidateDocument.Visible = dlCandidateDocument.Items.Count > 0 ? true : false ;

         
        }
        catch (Exception )
        {
        }

    }
    protected void btnReferralType_Click(object sender, EventArgs e)
    {
        if (ddlReferraltype != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "HRCandidateRefferalTypeReference";
            ReferenceControlNew1.DataTextField = "RefferalType";
            ReferenceControlNew1.DataValueField = "RefferaltypeId";
            ReferenceControlNew1.FunctionName = "FillComboRefferalType";
            ReferenceControlNew1.SelectedValue = ddlReferraltype.SelectedValue;
            ReferenceControlNew1.DisplayName = "Referral Type";
            //ReferenceControlNew1.DataTextFieldArabic = "ReligionArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    public void FillComboRefferedBy()
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();
        string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
         switch (ddlReferraltype.SelectedValue)
        {
            case "1":

                if (dsTemp.Tables[1] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[1];
                    ddlReferredBy.DataTextField = "Agency";
                    ddlReferredBy.DataValueField = "AgencyId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(str, "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible = true;


                }
                break;
             case "2":
                if (dsTemp.Tables[21] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[21];
                    ddlReferredBy.DataTextField = "Description";
                    ddlReferredBy.DataValueField = "JobPortalId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(str, "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible = true;
                }
                break;
             case "3":
                if (dsTemp.Tables[20] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[20];
                    ddlReferredBy.DataTextField = "EmployeeFullName";
                    ddlReferredBy.DataValueField = "EmployeeID";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(str, "-1"));

                    this.SetSelectedIndex(this.ddlReferredBy);
                    btnReferredBy.Visible  = false;
                    
                }
                break;
             case "-1":
                ddlReferredBy.Items.Clear();
                ddlReferredBy.DataSource = null;
                ddlReferredBy.DataBind();
                ddlReferredBy.Items.Insert(0, new ListItem(str, "-1"));
                break;

        }
         updReferredBy.Update();
         updReferralType.Update();

      
    }
    protected void ddlReferraltype_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillComboRefferedBy();
    }
    protected void btnReferredBy_Click(object sender, EventArgs e)
    {
        if (ddlReferredBy != null)
        {

            switch (ddlReferraltype.SelectedValue)
            {
                case "1":
                    AgencyReference.BindDataList();
                    ModalPopoUpAgency.Show();
                    updAgency.Update();
                    break;
                case "2":
                    ReferenceControlNew1.ClearAll();
                    ReferenceControlNew1.TableName = "HRJobPortalreference";
                    ReferenceControlNew1.DataTextField = "Description";
                    ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
                    ReferenceControlNew1.DataValueField = "JobPortalId";
                    ReferenceControlNew1.FunctionName = "FillComboRefferedBy";
                    ReferenceControlNew1.SelectedValue = ddlReferredBy.SelectedValue;
                    ReferenceControlNew1.DisplayName = GetLocalResourceObject("JobPortal.Text").ToString();

                    //ReferenceControlNew1.DataTextFieldArabic = "RefferalTypeArb";
                    ReferenceControlNew1.PopulateData();

                    mdlPopUpReference.Show();
                    updModalPopUp.Update();
                    break;

            }

       
        }
    }
    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }

    protected void dlCandidateDocument_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Image img = (Image)e.Item.FindControl("img");
            Image imgDelete = (Image)e.Item.FindControl("imgDelete");
            HiddenField hfIsResume = (HiddenField)e.Item.FindControl("hfIsResume");


            if (img != null && hfIsResume != null)
            {

                imgDelete .Visible = (hfIsResume.Value != "Resume");

                if (!IsValidImage(img.ImageUrl))
                {
                    img.ImageUrl = "../Images/files.png";
                }
            }

           
        }
    }

    protected void img_Click(object sender, ImageClickEventArgs e)
    {
        string FileName = ((ImageButton)sender).CommandArgument.ToString();

        if (IsValidImage(FileName))
        {
            string Page = "Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OPen", "window.open('" + Page + "');", true);

           
        }
        else
        {



            string Page = "Download.aspx?FileName=" + ((ImageButton)sender).CommandArgument.ToString() + "&type=Candidate";

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "OPen", "window.open('" + Page + "','','width=400,height=600');", true);

            
        }
        
    }
}
