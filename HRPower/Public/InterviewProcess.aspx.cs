﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class Public_InterviewProcess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("ProcessInterview.Title").ToString();

        InterviewProcessPager.Fill += new controls_Pager.FillPager(FillSchedules);

        if (!IsPostBack)
        {

            FillSearchCombos();

            FillInterviewer(0);
            EnableMenus();
            FillSchedules();

            divSchedules.Style["display"] = "block";
            dvMain.Style["display"] = "none";
            ViewState["InterviewerID"] = new clsUserMaster().GetUserId();

           
        }

    }

    public void EnableMenus()
    {
       
        int RoleID =new clsUserMaster().GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(RoleID, (int)eMenuID.InterviewProcess);

            if (dtm.Rows.Count > 0)
            {
                if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                {
                    lnkEvaluate.Enabled = true;
                    ViewState["IsEvaluate"] = true;
                }
                else
                {
                    lnkEvaluate.Enabled =  false;
                    ViewState["IsEvaluate"] = false;
                }

                DataTable dtResult = new clsRoleSettings().GetPermissions(RoleID, (int)eMenuID.InterviewResult);

                if (dtResult.Rows.Count > 0)
                {
                    lnkInterviewREsult.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                    ViewState["IsViewResult"] = dtm.Rows[0]["IsView"].ToBoolean();
                }
                else
                {
                    lnkEvaluate.Enabled = lnkInterviewREsult.Enabled = false ;
                }
            }
        }
        else
        {
            lnkEvaluate.Enabled = lnkInterviewREsult.Enabled = true;
        }

    }
    protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCandidates();
        FillSchedules();
        updViewProcess.Update();
    }

    protected void ddlInterviewer_SelectedIndexChanged(object sender, EventArgs e)
    {
        InterviewProcessPager.CurrentPage = 0;
        FillSchedules();
        updViewProcess.Update();
    }
    protected void ddlCandidate_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSchedules();
        updViewProcess.Update();
    }
    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSchedules();
        updViewProcess.Update();
    }

    private void FillCandidates()
    {
        ddlCandidate.DataSource = clsInterviewProcess.FillCandidates(ddlJob.SelectedValue.ToInt32());
        ddlCandidate.DataBind();
        ddlCandidate.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "حدد مرشح" : "Select Candidate", "-1"));
    }

    private void FillSearchCombos()
    {
        DataSet ds = clsInterviewProcess.FillJobAndStatus();

        ddlJob.DataSource = ds.Tables[0];
        ddlJob.DataBind();
        ddlJob.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ?"حدد وظيفة" :"Select Job", "-1"));



        ddlStatus.DataSource = ds.Tables[1];
        ddlStatus.DataBind();
        ddlStatus.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "حدد الوضع" : "Select Status", "-1"));

        ddlCandidate.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "حدد مرشح" : "Select Candidate", "-1"));


    }
    private void FillInterviewer(int InterviewerID)
    {
        ddlInterviewer.DataSource = clsInterviewProcess.FillInterviewer(0);
        ddlInterviewer.DataBind();

        ddlInterviewer.SelectedValue =Convert.ToString(new clsUserMaster().GetUserId());
    }

    private void FillSchedules()
    {

        if (new clsRoleSettings().IsMenuEnabledForInterviewer(new clsUserMaster().GetRoleId(), (int)eMenuID.InterviewProcess, "IsView"))
        {
            divSearch.Style["display"] = "block";
            //if (new clsUserMaster().GetRoleId() <= 4)
            //{
          
                DataTable dt = clsInterviewProcess.FillSchedules(InterviewProcessPager.CurrentPage + 1, InterviewProcessPager.PageSize, "", "desc", "JobID", ddlJob.SelectedValue.ToInt32(), ddlCandidate.SelectedValue.ToInt32(), ddlStatus.SelectedValue.ToInt32(), ddlInterviewer.SelectedValue.ToInt32());

                if (dt.Rows.Count > 0)
                {
                    InterviewProcessPager.Total = Convert.ToInt32(dt.Rows[0]["TotalCount"]); //Getting the total count of data

                    lblNoData.Style["display"] = "none";
                    lblNoData.Text = string.Empty;

                    InterviewProcessPager.Visible = true;
                    gvInterviewProcess.DataSource = dt;
                    gvInterviewProcess.DataBind();

                }
                else
                {
                    
                    gvInterviewProcess.DataSource = null;
                    gvInterviewProcess.DataBind();
                    InterviewProcessPager.Visible = false;
                    lblNoData.Style["display"] = "block";
                    lblNoData.Text =Convert.ToString(GetGlobalResourceObject("ControlsCommon", "NoRecordFound"));
                }

            //}
            //else
            //{
            //    lblNoData.Style["display"] = "block";
            //    lblNoData.Text = "No Data Found.";
            //}


            ViewState["InterviewProcessID"] = null;
            ViewState["CandidateID"] = null;
            ViewState["IsActive"] = null;
            ViewState["ViewSingle"] = null;
            ViewState["InterviewerID"] = null;
        }
        else
        {
            divSearch.Style["display"] = "none";

            lblNoData.Style["display"] = "block";
            lblNoData.Text =Convert.ToString(GetGlobalResourceObject("ControlsCommon", "NoPermission"));
        }

    }

    //private void GetAllCandidates()
    //{
    //    ddlCandidates.DataSource = clsInterviewProcess.GetAllCandidates(ddlSchedules.SelectedValue.ToInt32());
    //    ddlCandidates.DataTextField = "CandidateName";
    //    ddlCandidates.DataValueField = "CandidateID";
    //    ddlCandidates.DataBind();
    //}


    private void SetGrid()
    {
        string Message = string.Empty;
        try
        {
            dlEvaluate.DataSource = null;
            dlEvaluate.DataBind();
            string Name = "";
            lblWarning.Text = string.Empty;

            int Status = clsInterviewSchedule.IsCandidateScheduled(ViewState["CandidateID"].ToInt64(), ViewState["JobID"].ToInt32());
            if (Status == (int)eCandidateResult.recommende_To_OtherJobs)  // Recommend to Other and Scheduled for other job
            {
                lblWarning.Text =clsGlobalization.IsArabicCulture() ? "لا يمكن تحديث نتائج ؛ المجدولة عن وظيفة أخرى .": "Cannot update Result;Scheduled for another job.";
                btnSaveEvaluation.Enabled = btnClearAll.Enabled = false;
            }
            else if (Status == (int)eCandidateResult.OfferSent)
            {
                lblWarning.Text = clsGlobalization.IsArabicCulture() ? "لا يمكن تحديث نتائج ؛ العرض المرسلة" : "Cannot update Result;Offer Sent";
                btnSaveEvaluation.Enabled = btnClearAll.Enabled = false;
            }
            else
            {
                lblWarning.Text = "";
                btnSaveEvaluation.Enabled = btnClearAll.Enabled = true ;
            }


            using (DataSet ds = clsInterviewProcess.GetCurrentActiveScheduleForEvaluation(new clsUserMaster().GetUserId(), true, ViewState["CandidateID"].ToInt64(), ViewState["JobID"].ToInt32()))
            {
                if (ds.Tables[0].Rows.Count > 0)
                {


                    Name = ds.Tables[0].Rows[0]["Name"].ToString();

                    string ar = clsGlobalization.IsArabicCulture() ? "مرشح" : "Candidate :";
                    lblName.Text = "(" + ar + (Name.Length > 50 ? Name.Substring(0, 60) + "..." : Name) + ")";

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        ViewState["IsNewCandidateAvailable"] = "true";
                        dlEvaluate.DataSource = ds.Tables[1];
                        dlEvaluate.DataBind();


                        if (ds.Tables[1].Rows[0]["Total"] == DBNull.Value)
                            lblResult.Text = clsGlobalization.IsArabicCulture() ? "مجموع علامات :" : "Total marks :";
                        else
                        {
                            string sMark = clsGlobalization.IsArabicCulture() ? "مجموع علامات :" : "Total marks :";
                            lblResult.Text = sMark + ds.Tables[1].Rows[0]["Total"].ToString();
                            //lblResult.Text = clsGlobalization.IsArabicCulture() ? "مجموع علامات :" : "Total marks :" + ds.Tables[1].Rows[0]["Total"].ToString();
                        }

                        if (ds.Tables[1].Rows[0]["InterviewProcessID"] == DBNull.Value)
                            ViewState["InterviewProcessID"] = "0";
                        else
                            ViewState["InterviewProcessID"] = ds.Tables[1].Rows[0]["InterviewProcessID"].ToString();


                    }
                    else
                    {

                         Message = clsGlobalization.IsArabicCulture() ? "الرجاء اختيار أي مرشح" : "Please select any candidate";
                        ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
                        ViewState["IsNewCandidateAvailable"] = "false";
                    }
                }
                else
                {
                     Message = clsGlobalization.IsArabicCulture() ? "الرجاء اختيار أي مرشح" : "Please select any candidate";
                    ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
                    ViewState["IsNewCandidateAvailable"] = "false";
                }

            }
        }
        catch (Exception ex)
        {
        }


    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void btnOK_Click(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void lnkInterviewREsult_Click(object sender, EventArgs e)
    {
        string Message = string.Empty;

        try
        {
            if (ViewState["ViewSingle"] ==null)
            {
                Message = clsGlobalization.IsArabicCulture() ? "الرجاء اختيار أي مرشح" : "Please select any candidate";
                ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
                return;
            }

            if (new clsUserMaster().GetRoleId() > 4)
            {
                if (ViewState["InterviewerID"].ToInt32() != new clsUserMaster().GetUserId().ToInt32())
                {
                    Message = clsGlobalization.IsArabicCulture() ? "لا يوجد لديك الإذن" : "You have no permission";
                    ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
                    return;
                }
            }

            if (ViewState["InterviewProcessID"] == null || ViewState["InterviewProcessID"].ToInt64() == 0)
            {
                ViewState["InterviewProcessID"] = clsInterviewProcess.GetInterviewProcessID(ViewState["InterviewerID"].ToInt32(), ViewState["CandidateID"].ToInt32(), ViewState["JobID"].ToInt32());
            }



            if (ViewState["InterviewProcessID"] != null && ViewState["InterviewProcessID"].ToInt64() > 0 && ViewState["JobID"].ToInt32() > 0)
            {
                InterviewResult1.SetGrid(ViewState["InterviewProcessID"].ToInt64(), ViewState["CandidateID"].ToInt64(), ViewState["JobID"].ToInt32());
                ModalPopupExtender3.Show();
                updPopUp.Update();
            }
            else
                ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Interview result is available only after evaluation');", true);
        }
        catch (Exception ex)
        {
        }

    }
    protected void lnkListEmp_Click(object sender, EventArgs e)
    {
        // Response.Redirect("ConfirmApplicant.aspx");
    }
    protected void btnSaveEvaluation_Click(object sender, EventArgs e)
    {


        clsInterviewEvaluationResult objEvaluationResult = new clsInterviewEvaluationResult();
        objEvaluationResult.Criteria = new System.Collections.Generic.List<clsInterviewCriteria>();



        foreach (DataListItem dl in dlEvaluate.Items)
        {
            if (dl.ItemType == ListItemType.Header)
            {



            }
            else if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
            {
                HiddenField hfCriteriaID = ((HiddenField)dl.FindControl("hfCriteriaID"));
                HiddenField hfJobScheduleDetailID = ((HiddenField)dl.FindControl("hfJobScheduleDetailID"));
                HiddenField hfJobID = ((HiddenField)dl.FindControl("hfJobID"));
                HiddenField hfCandidateID = ((HiddenField)dl.FindControl("hfCandidateID"));
                HiddenField hfJobLevelID = ((HiddenField)dl.FindControl("hfJobLevelID"));

                TextBox txtMark = ((TextBox)dl.FindControl("txtMark"));

                if (hfJobScheduleDetailID != null && hfJobID != null && hfCandidateID != null)
                {
                    objEvaluationResult.CandidateID = hfCandidateID.Value.ToInt32();
                    objEvaluationResult.InterviewerID = new clsUserMaster().GetUserId();
                    objEvaluationResult.JobScheduleDetailID = hfJobScheduleDetailID.Value.ToInt32();
                    objEvaluationResult.JobLevelID = hfJobLevelID.Value.ToInt32();
                    objEvaluationResult.JobID = hfJobID.Value.ToInt32();
                }



                if (hfCriteriaID != null && txtMark != null)
                {
                    objEvaluationResult.Criteria.Add(new clsInterviewCriteria()
                    {
                        CriteriaID = hfCriteriaID.Value.ToInt32(),
                        AssignedMark = txtMark.Text.ToDecimal()
                    });
                }

            }
        }
        if (objEvaluationResult.Criteria.Count > 0)
        {
            Int64 InterviewProcessID = 0;
            InterviewProcessID = new clsInterviewProcess().SaveEvaluationResult(objEvaluationResult);
            if (InterviewProcessID > 0)
            {
                ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("Resultsavedsuccessfully.Text") + "' );", true);
                ViewState["InterviewProcessID"] = InterviewProcessID;
            }
            else
                ScriptManager.RegisterClientScriptBlock(upSubmit, upSubmit.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("Failedtosaveresult.Text") + "');", true);
        }




        upSubmit.Update();


    }
    protected void lnkEvaluate_Click(object sender, EventArgs e)
    {
        //ModalPopupExtender2.Show();
        //updEvaluate.Update();
        //upSubmit.Update();
    }
    protected void lnkEvaluate_Click1(object sender, EventArgs e)
    {
        string Message = string.Empty;


        if (ViewState["ViewSingle"] == null)
        {
            Message = clsGlobalization.IsArabicCulture() ? "الرجاء اختيار أي مرشح" : "Please select any candidate";
            ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert(' " + Message + "');", true);
            return;
        }

        if (ViewState["InterviewerID"].ToInt32() != new clsUserMaster().GetUserId().ToInt32())
        {
            Message = clsGlobalization.IsArabicCulture() ? "لا يوجد لديك الإذن" : "You have no permission";
            ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
            return;
        }

        SetGrid();

        if (ViewState["IsNewCandidateAvailable"] != null && ViewState["IsNewCandidateAvailable"].ToString() == "true")
        {

            ModalPopupExtender2.Show();
            updEvaluate.Update();
            upMenu.Update();
        }

    }

   
    //protected void timer_Tick(object sender, EventArgs e)
    //{
    //    if (clsInterviewProcess.GetCurrentInterviewCount(ViewState["InterviewerID"].ToInt32()) == 0)
    //    {
    //        dvError.Attributes.Add("style", "display:block");
    //        dvMain.Attributes.Add("style", "display:none");
    //        ViewState["IsActive"] = "false";
    //    }
    //    else if (ViewState["IsActive"] == null || (ViewState["IsActive"] != null && ViewState["IsActive"].ToString() == "false"))
    //    {
    //        ViewState["IsActive"] = "true";
    //        ViewState["CandidateID"] = clsInterviewProcess.GetCurrentInterviewCount(ViewState["InterviewerID"].ToInt32());
    //        dvError.Attributes.Add("style", "display:none");
    //        dvMain.Attributes.Add("style", "display:block");


    //        CandidateView1.GetCandidateInfo(ViewState["CandidateID"].ToInt64());
    //    }
    //}

    protected void imgeViewOfferLetter_Click(object sender, ImageClickEventArgs e)
    {

    }


    protected void lnkViewOfferLetter_Click(object sender, EventArgs e)
    {


        if (ViewState["InterviewProcessID"] == null)
            ViewState["InterviewProcessID"] = clsInterviewProcess.GetInterviewProcessID(ViewState["InterviewerID"].ToInt32(), ViewState["CandidateID"].ToInt32(), ViewState["JobID"].ToInt32());



        int ResultTypeID = clsInterviewProcess.GetInterviewResult(ViewState["InterviewProcessID"].ToInt64());
        if (ResultTypeID != 1)
        {
            ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Offer letter can be view only for candidates in acceptable status');", true);
            return;
        }


        if (GetOfferLetter() != "")
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PrintWithoutHeader", "PrintWithoutHeader('" + divPrint.ClientID + "')", true);

    }


    private string GetOfferLetter()
    {

        //if (!chkManual.Checked)
        //{
        //    if (clsInterviewProcess.GetCurrentInterviewCount(ViewState["InterviewerID"].ToInt32()) == 0)
        //    {
        //        //ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Wait for new candidate to come');", true);
        //        return "";
        //    }
        //}
        if (ViewState["InterviewProcessID"] == null || ViewState["InterviewProcessID"].ToInt64() == 0)
        {
            ViewState["InterviewProcessID"] = clsInterviewProcess.GetInterviewProcessID(ViewState["InterviewerID"].ToInt32(), ViewState["CandidateID"].ToInt32(), ViewState["JobID"].ToInt32());
        }

        if (ViewState["InterviewProcessID"] != null && ViewState["InterviewProcessID"].ToInt64() > 0)
        {
            clsCandidateHome objHome = new clsCandidateHome();
            DataTable dtOfferLetter = clsInterviewProcess.GetCandidateOfferLetter(ViewState["InterviewProcessID"].ToInt64());
            if (dtOfferLetter != null && dtOfferLetter.Rows.Count > 0)
            {
                StreamReader reader = new StreamReader(Server.MapPath("~/ETHome/OfferLetterMail.htm"));
                string readFile = reader.ReadToEnd();
                string offerLetter = "";
                offerLetter = readFile;

                offerLetter = offerLetter.Replace("$$OfferLetterNumber$$", Convert.ToString(dtOfferLetter.Rows[0]["OfferLetterNumber"]));//
                offerLetter = offerLetter.Replace("$$Date$$", dtOfferLetter.Rows[0]["SentDate"].ToDateTime().ToString("dd MMM yyyy"));
                offerLetter = offerLetter.Replace("$$ApplicationReference$$", Convert.ToString(dtOfferLetter.Rows[0]["ApplicationReference"]));//or+canno
                offerLetter = offerLetter.Replace("$$ApplicantName$$", Convert.ToString(dtOfferLetter.Rows[0]["ApplicantName"]));

                divPrint.InnerHtml = offerLetter;


                return offerLetter;
            }
        }
        return "";
    }


    protected void lnkSendOfferLetter_Click(object sender, EventArgs e)
    {

        if (ViewState["InterviewProcessID"] == null)
            ViewState["InterviewProcessID"] = clsInterviewProcess.GetInterviewProcessID(ViewState["InterviewerID"].ToInt32(), ViewState["CandidateID"].ToInt32(), ViewState["JobID"].ToInt32());

        int ResultTypeID = clsInterviewProcess.GetInterviewResult(ViewState["InterviewProcessID"].ToInt64());
        if (ResultTypeID != 1)
        {
            ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Offer letter can be sent only for candidates in acceptable status');", true);
            return;
        }

        string OfferLetter = GetOfferLetter();
        if (OfferLetter != "")
        {
            string FromMailID = "";
            string ToMailID = "";


            clsInterviewProcess.UpdateOfferLetterStatus(ViewState["CandidateID"].ToInt64());

            DataTable dt = clsInterviewProcess.GetFromToMailIDs(ViewState["CandidateID"].ToInt64());
            if (dt.Rows.Count > 0)
            {
                FromMailID = dt.Rows[0]["FromMailID"].ToString();
                ToMailID = dt.Rows[0]["ToMailID"].ToString();


                if (FromMailID != "" && ToMailID != "")
                {
                    System.Threading.Thread t = new System.Threading.Thread(() => new clsMailSettings().SendMail(dt.Rows[0]["FromMailID"].ToString(), dt.Rows[0]["ToMailID"].ToString(), "Offer Letter", OfferLetter, clsMailSettings.AccountType.Email, false));
                    t.Start();
                }
                else
                    if (FromMailID == "")
                        ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Mail cannot be send since there is no user name set in the mail settings');", true);
                    else if (ToMailID == "")
                        ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('Mail cannot be send since there is no email id set for the candidate ');", true);
            }
        }
    }





    protected void chkManual_CheckedChanged(object sender, EventArgs e)
    {
        ViewState["InterviewProcessID"] = null;
        ViewState["CandidateID"] = null;
        ViewState["IsActive"] = null;
        dvError.Attributes.Add("style", "display:block ");
        dvMain.Attributes.Add("style", "display:none");
        if (chkManual.Checked)
        {
            
            ddlCandidates.Enabled = true;
            btnGo.Enabled = true;
            upMenu.Update();


        }
        else
        {
            
            ddlCandidates.Enabled = false;
            btnGo.Enabled = false;
            upMenu.Update();


        }
    }
    protected void ddlSchedules_SelectedIndexChanged(object sender, EventArgs e)
    {
      //  GetAllCandidates();
    }

    protected void ddlCandidates_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (chkManual.Checked)
        //{
            ViewState["InterviewProcessID"] = null;
            ViewState["CandidateID"] = null;
            ViewState["IsActive"] = null;
            dvError.Attributes.Add("style", "display:block ");
            dvMain.Attributes.Add("style", "display:none");
        //}
    }
    protected void btnGoBack_Click(object sender, EventArgs e)
    {
        FillSchedules();
        divSchedules.Style["display"] = "block";
        dvMain.Style["display"] = "none";
      

    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

       

            if (ddlCandidates.Items.Count > 0)
            {

                ViewState["CandidateID"] = ddlCandidates.SelectedValue.ToInt32();
 
                CandidateView1.GetCandidateInfo(ViewState["CandidateID"].ToInt64());

                SetGrid();
                ViewState["CandidateID"] = ddlCandidates.SelectedValue.ToInt32();
                CandidateView1.GetCandidateInfo(ViewState["CandidateID"].ToInt64());


                dvError.Attributes.Add("style", "display:none");
                dvMain.Attributes.Add("style", "display:block");
            }
            else
            {
                ViewState["CandidateID"] = null;
                dvError.Attributes.Add("style", "display:block ");
                dvMain.Attributes.Add("style", "display:none");
            }
      


    }

    protected void gvInterviewProcess_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            
            LinkButton ancCandidateLevels = (LinkButton)e.Row.FindControl("ancCandidateLevels");

            HtmlContainerControl divCandidateLevels = (HtmlContainerControl)e.Row.FindControl("divCandidateLevels");

            int CandidateStatusID = this.gvInterviewProcess.DataKeys[e.Row.RowIndex]["CandidateStatusId"].ToInt32();
       
            ancCandidateLevels.OnClientClick = "return toggleTabDiv('" + divCandidateLevels.ClientID + "')";

            AjaxControlToolkit.TabContainer tcPerformanceDetails = (AjaxControlToolkit.TabContainer)e.Row.FindControl("tcPerformanceDetails");
            ImageButton ibtnSendOffer = (ImageButton)e.Row.FindControl("ibtnSendOffer");
            HiddenField hfdStatus = (HiddenField)e.Row.FindControl("hfdStatus");
            int RoleID = new clsUserMaster().GetRoleId();


            if (RoleID <= 4 || (new clsRoleSettings().IsMenuEnabled(RoleID, (int)eMenuID.OfferLetter))) // Have permission
            {

                if (hfdStatus.Value == "Selected" || hfdStatus.Value == "مختار") // Check manager view permission) // Selected
                {
                    ibtnSendOffer.ImageUrl = "~/images/sent_icon.jpg";
                    ibtnSendOffer.Enabled = true;
                    ibtnSendOffer.ToolTip = clsGlobalization.IsArabicCulture() ? "إرسال خطاب العرض" : "Send Offer";
                }
                else
                {
                    ibtnSendOffer.ImageUrl = "~/images/sent_icon_grey.png";
                    ibtnSendOffer.Enabled = false;
                    ibtnSendOffer.ToolTip =clsGlobalization.IsArabicCulture() ? "إرسال خطاب العرض": "Send Offer";
                }

            }
            else
            {
                ibtnSendOffer.ImageUrl = "~/images/sent_icon_grey.png";
                ibtnSendOffer.Enabled = false;
                ibtnSendOffer.ToolTip = clsGlobalization.IsArabicCulture() ? "لا يوجد لديك الإذن" : "You have no Permission.";
            }


          

        }
    }

    protected void gvInterviewProcess_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      
        GridViewRow gr;

        switch (e.CommandName)
        {

            case "_ViewCandidate":
                ViewState["ViewSingle"] = "ViewMode";
                LinkButton lnkCandidate = (LinkButton)(e.CommandSource);
                gr = (GridViewRow)lnkCandidate.Parent.Parent;

                ViewState["CandidateID"] = gvInterviewProcess.DataKeys[gr.RowIndex]["CandidateId"];
                ViewState["JobID"] = gvInterviewProcess.DataKeys[gr.RowIndex]["JobID"];
                ViewState["InterviewerID"] = gvInterviewProcess.DataKeys[gr.RowIndex]["InterviewerID"];

                CandidateView1.GetCandidateInfo(ViewState["CandidateID"].ToInt64());

                SetGrid();


                dvError.Attributes.Add("style", "display:none");
                dvMain.Attributes.Add("style", "display:block");
                divSchedules.Attributes.Add("style", "display:none");
                updMain.Update();
                break;

            case "_ViewLevels":

                LinkButton ancCandidateLevels = (LinkButton)(e.CommandSource);

                gr = (GridViewRow)ancCandidateLevels.Parent.Parent;

                HtmlContainerControl divCandidateLevels = (HtmlContainerControl)gr.FindControl("divCandidateLevels");

                AjaxControlToolkit.TabContainer tcPerformanceDetails = (AjaxControlToolkit.TabContainer)gr.FindControl("tcPerformanceDetails");

                foreach (GridViewRow gvr in gvInterviewProcess.Rows)
                {
                    if (gvr.RowIndex != gr.RowIndex)
                    {
                        HiddenField hfCandidateLevels = (HiddenField)gvr.FindControl("hfCandidateLevels");

                        hfCandidateLevels.Value = "0";
                    }
                }

                GenerateTab(Convert.ToInt32(gvInterviewProcess.DataKeys[gr.RowIndex]["JobID"]), Convert.ToInt32(e.CommandArgument), tcPerformanceDetails);

                string sTimeDelay = "var t = setTimeout(\"SlideUpSlideDown('" + divCandidateLevels.ClientID + "', 500)\",50)";

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "toggle", sTimeDelay, true);

                break;


            case "_SendOffer": // Sends offer letter for the selected candidates

                ImageButton ibtnSendOffer = (ImageButton)(e.CommandSource);

                gr = (GridViewRow)ibtnSendOffer.Parent.Parent;

                long CandidateID = e.CommandArgument.ToInt64(), JobID = gvInterviewProcess.DataKeys[gr.RowIndex]["JobID"].ToInt32();
               
                Response.Redirect(string.Format("~/public/Offerletter.aspx?CandidateId={0}&JobId={1}", CandidateID, JobID ));
            
                break;
         }
    }

    public void GenerateTab(int JobID, Int64 iCandidateId, AjaxControlToolkit.TabContainer tcContainer)
    {
        StringBuilder sb;

        AjaxControlToolkit.TabPanel tpPanel;
        Literal ltrPanelContent;

        DataTable dtLevels = clsInterviewProcess.FillScheduleLevels(iCandidateId, JobID);
        ViewState["HistoryCount"] = dtLevels.Rows.Count;

        tcContainer.Controls.Clear();

        for (int i = 0; i < dtLevels.Rows.Count; i++)
        {
            sb = new StringBuilder();
            tpPanel = new AjaxControlToolkit.TabPanel();

            tpPanel.ID = "tbPanel" + (i + 1).ToString();
            tpPanel.HeaderText = Convert.ToString(dtLevels.Rows[i]["Description"]) == "" ? (clsGlobalization.IsArabicCulture() ? "مستوى المجلس" : "Board Level") : Convert.ToString(dtLevels.Rows[i]["Description"]);

            ltrPanelContent = new Literal();

            sb.Append("<table width='100%' border='0' style='font-size:11px;'>");
            sb.Append("<tr>");
            sb.Append("<td class='item_title' width='130px'>" + (clsGlobalization.IsArabicCulture() ? "حالة" : "Status") + "  </td>");
            sb.Append("<td class='item_title' width='5px'>:</td>");
            sb.Append("<td width='250px'>" + Convert.ToString(dtLevels.Rows[i]["Status"]) + "</td>");
            sb.Append("<td class='item_title' width='100px'>" + (clsGlobalization.IsArabicCulture() ? "نتيجة" : "Result") + "</td>");
            sb.Append("<td class='item_title' width='5px'>:</td>");
            sb.Append("<td>" + Convert.ToString(dtLevels.Rows[i]["Result"]) + "</td>");
            sb.Append("</tr>");
            sb.Append("<tr>");
            sb.Append("<td class='item_title'>" + (clsGlobalization.IsArabicCulture() ? "مجموع علامات :" : "Total Weighted Marks:") + "</td>");
            sb.Append("<td class='item_title'>:</td>");
            sb.Append("<td>" + Convert.ToString(dtLevels.Rows[i]["TotalWeightedMark"]) + "</td>");
            sb.Append("<td class='item_title'>" + (clsGlobalization.IsArabicCulture() ? "علامة%" : "Mark Percentage") + "</td>");
            sb.Append("<td class='item_title'>:</td>");
            sb.Append("<td>" + Convert.ToString(dtLevels.Rows[i]["MarkPercentage"]) + "</td>");
            sb.Append("</tr>");
          
            sb.Append("</table>");

            ltrPanelContent.ID = "ltr" + (i + 1).ToString();
            ltrPanelContent.Text = Convert.ToString(sb);

            tpPanel.Controls.Add(ltrPanelContent);

            tcContainer.Tabs.Add(tpPanel);
        }
    }

}
