﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.IO;
using System.Collections.Specialized;
using System.Net.Mail;

public partial class Public_EmployeeSalaryCertificate : System.Web.UI.Page
{
    clsEmployeeSalaryCertificate objCertificate;
    clsTemplates objTemplates;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("MasterPageCommon", "SalaryCertificate").ToString();
        objCertificate = new clsEmployeeSalaryCertificate();
        objTemplates = new clsTemplates();
       
        if (!IsPostBack)
        {
            BindCombo();
            if (Convert.ToInt32(ddlTemplates.SelectedValue) > 0)
                BindTemplate();
            else
                ajcSalaryCertificate.Content = string.Empty;
        }

    }

    private void BindCombo()
    {
        ddldepartment.DataSource = new clsBindComboBox().GetDepartmentReference();
        ddldepartment.DataTextField = "Department";
        ddldepartment.DataValueField = "DepartmentID";
        ddldepartment.DataBind();
        ddldepartment.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"),"-1"));
      

    
        objTemplates.Type = clsTemplates.TemplateType.SalaryCertificate;
        ddlTemplates.DataValueField = "Templateid";
        ddlTemplates.DataTextField = "Template";
        ddlTemplates.DataSource = objTemplates.GetTemplates();
        ddlTemplates.DataBind();
        ddlTemplates.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"),"-1"));

        BindEmployee();
      
    }
    private void BindEmployee()
    {
        ddlEmployee.DataSource = objCertificate.GetEmployees(Convert.ToInt32(ddldepartment.SelectedValue) , new clsUserMaster().GetCompanyId());
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"), "-1"));
    }
    protected void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
    {
       BindTemplate();
    }

    private void BindTemplate()
    {
        objTemplates = new clsTemplates();
        clsXML objXML = new clsXML();  
        
        objTemplates.TemplateId = Convert.ToInt32(ddlTemplates.SelectedValue);

        DataTable dt = objTemplates.GetTemplatesDetails();

        objXML.AttributeId = Convert.ToInt32(ddlTemplates.SelectedValue);

        ajcSalaryCertificate.Content = objXML.GetTemplateMessage();

          
       
    }
    protected void ddldepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindEmployee();
    }

    private string GetSalaryCertificateContent()
    {  
        ListDictionary ldReplacements = new ListDictionary();
        int iCreatedBY  = new clsUserMaster().GetEmployeeId().ToInt32();
        DataSet dstemplate = objCertificate.GetTemplateValues(Convert.ToInt32(ddlEmployee.SelectedValue),iCreatedBY);
        DataTable dtTemplate = dstemplate.Tables[0];
        DataTable dtAmt = dstemplate.Tables[1];
        string strAdditionDeduction = "";
        double dAmt = 0;

        if (dtAmt.Rows.Count > 0)
        {
           for(int i=1;i<dtAmt.Rows.Count;i++)
            {
                strAdditionDeduction = strAdditionDeduction + "," + dtAmt.Rows[i]["ADDITIONDEDUCTION"].ToString();
                dAmt = dAmt + Convert.ToDouble(dtAmt.Rows[i]["Amount"]);
            }
        }
        double dNetAmt = dAmt + Convert.ToDouble(dtAmt.Rows[0]["Amount"].ToString());
        if(strAdditionDeduction != "")
            strAdditionDeduction = strAdditionDeduction.Substring(1);

        if (dtTemplate.Rows.Count > 0)
        {

            ldReplacements.Add("<%Date%>", DateTime.Now.ToString("dd MMM yyyy"));
            ldReplacements.Add("<%Company Name%>", dtTemplate.Rows[0]["Companyname"].ToString());
            ldReplacements.Add("<%POBOx%>", dtTemplate.Rows[0]["POBOx"].ToString());
            ldReplacements.Add("<%City%>", dtTemplate.Rows[0]["City"].ToString());
            ldReplacements.Add("<%Countryname%>", dtTemplate.Rows[0]["Country"].ToString());
            ldReplacements.Add("<%Employee Name%>", dtTemplate.Rows[0]["Employee"].ToString());
            ldReplacements.Add("<%Nationality%>", dtTemplate.Rows[0]["Nationality"].ToString());
            ldReplacements.Add("<%Designation%>", dtTemplate.Rows[0]["Designation"].ToString());
            ldReplacements.Add("<%DateofJoining%>", dtTemplate.Rows[0]["DateofJoining"].ToString());
            ldReplacements.Add("<%Currency%>", dtTemplate.Rows[0]["Currency"].ToString());
            ldReplacements.Add("<%Department%>", dtTemplate.Rows[0]["Department"].ToString());
            ldReplacements.Add("<%Basic Pay%>", dtAmt.Rows[0]["Amount"].ToString());           
            ldReplacements.Add("<%Amount%>",dAmt.ToString());
            ldReplacements.Add("<%GrossSalary%>", dNetAmt.ToString());
            ldReplacements.Add("<%Allowances%>", strAdditionDeduction);
            ldReplacements.Add("<%PassportNumber%>", dtTemplate.Rows[0]["PassportNumber"].ToString());
            ldReplacements.Add("<%Created By%>", dtTemplate.Rows[0]["CreatedBy"].ToString());
            ldReplacements.Add("<%CreatedBy Department%>", dtTemplate.Rows[0]["CreatedByDepartment"].ToString());
           
                                                
        }

        string sMessage = ajcSalaryCertificate.Content.Replace("{$$", "<%");
        sMessage = sMessage.Replace("$$}", "%>");
        MailDefinition mdSendOffer = new MailDefinition();
        mdSendOffer.From = "info@HrPower.com";
        mdSendOffer.Priority = MailPriority.High;
        MailMessage mmContent = mdSendOffer.CreateMailMessage("info@HrPower.com", ldReplacements, sMessage, this);

        return mmContent.Body;
       
    }
    

    protected void btnPreview_Click(object sender, EventArgs e)
    {
     
        literalContent.Text =  new clsCommon().GetHeader() + GetSalaryCertificateContent();
        mpePreview.Show();
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
     
        Table tbl = new Table();

        tbl.ID = "tblPrint";

        tbl.Attributes.Add("style", "font-family:Tahoma, MS Sans Serif; font-size:15px");
        tbl.Width = Unit.Percentage(100);

        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        td.Text = string.Empty;
        td.Text = new clsCommon().GetHeader() + GetSalaryCertificateContent();

        tr.Cells.Add(td);
        tbl.Rows.Add(tr); 

        pnlPrint.Controls.Add(tbl);

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Print", "setTimeout(function(){Print('" + pnlPrint.ClientID + "')},500)", true);

        

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {

        if (txtEmail.Text != "")
        {
            clsMailSettings objMailSettings = new clsMailSettings();

            if (objMailSettings.SendMail(txtEmail.Text, "Salary Certificate", GetSalaryCertificateContent(), clsMailSettings.AccountType.Email, true, ""))
            {
                msgs.InformationalMessage(clsGlobalization.IsArabicCulture() ? " تم ارسال شهادة راتب" : "Salary certificate has been sent.");
            }
            else
            {
                msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "تعذر إرسال شهادة راتب." : "Salary certificate could not be sent.");
            }
        }
        else
        {
            msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? " الموظف ليس لديه معرف البريد الإلكتروني" : "Employee has no E-mail id.");
        }
        mpeMessage.Show();
       // upMessage.Update();
       
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlEmployee.SelectedValue.ToInt32() > 0)
        {
            DataTable dt = objCertificate.GetEmail(ddlEmployee.SelectedValue.ToInt32());
            if (dt.Rows.Count > 0)
                txtEmail.Text = dt.Rows[0]["EmailID"].ToString();
        }
    }
}
