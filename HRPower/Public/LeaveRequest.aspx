<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="LeaveRequest.aspx.cs" Inherits="Public_LeaveRequest" %>
   
<%@ Register Src="../Controls/AttachDocument.ascx" TagName="AttachDocument" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=fvLeaveRequest.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Leave Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>
    
<style type="text/css">
  legend{
  font-size:15px;
  }
</style>

   <div class="trRight" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" Width="22px"/>
    </div>
    <div class="grid">
    <table style="width: 100%" cellpadding="4" cellspacing="0">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hfPostedUrl" runat="server" />
                        <asp:FormView ID="fvLeaveRequest" runat="server" Width="100%" DataKeyNames="LeaveTypeId,RequestId,Halfday,StatusId,RequestedTo,Employee"
                            CellSpacing="1" OnDataBound="fvLeaveRequest_DataBound" OnItemCommand="fvLeaveRequest_ItemCommand"
                            CssClass="labeltext">
                            <EditItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="trLeft" style="width: 20%">
                                                <%--  Leave Type--%>
                                                <asp:Literal runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <table style="width: 100%;">
                                               </td>
                                                <td></td>
                                                <td></td>
                                                </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DropDownList ID="ddlLeaveType" runat="server" DataTextField="Description" DataValueField="LeaveTypeId"
                                                                CssClass="dropdownlist_mandatory" AutoPostBack="true" OnSelectedIndexChanged="GetBalanceLeave">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlLeaveType"
                                                                Display="Dynamic" meta:resourcekey="PleaseSelectLeaveType" SetFocusOnError="True"
                                                                CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                                
                                                        </td>
                                                       <%-- <td>
                                                         <asp:CheckBox ID="chkEncash" Text=" Vacation Encashment"  runat="server" Checked='<%# Eval("IsEncashBool")==null ? false : Eval("IsEncashBool") %>' Enabled="true" style="padding-top: 8px; line-height:30px;margin-left: 135px"  OnCheckedChanged="chkEncash_CheckedChanged" AutoPostBack="true" CausesValidation="false" ></asp:CheckBox>
                                                        </td>--%>
                                                        <td style="width: 44px;" runat="server" id="tdBalanceLabel">
                                                            <%--   Balance Leave:--%>
                                                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="BalanceLeave"></asp:Literal>
                                                        </td>
                                                        <td style="width: 130px;" align="left">
                                                            <%--   Balance Leave:--%>
                                                            <asp:TextBox ID="txtBalanceLeave" runat="server" Enabled="false" CssClass="textbox_disabled"
                                                                Width="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <%-----------------------------------------------------Compensatory-Off Section--------------------------------------------------%>
                                        <tr id="trCreditLeave" runat="server" style="width: 100%;">
                                            <td colspan="2">
                                                <asp:UpdatePanel ID="upnlrblCreditLeave" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:RadioButtonList ID="rblCreditLeave" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                            OnSelectedIndexChanged="rblCreditLeave_SelectedIndexChanged" CellPadding="1"
                                                            Style="width: 166%;" CellSpacing="1">
                                                            <asp:ListItem Selected="True" Value="1" meta:resourcekey="IsCredit"></asp:ListItem>
                                                            <asp:ListItem Value="0" meta:resourcekey="IsLeave"></asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr id="trFromComp" runat="server" style="width: 100%; display: none">
                                            <td style="width: 85%; padding-left: 10px;" colspan="2">
                                                <asp:CheckBox ID="chkCO" runat="server" meta:resourcekey="FromComboOff" TextAlign="Left"
                                                    AutoPostBack="true" OnCheckedChanged="chkCO_CheckedChanged" CausesValidation="false" />
                                            </td>
                                        </tr>
                                        <tr id="trWorkedDay" runat="server" style="width: 100px; display: none;">
                                            <td style="width: 100%;" colspan="2">
                                                <table cellpadding="2" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:UpdatePanel ID="upnldlCODays" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:DataList ID="dlCODays" runat="server" Width="50%" CellPadding="10" BorderColor="#307296"
                                                                        OnItemCommand="dlCODays_ItemCommand" BorderWidth="1">
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                                                                        <HeaderTemplate>
                                                                            <div style="width: 500px; font-size: 11px; float: left;" class="datalistheader">
                                                                                <div style="width: 50%; float: left;">
                                                                                    <asp:Literal ID="Literal35" runat="server" meta:resourcekey="WorkedDates"></asp:Literal>
                                                                                </div>
                                                                                <div style="width: 15%; float: right;">
                                                                                    &nbsp;
                                                                                </div>
                                                                                <div style="width: 15%; float: left;">
                                                                                    &nbsp;
                                                                                </div>
                                                                            </div>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <div style="width: 500px; float: left; padding-top: 4px">
                                                                                <div style="width: 60%; float: left;">
                                                                                    <div style="float: left; width: 40%;">
                                                                                        <asp:TextBox ID="txtCODate" runat="server" CssClass="textbox_mandatory" AutoPostBack="true"
                                                                                            CausesValidation="true" ValidationGroup="AddnewP" OnTextChanged="txtCODate_TextChanged"
                                                                                            Width="100px" Text='<%# Eval("WorkedDay") %>'></asp:TextBox></div>
                                                                                    <div style="float: left; margin-top:9px; width: 15%;">
                                                                                        <asp:ImageButton ID="ibtnCOdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                                    </div>
                                                                                    <div style="float: right; width: 40%; padding-left: 3px;">
                                                                                        <AjaxControlToolkit:CalendarExtender ID="extenderCODate" runat="server" TargetControlID="txtCODate"
                                                                                            PopupButtonID="ibtnCOdate" Format="dd/MM/yyyy">
                                                                                        </AjaxControlToolkit:CalendarExtender>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtCODate" runat="server" ErrorMessage="*" ControlToValidate="txtCODate"
                                                                                            Display="Dynamic" CssClass="error"></asp:RequiredFieldValidator>
                                                                                        <asp:CustomValidator ID="cvCODate" runat="server" ValidateEmptyText="true" ControlToValidate="txtCODate"
                                                                                            Display="Dynamic" ValidationGroup="AddnewP" CssClass="error" ClientValidationFunction="CheckFuturedate"></asp:CustomValidator>
                                                                                    </div>
                                                                                </div>
                                                                                <div style="width: 15%; float: left;">
                                                                                    <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/Delete18x18.png"
                                                                                        meta:resourcekey="Remove" CommandName="Remove" CausesValidation="false" CommandArgument='<%# Eval("RequestId") %>' />
                                                                                </div>
                                                                                <div style="width: 15%; float: left; margin-left: -43px;">
                                                                                    <asp:ImageButton ID="imgAddDays" runat="server" ImageUrl="~/images/Addition Deduction.png"
                                                                                        meta:resourcekey="Addnew" ValidationGroup="AddnewP" OnClick="imgAddDays_Click" /><%--ToolTip="Add new "--%>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trNoComboOff" runat="server" style="width: 100%">
                                            <td class="trLeft" style="width: 20%">
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="NumberofComboOffs"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtNoComboOff" runat="server" CssClass="textbox_mandatory" Width="50px" style="margin-left: 7px;"
                                                    AutoPostBack="true" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <%-------------------------------------------------------------------------------------------------------------------------------%>
                                        <tr runat="server" id="trHalfDay">
                                            <td class="trLeft" style="width: 20%">
                                                <%--  Half Day--%>
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="HalfDay"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:RadioButtonList ID="rblhalfDay" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                    OnSelectedIndexChanged="CheckHalfDay" CellPadding="1" CellSpacing="1">
                                                    <%--  <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>--%>
                                                    <asp:ListItem meta:resourcekey="Yes" Value="1">
                                                       
                                                       
                                                    </asp:ListItem>
                                                    <asp:ListItem meta:resourcekey="No" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trFromDate" style="width: 100%">
                                            <td class="trLeft" style="width: 20%">
                                                <%--Leave From Date--%>
                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="LeaveFromDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="74%">
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    ValidationGroup="Submit" CausesValidation="true" AutoPostBack="true" OnTextChanged="GetBalanceLeave"
                                                    Text='<%# Eval("LeaveFromDate") %>'></asp:TextBox>
                                                <asp:ImageButton ID="ibtnLeaveFromdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CausesValidation="false" CssClass="imagebutton" Style="margin-left: 5px;margin-top: 9px;" />
                                                <AjaxControlToolkit:CalendarExtender ID="extenderLeaveFromDate" runat="server" TargetControlID="txtFromDate"
                                                    PopupButtonID="ibtnLeaveFromdate" Format="dd/MM/yyyy" Enabled="true">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="CustomValidator1" runat="server" ValidateEmptyText="true"
                                                    ControlToValidate="txtFromDate" Display="Dynamic" ClientValidationFunction="valRequestFromToDate"
                                                    ValidationGroup="Submit" CssClass="error"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr runat="server" id="trToDate" style="width: 100%">
                                            <td class="trLeft" style="width: 20%">
                                                <%--   Leave To Date--%>
                                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="LeaveToDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    OnTextChanged="CalculateTotalNoofVacation" Text='<%# Eval("LeaveToDate") %>'
                                                    AutoPostBack="true"></asp:TextBox>
                                                <asp:ImageButton ID="ibtnLeaveTodate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" CausesValidation="false" Style="margin-left: 5px;margin-top: 9px;" />
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate"
                                                    PopupButtonID="ibtnLeaveTodate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                                                    Display="Dynamic" ClientValidationFunction="valRequestFromToDate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                                <%-- <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                                                    Display="Dynamic" ClientValidationFunction="checkFromToDate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>--%>
                                                <asp:CustomValidator ID="cvCompare" runat="server" ValidationGroup="Submit" ControlToValidate="txtToDate"
                                                    ClientValidationFunction="confirmLeaveHolidays" CssClass="error" Display="Dynamic"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <%-----------------------------------------------------Travel & Document Details-------------------------------------------------%>
                                        <tr id="trTravelDocumentAdd" runat="server" style="width: 272%; display: none;">
                                            <td style="width: 348px;">
                                                <asp:UpdatePanel ID="upnlTravelDocumentAdd" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div id="divTotalVacation" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal45" runat="server" meta:resourcekey="TotalDays"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 4px;">
                                                                <asp:TextBox ID="lblVacationDays"   runat="server" Text='<%# Eval("NumberOfDays") %>' Width="50px" Enabled="false" ></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div id="trEligibleLeaves" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal42" runat="server" meta:resourcekey="EligibleLeaves"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 4px;">
                                                                <asp:TextBox ID="txtEligibleLeaves" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                                                    Width="60px" Visible="false" Enabled="false"></asp:TextBox>
                                                                <asp:Button ID="btnEligibleLeaves" runat="server" meta:resourcekey="ShowEligibleLeaves"
                                                                    OnClick="btnEligibleLeaves_Click" />
                                                            </div>
                                                        </div>
                                                        <%--<div id="trFlightRequired" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal34" runat="server" meta:resourcekey="FlightRequired"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 1px;">
                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:RadioButtonList ID="rblFlightRequired" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                            CellPadding="1" CellSpacing="1">
                                                                            <asp:ListItem Selected="True" Value="1" meta:resourcekey="Yes" Style="Margin-Right :10px;Margin-left:10px"></asp:ListItem>
                                                                            <asp:ListItem Value="0" meta:resourcekey="No"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                 <%--       <div id="trSectorRequired" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal36" runat="server" meta:resourcekey="SectorRequired"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 1px;">
                                                                <asp:UpdatePanel ID="upnlSectorRequired" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:RadioButtonList ID="rblSectorRequired" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                            CellPadding="1" CellSpacing="1">
                                                                            <asp:ListItem Selected="True" Value="1" meta:resourcekey="Yes" Style="Margin-Right :10px;Margin-left:10px"></asp:ListItem>
                                                                            <asp:ListItem Value="0" meta:resourcekey="No"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div id="trContactAddress" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal24" runat="server" meta:resourcekey="ContactAddress"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 0px;">
                                                                <asp:TextBox ID="txtContactAddress" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("ContactAddress") %>'
                                                                    Width="200px" Height="50px" onchange="RestrictMulilineLength(this, 300);" onkeyup="RestrictMulilineLength(this, 300);"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div id="trTelephoneNo" runat="server" style="width: 272%">
                                                            <div class="trLeft" style="float: left; width: 25%; padding-left: 5px;">
                                                                <asp:Literal ID="Literal25" runat="server" meta:resourcekey="ContactTelephoneNo"></asp:Literal>
                                                            </div>
                                                            <div class="trRight" style="float: left; width: 70%; padding-left: 0px;">
                                                                <asp:TextBox ID="txtTelephoneNo" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("TelephoneNo") %>'
                                                                    MaxLength="20" Width="150px"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <%---------------------------------------------------------------------------------------------------------------------------------%>
                                        <tr>
                                            <td class="trLeft" style="width: 20%">
                                                <%--  Reason--%>
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" style="width: 75%; padding-left: 8px;">
                                                <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Width="250px" Height="50px" onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLength(this, 500);"
                                                    TextMode="MultiLine"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                                                    Display="Dynamic" meta:resourcekey="PleaseEnterReason" SetFocusOnError="True"
                                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="trEncah" runat="server">
                                            <td class="trLeft" style="width: 20%">
                                                <asp:Literal ID="Literal38" runat="server" meta:resourcekey="EncashfromCompensatoryOff"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <table style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 48%;">
                                                            <asp:TextBox ID="txtEncashCombo" runat="server" Width="50px"></asp:TextBox>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="fteEncashCombo" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers" TargetControlID="txtEncashCombo">
                                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                                        </td>
                                                        <td style="width: 378px;" runat="server" id="td1">
                                                            <asp:Literal ID="Literal39" runat="server" meta:resourcekey="BalanceCompensatoryLeaves"></asp:Literal>
                                                        </td>
                                                        <td style="width: 130px;" align="left">
                                                            <asp:TextBox ID="txtBalanceCombo" runat="server" Enabled="false" CssClass="textbox_disabled"
                                                                Width="30"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="trLeft" width="25%">
                                                <%-- Status--%>
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    DataValueField="StatusId">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <%-- Attach Document Insert--%>
                                                <td class="trLeft" width="25%">
                                                    <uc1:AttachDocument ID="AttachDocument1" RequestType="LeaveRequest" runat="server" />
                                                </td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" width="75%" align="right" style="padding-left:16px">
                                                <asp:HiddenField ID="hfdHoliday" runat="server" />
                                                <asp:HiddenField ID="hfConfirm" runat="server" />
                                                <%--  ValidationGroup="Submit"--%>
                                                <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" OnClientClick="return confirmLeave(this);"
                                                    CommandName="Add" CommandArgument='<%# Eval("RequestId") %>' ValidationGroup="Submit" />&nbsp;<%-- --%>
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandName="CancelRequest"
                                                    CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                                    Width="75px" /><%--OnClientClick="return confirmLeave(this);" --%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="innerdivheader" style="width: 26%">
                                                <%-- Requested By--%>
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="60%" style="padding-left:7px">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee") %>'></asp:Label>
                                            </td>
                                            <%--<td>
                                            <asp:CheckBox ID="chkEncash1" Text="Vacation Encashment"   runat="server" Checked='<%# Eval("IsEncashBool")==null ? false : Eval("IsEncashBool") %>' Enabled="true" style="padding-top: 8px;line-height: 30px;float: left;width: 389px;"  OnCheckedChanged="chkEncash1_CheckedChanged"  AutoPostBack="true" CausesValidation="false" ></asp:CheckBox>
                                            </td>--%>
                                            <%--<td class="trRight" width="10%">
                                                <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                                                    meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                                            </td>--%>
                                        </tr>
                                        <tr>
                                             <td class="innerdivheader" style="width: 26%">
                                                <asp:Literal ID="LtlLeaveType" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                                            </td>
                                            <td style="padding-left:7px">
                                                <asp:Label ID="lblLeaveType" runat="server" Text='<%# Eval("LeaveType") %>'></asp:Label>
                                            </td>
                                            <td class="innerdivheader" style="width: auto;" runat="server" id="tdBal">
                                                <%--   Balance Leave:--%>
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="BalanceLeave"></asp:Literal>
                                            </td>
                                            <td style="width: auto;" align="left" class="trLeft">
                                                <%--   Balance Leave:--%>
                                                <asp:TextBox ID="txtBal" runat="server" Enabled="false" CssClass="textbox_disabled"
                                                    Width="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trHalfDayView" runat="server">
                                             <td class="innerdivheader" style="width: 26%">
                                                <%--   Half day--%>
                                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="HalfDay"></asp:Literal>
                                            </td>
                                            <td class="trRight" style="padding-left:7px">
                                                <asp:Label ID="lblHalfDay" runat="server" Text='<%# Eval("HalfDay") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <%----------------Combo-Off------------------%>
                                        <tr id="trCompType" runat="server" style="display: none;">
                                            <td class="innerdivheader">
                                                <asp:Literal ID="Literal37" runat="server" meta:resourcekey="FromComboOff"></asp:Literal><%--meta:resourcekey="NoOfDays"--%>
                                            </td>
                                            <td class="trRight">
                                                <asp:Label ID="lblFromComboOff" runat="server" Text='<%# Eval("IsFromCombo") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <!-- display  days!-->
                                        <tr id="trComboOff" runat="server" style="display: none; width: 100%; height: auto;
                                            padding-left: 88px; overflow: scroll;" align="left">
                                            <td colspan="2">
                                                <asp:GridView ID="gvComboOff" runat="server" AutoGenerateColumns="false" Width="30%"
                                                    CellPadding="10" BorderColor="#307296">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:BoundField DataField="WorkedDay" meta:resourcekey="WorkedDates1" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr id="trCount" runat="server" style="width: 361%;">
                                            <td class="innerdivheader" style="width: 92.8%;">
                                                <asp:Literal ID="Literal23" runat="server" meta:resourcekey="NumberofComboOffs"></asp:Literal><%--meta:resourcekey="NoOfDays"--%>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblDuration" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <%------------------------------------------------------------------------%>
                                        <tr id="trFrom" runat="server" style="width: 361%;">
                                            <td class="innerdivheader" width="25%">
                                                <%-- Leave From Date--%>
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="LeaveFromDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblFromPeriod" runat="server" Text='<%# Eval("FromDate") %>'></asp:Label>
                                                <div runat="server" id="divApprovalFromDate" style="display: none; width: 100%">
                                                    <asp:TextBox ID="txtApprovalFromDate" runat="server" CssClass="textbox_mandatory"
                                                        Width="100px" AutoPostBack="true" OnTextChanged="GetNewBalanceLeave"></asp:TextBox>
                                                    <asp:ImageButton ID="ibtnLeaveApprovalFromdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CausesValidation="false" CssClass="imgbuttons" Style="margin-left: 5px" />
                                                    <AjaxControlToolkit:CalendarExtender ID="extenderLeaveApprovalFromDate" runat="server"
                                                        TargetControlID="txtApprovalFromDate" PopupButtonID="ibtnLeaveApprovalFromdate"
                                                        Format="dd/MM/yyyy" Enabled="true">
                                                    </AjaxControlToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtApprovalFromDate"
                                                        Display="Dynamic" ClientValidationFunction="checkFromToDateApproval" ValidationGroup="Submit"
                                                        CssClass="error"></asp:CustomValidator>
                                                </div>
                                                <asp:HiddenField ID="hdLeaveType" runat="server" Value='<%# Eval("LeaveTypeId") %>' />
                                                <asp:HiddenField ID="hdEmployeeID" runat="server" Value='<%# Eval("EmployeeId") %>' />
                                            </td>
                                        </tr>
                                        <tr id="trTo" runat="server" style="width: 361%;">
                                            <td class="innerdivheader" width="25%">
                                                <%-- Leave To Date--%>
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="LeaveToDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblToPeriod" runat="server" Text='<%# Eval("ToDate") %>'></asp:Label>
                                                <div runat="server" id="divApprovalToDate" style="display: none; width: 100%">
                                                    <asp:TextBox ID="txtApprovalToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                        AutoPostBack="true" OnTextChanged="GetNewBalanceLeave"></asp:TextBox>
                                                    <asp:ImageButton ID="ibtnLeaveApprovalTodate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CssClass="imgbuttons" CausesValidation="false" Style="margin-left: 5px" />
                                                    <AjaxControlToolkit:CalendarExtender ID="ceApprovalToDate" runat="server" TargetControlID="txtApprovalToDate"
                                                        PopupButtonID="ibtnLeaveApprovalTodate" Format="dd/MM/yyyy">
                                                    </AjaxControlToolkit:CalendarExtender>
                                                    <%--   <asp:CustomValidator ID="cvCompare" runat="server" ValidationGroup="Submit" ErrorMessage="Leave to date should be later than leave from date"
                                                        ControlToValidate="txtApprovalToDate" ClientValidationFunction="confirmLeaveHolidays"
                                                        CssClass="error" Display="Dynamic"></asp:CustomValidator>--%>
                                                    <asp:CustomValidator ID="cvCompare" runat="server" ValidationGroup="Submit" meta:resourcekey="ToDateMustBeGreaterThanFromDate"
                                                        ControlToValidate="txtApprovalToDate" ClientValidationFunction="confirmLeaveHolidays"
                                                        CssClass="error" Display="Dynamic"></asp:CustomValidator>
                                                </div>
                                                <%--   <%# Eval("ToDate") %>--%>
                                            </td>
                                        </tr>
                                        <%----------------------------------Travel & Documents View-----------------------------------------------%>
                                        <tr id="trTravelDocument" runat="server" style="display: none; width: 361%;">
                                            <td style="padding: 0px;">
                                                <table id="tdTravel" runat="server" style="width: 316%;" cellpadding="5" cellspacing="0">
                                                    <tr id="trTotalVacationView" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="width: 25%;">
                                                            <asp:Literal ID="Literal44" runat="server" meta:resourcekey="TotalDays"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="width: 70%; padding-left: 45px;">
                                                         <asp:TextBox ID="lblTotalLeaves" runat="server" Text='<%# Eval("NumberOfDays") %>'  Enabled="false"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trEligibleLeavesView" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="width: 25%;">
                                                            <asp:Literal ID="Literal43" runat="server" meta:resourcekey="EligibleLeaves"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="width: 75%; padding-left: 60px;">
                                                            <asp:TextBox ID="lblEligibleLeaves" runat="server" Width="50px" Visible="false" Enabled="false"></asp:TextBox>
                                                            <asp:Button ID="btnEligibleLeavesShow" runat="server" OnClick="btnEligibleLeaves_Click"
                                                                meta:resourcekey="ShowEligibleLeaves" />
                                                        </td>
                                                    </tr>
                                                    <%--<tr id="trFlightRequiredView" runat="server">
                                                        <td class="innerdivheader" style="width: 25%;">
                                                            <asp:Literal ID="Literal26" runat="server" meta:resourcekey="FlightRequired"></asp:Literal>
                                                        </td>--%>
                                                       <%-- <td class="trRight" style="width: 75%; padding-left: 60px;">
                                                            <asp:Label ID="lblFlightRequired" runat="server" Text='<%# Eval("IsFlightRequired") %>'></asp:Label>
                                                        </td>--%>
                                                    <%--</tr>
                                                    <tr id="trSectorRequiredView" runat="server">
                                                        <td class="innerdivheader" style="width: 25%;">
                                                            <asp:Literal ID="Literal27" runat="server" meta:resourcekey="SectorRequired"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="width: 75%; padding-left: 60px;">
                                                            <asp:Label ID="lblSectorRequired" runat="server" Text='<%# Eval("IsSectorRequired") %>'></asp:Label>
                                                        </td>
                                                    </tr>--%>
                                                    <tr id="trContactAddressView" runat="server">
                                                        <td class="innerdivheader" style="vertical-align: top" width="25%">
                                                            <asp:Literal ID="Literal28" runat="server" meta:resourcekey="ContactAddress"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="width: 75%; padding-left: 60px; word-break: break-all;">
                                                            <asp:Label ID="lblContactAddress" runat="server" Text='<%# Eval("ContactAddress") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trTelephoneNoView" runat="server">
                                                        <td class="innerdivheader" style="vertical-align: top" width="25%">
                                                            <asp:Literal ID="Literal29" runat="server" meta:resourcekey="ContactTelephoneNo"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="width: 75%; padding-left: 60px;">
                                                            <asp:Label ID="lblTelephoneNo" runat="server" Text='<%# Eval("TelephoneNo") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trPassportNo" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="vertical-align: top;">
                                                            <asp:Literal ID="Literal30" runat="server" meta:resourcekey="PassportNo"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="padding-left: 60px;">
                                                            <asp:Label ID="lblPassportNo" runat="server" Text='<%# Eval("PassportNumber") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trPassportExpiryDate" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="vertical-align: top;">
                                                            <asp:Literal ID="Literal31" runat="server" meta:resourcekey="ExpiryDate"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="padding-left: 60px;">
                                                            <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Eval("ExpiryDate") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trLCExpiry" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="vertical-align: top;">
                                                            <asp:Literal ID="Literal32" runat="server" meta:resourcekey="LCExpiry"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="padding-left: 60px;">
                                                            <asp:Label ID="lblLCExpiry" runat="server" Text='<%# Eval("LCExpiryDate") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trVisaExpiry" runat="server" style="display: none;">
                                                        <td class="innerdivheader" style="vertical-align: top;">
                                                            <asp:Literal ID="Literal33" runat="server" meta:resourcekey="VisaExpiry"></asp:Literal>
                                                        </td>
                                                        <td class="trRight" style="padding-left: 60px;">
                                                            <asp:Label ID="lblVisaExpiry" runat="server" Text='<%# Eval("VisaExpiryDate") %>'> </asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="height: auto; padding-left: 10px; overflow: scroll;">
                                            <td colspan="2">
                                                <div>
                                                    <asp:GridView ID="gvPreviousLoanDetails" runat="server" AutoGenerateColumns="false"
                                                        CellPadding="10" BorderColor="#307296">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:BoundField DataField="LoanNumber" meta:resourcekey="LoanNumber" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="LoanType" meta:resourcekey="LoanType" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="LoanDate" meta:resourcekey="LoanDate" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="LoanAmount" meta:resourcekey="LoanAmount" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="AmountRepaid" meta:resourcekey="AmountRepaid" ItemStyle-HorizontalAlign="Center" />
                                                            <asp:BoundField DataField="DueAmount" meta:resourcekey="DueAmount" ItemStyle-HorizontalAlign="Center" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <%--------------------------------------------------------------------------------------------------------%>
                                        <tr id="trEncashView" runat="server" style="width: 100%; display: none;">
                                            <td class="innerdivheader">
                                                <asp:Literal ID="Literal40" runat="server" meta:resourcekey="EncashfromCompensatoryOff"></asp:Literal>
                                            </td>
                                            <td style="width: auto;padding-left: 20px;" align="left">
                                                <asp:Label ID="lblEncashEdit" runat="server" Text='<%# Eval("EncashComboOffDay") %>'></asp:Label>
                                                <asp:TextBox ID="txtEncashComboEdit" runat="server" Width="50px" Text='<%# Eval("EncashComboOffDay") %>'
                                                    Visible="false"></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="fteEncashComboEdit" runat="server"
                                                    FilterMode="ValidChars" FilterType="Numbers" TargetControlID="txtEncashComboEdit">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                            </td>
                                            <td class="innerdivheader" style="width: 246px; float: right; margin-left: 32px;"
                                                runat="server">
                                                <asp:Literal ID="Literal41" runat="server" meta:resourcekey="BalanceCompensatoryLeaves"></asp:Literal>
                                                  <asp:TextBox ID="txtBalanceComboEdit" runat="server" Enabled="false" CssClass="textbox_disabled"
                                                    Width="30" style="float: right"></asp:TextBox>
                                            </td>
                                           
                                        </tr>
                                        <tr id="trUnpaid" runat="server" style="display: none;">
                                            <td class="innerdivheader" style="width: 25%">
                                                <%--   Unpaid--%>
                                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="UnPaid"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%;">
                                                            <asp:CheckBox ID="chkUnPaid" runat="server" meta:resourcekey="UnPaid" />
                                                        </td>
                                                        <td style="width: 10%;">
                                                            <%--  Balance Leave--%>
                                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="BalanceLeave"></asp:Literal>
                                                        </td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txtBalanceLeave" runat="server" CssClass="textbox_disabled" Enabled="false"
                                                                Width="30px"></asp:TextBox>
                                                            <asp:HiddenField ID="hdConfirm" runat="server" Value="0" />
                                                        </td>
                                                        <td style="width: 10%;">
                                                            <%--  No Of Holidays--%>
                                                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="NoOfHolidays"></asp:Literal>
                                                        </td>
                                                        <td style="width: 20%;">
                                                            <asp:TextBox ID="txtNoOfHolidays" runat="server" CssClass="textbox_disabled" Enabled="false"
                                                                Width="30px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td class="innerdivheader" style="width: 25%">
                                                <%--    Status--%>
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    Visible="false" DataValueField="StatusId">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td class="innerdivheader" style="vertical-align: top; width: 25%">
                                                <%--  Reason--%>
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%" style="word-break: break-all">
                                                <%--                 <%# ArrangeString(Eval("Reason").ToString(),60) %>--%>
                                                <asp:Label ID="lblReason" runat="server" Text='<%# Eval("Reason") %>'> </asp:Label>
                                                <asp:TextBox ID="txtEditReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Visible="false" Width="250px" Height="50px" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trlink">
                                            <td class="trLeft" width="50%" colspan="2">
                                                <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                                                    OnClick="lbReason_OnClick"></asp:LinkButton>
                                                <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trReasons">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                                    Width="500px">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                                        <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                                            ItemStyle-Width="130px" />
                                                        <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                                        <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none; width: 100%;" id="trAuthority">
                                            <td class="innerdivheader" style="width: 25%">
                                                <asp:Label ID="lbl1" runat="server" Text="Approval Hierarchy"></asp:Label>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                                    <ItemStyle ForeColor="#307296" Font-Bold="true" />
                                                    <ItemTemplate>
                                                        <asp:Table Width="99%">
                                                            <tr style="width: 99%; color: #307296;">
                                                                <td>
                                                                    <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                                                    <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </asp:Table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="innerdivheader" style="width: 25%">
                                                <%--  Requested To--%>
                                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <%# Eval("Requested") %>
                                            </td>
                                        </tr>
                                        <tr id="trForwardedBy" runat="server" visible='<%# GetVisibility(Eval("ForwardedBy"))%>'>
                                            <td class="innerdivheader" style="width: 25%">
                                                <%--  Forwarded By--%>
                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="ForwardedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <%# Eval("ForwardedBy")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <uc1:AttachDocument IsViewOnly="true" ID="ViewAttachDocuments" RequestType="LeaveRequest"
                                                    runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <%--    Attach Document--%>
                                            <td class="trLeft" width="25%">
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:HiddenField ID="hfChkHoliday" runat="server" />
                                                <asp:HiddenField ID="hfConfirmHoliday" runat="server" />
                                                <asp:Button ID="btnApprove" Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                                    runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    CommandName="Approve" Visible="false" OnClientClick="return confirmLeave(this);" />
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Submit%>' CommandArgument=' <%# Eval("StatusId") %>'
                                                    CommandName="RequestCancel" Visible="false" />
                                                <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Cancel%>' CommandName="Cancel" Visible="false" />
                                                <asp:Button ID="btnRequestForCancel" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Submit%>' CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    CommandName="RequestForCancel" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlLeaveRequest" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td valign="middle">
                <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlLeaveRequest" runat="server" Width="100%" DataKeyField="requestId"
                            OnItemCommand="dlLeaveRequest_ItemCommand" OnItemDataBound="dlLeaveRequest_ItemDataBound"
                            CssClass="labeltext">
                            <ItemStyle CssClass="labeltext" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="25" valign="top" style="padding-left: 5px">
                                            <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkLeave')" />
                                        </td>
                                        <td style="padding-left: 7px">
                                            <%--  Select All--%>
                                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField Value='<%# Eval("requestId")%>' ID="hdRequestID" runat="server" />
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server">
                                    <%--onmouseover="showCancel(this);" onmouseout="hideCancel(this);"--%>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td valign="top" rowspan="6" width="25" style="padding-left: 7px">
                                                        <asp:CheckBox ID="chkLeave" runat="server" />
                                                    </td>
                                                    <td width="100%">
                                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                            <tr>
                                                                <td valign="top" style="width: 488px">
                                                                    <asp:LinkButton ID="lnkLeave" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_View" CausesValidation="False"><%# Eval("Date")%></asp:LinkButton>
                                                                    <asp:HiddenField ID="LeaveType" runat="server" Value='<%# Eval("LeaveTypeId") %>' />
                                                                </td>
                                                                <td valign="top" align="right" width="40px">
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                                                        ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                                                                </td>
                                                                <td valign="top" align="left" width="40px">
                                                                    <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Cancel" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/cancel.png"
                                                                        ToolTip='<%$Resources:ControlsCommon,Cancel%>'></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding-left: 25px">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="90%">
                                                            <tr>
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%--  Leave Type--%>
                                                                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("LeaveType") %>
                                                                    <asp:HiddenField runat="server" ID="hdLeaveTypeID" Value='<%# Eval("LeaveTypeID") %>' />
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr runat="server" id="trRequestedDate">
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%--   Requested Date--%>
                                                                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="RequestedDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("LeaveFromDate") %>
                                                                    -
                                                                    <%# Eval("LeaveToDate")%>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trApprovalDate" style="display: none">
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%-- Approved Date--%>
                                                                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="ApprovedDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("ApprovedFromDate")%>
                                                                    -
                                                                    <%# Eval("ApprovedToDate")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%--  Reason--%>
                                                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" style="word-break: break-all">
                                                                    <%# Convert.ToString(Eval("Reason")) %>
                                                                    <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                                    <asp:HiddenField ID="hdIsVacation" runat="server" Value='<%# Eval("IsVacation") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%--Status--%>
                                                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("status") %>
                                                                    <%--   <%# Eval("RequestStatusDate")%> --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%-- Requested To--%>
                                                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("Requested")  %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc:Pager ID="LeaveRequestPager" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <%-- <asp:AsyncPostBackTrigger ControlID="dlLeaveRequest" EventName="ItemCommand" />--%>
                        <asp:AsyncPostBackTrigger ControlID="fvLeaveRequest" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    </div>
    
    
    
    
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add leave request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" meta:resourcekey="RequestLeave" CausesValidation="false"
                                OnClick="lnkAdd_Click">
                         
                              <%--  <asp:Literal ID="Literal21" runat="server"></asp:Literal>--%>
                           
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/Leave_request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" CausesValidation="false" runat="server" meta:resourcekey="ViewRequest"
                                OnClick="lnkView_Click">
                         
                           <%--     <asp:Literal ID="Literal23" runat="server" ></asp:Literal>
--%>
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" meta:resourcekey="Del" runat="server" OnClick="lnkDelete_Click">
                             <%--   <asp:Label id="Literal24"   runat="server" ></asp:Label>--%>
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                        
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
