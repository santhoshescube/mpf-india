﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_PerformanceEvaluation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hfIsArabic.Value = clsGlobalization.IsArabicCulture().ToString(); 
        this.Title = GetLocalResourceObject("PerformanceEvaluation.Text").ToString();
        if (!IsPostBack)
        {
            GetAllPendingEvaluations();
            SetPermission();
        }
        EnableDisableMenus();
    }




    private void EnableDisableMenus()
    {
        if (ViewState["IsCreate"] != null && Convert.ToBoolean(ViewState["IsCreate"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }

        lnkPendingEvaluations.Enabled =  Convert.ToBoolean(ViewState["IsCreate"]);
        lnkCompletedEvaluation.Enabled = Convert.ToBoolean(ViewState["IsView"]);

       
    }


    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerformanceEvaluation);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }
    private void GetAllPendingEvaluations()
    {
        dlEmployeeList.DataSource = clsPerformanceEvaluation.GetAllPendingEvaluation(new clsUserMaster().GetEmployeeId());
        dlEmployeeList.DataBind();

        if (((DataTable)dlEmployeeList.DataSource).Rows.Count == 0)
        {
            dvEmployeeList.Visible = Header.Visible = false;
            dvNoRecord.Visible = true;
        }
        else
        {
            dvEmployeeList.Visible = Header.Visible = true;
            dvNoRecord.Visible = false;
        }
    }
    protected void btnEvaluate_Click(object sender, EventArgs e)
    {
    }
    protected void dlEmployeeList_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Evaluate")
        {
           
        }
    }
    protected void dlEmployeeList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
       


    }
    protected void btnEvaluate_Click1(object sender, EventArgs e)
    {

        string CommandArgument = ((System.Web.UI.WebControls.Button)(sender)).CommandArgument.ToString();
        HiddenField hfEmployeeFullName = (HiddenField)((System.Web.UI.WebControls.WebControl)(sender)).Parent.FindControl("hfEmployeeFullName");
        hfTemplateID.Value = CommandArgument.Split('@')[0].ToString();
        hfPerformanceInitiationID.Value = CommandArgument.Split('@')[1].Split('^')[0].ToString();
        hfEmployeeID.Value = CommandArgument.Split('@')[1].Split('^')[1].ToString();

        hfPerformanceInitiationEvaluationID.Value = ((HiddenField)((System.Web.UI.WebControls.WebControl)(sender)).Parent.FindControl("hfPerformanceInitiationEvaluationID")).Value;

        DataTable dtMarks = new DataTable();
        dtMarks.Columns.Add("MarkID");
        dtMarks.Columns.Add("Mark");

        DataRow dr = null;
        DataTable dt = clsPerformanceEvaluation.GetAllGoals(hfPerformanceInitiationID.Value.ToInt32(), hfTemplateID.Value.ToInt32(), new clsUserMaster().GetEmployeeId(), hfEmployeeID.Value.ToInt32());
        if (dt.Rows.Count > 0)
        {
            bool IsGrading = Convert.ToBoolean(dt.Rows[0]["IsGrading"]);
            hfIsGrade.Value = IsGrading.ToString();
            decimal Marks = dt.Rows[0]["MaxMarksPerQuestion"].ToDecimal();

            if (IsGrading == false)
            {

                for (decimal i = 0; i <= Marks; i++)
                {
                    dr = dtMarks.NewRow();
                    dr["Mark"] = i.ToString();
                    dr["MarkID"] = i.ToString();
                    dtMarks.Rows.Add(dr);
                }

            }
            else
                dtMarks = clsPerformanceEvaluation.GetAllGrade();


            ViewState["dtMarks"] = dtMarks;
        }

        if (hfEmployeeFullName != null)
            lblHeading.Text =GetGlobalResourceObject("ControlsCommon","Employee").ToString()+":" +hfEmployeeFullName.Value.ToString();
        dlGoal.DataSource = dt;
        dlGoal.DataBind();

        mdGoal.Show();
        updGoal.Update();

    }
    protected void dlGoal_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            DropDownList ddlMark = ((DropDownList)e.Item.FindControl("ddlMark"));

            if (ddlMark != null)
            {
                DataTable dtMarks = ((DataTable)ViewState["dtMarks"]);
                ddlMark.DataSource = dtMarks;
                ddlMark.DataTextField = "Mark";
                ddlMark.DataValueField = "MarkID";
                ddlMark.DataBind();
                ddlMark.Items.Insert(0,(new ListItem() { Text =GetLocalResourceObject("Select.Text").ToString(), Value = "-1" }));
            }
        }
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void save_Click(object sender, EventArgs e)
    {
        clsPerformanceEvaluation objPeformanceEvaluation = new clsPerformanceEvaluation();
        objPeformanceEvaluation.EmployeeID = hfEmployeeID.Value.ToInt32();
        objPeformanceEvaluation.EvaluatorID = new clsUserMaster().GetEmployeeId();
        objPeformanceEvaluation.PeformanceInitiationID = hfPerformanceInitiationID.Value.ToInt32();
        objPeformanceEvaluation.IsGrade = Convert.ToBoolean(hfIsGrade.Value);
        objPeformanceEvaluation.PerformanceInitiationEvaluationID = hfPerformanceInitiationEvaluationID.Value.ToInt32();
        objPeformanceEvaluation.TemplateID = hfTemplateID.Value.ToInt32();
 
        System.Collections.Generic.List<clsGoalDetails> GoalList = new System.Collections.Generic.List<clsGoalDetails>();
        objPeformanceEvaluation.EvaluationDetailsID = dlGoal.DataKeyField[0].ToInt32();

        foreach (DataListItem dl in dlGoal.Items)
        {
            if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
            {
                HiddenField hfGoalID = (HiddenField)dl.FindControl("hfGoalID");
                DropDownList ddlMark = (DropDownList)dl.FindControl("ddlMark");

                if (hfGoalID != null && ddlMark != null)
                {
                    GoalList.Add(new clsGoalDetails()
                    {
                        GoalID = hfGoalID.Value.ToInt32(),
                        GradeID = ddlMark.SelectedValue.ToInt32(),
                        Mark = ddlMark.SelectedValue.ToDecimal(),
                    });

                }
            }

        }
        objPeformanceEvaluation.GoalList = GoalList;
        if (objPeformanceEvaluation.SavePerformanceEvaluation())
        {

            ////Delete message only if there is no pending initiation for the evaluator agains the initiationid
            //if(clsPerformanceEvaluation.GetPendingEvaluationsByInitiationID(objPeformanceEvaluation.PeformanceInitiationID,new clsUserMaster().GetEmployeeId()) ==0)  
            //    clsCommonMessage.DeleteMessages(objPeformanceEvaluation.PeformanceInitiationID, eReferenceTypes.PerformanceEvaluation, new clsUserMaster().GetEmployeeId());  



            //Delete message only if there is no pending initiation for the evaluator agains the initiationid
            if (clsPerformanceEvaluation.GetPendingEvaluationsByInitiationID(objPeformanceEvaluation.PeformanceInitiationID, new clsUserMaster().GetEmployeeId()) == 0)
                clsCommonMessage.DeleteMessages(objPeformanceEvaluation.PeformanceInitiationID, eReferenceTypes.PerformanceEvaluation, new clsUserMaster().GetEmployeeId());  
         



         
            ScriptManager.RegisterClientScriptBlock(updGoal, updGoal.GetType(), new Guid().ToString(), "alert('"+GetGlobalResourceObject("DocumentsCommon","SavedSuccessfully")+"');", true);
            GetAllPendingEvaluations();
        }
    }
  
    protected void lnkCompletedEvaluation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/ViewPerformanceEvaluation.aspx");
    }
}
