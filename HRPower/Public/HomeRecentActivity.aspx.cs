﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_HomeRecentActivity : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("HomeRecentActivity.Text").ToString();
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            int RoleID = objUserMaster.GetRoleId();
            if (RoleID <= 3 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.RecentActivity))) // 
            {
                ClearLists();
                LoadCombo();
                BindRecentActivity();

                //Announcements
                rptAnnouncements.DataSource = objHomePage.GetAnnouncements(objUserMaster.GetEmployeeId(),ddlCompany.SelectedValue.ToInt32());
                rptAnnouncements.DataBind();
            }
            else
            {
                lblMessage.Style["display"] = "block";
                //lblMessage.Text = "You have no permission to view this page";
                lblMessage.Text = GetLocalResourceObject("YouHaveNoPermissionToViewThisPage.Text").ToString();
            }
        }
    }
    private void ClearLists()
    {
        dlRecentActivity.DataSource = null;
        dlRecentActivity.DataBind();
        ViewState["BuddyId"] = null;
    }

    private void LoadCombo()
    {
        objHomePage = new clsHomePage();

        ddlCompany.DataSource = objHomePage.GetAllCompany(objUserMaster.GetUserId() , objUserMaster.GetUserId());
        ddlCompany.DataBind();
        //ddlCompany.Items.Insert(0, new ListItem(GetLocalResourceObject("AllCompany.Text").ToString(), "-1"));

        ddlVacancyActivity.DataSource = objHomePage.GetAllActivityVacancy();
        ddlVacancyActivity.DataBind();
        ddlVacancyActivity.Items.Insert(0, new ListItem(GetLocalResourceObject("AllVacancies.Text").ToString(), "-1"));

        ddlCandidate.DataSource = objHomePage.GetAllActivityCandidates();
        ddlCandidate.DataBind();
        ddlCandidate.Items.Insert(0, new ListItem(GetLocalResourceObject("AllCandidates.Text").ToString(), "-1"));

        ddlRecentActivityFilter.Items.Add(new ListItem(GetLocalResourceObject("AnyDate.Text").ToString(), "-1"));
        ddlRecentActivityFilter.Items.Add(new ListItem(GetLocalResourceObject("Today.Text").ToString(), DateTime.Now.ToString("dd MMM yyyy")));
        ddlRecentActivityFilter.Items.Add(new ListItem(GetLocalResourceObject("Last7days.Text").ToString(), DateTime.Now.Date.AddDays(-7).ToString("dd MMM yyyy")));
        ddlRecentActivityFilter.Items.Add(new ListItem(GetLocalResourceObject("Last30days.Text").ToString(), DateTime.Now.Date.AddDays(-30).ToString("dd MMM yyyy")));
        ddlRecentActivityFilter.Items.Add(new ListItem(GetLocalResourceObject("Last6months.Text").ToString(), DateTime.Now.AddMonths(-6).ToString("dd MMM yyyy")));

    }


    protected void BindRecentActivity()
    {
        lblMessage.Text = string.Empty;
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
       // DataSet ds = objHomePage.GetRecentActivity(objUserMaster.GetEmployeeId(), Convert.ToInt32(ddlVacancyActivity.SelectedValue), Convert.ToInt32(ddlCandidate.SelectedValue), pgrRecentActivity.CurrentPage + 1, pgrRecentActivity.PageSize, ddlRecentActivityFilter.SelectedValue);
        DataSet ds = objHomePage.GetRecentActivity(objUserMaster.GetUserId(), Convert.ToInt32(ddlVacancyActivity.SelectedValue), Convert.ToInt32(ddlCandidate.SelectedValue), pgrRecentActivity.CurrentPage + 1, pgrRecentActivity.PageSize, ddlRecentActivityFilter.SelectedValue, ddlCompany.SelectedValue.ToInt32());
        ViewState["Date"] = null;
        if (ds.Tables[0].Rows.Count == 0)
        {
            dlRecentActivity.ShowHeader = false;
            pgrRecentActivity.Total = 0;
            pgrRecentActivity.Visible = false;
        }
        else
        {
            dlRecentActivity.ShowHeader = true;
            pgrRecentActivity.Total = Convert.ToInt32(ds.Tables[0].Rows[0]["Total"]);
            pgrRecentActivity.Visible = true;
        }

        dlRecentActivity.DataSource = ds;
        dlRecentActivity.DataBind();

        if (dlRecentActivity.Items.Count == 0)
        {
            lblMessage.Style["display"] = "block";
            //lblMessage.Text = "No recent activities";
            lblMessage.Text = GetLocalResourceObject("NoRecentActivities.Text").ToString();
        }
        else
        {
            lblMessage.Style["display"] = "none";
        }
    }
    protected void ddlVacancyActivity_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrRecentActivity.CurrentPage = 0;
        BindRecentActivity();
    }
    protected void ddlCandidate_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrRecentActivity.CurrentPage = 0;
        BindRecentActivity();
    }
    protected void ddlRecentActivityFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindRecentActivity();
    }
    public string SetDate(object oActivityDate)
    {
        if (ViewState["Date"] == null)
        {
            ViewState["Date"] = oActivityDate;
            return Convert.ToString(oActivityDate);

        }
        else if (Convert.ToString(ViewState["Date"]) == Convert.ToString(oActivityDate))
        {
            return string.Empty;
        }
        else
        {
            ViewState["Date"] = oActivityDate;
            return Convert.ToString(oActivityDate);
        }
    }
    /// <summary>
    /// Returns the activity title depends on the activity type
    /// </summary>
    protected string GenerateRecentActivityTitle
    (
        object EmployeeID,
        object JobID,
        object JobTitle,
        object CandidateId,
        object CandidateName,
        object CandidateNameArb,
        object JobScheduleID,
        object OfferID,
        object RecentActivityTime,
        object RecentActivityTypeId
    )
    {
        string title = string.Empty;
        string tooltip ="";

        string heading = "";

        if (clsGlobalization.IsArabicCulture())
        {
            switch (Convert.ToInt32(RecentActivityTypeId))
            {
                case 1://Insert vacancy
                    heading = "انقر هنا لترى تفاصيل الوظيفة";
                    title = string.Format("نشرت فتح فرص العمل <a href='vacancy.aspx?JobID={0}' title='" + heading + "'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;

                case 2://Update vacancy
                    heading = "انقر هنا لترى تفاصيل الوظيفة";
                    title = string.Format("تحديث فتح فرص العمل <a href='vacancy.aspx?JobID={0}' title='" + heading + "'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;

                case 3://Insert candidate
                    heading = "انقر هنا لمشاهدة تفاصيل المرشح";
                    title = string.Format("وأضاف المرشح <a href='CandidateEng.aspx?CandidateId={0}' title='" + heading + "'>{1}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim());
                    break;

                case 4://Update candidate
                    heading = "انقر هنا لمشاهدة تفاصيل المرشح";
                    title = string.Format("مرشح تحديث <a href='CandidateEng.aspx?CandidateId={0}' title='" + heading + "'>{1}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim());
                    break;

                case 5://Schedule
                    heading = "انقر هنا لمشاهدة تفاصيل الجدول الزمني";
                    title = string.Format("من المقرر <a href='InterviewSchedule.aspx?JobScheduleId={0}' title='" + heading + "'>مقابلة</a> إلى <a href='vacancy.aspx?JobID={1}'>{2}</a>", JobScheduleID, JobID, JobTitle);
                    break;

                case 6://Association
                    heading = "انقر هنا لمشاهدة تفاصيل المرشح";
                    tooltip =" انقر هنا لمشاهدة تفاصيل الشغور";
                    title = string.Format("المرتبطة <a href='CandidateEng.aspx?CandidateId={0}' title='" + heading + "'>{1}</a> with <a href='vacancy.aspx?JobID={2}' title= " + tooltip + "  >{3}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim(), JobID, JobTitle);
                    break;

                case 7://Hire
                    heading = "انقر هنا لمشاهدة تفاصيل المرشح";
                    tooltip = " انقر هنا لمشاهدة تفاصيل الشغور";
                    title = string.Format("استأجرت <a href='CandidateEng.aspx?CandidateId={0}' title='" + heading + "'>{1}</a> <a href='vacancy.aspx?JobID={2}' title= " + tooltip + " >{3}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim(), JobID, JobTitle);
                    break;

                case 8://Cancel Schedule
                    heading = "انقر هنا لمشاهدة تفاصيل الجدول الزمني";
                    title = string.Format("ألغي <a href='InterviewSchedule.aspx?JobScheduleId={0}' title='" + heading + "'>interview</a> for <a href='Jobopening.aspx?JobID={1}'>{2}</a>", JobScheduleID, JobID, JobTitle);
                    break;

                case 9://Disassociation
                    heading = "انقر هنا لمشاهدة تفاصيل المرشح";
                    tooltip = " انقر هنا لمشاهدة تفاصيل الشغور";
                    title = string.Format("نأت <a href='CreateCandidate.aspx?CandidateId={0}' title='" + heading + "'>{1}</a> from <a href='vacancy.aspx?JobID={2}' title= " + tooltip + " >{3}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim(), JobID, JobTitle);
                    break;
                case 10://Send offer
                    heading ="انقر هنا لمشاهدة تفاصيل العرض";
                    tooltip = "انقر هنا لترى تفاصيل الوظيفة";
                    title = string.Format("العرض المرسلة <a href='OfferLetter.aspx?OfferID={0}' title='" + heading + "'>{1}</a> for <a href='vacancy.aspx?JobID={2}' title=" + tooltip + ">{3}</a>", OfferID, Convert.ToString(CandidateNameArb).Trim(), JobID, JobTitle);
                    break;
                case 11://Interview Process
                    heading = " انقر هنا لمشاهدة تفاصيل مقابلة";
                    tooltip = " انقر هنا لعرض الوظائف";
                    title = string.Format("مقابلة المجهزة لل <a href='InterviewProcess.aspx?CandidateID={0}' title='" + heading + "'>{1}</a> Job- <a href='vacancy.aspx?JobID={2}' title=" + tooltip + ">{3}</a>", CandidateId, Convert.ToString(CandidateNameArb).Trim(), JobID, JobTitle);
                    break;
                case 12://Approve vacancy
                    heading = "انقر هنا لترى تفاصيل الوظيفة";
                    title = string.Format("العمل المعتمدة <a href='vacancy.aspx?JobID={0}' title='" + heading + "'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;
                case 13://Closed vacancy
                    heading = "انقر هنا لترى تفاصيل الوظيفة";
                    title = string.Format("الوظيفة مغلقة <a href='vacancy.aspx?JobID={0}' title='" + heading + "'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;
            }
        }

        else
        {

            switch (Convert.ToInt32(RecentActivityTypeId))
            {
                case 1://Insert vacancy

                    title = string.Format("posted job opening <a href='vacancy.aspx?JobID={0}' title='Click here to view job details'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;

                case 2://Update vacancy

                    title = string.Format("Updated job opening <a href='vacancy.aspx?JobID={0}' title='Click here to view job details'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;

                case 3://Insert candidate

                    title = string.Format("Added candidate <a href='CandidateEng.aspx?CandidateId={0}' title='Click here to view candidate details'>{1}</a>", CandidateId, Convert.ToString(CandidateName).Trim());
                    break;

                case 4://Update candidate

                    title = string.Format("Updated candidate <a href='CandidateEng.aspx?CandidateId={0}' title='Click here to view candidate details'>{1}</a>", CandidateId, Convert.ToString(CandidateName).Trim());
                    break;

                case 5://Schedule

                    title = string.Format("Scheduled <a href='InterviewSchedule.aspx?JobScheduleId={0}' title='Click here to view schedule details'>interview</a> for <a href='vacancy.aspx?JobID={1}'>{2}</a>", JobScheduleID, JobID, JobTitle);
                    break;

                case 6://Association

                    title = string.Format("Associated <a href='CandidateEng.aspx?CandidateId={0}' title='Click here to view candidate details'>{1}</a> with <a href='vacancy.aspx?JobID={2}' title='Click here to view vacancy details'>{3}</a>", CandidateId, Convert.ToString(CandidateName).Trim(), JobID, JobTitle);
                    break;

                case 7://Hire

                    title = string.Format("Hired <a href='CandidateEng.aspx?CandidateId={0}' title='Click here to view candidate details'>{1}</a> <a href='vacancy.aspx?JobID={2}' title='Click here to view vacancy details'>{3}</a>", CandidateId, Convert.ToString(CandidateName).Trim(), JobID, JobTitle);
                    break;

                case 8://Cancel Schedule

                    title = string.Format("Cancelled <a href='InterviewSchedule.aspx?JobScheduleId={0}' title='Click here to view schedule details'>interview</a> for <a href='vacancy.aspx?JobID={1}'>{2}</a>", JobScheduleID, JobID, JobTitle);
                    break;

                case 9://Disassociation

                    title = string.Format("Disassociated <a href='CreateCandidate.aspx?CandidateId={0}' title='Click here to view candidate details'>{1}</a> from <a href='vacancy.aspx?JobID={2}' title='Click here to view vacancy details'>{3}</a>", CandidateId, Convert.ToString(CandidateName).Trim(), JobID, JobTitle);
                    break;
                case 10://Send offer

                    title = string.Format("Offer Sent <a href='OfferLetter.aspx?OfferID={0}' title='Click here to view offer details'>{1}</a> for <a href='vacancy.aspx?JobID={2}' title='Click here to view job details'>{3}</a>", OfferID, Convert.ToString(CandidateName).Trim(), JobID, JobTitle);
                    break;
                case 11://Interview Process

                    title = string.Format("interview processed for <a href='InterviewProcess.aspx?CandidateID={0}' title='Click here to view Interview details'>{1}</a> Job- <a href='vacancy.aspx?JobID={2}' title='Click here to view job '>{3}</a>", CandidateId, Convert.ToString(CandidateName).Trim(), JobID, JobTitle);
                    break;
                case 12://Approve vacancy
                    title = string.Format("Approved job <a href='vacancy.aspx?JobID={0}' title='Click here to view job details'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;
                case 13://Closed vacancy

                    title = string.Format("Closed job <a href='vacancy.aspx?JobID={0}' title='Click here to view job details'>{1}</a>", JobID, JobTitle, RecentActivityTime);
                    break;
            }
        }

        return title;
    }


    protected void dlRecentActivity_ItemCommand(object source, DataListCommandEventArgs e)
    {

        switch (e.CommandName)
        {
            case "_ViewCandidate":
                //dvMain.Style["display"] = "block";
                //dvViewOffers.Style["display"] = "none";

                //CandidateHistory.GetHistory(null, dlNewHires.DataKeys[e.Item.ItemIndex].ToInt32());
                //updOffer.Update();
                break;

        }
    }
    protected void dlRecentActivity_PreRender(object sender, EventArgs e)
    {
        objHomePage = new clsHomePage();

        if (ViewState["BuddyId"] != null)
            //lblRecentActivities.Text = "Recent Activities of " + objHomePage.GetFullName(Convert.ToInt32(ViewState["BuddyId"]));
            if (dlRecentActivity.Items.Count == 0)
            {
                dlRecentActivity.ShowHeader = pgrRecentActivity.Visible = false;
            }
            else
            {
                dlRecentActivity.ShowHeader = pgrRecentActivity.Visible = true;
            }
        if (dlRecentActivity.DataSource == null)
            tblRecentActivityHeader.Attributes.Add("style", "display:none");
        else
            tblRecentActivityHeader.Attributes.Add("style", "display:block");

        tblRecentActivityHeader.Visible = (!(dlRecentActivity.DataSource == null));
    }

   

    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {

        TableCell cell = (TableCell)e.Cell;
        cell.Width = Unit.Pixel(30);
        cell.Height = Unit.Pixel(30);
        if (e.Day.Date.ToString("dd MMM yyyy") == System.DateTime.Now.Date.ToString("dd MMM yyyy"))
        {
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Red;
            //cell.CssClass = "Currendate";
        }
        //cell.BorderStyle = BorderStyle.Solid;
        //cell.BorderWidth = Unit.Pixel(1);
        //cell.BorderColor = System.Drawing.Color.Black;
        cell.Style["font-size"] = "10px";
        //cell.Style["padding"] = "0";
        cell.Attributes.Add("title", "");
        if (e.Day.IsOtherMonth)
        {
            cell.Text = "";
            cell.CssClass = "tblBackground1";

        }
        else
        {

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();

            DataSet ds = objHomePage.GetCalendarDetails(objUserMaster.GetEmployeeId(), e.Day.Date.ToString("dd MMM yyyy"));
            cell.CssClass = "tblBackground1";
            string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : " Holiday:");
            if (ds.Tables[0].Rows.Count > 0)
            {
                sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                cell.ForeColor = System.Drawing.Color.Red;
            }
            string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : " Events: \n");
            int icount = 1;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                icount++;
            }
            if (sEvent != string.Empty)
                cell.ForeColor = System.Drawing.Color.Green;
            if (sEvent != string.Empty && sHoliday != string.Empty)
                cell.ForeColor = System.Drawing.Color.FromArgb(244, 166, 29);
            //cell.ForeColor = System.Drawing.Color.Magenta;
            string sTitle = (sHoliday == string.Empty ? string.Empty : sHoliday) + (sEvent == string.Empty ? string.Empty : sEvent);
            if (sTitle != string.Empty)
                cell.Attributes.Add("title", sTitle);

        }
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        objUserMaster = new clsUserMaster();
        BindRecentActivity();
        rptAnnouncements.DataSource = objHomePage.GetAnnouncements(objUserMaster.GetEmployeeId(), ddlCompany.SelectedValue.ToInt32());
        rptAnnouncements.DataBind();

        upRecentActivity.Update();
       
    }
}
