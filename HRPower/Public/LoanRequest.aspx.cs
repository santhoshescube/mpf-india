﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Reflection;

public partial class Public_LoanRequest : System.Web.UI.Page
{
    clsLoanRequest objRequest;
    clsUserMaster objUser;
    clsLeaveRequest objLeaveRequest;
    clsEmployee objEmployee;
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        LoanRequestPager.Fill += new controls_Pager.FillPager(BindDataList);
       
        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;    
            if (Request.QueryString["Requestid"] != null && Request.QueryString["Type"] == "Cancel")
            {
                int iRequestId = 0;
                ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                Session.Remove("Approve");

                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                TextBox txtsingleReason = (TextBox)fvLoanDetails.FindControl("txtsingleReason");
                Label lblSingleReason = (Label)fvLoanDetails.FindControl("lblSingleReason");
               
                txtsingleReason.Visible = true;
                lblSingleReason.Visible = false;               

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkAdd.OnClientClick = "return false;";
                lnkView.Enabled = false;
                lnkView.OnClientClick = "return false;";
            }
            else if (Request.QueryString["Requestid"] != null)
            {
                int iRequestId = 0;
                ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                {                   
                    
                    Session["Approve"] = true;
                   
                }
                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                if ((Request.QueryString["Requestid"]) != null)
                {
                    TextBox txtRptToAmount = (TextBox)fvLoanDetails.FindControl("txtRptToAmount");
                    Label lblAmount = (Label)fvLoanDetails.FindControl("lblAmount");
                    if (txtRptToAmount != null)
                    {
                        txtRptToAmount.Visible = true;
                        lblAmount.Visible = false;                       
                    }

                    else txtRptToAmount.Visible = false;
                    upForm.Update();
                }
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkAdd.OnClientClick = "return false;";
                lnkView.Enabled = false;
                lnkView.OnClientClick = "return false;";
            }
            else
            {
                Session.Remove("Approve");
                BindDataList();
                TextBox txtRptToAmount = (TextBox)fvLoanDetails.FindControl("txtRptToAmount");
                if (txtRptToAmount != null)
                    txtRptToAmount.Visible = false;
                upForm.Update();   
            }
        }
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    protected void cvValidateAssociates_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CheckBoxList cblEmployees = (CheckBoxList)fvLoanDetails.FindControl("cblEmployees");
        bool isSelected = false;
        foreach (ListItem item in cblEmployees.Items)
        {
            if (item.Selected)
            {
                isSelected = true;
                break;
            }
        }

        if (!isSelected)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }

    public void FillComboLoanTypes()
    {
        DropDownList rcLoanType = (DropDownList)fvLoanDetails.FindControl("rcLoanType");
        if (rcLoanType != null)
        {
            using (DataTable dt = new clsLoanRequest().FillLoanType())
            {
                string str;
                str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                rcLoanType.DataSource = dt;
                rcLoanType.DataTextField = "Description";
                rcLoanType.DataValueField = "LoanTypeId";
                rcLoanType.DataBind();
                rcLoanType.Items.Insert(0, new ListItem(str, "-1"));
                rcLoanType.SelectedValue = CurrentSelectedValue;
            }
        }
    }

    private void SendMessage(int StatusID, int RequestID)
    {
        switch (StatusID)
        {
            case (int)RequestStatus.Approved:
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Approval, eAction.Approved, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.Approved);
                break;
            case (int)RequestStatus.Rejected:
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Rejection, eAction.Reject, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.Reject);
                break;
            case (int)RequestStatus.Applied:
                clsCommonMessage.SendMessage(new clsUserMaster().GetEmployeeId(), RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Request, eAction.Applied, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.Applied);
                break;
            case (int)RequestStatus.RequestForCancel:
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Cancel_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.RequestForCancel);
                break;
            case (int)RequestStatus.Cancelled:
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Cancellation, eAction.Cancelled, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.Cancelled);
                break;


            case (int)RequestStatus.Pending:
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.LoanRequest, eMessageTypes.Loan_Cancellation, eAction.Pending, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.Loan, eAction.Pending);






                break;

        }

    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Session["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        fvLoanDetails.ChangeMode(FormViewMode.Insert);

        dlLoanRequest.DataSource = null;
        dlLoanRequest.DataBind();
        LoanRequestPager.Visible = false;

        lnkDelete.Enabled = false;

        lnkDelete.OnClientClick = "return false;";

        upForm.Update();
        upDetails.Update();

    }

    #region GetNewTable
    /// <summary>
    /// Get a new table with the fields same as grid
    /// </summary>
    /// <returns></returns>
    private DataTable GetNewTable()
    {
        DataTable datLoanDetails = new DataTable();
        datLoanDetails.Columns.Add("InstallmentNo", typeof(string));
        datLoanDetails.Columns.Add("InstallmentDate", typeof(string));
        datLoanDetails.Columns.Add("Amount", typeof(decimal));
        datLoanDetails.Columns.Add("InterestAmount", typeof(decimal));

        return datLoanDetails;
    }
    #endregion GetNewTable

    public void GetInstallments()
    {
        DataList dlInstallments = (DataList)fvLoanDetails.FindControl("dlInstallments");
        Label lblStartDate = (Label)fvLoanDetails.FindControl("lblStartDate");
        Label lblEndDate = (Label)fvLoanDetails.FindControl("lblEndDate");
        TextBox txtNoOfInstallments = (TextBox)fvLoanDetails.FindControl("txtNoOfInstallments");
        TextBox txtLoanAmount = (TextBox)fvLoanDetails.FindControl("txtLoanAmount");
        TextBox txtInterestRate = (TextBox)fvLoanDetails.FindControl("txtInterestRate");
        TextBox txtPaymentAmount = (TextBox)fvLoanDetails.FindControl("txtPaymentAmount");
        DropDownList ddlInterestType = (DropDownList)fvLoanDetails.FindControl("ddlInterestType");
        TextBox txtDate = (TextBox)dlInstallments.FindControl("txtDate");
        TextBox txtAmount = (TextBox)dlInstallments.FindControl("txtAmount");
        Label lblInstallmentNumber = (Label)dlInstallments.FindControl("lblInstallmentNumber");
        HiddenField hdIntetrestAmount = (HiddenField)dlInstallments.FindControl("hdIntetrestAmount");

        DataTable datLoanDetails = GetNewTable();
        int iCounter = 0;
        DateTime dtInstallmentDate, dtStartDate = clsCommon.Convert2DateTime(lblStartDate.Text);
        int intInterestType = ddlInterestType.SelectedValue.ToInt32();
        decimal decInstallmentAmount = 0, decInterestAmt = 0;
        string strInstallmentNo = string.Empty;

        for (iCounter = 0; iCounter < txtNoOfInstallments.Text.ToInt32(); iCounter++)
        {
            strInstallmentNo = clsUserMaster.GetCulture() == "ar-AE" ? ("أقساط") : ("Installments") + (iCounter + 1).ToStringCustom();

            if (iCounter == 0)
                dtInstallmentDate = dtStartDate;
            else
                dtInstallmentDate = Microsoft.VisualBasic.DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Month, iCounter, dtStartDate);

            if (decInstallmentAmount == 0)
                decInstallmentAmount = CalculateInstalmentAmount(intInterestType, txtInterestRate.Text.ToDecimal(), txtLoanAmount.Text.ToDecimal(), txtNoOfInstallments.Text.ToInt32());


            if (ddlInterestType.SelectedValue.ToInt32() != 1)
            {
                if (txtNoOfInstallments.Text.ToDecimal() > 0)
                {
                    decInterestAmt = (txtPaymentAmount.Text.ToDecimal() - txtLoanAmount.Text.ToDecimal()) / txtNoOfInstallments.Text.ToDecimal();
                }

                if (decInterestAmt < 0)
                    decInterestAmt = 0;
            }

            if (iCounter == txtNoOfInstallments.Text.ToInt32() - 1)
                lblEndDate.Text = dtInstallmentDate.ToString("dd MMM yyyy");
            DataRow drRow = datLoanDetails.NewRow();
            drRow["InstallmentNo"] = strInstallmentNo;
            drRow["InstallmentDate"] = dtInstallmentDate.ToString("dd/MM/yyyy");
            drRow["Amount"] = decInstallmentAmount.ToDecimal().ToString("0.00");
            drRow["InterestAmount"] = decInterestAmt;
            datLoanDetails.Rows.Add(drRow);
        }
        dlInstallments.DataSource = datLoanDetails;
        dlInstallments.DataBind();

    }

    #region CalculateInstalmentAmount
    /// <summary>
    /// Calacualte installment amount
    /// </summary>
    /// <param name="intInterestType">whether simple or none</param>
    /// <returns></returns>
    private decimal CalculateInstalmentAmount(int intInterestType, decimal decRate, decimal decLoanAmount, decimal decInstallmentNo)
    {
        decimal decInstallmentAmount = 0;
        try
        {
            TextBox txtPaymentAmount = (TextBox)fvLoanDetails.FindControl("txtPaymentAmount");
            decimal decSimpleInterest = 0;
            decimal decDenomination = 0;
            decDenomination = 12;

            if (intInterestType == 1)
            {
                decInstallmentAmount = decLoanAmount / decInstallmentNo;
                txtPaymentAmount.Text = decLoanAmount.ToString("0.00");
            }
            else if (intInterestType == 2)
            {
                decSimpleInterest = (decLoanAmount * (decRate / 100));
                decInstallmentAmount = (decLoanAmount + decSimpleInterest) / decInstallmentNo;
                txtPaymentAmount.Text = (decLoanAmount + decSimpleInterest).ToString("0.00");
            }

        }
        catch
        {
            decInstallmentAmount = 0;
        }

        // MdecInstallmentAmt = decInstallmentAmount;

        return decInstallmentAmount;
    }
    #endregion CalculateInstalmentAmount


    protected void fvLoanDetails_DataBound(object sender, EventArgs e)
    {
        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();

        HiddenField hfLoanDate = (HiddenField)fvLoanDetails.FindControl("hfLoanDate");

        if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(Session["Approve"]) == false)
        {
            HtmlTableRow trForwardedBy = (HtmlTableRow)fvLoanDetails.FindControl("trForwardedBy");
            if (trForwardedBy != null)
                trForwardedBy.Visible = false;
        }

        if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly)
        {
            Label lbViewCurrency = (Label)fvLoanDetails.FindControl("lblViewCurrency");
            HiddenField hdEmployeeID = (HiddenField)fvLoanDetails.FindControl("hdEmployeeID");
            //ImageButton imgBack = (ImageButton)fvLoanDetails.FindControl("imgBack");
            //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");

            objRequest = new clsLoanRequest();
            int iRequestId;
            TextBox txtRptToAmount = (TextBox)fvLoanDetails.FindControl("txtRptToAmount");
            Label lblAmount = (Label)fvLoanDetails.FindControl("lblAmount");
            if (Request.QueryString["RequestId"] != null)
            {                
                if (txtRptToAmount != null)
                {
                    txtRptToAmount.Visible = false;
                    lblAmount.Visible = true;
                    upForm.Update();
                }
               
                //if (imgBack != null)
                //    imgBack.Visible = true;
                iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                objRequest.RequestId = iRequestId;              
            }
            else
            {
                if (txtRptToAmount != null)
                {
                    txtRptToAmount.Visible = false;
                    lblAmount.Visible = true;
                    upForm.Update();
                }
                //if (imgBack != null)
                //    imgBack.Visible = false;
            } HtmlTableRow ltReasontd = (HtmlTableRow)fvLoanDetails.FindControl("ltReasontd");
            if (ibtnBack.Visible && ltReasontd != null)
            {
               // imgBack2.Visible = false;
                //ltReasontd.Style["display"] = "table-row";
                ltReasontd.Style["display"] = "none";
            }

            if (hdEmployeeID != null)
            {
                if (hdEmployeeID.Value.ToInt32() > 0)
                {
                    objRequest.EmployeeId = hdEmployeeID.Value.ToInt32();
                    if (lbViewCurrency != null)
                        lbViewCurrency.Text = objRequest.GetCurrency();
                }
            }
            else if (objUser.GetEmployeeId() > 0)
            {
                objRequest.EmployeeId = objUser.GetEmployeeId();
                if (lbViewCurrency != null)
                    lbViewCurrency.Text = objRequest.GetCurrency();
            }
            else
            {
                if (lbViewCurrency != null)
                    lbViewCurrency.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("درهم") : ("AED");
            }


        }
        DropDownList ddlEditStatus = (DropDownList)fvLoanDetails.FindControl("ddlEditStatus");
        TextBox txtEditReason = (TextBox)fvLoanDetails.FindControl("txtEditReason");
        //Label lblReason = (Label)fvLoanDetails.FindControl("lblReason");

        if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(Session["Approve"]) == true)
        {

            HiddenField hdIsHigher = (HiddenField)fvLoanDetails.FindControl("hdIsHigher");
            Button btnApprove = (Button)fvLoanDetails.FindControl("btnApprove");
            Label lbViewCurrency = (Label)fvLoanDetails.FindControl("lblViewCurrency");

            if (clsLeaveRequest.IsHigherAuthority((int)RequestType.Loan, objUser.GetEmployeeId(), fvLoanDetails.DataKey[1].ToInt32()))
            {
                HtmlGenericControl divHigher = (HtmlGenericControl)fvLoanDetails.FindControl("divHigher");
                HtmlGenericControl divReportingTo = (HtmlGenericControl)fvLoanDetails.FindControl("divReportingTo");
                TextBox txtLoanDate = (TextBox)fvLoanDetails.FindControl("txtLoanDate");
                TextBox txtNoOfInstallments = (TextBox)fvLoanDetails.FindControl("txtNoOfInstallments");
                HiddenField hdStartDate = (HiddenField)fvLoanDetails.FindControl("hdStartDate");
                Label lblLoanFromDate = (Label)fvLoanDetails.FindControl("lblLoanFromDate");
                //RequiredFieldValidator rfvRate = (RequiredFieldValidator)fvLoanDetails.FindControl("rfvRate");
                //RangeValidator rvRate = (RangeValidator)fvLoanDetails.FindControl("rvRate");
                TextBox txtInterestRate = (TextBox)fvLoanDetails.FindControl("txtInterestRate");
                DropDownList ddlInterestType = (DropDownList)fvLoanDetails.FindControl("ddlInterestType");
                Label lblBasicPay = (Label)fvLoanDetails.FindControl("lblBasicPay");
                HiddenField hdEmployeeID = (HiddenField)fvLoanDetails.FindControl("hdEmployeeID");
                TextBox txtLoanNumber = (TextBox)fvLoanDetails.FindControl("txtLoanNumber");



                if (lblLoanFromDate != null)
                {
                    txtLoanDate.Text = hdStartDate.Value = clsCommon.Convert2DateTime(lblLoanFromDate.Text).ToString("dd/MM/yyyy");
                }
                txtNoOfInstallments.Text = "1";
                hdIsHigher.Value = "1";
                if (divHigher != null && divReportingTo != null)
                {
                    divHigher.Style["display"] = "table-row";
                    divReportingTo.Style["display"] = "none";
                    DataTable dtLoanNo = objRequest.GetLoanNumber();
                    if (dtLoanNo.Rows.Count > 0)
                    {
                        txtLoanNumber.Text = dtLoanNo.Rows[0]["Prefix"].ToString();
                        //txtLoanNumber.Enabled = !Convert.ToBoolean(dtLoanNo.Rows[0]["IsAuto"]);
                    }
                }
                if (ddlEditStatus != null)
                {
                    ddlEditStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
                    ddlEditStatus.DataBind();
                    ddlEditStatus.DataBind();
                }
                if (lblBasicPay != null)
                {
                    lblBasicPay.Text = objRequest.GetBasicPay(hdEmployeeID.Value.ToInt32()).ToString("0.00");
                }
                objRequest.StatusId = Convert.ToInt32(ddlEditStatus.SelectedValue);
                GetInstallments();
                btnApprove.ValidationGroup = "FinalApproval";
                //rvRate.ValidationGroup = "";
                //rfvRate.ValidationGroup = "";
                txtInterestRate.Enabled = false;
                txtInterestRate.Text = "0";





                HiddenField hdEmployeeID1 = (HiddenField)fvLoanDetails.FindControl("hdEmployeeID");
                LinkButton lnkLoanDetails = ((LinkButton)fvLoanDetails.FindControl("lnkLoanDetails"));
                HtmlGenericControl dvPreviousLoan = (HtmlGenericControl)fvLoanDetails.FindControl("dvPreviousLoan");
                Label lblNoLoan = (Label)fvLoanDetails.FindControl("lblNoLoan");

                if (lnkLoanDetails != null && dvPreviousLoan != null)
                {
                    lnkLoanDetails.Attributes.Add("OnClick", "return ShowPreviousLoan(" + dvPreviousLoan.ClientID + ");");
                }

                GridView dgv = ((GridView)fvLoanDetails.FindControl("dgvPreviousLoan"));

                AjaxControlToolkit.ModalPopupExtender mpe = ((AjaxControlToolkit.ModalPopupExtender)fvLoanDetails.FindControl("mpe"));
                UpdatePanel updPreviousLoan = ((UpdatePanel)fvLoanDetails.FindControl("updPreviousLoan"));

                if (dgv != null && hdEmployeeID1 != null)
                {
                    DataTable dt = clsLoanRequest.GetEmployeePreviousLoanDetails(hdEmployeeID1.Value.ToInt32());
                    if (dt.Rows.Count == 0)
                    {
                        dgv.Visible = false;
                        lblNoLoan.Visible = true;

                    }
                    else
                    {
                        lblNoLoan.Visible = false;
                        dgv.Visible = true;
                        dgv.DataSource = dt;
                        dgv.DataBind();
                    }
                }

                if (mpe != null && updPreviousLoan != null)
                {
                    updPreviousLoan.Update();
                    mpe.Show();
                }





            }
            else
            {
                HtmlGenericControl divReportingTo = (HtmlGenericControl)fvLoanDetails.FindControl("divReportingTo");

                hdIsHigher.Value = "0";
                if (divReportingTo != null)
                    divReportingTo.Style["display"] = "table-row";
                if (ddlEditStatus != null)
                {
                    DataTable dt = new clsLeaveRequest().GetAllRequestStatus(1);
                    dt.DefaultView.RowFilter = "StatusID<>4";
                    ddlEditStatus.DataSource = dt.DefaultView.ToTable();
                    ddlEditStatus.DataBind();
                }
                objRequest.StatusId = Convert.ToInt32(ddlEditStatus.SelectedValue);
                btnApprove.ValidationGroup = "";
            }



            Label lblStatus = (Label)fvLoanDetails.FindControl("lblStatus");
            Button btCancel = (Button)fvLoanDetails.FindControl("btCancel");

            HtmlTableRow trForwardedBy = (HtmlTableRow)fvLoanDetails.FindControl("trForwardedBy");

            if (Convert.ToBoolean(Session["Approve"]) == true)
            {
                lblStatus.Visible = false;
                ddlEditStatus.Visible = true;
                txtEditReason.Visible = true;
                //lblReason.Visible = false;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                btnApprove.Visible = true;
                btCancel.Visible = true;

                trForwardedBy.Attributes.Add("style", "display:table-row");
            }
            else
            {
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                txtEditReason.Visible = false;
                //lblReason.Visible = true;
                btnApprove.Visible = false;
                btCancel.Visible = false;

                trForwardedBy.Attributes.Add("style", "display:none;");
            }
            //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");
            HtmlTableRow ltReasontd = (HtmlTableRow)fvLoanDetails.FindControl("ltReasontd");
            if (ibtnBack.Visible)
            {
                //imgBack2.Visible = false;//true;
                ltReasontd.Style["display"] = "table-row";
            }
        }
        //cancel request
        else if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestCancel"]) == true)
        {

            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvLoanDetails.FindControl("lblStatus");
            Button btnCancel = (Button)fvLoanDetails.FindControl("btnCancel");

            if (Convert.ToBoolean(ViewState["RequestCancel"]) == true)
            {
                lblStatus.Visible = false;
                ddlEditStatus.Visible = true;
                txtEditReason.Visible = true;
                //lblReason.Visible = false;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("7"));
                ddlEditStatus.Enabled = false;
                btnCancel.Visible = true;
                btnCancel.Text = "Submit";
            }
            else
            {
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                txtEditReason.Visible = false;
                //lblReason.Visible = true;
                btnCancel.Visible = false;
                // btCancel.Visible = false;
            }
            //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");
            //ImageButton imgBack = (ImageButton)fvLoanDetails.FindControl("imgBack");
            HtmlTableRow ltReasontd = (HtmlTableRow)fvLoanDetails.FindControl("ltReasontd");
            if (ibtnBack.Visible && ltReasontd != null)
            {
              //  imgBack2.Visible = true;
                ltReasontd.Style["display"] = "table-row";
            }
           
            //if (imgBack != null)
            //    imgBack.Visible = false;
            
        }
        else if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {

            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvLoanDetails.FindControl("lblStatus");
            Button btnRequestForCancel = (Button)fvLoanDetails.FindControl("btnRequestForCancel");



            if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
            {
                lblStatus.Visible = false;
                ddlEditStatus.Visible = true;
                txtEditReason.Visible = true;
                //lblReason.Visible = false;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
                ddlEditStatus.Enabled = false;
                btnRequestForCancel.Visible = true;


            }
            else
            {
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                txtEditReason.Visible = false;
                //lblReason.Visible = true;
                btnRequestForCancel.Visible = false;
            }
            //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");
            //ImageButton imgBack = (ImageButton)fvLoanDetails.FindControl("imgBack");
            HtmlTableRow ltReasontd = (HtmlTableRow)fvLoanDetails.FindControl("ltReasontd");
            //if (ibtnBack.Visible  && ltReasontd != null)
            //{
            //   // imgBack2.Visible = false;
            //    ltReasontd.Style["display"] = "none";
            //}


            if (ibtnBack.Visible)
            {
               // imgBack.Visible = true;
                ltReasontd.Style["display"] = "table-row";
            }
        }


        DropDownList rcLoanType = (DropDownList)fvLoanDetails.FindControl("rcLoanType");
        DropDownList ddlStatus = (DropDownList)fvLoanDetails.FindControl("ddlStatus");

        //if(rcLoanType!= null)
        //    rcLoanType.FillList();

        FillComboLoanTypes();

        if (ddlStatus != null)
        {
            ddlStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlStatus.DataBind();
        }

        if (fvLoanDetails.CurrentMode == FormViewMode.Edit)
        {

            if (fvLoanDetails.DataKey["LoanTypeId"] != null)
            {
                rcLoanType.SelectedValue = fvLoanDetails.DataKey["LoanTypeId"].ToString();
            }
            if (fvLoanDetails.DataKey["StatusId"] != null)
            {
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvLoanDetails.DataKey["StatusId"])));
            }
            if (fvLoanDetails.DataKey["RequestedTo"] != null)
            {
                clsLeaveRequest request = new clsLeaveRequest();
                request.EmployeeId = objUser.GetEmployeeId();
            }
        }

        if (Convert.ToBoolean(Session["Approve"]) == false && (fvLoanDetails.CurrentMode == FormViewMode.Edit | fvLoanDetails.CurrentMode == FormViewMode.Insert))
        {
            Label lbCurrency = (Label)fvLoanDetails.FindControl("lblCurrency");
            objRequest = new clsLoanRequest();
            if (!objRequest.CheckSalaryStrucutreExistance(objUser.GetEmployeeId()))
            {
                HtmlGenericControl divInsert = (HtmlGenericControl)fvLoanDetails.FindControl("divInsert");
                HtmlGenericControl divError = (HtmlGenericControl)fvLoanDetails.FindControl("divError");
                divError.Style["display"] = "table-row";
                divInsert.Style["display"] = "none";
            }
            else
            {
                if (objUser.GetEmployeeId() > 0)
                {
                    objRequest.EmployeeId = objUser.GetEmployeeId();
                    lbCurrency.Text = objRequest.GetCurrency();
                }
                else
                {
                    lbCurrency.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("درهم") : ("AED");
                }
                if (ddlStatus != null)
                {
                    ddlStatus.Enabled = false;
                    ddlStatus.CssClass = "dropdownlist_disabled";
                }
            }
            //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");
            //ImageButton imgBack = (ImageButton)fvLoanDetails.FindControl("imgBack");
            //if (imgBack2 != null)
            //    imgBack2.Visible = false;
            //if (imgBack != null)
            //    imgBack2.Visible = false;
        }

        if (fvLoanDetails.CurrentMode == FormViewMode.Insert)
            hfLoanDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
    }

    private void CalculatePaymentAmount(int iRowIndex)
    {
        try
        {
            DataList dlInstallments = (DataList)fvLoanDetails.FindControl("dlInstallments");
            TextBox txtInterestRate = (TextBox)fvLoanDetails.FindControl("txtInterestRate");
            TextBox txtLoanAmount = (TextBox)fvLoanDetails.FindControl("txtLoanAmount");
            TextBox txtPaymentAmount = (TextBox)fvLoanDetails.FindControl("txtPaymentAmount");
            Label lblEndDate = (Label)fvLoanDetails.FindControl("lblEndDate");



            decimal decAmount = 0, decRemainingAmt, decNewInstallmentAmt = 0, decInterestAmount = 0, decRemainingInterest = 0, decNewInterest = 0;
            int iCount = (dlInstallments.Items.Count - iRowIndex - 1);
            int iCounter = 0;
            if (iCount <= 0)
                return;
            for (iCounter = 0; iCounter <= iRowIndex; iCounter++)
            {
                TextBox txtAmount = (TextBox)dlInstallments.Items[iCounter].FindControl("txtAmount");
                HiddenField hdIntetrestAmount = (HiddenField)dlInstallments.Items[iCounter].FindControl("hdIntetrestAmount");
                decAmount += txtAmount.Text.ToDecimal();
                if (txtAmount.Text.ToDecimal() < hdIntetrestAmount.Value.ToDecimal())
                {
                    hdIntetrestAmount.Value = txtAmount.Text;
                }
                decInterestAmount += hdIntetrestAmount.Value.ToDecimal();
            }
            decRemainingAmt = txtPaymentAmount.Text.ToDecimal() - decAmount;
            decRemainingInterest = (txtPaymentAmount.Text.ToDecimal() - txtLoanAmount.Text.ToDecimal()) - decInterestAmount;

            decNewInstallmentAmt = (decRemainingAmt / iCount);
            decNewInterest = decRemainingInterest / iCount;

            if (decNewInstallmentAmt > 0)
            {
                for (int i = iCounter; i < dlInstallments.Items.Count; i++)
                {
                    TextBox txtAmount = (TextBox)dlInstallments.Items[i].FindControl("txtAmount");
                    HiddenField hdIntetrestAmount = (HiddenField)dlInstallments.Items[i].FindControl("hdIntetrestAmount");
                    txtAmount.Text = decNewInstallmentAmt.ToString("0.00");
                    hdIntetrestAmount.Value = decNewInterest.ToString("0.00");
                }

            }
        }
        catch (Exception Ex1)
        {


        }

    }

    protected void ddlEditStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Crop the string if it exceeds the width of container 
    /// </summary>
    protected string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    protected string ArrangeString(string value, int length)
    {
        int intlen = value.Length;
        int intPosition = 0;
        int intNewPosition = -3;
        string strCurrentstring = "";
        string strInsertString = "";
        if (intlen <= length)
            return value;
        else
        {
            int intCount = intlen / length;
            for (intPosition = 0; intPosition <= intCount; intPosition++)
            {
                intNewPosition += length;
                if (intNewPosition < intlen)
                    strCurrentstring = value.Substring(intNewPosition - 1, 1);

                if (strCurrentstring == " ")
                    strInsertString = "<p>";
                else
                    strInsertString = "-<p>";
                if (intNewPosition < intlen)
                    value = value.Insert(intNewPosition, strInsertString);
            }
            return value;
        }
    }


    private void InsertLoanRequest(int iRequestId)
    {
        objRequest = new clsLoanRequest();
        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();
        objEmployee = new clsEmployee();
        int intEmployeeID = objUser.GetEmployeeId();
        bool bIsInsert = true;
        string msg;
        DropDownList rcLoanType = (DropDownList)fvLoanDetails.FindControl("rcLoanType");
        TextBox txtLoanFromDate = (TextBox)fvLoanDetails.FindControl("txtLoanDate");
        TextBox txtReason = (TextBox)fvLoanDetails.FindControl("txtReason");
        TextBox txtAmount = (TextBox)fvLoanDetails.FindControl("txtLoanAmount");
        Label lblCurrency = (Label)fvLoanDetails.FindControl("lblCurrency");

        string sRequestedTo = "";
        sRequestedTo = clsLeaveRequest.GetRequestedTo(iRequestId, intEmployeeID, (int)RequestType.Loan);
        if (sRequestedTo == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("طلبك لا يمكن معالجة  الحين . يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
            msgs.WarningMessage(msg);
            //msgs.WarningMessage("Your request cannot process this time. Please contact HR department.");
            mpeMessage.Show();
            return;
        }

        if (iRequestId == 0)
        {
            objRequest.Mode = "INS";
            bIsInsert = true;
        }
        else
        {
            objRequest.Mode = "UPD";
            objRequest.RequestId = iRequestId;
            bIsInsert = false;
        }

        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.LoanType = Convert.ToInt32(rcLoanType.SelectedValue);
        objRequest.LoanFromDate = clsCommon.Convert2DateTime(txtLoanFromDate.Text);
        objRequest.Reason = txtReason.Text;
        objRequest.RequestedDate = DateTime.Now;
        objRequest.RequestedTo = sRequestedTo;
        objRequest.StatusId = 1;
        objRequest.LoanFromDate = clsCommon.Convert2DateTime(txtLoanFromDate.Text);

        objRequest.Amount = txtAmount.Text.ToDouble();

        // Check for duplication
        if (objRequest.IsLoanApplied())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تفاصيل القرض المطلوب  موجودة") : ("Requested Loan details exists.");
            msgs.WarningMessage(msg);
            //msgs.InformationalMessage("Requested Loan details exists.");
            mpeMessage.Show();
            return;
        }
        //Check if salary processed
        if (objRequest.InsertValidation())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تمت معالجة الراتب .لا يمكن طلب قرض") : ("Salary already processed.Cannot request loan.");
            msgs.WarningMessage(msg);
            //msgs.InformationalMessage("Salary already processed.Cannot request loan.");
            mpeMessage.Show();
            return;
        }
        iRequestId = objRequest.InsertRequest();

        //--------------------------------------------setting parameters for Message insertion into HRMessages-----------------------------------------
        SendMessage((int)RequestStatus.Applied, iRequestId);
        //------------------------------------------------------------------------------------------------------------------------------------

        if (!bIsInsert)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث تفاصيل الطلب بنجاح") : ("Request details updated successfully.");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Request details updated successfully.");
        }
        else
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تمت اضافة تفاصيل الطلب بنجاح") : ("Request details added successfully.");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Request details added successfully.");
        }

        upForm.Update();
        upDetails.Update();
        BindDataList();
        fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
        fvLoanDetails.DataSource = null;
        fvLoanDetails.DataBind();
        mpeMessage.Show();
    }

    public bool LoanValidation(int EmployeeID)
    {
        DataList dlInstallments = (DataList)fvLoanDetails.FindControl("dlInstallments");
        TextBox txtLoanDate = (TextBox)fvLoanDetails.FindControl("txtLoanDate");
        DateTime dtJoiningDate = objRequest.GetDateOfJoining(EmployeeID);
        string msg;
        if (dtJoiningDate > clsCommon.Convert2DateTime(txtLoanDate.Text))
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("ينبغي ان يكون ' تاريخ القرض '  أكبر من أو يساوي  تاريخ الالتحاق ") : ("The 'Loan date' should be greater than or equal to the 'Joining date'.");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("The 'Loan date' should be greater than or equal to the 'Joining date'.");
            return false;
        }

        DataTable dtPrevInstallments = objRequest.GetPreviousInstallments(EmployeeID);
        int intPercentage = objRequest.GetLoanInstallmentPercentage();
        decimal MdecBasicPay = objRequest.GetBasicPay(EmployeeID);
        decimal decBasicPay = (MdecBasicPay * intPercentage) / 100;
        decimal decTotalAmount = 0;

        if (dtPrevInstallments.Rows.Count > 0)
        {
            int iMonth = 0;
            int iYear = 0;

            foreach (DataListItem dgvRow in dlInstallments.Items)
            {
                TextBox txtDate = (TextBox)dgvRow.FindControl("txtDate");
                TextBox txtAmount = (TextBox)dgvRow.FindControl("txtAmount");
                if (iMonth != clsCommon.Convert2DateTime(txtDate.Text).Month || iYear != clsCommon.Convert2DateTime(txtDate.Text).Year)
                {
                    iMonth = clsCommon.Convert2DateTime(txtDate.Text).Month;
                    iYear = clsCommon.Convert2DateTime(txtDate.Text).Year;
                    decimal decSum = dtPrevInstallments.Compute("SUM(Amount)", "Month=" + iMonth + " AND Year=" + iYear + "").ToDecimal();

                    var sum = dlInstallments.Items.Cast<DataListItem>()
                                      .Where(r => ((clsCommon.Convert2DateTime(((TextBox)r.FindControl("txtDate")).Text).Month) == iMonth) && (clsCommon.Convert2DateTime(((TextBox)r.FindControl("txtDate")).Text).Year) == iYear)
                                       .Sum(t => (((TextBox)t.FindControl("txtAmount")).Text).ToDecimal());
                    if ((sum.ToDecimal() + decSum) > decBasicPay)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى التحقق مع مبلغ القرض نسبة في التكوين") : ("Please check with Loan Amount Percentage in Config");
                        msgs.InformationalMessage(msg);
                        //msgs.InformationalMessage("Please check with Loan Amount Percentage in Config");
                        return false;
                    }
                }
                if (txtAmount != null)
                {
                    decTotalAmount += txtAmount.Text.ToDecimal();
                }

            }
        }
        else
        {
            int Month = 0;
            int Year = 0;

            foreach (DataListItem dgvRow in dlInstallments.Items)
            {
                TextBox txtDate = (TextBox)dgvRow.FindControl("txtDate");
                TextBox txtAmount = (TextBox)dgvRow.FindControl("txtAmount");
                if (Month != clsCommon.Convert2DateTime(txtDate.Text).Month || Year != clsCommon.Convert2DateTime(txtDate.Text).Year)
                {
                    Month = clsCommon.Convert2DateTime(txtDate.Text).Month;
                    Year = clsCommon.Convert2DateTime(txtDate.Text).Year;


                    var sum = dlInstallments.Items.Cast<DataListItem>()
                                      .Where(r => ((clsCommon.Convert2DateTime(((TextBox)r.FindControl("txtDate")).Text).Month) == Month) && (clsCommon.Convert2DateTime(((TextBox)r.FindControl("txtDate")).Text).Year) == Year)
                                       .Sum(t => (((TextBox)t.FindControl("txtAmount")).Text).ToDecimal());
                    if (sum.ToDecimal() > decBasicPay)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى التحقق مع مبلغ القرض نسبة في التكوين") : ("Please check with Loan Amount Percentage in Config");
                        msgs.InformationalMessage(msg);
                        //msgs.InformationalMessage("Please check with Loan Amount Percentage in Config");
                        return false;
                    }
                }
                if (txtAmount != null)
                {
                    decTotalAmount += txtAmount.Text.ToDecimal();
                }

            }
        }
        TextBox txtPaymentAmount = (TextBox)fvLoanDetails.FindControl("txtPaymentAmount");
        if (txtPaymentAmount != null && decTotalAmount != txtPaymentAmount.Text.ToDecimal())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("إجمالي مبلغ القسط و المبلغ المدفوع  يختلف من قبل") : ("Total Installment amount and Payment Amount is different by");

            msgs.InformationalMessage(msg + Math.Abs(decTotalAmount - txtPaymentAmount.Text.ToDecimal()).ToString("0.00"));
            return false;
        }
        return true;

    }

    /// <summary>
    /// bind datalist with loan requests.
    /// </summary>
    private void BindDataList()
    {

        objRequest = new clsLoanRequest();
        objUser = new clsUserMaster();
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.Mode = "VIEW";
        objRequest.PageIndex = LoanRequestPager.CurrentPage + 1;
        objRequest.PageSize = LoanRequestPager.PageSize;
        DataTable oTable = objRequest.GetRequests();
        if (oTable.DefaultView.Count > 0)
        {
            lnkView.Enabled = true;
            dlLoanRequest.DataSource = oTable;
            dlLoanRequest.DataBind();

            LoanRequestPager.Total = objRequest.GetRecordCount();
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLoanRequest.ClientID + "');";

            lnkDelete.Enabled = true;
            fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
            fvLoanDetails.DataSource = null;
            fvLoanDetails.DataBind();
            LoanRequestPager.Visible = true;

        }
        else
        {
            lnkView.Enabled = false;
            dlLoanRequest.DataSource = null;
            dlLoanRequest.DataBind();
            LoanRequestPager.Visible = false;

            lnkDelete.Enabled = false;

            fvLoanDetails.ChangeMode(FormViewMode.Insert);
        }
        ViewState["Cancel"] = false;
    }


    protected void fvLoanDetails_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        objRequest = new clsLoanRequest();
        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();
        objEmployee = new clsEmployee();

        int iRequestId = 0;
        string msg;
        DropDownList ddlStatus = (DropDownList)fvLoanDetails.FindControl("ddlEditStatus");
        TextBox txtEditReason = (TextBox)fvLoanDetails.FindControl("txtEditReason");
        //Label lblReason = (Label)fvLoanDetails.FindControl("lblReason");
        TextBox txtsingleReason = (TextBox)fvLoanDetails.FindControl("txtsingleReason");
        Label lblSingleReason = (Label)fvLoanDetails.FindControl("lblSingleReason");

        Label lblLoanType = (Label)fvLoanDetails.FindControl("lblLoanType");
        Label lblLoanFromDate = (Label)fvLoanDetails.FindControl("lblLoanFromDate");
        Label lblViewCurrency = (Label)fvLoanDetails.FindControl("lblViewCurrency");
        Label lblAmount = (Label)fvLoanDetails.FindControl("lblAmount");
        HtmlTableRow ltReasontd = (HtmlTableRow)fvLoanDetails.FindControl("ltReasontd");
        DataTable dtRequestDetail;
        string sRequestedIds = string.Empty;
        string sToEmployees = string.Empty;
        int iRequestedById;
        string[] RequestedEmployes;
        string sRequestedTo = string.Empty;
        string[] SuperiorId;

        switch (e.CommandName)
        {
            case "Add":

                decimal dLoanAmt = 0.00M;
                TextBox txtLoanAmount = (TextBox)fvLoanDetails.FindControl("txtLoanAmount");
                RequiredFieldValidator rfvLoanAmount = (RequiredFieldValidator)fvLoanDetails.FindControl("rfvLoanAmount");
                Label lbl = (Label)fvLoanDetails.FindControl("lbl");
                if (Convert.ToString(e.CommandArgument) != "")
                {
                    iRequestId = Convert.ToInt32(e.CommandArgument);
                }
                if (!decimal.TryParse(txtLoanAmount.Text.Trim() == string.Empty ? "0" : txtLoanAmount.Text.Trim(), out dLoanAmt))
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("ينبغي أن يكون مبلغ القرض صالح") : ("Loan amount should be valid.");
                    lbl.Text = msg;//"Loan amount should be valid.";
                    lbl.Visible = true;

                }
                else if (dLoanAmt == 0)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("ينبغي أن يكون مبلغ القرض صالح") : ("Loan amount should be valid.");
                    lbl.Text = msg;//"Loan amount should be valid.";
                    //lbl.Text = "Loan amount should be valid.";
                    lbl.Visible = true;

                }
                else
                    InsertLoanRequest(iRequestId);

                break;

            case "CancelRequest":
                upForm.Update();
                upDetails.Update();
                //objRequest.Reason = txtEditReason.Text;
                BindDataList();
                this.Response.Redirect("LoanRequest.aspx", true);
                break;

            case "Approve":

                iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);

                objRequest.RequestId = iRequestId;
                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                objRequest.EmployeeId = objUser.GetEmployeeId();
                HiddenField hdEmployeeID = (HiddenField)fvLoanDetails.FindControl("hdEmployeeID");
                HiddenField hdLoanType = (HiddenField)fvLoanDetails.FindControl("hdLoanType");
                TextBox txtLoanDate = (TextBox)fvLoanDetails.FindControl("txtLoanDate");
                //higher authority
                if (clsLeaveRequest.IsHigherAuthority((int)RequestType.Loan, objUser.GetEmployeeId(), objRequest.RequestId))
                {

                    if (ddlStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved)
                    {
                        
                        TextBox txtLoanNumber = (TextBox)fvLoanDetails.FindControl("txtLoanNumber");
                        TextBox txtNoOfInstallments = (TextBox)fvLoanDetails.FindControl("txtNoOfInstallments");
                        HiddenField hdStartDate = (HiddenField)fvLoanDetails.FindControl("hdStartDate");
                        //HiddenField hdEmployeeID = (HiddenField)fvLoanDetails.FindControl("hdEmployeeID");
                        //HiddenField hdLoanType = (HiddenField)fvLoanDetails.FindControl("hdLoanType");
                        TextBox txtInterestRate = (TextBox)fvLoanDetails.FindControl("txtInterestRate");
                        DataList dlInstallments = (DataList)fvLoanDetails.FindControl("dlInstallments");
                        DropDownList ddlInterestType = (DropDownList)fvLoanDetails.FindControl("ddlInterestType");
                        TextBox txtAmount = (TextBox)fvLoanDetails.FindControl("txtLoanAmount");
                        TextBox txtPaymentAmount = (TextBox)fvLoanDetails.FindControl("txtPaymentAmount");
                        Label lblEndDate = (Label)fvLoanDetails.FindControl("lblEndDate");
                        if (objRequest.IsLoanNumberExists(txtLoanNumber.Text, hdEmployeeID.Value.ToInt32()))
                        {
                            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("عدد القروض موجود") : ("Loan number exists");
                            msgs.InformationalMessage(msg);
                            mpeMessage.Show();
                            return;
                        }

                        if (!LoanValidation(hdEmployeeID.Value.ToInt32()))
                        {
                            mpeMessage.Show();
                            return;
                        }


                        DataTable dt = GetNewTable();
                        int i;
                        for (i = 0; i < dlInstallments.Items.Count; i++)
                        {
                            TextBox txtDate = (TextBox)dlInstallments.Items[i].FindControl("txtDate");
                            TextBox txtInstallmentAmount = (TextBox)dlInstallments.Items[i].FindControl("txtAmount");
                            HiddenField hdIntetrestAmount = (HiddenField)dlInstallments.Items[i].FindControl("hdIntetrestAmount");
                            DataRow drRow = dt.NewRow();

                            drRow["InstallmentNo"] = i + 1;
                            drRow["InstallmentDate"] = clsCommon.Convert2DateTime(txtDate.Text);
                            drRow["Amount"] = txtInstallmentAmount.Text.Trim().ToDecimal();
                            drRow["InterestAmount"] = hdIntetrestAmount.Value.ToDecimal();
                            dt.Rows.Add(drRow);
                            if (i == dlInstallments.Items.Count - 1)
                                objRequest.EndDate = clsCommon.Convert2DateTime(txtDate.Text);
                        }
                        objRequest.Installments = dt;
                        objRequest.NoOfInstallments = txtNoOfInstallments.Text.ToInt32();
                        objRequest.LoanNumber = txtLoanNumber.Text.Trim();
                        objRequest.LoanDate = clsCommon.Convert2DateTime(txtLoanDate.Text);
                        //objRequest.LoanType = hdLoanType.Value.ToInt32();
                        objRequest.StartDate = clsCommon.Convert2DateTime(hdStartDate.Value);
                        objRequest.InterestRate = txtInterestRate.Text.ToDecimal();
                        objRequest.InterestType = ddlInterestType.SelectedValue.ToInt32();
                        objRequest.Amount = txtAmount.Text.ToDouble();
                        objRequest.PaymentAmount = txtPaymentAmount.Text.ToDecimal();
                        //objRequest.RequestedBy = hdEmployeeID.Value.ToInt32();
                        divPrint.Style["display"] = "block";
                    }
                    objRequest.LoanType = hdLoanType.Value.ToInt32();
                    objRequest.RequestedBy = hdEmployeeID.Value.ToInt32();
                    objRequest.Reason = txtEditReason.Text.Trim();
                    objRequest.UpdateLoanStatus(true);
                    Session["Approve"] = false;
                    objRequest.LoanDate = clsCommon.Convert2DateTime(txtLoanDate.Text);
                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                    {
                        new clsExportToCSV().ExportToCSV(objRequest.LoanDate);
                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                        //-----------------------------------------------------------------------------------------------------------

                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث طلب القرض بنجاح") : ("Loan Request updated successfully.");
                        msgs.InformationalMessage(msg);
                        //msgs.InformationalMessage("Loan Request updated successfully.");
                    }
                    else
                    {
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                        //msgs.InformationalMessage("Loan Request updated successfully.");
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث طلب القرض بنجاح") : ("Loan Request updated successfully.");
                        msgs.InformationalMessage(msg);
                    }

                }
                else
                {
                    TextBox txtRptToAmount = (TextBox)fvLoanDetails.FindControl("txtRptToAmount");
                    divPrint.Style["display"] = "none";
                    objRequest.Reason = txtEditReason.Text;
                    objRequest.Amount = txtRptToAmount.Text.ToDouble();
                    objRequest.UpdateLoanStatus(false);
                    //msgs.InformationalMessage("Loan request forwarded successfully.");

                    if (ddlStatus.SelectedValue == "3")
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث طلب القرض بنجاح") : ("Loan Request updated successfully.");
                    else
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم ارسال طلب القرض بنجاح") : ("Loan request forwarded successfully.");
                    msgs.InformationalMessage(msg);
                    Session["Approve"] = false;

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------

                    SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);

                }



                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
               
                if (ltReasontd != null)
                { 
                    ltReasontd.Style["display"] = "none";
                }
                lnkDelete.OnClientClick = "return false;";
                lnkDelete.Enabled = false;
                mpeMessage.Show();

                break;
            case "Cancel":
                if (ViewState["PrevPage"] != null)
                    this.Response.Redirect(ViewState["PrevPage"].ToString());
                else
                    this.Response.Redirect("home.aspx");
                break;

            case "RequestCancel":

                objRequest = new clsLoanRequest();
                objUser = new clsUserMaster();
                objLeaveRequest = new clsLeaveRequest();
                objEmployee = new clsEmployee();

                objRequest.RequestId = Convert.ToInt32(fvLoanDetails.DataKey["RequestId"]);
                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                iRequestId = Convert.ToInt32(fvLoanDetails.DataKey["RequestId"]);

                if (objRequest.CancelValidation(iRequestId))
                {
                    //msgs.InformationalMessage("Payment made.Cannot cancel request");
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم الدفع .لا يمكن الغاء الطلب") : ("Payment made.Cannot cancel request");
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
                objRequest.Reason = txtsingleReason.Text;
                objRequest.UpdateCancelStatus();

                //--------------------------------------------Message insertion into HRMessages-----------------------------------------

                SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);

                //--------------------------------------------------------------------------------------------------------------------------------------------------
                //msgs.InformationalMessage("Request updated successfully.");
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث الطلب بنجاح") : ("Request updated successfully.");
                msgs.InformationalMessage(msg);

                ViewState["RequestCancel"] = false;
                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                
                if (ltReasontd != null)
                {
                    ltReasontd.Style["display"] = "none";
                }
                mpeMessage.Show();

                break;

            case "RequestForCancel":

                objRequest = new clsLoanRequest();
                objUser = new clsUserMaster();

                iRequestId = Convert.ToInt32(fvLoanDetails.DataKey["RequestId"]);
                objRequest.RequestId = iRequestId;
                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                objRequest.EmployeeId = objUser.GetEmployeeId();
                //objRequest.Reason = txtEditReason.Text;
                objRequest.Reason = txtsingleReason.Text;

                if (objRequest.CancelValidation(iRequestId))
                {
                    //msgs.InformationalMessage("Payment made.Cannot cancel request");
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم الدفع .لا يمكن الغاء الطلب") : ("Payment made.Cannot cancel request");
                    msgs.InformationalMessage(msg);
                    mpeMessage.Show();
                    break;
                }
                objRequest.UpdateLoanCancellation();
                //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                //-----------------------------------------------------------------------------------------------------------                  

                ViewState["RequestForCancel"] = false;
                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);

                txtsingleReason.Visible = false;
                lblSingleReason.Visible = true;
                //msgs.InformationalMessage("Request updated successfully.");
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث الطلب بنجاح") : ("Request updated successfully.");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                
                if (ltReasontd != null)
                {
                    ltReasontd.Style["display"] = "none";
                }
                break;

        }
        upForm.Update();
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {

        Session["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        BindDataList();

        upForm.Update();
        upDetails.Update();
    }
    protected void dlLoanRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg;
        switch (e.CommandName)
        {
            case "_View":
                HiddenField hfLink = (HiddenField)fvLoanDetails.FindControl("hfLink");
                HtmlTableRow trReasons = (HtmlTableRow)fvLoanDetails.FindControl("trReasons");
                if (hfLink != null && trReasons != null)
                {
                    hfLink.Value = "0";
                    trReasons.Style["display"] = "none";
                }
                fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد أنك تريد حذف?');") : ("return confirm('Are you sure you want to delete?');");// "return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;
                break;

            case "_Edit":

                fvLoanDetails.ChangeMode(FormViewMode.Edit);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;

            case "_Cancel":
                int iRequestId = e.CommandArgument.ToInt32();
                objRequest = new clsLoanRequest();
                objUser = new clsUserMaster();
                objRequest.StatusId = (int)RequestStatus.RequestForCancel;
                objRequest.RequestId = iRequestId;
                objRequest.EmployeeId = objUser.GetEmployeeId();
                objRequest.Reason = "Loan Request for Cancellation";
                objRequest.UpdateCancelStatus();

                //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                SendMessage((int)RequestStatus.RequestForCancel, iRequestId);
                //--------------------------------------------------------------------------------------------------------------------------------------------------                   
                //msgs.InformationalMessage("Request updated successfully.");
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث الطلب بنجاح") : ("Request updated successfully.");
                msgs.InformationalMessage(msg);
                BindDataList();
                mpeMessage.Show();
                //ViewState["RequestCancel"] = true;
                //fvLoanDetails.ChangeMode(FormViewMode.ReadOnly);
                //BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                //lnkDelete.Enabled = false;
                //lnkDelete.OnClientClick = "return false;";

                break;
        }
    }
    /// <summary>
    /// bind selected request details in formview 
    /// </summary>
    /// <param name="iRequestId"></param>
    private void BindRequestDetails(int iRequestId)
    {
        objRequest = new clsLoanRequest();

        objRequest.Mode = "EDT";
        objRequest.RequestId = iRequestId;

        fvLoanDetails.DataSource = (clsUserMaster.GetCulture() == "ar-AE" ? objRequest.GetRequestDetails(1) : objRequest.GetRequestDetails(0)); //objRequest.GetRequestDetails();
        fvLoanDetails.DataBind();
        LoanRequestPager.Visible = false;

        HtmlTableRow trAuthority = (HtmlTableRow)fvLoanDetails.FindControl("trAuthority");
        DataList dl1 = (DataList)fvLoanDetails.FindControl("dl1");
        if (trAuthority != null)
        {
            objRequest.RequestId = iRequestId;            
            objRequest.CompanyId = objRequest.GetCompany();
            DataTable dt2 = objRequest.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                trAuthority.Style["display"] = "table-row";
                dl1.DataSource = dt2;
                dl1.DataBind();
            }
        }

        //link to display approval level remarks
        HtmlTableRow trlink = (HtmlTableRow)fvLoanDetails.FindControl("trlink");
        //ImageButton imgBack2 = (ImageButton)fvLoanDetails.FindControl("imgBack2");
        if (trlink != null)
            trlink.Style["display"] = "table-row";
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)       
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);                
        else       
            ViewState["RequestID"] = iRequestId;           
       
        dlLoanRequest.DataSource = null;
        dlLoanRequest.DataBind();
        upForm.Update();
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        objRequest = new clsLoanRequest();

        if (dlLoanRequest.Items.Count > 0)
        {
            foreach (DataListItem item in dlLoanRequest.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkLoan");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {
                    objRequest.RequestId = Convert.ToInt32(dlLoanRequest.DataKeys[item.ItemIndex]);
                    if (objRequest.IsLoanRequestApproved())
                    {
                        //  message = "Approved loan request(s) could not be deleted.";
                        //message = "Processing started request(s) cannot be deleted.";
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم بدأ معالجة الطلب (ات) .لا يمكن الحذف") : ("Processing started request(s) cannot be deleted.");
                        //message += "</ol>Please delete the existing details to proceed with employee deletion";
                    }
                    else
                    {
                        objRequest.Mode = "DEL";
                        objRequest.DeleteRequest();
                        clsCommonMessage.DeleteMessages(dlLoanRequest.DataKeys[item.ItemIndex].ToInt32(), eReferenceTypes.LoanRequest);
                        //message = "Request(s) deleted successfully";
                        message = clsUserMaster.GetCulture() == "ar-AE" ? (" تم حذف الطلب(ات) بنجاح") : ("Request(s) deleted successfully");

                    }

                }
            }
        }
        else
        {
            if (fvLoanDetails.CurrentMode == FormViewMode.ReadOnly)
            {
                objRequest.RequestId = Convert.ToInt32(fvLoanDetails.DataKey["RequestId"]);

                if (objRequest.IsLoanRequestApproved())
                {
                    // message = "Approved loan request could not be deleted.";
                    //message = "Processing started request(s) cannot be deleted.";
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم بدأ معالجة الطلب (ات) .لا يمكن الحذف") : ("Processing started request(s) cannot be deleted.");
                    //message += "</ol>Please delete the existing details to proceed with employee deletion";
                }
                else
                {
                    objRequest.Mode = "DEL";
                    objRequest.DeleteRequest();
                    clsCommonMessage.DeleteMessages(fvLoanDetails.DataKey["RequestId"].ToInt32(), eReferenceTypes.LoanRequest);
                    //message = "Request deleted successfully";
                    message = clsUserMaster.GetCulture() == "ar-AE" ? (" تم حذف الطلب  بنجاح") : ("Request deleted successfully");
                }
            }
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();

        upForm.Update();
        upDetails.Update();
    }


    protected void dlInstallments_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemIndex == 0)
        {
            TextBox txtDate = (TextBox)e.Item.FindControl("txtDate");
            if (txtDate != null)
                txtDate.Enabled = false;
        }
    }
    protected void dlLoanRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
            int iRequestId = 0;
            iRequestId = Convert.ToInt32(txtStatusId.Text);
            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");
            HiddenField hdRequestID = (HiddenField)e.Item.FindControl("hdRequestID");
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
            if (iRequestId == 2 || iRequestId == 3 || iRequestId == 4 || iRequestId == 5 || iRequestId == 6 || iRequestId == 7 || Convert.ToBoolean(hdForwarded.Value) == true)
            {
                imgEdit.Enabled = false;
                imgEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton imgCancel = (ImageButton)e.Item.FindControl("imgCancel");

            if (iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
            {
                imgCancel.Enabled = false;
                imgCancel.ImageUrl = "~/images/cancel_disable.png";
            }
            if (iRequestId == 1 && !Convert.ToBoolean(hdForwarded.Value))
            {
                imgCancel.Enabled = false;
                imgCancel.ImageUrl = "~/images/cancel_disable.png";
            }

            Label lblDlCurrency = (Label)e.Item.FindControl("lblDlCurrency");
            objRequest = new clsLoanRequest();
            if (objUser.GetEmployeeId() > 0)
            {
                objRequest.EmployeeId = objUser.GetEmployeeId();
                if (lblDlCurrency != null)
                    lblDlCurrency.Text = objRequest.GetCurrency();
            }
            else
            {
                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("درهم") : ("AED");
                if (lblDlCurrency != null)
                    lblDlCurrency.Text = msg;// "AED";
            }

            if (iRequestId == 5)
            {
                if (objRequest.CancelValidation(hdRequestID.Value.ToInt32()))
                {
                    imgCancel.Enabled = false;
                    imgCancel.ImageUrl = "~/images/cancel_disable.png";
                }
                HtmlTableRow trApproval = (HtmlTableRow)e.Item.FindControl("trApproval");
                trApproval.Style["display"] = "table-row";
            }

        }
    }

    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void txtInterestRate_TextChanged(object sender, EventArgs e)
    {
        GetInstallments();
    }
    protected void txtLoanAmount_TextChanged(object sender, EventArgs e)
    {
        GetInstallments();
    }
    protected void txtNoOfInstallments_TextChanged(object sender, EventArgs e)
    {
        GetInstallments();
    }
    protected void ddlInterestType_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetInstallments();
        DropDownList ddlInterestType = sender as DropDownList;
        //RequiredFieldValidator rfvRate = (RequiredFieldValidator)fvLoanDetails.FindControl("rfvRate");
        //RangeValidator rvRate = (RangeValidator)fvLoanDetails.FindControl("rvRate");
        TextBox txtInterestRate = (TextBox)fvLoanDetails.FindControl("txtInterestRate");

        if (ddlInterestType.SelectedValue == "1")
        {
            //rvRate.ValidationGroup = "";
            //rfvRate.ValidationGroup = "";
            txtInterestRate.Enabled = false;
            txtInterestRate.Text = "0";
        }
        else
        {
            //rvRate.ValidationGroup = "FinalApproval";
            //rfvRate.ValidationGroup = "FinalApproval";
            txtInterestRate.Enabled = true;
        }
    }
    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        TextBox txtAmount = sender as TextBox;
        DataListItem Item = txtAmount.Parent as DataListItem;
        CalculatePaymentAmount(Item.ItemIndex);
    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        TextBox txtDate = sender as TextBox;
        DataListItem Item = txtDate.Parent as DataListItem;
        DataList dlInstallments = (DataList)fvLoanDetails.FindControl("dlInstallments");
        Label lblEndDate = (Label)fvLoanDetails.FindControl("lblEndDate");
        if (Item.ItemIndex == dlInstallments.Items.Count - 1)
        {
            lblEndDate.Text = clsCommon.Convert2DateTime(((TextBox)dlInstallments.Items[Item.ItemIndex].FindControl("txtDate")).Text).ToString("dd MMM yyyy");
        }

    }
    protected void lnkLoanDetails_Click(object sender, EventArgs e)
    {
        GridView dgv = ((GridView)((LinkButton)sender).Parent.FindControl("dgvPreviousLoan"));

        AjaxControlToolkit.ModalPopupExtender mpe = ((AjaxControlToolkit.ModalPopupExtender)((LinkButton)sender).Parent.FindControl("mpe"));
        UpdatePanel updPreviousLoan = ((UpdatePanel)((LinkButton)sender).Parent.FindControl("updPreviousLoan"));

        if (dgv != null)
        {
            dgv.DataSource = clsLoanRequest.GetEmployeePreviousLoanDetails(1);
            dgv.DataBind();
        }

        if (mpe != null && updPreviousLoan != null)
        {
            updPreviousLoan.Update();
            mpe.Show();
        }


    }

    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsLoanRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            LinkButton lbReason = (LinkButton)fvLoanDetails.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)fvLoanDetails.FindControl("hfLink");
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                HtmlTableRow trReasons = (HtmlTableRow)fvLoanDetails.FindControl("trReasons");
                GridView gvReasons = (GridView)fvLoanDetails.FindControl("gvReasons");
                trReasons.Style["display"] = "table-row";

                DataTable dt = objRequest.DisplayReasons(objRequest.RequestId);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                HtmlTableRow trReasons = (HtmlTableRow)fvLoanDetails.FindControl("trReasons");
                trReasons.Style["display"] = "none";
            }
        }
    }
}
