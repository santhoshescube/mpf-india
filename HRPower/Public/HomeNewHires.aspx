﻿<%@ Page Language="C#" MasterPageFile="~/Master/ManagerHomeMasterPage.master" AutoEventWireup="true"
    CodeFile="HomeNewHires.aspx.cs" Inherits="Public_HomeNewHires" Title="New Hires" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li ><a href='ManagerHomeNew.aspx'> <asp:Literal ID="Literal1" runat ="server" Text= '<%$Resources:MasterPageCommon,DashBoard%>'>
            
            </asp:Literal> </a></li>
             <li ><a href='HomeRecentActivity.aspx'><span> <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal> </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal> </span></a></li>
            <li ><a href='HomeActiveVacancies.aspx'><span>  <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal> </span></a></li>
            <li class="selected"><a href='HomeNewHires.aspx'><span> <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal></span></a></li>
            <li><a href='HomeAlerts.aspx'><span><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal></span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/New_Hires.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b>   <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="NewHires"></asp:Literal> </b>
        </div>
    </div>
    <div style="float: left; width: 99%;">
        <asp:UpdatePanel ID="upNewHires" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="3" cellspacing="0" id="tblNewHiresHeader" runat="server" align="right">
                                <tr>
                                    <td>
                                        <div style="float: right; width: 96%; padding-right: 40px">
                                            <asp:DropDownList ID="ddlHireVacancy" runat="server" DataTextField="Job" DataValueField="JobId"
                                                Width="250px" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlHireVacancy_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <asp:DataList ID="dlNewHires" runat="server" BorderWidth="0px" CellPadding="0" Width="98%"
                                OnPreRender="dlNewHires_PreRender" DataKeyField="CandidateId">
                                <HeaderTemplate>
                                    <div style="float: left; width: 98%;" class="datalistheader">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 35%;">
                                                      <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Candidate"></asp:Literal>
                                                </td>
                                                <td style="width: 35%;">
                                                  <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="Job"></asp:Literal>
                                                </td>
                                                <td style="padding-right: 50px">
                                                   <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="Department"></asp:Literal> 
                                                </td>
                                                <%--<td width="120"  class="datalistHeaderWhite">
                                                                                        Join Date
                                                                                    </td>--%>
                                            </tr>
                                        </table>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top: 10px;">
                                        <tr style="padding-top: 10px" class="Dashboradlink">
                                            <td style="width: 35%;" class="datalistTrLeft">
                                                <a href='<%# "CandidateEng.aspx?CandidateId=" + Eval("CandidateId") %>'  meta:resourcekey="CandidateView">
                                                    <%# Eval("Salutation")%>
                                                    <%# Eval("CandidateName")%></a>
                                            </td>
                                            <td style="width: 35%;" class="datalistTrRight">
                                               <a href='<%# "Vacancy.aspx?JobID=" + Eval("JobID") %>' meta:resourcekey="JobView">
                                                    &nbsp;<%# Eval("Job")%></a>
                                            </td>
                                            <%--  <td>
                                                                                        <%# Eval("DateofJoining")%>
                                                                                    </td>--%>
                                            <td class="datalistTrRight">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td class="datalistTrRight">
                                                            &nbsp;<%# Eval("Department")%>
                                                        </td>
                                                        <td width="20"  style ="padding-right:10px;">
                                                     
                                                        
                                                            <asp:ImageButton CssClass="linkbutton" ID="ibtnMakeasEmployee" CommandName="_MakeEmployee"
                                                               meta:resourcekey="MakeEmployee"  Visible='<%# SetVisibility()%>'
                                                                PostBackUrl='<%# "~/public/CreateEmployee.aspx?CandidateId=" + Eval("CandidateId") + "&VacancyId=" + Eval("JobID") %>'
                                                                runat="server" ImageUrl="~/images/Make as Employee.png"></asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="pgrNewHires" runat="server" OnFill="BindNewHires" PageSize="10" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: none;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

