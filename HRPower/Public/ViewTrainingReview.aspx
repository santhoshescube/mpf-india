﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="ViewTrainingReview.aspx.cs" Inherits="Public_ViewTrainingReview" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
<div id="dvHavePermission" runat="server" style="width: 100%; margin-top: 20px;">
    <asp:DataList ID="DataList1" runat="server">
    <HeaderTemplate>
     <table width="100%">
    <tr>
    <td style="width: 15%; vertical-align: top">
                                    <asp:Label ID="lblempname" runat="server" CssClass="labeltext" ForeColor="White"
                                       meta:resourcekey="Name"></asp:Label>
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                    <asp:Label ID="lblStatus" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="Status"></asp:Label>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    <asp:Label ID="lblsuggested" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="Cource"></asp:Label>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    <asp:Label ID="lblremark" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="Remark"></asp:Label>
                                </td>
    </tr>
    </table>
    </HeaderTemplate>
    <ItemTemplate>
    <table>
      <tr>
                                <td style="width: 15%; vertical-align: top">
                                    <%# Eval("Name")%>
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                   
                                   
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    
                                </td>
                                
                                <td style="width: 10%; vertical-align: top;">
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("PerformanceInitiationID") %>'
                                        CommandName="_EditPerformance" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit %>' />
                                </td>
                            </tr>
    </table>
    </ItemTemplate>
    </asp:DataList>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

