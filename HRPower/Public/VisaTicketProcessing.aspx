﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="VisaTicketProcessing.aspx.cs" Inherits="Public_VisaTicketProcessing"
    Title="Visa/Ticket Processing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li class='selected'><a href='VisaTicketProcessing.aspx'><span>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></span></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upnlAddEdit" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddEdit" runat="server" style="width: 100%; display: none;">
                <fieldset id="fsVisa" runat="server" style="width: 94.7%; margin-left: 10px; color: #307296;
                    font-weight: bold;">
                    <legend><asp:Literal ID="Literal2" runat="server" meta:resourcekey="VisaDetails"></asp:Literal><%--Visa Details--%></legend>
                    <div style="float: left; width: 96%;">
                        <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                        </div>
                        <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                            <asp:DropDownList ID="ddlCandidate" runat="server" DataTextField="CandidateName"
                                DataValueField="CandidateID" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="ddlCandidate_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvCandidate" runat="server" CssClass="error" ErrorMessage="*"
                                Display="Dynamic" ControlToValidate="ddlCandidate" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <%--<asp:UpdatePanel ID="upnlProcessingStatus" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="float: left; width: 96%;">
                                <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="ProcessingStatus"></asp:Literal>
                                </div>
                                <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                    <asp:DropDownList ID="ddlProcessingStatus" runat="server" DataTextField="Status"
                                        DataValueField="StatusID" Width="200px" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnProcessingStatusReference" runat="server" CausesValidation="False"
                                        CommandName="ProcessingStatus" CssClass="referencebutton" OnClick="btnProcessingStatusReference_Click"
                                        Text="..." />
                                    <asp:RequiredFieldValidator ID="rfvProcessingStatus" runat="server" CssClass="error"
                                        ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlProcessingStatus" ValidationGroup="submit"
                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="float: left; width: 96%;">
                        <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="VisaSentStatus"></asp:Literal>
                        </div>
                        <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                            <asp:CheckBox ID="chkVisaSentStatus" runat="server" Checked="false" AutoPostBack="true"
                                OnCheckedChanged="chkVisaSentStatus_CheckedChanged" />
                        </div>
                    </div>
                    <div id="divVisaSentDate" runat="server" style="float: left; width: 100%; display: none;">
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="VisaSentDate"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtVisaSentdate" runat="server" CssClass="textbox_mandatory" Width="100px"></asp:TextBox>&nbsp;
                                <asp:ImageButton ID="ibtnVisaSentdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="false" CssClass="imagebutton" />
                                <AjaxControlToolkit:CalendarExtender ID="extenderVisaSentdate" runat="server" TargetControlID="txtVisaSentdate"
                                    Format="dd/MM/yyyy" PopupButtonID="ibtnVisaSentdate">
                                </AjaxControlToolkit:CalendarExtender>
                                <asp:CustomValidator ID="cvtxtVisaSentdatepnlTab1" runat="server" CssClass="error"
                                    ControlToValidate="txtVisaSentdate" Display="Dynamic" ValidationGroup="submit"
                                    ClientValidationFunction="Checkdate"></asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="rfvtxtVisaSentdatepnlTab1" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtVisaSentdate" Display="Dynamic" ValidationGroup="submit"
                                    CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal2" runat="server" Text="Visa Type"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:DropDownList ID="ddlVisaType" runat="server" DataValueField="VisaTypeID" DataTextField="VisaType"
                                    Width="200px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvVisatype" runat="server" CssClass="error" ErrorMessage="*"
                                    Display="Dynamic" ControlToValidate="ddlVisaType" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="VisaNumber"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtVisaNumber" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvVisaNumber" runat="server" ErrorMessage="*" ControlToValidate="txtVisaNumber"
                                    Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:DocumentsCommon,PlaceOfIssue%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtPlaceofIssue" CssClass="textbox_mandatory" MaxLength="45" runat="server"
                                    Width="182px"></asp:TextBox>
                            </div>
                        </div>
                    <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>
                                &nbsp;
                                <asp:ImageButton ID="btnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="False" />
                                <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                    Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                                <asp:CustomValidator ID="cvVisaIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                    ClientValidationFunction="validatevisaIssuedate" CssClass="error" ValidateEmptyText="True"
                                    ValidationGroup="submit" Display="Dynamic"></asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="rfvIssuedate" runat="server" ErrorMessage="*" ControlToValidate="txtIssuedate"
                                    Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                   <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal1" runat="server" Text="Validity Date"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtVisaValidityDate" runat="server" CssClass="textbox_mandatory"
                                    Width="100px"></asp:TextBox>&nbsp;
                                <asp:ImageButton ID="imgVisaValidityDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="false" CssClass="imagebutton" />
                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtVisaValidityDate"
                                    Format="dd/MM/yyyy" PopupButtonID="imgVisaValidityDate">
                                </AjaxControlToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvvalidity" runat="server" ErrorMessage="*" ControlToValidate="txtVisaValidityDate"
                                    Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                         <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>
                                &nbsp;
                                <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="False" />
                                <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                    Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                <asp:CustomValidator ID="cvVisaExpirydate" runat="server" ControlToValidate="txtExpiryDate"
                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="submit" ClientValidationFunction="validatevisaExpirydate"
                                    Display="Dynamic"></asp:CustomValidator>
                                <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ErrorMessage="*" ControlToValidate="txtExpiryDate"
                                    Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal30" runat="server" Text="Visa Amount"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                <asp:TextBox ID="txtVisaAmount" runat="server" MaxLength="15" Width="120px" />
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="fteVisaAmount" runat="server" FilterType="Numbers,Custom"
                                    TargetControlID="txtVisaAmount" ValidChars=".">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>--%>
                    <asp:UpdatePanel ID="upnlVisa" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvVisaDetails" runat="server" AutoGenerateColumns="False" RowStyle-HorizontalAlign="Center"
                                BorderColor="#307296" OnRowDataBound="gvVisaDetails_RowDataBound" Width="720px">
                                <HeaderStyle CssClass="datalistheader" />
                                <RowStyle HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl1" runat="server"  meta:resourcekey="ProcessingStatus"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="upnlProcessingStatus" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddlProcessingStatus" runat="server" DataTextField="Status"
                                                        DataValueField="StatusID" Width="100px" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <asp:Button ID="btnProcessingStatusReference" runat="server" CausesValidation="False"
                                                        CommandName="ProcessingStatus" CssClass="referencebutton" OnClick="btnProcessingStatusReference_Click"
                                                        Text="..." />
                                                    <asp:RequiredFieldValidator ID="rfvProcessingStatus" runat="server" CssClass="error"
                                                        ErrorMessage="*" Display="Dynamic" ControlToValidate="ddlProcessingStatus" ValidationGroup="submit"
                                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl2" runat="server" Text="Visa Number"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVisaNumber" runat="server" Text='<%Eval("VisaNumber") %>' Width="85px" />
                                            <asp:RequiredFieldValidator ID="rfvVisaNumber" runat="server" ErrorMessage="*" ControlToValidate="txtVisaNumber"
                                                Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl3" runat="server" Text="Visa Type"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlVisaType" runat="server" DataValueField="VisaTypeID" DataTextField="VisaType"
                                                Width="130px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvVisatype" runat="server" CssClass="error" ErrorMessage="*"
                                                Display="Dynamic" ControlToValidate="ddlVisaType" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl4" runat="server" meta:resourcekey="IssueDate"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="87px" MaxLength="10"
                                                Text='<%Eval("VisaIssueDate") %>'></asp:TextBox>
                                            <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                                Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                                            <asp:CustomValidator ID="cvVisaIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                                ClientValidationFunction="validatevisaIssuedate" CssClass="error" ValidateEmptyText="True"
                                                ValidationGroup="submit" Display="Dynamic"></asp:CustomValidator>
                                            <asp:RequiredFieldValidator ID="rfvIssuedate" runat="server" ErrorMessage="*" ControlToValidate="txtIssuedate"
                                                Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl5" runat="server" Text="Validity Date"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVisaValidityDate" runat="server" CssClass="textbox_mandatory"
                                                Text='<%Eval("VisaValidityDate") %>' Width="75px"></asp:TextBox>
                                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtVisaValidityDate"
                                                Format="dd/MM/yyyy" PopupButtonID="imgVisaValidityDate">
                                            </AjaxControlToolkit:CalendarExtender>
                                            <asp:RequiredFieldValidator ID="rfvvalidity" runat="server" ErrorMessage="*" ControlToValidate="txtVisaValidityDate"
                                                Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl6" runat="server" meta:resourcekey="ExpiryDate"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="87px" MaxLength="10"
                                                Text='<%Eval("VisaExpiryDate") %>'></asp:TextBox>
                                            <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                                Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                            <asp:CustomValidator ID="cvVisaExpirydate" runat="server" ControlToValidate="txtExpiryDate"
                                                CssClass="error" ValidateEmptyText="false" ValidationGroup="submit" ClientValidationFunction="validatevisaExpirydate"
                                                Display="Dynamic"></asp:CustomValidator>
                                            <%-- <asp:RequiredFieldValidator ID="rfvExpiryDate" runat="server" ErrorMessage="*" ControlToValidate="txtExpiryDate"
                                                Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>--%>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl7" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVisaAmount" runat="server" MaxLength="15" Width="54px" Text='<%Eval("VisaAmount") %>' />
                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="fteVisaAmount" runat="server" FilterType="Numbers,Custom"
                                                TargetControlID="txtVisaAmount" ValidChars=".">
                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl8" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtVisaRemarks" runat="server" MaxLength="500" TextMode="MultiLine"
                                                Width="145px" Text='<%Eval("VisaDetailsRemarks") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <HeaderTemplate>
                                            <asp:Literal ID="ltl8" runat="server" meta:resourcekey="AlertRequired"></asp:Literal>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAlert" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="updbtn" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="btnAdd" runat="server" CausesValidation="false" ImageUrl="~/images/AddSymbol.png"
                                                        OnClick="btnAdd_Click"></asp:ImageButton>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="updbtndel" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:ImageButton ID="imgDelete" ImageUrl="~/images/image/Delete_16px.png" runat="server"
                                                        CommandArgument='<%# Eval("VisaID") %>' OnClick="imgDelete_Click" CausesValidation="false" />
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="imgDelete" EventName="click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--Remarks--%>
                    <div style="float: left; width: 96%;">
                        <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal95" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                        </div>
                        <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                            <asp:TextBox ID="txtRemarks" runat="server" Height="70px" MaxLength="1000" TextMode="MultiLine"
                                Width="280px" />
                        </div>
                    </div>
                </fieldset>
                <fieldset id="fsTicket" runat="server" style="width: 94.7%; margin-left: 10px; color: #307296;
                    font-weight: bold;">
                    <legend> <asp:Literal ID="Literal3" runat="server" meta:resourcekey="TicketDetails"></asp:Literal><%--Ticket Details--%></legend>
                    <div style="float: left; width: 96%;">
                        <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="TicketSent"></asp:Literal>
                        </div>
                        <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                            <asp:CheckBox ID="chkTicketSent" runat="server" Checked="false" AutoPostBack="true"
                                OnCheckedChanged="chkTicketSent_CheckedChanged" />
                        </div>
                    </div>
                    <div id="divTicketDetails" runat="server" style="float: left; width: 96%; display: none;">
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="TicketSentDate"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtTicketDate" runat="server" CssClass="textbox_mandatory" Width="120px"></asp:TextBox>&nbsp;
                                <asp:ImageButton ID="imgTicketDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="false" CssClass="imagebutton" />
                                <AjaxControlToolkit:CalendarExtender ID="ceTicketDate" runat="server" TargetControlID="txtTicketDate"
                                    Format="dd-MMM-yyyy " PopupButtonID="imgTicketDate">
                                </AjaxControlToolkit:CalendarExtender>
                                <asp:CustomValidator ID="cvTicketDate" runat="server" CssClass="error" ControlToValidate="txtTicketDate"
                                    Display="Dynamic" ValidationGroup="submit"></asp:CustomValidator><%--ClientValidationFunction="Checkdate"--%>
                                <asp:RequiredFieldValidator ID="rfvTicketDate" runat="server" ControlToValidate="txtTicketDate"
                                    ValidationGroup="submit" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="ArrivalDate"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <div style="float: left;">
                                    <asp:TextBox ID="txtArrival" runat="server" CssClass="textbox_mandatory" Width="90px"></asp:TextBox>&nbsp;
                                    <asp:ImageButton ID="imgArrival" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                        CausesValidation="false" CssClass="imagebutton" />
                                    <AjaxControlToolkit:CalendarExtender ID="ceArrival" runat="server" TargetControlID="txtArrival"
                                        Format="dd/MM/yyyy" PopupButtonID="imgArrival">
                                    </AjaxControlToolkit:CalendarExtender>
                                    <asp:CustomValidator ID="cvArrival" runat="server" CssClass="error" ControlToValidate="txtArrival"
                                        Display="Dynamic" ValidationGroup="submit"></asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="rfvArrivalDate" runat="server" ControlToValidate="txtArrival"
                                        ValidationGroup="submit" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                                <div style="float: left;">
                                    &nbsp; @&nbsp;
                                </div>
                                <div style="float: left;">
                                    <asp:TextBox ID="txtArrivalTime" runat="server" CssClass="textbox_mandatory" Width="80px"
                                        MaxLength="7" Text='<%# Eval("ArrivalTime") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtArrivalTime" runat="server" ControlToValidate="txtArrivalTime"
                                        ValidationGroup="submit" ErrorMessage="*" CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <asp:UpdatePanel ID="upnlVenue" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                        <asp:Literal ID="Literal96" runat="server" meta:resourcekey="Venue"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:DropDownList ID="ddlVenue" runat="server" DataTextField="Venue" DataValueField="VenueID"
                                            Width="250px" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnVenueReferenece" runat="server" CausesValidation="False" CommandName="Venue"
                                            CssClass="referencebutton" OnClick="btnVenueReferenece_Click" Text="..." />
                                        <asp:RequiredFieldValidator ID="rfvVenue" runat="server" CssClass="error" Text="*"
                                            Display="Dynamic" ControlToValidate="ddlVenue" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                <asp:Literal ID="Literal97" runat="server" meta:resourcekey="PickUpRequired"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                <asp:RadioButtonList ID="rblPickUpReq" runat="server" RepeatDirection="Horizontal"
                                    AutoPostBack="True">
                                    <asp:ListItem Value="1" meta:resourcekey="Yes"></asp:ListItem>
                                    <asp:ListItem Selected="True" Value="0" meta:resourcekey="No"></asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal31" runat="server" meta:resourcekey="TicketAmount"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                <asp:TextBox ID="txtTicketAmount" runat="server" MaxLength="15" Width="120px" />
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="fteTicketAmount" runat="server" FilterType="Numbers,Custom"
                                    TargetControlID="txtTicketAmount" ValidChars=".">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                <asp:TextBox ID="txtTicketRemarks" runat="server" Height="70px" MaxLength="1000"
                                    TextMode="MultiLine" Width="280px" />
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div id="tblSubmit" style="width: 44%; float: left; margin-left: 32px; text-align: right;
                    padding-top: auto;">
                    <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                        ToolTip='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="submit" OnClientClick="return VisaTicketTabSelected('submit','divTab1');"
                        Width="75px" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Width="75px" Style="margin-left: 5px;"
                        CssClass="btnsubmit" CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                        ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClientClick="Disp(1);" OnClick="btnCancel_Click" />
                    <asp:HiddenField ID="hfMode" runat="server" />
                </div>
            </div>
            <%-----------------Pop-Up for ErrorMessages-----------------%>
            <div style="height: auto">
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
            </div>
            <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="btn" Text="" runat="server" />
                    </div>
                    <div id="pnlModalPopUp" runat="server" style="display: none;">
                        <uc:ReferenceNew ID="ReferenceNew1" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                        PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------View All Processing-----------------%>
    <asp:UpdatePanel ID="upnlViewAll" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewAll" runat="server" style="display: block;">
                <asp:DataList ID="dlVisaTicket" runat="server" Width="100%" DataKeyField="VisaTicketProcessID"
                    CssClass="labeltext" OnItemCommand="dlVisaTicket_ItemCommand" OnItemDataBound="dlVisaTicket_ItemDataBound">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="selectAll(this.id, 'chkSelect');" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkSelect" runat="server" /></div>
                            <div style="width: 30%; height: 30px; float: left; margin-right: 50%">
                                <asp:LinkButton ID="lnkVisaTicket" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("VisaTicketProcessID") %>'
                                    CommandName="VIEW" CausesValidation="False"><%# Eval("Candidate")%></asp:LinkButton>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("VisaTicketProcessID") %>'
                                    CommandName="EDIT" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Candidate")%>
                        </div>
                        <%-- <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ProcessingStatus"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Status")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="VisaSentStatus"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("VisaSentStatus")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="VisaNumber"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("VisaNumber")%>
                        </div>--%>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="ArrivalDate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("ArrivalDate")%>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="Pager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------View Single Candidate Processing-----------------%>
    <asp:UpdatePanel ID="upnlSingleView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleView" runat="server" style="width: 100%; height: 300px; float: left;
                padding-top: 2px;">
                <div id="div1" runat="server" style="width: 100%; height: 300px; float: left; padding-top: 2px;">
                    <asp:DataList ID="dlSingleView" runat="server" Width="100%" OnItemDataBound="dlSingleView_ItemDataBound">
                        <ItemStyle CssClass="item" />
                        <HeaderStyle CssClass="listItem" />
                        <HeaderTemplate>
                            <table width="95%" border="0" cellpadding="3" class="datalistheader">
                                <tr>
                                    <td valign="top" width="100%" style="padding-left: 5px;">
                                        <asp:Literal ID="Literal42" runat="server" meta:resourcekey="VisaTicketDetails"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Candidate"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px">
                                <%# Eval("Candidate")%>
                            </div>
                            <%----------------------------------MODIFICATIONS---------------------------------------  --%>
                            <div>
                                <table width="95%" border="0" cellpadding="3">
                                    <tr>
                                        <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold;
                                            padding-top: 10px;">
                                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="VisaDetails"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:GridView ID="gvVisa" runat="server" AutoGenerateColumns="false" Width="96%"
                                HeaderStyle-Height="10" BorderColor="#307296">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:BoundField DataField="Status" meta:resourcekey="ProcessingStatus1" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                    <%--<asp:BoundField DataField="VisaNumber" HeaderText="Number" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                    <asp:BoundField DataField="VisaType" HeaderText="Type" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />--%>
                                    <asp:BoundField DataField="VisaIssueDate" meta:resourcekey="IssueDate1" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                    <%-- <asp:BoundField DataField="VisaValidityDate" HeaderText="Validity Date" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />--%>
                                    <asp:BoundField DataField="VisaExpiryDate" meta:resourcekey="ExpiryDate1" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                    <asp:BoundField DataField="VisaAmount" meta:resourcekey="Amount1" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                    <asp:BoundField DataField="VisaDetailsRemarks" meta:resourcekey="Remarks1" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Font-Size="Small" />
                                </Columns>
                            </asp:GridView>
                            <%--------------------------------------------------------------------------------------  --%>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px; padding-top: 15px;">
                                <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px; padding-top: 15px;">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: auto; word-break: break-All;
                                padding-top: 15px;">
                                <%# Eval("VisaRemarks")%>
                            </div>
                            <div>
                                <table width="95%" border="0" cellpadding="3">
                                    <tr>
                                        <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold;
                                            padding-top: 20px;">
                                            <asp:Literal ID="Literal36" runat="server" meta:resourcekey="TicketDetails"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="TicketSent"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px">
                                <%# Eval("TicketSent")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal20" runat="server" meta:resourcekey="TicketSentDate"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                <%# Eval("TicketSentDate")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Arrivaldate"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px">
                                <%# Eval("Arrivaldate")%>&nbsp; @ &nbsp;
                                <%# Eval("ArrivalTime")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Venue"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                <%# Eval("Venue")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal23" runat="server" meta:resourcekey="PickUpRequired"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px; word-break: break-all;">
                                <%# Eval("PickUpReqlbl")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal33" runat="server" meta:resourcekey="TicketAmount"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: 29px;">
                                <%# Eval("TicketAmount")%>
                            </div>
                            <div class="trLeft" style="float: left; width: 26%; height: 29px">
                                <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                            </div>
                            <div class="trLeft" style="float: left; width: 5%; height: 29px">
                                :
                            </div>
                            <div class="trRight" style="float: left; width: 60%; height: auto; word-break: break-All;">
                                <%# Eval("TicketRemarks")%>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------------No Data-----------------%>
    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divNoData" class="error" runat="server" style="float: left; width: 96%;
                display: none;">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAdd" CausesValidation="False" runat="server" ImageUrl="~/images/visa_issue_add.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="False" OnClick="lnkAdd_Click"
                            meta:resourcekey="Add">
                         
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListJob" CausesValidation="false" runat="server" ImageUrl="~/images/visa_issue_view.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkView" runat="server" CausesValidation="False" OnClick="lnkView_Click"
                            meta:resourcekey="View"> 
                       
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDel" ImageUrl="~/images/delete.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" OnClick="lnkDelete_Click"
                            meta:resourcekey="Delete">
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
