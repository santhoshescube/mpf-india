﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Reflection;

/// <summary> 
/// Created By   :  Sruthy K
/// Created Date :  24-Sep-2013
/// Description  :  Document Request
/// </summary>
/// 
public partial class Public_DocumentRequest : System.Web.UI.Page
{
    #region Declaration
    clsDocumentRequestNew MobjclsDocumentRequestNew;
    private string CurrentSelectedValue
    {
        get;
        set;
    }

    #endregion
    protected void Page_PreInit(object sender, EventArgs e)
    {

        clsGlobalization.SetCulture(((Page)this));
    }
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("DocRequestTitle.Title").ToString();

        DocumentRequestPager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;
            hdnEmployeeID.Value = Convert.ToString(new clsUserMaster().GetEmployeeId()); // Get EmployeeID of Current User

            if (Request.QueryString["RequestId"] != null) // From HomePage
            {
                //hdnEmployeeID.Value = clsDocumentRequestNew.GetRequestedEmployeeID(Request.QueryString["RequestId"].ToInt32()).ToString();  
                ViewState["RequestID"] = hdnRequestID.Value = Request.QueryString["RequestId"]; 
                hdnAddMode.Value = "0";
                hdnIsHigherAuthority.Value = IsHigherAuthority(hdnRequestID.Value.ToInt32()) ? "1" : "0";
                hdnIsFromHomePage.Value = "1";

                lnkAdd.Enabled = lnkView.Enabled = lnkDelete.Enabled = false;
                upMenu.Update();

                if (Request.QueryString["Type"] != "Cancel")
                {
                    if (hdnIsHigherAuthority.Value.ToInt32() == 0)
                    {
                        ForwardRequest();
                    }
                    else
                    {    
                        IssueRequest();
                    }          
                }
                else
                {
                    CancelRequest();
                }                
            }
            else
            {
                ViewRequests();
                hdnIsFromHomePage.Value = "0";
            }
            upnlAdd.Update();

        }

        ReferenceControlNew.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }
    #endregion

    #region Reference Control Events
    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
    #endregion

    #region Functions

    /// <summary>
    /// Get Status by RequestID
    /// </summary>
    /// <returns>int</returns>
    protected int GetStatus()
    {
        return clsDocumentRequestNew.GetStatus(hdnRequestID.Value.ToInt32());
    }

    /// <summary>
    /// Issue Request
    /// </summary>
    private void IssueRequest()
    {
        if (GetStatus() == (int)RequestStatus.Approved) // If Already Approved Bind Single View
        {
            BindSingleView();
        }
        else
        {
            SetDivVisiblity(4); 

            txtDocumentDate.Enabled = ibtnDocumentDate.Enabled = false;

            LoadDropDown(0);
            FillRequest();
            lblIssuedBy.Text = GetEmployeeName(hdnEmployeeID.Value.ToInt64());

            LoadDropDown(3);
            FillIssueReason();

            DataTable dtBin =  clsDocumentRequestNew.GetBinNumber(hdnRequestID.Value.ToInt32());
            if (dtBin != null && dtBin.Rows.Count > 0)
            {
                lblBinNumber.Text = Convert.ToString(dtBin.Rows[0]["BinName"]);
                lblBinID.Text = Convert.ToString(dtBin.Rows[0]["BinID"]);
            }
            else
            {
                lblBinNumber.Text = lblBinID.Text = string.Empty;
            }
            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
                ibtnBack.Visible = true;
            }
            else
            {
                ViewState["RequestID"] = hdnRequestID.Value.ToInt32();
                ibtnBack.Visible = false;
            }
            MobjclsDocumentRequestNew = new clsDocumentRequestNew();
            MobjclsDocumentRequestNew.RequestID = Convert.ToInt32(ViewState["RequestID"]);
            MobjclsDocumentRequestNew.CompanyID = MobjclsDocumentRequestNew.GetCompanyID();

            DataTable dt2 = MobjclsDocumentRequestNew.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                dl3.DataSource = dt2;
                dl3.DataBind();
            }
        }
    }

    /// <summary>
    /// Fill Issue Reason
    /// </summary>
    public void FillIssueReason()
    {
        ddlReason.DataTextField = "IssueReason";
        ddlReason.DataValueField = "IssueReasonID";
        ddlReason.DataSource = clsDocumentRequestNew.GetAllIssueReason();
        ddlReason.DataBind();
        upnlReason.Update();

    }

    /// <summary>
    /// Cancel Request
    /// </summary>
    private void CancelRequest()
    {
        SetAuthorityViewVisible(2);
        LoadDropDown(6);
        txtEditReason.Visible = false;
        lblEditReason.Visible = true;
        BindAuthorityView();
    }

    /// <summary>
    /// Forward Request
    /// </summary>
    private void ForwardRequest()
    {
        SetAuthorityViewVisible(1);
        LoadDropDown(2);
        BindAuthorityView();
    }

    /// <summary>
    /// Bind Authority View - Approved,Forwarded,Cancelled,Rejected
    /// </summary>
    private void BindAuthorityView()
    {
        divAdd.Style["display"] = divView.Style["display"] = divSingleView.Style["display"] = "none";
        divAuthorityView.Style["display"] = "block";
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
        DataTable dtRequest = MobjclsDocumentRequestNew.GetSingle();
        if (dtRequest != null && dtRequest.Rows.Count > 0)
        {
            divViewDocumentType.InnerText = Convert.ToString(dtRequest.Rows[0]["DocumentType"]);
            divViewDocumentNumber.InnerText = Convert.ToString(dtRequest.Rows[0]["DocumentNumber"]);
            divViewDocumentDate.InnerText = Convert.ToString(dtRequest.Rows[0]["DocumentFromDate"]);
            
            lblEditReason.Text = Convert.ToString(dtRequest.Rows[0]["Reason"]);
            txtEditReason.Text = Convert.ToString(dtRequest.Rows[0]["Reason"]);
            //divViewReason.InnerText = Convert.ToString(dtRequest.Rows[0]["Reason"]);
            divViewStatus.InnerText = Convert.ToString(dtRequest.Rows[0]["Status"]);
            divViewRequestedBy.InnerText = Convert.ToString(dtRequest.Rows[0]["RequestedBy"]);
        }
        MobjclsDocumentRequestNew.CompanyID = MobjclsDocumentRequestNew.GetCompanyID();

        DataTable dt2 = MobjclsDocumentRequestNew.GetApprovalHierarchy();
        if (dt2.Rows.Count > 0)
        {
            dl2.DataSource = dt2;
            dl2.DataBind();
        }
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            ibtnBack.Visible = true;
        }
        else
        {
            ViewState["RequestID"] = MobjclsDocumentRequestNew.RequestID;
            ibtnBack.Visible = false;
        }   
        upnlAdd.Update();
        upnlSingleView.Update();
        upnlView.Update();
        upnlAuthorityView.Update();
    }

    /// <summary>
    /// Is Higher Authority
    /// </summary>
    /// <returns>true if success otherwise returns false</returns>
    private bool IsHigherAuthority(int RequestID)
    {
        return clsLeaveRequest.IsHigherAuthority((int)RequestType.Document, hdnEmployeeID.Value.ToInt64(),RequestID );
    }

    /// <summary>
    /// View Requests
    /// </summary>
    private void ViewRequests()
    {
        SetDivVisiblity(3);

        hdnRequestID.Value = "0";

        upnlAdd.Update();
        upnlView.Update();
        upnlSingleView.Update();

        DocumentRequestPager.CurrentPage = 0;

        BindDataList();
    }

    /// <summary>
    /// Bind Datalist
    /// </summary>
    private void BindDataList()
    {
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        MobjclsDocumentRequestNew.EmployeeID = hdnEmployeeID.Value.ToInt64();
        MobjclsDocumentRequestNew.PageIndex = DocumentRequestPager.CurrentPage + 1;
        MobjclsDocumentRequestNew.PageSize = DocumentRequestPager.PageSize;
        DataTable dtRequest = MobjclsDocumentRequestNew.GetAllRequests();

        if (dtRequest != null && dtRequest.Rows.Count > 0)
        {
            DocumentRequestPager.Total = MobjclsDocumentRequestNew.GetTotalRecordCount();
            dlDocumentRequest.DataSource = MobjclsDocumentRequestNew.GetAllRequests();
            dlDocumentRequest.DataBind();

            lnkView.Enabled = true;
            lnkDelete.Enabled = true;
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlDocumentRequest.ClientID + "');";
            DocumentRequestPager.Visible = true;
        }
        else
        {
            lnkView.Enabled = false;
            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
            dlDocumentRequest.DataSource = null;
            dlDocumentRequest.DataBind();

            AddNewRequest();

            DocumentRequestPager.Visible = false;
        }
        upnlView.Update();
        upMenu.Update();
    }

    /// <summary>
    /// Validate Request
    /// </summary>
    /// <returns>true if success otherwise returns false</returns>
    private bool ValidateRequest()
    {
        //First Time Request 
        if (Request.QueryString["RequestId"] == null)
        {
            string strRequestedTo = clsLeaveRequest.GetRequestedTo(hdnRequestID.Value.ToInt32(), hdnEmployeeID.Value.ToInt64(), (int)RequestType.Document);
            if (strRequestedTo == string.Empty)
            {
                ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا يمكن معالجة طلبك في هذا الوقت. يرجى الاتصال بقسم الموارد البشرية." : "Your request cannot process this time. Please contact HR department");
                mpeMessage.Show();
                return false;
            }
        }
        if (hdnAddMode.Value == "1" && clsDocumentRequestNew.IsRequestExists(hdnEmployeeID.Value.ToInt64(), ddlDocumentNumber.SelectedValue.ToInt32(), ddlDocumentType.SelectedValue.ToInt32()))
        {
            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب موجود مسبقا" : "Request Already Exists");
            mpeMessage.Show();
            return false;
        }
        else if (divIssue.Style["display"] == "block" && lblBinID.Text.ToInt32() == 0)
        {
            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "بن صالح عدد" : "Invalid Bin Number");
            mpeMessage.Show();
            return false;
        }
        else if (divIssue.Style["display"] == "block" && ddlReason.SelectedValue.ToInt32() == -1)
        {
            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يرجى تحديد سبب المشكلة" : "Please Select Issue Reason");
            mpeMessage.Show();
            return false;
        }
        else if (divIssue.Style["display"] == "block" && ddlIssueStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved && !clsDocumentRequestNew.IsEmployeeInService(hdnRequestID.Value.ToInt32()))
        {
            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "واستقر الموظف." : "Employee is Settled.");
            mpeMessage.Show();
            return false;
        }

        return true;
    }

    /// <summary>
    /// Save Request -- Approval,Reject,Save,Update,Pending
    /// </summary>
    private void SaveRequest()
    {
        try
        {
            if (hdnIsFromHomePage.Value.ToInt32() == 0) // Is From Home Page
            {
                MobjclsDocumentRequestNew = new clsDocumentRequestNew();
                MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
                MobjclsDocumentRequestNew.EmployeeID = hdnEmployeeID.Value.ToInt64();
                MobjclsDocumentRequestNew.DocumentID = ddlDocumentNumber.SelectedValue.ToInt32();
                MobjclsDocumentRequestNew.DocumentNumber = ddlDocumentNumber.SelectedItem.Text.Trim();
                MobjclsDocumentRequestNew.DocumentTypeID = ddlDocumentType.SelectedValue.ToInt32();
                MobjclsDocumentRequestNew.DocumentFromDate = clsCommon.Convert2DateTime(txtDocumentDate.Text);
                MobjclsDocumentRequestNew.Reason = txtReason.Text.Trim();
                MobjclsDocumentRequestNew.StatusID = (int)RequestStatus.Applied;
                MobjclsDocumentRequestNew.RequestedTo = clsLeaveRequest.GetRequestedTo(MobjclsDocumentRequestNew.RequestID.ToInt32(),  hdnEmployeeID.Value.ToInt64(), (int)RequestType.Document);

                if (MobjclsDocumentRequestNew.SaveRequest())
                {
                    hdnRequestID.Value = Convert.ToString(MobjclsDocumentRequestNew.RequestID);
                    SendMessage(MobjclsDocumentRequestNew.StatusID);
                    ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب المحفوظة بنجاح" : "Request Saved Successfully");
                    mpeMessage.Show();
                    ViewRequests();
                }
            }
            else
            {
                if (ddlIssueStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved || ddlIssueStatus.SelectedValue.ToInt32() == (int)RequestStatus.Rejected)// If Approved or Rejected
                {
                    MobjclsDocumentRequestNew = new clsDocumentRequestNew();
                    MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
                    MobjclsDocumentRequestNew.StatusID = ddlIssueStatus.SelectedValue.ToInt32();
                    MobjclsDocumentRequestNew.DocumentID = ddlDocumentNumber.SelectedValue.ToInt32();
                    MobjclsDocumentRequestNew.DocumentNumber = ddlDocumentNumber.SelectedItem.Text.Trim();
                    MobjclsDocumentRequestNew.DocumentTypeID = ddlDocumentType.SelectedValue.ToInt32();
                    MobjclsDocumentRequestNew.IssueDate = clsCommon.Convert2DateTime(txtIssueDate.Text);
                    MobjclsDocumentRequestNew.IssuedBy = hdnEmployeeID.Value.ToInt64();
                    MobjclsDocumentRequestNew.IssueReasonID = ddlReason.SelectedValue.ToInt32();
                    MobjclsDocumentRequestNew.BinID = lblBinID.Text.ToInt32();
                    MobjclsDocumentRequestNew.Reason = txtReason.Text;
                    if(txtExpectedReturnDate.Text != string.Empty)
                    MobjclsDocumentRequestNew.ExpectedReturnDate = clsCommon.Convert2DateTime(txtExpectedReturnDate.Text);

                    MobjclsDocumentRequestNew.Remarks = txtRemarks.Text.Trim();
                    MobjclsDocumentRequestNew.ApprovedBy = hdnEmployeeID.Value;


                    if (MobjclsDocumentRequestNew.SaveIssue()) 
                    {
                        hdnRequestID.Value = Convert.ToString(MobjclsDocumentRequestNew.RequestID);

                        if (MobjclsDocumentRequestNew.StatusID == (int)RequestStatus.Approved) // If Approved
                        {
                            divPrint.Style["display"] = "block";
                            if (MobjclsDocumentRequestNew.ExpectedReturnDate != null)
                            {
                                // Update Alerts
                                new clsAlerts().AlertMessage(MobjclsDocumentRequestNew.DocumentTypeID, ddlDocumentType.SelectedItem.Text, ddlDocumentType.SelectedItem.Text + " Document Number", MobjclsDocumentRequestNew.DocumentID, MobjclsDocumentRequestNew.DocumentNumber, clsCommon.Convert2DateTime(txtExpectedReturnDate.Text), "Handover", hdnEmployeeID.Value.ToInt32(), GetEmployeeName(hdnEmployeeID.Value.ToInt64()), false);
                            }
                        }
                        else divPrint.Style["display"] = "none";
                        upMenu.Update();
                        SendMessage(MobjclsDocumentRequestNew.StatusID);// Send Message

                        if (MobjclsDocumentRequestNew.StatusID == (int)RequestStatus.Approved)
                            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب المعتمدة بنجاح " : "Request Approved Successfully");
                        else
                            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب مرفوض بنجاح" : "Request Rejected Successfully");

                        mpeMessage.Show();
                        BindSingleView();
                    }
                }
                else // If Pending
                {
                    if (UpdateRequestStatus(ddlIssueStatus.SelectedValue.ToInt32())) 
                    {
                        SendMessage(ddlIssueStatus.SelectedValue.ToInt32());
                        ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب تحديث بنجاح" : "Request Updated Successfully");
                        mpeMessage.Show();
                        BindSingleView();
                    }
                }

            }
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    /// <summary>
    /// Send Message
    /// </summary>
    /// <param name="StatusID">StatusID</param>
    private void SendMessage(int StatusID)
    {
        switch(StatusID)
        {
            case (int)RequestStatus.Approved :
                    clsCommonMessage.SendMessage(null, hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document_Approval, eAction.Approved, "");
                    clsCommonMail.SendMail(hdnRequestID.Value.ToInt32(), MailRequestType.Document, eAction.Approved);

                    break;
            case (int)RequestStatus.Rejected :
                    clsCommonMessage.SendMessage(null, hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document_Rejection, eAction.Reject, "");
                    clsCommonMail.SendMail(hdnRequestID.Value.ToInt32(), MailRequestType.Document, eAction.Reject);
                    break;
            case (int)RequestStatus.Applied:
                    clsCommonMessage.SendMessage(hdnEmployeeID.Value.ToInt32(), hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document_Request, eAction.Applied, "");
                    clsCommonMail.SendMail(hdnRequestID.Value.ToInt32(), MailRequestType.Document, eAction.Applied);

                    break;
            case (int)RequestStatus.RequestForCancel :
                    clsCommonMessage.SendMessage(null, hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document__Cancel_Request, eAction.RequestForCancel, "");
                    clsCommonMail.SendMail(hdnRequestID.Value.ToInt32(), MailRequestType.Document, eAction.RequestForCancel);
                    break;
            case (int)RequestStatus.Cancelled :
                    clsCommonMessage.SendMessage(null, hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document_Cancellation, eAction.Cancelled, "");
                    clsCommonMail.SendMail(hdnRequestID.Value.ToInt32(), MailRequestType.Document, eAction.Cancelled);
                    break;
            case (int)RequestStatus.Pending:
                    clsCommonMessage.SendMessage(null, hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest, eMessageTypes.Document_Request, eAction.Pending, "");
                    break;

        }

    }

    /// <summary>
    /// Bind Single View
    /// </summary>
    private void BindSingleView()
    {
        SetDivVisiblity(2);

        dlDocumentRequest.DataSource = null;
        dlDocumentRequest.DataBind();

        lnkDelete.OnClientClick = "return confirm('Do you want to delete?');";
        upMenu.Update();

        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
        DataTable dtRequestDetails = MobjclsDocumentRequestNew.GetSingle();
        if (dtRequestDetails != null && dtRequestDetails.Rows.Count > 0)
        {
            divDocumentType.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["DocumentType"]);
            divDocumentNumber.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["DocumentNumber"]);
            divDocumentDate.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["DocumentFromDate"]);
            divReason.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["Reason"]);
            divStatus.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["Status"]);
            divIssuedBy.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["IssuedBy"]);
            divReasonForIssue.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["IssueReason"]);
            divBinNumber.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["BinName"]);
            divIssueDate.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["IssueDate"]);
            divExpectedReturnDate.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["ExpectedReturnDate"]);
            divRemarks.InnerText = Convert.ToString(dtRequestDetails.Rows[0]["Remarks"]);
            hdnStatusID.Value = Convert.ToString(dtRequestDetails.Rows[0]["StatusId"]);
            hdnForwarded.Value = Convert.ToString(dtRequestDetails.Rows[0]["Forwarded"]);
        }
        MobjclsDocumentRequestNew.CompanyID = MobjclsDocumentRequestNew.GetCompanyID();

        DataTable dt2 = MobjclsDocumentRequestNew.GetApprovalHierarchy();
        if (dt2.Rows.Count > 0)
        {
            dl1.DataSource = dt2;
            dl1.DataBind();
        }
        if (divIssuedBy.InnerText != string.Empty)
        {
            divViewIssue.Style["display"] = "block";
        }
        if (divExpectedReturnDate.InnerText != string.Empty)
        {
            divViewReturnDate.Style["display"] = "block";
        }
        if (divRemarks.InnerText != string.Empty)
        {
            divViewRemarks.Style["display"] = "block";
        }
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            ibtnBack.Visible = true;
        }
        else
        {
            ViewState["RequestID"] = MobjclsDocumentRequestNew.RequestID;
            ibtnBack.Visible = false;
        }   
        upnlView.Update();
        upnlSingleView.Update();

    }

    /// <summary>
    /// Delete Request
    /// </summary>
    private void DeleteRequest()
    {
        try
        {
            string strMessge = string.Empty;

            if (dlDocumentRequest.Items.Count > 0) // Delete in View Mode
            {
                foreach (DataListItem Item in dlDocumentRequest.Items)
                {
                    if (Item.ItemType == ListItemType.Item || Item.ItemType == ListItemType.AlternatingItem)
                    {
                        CheckBox chkDocument = Item.FindControl("chkDocument") as CheckBox;
                        if (chkDocument.Checked)
                        {
                            Label lblStatus = (Label)(Item.FindControl("lblStatusID"));
                            Label lblForwarded = (Label)(Item.FindControl("lblForwarded"));

                            if (lblStatus.Text.ToInt32() == (int)RequestStatus.Cancelled || lblStatus.Text.ToInt32() == (int)RequestStatus.Rejected || (lblStatus.Text.ToInt32() == (int)RequestStatus.Applied && lblForwarded.Text == "False"))
                            {
                                MobjclsDocumentRequestNew = new clsDocumentRequestNew();
                                MobjclsDocumentRequestNew.RequestID = dlDocumentRequest.DataKeys[Item.ItemIndex].ToInt32();
                                if (MobjclsDocumentRequestNew.DeleteRequest())
                                {
                                    clsCommonMessage.DeleteMessages(dlDocumentRequest.DataKeys[Item.ItemIndex].ToInt32(),eReferenceTypes.DocumentRequest);
                                    strMessge = "Request Deleted Successfully";
                                    ViewRequests();
                                }
                            }
                            else
                            {
                                chkDocument.Checked = false;
                                strMessge = clsGlobalization.IsArabicCulture() ? "تجهيز كتبت. طلب لا يمكن المحذوفة." : "Processing Started. Request Cannot be Deleted.";
                            }
                        }
                    }
                }
            }
            else // In Single View Mode
            {
                if (hdnStatusID.Value.ToInt32() == (int)RequestStatus.Cancelled || hdnStatusID.Value.ToInt32() == (int)RequestStatus.Rejected || (hdnStatusID.Value.ToInt32() == (int)RequestStatus.Applied && hdnForwarded.Value == "False"))
                {
                    MobjclsDocumentRequestNew = new clsDocumentRequestNew();
                    MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
                    if (MobjclsDocumentRequestNew.DeleteRequest())
                    {
                        clsCommonMessage.DeleteMessages(hdnRequestID.Value.ToInt32(), eReferenceTypes.DocumentRequest);
                        strMessge = clsGlobalization.IsArabicCulture() ? "طلب المحذوفة بنجاح" : "Request Deleted Successfully";
                        ViewRequests();
                    }
                }
                else
                {
                    strMessge = clsGlobalization.IsArabicCulture() ? "تجهيز كتبت. طلب لا يمكن المحذوفة." : "Processing Started. Request Cannot be Deleted.";
                }

            }
            ucMessage.InformationalMessage(strMessge);
            mpeMessage.Show();
        }
        catch
        {
        }
    }
    /// <summary>
    /// Get Employee Name by ID
    /// </summary>
    /// <param name="lngEmployeeID"></param>
    /// <returns></returns>
    private string GetEmployeeName(long lngEmployeeID)
    {
        return clsDocumentRequestNew.GetEmployeeName(lngEmployeeID);
    }

    /// <summary>
    /// Set Authority Div Visibility
    /// </summary>
    /// <param name="Mode"></param>
    private void SetAuthorityViewVisible(int Mode)
    {
        /* 1 - Forward
         * 2 - Request For Cancel
         * 3 - View Forward,Cancelled Request
         * */
        if (Mode == 1)
        {
            divStatusText.Style["display"] = "none";
            divddlStatus.Style["display"] = "block";
            divApprove.Style["display"] = "block";
        }
        else if (Mode == 2)
        {
            divStatusText.Style["display"] = "none";
            divddlStatus.Style["display"] = "block";
            divApprove.Style["display"] = "block";
            btnApprove.OnClientClick = "return true;";
            ddlStatus.Enabled = false;
        }
        else if (Mode == 3)
        {
            divddlStatus.Style["display"] = "none";
            divStatusText.Style["display"] = "block";
            divApprove.Style["display"] = "none";
        }
        upnlAuthorityView.Update();
    }

    /// <summary>
    /// Fill Request in Edit Mode
    /// </summary>
    private void FillRequest()
    {
        clsDocumentRequestNew objclsDocumentRequestNew = new clsDocumentRequestNew();
        objclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
        objclsDocumentRequestNew = objclsDocumentRequestNew.GetSingleForEdit();
        if (objclsDocumentRequestNew != null)
        {
            ddlDocumentType.SelectedValue = Convert.ToString(objclsDocumentRequestNew.DocumentTypeID);
            LoadDropDown(1);
            ddlDocumentNumber.SelectedValue = Convert.ToString(objclsDocumentRequestNew.DocumentID);
            txtDocumentDate.Text = objclsDocumentRequestNew.DocumentFromDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtReason.Text = objclsDocumentRequestNew.Reason;
            ddlDocumentType.Enabled = ddlDocumentNumber.Enabled = false;
            upnlDocumentType.Update();
            upnlDocumentNumber.Update();
            upnlAdd.Update();
        }
    }

    /// <summary>
    /// Request For Cancel
    /// </summary>
    private void RequestForCancel()
    {
        try
        {
            if (UpdateRequestStatus((int)RequestStatus.RequestForCancel))
                SendMessage((int)RequestStatus.RequestForCancel);
        }
        catch
        {
        }
    }

    /// <summary>
    /// Add New Request
    /// </summary>
    private void AddNewRequest()
    {
        SetDivVisiblity(1);
        ClearControls();
        LoadDropDown(0);
        LoadDropDown(1);
    }

    /// <summary>
    /// Set Div Visibility
    /// </summary>
    /// <param name="intMode"></param>
    private void SetDivVisiblity(int intMode)
    {
        /* 1 - Add New
         * 2 - Single View
         * 3 - View Requests
         * 4 - Issue Request
         * */

        if (intMode == 1)
        {
            divAdd.Style["display"] = "block";
            divView.Style["display"] = divIssue.Style["display"] = divIssueStatus.Style["display"] = divAuthorityView.Style["display"] = divSingleView.Style["display"] = "none";
        }
        else if (intMode == 2)
        {
            divSingleView.Style["display"] = "block";
            divAdd.Style["display"] = divView.Style["display"] = divAuthorityView.Style["display"] = "none";
            divViewIssue.Style["display"] = divAuthorityView.Style["display"] = divViewReturnDate.Style["display"] = divViewRemarks.Style["display"] = "none";
        }
        else if (intMode == 3)
        {
            divView.Style["display"] = "block";
            divAdd.Style["display"] = divSingleView.Style["display"] = divAuthorityView.Style["display"] = "none";
        }
        else if (intMode == 4)
        {
            divAdd.Style["display"] = divIssue.Style["display"] = "block";
            divView.Style["display"] = divSingleView.Style["display"] = divAuthorityView.Style["display"] = "none";

        }
        upnlAdd.Update();
        upnlView.Update();
        upnlSingleView.Update();
        upnlAuthorityView.Update();
    }

    /// <summary>
    /// Update Request Status
    /// </summary>
    /// <param name="intStatusID"></param>
    /// <returns></returns>
    private bool UpdateRequestStatus(int intStatusID)
    {
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();
        MobjclsDocumentRequestNew.StatusID = intStatusID;
        MobjclsDocumentRequestNew.EmployeeID = hdnEmployeeID.Value.ToInt64();
        if (intStatusID == 6)
        {
            MobjclsDocumentRequestNew.Reason = txtEditReason.Text;
        }
        else if (intStatusID == 7)
        {
            MobjclsDocumentRequestNew.Reason = "Document Request for Cancellation";
        }
        else
        {
            MobjclsDocumentRequestNew.Reason = txtReason.Text;
        }
        if (MobjclsDocumentRequestNew.UpdateRequestStatus())
        {
            ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب تحديث بنجاح" : "Request Updated Successfully");
            mpeMessage.Show();
            return true;
        }

        return false;
    }

    /// <summary>
    /// Load Dropdown
    /// </summary>
    /// <param name="intType"></param>
    private void LoadDropDown(int intType)
    {
        if (intType == 0)//Document Type
        {
            ddlDocumentType.DataTextField = "DocumentType";
            ddlDocumentType.DataValueField = "DocumentTypeID";
            ddlDocumentType.DataSource = clsDocumentRequestNew.GetAllDocumentTypes();
            ddlDocumentType.DataBind();
            upnlDocumentType.Update();
        }
        else if (intType == 1)// Document Number
        {
            ddlDocumentNumber.DataTextField = "ReferenceNumber";
            ddlDocumentNumber.DataValueField = "ReferenceID";
            ddlDocumentNumber.DataSource = clsDocumentRequestNew.GetDocumentNumberByDocumentType(ddlDocumentType.SelectedValue.ToInt32(), hdnEmployeeID.Value.ToInt64(), hdnRequestID.Value.ToInt32());
            ddlDocumentNumber.DataBind();
            upnlDocumentNumber.Update();
        }
        else if (intType == 2)// Reporting To Status
        {
            ddlStatus.DataTextField = "Status";
            ddlStatus.DataValueField = "StatusID";
            ddlStatus.DataSource = clsDocumentRequestNew.GetStatusForReportingTo();
            ddlStatus.DataBind();
        }
        else if (intType == 3)// Higher Authority Status
        {
            ddlIssueStatus.DataTextField = "Status";
            ddlIssueStatus.DataValueField = "StatusID";
            ddlIssueStatus.DataSource = clsDocumentRequestNew.GetStatusForHigherAuthority();
            ddlIssueStatus.DataBind();
        }
        else if (intType == 6)// Fill Status during Cancelled
        {
            DataTable dtStatus = new DataTable();
            dtStatus.Columns.Add(new DataColumn("StatusID", typeof(int)));
            dtStatus.Columns.Add(new DataColumn("Status", typeof(string)));

            DataRow dr = dtStatus.NewRow();
            dr["StatusID"] = (int)RequestStatus.Cancelled;
            dr["Status"] = clsGlobalization.IsArabicCulture() ? "ألغي" : "Cancelled";
            dtStatus.Rows.Add(dr);

            ddlStatus.DataTextField = "Status";
            ddlStatus.DataValueField = "StatusID";
            ddlStatus.DataSource = dtStatus;
            ddlStatus.DataBind();
        }

    }

    /// <summary>
    /// Clear Controls
    /// </summary>
    private void ClearControls()
    {
        hdnRequestID.Value = "0";
        hdnAddMode.Value = "1";
        hdnIsFromHomePage.Value = "0";
        ddlDocumentType.Enabled = ddlDocumentNumber.Enabled = true;
        txtDocumentDate.Text = txtIssueDate.Text = txtExpectedReturnDate.Text = new clsCommon().GetSysDate().ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtReason.Text = string.Empty;
        ibtnBack.Visible = false;
        dlDocumentRequest.DataSource = null;
        dlDocumentRequest.DataBind();

        lnkDelete.Enabled = false;
        lnkDelete.OnClientClick = "return false;";


        upnlDocumentType.Update();
        upnlDocumentNumber.Update();
        upnlAdd.Update();
        upnlView.Update();
        upMenu.Update();
    }

    #endregion

    #region Events

    protected void ddlDocumentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDocumentType.SelectedValue.ToInt32() > 0)
        {
            LoadDropDown(1);
            upnlDocumentType.Update();
            upnlDocumentNumber.Update();
        }
    }

    protected void ddlIssueStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlIssueStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved)
        {
            divIssue.Style["display"] = "block";
        }
        else
            divIssue.Style["display"] = "none";

        upnlAdd.Update();
    }

    protected void btnReason_Click(object sender, EventArgs e)
    {
        ReferenceControlNew.ClearAll();
        ReferenceControlNew.TableName = "DocumentIssueReference";
        ReferenceControlNew.DataTextField = "IssueReason";
        ReferenceControlNew.DataValueField = "IssueReasonID";
        ReferenceControlNew.FunctionName = "FillIssueReason";
        ReferenceControlNew.SelectedValue = ddlReason.SelectedValue;
        ReferenceControlNew.DisplayName = "Issue Reason";
        ReferenceControlNew.PredefinedField = "IsPredefined";
        ReferenceControlNew.DataTextFieldArabic = "IssueReasonArb";
        ReferenceControlNew.PopulateData();
        mdlPopUpReference.Show();
        upnlModalPopUp.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ValidateRequest())
            SaveRequest();
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlStatus.SelectedValue.ToInt32() == (int)RequestStatus.Approved || ddlStatus.SelectedValue.ToInt32() == (int)RequestStatus.Rejected)
            {
                MobjclsDocumentRequestNew = new clsDocumentRequestNew();

                MobjclsDocumentRequestNew.RequestID = hdnRequestID.Value.ToInt32();

                if (ddlStatus.SelectedValue.ToInt32() == (int)RequestStatus.Rejected)
                {
                    MobjclsDocumentRequestNew.Forwarded = false;
                    MobjclsDocumentRequestNew.ApprovedBy = hdnEmployeeID.Value;
                    MobjclsDocumentRequestNew.StatusID = (int)RequestStatus.Rejected;
                }
                else
                {
                    MobjclsDocumentRequestNew.Forwarded = true;
                    MobjclsDocumentRequestNew.ForwardedBy = hdnEmployeeID.Value.ToInt64();
                    MobjclsDocumentRequestNew.StatusID = (int)RequestStatus.Applied;
                }
                MobjclsDocumentRequestNew.Remarks = txtEditReason.Text;
                if (MobjclsDocumentRequestNew.UpdateForwardedStatus())
                {
                    if(MobjclsDocumentRequestNew.StatusID == (int)RequestStatus.Applied)
                        ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب إرسالها بنجاح" : "Request Forwarded Successfully");
                    else
                        ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب مرفوض بنجاح" : "Request Rejected Successfully");
                    mpeMessage.Show();
                    SendMessage(MobjclsDocumentRequestNew.StatusID == (int)RequestStatus.Rejected ? (int)RequestStatus.Rejected : (int)RequestStatus.Approved);
                    SetAuthorityViewVisible(3);
                    BindAuthorityView();
                }
            }
            else
            {
                if (UpdateRequestStatus((int)RequestStatus.Cancelled))
                {
                    ucMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "طلب إلغاء بنجاح" : "Request Cancelled Successfully");
                    mpeMessage.Show();
                    SendMessage((int)RequestStatus.Cancelled);
                }
                SetAuthorityViewVisible(3);

                BindAuthorityView();
            }
        }
        catch
        {
        }
        txtEditReason.Visible = false;
        lblEditReason.Visible = true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (hdnIsFromHomePage.Value.ToInt32() == 0)
            ViewRequests();
        else
        {
            if (ViewState["PrevPage"] != null)
                this.Response.Redirect(ViewState["PrevPage"].ToString());
            else
                this.Response.Redirect("home.aspx");
        }
    }

    protected void dlDocumentRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Edit":

                AddNewRequest();
                hdnRequestID.Value = Convert.ToString(e.CommandArgument);
                hdnAddMode.Value = "0";
                FillRequest();
                break;

            case "Cancel":

                hdnRequestID.Value = Convert.ToString(e.CommandArgument);
                hdnAddMode.Value = "0";
                RequestForCancel();
                ViewRequests();
                break;

            case "View":
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
                hdnRequestID.Value = Convert.ToString(e.CommandArgument);
                hdnAddMode.Value = "0";
                BindSingleView();
                break;
        }
    }

    protected void dlDocumentRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblStatusID = e.Item.FindControl("lblStatusID") as Label;
            ImageButton imgEdit = e.Item.FindControl("imgEdit") as ImageButton;
            ImageButton imgCancel = e.Item.FindControl("imgCancel") as ImageButton;
            Label lblForwarded = e.Item.FindControl("lblForwarded") as Label;

            if (lblStatusID.Text.ToInt32() == (int)RequestStatus.Applied && lblForwarded.Text == "False")
            {
                imgEdit.Enabled = true;
            }
            else
            {
                imgEdit.Enabled = false;
                imgEdit.ImageUrl = "~/images/edit_disable.png";

            }

            if ((lblStatusID.Text.ToInt32() == (int)RequestStatus.Applied && lblForwarded.Text == "False") || lblStatusID.Text.ToInt32() == (int)RequestStatus.Rejected || lblStatusID.Text.ToInt32() == (int)RequestStatus.RequestForCancel || lblStatusID.Text.ToInt32() == (int)RequestStatus.Cancelled)
            {
                imgCancel.Enabled = false;
                imgCancel.ImageUrl = "~/images/cancel_disable.png";
            }
            else
                imgCancel.Enabled = true;

            upnlView.Update();
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        DeleteRequest();
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewRequests();
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        AddNewRequest();
    }
    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            MobjclsDocumentRequestNew.RequestID = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                trReasons.Style["display"] = "block";

                DataTable dt = MobjclsDocumentRequestNew.DisplayReasons(MobjclsDocumentRequestNew.RequestID);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
            }
        }
    }
    protected void lbEditReason_OnClick(object source, EventArgs e)
    {
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            MobjclsDocumentRequestNew.RequestID = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfEditlink.Value) == 0)
            {
                hfEditlink.Value = "1";
                trEditReasons.Style["display"] = "block";

                DataTable dt = MobjclsDocumentRequestNew.DisplayReasons(MobjclsDocumentRequestNew.RequestID);
                if (dt.Rows.Count > 0)
                {
                    gvEditReasons.DataSource = dt;
                    gvEditReasons.DataBind();
                }
            }
            else
            {
                hfEditlink.Value = "0";
                trEditReasons.Style["display"] = "none";
            }
        }
    }
    protected void lbApproveReason_OnClick(object source, EventArgs e)
    {
        MobjclsDocumentRequestNew = new clsDocumentRequestNew();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            MobjclsDocumentRequestNew.RequestID = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfApproveLink.Value) == 0)
            {
                hfApproveLink.Value = "1";
                trApproveReason.Style["display"] = "block";

                DataTable dt = MobjclsDocumentRequestNew.DisplayReasons(MobjclsDocumentRequestNew.RequestID);
                if (dt.Rows.Count > 0)
                {
                    gvApproveReason.DataSource = dt;
                    gvApproveReason.DataBind();
                }
            }
            else
            {
                hfApproveLink.Value = "0";
                trApproveReason.Style["display"] = "none";
            }
        }
    }
    #endregion

   
}
