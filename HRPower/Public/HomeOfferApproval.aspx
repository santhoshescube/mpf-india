﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="HomeOfferApproval.aspx.cs" Inherits="Public_HomeOfferApproval"  meta:resourcekey="PageTitle"%>


<%@ Register Src="../Controls/CandidateToEmployeeHistory.ascx" TagName="CandidateHistory" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
 <div id='cssmenu'>
         <ul>
            <li ><a href='ManagerHomeNew.aspx'> <asp:Literal ID="Literal1" runat ="server" Text= '<%$Resources:MasterPageCommon,DashBoard%>'>
            
            </asp:Literal> </a></li>
             <li ><a href='HomeRecentActivity.aspx'><span> <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal> </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal> </span></a></li>
            <li ><a href='HomeActiveVacancies.aspx'><span>  <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal> </span></a></li>
            <li><a href='HomeNewHires.aspx'><span> <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal></span></a></li>
            <li><a href='HomeAlerts.aspx'><span><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal></span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
  
                
  
    <asp:UpdatePanel  ID ="updOffer" runat ="server"  UpdateMode ="Conditional">
    <ContentTemplate >
   
    <div id="dvMain" runat="server">
        <div style="float: left;">
            <h4 style="color: #30a5d6">
            </h4>
        </div>
        <div style="float: right; margin-top: 5px;">
            <asp:Button ID="btnGoBack" runat="server" CssClass="btnsubmit" OnClick="btnGoBack_Click"
                ToolTip="View PendingOffers" Text="<< Back" />
        </div>
        <uc1:CandidateHistory ID="CandidateHistory" runat="server" />
    </div>
    <div id ="dvViewOffers" runat ="server"  style="float: left; width: 99%;">
      <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/Pending_Request.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b><asp:Literal ID="Literal7" runat ="server" meta:resourcekey="PendingOffers" ></asp:Literal> </b>
        </div>
    </div>
        <asp:UpdatePanel ID="upNewHires" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="3" cellspacing="0" id="tblNewHiresHeader" runat="server" align="right">
                                <tr>
                                    <td>
                                        <div style="float: right; width: 96%; padding-right: 40px">
                                            <asp:DropDownList ID="ddlHireVacancy" runat="server" DataTextField="Job" DataValueField="JobId" Visible ="false" 
                                                Width="400px" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlHireVacancy_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </td>
                                      <td>
                                    <asp:DropDownList ID ="ddlStatus" runat ="server" CssClass="dropdownlist" AutoPostBack="True"  Width="200px" DataTextField ="OfferStatus" DataValueField ="OfferStatusID"
                                     OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" >
                                     
                                    </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <asp:DataList ID="dlNewHires" runat="server" BorderWidth="0px" CellPadding="0" Width="98%" 
                              DataKeyField="CandidateId" OnItemCommand="dlNewHires_ItemCommand">
                                <HeaderTemplate>
                                    <div style="float: left; width: 98%;" class="datalistheader">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="width: 40%;">
                                                    <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Candidate"></asp:Literal>
                                                </td>
                                                <td style="width: 30%;">
                                                      <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="Job"></asp:Literal>
                                                </td>
                                                <td style="width: 20%;">
                                                     <asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Status"></asp:Literal> 
                                                </td>
                                                 <td style="width: 20%;">
                                                      <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="Action"></asp:Literal>
                                                </td>
                                              
                                            </tr>
                                        </table>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin-top: 10px;">
                                        <tr style="padding-top: 10px" class="Dashboradlink">
                                            <td style="width: 40%;" class="datalistTrLeft">
                                                <asp:LinkButton ID="lnkCandidate" CssClass="linkbutton" runat="server" CommandArgument='<%# Eval("CandidateId") %>'
                                                    ToolTip="View Candidate" CommandName="_ViewCandidate" CausesValidation="False"><%#Eval("CandidateName")%> </asp:LinkButton>
                                            </td>
                                            <td style="width: 30%;" class="datalistTrRight">
                                               <a href='<%# "VacancyView.aspx?JobID=" + Eval("JobID") %>'">
                                                    &nbsp;<%# Eval("Job")%></a>
                                            </td>
                                         
                                            <td style="width: 20%;" class="datalistTrRight">
                                              
                                                 &nbsp;<%# Eval("OfferStatus")%>
                                          
                                            </td>
                                             <td style="width: 20%;" class="datalistTrRight">
                                                 <a class="actionbutton"  id ="ancApprove" runat ="server"  style="width:50px;text-align:center"     href='<%# GetRequestLink( Eval("OfferID")) %>'
                                                    visible='<%#IsApproveVisible(Eval("OfferStatusId")) %>' meta:resourcekey="Approve">
                                                   </a>  
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divRemarks" runat="server" class="datalistTrRight" style="word-break: break-all">
                                       <asp:Label ID="lblRemarks"   class="labeltext" runat ="server" style="margin-left:10px;"   Text = ' <%# SetMessage(Eval("Remarks"), Eval("RejectedBy"),Eval("OfferStatusID"))%>'></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="pgrOffer" runat="server" OnFill="BindOfferApprovals" PageSize="10" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: none;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
     </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
  <div style="display: block; width: 100%;" id="divCalendar">
        <div style="display: block; width: 100%; padding: 5px 2px 2px 2px; margin-left: 1px;">
            <asp:UpdatePanel ID="upcalender" runat="server">
                <ContentTemplate>
                    <asp:Calendar ID="Calendar5" Width="98%" runat="server" CellPadding="1" OnDayRender="Calendar1_DayRender"
                        NextMonthText="&amp;gt;&amp;gt;  " PrevMonthText="&amp;lt;&amp;lt;" BorderStyle="None">
                        <SelectedDayStyle ForeColor="Black" BorderColor="#0099FF" />
                        <TodayDayStyle CssClass="CalendarDay1" BorderColor="#CC0066" />
                        <DayStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <DayHeaderStyle CssClass="CalendarDay" />
                        <DayStyle CssClass="CalendarDay1" />
                        <SelectedDayStyle ForeColor="Red" />
                        <TitleStyle CssClass="CalendarDay1" />
                        <NextPrevStyle ForeColor="White" CssClass="CalendarNextPrev" />
                    </asp:Calendar>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
       <div style="float: left; font-size: 10px; padding: 2px 2px 2px 2px; width: 100%;">
                        <img src="../images/green.png" />
                        <%--Event--%> <asp:Literal ID="Literal8" runat ="server" Text='<%$Resources:ControlsCommon,Event%>'></asp:Literal>
                        <img src="../images/red.PNG" />
                        <%--Holiday--%><asp:Literal ID="Literal11" runat ="server" Text='<%$Resources:ControlsCommon,Holiday%>'></asp:Literal>
                        <img src="../images/golden.png" />
                        <%--Both--%> <asp:Literal ID="Literal9" runat ="server" Text='<%$Resources:ControlsCommon,Both%>'></asp:Literal>
                    </div>
    </div>
</asp:Content>

