﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true" CodeFile="SendMail.aspx.cs" Inherits="Public_SendMail" Title="Home-SendMail" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" Runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Home.aspx'>DASH BOARD </a></li>
            <li ><a href='HomeRecentActivity.aspx'>RECENT ACTIVITY </a></li>
            <li><a href='HomeActiveCandidates.aspx'><span>ACTIVE CANDIDATES </span></a></li>
            <li><a href='HomeActiveVcancies.aspx'><span>ACTIVE VACANCIES </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>PENDING REQUESTS </span></a></li>
            <li><a href='HomeSearchCandidates.aspx'><span>SEARCH CANDIDATES </span></a></li>
            <li><a href='HomeNewHires.aspx'><span>NEW HIRES </span></a></li>
            <li><a href=''><span>INBOX </span></a></li>
            <li class="selected"><a href="SendMail.aspx"><span>SEND MAIL</span></a> </li>
        </ul>
    </div>
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" Runat="Server">

    <script src="../js/HRPower.js" type="text/javascript"></script>
    
    <div style = "width:100%;">
        <div style = "width:100%;">
            <div style="display: none">
                <asp:Button ID="btnSubmit" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
                
        <div style = "float:left; width:100%;">
            <div runat ="server" id="dvTemplate">
                <div style="float:left; width:10%;" class="trLeft">
                    Templates
                </div>
                <div style="float:right; width:85%;" class="trRight">
                    <asp:UpdatePanel ID="upTemplates" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DropDownList ID="ddlTemplates" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                    DataTextField="Template" DataValueField="TemplateId" 
                                OnSelectedIndexChanged="ddlTemplates_SelectedIndexChanged" Width="35%">
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        
        <div style = "float:left; width:100%;">
            <div style="float:left; width:10%;" class="trLeft">
                From
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <asp:TextBox ID="txtFrom" runat="server" CssClass="textbox_mandatory" Width="35%">
                </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvemail" runat="server" ControlToValidate="txtFrom"
                    CssClass="error" Display="Static" ErrorMessage="Please enter from mail id"
                    ValidationGroup="SendMail">
                </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revFromEmail" runat="server" ControlToValidate="txtFrom"
                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter valid email" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ValidationGroup="SendMail">
                </asp:RegularExpressionValidator>
            </div>
        </div>
        
        <div style = "float:left; width:100%;">
            <div style="float:left; width:10%;" class="trLeft">
                To
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <asp:UpdatePanel ID="pnltxtToMailId" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="txtToMailId" runat="server" CssClass="textbox" Height="50px"
                            TextMode="MultiLine" Width="450px" ValidationGroup="SendMail">
                        </asp:TextBox>
                        <asp:CustomValidator ID="cvRecipients" runat="server" ControlToValidate="txtToMailId"
                            Display="Dynamic" SetFocusOnError="True" ClientValidationFunction="chkRecepients"
                            ValidationGroup="SendMail" ValidateEmptyText="True">
                        </asp:CustomValidator>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSend" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        
        <div style = "float:left; width:100%;">
            <div style="float:left; width:10%;" class="trLeft">
                Cc
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <asp:TextBox ID="txtCc" runat="server" CssClass="textbox" Width="70%"></asp:TextBox>
                <asp:CustomValidator ID="cvCc" runat="server" ControlToValidate="txtCc" Display="Dynamic"
                    SetFocusOnError="True" ClientValidationFunction="chkMails" ValidationGroup="SendMail">
                </asp:CustomValidator>
            </div>
        </div>
        
        <div style = "float:left; width:100%;">
            <div style="float:left; width:10%;" class="trLeft">
                Bcc
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <asp:TextBox ID="txtBcc" runat="server" CssClass="textbox" Width="70%"></asp:TextBox>
                <asp:CustomValidator ID="cvBcc" runat="server" ControlToValidate="txtBcc" Display="Dynamic"
                    SetFocusOnError="True" ClientValidationFunction="chkMails" ValidationGroup="SendMail">
                </asp:CustomValidator>
            </div>
        </div>
        
        <div style = "float:left; width:100%;">
            <div style="float:left; width:10%;" class="trLeft">
                Subject
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <asp:UpdatePanel ID="pnlTxtSubject" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSubject" runat="server" CssClass="textbox" Width="70%"></asp:TextBox>
                    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSend" EventName="Click" />
                </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        
        <div style = "float:left; width:98%;">
            <div style="float:left; width:10%;" class="trLeft">
                Attachments
            </div>
            <div style="float:right; width:85%;" class="trRight">
                <div id="divFiles" style="float:left; width:100%;" class="style1;">
                    <div style="float:left; width:40%;">
                        <asp:FileUpload ID="filAttachment" runat="server" onkeypress="return false;" 
                            onpaste="return false;"/>
                    </div>                        
                    <div style="float:left; width:50%;">                        
                        <div style="float:left; width:10%;">
                            <asp:ImageButton ID="btnAttachFiles" runat="server" ImageUrl="~/images/Attachment.png"
                                ToolTip="Attach Files" OnClick="btnAttachFiles_Click" ValidationGroup="Attach" />
                        </div>
                        <div style="float:left; width:30%;">
                            <a href="#" id="lnkAddMoreAttachments" class="linkbutton" onclick="addMailAttachments();">Add More Files</a>
                           <asp:HiddenField  ID="hdAttachCount" runat="server" Value="0"/>                           
                        </div>
                        <div style="float:right; width:40%;">
                            <asp:CustomValidator ID="cvAttachment" runat="server" ErrorMessage="Select a file"
                                ClientValidationFunction="valAttachment" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="Attach"></asp:CustomValidator>
                        </div>                                              
                    </div>
                </div>
                
                <div style="float:left; width:100%;" class="style1;">
                    <asp:UpdatePanel ID="pnlFiles" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DataList ID="dlFiles" runat="server" OnItemCommand="dlFiles_ItemCommand" Width="100%">
                                <ItemTemplate>
                                    <div style="float:left; width:5%;">
                                        <asp:UpdatePanel ID="pnlRemoveFile" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="ibtnDelete" runat="server" CausesValidation="False" CommandName="_Delete"
                                                    CssClass="imgButton" ImageUrl="~/images/Delete_Icon_Datalist.png" CommandArgument='<%# Eval("FilePath") %>'
                                                    ToolTip="Remove attachment" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div style="float:left; width:85%;">
                                        <%#Eval("FileName") %>
                                    </div>                                    
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlFiles" EventName="ItemCommand" />                                     
                            <asp:AsyncPostBackTrigger ControlID="btnSend" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>     
            </div>
        </div>
        
        <div style = "float:left; width:95%;">
            <asp:UpdatePanel ID="pnlEditor" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <AjaxControlToolkit:Editor ID="txtEditor" runat="server" Height="400px" TabIndex="2" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlTemplates" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        
        <div style = "float:left; width:95%;">
            <div style = "float:right; width:100px;" class="trRight">
                <asp:UpdatePanel ID="upnlSendMail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button ID="btnSend" runat="server" ValidationGroup="SendMail"
                           CssClass="btnsubmit" Text="Send" CommandArgument="MailId" CommandName="SAVE" 
                            onclick="btnSend_Click" />                        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
                        
            <asp:UpdateProgress ID="upProgress" runat="server" AssociatedUpdatePanelID="upnlSendMail">
                <ProgressTemplate>Your mail is sending now...
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>
        
        <div style = "float:left; width:100%;">
            <div class="trLeft" >
               <asp:Label ID="lblMsg" runat="server" CssClass="messagecss" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
