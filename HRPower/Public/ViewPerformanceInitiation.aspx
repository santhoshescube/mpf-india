﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="ViewPerformanceInitiation.aspx.cs" Inherits="Public_ViewPerformanceInitiation"
    Title="View Performance Initiation" %>

<%@ Register Src="../Controls/AssignEvaluators.ascx" TagName="AssignEvaluators" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <%-- <ul>
            <li><a href="SalesPerformance.aspx">Target Performance</a></li>
            <li ><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
            <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li class='selected'><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li><a href="PerformanceAction.aspx">Performance Action</a></li>
        </ul>--%>
        
           <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <%-- <div style="width: 100%; padding-bottom: 10px;">
                        <div style="width: 100%; padding-bottom: 10px;">
                            <div id="Div1" style="float: left; color: #30a5d6">
                               <img src="../images/Interview_Process_DetailsBig.png" />
                               <b>View Initiation</b>
                            </div>
                        </div>
                    </div>--%>
    <div id="dvHavePermission" runat="server" style="width: 100%; margin-top: 20px;">
        <asp:UpdatePanel ID="updViewperformanceinitiation" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlviewPeformance" Width="100%"
                    runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px"
                    RepeatLayout="Table" CellPadding="3" GridLines="Both" OnItemCommand="dlviewPeformance_ItemCommand"
                    OnItemDataBound="dlviewPeformance_ItemDataBound">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <ItemStyle ForeColor="#000066" />
                    <SeparatorStyle BackColor="AliceBlue" />
                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td style="width: 15%; vertical-align: top">
                                    <asp:Label ID="lblSlNoHeader" runat="server" CssClass="labeltext" ForeColor="White"
                                        Text='<%$Resources:ControlsCommon,slno%>'></asp:Label>
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                    <asp:Label ID="Label1" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,Company%>'></asp:Label>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    <asp:Label ID="Label2" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,Department%>'></asp:Label>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:Label ID="Label3" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,FromDate%>'></asp:Label>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:Label ID="Label4" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,ToDate%>'></asp:Label>
                                </td>
                                <td style="width: 10%; vertical-align: top;">
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <ItemTemplate>
                        <table width="100%" class="labeltext">
                            <tr>
                                <td style="width: 15%; vertical-align: top">
                                    <%# Eval("RowNumber")%>
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                    <%# Eval("CompanyName")%>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    <%# Eval("Department")%>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <%# Eval(" FromDate")%>
                                </td>
                                <td style="width: 15%; vertical-align: top;">
                                    <%# Eval(" ToDate")%>
                                </td>
                                <td style="width: 10%; vertical-align: top;">
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("PerformanceInitiationID") %>'
                                        CommandName="_EditPerformance" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit %>' />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%; vertical-align: top;">
                                    <asp:LinkButton ID="lnkviewdetails" runat="server" CommandArgument='<%# Eval("PerformanceInitiationID") %>'
                                        ToolTip='<%$Resources:ControlsCommon,ShowDetails %>' CommandName="_ViewDetails" CausesValidation="False" CssClass="labeltext"
                                        ForeColor="ActiveBorder" Text='<%$Resources:ControlsCommon,ShowDetails %>' ></asp:LinkButton>
                                    <asp:HiddenField ID="hfIsEvaluationStarted" runat="server" Value='<%#Eval("IsEvaluationStarted") %>' />
                                </td>
                            </tr>
                        </table>
                        <asp:HiddenField ID="hfviewdetails" runat="server" Value="0" />
                        <div id="divviewdetails" runat="server" style="display: none;">
                            <asp:DataList ID="dlEmployeeList" Width="100%" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Table" CellPadding="3" GridLines="Both">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                                <SeparatorStyle BackColor="AliceBlue" />
                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <HeaderTemplate>
                                    <table style ="width:100%">
                                        <tr>
                                            <td style="width: 10%; vertical-align: top">
                                                <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="Black"
                                                Text ='<%$Resources:ControlsCommon,slno %>'  Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="width: 30%; vertical-align: top">
                                                <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="Black"
                                                     Text='<%$Resources:ControlsCommon,Employee %>' Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="width:25%; vertical-align: top">
                                                <asp:Label ID="lblTemplateHeader" CssClass="labeltext" runat="server" ForeColor="Black"
                                                    Text='<%$Resources:ControlsCommon,Template %>' Font-Bold="true"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:Label ID="lblEvaluatorHeader" CssClass="labeltext" runat="server" ForeColor="Black"
                                                   Text='<%$Resources:ControlsCommon,Evaluators %>' Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle Font-Bold="true" ForeColor="Black" />
                                <ItemTemplate>
                                    <table  style="width:100%">
                                        <tr>
                                            <td  style="width: 10%;  vertical-align: top">
                                                <asp:Label ID="lblSlNo" CssClass="labeltext" runat="server" ToolTip='<%# Eval("SLNo") %>'
                                                    Text='<%# Eval("SLNo") %>'></asp:Label>
                                            </td>
                                            <td style="width:  30%; vertical-align: top">
                                                <asp:Label ID="lblEmployee" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EmployeeFullName") %>'
                                                    Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                <asp:HiddenField ID="hfEmployeeID" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                                </asp:HiddenField>
                                            </td>
                                            <td style="width: 25%; vertical-align: top; overflow: hidden">
                                                <asp:Label ID="lblTemplate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("TemplateName") %>'
                                                    Text='<%# Eval("TemplateName") %>'></asp:Label>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("EmployeeID") %>'>
                                                </asp:HiddenField>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:Label ID="lblEvaluator" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EvaluatorFullName") %>'
                                                    Text='<%# Eval("EvaluatorFullName") %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:Label ID="lblNoRecord" CssClass="error " runat="server" Text="No employees added for performance initiation."
                                Visible="false"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="peformanceDetailsPager" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div runat="server" style="text-align: center">
            <asp:Label ID="lblNoperformance" CssClass="error " runat="server" Text="No record found."
                Visible="false"></asp:Label>
        </div>
    </div>
    <div id="dvNoPermission" runat="server" style="margin-top: 10px; text-align: center;
        width: 100%">
        <asp:Label ID="NoPermission" runat="server" Text="You dont have permission to view this page"
            CssClass="error"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_initiation.png" />
                </div>
                <div class="name">
                        <h5><asp:LinkButton ID="lnkAdd" runat="server" Text='<%$Resources:ControlsCommon,AddInitiation%>'  CausesValidation="false" OnClick="lnkAdd_Click"> 
                    
                
		
<%--	Add Initiation
--%>	
	</asp:LinkButton></h5>
	
	
	
	
	
                </div>
                <div class="sideboxImage">
                    <img src="../images/view_performance_initiation.png" />
                </div>
                <div class="name">
                   <h5>  <asp:LinkButton ID="lnkViewInitiation" runat="server" Text='<%$Resources:ControlsCommon,ViewInitiation%>'  CausesValidation="false" OnClick="lnkViewInitiation_Click">
		
	<%--View Initiation--%>

	</asp:LinkButton>	
	</h5>
                </div>
                </a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
