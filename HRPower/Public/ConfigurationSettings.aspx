﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="ConfigurationSettings.aspx.cs" Inherits="Public_ConfigurationSettings"
    Title="Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li ><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li class="selected"><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
          
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%; float: left;">
        <div style="width: 100%; float: left;">
            <div style="display: none">
                <asp:Button ID="btnSubmit" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true" SkinID="popup">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <div style="width: 100%; float: left;vertical-align:top">
            <asp:UpdatePanel ID="upConfigurationSettings" runat="server">
                <ContentTemplate>
                    <asp:DataList ID="dlConfigurationSettings" runat="server" Width="100%" DataKeyField="ConfigurationID"
                        CellPadding="0" CellSpacing="0" OnItemCommand="dlConfigurationSettings_ItemCommand"
                        OnSelectedIndexChanged="dlConfigurationSettings_SelectedIndexChanged">
                        <HeaderTemplate>
                            <div style="width: 100%;  float: left;">
                                <div style="width: 98.5%; float: left;" class="datalistheader">
                                    <div style="width: 300px; float: left;">
                                        <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,Item%>'></asp:Literal>
                                    </div>
                                    <div style="float: left;">
                                        <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ControlsCommon,Value%>'></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </HeaderTemplate>
                        <FooterTemplate>
                            <div style="width: 100%; float: left;">
                                <div style="float: left;" class="trLeft">
                                    &nbsp;
                                </div>
                                <div style="float: right;" class="trRight">
                                    <asp:Button ID="btnSave" runat="server" CommandName="SAVE" Width="75px" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Save%>'
                                        CommandArgument="ConfigurationID" ValidationGroup="Configuration" />&nbsp;
                                    <asp:Button ID="btnReset" runat="server" CommandName="RESET" Width="75px" CssClass="btnsubmit"
                                        Text='<%$Resources:ControlsCommon,Reset%>' CommandArgument="ConfigurationID" OnClientClick="return confirm('Are you sure to reset all the configuration items??');" />
                                </div>
                            </div>
                        </FooterTemplate>
                        <ItemTemplate>
                            <div style="width: 100%;">
                                <div class="trLeft" style="width: 300px; float: left;">
                                    <%# Naming(Eval("ConfigurationItem")) %> <%--<%# Naming(Eval("ConfigurationItem")) %>--%>
                                </div>
                                <div class="trRight" style="float: left;">
                                    <asp:TextBox ID="txtConfigurationValue" runat="server" CssClass="textbox" Text='<%# Eval("ConfigurationValue") %>'
                                        Width="290px" MaxLength="100" Enabled='<%# IsEnabled(Eval("Predefined")) %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvtxtConfigurationValue" runat="server" Display="Dynamic"
                                        ControlToValidate="txtConfigurationValue" ErrorMessage='<%# "please enter "+Eval("ConfigurationItem") %>'
                                        SetFocusOnError="True" ValidationGroup="Configuration"></asp:RequiredFieldValidator>
                                   <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtConfigurationValue" TargetControlID="txtConfigurationValue"
                                        runat="server" FilterType="Numbers" Enabled='<%# IsNumeric(Eval("DefaultValue")) %>'>
                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"
                        Visible="False"></asp:Label>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlConfigurationSettings" EventName="ItemCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div style="width: 100%; float: left;" >
        <div style="vertical-align: top; padding: 3px; float: left;width:10%;">
            <img src="../images/Bulbe 14px.png" />
        </div>
        <div style="text-align: justify; float: left; width:85%;" >
            <span class="labeltext">Configuration of software implies controls to certain fields or
                forms in the software, according to which computations will be made and displayed
                by the software.</span>
        </div>
    </div>
    <div style="width: 100%; float: left; padding-top:10px" >
        <div style="float: left; vertical-align: top; padding: 3px; width:10%;">
            <img src="../images/Bulbe 14px.png" />
            </div>
            <div style="text-align: justify; float: left; width:85%;" >
                <span class="labeltext">E.g: If probation period in the company is 30 days then 'probation
                    end date' will be calculated as 30 days after the 'date of joining'.</span>
            </div>
        </div>
</asp:Content>
