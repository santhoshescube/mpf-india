﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="HomeDetailView.aspx.cs" Inherits="Public_HomeDetailView" Title='<%$Resources:MasterPageCommon,Home%>' %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">

    <script src="../css/jquery-1.2.6.min.js" type="text/javascript"></script>
<style>
  #popupmain
  {
  	width: 597px !important;
  }
</style>
    <div id='cssmenu'>
        <ul>
            <li class="selected"><a href='HomeDetailView.aspx'>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DashBoard%>'>
            
                </asp:Literal>
            </a></li>
            <li><a href='HomeRecentActivity.aspx'><span>
                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeActiveVacancies.aspx'><span>
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeNewHires.aspx'><span>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeAlerts.aspx'><span>
                <asp:Literal ID="Literalalerts" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal>
            </span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div id="divHeading" style="float: left; width: 100%; margin-top: 7px;">
        <div id="divLeft" style="float: left; width: 50%; padding-top: 5px">
            <div id="divLeft" style="float: left; width: 30%; text-align: center; font-size: 10px;
                font-weight: bold; font-family: Tahoma;">
                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:MasterPageCommon,Company%>'></asp:Literal></div>
            <div id="divRight" style="float: left; width: 70%">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlCompany" DataTextField="CompanyName" DataValueField="CompanyID"
                            AutoPostBack="true" runat="server" Width="216px" CssClass="dropdownlist_mandatory"
                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div style="float: left;">
            <asp:UpdatePanel ID="updMenu" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:ImageButton runat="server" ID="imgCustomize" ImageUrl="../images/Customize.png"
                        OnClick="lnkMenu_OnClick" />
                    <asp:LinkButton ID="lnkMenu" runat="server" CssClass="customize_link" OnClick="lnkMenu_OnClick"
                        Text='<%$Resources:ControlsCommon,Customize%>' ToolTip='<%$Resources:ControlsCommon,Customize%>'> </asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="padding-left: 710px">
            <asp:ImageButton ImageUrl="../images/graphical5.png" ID="imgDetailView" runat="server"
                ToolTip='<%$Resources:ControlsCommon,GraphicalView%>' OnClick="imgDetailView_Click" />
        </div>
    </div>
    <%-- <div>
    <div style="height: 30px; padding-left: 50px" id="divLeft">
        company</div>
    <div style="height: 30px; padding-left: 120px" id="divRight">
      <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName"
                                            DataValueField="CompanyID" CssClass="dropdownlist" AutoPostBack="True"  >
                                        </asp:DropDownList>
    </div></div>--%>
    <div id="columns">
        <asp:UpdatePanel ID="updMainWidget" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ul id="column1" class="column" style="width: 50%;">
                    <%-----------My Requests-----------%>
                    <li class="widget color-white" runat="server" id="divRequest">
                        <div class="widget-head">
                            <h3>
                                <asp:Literal ID="litRequests" runat="server" Text="My Requests"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divMyRequests" style="max-height: 170px; padding-top: 10px; text-align: left;
                                overflow: auto; margin-left: 5px; display: block">
                                <asp:Repeater ID="rptMyRequests" runat="server" OnItemDataBound="rptMyRequests_ItemDataBound">
                                    <ItemTemplate>
                                        <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                            <tr class="listItem">
                                                <td style="font-size: 12px;">
                                                    <a id="ancMyMessage" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                        <%# Eval("Messages") %></a>
                                                    <asp:ImageButton ID="imgMyRequestDelete" OnClick="imgMyRequestDelete_Click" runat="server"
                                                        ToolTip="Click here to delete request" ImageUrl="~/images/deletemsg.png" />
                                                    <asp:HiddenField ID="hfMyRequestMyRequest" Value='<%#Eval("MessageTypeID")%>' runat="server" />
                                                    <asp:HiddenField ID="hfMessageID" Value='<%#Eval("MessageID")%>' runat="server" />
                                                    <asp:HiddenField ID="hfReadStatus" runat="server" Value='<%#Convert.ToInt32(Eval("ReadStatus"))%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </li>
                    <%-----------My Messages----------------------------%>
                    <li class="widget color-white" runat="server" id="divMessage">
                        <div class="widget-head">
                            <h3>
                                <%-- Messages--%>
                                <asp:Literal ID="litMessages" runat="server" Text=" Messages"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divMessages" style="max-height: 170px; text-align: left; overflow: auto;
                                padding-bottom: 10px; padding-top: 10px; margin-left: 5px; display: block;">
                                <asp:UpdatePanel ID="updMessages" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptMessages" runat="server" OnItemDataBound="rptMessages_ItemDataBound">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td valign="top">
                                                            <asp:LinkButton ID="lnkMessage" class="Dashboradlink" runat="server" ToolTip='<%#GetToolTip(Eval("MessageTypeID")) %>'
                                                                OnClick="lnkMessage_Click">
                                                                <%#Eval("Messages")%>
                                                                (<%#Convert.ToString(Eval("MessageCount"))%>)
                                                                <asp:HiddenField ID="hfStatusID" Value='<%#Eval( "StatusID")%>' runat="server" />
                                                            </asp:LinkButton>
                                                            <asp:ImageButton ID="imgDelete" CssClass="homedeleteicon" runat="server" OnClick="imgDelete_Click"
                                                                ToolTip="Click here to delete message" ImageUrl="~/images/deletemsg.png" />
                                                            <asp:HiddenField ID="hfValues" Value='<%#Eval( "MessageTypeID")%>' runat="server" />
                                                            <asp:HiddenField ID="hfReferenceID" Value='<%#Eval( "MessageTypeID")%>' runat="server" />
                                                            <asp:HiddenField ID="hfReadStatus" Value='<%#Convert.ToInt32(Eval("MessageCount"))- Convert.ToInt32(Eval("ReadCount"))%>'
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                    <%-----------Evaluation----------------------------%>
                    <li class="widget color-white" runat="server" id="divPerEval">
                        <div class="widget-head">
                            <h3>
                                <%-- Performance Evaluation--%>
                                <asp:Literal ID="litPerformance" runat="server" Text="Performance Evaluation"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divEvaluation" style="max-height: 170px; text-align: left; overflow: auto;
                                padding-bottom: 10px; padding-top: 10px; margin-left: 5px; display: block;">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptEvaluation" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td valign="top">
                                                            <asp:LinkButton ID="lnkEvalMessage" class="Dashboradlink" runat="server" ToolTip='<%#GetToolTip(Eval("MessageTypeID")) %>'
                                                                OnClick="lnkEvalMessage_Click">
                                                                <%#Eval("Messages")%>
                                                                <asp:HiddenField ID="hfValues" Value='<%#Eval( "MessageTypeID")%>' runat="server" />
                                                                <asp:HiddenField ID="hfReferenceId" runat="server" Value=' <%#Eval("ReferenceId")%>'>
                                                                </asp:HiddenField>
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                    <%-----------Document Expiry Alerts-----------%>
                    <li class="widget color-white" id="divExpyAlert" runat="server">
                        <asp:UpdatePanel ID="updExpiryAlert" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="widget-head">
                                    <h3>
                                        <asp:Label ID="lblAlert" runat="server" Text=""></asp:Label>
                                    </h3>
                                </div>
                                <div class="widget-content">
                                    <div id="divExpiryAlert" style="max-height: 170px; padding-top: 10px; text-align: left;
                                        overflow: auto; margin-left: 5px; display: block">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <asp:Repeater ID="rptExpiryAlert" runat="server" OnItemDataBound="rptExpiryAlert_ItemDataBound">
                                                    <ItemTemplate>
                                                        <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr class="listItem">
                                                                <td style="font-size: 12px;" align="left">
                                                                    <asp:LinkButton ID="lnkExpiryAlert" runat="server" ToolTip="View Alerts" Text='<%# Eval("DESCRIPTIONS") %>'
                                                                        OnClick="lnkExpiryAlert_OnClick"></asp:LinkButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:HiddenField ID="hfExpiryDocumentTypeID" runat="server" Value=' <%#Eval("DOCUMENTTYPEID")%>'>
                                                        </asp:HiddenField>
                                                        <asp:HiddenField ID="hfExpiryStatus" runat="server" Value=' <%#Eval("sts")%>'></asp:HiddenField>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </li>
                    <%-----------Employee Leave Details-----------------%>
                    <li class="widget color-white" id="divLeave" runat="server">
                        <div class="widget-head">
                            <h3>
                                <asp:Label ID="lblLeave" runat="server" Text=""></asp:Label>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divLeaves" style="max-height: 170px; padding-top: 10px; text-align: left;
                                overflow: auto; margin-left: 5px; display: block">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                  <table border="0" cellpadding="0" cellspacing="0" width="97%" id="Table3" runat="server">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" valign="top" width="100px">
                                                        <asp:TextBox ID="txtLeaveMonth" runat="server" CssClass="textbox_mandatory" Width="90px"
                                                            AutoPostBack="true" onkeypress ="return false;"  onpaste="return false;" OnTextChanged="txtLeaveMonth_TextChanged" MaxLength="10"></asp:TextBox>
                                                             <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Animated="true"
                                                            Format="MMM/yyyy" PopupButtonID="txtLeaveMonth" TargetControlID="txtLeaveMonth" />
                                                    </td>
                                                    
                                                       
                                                   
                                                    <td>
                                                        <asp:DropDownList runat="server" DataValueField="LeaveTypeID" DataTextField="LeaveType"
                                                            ID="ddlLeaveType" AutoPostBack="true" CssClass="dropdown" 
                                                            onselectedindexchanged="ddlLeaveType_SelectedIndexChanged"  >
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                  
                                 
                                        <asp:Repeater ID="rptLeave" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td style="font-size: 12px;">
                                                            <a id="ancLeave" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                                <%# Eval("LeaveMessage")%></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                    <%-----------Daily basis Punching report----------------------------%>
                    <li class="widget color-white" id="divLate" runat="server">
                        <div class="widget-head">
                            <h3>
                                <%-- Todays Late Comers--%>
                                <asp:Literal ID="litTodaysLateComers" runat="server" Text="Todays Late Comers"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divLateComers" style="max-height: 170px; text-align: left; overflow: auto;
                                padding-bottom: 10px; padding-top: 10px; margin-left: 5px; display: block;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptLateComers" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td style="font-size: 12px;">
                                                            <a id="ancLateComers" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                                <%# Eval("EmployeeNumber")%>
                                                                <%# Eval("EmployeeFullName")%>
                                                                - Late By
                                                                <%# Eval("TimeDifference")%></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul id="column3" class="column" style="width: 50%;">
                    <%-----------My Attendance-----------%>
                    <li class="widget color-white" runat="server" id="divMyAttendance">
                        <div class="widget-head">
                            <h3>
                                <%--My Attendance--%>
                                <asp:Literal ID="litAttendance" runat="server" Text="My Attendance"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divAttendance" runat="server" style="max-height: 420px; padding-top: 2px;
                                text-align: left; overflow: auto; margin-left: 0px; display: block">
                                <table border="0" cellpadding="0" cellspacing="0" width="97%" id="Table1" runat="server">
                                    <tr>
                                        <td>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="textbox_mandatory" Width="90px"
                                                            AutoPostBack="true" OnTextChanged="txtDate_TextChanged" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:ImageButton ID="btnDateofBirth" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                            CausesValidation="False" />
                                                        <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Animated="true"
                                                            Format="dd/MM/yyyy" PopupButtonID="btnDateofBirth" TargetControlID="txtDate" />
                                                    </td>
                                                    <td align="right" width="80%" style="padding-left: 5px">
                                                        <asp:Label ID="lblShift" runat="server" Font-Size="12px"></asp:Label>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <div id="div1" style="max-height: 230px; padding-top: 1px; text-align: left; overflow: auto;
                                    margin-left: 0px; display: block">
                                    <table border="0" cellpadding="0" cellspacing="0" width="97%" id="tblRepeater" runat="server">
                                        <tr>
                                            <td class="trRight" style="padding-left: 10px; border: 1px solid rgb(53, 194, 238);
                                                border-radius: 8px;">
                                                <asp:DataList ID="dlAttendanceDetails" runat="server" CellPadding="0" CellSpacing="0"
                                                    Width="100%">
                                                    <HeaderTemplate>
                                                        <div style="float: left; width: 99%;" class="datalistHead">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" width="100">
                                                                        <%--Entry/Exit--%>
                                                                        <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,EntryExit%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%--Punching--%><asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Punching%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%-- Work Time--%><asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,WorkTime%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%--Break Time--%><asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,BreakTime%>'></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr style="font-size: 12px; color: #6a8bab;">
                                                                <td width="100">
                                                                    <b>
                                                                        <%#Eval("EntryExit") %></b>&nbsp;
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("Punching")%>
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("WorkTime").ToString().Trim() == "00:00:00" ? " " : Eval("WorkTime")%>&nbsp;
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("BreakTime").ToString().Trim() == "00:00:00" ? " " : Eval("BreakTime")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <table border="0" cellpadding="0" cellspacing="0" width="97%" id="tblsummary" runat="server"
                                    style="font-size: 12px; display: block; padding-top: 2px;">
                                    <tr>
                                        <td align="left" width="30%">
                                            <%-- Normal Work Time:--%><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,NormalWorkTime%>'></asp:Literal>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblNormalWorktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                        <td align="left" width="5%">
                                        </td>
                                        <td align="left" width="20%">
                                            <%--Work time:--%><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,WorkTime%>'></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblWorktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <%--Allowed Break Time:--%><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,AllowedBreakTime%>'></asp:Literal>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblAllowedBreakTime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                        <td align="left" width="5%">
                                        </td>
                                        <td align="left" width="20%">
                                            <%--Break Time:--%><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,BreakTime%>'></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblBreaktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="97%" id="Table2" runat="server"
                                    style="display: none;">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPolicy" runat="server" Style="font-size: 12px;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblConsequence" runat="server" Style="font-size: 12px;"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblConseq1" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblConseq2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblConseq3" runat="server"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblConseq4" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <%--   <asp:Timer ID="tmAttendance" runat="server" Interval="180000" OnTick="tmAttendance_Tick">
                        </asp:Timer>--%>
                        </div>
                    </li>
                    <%-----------Employee Performance----------------------------%>
                    <li class="widget color-white" id="divPerformance" runat="server">
                        <div class="widget-head">
                            <h3>
                                <asp:Literal ID="litMyPerformance" runat="server" Text="My Performance"></asp:Literal>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="div2" style="max-height: 170px; text-align: left; overflow: scroll; width: 100%">
                                <img id="imgPerformance" style="width: 100%" src="" runat="server" />
                            </div>
                        </div>
                    </li>
                    <%-----------Document Return Alerts-----------%>
                    <li class="widget color-white" runat="server" id="divReturnAlert">
                        <asp:UpdatePanel ID="updAlert" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="widget-head">
                                    <h3>
                                        <asp:Label ID="lblRetAlert" runat="server" Text=""></asp:Label></h3>
                                </div>
                                <div class="widget-content">
                                    <div id="divAlert" style="max-height: 170px; padding-top: 10px; text-align: left;
                                        overflow: auto; margin-left: 5px; display: block">
                                        <asp:Repeater ID="rptAlert" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td style="font-size: 12px;" align="left">
                                                            <asp:LinkButton ID="lnkAlert" runat="server" Text='<%# Eval("DESCRIPTIONS") %>' ToolTip="View Alerts"
                                                                OnClick="lnkAlert_OnClick"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hfDocumentTypeID" runat="server" Value=' <%#Eval("DOCUMENTTYPEID")%>'>
                                                </asp:HiddenField>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </li>
                    <%-----------Vacation leave requests-----------%>
                    <li class="widget color-white" id="divVacLeave" runat="server">
                        <div class="widget-head">
                            <h3>
                                <asp:Label ID="lblVacLeaveRequest" runat="server" Text=""></asp:Label>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divVacLeaveRequest" style="max-height: 170px; padding-top: 10px; text-align: left;
                                overflow: auto; margin-left: 5px; display: block">
                                <asp:UpdatePanel ID="updVacLeave" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptVacLeaveRequest" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td style="font-size: 12px;">
                                                            <a id="ancVacLeaveRequest" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                                <%# Eval("LeaveMessage")%></a>
                                                            <%--  <asp:LinkButton ID="lnkViewMoreVacation" runat ="server"   ToolTip ="View More" 
                                        Text= '<%# Eval("LeaveMessage") %>' OnClick ="lnkViewMoreVacation_OnClick" ></asp:LinkButton>--%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                    <%-----------Vacation Processing -------------%>
                    <li class="widget color-white" id="divVacProcess" runat="server">
                        <div class="widget-head">
                            <h3>
                                <asp:Label ID="lblVacLeaveProcessing" runat="server" Text=""></asp:Label>
                            </h3>
                        </div>
                        <div class="widget-content">
                            <div id="divVacLeaveProcessing" style="max-height: 170px; padding-top: 10px; text-align: left;
                                overflow: auto; margin-left: 5px; display: block">
                                <asp:UpdatePanel ID="updVacProcessing" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Repeater ID="rptVacLeaveProcessing" runat="server">
                                            <ItemTemplate>
                                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr class="listItem">
                                                        <td style="font-size: 12px;">
                                                            <a id="ancVacLeaveProcessing" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                                <%# Eval("LeaveMessage")%></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </li>
                </ul>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%-----------Alert Popup-----------%>
        <asp:UpdatePanel ID="updAlertPopup" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divAlertPopup" runat="server" style="display: none; width: 680px;">
                    <div id="popupmainwrap">
                        <div id="popupmain">
                            <div id="header11">
                                <div id="hbox1">
                                    <div id="headername">
                                        <asp:Label ID="Label1" runat="server" Text='<%$Resources:Requests,Alerts%>'></asp:Label>
                                    </div>
                                </div>
                                <div id="hbox2">
                                    <div id="close1">
                                        <a href="">
                                            <asp:ImageButton ID="imgPopupClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                                CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="contentwrap">
                                <div id="content">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td width="100%">
                                                <asp:UpdatePanel ID="upnlPopup" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <fieldset style="width: 95%; border-color: rgb(10, 194, 252); border-width: 1px;
                                                            border-radius: 10px;">
                                                            <div style="width: 100%; float: left; padding-top: 4px">
                                                                <div style="width: 50%; float: left; padding-top: 4px">
                                                                    <div style="width: 35%; float: left; padding-top: 4px">
                                                                        <%--Document Type--%>
                                                                        <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:DocumentsCommon,DocumentType%>'></asp:Literal>
                                                                    </div>
                                                                    <div style="float: right; padding-top: 4px;">
                                                                        <asp:DropDownList ID="ddlDocType" runat="server" Width="150px" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <asp:HiddenField ID="hfdIsExpiry" runat="server" />
                                                                </div>
                                                                <div style="width: 50%; float: right; padding-top: 4px">
                                                                    <div style="width: 2%; float: left; padding-top: 4px">
                                                                    </div>
                                                                    <div style="width: 10%; float: left; padding-top: 4px">
                                                                        <%--Date--%><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,Dates%>'></asp:Literal>
                                                                    </div>
                                                                    <div style="width: 2%; float: left; padding-top: 4px">
                                                                    </div>
                                                                    <div style="width: 25%; float: left; padding-top: 4px">
                                                                        <asp:TextBox runat="server" ID="txtfrom" Width="85px" AutoPostBack="true" OnTextChanged="txtfrom_TextChanged">
                                                                        </asp:TextBox>
                                                                        <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtfrom"
                                                                            PopupButtonID="txtfrom" Format="dd/MM/yyyy">
                                                                        </AjaxControlToolkit:CalendarExtender>
                                                                    </div>
                                                                    <div style="width: 3%; float: left; padding-top: 4px">
                                                                    </div>
                                                                    <div style="width: 25%; float: left; padding-top: 4px; margin-left: 10px;">
                                                                        <asp:TextBox runat="server" ID="txtTo" Width="85px" AutoPostBack="true" OnTextChanged="txtTo_TextChanged">
                                                                        </asp:TextBox>
                                                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTo"
                                                                            PopupButtonID="txtTo" Format="dd/MM/yyyy">
                                                                        </AjaxControlToolkit:CalendarExtender>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div style="width: 100%; float: left; padding-top: 4px; max-height: 190px; overflow: auto;">
                                                            <asp:DataList ID="dlAlerts" runat="server" BorderWidth="0px" CellPadding="0" CellSpacing="0"
                                                                Width="100%">
                                                                <HeaderTemplate>
                                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td height="30" width="30" valign="top" align="left">
                                                                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectHeaderAll(this.id,'chkAlert')" />
                                                                            </td>
                                                                            <td style="color: rgb(9, 101, 153);" align="left">
                                                                                <%--Message--%><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,Message%>'></asp:Literal>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tr style="font-size: 12px; color: #6a8bab;">
                                                                            <td width="30" style="vertical-align: bottom;">
                                                                                <asp:CheckBox ID="chkAlert" runat="server" />
                                                                                <asp:HiddenField ID="hfdDocTypeID" runat="server" Value='<%# Eval("DocumentTypeID")%>' />
                                                                                <asp:HiddenField ID="hfdCommonID" runat="server" Value='<%# Eval("CommonID")%>' />
                                                                            </td>
                                                                            <td style="padding-top: 8px; color: rgb(9, 101, 153)" align="left">
                                                                                <%# Eval("Message")%>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <table width="100%" style="display: none;">
                                                                        <tr>
                                                                            <td width="25" valign="top" style="padding-left: 5px">
                                                                                <asp:CheckBox ID="chk_FooterAll" runat="server" onclick="selectFooterAll(this.id,'chkAlert')" />
                                                                            </td>
                                                                            <td style="padding-left: 7px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FooterTemplate>
                                                            </asp:DataList>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:UpdatePanel ID="upSubmit" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="100%" style="font-size: 11px" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="left" style="padding-left: 5px;">
                                                                    <asp:Label ID="lblInfoEmployeeAdd" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="footerwrapper">
                                <div id="footer11">
                                    <div id="buttons">
                                        <asp:UpdatePanel ID="upnlAddEmployee" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btnAddEmployee" runat="server" ValidationGroup="AddPhase" OnClick="btnAddEmployee_Click"
                                                    CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,DoNotNotifyAgain%>' />
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" OnClick="btnCancel_Click"
                                                    ToolTip="Cancel" Text='<%$Resources:ControlsCommon,Cancel%>' />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeEmployee" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="divAlertPopup" TargetControlID="btnProxyEmployeePopup" PopupDragHandleControlID="tblDragHandle">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Button ID="btnProxyEmployeePopup" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <%-----------Widget popup-----------%>
        <asp:UpdatePanel ID="upnlMenu" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <div id="divMenu" runat="server" style="display: none;">
                    <div id="popupmainwrap">
                        <div id="popupmain">
                            <div id="header11">
                                <div id="hbox1">
                                    <div id="headername">
                                        <asp:Label ID="Label4" runat="server" Text="Widget"></asp:Label>
                                    </div>
                                </div>
                                <div id="hbox2">
                                    <div id="close1">
                                        <a href="">
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                                CausesValidation="false" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="contentwrap">
                                <div id="content">
                                    <asp:UpdatePanel ID="updWidget" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlMenu"
                                                DataKeyField="WidgetID" Width="100%" runat="server" RepeatColumns="4" BackColor="White"
                                                BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <ItemStyle ForeColor="#000066" />
                                                <SeparatorStyle BackColor="AliceBlue" />
                                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <HeaderTemplate>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 10%; overflow: hidden;">
                                                                <asp:CheckBox CssClass="labeltext" ForeColor="White" AutoPostBack="true" ID="chkSelectAll"
                                                                    OnCheckedChanged="chkSelectAll_CheckedChanged" Height="15px" runat="server" />
                                                            </td>
                                                            <td style="width: 90%; overflow: hidden; vertical-align: top; padding-left: 320px"
                                                                align="left">
                                                                <asp:Label ID="lblHeader" Text="Widget" ForeColor="White" CssClass="labeltext" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </HeaderTemplate>
                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td style="width: 20px; height: 15px; overflow: hidden;">
                                                                <asp:CheckBox ID="chkSelect" Height="15px" runat="server" Checked='<%# Convert.ToBoolean(Eval("Checked")) %>' />
                                                            </td>
                                                            <td style="vertical-align: top; width: 200px; height: 15px; overflow: hidden;" align="left">
                                                                <asp:Label ID="lblWidget" Height="15px" CssClass="labeltext" runat="server" ToolTip='<%# Eval("Widget") %>'
                                                                    Text='<%# Eval("Widget") %>'></asp:Label>
                                                                <asp:HiddenField ID="hfWidgetID" runat="server" Value='<%# Eval("WidgetID") %>'>
                                                                </asp:HiddenField>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div id="footerwrapper">
                                <div id="footer11">
                                    <div id="buttons">
                                        <asp:UpdatePanel ID="updSaveWidget" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btnWidgetSave" runat="server" ValidationGroup="AddPhase" CssClass="btnsubmit"
                                                    Text="Apply" OnClick="btnWidgetSave_Click" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMenu" runat="server" BackgroundCssClass="modalBackground"
                    PopupControlID="divMenu" TargetControlID="btnMenu" PopupDragHandleControlID="tblDragHandle">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Button ID="btnMenu" runat="server" Style="display: none" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div style="width: 98%; border-top: 1px solid #b8b8b8; border-bottom: 1px solid #b8b8b8;
        border-radius: 5px; height: auto; float: left; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
        <div style="margin: auto; text-align: center; font-size: 16px; width: 95%; padding: 5px;
            color: Black">
        </div>
        <div style="margin: auto; width: 99%;">
            <div style="width: 93%; text-align: left;" class="jobhead">
                <%--Calendar--%>
                <div style="display: block; width: 100%;" id="divCalendar">
                    <div style="display: block; width: 100%; padding: 5px 2px 2px 2px; margin-left: 1px;">
                        <asp:UpdatePanel ID="upcalender" runat="server">
                            <ContentTemplate>
                                <asp:Calendar ID="Calendar5" Width="98%" runat="server" CellPadding="1" NextMonthText="&amp;gt;&amp;gt;  "
                                    PrevMonthText="&amp;lt;&amp;lt;" BorderStyle="None" CssClass="CalendarDay">
                                   <SelectedDayStyle ForeColor="Black" BorderColor="#0099FF" />
                                    <TodayDayStyle CssClass="CalendarDay1" BorderColor="#CC0066" />
                                    <DayStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <DayHeaderStyle CssClass="CalendarDay" />
                                    <DayStyle CssClass="CalendarDay1" />
                                    <SelectedDayStyle ForeColor="Red" />
                                    <TitleStyle CssClass="CalendarDay1" />
                                    <NextPrevStyle ForeColor="White" CssClass="CalendarNextPrev" />
                                </asp:Calendar>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div style="float: left; font-size: 10px; padding: 2px 2px 2px 2px; width: 100%;">
                        <img src="../images/green.png" />
                        <%--Event--%>
                        <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,Event%>'></asp:Literal>
                        <img src="../images/red.PNG" />
                        <%--Holiday--%><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,Holiday%>'></asp:Literal>
                        <img src="../images/golden.png" />
                        <%--Both--%>
                        <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Both%>'></asp:Literal>
                    </div>
                </div>
                <%-- *********Job Openings*********--%>
                <div style="width: 98%; border: 1px solid #b8b8b8; border-radius: 5px; height: auto;
                    background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
                    <div style="margin: auto; text-align: center; font-size: 16px; width: 95%; padding: 5px;
                        color: Black">
                        <p>
                            <%-- Job Openings--%>
                            <asp:Literal ID="litJobOpening" runat="server"></asp:Literal>
                        </p>
                    </div>
                    <div style="margin: auto; text-align: center; border-bottom: 2px solid #f6c94e; width: 90%;">
                    </div>
                    <div style="margin: auto; width: 99%;">
                        <div style="width: 97%; text-align: left; overflow: auto; height: 300px;" class="jobhead">
                            <asp:UpdatePanel ID="updjob" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Repeater ID="repJob" runat="server" OnItemDataBound="repJob_ItemDataBound">
                                        <ItemTemplate>
                                            <%#Eval("Job")%>
                                            <asp:HiddenField ID="hfdJobID" Value='<%# Eval("JobID") %>' runat="server" />
                                            <div style="width: 93%; text-align: left;" class="jobsub">
                                                <asp:Repeater ID="repSkills" runat="server">
                                                    <ItemTemplate>
                                                        <div style="width: 90%; text-align: left;">
                                                            <img src="../images/golden.png" />
                                                            &nbsp; <b>
                                                                <%--Last date on--%>
                                                                <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,Lastdateon%>'></asp:Literal>
                                                            </b>
                                                            <%#Eval("Expirydate")%>
                                                        </div>
                                                        <div style="text-align: left; word-break: break-all;">
                                                            <b>
                                                                <%--Skills:--%>
                                                                <asp:Literal ID="litt" runat="server" Text='<%$Resources:ControlsCommon,Skills%>'></asp:Literal>
                                                            </b>
                                                            <%#Eval("Skills")%>
                                                        </div>
                                                        <div style="text-align: left;">
                                                            <b>
                                                                <%-- Vacancies:--%>
                                                                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,Vacancies%>'></asp:Literal>
                                                            </b>
                                                            <%#Eval("RequiredQuota")%>
                                                        </div>
                                                        <div style="text-align: left;">
                                                            <b>
                                                                <%-- Experience:--%>
                                                                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,Experience%>'></asp:Literal>
                                                            </b>
                                                            <%#Eval("Experience")%>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <%-- *********Announcements*********--%>
                <div style="width: 98%; border: 1px solid #b8b8b8; border-radius: 5px; height: auto;
                    margin-bottom: 5%; background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
                    <div style="margin: auto; text-align: center; font-size: 16px; width: 95%; padding: 5px;
                        color: Black">
                        <p>
                            <%--Announcements--%><asp:Literal ID="litt" runat="server" Text='<%$Resources:ControlsCommon,Announcements%>'></asp:Literal>
                        </p>
                    </div>
                    <div style="margin: auto; text-align: center; border-bottom: 2px solid #f6c94e; width: 90%;">
                    </div>
                    <div style="margin: auto; width: 99%;">
                        <div style="width: 90%; text-align: left; overflow: auto; height: 150px;" class="jobhead">
                            <asp:UpdatePanel ID="updAnnouncement" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <marquee direction="up" height="150px" width="100%" id="mqOthersTarget" scrollamount="1"
                                        class="announcement" onmouseover="this.setAttribute('scrollamount',0,0);" onmouseout="this.setAttribute('scrollamount',1,0);">
                                                                        <asp:Repeater ID="rptAnnouncements" runat="server">
                                                                          <ItemTemplate>
                                                                          <asp:HiddenField id="hfReferenceId" runat="server" value=' <%#Eval("ReferenceId")%>'></asp:HiddenField>
                                                                           <asp:HiddenField id="hfMessageTypeId" runat="server" value=' <%#Eval("MessageTypeId")%>'></asp:HiddenField>  
                                                                           <asp:HiddenField id="hfMessageID" runat="server" value=' <%#Eval("MessageID")%>'></asp:HiddenField>                                                                          
                                                                           <%#Eval("Message")%>                                                                      
                                      
                                                                       <br />    <br /> 
                                                                          </ItemTemplate>
                                                                        </asp:Repeater></marquee>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
