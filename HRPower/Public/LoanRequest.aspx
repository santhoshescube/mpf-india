﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="LoanRequest.aspx.cs" Inherits="Public_LoanRequest" Title="Loan Request" %>

<%--Title="Loan Request" --%>
<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">

        function ShowPreviousLoan(id) {

            $(id).slideToggle();

            return false;
        }
    </script>


    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=fvLoanDetails.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Loan Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>
    <style type="text/css">
      select
      {
      	margin-left: 0px!important;
      }
      input[type="text"], input[type="password"], input[type="textarea"]
      {
      	margin-left:-4px!important;
      }
    </style>
    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            Width="22px" style="margin: 5px;" meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <div class="grid">
    <table style="width: 100%" cellpadding="4" cellspacing="0">
        <tr>
            <td style="height: 25px">
                <div style="display: none">
                    <asp:Button ID="btnsubmit" runat="server" meta:resourcekey="submit" /><%--<%# Eval("Reason") %>--%>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FormView ID="fvLoanDetails" runat="server" OnDataBound="fvLoanDetails_DataBound"
                            CssClass="labeltext" Width="100%" CellSpacing="1" OnItemCommand="fvLoanDetails_ItemCommand"
                            DataKeyNames="LoanTypeId,RequestId,StatusId,RequestedTo,Employee">
                            <EditItemTemplate>
                                <div style="width: 100%" runat="server" id="divInsert">
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <%--Loan Type--%>
                                                <asp:Literal runat="server" meta:resourcekey="LoanType"></asp:Literal>
                                            </td>
                                            
                                            <td style="margin-left:-5px!important;" class="trRight" width="75%" valign="top">
                                                <asp:DropDownList ID="rcLoanType" runat="server" CssClass="dropdownlist_mandatory"
                                                    Width="180px" AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvSalutationpnlTab1" runat="server" ControlToValidate="rcLoanType"
                                                    CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseselectloantype" ValidationGroup="Submit"
                                                    InitialValue="-1"></asp:RequiredFieldValidator><%--ErrorMessage="Please select loan type"--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <%--Loan From Date--%>
                                                <asp:Literal runat="server" meta:resourcekey="DeductStartDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtLoanDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    Text='<%# Eval("LoanDate") %>'></asp:TextBox>
                                                <asp:HiddenField ID="hfLoanDate" runat="server" Value='<%# Eval("LoanDate") %>' />
                                                <asp:ImageButton ID="ibtnLoanDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" Style=" padding-top:9px" />
                                                <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtLoanDate"
                                                    PopupButtonID="ibtnLoanDate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtLoanDate"
                                                    Display="Dynamic" ClientValidationFunction="CheckRequestdate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal runat="server" meta:resourcekey="Amount"></asp:Literal><%--Amount--%>
                                                (<asp:Label ID="lblCurrency" runat="server" meta:resourcekey="AED"></asp:Label>)
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtLoanAmount" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    Text='<%# Eval("Amount").ToDecimal().ToString("0.00") %>' MaxLength="9"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvLoanAmount" runat="server" ControlToValidate="txtLoanAmount"
                                                    Display="Dynamic" meta:resourcekey="Pleaseenterloanamount" SetFocusOnError="True"
                                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl" runat="server" CssClass="error" Visible="false"></asp:Label>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmountExtender" runat="server"
                                                    FilterType="Numbers,Custom" TargetControlID="txtLoanAmount" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <%-- Reason--%>
                                                <asp:Literal runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Width="250px" Height="50px" style="margin-left: -4px!important;" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                                                    Display="Dynamic" meta:resourcekey="Pleaseenterreason" SetFocusOnError="True"
                                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter reason."--%>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="trLeft" width="25%">
                                                <%--Status--%>
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    DataValueField="StatusId">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" width="75%" align="right">
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" CssClass="btnsubmit"
                                                    meta:resourcekey="submit" Width="75px" CommandName="Add" UseSubmitBehavior="false"
                                                    CommandArgument='<%# Eval("RequestId") %>' />&nbsp;
                                                <%--Text="Submit"--%>
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                                                    CommandName="CancelRequest" Width="75px" /><%--Text="Cancel"--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divError" runat="server" style="width: 100%; display: none; text-align: center;">
                                    <asp:Label runat="server" Text="Salary structure not set" ForeColor="Red" Font-Size="Medium"
                                        meta:resourcekey="Salarystructurenotset"></asp:Label>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div id="divReportingTo" runat="server" style="display: block;">
                                    <table width="900px" class="tablecss">
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%--Requested By--%>
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee") %>'></asp:Label>
                                            </td>
                                            <%--<td class="trRight" width="5%">
                                                <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                                                    meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                                            </td>--%>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%--Loan Type--%>
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="LoanType"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblLoanType" runat="server" Text='<%# Eval("LoanType") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%--Loan From Date--%>
                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="DeductStartDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblLoanFromDate" runat="server" Text='<%# Eval("LoanFromDate") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                                <%--Amount--%>
                                                (<asp:Label ID="lblViewCurrency" runat="server" meta:resourcekey="AED"></asp:Label>)
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtRptToAmount" runat="server" Text='<%# Eval("Amount").ToDecimal().ToString("0.00") %>'></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmountExt" runat="server" FilterType="Numbers,Custom"
                                                    TargetControlID="txtRptToAmount" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount").ToDecimal().ToString("0.00") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" style="vertical-align: top;" width="25%">
                                                <%--Reason--%>
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%" style="word-break: break-all">
                                                <%--<%# Eval("Reason") %>--%><asp:Label ID="lblSingleReason" runat="server" Text='<%# Eval("Reason") %>'></asp:Label>
                                                <asp:TextBox ID="txtsingleReason" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trForwardedBy" runat="server" visible='<%# GetVisibility(Eval("ForwardedBy"))%>'>
                                            <td class="innerdivheader" width="25%">
                                                <%--Forwarded By--%>
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="ForwardedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <%# Eval("ForwardedBy")%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div runat="server">
                                    <%--style="width: 100%"--%>
                                    <table width="900px" class="tablecss">
                                        <tr>
                                            <td class="innerdivheader" style="width: 16.5%;">
                                                <%--Status--%>
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="60%">
                                                <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    Visible="false" DataValueField="StatusId" onchange="SetDivVisibility(this)">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblStatus" runat="server" Text=' <%# Eval("Status") %>'></asp:Label>
                                                <asp:HiddenField ID="hdIsHigher" runat="server" Value="0" />
                                            </td>
                                            <%--<td class="trRight" width="5%">
                                                <asp:ImageButton ID="imgBack2" runat="server" ImageUrl="~/images/back-button.png"
                                                    meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                                            </td>--%>
                                        </tr>
                                        <tr id="ltReasontd" runat="server">
                                            <td class="innerdivheader" style="width: 20%;">
                                                <asp:Literal ID="ltReason" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" style="width: 75%;">
                                                <%-- <asp:Label ID="lblReason" runat="server" ></asp:Label>--%><%--Text='<%# Eval("Reason") %>'--%>
                                                <asp:TextBox ID="txtEditReason" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                                                    Width="40%" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500" style="margin-left: 0px !important;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trlink">
                                            <td class="innerdivheader" width="50%" colspan="2">
                                                <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                                                    OnClick="lbReason_OnClick"></asp:LinkButton>
                                                <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trReasons">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                                    Width="800px">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                                        <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                                            ItemStyle-Width="300px" />
                                                        <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                                        <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none; width: 100%;" id="trAuthority">
                                            <td class="innerdivheader" style="width: 25%">
                                                <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label>
                                            </td>
                                            <td class="trRight" width="100%">
                                                <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal"
                                                    Width="100%">
                                                    <ItemStyle ForeColor="#307296" Font-Bold="true" />
                                                    <ItemTemplate>
                                                        <asp:Table Width="99%">
                                                            <tr style="width: 99%; color: #307296;">
                                                                <td>
                                                                    <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                                                    <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </asp:Table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="divHigher" runat="server" style="display: none; width: 100%">
                                    <table cellpadding="5" cellspacing="0" width="900px" class="tablecss">
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--Requested By--%>
                                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text='<%# Eval("Employee") %>'></asp:Label>
                                                <asp:HiddenField Value='<%# Eval("EmployeeId") %>' ID="hdEmployeeID" runat="server" />
                                            </td>
                                            <td style="width: 25%">
                                                <%--Loan Type--%>
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="LoanType"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblType" runat="server" Text='<%# Eval("LoanType") %>'></asp:Label>
                                                <asp:HiddenField Value='<%# Eval("LoanTypeId") %>' ID="hdLoanType" runat="server" />
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--Loan number--%>
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Loannumber"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtLoanNumber" MaxLength="15" runat="server"></asp:TextBox>
                                                <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtLoanNumber"
                                                    Display="None" ErrorMessage="Please enter loan number" ValidationGroup="FinalApproval"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td style="width: 25%">
                                                <%--Loan Date--%>
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="LoanDate"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtLoanDate" runat="server"></asp:TextBox>
                                                <asp:ImageButton ID="ibtnLoanDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" Style="padding-top:8px" />
                                                <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtLoanDate"
                                                    PopupButtonID="ibtnLoanDate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--Interest Type--%>
                                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="InterestType"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="ddlInterestType" runat="server" AutoPostBack="true" onchange="SetInterestRate(this);"
                                                    OnSelectedIndexChanged="ddlInterestType_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" meta:resourcekey="None"></asp:ListItem>
                                                    <asp:ListItem Value="2" meta:resourcekey="Simple"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%">
                                                <%--Interest Rate--%>
                                                <asp:Literal ID="Literal13" runat="server" meta:resourcekey="InterestRate"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtInterestRate" MaxLength="4" runat="server" AutoPostBack="True"
                                                    OnTextChanged="txtInterestRate_TextChanged"></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtInterestRate" runat="server"
                                                    FilterType="Numbers,Custom" TargetControlID="txtInterestRate" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                <%-- <asp:RangeValidator ID="rvRate" runat="server" ControlToValidate="txtInterestRate"
                                                    ErrorMessage="Interest rate should be less than 100" MaximumValue="100" MinimumValue="1" Type="Double"
                                                    Display="none" ValidationGroup="FinalApproval"></asp:RangeValidator>--%>
                                                <%--  <asp:RequiredFieldValidator runat="server" Display="None" ID="rfvRate"
                                                    ControlToValidate="txtInterestRate" ErrorMessage="Please enter Interest rate"
                                                    ValidationGroup="FinalApproval"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--No of installments--%>
                                                <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Noofinstallments"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtNoOfInstallments" MaxLength="2" runat="server" AutoPostBack="True"
                                                    OnTextChanged="txtNoOfInstallments_TextChanged"></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtNoOfInstallments" runat="server"
                                                    FilterType="Numbers" TargetControlID="txtNoOfInstallments">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                <%-- <asp:RequiredFieldValidator runat="server" Display="None" ID="rfvtxtNoOfInstallments"
                                                    ControlToValidate="txtNoOfInstallments" ErrorMessage="Please enter number of installments"
                                                    ValidationGroup="FinalApproval"></asp:RequiredFieldValidator>--%>
                                            </td>
                                            <td style="width: 25%">
                                                <%--Amount--%>
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtLoanAmount" runat="server" Text='<%# Eval("Amount").ToDecimal().ToString("0.00") %>'
                                                    MaxLength="7" AutoPostBack="True" OnTextChanged="txtLoanAmount_TextChanged"></asp:TextBox>
                                                <asp:HiddenField ID="hdLoanAmount" runat="server" Value='<%# Eval("Amount").ToDecimal().ToString("0.00") %>' />
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmount" runat="server" TargetControlID="txtLoanAmount"
                                                    FilterType="Numbers,Custom" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtLoanAmount"
                                                    Display="None" ErrorMessage="Please enter Amount" ValidationGroup="FinalApproval"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--Deduct Start Date--%>
                                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="DeductStartDate"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("LoanFromDate") %>'></asp:Label>
                                                <asp:HiddenField ID="hdStartDate" runat="server" />
                                            </td>
                                            <td style="width: 25%">
                                                <%--Deduct End Date--%>
                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="DeductEndDate"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 25%">
                                                <%--Payment Amount--%>
                                                <asp:Literal ID="Literal18" runat="server" meta:resourcekey="PaymentAmount"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:TextBox ID="txtPaymentAmount" runat="server" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width: 25%">
                                                <%--Basic Pay--%>
                                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="BasicPay"></asp:Literal>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblBasicPay" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <%--  <tr class="trLeft">
                                            <td style="width: 25%">
                                                
                                                <asp:Literal ID="Literal29" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td style="width: 25%; word-break: break-all" colspan="2">
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Reason") %>'></asp:Label>
                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>       --%>
                                        <tr>
                                            <td colspan="4">
                                                <asp:UpdatePanel ID="updPreviousLoan" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="lnkLoanDetails" meta:resourcekey="Clickheretoviewpreviousloandetails"
                                                            runat="server" OnClick="lnkLoanDetails_Click"></asp:LinkButton>
                                                        <asp:Button ID="btnTest" runat="server" Visible="false" />
                                                        <AjaxControlToolkit:ModalPopupExtender ID="mpe" runat="server" TargetControlID="btnTest"
                                                            PopupControlID="dvPreviousLoan">
                                                        </AjaxControlToolkit:ModalPopupExtender>
                                                        <div id="dvPreviousLoan" runat="server" style="display: none">
                                                            <asp:Label ID="lblNoLoan" runat="server" meta:resourcekey="Nopreviousloanfound" CssClass="error"></asp:Label>
                                                            <asp:GridView ID="dgvPreviousLoan" runat="server" BackColor="White" BorderColor="#CCCCCC"
                                                                BorderStyle="None" BorderWidth="1px" CellPadding="3">
                                                                <RowStyle ForeColor="#000066" />
                                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr class="trLeft">
                                            <td style="width: 100%; padding: 5px 10px; border: solid 1px black; border-radius: 10px" colspan="4">
                                                <asp:DataList ID="dlInstallments" runat="server" Width="100%" OnItemDataBound="dlInstallments_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft ">
                                                                <td style="width: 35%" class="trleft ">
                                                                    <%--Installment No--%>
                                                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="InstallmentNo"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <%--Installment Date--%>
                                                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="InstallmentDate"></asp:Literal>
                                                                </td>
                                                                <td style="width: 35%" class="trleft ">
                                                                    <%--Amount--%>
                                                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table width="100%">
                                                            <tr style="width: 100%" class="trleft">
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:Label ID="lblInstallmentNumber" runat="server" Text='<%# Eval("InstallmentNo") %>'></asp:Label>
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtDate" runat="server" Text='<%# Eval("InstallmentDate") %>' OnTextChanged="txtDate_TextChanged"
                                                                        AutoPostBack="true"></asp:TextBox>
                                                                    <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtDate"
                                                                        Format="dd/MM/yyyy">
                                                                    </AjaxControlToolkit:CalendarExtender>
                                                                </td>
                                                                <td style="width: 35%" class="trleft">
                                                                    <asp:TextBox ID="txtAmount" runat="server" Text='<%# Eval("Amount").ToDecimal().ToString("0.00") %>'
                                                                        AutoPostBack="true" OnTextChanged="txtAmount_TextChanged"></asp:TextBox>
                                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmount" runat="server" TargetControlID="txtAmount"
                                                                        FilterType="Numbers,Custom" ValidChars=".">
                                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                    <asp:HiddenField ID="hdIntetrestAmount" runat="server" Value='<%# Eval("InterestAmount").ToDecimal().ToString("0.00") %>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="true" />
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <table width="900px" class="tablecss">
                                        <tr>
                                            <td class="trLeft" width="80%">
                                            </td>
                                            <td class="trRight" width="20%">
                                                <asp:Button ID="btnApprove" runat="server" CssClass="btnsubmit" meta:resourcekey="submit"
                                                    CommandArgument=' <%# Eval("StatusId") %>' CommandName="Approve" Visible="false"
                                                    Width="75px" ValidationGroup="FinalApproval" /><%--Text="Submit"--%>
                                                <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                                                    CommandName="Cancel" Visible="false" Width="75px" /><%--Text="Cancel"--%>
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    CommandName="RequestCancel" Visible="false" Width="75px" />
                                                <asp:Button ID="btnRequestForCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    meta:resourcekey="submit" CommandName="RequestForCancel" Visible="false" Width="75px" /><%--Text="Submit"--%>
                                                <asp:CustomValidator ID="cvApprove" runat="server" ValidateEmptyText="true" ControlToValidate="txtLoanDate"
                                                    Display="none" ClientValidationFunction="valLoanDate" ValidationGroup="FinalApproval"
                                                    CssClass="error" ErrorMessage="Loan date should be less than Deduct start date"></asp:CustomValidator>
                                                <asp:CustomValidator ID="cvtxtDate" runat="server" ControlToValidate="txtPaymentAmount"
                                                    Display="none" ClientValidationFunction="valInstallmentDate" ValidationGroup="FinalApproval"
                                                    CssClass="error" ErrorMessage="Installment Date Should be Greater than Previous Installment Date"></asp:CustomValidator>
                                                <asp:ValidationSummary runat="server" ValidationGroup="FinalApproval" ShowMessageBox="true"
                                                    ShowSummary="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlLoanRequest" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="fvLoanDetails" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlLoanRequest" runat="server" Width="100%" DataKeyField="RequestId"
                            OnItemCommand="dlLoanRequest_ItemCommand" OnItemDataBound="dlLoanRequest_ItemDataBound"
                            CssClass="labeltext">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="25" valign="top" style="padding-left: 5px">
                                            <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkLoan')" />
                                        </td>
                                        <td style="padding-left: 7px">
                                            <%--Select All--%>
                                            <asp:Literal ID="Literal22" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField Value='<%# Eval("requestId")%>' ID="hdRequestID" runat="server" />
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server"
                                    onmouseover="showCancel(this);" onmouseout="hideCancel(this);">
                                    <tr class="row">
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr class="rownoborder">
                                                    <td valign="top" rowspan="6" width="25" style="padding-left: 7px">
                                                        <asp:CheckBox ID="chkLoan" runat="server" />
                                                    </td>
                                                    <td width="100%">
                                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                            <tr class="rownoborder">
                                                                <td>
                                                                    <asp:LinkButton ID="lnkLoan" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_View" CausesValidation="False"><%# Eval("Date")%></asp:LinkButton>
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Edit" ImageUrl="~/images/edit.png" meta:resourcekey="Edit" /><%--ToolTip="Edit" --%>
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Cancel" ImageUrl="~/images/cancel.png" meta:resourcekey="Cancel" /><%--ToolTip="Cancel" --%>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding-left: 25px">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <%--Loan Type--%>
                                                                    <asp:Literal ID="Literal22" runat="server" meta:resourcekey="LoanType"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("LoanType")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <%--Loan From Date--%>
                                                                    <asp:Literal ID="Literal23" runat="server" meta:resourcekey="DeductStartDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" width="75%">
                                                                    <%# Eval("LoanFromDate")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="RequestedAmount"></asp:Literal><%--Requested Amount--%>
                                                                    (<asp:Label ID="lblDlCurrency" runat="server" meta:resourcekey="AED"></asp:Label>)
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" width="75%">
                                                                    <%# Eval("Amount").ToDecimal().ToString("0.00")%>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trApproval" style="display: none">
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <%--Approved Amount--%>
                                                                    <asp:Literal ID="Literal25" runat="server" meta:resourcekey="ApprovedAmount"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" width="75%">
                                                                    <%# Eval("ApprovedAmount").ToDecimal().ToString("0.00")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <%--Reason--%>
                                                                    <asp:Literal ID="Literal26" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" style="word-break: break-all" width="75%">
                                                                    <%# Eval("Reason").ToString()%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="20%" valign="top" class="innerdivheader">
                                                                    <%--Status--%>
                                                                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" width="75%">
                                                                    <%# Eval("status")%>
                                                                    <%--  <%# Eval("RequestStatusDate")%> --%>
                                                                    <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="30%" valign="top" class="innerdivheader">
                                                                    <%--Requested To--%>
                                                                    <asp:Literal ID="Literal28" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("Requested")  %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc:Pager ID="LoanRequestPager" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlLoanRequest" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="fvLoanDetails" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add Loan Request_Big.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="RequestLoan">
                            
                               
                                <%--Request Loan--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/Loan_Request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="ViewRequest">
                           
                                <%--View Request--%>
                               
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            &nbsp;<asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="Delete">
                            
                                <%--Delete--%>
                        </h5>
                        </asp:LinkButton>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                        </h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                        
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
