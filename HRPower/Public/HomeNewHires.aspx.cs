﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_HomeNewHires : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            objUserMaster =new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
              int RoleID = objUserMaster.GetRoleId();
              if (RoleID <= 3 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.NewHires))) // 
              {
                  FillCombo();
                  BindNewHires();
              }
              else
              {
                  lblMessage.Style["display"] = "block";
                  lblMessage.Text = clsGlobalization.IsArabicCulture() ? " لا يوجد لديك الإذن" : "You have no permission";
              }

          
        }
    }
    private void FillCombo()
    {
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();

        DataSet dsVacancy = objHomePage.GetAllVacancies(-1, objUserMaster.GetCompanyId());

        ddlHireVacancy.DataSource = dsVacancy;
        ddlHireVacancy.DataBind();
        string sVacancy = clsGlobalization.IsArabicCulture() ? "جميع الوظائف الشاغرة" : "All Vacancies";
        ddlHireVacancy.Items.Insert(0, new ListItem(sVacancy, "-1"));


    }
    protected void dlNewHires_PreRender(object sender, EventArgs e)
    {
        if (dlNewHires.Items.Count == 0)
            pgrNewHires.Visible = false;
        else
            pgrNewHires.Visible = true;
        if (dlNewHires.DataSource == null)
            tblNewHiresHeader.Attributes.Add("style", "display:none");
        else
            tblNewHiresHeader.Attributes.Add("style", "display:block");
    }
    protected void btnNewHires_Click(object sender, EventArgs e)
    {
        ClearLists();

        BindNewHires();
    }
    private void ClearLists()
    {
        dlNewHires.DataSource = null;
        dlNewHires.DataBind();
        ddlHireVacancy.SelectedIndex = 0;

    }


    protected void ddlHireVacancy_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrNewHires.CurrentPage = 0;
        BindNewHires();
    }

    protected void BindNewHires()
    {
        //lblRecentActivities.Text = "New Hires";
        //imgHeading.ImageUrl = "~/images/New Hires_Big.png"

        objHomePage = new clsHomePage();
        objRoleSettings = new clsRoleSettings();
        objUserMaster = new clsUserMaster();
        //bool bHavePermission = objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.View_Candidate);
        //if (bHavePermission)
        //{
            DataSet ds = objHomePage.GetNewHires(pgrNewHires.CurrentPage + 1, pgrNewHires.PageSize, Convert.ToInt32(ddlHireVacancy.SelectedValue));

            if (ds.Tables[0].Rows.Count == 0)
            {
                dlNewHires.ShowHeader = false;
                pgrNewHires.Total = 0;
            }
            else
            {
                dlNewHires.ShowHeader = true;
                pgrNewHires.Total = Convert.ToInt32(ds.Tables[0].Rows[0]["Total"]);
            }

            dlNewHires.DataSource = ds;
            dlNewHires.DataBind();

            if (dlNewHires.Items.Count == 0)
            {
                lblMessage.Style["display"] = "block";
                lblMessage.Text = Convert.ToString(GetLocalResourceObject("Nonewhires.Text"));// "No new hires";
            }
            else
                lblMessage.Style["display"] = "none";
        //}
        //else
        //{
        //    lblMessage.Text = "You have no permission to view new hires";
        //}
    }

    public bool SetVisibility()
    {
        objRoleSettings = new clsRoleSettings();
        objUserMaster = new clsUserMaster();
        if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Make_AsEmployee))
            return true;
        else
            return false;
    }

}
