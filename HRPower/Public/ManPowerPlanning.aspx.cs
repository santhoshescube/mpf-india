﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Public_ManPowerPlanning : System.Web.UI.Page
{
    public List<PlanDetails> PlanDetailsL { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadPlanDetails();           

        }
    }
    protected void rdlDepartmentOrProject_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetDepartmentOrProject(rdlDepartmentOrProject.SelectedIndex == 0 ? eSelectedType.Department : eSelectedType.Project);
    }



    private void SetDepartmentOrProject(eSelectedType Type)
    {
        if (Type == eSelectedType.Department)
        {
            LitProjectOrDepartment.Text = "Department";
        }
        else
        {
            LitProjectOrDepartment.Text = "Project";
        }

        updProjectOrDepartment.Update();
        UpdProjectOrDeparmentDetails.Update();
    }



    private void LoadPlanDetails()
    {
        PlanDetailsL = new List<PlanDetails>();
        PlanDetailsL.Add(new PlanDetails()
        {
            Available = 10,
            DesignationID = -1,
            NewHire = 10,
            PayStructureAmount = 1000,
            PayStructureID = -1
        });

        dlPlanDetails.DataSource = PlanDetailsL;
        dlPlanDetails.DataBind();
          
    }


    private void FillAllDesignation()
    {
        DataTable dt = clsReportCommon.GetAllDesignation();
        dt.Rows.RemoveAt(0);
        dlDesignation.DataSource = dt;
        dlDesignation.DataBind();
    }

    private void FillAllSelectedDesignation()
    {
    }

    private void FillAllDesignation(DropDownList ddl)
    {
        DataTable dt = clsReportCommon.GetAllDesignation();
        dt.Rows[0]["Designation"] = "Select";
        ddl.DataSource = dt;
        ddl.DataTextField = "Designation";
        ddl.DataValueField = "DesignationID";
        ddl.DataBind();
    }

    private enum eSelectedType
    {
        Department = 1,
        Project = 2
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {

    }
    protected void btnList_Click(object sender, EventArgs e)
    {

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {

    }
    protected void btnEmail_Click(object sender, EventArgs e)
    {

    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        ViewState["CurrentDesignationID"] = ((ImageButton)sender).CommandArgument;
        updModalPopUp.Update();
        mdPopUp.Show();

    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }



    private void GetEmployeeAvailabilityDetails()
    {

        DataTable dt = new DataTable();
        dt.Columns.Add("EmployeeName");
        dt.Columns.Add("Details");

        DataRow dr;

        dr = dt.NewRow();
        dr["EmployeeName"] = "Test1";
        dr["Details"] = "No project assigned";
        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["EmployeeName"] = "Test2";
        dr["Details"] = "01-oct-2011 to 01-nov-2014";
        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["EmployeeName"] = "Test3";
        dr["Details"] = "01-dec-2011 to 01-jan-2012";
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["EmployeeName"] = "Test4";
        dr["Details"] = "01-jan-2011 to 01-feb-2014";
        dt.Rows.Add(dr);



        dlAvailableDetails.DataSource = dt;
        dlAvailableDetails.DataBind();

    }
    protected void imgGetAvailableDetails_Click(object sender, ImageClickEventArgs e)
    {
        GetEmployeeAvailabilityDetails();
        //updModalPopUp.Update();
    }
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        DataTable dt;
        dt = new DataTable();
        dt.Columns.Add("DesignationId");
        dt.Columns.Add("Designation");
        DataRow dr;

        foreach (DataListItem i in dlDesignation.Items)
        {
            CheckBox chkSelect = (CheckBox)i.FindControl("chkSelect");
            if (chkSelect != null && chkSelect.Checked)
            {
                HiddenField hfDesignationID = (HiddenField)i.FindControl("hfDesignationID");
                Label lblDesignation = (Label)i.FindControl("lblDesignation");

                if (hfDesignationID != null && lblDesignation != null)
                {

                    dr = dt.NewRow();
                    dr["DesignationID"] = hfDesignationID.Value;
                    dr["Designation"] = lblDesignation.Text;
                    dt.Rows.Add(dr);
                }

            }
        }
     //   FillAllSelectedDesignation(dt);
    }
    protected void dlDesignation_ItemCommand(object source, DataListCommandEventArgs e)
    {

    }
    protected void dlDesignation_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //foreach (DataListItem i in dlSelectedDesignation.Items)
        //{
        //    HiddenField hfDesignationID = (HiddenField)i.FindControl("hfDesignationID");
        //    Label lblBudjet = (Label)i.FindControl("lblBudjet");
        //    Label lblRequired = (Label)i.FindControl("lblRequired");
        //    Label lblAvailable = (Label)i.FindControl("lblAvailable");
        //    Label lblNewHires = (Label)i.FindControl("lblNewHires");
        //    Label lblFromDate = (Label)i.FindControl("lblFromDate");
        //    Label lblToDate = (Label)i.FindControl("lblToDate");
        //    Label lblFromDateL = (Label)i.FindControl("lblFromDateL");
        //    Label lblToDateL = (Label)i.FindControl("lblToDateL");


        //    if (hfDesignationID != null && hfDesignationID.Value == ViewState["CurrentDesignationID"].ToString())
        //    {
        //        if (lblBudjet != null && lblRequired != null && lblAvailable != null && lblNewHires != null && lblFromDate != null && lblToDate != null && lblFromDateL != null && lblToDateL != null)
        //        {
        //            lblBudjet.Text = txtBudjet.Text;
        //            lblAvailable.Text = txtAvailable.Text;
        //            lblRequired.Text = txtRequired.Text;
        //            lblNewHires.Text = txtNewHire.Text;
        //            lblFromDate.Text = txtFromDate.Text;
        //            lblToDate.Text = txtToDate.Text;

        //            lblFromDateL.Visible = lblToDateL.Visible = ((lblFromDate.Text = lblToDate.Text) != string.Empty);
        //        }
        //        break;
        //    }
        //}

        //mdPopUp.Dispose();
        //mdPopUp.Hide();
    }
    protected void lnkSchedule_Click(object sender, EventArgs e)
    {

        DataTable dt;
        dt = new DataTable();
        dt.Columns.Add("DesignationId");
        dt.Columns.Add("Designation");
        DataRow dr;

          /*Commented */ 
        //foreach (DataListItem i in dlSelectedDesignation.Items)
        //{
        //    HiddenField hfDesignationID = (HiddenField)i.FindControl("hfDesignationID");
        //    Label  lblDesignation = (Label)i.FindControl("lblDesignation");
        //    if (hfDesignationID != null && lblDesignation != null)
        //    {

        //        dr = dt.NewRow();
        //        dr["DesignationID"] = hfDesignationID.Value;
        //        dr["Designation"] = lblDesignation.Text;
        //        dt.Rows.Add(dr);
        //    }
        //}


        if (dt.Rows.Count == 0)
        {
            ScriptManager.RegisterClientScriptBlock(upMenu, upMenu.GetType(), new Guid().ToString(), "alert('No designation selected');", true);
            return;
        }
            
        HiringSchedule.LoadHiringSchedule(dt);
        MdHiringSchedule.Show();
        updHiringSchedule.Update();
    }
    protected void dlPlanDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            DropDownList ddlDesignation = ((DropDownList)e.Item.FindControl("ddlDesignation"));
            if (ddlDesignation != null)
            {
                FillAllDesignation(ddlDesignation);

            }



            DropDownList ddlPayStructure = ((DropDownList)e.Item.FindControl("ddlPayStructure"));
            if (ddlPayStructure != null)
            {
               // FillAllDesignation(ddlDesignation);

            }
        }
    }
}



public class PlanDetails
{
    public int DesignationID { get; set; }

    public int NewHire { get; set; }

    public int Available { get; set; }

    public int PayStructureID { get; set; }

    public decimal PayStructureAmount { get; set; }
}

