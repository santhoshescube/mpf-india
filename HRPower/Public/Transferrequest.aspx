<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="Transferrequest.aspx.cs" Inherits="Public_Transferrequest" Title="Transfer Request" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" Runat="Server">
    <div id='cssmenu'>
        <ul>
<li ><a href='EmployeeProfile.aspx'><span>Profile </span></a></li>
            <li><a href='PettyCash.aspx'><span>Petty Cash</span></a></li>
            <li ><a href='Leaverequest.aspx'><span>Leave</span></a></li>
            <li ><a href='Salaryadvancerequest.aspx'><span>Salary Advance </span></a></li>
            <li><a href="Loanrequest.aspx">Loan</a></li>
            <li class='selected'><a href="Transferrequest.aspx">Transfer</a></li>
            <li><a href="LeaveExtensionRequest.aspx">Leave Extension</a></li>
            <li><a href="Documentrequest.aspx">Document</a></li>
             <li  style="display:none"><a href="Visarequest.aspx">Visa</a></li>
            <li><a href="Resignation.aspx">Resignation</a></li>
            <li><a href="TrainingRequest.aspx">Training Request</a></li>        </ul>
    </div>

</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=fvTransferDetails.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Transfer Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>

    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            Width="22px" meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <table style="width: 100%" cellpadding="4" cellspacing="0">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnsubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FormView ID="fvTransferDetails" runat="server" Width="100%" OnDataBound="fvTransferDetails_DataBound"
                            CssClass="labeltext" OnItemCommand="fvTransferDetails_ItemCommand" DataKeyNames="RequestId,StatusId,RequestedTo,TransferFromId,TransferToId,TransferTypeId,Employee">
                            <EditItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%">
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TransferType"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlTransferType" runat="server" CssClass="dropdownlist_mandatory"
                                                    DataTextField="Description" DataValueField="TransferTypeId" AutoPostBack="true"
                                                    OnSelectedIndexChanged="TransferType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="TransferFrom"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:HiddenField ID="hidTransferFrom" runat="server" Value='<%# Eval("TransferFrom") %>' />
                                                <asp:TextBox ID="txtTransferFrom" runat="server" CssClass="textbox_disabled" Width="250"
                                                    Enabled="false" Text='<%# Eval("TransferFrom") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTransferFrom"
                                                    Display="Dynamic" meta:resourcekey="TransferFrom" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlTransferFrom" runat="server" CssClass="dropdownlist_mandatory"
                                                    Visible="false" DataTextField="Description" DataValueField="ProjectId">
                                                </asp:DropDownList>
                                                <%--     <asp:RequiredFieldValidator ID="rfvFromProject" runat="server" ControlToValidate="ddlTransferFrom"
                                                    Display="Dynamic" ErrorMessage="Please select a  project." SetFocusOnError="True"
                                                    CssClass="error"></asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="TransferTo"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlTransferTo" runat="server" CssClass="dropdownlist_mandatory"
                                                    Width="250" DataTextField="Description" DataValueField="Id">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlTransferTo"
                                                    Display="Dynamic" meta:resourcekey="TransferTo" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <%--  Transfer Date--%>
                                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="TransferDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtTransferDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    Text='<%# Eval("TDate") %>'></asp:TextBox>
                                                <asp:ImageButton ID="ibtnTransferDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" style="margin-top:7px;" />
                                                <AjaxControlToolkit:CalendarExtender ID="extenderTransferDate" runat="server" TargetControlID="txtTransferDate"
                                                    PopupButtonID="ibtnTransferDate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <%--  <asp:RequiredFieldValidator ID="rfvTransferdate" runat="server" 
                                                        ControlToValidate="txtTransferDate" Display="Dynamic" 
                                                        ErrorMessage="Please enter transfer date." SetFocusOnError="True" CssClass="error"
                                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>  --%>
                                                <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtTransferDate"
                                                    Display="Dynamic" ClientValidationFunction="Checkdate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <%--  Reason--%>
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                                                    Display="Dynamic" meta:resourcekey="PleaseEnterReason" SetFocusOnError="True"
                                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="trLeft" width="25%">
                                                <%--Status--%>
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    DataValueField="StatusId">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <%--     <tr>
                                            <td class="trLeft" width="25%">
                                                Requested To
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:CheckBoxList ID="cblEmployees" runat="server" AutoPostBack="true" CssClass="text"
                                                    DataTextField="EmployeeName" DataValueField="ParentId">
                                                </asp:CheckBoxList>
                                                <asp:CustomValidator ID="cvValidateAssociates" runat="server" OnServerValidate="cvValidateAssociates_ServerValidate"
                                                    ClientValidationFunction="CheckSelectioninCheckBoxList" Display="Dynamic" CssClass="error"
                                                    ErrorMessage="Please select at least one employee." ValidationGroup="Submit"></asp:CustomValidator>
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" width="75%" align="right">
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" CssClass="btnsubmit"
                                                    Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                                    Width="75px" CommandName="Add" UseSubmitBehavior="false" CommandArgument='<%# Eval("RequestId") %>' />&nbsp;
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandName="CancelRequest"
                                                    Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                                    Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%-- Requested By--%>
                                                <asp:Literal ID="literal12" meta:resourcekey="RequestedBy" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="60%">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Employee") %>'></asp:Label>
                                            </td>
                                            <%-- <td class="trRight" width="5%">
                                                <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                                                    meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                                            </td>--%>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="literal" meta:resourcekey="TransferType" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblTransferType" runat="server" Text='<%# Eval("TransferType") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%--Transfer From Date--%>
                                                <asp:Literal ID="literal7" meta:resourcekey="TransferDate" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblTransferFromDate" runat="server" Text='<%# Eval("TransferDate")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%--  Transfer From--%>
                                                <asp:Literal ID="literal8" meta:resourcekey="TransferFrom" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblTransferFrom" runat="server" Text='<%# Eval("TransferFrom")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%-- Transfer To--%>
                                                <asp:Literal ID="literal9" meta:resourcekey="TransferTo" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Label ID="lblTransferTo" runat="server" Text=' <%# Eval("TransferTo") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%" style="vertical-align: top;">
                                                <%-- Reason--%>
                                                <asp:Literal ID="literal10" Text='<%$Resources:ControlsCommon,Remarks%>' runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%" style="word-break: break-all">
                                                <asp:Label ID="lblReason" runat="server" Text='<%# Eval("Reason") %>'></asp:Label>
                                                <asp:TextBox ID="txtEditReason" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                                                <%--<asp:Label ID="lblReason" runat="server" > <%#ArrangeString(Eval("Reason").ToString(),60) %></asp:Label>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <%-- Status--%>
                                                <asp:Literal ID="literal11" meta:resourcekey="Status" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    Visible="false" DataValueField="StatusId">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblStatus" runat="server" Text=' <%# Eval("Status") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trlink">
                                            <td class="innerdivheader" width="50%" colspan="2">
                                                <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                                                    OnClick="lbReason_OnClick"></asp:LinkButton>
                                                <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trReasons">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                                    Width="800px">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                                        <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                                            ItemStyle-Width="300px" />
                                                        <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                                        <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trAuthority">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                                                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                                                <div id="ddl" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                                                    overflow-x: hidden;" runat="server">
                                                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                                                        <ItemTemplate>
                                                            <asp:Table Width="99%">
                                                                <tr style="width: 99%; color: #307296;">
                                                                    <td>
                                                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </asp:Table>
                                                        </ItemTemplate>
                                                    </asp:DataList></div>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="literal13" meta:resourcekey="RequestedTo" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <%# Eval("Requested") %>
                                            </td>
                                        </tr>
                                        <tr id="trForwardedBy" runat="server" visible='<%# GetVisibility(Eval("ForwardedBy"))%>'>
                                            <td class="innerdivheader" width="25%">
                                                <%-- Forwarded By--%>
                                                <asp:Literal ID="literal14" meta:resourcekey="ForwardedBy" runat="server"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <%# Eval("ForwardedBy")%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:Button ID="btnApprove" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    Width="75px" CommandName="Approve" Visible="false" Text='<%$Resources:ControlsCommon,Submit%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Submit%>' />
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    Width="75px" CommandName="RequestCancel" Visible="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Cancel%>' />
                                                <asp:Button ID="btnRequestForCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    CommandName="RequestForCancel" Visible="false" Width="75px" Text='<%$Resources:ControlsCommon,Submit%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Submit%>' />
                                                <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" Visible="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                                                    ToolTip='<%$Resources:ControlsCommon,Approve%>' CommandName="Cancel" Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvTransferDetails" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="dlTransferRequest" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlTransferRequest" runat="server" Width="100%" DataKeyField="RequestId"
                            OnItemCommand="dlTransferRequest_ItemCommand" OnItemDataBound="dlTransferRequest_ItemDataBound"
                            CssClass="labeltext">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="25" valign="top" style="padding-left: 5px">
                                            <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkTransfer')" />
                                        </td>
                                        <td style="padding-left: 7px">
                                            <%--Select All--%><asp:Literal ID="LiteralSA" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                                <asp:HiddenField Value='<%# Eval("requestId")%>' ID="hdRequestID" runat="server" />
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server"
                                    onmouseover="showCancel(this);" onmouseout="hideCancel(this);">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td valign="top" rowspan="9" width="25" style="padding-left: 7px">
                                                        <asp:CheckBox ID="chkTransfer" runat="server" />
                                                    </td>
                                                    <td width="100%">
                                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkTransfer" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_View" CausesValidation="False">
                                                                             <%# Eval("Date")%>  </asp:LinkButton>
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Edit" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>' />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Cancel" ImageUrl="~/images/cancel.png" ToolTip='<%$Resources:ControlsCommon,Cancel%>' />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding-left: 25px">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%-- Transfer Type--%>
                                                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TransferType"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" width="75%">
                                                                    <%# Eval("TransferType")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%--Transfer Date--%>
                                                                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="TransferDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("TransferDate") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%--  Transfer From--%>
                                                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="TransferFrom"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("TransferFrom") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%--Transfer To--%>
                                                                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TransferTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("TransferTo") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%-- Reason--%>
                                                                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left" style="word-break: break-all">
                                                                    <%#Eval("Reason").ToString() %>
                                                                    <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%-- Status--%>
                                                                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("status") %>
                                                                    <%--  <%# Eval("RequestStatusDate")%> --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="20%" class="innerdivheader">
                                                                    <%-- Requested To--%>
                                                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="5%">
                                                                    :
                                                                </td>
                                                                <td width="75%" valign="top" align="left">
                                                                    <%# Eval("requested") %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc:Pager ID="TransferRequestPager" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlTransferRequest" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="fvTransferDetails" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <img src="../images/Add transfer request.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" meta:resourcekey="RequestTransfer" CausesValidation="false"
                                OnClick="lnkAdd_Click"> 
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <img src="../images/Transfer_request.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" meta:resourcekey="ViewTransfer" CausesValidation="false"
                                OnClick="lnkView_Click">
		
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" meta:resourcekey="Delete" OnClick="lnkDelete_Click"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
