<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="SalaryStructure.aspx.cs" Inherits="Public_SalaryStructure" Title="Salary Structure" %>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>

<script runat="server">

    
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <%-- <ul>
            <li><a href="Employee.aspx"><span>Employee </span></a></li>
            <li class="selected"><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="ViewPolicy.aspx">View Policies</a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
       <%-- </ul>--%>
        <ul>
            <li ><a href='Employee.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li class="selected"><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal51" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeDutyRoaster.aspx">
                <%-- View Policies--%>
               <asp:Literal ID="Literal56" runat="server" Text='<%$Resources:MasterPageCommon,DutyRoaster %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <asp:UpdatePanel ID="upEmployees" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="fvEmployees" runat="server">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="100px" class="firstTrLeft">
                                                    <%--Company--%>
                                                    
                                                    
                                                    <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,Company%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td width="120px">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                        DataValueField="CompanyID" DataTextField="CompanyName" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="9%">
                                                    &nbsp;
                                                </td>
                                                <td width="150px" class="firstTrLeft">
                                                   <%-- Payment Mode--%>
                                                   <asp:Literal ID="Literal2" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentMode%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td class="firstTrRight">
                                                    <asp:UpdatePanel ID="upPaymentMode" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlPaymentMode" runat="server" Width="130px" CssClass="dropdownlist_mandatory"
                                                                AutoPostBack="true" DataValueField="PaymentModeID" DataTextField="Description"
                                                                OnSelectedIndexChanged="ddlPaymentMode_SelectedIndexChanged" Enabled="False">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="firstTrLeft">
                                                 <%--   Employee--%>
                                                 <asp:Literal ID="Literal3" runat ="server" Text ='<%$Resources:ControlsCommon,Employee%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="EmployeeID" DataTextField="EmployeeName" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlCompany" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                        CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseSelectEmployee%>' ValidationGroup="Employee"
                                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="firstTrLeft">
                                                   <%-- Payment Calculation--%>
                                                   <asp:Literal ID="Literal4" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentCalculation%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="upPaymentCalculation" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlPaymentCalculation" runat="server" Width="130px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="PayCalculationTypeID" DataTextField="Description" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlPaymentCalculation_SelectedIndexChanged" Enabled="False">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlPaymentMode" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="firstTrLeft">
                                                   <%-- Currency--%>
                                                   
                                                   <asp:Literal ID="Literal5" runat ="server" Text ='<%$Resources:ControlsCommon,Currency%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlCurrency" runat="server" Width="200px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="CurrencyID" DataTextField="Description">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td class="firstTrLeft">
                                                   <%-- Salary Process Day--%>
                                                   <asp:Literal ID="Literal6" runat ="server" Text ='<%$Resources:ControlsCommon,SalaryProcessDay%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSalaryProcessDay" runat="server" CssClass="dropdownlist_mandatory"
                                                        Width="50px" Enabled="False">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                        <asp:ListItem>13</asp:ListItem>
                                                        <asp:ListItem>14</asp:ListItem>
                                                        <asp:ListItem>15</asp:ListItem>
                                                        <asp:ListItem>16</asp:ListItem>
                                                        <asp:ListItem>17</asp:ListItem>
                                                        <asp:ListItem>18</asp:ListItem>
                                                        <asp:ListItem>19</asp:ListItem>
                                                        <asp:ListItem>20</asp:ListItem>
                                                        <asp:ListItem>21</asp:ListItem>
                                                        <asp:ListItem>22</asp:ListItem>
                                                        <asp:ListItem>23</asp:ListItem>
                                                        <asp:ListItem>24</asp:ListItem>
                                                        <asp:ListItem>25</asp:ListItem>
                                                        <asp:ListItem>26</asp:ListItem>
                                                        <asp:ListItem>27</asp:ListItem>
                                                        <asp:ListItem>28</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdSalaryID" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <%--                                          <tr>
                                                <td valign="top" class="firstTrLeft" >
                                                    Basic Pay
                                                </td>
                                                <td valign="top" class="firstTrRight">
                                                    <asp:UpdatePanel ID="upBasicPay" runat="server" UpdateMode="Always">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtBasicPay" CssClass="textbox_mandatory" MaxLength="12" runat="server"
                                                                onblur='CalculateNetAmount(this.id);' Style="text-align: right;" Width="160px"></asp:TextBox>
                                                            <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtBasicpay" runat="server" FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom" TargetControlID="txtBasicPay" ValidChars=".">
                                                            </AjaxControlToolkit:FilteredTextBoxExtender>
                                                         
                                                        </ContentTemplate>
                                                      </asp:UpdatePanel>
                                                </td>
                                         
                                            </tr>
                                          --%>
                                            <tr>
                                                <%--<td valign="top" class="trLeft">
                                                    &nbsp;
                                                </td>--%>
                                                <td colspan="5" class="firstTrRight" valign="top">
                                                    <div id="div1" class="TabbedPanels" style="font-size: 14px; width: 100%; float: left">
                                                        <ul class="TabbedPanelsTabGroup">
                                                            <li class="TabbedPanelsTabSelected" id="pnlpTab1" onclick="SetSalaryTab(this.id);">
                                                            
                                                            
                                                          <%--  General--%>
                                                               <asp:Literal ID="Literal40" runat ="server" Text ='<%$Resources:ControlsCommon,General%>'>
                                                    </asp:Literal>
                                                            
                                                            
                                                            </li>
                                                            <li class="TabbedPanelsTab" id="pnlpTab2" onclick="SetSalaryTab(this.id);">
                                                            <%--Other Remuneration--%>
                                                            <asp:Literal ID="Literal41" runat ="server" Text ='<%$Resources:ControlsCommon,OtherRenumeration%>'>
                                                    </asp:Literal>
                                                            
                                                            </li>
                                                        </ul>
                                                        <div class="TabbedPanelsContent" id="divpTab1" style="font-size: 13px; width: 100%;
                                                            float: left; height: 200px">
                                                            <div style="width: 100%; float: left">
                                                                <div class="container_head">
                                                                    <table width="100%" style="font-size: 11px">
                                                                        <tr>
                                                                            <td width="160px" align="left" class="labeltext">
                                                                               <%-- Particulars--%>
                                                                               <asp:Literal ID="Literal7" runat ="server" Text ='<%$Resources:ControlsCommon,Particulars%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="80px" class="labeltext">
                                                                               <%-- Category--%>
                                                                               <asp:Literal ID="Literal8" runat ="server" Text ='<%$Resources:ControlsCommon,Category%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="105px" class="labeltext">
                                                                              <%--  Amount--%>
                                                                              <asp:Literal ID="Literal9" runat ="server" Text ='<%$Resources:ControlsCommon,Amount%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td width="130px" align="left" class="labeltext">
                                                                               <%-- Deduction Policy--%>
                                                                               <asp:Literal ID="Literal10" runat ="server" Text ='<%$Resources:ControlsCommon,DeductionPolicy%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td width="25" align="left">
                                                                                <asp:Button ID="btnAllowances" runat="server" CssClass="referencebutton" Text="..."
                                                                                    ToolTip='<%$Resources:ControlsCommon,AddMoreParticulars%>' OnClick="btnAllowances_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="container_content" style="height: 200px; overflow: auto; background-color: White">
                                                                    <asp:UpdatePanel ID="updlDesignationAllowance" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:DataList ID="dlDesignationAllowances" runat="server" Width="96%" BackColor="White"
                                                                                DataKeyField="SalaryDetailID" CssClass="labeltext" OnItemCommand="dlDesignationAllowances_ItemCommand"
                                                                                OnItemDataBound="dlDesignationAllowances_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="150px">
                                                                                                <asp:DropDownList ID="ddlParticulars" runat="server" DataTextField="Description"
                                                                                                    AutoPostBack="true" CssClass="dropdownlist" Width="200" DataValueField="AddDedID"
                                                                                                    OnSelectedIndexChanged="ddlParticulars_SelectedIndexChanged" onchange="chkDuplicateParticulars(this)">
                                                                                                </asp:DropDownList>
                                                                                                <asp:HiddenField ID="hdParticulars" runat="server" Value='<%# Eval("AddDedID") %>' />
                                                                                            </td>
                                                                                            <td width="100px">
                                                                                                <asp:DropDownList ID="ddlCategory" runat="server" DataTextField="Description" AutoPostBack="true"
                                                                                                    CssClass="dropdownlist" Width="100px" DataValueField="CategoryID">
                                                                                                </asp:DropDownList>
                                                                                                <asp:HiddenField ID="hdCategory" runat="server" Value='<%# Eval("CategoryID") %>' />
                                                                                            </td>
                                                                                            <td visible="False" width="5">
                                                                                                <asp:TextBox ID="chkIsAddition" runat="server" CssClass="textbox" MaxLength="12"
                                                                                                    Style="text-align: right;" Visible="False" Width="5px"></asp:TextBox>
                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="chkIsAddition_FilteredTextBoxExtender"
                                                                                                    runat="server" FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="chkIsAddition"
                                                                                                    ValidChars=".">
                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                            </td>
                                                                                            <td width="75px" align="left">
                                                                                                <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                                                    onblur="CalculateSalaryStructure(this.id)" onchange="setVisibility(this);" MaxLength="12"
                                                                                                    Text='<% # Eval("Amount") %>'></asp:TextBox>
                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtAmount" runat="server" FilterMode="ValidChars"
                                                                                                    FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                <asp:HiddenField ID="hdtxtAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                            </td>
                                                                                            <td width="150px" align="left">
                                                                                                <%--   onchange="chkDuplicateDeductionPolicy(this)"--%>
                                                                                                <asp:DropDownList ID="ddlDeductionPolicy" Width="160px" DataTextField="PolicyName"
                                                                                                    DataValueField="DeductionPolicyID" CssClass="dropdownlist" runat="server" AutoPostBack="True"
                                                                                                    OnSelectedIndexChanged="ddlDeductionPolicy_SelectedIndexChanged" onchange="setVisibility2(this);">
                                                                                                </asp:DropDownList>
                                                                                                <asp:HiddenField ID="hdDeductionPolicy" runat="server" Value='<%# Eval("DeductionPolicyID") %>' />
                                                                                            </td>
                                                                                            <td width="15" align="right">
                                                                                                <asp:ImageButton ID="btnRemove" runat="server" ToolTip='<%$Resources:ControlsCommon,Delete %>' SkinID="DeleteIconDatalist"
                                                                                                    CommandArgument='<% # Eval("AddDedID") %>' CommandName="REMOVE_ALLOWANCE" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                            <asp:CustomValidator ID="cvAllowances" runat="server" ClientValidationFunction="validateAllowanceDeduction"
                                                                                ValidationGroup="Employee" Display="Dynamic" Text  ='<%$Resources:ControlsCommon,PleaseSelectParticulars %>' ></asp:CustomValidator>
                                                                            <asp:CustomValidator ID="cvAmount" runat="server" ClientValidationFunction="validateAmount"
                                                                                ValidationGroup="Employee" Display="Dynamic" Text  ='<%$Resources:ControlsCommon,PleaseEnterTheAmountOrSelectDeductionPolicy%>'></asp:CustomValidator>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="TabbedPanelsContent" id="divpTab2" style="display: none; height: 200px;">
                                                            <div style="width: 100%; float: left">
                                                                <div class="container_head">
                                                                    <table width="100%" style="font-size: 11px">
                                                                        <tr>
                                                                            <td width="200px" align="left" class="labeltext">
                                                                               <%-- Particulars--%>
                                                                               <asp:Literal ID="Literal11" runat ="server" Text ='<%$Resources:ControlsCommon,Particulars%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td align="left" width="150px" class="labeltext">
                                                                             <%--   Amount--%>
                                                                             <asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:ControlsCommon,Amount%>'>
                                                    </asp:Literal>
                                                                            </td>
                                                                            <td width="25" align="left">
                                                                                <asp:Button ID="btnOtherRemuneration" runat="server" CssClass="referencebutton" Text="..."
                                                                                  ToolTip='<%$Resources:ControlsCommon,OtherRenumeration%>'     OnClick="btnOtherRemuneration_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div class="container_content" style="height: 200px; overflow: auto; background-color: White">
                                                                    <asp:UpdatePanel ID="updlOtherRemuneration" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:DataList ID="dlOtherRemuneration" runat="server" Width="96%" BackColor="White"
                                                                                DataKeyField="SalaryID" CssClass="labeltext" OnItemCommand="dlOtherRemuneration_ItemCommand"
                                                                                OnItemDataBound="dlOtherRemuneration_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="200">
                                                                                                <asp:DropDownList ID="ddlRemunerationParticulars" runat="server" AutoPostBack="true"
                                                                                                    DataTextField="OtherRemuneration" DataValueField="OtherRemunerationID" CssClass="dropdownlist"
                                                                                                    Width="200" onchange="chkDuplicateRemunerationParticulars(this)">
                                                                                                </asp:DropDownList>
                                                                                                <asp:HiddenField ID="hdRemunerationParticulars" runat="server" Value='<%# Eval("OtherRemunerationID") %>' />
                                                                                            </td>
                                                                                            <td width="145" align="left">
                                                                                                <asp:TextBox ID="txtRemunerationAmount" runat="server" CssClass="textbox" Style="text-align: right;"
                                                                                                    Text='<% # Eval("Amount") %>' MaxLength="12"></asp:TextBox>
                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtRemunerationAmount" runat="server"
                                                                                                    FilterMode="ValidChars" FilterType="Numbers,Custom" TargetControlID="txtRemunerationAmount"
                                                                                                    ValidChars=".">
                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                <asp:HiddenField ID="hdtxtRemunerationAmount" runat="server" Visible="false" Value='<% # Eval("Amount") %>' />
                                                                                            </td>
                                                                                            <td width="15" align="right">
                                                                                                <asp:ImageButton ID="btnRemunerationRemove" runat="server" ToolTip="Delete Remunerationr"
                                                                                                    SkinID="DeleteIconDatalist" CommandName="REMOVE_Remuneration" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </ItemTemplate>
                                                                            </asp:DataList>
                                                                            <asp:CustomValidator ID="cvRemuneration" runat="server" ClientValidationFunction="validateRemuneration"
                                                                                ValidationGroup="Employee" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon, PleaseEnterRemunerationParticulars %>'></asp:CustomValidator>
                                                                            <asp:CustomValidator ID="cvRemunerationAmount" runat="server" ClientValidationFunction="validateRemunerationAmount"
                                                                                ValidationGroup="Employee" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon, PleaseEnterTheRemunerationAmount%>'></asp:CustomValidator>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100px" valign="top" class="trLeft">
                                                    &nbsp;
                                                </td>
                                                <td colspan="4" class="firstTrRight" valign="top">
                                                    <table width="100%" style="padding: 10px;">
                                                        <tr>
                                                            <td width="60px" class="firstTrLeft">
                                                               <%-- Addition--%>
                                                               <asp:Literal ID="Literal13" runat ="server" Text ='<%$Resources:ControlsCommon,Addition%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td width="110px">
                                                                <asp:TextBox ID="txtAdditionAmt" Width="110px" runat="server" CssClass="textbox_disabled"
                                                                    Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td width="70px" class="firstTrLeft">
                                                               <%-- Deduction--%>
                                                               <asp:Literal ID="Literal14" runat ="server" Text ='<%$Resources:ControlsCommon,Deduction%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td width="110px">
                                                                <asp:TextBox ID="txtDeductionAmt" runat="server" CssClass="textbox_disabled" Width="110px"
                                                                    Enabled="False"></asp:TextBox>
                                                            </td>
                                                            <td width="70px" class="firstTrLeft">
                                                               <%-- NetAmount--%>
                                                               <asp:Literal ID="Literal15" runat ="server" Text ='<%$Resources:ControlsCommon,NetAmount%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td width="110px">
                                                                <asp:TextBox ID="txtNetAmount" runat="server" CssClass="textbox_disabled" Width="110px"
                                                                    Enabled="False"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="padding-top: 25px;">
                                                <td class="trLeft" valign="top" width="100px">
                                                   <%-- Remarks--%>
                                                   <asp:Literal ID="Literal16" runat ="server" Text ='<%$Resources:ControlsCommon,Remarks%>'>
                                                    </asp:Literal>
                                                </td>
                                                <td class="firstTrRight" colspan="4" valign="top">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox" onchange="RestrictMulilineLength(this, 200);"
                                                        onkeyup="RestrictMulilineLength(this, 200);" TextMode="MultiLine" Width="98%"
                                                        Height="50px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    &nbsp;
                                                </td>
                                                <td colspan="4" valign="top" class="firstTrRight">
                                                    <table style="font-size: 11px; height: 64px; width: 100%;">
                                                        <tr>
                                                            <td width="80px" class="firstTrLeft">
                                                              <%--  Absent Policy--%>
                                                              <asp:Literal ID="Literal17" runat ="server" Text ='<%$Resources:ControlsCommon,AbsentPolicy%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td align="left" width="120px">
                                                                <asp:UpdatePanel ID="upAbsentPolicy" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlAbsentPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                        Width="150px" DataValueField="AbsentPolicyID">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <%--<td width="50">
                                                                                    <asp:Button ID="btnAbsentPolicyReference" runat="server" Text="..." CssClass="referencebutton"
                                                                                        CausesValidation="False" OnClick="btnAbsentPolicyReference_Click1" />
                                                                                    <asp:Panel ID="pnlAbsentPolicy" runat="server" Style="display: none;">
                                                                                        <uc:AbsentPolicy ID="ApEmployee" runat="server" ModalPopupID="mpeAbsentPolicy" OnUpdate="ApEmployee_Update" />
                                                                                    </asp:Panel>
                                                                                    <asp:Button ID="btnAbsentPolicy" runat="server" Text="Button" Style="display: none;" />
                                                                                    <AjaxControlToolkit:ModalPopupExtender ID="mpeAbsentPolicy" runat="server" BackgroundCssClass="modalBackground"
                                                                                        PopupControlID="pnlAbsentPolicy" TargetControlID="btnAbsentPolicy" />
                                                                                </td>--%>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td width="100px" class="firstTrLeft">
                                                              <%--  Encash Policy--%>
                                                              <asp:Literal ID="Literal18" runat ="server" Text ='<%$Resources:ControlsCommon,EncashPolicy%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td align="left" width="120px">
                                                                <asp:UpdatePanel ID="upEncashPolicy" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlEncashPolicy" Width="150px" runat="server" CssClass="dropdownlist"
                                                                                        DataTextField="Description" DataValueField="EncashPolicyID">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <%--<td width="50">
                                                                                    <asp:Button ID="btnEncashPolicyReference" runat="server" Text="..." CssClass="referencebutton"
                                                                                        CausesValidation="False" CommandName="ENCASH_POLICY" OnClick="btnEncashPolicyReference_Click" />
                                                                                    <asp:Panel ID="pnlEncashPolicy" runat="server" Style="display: none;">
                                                                                        <uc:EncashPolicy ID="EpEmployee" runat="server" ModalPopupID="mpeEncashPolicy" OnUpdate="EpEmployee_Update" />
                                                                                    </asp:Panel>
                                                                                    <asp:Button ID="btnEncashPolicy" runat="server" Text="Button" Style="display: none;" />
                                                                                    <AjaxControlToolkit:ModalPopupExtender ID="mpeEncashPolicy" runat="server" BackgroundCssClass="modalBackground"
                                                                                        PopupControlID="pnlEncashPolicy" TargetControlID="btnEncashPolicy" />
                                                                                </td>--%>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    &nbsp;
                                                </td>
                                                <td colspan="4" class="firstTrRight" valign="top">
                                                    <table width="100%" style="font-size: 11px">
                                                        <tr>
                                                            <td width="80px" class="firstTrLeft">
                                                               <%-- Holiday Policy--%>
                                                               <asp:Literal ID="Literal19" runat ="server" Text ='<%$Resources:ControlsCommon,HolidayPolicy%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td align="left" width="120px">
                                                                <asp:UpdatePanel ID="upHolidayPolicy" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlHolidayPolicy" runat="server" CssClass="dropdownlist" DataTextField="Description"
                                                                                        Width="150px" DataValueField="HolidayPolicyID">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <%--<td width="50">
                                                                                    <asp:Button ID="btnHolidayPolicyReference" runat="server" Text="..." CausesValidation="False"
                                                                                        CommandName="HOLIDAY_POLICY" CssClass="referencebutton" OnClick="btnHolidayPolicyReference_Click" />
                                                                                    <asp:Panel ID="pnlHolidayPolicy" runat="server" Style="display: none;">
                                                                                        <uc:HolidayPolicy ID="HpEmployee" runat="server" ModalPopupID="mpeHolidayPolicy"
                                                                                            OnUpdate="HpEmployee_Update" />
                                                                                    </asp:Panel>
                                                                                    <asp:Button ID="BtnHolidayPolicy" runat="server" Text="Button" Style="display: none;" />
                                                                                    <AjaxControlToolkit:ModalPopupExtender ID="mpeHolidayPolicy" runat="server" BackgroundCssClass="modalBackground"
                                                                                        PopupControlID="pnlHolidayPolicy" TargetControlID="btnHolidayPolicy" />
                                                                                </td>--%>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td valign="top" width="100px" class="firstTrLeft">
                                                              <%--  Overtime Policy--%>
                                                              <asp:Literal ID="Literal20" runat ="server" Text ='<%$Resources:ControlsCommon,OverTimePolicy%>'>
                                                    </asp:Literal>
                                                            </td>
                                                            <td align="left" width="120px">
                                                                <asp:UpdatePanel ID="upOvertimePolicy" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlOvertimePolicy" Width="150px" runat="server" CssClass="dropdownlist"
                                                                                        DataTextField="Description" DataValueField="OTPolicyID">
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <%--<td width="50">
                                                                                    <asp:Button ID="btnOvertimePolicyReference" runat="server" Text="..." CausesValidation="False"
                                                                                        CommandName="OVERTIME_POLICY" CssClass="referencebutton" OnClick="btnOvertimePolicyReference_Click" />
                                                                                    <asp:Panel ID="pnlOvertimePolicy" runat="server" Style="display: none;">
                                                                                        <uc:OvertimePolicy ID="OpEmployee" runat="server" ModalPopupID="mpeOvertimePolicy"
                                                                                            OnUpdate="OpEmployee_Update" />
                                                                                    </asp:Panel>
                                                                                    <asp:Button ID="BtnOvertimePolicy" runat="server" Text="Button" Style="display: none;" />
                                                                                    <AjaxControlToolkit:ModalPopupExtender ID="mpeOvertimePolicy" runat="server" BackgroundCssClass="modalBackground"
                                                                                        PopupControlID="pnlOvertimePolicy" TargetControlID="BtnOvertimePolicy" />
                                                                                </td>--%>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <asp:CustomValidator ID="cvAddAllowances" runat="server" ClientValidationFunction="validateAddAllowanceDeduction"
                                    ValidationGroup="Employee" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseAddBasicPay%>'></asp:CustomValidator>
                            </div>
                            <%--  <fieldset>--%>
                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                    <td class="trRight" style="float: right; padding-top: 20px;">
                                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" Text= '<%$Resources:ControlsCommon,Submit%>' runat="server" ToolTip= '<%$Resources:ControlsCommon,Submit%>'
                                        
                                            CommandName="SUBMIT" ValidationGroup="Employee" OnClick="btnSubmit_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnCancel" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>' runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                            CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                            <%-- </fieldset>--%>
                        </div>
                        <asp:DataList ID="dlEmployees" runat="server" BorderWidth="0px" CellPadding="0" Width="100%"
                            OnItemCommand="dlEmployees_ItemCommand" OnItemDataBound="dlEmployees_ItemBound"
                            CssClass="labeltext" DataKeyField="SalaryID">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td align="center" height="30" width="30">
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkEmployee');" />
                                        </td>
                                        <td style="padding-left: 5px;">
                                           <%-- Select All--%>
                                           <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,SelectAll%>'>
                                                    </asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle CssClass="listItem" />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 4px" id="tblDetails"
                                    runat="server">
                                    <%--onmouseover="showEdit(this);" onmouseout="hideEdit(this);"--%>
                                    <tr>
                                        <td valign="top" width="30">
                                            <asp:CheckBox ID="chkEmployee" runat="server" />
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; margin-top: 5px" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEmployee" CausesValidation="false" runat="server" CommandArgument='<%# Eval("SalaryID")%>'
                                                                        CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("EmployeeFullName")) %>'
                                                                        CssClass="listHeader bold"></asp:LinkButton>
                                                                    <%--[Employee Number---%><%# Eval("EmployeeNumber")%><%--]--%>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <%--<asp:LinkButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                        display: none;" CommandArgument='<%# Eval("SalaryID")%>' CommandName="ALTER">Edit</asp:LinkButton>--%>
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                        display: block;" CommandArgument='<%# Eval("SalaryID")%>' CommandName="ALTER"
                                                                        ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit %>'></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="item_title">
                                                                   <%-- Payment Mode--%>
                                                                   <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentMode%>'>
                                                    </asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("Payclassification")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="item_title" width="150px">
                                                                   <%-- Payment Calculation--%>
                                                                   
                                                                   <asp:Literal ID="Literal21" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentCalculation%>'>
                                                    </asp:Literal>
                                                                   
                                                                </td>
                                                                <td width="5">
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("Paycalculation")%>
                                                                </td>
                                                            </tr>
                                                            <%--    <tr>
                                                                <td class="item_title">
                                                                    BasicPay
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("BasicPay")%>
                                                                </td>
                                                            </tr>--%>
                                                            <tr>
                                                                <td class="item_title">
                                                                   <%-- Currency--%>
                                                                   
                                                                   <asp:Literal ID="Literal22" runat ="server" Text ='<%$Resources:ControlsCommon,Currency%>'>
                                                    </asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("Currency")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:FormView ID="fvSalarystructure" runat="server" BorderWidth="0px" CellPadding="0"
                            Width="100%" CellSpacing="0" CssClass="labeltext" DataKeyNames="SalaryID">
                            <ItemTemplate>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="firstTrLeft" width="150">
                                           <%-- Employee Name--%>
                                           <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:ControlsCommon,Employee%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="firstTrRight" align="left">
                                            &nbsp;<%# Eval("EmployeeFullName")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                           <%-- Payment Mode--%>
                                           <asp:Literal ID="Literal23" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentMode%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("Payclassification")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                          <%--  Payment Calculation--%>
                                          <asp:Literal ID="Literal24" runat ="server" Text ='<%$Resources:ControlsCommon,PaymentCalculation%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("Paycalculation")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                         <%--   Currency--%>
                                         <asp:Literal ID="Literal25" runat ="server" Text ='<%$Resources:ControlsCommon,Currency%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("Currency")%>
                                        </td>
                                    </tr>
                                    <%--        <tr>
                                        <td class="firstTrLeft">
                                            Basic Pay
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("BasicPay")%>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="trLeft">
                                          <%--  Total Additions--%>
                                          
                                           <asp:Literal ID="Literal26" runat ="server" Text ='<%$Resources:ControlsCommon,TotalAdditions%>'>
                                                    </asp:Literal>
                                          
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("TotalAdditions")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                          <%--  Total Deductions--%>
                                          
                                            <asp:Literal ID="Literal27" runat ="server" Text ='<%$Resources:ControlsCommon,TotalDeductions%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("TotDed")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                           <%-- Net Amount--%>
                                           
                                             <asp:Literal ID="Literal28" runat ="server" Text ='<%$Resources:ControlsCommon,NetAmount%>'>
                                                    </asp:Literal>
                                           
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("NetAmount")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="firstTrLeft">
                                           <%-- Remarks--%>
                                             <asp:Literal ID="Literal29" runat ="server" Text ='<%$Resources:ControlsCommon,Remarks%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" style="word-break: break-all" align="left">
                                            &nbsp;<%# Eval("Remarks")%>
                                        </td>
                                    </tr>
                                </table>
                                <h4 style="padding-bottom: 5px;">
                                    <%--Policy Details--%>
                                      <asp:Literal ID="Literal42" runat ="server" Text ='<%$Resources:ControlsCommon,PolicyDetails%>'>
                                                    </asp:Literal>
                                    
                                    </h4>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="trLeft" width="150">
                                          <%--  Absent Policy--%>
                                          
                                            <asp:Literal ID="Literal30" runat ="server" Text ='<%$Resources:ControlsCommon,AbsentPolicy%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("AbsentPolicy")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                          <%--  Encash Policy--%>
                                          
                                            <asp:Literal ID="Literal31" runat ="server" Text ='<%$Resources:ControlsCommon,EncashPolicy%>'>
                                                    </asp:Literal>
                                          
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("EncashPolicy")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                          <%--  Holiday Policy--%>
                                          
                                            <asp:Literal ID="Literal32" runat ="server" Text ='<%$Resources:ControlsCommon,HolidayPolicy%>'>
                                                    </asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("HolidayPolicy")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                           <%-- Overtime Policy--%>
                                           
                                             <asp:Literal ID="Literal33" runat ="server" Text ='<%$Resources:ControlsCommon,OverTimePolicy%>'>
                                                    </asp:Literal>
                                           
                                        </td>
                                        <td class="trRight" align="left">
                                            &nbsp;<%# Eval("OTPolicy")%>
                                        </td>
                                    </tr>
                                    <%--   <tr>
                                        <td class="trLeft">
                                            Incentive Type
                                        </td>
                                        <td align="left" class="trRight">
                                            &nbsp;<%# Eval("IncentivePolicy")%>
                                        </td>
                                    </tr>--%>
                                    <%-- <tr>
                                        <td class="trLeft">
                                            Settlement
                                        </td>
                                        <td align="left" class="trRight">
                                            &nbsp;<%# Eval("SettlementPolicy") %>
                                        </td>
                                    </tr>--%>
                                </table>
                                <h4 style="padding-bottom: 5px;">
                                    <%--Addition/Deduction Details--%>
                                      <asp:Literal ID="Literal43" runat ="server" Text ='<%$Resources:ControlsCommon,AdditionDeductionDetails%>'>
                                                    </asp:Literal>
                                    
                                    </h4>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="150" valign="top" class="firstTrLeft">
                                            &nbsp;
                                        </td>
                                        <td class="firstTrRight" valign="top">
                                            <div>
                                                <table width="100%" style="font-weight: bold;">
                                                    <tr>
                                                        <td width="150" valign="left">
                                                           <%-- Particulars--%>
                                                           
                                                             <asp:Literal ID="Literal34" runat ="server" Text ='<%$Resources:ControlsCommon,Particulars%>'>
                                                    </asp:Literal>
                                                           
                                                           
                                                        </td>
                                                                <td width="100" valign="left">
                                                           <%-- Category--%>
                                                           
                                                             <asp:Literal ID="Literal35" runat ="server" Text ='<%$Resources:ControlsCommon,Category%>'>
                                                    </asp:Literal>
                                                           
                                                           
                                                        </td>
                                                        <td width="100" valign="left">
                                                          <%--  Amount--%>
                                                            <asp:Literal ID="Literal36" runat ="server" Text ='<%$Resources:ControlsCommon,Amount%>'>
                                                    </asp:Literal>
                                                        </td>
                                                        <td width="100" valign="left">
                                                           <%-- Deduction Policy--%>
                                                           
                                                           
                                                            <asp:Literal ID="Literal37" runat ="server" Text ='<%$Resources:ControlsCommon,DeductionPolicy%>'>
                                                    </asp:Literal> 
                                                           
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="container_content" style="height: auto; overflow: auto;">
                                                <asp:UpdatePanel ID="updlAllowance" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DataList ID="dlAllowance" runat="server" Width="96%">
                                                            <ItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="150" valign="left">
                                                                            <%#Eval("AddDed")%>
                                                                        </td>
                                                                           <td width="100" align="left">
                                                                            <%#Eval("Category")%>
                                                                        </td>
                                                                        <td width="100" align="left">
                                                                            <%#Eval("Amount")%>
                                                                        </td>
                                                                        <td width="100" valign="left">
                                                                            <%#Eval("Paypolicy")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <h4 style="padding-bottom: 5px;">
                                   <%-- Remuneration Details--%>
                                    
                                      <asp:Literal ID="Literal44" runat ="server" Text ='<%$Resources:ControlsCommon,RemunerationDetails%>'>
                                                    </asp:Literal>
                                    </h4>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="150" valign="top" class="firstTrLeft">
                                            &nbsp;
                                        </td>
                                        <td class="firstTrRight" valign="top">
                                            <div>
                                                <table width="100%" style="font-weight: bold;">
                                                    <tr>
                                                        <td width="150" valign="left">
                                                           <%-- Particulars--%>
                                                           
                                                             <asp:Literal ID="Literal38" runat ="server" Text ='<%$Resources:ControlsCommon,Particulars%>'>
                                                    </asp:Literal>
                                                           
                                                        </td>
                                                        <td width="100" valign="left">
                                                          <%--  Amount--%>
                                                          
                                                            <asp:Literal ID="Literal39" runat ="server" Text ='<%$Resources:ControlsCommon,Amount%>'>
                                                    </asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="container_content" style="height: auto; overflow: auto;">
                                                <asp:UpdatePanel ID="updlRemuneration" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DataList ID="dlRemuneration" runat="server" Width="96%">
                                                            <ItemTemplate>
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td width="150" valign="left">
                                                                            <%#Eval("Particular")%>
                                                                        </td>
                                                                        <td width="100" align="left">
                                                                            <%#Eval("Amount")%>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:FormView>
                        <uc:Pager ID="pgrEmployees" runat="server" OnFill="Bind" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlEmployees" EventName="ItemCommand" />
                        <%--<asp:AsyncPostBackTrigger ControlID="fvEmployee" EventName="ItemCommand" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPrint" runat="server">
                    <ContentTemplate>
                        <div id="divPrint" runat="server" style="display: none">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div id="search" style="vertical-align: top">
        <div id="text">
        </div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
            TargetControlID="txtSearch" WatermarkText='<%$Resources:ControlsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
        <br />
        <div id="searchimg">
            <a href="">
                <asp:ImageButton ID="ImgSearch" OnClick="btnSearch_Click" ImageUrl="~/images/search.png"
                    runat="server" />
            </a>
        </div>
        <%--    <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
            WatermarkText="Search By" WatermarkCssClass="WmCss">
        </AjaxControlToolkit:TextBoxWatermarkExtender>--%>
    </div>
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/New_Salarystructure_Large.png"
                        runat="server" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkAddEmp" runat="server" OnClick="lnkAddEmp_Click" CausesValidation="false"> 
                          <%--  Add SalaryStructure--%>
                            <asp:Literal ID="Literal50" runat ="server" Text ='<%$Resources:ControlsCommon,AddSalaryStructure %>'>
                            
                            </asp:Literal>
                            
                           </asp:LinkButton> </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/List_salarystructure_Large.png"
                        runat="server" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkListEmp" runat="server" OnClick="btnList_Click" CausesValidation="false"> 
                          <%-- View Salary Structure--%>
                          <asp:Literal ID="Literal49" runat ="server" Text ='<%$Resources:ControlsCommon,ViewSalaryStructure %>'>
                            
                            </asp:Literal>
                             
                          </asp:LinkButton> </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                  <h5>  <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" CausesValidation="false"> 
                           <%--Delete--%>
                     <asp:Literal ID="Literal48" runat ="server" Text ='<%$Resources:ControlsCommon,Delete %>'>
                            
                            </asp:Literal>
                                  
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" CausesValidation="false">
                          <%-- Print--%>
                   <asp:Literal ID="Literal47" runat ="server" Text ='<%$Resources:ControlsCommon,Print %>'>
                            
                            </asp:Literal>
                                   
                          </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                       <h5> <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" CausesValidation="false">
                     
                           <%-- Email--%>
                            
                        <asp:Literal ID="Literal46" runat ="server" Text ='<%$Resources:ControlsCommon,Email %>'>
                            
                            </asp:Literal>
                                
                              </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddiDed" ImageUrl="~/images/addition_deduction.png" runat="server" />
                </div>
                <div class="name">
                     <h5>  <asp:LinkButton ID="lnkAddDed" runat="server" OnClick="lnkAddDed_Click" CausesValidation="false">
                      
                         <%--   Addition Deduction--%>
                            
                            
                            <asp:Literal ID="Literal45" runat ="server" Text ='<%$Resources:ControlsCommon,AdditionDeduction %>'>
                            
                            </asp:Literal>
                            
                             </asp:LinkButton> </h5>
                </div>
                <%--<div class="sideboxImage">
                    <asp:ImageButton ID="imgDedPolicy" ImageUrl="~/images/deduction policy.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkDedPolicy" runat="server" OnClick="lnkDedPolicy_Click">
                         <h5>
                            Deduction Policy</h5>  </asp:LinkButton>
                </div>--%>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgRemuneration" ImageUrl="~/images/remuneration.png" runat="server" />
                </div>
                <div class="name">
                       <h5><asp:LinkButton ID="lnkRemuneration" runat="server" OnClick="lnkRemuneration_Click" CausesValidation="false">
                      
                            <%--Other Remuneration--%>
                            
                            <asp:Literal ID="Lit1" runat ="server" Text ='<%$Resources:ControlsCommon,OtherRenumeration %>'>
                            
                            </asp:Literal>
                            
                            </asp:LinkButton>  </h5>
                </div>
                <%-- <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddPolicy" ImageUrl="../images/E-Mail.png" 
                        runat="server" Width="40px" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkAddPolicy" runat="server" OnClick="lnkAddPolicy_Click">
                         <h5>
                            Addition Policy</h5>  </asp:LinkButton>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlAdditionDeduction" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlAdditionDeduction" runat="server" Style="display: none">
                        <uc:AdditionDeduction ID="ucAdditionDeduction" runat="server" ModalPopupID="mpeAdditionDeduction" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeAdditionDeduction" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlAdditionDeduction" TargetControlID="Button3">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<asp:UpdatePanel ID="upDeductionPolicy" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView2" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button1" runat="server" />
                    </div>
                    <asp:Panel ID="pnlDeductionPolicy1" runat="server" Style="display: none">
                        <uc:DeductionPolicy ID="ucDeductionPolicy1" runat="server" ModalPopupID="mpeDeductionPolicy" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeDeductionPolicy" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlDeductionPolicy1" TargetControlID="Button1">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
