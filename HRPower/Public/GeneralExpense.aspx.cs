﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Collections.Generic;
using HRAutoComplete;
/// <summary>
/// Created By  :   MEGHA
/// Created On  :   18 DEC 2013
/// </summary>
public partial class Public_GeneralExpense : System.Web.UI.Page
{
    #region Declarations

    clsGeneralExpense objGeneralExpense = new clsGeneralExpense();
    clsUserMaster objUserMaster = new clsUserMaster();

    private string CurrentSelectedValue
    {
        get;
        set;
    }
    int ID = 0;
    #endregion

    #region Events

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        Pager.Fill += new controls_Pager.FillPager(BindDetails);
        if (!IsPostBack)
        {
            EnableMenus();//Sets manipulation permissions
            ListExpenses();//Binds all expenses
        }
        ReferenceNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    /// <summary>
    /// For creating a new Expense
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkNewGereneralExpense_Click(object sender, EventArgs e)
    {
        hfmode.Value = "0";
        
        lnkDeleteGereneralExpense.OnClientClick = "return false;";
        ClearControls();
        Bindcombos();
        DivVisibility("Add");       
    }

    /// <summary>
    /// Binds all Expenses onto datalist
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkViewGereneralExpense_Click(object sender, EventArgs e)
    {
        Pager.CurrentPage = 0;
        EnableMenus();
        BindDetails();      
    }

    /// <summary>
    /// Deletes Expenses
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkDeleteGereneralExpense_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = string.Empty;
            if (dlGeneralExpense.Items.Count > 0)//Deleting selected records
            {
                for (int i = 0; i < dlGeneralExpense.Items.Count; i++)
                {
                    CheckBox chkSelect = (CheckBox)dlGeneralExpense.Items[i].FindControl("chkSelect");

                    if (chkSelect == null)
                        continue;
                    if (chkSelect.Checked)
                    {
                        objGeneralExpense.ExpenseID = dlGeneralExpense.DataKeys[dlGeneralExpense.Items[i].ItemIndex].ToInt32();

                        Delete(Convert.ToInt32(objGeneralExpense.ExpenseID));

                    }
                }
            }
            if (ViewState["ExpenseID"].ToInt32() > 0)//deleting a single record
            {
                objGeneralExpense.ExpenseID = ViewState["ExpenseID"].ToInt32();
                Delete(objGeneralExpense.ExpenseID.ToInt32());
            }
            ListExpenses();

        }
        catch { }
    }

    /// <summary>
    /// For binding ExpenseType dropdown based on Company selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        BindExpenseCategory();
       
        objGeneralExpense.CompanyID = ddlCompany.SelectedValue.ToInt32();
        BindCurrency(ddlCompany.SelectedValue.ToInt32());
        DataTable dt = objGeneralExpense.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32());
        if (dt.Rows.Count > 0)
        {
            if (ddlExpenseCategory.SelectedValue == "1")
            {
                ddlExpenseType.DataSource = dt;
                ddlExpenseType.DataValueField = dt.Columns[0].ToString();
                ddlExpenseType.DataTextField = dt.Columns[1].ToString();
                ddlExpenseType.DataBind();
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
            }
            else
            {
                ddlVacancy.DataSource = dt;
                ddlVacancy.DataValueField = dt.Columns[0].ToString();
                ddlVacancy.DataTextField = dt.Columns[1].ToString();
                ddlVacancy.DataBind();
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
            }
        }
        else
        {
            ddlExpenseType.DataSource = dt;
            ddlExpenseType.DataBind();
            ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
            DataTable dtclear = new DataTable();
            ddlVacancy.DataSource = dtclear;
            ddlVacancy.DataBind();
            ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));            
        }
        upnlExpenseType.Update();
        upnlVacancy.Update();
    }    

    /// <summary>
    /// For binding ExpenseType dropdown based on Category selected
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddlExpenseCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");

        if (ddlExpenseCategory.SelectedValue.ToInt32() == -1)
        {
            ddlExpenseType.Enabled = btnExpenseType.Enabled = false;
            ddlVacancy.Enabled = true;
            lblVacancy.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد نوع المصروفات  ") : ("Select Expense Type"); 
        }
        else
        {
            ddlExpenseType.Enabled = true;
            if (ddlExpenseCategory.SelectedValue.ToInt32() == 1)
            {
                lblVacancy.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد نوع المصروفات  ") : ("Select Expense Type"); 
                btnExpenseType.Enabled = ddlExpenseType.Enabled = true;
                ddlVacancy.Enabled = false;
                rfvExpenseType.ValidationGroup = "submit";
                rfvExpenseType.ErrorMessage = "*";
                rfvVacancy.ValidationGroup = "";
                rfvVacancy.ErrorMessage = "";
            }
            else
            {
                lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                btnExpenseType.Enabled = ddlExpenseType.Enabled = false;
                ddlVacancy.Enabled = true;
                rfvVacancy.ValidationGroup = "submit";
                rfvVacancy.ErrorMessage = "*";
                rfvExpenseType.ValidationGroup = "";
                rfvExpenseType.ErrorMessage = "";
            }

            objGeneralExpense.CompanyID = ddlCompany.SelectedValue.ToInt32();
            DataTable dt = objGeneralExpense.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32());
            if (dt.Rows.Count > 0)
            {
                if (ddlExpenseCategory.SelectedValue.ToInt32() == 1)
                {
                    ddlExpenseType.DataSource = dt;
                    ddlExpenseType.DataValueField = dt.Columns[0].ToString();
                    ddlExpenseType.DataTextField = dt.Columns[1].ToString();
                    ddlExpenseType.DataBind();
                    ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
                    ddlVacancy.DataSource = null;
                    ddlVacancy.DataBind();
                }
                else
                {
                    ddlVacancy.DataSource = dt;
                    ddlVacancy.DataValueField = dt.Columns[0].ToString();
                    ddlVacancy.DataTextField = dt.Columns[1].ToString();
                    ddlVacancy.DataBind();
                    ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
                    ddlExpenseType.DataSource = null;
                    ddlExpenseType.DataBind();
                }
            }
            else
            {
                DataTable dtclear = new DataTable();
                ddlExpenseType.DataSource = dtclear;
                ddlExpenseType.DataBind();
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));
                ddlVacancy.DataSource = dtclear;
                ddlVacancy.DataBind();
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));
            }
            upnlExpenseType.Update();
            upnlVacancy.Update();
        }
    }

    /// <summary>
    /// Loads the reference control for General ExpenseType
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnExpenseType_Click(object sender, EventArgs e)
    {
        if (ddlExpenseType != null)
        {
            ReferenceNew1.ClearAll();
            ReferenceNew1.TableName = "HRExpenseTypeReference";
            ReferenceNew1.DataTextField = "ExpenseType";
            ReferenceNew1.DataTextFieldArabic = "ExpenseTypeArb";
            ReferenceNew1.DataValueField = "ExpenseTypeID";
            ReferenceNew1.FunctionName = "FillExpenseType";
            ReferenceNew1.SelectedValue = ddlExpenseType.SelectedValue;
            ReferenceNew1.DisplayName = GetLocalResourceObject("ExpenseType.Text").ToString();
            ReferenceNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    /// <summary>
    /// Save
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validateData())
        {
            InsertUpdateGeneralExpense();
            lnkViewGereneralExpense_Click(null, null);
        }
    }

    /// <summary>
    /// Clears controls and binds datalist
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearControls();
        int rowcount = objGeneralExpense.GetTotalRecordCount(objUserMaster.GetCompanyId());
        if (rowcount > 0)
            lnkViewGereneralExpense_Click(new object(), new EventArgs());
        else
            lnkNewGereneralExpense_Click(new object(), new EventArgs());
    }

    protected void dlGeneralExpense_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;

        objGeneralExpense.ExpenseID = Convert.ToInt32(e.CommandArgument);
        ViewState["ExpenseID"] = Convert.ToInt32(e.CommandArgument);
        switch (e.CommandName)
        {
            case "VIEW":
                DataTable dtView = objGeneralExpense.GetExpense();
                if (dtView.Rows.Count > 0)
                    dlGeneralExpenseView.DataSource = dtView;
                dlGeneralExpenseView.DataBind();

                lnkDeleteGereneralExpense.Enabled = ViewState["IsDelete"].ToBoolean();
                lnkDeleteGereneralExpense.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد من حذف هذه الوظيفة ؟');") : ("return confirm('Are you sure to delete this ?');");
                DivVisibility("SingleView");
                
                break;

            case "EDIT":       //binds data onto controls for updations

                if (ViewState["IsUpdate"].ToBoolean())
                {
                    EditExpense(objGeneralExpense.ExpenseID.ToInt32());
                    lnkDeleteGereneralExpense.Enabled = false;
                    lnkDeleteGereneralExpense.OnClientClick = "return false";
                    hfmode.Value = "1";
                    DivVisibility("Edit");
                    
                }
                break;
        }
    }

    protected void dlGeneralExpense_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        lnkDeleteGereneralExpense.OnClientClick = "return valDeleteDatalist('" + dlGeneralExpense.ClientID + "');";

    }

    #endregion

    #region Methods

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }

    private void ClearControls()
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");

        DataTable dtclear = new DataTable();
        ddlExpenseType.DataSource = dtclear;
        ddlExpenseType.DataBind();
        ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));

        ddlVacancy.DataSource = dtclear;
        ddlVacancy.DataBind();
        ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));

        ddlExpenseCategory.DataSource = dtclear;
        ddlExpenseCategory.DataBind();
        ddlExpenseCategory.Items.Insert(0, new ListItem(str, "-1"));

        lblVacancy.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد نوع المصروفات  ") : ("Select Expense Type"); 
        ddlCompany.SelectedIndex = ddlExpenseCategory.SelectedIndex = ddlExpenseType.SelectedIndex = ddlVacancy.SelectedIndex = -1;
        txtExpenseDate.Text = txtExpenseNumber.Text = txtRemarks.Text = lblCurrency.Text = string.Empty;
        rfvVacancy.ValidationGroup = rfvExpenseType.ValidationGroup = "";
        txtExpenseAmount.Text = "0";
        btnExpenseType.Enabled = false;
        ViewState["ExpenseID"] = null;
    }

    private void Bindcombos()
    {
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");

        //Binds company details
        ddlCompany.DataSource = clsVacancyAddEdit.GetActiveVacanciesCompany(objUserMaster.GetCompanyId());//objGeneralExpense.BindCompany();
        ddlCompany.DataBind();
        ddlCompany.Items.Insert(0, new ListItem(str, "-1"));        
    }

    private void BindExpenseCategory()
    {
        //Binds Expense Categories
        string str;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlExpenseCategory.DataSource = objGeneralExpense.BindExpenseCategory();
        ddlExpenseCategory.DataBind();
        ddlExpenseCategory.Items.Insert(0, new ListItem(str, "-1"));
    }  

    public void FillExpenseType()
    {
        objGeneralExpense.CompanyID = ddlCompany.SelectedValue.ToInt32();
        DataTable dtTemp = objGeneralExpense.BindExpenseType(ddlExpenseCategory.SelectedValue.ToInt32());

        if (dtTemp.Rows.Count > 0)
        {
            if (ddlExpenseCategory.SelectedValue == "1")
            {
                lblVacancy.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد نوع المصروفات  ") : ("Select Expense Type"); 
                ddlExpenseType.DataSource = dtTemp;
                ddlExpenseType.DataTextField = dtTemp.Columns[1].ToString();
                ddlExpenseType.DataValueField = dtTemp.Columns[0].ToString();
                ddlExpenseType.DataBind();
                string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                ddlExpenseType.Items.Insert(0, new ListItem(str, "-1"));

                this.SetSelectedIndex(this.ddlExpenseType);
                upnlExpenseType.Update();
            }
            else
            {
                lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                ddlVacancy.DataSource = dtTemp;
                ddlVacancy.DataTextField = dtTemp.Columns[1].ToString();
                ddlVacancy.DataValueField = dtTemp.Columns[0].ToString();
                ddlVacancy.DataBind();
                string str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                ddlVacancy.Items.Insert(0, new ListItem(str, "-1"));

                this.SetSelectedIndex(this.ddlVacancy);
                upnlVacancy.Update();
            }
        }
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    private bool validateData()
    {
        bool isValidate = true;
        string msg = string.Empty;

        objGeneralExpense.ExpenseAmount = Convert.ToDecimal(txtExpenseAmount.Text);
        objGeneralExpense.ExpenseNumber = txtExpenseNumber.Text;
        objGeneralExpense.ExpenseID = ViewState["ExpenseID"].ToInt32();
        
        if (ddlExpenseCategory.SelectedValue == "1")
        {
            if (ddlExpenseType.SelectedIndex == 0)
            {
                rfvExpenseType.ValidationGroup = "submit";
                rfvExpenseType.ErrorMessage = "*";
                rfvExpenseType.Visible = true;
                rfvVacancy.ValidationGroup = "";
                rfvVacancy.ErrorMessage = "";
                isValidate = false;
                upnlExpenseType.Update();
                upnlVacancy.Update();
            }
            else
             objGeneralExpense.ExpenseTypeID = ddlExpenseType.SelectedValue.ToInt32();
        }
        else
        {
            if (ddlVacancy.SelectedIndex == 0)
            {
                rfvVacancy.ValidationGroup = "submit";
                rfvVacancy.Visible = true;
                isValidate = false;
                rfvVacancy.ErrorMessage = "*";
                rfvExpenseType.ValidationGroup = "";
                rfvExpenseType.ErrorMessage = "";
                upnlVacancy.Update();
                upnlExpenseType.Update();
            }
            else
                objGeneralExpense.ReferenceID = ddlVacancy.SelectedValue.ToInt32();
        }
      
        if (objGeneralExpense.CheckExpenseNumberDuplication() > 0)
        {
            isValidate = false;
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("عدد حساب موجود") : ("Expense Number Exists");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
        return isValidate;
        upnlAdd.Update();
    }

    private void InsertUpdateGeneralExpense()
    {
        string msg = string.Empty;

        objGeneralExpense.CompanyID = ddlCompany.SelectedValue.ToInt32();
        objGeneralExpense.ExpenseCategoryID = ddlExpenseCategory.SelectedValue.ToInt32();
        if (ddlExpenseCategory.SelectedValue == "1")
            objGeneralExpense.ExpenseTypeID = ddlExpenseType.SelectedValue.ToInt32();
        else
            objGeneralExpense.ReferenceID = ddlVacancy.SelectedValue.ToInt32();
        objGeneralExpense.ExpenseNumber = txtExpenseNumber.Text;
        objGeneralExpense.ExpenseDate = clsCommon.Convert2DateTime(txtExpenseDate.Text.Trim()).ToString("dd MMM yyyy");
        objGeneralExpense.ExpenseAmount = Convert.ToDecimal(txtExpenseAmount.Text);
        objGeneralExpense.Remarks = txtRemarks.Text;
        objGeneralExpense.CreatedBy = Convert.ToInt64(objUserMaster.GetEmployeeId());

        if (hfmode.Value == "0")
        {
            ID = objGeneralExpense.AddUpdateGeneralExpense();
            if (ID > 0)
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحفوظة بنجاح") : ("Successfully Saved");

        }
        else
        {
            objGeneralExpense.ExpenseID = ViewState["ExpenseID"].ToInt32();
            objGeneralExpense.AddUpdateGeneralExpense();
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث بنجاح") : ("Successfully Updated");
        }
        mcMessage.InformationalMessage(msg);
        mpeMessage.Show();
    }

    private void EnableMenus()
    {
        int RoleID = objUserMaster.GetRoleId();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.GeneralExpense);
        if (RoleID > 4)
        {
            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = lnkNewGereneralExpense.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = lnkViewGereneralExpense.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = lnkDeleteGereneralExpense.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();                                
            }
        }
        else
        {
            lnkNewGereneralExpense.Enabled = lnkViewGereneralExpense.Enabled = lnkDeleteGereneralExpense.Enabled = true;
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }

        if (lnkDeleteGereneralExpense.Enabled)
            lnkDeleteGereneralExpense.OnClientClick = "return valDeleteDatalist('" + dlGeneralExpense.ClientID + "');";
        else
            lnkDeleteGereneralExpense.OnClientClick = "return false;";

        upnlViewAll.Update();
        upnlSingleViewExpense.Update();
    }

    private void ListExpenses()
    {
        int count = objGeneralExpense.GetTotalRecordCount(objUserMaster.GetCompanyId());
        if (count > 0)
        {
            DivVisibility("ViewAll");
            
            lnkDeleteGereneralExpense.OnClientClick = "return true;";
            lnkDeleteGereneralExpense.Enabled = ViewState["IsDelete"].ToBoolean();

            if (ViewState["IsView"].ToBoolean())
            {
                BindDetails();
                Pager.Visible = true;
            }
            else
            {
                dlGeneralExpense.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
               
                DivVisibility("NoData");
            }
        }
        else
        {
            if (!ViewState["IsView"].ToBoolean())
            {
                lnkNewGereneralExpense.Enabled = lnkViewGereneralExpense.Enabled = lnkDeleteGereneralExpense.Enabled = dlGeneralExpense.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
                DivVisibility("NoData");
                lnkDeleteGereneralExpense.OnClientClick = "return false;";
            }
            else if (ViewState["IsCreate"].ToBoolean())
                lnkNewGereneralExpense_Click(null, null);
            else
            {
                dlGeneralExpense.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("NoRecordsFound.Text").ToString();

                DivVisibility("NoData");
            }
        }
    }

    private void BindDetails()
    {
        try
        {
            objGeneralExpense.PageIndex = Pager.CurrentPage + 1;
            objGeneralExpense.PageSize = Pager.PageSize;
            DataTable dt = objGeneralExpense.GetAllExpenses(objUserMaster.GetCompanyId());

            if (dt.Rows.Count == 0)
            {
                dlGeneralExpense.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("NoRecordsFound.Text").ToString();
                DivVisibility("NoData");               
            }
            else
            {

                dlGeneralExpense.Visible = Pager.Visible = true;
                Pager.Total = objGeneralExpense.GetTotalRecordCount(objUserMaster.GetCompanyId());
                divNoData.Style["display"] = "none";
                DivVisibility("ViewAll");

                dlGeneralExpense.DataSource = dt;
                dlGeneralExpense.DataBind();
            }

            upnlViewAll.Update();
            upnlAdd.Update();
            upnlNoData.Update();
            upnlSingleViewExpense.Update();
        }
        catch (Exception ex)
        { }
    }

    private void BindCurrency(int CompanyID)
    {
        string Currency = objGeneralExpense.GetCurrency(CompanyID);
        lblCurrency.Text = "( " + Currency + ")";
    }

    private void EditExpense(int ExpenseID)
    {
        try
        {            
            ViewState["ExpenseID"] = ExpenseID;
            objGeneralExpense.ExpenseID = ExpenseID;
            DataTable dt = objGeneralExpense.GetExpense();
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["ExpenseTypeID"].ToInt32() > 0)
                    objGeneralExpense.ExpenseTypeID = dt.Rows[0]["ExpenseTypeID"].ToInt32();
                else
                    objGeneralExpense.ExpenseTypeID = dt.Rows[0]["ReferenceID"].ToInt32();
                Bindcombos();
                BindExpenseCategory();
                ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
                BindCurrency(ddlCompany.SelectedValue.ToInt32());
                ddlExpenseCategory.SelectedValue = dt.Rows[0]["ExpenseCategoryID"].ToString();
                FillExpenseType();
                if (dt.Rows[0]["ExpenseTypeID"].ToInt32() > 0)
                {
                    ddlExpenseType.SelectedValue = dt.Rows[0]["ExpenseTypeID"].ToString();
                    lblVacancy.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد نوع المصروفات  ") : ("Select Expense Type"); 
                    ddlVacancy.Enabled = false;
                }
                else
                {
                    ddlExpenseType.Enabled = false;
                    btnExpenseType.Enabled = false;
                    ddlVacancy.Enabled = true;
                    ddlVacancy.SelectedValue = dt.Rows[0]["ReferenceID"].ToString();
                    lblVacancy.Text = ddlExpenseCategory.SelectedItem.Text;
                }
               
                txtExpenseNumber.Text = dt.Rows[0]["ExpenseNumber"].ToString();
                txtExpenseDate.Text = dt.Rows[0]["ExpenseDate"].ToString();
                txtExpenseAmount.Text = dt.Rows[0]["ExpenseAmount"].ToString();
                txtRemarks.Text = dt.Rows[0]["remarks"].ToString();
            }
            upnlAdd.Update();
        }
        catch { }
    }

    private void Delete(int ExpenseID)
    {
        string msg = string.Empty;
        if (objGeneralExpense.DeleteExpense())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحذوفة بنجاح") : ("Deleted successfully");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
    }

    public void DivVisibility(string Val)
    {
        if (Val == "Add" || Val == "Edit")
        {
            divNoData.Style["display"] = "none";
            divAdd.Style["display"] = "block";
            divViewAll.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "none";
        }
        else if (Val == "SingleView")
        {
            divNoData.Style["display"] = "none";
            divAdd.Style["display"] = "none";
            divViewAll.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "block";
        }
        else if (Val == "ViewAll")
        {
            divNoData.Style["display"] = "none";
            divAdd.Style["display"] = "none";
            divViewAll.Style["display"] = "block";
            divSingleViewExpense.Style["display"] = "none";
        }
        else if (Val == "NoData")
        {
            divNoData.Style["display"] = "block";
            divAdd.Style["display"] = "none";
            divViewAll.Style["display"] = "none";
            divSingleViewExpense.Style["display"] = "none";
        }
        upnlViewAll.Update();
        upnlAdd.Update();
        upnlNoData.Update();
        upnlSingleViewExpense.Update();
    }

    #endregion

    //protected void ln_click(object sender, EventArgs e)
    //{
    //    if (ge != null && mdlPopUpReference1 != null)
    //    {
    //        ge.BindDataList();
    //        mdlPopUpReference1.Show();
    //        UpdatePanel2.Update();
    //    }
    //}
    
}
