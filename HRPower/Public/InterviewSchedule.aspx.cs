﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Collections.Specialized;
using System.Text;
using System.Globalization;
using System.Threading;

public partial class Public_InterviewSchedule : System.Web.UI.Page
{
    clsInterviewSchedule objInterviewSchedule;
    clsCommon objCommon;
    clsUserMaster objUserMaster;
    clsMailSettings objMailSettings;
    clsRoleSettings objRoleSettings;
   
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Title = GetLocalResourceObject("ScheduleInterview.Title").ToString();

        pgSchedule.Fill += new controls_Pager.FillPager(BindSchedules );
        if (!IsPostBack)
        {
            objCommon = new clsCommon();
            objRoleSettings = new clsRoleSettings();
            objUserMaster = new clsUserMaster();

            //txtInterviewDate.Text = Convert.ToDateTime(objCommon.GetSysDate()).ToString("dd/MM/yyyy");
            string strCurrentTime = objCommon.GetSysTime();
            string s = strCurrentTime.Substring(strCurrentTime.Length - 2, 2);
            txtInterviewTime.Text = strCurrentTime.Substring(0, strCurrentTime.LastIndexOf(s)) + " " + s;
            txtDuration.Text = "00:00";

            FillCombo();
            FillInterviewer(0);

            EnableMenus();
            ViewSchedules();
        }
        SetTiming();
    }


    public void EnableMenus()
    {
        objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.InterviewSchedule);
           
            if (dtm.Rows.Count > 0)
            {
                lnkSchedule.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                lnkScheduleList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsPrintEmail"] = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {
            lnkSchedule.Enabled = lnkScheduleList.Enabled = true;
            ViewState["IsDelete"] = ViewState["IsPrintEmail"] =  ViewState["IsUpdate"] = true;
        }

    }

    private void SetTiming()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "InterviewTime", "$(function () { $('#" + txtInterviewTime.ClientID + "').timeEntry(); });", true);

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Duration", "$(function () { $('#" + txtDuration.ClientID + "').timeEntry({show24Hours:true}); });", true);
    }

    private void FillInterviewer(long  JobScheduleID )
    {
        objInterviewSchedule = new clsInterviewSchedule();
        objUserMaster = new clsUserMaster();
        objInterviewSchedule.JobScheduleID = JobScheduleID;
        objInterviewSchedule.JobID = ddlJob.SelectedValue.ToInt32();
        objInterviewSchedule.JobLevelID = ddlJobLevel.SelectedValue.ToInt32();
        objInterviewSchedule.CompanyID = chkEmployees.Checked == true ? -1 : objUserMaster.GetCompanyId();
        chlInterviewer.DataSource = objInterviewSchedule.FillInterviewers();
        chlInterviewer.DataBind();
        updInterviewer.Update();
    }

    private void FillCombo()
    {
        objInterviewSchedule = new clsInterviewSchedule();
        objUserMaster = new clsUserMaster();

        objInterviewSchedule.JobScheduleID = hfdJobScheduleID.Value.ToInt64();
        objInterviewSchedule.CompanyID = objUserMaster.GetCompanyId();
        ddlJob.DataSource = objInterviewSchedule.FillJobes();
        ddlJob.DataBind();
        ddlJob.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

    }

    private void BindSchedules()
    {
      
        divSearch.Style["display"] = "block";
        divViewSchedule.Style["display"] = "block";
        objInterviewSchedule = new clsInterviewSchedule();
        objUserMaster = new clsUserMaster();

        objInterviewSchedule.PageIndex = pgSchedule.CurrentPage + 1;
        objInterviewSchedule.PageSize = pgSchedule.PageSize;

        //objInterviewSchedule.SortExpression = "InterviewScheduleId";
        //objInterviewSchedule.SortOrder = "desc";
        //objInterviewSchedule.ScheduledById = objUserMaster.GetEmployeeId();
        //objInterviewSchedule.Searchkey = txtSearch.Text;

        if (txtScheduleSearchDate.Text != "")
            objInterviewSchedule.ScheduleDate = clsCommon.Convert2DateTime(txtScheduleSearchDate.Text.Trim()).ToString("dd MMM yyyy");
        else
            objInterviewSchedule.ScheduleDate = null;
        objInterviewSchedule.CompanyID = objUserMaster.GetCompanyId();
        DataSet ds = objInterviewSchedule.GetAllInterviewSchedules();

        if (ds.Tables[0].Rows.Count != 0)
        {

            pgSchedule.Total = objInterviewSchedule.GetRecordCount();
            divNoCandidates.Style["display"] = "none";
            dlViewSchedules.DataSource = ds;
            dlViewSchedules.DataBind();
             pgSchedule.Visible = true;
            //lblNodata.Text = string.Empty;

        }
        else
        {
            divNoCandidates.Style["display"] = "block";
            lblNoCandidates.Text = clsGlobalization.IsArabicCulture() ? "لا جداول" : "No Schedules";
            dlViewSchedules.DataSource = null;
            dlViewSchedules.DataBind();
            pgSchedule.Visible = false;

            // lblNodata.Text = "No interviews scheduled yet";
        }
    }

    private void FillCandidates()
    {
        try
        {
            if (ddlJob.SelectedValue.ToInt32() <=0)
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يرجى تحديد أي وظيفة" : "Please select any Job");
                this.mpeMessage.Show();
                //ScriptManager.RegisterClientScriptBlock(updCandidates, updCandidates.GetType(), new Guid().ToString(), "alert('Please select any Job');", true);
                //return;
            }


            foreach (DataListItem item in dlCandidates.Items)
            {
                TextBox txtTime = (TextBox)item.FindControl("txtTime");
                ScriptManager.RegisterClientScriptBlock(dlCandidates, dlCandidates.GetType(), "Time", "$(function () { $('#" + txtTime.ClientID + "').timeEntry(); });", true);

            }
           

                objInterviewSchedule = new clsInterviewSchedule();
                objInterviewSchedule.JobID = ddlJob.SelectedValue.ToInt32();
                objInterviewSchedule.JobScheduleID = hfdJobScheduleID.Value.ToInt64();
                objInterviewSchedule.JobLevelID = ddlJobLevel.SelectedValue.ToInt32() <= 0 ? 0 : ddlJobLevel.SelectedValue.ToInt32();
                objInterviewSchedule.MaximumCandidates = txtResumeQuota.Text.ToInt16();

                DataTable dt = objInterviewSchedule.FillCandidates();

                dlCandidates.DataSource = dt;
                dlCandidates.DataBind();
                if (dt.Rows.Count > 0)
                {
                    divNoCandidate.Style["display"] = "none";
                    divCandidate.Style["display"] = "block";

                }
                else
                {
                    divNoCandidate.Style["display"] = "block";
                    divCandidate.Style["display"] = "none";
                }
           
        }
        catch (Exception ex)
        {

        }
    }

    private void ViewSchedules()
    {
        objRoleSettings = new clsRoleSettings();
        objUserMaster = new clsUserMaster();

        divAddSchedule.Style["display"] = "none";
        divViewSchedule.Style["display"] = "block";
        if (objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.InterviewSchedule, "IsView"))
        {
            BindSchedules();
            upSchedule.Update();
            upMenu.Update();
        }
        else
        {
            divNoCandidates.Style["display"] = "block";
            lblNoCandidates.Text = clsGlobalization.IsArabicCulture() ? "لا يوجد لديك الإذن لعرض جداول" : "You have no Permission to view schedules";
            pgSchedule.Visible = false;
            divSearch.Style["display"] = "none";
        }
    }

    private void PrintSchedules(DataListCommandEventArgs e)
    {
        objInterviewSchedule = new clsInterviewSchedule();
        objInterviewSchedule.JobScheduleID = Convert.ToInt32(e.CommandArgument);
        DataSet dsPrint = objInterviewSchedule.PrintSchedule();

        StringBuilder sb = new StringBuilder();
        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:13px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='left'> " +GetLocalResourceObject("InterviewSchedule.Text").ToString()  + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' cellpadding='6'cellspacing ='2' style='font-size:11px;font-family:Tahoma' border='0px'>");
        sb.Append("<tr><td style='width: 25%'> " + GetLocalResourceObject("ScheduleTitle.Text").ToString() + " </td><td style='width: 5%'> :</td><td style='width: 70%' align='left'>" + dsPrint.Tables[0].Rows[0]["ScheduleTitle"].ToString() + " </td></tr>");
        sb.Append("<tr><td style='width: 25%'> " + GetLocalResourceObject("JobType.Text").ToString() + " </td><td style='width: 5%'> :</td><td style='width: 70%' align='left'>" + dsPrint.Tables[0].Rows[0]["Level"].ToString() + " </td></tr>");
        sb.Append("<tr><td> " + GetLocalResourceObject("InterviewDate.Text").ToString() + "  </td><td> :</td><td>" + dsPrint.Tables[0].Rows[0]["ScheduleDate"].ToString() + " </td></tr>");
        sb.Append("<tr><td> " + GetLocalResourceObject("Time.Text").ToString() + "   </td><td> :</td><td>" + dsPrint.Tables[0].Rows[0]["ScheduleStartTime"].ToString() + " </td></tr>");
        sb.Append("<tr><td>  " + GetLocalResourceObject("Candidates.Text").ToString() + " </td><td> :</td><td>" + dsPrint.Tables[0].Rows[0]["maximumCandidates"].ToString() + " </td></tr>");
        sb.Append("<tr><td>  " + GetLocalResourceObject("Interviewers.Text").ToString() + " </td><td> :</td><td>" + dsPrint.Tables[0].Rows[0]["Interviewers"].ToString() + " </td></tr>");
        sb.Append("<tr><td>  " + GetLocalResourceObject("CandidatesProcessed.Text").ToString() + "</td><td> :</td><td>" + dsPrint.Tables[0].Rows[0]["ProcessCount"].ToString() + "</td></tr></table>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='left'>" + GetLocalResourceObject("CandidateList.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td><table width='100%' cellspacing='0' cellpadding='5' style='font-family:Tahoma;font-size:13px;' border='0'>");

        sb.Append("<tr style='background-color:#17acf6; font-weight:bold;color:White'><td style='width: 10%'>" + GetLocalResourceObject("SlNo.Text").ToString() + "</td><td style='width: 35%'> " + GetLocalResourceObject("CandidateName.Text").ToString() + " </td><td style='width: 20%'> " + GetLocalResourceObject("Time.Text").ToString() + "</td></tr>");
        for (int i = 0; i < dsPrint.Tables[1].Rows.Count; i++)
        {
            sb.Append("<tr><td>" + (i + 1).ToString() + "</td><td> " + dsPrint.Tables[1].Rows[i]["NameEng"].ToString() + "</td><td>" + dsPrint.Tables[1].Rows[i]["time"].ToString() + "</td></tr>");
        }

        sb.Append("</table></td></tr></table>");

        Table tblSchedule = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        td.Text = sb.ToString();
        tblSchedule.ID = "tblPrintSchedule";

        tr.Cells.Add(td);
        tblSchedule.Rows.Add(tr);
        tblSchedule.Width = Unit.Percentage(100);
        tblSchedule.Attributes.Add("Style", "Display:none;");
        pnlPrint.Controls.Add(tblSchedule);

        ScriptManager.RegisterStartupScript(pnlPrint, pnlPrint.GetType(), "Print", "Print('" + tblSchedule.ClientID + "')", true);



    }

    private void RemoveScheduleDetails(long JobScheduleID)
    {
        try
        {
            objInterviewSchedule = new clsInterviewSchedule();
            objInterviewSchedule.JobScheduleID = JobScheduleID;
            objInterviewSchedule.DeleteSchedule();
        }
        catch (Exception ex)
        {

        }

    }

    private void EditScheduleDetails(long JobScheduleID, string ScheduleDate)
    {
        int iProcessCount = 0;
        try
        {


            objInterviewSchedule = new clsInterviewSchedule();
            ViewState["JobScheduleID"] = JobScheduleID;
            objInterviewSchedule.JobScheduleID = JobScheduleID;
            objInterviewSchedule.ScheduleDate = ScheduleDate;
            using (DataSet ds = objInterviewSchedule.FillJobSchedules())
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    divAddSchedule.Style["display"] = "block";
                    divViewSchedule.Style["display"] = "none";


                    DataRow dr = ds.Tables[0].Rows[0];

                    hfdJobScheduleID.Value = Convert.ToString(dr["JobscheduleID"]);
                    FillCombo();
                    
                    ddlJob.SelectedValue = Convert.ToString(dr["JobID"]);
                    FillJobDetails();
                    FillJobLevels();
                    hfdIsBorad.Value = Convert.ToString(clsInterviewSchedule.IsBoard(ddlJob.SelectedValue.ToInt32()));
                    if ( dr["JobLevelID"].ToInt32() > 0)
                    ddlJobLevel.SelectedValue = Convert.ToString(dr["JobLevelID"]) ;
                    txtInterviewDate.Text = dr["ScheduleDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtInterviewTime.Text = Convert.ToString(dr["ScheduleStartTime"]);
                    txtDuration.Text = Convert.ToString(dr["Duration"]);
                    txtVenue.Text = Convert.ToString(dr["Venue"]);
                    txtResumeQuota.Text = Convert.ToString(dr["MaximumCandidates"]);

                    iProcessCount = dr["ProcessCount"].ToInt32();

                    FillInterviewer(hfdJobScheduleID.Value.ToInt64());

                    DataTable dt1 = ds.Tables[1];
                    if (dt1.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt1.Rows)
                        {
                            if (chlInterviewer.Items.FindByValue(Convert.ToString(row["InterviewerID"])) != null)
                            {
                                chlInterviewer.Items.FindByValue(Convert.ToString(row["InterviewerID"])).Selected = true;
                                chlInterviewer.Items.FindByValue(Convert.ToString(row["InterviewerID"])).Enabled = !(Convert.ToBoolean(row["IsExists"]));

                            }
                        }
                    }

                    chlInterviewer.Enabled = iProcessCount <= 0;
                    ddlJob.Enabled = ddlJobLevel.Enabled =txtInterviewDate.Enabled =ibtnInterviewDate.Enabled =  iProcessCount <= 0;


                    DataTable dt2 = ds.Tables[2];
                    if (dt2.Rows.Count > 0)
                    {
                        divNoCandidate.Style["display"] = "none";
                        divCandidate.Style["display"] = "block";

                        dlCandidates.DataSource = dt2;
                        dlCandidates.DataBind();
                    }
                    else
                    {

                        divNoCandidate.Style["display"] = "block";
                        divCandidate.Style["display"] = "none";
                    }

                }
                else
                {
                    ClearFields();

                }
            }


            upSchedule.Update();
        }
        catch (Exception ex)
        {

        }

    }

    private void ClearFields()
    {
        hfdJobScheduleID.Value = "0";
        // ddlJob.SelectedIndex = -1;
        objCommon = new clsCommon();

        string strCurrentTime = objCommon.GetSysTime();
        string s = strCurrentTime.Substring(strCurrentTime.Length - 2, 2);
        txtInterviewTime.Text = strCurrentTime.Substring(0, strCurrentTime.LastIndexOf(s)) + " " + s;
        txtVenue.Text = string.Empty;
        txtResumeQuota.Text = string.Empty;
        txtDuration.Text = "00:00";
        chlInterviewer.Enabled = true;
        ddlJob.Enabled = ddlJobLevel.Enabled = txtInterviewDate.Enabled = ibtnInterviewDate.Enabled = true;
        ddlJob.SelectedIndex = -1;
        ddlJobLevel.SelectedIndex = -1;
        FillInterviewer(0);
        divCandidate.Style["display"] = "none";
        dlCandidates.DataSource = null;
        dlCandidates.DataBind();

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtInterviewDate.Text != "" && lblExpiryDate.Text != "" && clsCommon.Convert2DateTime(txtInterviewDate.Text.Trim()).ToString("dd MMM yyyy").ToDateTime().Date > lblExpiryDate.Text.ToDateTime().Date)
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "يرجى التحقق من تاريخ صلاحية الوظيفة" : "Please check Job validity date");
                this.mpeMessage.Show();
                return;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "InterviewDate", "alert('Please check Job validity date ');", true);
                //return;
            }

            objInterviewSchedule = new clsInterviewSchedule();
            objUserMaster = new clsUserMaster();
            objMailSettings = new clsMailSettings();

            long ScheduleID = 0;
            int CandidateCount = 0;

            string sCandidateIDs = string.Empty;
            string sInterviewerIDs = string.Empty;
            string sScheduleTimings = string.Empty;
            string sStartTime = string.Empty;
            sStartTime = txtInterviewTime.Text;
            // Candidates and Time
            foreach (DataListItem item in dlCandidates.Items)
            {
                CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                TextBox txtTime = (TextBox)item.FindControl("txtTime");
                Label lblNameEng = (Label)item.FindControl("lblNameEng");

                if (chkSelect == null)
                    continue;
                if (chkSelect.Checked)
                {
                    if (ChckTimeDuplication(txtTime.Text, dlCandidates.DataKeys[item.ItemIndex].ToInt64()))
                    {
                        if (clsGlobalization.IsArabicCulture())
                        {
                            mcMessage.InformationalMessage("وجود جدول زمني للمرشح لل نفس الوقت");
                        }
                        else
                        {
                            mcMessage.InformationalMessage("Schedule exists for the candidate -" + lblNameEng.Text.Trim() + " for the same time");
                        }
                            this.mpeMessage.Show();
                        return;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Candidate", "alert('Schedule exists for the candidate -"+ lblNameEng.Text.Trim() + " for the sme time');", true);
                        //return;
                    }
                    sCandidateIDs += "," + Convert.ToString(dlCandidates.DataKeys[item.ItemIndex]);
                    sScheduleTimings += "," + txtTime.Text;
                    if (CandidateCount ==0) sStartTime = txtTime.Text ==string.Empty? txtInterviewTime.Text : txtTime.Text ;
                    CandidateCount += 1;
                    continue;
                }
            }

            if (CandidateCount == 0)
            {
                mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "الرجاء اختيار المرشحين" : "Please select Candidates");
                this.mpeMessage.Show();
                return;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Candidates", "alert('Please select Candidates ');", true);
                //return;
            }



            // Interviewers
            foreach (ListItem item in chlInterviewer.Items)
            {
                if (item.Selected)
                {
                    sInterviewerIDs += "," + Convert.ToString(item.Value);
                }
            }

            objInterviewSchedule.JobScheduleID = hfdJobScheduleID.Value.ToInt64();
            objInterviewSchedule.JobID = ddlJob.SelectedValue.ToInt32();
            objInterviewSchedule.JobLevelID = ddlJobLevel.SelectedValue.ToInt32();
            objInterviewSchedule.CandidateIDs = (sCandidateIDs.Contains(",") ? sCandidateIDs.Remove(0, 1) : sCandidateIDs);
            objInterviewSchedule.ScheduleDate = clsCommon.Convert2DateTime(txtInterviewDate.Text.Trim()).ToString("dd MMM yyyy");
            objInterviewSchedule.ScheduleStartTime = txtInterviewTime.Text;
            objInterviewSchedule.ScheduledTime = sStartTime;
            objInterviewSchedule.Venue = txtVenue.Text.Trim();
            objInterviewSchedule.Duration = txtDuration.Text;
            objInterviewSchedule.CreatedBy = objUserMaster.GetUserId();

            objInterviewSchedule.InterviewerIDs = sInterviewerIDs.Remove(0, 1);
            objInterviewSchedule.ScheduledTimes = sScheduleTimings.Remove(0, 1);

            ScheduleID = objInterviewSchedule.SaveSchedule();


            //---------------------------------------------- Candidate Mail Section ------------------------------------------------ //
            if (ScheduleID > 0)
            {
                DataTable dtMail = objInterviewSchedule.GetFromMail();

                string sFrom = string.Empty;
                if (dtMail.Rows.Count > 0)
                    sFrom = Convert.ToString(dtMail.Rows[0]["UserName"]);
                objInterviewSchedule.JobScheduleID = ScheduleID;

                Thread t = new Thread(() => new clsInterviewSchedule().SendMail(sFrom, ScheduleID));
                t.Start();

                // objInterviewSchedule.SendMail(sFrom,ScheduleID);
            }

            ViewSchedules();
        }
        catch (Exception ex)
        {

        }

    }
    private bool ChckTimeDuplication(string Time, long CandidateID)// Check duplication
    {
        if (clsInterviewSchedule.IsExists(hfdJobScheduleID.Value.ToInt64(),CandidateID, Time, ddlJob.SelectedValue.ToInt32(), ddlJobLevel.SelectedValue.ToInt32(), clsCommon.Convert2DateTime(txtInterviewDate.Text.Trim()).ToString("dd MMM yyyy")))
       
            return true;
        
        else
            return false;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtScheduleSearchDate.Text = "";
        ViewSchedules();
      
    }

    protected void dlCandidates_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                TextBox txtTime = (TextBox)e.Item.FindControl("txtTime");

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Time" + e.Item.ItemIndex.ToString(), "$(function () { $('#" + txtTime.ClientID + "').timeEntry(); });", true);

                HiddenField hfdIsCurrent = (HiddenField)e.Item.FindControl("hfdIsCurrent");
                HiddenField hfdIsAttended = (HiddenField)e.Item.FindControl("hfdIsAttended");
                Label lblNameEng = (Label)e.Item.FindControl("lblNameEng");


                HtmlControl tdCheckbox = (HtmlControl)e.Item.FindControl("tdCheckbox");


                if (hfdIsCurrent.Value == "1" || hfdIsAttended.Value == "1")
                {
                    tdCheckbox.Style["background-color"] = "green";
                    e.Item.Enabled = false;
                    lblNameEng.ToolTip = clsGlobalization.IsArabicCulture() ? "مقابلة عملية" : "Interview Processing";
                }
                else
                {
                    e.Item.Enabled = true;
                }


            }
            if (e.Item.ItemType == ListItemType.Footer)
            {

                DataList dlCandidates = (DataList)sender;

                DateTime dt = Convert.ToDateTime(txtInterviewTime.Text);
                Button btnSetTime = (Button)e.Item.FindControl("btnSetTime");
                Button brnClear = (Button)e.Item.FindControl("brnClear");

                btnSetTime.OnClientClick = "return setTime('" + dlCandidates.ClientID + "','" + txtInterviewTime.ClientID + "','" + txtDuration.ClientID + "');";
                brnClear.OnClientClick = "return clearTime('" + dlCandidates.ClientID + "');";

            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void dlViewSchedules_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objInterviewSchedule = new clsInterviewSchedule();
        objUserMaster = new clsUserMaster();
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton lnkScheduleTitle = e.Item.FindControl("lnkScheduleTitle") as LinkButton;
                HtmlGenericControl divScheduleDetails = e.Item.FindControl("divScheduleDetails") as HtmlGenericControl;
                DataList dlViewCandidates = e.Item.FindControl("dlViewCandidates") as DataList;

                HiddenField hfdProcessed = (HiddenField)e.Item.FindControl("hfdProcessed");
                ImageButton imgDelete = (ImageButton)e.Item.FindControl("imgDelete");
                ImageButton imgPrint = (ImageButton)e.Item.FindControl("imgPrint");
                ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");

                if (lnkScheduleTitle != null)
                {
                    lnkScheduleTitle.OnClientClick = "ToggleDiv(" + divScheduleDetails.ClientID + "); return false;";
                }
               

                // Permissions

                
                //

                if (hfdProcessed.Value.ToInt32() >= 1)
                {
                    imgDelete.Enabled = false;
                    imgDelete.ToolTip = "Interview started against this schedule";
                    imgDelete.ImageUrl = "../images/delete_popup_disable.png";
                }
                else
                {
                    imgDelete.Enabled =Convert.ToBoolean(ViewState["IsDelete"]);
                    imgDelete.ToolTip = Convert.ToBoolean(ViewState["IsPrintEmail"]) == true ? (clsGlobalization.IsArabicCulture() ? "حذف الجدول" : "Click here to delete schedule") : (clsGlobalization.IsArabicCulture() ? "لا توجد صلاحية ل حذف" : "No permission to delete");
                    imgDelete.ImageUrl = Convert.ToBoolean(ViewState["IsPrintEmail"]) == true ? "../images/delete_popup.png" : "../images/delete_popup_disable.png";

                }

                imgPrint.Enabled = Convert.ToBoolean(ViewState["IsPrintEmail"]);
                imgPrint.ToolTip = Convert.ToBoolean(ViewState["IsPrintEmail"]) == true ? (clsGlobalization.IsArabicCulture() ? "الجدول الطباعة" : "Print Schedule") : (clsGlobalization.IsArabicCulture() ? "لا توجد صلاحية ل طباعة" : "No permission to Print");

                imgEdit.Enabled = Convert.ToBoolean(ViewState["IsUpdate"]);
                imgEdit.ToolTip = Convert.ToBoolean(ViewState["IsUpdate"]) == true ? (clsGlobalization.IsArabicCulture() ? "تحرير الجدول" : "Edit Schedule") : (clsGlobalization.IsArabicCulture()? "لا توجد صلاحية ل تحرير" : "No permission to Edit");
                imgEdit.ImageUrl = Convert.ToBoolean(ViewState["IsUpdate"]) == true ? "~/images/edit.png" : "../images/edit_disable.png";


                objInterviewSchedule.JobScheduleID = dlViewSchedules.DataKeys[e.Item.ItemIndex].ToInt64();
                dlViewCandidates.DataSource = objInterviewSchedule.FillCandidatesForView();
                dlViewCandidates.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void dlViewSchedules_ItemCommand(object source, DataListCommandEventArgs e)
    {

        switch (e.CommandName)
        {
            case "ALTER":
                EditScheduleDetails(Convert.ToInt64(e.CommandArgument), string.Empty);
                break;
            case "REMOVE":
                RemoveScheduleDetails(Convert.ToInt64(e.CommandArgument));
                txtScheduleSearchDate.Text = "";
                ViewSchedules();
                break;
            case "Print":
                PrintSchedules(e);
                break;
        }
    }

    protected void lnkSchedule_Click(object sender, EventArgs e)
    {
        //txtDuration.Text = "00:00";
        chlInterviewer.Enabled = true;
        txtInterviewDate.Text = string.Empty;
        ClearFields();
        FillCombo();
        divAddSchedule.Style["display"] = "block";
        divViewSchedule.Style["display"] = "none";
        upSchedule.Update();
        upMenu.Update();
    }
    protected void lnkScheduleList_Click(object sender, EventArgs e)
    {
        txtScheduleSearchDate.Text = "";
        ViewSchedules();
    }
     
    protected void txtInterviewDate_TextChanged(object sender, EventArgs e)
    {
       // EditScheduleDetails(0, clsCommon.Convert2DateTime(txtInterviewDate.Text.Trim()).ToString("dd MMM yyyy"));
    }

    protected void imgShowCandidates_Click(object sender, EventArgs e)
    {
        FillCandidates();
    }

    // ----------------------------------------------------------Searching------
    protected void txtScheduleSearchDate_TextChanged(object sender, EventArgs e)
    {
        BindSchedules();
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        BindSchedules();
    }
 
    
    protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillJobLevels();
        FillJobDetails();
        FillInterviewer(0);
        hfdIsBorad.Value =Convert.ToString(clsInterviewSchedule.IsBoard(ddlJob.SelectedValue.ToInt32()));
    }
    protected void ddlJobLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        FillInterviewer(0);
    }
    
    private void FillJobLevels()
    {
        objInterviewSchedule = new clsInterviewSchedule();
        objInterviewSchedule.JobID = Convert.ToInt32(ddlJob.SelectedValue);
        ddlJobLevel.DataSource = objInterviewSchedule.FillJobLevels();
        ddlJobLevel.DataBind();
        ddlJobLevel.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ?"اختر" :"--Select--", "-1"));
    }

    private void FillJobDetails()
    {
        DataTable dtJobDetail = clsInterviewSchedule.GetJobDetails(ddlJob.SelectedValue.ToInt32());
        if (dtJobDetail.Rows.Count > 0)
        {
            divJobDetails.Style["display"] = "block";
            lbkLevel.Text = Convert.ToString(dtJobDetail.Rows[0]["Level"]);
            lblExpiryDate.Text = Convert.ToString(dtJobDetail.Rows[0]["Expirydate"]);
        }
        else
        {
            divJobDetails.Style["display"] = "none";
        }
    }

    // More Candidates
    protected void lnkMore_Click(object sender, EventArgs e)
    {
        ddlSrchQualification.DataSource = clsInterviewSchedule.FillQualification();
        ddlSrchQualification.DataBind();
        ddlSrchQualification.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "حدد التأهيل" : "-Select Qualification-", "-1"));

        ddlSrchQualification.SelectedIndex = -1;
        txtSrchSkillSet.Text = string.Empty;

        BindMoreCandidates();
        updMoreCandidate.Update();
    }
    protected void imgCanSearch_Click(object sender, ImageClickEventArgs e)
    {
        BindMoreCandidates();
        updMoreCandidates.Update();
    }
    private void BindMoreCandidates()
    {
      

        DataTable dtMoreCandidates = clsInterviewSchedule.GetMoreCandidates(ddlJob.SelectedValue.ToInt32(),ddlJobLevel.SelectedValue.ToInt32(),ddlSrchQualification.SelectedValue.ToInt32(),txtSrchSkillSet.Text.Trim());

        dlMoreCandidates.DataSource = dtMoreCandidates;
        dlMoreCandidates.DataBind();
        if (dtMoreCandidates.Rows.Count > 0)
        {
            mpeMoreCandidates.Show();
        }
        else
        {
            mcMessage.InformationalMessage(clsGlobalization.IsArabicCulture() ? "لا أكثر المرشحين لل قائمة" : "No more candidates to list");
            this.mpeMessage.Show();
        }
           // ScriptManager.RegisterStartupScript(this, this.GetType(), "More Candidates", "alert('No more candidates to list');", true);

    }

    protected void lnkAddCandidates_Click(object sender, EventArgs e)
    {

        lnkAddCandidates.OnClientClick = "return valPrintEmailDatalist('" + dlMoreCandidates.ClientID + "');";

        SetTiming();

        bool IsSelected = false;
        string sMoreId = string.Empty;
        string sCandidateId = string.Empty;

        foreach (DataListItem Item in dlMoreCandidates.Items)
        {
            CheckBox chkMoreSelect = (CheckBox)Item.FindControl("chkMoreSelect");

            if (chkMoreSelect.Checked)
            {
                IsSelected = true;
                sMoreId += "," + Convert.ToString(dlMoreCandidates.DataKeys[Item.ItemIndex]);
                
            }
        }

        if (!IsSelected)
        {
            mpeMoreCandidates.Hide();
            return;
        }
        else
        {

            string sSelectedIds = string.Empty;
            foreach (DataListItem Item in dlCandidates.Items)
            {

                CheckBox chkSelect = (CheckBox)Item.FindControl("chkSelect");

                if (chkSelect.Checked)

                    sSelectedIds += "," + Convert.ToString(dlCandidates.DataKeys[Item.ItemIndex]);

                sCandidateId += "," + Convert.ToString(dlCandidates.DataKeys[Item.ItemIndex]);
            }

            sSelectedIds = sSelectedIds + sMoreId.Remove(0, 1)  ;

            DataTable dtCandidates = clsInterviewSchedule.GetSelectedCandidates(sMoreId.Remove(0, 1) + sCandidateId, ddlJob.SelectedValue.ToInt32(), ddlJobLevel.SelectedValue.ToInt32());


            if (dtCandidates.Rows.Count > 0)
            {
                divNoCandidate.Style["display"] = "none";
                divCandidate.Style["display"] = "block";

            }
            else
            {
                divNoCandidate.Style["display"] = "block";
                divCandidate.Style["display"] = "none";
            }

            dlCandidates.DataSource = dtCandidates;
            dlCandidates.DataBind();

            txtResumeQuota.Text =Convert.ToString(dtCandidates.Rows.Count);


            updResumeQuota.Update();
            updCandidates.Update();

            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "SetTime", "setTime('" + dlCandidates.ClientID + "','" + txtInterviewTime.ClientID + "','" + txtDuration.ClientID + "');", true);

         
            //string[] Selected = sSelectedIds.Split(',');
            //foreach (string s in Selected)
            //{
            //    foreach (DataListItem Item in dlCandidates.Items)
            //    {
            //        if (Convert.ToString(dlCandidates.DataKeys[Item.ItemIndex]) == s)
            //        {
            //            CheckBox chkSelect = (CheckBox)Item.FindControl("chkSelect");
            //            TextBox txtTime = (TextBox)Item.FindControl("txtTime");
            //            chkSelect.Checked = true;
            //            txtTime.Enabled = true;
            //        }

            //    }
            //}
            //Label lblCandidates = (Label)fvVacancyDetails.FindControl("lblCandidates");
            //if (dlCandidates.Items.Count > 0)
            //    lblCandidates.Text = string.Empty;
            mpeMoreCandidates.Hide();
        }


    }

  //// Mail Sending
  //  protected void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
  //  {
  //  }
  //  protected void btnSendMail_Click(object sender, EventArgs e)
  //  {
  //  }
  //  protected void btnSend_Click(object sender, EventArgs e)
  //  {
     
  //      ddlTemplates.DataSource = clsInterviewSchedule.FillTemplates();
  //      ddlTemplates.DataBind();
  //      ddlTemplates.Items.Insert(0, new ListItem("Select", "-1"));
  //      updMail.Update();
  //      mpeMail.Show();
  //  }
    protected void chkEmployees_CheckedChanged(object sender, EventArgs e)
    {
        int JobScheduleID = ViewState["JobScheduleID"] == null ? 0 : ViewState["JobScheduleID"].ToInt32();
        FillInterviewer(JobScheduleID);
    }
}
