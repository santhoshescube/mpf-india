﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Qualification.aspx.cs" Inherits="Public_Qualification" Title="Qualification" %>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
     <div id='cssmenu'>
        <ul>
            <li ><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li ><a href="Visa.aspx"><asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li class="selected"><a href="Qualification.aspx"><asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx"><asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx"><asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx"><asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx"><asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <asp:UpdatePanel ID="upEmployeeVisa" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="fvEmployees" runat="server">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <%-- <tr>
                                                <td width="120px" class="trLeft">
                                                    Company
                                                </td>
                                                <td class="trRight">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                        DataValueField="CompanyID" DataTextField="CompanyName" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td width="150px" class="trLeft">
                                                     <asp:Literal ID="Literal1" runat ="server" Text ='<%$Resources:DocumentsCommon,Employee%>' ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="190px" CssClass="dropdownlist_mandatory" BackColor="LightYellow"
                                                                DataValueField="EmployeeID" DataTextField="EmployeeName">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                       Display="None" ErrorMessage="*" ValidationGroup="Employee"
                                                        InitialValue="-1"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hdQualificationID" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--University--%><asp:Literal ID="Literal2" runat ="server" meta:resourcekey="University" ></asp:Literal>
                                                </td>
                                                <td valign="top" class="trRight">
                                                    <asp:UpdatePanel ID="upnlUniversity" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlUniversity" runat="server" CssClass="dropdownlist_mandatory"
                                                                Width="190px">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnUniversity" runat="server" Text="..." CausesValidation="False"
                                                                CssClass="referencebutton" Height="20px" OnClick="btnUniversity_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdatePanel ID="upnlPopup" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div style="display: none">
                                                                <asp:Button ID="Button3" runat="server" />
                                                            </div>
                                                            <div id="pnlUniversity" runat="server" style="display: none;">
                                                                <uc:ReferenceNew ID="rnUniversity" runat="server" ModalPopupID="mpeLicenceType" />
                                                            </div>
                                                            <AjaxControlToolkit:ModalPopupExtender ID="mpeLicenceType" runat="server" TargetControlID="Button3"
                                                                PopupControlID="pnlUniversity" BackgroundCssClass="modalBackground">
                                                            </AjaxControlToolkit:ModalPopupExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <%--School/College--%><asp:Literal ID="Literal3" runat ="server" meta:resourcekey="SchoolCollege" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtSchoolOrCollege" CssClass="textbox_mandatory" MaxLength="90"
                                                        runat="server" Width="182px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--Qualification--%><asp:Literal ID="Literal4" runat ="server" meta:resourcekey="Qualification" ></asp:Literal>
                                                </td>
                                                <td valign="top" class="trRight">
                                                    <asp:UpdatePanel ID="updDegree" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="rcDegree" runat="server" CssClass="dropdownlist_mandatory" BackColor="LightYellow"
                                                                Width="190px">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnDegree" runat="server" Text="..." CausesValidation="False" CssClass="referencebutton"
                                                                Height="20px" OnClick="btnDegree_Click" />
                                                            <asp:RequiredFieldValidator ID="rfvUniversity" runat="server" ControlToValidate="rcDegree"
                                                                 Display="None" ErrorMessage="*"
                                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div style="display: none">
                                                                <asp:Button ID="Button1" runat="server" />
                                                            </div>
                                                            <div id="pnlModalPopUp" runat="server" style="display: none;">
                                                                <uc:DegreeReference ModalPopupID="mdlPopUpReference" ID="DegreeReference1" runat="server" />
                                                            </div>
                                                            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                                                                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                                                            </AjaxControlToolkit:ModalPopupExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                   <%-- Reference Number--%><asp:Literal ID="Literal5" runat ="server" meta:resourcekey="RefNumber" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtReferenceNumber" CssClass="textbox_mandatory" MaxLength="45" BackColor="LightYellow"
                                                        runat="server" Width="182px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvpassport" runat="server" ControlToValidate="txtReferenceNumber"
                                                         Display="Dynamic" ErrorMessage="*"
                                                        ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--From Period--%><asp:Literal ID="Literal6" runat ="server" meta:resourcekey="FromPeriod" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("FromDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnFromDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceFromDate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnFromDate" TargetControlID="txtFromDate" />
                                                            </td>
                                                            <td>
                                                                <%--<asp:CustomValidator ID="cvFromDate" runat="server" ControlToValidate="txtFromDate"
                                                                    ClientValidationFunction="validateLicenseFromDate" CssClass="error" ValidateEmptyText="True"
                                                                    ValidationGroup="Employee" Display="Dynamic"></asp:CustomValidator>--%>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--To Period--%><asp:Literal ID="Literal7" runat ="server" meta:resourcekey="ToPeriod" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("ToDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnToDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceToDate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnToDate" TargetControlID="txtToDate" />
                                                            </td>
                                                            <td>
                                                                 <asp:CustomValidator ID="cvToDate" runat="server" ControlToValidate="txtToDate"
                                                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Employee" ClientValidationFunction="validateQualificationPeriod"
                                                                    Display="None"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <%--Certificate Title--%><asp:Literal ID="Literal8" runat ="server" meta:resourcekey="CertificateTitle" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtCertificateTitle" CssClass="textbox_mandatory" MaxLength="45"
                                                        runat="server" Width="182px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <%--Grade--%><asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Grade" ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtGrade" CssClass="textbox_mandatory" MaxLength="9" runat="server"
                                                        Width="182px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" style="padding-left: 0px">
                                                    <asp:CheckBox ID="chkAttested" runat="server" meta:resourcekey="CertificatesAttested" />
                                                </td>
                                                <td class="trRight" style="padding-left: 80px">
                                                    <asp:CheckBox ID="chkClearanceRequired" runat="server" meta:resourcekey="UniversityClearenceRequired" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" style="padding-left: 0px">
                                                    <asp:CheckBox ID="chkVerified" runat="server" meta:resourcekey="CertificatesVerified" />
                                                </td>
                                                <td class="trRight" style="padding-left: 80px">
                                                    <asp:CheckBox ID="chkClearanceCompleted" runat="server" meta:resourcekey="ClearenceCompleted" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <%--Other Info--%><asp:Literal ID="Literal10" runat ="server" Text ='<%$Resources:DocumentsCommon,OtherInfo%>' ></asp:Literal>
                                                    

                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="textbox" Width="300px" TextMode="MultiLine"
                                                        Height="76px" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                   <%-- Current Status--%><asp:Literal ID="Literal11" runat ="server" Text ='<%$Resources:DocumentsCommon,CurrentStatus%>'  ></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:Label ID="lblCurrentStatus" runat="server" Text='<%$Resources:DocumentsCommon,New%>' Width="30%"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft" colspan="2">
                                                    <div id="divParse">
                                                        <fieldset style="padding: 10px 3px">
                                                            <legend><asp:Literal ID="Literal24" runat ="server"  Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal></legend>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <%--Document Name--%> <asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:DocumentsCommon,DocumentName%>'  ></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 3px" valign="top">
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%--Choose file..--%><asp:Literal ID="Literal13" runat ="server" Text ='<%$Resources:DocumentsCommon,ChooseFile%>'  ></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="175px">
                                                                        <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="50" Width="150px"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                                             Display="Dynamic" ErrorMessage="*"
                                                                            ValidationGroup="Employee1"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="cvVisadocnameduplicate" runat="server" ClientValidationFunction="validatevisadocnameduplicate"
                                                                            ControlToValidate="txtDocname" Display="Dynamic" ErrorMessage="Duplicate document name"
                                                                            ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="width: 2px">
                                                                    </td>
                                                                    <td valign="top" width="300px">
                                                                        <AjaxControlToolkit:AsyncFileUpload ID="fuVisa" runat="server" OnUploadedComplete="fuVisa_UploadedComplete"
                                                                            PersistFile="True" Width="310px" />
                                                                        <asp:CustomValidator ID="cvVisaAttachment" runat="server" ClientValidationFunction="validatevisaAttachment"
                                                                            CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>' ValidateEmptyText="True" ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="padding-left: 15px" valign="top" width="200px">
                                                                        <asp:LinkButton ID="btnattachvisadoc" runat="server" Enabled="true" OnClick="btnattachvisadoc_Click"
                                                                            Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="Employee1">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft" colspan="2">
                                                    <div id="divPassDoc" runat="server" class="container_content" style="display: none;
                                                        height: 150px; overflow: auto">
                                                        <asp:UpdatePanel ID="updldlPassportDoc" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DataList ID="dlVisaDoc" runat="server" Width="100%" DataKeyField="Node" GridLines="Horizontal"
                                                                    OnItemCommand="dlVisaDoc_ItemCommand">
                                                                    <HeaderStyle CssClass="datalistheader" />
                                                                    <HeaderTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="170">
                                                                                   <%-- Document Name--%><asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:DocumentsCommon,DocumentName%>'  ></asp:Literal>
                                                                                </td>
                                                                                <td width="150" align="left">
                                                                                   <%-- File Name--%> <asp:Literal ID="Literal14" runat ="server" Text ='<%$Resources:DocumentsCommon,FileName%>'  ></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="170">
                                                                                    <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="170px"></asp:Label>
                                                                                </td>
                                                                                <td width="150" align="left">
                                                                                    <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                                                                </td>
                                                                                <td width="25" align="left">
                                                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"
                                                                                        CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                    <td>
                                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Submit%>' Text='<%$Resources:ControlsCommon,Submit%>'
                                            ValidationGroup="Employee" OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel%>' Text='<%$Resources:ControlsCommon,Cancel%>'
                                            CausesValidation="false" OnClick="btnCancel_Click" />
                                           <%-- <asp:ValidationSummary runat="server" ShowSummary="true" ShowMessageBox="true" ValidationGroup="Employee"/>--%>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:DataList ID="dlQualification" runat="server" BorderWidth="0px" CellPadding="0"
                            CssClass="labeltext" Width="100%" OnItemCommand="dlQualification_ItemCommand"
                            DataKeyField="QualificationID" OnItemDataBound="dlQualification_ItemDataBound">
                            <ItemStyle CssClass="labeltext" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px;">
                                    <tr>
                                        <td align="left" height="30" width="30">
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkEmployee');" />
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <%--Select All--%><asp:Literal ID="Literal14" runat ="server" Text ='<%$Resources:ControlsCommon,SelectAll%>'  ></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 3px;" id="tblDetails"
                                    runat="server">
                                    <%--onmouseover="showEdit(this);" onmouseout="hideEdit(this);"--%>
                                    <tr>
                                        <td valign="top" width="30" style="padding-top: 5px">
                                            <asp:CheckBox ID="chkEmployee" runat="server" />
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEmployee" CausesValidation="false" runat="server" CommandArgument='<%# Eval("QualificationID")%>'
                                                                        CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("EmployeeFullName")) %>'
                                                                        CssClass="listHeader bold"></asp:LinkButton>
                                                                    [<%# Eval("EmployeeNumber")%>] &nbsp 
                                                                    <%--Reference No.--%><asp:Literal ID="Literal5" runat ="server" meta:resourcekey="RefNumber" ></asp:Literal> -
                                                                    <%# Eval("ReferenceNumber")%>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                        display: block;" CommandArgument='<%# Eval("QualificationID")%>' CommandName="ALTER"
                                                                        ImageUrl="~/images/edit.png" ToolTip="Edit"></asp:ImageButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="innerdivheader">
                                                                   <%-- Qualification--%><asp:Literal ID="Literal2" runat ="server" meta:resourcekey="Qualification" ></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("Degree")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader">
                                                                   <%-- Period--%><asp:Literal ID="Literal15" runat ="server" meta:resourcekey="Period" ></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("FromDate")+" - "+Eval("ToDate")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:FormView ID="fvQualification" runat="server" BorderWidth="0px" CellPadding="0" Width="100%"
                            CellSpacing="0" DataKeyNames="QualificationID">
                            <ItemTemplate>
                                <div id="mainwrap">
                                    <div id="main2" style="margin-top: 10px; margin-left: 10px">
                                        <div class="t22">
                                            <div class="ta22" style="width:40%">
                                                <div class="maindivheader" style="width:90%">
                                                    <div class="innerdivheader">
                                                        <%--Employee Name--%><asp:Literal ID="Literal16" runat ="server" Text ='<%$Resources:DocumentsCommon,Employee%>'  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                       <%-- University--%><asp:Literal ID="Literal17" runat ="server" meta:resourcekey="University"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                        <%--School/College--%><asp:Literal ID="Literal18" runat ="server" meta:resourcekey="SchoolCollege"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                        <%--Qualification--%><asp:Literal ID="Literal19" runat ="server" meta:resourcekey="Qualification"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                       <%-- Reference Number--%><asp:Literal ID="Literal20" runat ="server" meta:resourcekey="RefNumber"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                       <%-- From Period--%><asp:Literal ID="Literal21" runat ="server" meta:resourcekey="FromPeriod"  ></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                       <%-- To Period--%><asp:Literal ID="Literal22" runat ="server" meta:resourcekey="ToPeriod"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                       <%-- Certificate Title--%><asp:Literal ID="Literal23" runat ="server" meta:resourcekey="CertificateTitle"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                        <%--Grade/Percentage--%><asp:Literal ID="Literal24" runat ="server" meta:resourcekey="GradePercentage"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                      <%--  Certificates Attested?--%><asp:Literal ID="Literal25" runat ="server" meta:resourcekey="CertificatesAttested"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                        <%--Certificates Verified?--%><asp:Literal ID="Literal26" runat ="server" meta:resourcekey="CertificatesVerified"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader"  style="width:90%">
                                                       <%-- University Clearence Required?--%><asp:Literal ID="Literal27" runat ="server" meta:resourcekey="UniversityClearenceRequired"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                        <%--Clearence Completed?--%><asp:Literal ID="Literal28" runat ="server" meta:resourcekey="ClearenceCompleted"  ></asp:Literal>
                                                        </div>
                                                    <div class="innerdivheader">
                                                       <%-- Other Info--%><asp:Literal ID="Literal29" runat ="server" Text ='<%$Resources:DocumentsCommon,OtherInfo%>'  ></asp:Literal>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="ta3"  style="width:50%;height:700px">
                                                <div class="maindivheaderValue">
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("EmployeeFullName")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("University")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("SchoolOrCollege")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("Degree")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("ReferenceNumber")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("FromDate")%>
                                                    </div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("ToDate")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("CertificateTitle")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("GradeorPercentage")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("CertificatesAttested")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("CertificatesVerified")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("UniversityClearenceRequired")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("ClearenceCompleted")%></div>
                                                    <div class="innerdivheaderValue" style="word-break: break-all;">
                                                        <%# Eval("Remarks")%></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                        <uc:Pager ID="pgrEmployees" runat="server" OnFill="Bind" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fuVisa" EventName="UploadedComplete" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"></asp:Label>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlQualification" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPrint" runat="server">
                    <ContentTemplate>
                        <div id="divPrint" runat="server" style="display: none">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnIssueReceipt" runat="server" />
                        </div>
                        <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                            <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                            </uc1:DocumentReceiptIssue>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                            PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:ControlsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                   <%-- <a href="">--%>
                        <asp:ImageButton ID="ImgSearch" OnClick="btnSearch_Click" ImageUrl="~/images/search.png"
                            runat="server" />
                  <%--  </a>--%>
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_template.png" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="btnNew" runat="server" CausesValidation="False" OnClick="btnNew_Click" meta:resourcekey="AddQualification"></asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/view_performance_template.png" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click" CausesValidation="False" meta:resourcekey="ViewQualification">View Qualification</asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text ='<%$Resources:ControlsCommon,Delete%>' > <h5>
                           </h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="btnPrint" runat="server" OnClick="btnPrint_Click" Text ='<%$Resources:ControlsCommon,Print%>'>
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="btnEmail" runat="server" OnClick="btnEmail_Click" Text ='<%$Resources:ControlsCommon,Email%>'>
                         
                              </asp:LinkButton></h5>
                </div>
                <div id="divIssueReceipt" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div id="Div2" class="name" runat="server">
                       <h5> <asp:LinkButton ID="lnkDocumentIR"  CausesValidation ="false"   runat="server" OnClick="lnkDocumentIR_Click" Text ='<%$Resources:DocumentsCommon,Receipt%>'>
                             
                                  </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
