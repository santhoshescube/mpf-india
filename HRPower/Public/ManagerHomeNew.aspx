<%@ Page Language="C#" MasterPageFile="~/Master/ManagerHomeMasterPage.master" AutoEventWireup="true"
    CodeFile="ManagerHomeNew.aspx.cs" Inherits="Public_ManagerHomeNew" Title='<%$Resources:MasterPageCommon,Home%>' %>

<%@ Register Src="../Controls/Alert.ascx" TagName="Alert" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">

    <script src="../css/jquery-1.2.6.min.js" type="text/javascript"></script>

    <div id='cssmenu'>
        <ul>
            <li class="selected"><a href='ManagerHomeNew.aspx'>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DashBoard%>'>
            
                </asp:Literal>
            </a></li>
            <li><a href='HomeRecentActivity.aspx'><span>
                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeActiveVacancies.aspx'><span>
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeNewHires.aspx'><span>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeAlerts.aspx'><span>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal>
            </span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="divLeft" style="float: left; width: 50%; padding-top: 5px">
        <div id="divLeft" style="float: left; width: 30%;text-align: center; font-size: 10px; font-weight: bold;
                font-family: Tahoma; ">
            <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:MasterPageCommon,Company%>'></asp:Literal></div>
        <div id="divRight" style="float: left; width: 70%">
            <asp:UpdatePanel ID="updCompany" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:DropDownList ID="ddlCompany" DataTextField="CompanyName" DataValueField="CompanyID"
                        AutoPostBack="true" runat="server" Width="216px" CssClass="dropdownlist_mandatory"  onselectedindexchanged="ddlCompany_SelectedIndexChanged">
                    </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="divRight" style="float: left; width: 50%">
        <div id="divHeading" style="float: left; padding-left: 420px;">
            <asp:ImageButton ImageUrl="../images/Detail1.png" ID="imgDetailView" runat="server"
                ToolTip='<%$Resources:ControlsCommon,DetailView%>' OnClick="imgDetailView_Click" />
        </div>
    </div>
    <div id="divLeft" style="float: left; width: 50%">
        <%--left--%>
        <%--Document Expiry Alerts--%>
        <div style="background-color: White; border: solid 1px grey; float: left; font-size: 12px;
            overflow: auto; height: 300px; width: 450px;">
            <asp:UpdatePanel ID="updExpiryAlert" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Chart ID="Chart1" runat="server" Height="500px" Width="440px">
                        <Titles>
                            <asp:Title Name="Title1" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="true" Name="Default"
                                Enabled="false" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" IsVisibleInLegend="true" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BackColor="White" />
                        </ChartAreas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--Chart Pending requests--%>
        <div style="background-color: White; border: solid 1px grey; float: left; margin-top: 5px;
            font-size: 12px;">
            <asp:UpdatePanel ID="updRequests" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Chart ID="ChartRequests" runat="server" Height="300px" Width="450px">
                        <Titles>
                            <asp:Title Name="Title1" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                LegendStyle="Table" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BackColor="White" />
                        </ChartAreas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--Job Status--%>
        <div id="divJobChart" runat="server" style="background-color: White; border: solid 1px grey;
            float: left; margin-top: 5px; font-size: 12px; overflow: auto; height: 310px;
            width: 450px;">
            <asp:UpdatePanel ID="updJobStatus" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Chart ID="ChartJob" runat="server" Height="300px" Width="1000px" CssClass="divJobChart">
                        <Titles>
                            <asp:Title Name="Title1" Docking="Left" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Near" Docking="Top" IsTextAutoFit="false" LegendStyle="Row"
                                Enabled="false" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" IsVisibleInLegend="false" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BackColor="Ivory" />
                        </ChartAreas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--My Requests--%>
        <div style="width: 450px; border: 1px solid #b8b8b8; border-radius: 5px; height: auto;
            background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
            <div style="margin: auto; text-align: center; font-size: 10px; font-weight: bold;
                font-family: Tahoma; padding: 5px; color: Black">
                <p>
                    <%-- My Requests--%>
                    <asp:Literal ID="ltMyRequest" runat="server"></asp:Literal>
                </p>
            </div>
            <div style="margin: auto; text-align: center; border-bottom: 1px solid #f6c94e;">
            </div>
            <div style="margin: auto;">
                <div style="text-align: left; overflow: auto; max-height: 170px;" class="jobhead">
                    <asp:UpdatePanel ID="updMyRequests" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divMyRequests" runat="server" style="max-height: 170px; background-color: White;">
                                <asp:Repeater ID="rptMyRequests" runat="server" OnItemDataBound="rptMyRequests_ItemDataBound">
                                    <ItemTemplate>
                                        <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                            <tr class="listItem">
                                                <td style="font-size: 12px;">
                                                    <a id="ancMyMessage" runat="server" class="Dashboradlink" href="#" style="cursor: default;">
                                                        <%# Eval("Messages") %></a>
                                                    <asp:ImageButton ID="imgMyRequestDelete" OnClick="imgMyRequestDelete_Click" runat="server"
                                                        ToolTip="Click here to delete request" ImageUrl="~/images/deletemsg.png" />
                                                    <asp:HiddenField ID="hfMyRequestMyRequest" Value='<%#Eval("MessageTypeID")%>' runat="server" />
                                                    <asp:HiddenField ID="hfMessageID" Value='<%#Eval("MessageID")%>' runat="server" />
                                                    <asp:HiddenField ID="hfReadStatus" runat="server" Value='<%#Convert.ToInt32(Eval("ReadStatus"))%>' />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <%--   Current Job Openings--%>
        <div style="display: block; width: 450px; border: 1px solid #b8b8b8; border-radius: 5px;
            height: auto; background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
            <div style="margin: auto; text-align: center; font-size: 16px; padding: 5px; color: Black">
                <p>
                    <%-- Job Openings--%>
                    <asp:Literal ID="litJobOpening" runat="server"></asp:Literal>
                </p>
            </div>
            <div style="margin: auto; text-align: center; border-bottom: 2px solid #f6c94e;">
            </div>
            <div style="margin: auto;">
                <div style="text-align: left; overflow: auto; height: 300px;" class="jobhead">
                <asp:UpdatePanel ID="updJobTray" runat="server" UpdateMode="Always">
                <ContentTemplate >
                  <asp:Repeater ID="repJob" runat="server" OnItemDataBound="repJob_ItemDataBound">
                        <ItemTemplate>
                            <%#Eval("Job1")%>
                            <asp:HiddenField ID="hfdJobID" Value='<%# Eval("JobID") %>' runat="server" />
                            <div style="text-align: left;" class="jobsub">
                                <asp:Repeater ID="repSkills" runat="server">
                                    <ItemTemplate>
                                        <div style="text-align: left; word-break: break-all;">
                                            <img src="../images/golden.png" />
                                            &nbsp; <b>
                                                <%--Skills:--%>
                                                <asp:Literal ID="litt" runat="server" Text='<%$Resources:ControlsCommon,Skills%>'></asp:Literal>
                                            </b>:<%#Eval("Skills")%>
                                        </div>
                                        <div style="text-align: left;">
                                            <b>
                                                <%--Vacancies:--%>
                                                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,Vacancies%>'></asp:Literal>
                                            </b>:<%#Eval("RequiredQuota")%>
                                        </div>
                                        <div style="text-align: left;">
                                            <b>
                                                <%--  Experience:--%>
                                                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,Experience%>'></asp:Literal>
                                            </b>:<%#Eval("Experience")%>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ContentTemplate>
                </asp:UpdatePanel>
                  
                </div>
            </div>
        </div>
    </div>
    <%--right--%>
    <div id="divRight" style="float: left; width: 50%">
        <%--My Attendance--%>
        <div id="divMyAttendance" runat="server" style="margin-bottom: 10px; width: 470px;
            border: 1px solid #b8b8b8; border-radius: 5px; height: auto; background: white;
            float: left; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
            <div style="margin: auto; text-align: center; font-size: 10px; font-weight: bold;
                font-family: Tahoma; padding: 5px; color: Black">
                <p>
                    <%--My Attendance--%>
                    <asp:Literal ID="litAttendance" runat="server"></asp:Literal>
                </p>
            </div>
            <div style="margin: auto; text-align: center; border-bottom: 1px solid #f6c94e;">
            </div>
            <div style="margin: auto;">
                <div style="text-align: left; overflow: auto; max-height: 420px;" class="jobhead">
                    <div id="divAttendance" runat="server" style="max-height: 420px; padding-top: 2px;
                        text-align: left; overflow: auto; margin-left: 0px; display: block">
                        <table border="0" cellpadding="0" cellspacing="0" width="97%" id="Table1" runat="server">
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="updAtnDateee" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="textbox_mandatory" Width="90px"
                                                            AutoPostBack="true" OnTextChanged="txtDate_TextChanged" MaxLength="10"></asp:TextBox>
                                                    </td>
                                                    <td align="left" valign="top">
                                                        <asp:ImageButton ID="btnDateofBirth" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                            CausesValidation="False" />
                                                        <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Animated="true"
                                                            Format="dd/MM/yyyy" PopupButtonID="btnDateofBirth" TargetControlID="txtDate" />
                                                    </td>
                                                    <td align="right" width="80%" style="padding-left: 60px">
                                                        <asp:Label ID="lblShift" runat="server" Font-Size="12px"></asp:Label>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                        <div id="div1" style="max-height: 230px; padding-top: 1px; text-align: left; overflow: auto;
                            margin-left: 0px; display: block">
                            <asp:UpdatePanel ID="updAtndata" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="97%" id="tblRepeater" runat="server">
                                        <tr>
                                            <td class="trRight" style="padding-left: 10px; border: 1px solid rgb(53, 194, 238);
                                                border-radius: 8px;">
                                                <asp:DataList ID="dlAttendanceDetails" runat="server" CellPadding="0" CellSpacing="0"
                                                    Width="100%">
                                                    <HeaderTemplate>
                                                        <div style="float: left; width: 99%;" class="datalistHead">
                                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="left" width="100">
                                                                        <%--Entry/Exit--%>
                                                                        <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,EntryExit%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%--Punching--%><asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Punching%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%-- Work Time--%><asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,WorkTime%>'></asp:Literal>
                                                                    </td>
                                                                    <td align="left" width="100">
                                                                        <%--Break Time--%><asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,BreakTime%>'></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr style="font-size: 12px; color: #6a8bab;">
                                                                <td width="100">
                                                                    <b>
                                                                        <%#Eval("EntryExit") %></b>&nbsp;
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("Punching")%>
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("WorkTime").ToString().Trim() == "00:00:00" ? " " : Eval("WorkTime")%>&nbsp;
                                                                </td>
                                                                <td width="100">
                                                                    <%# Eval("BreakTime").ToString().Trim() == "00:00:00" ? " " : Eval("BreakTime")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <asp:UpdatePanel ID="updAtnTimes" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" width="97%" id="tblsummary" runat="server"
                                    style="font-size: 12px; display: none; padding-top: 2px;">
                                    <tr>
                                        <td align="left" width="30%">
                                            <%-- Normal Work Time:--%><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,NormalWorkTime%>'></asp:Literal>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblNormalWorktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                        <td align="left" width="5%">
                                        </td>
                                        <td align="left" width="20%">
                                            <%--Work time:--%><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,WorkTime%>'></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblWorktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" width="30%">
                                            <%--  Allowed Break Time:--%><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,AllowedBreakTime%>'></asp:Literal>
                                        </td>
                                        <td align="left" width="10%">
                                            <asp:Label ID="lblAllowedBreakTime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                        <td align="left" width="5%">
                                        </td>
                                        <td align="left" width="20%">
                                            <%--Break Time:--%><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,BreakTime%>'></asp:Literal>
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblBreaktime" runat="server" Font-Size="12px"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <table border="0" cellpadding="0" cellspacing="0" width="97%" id="Table2" runat="server"
                            style="display: none;">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblPolicy" runat="server" Style="font-size: 12px;"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblConsequence" runat="server" Style="font-size: 12px;"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblConseq1" runat="server"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblConseq2" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:Label ID="lblConseq3" runat="server"></asp:Label>
                                </td>
                                <td align="left">
                                    <asp:Label ID="lblConseq4" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%--Chart Leave/Late comers--%>
        <div style="background-color: White; border: solid 1px grey; float: left; font-size: 12px;">
            <%-- Chart Leave--%>
            <asp:UpdatePanel ID="updLeaveChart" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                 <asp:Chart ID="ChartLeave" runat="server" Height="300px" Width="470px">
                <Titles>
                    <asp:Title Name="Title1" />
                </Titles>
                <Legends>
                    <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                        Enabled="false" LegendStyle="Table" />
                </Legends>
                <Series>
                    <asp:Series Name="Default" />
                </Series>
                <ChartAreas>
                    <asp:ChartArea Name="ChartArea1" BorderWidth="0" />
                </ChartAreas>
            </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </div>
        <%-----------Evaluation------%>
        <div style="width: 470px; border: 1px solid #b8b8b8; border-radius: 5px; height: auto;
            background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
            <div style="margin: auto; text-align: center; font-size: 10px; font-weight: bold;
                font-family: Tahoma; padding: 5px; color: Black">
                <p>
                    <%-- Performance Evaluation--%>
                    <asp:Literal ID="litPerformance" runat="server"></asp:Literal>
                </p>
            </div>
            <div style="margin: auto; text-align: center; border-bottom: 1px solid #f6c94e;">
            </div>
            <div style="margin: auto;">
                <div style="text-align: left; overflow: auto; max-height: 170px;" class="jobhead">
                    <div id="divEvaluation" style="max-height: 170px; text-align: left; overflow: auto;
                        padding-bottom: 10px; padding-top: 10px; margin-left: 5px; display: block;">
                        <asp:Repeater ID="rptEvaluation" runat="server">
                            <ItemTemplate>
                                <table width="97%" border="0" cellpadding="0" cellspacing="0">
                                    <tr class="listItem">
                                        <td valign="top">
                                            <asp:LinkButton ID="lnkEvalMessage" class="Dashboradlink" runat="server" OnClick="lnkEvalMessage_Click">
                                                <%#Eval("Messages")%>
                                                <asp:HiddenField ID="hfValues" Value='<%#Eval( "MessageTypeID")%>' runat="server" />
                                                <asp:HiddenField ID="hfReferenceId" runat="server" Value=' <%#Eval("ReferenceId")%>'>
                                                </asp:HiddenField>
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
        <%--Document Return Alerts--%>
        <div style="background-color: White; border: solid 1px grey; float: left; margin-top: 5px;
            font-size: 12px;">
            <asp:UpdatePanel ID="updAlert" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Chart ID="ChartDocRetAlert" runat="server" Height="300px" Width="470px">
                        <Titles>
                            <asp:Title Name="Title1" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                LegendStyle="Table" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" IsVisibleInLegend="true" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BackColor="White" />
                        </ChartAreas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <%--Chart Employee--%>
        <div style="background-color: White; border: solid 1px grey; float: left; margin-top: 5px;
            font-size: 12px;">
            <asp:UpdatePanel ID="updEmpSerach" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="DivStatus" style="width: 450px; background-color: White; border: solid 0px grey;
                        font-size: 12px;">
                        <div style="float: left; width: 350px;">
                            <div style="float: left; width: 100px">
                           
                                <asp:RadioButton Width="94px" runat="server" GroupName="Emp" AutoPostBack="true"
                                     ID="rdbNationality" Text='<%$Resources:ControlsCommon,Nationality%>'
                                    ForeColor="DarkBlue" OnCheckedChanged="rdbNationality_CheckedChanged" />
                            </div>
                            <div style="float: left; width: 100px">
                                <asp:RadioButton runat="server" GroupName="Emp" ID="rdbCountry" AutoPostBack="true"
                                    Text='<%$Resources:ControlsCommon,Country%>' ForeColor="DarkBlue" OnCheckedChanged="rdbCountry_CheckedChanged" />
                            </div>
                            <div style="float: left; width: 100px">
                                <asp:RadioButton runat="server" GroupName="Emp" AutoPostBack="true" ID="rdbDepartment"
                                    Text='<%$Resources:ControlsCommon,Department%>' ForeColor="DarkBlue" OnCheckedChanged="rdbDepartment_CheckedChanged" />
                            </div>
                        </div>
                        <div style="float: left; width: 100px; margin-top: 2px">
                            <asp:DropDownList ID="ddlChartType" Style="width: 90px; margin-left: 0px;" runat="server"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                                <asp:ListItem Value="34" Text='<%$Resources:ControlsCommon,Pyramid%>'></asp:ListItem>
                                <asp:ListItem Value="18" Text='<%$Resources:ControlsCommon,Doughnut%>'> </asp:ListItem>
                                <asp:ListItem Value="3" Text='<%$Resources:ControlsCommon,Line%>'></asp:ListItem>
                                <asp:ListItem Value="17" Text='<%$Resources:ControlsCommon,Pie%>'></asp:ListItem>
                                <asp:ListItem Value="7" Text='<%$Resources:ControlsCommon,Bar%>'> </asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="updEmpChart" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Chart ID="ChartEmployee" runat="server" Height="400px" Width="470px">
                        <Titles>
                            <asp:Title Name="Title1" />
                        </Titles>
                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Bottom" IsTextAutoFit="False" Name="Default"
                                Enabled="true" LegendStyle="Table" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Default" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" BackColor="White" />
                        </ChartAreas>
                    </asp:Chart>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
