﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Announcements.aspx.cs" Inherits="Public_Announcements" Title="Announcements" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li class="selected"><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
           
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="pnlHeading" runat="server">
        <ContentTemplate>
            <table style="width: 100%" cellpadding="4" cellspacing="0">
                <tr>
                    <td>
                        <div style="display: none">
                            <asp:Button ID="btnsubmit" runat="server" Text="submit" />
                        </div>
                        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                            BorderStyle="None">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                            Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </td>
                </tr>
                <tr>
                    <td width="100%">
                        <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:FormView ID="fvAnnouncements" runat="server" Width="100%" DataKeyNames="AnnouncementId,CompanyId,AnnouncementGroupID"
                                    CellSpacing="1" OnItemCommand="fvAnnouncements_ItemCommand" OnDataBound="fvAnnouncements_DataBound"
                                    CssClass="labeltext">
                                    <EditItemTemplate>
                                        <div style="width: 100%;">
                                            <div style="width: 100%">
                                                <div style="width: 25%; float: left">
                                                    Company
                                                </div>
                                                <div style="width: 75%; float: left">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist" DataTextField="Name"
                                                        DataValueField="CompanyId" AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged"
                                                        Width="200px" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <br />
                                            <div style="width: 100%;">
                                                <div style="width: 25%; float: left">
                                                    From Date
                                                </div>
                                                <div style="width: 75%; float: left; padding-top: 5px">
                                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                        Text='<%# Eval("StartDate") %>' Enabled='<%#DisableEnable(Eval("StartDate")) %>'></asp:TextBox>
                                                    <asp:ImageButton ID="ibtnLeaveFromdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CausesValidation="false" CssClass="imagebutton" Enabled='<%#DisableEnable(Eval("StartDate")) %>' />
                                                    <AjaxControlToolkit:CalendarExtender ID="extenderLeaveFromDate" runat="server" TargetControlID="txtFromDate"
                                                        PopupButtonID="ibtnLeaveFromdate" Format="dd/MM/yyyy" Enabled="true">
                                                    </AjaxControlToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="cvStartDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtFromDate"
                                                        Display="Dynamic" ClientValidationFunction="validateAnnouncementFromDate" ValidationGroup="Submit"
                                                        CssClass="error"></asp:CustomValidator>
                                                </div>
                                            </div>
                                            <div style="width: 100%; padding-top: 5px">
                                                <div style="width: 25%; float: left">
                                                    To Date
                                                </div>
                                                <div style="width: 75%; float: left; padding-top: 5px">
                                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                        Text='<%# Eval("EndDate") %>'></asp:TextBox>
                                                    <asp:ImageButton ID="ibtnLeaveTodate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                        CssClass="imagebutton" CausesValidation="false" />
                                                    <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate"
                                                        PopupButtonID="ibtnLeaveTodate" Format="dd/MM/yyyy">
                                                    </AjaxControlToolkit:CalendarExtender>
                                                    <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtToDate"
                                                        Display="Dynamic" ClientValidationFunction="valRequestFromToDate" ValidationGroup="Submit"
                                                        CssClass="error"></asp:CustomValidator>
                                                    <asp:CustomValidator ID="cvCompare" runat="server" ValidationGroup="Submit" ErrorMessage="End date should be later than start date"
                                                        ControlToValidate="txtToDate" ClientValidationFunction="checkFromToDate" CssClass="error"
                                                        Display="Dynamic"></asp:CustomValidator>
                                                </div>
                                            </div>
                                            <div style="width: 100%; padding-top: 5px">
                                                <div style="width: 25%; float: left">
                                                    Employees
                                                </div>
                                                <div style="width: 75%; float: left; padding-top: 5px">
                                                    <div>
                                                        <fieldset>
                                                            <asp:RadioButton ID="rbtnGeneral" runat="server" AutoPostBack="True" Checked="True"
                                                                GroupName="D" OnCheckedChanged="rbtnGeneral_CheckedChanged" Text="All" />
                                                            <asp:RadioButton ID="rbtnDepartmentWise" runat="server" AutoPostBack="True" GroupName="D"
                                                                OnCheckedChanged="rbtnDepartmentWise_CheckedChanged" Text="Department Wise" />
                                                            <asp:RadioButton ID="rbtnEmployeeWise" runat="server" AutoPostBack="True" GroupName="D"
                                                                OnCheckedChanged="rbtnEmployeeWise_CheckedChanged" Text="Employee Wise" />
                                                        </fieldset>
                                                    </div>
                                                    <div id="divEmployees" runat="server" style="margin-top: 5px;">
                                                        <div style="float: left; width: 240px; position: relative; height: 170px; border: solid 1px #ccc;">
                                                            <asp:UpdatePanel ID="upnlDepartments" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div style="padding-left: 3px;">
                                                                        <asp:CheckBox ID="chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged"
                                                                            Text="All" />
                                                                    </div>
                                                                    <div id="DepEmp" class="save_scroll" style="width: 240px; overflow: auto; height: 140px;
                                                                        position: relative; border-top: solid 1px #ccc;">
                                                                        <div>
                                                                            <asp:CheckBoxList ID="chklstDepartments" runat="server" AutoPostBack="True" OnSelectedIndexChanged="chklstDepartments_SelectedIndexChanged">
                                                                            </asp:CheckBoxList>
                                                                            <asp:CheckBoxList ID="chklstEmployees" runat="server" AutoPostBack="True" OnSelectedIndexChanged="chklstEmployees_SelectedIndexChanged">
                                                                            </asp:CheckBoxList>
                                                                        </div>
                                                                    </div>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="rbtnDepartmentWise" EventName="CheckedChanged" />
                                                                    <asp:AsyncPostBackTrigger ControlID="rbtnEmployeeWise" EventName="CheckedChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                        <div style="float: left; width: 10px;">
                                                            &nbsp;
                                                        </div>
                                                        <div style="float: left; width: 230px; position: relative; height: 170px; border: solid 1px #ccc;">
                                                            <div class="bold" style="padding-left: 3px;">
                                                                Employees
                                                            </div>
                                                            <div id="SelectedEmployees" class="save_scroll" style="width: 230px; overflow: auto;
                                                                height: 140px; position: relative; border-top: solid 1px #ccc;">
                                                                <div>
                                                                    <asp:UpdatePanel ID="upnlSelectedEmployees" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <asp:Repeater ID="rptrSelectedEmployees" runat="server">
                                                                                <ItemTemplate>
                                                                                    <div style="padding-left: 5px;">
                                                                                        <%# Eval("EmployeeName") %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="chkAll" EventName="CheckedChanged" />
                                                                            <asp:AsyncPostBackTrigger ControlID="chklstDepartments" EventName="SelectedIndexChanged" />
                                                                            <asp:AsyncPostBackTrigger ControlID="chklstEmployees" EventName="SelectedIndexChanged" />
                                                                            <asp:AsyncPostBackTrigger ControlID="rbtnDepartmentWise" EventName="CheckedChanged" />
                                                                            <asp:AsyncPostBackTrigger ControlID="rbtnEmployeeWise" EventName="CheckedChanged" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="height: 1px; clear: both;">
                                                            &nbsp;
                                                        </div>
                                                        <asp:Label ID="lblNoSelection" runat="server" CssClass="error" Visible="false"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="width: 100%; padding-top: 5px">
                                                <div style="width: 25%; float: left">
                                                    Message
                                                </div>
                                                <div style="width: 75%; float: left; padding-top: 5px">
                                                    <asp:TextBox ID="txtMessage" runat="server" CssClass="textbox_mandatory" Height="50px"
                                                        onchange="RestrictMulilineLength(this, 2000);" onkeyup="RestrictMulilineLength(this, 2000);"
                                                        Text='<%# Eval("Message") %>' TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtMessage"
                                                        CssClass="error" Display="Dynamic" ErrorMessage="Please enter message." SetFocusOnError="True"
                                                        ValidationGroup="Submit"></a></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div style="width: 100%; padding-top: 5px">
                                                <div style="width: 25%; float: left">
                                                </div>
                                                <div style="width: 75%; float: right; padding-top: 5px">
                                                    <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" CssClass="btnsubmit"
                                                        Text="Submit" CommandName="Add" UseSubmitBehavior="false" Width="75px" CommandArgument='<%# Eval("AnnouncementId") %>' />
                                                    <asp:Button ID="btnCancel" runat="server" Width="75px" CssClass="btnsubmit" CommandName="CancelRequest"
                                                        Text="Cancel" CausesValidation="false" />
                                                </div>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <div style="width: 100%">
                                                <div style="width: 100%">
                                                    <div style="width: 25%; float: left">
                                                       Company
                                                    </div>
                                                    <div style="width: 75%; float: left"> 
                                                     <%# Eval("Company")%>
                                                    </div>
                                                </div>
                                                <div style="width: 100%">
                                                    <div style="width: 25%; float: left">
                                                       From Date
                                                    </div>
                                                    <div style="width: 75%; float: left"> 
                                                      <%# Eval("StartDate") %>
                                                    </div>
                                                </div>
                                                
                                                <div style="width: 100%">
                                                    <div style="width: 25%; float: left">
                                                         To Date
                                                    </div>
                                                    <div style="width: 75%; float: left"> 
                                                    <%# Eval("EndDate") %>
                                                    </div>
                                                </div>
                                           
                                           <div style="width: 100%">
                                                    <div style="width: 25%; float: left">
                                                         Message
                                                    </div>
                                                    <div style="width: 75%; float: left"> 
                                                     <%# Eval("Message") %>
                                                    </div>
                                                </div>
                                           
                                           
                                            </div>
                                          
                                      
                                    </ItemTemplate>
                                </asp:FormView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="fvAnnouncements" EventName="ItemCommand" />
                                <asp:AsyncPostBackTrigger ControlID="gvAnnouncements" EventName="RowCommand" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td valign="middle">
                        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:GridView ID="gvAnnouncements" runat="server" AutoGenerateColumns="False" GridLines="None"
                                    Width="100%" DataKeyNames="AnnouncementId" CellSpacing="0" CellPadding="0" Style="margin-right: 0px"
                                    OnRowCommand="gvAnnouncements_RowCommand" CssClass="labeltext">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <div style="width: 100%; padding-top: 10px; float: left;">
                                                    <div style="width: 100%; float: left;" class="datalistheader">
                                                        <div style="width: 10%; float: left;">
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" onclick="selectAll(this.id,'chkAnnouncements')" />
                                                        </div>
                                                        <div style="float: left; width: 35%">
                                                            Announcements
                                                        </div>
                                                        <div style="float: left; width: 20%">
                                                            Company
                                                        </div>
                                                        <div style="float: left; width: 15%">
                                                            From Date
                                                        </div>
                                                        <div style="float: left; width: 15%">
                                                            To Date
                                                        </div>
                                                        <div style="float: left; width: 5%">
                                                        </div>
                                                    </div>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div style="width: 100%; padding-left: 5px">
                                                    <div style="float: left; width: 10%;">
                                                        <asp:CheckBox ID="chkAnnouncements" runat="server" />
                                                    </div>
                                                    <div style="float: left; width: 35% ;word-break:break-all;">
                                                        <%#Eval("Message")%>
                                                    </div>
                                                    <div style="float: left; width: 20%">
                                                        <%#Eval("Company")%>&nbsp;
                                                    </div>
                                                    <div style="float: left; width: 15%">
                                                        <%#Eval("StartDate")%>&nbsp;
                                                    </div>
                                                    <div style="float: left; width: 15%">
                                                        <%#Eval("EndDate")%>
                                                    </div>
                                                    <div style="float: left; width: 5%; padding-top: 5px">
                                                        <asp:ImageButton ID="ibtnEdit" runat="server" CommandArgument='<%# Eval("Announcementid") %>'
                                                            CommandName="_Edit" ImageUrl="~/images/edit_icon_popup.jpg" ToolTip="Edit"></asp:ImageButton>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <uc:Pager ID="AnnouncementPager" runat="server" />
                                <center>
                                    <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label></center>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="fvAnnouncements" EventName="ItemCommand" />
                                <asp:AsyncPostBackTrigger ControlID="gvAnnouncements" EventName="RowCommand" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upnlMenus" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/announce_add.png" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkAdd" runat="server" Text="Add" CausesValidation="False" CssClass="labeltext"
                        OnClick="lnkAdd_Click"></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <img src="../images/announce_view.png" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkView" runat="server" Text="View" CausesValidation="False"
                        OnClick="lnkView_Click" CssClass="labeltext"></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <img src="../images/Delete.png" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CausesValidation="False"
                        CssClass="labeltext" OnClick="lnkDelete_Click"></asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
  <style type="text/css">
  select
  {
  	margin-left:0px;
  }
  	
</style>  
    
</asp:Content>
