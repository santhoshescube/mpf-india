﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="MailSettings.aspx.cs" Inherits="Public_MailSettings" Title="Mail Settings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li class="selected"><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            <%-- <li><a href="Projects.aspx">
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:SettingsCommon,Projects%>'></asp:Literal></a></li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div runat="server" id="MailSetting" style="width: 100%; float: left;">
        <div style="width: 100%; float: left;">
            <div style="display: none">
                <asp:Button ID="btnSubmit" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="upnlMessage" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <div style="width: 100%; float: left;">
            <asp:UpdatePanel ID="upMailSettings" runat="server">
                <ContentTemplate>
                    <asp:FormView ID="fvMailSettings" runat="server" Width="100%" OnDataBound="fvMailSettings_DataBound"
                        OnItemCommand="fvMailSettings_ItemCommand" DataKeyNames="AccountType,EnableSsl,Password"
                        CellSpacing="0">
                        <ItemTemplate>
                            <div style="width: 100%; float: left;">
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        Type
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:RadioButtonList ID="rblType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True"
                                            OnSelectedIndexChanged="rblType_SelectedIndexChanged">
                                            <asp:ListItem Value="1" Selected="True">Email</asp:ListItem>
                                            <asp:ListItem Value="2">SMS</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        Username
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtUsername" runat="server" CssClass="textbox" Text='<%# Eval("UserName")%>'
                                            MaxLength="50" Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsername" runat="server" ControlToValidate="txtUsername"
                                            ValidationGroup="SaveSettings" CssClass="error" Display="Dynamic" ErrorMessage="Please enter username"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revUsername" runat="server" ControlToValidate="txtUsername"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter valid email" SetFocusOnError="True"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="SaveSettings"></asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        Password
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" Text='<%# Eval("Password") %>'
                                            TextMode="Password" Width="200px" MaxLength="50"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter password" ValidationGroup="SaveSettings"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Label ID="lblIncomingServer" Text="Incoming Server" runat="server"></asp:Label>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtIncomingServer" MaxLength="50" runat="server" CssClass="textbox"
                                            Text='<%# Eval("IncomingServer") %>' Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvIncomingServer" runat="server" ControlToValidate="txtIncomingServer"
                                            CssClass="error" ErrorMessage="Please enter incoming server name" ValidationGroup="SaveSettings"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        <asp:Label ID="lblOutgoingServer" runat="server" Text="Outgoing Server"></asp:Label>
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:TextBox ID="txtOutgoingServer" MaxLength="50" runat="server" CssClass="textbox"
                                            Text='<%# Eval("OutgoingServer") %>' Width="200px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvOutgoingServer" runat="server" ControlToValidate="txtOutgoingServer"
                                            CssClass="error" ErrorMessage="Please enter outgoing server name" ValidationGroup="SaveSettings"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        Port Number
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <div style="width: 100%; float: left;">
                                            <div style="width: 60; float: left;">
                                                <asp:TextBox ID="txtPortNumber" runat="server" MaxLength="50" CssClass="textbox"
                                                    Text='<%# Eval("PortNumber") %>' Width="50px"></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbPortNumber" runat="server" FilterMode="ValidChars"
                                                    FilterType="Numbers" TargetControlID="txtPortNumber" />
                                            </div>
                                            <div style="width: 5px; float: left;">
                                                &nbsp;
                                            </div>
                                            <div style="width: auto; float: left;">
                                                <asp:CheckBox ID="chkEnableSSL" runat="server" Text="Enable SSL" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <div style="width: 150px; float: left;" class="trLeft">
                                        &nbsp;
                                    </div>
                                    <div style="width: auto; float: left;" class="trRight">
                                        <asp:Button ID="btnSaveSettings" runat="server" Text="Submit" CssClass="btnsubmit"
                                            ValidationGroup="SaveSettings" CommandArgument="MailId" CommandName="SAVE" />
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:FormView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"
        Visible="False"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div style="width: 100%; float: left;">
        <div style="vertical-align: top; padding: 3px; float: left; width:10%;">
            <img src="../images/Bulbe 14px.png" />
        </div>
        <div style="text-align: justify; float: left; width:85%;">
            <span class="labeltext">Mail settings refers to server connectivity details which are
                essential for configuring user email and provider within the system software.</span>
        </div>
    </div>
</asp:Content>
