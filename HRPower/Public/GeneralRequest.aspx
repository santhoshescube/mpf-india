﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="GeneralRequest.aspx.cs" Inherits="Public_GeneralRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=divSingleView.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>General Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>

    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png" Width= "22px"
            meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <asp:UpdatePanel ID="upAddRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAddRequest" runat="server" style="width: 100%; height: 75px; float: left;
                padding-top: 20px;">
                <asp:HiddenField ID="hfRequestID" runat="server" />
                <asp:HiddenField ID="hfReqTypeID" runat="server" />
                <div style="width: 100%; height: 30px; float: left;">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="RequestType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        <asp:UpdatePanel ID="updtype" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlType" runat="server" Width="200px" AutoPostBack="true" DataTextField="RequestType"
                                    OnSelectedIndexChanged="ddlType_SelectedIndexChanged" DataValueField="RequestId">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlType"
                                    InitialValue="-1" Display="Dynamic" meta:resourcekey="PleaseSelectType" SetFocusOnError="True"
                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Literal ID="litDate" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                        Style="margin-right: 10px"></asp:TextBox>
                    <asp:ImageButton ID="ImageButton1" style="margin-top:7px;" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                        CssClass="imagebutton" />
                    <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDate"
                        PopupButtonID="ImageButton1" Format="dd/MM/yyyy">
                    </AjaxControlToolkit:CalendarExtender>
                </div>
           
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
               <ContentTemplate>
                
                    <div class="trLeft" visible="false" id="divLeft" style="width: 25%; height: 30px; float: left;" runat="server">
              
                </div>
                <div class="trRight"  visible="false" id="divRight" style="width: 70%; height: 30px; float: left;  height: 120Px;overflow: auto;" runat="server">
                  <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                   <ContentTemplate>--%>
                   <label id="lblUnclosedLoans"  runat="server" style="color:Red">Unclosed Loans</label>
                    <asp:GridView ID="gdvPendingLoan" runat="server" AutoGenerateColumns="False" 
                        CellPadding="5" ForeColor="#333333" BorderColor="#3399FF" 
                        BorderStyle="Solid" BorderWidth="1px" Width="300px">
                        <RowStyle  ForeColor="#333333"  />
                        <Columns>
                            <asp:BoundField DataField="LoanNumber" HeaderText="Loan Number" 
                                SortExpression="LoanNumber" />
                            <asp:BoundField DataField="BalanceAmount" HeaderText="Balance Amount" 
                                SortExpression="BalanceAmount" />
                        </Columns>
                       
                    </asp:GridView> 
                   <%--     </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </div>
                  </ContentTemplate>
                   </asp:UpdatePanel>
                
                
                
              
                <asp:UpdatePanel ID="updFlightNumber" runat="server">
                    <ContentTemplate>
                        <div id="divFlightNumber" runat="server" style="display: none">
                            <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="FlightNumber"></asp:Literal>
                            </div>
                            <div class="trRight" style="width: 70%; height: 30px; float: left;">
                                <asp:TextBox ID="txtFlightNumber" runat="server" CssClass="textbox" Width="150px"
                                    Style="margin-right: 10px"></asp:TextBox>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlAssetType" runat="server">
                    <ContentTemplate>
                        <div id="divAsset" runat="server" style="display: none">
                            <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="AssetType"></asp:Literal>
                            </div>
                            <div class="trRight" style="width: 70%; height: 30px; float: left;">
                                <asp:DropDownList ID="ddlAssetType" runat="server" Width="200px" AutoPostBack="true"
                                    DataTextField="BenefitTypeName" OnSelectedIndexChanged="ddlAssetType_SelectedIndexChanged"
                                    DataValueField="BenefitTypeID">
                                </asp:DropDownList>
                            </div>
                            <%--    <div style="padding-left: 10px;">
                                <asp:GridView ID="gvAssetDetails" runat="server" AutoGenerateColumns="false" Width="96%"
                                    HeaderStyle-Height="10" BorderColor="#307296">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <Columns>
                                        <asp:BoundField DataField="BenefitTypeName" HeaderText="Name" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="ReferenceNumber" HeaderText="Number" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="ExpiryDate" HeaderText="Expiry Date" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="PurchaseValue" HeaderText="Purchase Value" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                        <asp:BoundField DataField="Status" HeaderText="Asset Status" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Font-Size="Small" />
                                    </Columns>
                                </asp:GridView>
                            </div>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlLeave" runat="server">
                    <ContentTemplate>
                        <%--<div id="divLeave" runat="server" style="display: none">
                            <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                            </div>--%>
                            <%--<div class="trRight" style="width: 70%; height: 30px; float: left;">
                                <asp:DropDownList ID="ddlLeaveType" runat="server" Width="200px" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlLeaveType_SelectedIndexChanged">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Sick"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="Vacation"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="rfvl" runat="server" ControlToValidate="ddlLeaveType"
                                    InitialValue="0" Display="Dynamic" ErrorMessage="*" SetFocusOnError="True" CssClass="error"
                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </div>--%>
                   <%--     </div>--%>
                        <div id="divRejoinDate" runat="server" style="display: none;">
                            <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                                <asp:Literal ID="Literal20" runat="server" meta:resourcekey="RejoinDate"></asp:Literal>
                            </div>
                            <div class="trRight" style="width: 70%; height: 30px; float: left;">
                                <asp:HiddenField ID="hdnID" runat="server" />
                                <asp:TextBox ID="txtRejoinDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                    OnTextChanged="txtRejoinDate_OnTextChanged" AutoPostBack="true" Style="margin-right: 10px"></asp:TextBox>
                                <asp:ImageButton ID="ibtnRejoin" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CssClass="imagebutton" />
                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtRejoinDate"
                                    PopupButtonID="ibtnRejoin" Format="dd/MM/yyyy">
                                </AjaxControlToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfvl2" runat="server" ControlToValidate="txtRejoinDate"
                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </div>
                            <div style="width: 70%; height: 30px; float: left; margin-left: 250px; line-height: 25px;
                                font-family: Verdana; font-size: 12px; font-weight: 200; color: #2AB0FF;">
                                <asp:Label ID="lblVacation" runat="server"></asp:Label></div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; padding-top: 25px;">
                    <asp:UpdatePanel ID="updRemarks" runat="server">
                        <ContentTemplate>
                            <asp:Literal ID="litRemarks" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="trRight" style="width: 70%; float: left; padding-top: 25px;">
                    <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="71%"
                        TextMode="MultiLine" Height="100px"></asp:TextBox>
                        
                </div>
                
                
                
                <div style="width: 75%; height: 30px; float: left; text-align: right;">
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                        Width="75px" ValidationGroup="Submit" OnClick="btnSubmit_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                        Width="75px" Style="margin-left: 10px;" OnClick="btnCancel_Click"  CausesValidation="false"/><%--Text="Cancel"--%>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upViewRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divViewRequest" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <asp:DataList ID="dlRequest" runat="server" Width="100%" DataKeyField="requestId"
                    CssClass="labeltext" OnItemCommand="dlRequest_ItemCommand" OnItemDataBound="dlRequest_ItemDataBound">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <div style="width: 100%; height: 30px; float: left;">
                            <div style="width: 5%; height: 30px; float: left; margin-left: 3.3%">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkRequest')" />
                            </div>
                            <div style="width: 90%; height: 30px; float: left;">
                                <%-- Select All--%>
                                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; height: 50px; float: left;">
                            <div class="trLeft" style="width: 5%; height: 30px; float: left; margin-left: 2%">
                                <asp:CheckBox ID="chkRequest" runat="server" /></div>
                            <div style="width: 30%; height: 30px; float: left; margin-right: 50%">
                                <asp:LinkButton ID="lnkRequest" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                    ToolTip="View" CommandName="_View" CausesValidation="False"><%# Eval("Date")%></asp:LinkButton>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Edit" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/edit.png"
                                    ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton><%--ToolTip="Edit"--%>
                            </div>
                            <div style="width: 4%; height: 30px; float: left;">
                                <asp:ImageButton ID="btnCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                    CommandName="_Cancel" Style="display: block" CssClass="linkbutton" ImageUrl="~/images/cancel.png"
                                   ToolTip='<%$Resources:ControlsCommon,Cancel%>'></asp:ImageButton><%--ToolTip="Request For Cancel"--%>
                            </div>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Type--%>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="RequestType"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("RequestType")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Date--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("FromDate")%>
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Requested To--%>
                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Requested")%>
                            <asp:Label ID="lbRequested" runat="server" Text=' <%# Eval("RequestedTo") %>' Visible="false"> </asp:Label>
                            <asp:HiddenField ID="hfType" runat="server" Value='<%# Eval("RequestTypeId") %>' />
                        </div>
                        <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 10%">
                            <%--Status--%>
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left;">
                            :
                            <%# Eval("Status")%>
                            <asp:HiddenField ID="hfForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                            <asp:HiddenField ID="hfStatusID" runat="server" Value='<%# Eval("StatusID") %>' />
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="RequestPager" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upSingleView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleView" runat="server" style="width: 100%; height: 300px; float: left;
                padding-top: 20px;">
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <asp:Literal ID="Literal91" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 55%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblRequestedBy" runat="server"></asp:Label>
                </div>
                <%--<div class="trLeft" style="width: 8%; height: 30px; float: left;">
                    <asp:ImageButton ID="imgBack2" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                </div>--%>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="RequestType"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblType" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>
                <div id="divFlightSingleView" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="FlightNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblFlightNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div id="divAssetSingleView" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="AssetType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblAssetType" runat="server"></asp:Label>
                    </div>
                    <%--   <div style="padding-left: 104px;">
                        <asp:GridView ID="gvAssetDisp" runat="server" AutoGenerateColumns="false" Width="90%"
                            HeaderStyle-Height="10" BorderColor="#307296">
                            <HeaderStyle CssClass="datalistheader" />
                            <Columns>
                                <asp:BoundField DataField="BenefitTypeName" HeaderText="Name" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="Number" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="ExpiryDate" HeaderText="Expiry Date" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="PurchaseValue" HeaderText="Purchase Value" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="Status" HeaderText="Asset Status" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                            </Columns>
                        </asp:GridView>
                    </div>--%>
                </div>
                <div id="divRejoinSingleView" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblLeaveType" runat="server"></asp:Label>
                    </div>
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="RejoinDate"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblRejoinDate" runat="server"></asp:Label>
                    </div>
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left; color: #2AB0FF;">
                        <asp:Label ID="lblVacationView" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%;">
                    <%--padding-top: 20px;--%>
                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    <%--padding-top: 20px;--%>
                    :
                    <asp:Label ID="lblRequestedToNames" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Reason"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: auto; float: left; height: 60px;
                    overflow-y: scroll; overflow-x: hidden; word-break: break-all;">
                    :
                    <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                    <asp:TextBox ID="txtEditRemarks" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                        Width="200px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                </div>
                <div id="divForwardRequest" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left; margin-left: 5%">
                        <%--New Status--%>
                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="NewStatus"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left;">
                        <asp:DropDownList ID="ddlForwardStatus" runat="server" Width="202px">
                        </asp:DropDownList>
                    </div>
                    <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                        <asp:Button ID="btnForwardExpense" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                            Width="75px" />
                        <%--Text="Submit"--%>
                        <asp:Button ID="btnForwardCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                            Width="75px" Style="margin-left: 10px; margin-right: 35%;" /><%--Text="Cancel"--%>
                    </div>
                </div>
                <div class="trLeft" style="width: 75%; height: 30px; float: left; margin-left: 5%">
                    <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                        OnClick="lbReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 92%; height: auto; float: left; overflow-y: scroll;
                    margin-left: 5%; overflow-x: hidden; word-break: break-all;" id="trReasons" runat="server">
                    <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 25.5%; height: 30px; float: left; margin-left: 5%;">
                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="Div1" class="trRight" style="width: 67%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td>
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upApproveRequest" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divApproveRequest" runat="server" style="width: 100%; height: 300px; float: left;
                padding-top: 20px;">
                <asp:HiddenField ID="hfTypeID" runat="server" />
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Requested By--%>
                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" style="width: 60%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveRequestedBy" runat="server"></asp:Label>
                    <div style="float: right; color: Gray;">
                        <img src="../images/pendingdocs.png" id="imgPending" runat="server" />
                        <asp:LinkButton ID="lnkShowDocs" runat="server" OnClick="lnkShowDocs_Click" Text="Pending Documents / Assets"></asp:LinkButton>
                    </div>
                </div>
                <%--<div class="trRight" style="width: 10%; height: 30px; float: left;">
                    <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                        ToolTip="Back to previous page" OnClick="imgback_Click" />
                </div>--%>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Type--%>
                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="RequestType"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveType" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Date--%>
                    <asp:Literal ID="litApDate" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblApproveDate" runat="server"></asp:Label>
                </div>
                <div id="divFlightApproval" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="FlightNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblApproveFlightNo" runat="server"></asp:Label>
                    </div>
                </div>
                <div id="divAssetApprove" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="AssetType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblApproveAssetType" runat="server"></asp:Label>
                    </div>
                    <%--   <div style="padding-left: 104px;">
                        <asp:GridView ID="gvApproveAssetDetails" runat="server" AutoGenerateColumns="false"
                            Width="90%" HeaderStyle-Height="10" BorderColor="#307296">
                            <HeaderStyle CssClass="datalistheader" />
                            <Columns>
                                <asp:BoundField DataField="BenefitTypeName" HeaderText="Name" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="ReferenceNumber" HeaderText="Number" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="PurchaseDate" HeaderText="Purchase Date" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="ExpiryDate" HeaderText="Expiry Date" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="PurchaseValue" HeaderText="Purchase Value" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                                <asp:BoundField DataField="Status" HeaderText="Asset Status" ItemStyle-HorizontalAlign="Center"
                                    ItemStyle-Font-Size="Small" />
                            </Columns>
                        </asp:GridView>
                    </div>--%>
                </div>
                <div id="divLeaveApprove" runat="server" style="display: none">
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Literal ID="Literal27" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblLeaveType1" runat="server"></asp:Label>
                    </div>
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                        <asp:Literal ID="Literal28" runat="server" meta:resourcekey="RejoinDate"></asp:Literal>
                    </div>
                    <div class="trRight" style="width: 70%; height: 30px; float: left;">
                        :
                        <asp:Label ID="lblRejoinDate1" runat="server"></asp:Label>
                    </div>
                    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    </div>
                    <div class="trRight" style="width: 60%; height: 30px; float: left; color: #2AB0FF;">
                        <asp:Label ID="lblVacationApprove" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="trLeft" style="word-break: break-all; width: 25%; height: auto; float: left;
                    overflow: auto">
                    <%--REason--%>
                    <asp:Literal ID="litApReason" runat="server" meta:resourcekey="Reason"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: auto; float: left; overflow: auto">
                    :
                    <asp:Label ID="lblApReason" runat="server"></asp:Label>
                </div>
                
                
                
               <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                   <ContentTemplate>
                
                      <div class="trLeft" visible="false" id="divLeft1" style="width: 25%; height: 30px; float: left;" runat="server">
               <label id="Label1"  runat="server" style="font-weight:normal">Unclosed Loans</label>
                </div>
                <div class="trRight"  visible="false" id="divRight1" style="width: 70%; height: 30px; float: left;  height: 120px;overflow: auto;" runat="server">
                
                  
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                        CellPadding="5" ForeColor="#333333" BorderColor="#3399FF" 
                        BorderStyle="Solid" BorderWidth="1px" Width="300px">
                        <RowStyle  ForeColor="#333333"  />
                        <Columns>
                            <asp:BoundField DataField="LoanNumber" HeaderText="Loan Number" 
                                SortExpression="LoanNumber" />
                            <asp:BoundField DataField="BalanceAmount" HeaderText="Balance Amount" 
                                SortExpression="BalanceAmount" />
                        </Columns>
                       
                    </asp:GridView> 
                  
                </div>
               </ContentTemplate>
                </asp:UpdatePanel>
                
                
                
                
                
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Status--%>
                    <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    :
                    <asp:Label ID="lblCurrentStatus" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--New Status--%>
                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="NewStatus"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:DropDownList ID="ddlApproveStatus" runat="server" Width="202px" DataTextField="Description"
                        DataValueField="StatusId">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvddlApproveStatus" runat="server" CssClass="error"
                        meta:resourcekey="PleaseselectaStatus" Display="Dynamic" ControlToValidate="ddlApproveStatus"
                        ValidationGroup="SubmitApprove" InitialValue="-1"></asp:RequiredFieldValidator><%--ErrorMessage="Please select a Status"--%>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <%--Remarks--%>
                    <asp:Literal ID="Literal25" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 55px; float: left; overflow: auto">
                    <asp:TextBox ID="txtApproveRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                        TextMode="MultiLine" Height="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvtxtApproveRemarks" runat="server" meta:resourcekey="Pleaseenterremarks"
                        ControlToValidate="txtApproveRemarks" Display="Dynamic" ValidationGroup="SubmitApprove"
                        CssClass="error"></asp:RequiredFieldValidator><%--ErrorMessage="Please enter remarks."--%>
                </div>
                <div class="trLeft" style="width: 75%; height: 30px; float: left;">
                    <asp:LinkButton ID="lbApproveReason" meta:resourcekey="ClicktoViewApprovalDetails"
                        runat="server" OnClick="lbApproveReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfApprovelink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 90%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden; word-break: break-all;" id="trApproveReasons" runat="server">
                    <asp:GridView ID="gvApproveReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                    <asp:Label ID="lbl2" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="div2" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl2" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                            <asp:Table Width="99%">
                                <tr style="width: 99%; color: #307296;">
                                    <td>
                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                    </td>
                                </tr>
                            </asp:Table>
                        </ItemTemplate>
                    </asp:DataList></div>
                 
                 
                 
                 
           
                   
                 
                 
                 
                 
                 
                 
                <div id="divApproveTheRequest" runat="server" style="display: none; width: 100%;
                    height: 30px; float: left; text-align: right; margin-top: 20px;">
                    <asp:Button ID="btnApproveRequest" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                        Width="75px" ValidationGroup="SubmitApprove" OnClick="btnApproveRequest_Click" /><%--Text="Submit"--%>
                    <asp:Button ID="btnApproveCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                        Width="75px" Style="margin-left: 10px; margin-right: 35%;" OnClick="btnApproveCancel_Click" /><%--Text="Cancel"--%>
                </div>
                
                
                
  
                
              
                
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                   
                
                <div id="divCancelTheRequest" runat="server" style="display: none">
                    <div style="width: 100%; height: 30px; float: left; text-align: right; margin-top: 20px;">
                        <asp:Button ID="btnCancelSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                            Width="75px" OnClick="btnCancelExpense_Click" /><%--Text="Submit"--%>
                        <asp:Button ID="btnCancelCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>'
                            Width="75px" Style="margin-left: 10px; margin-right: 35%;" OnClick="btnCancelCancel_Click" />
                        <%--Text="Cancel"--%>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-----------Show Pending Documents-----------%>
    <asp:UpdatePanel ID="upnlPendingDocs" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="divDocs" runat="server" style="display: none;">
                <div id="popupmainwrap">
                    <div id="popupmain">
                        <div id="header11">
                            <div id="hbox1">
                                <div id="headername">
                                    <asp:Label ID="Label4" runat="server" Text="Pending Documents"></asp:Label>
                                </div>
                            </div>
                            <div id="hbox2">
                                <div id="close1">
                                    <a href="">
                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                            CausesValidation="false" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="contentwrap">
                            <div id="content">
                                <asp:UpdatePanel ID="updWidget" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divPendingDocs" style="max-height: 170px; padding-top: 10px; padding-bottom: 10px;
                                            text-align: left; width: 500px; overflow: auto; margin-left: 5px; margin-right: 5px;">
                                            <asp:DataList RepeatLayout="Table" SeparatorStyle-BackColor="AliceBlue" ID="dlDocs"
                                                Width="100%" runat="server" RepeatColumns="1" BackColor="White" BorderColor="#CCCCCC"
                                                BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both">
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <ItemStyle ForeColor="#000066" />
                                                <SeparatorStyle BackColor="AliceBlue" />
                                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                <ItemTemplate>
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%; overflow: hidden;" align="left">
                                                                <asp:Label ID="lblWidget" Height="15px" CssClass="labeltext" runat="server" Text='<%# Eval("Docs") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                            <asp:Label ID="lblMessage" runat="server" Text="No Pending documents" Visible="false"></asp:Label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeDocs" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="divDocs" TargetControlID="btnMenu" PopupDragHandleControlID="tblDragHandle">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Button ID="btnMenu" runat="server" Style="display: none" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upMessage" runat="server">
        <ContentTemplate>
            <div>
                <div style="display: none">
                    <asp:Button ID="btnsubmit1" runat="server" meta:resourcekey="Submit" /><%--Text="submit"--%>
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgAdd" ImageUrl="~/images/expense.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="CreateRequest" CausesValidation="false">
                                <%--Request Expense--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:Image ID="imgView" ImageUrl="~/images/view_expense.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="ViewRequest" CausesValidation="false">
                                <%--View Expense--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="DeleteRequest" CausesValidation="false">
                                <%--Delete--%>
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>' CausesValidation="false">                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
