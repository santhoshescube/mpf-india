﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class Public_Transferrequest : System.Web.UI.Page
{
    clsTransferRequests objRequest;
    clsUserMaster objUser;
    clsLeaveRequest objLeaveRequest;
    clsEmployee objEmployee;
    clsMailSettings objMailSettings;
    clsMessage objMessage;

    //protected void Page_PreInit(object sender, EventArgs e)
    //{
    //    clsGlobalization.SetCulture(((Page)this));
    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("TransferRequest.Text").ToString();
        TransferRequestPager.Fill += new controls_Pager.FillPager(BindDataList);
        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;
            if (Request.QueryString["Requestid"] != null && Request.QueryString["Type"] == "Cancel")
            {
                int iRequestId = 0;
                 ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                //if (fvTransferDetails.DataKey["Employee"] != DBNull.Value)
                //    lblHeading.Text = lblHeading.Text + " " + "(" + Convert.ToString(fvTransferDetails.DataKey["Employee"]) + ")";
                Label lblReason = (Label)fvTransferDetails.FindControl("lblReason");
                TextBox txtEditreason = (TextBox)fvTransferDetails.FindControl("txtEditreason");
                lblReason.Visible = false;
                txtEditreason.Visible = true;

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
            }

            else if (Request.QueryString["Requestid"] != null)
            {
                int iRequestId = 0;
                 ViewState["RequestID"] = iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                if (iRequestId > 0)
                    ViewState["Approve"] = true;
                fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);
                //if (fvTransferDetails.DataKey["Employee"] != DBNull.Value)
                //    lblHeading.Text = lblHeading.Text + " " + "(" + Convert.ToString(fvTransferDetails.DataKey["Employee"]) + ")";

                Label lblReason = (Label)fvTransferDetails.FindControl("lblReason");
                TextBox txtEditreason = (TextBox)fvTransferDetails.FindControl("txtEditreason");
                lblReason.Visible = false;
                txtEditreason.Visible = true;

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkAdd.OnClientClick = "return false;";
                lnkView.Enabled = false;
                lnkView.OnClientClick = "return false;";
            }
            else
            {
                BindDataList();
            }
        }
    }

    protected void fvTransferDetails_DataBound(object sender, EventArgs e)
    {
        try
        {
            objUser = new clsUserMaster();
            int RequestID = fvTransferDetails.DataKey["RequestId"].ToInt32();  
            DropDownList ddlEditStatus = (DropDownList)fvTransferDetails.FindControl("ddlEditStatus");
           
            if (fvTransferDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == false)
            {
                HtmlTableRow trForwardedBy = (HtmlTableRow)fvTransferDetails.FindControl("trForwardedBy");
                if (trForwardedBy != null)
                    trForwardedBy.Visible = false;
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = false;
            }

            if (fvTransferDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == true)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
                ddlEditStatus.DataBind();

                Label lblStatus = (Label)fvTransferDetails.FindControl("lblStatus");
                Button btnApprove = (Button)fvTransferDetails.FindControl("btnApprove");
                Button btCancel = (Button)fvTransferDetails.FindControl("btCancel");

                HtmlTableRow trForwardedBy = (HtmlTableRow)fvTransferDetails.FindControl("trForwardedBy");

                if (Convert.ToBoolean(ViewState["Approve"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                    btnApprove.Visible = true;
                    btCancel.Visible = true;
                    trForwardedBy.Attributes.Add("style", "dispaly:block");
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    btnApprove.Visible = false;
                    btCancel.Visible = false;
                    trForwardedBy.Attributes.Add("style", "dispaly:none");

                }
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = true;
            }

            else if (fvTransferDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestCancel"]) == true)
            {
                //  DropDownList ddlEditStatus = (DropDownList)fvTransferDetails.FindControl("ddlEditStatus");
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();

                Label lblStatus = (Label)fvTransferDetails.FindControl("lblStatus");
                Button btnCancel = (Button)fvTransferDetails.FindControl("btnCancel");
                if (Convert.ToBoolean(ViewState["RequestCancel"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("7"));
                    btnCancel.Visible = true;
                    ddlEditStatus.Enabled = false;
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    btnCancel.Visible = false;
                }
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = true;
            }
            else if (fvTransferDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
            {
                // DropDownList ddlEditStatus = (DropDownList)fvTransferDetails.FindControl("ddlEditStatus");
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();

                Label lblStatus = (Label)fvTransferDetails.FindControl("lblStatus");
                Button btnRequestForCancel = (Button)fvTransferDetails.FindControl("btnRequestForCancel");
                if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
                {
                    lblStatus.Visible = false;
                    ddlEditStatus.Visible = true;
                    ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
                    btnRequestForCancel.Visible = true;
                    ddlEditStatus.Enabled = false;
                }
                else
                {
                    lblStatus.Visible = true;
                    ddlEditStatus.Visible = false;
                    btnRequestForCancel.Visible = false;
                }
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = true;
            }
            DropDownList ddlStatus = (DropDownList)fvTransferDetails.FindControl("ddlStatus");
            if (fvTransferDetails.CurrentMode == FormViewMode.Edit | fvTransferDetails.CurrentMode == FormViewMode.Insert)
            {

                //RequiredFieldValidator rfv = (RequiredFieldValidator)fvTransferDetails.FindControl("rfvFromProject");
                int iRequestType = 0;
                if (fvTransferDetails.DataKey["TransferTypeId"] != null)
                    iRequestType = Convert.ToInt32(fvTransferDetails.DataKey["TransferTypeId"]);

                //if (iRequestType != 5)
                //    rfv.ValidationGroup = "";
                //else
                //    rfv.ValidationGroup = "Submit";

                if (ddlStatus != null)
                {
                    ddlStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                    ddlStatus.DataBind();
                }

                if (fvTransferDetails.CurrentMode == FormViewMode.Edit)
                {

                    if (fvTransferDetails.DataKey["StatusId"] != null)
                    {
                        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvTransferDetails.DataKey["StatusId"])));
                    }
                }
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = false;
            }
            if (Convert.ToBoolean(ViewState["Approve"]) == false && (fvTransferDetails.CurrentMode == FormViewMode.Edit | fvTransferDetails.CurrentMode == FormViewMode.Insert))
            {
                if (ddlStatus != null)
                {
                    ddlStatus.Enabled = false;
                    ddlStatus.CssClass = "dropdownlist_disabled";
                }
                //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
                //if (imgBack != null)
                //    imgBack.Visible = false;
            }
            if (fvTransferDetails.CurrentMode == FormViewMode.Insert | fvTransferDetails.CurrentMode == FormViewMode.Edit)
            {
                FillTransferType();
                FillTextBox();
            }

            if (!clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.Transfer), objUser.GetEmployeeId(), RequestID) && ddlEditStatus != null)
            {
                ddlEditStatus.Items.Remove(new ListItem("Pending", "4"));
            }
        }
        catch
        {
        }

    }

    private void FillTransferType()
    {
        clsTransferRequests objRequests = new clsTransferRequests();

        DropDownList ddlTransferType = (DropDownList)fvTransferDetails.FindControl("ddlTransferType");
        if (ddlTransferType != null)
        {
            ddlTransferType.DataSource = objRequests.FillTranferType();
            ddlTransferType.DataBind();

            ddlTransferType.SelectedIndex = ddlTransferType.Items.IndexOf(ddlTransferType.Items.FindByValue(Convert.ToString(fvTransferDetails.DataKey["TransferTypeId"])));
        }
    }
    protected void TransferType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTextBox();
    }

    protected void FillTextBox()
    {
        try
        {
            clsTransferRequests objRequests = new clsTransferRequests();
            clsUserMaster objUser = new clsUserMaster();

            DropDownList ddlTransferType = (DropDownList)fvTransferDetails.FindControl("ddlTransferType");
            TextBox txtTransferFrom = (TextBox)fvTransferDetails.FindControl("txtTransferFrom");
            HiddenField hdTransferFrom = (HiddenField)fvTransferDetails.FindControl("hidTransferFrom");

            DropDownList ddlTransferFrom = (DropDownList)fvTransferDetails.FindControl("ddlTransferFrom");
            DropDownList ddlTransferTo = (DropDownList)fvTransferDetails.FindControl("ddlTransferTo");
            //RequiredFieldValidator rfv = (RequiredFieldValidator)fvTransferDetails.FindControl("rfvFromProject");


            if (ddlTransferType.SelectedValue.ToInt32() == 5)
            {
                txtTransferFrom.Visible = false;
                ddlTransferFrom.Visible = true;


                objRequests.EmployeeId = objUser.GetEmployeeId();
                objRequests.Mode = "PRJ";

                ddlTransferFrom.DataSource = objRequests.GetValue();
                ddlTransferFrom.DataBind();
                //rfv.ValidationGroup = "Submit";

                ddlTransferFrom.SelectedIndex = ddlTransferFrom.Items.IndexOf(ddlTransferFrom.Items.FindByValue(Convert.ToString(fvTransferDetails.DataKey["TransferFromId"])));

                hdTransferFrom.Value = Convert.ToString(ddlTransferFrom.SelectedValue);
            }
            else
            {
                //rfv.ValidationGroup = "";
                if (ddlTransferType.SelectedValue.ToInt32() == 1)
                    objRequests.Mode = "COM";
                else if (ddlTransferType.SelectedValue.ToInt32() == 2)
                    objRequests.Mode = "DEP";
                else if (ddlTransferType.SelectedValue.ToInt32() == 3)
                    objRequests.Mode = "DES";
                else if (ddlTransferType.SelectedValue.ToInt32() == 4)
                    objRequests.Mode = "EMT";


                objRequests.EmployeeId = objUser.GetEmployeeId();
                DataTable oTable = objRequests.GetValue();
                if (oTable.DefaultView.Count > 0)
                {
                    txtTransferFrom.Text = Convert.ToString(oTable.Rows[0]["Description"]);
                    hdTransferFrom.Value = Convert.ToString(oTable.Rows[0]["id"]);
                }
                else
                {
                    txtTransferFrom.Text = "";
                }

                txtTransferFrom.Visible = true;
                ddlTransferFrom.Visible = false;
            }

            if (ddlTransferType.SelectedValue.ToInt32() == 1)
                objRequests.Mode = "COMALL";
            else if (ddlTransferType.SelectedValue.ToInt32() == 2)
                objRequests.Mode = "DEPALL";
            else if (ddlTransferType.SelectedValue.ToInt32() == 3)
                objRequests.Mode = "DESALL";
            else if (ddlTransferType.SelectedValue.ToInt32() == 4)
                objRequests.Mode = "EMTALL";
            //else if (ddlTransferType.SelectedValue.ToInt32() == 5)
            //    objRequests.Mode = "EMTALL";


            objRequests.EmployeeId = objUser.GetEmployeeId();
            DataTable oTableTransferTo = objRequests.GetValue();
            if (hdTransferFrom.Value != null)
            {
                if (oTableTransferTo.Rows.Count > 0)
                {
                    foreach (DataRow row in oTableTransferTo.Rows)
                    {
                        if (ddlTransferType.SelectedValue != "5")
                        {
                            if (hdTransferFrom.Value == row.ItemArray[0].ToString())
                            {
                                row.Delete();
                                oTableTransferTo.AcceptChanges();
                                break;
                            }
                        }
                        else
                        {
                            foreach (ListItem item in ddlTransferFrom.Items)
                            {
                                if (item.Value == row.ItemArray[0].ToString())
                                {
                                    row.Delete();
                                    break;

                                }
                            }

                        }
                    }
                    oTableTransferTo.AcceptChanges();
                }
            }


            ddlTransferTo.DataSource = oTableTransferTo;
            ddlTransferTo.DataBind();
            ddlTransferTo.SelectedIndex = ddlTransferTo.Items.IndexOf(ddlTransferTo.Items.FindByValue(Convert.ToString(fvTransferDetails.DataKey["TransferToId"])));
        }
        catch
        {
        }
    }

    /// <summary>
    /// insert and update request details
    /// </summary>
    /// <param name="iRequestId">requestId</param>
    private void InsertUpdateRequests(int iRequestId)
    {
        try
        {
            objRequest = new clsTransferRequests();
            objUser = new clsUserMaster();
            objLeaveRequest = new clsLeaveRequest();
            objMailSettings = new clsMailSettings();
            objEmployee = new clsEmployee();
            objMessage = new clsMessage();
            //clsMessageDetails objclsMessageDetails;
            //clsSendMailForRequest objMail = new clsSendMailForRequest();
            bool bIsInsert = true;
            //string sFrom = "";

            DropDownList ddlTransferType = (DropDownList)fvTransferDetails.FindControl("ddlTransferType");
            TextBox txtTransferFrom = (TextBox)fvTransferDetails.FindControl("txtTransferFrom");
            DropDownList ddlTransferTo = (DropDownList)fvTransferDetails.FindControl("ddlTransferTo");
            TextBox txtTransferDate = (TextBox)fvTransferDetails.FindControl("txtTransferDate");
            TextBox txtReason = (TextBox)fvTransferDetails.FindControl("txtReason");
            HiddenField hdTransferFrom = (HiddenField)fvTransferDetails.FindControl("hidTransferFrom");
            Button btnSubmit = (Button)fvTransferDetails.FindControl("btnSubmit");
            //CheckBoxList chkRequested = (CheckBoxList)fvTransferDetails.FindControl("cblEmployees");
            DropDownList ddlTransferFrom = (DropDownList)fvTransferDetails.FindControl("ddlTransferFrom");


            string sRequestedTo = "";
            objRequest.EmployeeId = objUser.GetEmployeeId();
            sRequestedTo = clsLeaveRequest.GetRequestedTo(iRequestId,  objUser.GetEmployeeId(), (int)RequestType.Transfer);
            if (sRequestedTo == "")
            {
               // msgs.WarningMessage("Your request cannot process this time. Please contact HR department.");
                msgs.WarningMessage(GetLocalResourceObject("ContactHR").ToString());
                mpeMessage.Show();
                return;
            }

            if (iRequestId > 0)
            {
                objRequest.Mode = "UPD";
                objRequest.RequestId = iRequestId;
                bIsInsert = false;
            }
            else
            {
                objRequest.Mode = "INS";
                bIsInsert = true;
            }

            objRequest.Reason = txtReason.Text;
            objRequest.RequestedTo = sRequestedTo;
            objRequest.StatusId = 1;
            objRequest.EmployeeId = objUser.GetEmployeeId();
            objRequest.Transferdate = clsCommon.Convert2DateTime(txtTransferDate.Text);
            if (ddlTransferType.SelectedValue != "5")
                objRequest.TransferFrom = Convert.ToInt32(hdTransferFrom.Value);
            else
                objRequest.TransferFrom = Convert.ToInt32(ddlTransferFrom.SelectedValue);

            objRequest.TransferTo = Convert.ToInt32(ddlTransferTo.SelectedValue);
            objRequest.TransferType = Convert.ToInt32(ddlTransferType.SelectedValue);

            if (TransferValidations())
            {
                iRequestId = objRequest.InsertUpdateRequest();
                //--------------------------------------------setting parameters for Message insertion into HRMessages-----------------------------------------
                SendMessage((int)RequestStatus.Applied, iRequestId);
                //------------------------------------------------------------------------------------------------------------------------------------
                if (!bIsInsert)
                {
                    //msgs.InformationalMessage("Request details updated successfully.");
                    msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                    mpeMessage.Show();
                }
                else
                {
                    //msgs.InformationalMessage("Request details added successfully.");
                    msgs.InformationalMessage(GetLocalResourceObject("RequestAddedSuccessfully").ToString());
                    mpeMessage.Show();
                }
         
                upForm.Update();
                upnlDatalist.Update();
                BindDataList();
            }
        }
        catch
        {
        }
    }

    private bool TransferValidations()
    {
        try
        {
            TextBox txtTransferDate = (TextBox)fvTransferDetails.FindControl("txtTransferDate");
            if (objRequest.CheckTransferExists())
            {
                //msgs.InformationalMessage("Request already exists.");
                msgs.InformationalMessage(GetLocalResourceObject("RequestAlreadyExists").ToString());
                mpeMessage.Show();
                return false;
            }
            else if (objRequest.CheckDuplication())
            {
               // msgs.InformationalMessage("Request already exists.");
                msgs.InformationalMessage(GetLocalResourceObject("RequestAlreadyExists").ToString());
                mpeMessage.Show();
                return false;
            }
            else if (objRequest.CheckSalaryAlreadyProcessed(objUser.GetEmployeeId(), clsCommon.Convert2DateTime(txtTransferDate.Text)))
            {
                //msgs.InformationalMessage("Payment Exist in this Date.");
                msgs.InformationalMessage(GetLocalResourceObject("PaymentExists").ToString());
                mpeMessage.Show();
                return false;
            }

            else
                return true;
        }
        catch
        {
            return false;
        }
      
    }

    private void BindDataList()
    {
        try
        {
            objRequest = new clsTransferRequests();
            objUser = new clsUserMaster();
            objRequest.EmployeeId = objUser.GetEmployeeId();
            objRequest.Mode = "VIEW";
            objRequest.PageIndex = TransferRequestPager.CurrentPage + 1;
            objRequest.PageSize = TransferRequestPager.PageSize;
            DataTable oTable = objRequest.GetRequests();
            if (oTable.DefaultView.Count > 0)
            {
                dlTransferRequest.DataSource = oTable;
                dlTransferRequest.DataBind();

                TransferRequestPager.Total = objRequest.GetRecordCount();
                lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlTransferRequest.ClientID + "');";

                lnkDelete.Enabled = true;

                fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                fvTransferDetails.DataSource = null;
                fvTransferDetails.DataBind();
                TransferRequestPager.Visible = true;

            }
            else
            {
                dlTransferRequest.DataSource = null;
                dlTransferRequest.DataBind();
                lnkDelete.Enabled = false;
                fvTransferDetails.ChangeMode(FormViewMode.Insert);
                TransferRequestPager.Visible = false;
            }
        }
        catch
        {
        }

    }
    protected void fvTransferDetails_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            objRequest = new clsTransferRequests();
            objUser = new clsUserMaster();
            objLeaveRequest = new clsLeaveRequest();
            objMailSettings = new clsMailSettings();
            objEmployee = new clsEmployee();
            objMessage = new clsMessage();

            int iRequestId = 0;
            DropDownList ddlStatus = (DropDownList)fvTransferDetails.FindControl("ddlEditStatus");

            Label lblTransferType = (Label)fvTransferDetails.FindControl("lblTransferType");
            Label lblTransferFromDate = (Label)fvTransferDetails.FindControl("lblTransferFromDate");
            Label lblTransferFrom = (Label)fvTransferDetails.FindControl("lblTransferFrom");
            Label lblTransferTo = (Label)fvTransferDetails.FindControl("lblTransferTo");
            Label lblReason = (Label)fvTransferDetails.FindControl("lblReason");            
            TextBox txtEditreason = (TextBox)fvTransferDetails.FindControl("txtEditreason");
           
            int iRequestedById;


            switch (e.CommandName)
            {
                case "Add":

                    if (Convert.ToString(e.CommandArgument) != "")
                    {
                        iRequestId = Convert.ToInt32(e.CommandArgument);
                    }

                    InsertUpdateRequests(iRequestId);
                    break;

                case "CancelRequest":
                    upForm.Update();
                    upnlDatalist.Update();
                    BindDataList();

                    break;

                case "Approve":

                    iRequestId = Convert.ToInt32(Request.QueryString["Requestid"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                    string sFrom = string.Empty;
                    objLeaveRequest.EmployeeId = objUser.GetEmployeeId();
                    iRequestedById = objRequest.GetRequestedById();

                    if (ddlStatus.SelectedValue.ToInt32() == 5 && objRequest.GetWorkStatusId() < 6)
                    {
                        // msgs.InformationalMessage("Failed to approve the request. The requested employee is no longer in service.");
                        msgs.InformationalMessage(GetLocalResourceObject("EmployeeNotInService").ToString());


                        mpeMessage.Show();
                        return;
                    }

                    if (clsLeaveRequest.IsHigherAuthority((int)RequestType.Transfer, objUser.GetEmployeeId(), objRequest.RequestId))
                    {

                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 && objRequest.CheckSalaryAlreadyProcessed(iRequestedById, clsCommon.Convert2DateTime(lblTransferFromDate.Text)))
                        {
                           // msgs.InformationalMessage("Cannot Approve,Payment Exist in this Date.");

                            msgs.InformationalMessage(GetLocalResourceObject("CannotApprovePaymentExists").ToString());
                            mpeMessage.Show();
                            return;
                        }
                        objRequest.Reason = txtEditreason.Text;
                        objRequest.UpdateTransferStatus(true);
                        divPrint.Style["display"] = "block";
                        ViewState["Approve"] = false;
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                        {
                            //msgs.InformationalMessage("Request updated successfully.");
                            msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                            //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                            SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                            //-----------------------------------------------------------------------------------------------------------
                        }
                        else
                        {
                            SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                            msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                            //msgs.InformationalMessage("Request updated successfully.");
                        }
                    }
                    else
                    {
                        divPrint.Style["display"] = "none";
                        objRequest.Reason = txtEditreason.Text;
                        objRequest.UpdateTransferStatus(false);
                        ViewState["Approve"] = false;
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                        {
                           //msgs.InformationalMessage("Request updated successfully.");
                            msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                        }
                        else
                           // msgs.InformationalMessage("Your transfer request is forwarded successfully.");
                            msgs.InformationalMessage(GetLocalResourceObject("TransferRequestForwarded").ToString());

                        //--------------------------------------------Forward transfer request-----------------------------------------
                        objRequest.EmployeeId = objUser.GetEmployeeId();
                        objEmployee.EmployeeID = objUser.GetEmployeeId();
                        objEmployee.GetOfficialEmailDetails();
                        sFrom = objEmployee.OfficialEmail;
                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                        //-----------------------------------------------------------------------------------------------------------
                    }

                    fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(iRequestId);
                    lnkDelete.OnClientClick = "return false;";
                    lnkDelete.Enabled = false;

                    mpeMessage.Show();
                    break;


                case "Cancel":
                    if (ViewState["PrevPage"] != null)
                        this.Response.Redirect(ViewState["PrevPage"].ToString());
                    else
                        this.Response.Redirect("home.aspx");

                    break;

                    //objRequest.RequestId = Convert.ToInt32(fvTransferDetails.DataKey["RequestId"]);
                    //objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);

                    //iRequestId = Convert.ToInt32(fvTransferDetails.DataKey["RequestId"]);
                    //if (!objRequest.CheckTransferDone(objUser.GetEmployeeId(), iRequestId))
                    //{
                    //    objRequest.UpdateCancelStatus();
                    //    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    //    SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                    //    //--------------------------------------------------------------------------------------------------------------------------------------------------
                    //    ViewState["RequestCancel"] = false;
                    //    fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                    //    BindRequestDetails(iRequestId);
                    //    msgs.InformationalMessage("Successfully updated request details.");
                    //    mpeMessage.Show();
                    //}
                    //else
                    //{
                    //    msgs.InformationalMessage("Request Already Transferred.");
                    //    mpeMessage.Show();
                    //}

                    break;

                case "RequestForCancel":

                    iRequestId = Convert.ToInt32(fvTransferDetails.DataKey["RequestId"]);
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.EmployeeId = objUser.GetEmployeeId();

                    if (!objRequest.CheckTransferDone(objRequest.GetRequestedById(), iRequestId))
                    {
                        objRequest.Reason = txtEditreason.Text;
                        objRequest.TransferRequestCancelled();
                        iRequestedById = objRequest.GetRequestedById();
                        objEmployee.EmployeeID = objUser.GetEmployeeId();
                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(ddlStatus.SelectedValue.ToInt32(), iRequestId);
                        //-----------------------------------------------------------------------------------------------------------                  
                        ViewState["RequestForCancel"] = false;
                        fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                        BindRequestDetails(iRequestId);
                       // msgs.InformationalMessage("Request details updated successfully.");

                        msgs.InformationalMessage(GetLocalResourceObject("RequestUpdatedSuccessfully").ToString());
                    }
                    else
                    {
                        //msgs.InformationalMessage("Cannot cancel,Request Already Transferred.");
                        msgs.InformationalMessage(GetLocalResourceObject("RequestAlreadyTransferred").ToString());
                    }
                    mpeMessage.Show();
                    break;
            }
        }
        catch
        {
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["Approve"] = false;
            ViewState["RequestForCancel"] = false;
            ViewState["RequestCancel"] = false;
            fvTransferDetails.ChangeMode(FormViewMode.Insert);

            dlTransferRequest.DataSource = null;
            dlTransferRequest.DataBind();
            TransferRequestPager.Visible = false;

            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";

            upForm.Update();
            upnlDatalist.Update();
        }
        catch
        {
        }

    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["Approve"] = false;
            ViewState["RequestForCancel"] = false;
            ViewState["RequestCancel"] = false;

            BindDataList();

            upForm.Update();
            upnlDatalist.Update();
        }
        catch
        {
        }
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            objRequest = new clsTransferRequests();
            objMessage = new clsMessage();
            string message = string.Empty;

            if (dlTransferRequest.Items.Count > 0)
            {
                foreach (DataListItem item in dlTransferRequest.Items)
                {
                    CheckBox chkRequest = (CheckBox)item.FindControl("chkTransfer");
                    if (chkRequest == null)
                        continue;
                    if (chkRequest.Checked)
                    {
                        objRequest.RequestId = Convert.ToInt32(dlTransferRequest.DataKeys[item.ItemIndex]);
                        if (objRequest.IsTransferRequestApproved())
                        {
                            //message = "Approved travel request(s) could not be deleted.";
                            //message = "Processing started request(s) cannot be deleted.";

                            message = GetLocalResourceObject("CannotDeleteProcessingStarted").ToString();

                            //message += "</ol>Please delete the existing details to proceed with employee deletion";
                        }
                        else
                        {
                            objRequest.RequestId = Convert.ToInt32(dlTransferRequest.DataKeys[item.ItemIndex]);
                            objRequest.Mode = "DEL";
                            objRequest.DeleteRequest();
                            clsCommonMessage.DeleteMessages(dlTransferRequest.DataKeys[item.ItemIndex].ToInt32(), eReferenceTypes.TransferRequest);
                            //message = "Request(s) deleted successfully";

                            message = GetLocalResourceObject("RequestDeletedSuccessfully").ToString();
                        }
                    }
                }

                //mpeMessage.Show();
            }
            else if (fvTransferDetails.CurrentMode == FormViewMode.ReadOnly)
            {
                objRequest.RequestId = Convert.ToInt32(fvTransferDetails.DataKey["RequestId"]);

                if (objRequest.IsTransferRequestApproved())
                {
                    //message = "Approved travel request(s) could not be deleted.";
                    //message = "Processing started request(s) cannot be deleted.";
                    message = GetLocalResourceObject("CannotDeleteProcessingStarted").ToString();
                    //message += "</ol>Please delete the existing details to proceed with employee deletion";
                }
                else
                {
                    objRequest.Mode = "DEL";
                    objRequest.DeleteRequest();
                    clsCommonMessage.DeleteMessages(fvTransferDetails.DataKey["RequestId"].ToInt32(), eReferenceTypes.TransferRequest);
                    //message = "Request(s) deleted successfully";
                    message = GetLocalResourceObject("RequestDeletedSuccessfully").ToString();
                }
            }

            msgs.InformationalMessage(message);
            mpeMessage.Show();
            BindDataList();

            upForm.Update();
            upnlDatalist.Update();
        }
        catch
        {
        }
    }

    /// <summary>
    /// get request details to bind form view.
    /// </summary>
    /// <param name="iRequestId"></param>
    private void BindRequestDetails(int iRequestId)
    {
        try
        {
            objRequest = new clsTransferRequests();
            objRequest.Mode = "EDT";
            objRequest.RequestId = iRequestId;

            fvTransferDetails.DataSource = objRequest.GetRequestDetails();
            fvTransferDetails.DataBind();

            //link to display approval level remarks
            HtmlTableRow trlink = (HtmlTableRow)fvTransferDetails.FindControl("trlink");
            //ImageButton imgBack = (ImageButton)fvTransferDetails.FindControl("imgBack");
           
            if (trlink != null)
                trlink.Style["display"] = "table-row";

            HtmlTableRow trAuthority = (HtmlTableRow)fvTransferDetails.FindControl("trAuthority");
            DataList dl1 = (DataList)fvTransferDetails.FindControl("dl1");
            if (trAuthority != null)
            {
                
                objRequest.RequestId = iRequestId;
                objRequest.CompanyID = objRequest.GetCompanyID();
                DataTable dt2 = objRequest.GetApprovalHierarchy();
                if (dt2.Rows.Count > 0)
                {
                    trAuthority.Style["display"] = "table-row";
                    dl1.DataSource = dt2;
                    dl1.DataBind();
                }
            }
            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
                //if (imgBack != null)
                //    imgBack.Visible = true;
            }
            else
            {
                ViewState["RequestID"] = iRequestId;
                //if (imgBack != null)
                //    imgBack.Visible = false;
            }
            dlTransferRequest.DataSource = null;
            dlTransferRequest.DataBind();

            TransferRequestPager.Visible = false;
        }
        catch
        {
        }
    }

    protected void dlTransferRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "_View":
                    HiddenField hfLink = (HiddenField)fvTransferDetails.FindControl("hfLink");
                    HtmlTableRow trReasons = (HtmlTableRow)fvTransferDetails.FindControl("trReasons");
                    if (hfLink != null && trReasons != null)
                    {
                        hfLink.Value = "0";
                        trReasons.Style["display"] = "none";
                    }
                    fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                    BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                    lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("ControlsCommon", "DeleteConfirmation") + "');";
                    lnkDelete.Enabled = true;
                   
                    dlTransferRequest.DataSource = null;
                    dlTransferRequest.DataBind();
                    TransferRequestPager.Visible = false;
                    break;

                case "_Edit":

                    fvTransferDetails.ChangeMode(FormViewMode.Edit);
                    BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                    lnkDelete.Enabled = false;
                    lnkDelete.OnClientClick = "return false;";
                    break;

                case "_Cancel":

                    ViewState["RequestCancel"] = true;
                    //fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                    //BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                    //lnkDelete.Enabled = false;
                    //lnkDelete.OnClientClick = "return false;";
                    objRequest = new clsTransferRequests();
                    objRequest.RequestId = Convert.ToInt32(e.CommandArgument);
                    objRequest.StatusId = (int)RequestStatus.RequestForCancel;
                    objRequest.Reason = clsUserMaster.GetCulture() == "ar-AE" ? ("Transfer Request for Cancellation") : ("Transfer Request for Cancellation.");
                    //iRequestId = Convert.ToInt32(fvTransferDetails.DataKey["RequestId"]);
                    if (!objRequest.CheckTransferDone(new clsUserMaster().GetEmployeeId(), objRequest.RequestId))
                    {
                        objRequest.UpdateCancelStatus();
                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                        SendMessage(objRequest.StatusId, objRequest.RequestId);
                        //--------------------------------------------------------------------------------------------------------------------------------------------------
                        ViewState["RequestCancel"] = false;
                        //fvTransferDetails.ChangeMode(FormViewMode.ReadOnly);
                        //BindRequestDetails(objRequest.RequestId);
                        BindDataList();
                       // msgs.InformationalMessage("Successfully updated request details.");
                        msgs.InformationalMessage(GetLocalResourceObject("SuccessfullyUpdated").ToString());
                        mpeMessage.Show();
                    }
                    else
                    {
                        //msgs.InformationalMessage("Request Already Transferred.");
                        msgs.InformationalMessage(GetLocalResourceObject("TransferredAlready").ToString());
                        mpeMessage.Show();
                    }
                    lnkDelete.Enabled = false;
                    lnkDelete.OnClientClick = "return false;";
                    break;
            }
        }
        catch
        {
        }
    }
    protected void dlTransferRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
            {
                TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
                HiddenField hdRequestID = (HiddenField)e.Item.FindControl("hdRequestID");
                int iRequestId = 0;
                iRequestId = Convert.ToInt32(txtStatusId.Text);
                ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");
                HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
                if (iRequestId == 2 || iRequestId == 3 || iRequestId == 4 || iRequestId == 5 || iRequestId == 6 || iRequestId == 7 || Convert.ToBoolean(hdForwarded.Value) == true)
                {
                    imgEdit.Enabled = false;
                    imgEdit.ImageUrl = "~/images/edit_disable.png";

                }
                ImageButton imgCancel = (ImageButton)e.Item.FindControl("imgCancel");
                imgCancel.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm(' هل تريد إلغاء هذا الطلب؟');") : ("return confirm('Do you want to cancel this request?');"); 
                if (iRequestId == 1 && !Convert.ToBoolean(hdForwarded.Value))
                {
                    imgCancel.Enabled = false;
                    imgCancel.ImageUrl = "~/images/cancel_disable.png";
                }

                if (iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
                {
                    imgCancel.Enabled = false;
                    imgCancel.ImageUrl = "~/images/cancel_disable.png";
                }
                if (iRequestId == 5 && objRequest.CheckTransferDone(objUser.GetEmployeeId(), hdRequestID.Value.ToInt32()))
                {
                    imgCancel.Enabled = false;
                    imgCancel.ImageUrl = "~/images/cancel_disable.png";
                }
            }
        }
        catch
        {
        }
    }

    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Crop the string if it exceeds the width of container 
    /// </summary>
    protected string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    protected string ArrangeString(string value, int length)
    {
        int intlen = value.Length;
        int intPosition = 0;
        int intNewPosition = -3;
        string strCurrentstring = "";
        string strInsertString = "";
        if (intlen <= length)
            return value;
        else
        {
            int intCount = intlen / length;
            for (intPosition = 0; intPosition <= intCount; intPosition++)
            {
                intNewPosition += length;
                if (intNewPosition < intlen)
                    strCurrentstring = value.Substring(intNewPosition - 1, 1);

                if (strCurrentstring == " ")
                    strInsertString = "<p>";
                else
                    strInsertString = "-<p>";
                if (intNewPosition < intlen)
                    value = value.Insert(intNewPosition, strInsertString);
            }
            return value;
        }
    }

    private void SendMessage(int StatusID, int intRequestID)
    {
        switch (StatusID)
        {
            case (int)RequestStatus.Approved:
                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Approval, eAction.Approved, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Transfer, eAction.Approved);
                break;
            case (int)RequestStatus.Rejected:
                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Rejection, eAction.Reject, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Transfer, eAction.Reject);
                break;
            case (int)RequestStatus.Applied:
                clsCommonMessage.SendMessage(objUser.GetEmployeeId(), intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Request, eAction.Applied, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Transfer, eAction.Applied);
                break;
            case (int)RequestStatus.RequestForCancel:
                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Cancel_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Transfer, eAction.RequestForCancel);
                break;
            case (int)RequestStatus.Cancelled:
                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Cancellation, eAction.Cancelled, "");
                clsCommonMail.SendMail(intRequestID, MailRequestType.Transfer, eAction.Cancelled);
                break;
            case (int)RequestStatus.Pending:
                clsCommonMessage.SendMessage(null, intRequestID, eReferenceTypes.TransferRequest, eMessageTypes.Transfer_Request, eAction.Pending, "");
                break;

        }

    }

    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsTransferRequests();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            LinkButton lbReason = (LinkButton)fvTransferDetails.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)fvTransferDetails.FindControl("hfLink");
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                HtmlTableRow trReasons = (HtmlTableRow)fvTransferDetails.FindControl("trReasons");
                GridView gvReasons = (GridView)fvTransferDetails.FindControl("gvReasons");
                trReasons.Style["display"] = "table-row";

                DataTable dt = objRequest.DisplayReasons(objRequest.RequestId);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                HtmlTableRow trReasons = (HtmlTableRow)fvTransferDetails.FindControl("trReasons");
                trReasons.Style["display"] = "none";
            }
        }
    }
}
