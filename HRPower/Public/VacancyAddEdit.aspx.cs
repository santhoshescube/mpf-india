﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class Public_VacancyAddEdit : System.Web.UI.Page
{
    #region Declarations

    clsVacancyAddEdit objVacancy = new clsVacancyAddEdit();
    clsUserMaster objUserMaster = new clsUserMaster();
    private string CurrentSelectedValue { get; set; }
    string str;

    #endregion

    #region Events
    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        int JobID = 0;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (!IsPostBack)
        {
            EnableMenus();
            ViewState["PrevPage"] = Request.UrlReferrer;
            if (Request.QueryString["JobID"] != null && Request.QueryString["Approval"] == null)//From Vacancy datalist edit itemcommand
            {
                JobID = Convert.ToInt32(Server.UrlDecode(Request.QueryString["JobID"].ToString()));
                ViewState["JobID"] = JobID;
                hfMode.Value = "1";
                BindInitials(JobID, false);
            }
            else if (Request.QueryString["Approval"] != null && Request.QueryString["JobId"] != null)//From Pending Requests for Approval/Rejection
            {
                JobID = Convert.ToInt32(Request.QueryString["JobID"].ToString());
                ViewState["JobID"] = JobID;
                ViewState["Approval"] = Request.QueryString["Approval"].ToBoolean();
                hfMode.Value = "1";
                BindInitials(JobID, true);
            }
            else
            {
                JobID = 0;
                hfMode.Value = "0";
                ClearControls();
                BindInitials(JobID, false);
                upAdd.Update();
            }
        }
        else
        {
            if (Request.QueryString["JobID"] != null || ViewState["JobID"].ToInt32() > 0)
                hfMode.Value = "1";
            else
            {
                hfMode.Value = "0";
            }
        }
        DesignationReference.OnSave -= new Controls_DesignationReference.saveHandler(DesignationReference_OnSave);
        DesignationReference.OnSave += new Controls_DesignationReference.saveHandler(DesignationReference_OnSave);
        ucDegreeReference.OnSave -= new controls_DegreeReference.SaveDegree(DegreeReference_OnSave);
        ucDegreeReference.OnSave += new controls_DegreeReference.SaveDegree(DegreeReference_OnSave);
    }
    void DegreeReference_OnSave(int SelectedValue)
    {
        BindQualifications();   
        if(ViewState["Degrees"]!=null)
        {
            DataTable dt = (DataTable)ViewState["Degrees"];
            foreach (DataRow dr in dt.Rows)
            {
                foreach (ListItem chk in lstQualification.Items)
                {
                    if (chk.Value.ToInt32() == dr["DegreeId"].ToInt32())
                        chk.Selected = true;
                }
            }
        }
        uprcDegree.Update();
    }

    protected void ddlDesignation_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //if (txtPaystructure.Text == "" || (txtPaystructure.Text != ddlDesignation.SelectedItem.ToString()))
        //    txtPaystructure.Text = ddlDesignation.SelectedItem.ToString();
        //updDesignation.Update();
        //upAdd.Update();
    }
    /// <summary>
    /// Loads a Pop up for adding a new Designation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDesignation_Click(object sender, EventArgs e)
    {
        if (ddlDesignation != null)
        {
            DesignationReference.GetData();
            mdlPopDesignation.Show();
            updDesignation.Update();
        }
    }
    //protected void ddlPlanning_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlPlanning.SelectedValue.ToInt32() > 0)
    //        divBudgetedAmount.Style["display"] = "block";
    //    else
    //        divBudgetedAmount.Style["display"] = "none";
    //}
    protected void rblIsBudgeted_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblIsBudgeted.SelectedValue == "1")
            divBudgetedAmount.Style["display"] = "block";
        else
            divBudgetedAmount.Style["display"] = "none";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ValidateData();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearControls();
        if(ViewState["PrevPage"] != null && Request.QueryString["JobID"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
        else
            Response.Redirect("Vacancy.aspx");
    }
    protected void btnDegree_Click(object sender, EventArgs e)
    {

        DataTable dtSelectedVacancy =new DataTable();
        dtSelectedVacancy.Columns.Add("DegreeId");

        foreach (ListItem chk in lstQualification.Items)
        {
            if (chk.Selected)
            {
                DataRow dr = dtSelectedVacancy.NewRow();
                dr["DegreeId"] = chk.Value.ToInt32();
                dtSelectedVacancy.Rows.Add(dr);
            }
        }
        ViewState["Degrees"] = dtSelectedVacancy;

        AddNewDegree();
    }

    #region RightSideMenuEvents

    protected void lnkDeleteJob_Click(object sender, EventArgs e)
    {

    }
    protected void lnkAddCriteria_Click(object sender, EventArgs e)
    {
        if (CriteriaReferenceControl != null && mdlPopUpReference != null)
        {
            CriteriaReferenceControl.BindDataList();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void lnkAddparticular_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Insert);
        controls_AdditionDeduction ucAdditionDeduction = (controls_AdditionDeduction)FormView1.FindControl("ucAdditionDeduction");

        AjaxControlToolkit.ModalPopupExtender mpeAdditionDeduction = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeAdditionDeduction");
        if (ucAdditionDeduction != null && mpeAdditionDeduction != null)
        {
            ucAdditionDeduction.BindDataList();
            mpeAdditionDeduction.Show();
            upnlAdditionDeduction.Update();
        }
    }
    protected void lnkNewJob_Click(object sender, EventArgs e)
    {
        clsVacancyAddCriteria.ID = 0;
        clsVacancyPayStructure.Designation = string.Empty;
        clsVacancyPayStructure.ID = 0;
        clsVacancyOfferLetterApproval.ID = 0;
        clsVacancyKPIKRA.ID = 0;
        Response.Redirect("VacancyAddEdit.aspx");
    }
    #endregion

    #endregion

    #region Methods

    public void EnableMenus()
    {        
        int RoleID = objUserMaster.GetRoleId();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        if (RoleID > 4)//RoleID based checking
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.JobOpening);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = lnkNewJob.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = lnkViewjob.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
                
                if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                {
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = lnkVacancyCriteria.Enabled =
                        lnkVacancyPayStructure.Enabled = lnkVacancyOfferLetterApproval.Enabled = lnkKPIKRA.Enabled =  true;
                }
                else
                {
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = lnkVacancyCriteria.Enabled =
                        lnkVacancyPayStructure.Enabled = lnkVacancyOfferLetterApproval.Enabled = lnkKPIKRA.Enabled = false;
                }
            }
        }
        else
        {
            lnkNewJob.Enabled = lnkViewjob.Enabled = lnkDelete.Enabled = true;
            lnkVacancyPayStructure.Enabled = lnkVacancyOfferLetterApproval.Enabled = lnkKPIKRA.Enabled = true;
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }
        if (ViewState["IsUpdate"].ToBoolean())
            lnkVacancyCriteria.Enabled = lnkVacancyPayStructure.Enabled = lnkVacancyOfferLetterApproval.Enabled = lnkKPIKRA.Enabled = true;
        else
            lnkVacancyCriteria.Enabled = lnkVacancyPayStructure.Enabled = lnkVacancyOfferLetterApproval.Enabled = lnkKPIKRA.Enabled = false;

        if (ViewState["IsCreate"].ToBoolean() || ViewState["IsUpdate"].ToBoolean())
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = true;
        else
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = false;
        upAdd.Update();
    }

    private void ClearControls()
    {
        txtJobCode.Text = txtJobTitle.Text = txtLastDate.Text = txtRemarks.Text = "";
        rblAccomodation.SelectedValue = rblTransportation.SelectedValue = "0";
        rblFeesPayable.SelectedValue = rblIsBudgeted.SelectedValue = "0";
        rblGender.SelectedValue = rblLocalOverseasRecruitment.SelectedValue = "3";

        txtRequiredQuota.Text = txtSkills.Text = txtExperience.Text = txtPlaceOfWork.Text = "";
        txtWeeklyWorkingDays.Text = txtDailyWorkingHours.Text = txtContractPeriod.Text = txtAnnualLeave.Text = "";
        txtLegalTerms.Text = txtAirTicket.Text = txtBudgetedAmount.Text = txtJoiningDate.Text = txtLanguageCapabilities.Text = "";
        txtMinAge.Text = txtMaxAge.Text = "";
        ViewState["JobID"] = ViewState["ApprovedBy"] = ViewState["CreatedBy"] = null;
    }
    private void BindInitials(int JobID, bool Approval)
    {
        ViewState["JobID"] = JobID;
        BindCombos();

        if (Approval)
        {
            FillStatus(1);      //Fills combo only with Approved and Rejected Status
            ddlStatus.SelectedValue = "5";
            divCloseJob.Style["Display"] = "block";
        }
        else
        {
            FillStatus(0);      //Fills combo with all status when adding a new job
            ddlStatus.SelectedValue = "1";
            divCloseJob.Style["Display"] = "none";
        }

        DataSet ds = clsVacancyView.GetVacancyDetails(JobID);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];  //Binds Tab1 with Details

            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["EmploymentTypeID"].ToString() != "")
                    ddlEmploymentType.SelectedValue = dt.Rows[0]["EmploymentTypeID"].ToString();
                else
                    ddlEmploymentType.SelectedValue = "1";
                ddlCompany.SelectedValue = dt.Rows[0]["CompanyID"].ToString();
                ddlDepartment.SelectedValue = dt.Rows[0]["DepartmentID"].ToString();
                ddlDesignation.SelectedValue = dt.Rows[0]["DesignationID"].ToString();

                txtJobCode.Text = dt.Rows[0]["jobCode"].ToString();
                txtJobTitle.Text = dt.Rows[0]["JobTitle"].ToString();
                txtLastDate.Text = dt.Rows[0]["ExpiryDate"].ToString();
                txtRemarks.Text = dt.Rows[0]["Remarks"].ToString();

                txtRequiredQuota.Text = dt.Rows[0]["RequiredQuota"].ToString();
                txtSkills.Text = dt.Rows[0]["Skills"].ToString();
                txtExperience.Text = dt.Rows[0]["Experience"].ToString();

                txtPlaceOfWork.Text = dt.Rows[0]["PlaceofWork"].ToString();
                txtWeeklyWorkingDays.Text = dt.Rows[0]["WeeklyWorkingDays"].ToString();
                txtDailyWorkingHours.Text = dt.Rows[0]["DailyWorkingHour"].ToString();
                if (dt.Rows[0]["IsAccomodationBool"].ToBoolean() == true)
                    rblAccomodation.SelectedValue = "0";
                else
                    rblAccomodation.SelectedValue = "1";

                if (dt.Rows[0]["IsTransportationBool"].ToBoolean() == true)
                    rblTransportation.SelectedValue = "0";
                else
                    rblTransportation.SelectedValue = "1";

                txtContractPeriod.Text = dt.Rows[0]["ContractPeriod"].ToString();
                txtAirTicket.Text = dt.Rows[0]["AirTicket"].ToString();
                txtAnnualLeave.Text = dt.Rows[0]["AnnualLeaveDays"].ToString();
                txtLegalTerms.Text = dt.Rows[0]["LegalTerms"].ToString();
                if (dt.Rows[0]["IsBudgeted"].ToBoolean())
                {
                    divBudgetedAmount.Style["display"] = "block";
                    rblIsBudgeted.SelectedValue = "1";
                }
                else
                {
                    divBudgetedAmount.Style["display"] = "none";
                    rblIsBudgeted.SelectedValue = "0";
                }

                txtBudgetedAmount.Text = dt.Rows[0]["BudgetedAmount"].ToDecimal().ToString("F" + 2);

                if (dt.Rows[0]["CreatedBy"].ToInt32() > 0 || dt.Rows[0]["CreatedBy"].ToInt32() == -1)
                {
                    ViewState["CreatedBy"] = dt.Rows[0]["CreatedBy"].ToInt32();
                }

                if (dt.Rows[0]["JobStatusID"].ToInt32() == 5 || dt.Rows[0]["JobStatusID"].ToInt32() == 3 || dt.Rows[0]["JobStatusID"].ToInt32() == 4)
                {
                    ddlStatus.Enabled = false;
                    ddlStatus.SelectedValue = dt.Rows[0]["JobStatusID"].ToString();
                }
                else
                {
                    ddlStatus.Enabled = true;
                    objVacancy.JobStatusID = (int)eAction.Applied;
                }

                if (dt.Rows[0]["ApprovedBy"].ToInt32() > 0 || dt.Rows[0]["ApprovedBy"].ToInt32() == -1)
                    ViewState["ApprovedBy"] = dt.Rows[0]["ApprovedBy"].ToInt32();
                else
                    ViewState["ApprovedBy"] = 0;

                //Values to be used by usercontrols
                clsVacancyAddCriteria.ID = JobID;
                clsVacancyPayStructure.Designation = ddlDesignation.SelectedItem.Text;
                clsVacancyPayStructure.ID = JobID;
                clsVacancyOfferLetterApproval.ID = JobID;
                clsVacancyKPIKRA.ID = JobID;

                //---------datas of tab2 - preferences----------------------

                txtLanguageCapabilities.Text = dt.Rows[0]["Languages"].ToString();
                txtMinAge.Text = dt.Rows[0]["MinAge"].ToString();
                txtMaxAge.Text = dt.Rows[0]["MaxAge"].ToString();
                rblGender.SelectedValue = dt.Rows[0]["GenderRecruID"].ToString();
                rblLocalOverseasRecruitment.SelectedValue = dt.Rows[0]["RecruitmentTypeID"].ToString();
                rblFeesPayable.SelectedValue = dt.Rows[0]["RecruitmentFeesPayable"].ToString();
                if (dt.Rows[0]["JoiningDate"].ToString() == "01/01/1900")
                    txtJoiningDate.Text = "";
                else
                    txtJoiningDate.Text = dt.Rows[0]["JoiningDate"].ToString();
                //-----------------------------------------------------------

                //Binds Qualification Details
                dt = new DataTable();
                dt = ds.Tables[3];
                if (dt.Rows.Count == 0)
                    dt.Rows.Add(dt.NewRow());

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                    
                        foreach (ListItem chk in lstQualification.Items)
                        {
                            if (chk.Value.ToInt32() == dr["DegreeId"].ToInt32())
                                chk.Selected = true;
                        }
                    }
                }
            }
        }
        if (JobID > 0)
        {
            lnkVacancyCriteria.Visible = lnkVacancyPayStructure.Visible = lnkVacancyOfferLetterApproval.Visible = lnkKPIKRA.Visible =
            imgVacancyCriteria.Visible = imgVacancyPayStructure.Visible = imgvacancyOffer.Visible = imgKPIKRA.Visible =  true;
        }
        else
        {
            lnkVacancyCriteria.Visible = lnkVacancyPayStructure.Visible = lnkVacancyOfferLetterApproval.Visible = lnkKPIKRA.Visible =
            imgVacancyCriteria.Visible = imgVacancyPayStructure.Visible = imgvacancyOffer.Visible = imgKPIKRA.Visible = false;
        }

        lnkDelete.Enabled = false;
        upAdd.Update();
    }

    #region DropdownListBind

    private void BindCombos()
    {
        BindGender();
        BindRecruitmentLocationType();
        BindQualifications();
        BindCompanies();
        BindDepartments();
        BindDesignations();
        //BindPlannings();
        BindEmploymentType();
    }
    private void FillStatus(int IsAdd)
    {
        DataTable dt = clsVacancyAddEdit.GetJobStatusReference();
        if (IsAdd == 0)
            ddlStatus.DataSource = dt;
        else
        {
            dt.DefaultView.RowFilter = "JobStatusID  in (3,5)";  //Filters the contents only allowing Approved And Rejected status
            ddlStatus.DataSource = dt.DefaultView.ToTable();
        }
        ddlStatus.DataBind();
    }
    //private void BindPlannings()
    //{
    //    ddlPlanning.Items.Insert(0, new ListItem(str, "-1"));
    //}
    private void BindEmploymentType()
    {
        ddlEmploymentType.DataSource = clsVacancyAddEdit.GetEmploymentType();
        ddlEmploymentType.DataBind();
        ddlEmploymentType.Items.Insert(0, new ListItem(str, "-1"));
    }
    private void BindDesignations()
    {
        ddlDesignation.DataSource = clsVacancyAddEdit.GetAllDesignations(objUserMaster.GetCompanyId(), (ViewState["JobID"].ToInt32() > 0) ? ViewState["JobID"].ToInt32() : 0);
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, new ListItem(str, "-1"));
    }
    private void BindDepartments()
    {
        ddlDepartment.DataSource = clsVacancyAddEdit.GetDepartments();
        ddlDepartment.DataBind();
        ddlDepartment.Items.Insert(0, new ListItem(str, "-1"));
    }
    private void BindCompanies()
    {
        ddlCompany.DataSource = clsVacancyAddEdit.GetCompany(objUserMaster.GetCompanyId(), (ViewState["JobID"].ToInt32() > 0)?ViewState["JobID"].ToInt32() :0);
        ddlCompany.DataBind();
        ddlCompany.Enabled = false;
    }
    private void BindGender()
    {
        rblGender.DataSource = clsVacancyAddEdit.GetGender();
        rblGender.DataTextField = "Gender";
        rblGender.DataValueField = "GenderRecruID";
        rblGender.DataBind();
        rblGender.SelectedValue = "3";
    }
    private void BindRecruitmentLocationType()
    {
        rblLocalOverseasRecruitment.DataSource = clsVacancyAddEdit.GetRecruitmentLocationType();
        rblLocalOverseasRecruitment.DataTextField = "RecruitmentType";
        rblLocalOverseasRecruitment.DataValueField = "RecruitmentTypeID";
        rblLocalOverseasRecruitment.DataBind();
        rblLocalOverseasRecruitment.SelectedValue = "3";
    }
    private void BindQualifications()
    {
        lstQualification.DataSource = clsVacancyAddEdit.GetAllQualifications(Request.QueryString["JobID"] == null ? -1 :Convert.ToInt32(Request.QueryString["JobID"]));
        lstQualification.DataTextField = "Degree";
        lstQualification.DataValueField = "DegreeID";
        lstQualification.DataBind();
    }

    #endregion

    void DesignationReference_OnSave(string SelectedValue)
    {
        BindDesignations();
        ddlDesignation.SelectedValue = SelectedValue;
        updDesignation.Update();
    }

    private bool Validate()
    {
        if (ddlCompany.SelectedValue == "-1")
        {
            RequiredFieldValidator4.Validate();
            return false;
        }
        else if (ddlDepartment.SelectedValue == "-1")
        {
            rfvddlDepartment.Validate();
            return false;
        }
        else if (ddlDesignation.SelectedValue == "-1")
        {
            rfvDesignation.Validate();
            return false;
        }
        else if (txtJobCode.Text.Trim() == string.Empty)
        {
            rfvtxtJobCodepnlTab1.Validate();
            return false;
        }
        else if (txtJobTitle.Text.Trim() == string.Empty)
        {
            rfvtxtVacancyTitlepnlTab1.Validate();
            return false;
        }
        else if (txtRequiredQuota.Text.Trim() == string.Empty)
        {
            rfvtxtNoofVacanciespnlTab1.Validate();
            return false;
        }
        else if (ddlEmploymentType.SelectedValue == "-1")
        {
            rfvddlEmploymentType.Validate();
            return false;
        }
        else if (txtLastDate.Text.Trim() == string.Empty)
        {
            rfvtxtLastDatepnlTab1.Validate();
            return false;
        }
        else
            return true;

    }
    private void ValidateData()
    {
        string msg = string.Empty;
        objUserMaster = new clsUserMaster();
        bool val = true;
        int type = 0;
        if (Validate())
        {
            if (clsVacancyAddEdit.DoesVacancyExist(ViewState["JobID"].ToInt32()))
            {
                type = 1;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحديث وظيفة، التي عملية مقابلة .") : ("Cannot update job, Interview process started .");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (clsCommon.Convert2DateTime(txtLastDate.Text) < Convert.ToDateTime(DateTime.Now.Date))
            {
                type = 1;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تاريخ صلاحية لا يمكن أن يكون أقدم من التاريخ الحالي.") : ("Validity date cannot be earlier than current date.");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (txtJoiningDate.Text.Trim() != string.Empty && clsCommon.Convert2DateTime(txtJoiningDate.Text) < Convert.ToDateTime(DateTime.Now.Date))
            {
                type = 2;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تاريخ الانضمام لا يمكن أن يكون أقدم من التاريخ الحالي.") : ("Joining date cannot be earlier than current date.");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (rblIsBudgeted.SelectedValue == "1" && (txtBudgetedAmount.Text == "" || txtBudgetedAmount.Text.ToDecimal() == 0))
            {
                type = 1;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("ادخل المبلغ المدرج في الميزانية. ") : ("Enter Budgeted Amount.");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (clsVacancyAddEdit.CheckDuplicateJobCode(ViewState["JobID"].ToInt32(), txtJobCode.Text.Trim()) > 0)
            {
                type = 1;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("رمز الوظيفة موجود بالفعل") : ("Job Code already exists");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (txtMaxAge.Text.ToInt32() != 0 || txtMinAge.Text.ToInt32() != 0)
            {
                if ((txtMaxAge.Text.ToInt32() < 18 || txtMinAge.Text.ToInt32() < 18))
                {
                    type = 2;
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وينبغي أن يكون الحد الأدنى للسن أكبر من 18") : ("Minimum age should be greater than 18");
                    mcMessage.InformationalMessage(msg);
                    mpeMessage.Show();
                    val = false;
                }
            }
            else if (txtMinAge.Text.ToInt32() > txtMaxAge.Text.ToInt32())
            {
                type = 2;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون الحد الأقصى لعمر أكبر من الحد الأدنى للسن") : ("Maximum age should be greater than minimum age");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }
            else if (clsLeaveRequest.GetRequestedTo(Convert.ToInt32(ViewState["JobID"]), objUserMaster.GetEmployeeId(), (int)eReferenceTypes.JobApproval, ddlCompany.SelectedValue.ToInt32()).ToInt32() <= 0 && hfMode.Value == "0")
            {
                type = 1;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في هذا الوقت. يرجى الاتصال بقسم الموارد البشرية.") : ("Your request cannot process this time. Please contact HR department.");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                val = false;
            }              
            if (val)
            {
                if (ViewState["JobID"] != null && ViewState["Approval"] != null)//Checking for criterias exists during vacancy approval
                {
                    if (JobApprovalProcess())
                        Save();
                }
                else
                    Save();
            }
            else
            {
                switch (type)
                {
                    case 1:
                        string script1 = "var d = setTimeout(\"setVacancyPostTab('" + pnlTab1.ClientID + "')\", 0)";
                        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), Guid.NewGuid().ToString(), script1, true);
                        break;
                    case 2:
                        string script2 = "var d = setTimeout(\"setVacancyPostTab('" + pnlTab2.ClientID + "')\", 0)";
                        ScriptManager.RegisterClientScriptBlock(txtLanguageCapabilities, txtLanguageCapabilities.GetType(), Guid.NewGuid().ToString(), script2, true);
                        break;
                }
            }
        }
    }
    private bool JobApprovalProcess()
    {
        bool val = true;
        if (Request.QueryString["Approval"].ToBoolean() == true && Convert.ToInt32(Request.QueryString["JobID"]) > 0)
        {
            objVacancy.JobID = Convert.ToInt32(Request.QueryString["JobID"]);
        }
        else
        {
            objVacancy.JobID = Convert.ToInt32(ViewState["JobID"]);
        }
        if (objUserMaster.GetEmployeeId() == -1)
            objVacancy.ApprovedBy = Convert.ToInt32(null);
        else
            objVacancy.ApprovedBy = objUserMaster.GetEmployeeId();

        if (ddlStatus.SelectedValue == "5" && clsLeaveRequest.IsHigherAuthority((int)eReferenceTypes.JobApproval, new clsUserMaster().GetEmployeeId(), Convert.ToInt32(ViewState["JobID"])))
        {
            objVacancy.JobStatusID = ddlStatus.SelectedValue.ToInt32();
            objVacancy.IsForwarded = false;
        }
        else
        {
            
            if (ddlStatus.SelectedValue == "3")//Rejected
            {
                objVacancy.JobStatusID = "3".ToInt32(); ;
                objVacancy.IsForwarded = false;                
            }
            else
            {
                objVacancy.JobStatusID = "1".ToInt32();
                objVacancy.IsForwarded = true;
            }
        }

        if (clsVacancyAddEdit.CheckCriteriaExists(objVacancy.JobID) > 0)
        {
            val = true;
        }
        else
        {
            if (ddlStatus.SelectedValue == "5")
            {
                val = false;
                string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة كما لا تضاف المقاييس") : ("Cannot Approve as criterias are not added");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }
        if (clsVacancyAddEdit.CheckPayStructureExists(objVacancy.JobID) > 0)
        {
            val = true;
        }
        else
        {
            val = false;
            string msg = clsUserMaster.GetCulture() == "ar-AE" ? ("Cannot Approve as PayStructure does not exist") : ("Cannot Approve as PayStructure does not exist");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
        }
        if (val)
        {
            if (objVacancy.UpdateJobWhenApproved() > 0)
                return true;
            else
                return false;
        }
        else
            return false;
    }
    private void Save()
    {
        try
        {
            if (ViewState["JobID"].ToInt32() > 0 && hfMode.Value == "1")
            {
                objVacancy.DeleteGeneralReferenceTables(ViewState["JobID"].ToInt32());// Deletes from QualificationDetails table   
                objVacancy.JobID = ViewState["JobID"].ToInt32();
            }

            objVacancy.JobCode = txtJobCode.Text;
            objVacancy.CompanyId = Convert.ToInt32(ddlCompany.SelectedValue);
            objVacancy.DepartmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            objVacancy.DesignationID = Convert.ToInt32(ddlDesignation.SelectedValue);
            objVacancy.JobTitle = txtJobTitle.Text.Trim();
            objVacancy.ExpiryDate = clsCommon.Convert2DateTime(txtLastDate.Text.Trim()).ToString("dd MMM yyyy");
            objVacancy.RequiredQuota = Convert.ToInt32(txtRequiredQuota.Text);
            objVacancy.EmploymentTypeID = Convert.ToInt32(ddlEmploymentType.SelectedValue);
            objVacancy.Remarks = txtRemarks.Text;
            objVacancy.Skills = txtSkills.Text;
            objVacancy.Experience = txtExperience.Text;
            objVacancy.PlaceOfWork = txtPlaceOfWork.Text;
            objVacancy.WeeklyWorkingDays = txtWeeklyWorkingDays.Text.ToInt32();
            objVacancy.DailyWorkingHours = txtDailyWorkingHours.Text.ToDecimal();
            if (rblAccomodation.SelectedValue == "1")
                objVacancy.IsAccomodation = false;
            else
                objVacancy.IsAccomodation = true;
            if (rblTransportation.SelectedValue == "1")
                objVacancy.IsTransportation = false;
            else
                objVacancy.IsTransportation = true;
            objVacancy.ContractPeriod = txtContractPeriod.Text;
            objVacancy.AirTicket = txtAirTicket.Text;
            objVacancy.AnnualLeaveDays = txtAnnualLeave.Text.ToInt32();
            objVacancy.LegalTerms = txtLegalTerms.Text;
            objVacancy.BudgetedAmount = txtBudgetedAmount.Text.ToDecimal();
            if (ViewState["CreatedBy"] != null)
            {
                objVacancy.CreatedBy = ViewState["CreatedBy"].ToInt32();
            }
            else
            {
                objVacancy.CreatedBy = objUserMaster.GetUserId();
            }

            if (ViewState["ApprovedBy"] != null)
            {
                objVacancy.ApprovedBy = ViewState["ApprovedBy"].ToInt32();
            }
            else
            {
                objVacancy.ApprovedBy = 0;
            }

            if (hfMode.Value == "0")
                objVacancy.JobStatusID = (int)eAction.Applied;

            if (rblIsBudgeted.SelectedValue == "1")
            {
                objVacancy.IsBudgeted = true;
                objVacancy.BudgetedAmount = txtBudgetedAmount.Text.ToDecimal();
            }
            else
            {
                objVacancy.IsBudgeted = false;
                objVacancy.BudgetedAmount = 0;
            }
            //tab2 - PREFERENCES
            objVacancy.Languages = txtLanguageCapabilities.Text.Trim();
            objVacancy.MinAge = txtMinAge.Text.ToInt32();
            objVacancy.MaxAge = txtMaxAge.Text.ToInt32();
            objVacancy.GenderRecruID = rblGender.SelectedValue.ToInt32();
            objVacancy.RecruitmentTypeID = rblLocalOverseasRecruitment.SelectedValue.ToInt32();
            objVacancy.RecruitmentFeesPayable = rblFeesPayable.SelectedValue.ToInt32() == 1 ? true : false;
            objVacancy.JoiningDate = clsCommon.Convert2DateTime(txtJoiningDate.Text.Trim()).ToString("dd MMM yyyy");

            if (hfMode.Value == "0")
                objVacancy.ReportingTo = clsLeaveRequest.GetRequestedTo(Convert.ToInt32(ViewState["JobID"]), objUserMaster.GetEmployeeId(), (int)eReferenceTypes.JobApproval, ddlCompany.SelectedValue.ToInt32()).ToInt32();//clsVacancyAddEdit.GetRequestedTo(objUserMaster.GetEmployeeId());

            int JobID = objVacancy.AddUpdateVacancy();
            ViewState["JobID"] = JobID;

            clsVacancyAddCriteria obj = new clsVacancyAddCriteria();
            clsVacancyAddCriteria.ID = JobID;
            clsVacancyPayStructure.Designation = ddlDesignation.SelectedItem.Text;
            clsVacancyPayStructure.ID = JobID;
            clsVacancyOfferLetterApproval.ID = JobID;
            clsVacancyKPIKRA.ID = JobID;

            if (JobID > 0)
            {
                objVacancy.JobID = JobID;
                foreach (ListItem chk in lstQualification.Items)
                {
                    if (chk.Selected)
                    {
                        objVacancy.DegreeId = chk.Value.ToInt32();
                        objVacancy.InsertQualificationList();
                    }
                }
            }
            string msg = string.Empty;
            string dispMsg = string.Empty;
            //Messages
            if (hfMode.Value == "0")    //On Insertion of a new Job
            {
                clsCommonMessage.SendMessage(new clsUserMaster().GetEmployeeId(), objVacancy.JobID, eReferenceTypes.JobApproval, eMessageTypes.Vacancy_Approval, eAction.Applied, "");
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد أضيفت المهمة بنجاح") : ("Vacancy has been Added successfully");
                mcMessage.InformationalMessage(msg);
                hfMode.Value = "1";
            }
            else if (hfMode.Value == "1" && ViewState["Approval"] != null)
            {
                if (ddlStatus.SelectedValue.ToInt32() == 3)         //On Rejection of a job 
                {
                    clsCommonMessage.SendMessage(null, objVacancy.JobID, eReferenceTypes.JobApproval, eMessageTypes.Vacancy_Approval, eAction.Reject, "");
                    dispMsg = "Rejected";
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد رفض الوظيفة") : ("Vacancy has been Rejected");
                    mcMessage.InformationalMessage(msg);
                }
                else if (ddlStatus.SelectedValue.ToInt32() == 5)    //On Approval of a job   
                {
                    clsCommonMessage.SendMessage(null, objVacancy.JobID, eReferenceTypes.JobApproval, eMessageTypes.Vacancy_Approval, eAction.Approved, "");

                    if (clsLeaveRequest.IsHigherAuthority((int)eReferenceTypes.JobApproval, new clsUserMaster().GetEmployeeId(), Convert.ToInt32(ViewState["JobID"])))
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد وافق المهمة بنجاح") : ("Vacancy has been Approved successfully");
                        dispMsg = "Approved";
                    }
                    else
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد وافق المهمة بنجاح") : ("Vacancy has been forwarded successfully");
                        dispMsg = "forwarded";
                    }
                    mcMessage.InformationalMessage(msg);
                }
            }
            else   //On Updation of a job
            {
                clsCommonMessage.SendMessage(new clsUserMaster().GetEmployeeId(), objVacancy.JobID, eReferenceTypes.JobApproval, eMessageTypes.Vacancy_Approval, eAction.Applied, "");
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث المهمة بنجاح") : ("Vacancy has been Updated successfully");
                mcMessage.InformationalMessage(msg);
            }
            mpeMessage.Show();
            if ((hfMode.Value == "1" && (Request.QueryString["Approval"] != null && Request.QueryString["JobID"] != null)))
            {

                string RedirectLink = "Vacancy.aspx?dispMsg=" + dispMsg;//JobID=" + Request.QueryString["JobID"].ToInt32() + "&
                Response.Redirect(RedirectLink);
            }
            lnkVacancyCriteria.Visible = lnkVacancyPayStructure.Visible = lnkVacancyOfferLetterApproval.Visible =
            imgVacancyCriteria.Visible = imgVacancyPayStructure.Visible = imgvacancyOffer.Visible = true;
            upMenu.Update();
        }
        catch (Exception ex )
        { 
        
        
        }
    }

    public void AddNewDegree()
    {
        ucDegreeReference.FillList();
        mpeDegreeReference.Show();
        upDegreeReference.Update();

    }
    #endregion
}
