﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="CreateEmployee.aspx.cs" Inherits="Public_CreateEmployee" Title="Employee" %>

<%@ Reference Control="~/controls/Message.ascx" %>
<%@ Register Src="../controls/SalaryScale.ascx" TagName="SalaryScale" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
  
   
    <script src="../js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>

    <script src="../js/jquery/bootstrap-multiselect.js" type="text/javascript"></script>

    <script src="../js/jquery/bootstrap-2.3.2.min.js" type="text/javascript"></script>

<style type="text/css">
  select
  {
  	margin-left:0px;
  }
</style>

    <div id='cssmenu'>
        <ul>
            <li class="selected"><a href='Employee.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal60" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function filRecentPhoto_UploadedComplete(source, args)
        {
            var length = args.get_length();
            //var fileupload = $get('<%=filRecentPhoto.ClientID %>');
            var fileupload = $get('ctl00_public_content_filRecentPhoto_ctl02');
            
//            if(!isImage('ctl00_ctl00_content_public_content_filRecentPhoto_ctl02'))
            if(!isImage('ctl00_public_content_filRecentPhoto_ctl02'))
            {
                alert('Invalid file type');            
            }
//            else if(length > 307200)
//            {
//               alert('Image Size Exceeds Allowed Limit (300KB)');    
//               var input = fileupload.getElementsByTagName('input');
//               for(var i=0;i<input.length;i++)
//               {
//                   if(input[i].type == 'file')
//                   {
//                        input[i].value = "";
//                        input[i].style.backgroundColor = "white";
//                   }
//                    
//               }
//            }
            else
            WidthPreview2(source, args, 300, 'imgRecentPhotoPreview','Recent');
        }
        
        
        
    </script>
    
    

    <div style="width: 100%">
        <div>
            <div id="div1" class="TabbedPanels" style="width: 100%;">
                <ul class="TabbedPanelsTabGroup">
                    <li class="TabbedPanelsTabSelected" id="pnlTab1" onclick="SetEmployeeTab(this.id)">
                        <%-- Employee Details--%>
                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="EmployeeDetails"></asp:Literal>
                    </li>
                    <li class="TabbedPanelsTab" id="pnlTab2" onclick="SetEmployeeTab(this.id)">
                        <%--                    Work Profile--%>
                        <asp:Literal ID="Literal53" runat="server" meta:resourcekey="WorkProfile"></asp:Literal>
                    </li>
                    <li class="TabbedPanelsTab" id="pnlTab3" onclick="SetEmployeeTab(this.id)">
                        <%--   Personal Facts--%>
                        <asp:Literal ID="Literal54" runat="server" meta:resourcekey="PersonalFacts"></asp:Literal>
                    </li>
                    <li class="TabbedPanelsTab" id="pnlTab4" onclick="SetEmployeeTab(this.id)">
                        <%-- Accounts Info--%>
                        <asp:Literal ID="Literal55" runat="server" meta:resourcekey="AccountsInfo"></asp:Literal>
                    </li>
                    <li class="TabbedPanelsTab" id="pnlTab5" onclick="SetEmployeeTab(this.id)">
                        <%-- Contacts Info--%>
                        <asp:Literal ID="Literal56" runat="server" meta:resourcekey="ContactsInfo"></asp:Literal>
                    </li>
                </ul>
            </div>
            <div class="TabbedPanelsContentGroup">
                <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Working At--%>
                            <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,WorkingAt %>'></asp:Literal>
                            <font color="Red">*</font>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updCompany" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 35%; height: 29px">
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist_mandatory"
                                            DataTextField="Name" DataValueField="CompanyID" Width="200px" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged" Style="background-color: InfoBackground;">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: right; width: 60%; height: 29px; padding-left: 3px; line-height: normal">
                                        <asp:RequiredFieldValidator ID="rfvCompanypnlTab1" runat="server" ControlToValidate="ddlCompany"
                                            CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectCompany" ValidationGroup="Employee"
                                            InitialValue="-1"></asp:RequiredFieldValidator>
                                        <asp:HiddenField ID="hdFinYearStartDate" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="width: 100%">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:ControlsCommon,EmployeeNumber %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:UpdatePanel ID="updEmpNo" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtEmployeeNumber" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                            Width="150px" Text='<%# Eval("EmployeeNumber") %>'></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                                <asp:RequiredFieldValidator ID="rfvEmployeeNumberpnlTab1" runat="server" ControlToValidate="txtEmployeeNumber"
                                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter employee number"
                                    ValidationGroup="Employee"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Salutation--%>
                                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ControlsCommon,Salutation %>'></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updSalutation" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%; height: 29px">
                                            <asp:DropDownList ID="ddlSalutation" runat="server" CssClass="dropdownlist_mandatory"
                                                Width="80px" Style="background-color: InfoBackground;">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnSalutation" runat="server" CausesValidation="False" CommandName="Accounthead"
                                                CssClass="referencebutton" OnClick="btnSalutation_Click" Text="..." />
                                        </div>
                                        <div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                                            <asp:RequiredFieldValidator ID="rfvSalutationpnlTab1" runat="server" ControlToValidate="ddlSalutation"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectSalutation"
                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div style="width: 50%">
                                <div class="trLeft" style="float: left; width: 50%; height: 29px">
                                    <%-- First Name--%>
                                    <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ControlsCommon,FirstName %>'></asp:Literal>
                                    <font color="Red">*</font>
                                </div>
                                <div class="trRight" style="float: left; width: 42%; height: auto">
                                    <div style="float: left; width: 42%; height: 29px">
                                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                            Width="150px" Text='<%# Eval("FirstName") %>' Style="background-color: InfoBackground;"></asp:TextBox>
                                    </div>
                                    <div style="clear: both; float: left; width: 100%; height: auto; padding-left: 3px;
                                        line-height: normal">
                                        <asp:RequiredFieldValidator ID="rfvFirstNamepnlTab1" runat="server" ControlToValidate="txtFirstName"
                                            CssClass="error" Display="Dynamic" meta:resourcekey="PleaseEnterFirstName" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div style="float: right; width: 50%">
                                <div class="trRight" style="float: left; width: 56%; height: 29px">
                                    <asp:TextBox Style="margin-left: 60px; background-color: InfoBackground" ID="txtFirstNameArb"
                                        runat="server" CssClass="textbox" MaxLength="50" Width="150px" Text='<%# Eval("FirstNameArb") %>'></asp:TextBox>
                                    <div style="clear: both; float: left; width: 100%; height: auto; margin-left: 58px;
                                        padding-top: 4px; line-height: normal">
                                        <asp:RequiredFieldValidator ID="rfFirstNameArbpnlTab1" runat="server" ControlToValidate="txtFirstNameArb"
                                            CssClass="error" Display="Dynamic" meta:resourcekey="PleaseEnterFirstNameArb"
                                            ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="trLeft" style="float: left; text-align: right; width: 35%; height: 29px">
                                    الأسم الأول
                                </div>
                            </div>
                        </div>
                        <div style="clear: both; height: auto">
                            <div style="width: 100%">
                                <div style="float: left; width: 50%">
                                    <div class="trLeft" style="float: left; width: 50%; height: 29px">
                                        <%--  Middle Name--%>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:ControlsCommon,MiddleName %>'></asp:Literal>
                                    </div>
                                    <div class="trRight" style="float: left; width: 25%; height: 29px">
                                        <asp:TextBox ID="txtMiddleName" runat="server" CssClass="textbox" MaxLength="50"
                                            Width="150px" Text='<%# Eval("MiddleName") %>'></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: right; width: 50%">
                                    <div class="trRight" style="float: left; width: 56%; height: 29px">
                                        <asp:TextBox Style="margin-left: 60px" ID="txtMiddleNameArabic" runat="server" CssClass="textbox"
                                            MaxLength="50" Width="150px" Text='<%# Eval("MiddleNameArb") %>'></asp:TextBox>
                                    </div>
                                    <div class="trLeft" style="float: left; text-align: right; width: 35%; height: 29px">
                                        الاسم الأوسط
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="clear: both; height: auto">
                            <div style="float: left; width: 50%">
                                <div class="trLeft" style="float: left; width: 50%; height: 29px">
                                    <%--  Last Name--%>
                                    <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,LastName %>'></asp:Literal>
                                </div>
                                <div class="trRight" style="float: left; width: 44%; height: 29px">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" MaxLength="50" Width="150px"
                                        Text='<%# Eval("LastName") %>'></asp:TextBox>
                                </div>
                            </div>
                            <div style="float: right; width: 50%">
                                <div class="trRight" style="float: left; width: 56%; height: 29px">
                                    <asp:TextBox Style="margin-left: 60px" ID="txtLastNameArb" runat="server" CssClass="textbox"
                                        MaxLength="50" Width="150px" Text='<%# Eval("LastNameArb") %>'></asp:TextBox>
                                </div>
                                <div class="trLeft" style="float: left; text-align: right; width: 35%; height: 29px">
                                    اسم العائلة
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--Gender--%>
                            <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,Gender %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text='<%$Resources:ControlsCommon,Male %>'>
                                
                                
                                <%--Male--%>
                                
                                </asp:ListItem>
                                <asp:ListItem Text='<%$Resources:ControlsCommon,FeMale %>'>
                                
                               <%-- Female--%>
                               
                                </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Date of Birth--%>
                            <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,DateOfBirth %>'></asp:Literal>
                            <font color="Red">*</font>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:TextBox ID="txtDateofBirth" runat="server" CssClass="textbox_mandatory" Width="100px"
                                    MaxLength="10" Text='<%# GetDate(Eval("DateofBirth")) %>' Style="background-color: InfoBackground;"></asp:TextBox>
                                <asp:ImageButton ID="btnDateofBirth" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="False" />
                                <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Animated="true"
                                    Format="dd/MM/yyyy" PopupButtonID="btnDateofBirth" TargetControlID="txtDateofBirth" />
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                                <asp:RequiredFieldValidator ID="RfvDOBpnlTab1" runat="server" ControlToValidate="txtDateofBirth"
                                    CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectDateOfBirth"
                                    ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvDateofBirthpnlTab1" runat="server" ClientValidationFunction="validateDateofBirth"
                                    ControlToValidate="txtDateofBirth" CssClass="error" Display="Dynamic" ValidateEmptyText="True"
                                    ValidationGroup="Employee"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Work Status--%>
                            <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:ControlsCommon,WorkStatus %>'></asp:Literal>
                            <font color="Red">*</font>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updWorkStatus" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="float: left; width: 45%; height: 29px">
                                        <asp:DropDownList ID="ddlWorkStatus" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="200px" AutoPostBack="True" ClientIndexChanged="chkWorkstatus" Style="background-color: InfoBackground;"
                                            OnSelectedIndexChanged="ddlWorkStatus_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnWorkStatus" runat="server" CausesValidation="False" CommandName="Accounthead"
                                            CssClass="referencebutton" OnClick="btnWorkStatus_Click" Text="..." />
                                    </div>
                                    <div style="float: right; width: 52%; height: 29px; padding-left: 3px; line-height: normal">
                                        <asp:RequiredFieldValidator ID="rfvWorkStatuspnlTab2pnlTab1" runat="server" ControlToValidate="ddlWorkStatus"
                                            CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectWorkStatus"
                                            ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Nationality--%>
                            <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Nationality %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updNationality" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlNationality" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnNationality" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        OnClick="btnNationality_Click" Text="..." />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%-- Country--%>
                            <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,Country %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlCountry" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        Text="..." OnClick="btnCountry_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%-- Religion--%>
                            <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:ControlsCommon,Religion %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updReligion" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlReligion" runat="server" Width="200px">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnReligion" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        OnClick="btnReligion_Click" Text="..." />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%-- Employment Type--%>
                        <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,EmploymentType %>'></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="updEmployementType" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DropDownList ID="ddlEmployementType" runat="server" CssClass="dropdownlist_mandatory"
                                    Width="200px" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:Button ID="btnEmployementType" runat="server" CausesValidation="False" Text="..."
                                    CommandName="Accounthead" CssClass="referencebutton" OnClick="btnEmployementType_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%-- Date of Joining--%>
                            <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,DateOfJoining %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <div style="float: left; width: 25%; height: 29px">
                                <asp:TextBox ID="txtDateofJoining" runat="server" CssClass="textbox" Width="100px"
                                    MaxLength="10" onchange="setupProbationDates(this);" Text='<%# GetDate(Eval("DateofJoining")) %>'></asp:TextBox>
                                <asp:HiddenField ID="hdCandidateID" runat="server" Value="false" />
                                <asp:HiddenField ID="hdcandidateDate" runat="server" Value="false" />
                                <asp:HiddenField ID="hdCurrentDate" runat="server" Value="false" />
                                <asp:ImageButton ID="btnDateofJoining" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                    CausesValidation="False" />
                                <AjaxControlToolkit:CalendarExtender ID="ceDateofJoining" runat="server" Animated="true"
                                    Format="dd/MM/yyyy" PopupButtonID="btnDateofJoining" TargetControlID="txtDateofJoining" />
                            </div>
                            <div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                                <asp:CustomValidator ID="cvDateofJoiningFinancialYearpnlTab1" runat="server" ControlToValidate="txtDateofJoining"
                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Employee" ClientValidationFunction="validateDateofJoining"
                                    Display="Dynamic"></asp:CustomValidator>
                            </div>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%-- Probation End Date--%>
                            <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,ProbationEndDate %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div style="float: left; width: 25%; height: 29px">
                                        <asp:TextBox ID="txtProbationEndDate" runat="server" CssClass="textbox" Width="100px"
                                            MaxLength="10" Text='<%# GetDate(Eval("ProbationEndDate")) %>' Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="btnProbationEndDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CausesValidation="False" />
                                        <AjaxControlToolkit:CalendarExtender ID="ceProbationEndDate" runat="server" Animated="true"
                                            Format="dd/MM/yyyy" PopupButtonID="btnProbationEndDate" TargetControlID="txtProbationEndDate" />
                                        <div style="display: none">
                                            <asp:HiddenField ID="hdDefaultProbationPeriod" runat="server" Value='<%# Eval("DefaultProbationPeriod") %>' />
                                        </div>
                                    </div>
                                    <div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                                        <asp:CustomValidator ID="cvProbationEndDatepnlTab1" runat="server" ControlToValidate="txtProbationEndDate"
                                            CssClass="error" Display="Dynamic" ValidationGroup="Employee" ClientValidationFunction="validateProbationEndDate"
                                            ValidateEmptyText="True"></asp:CustomValidator>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,AppraisalDate %>'></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div style="float: left; width: 25%; height: 29px">
                                        <asp:TextBox ID="txtAppraisalDate" runat="server" CssClass="textbox" Width="100px" Format="dd/MM/yyyy"
                                            MaxLength="10" Text='<%# GetDate(Eval("ProbationEndDate")) %>' Enabled="true"></asp:TextBox>
                                        <asp:ImageButton ID="btnAppraisalDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CausesValidation="False" />
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Animated="true"
                                            Format="dd/MM/yyyy" PopupButtonID="btnAppraisalDate" TargetControlID="txtAppraisalDate" />
                                    </div>
                                    <div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtAppraisalDate"
                                            CssClass="error" Display="Dynamic" ValidationGroup="Employee" ValidateEmptyText="True"></asp:CustomValidator>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div class="TabbedPanelsContent" id="divTab2" style="display: none;">
                    <div style="width: 100%">
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--Work Location--%>
                                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,WorkLocation %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updWorkLocation" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 45%; height: 29px">
                                            <asp:DropDownList ID="ddlWorkLocation" runat="server" CssClass="dropdownlist_mandatory"
                                                Width="200px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Department--%>
                                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:ControlsCommon,Department %>'></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updDepartment" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="200px" AutoPostBack="True" Style="background-color: InfoBackground;">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnDepartment" runat="server" CausesValidation="False" CommandName="Accounthead"
                                            Text="..." CssClass="referencebutton" OnClick="btnDepartment_Click" />
                                        <div style="float: right; width: 52%; height: 29px; padding-left: 3px; line-height: normal">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3pnlTab2" runat="server" ControlToValidate="ddlDepartment"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectDepartment"
                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Designation
                                --%>
                                <asp:Literal ID="Literal181" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updDesignation" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="dropdownlist_mandatory"
                                            DataTextField="Designation" DataValueField="DesignationID" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged"
                                            AutoPostBack="True" Width="200px" Style="background-color: InfoBackground;">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnDesignation" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                    Text="..."  OnClick="btnDesignation_Click" />
                                        <div style="float: right; width: 52%; height: 29px; padding-left: 3px; line-height: normal">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4pnlTab2" runat="server" ControlToValidate="ddlDesignation"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectDesignation"
                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--    Work Policy--%>
                                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:ControlsCommon,WorkPolicy %>'></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updWorkPolicy" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlPolicy" runat="server" CssClass="dropdownlist_mandatory"
                                            DataTextField="Description" DataValueField="PolicyID" Width="200px" Style="background-color: InfoBackground;">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnPolicy" runat="server" Text="Button" Style="display: none;" />
                                        <div style="float: right; width: 52%; height: 29px; padding-left: 3px; line-height: normal">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1pnlTab2" runat="server" ControlToValidate="ddlPolicy"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectWorkPolicy"
                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--Leave Policy--%>
                                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:ControlsCommon,LeavePolicy %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updLeavePolicy" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlLeavePolicy" runat="server" CssClass="dropdownlist" DataTextField="LeavePolicyName"
                                            DataValueField="LeavePolicyID" Width="200px">
                                        </asp:DropDownList>
                                        <asp:Panel ID="pnlLeavePolicy" runat="server" Style="display: none;">
                                        </asp:Panel>
                                        <asp:Button ID="btnLeavePolicy" runat="server" Text="Button" Style="display: none;" />
                                        <AjaxControlToolkit:ModalPopupExtender ID="mpeLeavePolicy" runat="server" BackgroundCssClass="modalBackground"
                                            PopupControlID="pnlLeavePolicy" TargetControlID="btnLeavePolicy" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Reporting To--%>
                                <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:ControlsCommon,ReportingTo %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updReportingTo" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlReportingTo" runat="server" CssClass="dropdownlist" DataTextField="EmployeeName"
                                            DataValueField="EmployeeID" Width="200px">
                                        </asp:DropDownList>
                                        <asp:CheckBox ID="chkEmployees" runat="server" Text='<%$Resources:ControlsCommon,SelectAllEmployees%>'
                                            AutoPostBack="True" OnCheckedChanged="chkEmployees_CheckedChanged" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                         <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Reporting To--%>
                                <asp:Literal ID="Literal62" runat="server" Text='<%$Resources:ControlsCommon,HOD%>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="updHOD" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlHOD" runat="server" CssClass="dropdownlist" DataTextField="EmployeeName"
                                            DataValueField="EmployeeID" Width="200px">
                                        </asp:DropDownList>
                                        <asp:CheckBox ID="chkHOD" runat="server" Text='<%$Resources:ControlsCommon,SelectAllEmployees%>'
                                            AutoPostBack="True" OnCheckedChanged="chkHOD_CheckedChanged" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                          <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Reporting To--%>
                                <asp:Literal ID="Literal64" runat="server" Text='<%$Resources:ControlsCommon,ProcessType%>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                               
                                        <asp:DropDownList ID="ddlProcessType" runat="server" CssClass="dropdownlist" DataTextField="ProcessType"
                                            DataValueField="ProcessTypeID" Width="200px">
                                        </asp:DropDownList>
                                      
                                   
                            </div>
                        </div>
                        <div style="height: auto;">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Official E-mail--%>
                                <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:ControlsCommon,OfficialEmail %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtOfficialEmail" runat="server" CssClass="textbox" Width="250px"
                                    MaxLength="50" Text='<%# Eval("Email")%>'></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RfvOfficialEmailpnlTab2" runat="server" ControlToValidate="txtOfficialEmail"
                                    CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterValidEmail %>'
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Employee"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--             Password--%>
                                <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:ControlsCommon,Password %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtOfficialEmailPassword" runat="server" CssClass="textbox" TextMode="Password"
                                    MaxLength="50" Width="125px"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RfvOfficialEmailPasswordpnlTab2" runat="server" ControlToValidate="txtOfficialEmailPassword"
                                    CssClass="error" Display="Static" ErrorMessage="Please enter official email password"
                                    ValidationGroup="Employee"></asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                           <div  id="divDeviceUser" runat ="server" style="display:none; height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Reporting To--%>
                                <asp:Literal ID="Literal63" runat="server" Text='<%$Resources:ControlsCommon,DeviceUser %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                      <asp:TextBox ID="txtDeviceUserID" runat ="server"  MaxLength ="30"></asp:TextBox>  
                            </div>
                        </div>
                        <%--  <div style="height: auto" id="dvChkUser" runat="server">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                             
                                <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,User %>'></asp:Literal>
                            </div>
                            <asp:UpdatePanel ID="updChkEmployee" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                                        <asp:CheckBox ID="chkUser" runat="server" meta:resourcekey="SetEmployeeAsUser" AutoPostBack="true"
                                            CausesValidation="false" OnCheckedChanged="chkUser_CheckedChanged" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <asp:UpdatePanel ID="updRoles" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="height: auto" id="dvRole" runat="server">
                                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                      
                                        <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ControlsCommon,Role %>'></asp:Literal>
                                    </div>
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                                        <asp:DropDownList ID="ddlRole" runat="server" CssClass="dropdownlist_disabled" DataTextField="RoleName"
                                            DataValueField="RoleID" Width="200px" Enabled="False">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvrolepnlTab2" runat="server" ControlToValidate="ddlRole"
                                            CssClass="error" Display="Static" ErrorMessage="Please select any role" InitialValue="-1"
                                            SetFocusOnError="True" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="height: auto" id="dvUserName" runat="server">
                                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                        
                                        <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,UserName %>'></asp:Literal>
                                    </div>
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtusername" runat="server" CssClass="textbox_disabled" MaxLength="40"
                                            Width="150px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUsernamepnlTab2pnlTab2" runat="server" ControlToValidate="txtusername"
                                            CssClass="error" Display="Static" ErrorMessage="Please enter user name" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="height: auto" id="dvPassword" runat="server">
                                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                 
                                        <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:ControlsCommon,Password %>'></asp:Literal>
                                    </div>
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtpassword" runat="server" CssClass="textbox_disabled" MaxLength="40"
                                            TextMode="Password" Width="150px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvpasswordpnlTab2pnlTab2" runat="server" ControlToValidate="txtpassword"
                                            CssClass="error" Display="Static" ErrorMessage="Please enter password" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </div>
                </div>
                <div class="TabbedPanelsContent" id="divTab3" style="display: none;">
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--Father's Name--%>
                            <asp:Literal ID="Literal28" runat="server" meta:resourcekey="FathersName"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:TextBox ID="txtFathersName" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                Width="125px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--  Mothers's Name--%>
                            <asp:Literal ID="Literal29" runat="server" meta:resourcekey="MothersName"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:TextBox ID="txtMothersName" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                Width="125px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%-- Spouse Name--%>
                            <asp:Literal ID="Literal30" runat="server" meta:resourcekey="SpouseName"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:TextBox ID="txtSpouseName" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                Width="125px"></asp:TextBox>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--Languages--%>
                            <asp:Literal ID="Literal52" runat="server" meta:resourcekey="Languages"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                             <select id="ddlLanguage" runat="server" multiple="true">
                                    </select>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <asp:Literal ID="Literal51" runat="server" meta:resourcekey="MotherTongue"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <asp:UpdatePanel ID="updMotherTongue" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                      <asp:DropDownList ID="ddlMotherTongue" runat="server" CssClass="dropdownlist" Width="200px">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnMotherTongue" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        Text="..." OnClick="btnMotherTongue_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%;">
                            <%-- Identification--%>
                            <asp:Literal ID="Literal50" runat="server" meta:resourcekey="Identification"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%;">
                            <asp:TextBox ID="txtIdentification" TextMode="MultiLine" runat="server" CssClass="textbox"
                                MaxLength="200" Width="270px" Height="40" onchange="RestrictMulilineLength(this, 200);"
                                onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                    <div style="height: auto">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--  Blood Group--%>
                            <asp:Literal ID="Literal49" runat="server" meta:resourcekey="BloodGroup"></asp:Literal>
                        </div>
                        <div class="trRight" style="float: left; width: 70%; height: 29px">
                            <%--<asp:TextBox ID="txtBloodGroup" runat="server" CssClass="textbox_mandatory" MaxLength="20"
                                Width="125px"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddlBloodGroup" runat="server" Width="70px">
                                <asp:ListItem Value="A+">A+</asp:ListItem>
                                <asp:ListItem Value="A-">A-</asp:ListItem>
                                <asp:ListItem Value="B+">B+</asp:ListItem>
                                <asp:ListItem Value="B-">B-</asp:ListItem>
                                <asp:ListItem Value="AB+">AB+</asp:ListItem>
                                <asp:ListItem Value="AB-">AB-</asp:ListItem>
                                <asp:ListItem Value="O+">O+</asp:ListItem>
                                <asp:ListItem Value="O-">O-</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="updRecentPhoto" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="height: auto; margin: 0px; padding: 0px">
                                <div class="trLeft" style="float: left; width: 25%;">
                                    <%-- Recent photo--%>
                                    <asp:Literal ID="Literal48" runat="server" Text='<%$Resources:ControlsCommon,RecentPhoto %>'></asp:Literal>
                                </div>
                                <div class="trRight" style="float: left; width: 70%; margin-left: 6px">
                                    <AjaxControlToolkit:AsyncFileUpload ID="filRecentPhoto" runat="server" PersistFile="True"
                                        onpaste="return false;" OnClientUploadComplete="filRecentPhoto_UploadedComplete"
                                        OnUploadedComplete="filRecentPhoto_UploadedComplete" EnableViewState="false" />
                                    <div style="display: block;" id="divImage" runat="server">
                                        <asp:Image ID="imgRecentPhotoPreview" runat="server" Height="90px" Width="75px" />
                                        <asp:HiddenField ID="hdnRecentPhotoURL" Value="" runat="server" />
                                        <asp:HiddenField ID="hdnRecentGUID" Value="" runat="server" />
                                        <asp:LinkButton ID="lnkremoveRecentPhoto" CausesValidation="false" meta:resourcekey="RemovePhoto"
                                            runat="server" OnClick="lnkremoveRecentPhoto_Click">
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div style="clear: both">
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="TabbedPanelsContent" id="divTab4" style="display: none;">
                    <div style="width: 100%">
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Transaction Type--%>
                                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:ControlsCommon,TransactionType %>'></asp:Literal>
                                <font color="Red">*</font>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="upTransactionType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlTransactionType" runat="server" CssClass="dropdownlist"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlTransactionType_SelectedIndexChanged"
                                            DataTextField="Description" DataValueField="TransactionTypeID" Width="200px"
                                            Style="background-color: InfoBackground;">
                                        </asp:DropDownList>
                                        <div style="float: right; width: 60%; height: 29px; padding-left: 3px; line-height: normal">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7pnlTab4" runat="server" ControlToValidate="ddlTransactionType"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectTransactionType"
                                                ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                            <asp:HiddenField ID="HiddenField2" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Employer Bank Name--%>
                                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankName %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="upEmployerBank" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlEmployerBankName" runat="server" CssClass="dropdownlist"
                                            DataTextField="BankName" DataValueField="BankNameID" Width="200px" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlEmployerBankName_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:CustomValidator ID="cvEmployerBankNamepnlTab4pnlTab4" runat="server" ClientValidationFunction="validateTransactionType"
                                            ControlToValidate="ddlEmployerBankName" CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectEmployersBank"
                                            ValidationGroup="Employee"></asp:CustomValidator>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlCompany" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlTransactionType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Employer Bank A/C--%>
                                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankAcc %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="upEmployerBankAC" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlCompanyAccount" runat="server" CssClass="dropdownlist" DataTextField="AccountNo"
                                            DataValueField="CompanyAccID" Width="200px">
                                        </asp:DropDownList>
                                        <asp:CustomValidator ID="cvCompanyAccountpnlTab4pnlTab4" runat="server" ClientValidationFunction="validateTransactionType"
                                            ControlToValidate="ddlCompanyAccount" CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectEmployerBankAcc"
                                            ValidationGroup="Employee"></asp:CustomValidator>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlCompany" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlEmployerBankName" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlTransactionType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--Employee Bank Name--%>
                                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankName %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="upEmployeeBank" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlEmployeeBankName" runat="server" CssClass="dropdownlist"
                                            Width="200px" DataTextField="BankName" DataValueField="BankNameID">
                                        </asp:DropDownList>
                                        <asp:CustomValidator ID="cvEmployeeBankNamepnlTab4pnlTab4" runat="server" ClientValidationFunction="validateTransactionType"
                                            ControlToValidate="ddlEmployeeBankName" CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectEmployeeBank"
                                            ValidationGroup="Employee"></asp:CustomValidator>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlCompany" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlEmployerBankName" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlTransactionType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--   Employee Bank A/C--%>
                                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankAcc %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="textbox" Width="192px"
                                            MaxLength="40" Text='<%# Eval("AccountNumber") %>'></asp:TextBox>
                                        <asp:CustomValidator ID="cvAccountNumberpnlTab4pnlTab4" runat="server" ClientValidationFunction="validateTransactionType"
                                            ValidateEmptyText="True" ControlToValidate="txtAccountNumber" CssClass="error"
                                            Display="Dynamic" meta:resourcekey="PleaseEnterEmployeeBankAcc" ValidationGroup="Employee"></asp:CustomValidator>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlTransactionType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="TabbedPanelsContent" id="divTab5" style="display: none;">
                    <div style="width: 100%">
                        <div style="padding-left: 10px; margin-bottom: 10px">
                            <h4>
                                <%-- Permanent Info--%>
                                <asp:Literal ID="Literal47" runat="server" Text='<%$Resources:ControlsCommon,PermanentInfo %>'></asp:Literal>
                            </h4>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%--   Address--%>
                                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:ControlsCommon,AddressLine1%>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:TextBox ID="txtAddressLine1" TextMode="MultiLine" runat="server" CssClass="textbox"
                                    MaxLength="200" Width="270px" Height="40" Text='<%# Eval("AddressLine1") %>'
                                    onchange="RestrictMulilineLength(this, 100);" onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%--  Phone--%>
                                <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:ControlsCommon,AddressLine2 %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 50px">
                                <asp:TextBox ID="txtAddressLine2" TextMode="MultiLine" runat="server" CssClass="textbox"
                                    MaxLength="200" Width="270px" Height="40" Text='<%# Eval("AddressLine2") %>'
                                    onchange="RestrictMulilineLength(this, 100);" onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox>
                               
                            </div>
                        </div>
                        <div style="height: auto;">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  E-mail--%>
                                <asp:Literal ID="Literal38" runat="server" Text='<%$Resources:ControlsCommon,City %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtCity" runat="server" CssClass="textbox" Width="250px" MaxLength="50"
                                    Text='<%# Eval("City")%>'></asp:TextBox>
                               
                            </div>
                            </div> 
                            <div style="height: auto;">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  E-mail--%>
                                <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ControlsCommon,state %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtState" runat="server" CssClass="textbox" Width="250px" MaxLength="50"
                                    Text='<%# Eval("StateProvinceRegion")%>'></asp:TextBox>
                               
                            </div>
                            </div>
                            <div style="height: auto;">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  E-mail--%>
                                <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,Zip %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtZip" runat="server" CssClass="textbox" Width="250px" MaxLength="20"
                                    Text='<%# Eval("ZipCode")%>'></asp:TextBox>
                               
                            </div>
                            </div> 
                             <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  Phone--%>
                                <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:ControlsCommon,PermanentPhone %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtPermanentPhone" runat="server" CssClass="textbox" MaxLength="15"
                                    Width="170px" Text='<%# Eval("LocalPhone") %>'></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                    FilterType="Numbers,Custom" TargetControlID="txtPermanentPhone" ValidChars="+ ()-">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div style="height: auto;">
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%--  E-mail--%>
                                <asp:Literal ID="Literal61" runat="server" Text='<%$Resources:ControlsCommon,PermanentEmail %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" Width="250px" MaxLength="50"
                                    Text='<%# Eval("Email")%>'></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1pnlTab5" runat="server"
                                    ControlToValidate="txtEmail" CssClass="error" Display="Dynamic" ErrorMessage='<%$Resources:ControlsCommon,PleaseEnterValidEmail %>'
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Employee"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                            
                            
                       
                        <div style="padding-left: 10px; margin-bottom: 10px">
                            <h4>
                                <%--  Emergency Info--%>
                                <asp:Literal ID="Literal46" runat="server" Text='<%$Resources:ControlsCommon,EmergencyInfo %>'></asp:Literal>
                            </h4>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%-- Emergency Address--%>
                                <asp:Literal ID="Literal39" runat="server" Text='<%$Resources:ControlsCommon,EmergencyAddress %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:TextBox ID="txtEmergencyAddress" TextMode="MultiLine" runat="server" CssClass="textbox"
                                    MaxLength="200" Width="270px" Height="40" Text='<%# Eval("LocalAddress") %>'
                                    onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%--Emergency Phone--%>
                                <asp:Literal ID="Literal40" runat="server" Text='<%$Resources:ControlsCommon,EmergencyPhone %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:TextBox ID="txtEmergencyPhone" runat="server" CssClass="textbox" MaxLength="15"
                                    Width="170px" Text='<%# Eval("LocalPhone") %>'></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                    FilterType="Numbers,Custom" TargetControlID="txtEmergencyPhone" ValidChars="+ ()-">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div style="clear: both; padding-left: 10px; margin-bottom: 10px">
                            <h4>
                                <asp:Literal ID="Literal45" runat="server" Text='<%$Resources:ControlsCommon,LocalInfo %>'></asp:Literal>
                            </h4>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%-- Local Address--%>
                                <asp:Literal ID="Literal41" runat="server" Text='<%$Resources:ControlsCommon,LocalAddress %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:TextBox ID="txtLocalAddress" TextMode="MultiLine" runat="server" CssClass="textbox"
                                    MaxLength="200" Width="270px" Height="40" Text='<%# Eval("LocalAddress") %>'
                                    onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div class="trLeft" style="float: left; width: 25%; height: 50px">
                                <%--  Local Phone--%>
                                <asp:Literal ID="Literal42" runat="server" Text='<%$Resources:ControlsCommon,LocalPhone %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:TextBox ID="txtLocalPhone" runat="server" CssClass="textbox" MaxLength="15"
                                    Width="170px" Text='<%# Eval("LocalPhone") %>'></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                    FilterType="Numbers,Custom" TargetControlID="txtLocalPhone" ValidChars="+ ()-">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div style="height: auto">
                            <div style="clear: both; height: 1px;">
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                <%-- Mobile--%>
                                <asp:Literal ID="Literal43" runat="server" Text='<%$Resources:ControlsCommon,LocalMobile %>'></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtLocalMobile" runat="server" CssClass="textbox" MaxLength="15"
                                    Width="170px" Text='<%# Eval("LocalMobile") %>'></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtLocalMobile" runat="server"
                                    FilterType="Numbers,Custom" TargetControlID="txtLocalMobile" ValidChars="+ ()-">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%; padding-top: 90px">
                        <div style="padding-left: 10px; margin-bottom: 10px">
                            <h4>
                                <%-- Other Info--%>
                                <asp:Literal ID="Literal44" runat="server" Text='<%$Resources:ControlsCommon,OtherInfo %>'></asp:Literal>
                            </h4>
                        </div>
                        <div>
                            <div style="clear: both; height: 1px;">
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 48px;">
                            </div>
                            <div class="trRight" style="float: left; width: 70%; height: 48px;">
                                <asp:TextBox ID="txtOtherInfo" runat="server" TextMode="MultiLine" CssClass="textbox"
                                    MaxLength="200" Width="270px" Height="40" Text='<%# Eval("EmergencyContactAddress") %>'
                                    onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                            </div>
                            <span style="clear: both;"></span>
                        </div>
                    </div>
                </div>
                <!--      KPI/KRA      !-->
                <%--<div class="TabbedPanelsContent" id="divTab6" style="display: none;">
                    <asp:UpdatePanel ID="updKpiKra" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div id="divKPIKRA" style="float: left; width: 96%; display: block;" runat="server">
                                <div id="divKPI" style="float: left; width: 47%;" runat="server">
                                    <asp:UpdatePanel ID="upnlKPI" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend>KPI</legend>
                                                <asp:GridView ID="gvKpi" runat="server" AutoGenerateColumns="False" Width="250px"
                                                    RowStyle-HorizontalAlign="Center" BorderColor="#307296" OnRowDataBound="gvKpi_RowDataBound">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <RowStyle HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:Literal ID="ltlKpi" runat="server" Text="KPI"></asp:Literal>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlKpi" runat="server" Width="232px" DataTextField="Kpi" DataValueField="KpiID">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:Literal ID="ltlKpiMarks" runat="server" Text="Marks"></asp:Literal>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtKpiMarks" runat="server" MaxLength="3" Width="60px" Text='<%Eval("KPIValue") %>'></asp:TextBox>
                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeKpiMarks" FilterType="Numbers,Custom"
                                                                    ValidChars="." TargetControlID="txtKpiMarks" runat="server">
                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="updbtnkpi" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="btnAddKpi" runat="server" CausesValidation="false" ImageUrl="~/images/AddSymbol.png"
                                                                            OnClick="btnAddKpi_Click"></asp:ImageButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="btnAddKpi" EventName="Click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="updbtnkpidel" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="imgKpiDel" ImageUrl="~/images/image/Delete_16px.png" runat="server"
                                                                            CommandArgument='<%# Eval("EmployeeKpiID") %>' OnClick="imgKpiDel_Click" CausesValidation="false" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="imgKpiDel" EventName="click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div id="divKRA" style="float: left; width: 47%;" runat="server">
                                    <asp:UpdatePanel ID="upnlKRA" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <fieldset style="margin-left: 36px;">
                                                <legend>KRA</legend>
                                                <asp:GridView ID="gvKra" runat="server" AutoGenerateColumns="False" Width="250px"
                                                    RowStyle-HorizontalAlign="Center" BorderColor="#307296" OnRowDataBound="gvKra_RowDataBound">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <RowStyle HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:Literal ID="ltlKra" runat="server" Text="KRA"></asp:Literal>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlKra" runat="server" Width="232px" DataTextField="Kra" DataValueField="KraID">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:Literal ID="ltlKraMarks" runat="server" Text="Marks"></asp:Literal>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtKraMarks" runat="server" MaxLength="3" Width="60px"></asp:TextBox>
                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeKraMarks" FilterType="Numbers,Custom"
                                                                    ValidChars="." TargetControlID="txtKraMarks" runat="server">
                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="updbtnkra" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="btnAddKra" runat="server" OnClick="btnAddKra_onClick" CausesValidation="false"
                                                                            ImageUrl="~/images/AddSymbol.png"></asp:ImageButton>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="btnAddKra" EventName="click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="updbtnkradel" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:ImageButton ID="imgKraDel" ImageUrl="~/images/image/Delete_16px.png" runat="server"
                                                                            CommandArgument='<%# Eval("EmployeeKraID") %>' OnClick="imgKraDel_Click" CausesValidation="false" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="imgKraDel" EventName="click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>--%>
                <div class="TabbedPanelsContent">
                    <div style="clear: both; height: 1px;">
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,SUbmit %>'
                            CommandName="SUBMIT" Text='<%$Resources:ControlsCommon,SUbmit %>' ValidationGroup="Employee"
                            OnClientClick="return ValidateEmployeePage('Employee',this.id);" OnClick="btnSubmit_Click"
                            Width="75px" />
                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel %>'
                            CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel %>' CommandName="_Cancel"
                            OnClick="btnCancel_Click" Width="75px" />
                    </div>
                </div>
            </div>
            <div style="height: auto">
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnMsg" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
                    </div>
                    <asp:Panel ID="pnlMsg" runat="server" Style="display: none;" BorderWidth="0px" BorderStyle="None">
                        <asp:UpdatePanel ID="updMsg" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMsg" runat="server" ModalPopupId="mpeMsg" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMsg" runat="server" TargetControlID="btnMsg"
                        PopupControlID="pnlMsg" PopupDragHandleControlID="pnlMsg" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
            </div>
            <asp:UpdatePanel ID="updMessage" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                            Drag="true">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="btn" Text="" runat="server" />
                    </div>
                    <div id="pnlModalPopUp" runat="server" style="display: none;">
                        <uc:ReferenceNew ID="ReferenceNew1" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                        PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <asp:UpdatePanel ID="upPrint" runat="server">
        <ContentTemplate>
            <div id="divPrint" runat="server" style="display: none">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<asp:UpdatePanel ID="updMdlWorkPolicy" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit" OnLoad="FormView1_Load">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlmdlWorkPolicy" Style="display: none" runat="server">
                        <uc:WorkPolicy ID="ucWorkPolicy" runat="server" ModalPopupID="mpeWorkPolicy" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeWorkPolicy" runat="server" TargetControlID="Button3"
                        PopupControlID="pnlmdlWorkPolicy" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
    <%--<asp:UpdatePanel ID="updMdlLeavePolicy" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView2" runat="server" DefaultMode="Edit" OnLoad="FormView2_Load">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlmdlLeavePolicy" Style="display: none" runat="server">
                        <uc:LeavePolicy ID="ucLeavePolicy" runat="server" ModalPopupID="mdlPopUpLeave" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpLeave" runat="server" TargetControlID="Button3"
                        PopupControlID="pnlmdlLeavePolicy" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
    <asp:UpdatePanel ID="updLocationReference" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none;">
                <input type="button" id="btnLocation" runat="server" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeLocationReference" runat="server" BackgroundCssClass="modalBackground"
                PopupControlID="pnlLocationReference" TargetControlID="btnLocation">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlLocationReference" runat="server" Style="display: none;">
                <uc:LocationReference ID="ucLocationReference" runat="server" ModalPopupID="mpeLocationReference" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updDesignationReference" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btnPopDesignation" runat="server" />
            </div>
            <div id="divDesignation" runat="server" style="display: none;">
                <uc:DesignationReference ID="DesignationReference" runat="server" ModalPopupID="mdlPopDesignation" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopDesignation" runat="server" TargetControlID="btnPopDesignation"
                PopupControlID="divDesignation" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updMdlSalaryScale" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView3" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlmdlSalaryScale" Style="display: none" runat="server">
                        <uc:SalaryScale ID="ucSalaryScale" runat="server" ModalPopupID="mdlPopUpSalaryScale">
                        </uc:SalaryScale>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpSalaryScale" runat="server" TargetControlID="Button3"
                        PopupControlID="pnlmdlSalaryScale" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <%--<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
        <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
            <tr>
                <td style="width: 65%">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                </td>
                <td style="width: 35%; text-align: left">
                    <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                        ImageUrl="~/images/search.png" ToolTip="Click here to search" ImageAlign="AbsMiddle" />
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                        WatermarkText="Search By" WatermarkCssClass="WmCss">
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </td>
            </tr>
        </table>
    </asp:Panel>--%>
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddEmp" ImageUrl="~/images/New_Employee_Big.PNG" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnNew" runat="server" OnClick="btnNew_Click">
                            <%--  Add Employee--%>
                            <asp:Literal ID="Literal59" runat="server" Text='<%$Resources:ControlsCommon,AddEmployee %>'>
                            
                            </asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListEmp" ImageUrl="~/images/listemployees.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click">
                            <%-- View Employee--%>
                            <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:ControlsCommon,ViewEmployee %>'>
                            
                            </asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnDelete" Text='<%$Resources:ControlsCommon,Delete %>' runat="server"
                            CausesValidation="false"> 
                          <%-- Delete--%>
                           
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnPrint" Text='<%$Resources:ControlsCommon,Print %>' runat="server"
                            CausesValidation="false">
                         <%--Print--%>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnEmail" Text='<%$Resources:ControlsCommon,Email %>' runat="server"
                            CausesValidation="false">
              
             <%--   Email--%>  
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
    function pageLoad(sender ,args)
    {
    
 

$(function() {
    	        $('#ctl00_public_content_ddlLanguage').multiselect({
    	            includeSelectAllOption: true })
    	       });

    $(document).ready(function() {
                                    $('#ctl00_public_content_ddlLanguage').multiselect();
                                 });
                                 
              
                                 
      
			  }
          	    
    </script>

</asp:Content>
