﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Bank.aspx.cs" Inherits="Public_Bank" meta:resourcekey="PageTitle"%>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li class="selected"><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnSubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="upnlMessage" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upFormView" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divForm" runat="server">
                            <asp:FormView ID="fvBankDetails" runat="server" Width="100%" DefaultMode="Edit" OnItemCommand="fvBankDetails_ItemCommand"
                                DataKeyNames="BankID,BankBranchID,CountryID" OnDataBound="fvBankDetails_DataBound"
                                CellSpacing="0">
                                <EditItemTemplate>
                                    <div>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                  <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="BankName" ></asp:Literal>
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="upnlBank" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="rcBankName" runat="server" CssClass="dropdownlist_mandatory">
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnNewBank" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                    OnClick="btnNewBank_Click" Text="..." />
                                                                <asp:RequiredFieldValidator ID="rfBank" runat="server" InitialValue="-1" ControlToValidate="rcBankName"
                                                                    CssClass="error" ValidationGroup="submit" ErrorMessage="Please select a bank"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="BranchName" ></asp:Literal>
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtBranchName" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                                        Width="200px" Text='<%# Eval("BranchName") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvtxtBranchName" runat="server" ControlToValidate="txtBranchName"
                                                        Display="Dynamic" ErrorMessage="Please enter Branch Name." ValidationGroup="submit"
                                                        CssClass="error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Bank Code
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtBankCode" runat="server" Text='<%# Eval("BankCode") %>' CssClass="textbox"
                                                        MaxLength="15" Width="150px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Address
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="textbox" Height="50px" TextMode="MultiLine"
                                                        MaxLength="100" Text='<%# Eval("Address") %>' Width="170px" onchange="RestrictMulilineLength(this, 100);" onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Telephone
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtTelephone" runat="server" CssClass="textbox" MaxLength="14" Text='<%# Eval("Telephone") %>'
                                                        Width="200px"></asp:TextBox>
                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="txtTelephoneExtender" runat="server"
                                                        FilterType="Numbers,Custom" ValidChars="+ ()-" TargetControlID="txtTelephone">
                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Fax
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtFax" runat="server" CssClass="textbox" Text='<%# Eval("Fax") %>'
                                                        MaxLength="25" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Country
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:UpdatePanel ID="upnlCountry" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="rcCountry" runat="server" CssClass="dropdownlist">
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnNewCountry" runat="server" CausesValidation="False" CommandName="Accounthead"  Text="..."
                                                                    CssClass="referencebutton" OnClick="btnNewCountry_Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Email
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="40" Text='<%# Eval("Email") %>'
                                                        CssClass="textbox" Width="200px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationGroup="submit"
                                                        ControlToValidate="txtEmail" CssClass="error" Display="Dynamic" ErrorMessage="Invalid Email"
                                                        SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    Website
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtWebsite" runat="server" MaxLength="40" Text='<%# Eval("Website") %>'
                                                        CssClass="textbox" Width="200px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="revtxtWebsite" runat="server" ValidationGroup="submit"
                                                        ControlToValidate="txtWebsite" Display="Dynamic" ErrorMessage="Invalid website address"
                                                        SetFocusOnError="True" CssClass="error" ValidationExpression="(((http)://)|(www\.))+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,6})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9\&amp;amp;%_\./-~-]*)?"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft" width="25%">
                                                    IBAN Code
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:TextBox ID="txtBankRoutingCode" runat="server" CssClass="textbox" Text='<%# Eval("UAEBankCode") %>'
                                                        Width="150px" MaxLength="23"></asp:TextBox>
                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="txtBankRoutingCodeExtender" runat="server"
                                                        FilterType="Numbers" TargetControlID="txtBankRoutingCode">
                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr align="right">
                                                <td class="trLeft" width="25%">
                                                    &nbsp;
                                                </td>
                                                <td class="trRight" width="75%">
                                                    <asp:Button ID="btnSubmit" runat="server" ValidationGroup="submit" CssClass="btnsubmit"
                                                        Text="Submit" CommandName="Add"  Width="75px" />
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandName="Resume"
                                                        Text="Cancel" CausesValidation="False"  Width="75px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <div id="mainwrap">
                                        <div id="main2" style="margin-top: 10px; margin-left: 10px">
                                            <div class="t22">
                                                <div class="ta22">
                                                    <div class="maindivheader">
                                                        <div class="innerdivheader">
                                                          <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="BankName" ></asp:Literal></div>
                                                        <div class="innerdivheader">
                                                             <asp:Literal ID="Literal3" runat ="server" meta:resourcekey="BranchName" ></asp:Literal></div>
                                                        <div class="innerdivheader">
                                                             <asp:Literal ID="Literal4" runat ="server" meta:resourcekey="BankCode" ></asp:Literal></div>
                                                        <div class="innerdivheader"  style="height:50px">
                                                            <asp:Literal ID="Literal5" runat ="server" meta:resourcekey="Address" ></asp:Literal>
                                                        </div>
                                                        <div class="innerdivheader">
                                                             <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="Telephone" ></asp:Literal></div>
                                                        <div class="innerdivheader">
                                                           <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Fax" ></asp:Literal>  </div>
                                                        <div class="innerdivheader">
                                                           <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="Country" ></asp:Literal>  
                                                        </div>
                                                        <div class="innerdivheader" runat="server">
                                                             <asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Email" ></asp:Literal> 
                                                        </div>
                                                        <div class="innerdivheader">
                                                          <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="Website" ></asp:Literal>   
                                                        </div>
                                                        <div class="innerdivheader">
                                                            <asp:Literal ID="Literal11" runat ="server" meta:resourcekey="IBANCode" ></asp:Literal> 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ta3">
                                                    <div class="maindivheaderValue">
                                                        <div class="innerdivheaderValue">
                                                            <%#Eval("BankName")%></div>
                                                        <div class="innerdivheaderValue">
                                                            <%# Eval("BranchName")%></div>
                                                        <div class="innerdivheaderValue">
                                                            <%# Eval("BankCode")%></div>
                                                        <div class="innerdivheaderValue" style="height:50px;word-break:break-all">
                                                            <%# Eval("Address")%>
                                                        </div>
                                                        <div class="innerdivheaderValue">
                                                            <%#Eval("Telephone")%></div>
                                                        <div class="innerdivheaderValue">
                                                            <%#Eval("Fax")%></div>
                                                        <div class="innerdivheaderValue">
                                                            <%#Eval("Country")%>
                                                        </div>
                                                        <div class="innerdivheaderValue" runat="server">
                                                            <%#Eval("Email")%>
                                                        </div>
                                                        <div class="innerdivheaderValue">
                                                            <%# Eval("Website")%>
                                                        </div>
                                                        <div class="innerdivheaderValue">
                                                            <%#Eval("UAEBankCode")%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div style="float:right;display:none">
                                                 <asp:ImageButton ID="imgFVEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/edit.png" CommandArgument='<%# Eval("BankBranchId") %>' CommandName="Modify" CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlBankDetails" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upDataList" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divList" runat="server">
                            <asp:DataList ID="dlBankDetails" runat="server" Width="100%" DataKeyField="BankBranchID"
                                OnItemCommand="dlBankDetails_ItemCommand" OnItemDataBound="dlBankDetails_ItemDataBound">
                                <ItemStyle CssClass="labeltext" />
                                <HeaderStyle CssClass="listItem" />
                                <HeaderTemplate>
                                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                        <tr style="display: none">
                                            <td align="left" width="25" valign="top" style="padding-left: 8px; padding-top: 5px">
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id,'chkBank')" />
                                            </td>
                                            <td style="padding-left: 7px" class="trLeft">
                                                Select All
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table cellpadding="3" cellspacing="0" border="0" width="100%" id="tblBankDetails"
                                        runat="server" onmouseover="showEditButton(this, 'lnkBranchName');" onmouseout="hideEditButton(this, 'lnkBranchName');">
                                        <tr >
                              
                                            <td width="25px" valign="top" style="padding-left: 8px; padding-top: 5px;display: none">
                                                <asp:CheckBox ID="chkBank" runat="server" />
                                            </td>
                                            <td style="padding-left: 7px">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="300">
                                                            <asp:LinkButton ID="lnkBankName" runat="server" CssClass="listHeader bold" CommandName="View"
                                                                CommandArgument='<%#Eval("BankBranchID")%>' ToolTip="View details" CausesValidation="false">
                                                                     <%#Eval("BankName")%></asp:LinkButton>
                                                        </td>
                                                        <td align="right" style="display:none">
                                                        
                                                         <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit" CommandArgument='<%# Eval("BankBranchID") %>' CommandName="Modify" CausesValidation="False" />
                                                         
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="padding-left: 25px">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                                    <tr>
                                                        <td width="120px" class="innerdivheader">
                                                           <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="BranchName" ></asp:Literal>
                                                        </td>
                                                        <td width="2%">
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("BranchName")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="BankPager" runat="server" Visible="false" />
                            <asp:Label ID="lblBank" runat="server" Style="display: none;" CssClass="error"></asp:Label>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvBankDetails" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: none;"></asp:Label>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlBankDetails" EventName="ItemCommand" />
            <asp:AsyncPostBackTrigger ControlID="fvBankDetails" EventName="ItemCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPrint">
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
        <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
            <tr>
                <td style="width: 65%">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                </td>
                <td style="width: 35%; text-align: left; padding-left: 8px">
                    <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                        ImageUrl="~/images/search.png" ToolTip="Click here to search" ImageAlign="Middle"
                        OnClick="btnSearch_Click" />
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                        WatermarkCssClass="WmCss"   WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>'>
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
              <%--  <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddBank" ImageUrl="~/images/Add_bankBig.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click"> <h5>
                            Add Bank</h5></asp:LinkButton>
                </div>--%>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListBank" ImageUrl="~/images/Bank_listBig.PNG" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click"  meta:resourcekey="ListBanks">
                           </asp:LinkButton>
                </div>
             <%--   <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click"> <h5>
                           Delete</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="~/images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">
                          <h5> Print</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click">
                         <h5>
                            Email</h5>  </asp:LinkButton>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
