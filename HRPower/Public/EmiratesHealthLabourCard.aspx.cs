﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.Reflection;
using System.IO;
using System.Text;

public partial class Public_EmiratesHealthLabourCard : System.Web.UI.Page
{
    #region Declarations
    //------------permission-------------------//
    private bool MblnPrintEmailPermission = false;        //To set Print Email Permission
    private bool MblnAddPermission = false;           //To set Add Permission
    private bool MblnUpdatePermission = true;      //To set Update Permission For Edit button In DataList 
    private bool MblnDeletePermission = false;      //To set Delete Permission
    private bool MblnAddUpdatePermission = false;   //To set Add Update Permission
    private string sCommonMessage = String.Empty;

    public int GlCompanyId =new clsUserMaster().GetCompanyId();
    public int MiEmployeeID = 0;
    public int MiHealthCardID = 0;
    private Boolean MiOKClose = false;
    private string MsDocName = String.Empty;
    // ClsLogWriter MobjLog=new ClsLogWriter();

    //Receipt Or Issue Permissions
    public bool mblnAddIssue = false;
    public bool mblnAddReceipt = false;
    public bool mblnEmail = false;
    public bool mblnDelete = false;
    public bool mblnUpdate = false;

    clsUserMaster objUser;
    clsBindComboBox objCommon;
    clsEmiratesHealthLabourCard objCard;
    clsDocumentAlert objAlert;
    #endregion Declarations

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterAutoComplete();
        pgrCards.Fill += new controls_Pager.FillPager(BindDatalist);
        ViewState["TypeID"] = Request.QueryString["typeid"];

        if (ViewState["TypeID"].ToInt32() == 1)
        {
            Page.Title = GetGlobalResourceObject("MasterPageCommon", "HealthCard").ToString();
           liHealth.Attributes["class"] = "selected";
        }
        else if (ViewState["TypeID"].ToInt32() == 2)
        {
            Page.Title = GetGlobalResourceObject("MasterPageCommon", "LabourCard").ToString();
           liLabour.Attributes["class"] = "selected";
        }
        else
        {
            Page.Title = GetGlobalResourceObject("MasterPageCommon", "NationalIDCard").ToString();
           liEmirates.Attributes["class"] = "selected";
        }

        if (!IsPostBack)
        {            
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            SetPermission();
            EnableMenus();
        }

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
    }

    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>" + Result + "</h5>";
        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        lblCurrentStatus.Text = "Issued";
        //    }

        //    upnlCreateCard.Update();
        //    upMenu.Update();
        //}
    }
    
    #region BindDatalist
    public void BindDatalist()
    {
        hfCardID.Value = "0";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        divAddNew.Style["display"] = "none";
        divDataList.Style["display"] = "block";
        divViewCardDetails.Style["display"] = "none";

        int iFormType = Request.QueryString["typeid"].ToInt32();
        objCard = new clsEmiratesHealthLabourCard();
        pgrCards.Visible = true;
        objUser = new clsUserMaster();
       int UserId = objUser.GetUserId();
       int CompanyId = objUser.GetCompanyId();
       // pgrCards.CurrentPage = 0;
        pgrCards.Total = objCard.GetCount(txtSearch.Text.Trim(), iFormType,CompanyId);
        DataTable dtCards = objCard.GetCards(pgrCards.PageSize, pgrCards.CurrentPage + 1, txtSearch.Text, CompanyId, iFormType);

        if(dtCards.Rows.Count>0)
        {
            pgrCards.Total = objCard.GetCount(txtSearch.Text.Trim(), iFormType,CompanyId);  
            dlCards.DataSource = dtCards;
            dlCards.DataBind();
            lblNoData.Visible = false;
            pgrCards.Visible = true;
        }
        else
        {
            dlCards.DataSource = null;
            dlCards.DataBind();
            pgrCards.Visible = false;
            lblNoData.Visible = true;

            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoDocuments").ToString();
            else
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();
        }
        
        upnlNoData.Update();
        upnlCreateCard.Update();
        txtSearch.Text = "";
    }
    #endregion BindDatalist

    #region RegisterAutoComplete
    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        if (Request.QueryString["typeid"].ToInt32() == 1)
        {
            clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.HealthCard,objUser.GetCompanyId());
        }
        else if (Request.QueryString["typeid"].ToInt32() == 2)
        {
            clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.LabourCard, objUser.GetCompanyId());
        }
        else
        {
            clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.EmiritesCard, objUser.GetCompanyId());
        }
    }
    #endregion RegisterAutoComplete

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
        {            
            if (Request.QueryString["typeid"].ToInt32() == 1)//Health card
                dt = objRoleSettings.GetPermissions(RoleId, (int)eMenuID.HealthCard);
            else if (Request.QueryString["typeid"].ToInt32() == 2)//Labour card
                dt = objRoleSettings.GetPermissions(RoleId, (int)eMenuID.LabourCard);
            else //Emirates card
                dt = objRoleSettings.GetPermissions(RoleId, (int)eMenuID.NationalIDCard);
        }
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dt = (DataTable)ViewState["Permission"];

        if (dt.Rows.Count > 0)
        {
            lnkRenew.Enabled = lnkAddCards.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListCards.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        clsUserMaster objUser = new clsUserMaster();
        int UserID;
        UserID = objUser.GetRoleId();

        if (UserID > 3)
        {
            if (lnkListCards.Enabled)
            {
                BindDatalist();
                btnSearch.Enabled = true;
            }
            else if (lnkAddCards.Enabled)
                AddNew();
            else
            {
                pgrCards.Visible = false;
                dlCards.DataSource = null;
                dlCards.DataBind();
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Visible = true;
                btnSearch.Enabled = false;
                divAddNew.Style["display"] = "none";
            }
        }
        else
            BindDatalist();

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlCards.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlCards.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlCards.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlCards.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }

    #region ClearFields
    public void ClearFields()
    {
        EnableMenus();
        ViewState["blAddStatus"] = true;
        txtCardNumber.Text = string.Empty;
        ddlEmployee.SelectedIndex = -1;
        hfCardID.Value = "0";
        txtPersonalNumber.Text = string.Empty;
        txtOtherInfo.Text = string.Empty;
        txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
        txtIssuedate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = lnkRenew.OnClientClick = "return false;";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        lblCurrentStatus.Text = GetGlobalResourceObject("DocumentsCommon", "New").ToString();
        ViewState["OtherDoc"] = null;
        dlOtherDoc.DataSource = null;
        dlOtherDoc.DataBind();
    }
    #endregion ClearFields

    #region FillFields
    public void FillFields(int iCardID)
    {
        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";
        int intDocType, iOperationType = (int)OperationType.Employee;
        if (ViewState["TypeID"].ToInt32() == 1)
            intDocType = (int)DocumentType.Health_Card;
        else if (ViewState["TypeID"].ToInt32() == 2)
            intDocType = (int)DocumentType.Labour_Card;
        else
            intDocType = (int)DocumentType.National_ID_Card;
        pgrCards.Visible = false;
        objCard = new clsEmiratesHealthLabourCard();
        divDataList.Style["display"] = "none";
        divViewCardDetails.Style["display"] = "none";
        divAddNew.Style["display"] = "block";
        DataTable dtCardDetails = objCard.GetCardDetails(iCardID, ViewState["TypeID"].ToInt32(), intDocType);
        if (dtCardDetails.Rows.Count > 0)
        {
            ViewState["blAddStatus"] = false;
            LoadCombos();
            ddlEmployee.SelectedValue = Convert.ToString(dtCardDetails.Rows[0]["EmployeeID"]);
            txtCardNumber.Text = Convert.ToString(dtCardDetails.Rows[0]["CardNumber"]);
            hfCardID.Value = Convert.ToString(dtCardDetails.Rows[0]["CardID"]);
            txtPersonalNumber.Text = Convert.ToString(dtCardDetails.Rows[0]["CardPersonalNumber"]);
            txtOtherInfo.Text = Convert.ToString(dtCardDetails.Rows[0]["Remarks"]);
            txtExpiryDate.Text = dtCardDetails.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd/MM/yyyy");
            txtIssuedate.Text = dtCardDetails.Rows[0]["IssueDate"].ToDateTime().ToString("dd/MM/yyyy");
            lblCurrentStatus.Text = Convert.ToString(dtCardDetails.Rows[0]["Status"]);
        }
        
        if (clsDocumentMaster.IsReceipted(iOperationType, intDocType, hfCardID.Value.ToInt32()))
        {
            imgDocIR.ImageUrl = "~/images/document_issue.png";
           // lnkDocumentIR.Text = "<h5>Issue</h5>";
        }
        else
        {
            imgDocIR.ImageUrl = "~/images/document_reciept.png";
          //  lnkDocumentIR.Text = "<h5>Receipt</h5>";
        }
        DataTable datDocument = new DataTable();
        datDocument = objCard.GetAttachments(hfCardID.Value.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), intDocType);

        dlOtherDoc.DataSource = datDocument;
        dlOtherDoc.DataBind();

        ViewState["OtherDoc"] = datDocument;

        if (dlOtherDoc.Items.Count > 0)
            divOtherDoc.Style["display"] = "block";
        else
            divOtherDoc.Style["display"] = "none";
        upMenu.Update();
        upnlCreateCard.Update();

        if (Convert.ToInt32(dtCardDetails.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";

        if (Convert.ToBoolean(dtCardDetails.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            divRenew.Style["display"] = "none";
            if (clsGlobalization.IsArabicCulture())
                lblCurrentStatus.Text += " ] " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
            else
                lblCurrentStatus.Text += " [ " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
           
            ControlEnableDisble(false);
        }
        else
        {
            divRenew.Style["display"] = "block";
            ControlEnableDisble(true);
        }

        upMenu.Update();
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        ddlEmployee.Enabled = blnTemp;
        txtCardNumber.Enabled = txtPersonalNumber.Enabled = txtOtherInfo.Enabled = blnTemp;
        txtExpiryDate.Enabled = txtIssuedate.Enabled = blnTemp;
        ibtnIssueDate.Enabled = ibtnExpiryDate.Enabled = blnTemp;
    }

    #endregion FillFields

    #region ViewCardDetails
    public void ViewCardDetails(int iCardID)
    {
        
        pgrCards.Visible = false;
        objCard = new clsEmiratesHealthLabourCard();
        divDataList.Style["display"] = "none";
        divAddNew.Style["display"] = "none";
        divViewCardDetails.Style["display"] = "block";
        dlCards.DataSource = null;
        dlCards.DataBind();

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return true;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return true;";

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');";
        if (lnkRenew.Enabled)
        lnkRenew.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretorenewtheselecteditems").ToString() + "');";

        upMenu.Update();
        DataTable dtCardDetails = objCard.GetCardDetails(iCardID, ViewState["TypeID"].ToInt32(),0);

        if (dtCardDetails.Rows.Count > 0)
        {
            if (ViewState["TypeID"].ToInt32() == 1)
                divHeading.InnerHtml = GetLocalResourceObject("EmployeeHealthCard.Text").ToString();
            else if (ViewState["TypeID"].ToInt32() == 2)
                divHeading.InnerHtml = GetLocalResourceObject("EmployeeLabourCard.Text").ToString();
            else
                divHeading.InnerHtml = GetLocalResourceObject("EmployeeNationalIDCard.Text").ToString();
            divEmployee.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["Employee"]);
            divCardNumber.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["CardNumber"]);
            hfCardID.Value = Convert.ToString(dtCardDetails.Rows[0]["CardID"]);
            divPersonalNumber.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["CardPersonalNumber"]);
            divRemarks.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["Remarks"]);
            divExpiryDate.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["ExpiryDate"]);
            divIssueDate.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["IssueDate"]);
            divIsDelete.InnerHtml = Convert.ToString(dtCardDetails.Rows[0]["IsDelete"]);

            string renew = dtCardDetails.Rows[0]["IsRenewed"].ToString();
            if (renew.ToBoolean())
            {
                renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
            }
            else
            {
                renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
            }
            divRenew1.InnerText = renew;
        }
        upnlCreateCard.Update();
    }
    #endregion ViewCardDetails

    #region AddNew
    private void AddNew()
    {
        ControlEnableDisble(true);
        txtSearch.Text = string.Empty;
        lblNoData.Visible = false;
        upnlNoData.Update();
        pgrCards.Visible = false;
        upMenu.Update();

        ViewState["blAddStatus"] = true;
        LoadCombos();
        ClearFields();

        divDataList.Style["display"] = "none";
        divAddNew.Style["display"] = "block";
        divViewCardDetails.Style["display"] = "none";

        ViewState["blChangeFalg"] = false;
        // SetEnableDisable();
        if (MiEmployeeID > 0)
        {
            ddlEmployee.SelectedValue = MiEmployeeID.ToString();
            ddlEmployee.Enabled = false;
            txtCardNumber.Focus();
        }
        else
        {
            ddlEmployee.Enabled = true;
            ddlEmployee.Focus();
        }

        ViewState["blChangeFalg"] = false;
        upnlCreateCard.Update();
        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";
        pgrCards.Visible = false;
        lblNoData.Visible = false;
    }
    #endregion AddNew

    #region LoadCombos
    private void LoadCombos()
    {
        objUser = new clsUserMaster();
        objCard = new clsEmiratesHealthLabourCard();
        objCard.CompanyID = objUser.GetCompanyId();
        DataTable dtCombos = objCard.GetEmployees(MiEmployeeID.ToInt32(), ViewState["blAddStatus"].ToBoolean());
        ddlEmployee.DataSource = dtCombos;
        ddlEmployee.DataBind();
    }
    #endregion LoadCombos

    #region SaveCardDetails
    public bool SaveCardDetails()
    {
        objCard = new clsEmiratesHealthLabourCard();
        objAlert = new clsDocumentAlert();
        int intDocType, iOperationType = (int)OperationType.Employee;
        if (ViewState["TypeID"].ToInt32() == 1)
            intDocType = (int)DocumentType.Health_Card;
        else if (ViewState["TypeID"].ToInt32() == 2)
            intDocType = (int)DocumentType.Labour_Card;
        else
            intDocType = (int)DocumentType.National_ID_Card;
        
        if (objCard.CheckDuplication(hfCardID.Value.ToInt32(), txtCardNumber.Text.Trim(),ViewState["TypeID"].ToInt32()))
        {
            mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "CardNumberExist").ToString());
            mpeMessage.Show();
            return false ;
        }
        else
        {
            int iCardID = 0;
            objCard.CardNumber = txtCardNumber.Text.Trim();
            objCard.CardPersonalIDNumber = txtPersonalNumber.Text.Trim();
            objCard.IssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text.Trim());
            objCard.ExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text.Trim());
            objCard.Remarks = txtOtherInfo.Text.Trim();
            objCard.EmployeeID = ddlEmployee.SelectedValue.ToInt32();

            if (hfCardID.Value.ToInt32() > 0)
                objCard.CardID = hfCardID.Value.ToInt32();
            iCardID = objCard.SaveCardDetails(ViewState["blAddStatus"].ToBoolean(),ViewState["TypeID"].ToInt32());
            if (iCardID > 0)
            {
                
                Label lblDocname, lblActualfilename, lblFilename;
                string strDoctypeName = clsDocumentMaster.GetDocumentType(intDocType);
                clsDocumentMaster.DeleteTreeMaster(iCardID, intDocType);

                if (hfCardID.Value.ToInt32() <= 0)
                {
                    objAlert.AlertMessage(intDocType,strDoctypeName , strDoctypeName + " Number", iCardID, txtCardNumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Emp", ddlEmployee.SelectedValue.ToInt32(), "", false);
                }

                foreach (DataListItem item in dlOtherDoc.Items)
                {

                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    lblActualfilename = (Label)item.FindControl("lblActualfilename"); 

                    if (dlOtherDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objCard.SaveTreeMaster(iCardID, intDocType, ddlEmployee.SelectedValue.ToInt32(), ddlEmployee.SelectedItem.Text.Trim(), lblDocname.Text.Trim(), lblFilename.Text.Trim());

                        string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";


                        if (File.Exists(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(strPath))
                                Directory.CreateDirectory(strPath);

                            File.Move(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString()), strPath + "/" + lblFilename.Text.Trim().ToString());
                        }

                    }
                    else
                    {
                        objCard.SaveTreeMaster(iCardID, intDocType, ddlEmployee.SelectedValue.ToInt32(), ddlEmployee.SelectedItem.Text.Trim(), lblDocname.Text.Trim(), lblFilename.Text.Trim());

                        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                        if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(Path))
                                Directory.CreateDirectory(Path);

                            File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                        }
                    }
                }
            }

            if (ViewState["blAddStatus"].ToBoolean())
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString());
            else
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString());

            mpeMessage.Show();
            FillFields(iCardID);

        }
        return true;
    }
    #endregion SaveCardDetails

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfCardID.Value.ToInt32() > 0)
            {
                int id = 0;
                int intDocType = 0;
                objCard = new clsEmiratesHealthLabourCard();
                objAlert = new clsDocumentAlert();
                objCard.CardID = hfCardID.Value.ToInt32();
                try
                {
                    objCard.BeginEmp(ViewState["TypeID"].ToInt32());
                    id = objCard.UpdateRenewEmployeeEmiratesCard(ViewState["TypeID"].ToInt32());
                    objCard.Commit();
                }
                catch (Exception ex)
                {
                    objCard.RollBack();
                }

                if (ViewState["TypeID"].ToInt32() == 1)
                    intDocType = (int)DocumentType.Health_Card;
                else if (ViewState["TypeID"].ToInt32() == 2)
                    intDocType = (int)DocumentType.Labour_Card;
                else
                    intDocType = (int)DocumentType.National_ID_Card;

                objAlert.DeleteExpiryAlertMessage(intDocType, id);

                string strDoctypeName = clsDocumentMaster.GetDocumentType(intDocType);
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "RenewedSuccessfully").ToString());
                mpeMessage.Show();
                hfCardID.Value = objCard.CardID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                lnkListCards_Click(sender, e);
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    #region SetEnableDisable()
    private void SetEnableDisable()
    {
        btnSubmit.Enabled =btnCancel.Enabled=  ViewState["blChangeFalg"].ToBoolean();
        //BtnOk.Enabled =  ViewState["blChangeFalg"].ToBoolean();
        //BindingNavigatorSaveItem.Enabled =  ViewState["blChangeFalg"].ToBoolean();
        //BtnPrint.Enabled = BtnMail.Enabled = BindingNavigatorAddNewItem.Enabled = BindingNavigatorDeleteItem.Enabled = ToolStripDropDownBtnMore.Enabled = (!blAddStatus);
    }
    #endregion SetEnableDisable()

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (SaveCardDetails())
        lnkListCards_Click(sender, e);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearFields();
    }

    protected void txtIssueDate_TextChanged(object sender, EventArgs e)
    {
    }

    protected void lnkAddCards_Click(object sender, EventArgs e)
    {
        AddNew();
    }
    protected void lnkListCards_Click(object sender, EventArgs e)
    {
        pgrCards.CurrentPage = 0;
        EnableMenus();
    }

    protected void dlCards_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "EDIT": FillFields(e.CommandArgument.ToInt32());
                
                break;
            case "VIEW": ViewCardDetails(e.CommandArgument.ToInt32());
                ViewState["CardID"] = e.CommandArgument;
                break;
        }
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        CheckBox chkItem;
        LinkButton lnkCard;
        Label lblIsDelete;
        objCard = new clsEmiratesHealthLabourCard();

        string strMessage = string.Empty;

        if (dlCards.Items.Count > 0)
        {
            foreach (DataListItem item in dlCards.Items)
            {
                chkItem = (CheckBox)item.FindControl("chkItem");
                lnkCard = (LinkButton)item.FindControl("lnkCard");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkItem == null)
                    continue;

                if (chkItem.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    objCard.DeleteCard(dlCards.DataKeys[item.ItemIndex].ToInt32(), ViewState["TypeID"].ToInt32());
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString(); 
                }
                else
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
            }            
        }
        else
        {
            if (Convert.ToBoolean(divIsDelete.InnerText.ToInt32()))
            {
                objCard.DeleteCard(ViewState["CardID"].ToInt32(), ViewState["TypeID"].ToInt32());
                strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
            }
            else
                strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
        }

        if (strMessage == "")
            strMessage = GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString();
        
        mcMessage.InformationalMessage(strMessage);
        mpeMessage.Show();
        EnableMenus();
    }
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        upMenu.Update();
        objCard = new clsEmiratesHealthLabourCard();
        if (hfCardID.Value.ToInt32() == 0)
        {
            CheckBox chkItem;

            if (dlCards.Items.Count > 0)
            {
                string sDocumentIDs = string.Empty;

                foreach (DataListItem item in dlCards.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    sDocumentIDs += "," + dlCards.DataKeys[item.ItemIndex];
                }

                if (sDocumentIDs != string.Empty)
                    sDocumentIDs = sDocumentIDs.Remove(0, 1);

                if (sDocumentIDs.Contains(","))
                    divPrint.InnerHtml = PrintDetails(sDocumentIDs.ToString(), ViewState["TypeID"].ToInt32(), false);
                else
                {
                    divPrint.InnerHtml = PrintDetails(sDocumentIDs.ToString(), ViewState["TypeID"].ToInt32(), true);
                }
            }

        }
        else
        {

            divPrint.InnerHtml = PrintDetails(hfCardID.Value.ToString(), ViewState["TypeID"].ToInt32(), true);
        }

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
    }

    private string PrintDetails(string sDocIds, int iCardType, bool isSingle)
    {
        DataTable datPrint = objCard.PrintSelected(sDocIds, ViewState["TypeID"].ToInt32());
        StringBuilder sb = new StringBuilder();
        string renew = "";
        if (datPrint != null && datPrint.Rows.Count > 0)
        {
            if (isSingle)
            {
                //renew = datPrint.Rows[0]["IsRenewed"].ToString();
                //if (renew.ToBoolean())
                //{
                //    renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                //}
                //else
                //{
                //    renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                //} 
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;word-break:break-all'>" + Convert.ToString(datPrint.Rows[0]["CardNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["EmployeeFullName"]) + "</td></tr>");
                sb.Append("<tr><td width='150px' style='word-break:break-all'>" + GetLocalResourceObject("PersonalNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["CardPersonalNumber"]) + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["IssueDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + "</td><td>:</td><td>" + datPrint.Rows[0]["IsRenewed"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px' style='word-break:break-all'>" + GetGlobalResourceObject("DocumentsCommon", "otherInfo").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'>" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'> " + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + " </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Cardnumber").ToString() + " </td><td width='150px'>" + GetLocalResourceObject("PersonalNumber.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Expirydate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='7'><hr></td></tr>");
                for (int i = 0; i < datPrint.Rows.Count; i++)
                {
                   
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["EmployeeFullName"]) + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardNumber"]) + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardPersonalNumber"]) + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IssueDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IsRenewed"].ToString() + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }


    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        upMenu.Update();

        if (dlCards.Items.Count > 0)
        {
            string sDocumentIDs = string.Empty;

            if (hfCardID.Value.ToInt32() == 0)
            {
                CheckBox chkItem;

                foreach (DataListItem item in dlCards.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    sDocumentIDs += "," + dlCards.DataKeys[item.ItemIndex];
                }
                if (sDocumentIDs != string.Empty)
                    sDocumentIDs = sDocumentIDs.Remove(0, 1);
            }
            else
            {
                sDocumentIDs = hfCardID.Value.ToString();
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=EmployeeCards&CardID=" + sDocumentIDs + "&typeid=" + ViewState["TypeID"].ToInt32() + "', 835, 585);", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=EmployeeCards&CardID=" + Convert.ToString(ViewState["CardID"]) + "&typeid=" + ViewState["TypeID"].ToInt32() + "', 835, 585);", true);
        }
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        pgrCards.CurrentPage = 0;
        this.RegisterAutoComplete();
        EnableMenus();
    }
    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (!ViewState["blAddStatus"].ToBoolean())
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            
            int intOperationTypeID = (int)OperationType.Employee,intDocType;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hfCardID.Value.ToInt32();
            if(ViewState["TypeID"].ToInt32()==1)
                intDocType = (int)DocumentType.Health_Card;
            else if (ViewState["TypeID"].ToInt32() == 2)
                intDocType = (int)DocumentType.Labour_Card;
            else
                intDocType= (int)DocumentType.National_ID_Card;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtCardNumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            lnkListCards_Click(sender, e);
        }
        else
            return;
    }

    protected void fuDocument_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string strFilename = string.Empty;
        string strActualFilename = string.Empty;
        if (fuDocument.HasFile)
        {
            strActualFilename = fuDocument.FileName;
            strFilename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuDocument.FileName);
            Session["Filename"] = strFilename;
            fuDocument.SaveAs(Server.MapPath("~/Documents/temp/") + strFilename);
        }
    }

    protected void btnattachpassdoc_Click(object sender, EventArgs e)
    {
        divOtherDoc.Style["display"] = "block";
        objCard = new clsEmiratesHealthLabourCard();
        DataTable dtDocs;
        DataRow drRow;

        if (ViewState["OtherDoc"] == null)
            dtDocs = objCard.GetAttachments(-1, -1,0);
        else
        {
            DataTable dtTemp = (DataTable)this.ViewState["OtherDoc"];
            dtDocs = dtTemp;
        }


        drRow = dtDocs.NewRow();

        drRow["Docname"] = Convert.ToString(txtDocname.Text.Trim());
        drRow["Filename"] = Session["Filename"];
        drRow["Actualfilename"] = Session["Filename"];
        dtDocs.Rows.Add(drRow);


        dlOtherDoc.DataSource = dtDocs;
        dlOtherDoc.DataBind();
        ViewState["OtherDoc"] = dtDocs;
        txtDocname.Text = string.Empty;

        updOtherDoc.Update();
    }

    protected void dlOtherDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        int intDocType, iOperationType = (int)OperationType.Employee;
        if (ViewState["TypeID"].ToInt32() == 1)
            intDocType = (int)DocumentType.Health_Card;
        else if (ViewState["TypeID"].ToInt32() == 2)
            intDocType = (int)DocumentType.Labour_Card;
        else
            intDocType = (int)DocumentType.National_ID_Card;
        objCard = new clsEmiratesHealthLabourCard();

        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":

                Label lblDocname, lblFilename;

                if (e.CommandArgument != string.Empty)
                {

                    lblFilename = (Label)e.Item.FindControl("lblFilename");

                    string strFilename = lblFilename.Text.Trim();
                    string strFolder = string.Empty;


                    clsDocumentMaster.SingleNodeDelete(e.CommandArgument.ToInt32());

                   
                    strFolder = Convert.ToString((DocumentType)intDocType);

                    string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + strFolder;

                    if (File.Exists(strPath + "/" + Convert.ToString(strFilename)))
                        File.Delete(strPath + "/" + Convert.ToString(strFilename));

                }



                DataTable dtDocs = objCard.GetAttachments(-1,-1,0);
                DataRow drRow;
                foreach (DataListItem Item in dlOtherDoc.Items)
                {
                    if (e.Item.ItemIndex == Item.ItemIndex)
                        continue;

                    drRow = dtDocs.NewRow();

                    lblDocname = (Label)Item.FindControl("lblDocname");
                    lblFilename = (Label)Item.FindControl("lblFilename");

                    drRow["Node"] = dlOtherDoc.DataKeys[Item.ItemIndex];
                    drRow["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    drRow["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dtDocs.Rows.Add(drRow);
                }

                dlOtherDoc.DataSource = dtDocs;
                dlOtherDoc.DataBind();

                ViewState["OtherDoc"] = dtDocs;
                break;
        }
    }

    protected void dlCards_ItemBound(object source, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");

        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }

    public string GetImageUrl(string strType)
    {
        string ImageUrl = string.Empty;
        string sFilename = "";
        if (ViewState["TypeID"].ToInt32() == 1)
            sFilename = "Health Card";
        else if (ViewState["TypeID"].ToInt32() == 2)
            sFilename = "Labour Card";
        else if (ViewState["TypeID"].ToInt32() == 3)
            sFilename = "National ID Card";
        
        ImageUrl = "../images/"+strType + "_" + sFilename+".png";
        return ImageUrl;
        upMenu.Update();

    }
}