﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using System.Reflection;


public partial class Public_SalaryAdvanceRequest : System.Web.UI.Page
{
    clsSalaryRequest objRequest;
    clsUserMaster objUser;
    clsLeaveRequest objLeaveRequest;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        SalaryRequestpager.Fill += new controls_Pager.FillPager(BindDataList);

        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;
            int iRequestId = 0;

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;
            if (Request.QueryString["RequestId"] != null && Request.QueryString["Type"] == "Cancel")
            {
                iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (iRequestId > 0)
                    ViewState["RequestForCancel"] = true;
                fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);               
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;
            }
            else if (Request.QueryString["RequestId"] != null)
            {
                iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (iRequestId > 0)
                    ViewState["Approve"] = true;
                fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(iRequestId);

                TextBox txtAdvanceAmount = (TextBox)fvSalaryDetails.FindControl("txtAdvanceAmount");
                ViewState["RequestedAdvAmt"] = txtAdvanceAmount.Text.ToDouble();

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;
            }
            else
            {
                BindDataList();
            }
        }
    }

    private void RemoveQueryString(string Query)
    {
        PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isReadOnly.SetValue(this.Request.QueryString, false, null);

        Request.QueryString.Remove(Query);
        Response.ClearContent();
    }

    protected void cvValidateAssociates_ServerValidate(object source, ServerValidateEventArgs args)
    {
        CheckBoxList cblEmployees = (CheckBoxList)fvSalaryDetails.FindControl("cblEmployees");
        bool isSelected = false;
        foreach (ListItem item in cblEmployees.Items)
        {
            if (item.Selected)
            {
                isSelected = true;
                break;
            }
        }

        if (!isSelected)
        {
            args.IsValid = false;
        }
        else
        {
            args.IsValid = true;
        }
    }
    
    private void BindDataList()
    {

        objRequest = new clsSalaryRequest();
        objUser = new clsUserMaster();
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.Mode = "VIEW";
        objRequest.PageIndex = SalaryRequestpager.CurrentPage + 1;
        objRequest.PageSize = SalaryRequestpager.PageSize;
        DataTable oTable = objRequest.GetRequests();
        if (oTable.DefaultView.Count > 0)
        {
            lnkView.Enabled = true;
            dlSalaryRequest.DataSource = oTable;
            dlSalaryRequest.DataBind();

            SalaryRequestpager.Total = objRequest.GetRecordCount();
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlSalaryRequest.ClientID + "');";

            lnkDelete.Enabled = true;

            fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
            fvSalaryDetails.DataSource = null;
            fvSalaryDetails.DataBind();

            SalaryRequestpager.Visible = true;
        }
        else
        {
            lnkView.Enabled = false;
            dlSalaryRequest.DataSource = null;
            dlSalaryRequest.DataBind();
            lnkDelete.Enabled = false;
            fvSalaryDetails.ChangeMode(FormViewMode.Insert);
            SalaryRequestpager.Visible = false;
        }


    }

    private void BindRequestDetails(int iRequestId)
    {
        objRequest = new clsSalaryRequest();
        objRequest.Mode = "EDT";
        objRequest.RequestId = iRequestId;

        fvSalaryDetails.DataSource = objRequest.GetRequestDetails();
        fvSalaryDetails.DataBind();
        //link to display approval level remarks
        HtmlTableRow trlink = (HtmlTableRow)fvSalaryDetails.FindControl("trlink"); 
        if(trlink != null)
            trlink.Style["display"] = "table-row";

        HtmlTableRow trAuthority = (HtmlTableRow)fvSalaryDetails.FindControl("trAuthority");
        DataList dl1 = (DataList)fvSalaryDetails.FindControl("dl1");
        if (trAuthority != null)
        {
            objRequest = new clsSalaryRequest();
            objRequest.RequestId = iRequestId;
            objRequest.CompanyId = objRequest.GetCompanyID();
            DataTable dt2 = objRequest.GetApprovalHierarchy();
            if (dt2.Rows.Count > 0)
            {
                trAuthority.Style["display"] = "table-row";
                dl1.DataSource = dt2;
                dl1.DataBind();
            }
        }
        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
        else
            ViewState["RequestID"] = objRequest.RequestId;        

        dlSalaryRequest.DataSource = null;
        dlSalaryRequest.DataBind();

        SalaryRequestpager.Visible = false;
    }
    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsSalaryRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {           
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            LinkButton lbReason = (LinkButton)fvSalaryDetails.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)fvSalaryDetails.FindControl("hfLink");
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                HtmlTableRow trReasons = (HtmlTableRow)fvSalaryDetails.FindControl("trReasons");
                GridView gvReasons = (GridView)fvSalaryDetails.FindControl("gvReasons");
                trReasons.Style["display"] = "table-row";

                DataTable dt = objRequest.DisplayReasons(objRequest.RequestId);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                HtmlTableRow trReasons = (HtmlTableRow)fvSalaryDetails.FindControl("trReasons");
                trReasons.Style["display"] = "none";
            }
        }
    }
    
    protected void dlSalaryRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_View":
                HiddenField hfLink = (HiddenField)fvSalaryDetails.FindControl("hfLink");
                HtmlTableRow trReasons = (HtmlTableRow)fvSalaryDetails.FindControl("trReasons");
               
                if (hfLink != null && trReasons != null)
                {
                    hfLink.Value = "0";
                    trReasons.Style["display"] = "none";
                }

                fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                
                if (clsUserMaster.GetCulture() == "ar-AE")
                    lnkDelete.OnClientClick = "return confirm('هل أنت متأكد أنك تريد حذف؟');";
                else
                    lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;
                break;

            case "_Edit":

                fvSalaryDetails.ChangeMode(FormViewMode.Edit);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;

            case "_Cancel":
                Label lbRequested = (Label)e.Item.FindControl("lbRequested");
                RequestCancel(Convert.ToInt32(e.CommandArgument), lbRequested.Text);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;
        }
    }

    private void RequestCancel(int RequestId, string sRequestedIds)
    {
        objUser = new clsUserMaster();
        objRequest = new clsSalaryRequest();

        int EmployeeID = objUser.GetEmployeeId();

        objRequest.RequestId = RequestId;
        objRequest.EmployeeId = EmployeeID;
        objRequest.StatusId = Convert.ToInt32(RequestStatus.RequestForCancel);
        objRequest.Reason = "Salary Advance Request for Cancellation";
        objRequest.UpdateCancelStatus();

        clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.RequestForCancel, "");
        clsCommonMail.SendMail(RequestId, MailRequestType.SalaryAdvance, eAction.RequestForCancel);
        //--------------------------------------------------------------------------------------------------------------------------------------------------
        BindDataList();
        if (clsUserMaster.GetCulture() == "ar-AE")
            msgs.InformationalMessage("تم تحديث التفاصيل بنجاح.");
        else
            msgs.InformationalMessage("Successfully updated request details.");

        mpeMessage.Show();
    }
    
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        fvSalaryDetails.ChangeMode(FormViewMode.Insert);

        dlSalaryRequest.DataSource = null;
        dlSalaryRequest.DataBind();

        lnkDelete.Enabled = false;
        lnkDelete.OnClientClick = "return false;";
        SalaryRequestpager.Visible = false;

        upForm.Update();
        upnlDatalist.Update();
    }

    public void DisplaySalaryAdvLimit(int EmpId, Label lbAmount)
    {
        objRequest = new clsSalaryRequest();
        objRequest.EmployeeId = EmpId;

        DataTable dtCm = objRequest.GetAdvaceAmountAndPercentage();
        
        if (lbAmount != null)
            lbAmount.Text = dtCm.Rows[0]["Amount"].ToDecimal().ToString("F" + 2);
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        BindDataList();

        upForm.Update();
        upnlDatalist.Update();
    }
    protected void SetValues(object sender, EventArgs e)
    {
        Label lbRequestedAmount = (Label)fvSalaryDetails.FindControl("lbRequestedAmount");
        Label lbApprovedAmount = (Label)fvSalaryDetails.FindControl("lbApprovedAmount");
        TextBox txtDate = (TextBox)fvSalaryDetails.FindControl("txtSalaryDate");
        Label lbDate = new Label();
        if (txtDate != null)
        {
            lbDate.Text = txtDate.Text;
        }
        Label lbLoanAmount = (Label)fvSalaryDetails.FindControl("lbLoanAmount");
        Label lbGrosssalary = new Label();

        objUser = new clsUserMaster();
        int EmployeeID = objUser.GetEmployeeId();

        RequestedApproveddetails(lbRequestedAmount, lbApprovedAmount, lbLoanAmount, lbGrosssalary, lbDate.Text, EmployeeID);
    }
    protected void fvSalaryDetails_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        objRequest = new clsSalaryRequest();
        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();

        int EmployeeID = objUser.GetEmployeeId();

        string sToEmployees = string.Empty;

        DropDownList ddlStatus = (DropDownList)fvSalaryDetails.FindControl("ddlEditStatus");

        Label lblDate = (Label)fvSalaryDetails.FindControl("lblDate");
        Label lblAmount = (Label)fvSalaryDetails.FindControl("lblAmount");
        Label lblReason = (Label)fvSalaryDetails.FindControl("lblReason");
        TextBox txtRemarks = (TextBox)fvSalaryDetails.FindControl("txtRemarks");

        Label lblViewCurrency = (Label)fvSalaryDetails.FindControl("lblViewCurrency");

        string sRequestedIds = string.Empty;
        string sToMailId = string.Empty;
        int iRequestedById, iRequestId = 0;

        switch (e.CommandName)
        {
            case "Add":

                string RequestedTo = clsLeaveRequest.GetRequestedTo(e.CommandArgument.ToInt32(), Convert.ToInt64(objUser.GetEmployeeId()), Convert.ToInt32(RequestType.SalaryAdvance));
                if (RequestedTo == string.Empty)
                {
                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في الوقت الحالي.يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
                    msgs.InformationalMessage(message);
                    mpeMessage.Show();
                }
                else
                {
                    bool bIsInsert = true;

                    TextBox txtAmount = fvSalaryDetails.FindControl("txtAmount") as TextBox;
                    TextBox txtReason = fvSalaryDetails.FindControl("txtReason") as TextBox;
                    TextBox txtSalaryDate = fvSalaryDetails.FindControl("txtSalaryDate") as TextBox;

                    Label lblCurrency = (Label)fvSalaryDetails.FindControl("lblCurrency");
                    Label lbl = (Label)fvSalaryDetails.FindControl("lbl");


                    decimal dSalAdvAmt = 0.000M;
                    if (!decimal.TryParse(txtAmount.Text.Trim() == string.Empty ? "0" : txtAmount.Text.Trim(), out dSalAdvAmt))
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب ان يكون المبلغ صحيح") : ("Amount should be valid.");

                        lbl.Text = message;
                        lbl.Visible = true;
                        return;
                    }
                    else if (dSalAdvAmt == 0)
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب ان يكون المبلغ صحيح") : ("Amount should be valid.");

                        lbl.Text = message;
                        lbl.Visible = true;
                        return;
                    }
                    double dAmount = 0;

                    if (Convert.ToString(e.CommandArgument) != "")
                    {
                        iRequestId = Convert.ToInt32(e.CommandArgument);
                    }
                    objRequest.EmployeeId = EmployeeID;
                    dAmount = objRequest.GetAdvanceAmount();
                    if (objRequest.CheckEmployeeSalaryIsProcessed(EmployeeID.ToInt64(), clsCommon.Convert2DateTime(txtSalaryDate.Text)) > 0)
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("تتم معالجة الراتب بالفعل في هذه الفترة.") : ("Salary is already processed for this period");
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();
                    }

                    else if (dAmount == 0.0)
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لم يتم تعريف هيكل الرواتب الخاص بك. يرجى الاتصال السلطة المختصة.") : ("Your Salary structure is not defined.Please contact concerned authority .");
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();
                    }
                    else
                    {
                        if (iRequestId == 0)
                        {
                            //set values 
                            objRequest.Mode = "INS";
                        }
                        else
                        {
                            objRequest.Mode = "UPT";
                            objRequest.RequestId = iRequestId;
                            bIsInsert = false;
                        }

                        objRequest.Amount = txtAmount.Text.ToDouble();
                        objRequest.Reason = txtReason.Text;
                        objRequest.RequestedTo = RequestedTo;
                        objRequest.RequestedDate = clsCommon.Convert2DateTime(txtSalaryDate.Text);
                        objRequest.StatusId = Convert.ToInt32(RequestStatus.Applied);

                        iRequestId = objRequest.InsertRequest();
                        clsCommonMessage.SendMessage(EmployeeID, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Applied);

                        string message = "";
                        if (bIsInsert)
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم اضافة تفاصيل الطلب بنجاح") : ("Request details added successfully.");
                        else
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث تفاصيل الطلب بنجاح") : ("Request details updated successfully.");
                        
                        upForm.Update();
                        upnlDatalist.Update();
                        BindDataList();
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();
                    }
                }
                break;

            case "CancelRequest":
                upForm.Update();
                upnlDatalist.Update();             
                BindDataList();
                break;

            case "Approve":
                iRequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                TextBox txtAdvanceAmount = (TextBox)fvSalaryDetails.FindControl("txtAdvanceAmount");
                TextBox txtAccDate = (TextBox)fvSalaryDetails.FindControl("txtAccDate");

                if (txtAdvanceAmount.Text == "" | Convert.ToDouble(txtAdvanceAmount.Text) == 0)
                {
                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب ان يكون المبلغ صحيح") : ("Amount should be valid.");
                    msgs.InformationalMessage(message);
                    mpeMessage.Show();
                }
                else
                {
                    objRequest.RequestId = iRequestId;
                    iRequestedById = objRequest.GetRequestedById();

                    objRequest = new clsSalaryRequest();
                    objRequest.RequestId = iRequestId;
                    objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                    objLeaveRequest.EmployeeId = EmployeeID;

                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                    {
                        if (objRequest.GetWorkStatusId() < 6)
                        {
                            string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Cannot approve the request. The requested employee is no longer in service.");
                            msgs.InformationalMessage(message);
                            mpeMessage.Show();
                        }
                        else
                        {
                             if (clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.SalaryAdvance), Convert.ToInt64(EmployeeID), objRequest.RequestId))
                            {
                                objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                {
                                    
                                    if (SalaryAdvanceEntryValidations(iRequestedById, iRequestId))
                                    {
                                        objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                                        objRequest.UserId = objUser.GetUserId();
                                        objRequest.RequestedDate = clsCommon.Convert2DateTime(txtAccDate.Text);
                                        objRequest.Amount = txtAdvanceAmount.Text.ToDouble();
                                        objRequest.Reason = txtRemarks.Text;

                                        objRequest.UpdateAdvanceStatus(true);
                                        new clsExportToCSV().ExportToCSV(objRequest.RequestedDate);
                                        divPrint.Style["display"] = "block";
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                                else
                                {
                                    objRequest.Reason = txtRemarks.Text;
                                    objRequest.UpdateAdvanceStatus(true);
                                }

                                objRequest.RequestId = iRequestId;

                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5 || Convert.ToInt32(ddlStatus.SelectedValue) == 3 || Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                                {
                                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                                    msgs.InformationalMessage(message);
                                    mpeMessage.Show();

                                    if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                    {
                                        //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Approved, "");
                                        clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Approved);
                                        ////-----------------------------------------------------------------------------------------------------------
                                    }
                                    else if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                                    {
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Reject, "");
                                        clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Reject);
                                    }

                                    else if (Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                                    {
                                        clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Pending, "");
                                    }
                                }
                                else
                                {
                                    string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                                    msgs.InformationalMessage(message);
                                    mpeMessage.Show();
                                }
                            }
                            else
                            {
                                divPrint.Style["display"] = "none";
                                objRequest.Reason = txtRemarks.Text;
                              //  objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue) == 5 ? 1 : Convert.ToInt32(ddlStatus.SelectedValue);

                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);// == 5 ? 1 : Convert.ToInt32(ddlStatus.SelectedValue);
                                objRequest.UpdateAdvanceStatus(false);
                                string message = "";
                                if (Convert.ToInt32(ddlStatus.SelectedValue) == 5)
                                {
                                    clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Approved, "");

                                    clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Approved);
                                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم توجيه طلب الحصول على راتب مقدم بنجاح") : ("Salary advance request forwarded successfully.");
                                }
                                else
                                {
                                    clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Reject);
                                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم رفض طلب الحصول على راتب مسبق بنجاح") : ("Salary advance request rejected successfully.");
                                }

                                msgs.InformationalMessage(message);
                                mpeMessage.Show();
                            }
                        }
                        ViewState["Approve"] = false;
                        ViewState["RequestedAdvAmt"] = null;
                        fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                        BindRequestDetails(iRequestId);
                        lnkDelete.OnClientClick = "return false;";
                        lnkDelete.Enabled = false;

                        RemoveQueryString("RequestId");
                    }
                    else
                    {
                        objRequest.RequestId = iRequestId;
                        iRequestedById = objRequest.GetRequestedById();
                        objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);

                        objRequest.EmployeeId = iRequestedById;
                        objRequest.Reason = txtRemarks.Text;
                        objRequest.UpdateAdvanceStatus(true);
                        string message = "";
                        if (Convert.ToInt32(ddlStatus.SelectedValue) == 3)
                        {
                            clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Reject, "");
                            clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Reject);
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب سلفة على المرتب رفض بنجاح.") : ("Salary advance request rejected successfully.");
                        }
                        else if (Convert.ToInt32(ddlStatus.SelectedValue) == 4)
                        {
                            clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Pending, "");
                            message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                        }
                        ViewState["Approve"] = false;
                        ViewState["RequestedAdvAmt"] = null;
                        fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                        BindRequestDetails(iRequestId);
                        lnkDelete.OnClientClick = "return false;";
                        lnkDelete.Enabled = false;
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();

                        RemoveQueryString("RequestId");
                    }
                }

                break;
            case "Cancel":
                if (ViewState["PrevPage"] != null)
                    this.Response.Redirect(ViewState["PrevPage"].ToString());
                else
                    this.Response.Redirect("home.aspx");

                break;

            case "RequestForCancel":
                iRequestId = Convert.ToInt32(fvSalaryDetails.DataKey["RequestId"]);
                objRequest.RequestId = iRequestId;
                DataTable dtDate = objRequest.GetApprovalDate();

                string sDate = dtDate.Rows[0]["Accountdate"].ToString();
                string strmessage = "";
                if (objRequest.GetWorkStatusId() < 6)
                {
                    strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل في إلغاء الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Failed to cancel the request. The requested employee is no longer in service.");
                }
                else if (objRequest.CheckEmployeeSalaryIsProcessed(objRequest.GetRequestedById().ToInt64(), clsCommon.Convert2DateTime(sDate)) > 0)
                {
                    strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("ففشل في إلغاء الطلب. تتم معالجة الراتب بالفعل لهذه الفترة") : ("Failed to cancel the request. Salary is already processed for this period.");
                }
                else
                {
                    objRequest.RequestId = iRequestId;
                    objRequest.StatusId = Convert.ToInt32(ddlStatus.SelectedValue);
                    objRequest.EmployeeId = EmployeeID;
                    objRequest.Reason = txtRemarks.Text; //txtEditReason.Text;//
                    objRequest.SalaryAdvanceRequestCancelled();

                    iRequestedById = objRequest.GetRequestedById();

                    clsCommonMail.SendMail(iRequestId, MailRequestType.SalaryAdvance, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------

                    clsCommonMessage.SendMessage(null, iRequestId, eReferenceTypes.SalaryAdvanceRequest, eMessageTypes.Salary__Advance_Request, eAction.Cancelled, "");


                    //-----------------------------------------------------------------------------------------------------------      

                    ViewState["RequestForCancel"] = false;
                    BindRequestDetails(iRequestId);
                    //txtEditReason.Visible = false;
                    //lblReason.Visible = true;
                    strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                }
                msgs.InformationalMessage(strmessage);
                mpeMessage.Show();
                break;
        }
    }

    /// <summary>
    /// salary advance request validations
    /// </summary>
    private bool SalaryAdvanceEntryValidations(int EmployeeID, int RequestID)
    {
        objRequest = new clsSalaryRequest();

        TextBox txtAdvanceAmount = fvSalaryDetails.FindControl("txtAdvanceAmount") as TextBox;
        Label lblDate = (Label)fvSalaryDetails.FindControl("lblDate");
        TextBox txtAccDate = fvSalaryDetails.FindControl("txtAccDate") as TextBox;


        DateTime fromDate = (clsCommon.Convert2DateTime(txtAccDate.Text).Month.ToString() + "/" + "01/" + clsCommon.Convert2DateTime(txtAccDate.Text).Year.ToString()).ToDateTime();
        DateTime toDate = fromDate.AddDays(DateTime.DaysInMonth(clsCommon.Convert2DateTime(txtAccDate.Text).Year, clsCommon.Convert2DateTime(txtAccDate.Text).Month) - 1).ToDateTime();
        DataTable dtTotalAmount = objRequest.GetTotalSalaryAdvance(EmployeeID.ToInt64(), fromDate, toDate);

        objRequest.EmployeeId = EmployeeID;
        objRequest.RequestId = RequestID;
        string message = "";
        if (clsCommon.Convert2DateTime(txtAccDate.Text) > DateTime.Now)
        {
            message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. تاريخ الحساب هو أكبر من التاريخ الحالي.") : ("Cannot approve the request. Account date is greater than current date.");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;
        }

        double dAdvLimit = objRequest.GetAdvanceAmount();

        double dAppAmount = Convert.ToDouble(dtTotalAmount.Rows[0]["Amount"]);

        double dAmountforApproval = dAppAmount + Convert.ToDouble(txtAdvanceAmount.Text);
        if (dAdvLimit < dAmountforApproval)
        {

            message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. تم تجاوز الحد الاقصى للراتب المسبق") : ("Cannot approve the request. Salary Advance Limit Exceed .");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;
        }

        if (objRequest.GetWorkStatusId() < 6)
        {

            message = clsUserMaster.GetCulture() == "ar-AE" ? ("الموظف المطلوب لم يعد في الخدمة.") : ("The requested employee is no longer in service.");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;


        }

        objRequest.RequestedDate = clsCommon.Convert2DateTime(txtAccDate.Text);
        if (objRequest.CheckDateOfJoining())
        {

            message = clsUserMaster.GetCulture() == "ar-AE" ? ("يجب أن يكون تاريخ حساب أكبر من تاريخ الانضمام.") : ("Account date should be greater than the joining date.");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;


        }
        if (objRequest.CheckEmployeeSalaryIsProcessed(EmployeeID, clsCommon.Convert2DateTime(txtAccDate.Text)) > 0)
        {

            message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. تتم معالجة الراتب بالفعل لهذه الفترة") : ("Cannot approve the request. Salary is already processed for this period");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;


        }


        if (txtAdvanceAmount.Text.ToDouble() > ViewState["RequestedAdvAmt"].ToDouble())
        {
            message = clsUserMaster.GetCulture() == "ar-AE" ? (ViewState["RequestedAdvAmt"].ToDouble() + "يرجى التحقق من كمية مسبقا. المبلغ المطلوب هو") : "Please check the advance amount.Requested amount is " + ViewState["RequestedAdvAmt"].ToDouble();
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            return false;


        }

        return true;
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;

        if (dlSalaryRequest.Items.Count > 0)
        {
            foreach (DataListItem item in dlSalaryRequest.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkAdvance");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {

                    if (DeleteRequest(Convert.ToInt32(dlSalaryRequest.DataKeys[item.ItemIndex])))
                        message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : ("Request(s) deleted successfully");
                    else
                        message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted.");




                }
            }

        }
        else
        {
            if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly)
            {
                if (DeleteRequest(Convert.ToInt32(fvSalaryDetails.DataKey["RequestId"])))
                    message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : "Request deleted successfully";
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب لا يمكن حذفها.") : "Processing started request cannot be deleted.";
            }
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();

        upForm.Update();
        upnlDatalist.Update();
    }

    private bool DeleteRequest(int requestId)
    {
        string message = string.Empty;

        objRequest = new clsSalaryRequest();
        objRequest.RequestId = requestId;

        if (objRequest.IsSalaryRequestApproved())
        {
            return false;
        }
        else
        {
            objRequest.Mode = "DEL";
            objRequest.DeleteRequest();
            clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.SalaryAdvanceRequest);
            return true;
        }
    }

    protected void fvSalaryDetails_DataBound(object sender, EventArgs e)
    {
        objUser = new clsUserMaster();
        clsLoanRequest loanRequest = new clsLoanRequest();
        objLeaveRequest = new clsLeaveRequest();

        HiddenField hfLoanDate = (HiddenField)fvSalaryDetails.FindControl("hfLoanDate");        
        int EmployeeID = objUser.GetEmployeeId();
        int RequestId = 0;

        if (Request.QueryString["RequestId"] != null)
        {
            RequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
            ViewState["RequestID"] = objRequest.RequestId;
        }        
        if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == false)
        {
            HtmlTableRow trForwardedBy = (HtmlTableRow)fvSalaryDetails.FindControl("trForwardedBy");
            if (trForwardedBy != null)
                trForwardedBy.Visible = false;

            HtmlTableRow trappAmount = (HtmlTableRow)fvSalaryDetails.FindControl("trappAmount");
            if (trappAmount != null)
                trappAmount.Style["display"] = "table-row";           
        }

        if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly)
        {
            //ImageButton imgBack = (ImageButton)fvSalaryDetails.FindControl("imgBack");
            Label lbViewCurrency = (Label)fvSalaryDetails.FindControl("lblViewCurrency");
            int EmpId = 0;
            if (Request.QueryString["RequestId"] != null)
            {
                //if (imgBack != null)
                //    imgBack.Visible = true;
                objRequest.RequestId = Convert.ToInt32(Request.QueryString["RequestId"]);
                EmpId = objRequest.GetRequestedById();
            }
            else
            {
                //if (imgBack != null)
                //    imgBack.Visible = false;
                EmpId = EmployeeID;
            }
            if (lbViewCurrency != null)
            {
                loanRequest.EmployeeId = EmpId;
                lbViewCurrency.Text = EmpId == 0 ? "AED" : loanRequest.GetCurrency();
            }
        }

        if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["Approve"]) == true)
        {
            DropDownList ddlEditStatus = (DropDownList)fvSalaryDetails.FindControl("ddlEditStatus");

            ddlEditStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
            ddlEditStatus.DataBind();

            Label lbRequestedAmount = (Label)fvSalaryDetails.FindControl("lbRequestedAmount");
            Label lbApprovedAmount = (Label)fvSalaryDetails.FindControl("lbApprovedAmount");
            Label lbDate = (Label)fvSalaryDetails.FindControl("lblDate");
            Label lbLoanAmount = (Label)fvSalaryDetails.FindControl("lbLoanAmount");
            Label lbGrosssalary = (Label)fvSalaryDetails.FindControl("lbGrosssalary");
            if (lbApprovedAmount != null)
                RequestedApproveddetails(lbRequestedAmount, lbApprovedAmount, lbLoanAmount, lbGrosssalary, lbDate.Text, objRequest.GetRequestedById());
            
            Label lblAdvanceAmount = fvSalaryDetails.FindControl("lbAdvanceLimit") as Label;
            if (lblAdvanceAmount != null)
            {
                objRequest.RequestId = RequestId;
                DisplaySalaryAdvLimit(objRequest.GetRequestedById(), lblAdvanceAmount);

                lblAdvanceAmount.Text = GetLocalResourceObject("AdvanceLimit.Text").ToString() + " : " + lblAdvanceAmount.Text;
            }

            lbGrosssalary.Visible = clsLeaveRequest.IsHigherAuthority((int)RequestType.SalaryAdvance, Convert.ToInt64(EmployeeID), RequestId);
            
            Label lblStatus = (Label)fvSalaryDetails.FindControl("lblStatus");
            Button btnApprove = (Button)fvSalaryDetails.FindControl("btnApprove");
            Button btCancel = (Button)fvSalaryDetails.FindControl("btCancel");
            HtmlTableCell trAmount = (HtmlTableCell)fvSalaryDetails.FindControl("trAmount");
            HtmlTableCell trAdvAmount = (HtmlTableCell)fvSalaryDetails.FindControl("trAdvAmount");
            HtmlTableRow trAccDate = (HtmlTableRow)fvSalaryDetails.FindControl("trAccDate");
            HtmlTableRow trRemarks = (HtmlTableRow)fvSalaryDetails.FindControl("trRemarks");
            
            if (Request.QueryString["RequestId"] != null)
            {
                lblStatus.Visible = false;
                ddlEditStatus.Visible = true;
                ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("5"));
                btnApprove.Visible = true;
                btCancel.Visible = true;
            }
            else
            {
                lblStatus.Visible = true;
                ddlEditStatus.Visible = false;
                btnApprove.Visible = false;
                btCancel.Visible = false;
            }

            if (clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.SalaryAdvance), Convert.ToInt64(EmployeeID), RequestId))
            {
                trAmount.Style["display"] = "none";
                trAdvAmount.Style["display"] = "block";
                trAccDate.Style["display"] = "table-row";
                trRemarks.Style["display"] = "table-row";
            }
            else
            {               
                ddlEditStatus.Items.Remove(new ListItem("Pending", "4"));
            }
            //ImageButton imgBack = (ImageButton)fvSalaryDetails.FindControl("imgBack");
            if (ibtnBack.Visible)
            {
               //imgBack.Visible = true;
                trRemarks.Style["display"] = "table-row";
            }
        }
        else if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestCancel"]) == true)
        {
            Label lbRequestedAmount = (Label)fvSalaryDetails.FindControl("lbRequestedAmount");
            Label lbApprovedAmount = (Label)fvSalaryDetails.FindControl("lbApprovedAmount");
            Label lbDate = (Label)fvSalaryDetails.FindControl("lblDate");
            Label lbLoanAmount = (Label)fvSalaryDetails.FindControl("lbLoanAmount");
            Label lbGrosssalary = (Label)fvSalaryDetails.FindControl("lbGrosssalary");
            
            if (lbApprovedAmount != null)
                RequestedApproveddetails(lbRequestedAmount, lbApprovedAmount, lbLoanAmount, lbGrosssalary, lbDate.Text, objRequest.GetRequestedById());

            Label lblAdvanceAmount = fvSalaryDetails.FindControl("lbAdvanceLimit") as Label;
            if (lblAdvanceAmount != null)
            {
                objRequest.RequestId = RequestId;
                DisplaySalaryAdvLimit(objRequest.GetRequestedById(), lblAdvanceAmount);

                lblAdvanceAmount.Text = GetLocalResourceObject("AdvanceLimit.Text").ToString() + " : " + lblAdvanceAmount.Text;
            }
            lbGrosssalary.Visible = clsLeaveRequest.IsHigherAuthority((int)RequestType.SalaryAdvance, Convert.ToInt64(EmployeeID), RequestId);

            DropDownList ddlEditStatus = (DropDownList)fvSalaryDetails.FindControl("ddlEditStatus");

            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvSalaryDetails.FindControl("lblStatus");
            Button btnCancel = (Button)fvSalaryDetails.FindControl("btnCancel");

            lblStatus.Visible = false;
            ddlEditStatus.Visible = true;
            ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("7"));
            ddlEditStatus.Enabled = false;
            btnCancel.Visible = true;
            //ImageButton imgBack = (ImageButton)fvSalaryDetails.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = true;
        }
        else if (fvSalaryDetails.CurrentMode == FormViewMode.ReadOnly && Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            DropDownList ddlEditStatus = (DropDownList)fvSalaryDetails.FindControl("ddlEditStatus");

            Label lbRequestedAmount = (Label)fvSalaryDetails.FindControl("lbRequestedAmount");
            Label lbApprovedAmount = (Label)fvSalaryDetails.FindControl("lbApprovedAmount");
            Label lbDate = (Label)fvSalaryDetails.FindControl("lblDate");
            Label lbLoanAmount = (Label)fvSalaryDetails.FindControl("lbLoanAmount");
            Label lbGrosssalary = (Label)fvSalaryDetails.FindControl("lbGrosssalary");
            HtmlTableRow trRemarks = (HtmlTableRow)fvSalaryDetails.FindControl("trRemarks");

            trRemarks.Style["display"] = "table-row";
            if (lbApprovedAmount != null)
                RequestedApproveddetails(lbRequestedAmount, lbApprovedAmount, lbLoanAmount, lbGrosssalary, lbDate.Text, objRequest.GetRequestedById());
            Label lblAdvanceAmount = fvSalaryDetails.FindControl("lbAdvanceLimit") as Label;
            if (lblAdvanceAmount != null)
            {
                objRequest.RequestId = RequestId;
                DisplaySalaryAdvLimit(objRequest.GetRequestedById(), lblAdvanceAmount);
                lblAdvanceAmount.Text = GetLocalResourceObject("AdvanceLimit.Text").ToString() + " : " + lblAdvanceAmount.Text;
            }

            lbGrosssalary.Visible = clsLeaveRequest.IsHigherAuthority((int)RequestType.SalaryAdvance, Convert.ToInt64(EmployeeID), RequestId);

            if (ddlEditStatus != null)
            {
                ddlEditStatus.DataSource = new clsLeaveRequest().GetAllStatus();
                ddlEditStatus.DataBind();
            }

            Label lblStatus = (Label)fvSalaryDetails.FindControl("lblStatus");
            Button btnRequestForCancel = (Button)fvSalaryDetails.FindControl("btnRequestForCancel");

            lblStatus.Visible = false;
            ddlEditStatus.Visible = true;
            ddlEditStatus.SelectedIndex = ddlEditStatus.Items.IndexOf(ddlEditStatus.Items.FindByValue("6"));
            ddlEditStatus.Enabled = false;
            btnRequestForCancel.Visible = true;
            //ImageButton imgBack = (ImageButton)fvSalaryDetails.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = true;
        }
        
        if (fvSalaryDetails.CurrentMode == FormViewMode.Insert | fvSalaryDetails.CurrentMode == FormViewMode.Edit)
        {
            DropDownList ddlStatus = (DropDownList)fvSalaryDetails.FindControl("ddlStatus");
            ddlStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlStatus.DataBind();
            TextBox txtSalaryDate = (TextBox)fvSalaryDetails.FindControl("txtSalaryDate");
            if (fvSalaryDetails.CurrentMode == FormViewMode.Insert)
                txtSalaryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            Label lbRequestedAmount = (Label)fvSalaryDetails.FindControl("lbRequestedAmount");
            Label lbApprovedAmount = (Label)fvSalaryDetails.FindControl("lbApprovedAmount");
            Label lbLoanAmount = (Label)fvSalaryDetails.FindControl("lbLoanAmount");
            
            RequestedApproveddetails(lbRequestedAmount, lbApprovedAmount, lbLoanAmount, null, txtSalaryDate.Text, EmployeeID);

            Label lbCurrency = (Label)fvSalaryDetails.FindControl("lblCurrency");
            Label lblAdvanceAmount = fvSalaryDetails.FindControl("lblAdvanceAmount") as Label;
            if (EmployeeID > 0)
            {
                loanRequest.EmployeeId = EmployeeID;
                if (lbCurrency != null)
                    lbCurrency.Text = loanRequest.GetCurrency();

                DisplaySalaryAdvLimit(EmployeeID, lblAdvanceAmount);
            }
            else
            {
                if (lbCurrency != null)
                    lbCurrency.Text = "AED";
            }
            if (Convert.ToBoolean(ViewState["Approve"]) == false && (fvSalaryDetails.CurrentMode == FormViewMode.Edit | fvSalaryDetails.CurrentMode == FormViewMode.Insert))
            {
                ddlStatus.Enabled = false;
                ddlStatus.CssClass = "dropdownlist_disabled";
            }

            if (fvSalaryDetails.CurrentMode == FormViewMode.Edit)
            {
                if (fvSalaryDetails.DataKey["StatusId"] != DBNull.Value)
                {
                    ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(Convert.ToString(fvSalaryDetails.DataKey["StatusId"])));
                }
            }
            if (fvSalaryDetails.CurrentMode == FormViewMode.Insert)
                hfLoanDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            //ImageButton imgBack = (ImageButton)fvSalaryDetails.FindControl("imgBack");
            //if (imgBack != null)
            //    imgBack.Visible = false;
        }
    }
    /// <summary>
    /// Get requested amount, approved amount,loan installment amount and gross salary orf requested employee
    /// </summary>
    /// <param name="lbRequestedAmount"></param>
    /// <param name="lbApprovedAmount"></param>
    /// <param name="lbloanAmount"></param>
    /// <param name="lbGrosSal"></param>
    /// <param name="sDate"></param>
    /// <param name="EmployeeID"></param>
    private void RequestedApproveddetails(Label lbRequestedAmount, Label lbApprovedAmount, Label lbloanAmount, Label lbGrosSal, string sDate, int EmployeeID)
    {
        DateTime fromDate = (clsCommon.Convert2DateTime(sDate).Month.ToString() + "/" + "01/" + clsCommon.Convert2DateTime(sDate).Year.ToString()).ToDateTime();
        DateTime toDate = fromDate.AddDays(DateTime.DaysInMonth(clsCommon.Convert2DateTime(sDate).Year, clsCommon.Convert2DateTime(sDate).Month) - 1).ToDateTime();
        objRequest = new clsSalaryRequest();

        DataTable dtTotalAmount = objRequest.GetTotalSalaryAdvance(EmployeeID.ToInt64(), fromDate, toDate);
        objRequest.EmployeeId = EmployeeID;
        objRequest.Month = clsCommon.Convert2DateTime(sDate).Month;
        objRequest.Year = clsCommon.Convert2DateTime(sDate).Year;
        double dAdvAmount = objRequest.GetMonthTotalAdvance();

        DataSet dsLoan = objRequest.GetLoanAmount();

        if (dtTotalAmount.Rows.Count > 0)
        {
            if (clsUserMaster.GetCulture() == "en-US")
            {
                lbApprovedAmount.Text = "Approved  Amount : " + dtTotalAmount.Rows[0]["Amount"].ToString();
                lbRequestedAmount.Text = "Requested  Amount : " + dAdvAmount.ToString();
            }
            else
            {
                lbApprovedAmount.Text = "الكتاب وقت البدء : " + dtTotalAmount.Rows[0]["Amount"].ToString();
                lbRequestedAmount.Text = "الكتاب وقت البدء : " + dAdvAmount.ToString();
            }
        }
        else
        {
            lbApprovedAmount.Text = "";
            lbRequestedAmount.Text = "";
        }
        if (dsLoan.Tables[0].Rows.Count > 0)
        {
            lbloanAmount.Text = dsLoan.Tables[0].Rows[0]["LoanAmount"] != null ? Convert.ToString(dsLoan.Tables[0].Rows[0]["LoanAmount"]) : "0";
            lbloanAmount.Visible = lbloanAmount.Text != "";
            if (clsUserMaster.GetCulture() == "en-US")
            {
                lbloanAmount.Text = "Loan  Amount  : " + lbloanAmount.Text;
            }
            else
            {
                lbloanAmount.Text = "الكتاب وقت البدء  : " + lbloanAmount.Text;
            }
        }
        else
        {
            lbloanAmount.Text = "";
        }

        if (dsLoan.Tables[1].Rows.Count > 0)
        {
            if (lbGrosSal != null)
            {
                lbGrosSal.Text = dsLoan.Tables[1].Rows[0]["Amount"] != null ? Convert.ToString(dsLoan.Tables[1].Rows[0]["Amount"]) : "0";
                if (clsUserMaster.GetCulture() == "en-US")
                {
                    lbGrosSal.Text = "Gross Salary : " + lbGrosSal.Text;
                }
                else
                {
                    lbGrosSal.Text = "رقم الحساب : " + lbGrosSal.Text;
                }
            }
        }
    }

    protected void dlSalaryRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            TextBox txtStatusId = (TextBox)e.Item.FindControl("txtStatusId");
            int iRequestId = 0;
            iRequestId = Convert.ToInt32(txtStatusId.Text);

            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hdForwarded");
            if (iRequestId != 1 || Convert.ToBoolean(hdForwarded.Value) == true)
            {
                imgEdit.Enabled = false;
                imgEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton imgCancel = (ImageButton)e.Item.FindControl("imgCancel");
            if ((iRequestId == 1 && Convert.ToBoolean(hdForwarded.Value) == false) | iRequestId == 6 || iRequestId == 7 || iRequestId == 3)
            {
                imgCancel.Enabled = false;
                imgCancel.ImageUrl = "~/images/cancel_disable.png";
            }

            Label lbDlCurrency = (Label)e.Item.FindControl("lblDlCurrency");
            clsLoanRequest request = new clsLoanRequest();
            if (objUser.GetEmployeeId() > 0)
            {
                request.EmployeeId = objUser.GetEmployeeId();
                if (lbDlCurrency != null)
                    lbDlCurrency.Text = request.GetCurrency();
            }
            else
            {
                if (lbDlCurrency != null)
                    lbDlCurrency.Text = "AED";
            }
        }
    }

    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /// <summary>
    /// Crop the string if it exceeds the width of container 
    /// </summary>
    protected string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
    }
}
