﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Candidates.aspx.cs" Inherits="Public_Candidates" %>

<%@ Register Src="../Controls/CandidateView.ascx" TagName="CandidateView" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li class='selected'><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div>
        <div style="display: none">
            <asp:Button ID="btnSubmitMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:HiddenField ID="hfCandidateID" runat="server" />
    <asp:UpdatePanel ID="upfvCandidate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divListCandidate" runat="server" style="float: left; width: 100%;">
                <asp:DataList ID="dlCandidate" runat="server" DataKeyField="CandidateID" OnItemCommand="dlCandidate_ItemCommand"
                    OnItemDataBound="dlCandidate_ItemDataBound" Width="100%" CssClass="labeltext">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table border="0" cellpadding="5" cellspacing="0" width="100%">
                            <tr>
                                <td style="padding-left: 5px" valign="top" width="25px">
                                    <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id,'chkSelect')" />
                                </td>
                                <td style="padding-left: 8px">
                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table id="tblDetails" runat="server" border="0" cellpadding="2" cellspacing="0"
                            onmouseout="hideEditButton(this, 'lnkEmail');" onmouseover="showEditButton(this, 'lnkEmail');"
                            width="100%">
                            <tr valign="top">
                                <td rowspan="5" style="padding-left: 5px" valign="top" width="25px">
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </td>
                                <td rowspan="5" valign="top" width="105">
                                    <img alt="" src='<%# GetCandidatePhoto(Eval("CandidateID")) %>' style="border: 1px solid #F0F0F0;
                                        padding: 2px" width="100" />
                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td style="padding-bottom: 10px">
                                                            <asp:LinkButton ID="lnkCandidate" runat="server" CausesValidation="false" CommandArgument='<%# Eval("CandidateID")%>'
                                                                CommandName="View" CssClass="listHeader bold" ToolTip="view"> <%#GetCandidateName(Eval("CandidateName"),Eval("Qualification"), Eval("Experience"), Eval("JobCode"))%>
                                                                
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td width="50px" align="right" valign="top">
                                                            <asp:ImageButton ID="imgHistory" runat="server" CausesValidation="false" Width="18px"
                                                                ImageUrl="~/images/view_history.png" CommandArgument='<%# Eval("CandidateID") %>'
                                                                Style="display: block" ToolTip='<%$Resources:ControlsCommon,History%>' CommandName="History" />
                                                        </td>
                                                        <td align="left" valign="top" width="50px">
                                                            <asp:ImageButton ID="lnkEmail" runat="server" CausesValidation="false" Width="20px"
                                                                ImageUrl="~/images/edit.png" CommandArgument='<%# Eval("CandidateID") %>' CommandName="Modify"
                                                                meta:resourcekey="Edit" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="item_title" width="100px">
                                                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Telephone"></asp:Literal>
                                                        </td>
                                                        <td width="3%">
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("CurrentTelephone")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table border="0" cellpadding="2" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td class="item_title" width="100px">
                                                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Email"></asp:Literal>
                                                        </td>
                                                        <td width="3%">
                                                            :
                                                        </td>
                                                        <td>
                                                            <%# Eval("EmailID") %>
                                                        </td>
                                                        <td align="right" valign="top" width="100px">
                                                            <span class="statusspan">
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="CandidatePager" runat="server" />
                <center>
                    <asp:Label ID="lblCandidates" runat="server" CssClass="error" Style="display: none;"
                        Width="100%"></asp:Label>
                </center>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button2" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="Button2"
                        PopupControlID="Div2" BackgroundCssClass="modalBackground">
                    </AjaxControlToolkit:ModalPopupExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="divPrintDisplay" runat="server" style="float: left; width: 100%;">
                <uc1:CandidateView ID="CandidateView1" runat="server" />
            </div>
            <div id="divCandidateHistory" runat="server" style="float: left; width: 100%;">
                <uc:CandidateToEmployeeHistory ID="Candidatehistorycontrol" runat="server"></uc:CandidateToEmployeeHistory>
            </div>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn2" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn3" runat="server" />
            </div>
            <asp:Panel ID="modalpopVacancy" runat="server" Width="400px" Style="display: none;">
                <div id="popupmainwrap">
                    <div id="popupmain" style="width: 100%">
                        <div id="header11">
                            <div id="hbox1">
                                <div id="headername">
                                    <asp:Label ID="lblHeading" runat="server" Text="Associate Vacancy"></asp:Label>
                                </div>
                            </div>
                            <div id="hbox2">
                                <div id="close1">
                                    <a href="">
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                            CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div id="toolbar">
                            <div id="imgbuttons">
                            </div>
                        </div>
                        <div id="contentwrap">
                            <div id="content">
                                <fieldset style="padding: 10px;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 70px;">
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                                Vacancy
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlVacancy" runat="server" CssClass="dropdownlist_mandatory"
                                                    DataTextField="JobTitle" DataValueField="VacancyId" Width="250px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                        </div>
                        <div id="footerwrapper">
                            <div id="footer11">
                                <div id="buttons">
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CausesValidation="false"
                                        Text="Cancel" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeCandidateAssociation" runat="server"
                BackgroundCssClass="modalBackground" CancelControlID="btnCancel" PopupControlID="modalpopVacancy"
                PopupDragHandleControlID="modalpopVacancy" TargetControlID="btn">
            </AjaxControlToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlPrint" runat="server">
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div>
                    <%-- <AjaxControlToolkit:AutoCompleteExtender CompletionInterval ="1" CompletionSetCount ="10" MinimumPrefixLength ="1" TargetControlID ="txtSearch" ServicePath ="~/App_Code/AutoCompleteService.cs" ServiceMethod ="GetSuggestionsCandidates" ID="AutoCompleteExtender1" runat="server">
            </AjaxControlToolkit:AutoCompleteExtender>--%>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="wmDuration" runat="server" TargetControlID="txtSearch"
                        meta:resourcekey="SearchBy" WatermarkCssClass="textWmcss" Enabled="True">
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </div>
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="btnGo" runat="server" CausesValidation="false" ImageUrl="../images/search.png"
                        ToolTip="Click here to search" ImageAlign="AbsMiddle" OnClick="btnGo_Click" />
                </div>
            </div>
            <span style="font-size: 17px; width: 145px; margin-left: 15px; margin-top: 7px; float: right;
                padding-right: 72px; margin-bottom: 12px; color: #178fba;">
                <asp:LinkButton ID="ancAdvanceSearch" meta:resourcekey="AdvanceSearch" CausesValidation="false"
                    runat="server" OnClick="lnkAdvanceSearch_Click"></asp:LinkButton>
            </span>
            <div id="divAdvanceSearch" runat="server" style="display: none; max-height: 400px">
                <fieldset style="padding-right: 5px; margin-left: 20px; width: 80%; margin-top: 5px;
                    border-radius: 8px; float: left; margin-bottom: 14px; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 3px 2px rgba(208, 223, 226, 0.4) inset;">
                    <table cellpadding="2" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="SkillSet"></asp:Literal>
                                </span>
                                <br />
                                <asp:TextBox ID="txtSrchSkillSet" Width="150px" TextMode="MultiLine" CssClass="textbox"
                                    runat="server" onchange="RestrictMulilineLength(this, 100);" onkeyup="RestrictMulilineLength(this, 100);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Experience"></asp:Literal>
                                </span>
                                <br />
                                <asp:DropDownList ID="ddlSrchMinExperience" CssClass="dropdownlist" Width="55px"
                                    runat="server">
                                </asp:DropDownList>
                                &nbsp;&nbsp;
                                <asp:DropDownList ID="ddlSrchMaxExperience" Width="55px" CssClass="dropdownlist"
                                    runat="server">
                                </asp:DropDownList>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Qualification"></asp:Literal></span><br />
                                <asp:UpdatePanel ID="upddlSrchQualification" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlSrchQualification" DataTextField="Qualification" Width="120px"
                                            DataValueField="DegreeID" CssClass="dropdownlist" runat="server" OnSelectedIndexChanged="ddlSrchQualification_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Category"></asp:Literal></span><br />
                                <asp:UpdatePanel ID="upddlSrchSpecialization" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlSrchSpecialization" CssClass="dropdownlist" runat="server"
                                            Width="120px" DataTextField="QualificationCategory" DataValueField="QualificationCategoryID">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlSrchQualification" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="GratuationYear"></asp:Literal></span><br />
                                <asp:DropDownList ID="ddlSrchPassoutFrom" CssClass="dropdownlist" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Country"></asp:Literal></span><br />
                                <asp:DropDownList ID="ddlCountry" CssClass="dropdownlist" runat="server" DataTextField="CountryName"
                                    DataValueField="CountryID">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Referraltype"></asp:Literal></span><br />
                                <asp:DropDownList ID="ddlReferralType" CssClass="dropdownlist" AutoPostBack="true"
                                    runat="server" DataTextField="RefferalType" DataValueField="RefferaltypeId" OnSelectedIndexChanged="ddlReferralType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="font-size: 10px" class="item_title">
                                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ReferredBy"></asp:Literal></span><br />
                                <asp:DropDownList ID="ddlReferredBy" CssClass="dropdownlist" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="float: right; margin-right: 30px;">
                                    <asp:Button ID="btnadvanceSearch" CssClass="btnsubmit" runat="server" OnClick="btnadvanceSearch_Click"
                                        meta:resourcekey="Search" /></div>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddCandidate" ImageUrl="~/images/Add candidatesBig.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddCandidate" runat="server" OnClick="lnkAddCandidate_Click"
                            CausesValidation="false" meta:resourcekey="AddCandidate">
                    
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListCandidate" ImageUrl="~/images/Candidate_Details.png"
                        CausesValidation="false" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkListCandidate" runat="server" OnClick="lnkListCandidate_Click"
                            CausesValidation="false" meta:resourcekey="ListCandidate">
                     
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkPrint" runat="server" Enabled="false" CausesValidation="false"
                            OnClick="lnkPrint_Click" meta:resourcekey="Print">
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/Delete.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" Enabled="true" CausesValidation="false"
                            OnClick="lnkDelete_Click" meta:resourcekey="Delete">
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/offer_acceptance.png" runat="server"
                        CausesValidation="false" />
                </div>
                <div class="name" style="min-height: 100px;">
                    <h5>
                        <asp:LinkButton ID="ancOfferAcceptance" runat="server" OnClick="lnkOfferAcceptance_Click"
                            CausesValidation="false" meta:resourcekey="OfferAcceptance">
                        </asp:LinkButton></h5>
                    <asp:Label ID="lblOfferStatus" runat="server" Font-Bold="true" Font-Size="10"></asp:Label><br />
                    <div style="overflow: auto;">
                        <br />
                        <asp:Label ID="lblOfferRemarks" runat="server" Width="100%" Font-Bold="false" Font-Size="9"></asp:Label>
                    </div>
                </div>
                <div id="divOfferAcceptance" runat="server" style="display: none; max-height: 250px">
                    <fieldset style="padding-right: 5px; margin-left: 5px; width: 85%; margin-top: 5px;
                        border-radius: 8px; float: left; margin-bottom: 14px; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 3px 2px rgba(208, 223, 226, 0.4) inset;">
                        <table cellpadding="4" cellspacing="0" border="0" class="labeltext" width="100%">
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="rblOffer" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                                        CellPadding="5" CellSpacing="5">
                                        <asp:ListItem meta:resourcekey="Accept" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem meta:resourcekey="Reject" Value="0"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                    <asp:TextBox ID="txtRejectRemarks" runat="server" TextMode="MultiLine" Width="175px"
                                        Height="50px" onchange="RestrictMulilineLength(this, 100);" onkeyup="RestrictMulilineLengthCandidate(this, 100);"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ErrorMessage="*" ControlToValidate="txtRejectRemarks"
                                        ValidationGroup="Offer"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnOfferAcceptance" runat="server" CssClass="btnsubmit" ValidationGroup="Offer"
                                        Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                        OnClick="btnOfferAcceptance_Click" />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
