﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Web.UI.DataVisualization;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic;


public partial class Public_ManagerHomeNew : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsEmployee objEmployee;
    clsRoleSettings objRoleSettings;
    clsCommonMessage objComMessage;

    protected void Page_Load(object sender, EventArgs e)
    {

        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        ltMyRequest.Text = clsGlobalization.IsArabicCulture() ? "طلباتي" : "My Requests";
        litJobOpening.Text = clsGlobalization.IsArabicCulture() ? "وضائف شاغرة" : "Job Openings";
        litAttendance.Text = clsGlobalization.IsArabicCulture() ? "حضوري" : "My Attendance";
        litPerformance.Text = clsGlobalization.IsArabicCulture() ? "تقييم الأداء" : "Performance Evaluation";
    
        if (!IsPostBack)
        {
            int iEmployeeId = objUserMaster.GetEmployeeId();
            FillCompany();
            FillRequests();

            FillLeaveChart();

            // Attendance
            if (new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.MyAttendance).ToUpper() == "YES")
            {
                divMyAttendance.Style["display"] = "block";
                BindAttendance();
            }
            else
            {
                divMyAttendance.Style["display"] = "none";
            }

            FillMyRequests(iEmployeeId);

            FillDocReturnAlerts(ddlCompany.SelectedValue.ToInt32());

            //Evaluations

            rptEvaluation.DataSource = clsHomePage.GetEvaluations(iEmployeeId,ddlCompany.SelectedValue.ToInt32());
            rptEvaluation.DataBind();

            FillDocExpiryAlerts(ddlCompany.SelectedValue.ToInt32());
            updExpiryAlert.Update();

            FillJobTray(ddlCompany.SelectedValue.ToInt32());

            FillJobStatus(ddlCompany.SelectedValue.ToInt32());
            rdbNationality.Checked = true;
            //rdbNationality_CheckedChanged(new EventArgs(), e);
            FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف جنسية الموظف" : "Employee Nationalitywise", (int)eEmpSearchType.Nationality, ddlChartType.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();

        }
    }

    // fill company
    private void FillCompany()
    {
        objHomePage = new clsHomePage();
      
        ddlCompany.DataSource = objHomePage.GetAllCompany(objUserMaster.GetEmployeeId(),objUserMaster.GetUserId());
        ddlCompany.DataBind();
      //  ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUserMaster.GetCompanyId().ToString()));
        updCompany.Update();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        int CompanyId = ddlCompany.SelectedValue.ToInt32();
        FillDocExpiryAlerts(CompanyId);
        FillDocReturnAlerts(CompanyId);
        FillJobStatus(CompanyId);
        FillJobTray(CompanyId);
        FillLeaveChart();
        FillRequests();
        FillMyRequests(new clsUserMaster().GetEmployeeId());
        //rdbNationality.Checked = true;
        FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف جنسية الموظف" : "Employee Nationalitywise", (int)eEmpSearchType.Nationality, ddlChartType.SelectedValue.ToInt32(), CompanyId);
        updEmpSerach.Update();
        updEmpChart.Update();
        updRequests.Update();

        updJobStatus.Update();
        updAlert.Update();
        updExpiryAlert.Update();
        updMyRequests.Update();

    }

    /// <summary>
    /// Employee Count Chart Nationalitywise/Countrywise/Departmentwise/Designationwise
    /// </summary>
    /// <param name="Title"></param>
    /// <param name="SearchType"></param>
    /// <param name="ChartType"></param>
    #region FillEmployeeChart

        private void FillEmployeeChart(string Title, int SearchType, int ChartType,int CompanyId)
        {
            Int64 Total =0;
            //rdbNationality.Checked = true;
            DataSet ds = clsHomePage.FillEmployeeChart(CompanyId);

            DataTable dt = ds.Tables[SearchType];
            if (dt.Rows.Count > 0)
                Total = Convert.ToInt64(dt.Compute("Sum(EmpCount)", ""));


            double[] yValues = new double[dt.Rows.Count];
            string[] xValues = new string[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                yValues[i] = Convert.ToDouble(dt.Rows[i]["EmpCount"]);
                xValues[i] = Convert.ToString(dt.Rows[i]["Type"]);
            }

            ChartEmployee.Series["Default"].Points.DataBindXY(xValues, yValues);


            //Set Pie chart type
            ChartEmployee.Series["Default"].ChartType = (SeriesChartType)ChartType; //SeriesChartType.Pie;
            ChartEmployee.Series["Default"].Color = Color.Green;

            //Set labels style (Inside, Outside, Disabled)


            ChartEmployee.Series[0]["PieLabelStyle"] = "Inside";

            //Set chart title, color and font
            ChartEmployee.Titles[0].Text =" (" + Convert.ToString( Total) + ")" + " " + Title ;
            ChartEmployee.Titles[0].ForeColor = Color.DarkBlue;
            ChartEmployee.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);

            if (ChartEmployee.Series["Default"].ChartType == SeriesChartType.Bar)
            {
                ChartEmployee.Series["Default"].IsValueShownAsLabel = false;
                ChartEmployee.Legends[0].Enabled = false ;
            }
            else
            {
                ChartEmployee.Series["Default"].IsValueShownAsLabel = true;
                ChartEmployee.Legends[0].Enabled = true;
            }
            ChartEmployee.ChartAreas["ChartArea1"].AxisX.Interval = 1;

              
        }
        protected void rdbNationality_CheckedChanged(object sender, EventArgs e)
        {

            FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف جنسية الموظف" : "Employee Nationalitywise", (int)eEmpSearchType.Nationality, ddlChartType.SelectedValue.ToInt32(),ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();
        }
        protected void rdbCountry_CheckedChanged(object sender, EventArgs e)
        {

            FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف بلد الموظف": "Employee Countrywise", (int)eEmpSearchType.Country, ddlChartType.SelectedValue.ToInt32(),ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();
        }
        protected void rdbDepartment_CheckedChanged(object sender, EventArgs e)
        {

            FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف قسم الموظف" : "Employee Departmentwise", (int)eEmpSearchType.Department, ddlChartType.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();
        }
        protected void rdbDesignation_CheckedChanged(object sender, EventArgs e)
        {
            FillEmployeeChart("Employee Designationwise", (int)eEmpSearchType.Designation, ddlChartType.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();
        }
        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int SearchType = 0; ;
            if (rdbNationality.Checked) SearchType = (int)eEmpSearchType.Nationality;
            else if (rdbCountry.Checked) SearchType = (int)eEmpSearchType.Country;
            else if (rdbDepartment.Checked) SearchType = (int)eEmpSearchType.Department;
            FillEmployeeChart(clsGlobalization.IsArabicCulture() ? "صنف جنسية الموظف" : "Employee Nationalitywise", SearchType, ddlChartType.SelectedValue.ToInt32(), ddlCompany.SelectedValue.ToInt32());
            updEmpSerach.Update();
            updEmpChart.Update();
        }

    #endregion FillEmployeeChart

    #region FillRequests
    /// <summary>
    /// Fill all requests
    /// </summary>
        private void FillRequests()
        {
            string sPendingRequestType = string.Empty;

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            int iEmployeeId = objUserMaster.GetEmployeeId();

            DataSet  ds = clsHomePage.GetMessageChart(iEmployeeId, objUserMaster.GetRoleId(),ddlCompany.SelectedValue.ToInt32());

            double[] yValues = new double[ds.Tables[0].Rows.Count];
            string[] xValues = new string[ds.Tables[0].Rows.Count];
            double[] zValues = new double[ds.Tables[0].Rows.Count];
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                yValues[i] = Convert.ToDouble(ds.Tables[0].Rows[i]["MessageCount"]);
                xValues[i] = Convert.ToString(ds.Tables[0].Rows[i]["Messages"]);
                zValues[i] = Convert.ToDouble(ds.Tables[0].Rows[i]["RequestTypeID"]);
            }

            ChartRequests.Series["Default"].Points.DataBindXY(xValues, yValues);
            //Set Pie chart type
            ChartRequests.Series["Default"].ChartType = SeriesChartType.Pie   ; //SeriesChartType.Pie;
            ChartRequests.Series["Default"].Color = Color.Green ;

            ChartRequests.Titles[0].Text = " [" + Convert.ToInt64(ds.Tables[0].Rows.Count > 0 ? ds.Tables[0].Compute("Sum(MessageCount)", "") : 0) + "]"  +  " "  + (clsGlobalization.IsArabicCulture() ? "الرسائل" : "Messages") ; 
            ChartRequests.Titles[0].ForeColor = Color.DarkBlue;
            ChartRequests.Titles[0].ShadowColor = Color.LightGray;
            ChartRequests.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);
           // ChartEmployee.Legends[0].Enabled = true;
            ChartRequests.Series["Default"].IsValueShownAsLabel = true;


            Session["StatusID"] = -1;

            for (int i = 0; i < yValues.Length; i++)
            {
                switch ((int) zValues[i])
                {


                    case (int)eRequestType.PerformanceEvaluation:


                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "PerformanceEvaluation").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "PerformanceEvaluation.aspx?PerformanceInitiationId=" ;
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view performance evaluation requests";
                        break;


                    case (int)eRequestType.JobApproval:


                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Job").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view vacancy requests";
                        break;




                    case (int)eRequestType.CompensatoryOffRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "CompensatoryOff").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view compensatory leave requests";
                    
                        break;


                    case (int)eRequestType.TimeExtensionRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "TimeExtension").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view time extension requests";
                    

                        break;




                    case (int)eRequestType.VacationExtensionRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "VacationExtension").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view vacation extension requests";
                        break;





                    case (int)eRequestType.VacationLeaveRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "VacationLeave").ToString();
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view vacation leave requests";


                        break;




                    case (int)eRequestType.DocumentRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Document").ToString(); 
                       

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مستندات" : "Click here to view Document requests";
                        break;

                    case (int)eRequestType.ExpenseRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Expense").ToString(); 

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات  مصروف" : "Click here to view Expense requests";
                        break;

                   

                    case (int)eRequestType.LeaveExtensionRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "LeaveExtension").ToString(); //"Leave Extension";
                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات   تمديد الاجازة" : "Click here to view Leave Extension requests";
                        break;

                    case (int)eRequestType.LeaveRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Leave").ToString();//"Leave";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات اجازة / عطلة" : "Click here to view Leave requests";
                        break;
                    case (int)eRequestType.RejoinRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Rejoinrequest").ToString();//"Rejoin";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? " انقر هنا لعرض طلبات الاستقالة" : "Click here to view Rejoin requests";
                        break;
                    case (int)eRequestType.ResignationRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Resignation").ToString();//"Resignatioon";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? " انقر هنا لعرض طلبات الاستقالة" : "Click here to view Resignation requests";
                        break;
                    case (int)eRequestType.TicketRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Ticket").ToString();//"Ticket";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات التذاكر" : "Click here to view Ticket requests";
                        break;
                    case (int)eRequestType.AssetRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Asset").ToString();//"Asset";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? " انقر هنا لعرض طلبات الاستقالة" : "Click here to view Asset requests";
                        break;
                    case (int)eRequestType.GeneralRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "General").ToString();//"General";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? " انقر هنا لعرض طلبات الاستقالة" : "Click here to view General requests";
                        break;

                    case (int)eRequestType.TravelRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Travel").ToString();//"Travel";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات السفر" : "Click here to view Travel requests";
                        break;
                    case (int)eRequestType.AttendanceRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Attendance").ToString();//"Leave";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? " لعق هنا لعرض طلبات الحضور" : "Click here to view Attendance requests";
                        break;
                    case (int)eRequestType.LoanRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Loan").ToString(); //"Loan";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات   قرض" : "Click here to view Loan requests";
                        break;

                    case (int)eRequestType.OfferLetterApproval:
                        ChartRequests.Series["Default"].Points[i].Url = "HomeOfferApproval.aspx";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلب مصادقة على العرض" : "Click here to view offer approvals";
                      
                        break;

                    case (int)eRequestType.PerformanceAction:
                        break;

           
                    case (int)eRequestType.SalaryAdvanceRequest:

                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "SalaryAdvance").ToString(); //"Salary Advance";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات تسبيق على الراتب" : "Click here to view Salary advance requests";
                        break;

                    case (int)eRequestType.TrainingRequest:
                        break;

                    case (int)eRequestType.TransferRequest:
                        sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Transfer").ToString();//"Transfer";

                        ChartRequests.Series["Default"].Points[i].Url = "HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "";
                        ChartRequests.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "انقر هنا لعرض طلبات تحويل" : "Click here to view Transfer requests";

                        break;

                }
            }
             
        }
     #endregion FillRequests

        #region FillMyRequests
     //Requests
        private void FillMyRequests(int EmpID)
        {
            rptMyRequests.DataSource = objHomePage.GetMyRequests(EmpID, ddlCompany.SelectedValue.ToInt32());
            rptMyRequests.DataBind();
        }
           
        #endregion FillMyRequests


        /// <summary>
    /// Fill Late Comers/Leave/Vacation/Processing employees list
    /// </summary>
    private void FillLeaveChart()
        {
            int Exists = 0;
            
            DataTable dt = clsHomePage.FillLeaveChartCount(ddlCompany.SelectedValue.ToInt32());

            double[] yValues = new double[dt.Rows.Count];
            string[] xValues = new string[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                yValues[i] = Convert.ToDouble(dt.Rows[i]["Count"]);
                xValues[i] = Convert.ToString(dt.Rows[i]["Type"]) + " (" + yValues[i] + ")";
               
            }


            if (xValues.Length > 0)
            {
                xValues[0] = clsGlobalization.IsArabicCulture() ? "المتأخرين" : "Late Comers";
                xValues[1] = clsGlobalization.IsArabicCulture() ? "الاجازة" : "Leaves";
                xValues[2] = clsGlobalization.IsArabicCulture() ? "الاجازة السنوية" : "Vacation Leave";
                xValues[3] = clsGlobalization.IsArabicCulture() ? "معالجة الاجازة" : "Vacation Process";
            }
            

            // Populate series data
         
            ChartLeave.Series["Default"].Points.DataBindXY(xValues, yValues);

            //Set the colors of the Pie chart

            ChartLeave.Series["Default"].Points[0].Color = Color.Red;
            ChartLeave.Series["Default"].Points[1].Color = Color.Green;
            ChartLeave.Series["Default"].Points[2].Color = Color.Navy;
            ChartLeave.Series["Default"].Points[3].Color = Color.Gold ;



             string LateComers = clsHomePage.GetLateComersList(ddlCompany.SelectedValue.ToInt32());// Late comers
             string LeaveList = clsHomePage.GetLeaveList(ddlCompany.SelectedValue.ToInt32());// Leave
             string VacList = clsHomePage.GetVacLeaveList(ddlCompany.SelectedValue.ToInt32()); // Vacation leavelist
             string VacProcList = clsHomePage.GetVacProcLeaveList(ddlCompany.SelectedValue.ToInt32());// vacation Processing list
            ChartLeave.Series["Default"].Points[0].ToolTip = LateComers;
            ChartLeave.Series["Default"].Points[1].ToolTip = LeaveList;
            ChartLeave.Series["Default"].Points[2].ToolTip = VacList;
            ChartLeave.Series["Default"].Points[3].ToolTip = VacProcList;

            //Set Pie chart type
            ChartLeave.Series["Default"].ChartType = SeriesChartType.Column ;

                 

            //Set chart title, color and font
            ChartLeave.Titles[0].Text = clsGlobalization.IsArabicCulture() ? "المتأخرين/ الاجازة/العطلة/معالجة" : "Late Comers/Leave/Vacation/Processing";
            ChartLeave.Titles[0].ForeColor = Color.DarkBlue;
            ChartLeave.Titles[0].ShadowColor = Color.LightGray;
            ChartLeave.Titles[0].Font = new Font("Arial Black", 8, FontStyle.Regular);


        }


    #region MyAttendance
        #region Attendance

      
        protected void BindAttendance()
        {
            try
            {
                DataTable dtbreaktime = null;
                string shiftToTime = string.Empty;
                lblConsequence.Text = string.Empty;
                lblShift.Text = string.Empty;
                lblPolicy.Text = string.Empty;
                lblConseq1.Text = string.Empty;
                lblConseq2.Text = string.Empty;
                lblConseq3.Text = string.Empty;
                lblConseq4.Text = string.Empty;

                bool bExists = false;
                if (txtDate.Text == string.Empty)
                    txtDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

                DateTime dtDate = clsCommon.Convert2DateTime(txtDate.Text);

                DataSet dsAttendance = null;
                DataTable dtAttendance = null;
                DataTable dtAttendanceSummary = null;

                objHomePage = new clsHomePage();
                objUserMaster = new clsUserMaster();
                int iEmployeeId = objUserMaster.GetEmployeeId();
                int iCompanyID = objUserMaster.GetCompanyId();
                int iDayID = 0;
                if (Convert.ToInt32(dtDate.DayOfWeek) == 6)
                {
                    iDayID = 1;
                }
                else if (dtDate.DayOfWeek == 0)
                {
                    iDayID = 2;
                }
                else
                {
                    iDayID = Convert.ToInt32(dtDate.DayOfWeek) + 2;
                }

                if (objHomePage.IsAttendanceExists(iEmployeeId, iCompanyID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy")))
                {
                    bExists = true;
                    dtAttendance = objHomePage.GetEmployeeDayEntryExitDetails(iEmployeeId, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"), 0);
                    dtAttendanceSummary = objHomePage.GetAttendanceSummary(iEmployeeId, iCompanyID, iDayID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"));
                }
                else
                {
                    bExists = false;
                    dsAttendance = objHomePage.GetAttendance(iEmployeeId, iCompanyID, iDayID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"));
                    dtAttendance = dsAttendance.Tables[0];

                }
                //--------------------------------------------------------Breaktime  after normal worktime----------------------------------

                if (dtAttendance.Rows.Count > 0)
                {
                    lblNoData.Style["display"] = "none";
                    dlAttendanceDetails.Visible = true;
                   // tblsummary.Visible = true;
                    tblsummary.Style["display"] = "block";
                    dlAttendanceDetails.DataSource = dtAttendance;
                    dlAttendanceDetails.DataBind();
                }
                else
                {
                    dlAttendanceDetails.DataSource = null;
                    lblNoData.Style["display"] = "block";
                    lblNoData.Text = clsGlobalization.IsArabicCulture() ? "لا يوجد بصم" : "No punchings found.";
                    dlAttendanceDetails.Visible = false;
                    //tblsummary.Visible = false;
                    tblsummary.Style["display"] = "none";
                }
                lblNormalWorktime.Text = lblWorktime.Text = lblAllowedBreakTime.Text =lblBreaktime.Text = string.Empty;
                //------------------------------------------------------------Calculation---------------------------------------------------
                String stotalworktime = "0";
                String stotalbreaktime = "0";

                String sworktime = "0";
                String sbreaktime = "0";

                int ssworktime = 0;
                int ssbreaktime = 0;

                int iWorktime = 0;
                int ibreaktime = 0;
                if (dtAttendance.Rows.Count > 0)
                {
                    int i;
                    for (i = 0; i <= dtAttendance.Rows.Count - 1; i++)
                    {
                        if (dtAttendance.Rows[i].ItemArray[3].ToString() != "" && dtAttendance.Rows[i].ItemArray[3].ToString() != "0")
                        {
                            ssworktime += Convert.ToInt32((DateAndTime.DateDiff(DateInterval.Minute, Convert.ToDateTime(dtAttendance.Rows[i].ItemArray[3]), DateAndTime.Today, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)));
                        }

                    }
                    for (i = 0; i <= dtAttendance.Rows.Count - 1; i++)
                    {
                        if (dtAttendance.Rows[i].ItemArray[4].ToString() != "" && dtAttendance.Rows[i].ItemArray[4].ToString() != "0")
                        {
                            ssbreaktime += Convert.ToInt32((DateAndTime.DateDiff(DateInterval.Minute, Convert.ToDateTime(dtAttendance.Rows[i].ItemArray[4]), DateAndTime.Today, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)));
                        }

                    }


                    TimeSpan Worktimeresult = TimeSpan.FromMinutes(Math.Abs(ssworktime));
                    TimeSpan Breaktimeresult = TimeSpan.FromMinutes(Math.Abs(ssbreaktime));

                    lblWorktime.Text = string.Format("{0:D2}:{1:D2} : {2:D2}", Worktimeresult.Hours, Worktimeresult.Minutes, Worktimeresult.Seconds); ;
                    lblBreaktime.Text = string.Format("{0:D2}:{1:D2} : {2:D2}", Breaktimeresult.Hours, Breaktimeresult.Minutes, Breaktimeresult.Seconds);

                    if (bExists == false)
                    {
                        if (dsAttendance.Tables[1].Rows.Count > 0)
                        {
                            lblShift.Text = "Shift - " + Convert.ToString(dsAttendance.Tables[1].Rows[0]["ShiftName"]) + "(" + Convert.ToString(dsAttendance.Tables[1].Rows[0]["FromTime"]) + "-" + Convert.ToString(dsAttendance.Tables[1].Rows[0]["ToTime"]) + ")";
                            lblNormalWorktime.Text = Convert.ToString(dsAttendance.Tables[1].Rows[0]["Duration"]);
                            lblAllowedBreakTime.Text = Convert.ToString(dsAttendance.Tables[1].Rows[0]["AllowedBreakTime"]);
                        }
                    }
                    else
                    {
                        if (dtAttendanceSummary.Rows.Count > 0)
                        {
                            lblShift.Text = "Shift - " + Convert.ToString(dtAttendanceSummary.Rows[0]["ShiftName"]) + "(" + Convert.ToString(dtAttendanceSummary.Rows[0]["FromTime"]) + "-" + Convert.ToString(dtAttendanceSummary.Rows[0]["ToTime"]) + ")";
                            lblNormalWorktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["Duration"]);
                            lblAllowedBreakTime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["AllowedBreakTime"]);
                            //lblWorktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["WorkTime"]);
                            //lblBreaktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["BreakTime"]);
                        }
                    }
                }

                if (dtAttendance.Rows.Count > 0)
                {
                    //DataTable dtConsequence = objHomePage.GetPolicyConsequence(iEmployeeId);
                    //if (dtConsequence.Rows.Count > 0)
                    //{
                    //    lblConsequence.Text = "Consequences";
                    //    lblPolicy.Text = "Policy name - " + Convert.ToString(dtConsequence.Rows[0]["BreakTime"]);
                    //    lblConseq1.Text = Convert.ToString(dtConsequence.Rows[1]["BreakTime"]);
                    //    lblConseq2.Text = Convert.ToString(dtConsequence.Rows[2]["BreakTime"]);
                    //    lblConseq3.Text = Convert.ToString(dtConsequence.Rows[3]["BreakTime"]);
                    //    lblConseq4.Text = Convert.ToString(dtConsequence.Rows[4]["BreakTime"]);
                    //}
                }
                updAtndata.Update();
                updAtnTimes.Update();

            }
            catch (Exception ex)
            {
            }
        }
        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            BindAttendance();
        }

        #endregion Attendance
    #endregion MyAttendance

    #region MyRequests
        protected void rptMyRequests_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HiddenField hfReadStatus = (HiddenField)e.Item.FindControl("hfReadStatus");
            if (Convert.ToInt32(hfReadStatus.Value) == 1)
            {
                HtmlAnchor ancMyMessage = (HtmlAnchor)e.Item.FindControl("ancMyMessage");
                ancMyMessage.Style["color"] = "gray";
                //ancMyMessage.CssClass = "readedMessage";
            }
        }
        protected void imgMyRequestDelete_Click(object sender, ImageClickEventArgs e)
        {


            objUserMaster = new clsUserMaster();
            objHomePage = new clsHomePage();
            objComMessage = new clsCommonMessage();

            try
            {
                RepeaterItem rItm = (RepeaterItem)((ImageButton)sender).Parent;

                HiddenField hfMyRequestMyRequest = (HiddenField)rItm.FindControl("hfMyRequestMyRequest");
                HiddenField hfMessageID = (HiddenField)rItm.FindControl("hfMessageID");

                //int MessageTypeId = Convert.ToInt32(hfMyRequestMyRequest.Value);
                //string  MessggeID =Convert.ToString( hfMessageID.Value);

                objComMessage.DeleteMessage(hfMessageID.Value.ToInt64(), objUserMaster.GetEmployeeId());

                rptMyRequests.DataSource = objHomePage.GetMyRequests(objUserMaster.GetEmployeeId(),ddlCompany.SelectedValue.ToInt32());
                rptMyRequests.DataBind();


            }
            catch (Exception)
            {
            }
        }
    #endregion MyRequests

    #region DocumentReturnAlerts

        private void FillDocReturnAlerts(int CompanyID)
        {
           
            DataSet ds = clsHomePage.GetReturnAlert(new clsUserMaster().GetUserId(), "GAR",CompanyID);

            double[] yValues = new double[ds.Tables[1].Rows.Count];
            string[] xValues = new string[ds.Tables[1].Rows.Count];
            double[] zValues = new double[ds.Tables[1].Rows.Count];
            string [] pValues = new string [ds.Tables[1].Rows.Count];

            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                yValues[i] = Convert.ToDouble(ds.Tables[1].Rows[i]["Counts"]);
                xValues[i] =" (" + yValues[i] + ")" + " " + Convert.ToString(ds.Tables[1].Rows[i]["RetAlert"]) ;
                zValues[i] = Convert.ToDouble(ds.Tables[1].Rows[i]["DOCUMENTTYPEID"]);
                pValues[i] = Convert.ToString(ds.Tables[1].Rows[i]["RetAlert"]);
            }

            ChartDocRetAlert.Series["Default"].Points.DataBindXY(xValues, yValues);
            //Set Pie chart type
            ChartDocRetAlert.Series["Default"].ChartType = SeriesChartType.Pie; //SeriesChartType.Pie;
            ChartDocRetAlert.Series["Default"].Color = Color.Green;

            ChartDocRetAlert.Titles[0].Text = " [" + Convert.ToString(ds.Tables[0].Rows[0]["AlCount"]) + "]"  + " " + (clsGlobalization.IsArabicCulture() ? "منبه ارجاع الوثائق و المستندات" : "Document Return Alerts") ;
            ChartDocRetAlert.Titles[0].ForeColor = Color.DarkBlue;
            ChartDocRetAlert.Titles[0].ShadowColor = Color.LightGray;
            ChartDocRetAlert.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);
            // ChartEmployee.Legends[0].Enabled = true;
            ChartDocRetAlert.Series["Default"].IsValueShownAsLabel = true;


            // ChartDocRetAlert.Series["Default"].Points[0].Url = "HomeAlerts.aspx";

            //Alert.FillAlerts();
            //updAlertPopup.Update();
            //ChartDocRetAlert.Series["Default"].Points[0].Url = "#";

            for (int i = 0; i < zValues.Length; i++)
            {
                ChartDocRetAlert.Series["Default"].Points[i].Url = "HomeAlerts.aspx?DocumentTypeID=" + Convert.ToInt32(zValues[i]) + "&IsExpiry=0";
                ChartDocRetAlert.Series["Default"].Points[i].ToolTip =clsGlobalization.IsArabicCulture()? "عرض": "View " + pValues[i];
            }
        }

        private void FillDocExpiryAlerts(int CompanyID)
        
        {
            DataSet ds = clsHomePage.GetExpiryAlert(new clsUserMaster().GetUserId(), "GA",CompanyID);

            double[] yValues = new double[ds.Tables[1].Rows.Count];
            string[] xValues = new string[ds.Tables[1].Rows.Count];
            double[] zValues = new double[ds.Tables[1].Rows.Count];
            string[] pValues = new string[ds.Tables[1].Rows.Count];
            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                yValues[i] = Convert.ToDouble(ds.Tables[1].Rows[i]["Counts"]);
             
                xValues[i] = " (" + yValues[i] + ")" + " " +  Convert.ToString(ds.Tables[1].Rows[i]["RetAlert"]) ;
                zValues[i] = Convert.ToDouble(ds.Tables[1].Rows[i]["DOCUMENTTYPEID"]);
                pValues[i] = Convert.ToString(ds.Tables[1].Rows[i]["RetAlert"]);
            }

            Chart1.Series["Default"].Points.DataBindXY(xValues, yValues);
            //Set Pie chart type
            Chart1.Series["Default"].ChartType = SeriesChartType.Bar ; //SeriesChartType.Pie;
            Chart1.Series["Default"].Color = Color.Orange    ;

            Chart1.Titles[0].Text = " [" + Convert.ToString(ds.Tables[0].Rows[0]["AlCount"]) + "]"  +  " " + (clsGlobalization.IsArabicCulture() ? "منبه انتهاء صلاحية الوثائق" : "Document Expiry Alerts") ;
            Chart1.Titles[0].ForeColor = Color.DarkBlue;
            Chart1.Titles[0].ShadowColor = Color.LightGray;
            Chart1.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);



            Chart1.Series["Default"].IsXValueIndexed = true;
            Chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;

            for (int i = 0; i < zValues.Length; i++)
            {
                Chart1.Series["Default"].Points[i].Url = "HomeAlerts.aspx?DocumentTypeID=" + Convert.ToInt32(zValues[i]) + "&IsExpiry=1";
                Chart1.Series["Default"].Points[i].ToolTip = clsGlobalization.IsArabicCulture() ? "عرض" : "View " + pValues[i];
            }
        }


    #endregion DocumentReturnAlerts

    #region Evaluation
        protected void lnkEvalMessage_Click(object sender, EventArgs e)
        {
            try
            {
                objComMessage = new clsCommonMessage();

                RepeaterItem rItm = (RepeaterItem)((LinkButton)sender).Parent;
                HiddenField hfValues = (HiddenField)rItm.FindControl("hfValues");
                HiddenField hfReferenceId = (HiddenField)rItm.FindControl("hfReferenceId");
                int MessageTypeId = Convert.ToInt32(hfValues.Value);
                objComMessage.MessageType = (eMessageType)Enum.Parse(typeof(eMessageType), hfValues.Value);
                switch (MessageTypeId)
                {
                    case (int)eMessageType.PerformanceEvaluation:
                        Response.Redirect("PerformanceEvaluation.aspx?PerformanceInitiationId=" + hfReferenceId.Value.ToInt32().ToString());
                        break;
                    case (int)eMessageType.PerformanceAction:
                        Response.Redirect("Action.aspx?RequestID=" + hfReferenceId.Value.ToInt32().ToString());
                        break;
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion Evaluation

    protected void imgDetailView_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("HomeDetailView.aspx");
        }

    #region JObTray
        private void FillJobTray(int CompanyID)
        {
            DataTable dt = clsHomePage.FillJob(CompanyID);

            repJob.DataSource = dt;
            repJob.DataBind();

        }

        protected void repJob_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater repSkills = (Repeater)e.Item.FindControl("repSkills");
            HiddenField hfdJobID = (HiddenField)e.Item.FindControl("hfdJobID");
            repSkills.DataSource = clsHomePage.FillJobDetails(hfdJobID.Value.ToInt32());
            repSkills.DataBind();
        }
        #endregion JObTray

    #region JobStatusChart
        private void FillJobStatus(int CompanyID)
        {
            
            DataTable dt = clsHomePage.FillJobStatus(CompanyID);

            string[] xValues = new string[dt.Rows.Count];
            double[] yValues = new double[dt.Rows.Count]; // Vacancies
            double[] y1Values = new double[dt.Rows.Count]; // Applied
            double[] y2Values = new double[dt.Rows.Count]; // Scheduled
            double[] y3Values = new double[dt.Rows.Count]; // Selected
            double[] y4Values = new double[dt.Rows.Count]; // Vacant

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                xValues[i] = Convert.ToString(dt.Rows[i]["Job"]);

                yValues[i] = Convert.ToDouble(dt.Rows[i]["Vacancies"]);
                y1Values[i] = Convert.ToDouble(dt.Rows[i]["Applied"]);
                y2Values[i] = Convert.ToDouble(dt.Rows[i]["Scheduled"]);
                y3Values[i] = Convert.ToDouble(dt.Rows[i]["Selected"]);
                y4Values[i] = Convert.ToDouble(dt.Rows[i]["Vacant"]);

            }

            Series Vacancies = new Series();
            Vacancies.Name =clsGlobalization.IsArabicCulture() ? "المناصب الشاغرة" : "Vacancies";
            //Vacancies.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

            Series Applied = new Series();
            Applied.Name = clsGlobalization.IsArabicCulture() ? "طلبات مستلمة" : "Applied";
            //Applied.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

            Series Scheduled = new Series();
            Scheduled.Name = clsGlobalization.IsArabicCulture() ? "مجدولة" : "Scheduled";
           // Scheduled.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

            Series Selected = new Series();
            Selected.Name = clsGlobalization.IsArabicCulture() ? "مختار" : "Selected";
           // Selected.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

            Series Vacant = new Series();
            Vacant.Name =clsGlobalization.IsArabicCulture() ? "شاغر" : "Vacant";
          //  Vacant.ChartType = SeriesChartType.Bar; //(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

            ChartJob.Series.Add(Vacancies );
            ChartJob.Series.Add(Applied);
            ChartJob.Series.Add(Scheduled );
            ChartJob.Series.Add(Selected);
            ChartJob.Series.Add(Vacant);

            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "المناصب الشاغرة" : "Vacancies"].Points.DataBindXY(xValues, yValues);
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "طلبات مستلمة" : "Applied"].Points.DataBindXY(xValues, y1Values);
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "مجدولة" : "Scheduled"].Points.DataBindXY(xValues, y2Values);
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "مختار" : "Selected"].Points.DataBindXY(xValues, y3Values);
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "شاغر" : "Vacant"].Points.DataBindXY(xValues, y4Values);

            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "المناصب الشاغرة" : "Vacancies"].Color = Color.Gold;
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "طلبات مستلمة" : "Applied"].Color = Color.LightBlue;
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "مجدولة" : "Scheduled"].Color = Color.Plum;
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "مختار" : "Selected"].Color = Color.Green;
            ChartJob.Series[clsGlobalization.IsArabicCulture() ? "شاغر" : "Vacant"].Color = Color.Red;

            ChartJob.Titles[0].Text = clsGlobalization.IsArabicCulture() ? "مهنة" : "Job";
            ChartJob.Titles[0].ForeColor = Color.DarkBlue;
            ChartJob.Titles[0].ShadowColor = Color.LightGray;
            ChartJob.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);
            ChartJob.Legends[0].Enabled = true;
           
            ChartJob.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            ChartJob.Palette = ChartColorPalette.None;
            ChartJob.Legends[0].Font = new Font(new FontFamily("Tahoma"), 8);

        }
    #endregion JobStatusChart
}

public enum eLeaveChart
{
    LateComers=0,
    Leave =1,
    VacationLeave=2,
    VacationProcess=3
    
}
public enum eEmpSearchType
{
    Nationality = 0,
    Country = 1,
    Department = 2,
    Designation = 3

}
public enum eRequestType
{
    LeaveRequest = 1,
    LoanRequest = 2,
    SalaryAdvanceRequest = 3,
    TransferRequest = 4,
    ExpenseRequest = 5,
    DocumentRequest = 7,
    LeaveExtensionRequest = 8,
    ResignationRequest = 9,
    TrainingRequest = 10,
    PerformanceAction = 11,
    JobApproval = 12,
    OfferLetterApproval = 13,
    AttendanceRequest = 14,
    PerformanceEvaluation = 15,
    TicketRequest = 16,
    TravelRequest = 17,
    CompensatoryOffRequest = 18,
    VacationLeaveRequest = 19,
    TimeExtensionRequest = 20,
    VacationExtensionRequest = 21,
    AssetRequest = 22,
    GeneralRequest = 23,
    RejoinRequest = 24
}
