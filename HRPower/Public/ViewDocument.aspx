﻿<%@ Page Language="C#" MasterPageFile="~/Master/DocumentMasterPage.master" AutoEventWireup="true"
    CodeFile="ViewDocument.aspx.cs" Inherits="Public_ViewDocument" Title="View Document"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <script src="../js/Common.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            <div style="width: 99%; height: 50px; margin-top: 10px; margin-bottom: 10px; padding-left:1%; float: left;">
                <ul class="dark_menu">
                    <li><a href="">
                    <%--Menu --%><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:ControlsCommon,Menu%>'></asp:Literal>
                    &nbsp; &#x25Bc</a>
                       
                            <ul>
                                
                                <li><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
                                <li><a href="Visa.aspx"><asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
                                <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
                                <li><a href="Qualification.aspx"><asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
                                <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
                                <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
                                <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
                                <li><a href="InsuranceCard.aspx"><asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
                                <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
                                <li><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
                                <li><a href="Insurance.aspx"><asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
                                <li><a href="Documents.aspx"><asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
                                <li class="selected"><a href="ViewDocument.aspx"><asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
                            </ul>
                        
                    </li>
                </ul>
            </div>
            <div style="width: 100%; float: left;">
                <div style="width: 34%; padding-left: 1%; float: left; background-color: #FFFFFF;">
                    <div style="width: 100%; height: 25px; margin-top: 20px; margin-bottom: 20px; float: left;">
                        <div style="width: 45%; margin-left: 5%; height: 25px; float: left;">
                            <asp:RadioButton ID="rbEmployeeMode" runat="server" GroupName="ViewMode" OnCheckedChanged="rbEmployeeMode_CheckedChanged"
                                meta:resourcekey="EmployeeWise" AutoPostBack="True" />
                        </div>
                        <div style="width: 45%; height: 25px; margin-right: 5%; float: left;">
                            <asp:RadioButton ID="rbCompanyMode" runat="server" GroupName="ViewMode" OnCheckedChanged="rbPolicyMode_CheckedChanged"
                                meta:resourcekey="CompanyWise" AutoPostBack="True" />
                        </div>
                    </div>
                    <div id="divEmployeeMode" runat="server" style="width: 100%; height: 100%">
                        <div style="width: 100%; height: 20px; float: left; padding-left: 2%; padding-bottom: 5%;">
                            <div style="width: 60%; height: 20px; float: left;">
                                <asp:TextBox ID="txtSearch" runat="server" meta:resourcekey="SearchEmployee" Style="width: 100%;"></asp:TextBox>
                            </div>
                            <div style="width: 25%; padding-left: 5%; height: 20px; float: left;">
                                <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" ImageUrl="../images/search.png"
                                    ToolTip="Search" />
                            </div>
                        </div>
                        <div style="width: 100%; float: left; overflow: auto;">
                            <asp:UpdatePanel ID="upEmployeeDocument" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="min-height: 800px; height: auto; display: block;">
                                        <asp:TreeView ID="tvEmployeeDocument" runat="server" NodeStyle-CssClass="treeNode2"
                                            SelectedNodeStyle-CssClass="treeNodeSelected2" ShowLines="True" OnSelectedNodeChanged="tvEmployeeDocument_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div id="divPolicyMode" runat="server" style="width: 100%; height: 100%">
                        <div style="width: 100%; float: left; overflow: auto;">
                            <asp:UpdatePanel ID="upCompanyDocument" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="min-height: 800px; height: auto; display: block;">
                                        <asp:TreeView ID="tvCompanyDocument" runat="server" NodeStyle-CssClass="treeNode2"
                                            SelectedNodeStyle-CssClass="treeNodeSelected2" ShowLines="True" OnSelectedNodeChanged="tvCompanyDocument_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="updivContent" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 60%; margin-left: 2%; margin-right: 1%; float: right">
                            <div id="divContent" runat="server" style="width: 98%; padding-left: 1%; padding-right: 1%;
                                padding-bottom: 5%; float: left; border-radius: 5px;">
                            </div>
                            <div id="divDocumentFiles" runat="server" style="width: 98%; padding-left: 1%; padding-right: 1%;
                                padding-bottom: 5%; margin-top: 20px; float: left; border-radius: 5px;">
                                <asp:DataList ID="dlDoctmentsFiles" runat="server" OnItemCommand="dlDoctmentsFiles_ItemCommand"
                                    DataKeyField="FileName" GridLines="Horizontal" Width="100%" BorderStyle="None">
                                    <FooterStyle BackColor="Black" />
                                    <AlternatingItemStyle BackColor="White" BorderStyle="None" />
                                    <ItemStyle BackColor="#E8EEF0" BorderStyle="None" />
                                    <HeaderTemplate>
                                        <div style="width: 100%; height: 20px; padding-top: 20px; font-size: medium; font-weight: bold;
                                            text-align: center; color: #000000;">
                                           <%-- Attached Documents--%><asp:Literal ID="Literal1" runat ="server"  meta:resourcekey="AttachedDocuments"  ></asp:Literal>
                                            </div>
                                        <div style="float: left; width: 100%;">
                                            <div style="float: left; width: 80%; font-weight: bold; color: #000000;" class="trLeft">
                                                <%--Document Name--%><asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:DocumentsCommon,DocumentName%>'  ></asp:Literal>
                                                                    </td>
                                            </div>
                                            <div style="float: right; width: 20%;" class="trRight">
                                            </div>
                                    </HeaderTemplate>
                                    <HeaderStyle BorderStyle="None" />
                                    <ItemTemplate>
                                        <div style="float: left; width: 100%;">
                                            <div style="float: left; width: 68%; padding-left: 2%;" class="trLeft">
                                                <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                    Width="150px"></asp:Label>
                                            </div>
                                            <div style="float: left; width: 5%;" class="trLeft">
                                                <a href='<%#GetViewLink(Eval("FileName")) %>' target="_blank" title="View">
                                                    <asp:Image ID="imgThumbnail" runat="server" Height="20px" Width="20px" ImageUrl='<%#GetViewLink(Eval("FileName"))%>'
                                                        onerror="imgNotImage(this);" />
                                                </a>
                                            </div>
                                            <div style="float: left; width: 5%;" class="trLeft">
                                                <a href='<%#GetViewLink(Eval("FileName")) %>' download='<%#GetViewLink(Eval("FileName")) %>'
                                                    title="Download">
                                                    <%--Download--%><asp:Literal ID="Literal12" runat ="server" Text ='<%$Resources:ControlsCommon,Download%>'  ></asp:Literal>
                                                     </a>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
