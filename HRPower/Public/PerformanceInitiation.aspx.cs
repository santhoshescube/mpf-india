﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;

public partial class Public_PerformanceInitiation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PerformanceInitiation.Title").ToString();
        hfIsArabic.Value = clsGlobalization.IsArabicCulture().ToString();
        if (!IsPostBack)
        {
            FillAllCompanies();
            FillAllDepartments();
            Session["ResultList"] = null;
            dvNoRecord.Visible = true;

            if (Request.QueryString["PerformanceInitiationID"] != null)
            {
                hfPerformanceInitiationID.Value = Request.QueryString["PerformanceInitiationID"].ToString ();
                GetPerformanceInitiationDetails();
            }
            SetPermission();
        }

        EnableDisableMenus();
        AssignEvaluators1.eResult -= new Controls_AssignEvaluators.Result(AssignEvaluators1_eResult);
        AssignEvaluators1.eResult += new Controls_AssignEvaluators.Result(AssignEvaluators1_eResult);

       
    }


    private void EnableDisableMenus()
    {
        if (ViewState["IsCreate"] != null && Convert.ToBoolean(ViewState["IsCreate"]) == true)
        {
           dvRecord.Style["display"]="block";
           dvNoPermission.Style["display"]="none";
        }
        else
        {
            dvRecord.Style["display"] = "none";
            dvNoPermission.Style["display"]="block";
        }

        lnkAdd.Enabled = btnSave.Enabled = Convert.ToBoolean(ViewState["IsCreate"]);
        lnkViewInitiation.Enabled = Convert.ToBoolean(ViewState["IsView"]);

        updEmployees.Update();
    }


    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerfomanceInitiation);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }




    private void GetPerformanceInitiationDetails()
    {
        DataTable dtPerformanceInitiationMainDetails = clsPerformanceInitiation.GetPerformanceInitiationMainDetails(hfPerformanceInitiationID.Value.ToInt32());

        if (dtPerformanceInitiationMainDetails.Rows.Count > 0)
        {
            ddlCompany.SelectedValue = dtPerformanceInitiationMainDetails.Rows[0]["CompanyID"].ToString();
            ddlDepartment.SelectedValue = dtPerformanceInitiationMainDetails.Rows[0]["DepartmentID"].ToString();
            txtFromDate.Text = dtPerformanceInitiationMainDetails.Rows[0]["FromDate"].ToString();
            txtToDate.Text = dtPerformanceInitiationMainDetails.Rows[0]["ToDate"].ToString();
            Session["ResultList"] = clsPerformanceInitiation.GetPerformanceInitationDetails(hfPerformanceInitiationID.Value.ToInt32());
            updEmployees.Update();

            FillEmployeeGrid();
        }
    }

    void AssignEvaluators1_eResult(object ResultList)
    {
        if (ResultList != null )
        {
            Session ["ResultList"] = ResultList;
           // ViewState["ResultDt"] = ResultDt;
            FillEmployeeGrid();
        }

        //mdEvaluators.Hide();
        //mdEvaluators.Dispose();
        updEmployees.Update(); 
        updEvaluators.Update(); 
    }

    public void FillAllCompanies()
    {
        clsUserMaster objuser = new clsUserMaster();
        clsPerformanceInitiation objPerformanceInitiation = new clsPerformanceInitiation();
        clsPerformanceInitiation.intCompanyID = objuser.GetCompanyId();
        ddlCompany.DataSource = clsPerformanceInitiation.GetAllCompanies();
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();


        ddlCompany.SelectedValue = new clsUserMaster().GetCompanyId().ToString();
        ddlCompany.Enabled = false;
    }


    public void FillAllDepartments()
    {
        ddlDepartment.DataSource = clsPerformanceInitiation.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();
    }

  
    private void FillEmployeeGrid()
    {
        StringBuilder EvaluatorName;
        DataTable dtEmployeeList = new DataTable();
        DataRow drEmployeeList = null;
        int RowIndex = 0;

        dtEmployeeList.Columns.Add("SLNo");
        dtEmployeeList.Columns.Add("TemplateName");
        dtEmployeeList.Columns.Add("EmployeeID");
        dtEmployeeList.Columns.Add("EmployeeFullName");
        dtEmployeeList.Columns.Add("EvaluatorFullName");
        EmployeeInitiation c = ((EmployeeInitiation)Session["ResultList"]);


        foreach (InitiationEmployees employee in c.InitiationResult)
        {
            RowIndex = RowIndex + 1;

            drEmployeeList = dtEmployeeList.NewRow();
            drEmployeeList["SLNo"] = RowIndex.ToString();
            drEmployeeList["TemplateName"] = employee.TemplateName;
            drEmployeeList["EmployeeID"] = employee.EmployeeID;
            drEmployeeList["EmployeeFullName"] = employee.EmployeeName;

            EvaluatorName = new StringBuilder();
            foreach (Evaluators evaluator in employee.InitiationEvaluators)
            {
                EvaluatorName.Append(evaluator.EvaluatorName);
                EvaluatorName.Append(",");

            }
            drEmployeeList["EvaluatorFullName"] = EvaluatorName.Remove(EvaluatorName.Length - 1, 1);
            dtEmployeeList.Rows.Add(drEmployeeList);
        }


        dlEmployeeList.DataSource = dtEmployeeList.Rows.Count == 0 ? null : dtEmployeeList;
        dlEmployeeList.DataBind();

        dvNoRecord.Visible = dtEmployeeList.Rows.Count ==0;
        updEmployees.Update();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Validate())
        {

            if (Session["ResultList"] != null)
            {
                EmployeeInitiation employeeList = ((EmployeeInitiation)Session["ResultList"]);
                if (employeeList == null || employeeList.InitiationResult.Count == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PleaseAddAnyEmployee").ToString() + "');", true);
                    return;
                }
                int PerformanceInitiationID = new clsPerformanceDTO()
                  {
                      PerformanceInitiationID = Request.QueryString["PerformanceInitiationID"] != null ? Request.QueryString["PerformanceInitiationID"].ToInt32() : 0,
                      CompanyID = ddlCompany.SelectedValue.ToInt32(),
                      DepartmentID = ddlDepartment.SelectedValue.ToInt32(),
                      FromDate = txtFromDate.Text.Trim().ToDateTime(),
                      ToDate = txtToDate.Text.Trim().ToDateTime(),
                      EmployeeList = ((EmployeeInitiation)Session["ResultList"])
                  }.SavePerformanceInitiation();

                if (PerformanceInitiationID > 0)
                {

                  

                   // ((System.Web.HttpApplication)(sender)).Request.CurrentExecutionFilePath
                    //Insert message

                   // clsCommonMessage.SendMessage(new clsUserMaster().GetEmployeeId(), PerformanceInitiationID, eReferenceTypes.PerformanceEvaluation, eMessageTypes.Performance__Evaluation, eAction.Applied, "");
                    //clsCommonMail.SendMail(PerformanceInitiationID, MailRequestType.PerformanceInitiation, eAction.Applied);    


                    clsCommonMessage.SendMessage(new clsUserMaster().GetEmployeeId(), PerformanceInitiationID, eReferenceTypes.PerformanceEvaluation, eMessageTypes.Performance__Evaluation, eAction.Applied, "");
                    clsCommonMail.SendMail(PerformanceInitiationID, MailRequestType.PerformanceInitiation, eAction.Applied);    



//                    ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('Performance initiation saved successfully');", true);
                    ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PerformanceInitiationSavedSuccessFully").ToString() + "');", true);
                    Clear();
                }
                else
                {
                    //ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('Failed to save performance initiation.Try again');", true);
                    ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("FailedToSavePerformanceInitiation").ToString() + "');", true);

                }

            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('Please add any employee');", true);
                ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('" + GetLocalResourceObject("PleaseAddAnyEmployee").ToString() + "');", true);

                return;


            }
        }


    }


    private void Clear()
    {
        Session["ResultList"] = null;
        txtFromDate.Text = "";
        txtToDate.Text = "";
        ddlCompany.SelectedIndex = 0;
        ddlDepartment.SelectedIndex = 0;
        dlEmployeeList.DataSource = null;
        dlEmployeeList.DataBind();
    }

    private void ClearEmployeesAndEvaluators()
    {
        Session["ResultList"] = null;
        dlEmployeeList.DataSource = null;
        dlEmployeeList.DataBind();
        upd.Update();
    }
  
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PerformanceInitiation.aspx");
    }


    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        int EmployeeID = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument.ToInt32();
        EmployeeInitiation employee = ((EmployeeInitiation)Session["ResultList"]);

        employee.InitiationResult.Remove(employee.InitiationResult.Find(t => t.EmployeeID == EmployeeID));
        Session["ResultList"] = employee;
        FillEmployeeGrid();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/PerformanceInitiation.aspx");
    }
    protected void lnkViewInitiation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/ViewPerformanceInitiation.aspx");
    }
   


    protected void Clear(object sender, EventArgs e)
    {
        ClearEmployeesAndEvaluators();
    }
    protected void imgAddEmployee_Click(object sender, ImageClickEventArgs e)
    {
        if (Validate())
        {
            AssignEvaluators1.CompanyID = ddlCompany.SelectedValue.ToInt32();

            if (Session["ResultList"] != null)
                AssignEvaluators1.AlreadyAssignedEmployees = ((EmployeeInitiation)Session["ResultList"]);
            else
                AssignEvaluators1.AlreadyAssignedEmployees = null;

            AssignEvaluators1.LoadEmployeeAndEvaluators(ddlCompany.SelectedValue.ToInt32(), hfPerformanceInitiationID.Value.ToInt32(), ddlDepartment.SelectedValue.ToInt32(), txtFromDate.Text.ToDateTime(), txtToDate.Text.ToDateTime());
            mdEvaluators.Show();
            updEvaluators.Update();
        }

    }




    public bool Validate()
    {
        DateTime dtDate;
        if (txtFromDate.Text == "")
        {
            //ShowMessage("Please enter from date");
            ShowMessage(GetLocalResourceObject("PleaseEnterFromDate.Text").ToString());
            return false;
        }
        else if (!(DateTime.TryParse(txtFromDate.Text, out dtDate)))
        {
           // ShowMessage("Invalid from date");
            ShowMessage(GetLocalResourceObject("InvalidFromDate.Text").ToString());
            return false;
        }

        else if (txtToDate.Text != "" && (!(DateTime.TryParse(txtToDate.Text, out dtDate))))
        {
            //ShowMessage("Invalid to date");
            ShowMessage(GetLocalResourceObject("InvalidToDate.Text").ToString());
            return false;
        }

        else if (txtFromDate.Text != "" && txtToDate.Text != "" && txtToDate.Text.ToDateTime().Date < txtFromDate.Text.ToDateTime().Date)
        {
            //ShowMessage("To date must be greater than from date");

            ShowMessage(GetLocalResourceObject("ToDateMustBeGreaterThanFromDate.Text").ToString());	

            return false;
        }
        return true;
    }



    public void ShowMessage(string Message)
    {
        ScriptManager.RegisterClientScriptBlock(updEmployees, updEmployees.GetType(), new Guid().ToString(), "alert('" + Message + "');", true);
    }
}