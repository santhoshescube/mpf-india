﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="HolidayCalendar.aspx.cs" Inherits="Public_HolidayCalendar" Title="HolidayCalendar" %>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li class="selected"><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            <%-- <li><a href="Projects.aspx">
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:SettingsCommon,Projects%>'></asp:Literal></a></li>--%>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table runat="server" id="Calender" style="width: 100%">
        <%-- <tr>
        onmouseover="showEdit(this);" onmouseout="hideEdit(this);"
            <td valign="top" style="padding: 0px; width: 10px;">
                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                    display: block;" CommandName="_Entry" SkinID="hoverPlus" CommandArgument='<%# Eval("FullDate") %>'
                    ToolTip="Add Time Entry" />
            </td>
        </tr>--%>
        <tr>
            <td align="right">
                Select Company
            </td>
            <td align="right">
                <asp:UpdatePanel ID="upnlCompany" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlSelectCompany" runat="server" CssClass="dropdownlist" DataTextField="Name"
                            DataValueField="CompanyId" AutoPostBack="true" 
                            OnSelectedIndexChanged="ddlSelectCompany_SelectedIndexChanged" Width="255px" Enabled="false">
                        </asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Calendar1" EventName="SelectionChanged" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" OnPreRender="UpdatePanel2_PreRender"
                    UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Calendar ID="Calendar1" runat="server" CellPadding="1" Height="305px" Width="672px"
                            OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged"
                            BorderColor="Black" BorderWidth="1px" NextMonthText=">>" PrevMonthText="<<" TitleStyle-Font-Bold="true"
                            OnVisibleMonthChanged="Calendar1_VisibleMonthChanged" OnPreRender="Calendar1_PreRender">
                            <SelectedDayStyle ForeColor="Black" />
                            <DayStyle HorizontalAlign="left" VerticalAlign="Top" />
                            <NextPrevStyle CssClass="CalendarHeader" />
                            <DayHeaderStyle CssClass="datalistheader" />
                            <TitleStyle CssClass="CalendarHeader" />
                        </asp:Calendar>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlSelectCompany" EventName="SelectedIndexChanged" />
                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td align="left">
                            <div style="display: none;">
                                <input type="button" runat="server" id="btn" />
                            </div>
                            <AjaxControlToolkit:ModalPopupExtender ID="mpeHoliday" runat="server" BackgroundCssClass="modalBackground"
                                TargetControlID="btn" PopupControlID="pnlHoliday" CancelControlID="ibtnClose"
                                BehaviorID="mpeBHoliday" Drag="true" PopupDragHandleControlID="divDrag">
                            </AjaxControlToolkit:ModalPopupExtender>
                            <asp:Panel ID="pnlHoliday" runat="server" Style="display: block">
                                <%--   <table width="560" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr>
                                        <td width="20" align="left" valign="top" class="tdTopLeft">
                                        </td>
                                        <td width="520" align="left" valign="middle" class="tdTopCenter">
                                            <div id="divDrag" runat="server">
                                                <div id="header11">
                                                    <div id="hbox1">
                                                        <div id="headername">
                                                            <asp:Label ID="Label1" runat="server" Text="Holiday/Event Entries"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="hbox2">
                                                        <div id="close1">
                                                            <a href="">
                                                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                                                    CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </td>
                                        <td width="20" class="tdTopRight">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" width="20" class="tdMiddleLeft">
                                            &nbsp;
                                        </td>
                                        <td height="150" width="520" style="background-color: White;" valign="top" align="left">
                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td class="trLeft" width="20%">
                                                                &nbsp;
                                                            </td>
                                                            <td class="trRight" width="80%" style="padding-left: 5px">
                                                                <asp:RadioButton ID="rdbEvents" runat="server" GroupName="Add" Text="Events" AutoPostBack="true"
                                                                    OnCheckedChanged="rdbEvents_CheckedChanged" />
                                                                <asp:RadioButton ID="rdbHoliday" runat="server" GroupName="Add" AutoPostBack="true"
                                                                    Text="Holiday" OnCheckedChanged="rdbHoliday_CheckedChanged" />
                                                                <asp:ImageButton ID="imgAddNew" style="padding-left:20px;" runat="server" CausesValidation="False" OnClick="imgAddNew_Click"
                                                                   ImageUrl="~/images/Plus.png" />
                                                            </td>
                                                        </tr>
                                                        <tr id="trHolidayType" runat="server">
                                                            <td class="trLeft" width="20%">
                                                                Holiday Type
                                                            </td>
                                                            <td class="trRight" width="80%" valign="top">
                                                              
                                                                <asp:DropDownList ID="rcHolidayType" ValidationGroup="Submit" runat="server" CssClass="dropdownlist_mandatory"
                                                                    Width="200px" AutoPostBack="False">
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hfId" runat="server" Value="0" />
                                                                <asp:ImageButton ID="imgHolidayType" runat="server" Visible="false" CausesValidation="False"
                                                                    CommandName="Accounthead" CssClass="imagebutton" SkinID="Reference" OnClick="imgHolidayType_Click" />
                                                                <asp:RequiredFieldValidator ID="rfvHolidayType" runat="server" ControlToValidate="rcHolidayType"
                                                                    CssClass="error" Display="Dynamic" ErrorMessage="Please select holiday type"
                                                                    ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr id="trEventTitle" runat="server">
                                                            <td class="trLeft" width="20%">
                                                                Title
                                                            </td>
                                                            <td class="trRight" width="80%">
                                                                <asp:TextBox ID="txtTitle" runat="server" MaxLength="100" CssClass="textbox_mandatory"
                                                                    Width="240px"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                                                    Display="Dynamic" ErrorMessage="Please enter title." SetFocusOnError="True" CssClass="error"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trLeft" width="20%">
                                                                Description
                                                            </td>
                                                            <td class="trRight" width="80%">
                                                                <asp:TextBox ID="txtDescription" runat="server" MaxLength="200" Height="41px" Width="240px"
                                                                    TextMode="MultiLine"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDescription"
                                                                    Display="Dynamic" ErrorMessage="Please enter description." CssClass="error"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="trLeft" width="20%">
                                                                <asp:Label ID="lblDate" runat="server" Visible="false"></asp:Label>
                                                            </td>
                                                            <td class="trRight" width="80%">
                                                                <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text="Submit" ValidationGroup="Submit"
                                                                    OnClick="btnSubmit_Click" />
                                                                <asp:Label ID="lbMsg" runat="server" CssClass="error"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="container" colspan="2">
                                                                <div id="divListHead" class="container_head" runat="server" visible="false">
                                                                    <table width="100%" style="font-size: 11px;">
                                                                        <tr>
                                                                            <td id="tdEvent" runat="server" width="150px">
                                                                            </td>
                                                                            <td width="300px">
                                                                                Description
                                                                            </td>
                                                                            <td width="20px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                                <div style ="max-height :200px; overflow:auto  " >
                                                                    <asp:DataList ID="dlHoliday" runat="server" Style="background-color: White; width: 500px"
                                                                        OnSelectedIndexChanged="dlHoliday_SelectedIndexChanged">
                                                                        <ItemTemplate>
                                                                            <table style="font-size: 11px;" width="100%">
                                                                                <tr>
                                                                                 
                                                                                    <td width="150px">
                                                                                        <asp:LinkButton ValidationGroup="test" ID="lnkEvent" runat="server" Text='<%# Eval("Name") %>'
                                                                                            CommandArgument='<%# Eval("ID") %>' OnClick="lnkEvent_Click"></asp:LinkButton>
                                                                                    </td>
                                                                                    <td >
                                                                                        <%#clsCommon.Crop(Convert.ToString(Eval("Description")),50)%>
                                                                                    </td>
                                                                                    <td width="20px">
                                                                                        <asp:ImageButton ValidationGroup="test" ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon_popup.png"
                                                                                            CommandArgument='<%# Eval("ID") %>' OnClick="imgDelete_Click" OnClientClick="return confirm('Do you wish to delete this information?');" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ItemTemplate>
                                                                        <ItemStyle CssClass="item" />
                                                                    </asp:DataList>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="Calendar1" EventName="SelectionChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="ddlSelectCompany" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td align="left" valign="top" width="20" class="tdMiddleRight">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20" height="20" align="right" valign="top" class="tdBottomLeft">
                                            &nbsp;
                                        </td>
                                        <td height="20" width="520" align="left" valign="top" class="tdBottomCenter">
                                        </td>
                                        <td width="20" height="20" align="left" valign="top" class="tdBottomRight">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>--%>
                                <div id="popupmainwrap" style="width: 500px">
                                    <div id="popupmain" style="width: 100%">
                                        <div id="header11">
                                            <div id="hbox1">
                                                <div id="headername">
                                                    <asp:Label ID="Label1" runat="server" Text="Holiday/Event Entries"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="hbox2">
                                                <div id="close1">
                                                    <a href="">
                                                        <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                                            CausesValidation="true" ValidationGroup="dummy_not_using" ToolTip="Close" /></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="toolbar">
                                            <div id="imgbuttons">
                                             <asp:UpdatePanel ID="upToolbar" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <asp:ImageButton ID="imgAddNew" runat="server" CausesValidation="False" OnClick="imgAddNew_Click"
                                                    ImageUrl="~/images/Plus.png" />
                                                <asp:ImageButton ID="btnSaveData" runat="server" ToolTip="Save Data" OnClick="btnSaveData_Click"
                                                    SkinID="PopupSave" ImageUrl="~/images/Save Blue.png" ValidationGroup="Submit" />
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div id="contentwrap">
                                            <div id="content">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="trLeft" width="20%">
                                                                    &nbsp;
                                                                </td>
                                                                <td class="trRight" width="80%" style="padding-left: 5px">
                                                                    <asp:RadioButton ID="rdbEvents" runat="server" GroupName="Add" Text="Events" AutoPostBack="true"
                                                                        OnCheckedChanged="rdbEvents_CheckedChanged" />
                                                                    <asp:RadioButton ID="rdbHoliday" runat="server" GroupName="Add" AutoPostBack="true"
                                                                        Text="Holiday" OnCheckedChanged="rdbHoliday_CheckedChanged" />
                                                                </td>
                                                            </tr>
                                                            <tr id="trHolidayType" runat="server">
                                                                <td class="trLeft" width="20%">
                                                                    Holiday Type
                                                                </td>
                                                                <td class="trRight" width="80%" valign="top">
                                                                    <asp:DropDownList ID="rcHolidayType" ValidationGroup="Submit" runat="server" CssClass="dropdownlist_mandatory"
                                                                        Width="200px" AutoPostBack="False">
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField ID="hfId" runat="server" Value="0" />
                                                                    <asp:ImageButton ID="imgHolidayType" runat="server" Visible="false" CausesValidation="False"
                                                                        CommandName="Accounthead" CssClass="imagebutton" SkinID="Reference" OnClick="imgHolidayType_Click" />
                                                                    <asp:RequiredFieldValidator ID="rfvHolidayType" runat="server" ControlToValidate="rcHolidayType"
                                                                        CssClass="error" Display="Dynamic" ErrorMessage="Please select holiday type"
                                                                        ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr id="trEventTitle" runat="server">
                                                                <td class="trLeft" width="20%">
                                                                    Title
                                                                </td>
                                                                <td class="trRight" width="80%">
                                                                    <asp:TextBox ID="txtTitle" runat="server" MaxLength="100" CssClass="textbox_mandatory"
                                                                        Width="240px"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                                                                        Display="Dynamic" ErrorMessage="Please enter title." SetFocusOnError="True" CssClass="error"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="trLeft" width="20%">
                                                                    Description
                                                                </td>
                                                                <td class="trRight" width="80%">
                                                                    <asp:TextBox ID="txtDescription" runat="server" MaxLength="200" Height="41px" Width="240px"
                                                                        TextMode="MultiLine"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtDescription"
                                                                        Display="Dynamic" ErrorMessage="Please enter description." CssClass="error"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="trLeft" width="20%">
                                                                    <asp:Label ID="lblDate" runat="server" Visible="false"></asp:Label>
                                                                </td>
                                                                <td class="trRight" width="80%">
                                                                    <asp:Label ID="lbMsg" runat="server" CssClass="error"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="container" colspan="2">
                                                                    <div id="divListHead" class="container_head" runat="server" visible="false">
                                                                        <table width="100%" style="font-size: 11px;">
                                                                            <tr>
                                                                                <td id="tdEvent" runat="server" width="150px">
                                                                                </td>
                                                                                <td width="300px">
                                                                                    Description
                                                                                </td>
                                                                                <td width="20px">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <div style="max-height: 200px; overflow: auto">
                                                                        <asp:DataList ID="dlHoliday" runat="server" Style="background-color: White; width: 100%"
                                                                            OnSelectedIndexChanged="dlHoliday_SelectedIndexChanged">
                                                                            <ItemTemplate>
                                                                                <table style="font-size: 11px;" width="100%">
                                                                                    <tr>
                                                                                        <td width="150px">
                                                                                            <asp:LinkButton ValidationGroup="test" ID="lnkEvent" runat="server" Text='<%# Eval("Name") %>'
                                                                                                CommandArgument='<%# Eval("ID") %>' OnClick="lnkEvent_Click"></asp:LinkButton>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%#clsCommon.Crop(Convert.ToString(Eval("Description")),50)%>
                                                                                        </td>
                                                                                        <td width="20px">
                                                                                            <asp:ImageButton ValidationGroup="test" ID="imgDelete" runat="server" ImageUrl="~/images/delete_icon_popup.png"
                                                                                                CommandArgument='<%# Eval("ID") %>' OnClick="imgDelete_Click" OnClientClick="return confirm('Do you wish to delete this information?');" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                            <ItemStyle CssClass="item" />
                                                                        </asp:DataList>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Calendar1" EventName="SelectionChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddlSelectCompany" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnSaveData" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                        <div id="footerwrapper">
                                            <div id="footer11">
                                                <div id="buttons">
                                                    <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text="Submit" ValidationGroup="Submit"
                                                        OnClick="btnSubmit_Click" Width="60px" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="display: none">
                                        <asp:Button ID="Button1" runat="server" />
                                    </div>
                                    <asp:Panel ID="pnlModalPopUp" Style="display: none" runat="server">
                                        <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
                                    </asp:Panel>
                                    <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                                        PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                                    </AjaxControlToolkit:ModalPopupExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: none;"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <table>
        <tr>
            <td align="left" valign="top" style="padding: 3px">
                <img src="../images/Bulbe 14px.png" />
            </td>
            <td style="text-align: justify">
                <span class="labeltext">A calendar with editable details of permitted holidays, normal
                    working days and weekly off days, as well as all events or special dates, as applicable
                    to company or organization.</span>
            </td>
        </tr>
    </table>
</asp:Content>
