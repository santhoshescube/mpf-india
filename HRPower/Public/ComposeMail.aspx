﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ComposeMail.aspx.cs" Inherits="public_ComposeMail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <base target="_self" />
    <title>Compose Mail</title>
    <link href="../css/Controls.css" rel="stylesheet" type="text/css" />
  
    <script src="../js/Common.js" type="text/javascript"></script>

    <script src="../js/HRPower.js" type="text/javascript"></script>

    <link href="../css/style_common.css" rel="stylesheet" type="text/css" />
    <link href="../css/default_common.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <div>
        <form id="form1" runat="server" enctype="multipart/form-data" style="margin: 0; padding: 0;">
        <div id="forms">
          <div  style="height: 650; width: 700px;">
            <div style="display: none">
                <AjaxControlToolkit:ToolkitScriptManager ID="ajxManager" runat="server">
                </AjaxControlToolkit:ToolkitScriptManager>
            </div>
            <div style="max-height: 100; overflow: auto;" class="labeltext">
                <div>
                    <asp:Panel ID="pnlRecipients" runat="server"    CssClass="item_title"
                        Style="padding: 3px;">
                        <div style="height: 65px; overflow: auto; background-color: #ffffff">
                            <asp:CheckBoxList ID="chkRecipients" runat="server" OnDataBound="chkRecipients_DataBound"
                                RepeatColumns="2" CssClass="labeltext">
                            </asp:CheckBoxList>
                        </div>
                    </asp:Panel>
                </div>
                <div>
                    <asp:Panel ID="pnlTo" runat="server"  CssClass="item_title">
                        <asp:TextBox ID="txtTo" runat="server" CssClass="textbox" TextMode="MultiLine" Width="600px"
                            Height="40px"></asp:TextBox>
                        <br />
                        <asp:CustomValidator ID="cvTo" runat="server" ClientValidationFunction="valRecipients"
                            CssClass="error" Display="Dynamic" ValidationGroup="Send"></asp:CustomValidator>
                    </asp:Panel>
                </div>
                <div style="padding-top: 5px">
                    <div style="width: 15%; float: left;padding-left:5px">
                         <asp:Literal ID="Literal30" runat ="server" meta:resourcekey="Subject"></asp:Literal>    
                    </div>
                    <div style="width: 75%; float: left">
                        <asp:TextBox ID="txtSubject" runat="server" CssClass="textbox" Width="400px"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div style="padding-top: 10px" >
                    <div style="width: 140px; float: left;padding-top:5px; padding-left:5px">
                         <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="Files"></asp:Literal>    
                    </div>
                    <div style="width: 500px; float: left;" id="divFiles">
                        <div>
                            <asp:FileUpload ID="filAttachment" runat="server" /> &nbsp;<a href="#" class="linkbutton" onclick="addAttachment();">Add File</a></div>
                       
                    </div>
                </div>
               <br />
                <div>
                    <div style="float:right">
                        <asp:Button ID="imgAttach" runat="server" OnClick="imgAttach_Click" ValidationGroup="Attach"
                            CssClass="referencebutton" Text='<%$Resources:ControlsCommon,Attach%>' />
                    </div>
                    <div>
                        <asp:CustomValidator ID="cvAttachment" runat="server" CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                            ValidationGroup="Attach" ClientValidationFunction="valAttachment"></asp:CustomValidator>
                    </div>
                </div>
                <div style="max-height: 50px; overflow: auto; width: 640px">
                    <asp:UpdatePanel ID="pnlFiles" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:DataList ID="dlFiles" runat="server" OnItemCommand="dlFiles_ItemCommand" RepeatColumns="3"
                                RepeatDirection="Horizontal" BorderWidth="1px" CellPadding="0" CellSpacing="0"
                                BorderColor="Silver" BorderStyle="Solid" GridLines="Vertical" CssClass="labeltext">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <%# Eval("FileName") %>
                                            </td>
                                            <td width="20">
                                                <asp:UpdatePanel ID="pnlDelete" runat="server" RenderMode="Inline" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:ImageButton ID="btnDeleteFile" runat="server" CommandArgument='<%# Eval("FilePath") %>'
                                                            CommandName="REMOVE" ImageUrl="~/images/delete_icon.png" OnClientClick="return confirm('Are you sure to remove this attachment?');"
                                                            CssClass="imagebutton" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <ItemStyle BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
                            </asp:DataList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlFiles" EventName="ItemCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div style="padding-top :5px">
                <AjaxControlToolkit:Editor ID="txtBody" runat="server" Width="700px" Height="250px" />
            </div>
            <div style="float: right;padding-top:5px; padding-right:10px">
                <asp:UpdatePanel ID="pnlSend" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button ID="btnSend" runat="server" Text='<%$Resources:ControlsCommon,Send%>'  CssClass="btnsubmit" Width="75px"
                            ValidationGroup="Send" OnClick="btnSend_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        
        </div>
        </div>
        </form>
    </div>
</body>
</html>
