﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="VacancyView.aspx.cs" Inherits="Public_VacancyView" Title="Job Openings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li class='selected'><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="updJobView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divView" style="display: block; width: 100%" class="labeltext" runat="server">
                <table cellpadding="5" cellspacing="0" border="0" width="90%">
                    <tr>
                        <td valign="top" align="left">
                            <asp:DataList ID="dlJobDetails" runat="server" Width="100%">
                                <ItemStyle CssClass="item" />
                                <HeaderStyle CssClass="listItem" />
                                <HeaderTemplate>
                                    <table width="95%" border="0" cellpadding="3" class="datalistheader">
                                        <tr>
                                            <td valign="top" width="100%" style="padding-left: 5px;">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="JobInfo"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="padding-left: 20px;">
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Company"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("CompanyName") %>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Department"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Department") %>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Designation"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Designation")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="JobCode"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Jobcode") %>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="JobTitle"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("JobTitle") %>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="JobDescription"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("Remarks") %>
                                            </div>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="InterviewType"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%--<%#Convert.ToBoolean(Eval("IsBoardInterview")) == true ? "Board" : "Multi-Level"%>--%>
                                            <%# Eval("IsBoardInterview")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="JobValidityDate"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("ExpiryDate") %>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Isthepositionbudgeted"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("IsBudgetedShow") %>
                                        </div>
                                        <div id="divBudgeted" runat="server">
                                            <div class="trleft" style="float: left; width: 27%; height: 29px">
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="BudgetedAmount"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: 29px">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: 29px">
                                                <%# Eval("BudgetedAmount") %>
                                            </div>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="RequiredQuota"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("RequiredQuota") %>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="EmploymentType"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("EmploymentType") %>
                                        </div>
                                        <%--///--%>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Qualification"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("Degree") %>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="WeeklyWorkingDays"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("WeeklyWorkingDays")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="DailyWorkingHours"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("DailyWorkingHour")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="AccomodationAvailability"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%--<%# Convert.ToBoolean(Eval("IsAccomodation"))==true?"Yes":"No"%>--%>
                                                <%# Eval("IsAccomodation")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal18" runat="server" meta:resourcekey="TransportationAvailability"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%--<%# Convert.ToBoolean(Eval("IsTransportation")) == true ? "Yes":"No"%>--%>
                                                <%# Eval("IsTransportation")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="LegalTerms"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("LegalTerms")%>
                                            </div>
                                        </div>
                                        <%--///--%>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Skills"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("Skills") %>
                                            </div>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Experience"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("Experience")%>
                                        </div>
                                        <div style="float: left; width: 100%; height: auto;">
                                            <div class="trleft" style="float: left; width: 27%; height: auto">
                                                <asp:Literal ID="Literal22" runat="server" meta:resourcekey="PlaceofWork"></asp:Literal>
                                            </div>
                                            <div class="trleft" style="float: left; width: 10%; height: auto">
                                                :
                                            </div>
                                            <div class="trright" style="float: left; width: 60%; height: auto; overflow: auto;
                                                word-break: break-all;">
                                                <%# Eval("PlaceofWork")%>
                                            </div>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal23" runat="server" meta:resourcekey="ContractPeriod"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%# Eval("ContractPeriod")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal24" runat="server" meta:resourcekey="JobStatus"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%#Eval("JobStatus")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal25" runat="server" meta:resourcekey="LanguageCapabilities"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 60px; overflow-y: scroll;
                                            overflow-x: hidden; word-break: break-all;">
                                            <%#Eval("Languages")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal26" runat="server" meta:resourcekey="Age"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px;">
                                            <%#Eval("Age")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Gender"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px;">
                                            <%#Eval("Gender")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal28" runat="server" meta:resourcekey="LocalOverseasRecruitment"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px;">
                                            <%#Eval("RecruitmentType")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal29" runat="server" meta:resourcekey="FeesPayable"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px;">
                                            <%#Eval("RecruitmentFeesPayableShow")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal30" runat="server" meta:resourcekey="JoiningDate"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px;">
                                            <%#Eval("JoiningDate")%>
                                        </div>
                                        <div class="trleft" style="float: left; width: 27%; height: 29px">
                                            <asp:Literal ID="Literal31" runat="server" meta:resourcekey="OfferApproval"></asp:Literal>
                                        </div>
                                        <div class="trleft" style="float: left; width: 10%; height: 29px">
                                            :
                                        </div>
                                        <div class="trright" style="float: left; width: 60%; height: 29px">
                                            <%-- <%# Convert.ToBoolean(Eval("IsOfferApproval")) == true ? "Yes":"No"%>--%>
                                            <%# Eval("IsOfferApproval")%>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <table id="Table1" width="95%" border="0" cellpadding="2" runat="server">
                                <tr>
                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold">
                                        <asp:Literal ID="Literal32" runat="server" meta:resourcekey="SelectionCriteria"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-left: 20px;">
                                <div id="Div1" runat="server" style="overflow: scroll; overflow-x: hidden; height: auto;
                                    max-height: 200px; width: 700px;">
                                    <asp:GridView ID="gvCriteria" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                        Width="689px">
                                        <HeaderStyle CssClass="datalistheader" />
                                        <Columns>
                                            <asp:BoundField DataField="LevelName" ItemStyle-HorizontalAlign="Center" meta:resourcekey="LevelName" />
                                            <asp:BoundField DataField="Criteria" meta:resourcekey="Criteria1" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Weightage" meta:resourcekey="Weightage1" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField DataField="Description" meta:resourcekey="Description1" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="250px" ItemStyle-Wrap="true" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <div class="trleft" style="float: left; width: 27%; height: 29px">
                                    <asp:Literal ID="Literal33" runat="server" meta:resourcekey="TotalWeightage"></asp:Literal>
                                </div>
                                <div class="trleft" style="float: left; width: 10%; height: 29px">
                                    :
                                </div>
                                <div class="trright" style="float: left; width: 60%; height: 29px">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </div>
                                <div class="trleft" style="float: left; width: 27%; height: 29px">
                                    <asp:Literal ID="Literal34" runat="server" meta:resourcekey="MaximumMarkInterviewer"></asp:Literal>
                                </div>
                                <div class="trleft" style="float: left; width: 10%; height: 29px">
                                    :
                                </div>
                                <div class="trright" style="float: left; width: 60%; height: 29px">
                                    <asp:Label ID="lbViewTotalWeightage" runat="server"></asp:Label>
                                </div>
                                <div class="trleft" style="float: left; width: 27%; height: 29px">
                                    <asp:Literal ID="Literal35" runat="server" meta:resourcekey="MinimumPassMark"></asp:Literal>
                                </div>
                                <div class="trleft" style="float: left; width: 10%; height: 29px">
                                    :
                                </div>
                                <div class="trright" style="float: left; width: 60%; height: 29px">
                                    <asp:Label ID="lbViewPassMark" runat="server"></asp:Label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <table width="95%" border="0" cellpadding="3">
                                <tr>
                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold">
                                        <asp:Literal ID="Literal36" runat="server" meta:resourcekey="PayStructure"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-left: 20px;">
                                <div style="float: left; width: 100%; height: auto;">
                                    <div class="trleft" style="float: left; width: 27%; height: auto">
                                        <asp:Literal ID="Literal37" runat="server" meta:resourcekey="PayStructureName"></asp:Literal>
                                    </div>
                                    <div class="trleft" style="float: left; width: 10%; height: auto">
                                        :
                                    </div>
                                    <div class="trright" style="float: left; width: 58%; height: 29px">
                                        <asp:Label ID="lblPay" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <asp:GridView ID="gvSalaryInfo" runat="server" AutoGenerateColumns="false" Width="70%"
                                    BorderColor="#307296">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <Columns>
                                        <asp:BoundField DataField="AdditionDeduction" meta:resourcekey="Particular1" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Amount" meta:resourcekey="Amount1" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>
                                <div class="trleft" style="float: left; width: 27%; height: 29px">
                                    <asp:Literal ID="Literal38" runat="server" meta:resourcekey="GrossSalary"></asp:Literal>
                                </div>
                                <div class="trleft" style="float: left; width: 10%; height: 29px">
                                    :
                                </div>
                                <div class="trright" style="float: left; width: 60%; height: 29px">
                                    <asp:Label ID="lbViewAdiitions" runat="server"></asp:Label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="left">
                            <table width="95%" border="0" cellpadding="3">
                                <tr id="trOffer" runat="server">
                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold">
                                        <asp:Literal ID="Literal39" runat="server" meta:resourcekey="OfferLetterApprovalAuthorities"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                            <asp:GridView ID="gvOffer" runat="server" AutoGenerateColumns="false" Width="70%"
                                CellPadding="10" BorderColor="#307296">
                                <HeaderStyle CssClass="datalistheader" />
                                <Columns>
                                    <asp:BoundField DataField="Level" meta:resourcekey="Authority1" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="AuthorizationHierarchyStartingfromHigherAuthority1"
                                        ItemStyle-HorizontalAlign="Center" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td valign="top" align="left">
                            <table width="95%" border="0" cellpadding="3">
                                <tr id="trKPIKRA" runat="server">
                                    <td valign="top" width="100%" style="padding-left: 5px; color: #307296; font-weight: bold">
                                        <asp:Literal ID="Literal40" runat="server" Text="KPI/KPI"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                            <div id="divKPI" runat="server" style="float: left; width: 47%;">
                                <asp:GridView ID="gvKPI" runat="server" AutoGenerateColumns="false" Width="100%" CellPadding="10"
                                    BorderColor="#307296">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <Columns>
                                        <asp:BoundField DataField="Kpi" HeaderText="KPI" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="KPIValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="divKRA" runat="server" style="float: left; width: 47%; padding-left: 10px;">
                                <asp:GridView ID="gvKRA" runat="server" AutoGenerateColumns="false" Width="100%" CellPadding="10"
                                    BorderColor="#307296">
                                    <HeaderStyle CssClass="datalistheader" />
                                    <Columns>
                                        <asp:BoundField DataField="Kra" HeaderText="KRA" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="KRAValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>--%>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="always">
        <ContentTemplate>
            <%-- <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
                    <tr>
                        <td style="width: 65%">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 35%; text-align: left; padding-left: 8px">
                            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                                ImageUrl="~/images/search.png" meta:resourcekey="Clickheretosearch" ImageAlign="AbsMiddle"
                                OnClick="btnSearch_Click" />
                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                                meta:resourcekey="SearchBy1" WatermarkCssClass="WmCss">
                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                </table>
            </asp:Panel>--%>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgNewJob" ImageUrl="~/images/Active_Vacancies.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkNewJob" runat="server" CausesValidation="False" PostBackUrl="~/Public/VacancyAddEdit.aspx"
                            meta:resourcekey="AddJob">    <%--  OnClick="lnkNewJob_Click"--%>        
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListJob" CausesValidation="false" ImageUrl="~/images/Vacancy_ListBig.PNG"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkViewjob" runat="server" CausesValidation="False" PostBackUrl="~/Public/Vacancy.aspx"
                            meta:resourcekey="ViewJob">     <%-- OnClick="lnkViewjob_Click"        --%>  
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddJob" ImageUrl="~/images/delete.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" OnClick="lnkDeleteJob_Click"
                            meta:resourcekey="Delete"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgCriteria" CausesValidation="false" ImageUrl="~/images/add_criteria.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddCriteria" runat="server" CausesValidation="False" OnClick="lnkAddCriteria_Click"
                            meta:resourcekey="AddCriteria"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddParticular0" CausesValidation="false" ImageUrl="~/images/add_addition_deduction.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddparticular" runat="server" CausesValidation="False" OnClick="lnkAddparticular_Click"
                            meta:resourcekey="AddParticulars"> 
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="height: auto">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:CriteriaReference ID="CriteriaReferenceControl" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlAdditionDeduction" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlAdditionDeduction" runat="server" Style="display: none">
                        <uc:AdditionDeduction ID="ucAdditionDeduction" runat="server" ModalPopupID="mpeAdditionDeduction" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeAdditionDeduction" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlAdditionDeduction" TargetControlID="Button3">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
