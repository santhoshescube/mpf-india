﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="EmployeeSeperation.aspx.cs" Inherits="Public_EmployeeSeperation" Title="Employee Separation" %>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>

<script runat="server">

  
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(document).ready(
        function () 
        {
            $('#ctl00_ctl00_content_public_content_fvTerminationRequest_txtEmployeeCode').keyup(function(event)
                {
                    if(event.keyCode==13)
                        {$('#ctl00_ctl00_content_public_content_fvTerminationRequest_imgSearch').click();}
                }
                );});


        function RemarksValidation(sender,args)
        {
        if(sender.value.length >=150)
        {
        return false;
        }
        else
        return true;
        }
        function GetSuggestionsBySearch()
        {
            PageMethods.AutoComplete_Search(onSucceeded, onFailed);
        }
        function onSucceeded()
        {
        
        }
        function onFailed()
        {
        }

        function  Confirmation()
        {
        Page_ClientValidate("Submit");
        if(Page_IsValid)
        {
            return confirm('Are you sure you want to submit ?'); 
        }
        return Page_IsValid;
        }

        function  ConfirmationCancel()
        {
        Page_ClientValidate("Submit");
        if(Page_IsValid)
        {
            return confirm('The action cannot be undo ..\n Are you sure you want to separate the employee   ?'); 
        }
        return Page_IsValid;
        }

        function Change(e,v)
        {

        }

        function  AutoCompleteContextKEy()
        {
            var s = 'sdsds';
            return s;
        }
        
        function AutoComplete_Search(sender, arg)
        {
            var type = sender._id.replace("ctl00_ctl00_content_public_content_fvTerminationRequest_","");
            var button =   document.getElementById(sender._id.substr(0,sender._id.lastIndexOf('_') + 1) +"imgSearch")
            Clear(button,type);
            if(button != null)
            button.click('imgSearch_Click');

        }
        function AutoComplete_Search1(sender, arg)
        {
            var type = sender._id.replace("ctl00_ctl00_content_public_content_fvTerminationRequest_","");
            var button =   document.getElementById(sender._id.substr(0,sender._id.lastIndexOf('_') + 1) +"btnSearch")
            Clear(button,type);
            if(button != null)
            button.click('imgSearch_Click');

        }



        function Clear(btn,type)
        {
         var txtEmployeeCode  =   document.getElementById(btn.id.substr(0,btn.id.lastIndexOf('_') + 1) +"txtEmployeeCode")
         var txtEmployeeName  =   document.getElementById(btn.id.substr(0,btn.id.lastIndexOf('_') + 1) +"txtEmployeeName")
         
         if(type == "AutoCompleteExtender1")
         {
          if(txtEmployeeName != null)
            txtEmployeeName.value =""; 
         }
         else
         {
         if(txtEmployeeCode != null)
          txtEmployeeCode.value = "";  
         }
         
        }

        function AutoCompleteClear(sender,arg)
        {

        }

        function OnSelectedIndexChange(ddl)
        {
            document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) + "cvDateOfSeperation").style.display='none';
            document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) + "txtDateofSepearation").value ="";    
            
            if(ddl.options[ddl.selectedIndex].value !== "0" && ddl.options[ddl.selectedIndex].value !="4")
              {
                   document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) + "test").style.display = trDisplay();
                   document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"seperationDate").innerHTML = ' Date of ' + ddl.options[ddl.selectedIndex].text;
                   
                  
                  if(ddl.options[ddl.selectedIndex].value == "1" || ddl.options[ddl.selectedIndex].value == "5")
                  {
                     document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"ReasonForSeperation").style.display = trDisplay();
                     document.getElementById( ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"lblReasonForSeperation").innerHTML = 'Reason for ' + ddl.options[ddl.selectedIndex].text;
                  }
                 else
                  {
                    document.getElementById( ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"ReasonForSeperation").style.display = 'none';
                  }
             }
             else if(ddl.options[ddl.selectedIndex].value == "4")
                   {
                    alert("You cannot change the status to resigned");

                   }
             else
             {
                  document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"test").style.display = 'none';
                  document.getElementById(ddl.id.substr(0,ddl.id.lastIndexOf('_') + 1) +"ReasonForSeperation").style.display = 'none';
            }
           
        }

    </script>

    <div id='cssmenu'>
        <ul>
            <li><a href='Employee.aspx'><span>Employee </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="Passport.aspx">Passport</a></li>
            <li><a href="Visa.aspx">Visa</a></li>
            <li><a href="Documents.aspx">Other Documents</a></li>
            <li><a href="EmployeeViewDocs.aspx">View Documents</a></li>
            <li class="selected"><a href="EmployeeSeperation.aspx">Employee Separation</a></li>
            <li><a href="EmployeeTakeOver.aspx">Takeover</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="text-align: center">
                <asp:Label ID="lblNoRecord" runat="server" CssClass="error" Text="" Style="width: 99%"></asp:Label>
            </div>
            <asp:FormView ID="fvTerminationRequest" runat="server" Width="100%" CellSpacing="1"
                OnDataBound="fvTerminationRequest_DataBound" OnItemCommand="fvTerminationRequest_ItemCommand"
                DefaultMode="Edit" Style="margin-right: 1px">
                <EditItemTemplate>
                    <div>
                        <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                            <tr>
                                <td class="trLeft" width="150px">
                                    Company
                                </td>
                                <td class="trRight">
                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownlist_mandatory"
                                        DataTextField="CompanyName" DataValueField="CompanyID" Width="200px" AutoPostBack="True"
                                        OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvddlCompanypnlTab1" runat="server" CssClass="error"
                                        ErrorMessage="Please select company" Display="Dynamic" ControlToValidate="ddlCompany"
                                        ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Employee Number
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:TextBox ID="txtEmployeeCode" runat="server" CssClass="textbox_mandatory" Width="150px"
                                        onkeypress="Change(event,this.value)" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="error"
                                        ErrorMessage="Please select an employee code" Display="Dynamic" ControlToValidate="txtEmployeeCode"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <div style="display: none">
                                        <asp:ImageButton ID="imgSearch" runat="server" SkinID="GoButton" OnClick="imgSearch_Click" />
                                    </div>
                                    <AjaxControlToolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                        TargetControlID="txtEmployeeCode" ServiceMethod="GetSuggestions" ServicePath="~/AutoCompleteService.asmx"
                                        MinimumPrefixLength="1" CompletionInterval="1" CompletionSetCount="10" EnableViewState="false"
                                        UseContextKey="true" ContextKey="return AutoCompleteContextKEy()" OnClientItemSelected="AutoComplete_Search"
                                        OnClientShowing="AutoCompleteClear">
                                    </AjaxControlToolkit:AutoCompleteExtender>
                                    <asp:HiddenField ID="hfEmployeeId" runat="server" />
                                    <asp:HiddenField ID="hfEmployeeSeperatedId" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Employee Name
                                </td>
                                <td class="trRight">
                                    <asp:DropDownList ID="ddlEmployees" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlEmployees_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlEmployees"
                                        CssClass="error" Display="Dynamic" ErrorMessage="Please select an employee" SetFocusOnError="True"
                                        InitialValue="0" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Working Status
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:TextBox ID="txtWorkingStatus" runat="server" CssClass="textbox" Width="100px"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Separation Type
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:DropDownList ID="ddlWorkStatus" runat="server" CssClass="dropdownlist_mandatory"
                                        Width="150px" onchange="OnSelectedIndexChange(this);">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlWorkStatus"
                                        CssClass="error" Display="Dynamic" InitialValue="0" ErrorMessage="Please select separation type"
                                        SetFocusOnError="True" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Date of joining
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:TextBox ID="txtDateofjoining" runat="server" CssClass="textbox_mandatory" Width="100px"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="test" runat="server" style="display: none">
                                <td class="trLeft" width="25%" runat="server" id="seperationDate">
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:TextBox ID="txtDateofSepearation" runat="server" CssClass="textbox_mandatory"
                                        Width="100px" MaxLength="10"></asp:TextBox>
                                    <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtDateofSepearation"
                                        Format="dd/MM/yyyy">
                                    </AjaxControlToolkit:CalendarExtender>
                                    <asp:CustomValidator ID="cvDateOfSeperation" ValidationGroup="Submit" ControlToValidate="txtDateofSepearation"
                                        Display="Dynamic" CssClass="error" ValidateEmptyText="true" runat="server" ClientValidationFunction="ValidateDateOfSeperation"></asp:CustomValidator>
                                    <asp:HiddenField ID="hfCurrentDate" runat="server" />
                                </td>
                            </tr>
                            <tr id="ReasonForSeperation" runat="server" style="display: none">
                                <td class="trLeft" width="25%" id="lblReasonForSeperation" runat="server">
                                </td>
                                <td class="trRight" width="70%">
                                    <asp:UpdatePanel ID="updReasonForSeperation" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:DropDownList ID="ddlReasonForSeperation" runat="server" CssClass="dropdownlist_mandatory"
                                                Width="180px" AutoPostBack="True">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnReasonForSeperation" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                Text="..." OnClick="btnReasonForSeperation_Click" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    Remarks
                                </td>
                                <td class="trRight" width="75%">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="TExtbox" Width="225px" Height="90px"
                                        TextMode="MultiLine" MaxLength="200" onchange="RestrictMulilineLength(this, 200);"
                                        onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="25%">
                                    <asp:CheckBoxList ID="CheckBoxList1" AutoPostBack="true" runat="server" CssClass="text"
                                        CausesValidation="false" DataTextField="EmployeeName" DataValueField="Parentid">
                                    </asp:CheckBoxList>
                                </td>
                                <td class="trRight" width="75%" align="right">
                                    <asp:Button ID="btnSubmit" runat="server" CausesValidation="false" CommandName="Add"
                                        Text="Submit" OnClick="btnSubmit_Click1" OnClientClick="return Confirmation()"
                                        CssClass="btnsubmit" ValidationGroup="Submit" Visible="False" Width="75px" />
                                    <asp:Button ID="btnSaveAndSeperate" runat="server" ValidationGroup="Submit" Text="Submit & Separate"
                                        CssClass="btnsubmit" CausesValidation="false" CommandName="Add" OnClientClick="return ConfirmationCancel()"
                                        OnClick="btnSaveAndSeperate_Click" />&nbsp;
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandName="CancelRequest"
                                        Text="Cancel" CausesValidation="false" OnClick="btnCancel_Click1" Width="75px" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </EditItemTemplate>
                <ItemTemplate>
                    <div id="mainwrap">
                        <div id="main2" style="margin-top: 10px; margin-left: 10px">
                            <div class="t22">
                                <div class="ta22">
                                    <div class="maindivheader">
                                        <div class="innerdivheader">
                                            Company</div>
                                        <div class="innerdivheader">
                                            Employee Number</div>
                                        <div class="innerdivheader">
                                            Employee Name</div>
                                        <div class="innerdivheader">
                                            Working Status
                                        </div>
                                        <div class="innerdivheader">
                                            Date of joining</div>
                                        <div class="innerdivheader">
                                            Date of Separation</div>
                                        <div class="innerdivheader">
                                            Separation Type
                                        </div>
                                        <div class="innerdivheader" style="display: none;" id="DivReasonLeft" runat="server">
                                            <asp:Label ID="lblReasonCaption" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div class="innerdivheader">
                                            Remarks
                                        </div>
                                    </div>
                                </div>
                                <div class="ta3">
                                    <div class="maindivheaderValue">
                                        <div class="innerdivheaderValue">
                                            <%# Eval("CompanyName")%></div>
                                        <div class="innerdivheaderValue">
                                            <%# Eval("EmployeeNumber")%></div>
                                        <div class="innerdivheaderValue">
                                            <%# Eval("EmployeeFullName")%></div>
                                        <div class="innerdivheaderValue">
                                            <%# Eval("WorkStatus")%>
                                        </div>
                                        <div class="innerdivheaderValue">
                                            <%# Convert.ToDateTime(Eval("DateOfJoining")).ToString("dd/MM/yyyy")%></div>
                                        <div class="innerdivheaderValue">
                                            <%# Convert.ToDateTime(Eval("SeperatedDate")).ToString("dd/MM/yyyy") %></div>
                                        <div class="innerdivheaderValue">
                                            <%# Eval("SeperationTypeS")%>
                                        </div>
                                        <div class="innerdivheaderValue" id="DivReasonRight" runat="server" style="display: none">
                                            <asp:Label ID="lblReasonMessage" runat="server" Text='<%# Eval("SeperatedReason") %>'></asp:Label>
                                        </div>
                                        <div class="innerdivheaderValue" style="word-break: break-all; width: 400px;height:auto">
                                            <%# Eval("Remarks")%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:FormView>
            <div>
                <asp:HiddenField ID="hdnEmployeeID" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataList ID="dlTermination" runat="server" Width="100%" BorderWidth="0px" CellPadding="0"
                OnItemDataBound="dlTermination_ItemDataBound">
                <ItemStyle CssClass="labeltext" />
                <HeaderTemplate>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td height="0" width="30" valign="top" align="left" style="padding-top: 8px; padding-left: 4px">
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectHeaderAll(this.id,'chkEmployee')" />
                            </td>
                            <td style="padding-left: 5px" class="trLeft">
                                Select All
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%" id="tblDetails" runat="server" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top" rowspan="5" width="30" style="padding-top: 5px">
                                <asp:CheckBox ID="chkEmployee" runat="server" />
                                <asp:HiddenField ID="hfIsCompany" runat="server" Value='<%# Eval("EmployeeId") %>' />
                            </td>
                            <td valign="top" width="100" align="left" style="padding-top: 10px">
                                <asp:LinkButton ID="imgLogo" runat="server" CommandName="_View" Style="padding: 2px"
                                    CausesValidation="False">
                                           <img alt="" src='<%#GetEmployeeRecentPhoto(Eval("EmployeeID"), Eval("RecentPhoto"), 100)%>' /> </asp:LinkButton>
                            </td>
                            <td valign="top" style="padding-left: 10px">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkCompany" runat="server" CssClass="listHeader bold" Text='<%# Eval("SeperatedEmployee") %>'
                                                            OnClick="lnkCompany_Click" CommandArgument='<%# Eval("EmployeeId") %>'></asp:LinkButton>&nbsp;
                                                        [<%# Eval("EmployeeNumber") %>]
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td class="innerdivheader" valign="top">
                                                        Joining Date
                                                    </td>
                                                    <td align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("DateOfJoining")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="innerdivheader" valign="top">
                                                        Working Status
                                                    </td>
                                                    <td align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("CurrentWorkingStatus")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="innerdivheader" valign="top">
                                                        Separation Type
                                                    </td>
                                                    <td align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("WorkStatus")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="innerdivheader" width="100px" valign="top">
                                                        Separated Date
                                                    </td>
                                                    <td width="10" align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("SeperatedDate")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="Td2" class="innerdivheader" runat="server" width="120px" valign="top">
                                                        <asp:Label ID="lblSeperatedReason" runat="server" Text=""></asp:Label>
                                                    </td>
                                                    <td width="10" align="center" valign="top">
                                                        <asp:Label ID="lblSeperatedColon" runat="server" Text=":"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSeperatedReason1" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top" width="50px">
                                <asp:LinkButton ID="btnCancel" runat="server" CausesValidation="False" CommandName="_Edit"
                                    Style="border-style: none; display: none;" OnClientClick=" if(this.disabled){return false;} else { return confirm('Are you sure you want to cancel the request ?');} "
                                    OnClick="btnCancel_Click" CommandArgument='<%# Eval("EmployeeSeperatedId") %>'>Cancel |  </asp:LinkButton>
                                &nbsp;
                            </td>
                            <td valign="top" align="left" width="35">
                                <asp:LinkButton ID="btnEdit" runat="server" CommandName="_Edit" CausesValidation="False"
                                    Style="border-style: none; display: none;" OnClick="btnEdit_Click" CommandArgument='<%# Eval("EmployeeSeperatedId") %>'>Edit</asp:LinkButton>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle CssClass="listItem" />
                <FooterTemplate>
                    <table width="100%">
                        <tr>
                            <td height="0" width="30" valign="top" align="left" style="padding-top: 8px; padding-left: 4px">
                                <asp:CheckBox ID="chk_FooterAll" runat="server" onclick="selectFooterAll(this.id,'chkEmployee')" />
                            </td>
                            <td style="padding-left: 5px" class="trLeft">
                                Select All
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:DataList>
            <uc:Pager ID="TerminationPager" runat="server" />
            <asp:Panel ID="pnlSelectedCompanies" runat="server" Style="display: none;">
            </asp:Panel>
            <asp:UpdatePanel ID="upPrint" runat="server">
                <ContentTemplate>
                    <div id="divPrint" runat="server" style="display: none">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="display: none">
        <asp:Button ID="btn1" runat="server" />
    </div>
    <div id="PopUp" runat="server" style="display: none">
        <asp:UpdatePanel ID="UpdPanelPopUp" runat="server">
            <ContentTemplate>
                <uc:Message ID="Message1" runat="server" ModalPopupId="ModalPopupExtender1" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <AjaxControlToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btn1"
        PopupControlID="PopUp" BackgroundCssClass="modalBackground" PopupDragHandleControlID="PopUp">
    </AjaxControlToolkit:ModalPopupExtender>
    <div style="display: none">
        <asp:Button ID="btnPopUp" runat="server" Text="Test" />
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px">
                    <tr>
                        <td style="width: 65%">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss" MaxLength="50"></asp:TextBox>
                        </td>
                        <td style="width: 35%; text-align: left; padding-left: 8px">
                            <asp:ImageButton ID="btnSearch" runat="server" CausesValidation="false" SkinID="GoButton"
                                ImageUrl="~/images/search.png" ToolTip="Click here to search" ImageAlign="AbsMiddle"
                                OnClick="btnSearch_Click" />
                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtSearch"
                                WatermarkText="Search By" WatermarkCssClass="WmCss">
                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                            <AjaxControlToolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server"
                                TargetControlID="txtSearch" ServiceMethod="GetAllSuggestions" ServicePath="~/AutoCompleteService.asmx"
                                MinimumPrefixLength="1" CompletionInterval="1" OnClientItemSelected="AutoComplete_Search1"
                                CompletionSetCount="10" EnableViewState="false">
                            </AjaxControlToolkit:AutoCompleteExtender>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAdd" ImageUrl="~/images/New_terminateemployee.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click"> <h5>
                            Employee Separation</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgList" ImageUrl="~/images/List_terminateemployee.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click"> <h5>
                           List Separation</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click">
                          <h5> Print</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click">
                         <h5>
                            Email</h5>  </asp:LinkButton>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
