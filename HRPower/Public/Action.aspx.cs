﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.Collections.Generic;

/* Created By   :   Sruthy K
 * Created Date :   23 Oct 2013
 * Purpose      :   Add & Update Performance Action
 * */
public partial class Public_Action : System.Web.UI.Page
{
    #region Declaration
    clsAction MobjclsAction;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PerformanceAction.Text").ToString();
        if (!IsPostBack)
        {
            ClearControls();

            if (Request.QueryString["EmployeeID"] != null) // From Performance Action Page - Add New mode
            {
                hdnEmployeeID.Value = Request.QueryString["EmployeeID"];
                txtWithEffectDate.Text = lblActionDate.Text = FormatDate(System.DateTime.Today);
                lblFromDate.Text = FormatDate(clsCommon.Convert2DateTime(Request.QueryString["FromDate"]));
                lblToDate.Text = FormatDate(clsCommon.Convert2DateTime(Request.QueryString["ToDate"]));
            }
            else if (Request.QueryString["PerformanceActionID"] != null)// From Performance Action Page - Edit mode
            {
                hdnPerformanceActionID.Value = Request.QueryString["PerformanceActionID"];
            }
            else if (Request.QueryString["RequestID"] != null)  // From Home Page
            {
                hdnPerformanceActionID.Value = Request.QueryString["RequestID"];
                hdnIsForApproval.Value = "1";
                btnSubmit.Text = "Approve";
            }

            if (hdnPerformanceActionID.Value.ToInt32() > 0)
            {
                ViewAction();
            }

            lblEmployee.Text = GetEmployeeName();

        }
    }
    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        CalculateNetAmount();
    }
    #endregion

    #region Functions

    private string FormatDate(DateTime Date)
    {
        return Date.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
    }

    private void ClearControls()
    {
        hdnEmployeeID.Value = hdnPerformanceActionID.Value = hdnIsForApproval.Value = "0";
        chkAlterSalary.Checked = false;
        chkJobPromotion.Checked = false;
        txtRemarks.Text = txtFeedback.Text = txtActionRemarks.Text = string.Empty;
        EnableorDisableValidators();
    }

    private void ViewAction()
    {
        DataTable dtAction = clsAction.GetActionDetails(hdnPerformanceActionID.Value.ToInt32());
        if (dtAction.Rows.Count > 0)
        {
            hdnEmployeeID.Value = Convert.ToString(dtAction.Rows[0]["EmployeeID"]);
            lblFromDate.Text = FormatDate(clsCommon.Convert2DateTime(dtAction.Rows[0]["FromDate"]));
            lblToDate.Text = FormatDate(clsCommon.Convert2DateTime(dtAction.Rows[0]["ToDate"]));
            lblActionDate.Text = FormatDate(clsCommon.Convert2DateTime(dtAction.Rows[0]["ActionDate"]));
            txtWithEffectDate.Text = FormatDate(clsCommon.Convert2DateTime(dtAction.Rows[0]["WithEffectDate"]));
            txtActionRemarks.Text = Convert.ToString(dtAction.Rows[0]["Remarks"]);
            txtFeedback.Text = Convert.ToString(dtAction.Rows[0]["Feedback"]);

            chkAlterSalary.Checked = Convert.ToBoolean(dtAction.Rows[0]["IsSalaryAltered"]);
            AlterSalary();
            chkJobPromotion.Checked = Convert.ToBoolean(dtAction.Rows[0]["IsJobPromotion"]);
            SetJobPromotion();

            upnlSalaryStructure.Update();
        }
    }

    private string GetEmployeeName()
    {
        return clsAction.GetEmployeeName(hdnEmployeeID.Value.ToInt64());
    }

    private void AlterSalary()
    {
        if (chkAlterSalary.Checked)
        {
            divSalaryStructure.Style["display"] = "block";
            BindSalaryStructure();
        }
        else
        {
            divSalaryStructure.Style["display"] = "none";
        }
        EnableorDisableValidators();
    }

    private void EnableorDisableValidators()
    {
        rfvEmployee.Enabled = cvAddAllowances.Enabled = cvAmount.Enabled = cvRemuneration.Enabled = cvRemunerationAmount.Enabled = cvAddAllowances.Enabled = vsSummary.Enabled = chkAlterSalary.Checked;
    }

    private void BindSalaryStructure()
    {
        DataTable dtSalaryStructure = clsAction.GetSalaryStructureMaster(hdnEmployeeID.Value.ToInt64(), hdnPerformanceActionID.Value.ToInt32());
        if (dtSalaryStructure.Rows.Count > 0)
        {
            FillDropDowns(0);

            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["CompanyID"])));
            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["EmployeeID"])));
            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["PaymentClassificationID"])));
            ddlSalaryProcessDay.SelectedIndex = ddlSalaryProcessDay.Items.IndexOf(ddlSalaryProcessDay.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["SalaryDay"])));
            ddlPaymentCalculation.SelectedIndex = ddlPaymentCalculation.Items.IndexOf(ddlPaymentCalculation.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["PayCalculationTypeID"])));
            ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["CurrencyId"])));

            ddlAbsentPolicy.SelectedIndex = ddlAbsentPolicy.Items.IndexOf(ddlAbsentPolicy.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["AbsentPolicyId"])));
            ddlEncashPolicy.SelectedIndex = ddlEncashPolicy.Items.IndexOf(ddlEncashPolicy.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["EncashPolicyId"])));
            ddlHolidayPolicy.SelectedIndex = ddlHolidayPolicy.Items.IndexOf(ddlHolidayPolicy.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["HolidayPolicyId"])));
            ddlOvertimePolicy.SelectedIndex = ddlOvertimePolicy.Items.IndexOf(ddlOvertimePolicy.Items.FindByValue(Convert.ToString(dtSalaryStructure.Rows[0]["OTPolicyId"])));

            txtRemarks.Text = Convert.ToString(dtSalaryStructure.Rows[0]["Remarks"]);


            dlDesignationAllowances.DataSource = clsAction.GetEmployeeAllowanceDetails(dtSalaryStructure.Rows[0]["SalaryID"].ToInt64(), hdnPerformanceActionID.Value.ToInt32());
            dlDesignationAllowances.DataBind();
            upnlDesignationAllowance.Update();
            upnlSalaryStructure.Update();

            foreach (DataListItem item in dlDesignationAllowances.Items)
            {

                DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

                //basic pay
                if (ddlParticulars.SelectedItem.Value == "1")
                {
                    ddlCategory.SelectedValue = "1";
                    ddlCategory.Enabled = false;

                }
                else

                    ddlCategory.Enabled = true;

                if (txtAmount == null) return;
                ScriptManager.RegisterClientScriptBlock(txtAmount, txtAmount.GetType(), "Calculate", "setTimeout(function(){CalculateSalaryStructureForAction('" + txtAmount.ClientID + "')},200)", true);
            }

            dlOtherRemuneration.DataSource = clsAction.GetOtherRemuneration(dtSalaryStructure.Rows[0]["SalaryID"].ToInt64(), hdnPerformanceActionID.Value.ToInt32());
            dlOtherRemuneration.DataBind();
            upnlOtherRemuneration.Update();
        }
    }

    private void BindDropdown(DataTable dtSource, string TextField, string ValueField, DropDownList ddlSource, bool AddNewRow)
   {
        ddlSource.DataTextField = TextField;
        ddlSource.DataValueField = ValueField;
        ddlSource.DataSource = dtSource;
        ddlSource.DataBind();

        if (AddNewRow)
            ddlSource.Items.Insert(0, new ListItem(GetGlobalResourceObject("DocumentsCommon","Select").ToString(), "-1"));
    }

    /// <summary>
    /// Fill Dropdowns
    /// </summary>
    /// <param name="Mode">Mode</param>
    private void FillDropDowns(int Mode)
    {
        if (Mode == 0) // Fill Salary Structure DropDowns
        {
            //Company
            BindDropdown(clsAction.FillCompany(hdnEmployeeID.Value.ToInt64()), "CompanyName", "CompanyID", ddlCompany, false);

            //Employee
            DataTable dtEmployee = new DataTable();
            dtEmployee.Columns.Add(new DataColumn("EmployeeID", typeof(long)));
            dtEmployee.Columns.Add(new DataColumn("Name", typeof(string)));
            DataRow dRow = dtEmployee.NewRow();
            dRow["EmployeeID"] = hdnEmployeeID.Value.ToInt64();
            dRow["Name"] = GetEmployeeName();
            dtEmployee.Rows.Add(dRow);

            BindDropdown(dtEmployee, "Name", "EmployeeID", ddlEmployee, false);

            //Currency
            BindDropdown(clsAction.FillCurrency(hdnEmployeeID.Value.ToInt64()), "CurrencyName", "CurrencyID", ddlCurrency, false);

            //Payment Mode
            BindDropdown(clsAction.FillPaymentMode(), "PaymentClassification", "PaymentClassificationID", ddlPaymentMode, false);

            //Payment Calculation
            BindDropdown(clsAction.FillPaymentCalculationType(), "PayCalculationType", "PayCalculationTypeID", ddlPaymentCalculation, false);

            /* Fill Policy Dropdowns */

            //Absent Policy
            BindDropdown(clsAction.FillAbsentPolicies(), "AbsentPolicy", "AbsentPolicyID", ddlAbsentPolicy, true);

            // Encash Policy
            BindDropdown(clsAction.FillEncashPolicies(), "EncashPolicy", "EncashPolicyID", ddlEncashPolicy, true);

            // Holiday Policy
            BindDropdown(clsAction.FillHolidayPolicies(), "HolidayPolicy", "HolidayPolicyID", ddlHolidayPolicy, true);

            //OverTime Policy
            BindDropdown(clsAction.FillOTPolicies(), "OTPolicy", "OTPolicyID", ddlOvertimePolicy, true);
        }
        else if (Mode == 1) // Fill Job Promotion DropDowns
        {
            //Designation
            BindDropdown(clsAction.FillDesignations(), "Designation", "DesignationID", ddlDesignation, false);

            //WorkPolicy
            BindDropdown(clsAction.FillWorkPolicies(), "WorkPolicy", "WorkPolicyID", ddlWorkPolicy, false);
        }
    }

    private void CalculateNetAmount()
    {
        if (dlDesignationAllowances.Items.Count == 0)
        {
            txtNetAmount.Text = txtAdditionAmt.Text = txtDeductionAmt.Text = "0.00";
        }
        else
        {
            txtNetAmount.Text = "0.00";
            foreach (DataListItem Item in dlDesignationAllowances.Items)
            {
                TextBox txtAmount = (TextBox)Item.FindControl("txtAmount");
                DropDownList ddlDeductionPolicy = (DropDownList)Item.FindControl("ddlDeductionPolicy");

                if(ddlDeductionPolicy.SelectedItem.Value.ToInt32() > 0)
                    CalculateDeductionAmount(ddlDeductionPolicy);

                txtNetAmount.Text = (txtNetAmount.Text.ToDecimal() + txtAmount.Text.ToDecimal()).ToString();
               // if (txtAmount == null) return;
                ScriptManager.RegisterClientScriptBlock(txtAmount, txtAmount.GetType(), "Calculate", "setTimeout(function(){CalculateSalaryStructureForAction('" + txtAmount.ClientID + "')},250)", true);












                //if (ddlParticulars.SelectedItem.Text.EndsWith("(+"))
                //    NetAmount = txtNetAmount.Text.ToDecimal() + txtAmount.Text.ToDecimal();










            }
        }
        upnlSalaryStructure.Update();
    }

    private void UpdateAllowancesDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("SalaryDetailID");
        dt.Columns.Add("AddDedID");
        dt.Columns.Add("Amount");
        dt.Columns.Add("DeductionPolicyID");
        dt.Columns.Add("CategoryID");

        DataRow dw;

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
            HiddenField hfCatagoryID = (HiddenField)item.FindControl("hfCatagoryID");
            dw = dt.NewRow();

            dw["SalaryDetailID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
            dw["AddDedID"] = ddlParticulars.SelectedValue.ToInt32();
            dw["Amount"] = (txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text).ToDecimal();
            dw["DeductionPolicyID"] = ddlDeductionPolicy.SelectedValue.ToInt32();
            dw["CategoryID"] = hfCatagoryID.Value;
            dt.Rows.Add(dw);
        }
        ViewState["Allowances"] = dt;
    }

    private void NewAllowancesAdd()
    {
        DataTable dtAllowance = (DataTable)ViewState["Allowances"];

        if (dtAllowance == null)
        {
            dtAllowance = new DataTable();
            dtAllowance.Columns.Add("SalaryDetailID");
            dtAllowance.Columns.Add("AddDedID");
            dtAllowance.Columns.Add("Amount");
            dtAllowance.Columns.Add("DeductionPolicyID");
            dtAllowance.Columns.Add("CategoryID");
        }
        DataRow dw = dtAllowance.NewRow();
        dtAllowance.Rows.Add(dw);

        dlDesignationAllowances.DataSource = dtAllowance;
        dlDesignationAllowances.DataBind();
    }

    private void UpdateRemunerationDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("SalaryID");
        dt.Columns.Add("OtherRemunerationID");
        dt.Columns.Add("Amount");

        DataRow dw;

        foreach (DataListItem item in dlOtherRemuneration.Items)
        {
            DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
            TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

            dw = dt.NewRow();

            dw["SalaryID"] = dlOtherRemuneration.DataKeys[item.ItemIndex];
            dw["OtherRemunerationID"] = ddlRemunerationParticulars.SelectedValue.ToInt32();
            dw["Amount"] = (txtRemunerationAmount.Text == string.Empty ? "0.000" : txtRemunerationAmount.Text).ToDecimal();

            dt.Rows.Add(dw);
        }
        ViewState["Remuneration"] = dt;
    }

    private void NewRemunerationAdd()
    {
        DataTable dtRemuneration = (DataTable)ViewState["Remuneration"];

        if (dtRemuneration == null)
        {
            dtRemuneration = new DataTable();

            dtRemuneration.Columns.Add("SalaryID");
            dtRemuneration.Columns.Add("OtherRemunerationID");
            dtRemuneration.Columns.Add("Amount");
        }
        DataRow dw = dtRemuneration.NewRow();
        dtRemuneration.Rows.Add(dw);

        dlOtherRemuneration.DataSource = dtRemuneration;
        dlOtherRemuneration.DataBind();
    }

    private void SetJobPromotion()
    {
        if (chkJobPromotion.Checked)
        {
            divJobPromotion.Style["display"] = "block";
            FillJobPromotion();
        }
        else
            divJobPromotion.Style["display"] = "none";
    }

    private void FillJobPromotion()
    {
        FillDropDowns(1);

        DataTable dtEmployee = new DataTable();
        if (hdnPerformanceActionID.Value.ToInt32() > 0)
        {
            dtEmployee = clsAction.GetJobPromotionInEditMode(hdnPerformanceActionID.Value.ToInt32());
        }
        else
        {
            dtEmployee = clsAction.GetCurrentWorkPolicyAndDesignation(hdnEmployeeID.Value.ToInt64());
        }

        if (dtEmployee.Rows.Count > 0)
        {
            ddlDesignation.SelectedIndex = ddlDesignation.Items.IndexOf(ddlDesignation.Items.FindByValue(Convert.ToString(dtEmployee.Rows[0]["DesignationID"])));
            ddlWorkPolicy.SelectedIndex = ddlWorkPolicy.Items.IndexOf(ddlWorkPolicy.Items.FindByValue(Convert.ToString(dtEmployee.Rows[0]["WorkPolicyID"])));
        }

    }

    private void Save()
    {
        FillAction();
        FillSalaryStructure();
        if (MobjclsAction.SaveAction())
        {
            hdnPerformanceActionID.Value = Convert.ToString(MobjclsAction.PerformanceActionID);
            if (!MobjclsAction.IsApproved)
            {
               // clsCommonMessage.SendMessage(hdnEmployeeID.Value.ToInt32(), hdnPerformanceActionID.Value.ToInt32(), eReferenceTypes.PerformanceAction, eMessageTypes.Performance__Action, eAction.Approved, "");

                clsCommonMessage.SendMessage(hdnEmployeeID.Value.ToInt32(), hdnPerformanceActionID.Value.ToInt32(), eReferenceTypes.PerformanceAction, eMessageTypes.Performance__Action, eAction.Applied, "");

            }
            else
            {

                clsCommonMessage.SendMessage(hdnEmployeeID.Value.ToInt32(), hdnPerformanceActionID.Value.ToInt32(), eReferenceTypes.PerformanceAction, eMessageTypes.Performance__Action, eAction.Approved, "");

               // clsCommonMessage.DeleteMessages(MobjclsAction.PerformanceActionID, eReferenceTypes.PerformanceAction);
                //clsCommonMail.SendMail(MobjclsAction.PerformanceActionID, MailRequestType.PerformanceAction, eAction.Approved);
            }
            Response.Redirect("ViewPerformanceAction.aspx");
        }
    }

    private void FillAction()
    {
        MobjclsAction = new clsAction();
        MobjclsAction.PerformanceActionID = hdnPerformanceActionID.Value.ToInt32();
        MobjclsAction.EmployeeID = hdnEmployeeID.Value.ToInt64();
        MobjclsAction.ActionById = new clsUserMaster().GetEmployeeId();
        MobjclsAction.ActionDate = clsCommon.Convert2DateTime(lblActionDate.Text);
        MobjclsAction.FromDate = clsCommon.Convert2DateTime(lblFromDate.Text);
        MobjclsAction.ToDate = clsCommon.Convert2DateTime(lblToDate.Text);
        MobjclsAction.WithEffectDate = clsCommon.Convert2DateTime(txtWithEffectDate.Text);
        MobjclsAction.IsApproved = hdnIsForApproval.Value.ToInt32() == 0 ? false : true;
        MobjclsAction.IsSalaryAltered = chkAlterSalary.Checked;
        MobjclsAction.IsJobPromotion = chkJobPromotion.Checked;


        if (MobjclsAction.IsJobPromotion)
        {
            DataTable dtEmployee = clsAction.GetCurrentWorkPolicyAndDesignation(hdnEmployeeID.Value.ToInt64());
            MobjclsAction.PreviousDesignationID = dtEmployee.Rows[0]["DesignationID"].ToInt32();
            MobjclsAction.PreviousWorkPolicyID = dtEmployee.Rows[0]["WorkPolicyID"].ToInt32();
            MobjclsAction.CurrentDesignationID = ddlDesignation.SelectedValue.ToInt32();
            MobjclsAction.CurrentWorkPolicyID = ddlWorkPolicy.SelectedValue.ToInt32();
        }
        else
        {
            MobjclsAction.PreviousDesignationID = MobjclsAction.PreviousWorkPolicyID = MobjclsAction.CurrentDesignationID = MobjclsAction.CurrentWorkPolicyID = null;
        }

        if (MobjclsAction.IsSalaryAltered)
        {
            MobjclsAction.PreviousNetSalary = clsAction.GetCurrentSalary(hdnEmployeeID.Value.ToInt64());
            MobjclsAction.CurrentNetSalary = txtNetAmount.Text.ToDecimal();
        }
        else
        {
            MobjclsAction.PreviousNetSalary = MobjclsAction.CurrentNetSalary = null;
        }

        MobjclsAction.Remarks = txtActionRemarks.Text.Trim();
        MobjclsAction.Feedback = txtFeedback.Text.Trim();
    }

    private void FillSalaryStructure()
    {
        if (MobjclsAction.IsSalaryAltered)
        {
            MobjclsAction.objPerformanceSalaryStructureMaster = new clsPerformanceSalaryStructureMaster();
            MobjclsAction.objPerformanceSalaryStructureMaster.EmployeeID = hdnEmployeeID.Value.ToInt64();
            MobjclsAction.objPerformanceSalaryStructureMaster.PayCalculationTypeID = ddlPaymentCalculation.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.PaymentClassificationID = ddlPaymentMode.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.SalaryDay = ddlSalaryProcessDay.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.CurrencyID = ddlCurrency.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.Remarks = txtRemarks.Text.Trim();
            MobjclsAction.objPerformanceSalaryStructureMaster.OTPolicyID = ddlOvertimePolicy.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.AbsentPolicyID = ddlAbsentPolicy.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.HolidayPolicyID = ddlHolidayPolicy.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.EncashPolicyID = ddlEncashPolicy.SelectedValue.ToInt32();
            MobjclsAction.objPerformanceSalaryStructureMaster.NetAmount = txtNetAmount.Text.ToDecimal();

            MobjclsAction.lstPerformanceSalaryStructureDetails = new List<clsPerformanceSalaryStructureDetails>();
            int SerialNo = 1;
            foreach (DataListItem Item in dlDesignationAllowances.Items)
            {
                if (Item.ItemType == ListItemType.Item || Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddlParticulars = (DropDownList)Item.FindControl("ddlParticulars");
                    TextBox txtAmount = (TextBox)Item.FindControl("txtAmount");
                    DropDownList ddlDeductionPolicy = (DropDownList)Item.FindControl("ddlDeductionPolicy");
                    DropDownList ddlCategory = (DropDownList)Item.FindControl("ddlCategory");

                    clsPerformanceSalaryStructureDetails objSalaryStructureDetails = new clsPerformanceSalaryStructureDetails();
                    objSalaryStructureDetails.SerialNo = SerialNo;
                    objSalaryStructureDetails.AdditionDeductionID = ddlParticulars.SelectedValue.ToInt32();
                    objSalaryStructureDetails.Amount = txtAmount.Text.ToDecimal();
                    objSalaryStructureDetails.CategoryID = ddlCategory.SelectedValue.ToInt32();

                    if (ddlDeductionPolicy.SelectedValue.ToInt32() > 0)
                    {
                        objSalaryStructureDetails.AdditionDeductionPolicyId = ddlDeductionPolicy.SelectedValue.ToInt32();
                    }
                    else
                        objSalaryStructureDetails.AdditionDeductionPolicyId = null;

                    MobjclsAction.lstPerformanceSalaryStructureDetails.Add(objSalaryStructureDetails);
                    SerialNo = SerialNo + 1;
                }
            }

            MobjclsAction.lstPerformanceSalaryOtherRemuneration = new List<clsPerformanceSalaryOtherRemuneration>();
            foreach (DataListItem Item in dlOtherRemuneration.Items)
            {
                clsPerformanceSalaryOtherRemuneration objOtherRemuneration = new clsPerformanceSalaryOtherRemuneration();
                DropDownList ddlRemunerationParticulars = (DropDownList)Item.FindControl("ddlRemunerationParticulars");
                TextBox txtRemunerationAmount = (TextBox)Item.FindControl("txtRemunerationAmount");
                objOtherRemuneration.OtherRemunerationID = ddlRemunerationParticulars.SelectedValue.ToInt32();
                objOtherRemuneration.Amount = txtRemunerationAmount.Text.ToDecimal();
                MobjclsAction.lstPerformanceSalaryOtherRemuneration.Add(objOtherRemuneration);
            }
        }
    }

    private bool ValidateAction()
    {
        if (chkJobPromotion.Checked)
        {
            if (ddlDesignation.Items.Count == 0)
            {
                ucMessage.InformationalMessage("Please Select a Designation");
                mpeMessage.Show();
                return false;
            }
            else if (ddlWorkPolicy.Items.Count == 0)
            {
                ucMessage.InformationalMessage("Please Select a Work Policy");
                mpeMessage.Show();
                return false;
            }
        }
        if (chkAlterSalary.Checked)
        {
            if(ddlCompany.Items.Count == 0)
            {
                ucMessage.InformationalMessage("Please Select a Company");
                mpeMessage.Show();
                return false;
            }
            else if (txtNetAmount.Text.ToDecimal() < 0)
            {
                ucMessage.WarningMessage("Net Amount Cannot be Negative");
                mpeMessage.Show();
                return false;
            }
        }
        return true;
    }

    private void CalculateDeductionAmount(DropDownList ddlDeductionPolicy)
    {
        DataListItem Item = (DataListItem)ddlDeductionPolicy.Parent;
        HiddenField hdnDeductionAmount = (HiddenField)Item.FindControl("hdnDeductionAmount");
        hdnDeductionAmount.Value = "0";
        if (ddlDeductionPolicy.SelectedValue.ToInt32() > 0)
        {
            hdnDeductionAmount.Value = CalculateDeductionPolicyAmount(ddlDeductionPolicy.SelectedValue.ToInt32()).ToString("0.00");
            upnlSalaryStructure.Update();
        }
    }

    public decimal CalculateDeductionPolicyAmount(int iDeductionPolicyID)
    {
        decimal decCalculatedSalary = 0, decAmountLimit, decEmployerPart, decEmployeePart, decFixedAmount, decBasicPay = 0, decGross = 0;
        int iRateOnly, iAdditionDeductionID, iAdditionID, iParameterID, iCanContinue = 0;
        foreach (DataListItem Item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)Item.FindControl("ddlParticulars");
            TextBox txtAmount = (TextBox)Item.FindControl("txtAmount");
            if (clsAction.IsAddition(ddlParticulars.SelectedValue.ToInt32()))
            {
                decGross += txtAmount.Text.ToDecimal();
                if (ddlParticulars.SelectedValue.ToInt32() == 1)
                {
                    decBasicPay += txtAmount.Text.ToDecimal();
                }
            }
        }

        DataSet dsDetails = clsAction.GetDeductionPolicyDetails(iDeductionPolicyID);
        DataTable dtPolicyDetails = dsDetails.Tables[0];
        //AdditionIDs of the deduction policy
        string[] strAdditionIDs = dsDetails.Tables[1].Rows[0][0].ToStringCustom().Split(',');
        if (dtPolicyDetails.Rows.Count > 0)
        {
            iRateOnly = dtPolicyDetails.Rows[0]["RateOnly"].ToInt32();
            iAdditionID = dtPolicyDetails.Rows[0]["AdditionID"].ToInt32();
            iAdditionDeductionID = dtPolicyDetails.Rows[0]["AdditionDeductionID"].ToInt32();
            iParameterID = dtPolicyDetails.Rows[0]["ParameterID"].ToInt32();
            decAmountLimit = dtPolicyDetails.Rows[0]["AmountLimit"].ToDecimal();
            decEmployerPart = dtPolicyDetails.Rows[0]["EmployerPart"].ToDecimal();
            decEmployeePart = dtPolicyDetails.Rows[0]["EmployeePart"].ToDecimal();
            decFixedAmount = dtPolicyDetails.Rows[0]["FixedAmount"].ToDecimal();

            // IF RateOnly  = 1 THEN FIXED RATE ELSE RATE CALCULATION BASED ON THE PARAMETERS SPECIFIED
            if (iRateOnly == 1)
            {
                decCalculatedSalary = decEmployeePart;
                iCanContinue = 1;
            }
            else
            {
                //CALCULATION BASED ON THE ADDITIONID VALUE EITHER BASIC PAY AND GROSS SALARY
                //PARAMETER VALUE :18 (CALCULATION BASED ON GROSS SALARY)
                //	              :1  (CALCULATION BASED ON BASIC PAY)
                decCalculatedSalary = 0;
                if (iAdditionID == 1)//If basic pay or gross
                {
                    decCalculatedSalary = decBasicPay;
                }
                else
                {
                    decCalculatedSalary = decGross;
                }
                iCanContinue = 2;
                //CHECKING WHETHER WE NEED TO CALCULATE THE DEDUCTION AMOUNT BASED ON THE PARAMETER VALUE
                if (iParameterID == 1)//CALCULATED SALARY LESS THAN THE  AMOUNT LIMIT
                {
                    if (decCalculatedSalary < decAmountLimit)
                        iCanContinue = 1;
                }
                else if (iParameterID == 2)
                {
                    if (decCalculatedSalary > decAmountLimit)//CALCULATED SALARY GREATER THAN THE  AMOUNT LIMIT
                        iCanContinue = 1;
                }
                else if (iParameterID == 3)//CALCULATED SALARY  EQUAL TO THE  AMOUNT LIMIT
                {
                    if (decCalculatedSalary == decAmountLimit)
                        iCanContinue = 1;
                }
                else if (iParameterID == 4)//CALCULATED SALARY LESS THAN OR  EQUAL TO THE  AMOUNT LIMIT
                {
                    if (decCalculatedSalary <= decAmountLimit)
                        iCanContinue = 1;
                }
                else if (iParameterID == 5)//CALCULATED SALARY GREATER THAN OR  EQUAL TO THE  AMOUNT LIMIT
                {
                    if (decCalculatedSalary >= decAmountLimit)
                        iCanContinue = 1;
                }

                if (iCanContinue == 1)
                {
                    //CALCULATE THE SUM OF ALL THE PARAMETERS
                    decCalculatedSalary = 0;
                    foreach (DataListItem Item in dlDesignationAllowances.Items)
                    {
                        DropDownList ddlParticular = (DropDownList)Item.FindControl("ddlParticulars");
                        TextBox txtAmount = (TextBox)Item.FindControl("txtAmount");
                        //CHECKING IF THE ADDITION IDS SPECIFIED IN THE DEDUCTION POLICY IS SELECTED BY USER.
                        //IF YES ,THEIR AMOUNT IS SUMMED UP
                        if (clsAction.IsAddition(ddlParticular.SelectedValue.ToInt32()) && strAdditionIDs.Contains(ddlParticular.SelectedValue.ToStringCustom().Trim()))
                        {
                            decCalculatedSalary += txtAmount.Text.ToDecimal();
                        }
                    }
                    //THE EMPLOYEE DEDUCTION AMOUNT IS THE EMPLOYEE CONTRIBUTION PERCENTAGE OF THE CALCULATED SALARY
                    decCalculatedSalary = ((decEmployeePart / 100) * decCalculatedSalary);
                }
            }
        }
        //RETURN CalculatedSalary AS DeductionAmount
        if (iCanContinue == 1)
        {
            return decCalculatedSalary;
        }
        else
            return 0;
    }

    private void EnableOrDisableDeductionPolicy(DropDownList ddlParticulars)
    {
        TextBox txtAmount = (TextBox)ddlParticulars.Parent.FindControl("txtAmount");
        DropDownList ddlDeductionPolicy = (DropDownList)ddlParticulars.Parent.FindControl("ddlDeductionPolicy");
        BindDropdown(clsAction.FillDeductionPolicy(ddlParticulars.SelectedValue.ToInt32()), "PolicyName", "DeductionPolicyID", ddlDeductionPolicy, true);

        if (!clsAction.IsAddition(ddlParticulars.SelectedValue.ToInt32()))
        {
            txtAmount.Enabled = true;
            txtAmount.CssClass = "textbox";
            ddlDeductionPolicy.CssClass = "dropdownlist";
            ddlDeductionPolicy.Enabled = true;
        }
        else
        {
            txtAmount.CssClass = "textbox_disabled";
            ddlDeductionPolicy.SelectedIndex = 0;
            ddlDeductionPolicy.CssClass = "dropdownlist_disabled";
            ddlDeductionPolicy.Enabled = false;
        }
    }

    #endregion

    #region Events

    protected void chkAlterSalary_CheckedChanged(object sender, EventArgs e)
    {
        AlterSalary();
    }
    protected void dlDesignationAllowances_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DropDownList ddlParticulars = (DropDownList)e.Item.FindControl("ddlParticulars");
        DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
        DropDownList ddlDeductionPolicy = (DropDownList)e.Item.FindControl("ddlDeductionPolicy");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");
        HiddenField hdDeductionPolicy = (HiddenField)e.Item.FindControl("hdDeductionPolicy");
        HiddenField hdParticulars = (HiddenField)e.Item.FindControl("hdParticulars");
        HiddenField hfCatagoryID = (HiddenField)e.Item.FindControl("hfCatagoryID");


        BindDropdown(clsAction.FillParticulars(), "Description", "AddDedID", ddlParticulars, true);
        ddlParticulars.SelectedIndex = ddlParticulars.Items.IndexOf(ddlParticulars.Items.FindByValue(Convert.ToString(hdParticulars.Value)));
        EnableOrDisableDeductionPolicy(ddlParticulars);
        if (Convert.ToString(hdDeductionPolicy.Value) != "")
            ddlDeductionPolicy.SelectedIndex = ddlDeductionPolicy.Items.IndexOf(ddlDeductionPolicy.Items.FindByValue(Convert.ToString(hdDeductionPolicy.Value)));
        CalculateNetAmount();





        ddlCategory.DataTextField = clsUserMaster.GetCulture() == "ar-AE" ? ("AmountCategoryArb") : ("AmountCategory"); 
        ddlCategory.DataValueField = "CategoryID";
        ddlCategory.DataSource = clsAction.GetAllCategories();
        ddlCategory.DataBind();



        if (ddlParticulars.SelectedItem.Value == "1")
        {
            ddlCategory.SelectedValue = "1";
            ddlCategory.Enabled = false ;
        }
        else
            ddlCategory.Enabled = true ;
            

        ddlCategory.SelectedValue = hfCatagoryID.Value.ToString();

        upnlDesignationAllowance.Update();
    }

    protected void dlDesignationAllowances_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ALLOWANCE":

                DataTable dtAllowance = new DataTable();
                dtAllowance.Columns.Add("SalaryDetailID");
                dtAllowance.Columns.Add("AddDedID");
                dtAllowance.Columns.Add("Amount");
                dtAllowance.Columns.Add("DeductionPolicyID");
                dtAllowance.Columns.Add("CategoryID");

                DataRow dRow;

                foreach (DataListItem item in dlDesignationAllowances.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                    HiddenField hfCatagoryID = (HiddenField)item.FindControl("hfCatagoryID");

                    dRow = dtAllowance.NewRow();

                    dRow["SalaryDetailID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
                    dRow["AddDedID"] = ddlParticulars.SelectedValue.ToInt32();
                    dRow["Amount"] = (txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text).ToDecimal();
                    dRow["DeductionPolicyID"] = (ddlDeductionPolicy.SelectedValue).ToInt32();
                    dRow["CategoryID"] = hfCatagoryID.Value;

                    if (item != e.Item)
                    {
                        dtAllowance.Rows.Add(dRow);
                    }

                    ViewState["Allowances"] = dtAllowance;
                }
                dlDesignationAllowances.DataSource = ViewState["Allowances"];
                dlDesignationAllowances.DataBind();
                CalculateNetAmount();
                break;
        }
    }

    protected void ddlDeductionPolicy_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalculateNetAmount();
    }

    protected void dlOtherRemuneration_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_Remuneration":

                DataTable dtOtherRemuneration = new DataTable();
                dtOtherRemuneration.Columns.Add("SalaryID");
                dtOtherRemuneration.Columns.Add("OtherRemunerationID");
                dtOtherRemuneration.Columns.Add("Amount");

                DataRow dRow;

                foreach (DataListItem Item in dlOtherRemuneration.Items)
                {
                    DropDownList ddlRemunerationParticulars = (DropDownList)Item.FindControl("ddlRemunerationParticulars");
                    TextBox txtRemunerationAmount = (TextBox)Item.FindControl("txtRemunerationAmount");

                    dRow = dtOtherRemuneration.NewRow();

                    dRow["SalaryID"] = dlOtherRemuneration.DataKeys[Item.ItemIndex];
                    dRow["OtherRemunerationID"] = ddlRemunerationParticulars.SelectedValue.ToInt32();
                    dRow["Amount"] = (txtRemunerationAmount.Text == string.Empty ? "0.000" : txtRemunerationAmount.Text).ToDecimal();

                    if (Item != e.Item)
                    {
                        dtOtherRemuneration.Rows.Add(dRow);
                    }
                    ViewState["Remuneration"] = dtOtherRemuneration;
                }
                dlOtherRemuneration.DataSource = ViewState["Remuneration"];
                dlOtherRemuneration.DataBind();
                break;
        }
    }

    protected void dlOtherRemuneration_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        DropDownList ddlRemunerationParticulars = (DropDownList)e.Item.FindControl("ddlRemunerationParticulars");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtRemunerationAmount");
        HiddenField hdRemunerationParticulars = (HiddenField)e.Item.FindControl("hdRemunerationParticulars");

        BindDropdown(clsAction.FillOtherRemunerationParticulars(), "OtherRemuneration", "OtherRemunerationID", ddlRemunerationParticulars, true);

        if (Convert.ToString(hdRemunerationParticulars.Value) != "")
        {
            ddlRemunerationParticulars.SelectedIndex = ddlRemunerationParticulars.Items.IndexOf(ddlRemunerationParticulars.Items.FindByValue(Convert.ToString(hdRemunerationParticulars.Value)));
        }
        upnlOtherRemuneration.Update();
    }

    protected void ddlParticulars_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlParticulars = (DropDownList)sender;
        EnableOrDisableDeductionPolicy(ddlParticulars);
    }



    protected void btnAllowances_Click(object sender, EventArgs e)
    {
        ViewState["Allowances"] = null;
        UpdateAllowancesDataList();
        NewAllowancesAdd();
    }

    protected void chkJobPromotion_CheckedChanged(object sender, EventArgs e)
    {
        SetJobPromotion();
    }

    protected void btnOtherRemuneration_Click(object sender, EventArgs e)
    {
        ViewState["Remuneration"] = null;
        UpdateRemunerationDataList();
        NewRemunerationAdd();
        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "setPolicyTab", "SetSalaryTab('pnlpTab2')", true);
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ValidateAction())
        {
            Save();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewPerformanceAction.aspx");
    }

    #endregion

}
