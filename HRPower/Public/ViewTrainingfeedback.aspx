﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="ViewTrainingfeedback.aspx.cs" Inherits="Public_ViewTrainingfeedback" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">

<div id="dvHavePermission" runat="server" style="width: 100%; margin-top: 20px;">

    <asp:DataList ID="dlviewTrainingfeedback" runat="server">
    <HeaderTemplate>
    <table width="100%">
    <tr>
    <td style="width: 15%; vertical-align: top">
                                    <asp:Label ID="lbltraining" runat="server" CssClass="labeltext" ForeColor="White"
                                       meta:resourcekey="TrainingScheduleTitle"></asp:Label>
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                    <asp:Label ID="lblempname" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="EmployeeName"></asp:Label>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                    <asp:Label ID="lblremark" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey="Remarks"></asp:Label>
                                </td>
    </tr>
    </table>
    </HeaderTemplate>
    <ItemTemplate>
    <table>
      <tr>
                                <td style="width: 15%; vertical-align: top">
                                     <%# Eval("ScheduleID")%>
                                   
                                </td>
                                <td style="width: 25%; vertical-align: top">
                                    <%# Eval("EmployeeName")%>
                                </td>
                                <td style="width: 20%; vertical-align: top">
                                   <%# Eval("Feedback")%>
                                    
                                </td>
                                
                                <td style="width: 10%; vertical-align: top;">
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("PerformanceInitiationID") %>'
                                        CommandName="_EditPerformance" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit %>' />
                                </td>
                            </tr>
     </table>
    </ItemTemplate>
    </asp:DataList>
</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

