﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="HomeRecentActivity.aspx.cs" Inherits="Public_HomeRecentActivity" Title="Home-Recent Activity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
 <div id='cssmenu'>
        <ul>
            <li><a href='ManagerHomeNew.aspx'>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DashBoard%>'>
            
                </asp:Literal>
            </a></li>
            <li class="selected"><a href='HomeRecentActivity.aspx'><span>
                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeActiveVacancies.aspx'><span>
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeNewHires.aspx'><span>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeAlerts.aspx'><span>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal>
            </span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
    <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/Recent_Activity.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b>
            
           <%-- Recent Activities --%>
           
           <asp:Literal ID="Literal1111" runat ="server" meta:resourcekey="RecentActivities">
           </asp:Literal>
            
            </b>
        </div>
    </div>
    <div style="float: left; width: 98%;">
        <asp:UpdatePanel ID="upRecentActivity" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <table width="98%" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="3" cellspacing="0" id="tblRecentActivityHeader" runat="server"
                                style="display: block;" align="right">
                                <tr>
                                    <td valign="top">
                                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName" Width="220px"
                                            DataValueField="CompanyID" CssClass="dropdownlist" AutoPostBack="True" 
                                            onselectedindexchanged="ddlCompany_SelectedIndexChanged" >
                                        </asp:DropDownList>
                                    </td> 
                                    <td valign="top">
                                        <asp:DropDownList ID="ddlVacancyActivity" runat="server" DataTextField="JobTitle"
                                            DataValueField="JobID" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlVacancyActivity_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCandidate" runat="server" DataTextField="CandidateName"
                                            DataValueField="CandidateId" CssClass="dropdownlist" AutoPostBack="True" OnSelectedIndexChanged="ddlCandidate_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRecentActivityFilter" runat="server" CssClass="dropdownlist"
                                            AutoPostBack="True" OnSelectedIndexChanged="ddlRecentActivityFilter_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <caption>
                        <br />
                        <tr>
                            <td style="padding-top: 5px;">
                                <asp:DataList ID="dlRecentActivity" runat="server" BorderWidth="0px" 
                                    CellPadding="0" CellSpacing="0" OnItemCommand="dlRecentActivity_ItemCommand" 
                                    OnPreRender="dlRecentActivity_PreRender" Width="100%">
                                    <HeaderTemplate>
                                        <div class="datalistheader" style="float: left; width: 98%;">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" width="100">
                                                        <%-- Date--%>
                                                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Date"> </asp:Literal>
                                                    </td>
                                                    <td align="left">
                                                        <%-- Activity--%>
                                                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Date"> </asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td class="Dashboradlink" width="100">
                                                    <b><%#SetDate(Eval("RecentActivityDate")) %></b>&nbsp;
                                                </td>
                                                <td class="Dashboradlink">
                                                    <%# 
                                                                                        GenerateRecentActivityTitle
                                                                                        (
                                                                                                Eval("EmployeeID"), 
                                                                                                Eval("JobID"),
                                                                                                Eval("JobTitle"),
                                                                                                Eval("CandidateId"),
                                                                                                Eval("CandidateName"),
                                                                                                Eval("CandidateNameArb"),
                                                                                                Eval("JobScheduleId"),
                                                                                                Eval("OfferID"),                                                           
                                                                                                Eval("RecentActivityTime"),
                                                                                                Eval("RecentActivityTypeId")
                                                                                        )
                                                %>, <%# Eval("RecentActivityTime")%>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 5px;">
                                <uc:Pager ID="pgrRecentActivity" runat="server" OnFill="BindRecentActivity" 
                                    PageSize="20" Visible="false" />
                            </td>
                        </tr>
                    </caption>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: none;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">

    <div style="display: block; width: 100%;" id="divCalendar">
        <div style="display: block; width: 100%; padding: 5px 2px 2px 2px; margin-left: 1px;">
            <asp:UpdatePanel ID="upcalender" runat="server">
                <ContentTemplate>
                    <asp:Calendar ID="Calendar5" Width="98%" runat="server" CellPadding="1"
                        NextMonthText="&amp;gt;&amp;gt;  " PrevMonthText="&amp;lt;&amp;lt;" 
                        BorderStyle="None">
                         <SelectedDayStyle ForeColor="Black" BorderColor="#0099FF" />
                        <TodayDayStyle CssClass="CalendarDay1" BorderColor="#CC0066" />
                        <DayStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <DayHeaderStyle CssClass="CalendarDay" />
                        <DayStyle CssClass="CalendarDay1" />
                        <SelectedDayStyle ForeColor="Red" />
                        <TitleStyle CssClass="CalendarDay1" />
                        <NextPrevStyle ForeColor="White" CssClass="CalendarNextPrev" />
                    </asp:Calendar>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
         <div style="float: left; font-size: 10px; padding: 2px 2px 2px 2px; width: 100%;">
                        <img src="../images/green.png" />
                        <%--Event--%> <asp:Literal ID="Literal81" runat ="server" Text='<%$Resources:ControlsCommon,Event%>'></asp:Literal>
                        <img src="../images/red.PNG" />
                        <%--Holiday--%><asp:Literal ID="Literal82" runat ="server" Text='<%$Resources:ControlsCommon,Holiday%>'></asp:Literal>
                        <img src="../images/golden.png" />
                        <%--Both--%> <asp:Literal ID="Literal9" runat ="server" Text='<%$Resources:ControlsCommon,Both%>'></asp:Literal>
                    </div>
    </div>
    
      <%-- *********Announcements*********--%>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" RenderMode="Inline">
            <ContentTemplate>
                <div style="width: 98%; border: 1px solid #b8b8b8; border-radius: 5px; height: auto;
                    margin-bottom: 5%; background: white; float: left; margin-top: 2%; box-shadow: 0 2px 2px rgba(105, 108, 109, 0.7), 0 0 2px 2px rgba(208, 223, 226, 0.4) inset;">
                    <div style="margin: auto; text-align: center; font-size: 16px; width: 95%; padding: 5px;
                        color: Black">
                        <p>
                            <%--Announcements--%><asp:Literal ID="litt" runat ="server" Text='<%$Resources:ControlsCommon,Announcements%>'></asp:Literal>
                            </p>
                    </div>
                    <div style="margin: auto; text-align: center; border-bottom: 2px solid #f6c94e; width: 90%;">
                    </div>
                    <div style="margin: auto; width: 99%;">
                        <div style="width: 90%; text-align: left; overflow: auto; height: 150px;" class="jobhead">
                            <marquee direction="up" height="150px" width="100%" id="mqOthersTarget" scrollamount="1"
                                class="announcement" onmouseover="this.setAttribute('scrollamount',0,0);" onmouseout="this.setAttribute('scrollamount',1,0);">
                                                                        <asp:Repeater ID="rptAnnouncements" runat="server">
                                                                          <ItemTemplate>
                                                                          <asp:HiddenField id="hfReferenceId" runat="server" value=' <%#Eval("ReferenceId")%>'></asp:HiddenField>
                                                                           <asp:HiddenField id="hfMessageTypeId" runat="server" value=' <%#Eval("MessageTypeId")%>'></asp:HiddenField>  
                                                                           <asp:HiddenField id="hfMessageID" runat="server" value=' <%#Eval("MessageID")%>'></asp:HiddenField>                                                                          
                                                                           <%#Eval("Message")%>                                                                      
                                      
                                                                       <br />    <br /> 
                                                                          </ItemTemplate>
                                                                        </asp:Repeater></marquee>
                        </div>
                    </div>
                </div>
                </ContentTemplate> 
                </asp:UpdatePanel> 
</asp:Content>