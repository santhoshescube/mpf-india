﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.Drawing;
using System.Web.UI.DataVisualization;
using System.Web.UI.DataVisualization.Charting;

/* Created By   :   Sruthy K
 * Created Date :   13 Oct 2013
 * Purpose      :   Sales Performance
 * */
public partial class Public_SalesPerformance : System.Web.UI.Page
{
    #region Declaration
    clsSalesPerformance MobjclsSalesPerformance;
    clsUserMaster objUser;
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        hfIsArabic.Value = clsGlobalization.IsArabicCulture().ToString();
        this.Title = GetLocalResourceObject("SalesPerformance.Text").ToString();
        PerformancePager.Fill += new controls_Pager.FillPager(BindGridView);
        if (!IsPostBack)
        {
            FillDropDowns(0);
            txtPeriod.Text = new clsCommon().GetSysDate().ToDateTime().ToString("MMM-yyyy", CultureInfo.InvariantCulture); 
            SetPermission();
            divView.Style["display"] = "none";
            upnlView.Update();
        }

        EnableDisableMenus();
    }
    #endregion

    #region Functions
    private void EnableDisableMenus()
    {
        if (ViewState["IsView"] != null && Convert.ToBoolean(ViewState["IsView"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }
    }

    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtPermission = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerfomanceTargetBased);

            if (dtPermission.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtPermission.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtPermission.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtPermission.Rows[0]["IsUpdate"].ToBoolean();
                ViewState["IsDelete"] = dtPermission.Rows[0]["IsDelete"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = ViewState["IsDelete"] = true;
        }

    }

    private void FillDropDowns(int Mode)
    {
        objUser = new clsUserMaster();
        if (Mode == 0)
        {
            ddlDepartment.DataTextField = "Department";
            ddlDepartment.DataValueField = "DepartmentID";
            ddlDepartment.DataSource = clsSalesPerformance.GetAllDepartments();
            ddlDepartment.DataBind();
            upnlDepartment.Update();
        }
        if (Mode == 0 || Mode == 1)
        {
            
            MobjclsSalesPerformance = new clsSalesPerformance();
            MobjclsSalesPerformance.CompanyID = objUser.GetCompanyId();
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataSource = clsSalesPerformance.GetAllEmployees(ddlDepartment.SelectedValue.ToInt32(), MobjclsSalesPerformance.CompanyID);
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem(GetGlobalResourceObject("ControlsCommon","Any").ToString(), "-1"));
            upnlEmployee.Update();
        }

    }

    private void View()
    {
        //divView.Style["display"] = "block";
        //AttendanceChart.Style["display"] = "none";

        PerformancePager.CurrentPage = 0;
        BindGridView();
    }

    private void BindGridView()
    {
        objUser = new clsUserMaster();
        MobjclsSalesPerformance = new clsSalesPerformance();
      
        PerformancePager.Total = clsSalesPerformance.GetTotalRecords(ddlDepartment.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt64());
        MobjclsSalesPerformance = new clsSalesPerformance();
        MobjclsSalesPerformance.DepartmentID = ddlDepartment.SelectedValue.ToInt32();
        MobjclsSalesPerformance.PageIndex = PerformancePager.CurrentPage + 1;
        MobjclsSalesPerformance.PageSize = PerformancePager.PageSize;
        MobjclsSalesPerformance.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
        MobjclsSalesPerformance.CompanyID = objUser.GetCompanyId();
        DataTable dtPerformance = MobjclsSalesPerformance.GetEmployees();
        if (dtPerformance.Rows.Count > 0)
        {
            divView.Style["display"] = "block";
            dgvPerformance.DataSource = dtPerformance;
            dgvPerformance.DataBind();
            PerformancePager.Visible = true;
            
        }
        else
        {
            PerformancePager.Visible = false;
            divView.Style["display"] = "none";
            dgvPerformance.DataSource = null;
            dgvPerformance.DataBind();
            ucMessage.InformationalMessage(GetLocalResourceObject("NoInformationFound.Text").ToString());
            mpeMessage.Show();
            return;
        }
        upnlView.Update();
    }

    private bool Delete()
    {
        MobjclsSalesPerformance = new clsSalesPerformance();

        foreach (GridViewRow Row in dgvPerformance.Rows)
        {
            if (Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnPerformanceID = Row.FindControl("hdnPerformanceID") as HiddenField;
                if (hdnPerformanceID.Value.ToInt32() > 0)
                {
                    MobjclsSalesPerformance.lstPerformance.Add(new clsSalesPerformance() { PerformanceID = hdnPerformanceID.Value.ToInt32() });
                }
            }
        }
        if (MobjclsSalesPerformance.lstPerformance.Count > 0)
        {
            return MobjclsSalesPerformance.Delete();
        }
        return true;
    }

    private void Save()
    {
        MobjclsSalesPerformance = new clsSalesPerformance();

        foreach (GridViewRow Row in dgvPerformance.Rows)
        {
            if (Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnEmployeeID = Row.FindControl("hdnEmployeeID") as HiddenField;
                TextBox txtProjection = Row.FindControl("txtProjection") as TextBox;
                TextBox txtActual = Row.FindControl("txtActual") as TextBox;

                if (hdnEmployeeID != null && txtProjection != null && txtActual != null)
                {
                    if (txtProjection.Text.ToDecimal() > 0)
                        MobjclsSalesPerformance.lstPerformance.Add(new clsSalesPerformance() { EmployeeID = hdnEmployeeID.Value.ToInt64(), Projection = txtProjection.Text.ToDecimal(), Actual = txtActual.Text.ToDecimal(), FromDate = clsCommon.Convert2DateTime(txtPeriod.Text), ToDate = GetMonthLastDate(clsCommon.Convert2DateTime(txtPeriod.Text)) });
                }
            }
        }
        if (MobjclsSalesPerformance.lstPerformance.Count > 0)
        {
            if (MobjclsSalesPerformance.Save())
            {
                ucMessage.InformationalMessage(GetLocalResourceObject("SavedSuccessfully.Text").ToString());
                mpeMessage.Show();
                View();
            }
        }

    }

    private DateTime GetMonthLastDate(DateTime Date)
    {
        DateTime MonthLastDate = Date.AddDays((DateTime.DaysInMonth(Date.Year, Date.Month) - 1).ToDouble());
        return MonthLastDate;
    }

    private void BindChart()
    {
        MobjclsSalesPerformance = new clsSalesPerformance();
        MobjclsSalesPerformance.EmployeeID = hdnSelectedEmployee.Value.ToInt64();
        MobjclsSalesPerformance.FromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
        MobjclsSalesPerformance.ToDate = clsCommon.Convert2DateTime(txtToDate.Text);

        DataTable dtChart = MobjclsSalesPerformance.GetDetailsForChart();

        if (dtChart.Rows.Count > 0)
        {
            string yValues = string.Format("{0}", Convert.ToString(dtChart.Rows[0]["Projection"]));
            string y1Values = string.Format("{0}", Convert.ToString(dtChart.Rows[0]["Actual"]));

            divImage.Style["display"] = "block";
            imgChart.Src = string.Format("SalesTargetChart.aspx?Height=400&Width=400&yValues={0}&y1Values={1}&IsFromHomePage={2}&Type={3}&Docking={4}",yValues,y1Values, "False", System.Web.UI.DataVisualization.Charting.SeriesChartType.Column.ToString(), System.Web.UI.DataVisualization.Charting.Docking.Bottom);
            divNoImage.Style["display"] = "none";
        }
        else
        {
            imgChart.Src = "";
            divImage.Style["display"] = "none";
            divNoImage.Style["display"] = "block";
        }
        upnlChart.Update();
    }

    #endregion

    #region Events
    protected void btnShow_Click(object sender, EventArgs e)
    {

            divView.Visible = PerformancePager.Visible  = true;
            View();
            upnlView.Update();

    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDepartment.SelectedValue.ToInt32() > 0)
        {
            FillDropDowns(1);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        BindGridView();
    }

    protected void btnViewChart_Click(object sender, EventArgs e)
    {
        BindChart();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Delete())
        {
            Save();
        }
    }

    protected void dgvPerformance_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox txtProjection = e.Row.FindControl("txtProjection") as TextBox;
            TextBox txtActual = e.Row.FindControl("txtActual") as TextBox;
            HiddenField hdnEmployeeID = e.Row.FindControl("hdnEmployeeID") as HiddenField;
            HiddenField hdnPerformanceID = e.Row.FindControl("hdnPerformanceID") as HiddenField;
            ImageButton imgbtnDelete = e.Row.FindControl("imgbtnDelete") as ImageButton;

            if (txtProjection != null && txtActual != null && hdnEmployeeID != null && hdnPerformanceID != null && imgbtnDelete != null)
            {
                DataTable dtPerformance = clsSalesPerformance.GetPerformanceDetails(hdnEmployeeID.Value.ToInt64(), clsCommon.Convert2DateTime(txtPeriod.Text));
                if (dtPerformance.Rows.Count > 0)
                {
                    txtProjection.Text = Convert.ToString(dtPerformance.Rows[0]["Projection"]);
                    txtActual.Text = Convert.ToString(dtPerformance.Rows[0]["Actual"]);
                    hdnPerformanceID.Value = Convert.ToString(dtPerformance.Rows[0]["PerformanceID"]);
                    imgbtnDelete.Enabled = Convert.ToBoolean(ViewState["IsDelete"]) == true ? true : false;
                }
                else
                {
                    txtProjection.Text = txtActual.Text = string.Empty;
                    imgbtnDelete.Enabled = false;
                }
                if(!imgbtnDelete.Enabled)
                    imgbtnDelete.ImageUrl = "~/images/delete_popup_disable.png";
            }
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            Button btnSubmit = (Button)e.Row.FindControl("btnSubmit");
            if (btnSubmit != null)
            {
                if (Convert.ToBoolean(ViewState["IsCreate"]) || Convert.ToBoolean(ViewState["IsUpdate"]))
                    btnSubmit.Enabled = true;
                else
                    btnSubmit.Enabled = false;
            }
        }
        
    }

    protected void dgvPerformance_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow Row = null;
        switch (e.CommandName)
        {
            case "DeletePerformance":
                ImageButton imgDelete = (ImageButton)e.CommandSource;
                Row = (GridViewRow)imgDelete.Parent.Parent;
                HiddenField hdnPerformanceID = Row.FindControl("hdnPerformanceID") as HiddenField;
                if (hdnPerformanceID != null)
                {
                    MobjclsSalesPerformance = new clsSalesPerformance();
                    MobjclsSalesPerformance.lstPerformance.Add(new clsSalesPerformance() { PerformanceID = hdnPerformanceID.Value.ToInt32() });
                    if (MobjclsSalesPerformance.Delete())
                    {
                      //  ucMessage.InformationalMessage("Deleted Successfully");
                        ucMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon","DeletedSuccessfully").ToString());
                        mpeMessage.Show();
                        View();
                    }
                }
                break;

            case "ViewChart":
                ImageButton imgViewChart = (ImageButton)e.CommandSource;
                Row = (GridViewRow)imgViewChart.Parent.Parent;
                HiddenField hdnEmployeeID = (HiddenField)Row.FindControl("hdnEmployeeID");
                Label lblEmployee = (Label)Row.FindControl("lblEmployee");
                hdnSelectedEmployee.Value = hdnEmployeeID.Value;
                txtFromDate.Text = txtToDate.Text = txtPeriod.Text;
                lblSelectedEmployee.Text = lblEmployee.Text;
                BindChart();
                upnlChart.Update();
                mpeChart.Show();

                break;
        }
    }

    #endregion

}
