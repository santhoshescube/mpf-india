﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="PerformanceEvaluation.aspx.cs" Inherits="Public_PerformanceEvaluation"
    Title="Performance Evaluation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <%--  <ul>
            <li><a href="SalesPerformance.aspx">Target Performance</a></li>
            <li><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
            <li><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li class='selected'><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li><a href="PerformanceAction.aspx">Performance Action</a></li>
        </ul>--%>
        <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li class='selected'><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
function ValidateSave()
{
    Page_ClientValidate();
    if(Page_IsValid)
    {
        var hfIsArabic = document.getElementById('<%=hfIsArabic.ClientID %>').value;
        if(hfIsArabic =='True')
            return  confirm(' هل أنت متأكد أنك تريد حفظ النتيجة؟');
        else
            return  confirm('Are you sure you want to save the result ?');
     }
    else 
        return false 
}

    </script>

    <div id="dvHavePermission" runat="server">
        <asp:UpdatePanel ID="upd" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hfIsGrade" runat="server" />
                <asp:HiddenField ID="hfIsArabic" runat="server" />
                <asp:HiddenField ID="hfTemplateID" runat="server" />
                <asp:HiddenField ID="hfEmployeeID" runat="server" />
                <asp:HiddenField ID="hfPerformanceInitiationID" runat="server" />
                <asp:HiddenField ID="hfPerformanceInitiationEvaluationID" runat="server" />
                <div style="width: 100%">
                    <div style="float: left; width: 80%; margin: 7px; margin-bottom: 0px">
                        <h4 id="Header" runat="server" style="color: #30a5d6">
                            <%-- Evaluation pending List :--%>
                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="EvaluationPendingList">
                            </asp:Literal>
                        </h4>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="margin-top: 10px; min-width: 100%; height: 550px; overflow: scroll">
                        <div id="dvEmployeeList" runat="server">
                            <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlEmployeeList" Width="100%"
                                DataKeyField="PerformanceInitiationEvaluationID" runat="server" BackColor="White"
                                BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Table"
                                CellPadding="3" GridLines="Both" OnItemCommand="dlEmployeeList_ItemCommand" OnItemDataBound="dlEmployeeList_ItemDataBound">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                                <SeparatorStyle BackColor="AliceBlue" />
                                <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 60px; vertical-align: top">
                                                <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                    Text='<%$Resources:ControlsCommon,SlNo %>'></asp:Label>
                                            </td>
                                            <td style="width: 350px; vertical-align: top">
                                                <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                    Text='<%$Resources:ControlsCommon,Employee%>'></asp:Label>
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="Label3" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,FromDate %>'></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="Label4" CssClass="labeltext" runat="server" ForeColor="White" Text='<%$Resources:ControlsCommon,ToDate %>'></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 60px; vertical-align: top">
                                                <asp:Label ID="lblSlNo" CssClass="labeltext" runat="server" ToolTip='<%# Eval("SLNo") %>'
                                                    Text='<%# Eval("SLNo") %>'></asp:Label>
                                            </td>
                                            <td style="width: 350px; vertical-align: top">
                                                <asp:Label ID="lblEmployee" CssClass="labeltext" runat="server" ToolTip='<%# Eval("EmployeeFullName") %>'
                                                    Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                <asp:HiddenField ID="hfEmployee" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                                <asp:HiddenField ID="hfPerformanceInitiationID" runat="server" Value='<%# Eval("PerformanceInitiationID") %>' />
                                                <asp:HiddenField ID="hfTemplateID" runat="server" Value='<%# Eval("TemplateID") %>' />
                                            </td>
                                            <td style="width: 130px; vertical-align: top">
                                                <asp:Label ID="lblFromDate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("FromDate") %>'
                                                    Text='<%# Eval("FromDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 100px; vertical-align: top">
                                                <asp:Label ID="lblToDate" CssClass="labeltext" runat="server" ToolTip='<%# Eval("ToDate") %>'
                                                    Text='<%# Eval("ToDate") %>'></asp:Label>
                                            </td>
                                            <td style="width: 18px">
                                                <asp:HiddenField Value='<%# Eval("EmployeeFullName") %>' ID="hfEmployeeFullName"
                                                    runat="server" />
                                                <asp:Button Height="25px" Width="65px" ID="btnEvaluate" runat="server" meta:resourcekey="Evaluate"
                                                    CausesValidation="False" CssClass="btnsubmit" CommandName="Evaluate" CommandArgument='<%# Eval("TemplateID")+"@"+Eval("PerformanceInitiationID")+"^"+Eval("EmployeeID") %>'
                                                    OnClick="btnEvaluate_Click1" />
                                                <asp:HiddenField ID="hfPerformanceInitiationEvaluationID" runat="server" Value='<%# Eval("PerformanceInitiationEvaluationID") %>' />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div id="dvNoRecord" runat="server" style="text-align: center">
                            <asp:Label ID="lblNoRecord" CssClass="error " runat="server" Text='<%$Resources:ControlsCommon,NoRecordFound %>'></asp:Label>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="updGoal" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AjaxControlToolkit:ModalPopupExtender Drag="true" PopupControlID="popupmain1" TargetControlID="btn"
                    ID="mdGoal" runat="server">
                </AjaxControlToolkit:ModalPopupExtender>
                <asp:Button ID="btn" runat="server" Style="display: none" />
                <div id="popupmain1" style="width: 70%; height: auto; overflow: auto; background-color: White">
                    <div id="header11" style="width: 100%; background: gray">
                        <div id="hbox1">
                            <div id="headername">
                                <asp:Label ID="lblHeading" runat="server" meta:resourcekey="SetMark"></asp:Label>
                            </div>
                        </div>
                        <div id="hbox2">
                            <div id="close1">
                                <a href="">
                                    <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png"
                                        OnClick="ibtnClose_Click" CausesValidation="true" ValidationGroup="dummy_not_using" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%; overflow: hidden;">
                        <div style="display: block; border: solid 1px black">
                            <div style="min-width: 100%; height: 50%; overflow: auto">
                                <asp:DataList SeparatorStyle-BackColor="AliceBlue" ID="dlGoal" Width="100%" runat="server"
                                    DataKeyField="EvaluationDetailID" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"
                                    BorderWidth="1px" CellPadding="3" GridLines="Both" OnItemDataBound="dlGoal_ItemDataBound">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                    <SeparatorStyle BackColor="AliceBlue" />
                                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td style="width: 60px; vertical-align: top">
                                                    <asp:Label ID="lblSlNoHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                        Text='<%$Resources:ControlsCommon,SlNo %>'></asp:Label>
                                                </td>
                                                <td style="width: 740px; vertical-align: top">
                                                    <asp:Label ID="lblEmployeeHeader" CssClass="labeltext" runat="server" ForeColor="White"
                                                        meta:resourcekey="Goal"></asp:Label>
                                                </td>
                                                <td style="width: 18px">
                                                    <asp:Label ID="Label1" CssClass="labeltext" runat="server" ForeColor="White" meta:resourcekey='Mark'></asp:Label>
                                                </td>
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td style="width: 60px; vertical-align: top">
                                                    <asp:Label ID="lblSlNo" CssClass="labeltext" runat="server" ToolTip='<%# Eval("SLNo") %>'
                                                        Text='<%# Eval("SLNo") %>'></asp:Label>
                                                </td>
                                                <td style="width: 740px; vertical-align: top">
                                                    <asp:Label ID="lblGoal" CssClass="labeltext" runat="server" ToolTip='<%# Eval("Goal") %>'
                                                        Text='<%# Eval("Goal") %>'></asp:Label>
                                                    <asp:HiddenField ID="hfGoalID" runat="server" Value='<%# Eval("GoalID") %>' />
                                                    <asp:HiddenField ID="hfEvaluationDetailID" runat="server" Value='<%# Eval("EvaluationDetailID") %>' />
                                                    <%-- <asp:HiddenField ID="hfPerformanceInitiationID" runat="server" Value ='<%# Eval("PerformanceInitiationID") %>' />
                                      <asp:HiddenField ID="hfTemplateID" runat="server" Value ='<%# Eval("TemplateID") %>' />
                              --%>
                                                </td>
                                                <td style="width: 18px">
                                                    <asp:DropDownList ID="ddlMark" ValidationGroup="test" Height="25px" Width="100px"
                                                        runat="server" CssClass="btnsubmit" />
                                                    <asp:RequiredFieldValidator ID="rf" runat="server" ControlToValidate="ddlMark" InitialValue="-1"
                                                        meta:resourcekey="SelectMark" CssClass="error" ValidationGroup="test"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both; height: 100%" id="footerwrapper">
                        <div id="footer11" style="width: 100%">
                            <div style="float: right; width: 14%; margin-top: 5px">
                                <asp:Button ID="btnsave" Width="50px" ValidationGroup="test" OnClientClick="return ValidateSave();"
                                    runat="server" Text='<%$Resources:ControlsCommon,Save%>' CssClass="btnsubmit"
                                    OnClick="save_Click" />
                                <asp:Button ID="btnCancel" Width="50px" runat="server" Text='<%$Resources:ControlsCommon,Cancel%>'
                                    CssClass="btnsubmit" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="dvNoPermission" runat="server" style="margin-top: 10px; display: none; text-align: center;
        width: 100%">
        <asp:Label ID="lblNoPermission" runat="server" CssClass="error" Text="You have no permission to view this page">
        </asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/add_performance_evaluation.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkPendingEvaluations" Text='<%$Resources:ControlsCommon,NewEvaluation %>'
                            runat="server" CausesValidation="false"> 
	         <%--       New Evaluation--%>
	                
	             
	                
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/view_performance_evaluation.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkCompletedEvaluation" runat="server" Text='<%$Resources:ControlsCommon,ViewEvaluation %>'
                            CausesValidation="false" OnClick="lnkCompletedEvaluation_Click"> <h5>
	              <%--  View Evaluation--%>
	                
                        </asp:LinkButton></h5>
                </div>
                </a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
