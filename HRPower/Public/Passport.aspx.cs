﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using System.Text;


public partial class Public_Passport : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsEmployeePassport objEmployeePassport;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;

    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button
    private string CurrentSelectedValue { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterAutoComplete();
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        if (!IsPostBack)
        {
            objEmployeePassport = new clsEmployeePassport();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
          
            BindCombos();
            SetPermission();
            EnableMenus();
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            pgrEmployees.Visible = false;
            lblNoData.Visible = false;

            txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

            getPassportList();
        }

        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    #region Issue Receipt
    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>" + Result + "</h5>";

        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        lblCurrentStatus.Text = "Issued";
        //    }

        //    upMenu.Update();           
        //}
    }

    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hfMode.Value == "Edit")
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            
            int intOperationTypeID = (int)OperationType.Employee, intDocType;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdPassportID.Value.ToInt32();
            intDocType = (int)DocumentType.Passport;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtPassport.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            getPassportList();
        }
        else
            return;
    }

    #endregion Issue Receipt
    protected void Bind()
    {
        objEmployeePassport = new clsEmployeePassport();
        objUser = new clsUserMaster();

        objEmployeePassport.PageIndex = pgrEmployees.CurrentPage + 1;
        objEmployeePassport.PageSize = pgrEmployees.PageSize;
        objEmployeePassport.SearchKey = txtSearch.Text;
        objEmployeePassport.CompanyID = objUser.GetCompanyId();
        if (ddlExpression.SelectedValue.ToInt32() > 0)
            objEmployeePassport.SortExpression = ddlExpression.SelectedValue.ToInt32();
        else
            objEmployeePassport.SortExpression = 1;

        switch (ddlOrder.SelectedValue.ToInt32())
        {
            case 1:
                objEmployeePassport.SortOrder = "ASC";
                break;
            case 2:
                objEmployeePassport.SortOrder = "DESC";
                break;
            default:
                objEmployeePassport.SortOrder = "DESC";
                break;
        } 
        pgrEmployees.Total = objEmployeePassport.GetCount();
        DataSet ds = objEmployeePassport.GetAllEmployeeSalaryStructures();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlEmployeepassport.DataSource = ds;
            dlEmployeepassport.DataBind();         
            pgrEmployees.Visible = true;
            lblNoData.Visible = false;
            divSort.Style["display"] = "block";
        }
        else
        {
            dlEmployeepassport.DataSource = null;
            dlEmployeepassport.DataBind();
            pgrEmployees.Visible = false;
            lblNoData.Visible = true;
            divSort.Style["display"] = "none";
            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoDocuments").ToString();
            else
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();
        }

        fvEmployees.Visible = false;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('Employee');");
        upnlNoData.Update();
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
        else
            return string.Empty;

    }

    private void BindCombos()
    {
        objEmployee = new clsEmployee();
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        objEmployee.CompanyID = objUser.GetCompanyId();
        ddlCompany.DataSource = objEmployee.GetAllCompanies();
        ddlCompany.DataBind();

        if (ddlCompany.Items.Count == 0)
            ddlCompany.Items.Add(new ListItem(GetGlobalResourceObject("DocumentsCommon","Select").ToString(), "-1"));

        ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);

       // if (!(ddlCompany.Enabled))
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

        ddlCompany.Enabled = false;

        objEmployeePassport.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);
        FillCountry();
    }

    public void FillCountry()
    {
        objEmployeePassport = new clsEmployeePassport();
        ddlCountryOfOrigin.DataSource = objEmployeePassport.FillCountry();
        ddlCountryOfOrigin.DataBind();

        if (CurrentSelectedValue != null && CurrentSelectedValue != string.Empty)
        {
            if(ddlCountryOfOrigin.Items.FindByValue(CurrentSelectedValue) != null)
                ddlCountryOfOrigin.SelectedValue = CurrentSelectedValue;
        }

        updCountryOfOrigin.Update();
    }

    private void FillEmployees(long lngEmployeeID)
    {
        if (objEmployeePassport == null)
            objEmployeePassport = new clsEmployeePassport();

        ddlEmployee.DataSource = objEmployeePassport.FillEmployees(lngEmployeeID);
        ddlEmployee.DataBind();

        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem(GetGlobalResourceObject("DocumentsCommon", "Select").ToString(), "-1"));
    }

    protected void imgCountryOfOrigin_Click(object sender,EventArgs e)
    {        
        if (ddlCountryOfOrigin != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "FillCountry";
            ReferenceControlNew1.SelectedValue = ddlCountryOfOrigin.SelectedValue;
            ReferenceControlNew1.DisplayName = GetGlobalResourceObject("DocumentsCommon", "Country").ToString();
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
            updCountryOfOrigin.Update();
        }
    }

    protected void NewPassport()
    {
        ControlEnableDisble(true);
        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        divPassDoc.Style["display"] = "none";
        fvPassport.Visible = false;
        chkPagesAvailable.Checked = false;
        hdPassportID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        pgrEmployees.Visible = false;
        objEmployeePassport = new clsEmployeePassport();
        fvEmployees.Visible = true;
        txtnameinpassport.Text = string.Empty;
        txtPassport.Text = string.Empty;
        txtPlaceofIssue.Text = string.Empty;
        txtplaceofbirth.Text = string.Empty;
        txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
        chkPagesAvailable.Checked = false;
        lblCurrentStatus.Text = GetGlobalResourceObject("documentsCommon", "New").ToString();
        objEmployeePassport.PassportID = -1;
        objEmployeePassport.EmployeeID = -1;

        DataTable dtpassportdoc = objEmployeePassport.GetPassportDocuments().Tables[0];
        dlPassportDoc.DataSource = dtpassportdoc;
        dlPassportDoc.DataBind();
        ViewState["PasDoc"] = dtpassportdoc;
        txtDocname.Text = string.Empty;

        divResidencePermit.Style["display"] = "block";
        divOldPassport.Style["display"] = "none";
        divEntryRestrictions.Style["display"] = "none";

        txtResidencePermit.Text = string.Empty;
        txtOldPassport.Text = string.Empty;
        txtEntryRestrictions.Text = string.Empty; 
        ddlRemarks.SelectedValue = "1";
        upnlRemarks.Update();

        objEmployeePassport.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);
        dlEmployeepassport.DataSource = null;
        dlEmployeepassport.DataBind();
        upnlNoData.Update();

        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";
        upMenu.Update();
    }

    protected void lnkListEmp_Click(object sender, EventArgs e)
    {
        getPassportList();        
    }

    private void getPassportList()
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        txtSearch.Text = string.Empty;
        pgrEmployees.CurrentPage = 0;
        fvPassport.Visible = false;
        fvPassport.DataSource = null;
        fvPassport.DataBind();
        Bind();
        hfMode.Value = "List";
        EnableMenus();
        upEmployeePassport.Update();
    }

    protected void lnkAddPass_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        NewPassport();
        txtSearch.Text = "";
        upEmployeePassport.Update();
        divSort.Style["display"] = "none";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {        
        int id = 0;
        try
        { 
            objAlert = new clsDocumentAlert();
            objEmployeePassport = new clsEmployeePassport();

            objEmployeePassport.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objEmployeePassport.NameinPassport = Convert.ToString(txtnameinpassport.Text.Trim());
            objEmployeePassport.PassportNumber = Convert.ToString(txtPassport.Text.Trim());
            objEmployeePassport.PlaceofIssue = Convert.ToString(txtPlaceofIssue.Text.Trim());
            objEmployeePassport.CountryID = Convert.ToInt32(ddlCountryOfOrigin.SelectedValue);
            objEmployeePassport.PlaceOfBirth = Convert.ToString(txtplaceofbirth.Text);
            objEmployeePassport.IssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text);
            objEmployeePassport.ExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text);
            objEmployeePassport.PagesAvailable = chkPagesAvailable.Checked;
            objEmployeePassport.ResidencePermit = txtResidencePermit.Text.Trim();
            objEmployeePassport.OldPassport = txtOldPassport.Text.Trim();
            objEmployeePassport.EntryRestrictions = txtEntryRestrictions.Text.Trim();

            if (hfMode.Value == "Insert")
            {
                objEmployeePassport.PassportID = 0;

                if (objEmployeePassport.IsPassportNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("PassportNumberexists.Text").ToString());
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objEmployeePassport.BeginEmp();
                        id = objEmployeePassport.InsertEmployeePassport();
                        objEmployeePassport.PassportID = id;
                        objAlert.AlertMessage((int)DocumentType.Passport, "Passport", "PassportNumber", id, txtPassport.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Emp", Convert.ToInt32(ddlEmployee.SelectedValue), ddlEmployee.SelectedItem.Text, false);
                        objEmployeePassport.Commit();
                    }
                    catch
                    {
                        objEmployeePassport.RollBack();
                    }
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objEmployeePassport.PassportID = hdPassportID.Value.ToInt32();

                if (objEmployeePassport.IsPassportNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("PassportNumberexists.Text").ToString());
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objEmployeePassport.BeginEmp();
                        id = objEmployeePassport.UpdateEmployeePassport();
                        objEmployeePassport.PassportID = id;                        
                        objEmployeePassport.Commit();
                    }
                    catch (Exception ex)
                    {
                        objEmployeePassport.RollBack();
                    }
                }
            }

            if (id == 0) return;            
            Label lblDocname, lblActualfilename, lblFilename;

            foreach (DataListItem item in dlPassportDoc.Items)
            {
                lblDocname = (Label)item.FindControl("lblDocname");
                lblFilename = (Label)item.FindControl("lblFilename");
                lblActualfilename = (Label)item.FindControl("lblActualfilename");
                objEmployeePassport.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objEmployeePassport.PassportID = id;
                objEmployeePassport.PassportNumber = Convert.ToString(txtPassport.Text.Trim());
                objEmployeePassport.Docname = lblDocname.Text.Trim();
                objEmployeePassport.Filename = lblFilename.Text.Trim();

                if (dlPassportDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                {
                    objEmployeePassport.InsertEmployeePassportTreemaster();
                    
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";
                    clsCommon.WriteToLog("Physical path is" + Path);
                    if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                    {
                        if (!Directory.Exists(Path))
                            Directory.CreateDirectory(Path);
                        clsCommon.WriteToLog("Directory is created");
                        File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                        clsCommon.WriteToLog("File moved successfully");
                    }
                }
            }

            // Move attachment to folder
            if (hfMode.Value == "Insert")
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon","savedsuccessfully").ToString());
            else
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon","UpdatedSuccessfully").ToString());

            mpeMessage.Show();
            divIssueReceipt.Style["display"] = "block";
            divRenew.Style["display"] = "block";
            hfMode.Value = "Edit";
            hdPassportID.Value = objEmployeePassport.PassportID.ToStringCustom();
            EnableMenus();
            upMenu.Update();
            upEmployeePassport.Update();
            getPassportList();
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        { 
            if (hfMode.Value == "Edit")
            {
                int id = 0;
                objAlert = new clsDocumentAlert();
                objEmployeePassport = new clsEmployeePassport();
                objEmployeePassport.PassportID = hdPassportID.Value.ToInt32();
                try
                {
                    objEmployeePassport.BeginEmp();
                    id = objEmployeePassport.UpdateRenewEmployeePassport();
                    objEmployeePassport.Commit();
                }
                catch (Exception ex)
                {
                    objEmployeePassport.RollBack();
                }

                objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.Passport), id);

                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "RenewedSuccessfully").ToString());
                mpeMessage.Show();
                divIssueReceipt.Style["display"] = "block";
                divRenew.Style["display"] = "block";
                hfMode.Value = "Edit";
                hdPassportID.Value = objEmployeePassport.PassportID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                upEmployeePassport.Update();
                getPassportList();
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();       

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Passport);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];

        if (dtm.Rows.Count > 0)
        {
            lnkRenew.Enabled = lnkAddPass.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            lnkListPass.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dtm.Rows[0]["IsUpdate"].ToBoolean();
        }

        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();

        if (UserID > 3)
        {
            if (lnkListPass.Enabled)
            {
                Bind();
                ImgSearch.Enabled = true;
            }
            else if (lnkAddPass.Enabled)
                NewPassport();
            else
            {
                pgrEmployees.Visible = false;
                dlEmployeepassport.DataSource = null;
                dlEmployeepassport.DataBind();
                fvEmployees.Visible = false;
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Visible = true;
                ImgSearch.Enabled = false;
            }
        }

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlEmployeepassport.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeepassport.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeepassport.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlEmployeepassport.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }

    private void BindViewMode(int PassportID)
    {
        dlEmployeepassport.DataSource = null;
        dlEmployeepassport.DataBind();
        pgrEmployees.Visible = false;
        fvEmployees.Visible = false;
        fvPassport.Visible = true;
        hfMode.Value = "List";
        objEmployeePassport.PassportID = PassportID;
        hdPassportID.Value = Convert.ToString(PassportID);
        fvPassport.DataSource = objEmployeePassport.GetEmpSalaryStructure();
        fvPassport.DataBind();
        divPrint.InnerHtml = PrintDetails(PassportID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = sPrinttbl;
            if(lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return showMailDialog('Type=Passport&PassportId=" + Convert.ToString(fvPassport.DataKey["PassportID"]) + "');";
            if(lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return confirm('" +GetGlobalResourceObject("DocumentsCommon","Areyousuretodeletetheselecteditems").ToString() + "');";
            if(lnkRenew.Enabled)
                lnkRenew.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretorenewtheselecteditems").ToString() + "');";
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objEmployeePassport = new clsEmployeePassport();
        CheckBox chkEmployee;
        LinkButton btnEmployee;
        Label lblIsDelete;
        string message = string.Empty;
        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

        if (dlEmployeepassport.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployeepassport.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    objEmployeePassport.PassportID = Convert.ToInt32(dlEmployeepassport.DataKeys[item.ItemIndex]);

                    DataTable dt = objEmployeePassport.GetPassportFilenames();
                    objEmployeePassport.Delete();
                    objAlert = new clsDocumentAlert();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }

                    message = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString(); 
                }
                else
                    message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString(); 
            }
        }
        else
        {
            if (hfMode.Value == "List")
            {
                objEmployeePassport.PassportID = Convert.ToInt32(fvPassport.DataKey["PassportID"]);

                if (Convert.ToBoolean(fvPassport.DataKey["IsDelete"]))
                {
                    DataTable dt = objEmployeePassport.GetPassportFilenames();
                    objEmployeePassport.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }

                    message = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString(); ;
                }
                else
                    message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString(); 
            }
        }

        if (message == "")
            message = GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString();

        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        getPassportList();      
        upEmployeePassport.Update();
    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        if (dlEmployeepassport.Items.Count > 0)
        {
            objEmployeePassport = new clsEmployeePassport();
            string sPassportIds = string.Empty;

            if (fvPassport.DataKey["PassportID"] == DBNull.Value || fvPassport.DataKey["PassportID"] == null)
            {
                CheckBox chkEmployee;

                foreach (DataListItem item in dlEmployeepassport.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sPassportIds += "," + dlEmployeepassport.DataKeys[item.ItemIndex];
                }

                sPassportIds = sPassportIds.Remove(0, 1);
            }
            else
                sPassportIds = fvPassport.DataKey["PassportID"].ToString();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Passport&PassportId=" + sPassportIds + "', 835, 585);", true);
        }
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        if (dlEmployeepassport.Items.Count > 0)
        {
            objEmployeePassport = new clsEmployeePassport();

            if (fvPassport.DataKey["PassportID"] == DBNull.Value || fvPassport.DataKey["PassportID"] == null)
            {
                CheckBox chkEmployee;
                string sPassportIds = string.Empty;

                foreach (DataListItem item in dlEmployeepassport.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sPassportIds += "," + dlEmployeepassport.DataKeys[item.ItemIndex];
                }

                sPassportIds = sPassportIds.Remove(0, 1);

                if (sPassportIds.Contains(","))
                    divPrint.InnerHtml = PrintList(sPassportIds);
                else
                    divPrint.InnerHtml = PrintDetails(Convert.ToInt32(sPassportIds));
            }
            else
                divPrint.InnerHtml = PrintDetails(Convert.ToInt32(fvPassport.DataKey["PassportID"]));

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }   
    }

    protected void dlEmployeepassport_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        objEmployeePassport = new clsEmployeePassport();
        objUser = new clsUserMaster();

        switch (e.CommandName)
        {
            case "ALTER":
                NewPassport();
                hfMode.Value = "Edit";
                objEmployeePassport.PassportID = Convert.ToInt32(e.CommandArgument);
                hdPassportID.Value = Convert.ToString(e.CommandArgument);

                upnlRemarks.Update();
                lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
                lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

                BindFormView(Convert.ToInt32(e.CommandArgument));
                txtSearch.Text = string.Empty;
                divSort.Style["display"] = "none";
                break;
            case "VIEW":
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                divIssueReceipt.Style["display"] = "none";
                divRenew.Style["display"] = "none";
                divSort.Style["display"] = "none";
                break;
        }
    }

    protected void dlEmployeepassport_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }

    private void BindFormView(int PassportID)
    {
        fvEmployees.Visible = true;
        objEmployeePassport.PassportID = PassportID;

        DataTable dt = objEmployeePassport.GetEmployeeSalaryStructure().Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrEmployees.Visible = false;
        BindPassportDetails(dt);
        dlEmployeepassport.DataSource = null;
        dlEmployeepassport.DataBind();
        divPrint.InnerHtml = PrintDetails(PassportID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            lnkPrint.OnClientClick = sPrinttbl;
            lnkEmail.OnClientClick = "return showMailDialog('Type=Passport&PassportId=" + Convert.ToString(fvPassport.DataKey["PassportID"]) + "');";
            lnkDelete.OnClientClick = "return true;";
            lnkRenew.OnClientClick = "return true;";
        }

        if (Convert.ToInt32(dt.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";

        if (Convert.ToBoolean(dt.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            divRenew.Style["display"] = "none";
            if (clsGlobalization.IsArabicCulture())
                lblCurrentStatus.Text += " ] " +  GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
            else
                lblCurrentStatus.Text += " [ " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
         
            ControlEnableDisble(false);
        }
        else
        {
            divRenew.Style["display"] = "block";
            ControlEnableDisble(true);
        }
        
        upMenu.Update();
    }

    public string PrintList(string sPassportIds)
    {
        StringBuilder sb = new StringBuilder();

        DataTable dt = objEmployeePassport.CreateSelectedEmployeesPrintContent(sPassportIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("PassportInfo.Text") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee") + " </td><td width='150px'>" + GetLocalResourceObject("PassportNumber.Text") + "</td><td width='150px'>" + GetLocalResourceObject("NameInPassport.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Country") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon","PlaceofIssue") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td width='100px'>" + GetLocalResourceObject("OldPassport.Text") + "</td><td width='100px'>" + GetLocalResourceObject("EntryRestrictions.Text") + "</td><td width='100px'>" + GetLocalResourceObject("Residence.Text") + "</td></tr>");
        sb.Append("<tr><td colspan='10'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PassportNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NameinPassport"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + dt.Rows[i]["IsRenewed"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["OldPassportNumbers"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["EntryRestrictions"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["ResidencePermits"].ToStringCustom() + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    private string PrintDetails(int passportID)
    {
        objEmployeePassport.PassportID = passportID;
        DataTable dt = objEmployeePassport.GetPrintText();
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("PassportInfo.Text") +"</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PassportNumber.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["PassportNumber"]) + "</td></tr>");
        sb.Append("<tr><td> " + GetLocalResourceObject("NameInPassport.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["NameinPassport"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon","Country") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon","Placeofissue") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("PlaceofBirth.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceOfBirth"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + dt.Rows[0]["IsRenewed"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("OldPassport.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["OldPassportNumbers"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("EntryRestrictions.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["EntryRestrictions"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Residence.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["ResidencePermits"].ToStringCustom() + "</td></tr>");


        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");
        return sb.ToString();
    }
    private void BindPassportDetails(DataTable dt)
    {
        upMenu.Update();
        if (dt.Rows.Count > 0)
        {
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            objEmployeePassport.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            FillEmployees(dt.Rows[0]["EmployeeID"].ToInt64());
            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dt.Rows[0]["EmployeeID"])));
            ddlCountryOfOrigin.SelectedValue = Convert.ToString(dt.Rows[0]["CountryID"]);
            txtPassport.Text = Convert.ToString(dt.Rows[0]["PassportNumber"]);
            txtnameinpassport.Text = Convert.ToString(dt.Rows[0]["NameinPassport"]);
            txtPlaceofIssue.Text = Convert.ToString(dt.Rows[0]["PlaceofIssue"]);
            txtplaceofbirth.Text = Convert.ToString(dt.Rows[0]["PlaceOfBirth"]);
            txtIssuedate.Text = Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy");
            txtExpiryDate.Text = Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy");            
            chkPagesAvailable.Checked = dt.Rows[0]["PassportPagesAvailable"].ToBoolean();
            objEmployeePassport.PassportID = Convert.ToInt32(dt.Rows[0]["PassportID"]);
            objEmployeePassport.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"]);
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToStringCustom();
            ddlRemarks.SelectedValue = "1";
            txtEntryRestrictions.Text = dt.Rows[0]["EntryRestrictions"].ToStringCustom();
            txtOldPassport.Text = dt.Rows[0]["OldPassportNumbers"].ToStringCustom();
            txtResidencePermit.Text = dt.Rows[0]["ResidencePermits"].ToStringCustom();
            upnlRemarks.Update();

            DataTable dtpassportdoc = objEmployeePassport.GetPassportDocuments().Tables[0];
            dlPassportDoc.DataSource = dtpassportdoc;
            dlPassportDoc.DataBind();
            ViewState["PasDoc"] = dtpassportdoc;

            if (dlPassportDoc.Items.Count > 0)
                divPassDoc.Style["display"] = "block";
            else
                divPassDoc.Style["display"] = "none";
        }
    }

    private void ControlEnableDisble(bool blnTemp)
    {
       // ddlCompany.Enabled = ddlEmployee.Enabled = ddlCountryOfOrigin.Enabled = blnTemp;
        txtPassport.Enabled = txtnameinpassport.Enabled = txtPlaceofIssue.Enabled = txtplaceofbirth.Enabled = blnTemp;
        txtIssuedate.Enabled = txtExpiryDate.Enabled = chkPagesAvailable.Enabled = blnTemp;
        txtEntryRestrictions.Enabled = txtOldPassport.Enabled = txtResidencePermit.Enabled = blnTemp;
        imgCountryOfOrigin.Enabled = btnIssuedate.Enabled = btnExpiryDate.Enabled = blnTemp;
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, HRAutoComplete.AutoCompletePage.Passport,objUser.GetCompanyId());
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        if (lnkListPass.Enabled)
        {
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            dlEmployeepassport.Style["dispaly"] = "block";
            EnableMenus();
            fvPassport.Visible = false;
            pgrEmployees.CurrentPage = 0;
            Bind();
            this.RegisterAutoComplete();
            upEmployeePassport.Update();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        Bind();
        upMenu.Update();        
    }

    protected void btnattachpassdoc_Click(object sender, EventArgs e)
    {        
        divPassDoc.Style["display"] = "block";

        DataTable dt;
        DataRow dw;
        objEmployeePassport = new clsEmployeePassport();

        objEmployeePassport.PassportID = -1;
        objEmployeePassport.EmployeeID = -1;

        if (ViewState["PasDoc"] == null)
            dt = objEmployeePassport.GetPassportDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["PasDoc"];
            dt = dtDoc;
        }

        if (hfMode.Value == "Insert" || hfMode.Value == "Edit")
        {
            dw = dt.NewRow();
            dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
            dw["Filename"] = Session["Filename"];
            dt.Rows.Add(dw);
        }

        dlPassportDoc.DataSource = dt;
        dlPassportDoc.DataBind();
        ViewState["PasDoc"] = dt;
        txtDocname.Text = string.Empty;
        updldlPassportDoc.Update();
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        objEmployeePassport = new clsEmployeePassport();
        objEmployeePassport.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);
    }

    protected void dlPassportDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objEmployeePassport = new clsEmployeePassport();

                if (e.CommandArgument != string.Empty)
                {
                    objEmployeePassport.Node = Convert.ToInt32(e.CommandArgument);
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objEmployeePassport.DeletePassportDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }

                objEmployeePassport.PassportID = -1;
                objEmployeePassport.EmployeeID = -1;
                DataTable dt = objEmployeePassport.GetPassportDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlPassportDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();

                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    dw["Node"] = dlPassportDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                if (dt.Rows.Count > 0)
                {
                    dlPassportDoc.DataSource = dt;
                    dlPassportDoc.DataBind();
                    dlPassportDoc.Visible = true;
                }
                else
                {
                    dlPassportDoc.Visible = false;
                }

                ViewState["PasDoc"] = dt;
                break;
        }
    }
    
    protected void fuPassport_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuPassport.HasFile)
        {
            actualfilename = fuPassport.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuPassport.FileName);
            Session["Filename"] = filename;
            fuPassport.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }

    protected void ddlRemarks_SelectedIndexChanged(object sender, EventArgs e)
    {    
        if (ddlRemarks.SelectedValue.ToInt32() > 0)
        {
            divResidencePermit.Style["display"] = "none";
            divOldPassport.Style["display"] = "none";
            divEntryRestrictions.Style["display"] = "none";

            if (ddlRemarks.SelectedValue.ToInt32() == 1)
                divResidencePermit.Style["display"] = "block";
            else if (ddlRemarks.SelectedValue.ToInt32() == 2)
                divOldPassport.Style["display"] = "block";
            else
                divEntryRestrictions.Style["display"] = "block";

            upnlRemarks.Update();
        }
    }

    protected void ddlExpression_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind();
    }
    protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
    {        
        Bind();
    }

}