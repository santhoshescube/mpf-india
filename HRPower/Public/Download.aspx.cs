﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net;
//using MailBee;
//using MailBee.Mime;
//using MailBee.Security;
//using MailBee.ImapMail;

public partial class public_Download : System.Web.UI.Page
{
    //private Imap imap;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["type"] == "inbox")
        {
           
            //Connect();

            //MailMessage message = imap.DownloadEntireMessage(Convert.ToInt64(Request.QueryString["MessageId"]), true);

            //Attachment attachment = message.Attachments[Request.QueryString["FileName"]];

            //string fileName = Guid.NewGuid().ToString("N");

            //attachment.Save(Server.MapPath("~/documents/temp/") + fileName + System.IO.Path.GetExtension(attachment.Filename), true);

            //clsCommon.Download(Server.UrlDecode(Server.MapPath("~/documents/temp/") + fileName + System.IO.Path.GetExtension(attachment.Filename)));
        }
        else if (Request.QueryString["type"] == "Expense")
            clsCommon.Download(ConfigurationManager.AppSettings.Get("_PHY_PATH").ToString() + Request.QueryString["file"]);
        else if (Request.QueryString["type"] == "Document")
            clsCommon.Download(ConfigurationManager.AppSettings.Get("_PHY_PATH").ToString() + Request.QueryString["file"]);
        else if (Request.QueryString["type"] == "Candidate")
        {
            if (IsValidImage( Request.QueryString["FileName"].ToString()))
                imgImage.ImageUrl =  Request.QueryString["FileName"].ToString();
            else
            {

                string FileName = Server.MapPath( Request.QueryString["FileName"].ToString());
                FileInfo file = new FileInfo(FileName);
                if (file.Exists)
                {


                    if (file.Extension == ".pdf")
                    {

                        Response.ContentType = "application/pdf";

                        WebClient client = new WebClient();
                        Byte[] buffer = client.DownloadData(FileName);

                        if (buffer != null)
                        {

                            Response.ContentType = "application/pdf";
                            //else if (file.Extension == ".doc" || file.Extension == ".docx")
                            //    Response.ContentType = "application/ms-word";
                            //else if (file.Extension == ".xls" || file.Extension == ".xlsx")
                            //    Response.ContentType = "application/vnd.ms-excel";
                            //else if (file.Extension == ".txt")
                            //    Response.ContentType = "application/text";



                            Response.AddHeader("content-length", buffer.Length.ToString());
                            Response.BinaryWrite(buffer);

                        }
                    }

                    else
                    {





                        FileStream s = File.Open(FileName, FileMode.Open);

                        int i = Convert.ToInt32(s.Length);

                        byte[] b = new byte[i];

                        s.Read(b, 0, i);

                        s.Close();


                        Response.Clear();


                        if (file.Extension == ".doc" || file.Extension == ".docx")
                            Response.ContentType = "application/word";
                        else if (file.Extension == ".xls" || file.Extension == ".xlsx")
                            Response.ContentType = "application/vnd.ms-excel";
                        else if (file.Extension == ".txt")
                            Response.ContentType = "application/text";
                        Response.AddHeader("content-disposition", "filename=" + FileName.Remove(0, FileName.LastIndexOf("\\") + 1));

                        Response.OutputStream.Write(b, 0, b.Length);
                        Response.OutputStream.Flush();
                        Response.OutputStream.Close();

                        Response.Flush();
                        Response.Close();
                        Response.End();
                    }

                }
            }
        }
        else
        {
            clsCommon.Download(ConfigurationManager.AppSettings.Get("_PHYSICAL_PATH").ToString() + Request.QueryString["file"]);
        }


        
    }

    private bool IsValidImage(string FileName)
    {
        try
        {
            string ext = FileName.Remove(0, FileName.LastIndexOf(".")).Trim();
            if (ext.ToLower() == ".jpg" || ext.ToLower() == ".jpeg" || ext.ToLower() == ".gif" || ext.ToLower() == ".bmp" || ext.ToLower() == ".png" || ext.ToLower() == ".tif")
                return true;
            else
                return false;
        }
        catch (Exception) { return false; }
    }

    //private bool Connect()
    //{
    //    bool connected = false;

    //    if (imap == null)
    //    {
    //        Imap.LicenseKey = ConfigurationManager.AppSettings["_MAILBEE"];

    //        imap = new Imap();

    //        imap.SslMode = SslStartupMode.OnConnect;

    //        imap.Connect("pop." + Request.QueryString["Username"].Substring(Request.QueryString["Username"].LastIndexOf('@') + 1), 993);

    //        if (imap.Login(Request.QueryString["Username"], clsCommon.Decrypt(Request.QueryString["Password"], ConfigurationManager.AppSettings["_ENCRYPTION"]), AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, null))
    //        {
    //            connected = true;
    //            imap.SelectFolder("Inbox");
    //        }

    //        return connected;
    //    }
    //    else
    //        return false;
    //}
}
