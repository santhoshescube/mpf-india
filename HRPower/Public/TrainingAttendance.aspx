﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="TrainingAttendance.aspx.cs" Inherits="Public_TrainingAttendance" Title="Training Attendance " %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
  <div id='cssmenu'>
        <ul>
            <li><a href='TrainingCourse.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text="Training Course"></asp:Literal></span></a></li>
            <li><a href='TrainingSchedule.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text="Training Schedule"></asp:Literal></span></a></li>
            <li  class='selected'><a href="#">
                <asp:Literal ID="Literal92" runat="server" Text="Attendance"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal93" runat="server" Text="Feedback"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal94" runat="server" Text="Employee Review"></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
        <asp:Literal ID="Literal8" runat="server" meta:ResourceKey="Schedule"></asp:Literal>
    </div>
    <div class="trRight" style="width: 70%; height: 30px; float: left;">
        <asp:DropDownList ID="ddlSchedule" runat="server" Height="25px" Width="222px" 
            AutoPostBack="True" onselectedindexchanged="ddlSchedule_SelectedIndexChanged">
            <asp:ListItem Text="Asp.net Schedule"></asp:ListItem>
            <asp:ListItem Text="Java Schedule"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
        <asp:Literal ID="Literal4" runat="server" meta:ResourceKey="Startdate"></asp:Literal>
    </div>
    <div class="trRight" style="width: 70%; height: 30px; float: left;">
        <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox_mandatory" Width="100px"
            MaxLength="10" Enabled="False"></asp:TextBox>
        <asp:ImageButton ID="btnFromDate" runat="server" CausesValidation="False" 
            ImageUrl="~/images/Calendar_scheduleHS.png" Enabled="False" Visible="False" />
        <AjaxControlToolkit:CalendarExtender ID="AjaxCalDate" runat="server" Format="dd/MM/yyyy"
            PopupButtonID="btnFromDate" TargetControlID="txtStartDate" Enabled="True">
        </AjaxControlToolkit:CalendarExtender>
    </div>
    <div class="trLeft" style="width: 25%; height: 30px; float: left;">
        <asp:Literal ID="Literal1" runat="server" meta:ResourceKey="Enddate"></asp:Literal>
    </div>
    <div class="trRight" style="width: 70%; height: 30px; float: left;">
        <asp:TextBox ID="txtEndDate" runat="server" CssClass="textbox_mandatory" Width="100px"
            MaxLength="10" Enabled="False"></asp:TextBox>
        <asp:ImageButton ID="btnToDate" runat="server" CausesValidation="False" 
            ImageUrl="~/images/Calendar_scheduleHS.png" Enabled="False" Visible="False" />
        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
            PopupButtonID="btnToDate" TargetControlID="txtEndDate" Enabled="True">
        </AjaxControlToolkit:CalendarExtender>
    </div>
    
   <div class="trRight" style="width: 100%; height: 150px; float: left;overflow:auto">
      
    <asp:Table ID="tblAttendance" runat="server" BorderStyle="Ridge" 
        BorderWidth="1px" CssClass="labeltext" CellPadding="2" Width="98%">
    </asp:Table>
    </div> 
    
     <div style="clear: both; height: auto">
                </div>
                <div style="float: right; margin-top: 6px; width: 25%">
                    <asp:Button ID="btnSave" Width="75px" runat="server" Text='<%$ Resources:ControlsCommon,Save %>'
                        CssClass="btnsubmit" ValidationGroup="SaveInitiation" 
                        OnClientClick="return Save(true);" />&nbsp
                    <asp:Button Width="75px" ID="btnCancel" runat="server" Text='<%$ Resources:ControlsCommon,Cancel %>'
                        CssClass="btnsubmit" />
                </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
</asp:Content>
