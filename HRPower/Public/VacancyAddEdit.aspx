﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="VacancyAddEdit.aspx.cs" Inherits="Public_VacancyAddEdit" Title="Untitled Page"
    EnableViewState="true" %>

<%@ Register Src="~/Controls/VacancyControls/VacancyCriteria.ascx" TagName="Criteria"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/VacancyControls/VacancyPayStructure.ascx" TagName="PayStructure"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/VacancyControls/VacancyOfferLetterApproval.ascx" TagName="OfferLetter"
    TagPrefix="uc" %>
<%@ Register Src="~/Controls/VacancyControls/VacancyKPIKRA.ascx" TagName="KPIKRA"
    TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">



    <div id='cssmenu'>
        <ul>
            <li class='selected'><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upAdd" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAdd" class="TabbedPanels" style="width: 100%; display: block;" runat="server">
                <ul class="TabbedPanelsTabGroup">
                    <li class="TabbedPanelsTabSelected" id="pnlTab1" runat="server" onclick="setVacancyPostTab(this.id);">
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="General"></asp:Literal>
                    </li>
                    <li class="TabbedPanelsTab" id="pnlTab2" runat="server" onclick="setVacancyPostTab(this.id);">
                        <asp:Literal ID="Literal85" runat="server" meta:resourcekey="Preferences"></asp:Literal>
                    </li>
                </ul>
                <div class="TabbedPanelsContentGroup" id="divaddmain" runat="server">
                    <!-- GENERAL  !-->
                    <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
                        <asp:UpdatePanel ID="upnlGeneral" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Company"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="CompanyName" DataValueField="CompanyID"
                                            Width="200px" AutoPostBack="true" CausesValidation="false">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="error"
                                            meta:resourcekey="PleaseselectCompany" Display="Dynamic" ControlToValidate="ddlCompany"
                                            ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Department"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" DataTextField="Department" DataValueField="DepartmentID"
                                            Width="200px" AutoPostBack="true" CausesValidation="false">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddlDepartment" runat="server" CssClass="error"
                                            meta:resourcekey="PleaseselectDepartment" Display="Dynamic" ControlToValidate="ddlDepartment"
                                            ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <asp:UpdatePanel ID="updDesignation" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Designation"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 27%; height: 29px">
                                                <asp:DropDownList ID="ddlDesignation" runat="server" DataTextField="Designation"
                                                    CausesValidation="false" DataValueField="DesignationID" Width="200px" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlDesignation_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="firstTrleft" style="float: left; width: 4%; height: 29px; margin-top: 5px;">
                                                <asp:Button ID="btnDesignation" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                    Text="..." OnClick="btnDesignation_Click" />
                                            </div>
                                            <div class="firstTrleft" style="float: left; width: 25%; height: 29px; margin-top: 5px;">
                                                <asp:RequiredFieldValidator ID="rfvDesignation" runat="server" CssClass="error" meta:resourcekey="PleaseselectDesignation"
                                                    Display="Dynamic" ControlToValidate="ddlDesignation" ValidationGroup="submit"
                                                    InitialValue="-1"></asp:RequiredFieldValidator>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%-- <div style="float: left; width: 96%;">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal2" runat="server" Text="Planning"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 27%; height: 29px">
                                <asp:DropDownList ID="ddlPlanning" runat="server" Width="200px" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlPlanning_OnSelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="float: left; width: 96%; display: none;" id="divBudgetedAmount" runat="server">
                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                <asp:Literal ID="Literal77" runat="server" meta:resourcekey="BudgetedAmount"></asp:Literal>
                            </div>
                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                <asp:TextBox ID="txtBudgetedAmount" runat="server" MaxLength="7" Width="10%"></asp:TextBox>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                    FilterMode="ValidChars" FilterType="Custom" ValidChars="0123456789." TargetControlID="txtBudgetedAmount">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>--%>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="JobCode"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtJobCode" MaxLength="10" runat="server" CssClass="textbox_mandatory"
                                            CausesValidation="false" Width="100px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtJobCodepnlTab1" runat="server" meta:resourcekey="Pleaseenterjobcode"
                                            ControlToValidate="txtJobCode" CssClass="error" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="JobTitle"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtJobTitle" runat="server" CssClass="textbox_mandatory" MaxLength="50"
                                            CausesValidation="false" Width="250px"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtVacancyTitlepnlTab1" runat="server" meta:resourcekey="Pleaseenterjobtitle"
                                            ControlToValidate="txtJobTitle" Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator><%--ValidationGroup="submit"--%>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="JobDescription"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 60px">
                                        <asp:TextBox ID="txtRemarks" runat="server" onchange="RestrictMulilineLength(this, 500);"
                                            onkeyup="RestrictMulilineLength(this, 500);" CssClass="textbox" Width="200px"
                                            CausesValidation="false" TextMode="MultiLine" Height="50px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="RequiredQuota"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtRequiredQuota" runat="server" MaxLength="4" Width="10%" CausesValidation="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvtxtNoofVacanciespnlTab1" runat="server" CssClass="error"
                                            Display="Dynamic" ControlToValidate="txtRequiredQuota" ValidationGroup="submit"
                                            meta:resourcekey="PleaseenterrequiredQuota"></asp:RequiredFieldValidator>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteNoofVacancies" runat="server"
                                            FilterType="Numbers" TargetControlID="txtRequiredQuota">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="EmploymentType"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:DropDownList ID="ddlEmploymentType" runat="server" DataTextField="EmploymentType"
                                            CausesValidation="false" DataValueField="EmploymentTypeID" Width="200px" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvddlEmploymentType" runat="server" CssClass="error"
                                            Text="Please select Employment Type" Display="Dynamic" ControlToValidate="ddlEmploymentType"
                                            ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Qualification"></asp:Literal>
                                       
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 120px; ">
                                        <asp:UpdatePanel ID="uprcDegree" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div style="float: left; width: 70%; padding-left: 3px">
                                                    <asp:ListBox Width="94%" Height="120px" ID="lstQualification" runat="server" SelectionMode="Multiple">
                                                    </asp:ListBox>
                                                    <span style="font-size: smaller; font-style: italic; color: Gray;" >
                                                    <asp:Literal ID="Literal38" runat="server" meta:resourcekey="Ctrlkey"></asp:Literal>
                                                         </span>
                                                </div>
                                                <div>
                                                    <asp:Button ID="btnDegree" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                        Text="..." OnClick="btnDegree_Click" /></div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                       
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;padding-top:10px">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 24px">
                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Skills"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtSkills" runat="server" onchange="RestrictMulilineLength(this, 200);"
                                            onkeyup="RestrictMulilineLength(this, 200);" CssClass="textbox" Width="400px"
                                            CausesValidation="false" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 28px">
                                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Experience"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 28px">
                                        <asp:TextBox ID="txtExperience" runat="server" CssClass="textbox" Width="40px" MaxLength="2"
                                            CausesValidation="false"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtExperienceExtender" runat="server"
                                            FilterType="Numbers" TargetControlID="txtExperience">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="PlaceofWork"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtPlaceOfWork" MaxLength="100" runat="server" Width="100px" CausesValidation="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="WeeklyWorkingDays"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtWeeklyWorkingDays" MaxLength="2" runat="server" Width="40px"
                                            CausesValidation="false"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteWeeklyWorkingDays" runat="server"
                                            FilterType="Numbers" TargetControlID="txtWeeklyWorkingDays">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                        <asp:CustomValidator ID="cvWeeklyWorkingDays" runat="server" ControlToValidate="txtWeeklyWorkingDays"
                                            ValidationGroup="submit" CssClass="error" ValidateEmptyText="true" ClientValidationFunction="ValidateWorkingDays"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal18" runat="server" meta:resourcekey="DailyWorkingHours"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtDailyWorkingHours" MaxLength="5" runat="server" Width="40px"
                                            CausesValidation="false"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteDailyWorkingHours" runat="server"
                                            FilterType="Numbers,Custom" ValidChars="." TargetControlID="txtDailyWorkingHours">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                        <asp:CustomValidator ID="cvDailyWorkingHours" runat="server" ControlToValidate="txtDailyWorkingHours"
                                            CssClass="error" ValidateEmptyText="true" ValidationGroup="submit" ClientValidationFunction="ValidateDailyWorkingHours"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="AccomodationAvailability"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:RadioButtonList ID="rblAccomodation" runat="server" RepeatDirection="Horizontal"
                                            CausesValidation="false" AutoPostBack="True">
                                            <asp:ListItem Selected="True" Value="0" meta:resourcekey="Yes"></asp:ListItem>
                                            <asp:ListItem Value="1" meta:resourcekey="No"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="TransportationAvailability"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:RadioButtonList ID="rblTransportation" runat="server" RepeatDirection="Horizontal"
                                            AutoPostBack="True" CausesValidation="false">
                                            <asp:ListItem Selected="True" Value="0" meta:resourcekey="Yes"></asp:ListItem>
                                            <asp:ListItem Value="1" meta:resourcekey="No"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="ContractPeriod"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtContractPeriod" MaxLength="50" runat="server" Width="100px" CausesValidation="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="AirTicket"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtAirTicket" MaxLength="100" runat="server" Width="100px" CausesValidation="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal23" runat="server" meta:resourcekey="AnnualLeave"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtAnnualLeave" MaxLength="3" runat="server" Width="40px" CausesValidation="false"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteAnnualLeave" runat="server" FilterType="Numbers"
                                            TargetControlID="txtAnnualLeave">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                        <asp:CustomValidator ID="cvAnnualLeave" runat="server" ControlToValidate="txtAnnualLeave"
                                            CssClass="error" ValidateEmptyText="true" ValidationGroup="submit" ClientValidationFunction="ValidateAnnualLeave"></asp:CustomValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal24" runat="server" meta:resourcekey="LegalTerms"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 66px">
                                        <asp:TextBox ID="txtLegalTerms" runat="server" onchange="RestrictMulilineLength(this, 4000);"
                                            onkeyup="RestrictMulilineLength(this, 4000);" CssClass="textbox" Width="250px"
                                            CausesValidation="false" TextMode="MultiLine" Height="60px"></asp:TextBox>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 25px">
                                        <asp:Literal ID="Literal86" runat="server" meta:resourcekey="Isthepositionbudgeted"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:RadioButtonList ID="rblIsBudgeted" runat="server" RepeatDirection="Horizontal"
                                            CausesValidation="false" AutoPostBack="True" OnSelectedIndexChanged="rblIsBudgeted_SelectedIndexChanged">
                                            <asp:ListItem Value="1" meta:resourcekey="Yes"></asp:ListItem>
                                            <asp:ListItem Selected="True" Value="0" meta:resourcekey="No"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%; display: none;" id="divBudgetedAmount" runat="server">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal77" runat="server" meta:resourcekey="BudgetedAmount"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                        <asp:TextBox ID="txtBudgetedAmount" runat="server" MaxLength="7" Width="10%" CausesValidation="false"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                            FilterMode="ValidChars" ValidChars="0123456789." FilterType="Custom" TargetControlID="txtBudgetedAmount">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                        <%--FilterMode="ValidChars" ValidChars="0123456789." --%>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal25" runat="server" meta:resourcekey="JobValidityDate"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:TextBox ID="txtLastDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                            CausesValidation="false"></asp:TextBox>&nbsp;
                                        <asp:ImageButton ID="ibtnLastDate" runat="server" style="margin-top:5px;" ImageUrl="~/images/Calendar_scheduleHS.png"
                                            CausesValidation="false" CssClass="imagebutton" />
                                        <AjaxControlToolkit:CalendarExtender ID="extenderLastDate" runat="server" TargetControlID="txtLastDate"
                                            Format="dd/MM/yyyy" PopupButtonID="ibtnLastDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                        <asp:CustomValidator ID="cvtxtLastDatepnlTab1" runat="server" CssClass="error" ControlToValidate="txtLastDate"
                                            Display="Dynamic" ClientValidationFunction="Checkdate" ValidationGroup="submit"></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvtxtLastDatepnlTab1" runat="server" meta:resourcekey="Pleaseenterexpirydate"
                                            ControlToValidate="txtLastDate" Display="Dynamic" ValidationGroup="submit" CssClass="error"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 96%;" id="divCloseJob" runat="server">
                                    <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                        <asp:Literal ID="Literal26" runat="server" meta:resourcekey="SelecttoAlterJobStatus"></asp:Literal>
                                    </div>
                                    <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                        <asp:DropDownList ID="ddlStatus" runat="server" DataTextField="JobStatus" DataValueField="JobStatusID"
                                            CausesValidation="false" Width="200px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="TabbedPanelsContent" id="divTab2" style="display: none;">
                        <div style="padding: 10 10 10 10px" class="labeltext">
                            <br />
                            <asp:UpdatePanel ID="upnlPreferences" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="padding-left: 10px; width: 100%;">
                                        <div id="divLanguage" style="float: left; width: 96%;" runat="server">
                                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 25%; height: 24px">
                                                <asp:Literal ID="Literal87" runat="server" meta:resourcekey="LanguageCapabilities"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: auto;">
                                                <asp:TextBox ID="txtLanguageCapabilities" runat="server" onchange="RestrictMulilineLength(this, 200);"
                                                    onkeyup="RestrictMulilineLength(this, 200);" CssClass="textbox" Width="250px"
                                                    CausesValidation="false" Height="50px" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div id="divAge" style="float: left; width: 96%;" runat="server">
                                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 24.4%; height: 24px">
                                                <asp:Literal ID="Literal88" runat="server" meta:resourcekey="Age"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 29px">
                                                <div class="firstTrLeft" style="float: left; vertical-align: top; width: 10%; height: 24px">
                                                    <asp:TextBox ID="txtMinAge" runat="server" MaxLength="3" Width="80%" CausesValidation="false"></asp:TextBox>
                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                        FilterType="Numbers" TargetControlID="txtMinAge">
                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                </div>
                                                <div class="firstTrLeft" style="float: left; vertical-align: top; height: 24px">
                                                    &nbsp;-&nbsp;
                                                </div>
                                                <div class="firstTrRight" style="float: left; width: 50%; height: 29px">
                                                    <asp:TextBox ID="txtMaxAge" runat="server" MaxLength="3" Width="16%" CausesValidation="false"></asp:TextBox>
                                                    <AjaxControlToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                        FilterType="Numbers" TargetControlID="txtMaxAge">
                                                    </AjaxControlToolkit:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 96%;">
                                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 24.4%; height: 25px">
                                                <asp:Literal ID="Literal89" runat="server" meta:resourcekey="Gender"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                                <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" AutoPostBack="True"
                                                    CausesValidation="false">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 96%;">
                                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 24.4%; height: 25px">
                                                <asp:Literal ID="Literal95" runat="server" meta:resourcekey="LocalOverseasRecruitment"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                                <asp:RadioButtonList ID="rblLocalOverseasRecruitment" runat="server" RepeatDirection="Horizontal"
                                                    CausesValidation="false" AutoPostBack="True">
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 96%;">
                                            <div class="firstTrLeft" style="float: left; vertical-align: top; width: 24.4%; height: 25px">
                                                <asp:Literal ID="Literal96" runat="server" meta:resourcekey="FeesPayable"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                                <asp:RadioButtonList ID="rblFeesPayable" runat="server" RepeatDirection="Horizontal"
                                                    CausesValidation="false" AutoPostBack="True">
                                                    <asp:ListItem Value="1" meta:resourcekey="Yes"></asp:ListItem>
                                                    <asp:ListItem Selected="True" Value="0" meta:resourcekey="No"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 96%;">
                                            <div class="firstTrLeft" style="float: left; width: 25%; height: 29px">
                                                <asp:Literal ID="Literal97" runat="server" meta:resourcekey="JoiningDate"></asp:Literal>
                                            </div>
                                            <div class="firstTrRight" style="float: left; width: 70%; height: 31px">
                                                <asp:TextBox ID="txtJoiningDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    CausesValidation="false"></asp:TextBox>&nbsp;
                                                <asp:ImageButton ID="imgJoiningDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CausesValidation="false" CssClass="imagebutton" />
                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtJoiningDate"
                                                    Format="dd/MM/yyyy" PopupButtonID="imgJoiningDate">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="cvDateofJoining" runat="server" CssClass="error" ControlToValidate="txtJoiningDate"
                                                    Display="Dynamic" ValidationGroup="submit" ClientValidationFunction="Checkdate"></asp:CustomValidator>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <div id="tblSubmit" style="width: 44%; float: left; margin-left: 22px; text-align: right;">
                    <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                        ToolTip='<%$Resources:ControlsCommon,Submit%>' Width="75px" CausesValidation="false"
                        OnClick="btnSubmit_Click" /><%--CausesValidation="false"  ValidationGroup="submit" OnClientClick="return setVacancyTabSelected('submit','divTab1');"--%>
                    <asp:Button ID="btnCancel" runat="server" Width="75px" Style="margin-left: 5px;"
                        CssClass="btnsubmit" CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>'
                        ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClick="btnCancel_Click" />
                    <asp:HiddenField ID="hfMode" runat="server" />
                    <asp:HiddenField ID="hfID" runat="server" />
                </div>
            </div>
             
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgNewJob" ImageUrl="~/images/Active_Vacancies.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkNewJob" runat="server" CausesValidation="False" OnClick="lnkNewJob_Click"
                            meta:resourcekey="AddJob"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListJob" CausesValidation="false" ImageUrl="~/images/Vacancy_ListBig.PNG"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkViewjob" runat="server" CausesValidation="False" PostBackUrl="~/Public/Vacancy.aspx"
                            meta:resourcekey="ViewJob"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddJob" ImageUrl="~/images/delete.png" CausesValidation="False"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="False" OnClick="lnkDeleteJob_Click"
                            meta:resourcekey="Delete"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgCriteria" CausesValidation="false" ImageUrl="~/images/add_criteria.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddCriteria" runat="server" CausesValidation="False" OnClick="lnkAddCriteria_Click"
                            meta:resourcekey="AddCriteria"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddParticular0" CausesValidation="false" ImageUrl="~/images/add_addition_deduction.png"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddparticular" runat="server" CausesValidation="False" OnClick="lnkAddparticular_Click"
                            meta:resourcekey="AddParticulars"> 
                        </asp:LinkButton></h5>
                </div>
                <!-- ---------------------------------------------------------------------------  !-->
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgVacancyCriteria" CausesValidation="false" ImageUrl="~/images/add_vacancy_criteria.png"
                        runat="server" Visible="false" />
                </div>
                <div class="name">
                    <h5>
                        <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="criteria" RepositionMode="None"
                                    Drag="true" PopupControlID="popciteria" TargetControlID="lnkVacancyCriteria"
                                    ID="pop" runat="server">
                                </AjaxControlToolkit:ModalPopupExtender>
                                <asp:LinkButton ID="lnkVacancyCriteria" runat="server" CausesValidation="False" meta:resourcekey="SaveCriteria"
                                    PostBackUrl="VacancyAddCriteria.aspx" Visible="false">                         
                                </asp:LinkButton>
                                <div id="popciteria" style="width: 100%; height: 80%;">
                                    <uc:Criteria ID="vc" runat="server" EnableViewState="true" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgVacancyPayStructure" CausesValidation="false" ImageUrl="~/images/add_vacancy_paystructure.png"
                        runat="server" Visible="false" />
                </div>
                <div class="name">
                    <h5>
                       <asp:UpdatePanel ID="updPayStructure" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="PayStructure" RepositionMode="None"
                                    Drag="true" PopupControlID="popPayStructure" TargetControlID="lnkVacancyPayStructure"
                                    ID="mpePayStructure" runat="server">
                                </AjaxControlToolkit:ModalPopupExtender>
                                <asp:LinkButton ID="lnkVacancyPayStructure" runat="server" CausesValidation="False"
                                    PostBackUrl="VacancyPayStructure.aspx" meta:resourcekey="SavePayStructure" Visible="false">                         
                                </asp:LinkButton></h5>
                                <div id="popPayStructure" style="width: 100%; height: 80%;">
                                    <uc:PayStructure ID="ps" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgvacancyOffer" CausesValidation="false" ImageUrl="~/images/add_vacancy_offer_letter_approval.png"
                        runat="server" Visible="false" />
                </div>
                <div class="name">
                    <h5>
                       <asp:UpdatePanel ID="updOfferLetter" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="OfferLetter" RepositionMode="None"
                                    Drag="true" PopupControlID="popOfferLetter" TargetControlID="lnkVacancyOfferLetterApproval"
                                    ID="mpeOfferLetter" runat="server">
                                </AjaxControlToolkit:ModalPopupExtender>
                                <asp:LinkButton ID="lnkVacancyOfferLetterApproval" runat="server" CausesValidation="False"
                                    PostBackUrl="VacancyOfferLetterApproval.aspx" meta:resourcekey="SaveOfferLetterApproval"
                                    Visible="false">                         
                                </asp:LinkButton>
                                <div id="popOfferLetter" style="width: 100%; height: 80%;">
                                    <uc:OfferLetter ID="ol" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                        <h5>
                        </h5>
                    </h5>
                </div>
                <div id="kpikra" runat="server" style="display:none;">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgKPIKRA" CausesValidation="false" ImageUrl="~/images/add_vacancy_offer_letter_approval.png"
                            runat="server" Visible="false" />
                    </div>
                    <div class="name">
                        <h5>
                            <asp:UpdatePanel ID="upnlKPIKRA" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <AjaxControlToolkit:ModalPopupExtender PopupDragHandleControlID="KPIKRA" RepositionMode="None"
                                        Drag="true" PopupControlID="popKPIKRA" TargetControlID="lnkKPIKRA" ID="ModalPopupExtender1"
                                        runat="server">
                                    </AjaxControlToolkit:ModalPopupExtender>
                                    <asp:LinkButton ID="lnkKPIKRA" runat="server" CausesValidation="False" PostBackUrl="VacancyKPIKRA.aspx"
                                        Text="Save KPI/KRA" Visible="false">                         
                                    </asp:LinkButton>
                                    <div id="popKPIKRA" style="width: 100%; height: 80%;">
                                        <uc:KPIKRA ID="KPIKRAReference" runat="server" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <h5>
                            </h5>
                            <h5>
                            </h5>
                            <h5>
                            </h5>
                        </h5>
                    </div>
                </div>
                <!-- ---------------------------------------------------------------------------  !-->
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div style="height: auto">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="updDesignationReference" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btnPopDesignation" runat="server" />
            </div>
            <div id="divDesignation" runat="server" style="display: none;">
                <uc:DesignationReference ID="DesignationReference" runat="server" ModalPopupID="mdlPopDesignation" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopDesignation" runat="server" TargetControlID="btnPopDesignation"
                PopupControlID="divDesignation" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upDegreeReference" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none;">
                        <input type="button" id="btnDegreeReference" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeDegreeReference" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlDegreeReference" TargetControlID="btnDegreeReference">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlDegreeReference" runat="server" Style="display: none;">
                        <uc:DegreeReference ID="ucDegreeReference" runat="server" ModalPopupID="mpeDegreeReference" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:CriteriaReference ID="CriteriaReferenceControl" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlAdditionDeduction" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit">
                <InsertItemTemplate>
                    <div style="display: none">
                        <asp:Button ID="Button3" runat="server" />
                    </div>
                    <asp:Panel ID="pnlAdditionDeduction" runat="server" Style="display: none">
                        <uc:AdditionDeduction ID="ucAdditionDeduction" runat="server" ModalPopupID="mpeAdditionDeduction" />
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeAdditionDeduction" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlAdditionDeduction" TargetControlID="Button3">
                    </AjaxControlToolkit:ModalPopupExtender>
                </InsertItemTemplate>
            </asp:FormView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
