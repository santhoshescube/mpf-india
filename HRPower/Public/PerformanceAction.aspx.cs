﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Collections.Generic;

public partial class Public_PerformanceAction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PerformanceAction.Text").ToString() ;
        if (!IsPostBack)
        {
            FillAllDepartments();
            FillAllEmployees();
            Session["ResultList"] = null;
            SetPermission();
        }
        EnableDisableMenus();
      
       
    }



    private void EnableDisableMenus()
    {
        if (ViewState["IsCreate"] != null && Convert.ToBoolean(ViewState["IsCreate"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }

        lnkAdd.Enabled  = Convert.ToBoolean(ViewState["IsCreate"]);
        lnkViewInitiation.Enabled = Convert.ToBoolean(ViewState["IsView"]);


    }


    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerformanceAction);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }

    public void GetAllEvaluationDetailByEmployee()
    {
        DataTable dt = clsPerformanceInitiation.GetAllEvaluationDetailByEmployee(ddlEmployee.SelectedValue.ToInt32());
        dlEmployeeList.DataSource = dt;
        dlEmployeeList.DataBind();
        ViewState["EmployeeList"] = dt;


        dvNoRecord.Visible = dt.Rows.Count == 0;
        dvEmployeeList.Visible = dt.Rows.Count > 0;

        updEmployees.Update();

    }
  

    public void FillAllEmployees()
    {
        DataTable dt = clsPerformanceInitiation.GetAllEmployees(ddlDepartment.SelectedValue.ToInt32());
        DataRow dr = dt.NewRow();
        dr["EmployeeID"] = 0;
        dr["EmployeeFullName"] = clsUserMaster.GetCulture() == "ar-AE" ? ("--اختر--") : ("--Select--"); 


        dt.Rows.InsertAt(dr, 0);

        //dt.Rows.Add(0,"Select");
        ddlEmployee.DataSource = dt;
        ddlEmployee.DataTextField = "EmployeeFullName";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }


    public void FillAllDepartments()
    {
        ddlDepartment.DataSource = clsPerformanceInitiation.GetAllDepartments();
        ddlDepartment.DataTextField = "Department";
        ddlDepartment.DataValueField = "DepartmentID";
        ddlDepartment.DataBind();

        ddlEmployee_SelectedIndexChanged(new object(), new EventArgs());
    }







    private void Clear()
    {
        Session["ResultList"] = null;
        ddlDepartment.SelectedIndex = 0;
    }
  
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }


    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/PerformanceAction.aspx");
    }
    protected void lnkViewInitiation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/ViewPerformanceAction.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetAllEvaluationDetailByEmployee();
    }
  
    protected void btnDetails_Click(object sender, ImageClickEventArgs e)
    {
       EvaluationDetails.Title = GetGlobalResourceObject("ControlsCommon","EvaluationSummary").ToString(); 
        EvaluationDetails.SetGrid(((ImageButton)sender).CommandArgument.ToInt32(), ddlEmployee.SelectedValue.ToInt32());

        updEvaluationDetails.Update();
        mdEvaluationDetails.Show();
    }
    protected void btnAction_Click(object sender, EventArgs e)
    {
        if (ViewState["EmployeeList"] != null)
        {
            DataTable dt = (DataTable)ViewState["EmployeeList"];
            string FromDate = "";
            string ToDate = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                FromDate = dt.Rows[0]["FromDate"].ToString();
                ToDate = dt.Rows[dt.Rows.Count - 1]["ToDate"].ToString();
            }

            Response.Redirect("~/public/Action.aspx?" + string.Format("EmployeeID={0}&FromDate={1}&ToDate={2}", ddlEmployee.SelectedValue.ToInt32(), FromDate, ToDate));
        }
        else
            ScriptManager.RegisterClientScriptBlock(btnAction, btnAction.GetType(), new Guid().ToString(), "alert('Something went wrong so please try again');", true);
    }
    protected void btnActionCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("PerformanceAction.aspx");
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillAllEmployees(); 
    }
}