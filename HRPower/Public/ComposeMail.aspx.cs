﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;

public partial class public_ComposeMail : System.Web.UI.Page
{
    clsMailSettings objMailSettings;
    clsUserMaster objUser;
    //clsCommon objCommon;
    clsCompany objCompany;
    clsVendor objVendor;
    clsInterviewSchedule objInterviewSchedule;
    clsBank objBank;
    
    clsJobPost objJobPost;
    clsTrainingRegisteration objTrainingRegisteration;
    clsTrainingSchedule objTrainingSchedule; 

    #region PreDefined

    protected void Page_Load(object sender, EventArgs e)
    {        
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        pnlRecipients.GroupingText = GetLocalResourceObject("Receipients.Text").ToString();
        pnlTo.GroupingText = GetLocalResourceObject("To.Text").ToString();

       // cssMaroon.Attributes.Clear();

        if (!IsPostBack)
        {
            objMailSettings = new clsMailSettings();

            chkRecipients.DataValueField = "EmployeeID";
            chkRecipients.DataTextField = "OfficialEmail";

            chkRecipients.DataSource = objMailSettings.FillChkBoxList();
            chkRecipients.DataBind();

            if (chkRecipients.Items.Count == 0)
                pnlRecipients.Visible = false;

            if (Request.QueryString["attachment"] != null)
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("FilePath");
                dt.Columns.Add("FileName");

                DataRow dw = dt.NewRow();

                dw["FilePath"] = Server.UrlDecode(Request.QueryString["attachment"]);
                dw["FileName"] = Path.GetFileName(Server.UrlDecode(Request.QueryString["attachment"]));

                dt.Rows.Add(dw);

                BindFiles(dt);
            }

            if (Request.QueryString["type"] != null)
            {
                txtBody.ActiveMode = AjaxControlToolkit.HTMLEditor.ActiveModeType.Preview;
                
                objUser = new clsUserMaster();

                //txtBody.Content = objCommon.GetTemplateHeader(objUser.GetEmployeeId());
                txtBody.Content += "<div style='font-family:Tahoma; font-size:11px'> " + GetContent() + "</div>";
                txtBody.Font.Size = new FontUnit(Unit.Point(8));
                //txtBody.Content += objCommon.GetTemplateFooter(objUser.GetEmployeeId());
            }
        }
    }

    protected void dlFiles_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE":
                DataTable dt = (DataTable)ViewState["_FILES"];

                DataRow dw = dt.NewRow();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["FilePath"].ToString() == e.CommandArgument.ToString())
                    {
                        dw = dt.Rows[i];

                        if (e.CommandArgument.ToString().Contains(@"\documents\attachments\"))
                        {
                            if (File.Exists(e.CommandArgument.ToString()))
                                File.Delete(e.CommandArgument.ToString());
                        }
                    }
                }

                dt.Rows.Remove(dw);

                BindFiles(dt);

                break;
        }
    }
        
    private void BindFiles(DataTable dt)
    {
        if (dt.Rows.Count == 0)
            dt = null;

        dlFiles.DataSource = dt;
        dlFiles.DataBind();

        ViewState["_FILES"] = dt;
    }

    private void RemoveFiles()
    {
        DataTable dt = (DataTable)ViewState["_FILES"];

        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FilePath"].ToString().Contains(@"\documents\attachments\"))
                {
                    if (File.Exists(dt.Rows[i]["FilePath"].ToString()))
                        File.Delete(dt.Rows[i]["FilePath"].ToString());
                }
            }
        }

        ViewState["_FILES"] = null;

        dlFiles.DataSource = null;
        dlFiles.DataBind();
        //txtTo.Text = "";
        //txtSubject.Text = "";
        //txtBody.Content = "";

        

    }

    protected void chkRecipients_DataBound(object sender, EventArgs e)
    {
        foreach (ListItem item in chkRecipients.Items)
            item.Attributes.Add("onclick", string.Format("addToTextBox(this, {0})", txtTo.ClientID));
    }

    #endregion

    private string GetContent()
    {
        string content = string.Empty;
        objCompany = new clsCompany();
        objVendor = new clsVendor();
        objInterviewSchedule = new clsInterviewSchedule();
        objBank = new clsBank();
       // objCandidates = new clsCandidates();
        objJobPost = new clsJobPost();

        switch (Request.QueryString["type"])
        {
            case "Company":
                if (Request.QueryString["CompanyId"] != null && Request.QueryString["CompanyId"].Split(',').Length>1)
                {
                    content = objCompany.CreateSelectedCompaniesContent(Request.QueryString["CompanyId"].ToString());
                }
                else
                {
                    content = objCompany.CreateSingleCompanyContent(Convert.ToInt32(Request.QueryString["CompanyId"]));
                }

                break;
            case "Vendor":
                if (Request.QueryString["VendorId"] != null && Request.QueryString["VendorId"].Split(',').Length > 1)
                {
                    content = objVendor.CreateSelectedVendorsContent(Request.QueryString["VendorId"].ToString());
                }
                else
                {
                    content = objVendor.CreateSingleVendorContent(Convert.ToInt32(Request.QueryString["VendorId"]));
                }

                break;

            case "InterviewSchedule":
                if (Request.QueryString["ScheduleId"] != null && Request.QueryString["ScheduleId"].Split(',').Length > 1)
                {
                    //content = objInterviewSchedule.CreateSelectedScheduleContent(Request.QueryString["ScheduleId"].ToString());
                }
                else
                {
                    //content = objInterviewSchedule.CreateSingleScheduleContent(Convert.ToInt32(Request.QueryString["ScheduleId"]));
                }

                break;
            case "WalkinSchedule":
                if (Request.QueryString["WalkinScheduleId"] != null && Request.QueryString["WalkinScheduleId"].Split(',').Length > 1)
                {
                    //content = objInterviewSchedule.CreateSelectedWalkinContent(Request.QueryString["WalkinScheduleId"].ToString());
                }
                else
                {
                    //content = objInterviewSchedule.CreateSingleWalkinContent(Convert.ToInt32(Request.QueryString["WalkinScheduleId"]));
                }

                break;

            case "EmployeeNew":

                         if (Request.QueryString["EmployeeId"] != null)
                {
                    content = Session["Content"].ToString();
                }

                break;





            case "Employee":

                clsEmployee objEmployee = new clsEmployee();

                if (Request.QueryString["EmployeeId"] != null && Request.QueryString["EmployeeId"].Split(',').Length > 1)
                {
                    //objEmployee.EmployeeID = Convert.ToInt32(Request.QueryString["EmployeeId"]);

                    content = objEmployee.CreateSelectedEmployeesContent(Request.QueryString["EmployeeId"].ToString());
                }
                else
                {
                    content = objEmployee.GetPrintText(Convert.ToInt32(Request.QueryString["EmployeeId"]));
                }

                break;








            case "SalaryStructure":

                clsUserMaster  objUser = new clsUserMaster ();

                if (Request.QueryString["SalaryId"] != null && Request.QueryString["SalaryId"].Split(',').Length > 1)
                {
                    //objEmployee.EmployeeID = Convert.ToInt32(Request.QueryString["EmployeeId"]);

                    content = objUser.CreateSelectedEmployeesContent(Request.QueryString["SalaryId"].ToString());
                }
                else
                {
                    content = objUser.GetPrintText(Convert.ToInt32(Request.QueryString["SalaryId"]));
                }

                break;



            case "SalaryStructureNew":

                if (Request.QueryString["SalaryId"] != null)
                {
                    content = Session["Content"].ToString();
                }

                break;












            case "Passport":

                clsEmployeePassport objEmployeePassport = new clsEmployeePassport();

                if (Request.QueryString["PassportId"] != null && Request.QueryString["PassportId"].Split(',').Length > 1)
                {
                   content = PrintPassportList(Request.QueryString["PassportId"].ToString());
                }
                else
                {
                   content = PrintPassporDetails (Convert.ToInt32(Request.QueryString["PassportId"]));
                }

                break;
            case "Visa":

                clsEmployeeVisa  objEmployeeVisa = new clsEmployeeVisa();

                if (Request.QueryString["VisaId"] != null && Request.QueryString["VisaId"].Split(',').Length > 1)
                {
                    content = PrintVisaList(Request.QueryString["VisaId"].ToString());
                }
                else
                {
                    content = PrintSingleVisa(Convert.ToInt32(Request.QueryString["VisaId"]));
                }

                break;
            case "DrivingLicense":

                clsDrivingLicence objDrivingLicence = new clsDrivingLicence();

                if (Request.QueryString["LicenseID"] != null && Request.QueryString["LicenseID"].Split(',').Length > 1)
                {
                    content = PrintLicenseList(Request.QueryString["LicenseID"].ToString());
                }
                else
                {
                    content = PrintSingleLicense(Convert.ToInt32(Request.QueryString["LicenseID"]));
                }

                break;
            case "Qualification":

                clsQualification objQualification = new clsQualification();

                if (Request.QueryString["QualificationID"] != null && Request.QueryString["QualificationID"].Split(',').Length > 1)
                {
                    content = PrintQualificationList(Request.QueryString["QualificationID"].ToString());
                }
                else
                {
                    content = PrintSingleQualification(Convert.ToInt32(Request.QueryString["QualificationID"]));
                }

                break;
            case "OtherDocuments":

                if (Request.QueryString["DocumentID"] != null && Request.QueryString["DocumentID"].Split(',').Length > 1)
                {
                    content =PrintOtherDocument(Request.QueryString["DocumentID"].ToString(),false );
                }
                else
                {

                    content = PrintOtherDocument(Request.QueryString["DocumentID"].ToString(),true);
                }

                break;
            case "TradeLicense":

                clsTradeLicense objTradeLicense = new clsTradeLicense();

                if (Request.QueryString["TradeLicenseID"] != null && Request.QueryString["TradeLicenseID"].Split(',').Length > 1)
                {
                    content = CreateSelectedEmployeesContent(Request.QueryString["TradeLicenseID"].ToString());
                }
                else
                {
                    content = GetSinglePrintText(Convert.ToInt32(Request.QueryString["TradeLicenseID"]));
                }

                break;
            case "LeaseAgreement":

                clsLeaseAgreement objclsLeaseAgreement = new clsLeaseAgreement();

                if (Request.QueryString["LeaseAgreementID"] != null && Request.QueryString["LeaseAgreementID"].Split(',').Length > 1)
                {
                    content = PrintLeaseAgreementList(Request.QueryString["LeaseAgreementID"].ToString());
                }
                else
                {
                    objclsLeaseAgreement.intLeaseAgreementID = Request.QueryString["LeaseAgreementID"].ToInt32();
                    content = PrintSingleLeaseAgreement(Request.QueryString["LeaseAgreementID"].ToInt32());
                }

                break;
            case "Insurance":

                clsInsurance objclsInsurance = new clsInsurance();

                if (Request.QueryString["InsuranceID"] != null && Request.QueryString["InsuranceID"].Split(',').Length > 1)
                {
                    content = GetMultipleInsurancePrint(Request.QueryString["InsuranceID"].ToString());
                }
                else
                {
                    objclsInsurance.intInsuranceID = Request.QueryString["InsuranceID"].ToInt32();
                    content = GetInsurancePrint(objclsInsurance.intInsuranceID);
                }

                break;
            case "EmployeeCards":
                clsEmiratesHealthLabourCard objclsEmiratesHealthLabourCard = new clsEmiratesHealthLabourCard();
                if (Request.QueryString["CardID"] != null && Request.QueryString["CardID"].Split(',').Length > 1)
                {
                    content = PrintCardDetails (Request.QueryString["CardID"].ToString(), Request.QueryString["typeid"].ToInt32(),false);
                }
                else
                {
                    content = PrintCardDetails(Request.QueryString["CardID"].ToString(), Request.QueryString["typeid"].ToInt32(), true);
                }

                break;
            case "Bank":

                if (Request.QueryString["BankNameID"] != null && Request.QueryString["BankNameID"].Split(',').Length > 1)
                {
                    content = objBank.CreateSelectedBankContent(Request.QueryString["BankNameID"].ToString());
                }
                else
                {
                    content = objBank.CreateSingleBankContent(Convert.ToInt32(Request.QueryString["BankNameID"]));
                }

                break;

            //case "Candidate":

            //    if (Request.QueryString["CandidateId"] != null && Request.QueryString["CandidateId"].Split(',').Length > 1)
            //    {
            //        content = objCandidates.CreateSelectedCandidateContent(Request.QueryString["CandidateId"].ToString());
            //    }
            //    else
            //    {
            //        content = objCandidates.CreateSingleCandidateContent(Convert.ToInt32(Request.QueryString["CandidateId"]));
            //    }

            //    break;

            case "Vacancy":

                if (Request.QueryString["VacancyId"] != null && Request.QueryString["VacancyId"].Split(',').Length > 1)
                {
                    content = objJobPost.CreateSelectedVacancyContent(Request.QueryString["VacancyId"].ToString());
                }
                else
                {
                    content = objJobPost.CreateSingleVacancyContent(Convert.ToInt32(Request.QueryString["VacancyId"]));
                }

                break;

            case "Project":               

                if (Request.QueryString["ProjectId"] != null && Request.QueryString["ProjectId"].Split(',').Length > 1)
                {                                     
                    content = PrintProjectdetails(Request.QueryString["ProjectId"].ToString(), false);
                }
                else
                {
                    content = PrintProjectdetails(Request.QueryString["ProjectId"].ToString(), true);
                }

                break;

            case "Benefit":

                clsBenefits objBenefits = new clsBenefits();

                if (Request.QueryString["EmployeeBenefitID"] != null && Request.QueryString["EmployeeBenefitID"].Split(',').Length > 1)
                {
                    objBenefits.EmployeeBenefitIDs = Request.QueryString["EmployeeBenefitID"].ToString();
                    content = objBenefits.CreateSelectedBenefitContent();
                }
                else
                {
                    objBenefits.EmployeeBenefitID = Convert.ToInt32(Request.QueryString["EmployeeBenefitID"]);
                    content = objBenefits.CreateSingleBenefitContent();
                }

                break;


               case "EmployeeSeperation":


                clsEmployeeSeperation objEmployeeSeperation = new clsEmployeeSeperation();
                if (Request.QueryString["SEmployeeId"] != null && Request.QueryString["SEmployeeId"].Split(',').Length > 1)
                {
                    content = objEmployeeSeperation.PrintMultipleItemSeperate(Request.QueryString["SEmployeeId"]);
                }
                else
                {
                    content = objEmployeeSeperation.PrintSingleItemSeperate(Convert.ToInt32(Request.QueryString["SEmployeeId"]));
                }

                break;

                
               case "EmployeeTakeOver":

               clsEmployeeTakeOver  objEmployeeTakeOver=new clsEmployeeTakeOver ();

                if (Request.QueryString["SEmployeeId"] != null && Request.QueryString["SEmployeeId"].Split(',').Length > 1)
                {
                    content = objEmployeeTakeOver.PrintMultipleItemTakeOver(Request.QueryString["SEmployeeId"]);
                }
                else
                {
                    content = objEmployeeTakeOver.PrintSingleItemTakeOver(Convert.ToInt32(Request.QueryString["SEmployeeId"]));
                }

                break;


                
               case "EmployeeHandOver":

                
                clsEmployeeTakeOver  objEmployeeTakeOver1=new clsEmployeeTakeOver ();
                if (Request.QueryString["SEmployeeId"] != null && Request.QueryString["SEmployeeId"].Split(',').Length > 1)
                {
                    content = objEmployeeTakeOver1.PrintMultipleItemHandOver(Request.QueryString["SEmployeeId"]);
                }
                else
                {
                    content = objEmployeeTakeOver1.PrintSingleItemHandOver(Convert.ToInt32(Request.QueryString["SEmployeeId"]));
                }

                break;

               case "TrainingRegistration":

                objTrainingRegisteration = new clsTrainingRegisteration();

                if (Request.QueryString["TrainingId"] != null && Request.QueryString["TrainingId"].Split(',').Length > 1)
                {
                    content = objTrainingRegisteration.CreateSelectedRegistrationContent(Request.QueryString["TrainingId"].ToString());
                }
                else
                {
                    content = objTrainingRegisteration.CreateSingleRegistrationContent(Request.QueryString["TrainingId"].ToString());
                }

                break;

               case "TrainingSchedule":

                objTrainingSchedule = new clsTrainingSchedule();

                //if (Request.QueryString["TrainingId"] != null && Request.QueryString["TrainingId"].Split(',').Length > 1)
                //{
                //    content = objTrainingSchedule.CreateSelectedScheduleContent(Request.QueryString["TrainingId"].ToString());
                //}
                //else
                //{
                //    content = objTrainingSchedule.CreateSingleScheduleContent(Request.QueryString["TrainingId"].ToString());
                //}

                break;
               case "InsuranceCard":

                clsInsuranceCard objclsInsuranceCard = new clsInsuranceCard();

                if (Request.QueryString["InsuranceCardIDs"]!= null &&(Request.QueryString["InsuranceCardIDs"].Split(',').Length > 1))
                {
                    content = PrintInsuranceCard(Request.QueryString["InsuranceCardIDs"].ToString(),false);
                }
                else
                {

                    content = PrintInsuranceCard(Request.QueryString["InsuranceCardIDs"].ToString(), true);
                }

                break;


            default:
                content = string.Empty;
                 
                break;
        }

        return content;
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        DataTable dt = (DataTable)ViewState["_FILES"];

        objMailSettings = new clsMailSettings();

        if (dt == null)
        {
            if (objMailSettings.SendMail(txtTo.Text, txtSubject.Text, txtBody.Content, clsMailSettings.AccountType.Email,false))
            {
                RemoveFiles();
                ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Mail has been sent successfully.');", true);
            }
            else
                ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Some mails could not be sent.');", true); 
        }
        else
        {
            if (dt.Rows.Count == 0)
            {
                if (objMailSettings.SendMail(txtTo.Text, txtSubject.Text, txtBody.Content, clsMailSettings.AccountType.Email,false))
                {
                    RemoveFiles();
                    ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Mail has been sent successfully.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Some mails could not be sent.');", true);
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (DataRow dw in dt.Rows)
                {
                    sb.Append("," + dw["FilePath"].ToString());
                }

                sb.Remove(0, 1);

                if (objMailSettings.SendMail(txtTo.Text, txtSubject.Text, txtBody.Content, clsMailSettings.AccountType.Email, sb.ToString(),false))
                {
                    {
                        RemoveFiles();
                        ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Mail has been sent successfully.');", true);
                    }
                }
                else
                    ScriptManager.RegisterStartupScript(btnSend, this.GetType(), "Msg", "alert('Some mails could not be sent.');", true);
            }
        }
    }
    private string PrintOtherDocument(string sDocumentIds, bool isSingle)
    {
        StringBuilder sb = new StringBuilder();
        DataTable datPrint = clsDocumentMaster.PrintSelected(sDocumentIds);
        if (datPrint != null && datPrint.Rows.Count > 0)
        {
            if (isSingle)
            {
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(datPrint.Rows[0]["DocumentNumber"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Other Document Information</td></tr>");
                sb.Append("<tr><td width='15%'>" + Convert.ToString(datPrint.Rows[0]["OperationType"]) + "</td><td width='5px'>:</td><td style='width:auto:max-width:85%'>" + Convert.ToString(datPrint.Rows[0]["DocumentNumber"]) + "</td></tr>");
                sb.Append("<tr><td>" + GetLocalResourceObject("DocumentNumber.Text").ToString() + " </td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td></tr>");
                sb.Append("<tr><td> " + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + " </td><td>:</td><td>" + (datPrint.Rows[0]["IssuedDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td> " + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + " </td><td>:</td><td>" + (datPrint.Rows[0]["IsRenewed"]).ToString() + "</td></tr>");
                sb.Append("<tr><td valign='top' >" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td width='5px' valign='top'>:</td><td style='word-break:break-all;'>" + Convert.ToString(datPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'>Other Document Information</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'> " + GetLocalResourceObject("Type.Text").ToString() + "  </td><td width='150px'>" + GetLocalResourceObject("DocumentNumber.Text").ToString() + " </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='7'><hr></td></tr>");
                for (int i = 0; i < datPrint.Rows.Count; i++)
                {
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["OperationType"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["DocumentNumber"]) + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IssuedDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IsRenewed"].ToString() + "</td>");
                    sb.Append("<td style='width:200px;word-break:break-all'>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }
    private string PrintProjectdetails(string sProjectIds, bool isSingle)
    {
        StringBuilder sb = new StringBuilder();
        clsProjectReference objProjectReference = new clsProjectReference();
        objProjectReference.ProjectIDs = sProjectIds;
        if (isSingle)
            objProjectReference.ProjectID = Convert.ToInt32(sProjectIds);

        DataSet ds = objProjectReference.Print();


        if (ds != null)
        {
            DataTable dt = ds.Tables[0];
            if (isSingle)
            {

                string sFontWeight = "font-weight:normal";
                sb.Append("<div style='padding:10px; margin:5px;'>");
                sb.Append("<center> <h5 style='margin:0; padding:0; text-transform:uppercase;'>");
                sb.Append(GetLocalResourceObject("ProjectDetails.Text").ToString() + "</h5> </center>");
                sb.Append("<hr style='border:solic 1px #CCCCCC;'/>");
                sb.Append("<div style='margin-left:10px; padding:5px 0;'>"); // apply 10px margin

                sb.Append("<span style='font-size:12px; font-weight:bold' > " + GetLocalResourceObject("General.Text").ToString() + " </span>");
                // General Info
                sb.Append("<table style='font-size: 11px; line-height: 20px; margin-top:5px;'>");

                sb.Append("<tr> <td style=''" + sFontWeight + "'width='200'> " + GetLocalResourceObject("ProjectCode.Text").ToString() + " </td> ");
                sb.Append("<td> : " + dt.Rows[0]["ProjectCode"] + "<td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Description.Text").ToString() + " </td>");
                sb.Append("<td> : " + dt.Rows[0]["Description"] + "</td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("StartDate.Text").ToString() + "</td> ");
                sb.Append("<td> : " + dt.Rows[0]["StartDate"] + "</td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("EstimatedDate.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["EstimatedFinishDate"] + "</td> </tr>");




                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("TeamMembers.Text").ToString() + " </td>");
                sb.Append("<td> : " + dt.Rows[0]["NoOfStaff"] + "</td> </tr>");


                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Budget.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["Budget"] + "</td> </tr>");


                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Remarks.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["Remarks"] + "</td> </tr>");

                sb.Append("</table>");

                // Project Team Details
                if (ds.Tables[1].Rows.Count > 0)
                {
                    sb.Append("</br>");
                    sb.Append("<span style='font-size:12px; font-weight:bold'> " + GetLocalResourceObject("TeamInfo.Text").ToString() + " </span>");
                    sb.Append("</br> </br>");
                    sb.Append("<table style='border:solid 1px #F0F0F0; border-collapse:collapse; width:90%; font-size:11px;' cellpadding='4'>");
                    sb.Append("<tr style='background-color:#F0F0F0; font-size:11px'> <td style='font-weight:bold'>  " + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> " + GetLocalResourceObject("StartDate.Text").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> " + GetLocalResourceObject("EndDate.Text").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'>  " + GetLocalResourceObject("Duration.Text").ToString() + "</td> </tr>");

                    foreach (DataRow Row in ds.Tables[1].Rows)
                    {
                        sb.Append("<tr> <td style='border:solid 1px #F0F0F0;'> " + Row["Employee"] + "</td>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["WorkStartDate"] + "</td>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["FinishDate"] + "</d>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["Duration"] + " </td> </tr>");
                    }

                    sb.Append("</table>");
                }

                sb.Append("</div>");
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;' border='0'>");
                sb.Append("<tr><td style='font-weight:bold;font-size:13px; text-transform:uppercase;' align='center'>" + GetLocalResourceObject("ProjectDetails.Text").ToString() + "</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table style='font-size:11px;font-family:Tahoma; width:100%;' border='0px'>");
                sb.Append("<tr><td colspan='6'><hr></td></tr>");
                sb.Append("<tr style='font-weight:bold;'>");
                // table header
                sb.Append("<td>" + GetLocalResourceObject("ProjectCode.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("Description.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("StartDate.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("EstimatedDate.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("TeamMembers.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("Budget.Text").ToString() + "</td>");

                sb.Append("<tr><td colspan='6'><hr></td></tr>");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["ProjectCode"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Description"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["StartDate"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EstimatedFinishDate"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NoOfStaff"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Budget"]) + "</td>");
                }
                sb.Append("</td></tr> </table> </td> </tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }
    private string PrintCardDetails(string sDocIds, int iCardType, bool isSingle)
    {
        clsEmiratesHealthLabourCard objCard = new clsEmiratesHealthLabourCard();
        DataTable datPrint = objCard.PrintSelected(sDocIds, iCardType);
        StringBuilder sb = new StringBuilder();
        string renew = "";
        if (datPrint != null && datPrint.Rows.Count > 0)
        {
            if (isSingle)
            {
               
               
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;word-break:break-all'>" + Convert.ToString(datPrint.Rows[0]["CardNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["EmployeeFullName"]) + "</td></tr>");
                sb.Append("<tr><td width='150px' style='word-break:break-all'>" + GetLocalResourceObject("PersonalNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["CardPersonalNumber"]) + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["IssueDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + "</td><td>:</td><td>" +  datPrint.Rows[0]["IsRenewed"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px' style='word-break:break-all'>" + GetGlobalResourceObject("DocumentsCommon", "otherInfo").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'>" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'> " + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + " </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Cardnumber").ToString() + " </td><td width='150px'>" + GetLocalResourceObject("PersonalNumber.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Expirydate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='7'><hr></td></tr>");
                for (int i = 0; i < datPrint.Rows.Count; i++)
                {
                    
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["EmployeeFullName"]) + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardNumber"]) + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + Convert.ToString(datPrint.Rows[i]["CardPersonalNumber"]) + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IssueDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IsRenewed"].ToString() + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }

    private string PrintInsuranceCard(string InsuranceCardId, bool IsSingle)
    {
        clsInsuranceCard  objclsInsuranceCard = new clsInsuranceCard();

        DataTable dtPrint = objclsInsuranceCard.PrintSingleInsuranceCard(InsuranceCardId);

        StringBuilder sb = new StringBuilder();
        if (dtPrint != null && dtPrint.Rows.Count > 0)
        {
            string renew = "";

            if (IsSingle)
            {

                renew = dtPrint.Rows[0]["IsRenewed"].ToString();
                if (renew.ToBoolean())
                {
                    renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                }
                else
                {
                    renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                }
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + dtPrint.Rows[0]["EmployeeFullName"].ToString() + " -" + Convert.ToString(dtPrint.Rows[0]["CardNumber"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("InsuranceCardDetails.Text").ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "CardNumber").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dtPrint.Rows[0]["CardNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PolicyNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dtPrint.Rows[0]["PolicyNumber"]) + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PolicyName.Text").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["PolicyName"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("InsuranceCompany.Text").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["Description"].ToString() + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "IssuedBy").ToString() + "</td><td width='5px'>:</td><td>" + dtPrint.Rows[0]["IssuedBy"].ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + dtPrint.Rows[0]["IssueDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + dtPrint.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd MMM yyyy") + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td>:</td><td>" + renew + "</td></tr>");
                sb.Append("<tr><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td width='5px'>:</td><td style='word-break:break-all'>" + Convert.ToString(dtPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("InsuranceCardDetails.Text").ToString() + "</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + " </td><td width='100px'> " + GetGlobalResourceObject("DocumentsCommon", "CardNumber").ToString() + " </td><td width='150px'>" + GetLocalResourceObject("PolicyNumber.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("PolicyName.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("InsuranceCompany.Text").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssuedBy").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'> " + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + " </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='11'><hr></td></tr>");
                for (int i = 0; i < dtPrint.Rows.Count; i++)
                {
                    renew = dtPrint.Rows[i]["IsRenewed"].ToString();
                    if (renew.ToBoolean())
                    {
                        renew = clsGlobalization.IsArabicCulture() ? "نعم" : "Yes";
                    }
                    else
                    {
                        renew = clsGlobalization.IsArabicCulture() ? "لا" : "No"; ;
                    }
                    sb.Append("<td>" + dtPrint.Rows[i]["EmployeeFullName"].ToString() + "</td>");
                    sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["CardNumber"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dtPrint.Rows[i]["PolicyNumber"]) + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["PolicyName"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["Description"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["IssuedBy"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["IssueDate"].ToString() + "</td>");
                    sb.Append("<td>" + dtPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + renew + "</td>");
                    sb.Append("<td style='word-break:break-all'>" + dtPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }

        }
        return sb.ToString();
    }

    private string PrintPassporDetails(int passportID)
    {
        clsEmployeePassport objEmployeePassport = new clsEmployeePassport();
        objEmployeePassport.PassportID = passportID;
        DataTable dt = objEmployeePassport.GetPrintText();
        StringBuilder sb = new StringBuilder();
        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("PassportInfo.Text") + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("PassportNumber.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["PassportNumber"]) + "</td></tr>");
        sb.Append("<tr><td> " + GetLocalResourceObject("NameInPassport.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["NameinPassport"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Country") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Placeofissue") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("PlaceofBirth.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceOfBirth"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + dt.Rows[0]["IsRenewed"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("OldPassport.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["OldPassportNumbers"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("EntryRestrictions.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["EntryRestrictions"].ToStringCustom() + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Residence.Text") + "</td><td>:</td><td style=\"word-break: break-all;\">" + dt.Rows[0]["ResidencePermits"].ToStringCustom() + "</td></tr>");


        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");
        return sb.ToString();
    }

    public string PrintPassportList(string sPassportIds)
    {
        StringBuilder sb = new StringBuilder();
        clsEmployeePassport objEmployeePassport = new clsEmployeePassport();
        DataTable dt = objEmployeePassport.CreateSelectedEmployeesPrintContent(sPassportIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("PassportInfo.Text") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee") + " </td><td width='150px'>" + GetLocalResourceObject("PassportNumber.Text") + "</td><td width='150px'>" + GetLocalResourceObject("NameInPassport.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Country") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "PlaceofIssue") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td width='100px'>" + GetLocalResourceObject("OldPassport.Text") + "</td><td width='100px'>" + GetLocalResourceObject("EntryRestrictions.Text") + "</td><td width='100px'>" + GetLocalResourceObject("Residence.Text") + "</td></tr>");
        sb.Append("<tr><td colspan='10'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PassportNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NameinPassport"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + dt.Rows[i]["IsRenewed"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["OldPassportNumbers"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["EntryRestrictions"].ToStringCustom() + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + dt.Rows[i]["ResidencePermits"].ToStringCustom() + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }
    protected void imgAttach_Click(object sender, EventArgs e)
    {
        HttpFileCollection attachments = Request.Files;

        DataTable dt = (DataTable)ViewState["_FILES"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("FilePath");
            dt.Columns.Add("FileName");
        }

        DataRow dw;

        string path = string.Empty;

        HttpPostedFile file;

        for (int i = 0; i < attachments.Count; i++)
        {
            file = attachments[i];

            if (file.ContentLength == 0)
                continue;

            path = Server.MapPath("~/documents/attachments/") + clsCommon.GetValidationGroup() + Path.GetExtension(file.FileName);

            file.SaveAs(path);

            dw = dt.NewRow();

            dw["FilePath"] = path;
            dw["FileName"] = Path.GetFileName(file.FileName);
            dt.Rows.Add(dw);
        }

        BindFiles(dt);

        foreach (ListItem item in chkRecipients.Items)
            item.Attributes.Add("onclick", string.Format("addToTextBox(this, {0})", txtTo.ClientID));
    }

    //Visa 

    private  string PrintVisaList(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
    
        clsEmployeeVisa objEmployeeVisa = new clsEmployeeVisa ();
        DataTable dt = objEmployeeVisa.GetVisaList(sVisaIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("VisaInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("VisaNumber.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("VisaType.Text").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "PlaceOfIssue").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Country").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='9'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string PrintSingleVisa(int iVisaID)
    {
        StringBuilder sb = new StringBuilder();

        clsEmployeeVisa objEmployeeVisa = new clsEmployeeVisa();
        DataTable dt = objEmployeeVisa.GetSingleVisa(iVisaID);

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("VisaInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("VisaNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaNumber"]) + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("VisaType.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaType"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "PlaceOfIssue").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Country").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }


    // Driving License
    private string PrintLicenseList(string sVisaIds)
    {
        clsDrivingLicence objLicense = new clsDrivingLicence();

        StringBuilder sb = new StringBuilder();
        DataTable dt = objLicense.CreateSelectedEmployeesPrintContent(sVisaIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>Driving License Information</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee") + "</td><td width='150px'>" + GetLocalResourceObject("DrivingLicenseNumber.Text") + "</td><td width='150px'>" + GetLocalResourceObject("LicenseType.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td width='100px'>" + GetLocalResourceObject("LicensedCountry.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td></tr>");
        sb.Append("<tr><td colspan='9'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenseType"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string PrintSingleLicense(int iVisaID)
    {
        clsDrivingLicence objLicense = new clsDrivingLicence();

            
        DataTable dt = objLicense.GetPrintText(iVisaID);

        StringBuilder sb = new StringBuilder();

      

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("DrivingLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("DrivingLicenseNumber.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicenseType.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenseType"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicensedCountry.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }

    // Qualification
    private string PrintQualificationList(string sVisaIds)
    {
       clsQualification  objQualification = new clsQualification();

        StringBuilder sb = new StringBuilder();

        DataTable dt = objQualification.GetPrintAll(sVisaIds);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'> " + GetLocalResourceObject("QualificationInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("RefNumber.Text").ToString() + "</td>");
        sb.Append("<td width='90px'>" + GetLocalResourceObject("University.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("SchoolCollege.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("Qualification.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("FromPeriod.Text").ToString() + "</td><td width='90px'>" + GetLocalResourceObject("ToPeriod.Text").ToString() + "</td>");
        sb.Append("<td width='90px'>" + GetLocalResourceObject("CertificateTitle.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("GradePercentage.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("CertificatesAttested.Text").ToString() + "</td>");
        sb.Append("<td width='50px'>" + GetLocalResourceObject("CertificatesVerified.Text").ToString() + "</td><td width='50px'>" + GetLocalResourceObject("UniversityClearenceRequired.Text").ToString() + "</td>");
        sb.Append("<td width='50px'>" + GetLocalResourceObject("ClearenceCompleted.Text").ToString() + "</td><td width='200px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='15'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ReferenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["University"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SchoolOrCollege"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Degree"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["FromDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ToDate"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificateTitle"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["GradeorPercentage"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesAttested"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CertificatesVerified"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["UniversityClearenceRequired"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["ClearenceCompleted"]) + "</td>");
            sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string PrintSingleQualification(int iVisaID)
    {
        clsQualification objQualification = new clsQualification();
        StringBuilder sb = new StringBuilder();
        DataTable dt = objQualification.GetPrintSingle(iVisaID);


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("QualificationInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("RefNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["ReferenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("University.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["University"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("SchoolCollege.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SchoolOrCollege"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Qualification.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Degree"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("FromPeriod.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["FromDate"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("ToPeriod.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ToDate"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificateTitle.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificateTitle"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("GradePercentage.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["GradeorPercentage"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificatesAttested.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesAttested"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("CertificatesVerified.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["CertificatesVerified"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("UniversityClearenceRequired.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["UniversityClearenceRequired"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("ClearenceCompleted.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["ClearenceCompleted"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }

    //Lease Agreement
    private string PrintLeaseAgreementList(string sVisaIds)
    {
        clsLeaseAgreement objclsLeaseAgreement = new clsLeaseAgreement();

        DataTable datData = objclsLeaseAgreement.GetMultipleLeaseAgreement(sVisaIds);
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Lease Agreement", 2);
        }
        return "";
    }
    public string PrintSingleLeaseAgreement(int iVisaID)
    {
        clsLeaseAgreement objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intLeaseAgreementID = iVisaID;
        DataTable datData = objclsLeaseAgreement.GetLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Lease Agreement", 1);
        }
        return "";
    }

    private string GetDocumentHtml(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
     
        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
      
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";


      
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";

     
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

     
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

    
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + " : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("LeaseAgreementDetails.Text").ToString() + "</div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("AgreementNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["AgreementNo"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Lessor.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Lessor"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Startdate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["StartDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Enddate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["EndDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("DocumentDate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["DocumentDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeasePeriod.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeasePeriod"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RentalValue.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["RentalValue"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("DepositHeld.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["DepositHeld"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Tenant.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Tenant"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ContactNumbers.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ContactNumbers"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Address.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Address"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LandLord.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Landlord"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("UnitNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["UnitNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeaseUnitType.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeaseUnitType"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("BuildingNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["BuildingNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Location.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Location"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PlotNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PlotNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Total.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Total"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + "</div> <div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("AgreementNumber.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Lessor.Text").ToString() + "</div> <div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Tenant.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("RentalValue.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("ContactNumbers.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Startdate.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Enddate.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("LeasePeriod.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("DocumentDate.Text").ToString() + "</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["AgreementNo"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Tenant"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Lessor"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["RentalValue"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ContactNumbers"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["StartDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["EndDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["LeasePeriod"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["DocumentDate"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }


    public string GetSinglePrintText(int iTradeLicenseID)
    {
        StringBuilder sb = new StringBuilder();
        clsTradeLicense objTradeLicense = new clsTradeLicense();
        DataTable dt = objTradeLicense.GetPrintText(iTradeLicenseID);

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("Sponsor.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Sponsor"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("SponserFee.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SponserFee"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicenseNumber.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("City.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["City"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Province.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Province"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Partner.Text").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Partner"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");
        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }

    public string CreateSelectedEmployeesContent(string sTradeLicenseIDs)
    {  
        StringBuilder sb = new StringBuilder();
        clsTradeLicense objTradeLicense = new clsTradeLicense();
        DataTable dt = objTradeLicense.CreateSelectedEmployeesPrintContent(sTradeLicenseIDs);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetLocalResourceObject("Company.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Sponsor.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("SponserFee.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("LicenseNumber.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("City.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Province.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Partner.Text") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td></tr>");
        sb.Append("<tr><td colspan='11'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CompanyName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Sponsor"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SponserFee"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["City"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Province"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Partner"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }


    public string GetMultipleInsurancePrint(string MultipleInsuranceIDs)
    {
        clsInsurance objclsInsurance = new clsInsurance();
        DataTable datData = objclsInsurance.GetMultipleInsurance(MultipleInsuranceIDs);
        if (datData.Rows.Count > 0)
        {
            string str = string.Empty;
            str = clsUserMaster.GetCulture() == "ar-AE" ? ("تأمين") : ("Insurance");
            return GetDocumentHtmlInsurance(datData, str, 2);
        }
        return "";

    }

    public string GetInsurancePrint(int id )
    {
        clsInsurance objclsInsurance = new clsInsurance();
        objclsInsurance.intInsuranceID = id;
        DataTable datData = objclsInsurance.GetInsurance();
        if (datData.Rows.Count > 0)
        {
            string str = string.Empty;
            str = clsUserMaster.GetCulture() == "ar-AE" ? ("تأمين") : ("Insurance");
            return GetDocumentHtmlInsurance(datData, str, 1);
        }
        return "";

    }

    private string GetDocumentHtmlInsurance(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        //string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        //string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";
        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
        string strHeaderRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";

        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        string strAltRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + " : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("InsuranceDetails.Text") + " </div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Asset.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Asset"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyNumber.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyName.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyName"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("InsuranceCompany.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["InsuranceCompany"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyAmount.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyAmount"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("ControlsCommon", "Remarks") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Asset.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyNumber.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyName.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("InsuranceCompany.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyAmount.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strHeaderRowInner20Style + ">" + GetGlobalResourceObject("ControlsCommon", "Remarks") + "</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Asset"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyNumber"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["InsuranceCompany"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyAmount"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["IssueDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ExpiryDate"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["Remarks"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }
}
