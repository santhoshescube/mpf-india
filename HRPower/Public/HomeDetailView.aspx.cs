﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.VisualBasic;
using System.Collections;
using System.Collections.Generic;


public partial class Public_HomeDetailView : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsEmployee objEmployee;
    clsRoleSettings objRoleSettings;
    clsCommonMessage objComMessage;

    protected void Page_Load(object sender, EventArgs e)
    {
        objUserMaster = new clsUserMaster();
        objEmployee = new clsEmployee();
        objRoleSettings = new clsRoleSettings();
        if (!IsPostBack)
        {
            objEmployee.EmployeeID = objUserMaster.GetEmployeeId();
            objHomePage = new clsHomePage();
            FillCompany();
            BindDashBoard();
            // Managerview permission
            int RoleID = objUserMaster.GetRoleId();
            bool IsManager = false;
            if (RoleID <= 4 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.ManagerView))) // Check manager view permission
                IsManager = true;

            SetWidget(IsManager);

            litRequests.Text = clsGlobalization.IsArabicCulture() ? "طلباتي" : "My Requests";
            litMessages.Text = clsGlobalization.IsArabicCulture() ? "الرسائل" : "Messages";
            litPerformance.Text = clsGlobalization.IsArabicCulture() ? "تقييم الأداء" : "Performance Evaluation";
            litTodaysLateComers.Text = clsGlobalization.IsArabicCulture() ? "المتأخرين" : "Todays Late Comers";
            litAttendance.Text = clsGlobalization.IsArabicCulture() ? "حضوري" : "My Attendance";
            litMyPerformance.Text = clsGlobalization.IsArabicCulture() ? " أدائي " : "My Performance";
            litJobOpening.Text = clsGlobalization.IsArabicCulture() ? "وضائف شاغرة" : "Job Openings";

           

        }
    }
    // fill company
    private void FillCompany()
    {
        objHomePage = new clsHomePage();
        ddlCompany.DataSource = objHomePage.GetAllCompany(objUserMaster.GetEmployeeId(),objUserMaster.GetUserId());
        ddlCompany.DataBind();
       // ddlCompany.SelectedValue = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUserMaster.GetCompanyId().ToString())).ToString();
    }
    private void FillLeavetypes()
    {
        ddlLeaveType.DataSource = clsHomePage.GetLeaveTypes();
        ddlLeaveType.DataBind();
    }

    #region FillMyRequests
    //Requests
    private void FillMyRequests(int EmpID)
    {
        rptMyRequests.DataSource = objHomePage.GetMyRequests(EmpID, ddlCompany.SelectedValue.ToInt32());
        rptMyRequests.DataBind();
    }

    #endregion FillMyRequests

    protected void GetLeaveDetails()
    {
        DataTable dtLeave = clsHomePage.GetLeaveDetails(ddlCompany.SelectedValue.ToInt32(), txtLeaveMonth.Text.ToDateTime().ToString("dd MMM yyyy"), ddlLeaveType.SelectedValue.ToInt32());
        if (dtLeave.Rows.Count > 0)
        {
            rptLeave.DataSource = dtLeave;
            rptLeave.DataBind();
            lblLeave.Text = clsGlobalization.IsArabicCulture() ? "ترك" : "Leave" + " ( " + dtLeave.Rows.Count + " ) ";
          
        }
        else
        {
            lblLeave.Text = clsGlobalization.IsArabicCulture() ? "تقرير الغياب اليومي " : "Leave";
            rptLeave.DataSource = null;
            rptLeave.DataBind();
        }
    }
    protected void BindDashBoard()
    {
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        int iEmployeeId = objUserMaster.GetEmployeeId();
        FillLeavetypes();
       


      txtLeaveMonth.Text = System.DateTime.Now.Date.ToString("MMM/yyyy");
        //Announcements
        rptAnnouncements.DataSource = objHomePage.GetAnnouncements(iEmployeeId,ddlCompany.SelectedValue.ToInt32());
        rptAnnouncements.DataBind();

        // Messages
        rptMessages.DataSource = objHomePage.GetMessages(iEmployeeId, objUserMaster.GetRoleId(),ddlCompany.SelectedValue.ToInt32());
        rptMessages.DataBind();

        FillMyRequests(iEmployeeId);

        //Evaluations

        rptEvaluation.DataSource = clsHomePage.GetEvaluations(iEmployeeId, ddlCompany.SelectedValue.ToInt32());
        rptEvaluation.DataBind();

        FillJobTray(ddlCompany.SelectedValue.ToInt32());

        // Document return Alert
        DataSet dsRetAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GAR",ddlCompany.SelectedValue.ToInt32());
        rptAlert.DataSource = dsRetAlert.Tables[1];
        rptAlert.DataBind();
        int RetCount = dsRetAlert.Tables[0].Rows[0]["AlCount"].ToInt32();
        lblRetAlert.Text = RetCount > 0 ? clsGlobalization.IsArabicCulture() ? "منبه ارجاع الوثائق و المستندات" : "Document Return Alerts" + " ( " + RetCount + " )" : clsGlobalization.IsArabicCulture() ? "منبه ارجاع الوثائق و المستندات" : "Document Return Alerts";

        // Bind chart
        BindChart();

        // Attendance
        if (new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.MyAttendance).ToUpper() == "YES")
        {
            //tmAttendance.Enabled = true;
            divMyAttendance.Style["display"] = "block";
            BindAttendance();
        }
        else
        {
            //tmAttendance.Enabled = false;
            divMyAttendance.Style["display"] = "none";
        }

        //---------------------------------------------------------------------- ManagerView Permissionssss

        int RoleID = objUserMaster.GetRoleId();
        if (RoleID <= 3 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.ManagerView))) // Check manager view permission
        {


            // Document expiry alert
            DataSet dsExpyAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GA",ddlCompany.SelectedValue.ToInt32());

            rptExpiryAlert.DataSource = dsExpyAlert.Tables[1];
            rptExpiryAlert.DataBind();
            int Count = dsExpyAlert.Tables[0].Rows[0]["AlCount"].ToInt32();
            lblAlert.Text = Count > 0 ? clsGlobalization.IsArabicCulture() ? "منبه انتهاء صلاحية الوثائق" : "Document Expiry Alerts" + " ( " + Count + " )" : clsGlobalization.IsArabicCulture() ? "منبه انتهاء صلاحية الوثائق" : "Document Expiry Alerts";

            // Employee Leave details

            GetLeaveDetails();


            // Employee Vacation requests
            DataTable dtVacation = clsHomePage.GetVacationLeaveDetails(ddlCompany.SelectedValue.ToInt32());
            if (dtVacation.Rows.Count > 0)
            {
                rptVacLeaveRequest.DataSource = dtVacation;
                rptVacLeaveRequest.DataBind();
                lblVacLeaveRequest.Text = clsGlobalization.IsArabicCulture() ? "طلبات العطل و الاجازات" : "Vacation Leave Requests" + " ( " + dtVacation.Rows.Count + " ) ";
            }
            else
            {
                lblVacLeaveRequest.Text = clsGlobalization.IsArabicCulture() ? "طلبات العطل و الاجازات" : "Vacation Leave Requests";
                rptVacLeaveRequest.DataSource = null;
                rptVacLeaveRequest.DataBind();
            }

            // Employee Vacation Processing

            DataTable dtVacationProcess = clsHomePage.GetVacationLeaveProcessing(ddlCompany.SelectedValue.ToInt32());
            if (dtVacationProcess.Rows.Count > 0)
            {
                rptVacLeaveProcessing.DataSource = dtVacationProcess;
                rptVacLeaveProcessing.DataBind();
                lblVacLeaveProcessing.Text = clsGlobalization.IsArabicCulture() ? "معالجة الاجازة" : "Vacation Processing" + " ( " + dtVacationProcess.Rows.Count + " ) ";
            }
            else
                lblVacLeaveProcessing.Text = clsGlobalization.IsArabicCulture() ? "معالجة الاجازة" : "Vacation Processing";

            //Late Comers List
            DataTable dtLateComers = clsHomePage.GetLateComers(ddlCompany.SelectedValue.ToInt32());
            if (dtLateComers.Rows.Count > 0)
            {
                rptLateComers.DataSource = dtLateComers;
                rptLateComers.DataBind();
            }

        }
        else
        {
            divExpyAlert.Visible = divLeave.Visible = divVacLeave.Visible = divLate.Visible = divVacProcess.Visible = false;
        }


    }

    private void BindChart()
    {
        DataTable dtTemp = clsSalesPerformance.GetBarChart(new clsUserMaster().GetEmployeeId());
        if (dtTemp.Rows.Count > 0)
        {
            divPerformance.Style["display"] = "block";

            int MaxCount = dtTemp.Rows.Count;
            string xValues = string.Empty;
            string yValues = string.Empty;
            string zValues = string.Empty;
            if (dtTemp.Rows.Count > 0)
            {
                for (int i = 0; i < dtTemp.Rows.Count; i++)
                {
                    xValues = xValues + "," + string.Format("{0}", Convert.ToString(dtTemp.Rows[i]["Month"]));
                    yValues = yValues + "," + string.Format("{0}", Convert.ToString(dtTemp.Rows[i]["Projection"]));
                    zValues = zValues + "," + string.Format("{0}", Convert.ToString(dtTemp.Rows[i]["Actual"]));
                }
                xValues = xValues.Substring(1, xValues.Length - 1);
                yValues = yValues.Substring(1, yValues.Length - 1);
                zValues = zValues.Substring(1, zValues.Length - 1);
                imgPerformance.Src = string.Format("SalesTargetChart.aspx?Height=400&Width=400&xValues={0}&yValues={1}&zValues={2}&IsFromHomePage={3}&Type={4}&Docking={5}", xValues, yValues, zValues, "True", System.Web.UI.DataVisualization.Charting.SeriesChartType.Column.ToString(), System.Web.UI.DataVisualization.Charting.Docking.Bottom);
            }
        }
        else
        {
            divPerformance.Style["display"] = "none";
        }
    }

    #region Messages

    protected void lnkMessage_Click(object sender, EventArgs e)
    {
        try
        {
            string sPendingRequestType = string.Empty;
            //objMessage = new clsMessage();
            objComMessage = new clsCommonMessage();
            objUserMaster = new clsUserMaster();
            RepeaterItem rItm = (RepeaterItem)((LinkButton)sender).Parent;
            HiddenField hfValues = (HiddenField)rItm.FindControl("hfValues");
            HiddenField hfStatusID = (HiddenField)rItm.FindControl("hfStatusID");
            int MessageTypeId = Convert.ToInt32(hfValues.Value);
            int StatusID = Convert.ToInt32(hfStatusID.Value);

            //  int iInitiationid =Convert.ToInt32(hfValues.Value.Split(',')[0]);
            Session["MessageType"] = MessageTypeId;
            Session["StatusID"] = StatusID;



            objComMessage.MessageType = (eMessageType)Enum.Parse(typeof(eMessageType), hfValues.Value);



            bool isReaded = objComMessage.SetReadStatus(objUserMaster.GetEmployeeId());

            switch (MessageTypeId)
            {

                case (int)eMessageType.VacancyApproval:
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Job").ToString();
                      Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
                
                    break;


                case (int)eMessageType.Compensatory__Off_Request:
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "CompensatoryOff").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
                    break;


                case (int)eMessageType.Time__Extension_Request :
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "TimeExtension").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
               
                    break;

                case (int)eMessageType.Vacation__Extension_Request:
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "VacationExtension").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
               
                    break;

                case (int)eMessageType.Leave__Extension_Request :
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "LeaveExtension").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
               
                    break;


                case (int)eMessageType.HireCandidate:

                    Response.Redirect("HomeActiveCandidates.aspx?CandidateStatusID=5");
                
                    break;
           
             
                case (int)eMessageType.PendingOfferStatus:
                    Response.Redirect("HomeOfferApproval.aspx");
                  
                    break;
                case (int)eMessageType.OfferRequest:
                    sPendingRequestType = "Offer Letter";

                    Response.Redirect("HomeOfferApproval.aspx?RequestType=" + sPendingRequestType + "");

                    break;

               // case (int)eMessageType.VacancyApproval:
                case (int)eMessageType.VacancyRequest:
                    Response.Redirect("HomeActiveVcancies.aspx");

                    
                    break;

                case (int)eMessageType.HiringPlan:
                case (int)eMessageType.HiringPlanApproval:
                case (int)eMessageType.HiringPlanCancel:
                    sPendingRequestType = "Hiring Plan";

                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    break;
              
                case (int)eMessageType.LeaveRequest:
                case (int)eMessageType.LeaveApproval:
                case (int)eMessageType.LeaveForward:
                case (int)eMessageType.LeaveRejection:
                case (int)eMessageType.LeaveCancelRequest:
                case (int)eMessageType.LeaveCancellation:

                    //sPendingRequestType = "Leave";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Leave").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");


                    break;

                case (int)eMessageType.CompensatoryOffExtensionRequest:
                case (int)eMessageType.CompensatoryOffExtensionApproval:
                case (int)eMessageType.CompensatoryOffExtensionForward:
                case (int)eMessageType.CompensatoryOffExtensionRejection:
                case (int)eMessageType.CompensatoryOffExtensionCancelRequest:
                case (int)eMessageType.CompensatoryOffExtensionCancellation:

                    //sPendingRequestType = "Leave";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "CompensatoryOff").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    break;
                case (int)eMessageType.VacationRequest:
                case (int)eMessageType.VacationApproval:
                case (int)eMessageType.VacationForward:
                case (int)eMessageType.VacationRejection:
                case (int)eMessageType.VacationCancelRequest:
                case (int)eMessageType.VacationCancellation:

                    //sPendingRequestType = "Vacation Leave";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "VacationLeave").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");


                    break;
                case (int)eMessageType.LoanRequest:
                case (int)eMessageType.LoanApproval:
                case (int)eMessageType.LoanForward:
                case (int)eMessageType.LoanRejection:
                case (int)eMessageType.LoanCancelRequest:
                case (int)eMessageType.LoanCancellation:

                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Loan").ToString();
                   // sPendingRequestType = "Loan";

                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    //ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText("Loan"));
                    //BindPendingRequests();
                    //upPendingRequests.Update();
                    break;

                case (int)eMessageType.ExpenseRequest:
                case (int)eMessageType.ExpenseApproval:
                case (int)eMessageType.ExpenseForward:
                case (int)eMessageType.ExpenseRejection:
                case (int)eMessageType.ExpenseCancelRequest:
                case (int)eMessageType.ExpenseCancellation:

                    //sPendingRequestType = "Expense";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Expense").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
                    break;

                case (int)eMessageType.SalaryAdvanceRequest:
                case (int)eMessageType.SalaryAdvanceApproval:
                case (int)eMessageType.SalaryAdvanceForward:
                case (int)eMessageType.SalaryAdvanceRejection:
                case (int)eMessageType.SalaryAdvanceCancelRequest:
                case (int)eMessageType.SalaryAdvanceCancellation:

                    //sPendingRequestType = "Salary Advance";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "SalaryAdvance").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    //ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText("Salary Advance"));
                    //BindPendingRequests();
                    //upPendingRequests.Update();
                    break;
                case (int)eMessageType.TransferRequest:
                case (int)eMessageType.TransferApproval:
                case (int)eMessageType.TransferForward:
                case (int)eMessageType.TransferRejection:
                case (int)eMessageType.TransferCancelRequest:
                case (int)eMessageType.TransferCancellation:

                   // sPendingRequestType = "Transfer";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Transfer").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");


                    //ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText("Transfer"));
                    //BindPendingRequests();
                    //upPendingRequests.Update();
                    break;
         

                case (int)eMessageType.DocumentRequest:
                case (int)eMessageType.DocumentApproval:
                case (int)eMessageType.DocumentForward:
                case (int)eMessageType.DocumentRejection:
                case (int)eMessageType.DocumentCancelRequest:
                case (int)eMessageType.DocumentCancellation:

                   // sPendingRequestType = "Document";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Document").ToString(); 
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    //ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText("Document"));
                    //BindPendingRequests();
                    //upPendingRequests.Update();
                    break;
                case (int)eMessageType.VisaRequest:
                case (int)eMessageType.VisaApproval:
                case (int)eMessageType.VisaForward:
                case (int)eMessageType.VisaRejection:
                case (int)eMessageType.VisaCancelRequest:
                case (int)eMessageType.VisaCancellation:

                    sPendingRequestType = "Visa";

                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    //ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText("Visa"));
                    //BindPendingRequests();
                    //upPendingRequests.Update();
                    break;
              
                  
              
                case (int)eMessageType.ResignationCancelRequest:
                case (int)eMessageType.ResignationForward:
                case (int)eMessageType.ResignationCancellation:
                case (int)eMessageType.ResignationRequest:
                case (int)eMessageType.ResignationRejection:
                case (int)eMessageType.ResignationApproval:

                    //sPendingRequestType = "Time Extension";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Resignation").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    break;
                case (int)eMessageType.TicketCancelRequest:
                case (int)eMessageType.TicketForward:
                case (int)eMessageType.TicketCancellation:
                case (int)eMessageType.TicketRequest:
                case (int)eMessageType.TicketRejection:
                case (int)eMessageType.TicketApproval:

                    //sPendingRequestType = "Time Extension";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Ticket").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    break;

                case (int)eMessageType.TravelCancelRequest:
                case (int)eMessageType.TravelForward:
                case (int)eMessageType.TravelCancellation:
                case (int)eMessageType.TravelRequest:
                case (int)eMessageType.TravelRejection:
                case (int)eMessageType.TravelApproval:

                    //sPendingRequestType = "Time Extension";
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Travel").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

                    break;

                case (int)eMessageType.PettyCash:

                    Response.Redirect("PettyCash.aspx?PettyCashId=" + new clsUserMaster().GetEmployeeId());

                    break;
                case (int)eMessageType.PettyCashDocument:
                    Response.Redirect("HomePettyCash.aspx");
                    break;
                case (int)eMessageType.AttendnceApproval:
                case (int)eMessageType.AttendnceCancellation:
                case (int)eMessageType.AttendnceCancelRequest:
                case (int)eMessageType.AttendnceForward:
                case (int)eMessageType.AttendnceRejection:
                case (int)eMessageType.AttendnceRequest:
                    sPendingRequestType = GetGlobalResourceObject("MasterPageCommon", "Attendance").ToString();
                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");
                    break;

                default:

                    sPendingRequestType = "";

                    Response.Redirect("HomePendingRequests.aspx?RequestType=" + sPendingRequestType + "");

              
                    break;

            }
        }
        catch (Exception)
        {
        }

    }

    protected void rptMessages_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        try
        {
            HiddenField hfReadStatus = (HiddenField)e.Item.FindControl("hfReadStatus");
            if (Convert.ToInt32(hfReadStatus.Value) == 0)
            {
                LinkButton lnkMessage = (LinkButton)e.Item.FindControl("lnkMessage");
                lnkMessage.Style["color"] = "gray";
                //lnkMessage.CssClass = "readedMessage";
            }
        }
        catch (Exception)
        {
        }
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        objHomePage = new clsHomePage();

        objComMessage = new clsCommonMessage();
        objUserMaster = new clsUserMaster();

        try
        {
            RepeaterItem rItm = (RepeaterItem)((ImageButton)sender).Parent;
            HiddenField hfValues = (HiddenField)rItm.FindControl("hfValues");
            HiddenField hfStatusID = (HiddenField)rItm.FindControl("hfStatusID");

            int MessageTypeId = Convert.ToInt32(hfValues.Value);
            int StatusID = Convert.ToInt32(hfStatusID.Value);

            objComMessage.MessageType = (eMessageType)Enum.Parse(typeof(eMessageType), hfValues.Value);

            if (objComMessage.SetDeleteStatus(objUserMaster.GetEmployeeId(), StatusID))
            {
                DataTable dtMessages = objHomePage.GetMessages(objUserMaster.GetEmployeeId(), objUserMaster.GetRoleId(),ddlCompany.SelectedValue.ToInt32());//objHomePage.GetMessages(objMessage.RequestedTo, objUserMaster.GetRoleId());
                rptMessages.DataSource = dtMessages;
                rptMessages.DataBind();
            }


            //  upnlDashBoard.Update();
        }
        catch (Exception)
        {
        }

    }

    #endregion Messages

    #region MyRequests

    protected void rptMyRequests_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hfReadStatus = (HiddenField)e.Item.FindControl("hfReadStatus");
        if (Convert.ToInt32(hfReadStatus.Value) == 1)
        {
            HtmlAnchor ancMyMessage = (HtmlAnchor)e.Item.FindControl("ancMyMessage");
            ancMyMessage.Style["color"] = "gray";
            //ancMyMessage.CssClass = "readedMessage";
        }
    }
    protected void imgMyRequestDelete_Click(object sender, ImageClickEventArgs e)
    {


        objUserMaster = new clsUserMaster();
        objHomePage = new clsHomePage();
        objComMessage = new clsCommonMessage();

        try
        {
            RepeaterItem rItm = (RepeaterItem)((ImageButton)sender).Parent;

            HiddenField hfMyRequestMyRequest = (HiddenField)rItm.FindControl("hfMyRequestMyRequest");
            HiddenField hfMessageID = (HiddenField)rItm.FindControl("hfMessageID");

            //int MessageTypeId = Convert.ToInt32(hfMyRequestMyRequest.Value);
            //string  MessggeID =Convert.ToString( hfMessageID.Value);

            objComMessage.DeleteMessage(hfMessageID.Value.ToInt64(), objUserMaster.GetEmployeeId());

            rptMyRequests.DataSource = objHomePage.GetMyRequests(objUserMaster.GetEmployeeId(), ddlCompany.SelectedValue.ToInt32());
            rptMyRequests.DataBind();


        }
        catch (Exception)
        {
        }
    }

    #endregion MyRequests

    #region Evaluation
    protected void lnkEvalMessage_Click(object sender, EventArgs e)
    {
        try
        {
            objComMessage = new clsCommonMessage();

            RepeaterItem rItm = (RepeaterItem)((LinkButton)sender).Parent;
            HiddenField hfValues = (HiddenField)rItm.FindControl("hfValues");
            HiddenField hfReferenceId = (HiddenField)rItm.FindControl("hfReferenceId");
            int MessageTypeId = Convert.ToInt32(hfValues.Value);
            objComMessage.MessageType = (eMessageType)Enum.Parse(typeof(eMessageType), hfValues.Value);
            switch (MessageTypeId)
            {
                case (int)eMessageType.PerformanceEvaluation:
                    Response.Redirect("PerformanceEvaluation.aspx?PerformanceInitiationId=" + hfReferenceId.Value.ToInt32().ToString());
                    break;
                case (int)eMessageType.PerformanceAction:
                    Response.Redirect("Action.aspx?RequestID=" + hfReferenceId.Value.ToInt32().ToString());
                    break;
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion Evaluation

    #region Announcements

    protected void rptAnnouncements_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


    }

    #endregion Announcements

    #region Alert

    protected void rptExpiryAlert_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hfExpiryStatus = (HiddenField)e.Item.FindControl("hfExpiryStatus");
        if (Convert.ToString(hfExpiryStatus.Value) == "Y")
        {
            LinkButton lnkExpiryAlert = (LinkButton)e.Item.FindControl("lnkExpiryAlert");
            lnkExpiryAlert.Style["color"] = "Red";
            //lnkMessage.CssClass = "readedMessage";
        }
    }


    protected void txtfrom_TextChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    protected void txtTo_TextChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();
    }
    protected void lnkAlert_OnClick(object sender, EventArgs e)
    {

        try
        {
            RepeaterItem repItem = (RepeaterItem)((LinkButton)sender).Parent;
            HiddenField hfDocumentTypeID = (HiddenField)repItem.FindControl("hfDocumentTypeID");

            FillDocumentType();

            if (ddlDocType.Items.Count > 0)
            {
                ddlDocType.SelectedIndex = (ddlDocType.Items.IndexOf(ddlDocType.Items.FindByValue(hfDocumentTypeID.Value)));
            }
            FillAlerts();

            btnAddEmployee.OnClientClick = "return valPrintEmailDatalist('" + dlAlerts.ClientID + "');";

            mpeEmployee.Show();
            updAlert.Update();
        }
        catch (Exception)
        {
        }
    }
    private void FillDocumentType()
    {
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
        int iEmployeeId = objUserMaster.GetEmployeeId();

        ddlDocType.DataTextField = "Description";
        ddlDocType.DataValueField = "DocumentTypeId";
        ddlDocType.DataSource = objHomePage.FillDocumentType(objUserMaster.GetUserId());
        ddlDocType.DataBind();
    }
    private void FillAlerts()
    {
        objUserMaster = new clsUserMaster();
        objHomePage = new clsHomePage();
        dlAlerts.DataSource = null;
        DataTable datAlerts = null;

        DateTime? dtFromDate = null;
        DateTime? dtToDate = null;

        hfdIsExpiry.Value = "0";
        try
        {
            if (txtfrom.Text != "") dtFromDate = clsCommon.Convert2DateTime(txtfrom.Text);
            if (txtTo.Text != "") dtToDate = clsCommon.Convert2DateTime(txtTo.Text);


            datAlerts = clsAlerts.GetAlerts(dtFromDate, dtToDate, objUserMaster.GetUserId(), 0, ddlDocType.SelectedItem.Text, 100, 0);

            if (datAlerts == null || datAlerts.Rows.Count == 0)
            {
                dlAlerts.DataSource = null;
                dlAlerts.DataBind();
            }
            else
            {
                dlAlerts.DataSource = datAlerts;
                dlAlerts.DataBind();
            }
            upnlPopup.Update();
        }
        catch (Exception)
        {
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.CancelPopup();
    }


    protected void btnAddEmployee_Click(object sender, EventArgs e)
    {
        try
        {
            objHomePage = new clsHomePage();
            List<AlertsDetails> Alerts = new List<AlertsDetails>();

            if (dlAlerts.Items.Count > 0)
            {
                DataRow dr;

                foreach (DataListItem item in dlAlerts.Items)
                {
                    CheckBox chkAlert = (CheckBox)item.FindControl("chkAlert");
                    HiddenField hfdDocTypeID = (HiddenField)item.FindControl("hfdDocTypeID");
                    HiddenField hfdCommonID = (HiddenField)item.FindControl("hfdCommonID");

                    if (chkAlert.Checked)
                    {
                        Alerts.Add(new AlertsDetails()
                        {
                            CommonId = hfdCommonID.Value.ToInt32(),
                            DocumentTypeID = hfdDocTypeID.Value.ToInt32()
                        }
                         );
                    }
                }
                if (new clsAlerts().DeleteAlerts(Alerts))
                {
                    if (hfdIsExpiry.Value == "1") FillExpiryAlerts(); else FillAlerts();

                }
            }
            // Document return alert
            DataSet dsReturnAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GAR",ddlCompany.SelectedValue.ToInt32());
            rptAlert.DataSource = dsReturnAlert.Tables[1];
            rptAlert.DataBind();
            int RetCount = dsReturnAlert.Tables[0].Rows[0]["AlCount"].ToInt32();
           // lblRetAlert.Text = RetCount > 0 ? "Document Return Alerts" + " ( " + RetCount + " )" : "Document Return Alerts";
            lblRetAlert.Text = RetCount > 0 ? clsGlobalization.IsArabicCulture() ? "منبه ارجاع الوثائق و المستندات" : "Document Return Alerts" + " ( " + RetCount + " )" : "Document Return Alerts";

            updAlert.Update();

            // Document expiry alert
            DataSet dsExpyAlert = objHomePage.GetAlert(objUserMaster.GetUserId(), "GA",ddlCompany.SelectedValue.ToInt32());

            rptExpiryAlert.DataSource = dsExpyAlert.Tables[1];
            rptExpiryAlert.DataBind();
            int Count = dsExpyAlert.Tables[0].Rows[0]["AlCount"].ToInt32();

            //lblAlert.Text = Count > 0 ? "Document Expiry Alerts" + " ( " + Count + " )" : "Document Expiry Alerts";
            lblAlert.Text = Count > 0 ? clsGlobalization.IsArabicCulture() ? "منبه انتهاء صلاحية الوثائق" : "Document Expiry Alerts" + " ( " + Count + " )" : "Document Expiry Alerts";

            updExpiryAlert.Update();
        }
        catch (Exception)
        {
        }
    }
    private void CancelPopup()
    {
        if (mpeEmployee != null) mpeEmployee.Hide();
    }

    #endregion Alert

    #region Attendance
    protected void BindAttendance()
    {
        try
        {
            DataTable dtbreaktime = null;
            string shiftToTime = string.Empty;
            lblConsequence.Text = string.Empty;
            lblShift.Text = string.Empty;
            lblPolicy.Text = string.Empty;
            lblConseq1.Text = string.Empty;
            lblConseq2.Text = string.Empty;
            lblConseq3.Text = string.Empty;
            lblConseq4.Text = string.Empty;

            bool bExists = false;
            if (txtDate.Text == string.Empty)
                txtDate.Text = DateTime.Now.ToString("dd-MMM-yyyy");

            DateTime dtDate = clsCommon.Convert2DateTime(txtDate.Text);

            DataSet dsAttendance = null;
            DataTable dtAttendance = null;
            DataTable dtAttendanceSummary = null;

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();
            int iEmployeeId = objUserMaster.GetEmployeeId();
            int iCompanyID = objUserMaster.GetCompanyId();
            int iDayID = 0;
            if (Convert.ToInt32(dtDate.DayOfWeek) == 6)
            {
                iDayID = 1;
            }
            else if (dtDate.DayOfWeek == 0)
            {
                iDayID = 2;
            }
            else
            {
                iDayID = Convert.ToInt32(dtDate.DayOfWeek) + 2;
            }

            if (objHomePage.IsAttendanceExists(iEmployeeId, iCompanyID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy")))
            {
                bExists = true;
                dtAttendance = objHomePage.GetEmployeeDayEntryExitDetails(iEmployeeId, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"), 0);
                dtAttendanceSummary = objHomePage.GetAttendanceSummary(iEmployeeId, iCompanyID, iDayID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"));
            }
            else
            {
                bExists = false;
                dsAttendance = objHomePage.GetAttendance(iEmployeeId, iCompanyID, iDayID, clsCommon.Convert2DateTime(txtDate.Text).ToString("dd-MMM-yyyy"));
                dtAttendance = dsAttendance.Tables[0];

            }
            //--------------------------------------------------------Breaktime  after normal worktime----------------------------------

            if (dtAttendance.Rows.Count > 0)
            {
                lblNoData.Style["display"] = "none";
                dlAttendanceDetails.Visible = true;
                tblsummary.Visible = true;
                dlAttendanceDetails.DataSource = dtAttendance;
                dlAttendanceDetails.DataBind();
            }
            else
            {
                dlAttendanceDetails.DataSource = null;
                lblNoData.Style["display"] = "block";
                lblNoData.Text = clsGlobalization.IsArabicCulture() ? "لا يوجد بصم" : "No punchings found.";
                dlAttendanceDetails.Visible = false;
                tblsummary.Visible = false;
            }

            //------------------------------------------------------------Calculation---------------------------------------------------
            String stotalworktime = "0";
            String stotalbreaktime = "0";

            String sworktime = "0";
            String sbreaktime = "0";

            int ssworktime = 0;
            int ssbreaktime = 0;

            int iWorktime = 0;
            int ibreaktime = 0;
            if (dtAttendance.Rows.Count > 0)
            {
                int i;
                for (i = 0; i <= dtAttendance.Rows.Count - 1; i++)
                {
                    if (dtAttendance.Rows[i].ItemArray[3].ToString() != "" && dtAttendance.Rows[i].ItemArray[3].ToString() != "0")
                    {
                        ssworktime += Convert.ToInt32((DateAndTime.DateDiff(DateInterval.Minute, Convert.ToDateTime(dtAttendance.Rows[i].ItemArray[3]), DateAndTime.Today, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)));
                    }

                }
                for (i = 0; i <= dtAttendance.Rows.Count - 1; i++)
                {
                    if (dtAttendance.Rows[i].ItemArray[4].ToString() != "" && dtAttendance.Rows[i].ItemArray[4].ToString() != "0")
                    {
                        ssbreaktime += Convert.ToInt32((DateAndTime.DateDiff(DateInterval.Minute, Convert.ToDateTime(dtAttendance.Rows[i].ItemArray[4]), DateAndTime.Today, Microsoft.VisualBasic.FirstDayOfWeek.Sunday, FirstWeekOfYear.Jan1)));
                    }

                }


                TimeSpan Worktimeresult = TimeSpan.FromMinutes(Math.Abs(ssworktime));
                TimeSpan Breaktimeresult = TimeSpan.FromMinutes(Math.Abs(ssbreaktime));

                lblWorktime.Text = string.Format("{0:D2}:{1:D2} : {2:D2}", Worktimeresult.Hours, Worktimeresult.Minutes, Worktimeresult.Seconds); ;
                lblBreaktime.Text = string.Format("{0:D2}:{1:D2} : {2:D2}", Breaktimeresult.Hours, Breaktimeresult.Minutes, Breaktimeresult.Seconds);

                if (bExists == false)
                {
                    if (dsAttendance.Tables[1].Rows.Count > 0)
                    {
                        lblShift.Text = "Shift - " + Convert.ToString(dsAttendance.Tables[1].Rows[0]["ShiftName"]) + "(" + Convert.ToString(dsAttendance.Tables[1].Rows[0]["FromTime"]) + "-" + Convert.ToString(dsAttendance.Tables[1].Rows[0]["ToTime"]) + ")";
                        lblNormalWorktime.Text = Convert.ToString(dsAttendance.Tables[1].Rows[0]["Duration"]);
                        lblAllowedBreakTime.Text = Convert.ToString(dsAttendance.Tables[1].Rows[0]["AllowedBreakTime"]);
                    }
                }
                else
                {
                    if (dtAttendanceSummary.Rows.Count > 0)
                    {
                        lblShift.Text = "Shift - " + Convert.ToString(dtAttendanceSummary.Rows[0]["ShiftName"]) + "(" + Convert.ToString(dtAttendanceSummary.Rows[0]["FromTime"]) + "-" + Convert.ToString(dtAttendanceSummary.Rows[0]["ToTime"]) + ")";
                        lblNormalWorktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["Duration"]);
                        lblAllowedBreakTime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["AllowedBreakTime"]);
                        //lblWorktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["WorkTime"]);
                        //lblBreaktime.Text = Convert.ToString(dtAttendanceSummary.Rows[0]["BreakTime"]);
                    }
                }
            }

            if (dtAttendance.Rows.Count > 0)
            {
                //DataTable dtConsequence = objHomePage.GetPolicyConsequence(iEmployeeId);
                //if (dtConsequence.Rows.Count > 0)
                //{
                //    lblConsequence.Text = "Consequences";
                //    lblPolicy.Text = "Policy name - " + Convert.ToString(dtConsequence.Rows[0]["BreakTime"]);
                //    lblConseq1.Text = Convert.ToString(dtConsequence.Rows[1]["BreakTime"]);
                //    lblConseq2.Text = Convert.ToString(dtConsequence.Rows[2]["BreakTime"]);
                //    lblConseq3.Text = Convert.ToString(dtConsequence.Rows[3]["BreakTime"]);
                //    lblConseq4.Text = Convert.ToString(dtConsequence.Rows[4]["BreakTime"]);
                //}
            }

        }
        catch (Exception ex)
        {
        }
    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        BindAttendance();
    }

    protected void txtLeaveMonth_TextChanged(object sender, EventArgs e)
    {
        GetLeaveDetails();
    }
    #endregion Attendance

    #region AlterSalaryDesignation

    public string GetToolTip(object oMessageTypeId)
    {
        //string sMode=Convert.ToString(oMode);

        switch (Convert.ToInt32(oMessageTypeId))
        {
            case (int)eMessageType.HireCandidate:
                return "Click here to hire candidate";

            case (int)eMessageType.CandiateEmployeeIntegragtion:
                return "Click here to make candidate as employee";


            case (int)eMessageType.PendingOfferStatus:
                return "Click here to send offer letter";


            case (int)eMessageType.VacancyRequest:
                return "Click here to view pending vacancies";


            case (int)eMessageType.AlterSalary:
                return "Click here to Apply Salary Structure";

            case (int)eMessageType.JobPromotion:
                return "Click here to Apply job promotion";

            default:
                return "Click here to view pending requests";


        }
    }
   
  
    protected void btnCancelAlterSalary_Click(object sender, EventArgs e)
    {
        this.CancelPopup();
    }
  
    protected void btnCancelDesignation_Click(object sender, EventArgs e)
    {
        this.CancelPopup();
    }

    #endregion AlterSalaryDesignation

    #region Widget
    // Widget Creation
    private void SetWidget(bool IsManager)
    {
        DataTable dtWidget = clsHomePage.GetUserWidgets(new clsUserMaster().GetUserId(), (IsManager ? 1 : 0));
        if (dtWidget.Rows.Count > 0)
        {
            foreach (DataRow row in dtWidget.Rows)
            {
                SetWidget(row["WidgetID"].ToInt32(), (row["Checked"].ToInt32() == 0 ? false : true), IsManager);
            }
        }


    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {

        foreach (DataListItem item in dlMenu.Items)
        {

            object chkSelect = item.FindControl("chkSelect");

            if (chkSelect != null)
            {
                ((CheckBox)chkSelect).Checked = ((System.Web.UI.WebControls.CheckBox)(sender)).Checked;
            }

        }
        //updMenu.Update();
        //upnlMenu.Update();

    }
    protected void chkRequest_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void lnkMenu_OnClick(object sender, EventArgs e)
    {
        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int RoleID = objUserMaster.GetRoleId();
        bool IsManager = false;
        if (RoleID <= 3 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.ManagerView))) // Check manager view permission
            IsManager = true;

        dlMenu.DataSource = clsHomePage.GetUserWidgets(new clsUserMaster().GetUserId(), (IsManager ? 1 : 0));
        dlMenu.DataBind();

        upnlMenu.Update();
        //updMainWidget.Update();
        updMenu.Update();
        mpeMenu.Show();
    }

    protected void btnWidgetSave_Click(object sender, EventArgs e)
    {
        string sWidgetIDs = string.Empty;

        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        // Managerview permission
        int RoleID = objUserMaster.GetRoleId();
        bool IsManager = false;
        if (RoleID <= 3 || (objRoleSettings.IsMenuEnabled(RoleID, (int)eMenuID.ManagerView))) // Check manager view permission
            IsManager = true;

        foreach (DataListItem item in dlMenu.Items)
        {
            CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");

            if (chkSelect.Checked)
            {
                SetWidget(dlMenu.DataKeys[item.ItemIndex].ToInt32(), true, IsManager);
                sWidgetIDs += "," + Convert.ToString(dlMenu.DataKeys[item.ItemIndex]);
            }
            else
            {
                SetWidget(dlMenu.DataKeys[item.ItemIndex].ToInt32(), false, IsManager);
            }
        }
        objHomePage = new clsHomePage();
        objHomePage.UpdateUserWidget(new clsUserMaster().GetUserId(), sWidgetIDs);
        updMainWidget.Update();
        mpeMenu.Hide();
        Response.Redirect("HomeDetailView.aspx");
        //updSaveWidget.Update();
        //  FillWidgets(sWidgetIDs);

    }
    private void SetWidget(int WidgetID, bool IsChecked, bool IsManager)
    {
        switch (WidgetID)
        {
            case (int)WidgetReference.Announcements:
                break;

            case (int)WidgetReference.DocumentReturnAlert:
                divReturnAlert.Style["display"] = IsChecked ? "block" : "none";
                break;
            case (int)WidgetReference.Messages:
                divMessage.Style["display"] = IsChecked ? "block" : "none";
                break;
            case (int)WidgetReference.MyPerformance:
                divPerformance.Style["display"] = IsChecked ? "block" : "none";
                break;
            case (int)WidgetReference.MyRequests:
                divRequest.Style["display"] = IsChecked ? "block" : "none";
                break;
            case (int)WidgetReference.RecruitmentInfo:
                divPerEval.Style["display"] = IsChecked ? "block" : "none";
                break;

            case (int)WidgetReference.Attendance:
                divMyAttendance.Style["display"] = IsChecked ? "block" : "none";
                break;

            case (int)WidgetReference.VacationProcess:
                divVacProcess.Style["display"] = IsChecked && IsManager ? "block" : "none";
                break;
            case (int)WidgetReference.VacationRequest:
                divVacLeave.Style["display"] = IsChecked && IsManager ? "block" : "none";
                break;
            case (int)WidgetReference.DocumentExpiryAlert:
                divExpyAlert.Style["display"] = IsChecked && IsManager ? "block" : "none";
                break;
            case (int)WidgetReference.LateComers:
                divLate.Style["display"] = IsChecked && IsManager ? "block" : "none";
                break;
            case (int)WidgetReference.LeaveDetails:
                divLeave.Style["display"] = IsChecked && IsManager ? "block" : "none";
                break;
        }
    }

    #endregion Widget
    // ------------------------------------------------------------ManagerView Permission-----------------------------------------------------------//

    #region ManagerViewPermission
    protected void lnkExpiryAlert_OnClick(object sender, EventArgs e)
    {

        RepeaterItem rptExpiryAlert = (RepeaterItem)((LinkButton)sender).Parent;
        HiddenField hfExpiryDocumentTypeID = (HiddenField)rptExpiryAlert.FindControl("hfExpiryDocumentTypeID");

        FillDocumentType();

        if (ddlDocType.Items.Count > 0)
        {
            ddlDocType.SelectedIndex = (ddlDocType.Items.IndexOf(ddlDocType.Items.FindByValue(hfExpiryDocumentTypeID.Value)));
        }

        FillExpiryAlerts();

        btnAddEmployee.OnClientClick = "return valPrintEmailDatalist('" + dlAlerts.ClientID + "');";

        mpeEmployee.Show();
        updAlert.Update();
    }

    private void FillExpiryAlerts()
    {
        objUserMaster = new clsUserMaster();
        objHomePage = new clsHomePage();

        dlAlerts.DataSource = null;
        DataTable datAlerts = null;

        DateTime? dtFromDate = null;
        DateTime? dtToDate = null;

        hfdIsExpiry.Value = "1";

        if (txtfrom.Text != "") dtFromDate = clsCommon.Convert2DateTime(txtfrom.Text);
        if (txtTo.Text != "") dtToDate = clsCommon.Convert2DateTime(txtTo.Text);


        datAlerts = clsAlerts.GetAlerts(dtFromDate, dtToDate, objUserMaster.GetUserId(), 1, ddlDocType.SelectedItem.Text, 100, 0);

        if (datAlerts == null || datAlerts.Rows.Count == 0)
        {
            dlAlerts.DataSource = null;
            dlAlerts.DataBind();
        }
        else
        {
            dlAlerts.DataSource = datAlerts;
            dlAlerts.DataBind();
        }
        upnlPopup.Update();

    }

    protected void lnkViewMoreVacation_OnClick(object sender, EventArgs e)
    {

    }

    protected void imgDetailView_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("ManagerHomeNew.aspx");
    }
    #endregion ManagerViewPermission


    #region RightMenuEvents
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {

        TableCell cell = (TableCell)e.Cell;
        cell.Width = Unit.Pixel(30);
        cell.Height = Unit.Pixel(30);
        if (e.Day.Date.ToString("dd MMM yyyy") == System.DateTime.Now.Date.ToString("dd MMM yyyy"))
        {
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Red;
            //cell.CssClass = "Currendate";
        }
        //cell.BorderStyle = BorderStyle.Solid;
        //cell.BorderWidth = Unit.Pixel(1);
        //cell.BorderColor = System.Drawing.Color.Black;
        cell.Style["font-size"] = "10px";
        //cell.Style["padding"] = "0";
        cell.Attributes.Add("title", "");
        if (e.Day.IsOtherMonth)
        {
            cell.Text = "";
            cell.CssClass = "tblBackground1";

        }
        else
        {

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();

            DataSet ds = objHomePage.GetCalendarDetails(objUserMaster.GetEmployeeId(), e.Day.Date.ToString("dd MMM yyyy"));
            cell.CssClass = "tblBackground1";
            string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : " Holiday:");
            if (ds.Tables[0].Rows.Count > 0)
            {
                sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                cell.ForeColor = System.Drawing.Color.Red;
            }
            string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : " Events: \n");
            int icount = 1;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                icount++;
            }
            if (sEvent != string.Empty)
                cell.ForeColor = System.Drawing.Color.Green;
            if (sEvent != string.Empty && sHoliday != string.Empty)
                cell.ForeColor = System.Drawing.Color.FromArgb(244, 166, 29);
            //cell.ForeColor = System.Drawing.Color.Magenta;
            string sTitle = (sHoliday == string.Empty ? string.Empty : sHoliday) + (sEvent == string.Empty ? string.Empty : sEvent);
            if (sTitle != string.Empty)
                cell.Attributes.Add("title", sTitle);

        }
    }

    private void FillJobTray(int CompanyID)
    {
        DataTable dt = clsHomePage.FillJob(CompanyID);

        repJob.DataSource = dt;
        repJob.DataBind();

    }

    protected void repJob_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater repSkills = (Repeater)e.Item.FindControl("repSkills");
        HiddenField hfdJobID = (HiddenField)e.Item.FindControl("hfdJobID");
        repSkills.DataSource = clsHomePage.FillJobDetails(hfdJobID.Value.ToInt32());
        repSkills.DataBind();
    }
    #endregion RightMenuEvents
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDashBoard();
        updMainWidget.Update();
        updjob.Update();
        updVacLeave.Update();
    }
    protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetLeaveDetails();
    }
}
