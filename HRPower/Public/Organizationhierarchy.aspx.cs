﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Organizationhierarchy : System.Web.UI.Page
{
    clsHieararchy objOrganizationHierarchy;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaser;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblWelcomeText.Text = new clsEmployee() { EmployeeID = new clsUserMaster().GetEmployeeId() }.GetWelcomeText();
        LtPerformanceAndTraining.Text = GetGlobalResourceObject("MasterPageCommon", "Performance").ToString();
       
        LitPlanningAndRecruitment.Text = GetGlobalResourceObject("MasterPageCommon", "Recruitment").ToString();
        if (!IsPostBack)
        {
            objRoleSettings = new clsRoleSettings();
            objUserMaser = new clsUserMaster();
            objOrganizationHierarchy = new clsHieararchy();
            if (objRoleSettings.IsMenuEnabled(objUserMaser.GetRoleId(), (int)eMenuID.HierarchyHR))
            {
                BindParentDesignation();
                BindAllDesignations();
                BindTree();
            }
            else
            {
                divMain.Visible = false;
                trRemove.Visible = false;

                divError.Visible = true;
                lblPermission.Text = "You don't have enough permission to view this page.";
            }
            SetPermission();

        }
        GetCompanyDetails();
    }


    private void GetCompanyDetails()
    {
        DataTable dt = clsUserMaster.GetUserCompanyDetail();
        if (dt.Rows.Count > 0)
        {
            imgCompanyLogo.ImageUrl = GetLogo(90);
            lblCompanyName.Text = dt.Rows[0]["CompanyName"].ToString();
        }
    }
    public string GetLogo(int width)
    {
        clsUserMaster objUser = new clsUserMaster();
        return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + objUser.GetCompanyId() + "&t=" + DateTime.Now.Ticks;
    }

    private void SetPermission()
    {
        int RoleID = 0;
        RoleID = objUserMaser.GetRoleId();

        if (RoleID > 3)
        {

            DataTable dtPermissions = objRoleSettings.GetModulePermissions(RoleID, (int)eModuleID.HomePage);
            if (dtPermissions.Rows.Count == 0)
            {

                ancHome.Disabled = ancEmployee.Disabled = ancPerformance.Disabled = ancSettings.Disabled = ancreports.Disabled = true;
                ancHome.HRef = ancEmployee.HRef = ancPerformance.HRef =  ancSettings.HRef = ancreports.HRef = "";
            }
            else
            {
                dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HomePage).ToString(); // Home
                ancHome.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancHome.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/HomeDetailView.aspx" : "";


               

                dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Employee).ToString(); // Employee
                ancEmployee.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancEmployee.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/Employee.aspx" : "";             


                dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Performance).ToString(); // Performance
                ancPerformance.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancPerformance.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/PerformanceInitiation.aspx" : "";

                dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.Reports).ToString(); // Reports
                ancreports.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancreports.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/reporting/RptCommonEss.aspx" : "";


                dtPermissions.DefaultView.RowFilter = "ModuleID =" + ((int)eModuleID.HRSettings).ToString(); // Settings
                ancSettings.Disabled = !(dtPermissions.DefaultView.ToTable().Rows.Count > 0);
                ancSettings.HRef = (dtPermissions.DefaultView.ToTable().Rows.Count > 0) ? "~/Public/RequestSettings.aspx" : "";

            }

        }

        if (RoleID == 4) // interviewer
        {
            ancHome.Disabled = false;
            ancHome.HRef = "~/Public/HomeDetailView.aspx";
            // Response.Redirect("~/public/Job.aspx");
        }



        if (RoleID < 5) ancManager.Visible = true;
        if (RoleID == 5) ancManager.Visible = false;

        if (RoleID > 5)
        {
            if (objRoleSettings.GetModulePermissions(RoleID, 0).Rows.Count > 0)
            {
                ancManager.Visible = true;
            }
            else
            {
                ancManager.Visible = false;
            }
        }

    }
    private void BindParentDesignation()
    {
        objOrganizationHierarchy = new clsHieararchy();
        ddlParentDesignation.DataSource = objOrganizationHierarchy.GetAllParents();
        ddlParentDesignation.DataBind();
        ddlParentDesignation.Items.Insert(0, new ListItem("None", "0"));
    }


    private void BindAllDesignations()
    {
        objOrganizationHierarchy = new clsHieararchy();
        lstChildDesignations.DataSource = objOrganizationHierarchy.GetAllDesignations();
        lstChildDesignations.DataBind();

    }
    private void BindTree()
    {
        objOrganizationHierarchy = new clsHieararchy();
        tvOrganizationalHierarchy.Nodes.Clear();
        DataTable dt = objOrganizationHierarchy.GetAllTreeParents();
        PopulateNodes(dt, tvOrganizationalHierarchy.Nodes);
        tvOrganizationalHierarchy.ExpandAll();
    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in dt.Rows)
        {
            TreeNode tn = new TreeNode();

            tn.Text = dw["Designation"].ToString();
            tn.Value = dw["HierarchyId"].ToString();

            nodes.Add(tn);

            tn.PopulateOnDemand = ((int)(dw["ChildCount"]) > 0);
        }
    }
    private void Populate(int iHierarchyId, TreeNode parentNode)
    {
        objOrganizationHierarchy = new clsHieararchy();

        objOrganizationHierarchy.HierarchyId = iHierarchyId;
        DataTable dt = objOrganizationHierarchy.GetAllNodes();

        PopulateNodes(dt, parentNode.ChildNodes);
    }
    protected void tvOrganizationalHierarchy_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        Populate(Convert.ToInt32(e.Node.Value), e.Node);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objOrganizationHierarchy = new clsHieararchy();
        objOrganizationHierarchy.Begin();

        foreach (TreeNode node in tvOrganizationalHierarchy.CheckedNodes)
        {
            try
            {

                DeleteAllNodes(node);

            }
            catch
            {
                objOrganizationHierarchy.Rollback();
                return;
            }

        }

        objOrganizationHierarchy.Commit();
        BindParentDesignation();
        BindAllDesignations();
        BindTree();
        upTree.Update();
    }

    private void DeleteAllNodes(TreeNode node)
    {

        if (node.ChildNodes.Count > 0)
        {
            foreach (TreeNode ChildNode in node.ChildNodes)
            {
                if (ChildNode.ChildNodes.Count > 0)
                {
                    DeleteAllNodes(ChildNode);
                }
                else
                {
                    DeleteNodes(ChildNode);
                }

            }
        }
        DeleteNodes(node);
    }
    private void DeleteNodes(TreeNode node)
    {

        objOrganizationHierarchy.Delete(Convert.ToInt32(node.Value));
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            objOrganizationHierarchy = new clsHieararchy();
            objOrganizationHierarchy.Begin();

            foreach (ListItem item in lstChildDesignations.Items)
            {
                if (item.Selected)
                {
                    objOrganizationHierarchy.DesignationID = Convert.ToInt32(item.Value);
                    objOrganizationHierarchy.ParentId = Convert.ToInt32(ddlParentDesignation.SelectedValue);
                    objOrganizationHierarchy.Insert();
                }
            }
            objOrganizationHierarchy.Commit();
        }
        catch (Exception ex)
        {
            objOrganizationHierarchy.Rollback();
        }

        BindParentDesignation();
        BindAllDesignations();
        BindTree();
        upTree.Update();
    }
  
    protected void lnkViewEmployeeHierarchy_Click(object sender, ImageClickEventArgs e)
    {

        HierarchyViewControl1.ShowHierarchy(eType.EmployeeHierarchy);
        mdHierarchy.Show();
        updModalHierarchy.Update();
        updHierarchy.Update();
    }
    protected void lnkViewOrganisationHierarchy_Click1(object sender, ImageClickEventArgs e)
    {
        HierarchyViewControl1.ShowHierarchy(eType.OrganizationHierarchy);
        mdHierarchy.Show();
        updModalHierarchy.Update();
        updHierarchy.Update(); 
    }
}
