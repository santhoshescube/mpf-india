﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using HRAutoComplete;

public partial class Public_OfferLetter : System.Web.UI.Page
{
    clsTemplates objTemplates;
    clsOfferLetter objOfferLetter;
    clsMailSettings objMailSettings;
    clsCommon objCommon;
    clsXML objXML;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUser;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("Offer.Title").ToString();

        OfferletterPager.Fill += new controls_Pager.FillPager(BindDatalist);
        // Auto complete
        this.RegisterAutoComplete();

        if (!IsPostBack)
        {
            objOfferLetter = new clsOfferLetter();
            objRoleSettings = new clsRoleSettings();
            objUser = new clsUserMaster();



            if (!objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsView"))
            {
                msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لعرض هذه الصفحة." : "You dont have enough permission to view this page.");
                mpeMessage.Show();
                OfferletterPager.Visible = false;
                lnkSend.Enabled = lnkCancelOfferLetter.Enabled = lnkView.Enabled = false;
            }
            else
            {
                lnkSend.Enabled = lnkCancelOfferLetter.Enabled = lnkView.Enabled = true;
                int iCandidateId = 0, iVacancyId = 0;
                if (Request.QueryString["OfferId"] != null)
                {
                    BindFormview(iCandidateId, iVacancyId, Convert.ToInt32(Request.QueryString["OfferId"]));
                    OfferletterPager.Visible = false;
                }
                else if (Request.QueryString["CandidateId"] == null && Request.QueryString["JobID"] == null)
                {
                    BindDatalist();
                }
                else
                {

                    iCandidateId = Convert.ToInt32(Request.QueryString["CandidateId"]);
                    iVacancyId = Convert.ToInt32(Request.QueryString["JobID"]);
                    lnkSend.Enabled = lnkCancelOfferLetter.Enabled = lnkView.Enabled = true;

                    objOfferLetter.CandidateId = iCandidateId;
                    objOfferLetter.VacancyId = iVacancyId;

                    int iOfferId = objOfferLetter.HasSentOfferLetter();

                    if (iOfferId > 0)
                    {
                        BindFormview(iCandidateId, iVacancyId, iOfferId);
                        OfferletterPager.Visible = false;
                    }
                    else
                    {
                        fvOfferletter.ChangeMode(FormViewMode.Insert);

                        fvOfferletter.DataSource = null;
                        fvOfferletter.DataBind();
                    }

                }
                SetPermissions();
            }
        }
    }

    #region Functions

    public MailMessage GetOfferletterContent()
    {
        AjaxControlToolkit.HTMLEditor.Editor ajcOfferEditor = (AjaxControlToolkit.HTMLEditor.Editor)fvOfferletter.FindControl("ajcOfferEditor");
        TextBox txtTo = (TextBox)fvOfferletter.FindControl("txtTo");
        TextBox txtBasicPay = (TextBox)fvOfferletter.FindControl("txtBasicPay");
        TextBox txtSalary = (TextBox)fvOfferletter.FindControl("txtSalary");
        TextBox txtTerms = (TextBox)fvOfferletter.FindControl("txtTerms");
        DataList dlAllowanceDeduction = (DataList)fvOfferletter.FindControl("dlAllowanceDeduction");
        HiddenField hfCurrentDate = (HiddenField)fvOfferletter.FindControl("hfCurrentDate");
        DropDownList ddlPaymentMode = (DropDownList)fvOfferletter.FindControl("ddlPaymentMode");
        TextBox txtDateofJoining = (TextBox)fvOfferletter.FindControl("txtDateofJoining");
        HiddenField hfCandidateId = (HiddenField)fvOfferletter.FindControl("hfCandidateId");
        HiddenField hfVacancyId = (HiddenField)fvOfferletter.FindControl("hfJobId");
        DropDownList ddlDesignation = (DropDownList)fvOfferletter.FindControl("ddlDesignation");

        string sAllowances = string.Empty;
        string sDeductions = string.Empty;



        objOfferLetter = new clsOfferLetter();
        objOfferLetter.VacancyId = Convert.ToInt32(hfVacancyId.Value);
        objOfferLetter.CandidateId = Convert.ToInt32(hfCandidateId.Value);

        DataTable dt = objOfferLetter.GetTemplateFieldDetails();

        //Preview offer letter of the candidate

        MailDefinition mdSendOffer = new MailDefinition();
        ListDictionary ldReplacements = new ListDictionary();
        MailMessage mmContent;

        ldReplacements.Add("<%Candidate Name%>", dt.Rows[0]["CandidateName"].ToString());
        ldReplacements.Add("<%Company Name%>", dt.Rows[0]["Companyname"].ToString());
        ldReplacements.Add("<%Job Title%>", dt.Rows[0]["Job"].ToString());
        ldReplacements.Add("<%Gross Salary%>", txtSalary.Text.ToString());
        ldReplacements.Add("<%Basic Pay%>", txtBasicPay.Text.ToString());
        ldReplacements.Add("<%Payment Mode%>", ddlPaymentMode.SelectedItem.Text);
        ldReplacements.Add("<%Sent Date%>", hfCurrentDate.Value);
        ldReplacements.Add("<%Expected Join Date%>", txtDateofJoining.Text);
        ldReplacements.Add("<%Place of Work%>", dt.Rows[0]["Placeofwork"].ToString());
        ldReplacements.Add("<%Daily Working hours%>", dt.Rows[0]["DailyWorkingHour"].ToString());
        ldReplacements.Add("<%Weekly working days%>", dt.Rows[0]["Weeklyworkingdays"].ToString());
        ldReplacements.Add("<%Contract Period%>", dt.Rows[0]["ContractPeriod"].ToString());
        ldReplacements.Add("<%Accomodation Available%>", dt.Rows[0]["IsAccomodation"].ToString());
        ldReplacements.Add("<%Transportation Available%>", dt.Rows[0]["Istransportation"].ToString());
        ldReplacements.Add("<%Air Ticket%>", dt.Rows[0]["AirTicket"].ToString());
        ldReplacements.Add("<%Annual Leave days%>", dt.Rows[0]["AnnualLeaveDays"].ToString());
        ldReplacements.Add("<%Legal Terms%>", txtTerms.Text);
        ldReplacements.Add("<%Department%>", dt.Rows[0]["Department"].ToString());
        ldReplacements.Add("<%Designation%>", ddlDesignation.SelectedItem.Text);
        ldReplacements.Add("<%EmploymentType%>", dt.Rows[0]["EmploymentType"].ToString());

        mdSendOffer.From = "info@HrPower.com";
        mdSendOffer.Priority = MailPriority.High;

        string sMessage = ajcOfferEditor.Content.Replace("{$$", "<%");
        sMessage = sMessage.Replace("$$}", "%>");

        mmContent = mdSendOffer.CreateMailMessage(txtTo.Text, ldReplacements, sMessage, this);

        return mmContent;
    }
    private void SetPermissions()
    {
        string Message = string.Empty;

        objRoleSettings = new clsRoleSettings();

        if (objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsCreate") | objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsUpdate"))
        {
            lnkSend.OnClientClick = "return chkOffers('" + dlSentOffers.ClientID + "');";
        }
        else
        {
            Message = clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لإرسال خطاب العرض." : "You dont have enough permission to send offerletter.";
            lnkSend.OnClientClick = "alert('" + Message + "');return false;";

        }
        if (objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsDelete"))
        {
            lnkCancelOfferLetter.OnClientClick = "return checkOfferLetterCancel('" + dlSentOffers.ClientID + "');";
        }
        else
        {
            Message = clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لإلغاء العرض حرف.." : "You dont have enough permission to cancel offer-letter.";
            lnkCancelOfferLetter.OnClientClick = "alert('" + Message + "');return false;";
        }


    }

    public void BindDatalist()
    {
        string Message = string.Empty;

        objOfferLetter = new clsOfferLetter();
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        objOfferLetter.PageIndex = OfferletterPager.CurrentPage + 1;
        objOfferLetter.PageSize = OfferletterPager.PageSize;
        objOfferLetter.SortExpression = "OfferId";
        objOfferLetter.SortOrder = "desc";
        objOfferLetter.SearchKey = txtSearch.Text;


        DataTable dt = objOfferLetter.GetOfferLetters();

        dlSentOffers.DataSource = dt;
        dlSentOffers.DataBind();

        if (dt.Rows.Count == 0)
        {
            OfferletterPager.Visible = false;
            lnkSend.OnClientClick = lnkCancelOfferLetter.OnClientClick = "return false;";
        }
        else
        {
            OfferletterPager.Visible = lnkSend.Enabled = lnkCancelOfferLetter.Enabled = true;

            lnkSend.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsCreate");
            lnkCancelOfferLetter.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsDelete");
            if (lnkCancelOfferLetter.Enabled)
                lnkCancelOfferLetter.OnClientClick = "return checkOfferLetterCancel('" + dlSentOffers.ClientID + "');";
            else
            {
                Message = clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لإلغاء العرض حرف." : "You dont have enough permission to cancel offer-letter.";
                lnkCancelOfferLetter.OnClientClick = "alert('" + Message + "');return false;";
            }

            if (lnkSend.Enabled)
                lnkSend.OnClientClick = "return chkOffers('" + dlSentOffers.ClientID + "');";
            else
            {
                Message = clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لإرسال خطاب العرض.." : "You dont have enough permission to send offerletter.";
                lnkSend.OnClientClick = "alert('" + Message + "');return false;";
            }

            OfferletterPager.Total = objOfferLetter.GetTotalCount();


            lnkView.Enabled = false;
        }

        fvOfferletter.ChangeMode(FormViewMode.ReadOnly);

        fvOfferletter.DataSource = null;
        fvOfferletter.DataBind();


        upPopuppnl.Update();
        upMenus.Update();
    }

    public void BindFormview(int iCandidateId, int iVacancyId, int iOfferId)
    {
        OfferletterPager.Visible = false;
        objOfferLetter = new clsOfferLetter();

        fvOfferletter.ChangeMode(FormViewMode.Edit);

        objOfferLetter.CandidateId = iCandidateId;
        objOfferLetter.VacancyId = iVacancyId;
        objOfferLetter.OfferId = iOfferId;

        ViewState["CandidateId"] = iCandidateId;
        ViewState["JobId"] = iVacancyId;

        DataTable dt = objOfferLetter.GetVacancyDetails();

        fvOfferletter.DataSource = dt;
        fvOfferletter.DataBind();


        dlSentOffers.DataSource = null;
        dlSentOffers.DataBind();

    }

    public bool IsHired(object oStatus)
    {
        return (Convert.ToString(oStatus) == "Hired" ? false : true);
    }

    public string OfferLetterPreview(int iOfferId)
    {
        objOfferLetter = new clsOfferLetter();
        objCommon = new clsCommon();
        objXML = new clsXML();

        string sAllowances = string.Empty;
        string sDeductions = string.Empty;

        objXML.AttributeId = iOfferId;

        string sOfferletter = objXML.GetOfferLetterXml();

        objOfferLetter.OfferId = iOfferId;

        DataSet ds = objOfferLetter.GetSavedOfferLetter();

        DataTable dtOfferletter = ds.Tables[0];
        DataTable dtOfferDetails = ds.Tables[1];
        DataTable dtJobDetails = ds.Tables[2];

        //for (int i = 0; i < dtOfferDetails.Rows.Count; i++)
        //{
        //    if (Convert.ToBoolean(dtOfferDetails.Rows[i]["Addition"]))
        //        sAllowances = (sAllowances == string.Empty ? Convert.ToString(dtOfferDetails.Rows[i]["AllowanceName"]) + "-" +
        //            Convert.ToString(dtOfferDetails.Rows[i]["AllowanceAmount"]) : sAllowances + "," + (Convert.ToString(dtOfferDetails.Rows[i]["AllowanceName"]) + "-" + Convert.ToString(dtOfferDetails.Rows[i]["AllowanceAmount"])));
        //    else
        //        sDeductions = (sDeductions == string.Empty ? Convert.ToString(dtOfferDetails.Rows[i]["AllowanceName"]) + "-" +
        //            Convert.ToString(dtOfferDetails.Rows[i]["AllowanceAmount"]) : sDeductions + "," + (Convert.ToString(dtOfferDetails.Rows[i]["AllowanceName"]) + "-" + Convert.ToString(dtOfferDetails.Rows[i]["AllowanceAmount"])));
        //}

        MailDefinition mdSendOffer = new MailDefinition();
        ListDictionary ldReplacements = new ListDictionary();
        MailMessage mmContent;

        ldReplacements.Add("<%Candidate Name%>", Convert.ToString(dtOfferletter.Rows[0]["Candidate"]));
        ldReplacements.Add("<%Company Name%>", Convert.ToString(dtOfferletter.Rows[0]["CompanyName"]));
        ldReplacements.Add("<%Job Title%>", Convert.ToString(dtOfferletter.Rows[0]["Vacancy"]));
        ldReplacements.Add("<%Gross Salary%>", Convert.ToString(dtOfferletter.Rows[0]["PerAnnum"]));
        ldReplacements.Add("<%Basic Pay%>", Convert.ToString(dtOfferletter.Rows[0]["BasicPay"]));
        ldReplacements.Add("<%Payment Mode%>", Convert.ToString(dtOfferletter.Rows[0]["PaymentMode"]));
        ldReplacements.Add("<%Sent Date%>", Convert.ToString(dtOfferletter.Rows[0]["SentDate"]));
        ldReplacements.Add("<%Expected Join Date%>", Convert.ToString(dtOfferletter.Rows[0]["DateofJoining"]));

        ldReplacements.Add("<%Place of Work%>", dtJobDetails.Rows[0]["Placeofwork"].ToString());
        ldReplacements.Add("<%Daily Working hours%>", dtJobDetails.Rows[0]["DailyWorkingHour"].ToString());
        ldReplacements.Add("<%Weekly working days%>", dtJobDetails.Rows[0]["Weeklyworkingdays"].ToString());
        ldReplacements.Add("<%Contract Period%>", dtJobDetails.Rows[0]["ContractPeriod"].ToString());
        ldReplacements.Add("<%Accomodation Available%>", dtJobDetails.Rows[0]["IsAccomodation"].ToString());
        ldReplacements.Add("<%Transportation Available%>", dtJobDetails.Rows[0]["Istransportation"].ToString());
        ldReplacements.Add("<%Air Ticket%>", dtJobDetails.Rows[0]["AirTicket"].ToString());
        ldReplacements.Add("<%Annual Leave days%>", dtJobDetails.Rows[0]["AnnualLeaveDays"].ToString());
        ldReplacements.Add("<%Legal Terms%>", dtOfferletter.Rows[0]["Terms"].ToString());
        ldReplacements.Add("<%Department%>", dtJobDetails.Rows[0]["Department"].ToString());
        ldReplacements.Add("<%Designation%>", dtOfferletter.Rows[0]["Designation"].ToString());
        ldReplacements.Add("<%EmploymentType%>", dtJobDetails.Rows[0]["EmploymentType"].ToString());


        mdSendOffer.From = "info@HrPower.com";
        mdSendOffer.Priority = MailPriority.High;

        string sMessage = sOfferletter.Replace("{$$", "<%");
        sMessage = sMessage.Replace("$$}", "%>");


        mmContent = mdSendOffer.CreateMailMessage(Convert.ToString(dtOfferletter.Rows[0]["Email"]), ldReplacements, sMessage, this);
        return mmContent.Body;
    }

    public void SetPrintOffer(DataListItem e)
    {
        ImageButton imgPrint = (ImageButton)e.FindControl("imgPrint");
        Panel pnlPrint = (Panel)e.FindControl("pnlPrint");

        Table tbl = new Table();

        tbl.ID = "tblPrint" + e.ItemIndex;

        tbl.Attributes.Add("font", "Tahoma, MS Sans Serif 11px");
        tbl.Width = Unit.Percentage(100);

        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        td.Text = string.Empty;
        td.Text = objCommon.GetHeader() + OfferLetterPreview(Convert.ToInt32(dlSentOffers.DataKeys[e.ItemIndex]));

        tr.Cells.Add(td);
        tbl.Rows.Add(tr);

        pnlPrint.Controls.Add(tbl);

        ScriptManager.RegisterClientScriptBlock(imgPrint, imgPrint.GetType(), Guid.NewGuid().ToString(), "Print('" + pnlPrint.ClientID + "')", true);

        //  imgPrint.Attributes.Add("onclick", "Print('" + pnlPrint.ClientID + "')");
    }

    #endregion

    #region Events

    protected void dlAllowanceDeduction_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objOfferLetter = new clsOfferLetter();

        if (e.Item.ItemIndex != -1)
        {
            DropDownList ddlAllowancedeductions = (DropDownList)e.Item.FindControl("ddlAllowancedeductions");
            Label lblAddition = (Label)e.Item.FindControl("lblAddition");
            //if (lblAddition.Text == "")
            //    ddlAllowancedeductions.Visible = true;
            //else if (lblAddition.Text == "Basic Pay")
            //    e.Item.Style["display"] = "none";

            RadioButtonList rbtnAllowanceDeduction = (RadioButtonList)e.Item.FindControl("rbtnAllowanceDeduction");
            HiddenField hfAddition = (HiddenField)e.Item.FindControl("hfAddition");
            HiddenField hfDeductionPolicy = (HiddenField)e.Item.FindControl("hfDeductionPolicy");
            HiddenField hfPayPolicy = (HiddenField)e.Item.FindControl("hfPayPolicy");
            TextBox txtAmt = (TextBox)e.Item.FindControl("txtAmt");
            Label lblPayPolicyName = (Label)e.Item.FindControl("lblPayPolicyName");

            DataTable dt = objOfferLetter.GetAllowances();

            ddlAllowancedeductions.DataSource = dt;
            ddlAllowancedeductions.DataBind();

            string str = string.Empty;
            str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
            ddlAllowancedeductions.Items.Insert(0, new ListItem(str, "-1"));

            ddlAllowancedeductions.SelectedIndex = ddlAllowancedeductions.Items.IndexOf(ddlAllowancedeductions.Items.FindByText(Convert.ToString(dlAllowanceDeduction.DataKeys[e.Item.ItemIndex])));

            if (hfAddition.Value != string.Empty)
            {
                if (Convert.ToBoolean(hfAddition.Value))
                    rbtnAllowanceDeduction.SelectedIndex = 0;
                else
                    rbtnAllowanceDeduction.SelectedIndex = 1;

                rbtnAllowanceDeduction.Enabled = false;

                //if (hfDeductionPolicy.Value != string.Empty)
                //{
                //    if (Convert.ToBoolean(hfDeductionPolicy.Value))
                //    {
                //        txtAmt.CssClass = "textbox_disabled";
                //        txtAmt.Enabled = false;
                //    }
                //    else if (hfPayPolicy.Value != string.Empty)
                //    {
                //        txtAmt.Style["display"] = "none";
                //        txtAmt.Enabled = false;

                //        lblPayPolicyName.Style["display"] = "block";
                //        lblPayPolicyName.Text = hfPayPolicy.Value;
                //    }
                //    else
                //    {
                //        txtAmt.CssClass = "textbox";
                //        txtAmt.Enabled = true;
                //    }
                //}
            }
            //else
            //{
            //    txtAmt.CssClass = "textbox_disabled";
            //    txtAmt.Enabled = false;
            //}
        }

    }

    protected void dlAllowanceDeduction_ItemCommand(object source, DataListCommandEventArgs e)
    {
        DataTable dt;
        DataRow dr;

        TextBox txtBasicPay = (TextBox)fvOfferletter.FindControl("txtBasicPay");

        switch (e.CommandName)
        {
            case "_Remove":

                dt = new DataTable();

                dt.Columns.Add("AdditionDeduction");
                dt.Columns.Add("AdditionDeductionID");
                dt.Columns.Add("Amount");
                //dt.Columns.Add("Addition");
                dt.Columns.Add("IsAddition");
                dt.Columns.Add("DeductionPolicy");
                dt.Columns.Add("PayPolicy");

                foreach (DataListItem item in dlAllowanceDeduction.Items)
                {
                    DropDownList ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                    Label lblAddition = (Label)item.FindControl("lblAddition");
                    TextBox txtAmt = (TextBox)item.FindControl("txtAmt");
                    RadioButtonList rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                    HiddenField hfDeductionPolicy = (HiddenField)item.FindControl("hfDeductionPolicy");
                    HiddenField hfPayPolicy = (HiddenField)item.FindControl("hfPayPolicy");

                    dr = dt.NewRow();
                    dr["AdditionDeduction"] = ddlAllowancedeductions.SelectedItem.Text;
                    dr["Amount"] = txtAmt.Text;
                    dr["IsAddition"] = (ddlAllowancedeductions.SelectedValue == "-1" ? string.Empty : (Convert.ToString(rbtnAllowanceDeduction.SelectedValue == "1" ? true : false)));
                    dr["DeductionPolicy"] = "";
                    dr["PayPolicy"] = Convert.ToString(hfPayPolicy.Value);

                    if (item != e.Item)
                        dt.Rows.Add(dr);

                    ViewState["AllowanceDeduction"] = dt;
                }

                if (dt.Rows.Count > 0)
                {
                    dlAllowanceDeduction.DataSource = ViewState["AllowanceDeduction"];
                    dlAllowanceDeduction.DataBind();
                }
                else
                {
                    dr = dt.NewRow();
                    dt.Rows.Add(dr);

                    dlAllowanceDeduction.DataSource = dt;
                    dlAllowanceDeduction.DataBind();
                }

                decimal dSalary = 0;

                dSalary = (txtBasicPay.Text == string.Empty ? Convert.ToDecimal(0) : Convert.ToDecimal(txtBasicPay.Text));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(Convert.ToString(dt.Rows[i]["IsAddition"]) == string.Empty ? false : dt.Rows[i]["IsAddition"]))
                        dSalary = dSalary + Convert.ToDecimal(Convert.ToString(dt.Rows[i]["Amount"]) == string.Empty ? 0 : dt.Rows[i]["Amount"]);
                    else
                        dSalary = dSalary - Convert.ToDecimal(Convert.ToString(dt.Rows[i]["Amount"]) == string.Empty ? 0 : dt.Rows[i]["Amount"]);
                }

                DropDownList ddlPaymentMode = (DropDownList)fvOfferletter.FindControl("ddlPaymentMode");

                dSalary = dSalary * 12;
                txtSalary.Text = (dSalary == 0 ? "0.00" : string.Format("{0:#,###.00}", dSalary));

                ((TextBox)fvOfferletter.FindControl("txtSalary")).Text = string.Format("{0:#,###.00}", dSalary);

                break;
        }
    }
    protected void ddlOfferStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        HtmlTableRow trRemarks = (HtmlTableRow)fvOfferletter.FindControl("trRemarks");
        if (trRemarks != null)
        {
            trRemarks.Style["display"] = ddlOfferStatus.SelectedValue == "3" ? "table-row" : "none";
        }

    }
    protected void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
    {
        objTemplates = new clsTemplates();
        objXML = new clsXML();

        TextBox txtSubject = (TextBox)fvOfferletter.FindControl("txtSubject");
        AjaxControlToolkit.HTMLEditor.Editor ajcOfferEditor = (AjaxControlToolkit.HTMLEditor.Editor)fvOfferletter.FindControl("ajcOfferEditor");
        Button btnPreview = (Button)fvOfferletter.FindControl("btnPreview");

        if (Convert.ToInt32(ddlTemplates.SelectedValue) == -1)
        {
            txtSubject.Text = ajcOfferEditor.Content = string.Empty;
            btnPreview.Enabled = false;
        }
        else
        {
            objTemplates.TemplateId = Convert.ToInt32(ddlTemplates.SelectedValue);

            DataTable dt = objTemplates.GetTemplatesDetails();

            txtSubject.Text = Convert.ToString(dt.Rows[0]["Subject"]);

            objXML.AttributeId = Convert.ToInt32(ddlTemplates.SelectedValue);

            ajcOfferEditor.Content = objXML.GetTemplateMessage();

            btnPreview.Enabled = true;
        }
    }

    protected void fvOfferletter_DataBound(object sender, EventArgs e)
    {


        if (fvOfferletter.CurrentMode == FormViewMode.Edit || fvOfferletter.CurrentMode == FormViewMode.Insert)
        {
            OfferletterPager.Visible = false;

            lnkSend.Enabled = lnkCancelOfferLetter.Enabled = false;

            lnkSend.OnClientClick = lnkCancelOfferLetter.OnClientClick = "return false;";
            upMenus.Update();

            objOfferLetter = new clsOfferLetter();
            objTemplates = new clsTemplates();
            objXML = new clsXML();

            TextBox txtTo = (TextBox)fvOfferletter.FindControl("txtTo");
            TextBox txtBasicPay = (TextBox)fvOfferletter.FindControl("txtBasicPay");
            TextBox txtSalary = (TextBox)fvOfferletter.FindControl("txtSalary");
            TextBox txtTerms = (TextBox)fvOfferletter.FindControl("txtTerms");

            DataList dlAllowanceDeduction = (DataList)fvOfferletter.FindControl("dlAllowanceDeduction");
            DropDownList ddlTemplates = (DropDownList)fvOfferletter.FindControl("ddlTemplates");
            HiddenField hfCurrentDate = (HiddenField)fvOfferletter.FindControl("hfCurrentDate");
            HiddenField hfCandidateId = (HiddenField)fvOfferletter.FindControl("hfCandidateId");
            HiddenField hfVacancyId = (HiddenField)fvOfferletter.FindControl("hfJobId");
            //DropDownList ddlAllowancedeductions = (DropDownList)dlAllowanceDeduction.FindControl("ddlAllowancedeductions");
            DropDownList ddlPaymentMode = (DropDownList)fvOfferletter.FindControl("ddlPaymentMode");
            Button btnPreview = (Button)fvOfferletter.FindControl("btnPreview");
            Button btnSend = (Button)fvOfferletter.FindControl("btnSend");
            HiddenField hfConfirmHistory = (HiddenField)fvOfferletter.FindControl("hfConfirmHistory");
            TextBox txtDateofJoining = (TextBox)fvOfferletter.FindControl("txtDateofJoining");
            AjaxControlToolkit.HTMLEditor.Editor ajcOfferEditor = (AjaxControlToolkit.HTMLEditor.Editor)fvOfferletter.FindControl("ajcOfferEditor");
            RequiredFieldValidator rfvTemplates = (RequiredFieldValidator)fvOfferletter.FindControl("rfvTemplates");
            DropDownList ddlOfferStatus = (DropDownList)fvOfferletter.FindControl("ddlOfferStatus");
            DropDownList ddlDesignation = (DropDownList)fvOfferletter.FindControl("ddlDesignation");
            Label lbBPCurrency = (Label)fvOfferletter.FindControl("lbBPCurrency");
            Label lbAnnumCurrency = (Label)fvOfferletter.FindControl("lbAnnumCurrency");


            HtmlTableRow trStatus = (HtmlTableRow)fvOfferletter.FindControl("trStatus");

            txtDateofJoining.Attributes.Add("onkeypress", "return KeyRestrict(event, 'Number', '');");
            txtDateofJoining.Attributes.Add("onpaste", "return false;");

            objRoleSettings = new clsRoleSettings();
            objUser = new clsUserMaster();

            lnkView.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsView");


            if (objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsCreate") | objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsUpdate"))
            {
                btnSave.Enabled = btnSend.Enabled = true;

            }
            else
            {
                string Message = clsGlobalization.IsArabicCulture() ? "ليس لديك إذن ما يكفي لإرسال خطاب العرض." : "You dont have enough permission to send offerletter.";
                lnkSend.OnClientClick = "alert('" + Message + "');return false;";
                btnSave.OnClientClick = "alert('" + Message + "');return false;";
                btnSend.OnClientClick = "alert('" + Message + "');return false;";

            }

            ajcOfferEditor.Content = string.Empty;

            if (btnSend == null)
                return;

            int iCandidateId = 0, iVacancyId = 0;

            bool bHasOfferId = true;

            decimal dSalary = 0;

            if (fvOfferletter.DataKey["OfferId"] == null || fvOfferletter.DataKey["OfferId"] == DBNull.Value)
                objOfferLetter.OfferId = -1;
            else
                objOfferLetter.OfferId = Convert.ToInt32(fvOfferletter.DataKey["OfferId"]);

            if (fvOfferletter.CurrentMode == FormViewMode.Insert)
                btnPreview.Enabled = false;
            else
                btnPreview.Enabled = true;

            iCandidateId = Convert.ToInt32(Request.QueryString["CandidateId"]);
            iVacancyId = Convert.ToInt32(Request.QueryString["JobID"]);


            objTemplates.Type = clsTemplates.TemplateType.OfferLetter;

            ddlTemplates.DataSource = objTemplates.GetTemplates();
            ddlTemplates.DataBind();

            ddlTemplates.Items.Insert(0, new ListItem("Select", "-1"));


            DataSet dsDesig = objOfferLetter.GetOfferStatus();


            ddlDesignation.DataSource = dsDesig.Tables[1];
            ddlDesignation.DataBind();

            ddlDesignation.Items.Insert(0, new ListItem("Select", "-1"));

            objOfferLetter.CandidateId = iCandidateId;
            objOfferLetter.VacancyId = iVacancyId;
            if (fvOfferletter.DataKey["OfferId"] == DBNull.Value || fvOfferletter.DataKey["OfferId"] == null)
            {


                DataSet ds = objOfferLetter.GetCandidateDetails();

                DataTable dtCandidate = ds.Tables[0];
                DataTable dtVacancy = ds.Tables[1];




                txtTo.Text = Convert.ToString(dtCandidate.Rows[0]["Email"]);

                hfCandidateId.Value = Convert.ToString(iCandidateId);
                hfVacancyId.Value = Convert.ToString(iVacancyId);



                rfvTemplates.Enabled = true;
                bHasOfferId = false;

                DataTable dt = objOfferLetter.GetVacancyDetails();

                if (dt.Rows.Count > 0)
                {
                    lbAnnumCurrency.Text = lbBPCurrency.Text = dt.Rows[0]["currency"].ToString();
                    txtBasicPay.Text = string.Format("{0:#,###.00}", dt.Rows[0]["BasicPay"]);
                    txtTerms.Text = dt.Rows[0]["LegalTerms"].ToString();
                    txtDateofJoining.Text = dt.Rows[0]["dateofJoining"].ToString();
                    ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByText(dt.Rows[0]["PaymentMode"].ToString()));
                    ddlDesignation.SelectedIndex = dt.Rows[0]["DesignationId"] != null ? ddlDesignation.Items.IndexOf(ddlDesignation.Items.FindByValue(Convert.ToString(dt.Rows[0]["DesignationId"]))) : -1;
                    ddlTemplates.SelectedIndex = dt.Rows[0]["templateId"] != null ? ddlTemplates.Items.IndexOf(ddlTemplates.Items.FindByValue(Convert.ToString(dt.Rows[0]["templateId"]))) : -1;
                    objXML.AttributeId = dt.Rows[0]["OfferId"] != null ? Convert.ToInt32(dt.Rows[0]["OfferId"]) : 0;

                    ajcOfferEditor.Content = objXML.GetOfferLetterXml();

                    rfvTemplates.Enabled = false;

                }


                trStatus.Style["display"] = "none";




            }
            else
            {

                ddlOfferStatus.DataSource = dsDesig.Tables[0];
                ddlOfferStatus.DataBind();


                objOfferLetter.CandidateId = Convert.ToInt32(ViewState["CandidateId"]);
                objOfferLetter.VacancyId = Convert.ToInt32(ViewState["JobId"]);
                DataTable dt = objOfferLetter.GetVacancyDetails();

                if (dt.Rows.Count > 0)
                {
                    lbAnnumCurrency.Text = lbBPCurrency.Text = dt.Rows[0]["currency"].ToString();
                    txtBasicPay.Text = string.Format("{0:#,###.00}", dt.Rows[0]["BasicPay"]);
                    txtSalary.Text = string.Format("{0:#,###.00}", dt.Rows[0]["Salary"]);
                    txtTerms.Text = dt.Rows[0]["LegalTerms"].ToString(); ;
                    txtDateofJoining.Text = dt.Rows[0]["dateofJoining"].ToString();
                    ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByText(dt.Rows[0]["PaymentMode"].ToString()));
                    ddlTemplates.SelectedIndex = ddlTemplates.Items.IndexOf(ddlTemplates.Items.FindByValue(Convert.ToString(dt.Rows[0]["templateId"])));
                    ddlDesignation.SelectedIndex = dt.Rows[0]["DesignationId"] != null ? ddlDesignation.Items.IndexOf(ddlDesignation.Items.FindByValue(Convert.ToString(dt.Rows[0]["DesignationId"]))) : -1;

                    ViewState["Approval"] = Request.QueryString["OfferId"] != null;

                    if (Convert.ToInt32(dt.Rows[0]["OfferStatusId"]) == 1 && Convert.ToBoolean(ViewState["Approval"]))
                        trStatus.Style["display"] = "table-row";
                    else
                        trStatus.Style["display"] = "none";

                    if (Convert.ToBoolean(dt.Rows[0]["IsOfferApproval"]) == true)
                        btnSend.Visible = Convert.ToInt32(dt.Rows[0]["OfferStatusId"]) == 2 ? true : false;
                    else
                        btnSend.Visible = true;

                    if (btnSend.Visible)
                        btnSend.OnClientClick = "return true;";

                }
                if (Convert.ToInt32(dt.Rows[0]["CandidateStatusId"]) > 6)
                {

                    btnSave.Enabled = btnSend.Enabled = lnkSend.Enabled = false;
                }

                objXML.AttributeId = Convert.ToInt32(fvOfferletter.DataKey["OfferId"]);

                ajcOfferEditor.Content = objXML.GetOfferLetterXml();

                rfvTemplates.Enabled = false;
            }

            hfCurrentDate.Value = DateTime.Now.Date.ToString("dd/MM/yyyy");

            objOfferLetter.VacancyId = iVacancyId;

            DataTable dtAllowancesDeduction = objOfferLetter.BindVacancyAllowanceDeduction(bHasOfferId);

            if (dtAllowancesDeduction.Rows.Count == 0)
            {
                DataTable dt = new DataTable();

                dt.Columns.Add("AdditionDeduction");
                dt.Columns.Add("AdditionDeductionID");
                dt.Columns.Add("Amount");
                dt.Columns.Add("IsAddition");
                dt.Columns.Add("DeductionPolicy");
                dt.Columns.Add("PayPolicy");

                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);

                dlAllowanceDeduction.DataSource = dt;
                dlAllowanceDeduction.DataBind();
            }
            else
            {
                dlAllowanceDeduction.DataSource = dtAllowancesDeduction;
                dlAllowanceDeduction.DataBind();

            }

            ViewState["AllowanceDeduction"] = dtAllowancesDeduction;

            for (int i = 0; i < dtAllowancesDeduction.Rows.Count; i++)
            {
                if (dtAllowancesDeduction.Rows[i]["Amount"] != DBNull.Value)
                {
                    // ddlAllowancedeductions.SelectedValue = dtAllowancesDeduction.Rows[0]["AdditionDeductionID"].ToString();
                    if (Convert.ToInt32(dtAllowancesDeduction.Rows[i]["Addition"]) == 0)
                        dSalary = dSalary - Convert.ToDecimal(dtAllowancesDeduction.Rows[i]["Amount"]);
                    else
                        dSalary = dSalary + Convert.ToDecimal(dtAllowancesDeduction.Rows[i]["Amount"]);
                }
            }

            dSalary = (dSalary + Convert.ToDecimal(txtBasicPay.Text == "" ? "0" : txtBasicPay.Text)) * 12;
            txtSalary.Text = dSalary.ToString();


            ddlPaymentMode.DataSource = objOfferLetter.GetPaymentMode();
            ddlPaymentMode.DataBind();

            if (dlAllowanceDeduction.Items.Count > 0)
            {
                for (int i = 0; i < dlAllowanceDeduction.Items.Count; i++)
                {
                    DropDownList ddlAllowancedeductions = (DropDownList)dlAllowanceDeduction.Items[i].FindControl("ddlAllowancedeductions");


                    if (ddlAllowancedeductions != null)
                    {
                        if (dtAllowancesDeduction.Rows.Count > 0)
                        {
                            if (ddlAllowancedeductions.Items.Count > 0)
                                ddlAllowancedeductions.SelectedIndex = ddlAllowancedeductions.Items.IndexOf(ddlAllowancedeductions.Items.FindByValue(dtAllowancesDeduction.Rows[i]["AdditionDeductionID"].ToString()));
                        }
                    }
                }
            }
           
            if (dtAllowancesDeduction.Rows.Count > 0)
            {
                ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(Convert.ToString(dtAllowancesDeduction.Rows[0]["PaymentModeId"])));

            }

        }
    }

    protected void fvOfferletter_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        objOfferLetter = new clsOfferLetter();
        objMailSettings = new clsMailSettings();
        objCommon = new clsCommon();
        objXML = new clsXML();

        DataTable dt, dtOld;
        DataRow dr;

        Label lblCandidateName = (Label)fvOfferletter.FindControl("lblCandidateName");
        Label lblJobTitle = (Label)fvOfferletter.FindControl("lblJobTitle");
        Label lblCompany = (Label)fvOfferletter.FindControl("lblCompany");
        TextBox txtTo = (TextBox)fvOfferletter.FindControl("txtTo");
        TextBox txtBasicPay = (TextBox)fvOfferletter.FindControl("txtBasicPay");
        TextBox txtSalary = (TextBox)fvOfferletter.FindControl("txtSalary");
        DataList dlAllowanceDeduction = (DataList)fvOfferletter.FindControl("dlAllowanceDeduction");
        HiddenField hfCurrentDate = (HiddenField)fvOfferletter.FindControl("hfCurrentDate");
        TextBox txtDateofJoining = (TextBox)fvOfferletter.FindControl("txtDateofJoining");
        DropDownList ddlPaymentMode = (DropDownList)fvOfferletter.FindControl("ddlPaymentMode");
        DropDownList ddlDesignation = (DropDownList)fvOfferletter.FindControl("ddlDesignation");
        HiddenField hfCandidateId = (HiddenField)fvOfferletter.FindControl("hfCandidateId");
        HiddenField hfVacancyId = (HiddenField)fvOfferletter.FindControl("hfJobId");
        HiddenField hfConfirmHistory = (HiddenField)fvOfferletter.FindControl("hfConfirmHistory");
        AjaxControlToolkit.HTMLEditor.Editor ajcOfferEditor = (AjaxControlToolkit.HTMLEditor.Editor)fvOfferletter.FindControl("ajcOfferEditor");
        TextBox txtSubject = (TextBox)fvOfferletter.FindControl("txtSubject");
        TextBox txtCcto = (TextBox)fvOfferletter.FindControl("txtCcto");
        TextBox txtTerms = (TextBox)fvOfferletter.FindControl("txtTerms");
        AjaxControlToolkit.AsyncFileUpload fuAttachment = (AjaxControlToolkit.AsyncFileUpload)fvOfferletter.FindControl("fuAttachment");

        int iOfferdetails = 0;
        string sAllowances = string.Empty;
        string sDeductions = string.Empty;
        int iOfferId = 0;

        DropDownList ddlAllowancedeductions;

        TextBox txtAmt;
        RadioButtonList rbtnAllowanceDeduction;
        StringBuilder sb;
        HiddenField hfDeductionPolicy;
        HiddenField hfPayPolicy, hfAdditionDeductionID;


        bool bContent = true;

        int iCandidateID = Convert.ToInt32(hfCandidateId.Value);
        int iVacancyID = Convert.ToInt32(hfVacancyId.Value);
        string Message = string.Empty;

        switch (e.CommandName)
        {
            case "_Send":

                // Insert offerletter details 
                objOfferLetter.Begin();
                sb = new StringBuilder();

                try
                {
                    if (ajcOfferEditor.Content != string.Empty)
                    {
                        DateTime dtDateOfJoin = clsCommon.Convert2DateTime(txtDateofJoining.Text);

                        if (txtDateofJoining.Text.Trim() != string.Empty)
                        {
                            if (dtDateOfJoin == new DateTime(1900, 1, 1))
                            {
                                this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "غير صالح 'التسجيل المتوقعة من انضمام'" : "Invalid 'Expected Date of join'");
                                this.mpeMessage.Show();
                                this.upPopuppnl.Update();
                                return;
                            }
                            else if (dtDateOfJoin <= DateTime.Now.Date)
                            {
                                this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "يجب أن يكون التسجيل من المتوقع انضمام تاريخ المستقبل!" : "'Expected Date of join' must be a future date!");
                                this.mpeMessage.Show();
                                this.upPopuppnl.Update();
                                return;
                            }
                        }
                        else
                        {
                            this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "من فضلك ادخل التاريخ المتوقع للانضمام" : "Please enter Expected Date of join");
                            this.mpeMessage.Show();
                            this.upPopuppnl.Update();
                            return;
                        }

                        foreach (DataListItem item in dlAllowanceDeduction.Items)
                        {
                            ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                            txtAmt = (TextBox)item.FindControl("txtAmt");
                            rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                            hfAdditionDeductionID = (HiddenField)item.FindControl("hfAdditionDeductionID");


                            if (Convert.ToInt32(ddlAllowancedeductions.SelectedValue) != -1)
                            {
                                if (txtAmt.Text == string.Empty)
                                {
                                    Message = clsGlobalization.IsArabicCulture() ? "من فضلك ادخل المبلغ" : "Please enter amount for " + ddlAllowancedeductions.SelectedItem.Text + "";
                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('" + Message + "')", true);

                                    return;
                                }
                            }

                        }

                        objOfferLetter.CandidateId = Convert.ToInt32(hfCandidateId.Value);
                        objOfferLetter.GrossSalary = txtSalary.Text.ToDecimal();
                        objOfferLetter.VacancyId = Convert.ToInt32(hfVacancyId.Value);
                        objOfferLetter.BasicPay = txtBasicPay.Text.ToDecimal();
                        objOfferLetter.PaymentModeId = Convert.ToInt32(ddlPaymentMode.SelectedValue);
                        objOfferLetter.SentDate = clsCommon.Convert2DateTime(hfCurrentDate.Value).ToString("dd MMM yyyy");
                        objOfferLetter.DateofJoining = clsCommon.Convert2DateTime(txtDateofJoining.Text).ToString("dd MMM yyyy");
                        objOfferLetter.OfferId = (fvOfferletter.DataKey["OfferId"] == DBNull.Value ? -1 : Convert.ToInt32(fvOfferletter.DataKey["OfferId"]));
                        objOfferLetter.Subject = txtSubject.Text;
                        objOfferLetter.HasSend = false;
                        objOfferLetter.Terms = txtTerms.Text;
                        objOfferLetter.TemplateId = Convert.ToInt32(ddlTemplates.SelectedValue);
                        objOfferLetter.CreatedBy = new clsUserMaster().GetUserId();
                        objOfferLetter.Designation = Convert.ToInt32(ddlDesignation.SelectedValue);

                        iOfferId = objOfferLetter.InsertCandidateOffer(hfConfirmHistory.Value == string.Empty ? -1 : (hfConfirmHistory.Value == "1" ? 1 : 0));
                        //new clsCommonMessage().SaveOfferMessage(iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest,eAction.Applied , false);
                        foreach (DataListItem item in dlAllowanceDeduction.Items)
                        {
                            ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                            txtAmt = (TextBox)item.FindControl("txtAmt");
                            rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                            hfAdditionDeductionID = (HiddenField)item.FindControl("hfAdditionDeductionID");



                            if (Convert.ToInt32(ddlAllowancedeductions.SelectedValue) != -1)
                            {
                                objOfferLetter.OfferId = iOfferId;
                                objOfferLetter.AddDedID = ddlAllowancedeductions.SelectedValue.ToInt32();
                                objOfferLetter.AllowanceAmount = (txtAmt.Text == string.Empty ? 0 : Convert.ToDecimal(txtAmt.Text));
                                objOfferLetter.Addition = (Convert.ToInt32(rbtnAllowanceDeduction.SelectedValue) == 1 ? true : false);

                                objOfferLetter.InsertOfferletterDetails();



                                if (Convert.ToInt32(rbtnAllowanceDeduction.SelectedValue) == 1)
                                    sAllowances = (sAllowances == string.Empty ? Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text : sAllowances + "," + (Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text));
                                else
                                    sDeductions = (sDeductions == string.Empty ? Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text : sDeductions + "," + (Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text));
                            }

                        }
                        objOfferLetter.Commit();

                    }
                    else
                    {
                        bContent = false;
                    }
                }
                catch (Exception ex)
                {
                    objOfferLetter.RollBack();

                    throw ex;
                }

                if (bContent)
                {

                }
                else
                {
                    Message = clsGlobalization.IsArabicCulture() ? "عرض محتوى إلكتروني فارغة" : "Offer letter content is empty";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + Message + "')", true);
                }

                //Send offer letter to the candidate
                if (ajcOfferEditor.Content != string.Empty)
                {

                    if (hfConfirmHistory.Value == "1" || hfConfirmHistory.Value == string.Empty)
                    {
                        objXML.AttributeId = iOfferId;
                        objXML.Message = ajcOfferEditor.Content;

                        objXML.AddOfferLetterXml();
                    }
                    else
                    {
                        objXML.AttributeId = iOfferId;
                        objXML.Message = ajcOfferEditor.Content;


                        objXML.UpdateOfferLetterXml();
                    }


                    string sFrom = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.FromMail);
                    if (sFrom != string.Empty)
                    {

                        if (objMailSettings.SendMail(txtTo.Text, txtSubject.Text, GetOfferletterContent().Body, clsMailSettings.AccountType.Email, true, txtCcto.Text))
                        {
                            objOfferLetter.OfferId = iOfferId;
                            objOfferLetter.SuccessSend();

                            msgs.InformationalMessage(clsGlobalization.IsArabicCulture() ? "تم ارسال خطاب العرض." : "Offer letter has been sent.");
                            this.BindDatalist();
                        }
                        else
                        {
                            objOfferLetter.CandidateId = Convert.ToInt32(hfCandidateId.Value);
                            objOfferLetter.CandidateStatusId = Convert.ToInt32(CandidateStatus.OfferSent);
                            objOfferLetter.UpdateCandidateStatus();

                            msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "تعذر إرسال خطاب العرض." : "Offer letter could not be sent.");
                            BindFormview(iCandidateID, iVacancyID, iOfferId);
                        }

                    }
                    else
                        msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "من لم يتم تعيين عنصر البريد 'التكوين وبالتالي لا يمكن إرسال البريد" : "'From mail' config item is not set.Hence cannot send mail");

                    mpeMessage.Show();
                }

                break;

            case "_Preview": //Preview offer letter

                literalContent.Text = objCommon.GetHeader() + GetOfferletterContent().Body;

                mpePreviewOffer.Show();

                break;

            case "_AddAllowance":

                dtOld = new DataTable();

                dtOld.Columns.Add("AdditionDeduction");
                dtOld.Columns.Add("AdditionDeductionID");
                dtOld.Columns.Add("Amount");
                dtOld.Columns.Add("IsAddition");
                dtOld.Columns.Add("DeductionPolicy");
                dtOld.Columns.Add("PayPolicy");

                foreach (DataListItem item in dlAllowanceDeduction.Items)
                {

                    ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                    txtAmt = (TextBox)item.FindControl("txtAmt");
                    rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                    hfDeductionPolicy = (HiddenField)item.FindControl("hfDeductionPolicy");
                    hfPayPolicy = (HiddenField)item.FindControl("hfPayPolicy");
                    HiddenField hfAddition = (HiddenField)item.FindControl("hfAddition");
                    hfAdditionDeductionID = (HiddenField)item.FindControl("hfAdditionDeductionID");

                    dr = dtOld.NewRow();


                    dr["Amount"] = txtAmt.Text;
                    dr["AdditionDeduction"] = ddlAllowancedeductions.SelectedItem.Text;
                    dr["AdditionDeductionID"] = ddlAllowancedeductions.SelectedItem.Value;
                    dr["IsAddition"] = (ddlAllowancedeductions.SelectedValue == "-1" ? string.Empty : (Convert.ToString(rbtnAllowanceDeduction.SelectedValue == "1" ? true : false)));

                    dr["DeductionPolicy"] = "";
                    dr["PayPolicy"] = Convert.ToString(hfPayPolicy.Value);

                    dtOld.Rows.Add(dr);
                }

                dt = dtOld;

                if (dt == null)
                {
                    dt = new DataTable();

                    dt.Columns.Add("AdditionDeduction");
                    dt.Columns.Add("AdditionDeductionID");
                    dt.Columns.Add("Amount");

                    dt.Columns.Add("IsAddition");
                    dt.Columns.Add("DeductionPolicy");
                    dt.Columns.Add("PayPolicy");
                }

                dr = dt.NewRow();
                dt.Rows.Add(dr);

                dlAllowanceDeduction.DataSource = dt;
                dlAllowanceDeduction.DataBind();

                ViewState["AllowanceDeduction"] = dt;

                break;

            case "_Save":

                bContent = true;
                // Insert offerletter details 
               objOfferLetter.Begin();

                try
                {
                    if (ajcOfferEditor.Content != string.Empty)
                    {
                        objOfferLetter.CandidateId = Convert.ToInt32(hfCandidateId.Value);
                        objOfferLetter.VacancyId = Convert.ToInt32(hfVacancyId.Value);
                        objOfferLetter.BasicPay = txtBasicPay.Text.ToDecimal();
                        objOfferLetter.PaymentModeId = Convert.ToInt32(ddlPaymentMode.SelectedValue);
                        objOfferLetter.SentDate = clsCommon.Convert2DateTime(hfCurrentDate.Value).ToString("dd MMM yyyy");

                        DateTime dtDateOfJoin = clsCommon.Convert2DateTime(txtDateofJoining.Text);
                        foreach (DataListItem item in dlAllowanceDeduction.Items)
                        {
                            ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                            txtAmt = (TextBox)item.FindControl("txtAmt");
                            rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                            hfAdditionDeductionID = (HiddenField)item.FindControl("hfAdditionDeductionID");

                            if (Convert.ToInt32(ddlAllowancedeductions.SelectedValue) != -1)
                            {


                                if (txtAmt.Text == string.Empty)
                                {
                                    Message = clsGlobalization.IsArabicCulture() ? "من فضلك ادخل المبلغ" : "Please enter amount for " + ddlAllowancedeductions.SelectedItem.Text + "";
                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert(' " + Message + "')", true);
                                    return;
                                }
                            }
                        }
                        if (txtDateofJoining.Text.Trim() != string.Empty)
                        {
                            if (dtDateOfJoin == new DateTime(1900, 1, 1))
                            {
                                this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "غير صالح 'التسجيل المتوقعة من انضمام'" : "Invalid 'Expected Date of join'");
                                this.mpeMessage.Show();
                                this.upPopuppnl.Update();
                                return;
                            }
                            else if (dtDateOfJoin <= DateTime.Now.Date)
                            {
                                this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? " يجب أن يكون التسجيل من المتوقع انضمام تاريخ المستقبل!" : "'Expected Date of join' must be a future date!");
                                this.mpeMessage.Show();
                                this.upPopuppnl.Update();
                                return;
                            }
                        }
                        else
                        {
                            this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "من فضلك ادخل التاريخ المتوقع للانضمام" : "Please enter Expected Date of join");
                            this.mpeMessage.Show();
                            this.upPopuppnl.Update();
                            return;
                        }
                        HtmlTableRow trRemarks = (HtmlTableRow)fvOfferletter.FindControl("trRemarks");
                        if (trRemarks != null)
                        {

                            if (trRemarks.Style["display"] == "table-row")
                            {
                                if (txtRemarks.Text == "")
                                {
                                    this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "الرجاء إدخال الملاحظات" : "Please enter remarks");
                                    this.txtRemarks.Focus();
                                    this.mpeMessage.Show();
                                    this.upPopuppnl.Update();
                                    return;
                                }
                            }
                        }



                        objOfferLetter.DateofJoining = clsCommon.Convert2DateTime(txtDateofJoining.Text).ToString("dd MMM yyyy");

                        objOfferLetter.OfferId = (fvOfferletter.DataKey["OfferId"] == DBNull.Value ? -1 : Convert.ToInt32(fvOfferletter.DataKey["OfferId"]));
                        objOfferLetter.Subject = txtSubject.Text;
                        objOfferLetter.HasSend = false;
                        objOfferLetter.GrossSalary = txtSalary.Text.ToDecimal();
                        objOfferLetter.Terms = txtTerms.Text;
                        objOfferLetter.TemplateId = Convert.ToInt32(ddlTemplates.SelectedValue);
                        objOfferLetter.CreatedBy = new clsUserMaster().GetUserId();
                        objOfferLetter.Designation = Convert.ToInt32(ddlDesignation.SelectedValue);
                        // IF history of offerletter is needed set as true else as false. 

                        iOfferId = objOfferLetter.InsertCandidateOffer(hfConfirmHistory.Value == string.Empty ? -1 : (hfConfirmHistory.Value == "1" ? 1 : 0));
                        objOfferLetter.Commit();

                        DataTable dtApproval = objOfferLetter.GetJobApprovalLevels();

                        if (dtApproval.Rows.Count > 0)
                        {
                            if (fvOfferletter.DataKey["OfferId"] == null)
                            {
                                if (ddlOfferStatus.SelectedValue == "2")
                                    new clsCommonMessage().SaveOfferMessage(Convert.ToInt32(dtApproval.Rows[0]["EmployeeId"]), iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Applied, false);
                                else if (ddlOfferStatus.SelectedValue == "3")
                                {
                                    objOfferLetter.OfferId = iOfferId;
                                    objOfferLetter.OfferStatus = Convert.ToInt32(ddlOfferStatus.SelectedValue);
                                    objOfferLetter.CreatedBy = new clsUserMaster().GetEmployeeId();
                                    objOfferLetter.Remarks = txtRemarks.Text;
                                    objOfferLetter.UpdateOfferStatus();
                                    new clsCommonMessage().RejectOfferMessage(dtApproval, iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Reject);
                                }
                                else
                                {
                                    new clsCommonMessage().SaveOfferMessage(Convert.ToInt32(dtApproval.Rows[0]["EmployeeId"]), iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Applied, true);
                                }
                            }
                            else if (Convert.ToBoolean(ViewState["Approval"]) == true)
                            {
                                int iEmployeeId = 0;
                                bool isHigherAuthority = false;

                                for (int i = 0; i < dtApproval.Rows.Count; i++)
                                {
                                    if (Convert.ToInt32(dtApproval.Rows[i]["Employeeid"]) == new clsUserMaster().GetEmployeeId() && Convert.ToInt32(dtApproval.Rows[i]["level"]) != 1)
                                    {
                                        iEmployeeId = dtApproval.Rows[i + 1]["Employeeid"] != null ? Convert.ToInt32(dtApproval.Rows[i + 1]["Employeeid"]) : new clsUserMaster().GetEmployeeId();
                                        break;

                                    }
                                    else if (Convert.ToInt32(dtApproval.Rows[i]["level"]) == 1)
                                    {
                                        isHigherAuthority = true;
                                        iEmployeeId = Convert.ToInt32(dtApproval.Rows[i]["Employeeid"]);

                                    }
                                }
                                if (isHigherAuthority)
                                {
                                    objOfferLetter.OfferId = iOfferId;
                                    objOfferLetter.OfferStatus = Convert.ToInt32(ddlOfferStatus.SelectedValue);
                                    objOfferLetter.CreatedBy = new clsUserMaster().GetEmployeeId();
                                    objOfferLetter.Remarks = txtRemarks.Text;
                                    objOfferLetter.UpdateOfferStatus();
                                    if (ddlOfferStatus.SelectedValue == "2")
                                    clsCommonMessage.DeleteMessages(iOfferId, eReferenceTypes.OfferLetterApproval,new clsUserMaster().GetEmployeeId());
                                    if (ddlOfferStatus.SelectedValue == "3")
                                        new clsCommonMessage().RejectOfferMessage(dtApproval, iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Reject);
                                }

                                else
                                {
                                    objOfferLetter.OfferId = iOfferId;
                                    objOfferLetter.OfferStatus = Convert.ToInt32(ddlOfferStatus.SelectedValue);
                                    objOfferLetter.CreatedBy = new clsUserMaster().GetEmployeeId();
                                    objOfferLetter.Remarks = txtRemarks.Text;


                                    if (ddlOfferStatus.SelectedValue == "2")
                                    {
                                        new clsCommonMessage().SaveOfferMessage(iEmployeeId, iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Applied, false);
                                        objOfferLetter.OfferStatus = 1;
                                    }
                                    else
                                        new clsCommonMessage().RejectOfferMessage(dtApproval, iOfferId, eReferenceTypes.OfferLetterApproval, eMessageTypes.OfferRequest, eAction.Reject);
                                    objOfferLetter.UpdateOfferStatus();
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                objOfferLetter.OfferId = iOfferId;
                                objOfferLetter.OfferStatus = 2;
                                objOfferLetter.CreatedBy = new clsUserMaster().GetEmployeeId();
                                objOfferLetter.Remarks = txtRemarks.Text;
                                objOfferLetter.UpdateOfferStatus();
                            }
                            catch (Exception)
                            {

                            }


                        }



                        foreach (DataListItem item in dlAllowanceDeduction.Items)
                        {
                            ddlAllowancedeductions = (DropDownList)item.FindControl("ddlAllowancedeductions");
                            txtAmt = (TextBox)item.FindControl("txtAmt");
                            rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
                            hfAdditionDeductionID = (HiddenField)item.FindControl("hfAdditionDeductionID");

                            if (Convert.ToInt32(ddlAllowancedeductions.SelectedValue) != -1)
                            {
                                objOfferLetter.OfferId = iOfferId;
                                objOfferLetter.AddDedID = ddlAllowancedeductions.SelectedValue.ToInt32();
                                objOfferLetter.AllowanceAmount = Convert.ToDecimal(txtAmt.Text);
                                objOfferLetter.Addition = (Convert.ToInt32(rbtnAllowanceDeduction.SelectedValue) == 1 ? true : false);

                                objOfferLetter.InsertOfferletterDetails();

                                if (Convert.ToInt32(rbtnAllowanceDeduction.SelectedValue) == 1)
                                    sAllowances = (sAllowances == string.Empty ? Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text : sAllowances + "," + (Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text));
                                else
                                    sDeductions = (sDeductions == string.Empty ? Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text : sDeductions + "," + (Convert.ToString(ddlAllowancedeductions.SelectedItem.Text) + "-" + txtAmt.Text));

                            }

                        }
                    

                        this.msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "تم حفظ العرض إلكتروني بنجاح." : "Offer letter has been saved sucessfully.");

                        this.mpeMessage.Show();
                        this.upPopuppnl.Update();


                    }
                    else
                    {
                        bContent = false;
                    }
                }
                catch (Exception ex)
                {
                   objOfferLetter.RollBack();
                }

                if (bContent)
                {


                    //Save offer letter as xml file

                    if (hfConfirmHistory.Value == "1" || hfConfirmHistory.Value == string.Empty)
                    {
                        objXML.AttributeId = iOfferId;
                        objXML.Message = ajcOfferEditor.Content;

                        objXML.AddOfferLetterXml();
                    }
                    else
                    {
                        objXML.AttributeId = iOfferId;
                        objXML.Message = ajcOfferEditor.Content;

                        objXML.UpdateOfferLetterXml();
                    }

                    dt = (DataTable)ViewState["Files"];

                    sb = new StringBuilder();

                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dw in dt.Rows)
                                sb.Append("," + dw["FilePath"].ToString());

                            sb.Remove(0, 1);
                        }
                    }

                    BindFormview(iCandidateID, iVacancyID, iOfferId);
                }
                else
                {
                    Message = clsGlobalization.IsArabicCulture() ? "عرض محتوى إلكتروني فارغة" : "Offer letter content is empty";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + Message + "')", true);
                }
                break;
        }
    }



    protected void ddlAllowancedeductions_SelectedIndexChanged(object sender, EventArgs e)
    {
        objOfferLetter = new clsOfferLetter();

        DropDownList ddlAllowancedeductions = (DropDownList)sender;

        DataListItem item = (DataListItem)ddlAllowancedeductions.Parent;

        HiddenField hfAddition = (HiddenField)item.FindControl("hfAddition");
        HiddenField hfDeductionPolicy = (HiddenField)item.FindControl("hfDeductionPolicy");
        RadioButtonList rbtnAllowanceDeduction = (RadioButtonList)item.FindControl("rbtnAllowanceDeduction");
        TextBox txtAmt = (TextBox)item.FindControl("txtAmt");

        DataSet ds = new DataSet();

        try
        {
            if (Convert.ToInt32(ddlAllowancedeductions.SelectedValue) != -1)
            {
                objOfferLetter.AllowanceTypeId = Convert.ToInt32(ddlAllowancedeductions.SelectedValue);

                ds = objOfferLetter.IsAllowanceorDeduction();

                hfAddition.Value = Convert.ToString(ds.Tables[0].Rows[0][0]);

                rbtnAllowanceDeduction.Enabled = false;

                rbtnAllowanceDeduction.SelectedIndex = (hfAddition.Value == "False" ? 1 : 0);


            }
            else
            {
                rbtnAllowanceDeduction.Enabled = true;

            }
        }
        finally
        {
            if (ds != null) ds.Dispose();
        }
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Calc", "calSalaryOffered('" + ddlAllowancedeductions.ClientID + "', 0)", true);
        //calSalaryOffered(this.id, 0);
    }

    protected void dlSentOffers_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objOfferLetter = new clsOfferLetter();
        objCommon = new clsCommon();

        HiddenField hfVacancyId = (HiddenField)e.Item.FindControl("hfJobId");

        switch (e.CommandName)
        {
            case "_EditCandidateOffer":

                fvOfferletter.ChangeMode(FormViewMode.Edit);

                BindFormview(Convert.ToInt32(e.CommandArgument), Convert.ToInt32(hfVacancyId.Value), Convert.ToInt32(dlSentOffers.DataKeys[e.Item.ItemIndex]));

                break;

            case "_ShowHistory":

                LinkButton ancHistory = (LinkButton)e.CommandSource;

                objOfferLetter.CandidateId = Convert.ToInt32(e.CommandArgument);
                objOfferLetter.VacancyId = Convert.ToInt32(hfVacancyId.Value);

                DataList dlHistoryOffers = (DataList)e.Item.FindControl("dlHistoryOffers");

                DataTable dt = objOfferLetter.ShowHistory();

                dlHistoryOffers.DataSource = dt;
                dlHistoryOffers.DataBind();

                HtmlContainerControl divHistory = (HtmlContainerControl)e.Item.FindControl("divHistory");

                ScriptManager.RegisterClientScriptBlock(ancHistory, ancHistory.GetType(), "ShowOfferHistory", "SlideUpSlideDown('" + divHistory.ClientID + "', 500)", true);

                break;

            case "_ViewOffer":
                literalContent.Text = objCommon.GetHeader() + OfferLetterPreview(Convert.ToInt32(dlSentOffers.DataKeys[e.Item.ItemIndex]));

                mpePreviewOffer.Show();
                break;
            case "_Print":

                SetPrintOffer(e.Item);

                break;


        }
    }

    protected void dlHistoryOffers_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objOfferLetter = new clsOfferLetter();

        DataList dlHistoryOffers = (DataList)sender;

        if (e.Item.ItemIndex != -1)
        {
            DataList dlAllowancedeductions = (DataList)e.Item.FindControl("dlAllowancedeductions");

            objOfferLetter.OfferId = Convert.ToInt32(dlHistoryOffers.DataKeys[e.Item.ItemIndex]);

            dlAllowancedeductions.DataSource = objOfferLetter.ShowHistoryAllowanceDeductions();
            dlAllowancedeductions.DataBind();
        }
    }



    protected void dlSentOffers_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objCommon = new clsCommon();
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        HiddenField hfHistoryCount = (HiddenField)e.Item.FindControl("hfHistoryCount");
        ImageButton imgPrint = (ImageButton)e.Item.FindControl("imgPrint");
        LinkButton lnkViewCandidateOffer = (LinkButton)e.Item.FindControl("lnkViewCandidateOffer");
        HiddenField hfStatus = (HiddenField)e.Item.FindControl("hfStatus");
        HtmlContainerControl divHistory = (HtmlContainerControl)e.Item.FindControl("divHistory");
        CheckBox chkOffers = (CheckBox)e.Item.FindControl("chkOffers");

        if (hfStatus.Value.Trim() != "Waiting for Approval" && hfStatus.Value.Trim() != "في انتظار الموافقة" && hfStatus.Value.Trim() != "Approved" && hfStatus.Value.Trim() != "وافق" && hfStatus.Value.Trim() != "Sent" && hfStatus.Value.Trim() != "Sent" && hfStatus.Value.Trim() != "Not Sent" && hfStatus.Value.Trim() != "لا المرسلة" && hfStatus.Value.Trim() != "Offer Sent" && hfStatus.Value.Trim() != "نقدم المرسلة")
        {
            lnkViewCandidateOffer.OnClientClick = "return false;";
            lnkViewCandidateOffer.Attributes.Add("style", "text-decoration:none;cursor:default;");
            chkOffers.Enabled = false;
        }
        else
        {
            lnkSend.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsCreate");
            lnkCancelOfferLetter.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsDelete"); ;
            lnkViewCandidateOffer.OnClientClick = "return true;";
            chkOffers.Enabled = true;
        }

        imgPrint.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUser.GetRoleId(), (int)eMenuID.OfferLetter, "IsPrintEmail");


        HtmlContainerControl divAttachment = (HtmlContainerControl)e.Item.FindControl("divAttachment");
        if (divAttachment == null)
            return;
        Label lblAttachment = (Label)e.Item.FindControl("lblAttachment");

        HtmlImage imgFile = (HtmlImage)e.Item.FindControl("imgFile");
        HtmlAnchor ancDownload = (HtmlAnchor)e.Item.FindControl("ancDownload");


    }




    #endregion
    protected void lnkView_Click1(object sender, EventArgs e)
    {
        BindDatalist();
    }

    protected void lnkSend_Click1(object sender, EventArgs e)
    {
        objXML = new clsXML();
        objMailSettings = new clsMailSettings();
        objOfferLetter = new clsOfferLetter();

        string sContent = string.Empty;
        string sAllowances = string.Empty;
        string sDeductions = string.Empty;

        foreach (DataListItem item in dlSentOffers.Items)
        {
            CheckBox chkOffers = (CheckBox)item.FindControl("chkOffers");

            if (chkOffers.Checked)
            {
                objXML.AttributeId = Convert.ToInt32(dlSentOffers.DataKeys[item.ItemIndex]);

                sContent = objXML.GetOfferLetterXml();

                objOfferLetter.OfferId = Convert.ToInt32(dlSentOffers.DataKeys[item.ItemIndex]);

                DataSet ds = objOfferLetter.GetSavedOfferLetter();

                DataTable dtOfferletter = ds.Tables[0];
                DataTable dtOfferDetails = ds.Tables[1];
                DataTable dtJobDetails = ds.Tables[2];


                //Preview offer letter of the candidate

                if (dtOfferletter.Rows.Count > 0)
                {
                    if (dtOfferletter.Rows[0]["OfferStatusid"].ToInt32() == 2)
                    {
                        MailDefinition mdSendOffer = new MailDefinition();
                        ListDictionary ldReplacements = new ListDictionary();
                        MailMessage mmContent;

                        ldReplacements.Add("<%Candidate Name%>", Convert.ToString(dtOfferletter.Rows[0]["Candidate"]));
                        ldReplacements.Add("<%Company Name%>", Convert.ToString(dtOfferletter.Rows[0]["CompanyName"]));
                        ldReplacements.Add("<%Job Title%>", Convert.ToString(dtOfferletter.Rows[0]["Vacancy"]));
                        ldReplacements.Add("<%Gross Salary%>", Convert.ToString(dtOfferletter.Rows[0]["PerAnnum"]));
                        ldReplacements.Add("<%Basic Pay%>", Convert.ToString(dtOfferletter.Rows[0]["BasicPay"]));
                        ldReplacements.Add("<%Payment Mode%>", Convert.ToString(dtOfferletter.Rows[0]["PaymentMode"]));
                        ldReplacements.Add("<%Sent Date%>", Convert.ToString(dtOfferletter.Rows[0]["SentDate"]));
                        ldReplacements.Add("<%Expected Join Date%>", Convert.ToString(dtOfferletter.Rows[0]["DateofJoining"]));
                        ldReplacements.Add("<%Place of Work%>", dtJobDetails.Rows[0]["Placeofwork"].ToString());
                        ldReplacements.Add("<%Daily Working hours%>", dtJobDetails.Rows[0]["DailyWorkingHour"].ToString());
                        ldReplacements.Add("<%Weekly working days%>", dtJobDetails.Rows[0]["Weeklyworkingdays"].ToString());
                        ldReplacements.Add("<%Contract Period%>", dtJobDetails.Rows[0]["ContractPeriod"].ToString());
                        ldReplacements.Add("<%Accomodation Available%>", dtJobDetails.Rows[0]["IsAccomodation"].ToString());
                        ldReplacements.Add("<%Transportation Available%>", dtJobDetails.Rows[0]["Istransportation"].ToString());
                        ldReplacements.Add("<%Air Ticket%>", dtJobDetails.Rows[0]["AirTicket"].ToString());
                        ldReplacements.Add("<%Annual Leave days%>", dtJobDetails.Rows[0]["AnnualLeaveDays"].ToString());
                        ldReplacements.Add("<%Legal Terms%>", dtOfferletter.Rows[0]["Terms"].ToString());
                        ldReplacements.Add("<%Department%>", dtJobDetails.Rows[0]["Department"].ToString());
                        ldReplacements.Add("<%Designation%>", dtOfferletter.Rows[0]["Designation"].ToString());
                        ldReplacements.Add("<%EmploymentType%>", dtJobDetails.Rows[0]["EmploymentType"].ToString());


                        mdSendOffer.From = "info@HrPower.com";
                        mdSendOffer.Priority = MailPriority.High;

                        string sMessage = sContent.Replace("{$$", "<%");
                        sMessage = sMessage.Replace("$$}", "%>");

                        mmContent = mdSendOffer.CreateMailMessage(Convert.ToString(dtOfferletter.Rows[0]["Email"]), ldReplacements, sMessage, this);

                        if (objMailSettings.SendMail(Convert.ToString(dtOfferletter.Rows[0]["Email"]), Convert.ToString(dtOfferletter.Rows[0]["Subject"]), mmContent.Body, clsMailSettings.AccountType.Email, true))
                        {
                            objOfferLetter.OfferId = Convert.ToInt32(dlSentOffers.DataKeys[item.ItemIndex]);
                            objOfferLetter.SuccessSend();

                            msgs.InformationalMessage(clsGlobalization.IsArabicCulture() ? "تم ارسال خطاب العرض." : "Offer letter has been sent.");
                        }
                        else
                        {
                            objOfferLetter.CandidateId = Convert.ToInt32(dtOfferletter.Rows[0]["CandidateId"]);
                            objOfferLetter.CandidateStatusId = Convert.ToInt32(CandidateStatus.OfferSent);
                            objOfferLetter.UpdateCandidateStatus();
                            msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "تعذر إرسال خطاب العرض." : "Offer letter could not be sent.");
                        }

                    }
                    else
                    {
                        msgs.WarningMessage(clsGlobalization.IsArabicCulture() ? "تعذر إرسال خطاب العرض قبل الموافقة عليها. " : " Offer letter could not be sent before approval.");
                    }
                    mpeMessage.Show();
                }
            }
        }
    }
    protected void lnkCancelOfferLetter_Click(object sender, EventArgs e)
    {
        System.Collections.Generic.List<int> OfferLetterIds = new System.Collections.Generic.List<int>();
        if (this.objOfferLetter == null)
            this.objOfferLetter = new clsOfferLetter();

        foreach (DataListItem item in dlSentOffers.Items)
        {
            CheckBox chkOffers = (CheckBox)item.FindControl("chkOffers");

            if (chkOffers.Checked)
            {
                OfferLetterIds.Add(dlSentOffers.DataKeys[item.ItemIndex].ToInt32());
                objOfferLetter.OfferId = dlSentOffers.DataKeys[item.ItemIndex].ToInt32();
                objOfferLetter.CandidateStatusId = Convert.ToInt32(CandidateStatus.Selected);
                objOfferLetter.UpdateCandidateStatus();
            }

        }

        if (OfferLetterIds.Count > 0)
        {


            bool success = this.objOfferLetter.CancelOfferLetter(OfferLetterIds);

            if (success)
            {
                this.BindDatalist();
                msgs.InformationalMessage(clsGlobalization.IsArabicCulture() ? "تم إلغاء العرض المحدد حرف (ق)" : "Selected offer-letter(s) has been cancelled");
            }
            else
                msgs.InformationalMessage(clsGlobalization.IsArabicCulture() ? "فشل إلغاء العرض حرف!" : "Offer-letter cancellation failed!");

            mpeMessage.Show();
        }
    }
    protected void btnGo_Click(object sender, ImageClickEventArgs e)
    {
        BindDatalist();
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.OfferLetter , objUser.GetCompanyId());
    }

}
