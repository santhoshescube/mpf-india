﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"


    CodeFile="Templates.aspx.cs" Inherits="Public_Templates" Title="Templates" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
       <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>           
            <li><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li class="selected"><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
           
        </ul>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div style="width: 100%; float: left;">
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="pnlMsg" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="msgConfirm" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:FormView ID="fvTemplates" runat="server" Width="100%" DefaultMode="Insert" BorderWidth="0px"
                    CellPadding="0" OnDataBound="fvTemplates_DataBound" OnItemCommand="fvTemplates_ItemCommand"
                    DataKeyNames="TemplateId,TemplateTypeId,IsCurrent" CellSpacing="0">
                    <EditItemTemplate>
                        <div style="width: 100%; float: left;">
                            <div style="width: 100%; float: left;">
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Template Type
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <asp:DropDownList ID="ddlTemplateType" runat="server" DataTextField="TemplateType"
                                        DataValueField="TemplateTypeId" CssClass="dropdownlist" AutoPostBack="true" OnSelectedIndexChanged="ddlTemplateType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvTemplateType" runat="server" ControlToValidate="ddlTemplateType"
                                        ValidationGroup="ValSubmit" InitialValue="-1" ErrorMessage="Please select any template type"
                                        CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;">
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Template Title
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <asp:TextBox ID="txtTemplateTitle" Text='<%#Eval("Title") %>' runat="server" CssClass="textbox_mandatory"
                                        Width="150px" MaxLength="250"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rvTemplateTitle" ValidationGroup="ValSubmit" runat="server"
                                        ControlToValidate="txtTemplateTitle" ErrorMessage="Please enter title" CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div style="vertical-align: top; float: left; width: 100%;">
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Subject
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <asp:TextBox ID="txtSubject" Text='<%#Eval("Subject") %>' runat="server" CssClass="textbox_mandatory"
                                        Width="312px" MaxLength="250"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvSubject" ValidationGroup="ValSubmit" runat="server"
                                        ControlToValidate="txtSubject" ErrorMessage="Please enter subject" CssClass="error"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div>
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Merge Fields
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <asp:DropDownList ID="ddlFields" runat="server" CssClass="dropdownlist" Width="200px"
                                        DataTextField="FieldName" DataValueField="FieldValue">
                                    </asp:DropDownList>
                                    &nbsp;
                                    <asp:Button ID="btnAddFields" runat="server" CssClass="btnsubmit" Text="Add" /><%-- CommandName="_AddFields"--%>
                                </div>
                            </div>
                            <div style="width: 100%; vertical-align: top; float: left;">
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Message
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <%--<asp:UpdatePanel ID="upEditor" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                    <AjaxControlToolkit:Editor ID="txtBody" Content='<%#GetXmlMessage(Eval("TemplateId")) %>'
                                        runat="server" Width="100%" Height="275px" />
                                    <%--  </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="fvTemplates" EventName="ItemCommand" />
                                                </Triggers>
                                            </asp:UpdatePanel>--%>
                                   <%-- <asp:CustomValidator ID="rfvBody" ValidationGroup="ValSubmit"
                                                runat="server"  ClientValidationFunction="checkTemplateContent" ErrorMessage="Please enter any content "
                                                CssClass="error" ValidateEmptyText="true" ></asp:CustomValidator> --%>
                                </div>
                            </div>
                            <div style="vertical-align: top; float: left; width: 100%;">
                                <div class="trLeft" style="width: 20%; float: left;">
                                    Current Template
                                </div>
                                <div class="trRight" style="width: 75%; float: left;">
                                    <asp:RadioButton ID="rbtnYes" runat="server" Text="Yes" Checked="true" GroupName="Current" /><asp:RadioButton
                                        ID="rbtnNo" runat="server" Text="No" GroupName="Current" />
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%; float: right;">
                            <div style="float: right; width: 20%;" class="trleft">
                                &nbsp;
                            </div>
                            <div class="trRight" style="width: 75%; float: right; padding-left: 40%;">
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text="Submit" ToolTip="Submit"
                                    CommandName="_Submit" ValidationGroup="ValSubmit" CausesValidation="true" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text="Cancel" ToolTip="Cancel"
                                    CommandName="_Reset" CausesValidation="false" />
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
                <asp:DataList ID="dlTemplates" runat="server" Width="100%" DataKeyField="TemplateId"
                    OnItemCommand="dlTemplates_ItemCommand" BorderWidth="0px" CellPadding="0">
                    <HeaderTemplate>
                        <div style="width: 100%; padding-top: 10px; font-weight: bold;">
                            <div class="datalistheader" style="width: 100%; float: left;">
                                <div style="vertical-align: top; width: 25px; float: left;">
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onclick="selectAll(this.id,'chkTemplates')" />
                                </div>
                                <div style="width: 250px; float: left;">
                                    Title
                                </div>
                                <div style="width: 250px; float: left;">
                                    Template Type
                                </div>
                                <div style="width: auto; float: left;">
                                    Subject
                                </div>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div style="width: 100%; float: left;">
                            <div class="trLeft" style="width: 25px; float: left;">
                                <asp:CheckBox ID="chkTemplates" runat="server" />
                            </div>
                            <div class="trLeft" style="width: 250px; float: left;">
                                <asp:LinkButton ID="lnkViewTemplate" ToolTip="Edit Email Template" runat="server"
                                    Text='<%#clsCommon.Crop(Convert.ToString(Eval("Title")),50)%>' CommandArgument='<%#Eval("TemplateId")%>'
                                    CommandName="_EditTemplate"></asp:LinkButton>
                            </div>
                            <div style="width: 250px; float: left;" class="trRight">
                                <%#Eval("TemplateType")%>
                            </div>
                            <div style="float: left; width: auto;" class="trRight">
                                <%#clsCommon.Crop(Convert.ToString(Eval("Subject")), 50)%>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
                <asp:Label ID="lblNodata" runat="server" Style="display: block;" CssClass="error"
                    Visible="False"></asp:Label>
                <uc:Pager ID="pgTemplates" runat="server" PageSize="10" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">

<div id="search" style="vertical-align: top">
        <div id="text">
         
        </div>
        <asp:TextBox ID="txtSearch" runat="server" CssClass="WmCss"></asp:TextBox> &nbsp; &nbsp;
                        
        <br />
        <div id="searchimg">
            <%-- <img src="../images/search.png">--%>
            <asp:ImageButton ID="btnGo" runat="server" CausesValidation="false" ImageUrl="~/images/search.png"
                            ToolTip="Click here to search" ImageAlign="AbsMiddle" OnClick="btnGo_Click" />
            <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                WatermarkText="Search By" WatermarkCssClass="WmCss">
            </AjaxControlToolkit:TextBoxWatermarkExtender>
        </div>
    </div>
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
         <div id="sidebarsettings" style="width: 100%; float: left;">
                <div style="width: 100%; float: left;">
                    <div class="sideboxImage">
                       <img src="../images/Add templates_Big.png" />
                    </div>
                    <div id="name">
                         <asp:LinkButton ID="lnkNew" runat="server" Text="Add Email Template" ToolTip="New Email Templates"
                            CausesValidation="False" CssClass="labeltext" OnClick="lnkNew_Click"></asp:LinkButton>
                    </div>
                </div>
                <div style="width: 100%; float: left;">
                    <div class="sideboxImage">
                      <img src="../images/Template_list_Big.png" />
                    </div>
                    <div id="Div1">
                             <asp:LinkButton ID="lnkView" runat="server" Text="Template List" ToolTip="List the templates"
                            OnClick="lnkView_Click" CssClass="labeltext" CausesValidation="False"></asp:LinkButton>
                    </div>
                </div>
                
                <div style="width: 100%; float: left;">
                    <div class="sideboxImage">
                        <img src="../images/Delete.png" />
                    </div>
                    <div id="name">&nbsp;
                      <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" CausesValidation="False"
                            OnClick="lnkDelete_Click" CssClass="labeltext"></asp:LinkButton>
                    </div>
                </div>
            </div>
           
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <style type="text/css">
  select
  {
  	margin-left:0px;
  }
  	
</style>
    
    
</asp:Content>
