﻿<%@ Page Language="C#" %>

<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Web.UI.DataVisualization" %>
<%@ Import Namespace="System.Web.UI.DataVisualization.Charting" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.IO" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["IsFromHomePage"] == "False")
            {

                Chart myChart = new Chart();

                myChart.Height = Unit.Pixel(Convert.ToInt32(Request.QueryString["Height"]));
                myChart.Width = Unit.Pixel(Convert.ToInt32(Request.QueryString["Width"]));

                Series Projection = new Series();
                Projection.Name = GetGlobalResourceObject("ControlsCommon", "Projection").ToString();//Projection
                Projection.ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), Request.QueryString["Type"]);

                Series Actual = new Series();
                Actual.Name = GetGlobalResourceObject("ControlsCommon", "Actual").ToString();//Actual
                Actual.ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), Request.QueryString["Type"]);

                ChartArea myChartArea = new ChartArea();
                
                myChart.Legends.Add("myLegend");
                myChart.ChartAreas.Add(myChartArea);
                myChart.Series.Add(Projection);
                myChart.Series.Add(Actual);

                double[] yValues = new double[1];
                yValues[0] = Convert.ToDouble(Request.QueryString["yValues"]);
                
                double[] y1Values = new double[1];
                y1Values[0] = Convert.ToDouble(Request.QueryString["y1Values"]);

                bool hasValue = false;

                foreach (double val in yValues)
                {
                    if (val != 0.0d)
                        hasValue = true;
                }

                if (hasValue)
                {
                    //myChart.Series["Projection"].Points.AddY(yValues[0].ToDouble());
                    myChart.Series[GetGlobalResourceObject("ControlsCommon", "Projection").ToString()].Points.AddY(yValues[0].ToDouble());
                    
                    
                   // myChart.Series["Actual"].Points.AddY(y1Values[0].ToDouble());
                    myChart.Series[GetGlobalResourceObject("ControlsCommon", "Actual").ToString()].Points.AddY(y1Values[0].ToDouble());
                    
                    for (int iCounter = 0; iCounter < myChart.Series.Count; iCounter++)
                    {
                        myChart.Series[iCounter].MarkerStyle = MarkerStyle.Diamond;
                        myChart.Series[iCounter].MarkerSize = 8;
                        myChart.Series[iCounter].MarkerColor = myChart.Series[iCounter].BorderColor;
                        myChart.Series[iCounter].BorderDashStyle = ChartDashStyle.Solid;
                        myChart.Series[iCounter].BorderWidth = 4;
                        myChart.Series[iCounter]["PixelPointWidth"] = "80";
                        myChart.Series[iCounter].Label = "#VALY";
                        myChart.Series[iCounter].AxisLabel = "  ";
                        myChart.Series[iCounter].IsValueShownAsLabel = false;
                    }
                    myChart.Legends[0].Font = new Font(new FontFamily("Tahoma"), 10);
                    myChart.Palette = ChartColorPalette.None;
                    myChart.ChartAreas[0].AxisX.LineWidth = 0;

                    
                    myChartArea.AxisX.MajorGrid.Enabled = true;
                    myChartArea.AxisY.MajorGrid.Enabled = true;
                    myChartArea.AxisY.MajorGrid.LineColor = Color.LightGray; 
                    myChartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
                    myChartArea.AxisY.LineColor = Color.LightGray;
                    
                   if (Request.QueryString["Docking"] == null)
                        myChart.Legends[0].Docking = Docking.Bottom;
                    else
                        myChart.Legends[0].Docking = (Docking)Enum.Parse(typeof(Docking), Request.QueryString["Docking"]);
                }
                else
                {
                    myChart.Legends.RemoveAt(0);
                }

                MemoryStream ms = new MemoryStream();

                myChart.SaveImage(ms, ChartImageFormat.Jpeg);

                Response.ContentType = "image/jpeg";
                Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);

                ms.Dispose();

                Response.End();
            }
            else
            {
                Chart myChart = new Chart() { IsMapEnabled = true };
                myChart.RenderType = RenderType.ImageMap;


                myChart.Height = Unit.Pixel(Convert.ToInt32(Request.QueryString["Height"]));
                myChart.Width = Unit.Pixel(Convert.ToInt32(Request.QueryString["Width"]));

                Series Projection = new Series();
               // Projection.Name = "Projection";
                Projection.Name = GetGlobalResourceObject("ControlsCommon", "Projection").ToString();
                Projection.ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), Request.QueryString["Type"]);

                Series Actual = new Series();
                //Actual.Name = "Actual";
                Actual.Name = GetGlobalResourceObject("ControlsCommon", "Actual").ToString();
                Actual.ChartType = (SeriesChartType)Enum.Parse(typeof(SeriesChartType), Request.QueryString["Type"]);

                ChartArea myChartArea = new ChartArea();


                myChart.Legends.Add("myLegend");
                myChart.ChartAreas.Add(myChartArea);
                myChart.Series.Add(Projection);
                myChart.Series.Add(Actual);


                double[] zValues = new double[Request.QueryString["zValues"].Split(',').Length];

                for (int i = 0; i < zValues.Length; i++)
                {
                    zValues[i] = Convert.ToDouble(Request.QueryString["zValues"].Split(',')[i]);
                }

                double[] yValues = new double[Request.QueryString["yValues"].Split(',').Length];

                for (int i = 0; i < yValues.Length; i++)
                {
                    yValues[i] = Convert.ToDouble(Request.QueryString["yValues"].Split(',')[i]);
                }

                string[] xValues = Request.QueryString["xValues"].Split(',');

                bool hasValue = false;

                foreach (double val in yValues)
                {
                    if (val != 0.0d)
                        hasValue = true;
                }

                if (hasValue)
                {
                    myChart.Series[GetGlobalResourceObject("ControlsCommon", "Projection").ToString()].Points.DataBindXY(xValues, yValues);
                    myChart.Series[GetGlobalResourceObject("ControlsCommon", "Actual").ToString()].Points.DataBindXY(xValues, zValues);

                    for (int iCounter = 0; iCounter < myChart.Series.Count; iCounter++)
                    {
                        myChart.Series[iCounter].MarkerStyle = MarkerStyle.Diamond;
                        myChart.Series[iCounter].MarkerSize = 8;
                        myChart.Series[iCounter].MarkerColor = myChart.Series[iCounter].BorderColor;
                        myChart.Series[iCounter].BorderDashStyle = ChartDashStyle.Solid;
                        myChart.Series[iCounter].BorderWidth = 4;
                        myChart.Series[iCounter]["PixelPointWidth"] = "20";

                        myChart.Series[iCounter].LabelToolTip = "#VAL";
                    }

                    myChartArea.AxisX.IsMarginVisible = true;
                    myChartArea.AxisX.Interval = 1;
                    
                    myChart.Legends[0].Font = new Font(new FontFamily("Tahoma"), 10);
                    myChart.Palette = ChartColorPalette.None;

                    if (Request.QueryString["Docking"] == null)
                        myChart.Legends[0].Docking = Docking.Bottom;
                    else
                        myChart.Legends[0].Docking = (Docking)Enum.Parse(typeof(Docking), Request.QueryString["Docking"]);
                }
                else
                {
                    myChart.Legends.RemoveAt(0);
                }

                MemoryStream ms = new MemoryStream();

                myChart.SaveImage(ms, ChartImageFormat.Jpeg);

                Response.ContentType = "image/jpeg";
                Response.OutputStream.Write(ms.ToArray(), 0, ms.ToArray().Length);

                ms.Dispose();

                Response.End();

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
   
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    </div>
    </form>
</body>
</html>
