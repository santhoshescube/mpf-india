﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using HRAutoComplete;

public partial class Public_AllEmployeeDocuments : System.Web.UI.Page
{

    private eDocumentsSearchType SearchType { get; set; }
    private eDocumentsFilterType FilterType { get; set; }

    private List<EmployeesSubmittedDocuments> AllEmployeesSubmittedDocumentsList { get; set; }
    private List<AllDocumentTypes> AllDocumentTypeNameList { get; set; }

    private int Iterator = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        ddlType_SelectedIndexChanged(new object(), new EventArgs());

        if (!IsPostBack)
        {
            ddlStatus.SelectedIndex = 0;            
            //Inititally it will load all the employees with the status filteration of 'ANY'
            LoadAllEmployeesDocuments(string.Empty);
        }
        FilterType = ddlStatus.SelectedValue == "0" ? eDocumentsFilterType.Any : (ddlStatus.SelectedValue == "1" ? eDocumentsFilterType.Submitted : eDocumentsFilterType.NotSubmitted);

    }

    private void LoadAllEmployeesDocuments(string SearchCaption)
    {
        int TotalDocumentType = 0;
        int Total = 0;
        int RowCount = 0;
        Iterator = 0;

        DataSet ds = clsEmployee.GetAllEmployeesDocumentsDetails(SearchType == eDocumentsSearchType.DocumentType ? 1 : 2, SearchCaption);
        DataTable dtAllEmployees = ds.Tables[1];
        DataTable dtTemp = dtAllEmployees.Copy();
        
        //This List will contain all the employees submitted the documents
        AllEmployeesSubmittedDocumentsList = new List<EmployeesSubmittedDocuments>();

        //This List will keep all the document type name corresponding to the column index ..using this list to show the tool tip for the corresponding column
        AllDocumentTypeNameList = new List<AllDocumentTypes>();

        //Total Document type is the total no of columns minus these columns(SL.No Column,EmpID Column,EmployeeName Column)
        TotalDocumentType = ds.Tables[1].Columns.Count - 3;

        //Adding all the employees available documents to the list
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            AllEmployeesSubmittedDocumentsList.Add(new EmployeesSubmittedDocuments()
            {
                DocumentTypeID = dr["DocumentTypeID"].ToInt32(),
                EmployeeID = dr["EmployeeID"].ToInt32()
            });
        }

        RowCount = dtAllEmployees.Rows.Count;
        for (int i = 0; i < RowCount; i++)
        {
            DataRow dr = dtTemp.Rows[i];

            Total = 0;

            for (int j = 3; j < dr.ItemArray.Length; j++)
            {
                if (IsDocumentSubmitted(dr["EmployeeID"].ToInt32(), dr[j].ToInt32()))
                {
                    Total = Total + 1;
                }
            }

            if (FilterType == eDocumentsFilterType.NotSubmitted)
            {
                if (TotalDocumentType == Total)
                {
                    DataRow[] drAll = dtAllEmployees.Select("EmployeeID=" + dr["EmployeeID"].ToInt32());
                    dtAllEmployees.Rows.Remove(drAll[0]);
                }
            }
            else if (FilterType == eDocumentsFilterType.Submitted)
            {
                if (Total == 0)
                {
                    DataRow[] drAll = dtAllEmployees.Select("EmployeeID=" + dr["EmployeeID"].ToInt32());
                    dtAllEmployees.Rows.Remove(drAll[0]);
                }
            }
        }

        dgvAllEmployeeDocuments.DataSource = dtAllEmployees;
        dgvAllEmployeeDocuments.DataBind();

        dvNoRecord.Visible = dtAllEmployees.Rows.Count == 0;
        btnPrint.Enabled = dtAllEmployees.Rows.Count > 0;
        updDetails.Update();
    }




    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        int EmployeeID = 0;
        int DocumentTypeID = 0;


        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[0].Text = "Sl.No";
            e.Row.Cells[0].Width = new Unit("20px");

            for (int i = 3; i < e.Row.Cells.Count; i++)
            {
                AllDocumentTypeNameList.Add(new AllDocumentTypes() { Index = i, DocumentType = e.Row.Cells[i].Text.ToString() });
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Iterator = Iterator + 1;

            e.Row.Cells[0].Text = Iterator.ToString();

            e.Row.Cells[1].Visible = false;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {



                EmployeeID = e.Row.Cells[1].Text.ToInt32();
                for (int i = 3; i < e.Row.Cells.Count; i++)
                {
                    DocumentTypeID = e.Row.Cells[i].Text.ToInt32();
                    var DocumentType = AllDocumentTypeNameList.Where(t => t.Index == i).Select(t => t.DocumentType);

                    Image img = new Image();

                    foreach (var t in DocumentType)
                    {
                        img.ToolTip = t.ToString();
                        break;

                    }


                    if (IsDocumentSubmitted(EmployeeID, DocumentTypeID))
                        img.ImageUrl = "../images/Verified.png";
                    else
                        img.ImageUrl = "../images/NotVerified.png";

                    e.Row.Cells[i].Controls.Add(img);

                    e.Row.Cells[i].CssClass = "Verified";

                }
            }

        }

    }

    private void RegisterAutoComplete()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, (SearchType == eDocumentsSearchType.EmployeeName ? AutoCompletePage.AllEmployees : AutoCompletePage.DocumentType), objUserMaster.GetCompanyId());
    }

    private bool IsDocumentSubmitted(int EmployeeID, int DocumentTypeID)
    {
        var obj = AllEmployeesSubmittedDocumentsList.Where(t => t.EmployeeID == EmployeeID && t.DocumentTypeID == DocumentTypeID).Count();

        return obj > 0;

    }


    private class EmployeesSubmittedDocuments
    {
        public int EmployeeID { get; set; }

        public int DocumentTypeID { get; set; }
    }




    private class AllDocumentTypes
    {
        public string DocumentType { get; set; }
        public int Index { get; set; }
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        LoadAllEmployeesDocuments(txtSearch.Text.Trim());
    }
    protected void btnPrint_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", " setTimeout(function(){ Print('" + dvRecord.ClientID + "')},0);", true);

        // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", " setTimeout(function(){ window.print()},1000);", true);

    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SearchType = (ddlType.SelectedValue == "1" ? eDocumentsSearchType.DocumentType : eDocumentsSearchType.EmployeeName);
        if (clsUserMaster.GetCulture() == "ar-AE")
            lblEMployee.Text = SearchType == eDocumentsSearchType.DocumentType ? "نوع الوثيقة" : "اسم موظف";
        else
            lblEMployee.Text = SearchType == eDocumentsSearchType.DocumentType ? "Document Type" : "Employee Name";

        RegisterAutoComplete();

    }
}

public enum eDocumentsSearchType
{
    DocumentType = 1,
    EmployeeName = 2
}

public enum eDocumentsFilterType
{
    Any = 0,
    Submitted = 1,
    NotSubmitted = 2
}