﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_PerformanceTemplate : System.Web.UI.Page
{
    clsPerformancetemplate objTemplate;





    //protected void Page_PreInit(object sender, EventArgs e)
    //{
    //    clsGlobalization.SetCulture(((Page)this));
    //}




    protected void Page_Load(object sender, EventArgs e)
    {

        this.Title = GetLocalResourceObject("PerformanceTemplate.Text").ToString();

        Templatepager.Fill += new controls_Pager.FillPager(BindTemplateDatalist);
        
        if (!IsPostBack)
        {
            SetPermission();
            hdnAddMode.Value = "1";
            BindDepartments();
            rdlist.SelectedIndex = 0;
            BindGrades();
            BindDatalistGoals();
            ViewTemplates();
           
        }
        EnableDisableMenus();
    }



    private void EnableDisableMenus()
    {
        //if (ViewState["IsCreate"] != null && Convert.ToBoolean(ViewState["IsCreate"]) == true)
        //{
        //    dvViewHavePermission.Style["display"] = "block";
        //    dvNoPermission.Style["display"] = "none";
        //}
        //else
        //{
        //    dvViewHavePermission.Style["display"] = "none";
        //    dvNoPermission.Style["display"] = "block";
        //}

        lnkAdd.Enabled = Convert.ToBoolean(ViewState["IsCreate"]);
        lnkView.Enabled = Convert.ToBoolean(ViewState["IsView"]);
    }

    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerformanceTemplate);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }













    protected void btnAddQusetions_Click(object sender, EventArgs e)
    {
        string strMessgedupQuestion = string.Empty;
        objTemplate = new clsPerformancetemplate();
        
       
        if (clsPerformancetemplate.IsGoalNameExists(txtQuestion.Text, ViewState["GoalID"].ToInt32()))
        {
            strMessgedupQuestion =GetLocalResourceObject("GoalNameexists").ToString();
        }
        else
        {

            int GoalID = objTemplate.SaveGoals(txtQuestion.Text, ViewState["GoalID"].ToInt32());
            txtQuestion.Text = string.Empty;
            strMessgedupQuestion = GetLocalResourceObject("Goalsavedsuccessfully").ToString();
            BindDatalistGoals();
        }
      
        
        ucMessage.InformationalMessage(strMessgedupQuestion);
        mpeMessage.Show();
        //updQuestions.Update();
    }
    //protected void rdGrade_CheckedChanged(object sender, EventArgs e)
    //{
    //    GradeDivVisibility();
    //}
    private void GradeDivVisibility()
    {
        txtMaxMark.Text = string.Empty;
        divgrades.Style["display"] = "block";
        divSetMaxmarks.Style["display"] = "none";
        if (rdlist.SelectedIndex==1)
        {
            BindGrades();

        }
       
    }
    private void MarkDivVisibility()
    {
        txtMaxMark.Text = string.Empty;
        divSetMaxmarks.Style["display"] = "block";
        divgrades.Style["display"] = "none";
    }
    //protected void rdMark_CheckedChanged(object sender, EventArgs e)
    //{
    //    MarkDivVisibility();
    //}
    private void BindDepartments()
    {
        ddlDepartment.DataSource = clsPerformancetemplate.BindDepartments();
        ddlDepartment.DataBind();
    }
    private void BindGrades()
    {
        Repeater1.DataSource = clsPerformancetemplate.BindGrades();
        Repeater1.DataBind();
        divgrades.Style["display"] = "block";
    }
    protected void BindDatalistGoals()
    {
        txtQuestion.Text = string.Empty;
        ViewState["GoalID"] = "";
        DataTable dt = clsPerformancetemplate.GetQuestions();
        dlQuestionsList.DataSource = dt;
        dlQuestionsList.DataBind();
        if(dt.Rows.Count>0)
            divQuestionsList.Style["display"] = "block";
        else
            divQuestionsList.Style["display"] = "none";
    }
    private void AddNewtemplate()
    {
        hdnAddMode.Value = "1";
        rdlist.SelectedIndex = 0;
        MarkDivVisibility();
        txtTemplateName.Text = string.Empty;
        txtMaxMark.Text = string.Empty;
        SetDivVisiblity(1);
        BindDatalistGoals();
        lnkDelete.Enabled = false;
        lnkDelete.OnClientClick = "return false;";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        cmp.Enabled = false;
         
        objTemplate=new clsPerformancetemplate();
        bool IsGrading = Convert.ToBoolean(rdlist.SelectedIndex);
        if (hdnAddMode.Value=="1")
            hdnTemplateEditID.Value = string.Empty;
        
        if (rdlist.SelectedIndex == 0) 
        {
          if((txtMaxMark.Text == string.Empty)||(txtMaxMark.Text=="0.00")||(txtMaxMark.Text=="0")||(txtMaxMark.Text=="0.0"))
          {
           // ucMessage.InformationalMessage("Please enter a valid Mark");

             ucMessage.InformationalMessage(GetLocalResourceObject("PleaseenteravalidMark").ToString());
            mpeMessage.Show();
            cmp.Enabled = true;
            return;
          }
        
        }
        if (!ValidateForm())
        {
            cmp.Enabled = true;
            return;
        }
        if (clsPerformancetemplate.IsTemplateNameExists(txtTemplateName.Text, hdnTemplateEditID.Value.ToInt32(), hdnAddMode.Value))
        {
            //ucMessage.InformationalMessage("Template name exists");
            ucMessage.InformationalMessage(GetLocalResourceObject("Templatenameexists").ToString());
            mpeMessage.Show();
            cmp.Enabled = true;
            return;
        }
        else
        {
            int TemplateID = objTemplate.InsertUpdatePerformanceTemplate(txtTemplateName.Text, ddlDepartment.SelectedValue.ToInt32(), IsGrading, txtMaxMark.Text.ToDecimal(), hdnAddMode.Value, hdnTemplateEditID.Value.ToInt32());
            if (TemplateID > 0)
            {

                foreach (DataListItem item in dlQuestionsList.Items)
                {
                    if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                    {
                        clsPerformanceTemplatedetails objtempalteDetailsList = new clsPerformanceTemplatedetails();
                        CheckBox chkGoals = (CheckBox)item.FindControl("chkGoals");
                        if (chkGoals == null)
                            continue;
                        if (chkGoals.Checked)
                        {
                            objTemplate.InsertTemplateDetails(TemplateID, Convert.ToInt32(dlQuestionsList.DataKeys[item.ItemIndex]));
                        }
                    }
                }
                //ucMessage.InformationalMessage("Template added successfully");
                ucMessage.InformationalMessage(GetLocalResourceObject("Templateaddedsuccessfully").ToString());
                mpeMessage.Show();
                ViewTemplates();
            }
        }
        cmp.Enabled = true;
    }
    public bool ValidateForm()
    {
        //if (chk_All.Checked)
        //    return true;

        bool Selected = false;
        string strvalidationMessage = string.Empty;
        
            // EmployeeWise
            foreach (DataListItem item in dlQuestionsList.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox chkGoals = (CheckBox)item.FindControl("chkGoals");

                    if (chkGoals.Checked)
                    {
                        Selected = true;
                        break;
                    }
                }
            }
            //strvalidationMessage = "Please select a Goal.";
            strvalidationMessage =GetLocalResourceObject("PleaseselectaGoal").ToString();
       
    
        ucMessage.InformationalMessage(strvalidationMessage);
        mpeMessage.Show();
        return Selected;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ViewTemplates();
    }
    protected void chkGoals_CheckedChanged(object sender, EventArgs e)
    {
        
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
       
        AddNewtemplate();
       
    }
    private void SetDivVisiblity(int intMode)
    {
        /* 1 - Add New
         * 2 - Single View
         * 3 - View Requests
         * 4 - Issue Request
         * */

        if (intMode == 1)
        {
           
            divAdd.Style["display"] = "block";
            divView.Style["display"] =  "none";
            divSingleView.Style["display"] = "none";
            if (Convert.ToBoolean(ViewState["IsCreate"]) == true)
            {
                dvViewHavePermission.Style["display"] = "block";
                dvNoPermission.Style["display"] = "none";
            }
            else
            {
                dvViewHavePermission.Style["display"] = "none";
                dvNoPermission.Style["display"] = "block"; 
            }


        }
        else if (intMode == 2)
        {
            divSingleView.Style["display"] = "block";
            divAdd.Style["display"] = divView.Style["display"] = "none";
            if (Convert.ToBoolean(ViewState["IsView"]) == true)
            {
                dvViewHavePermission.Style["display"] = "block";
                dvNoPermission.Style["display"] = "none";
            }
            else
            {
                dvViewHavePermission.Style["display"] = "none";
                dvNoPermission.Style["display"] = "block"; 
            }

        }
        else
            if (intMode == 3)
            {
                divView.Style["display"] = "block";
                divAdd.Style["display"] = "none";
                divSingleView.Style["display"] = "none";

                if (Convert.ToBoolean(ViewState["IsView"]) == true)
                {
                    dvViewHavePermission.Style["display"] = "block";
                    dvNoPermission.Style["display"] = "none";
                }
                else
                {
                    dvViewHavePermission.Style["display"] = "none";
                    dvNoPermission.Style["display"] = "block"; 
                }

            }
       
        upnlAdd.Update();
        upnlView.Update();
        upnlSingleView.Update();
    }
    private void ViewTemplates()
    {
        SetDivVisiblity(3);
        upnlAdd.Update();
        upnlView.Update();
        upnlSingleView.Update();
        Templatepager.CurrentPage = 0;
        BindTemplateDatalist();
    }
    private void BindTemplateDatalist()
    {
       
       int PageIndex=Templatepager.CurrentPage + 1;
       int PageSize = Templatepager.PageSize;
        DataTable dttemplate = clsPerformancetemplate.GetTemplateList(PageIndex,PageSize);
        if (dttemplate != null && dttemplate.Rows.Count > 0)
        {
            Templatepager.Total = clsPerformancetemplate.GetTotalRecordCount();
            dlTemplateList.DataSource = dttemplate;
            dlTemplateList.DataBind();
            Templatepager.Visible = true;
            lnkDelete.Enabled = true;
            //lnkDelete.OnClientClick = "return valDeleteDatalistNew('" + dlTemplateList.ClientID + "');";
            lnkDelete.OnClientClick = "return valDeleteDatalistNew('" + dlTemplateList.ClientID + "');";



        }
        else
        {
            lnkView.Enabled = false;
            lnkDelete.Enabled = false;
            lnkDelete.OnClientClick = "return false;";
            dlTemplateList.DataSource = null;
            dlTemplateList.DataBind();

            AddNewtemplate();

            Templatepager.Visible = false;
        }
        upMenu.Update();
        upnlAdd.Update();
        upnlView.Update();
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {

       
        ViewTemplates();
    }
    protected void dlTemplateList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {

            HiddenField hdIsGrading = (HiddenField)e.Item.FindControl("hdIsGrading");
            HtmlTableRow trGradeScheme = (HtmlTableRow)e.Item.FindControl("trGradeScheme");
            HtmlTableRow trMarkScheme = (HtmlTableRow)e.Item.FindControl("trMarkScheme");
            Literal LitEvaluation =(Literal)e.Item.FindControl("LitEvaluation");


            if(LitEvaluation != null)
            {
                if (LitEvaluation.Text == "Mark")
                    LitEvaluation.Text = GetLocalResourceObject("Mark.Text").ToString();
                else
                    LitEvaluation.Text = GetLocalResourceObject("Grade.Text").ToString();

            }
            if (hdIsGrading.Value == "True")
            {

                trGradeScheme.Style["display"] = "table-row";
                trMarkScheme.Style["display"] = "none";
            }
            else
            {
                trMarkScheme.Style["display"] = "table-row";
                trGradeScheme.Style["display"] = "none";
            }

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("imgEdit");
            HiddenField hddlTemplateID = (HiddenField)e.Item.FindControl("hddlTemplateID");
            if (clsPerformancetemplate.IsPerformanceInitiationExists(hddlTemplateID.Value.ToInt32()))
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }

        }
    }
    private void DeleteRequest()
    {
        try
        {
            string strMessge = string.Empty;
            objTemplate = new clsPerformancetemplate();
            if (dlTemplateList.Items.Count > 0) // Delete in View Mode
            {
                foreach (DataListItem Item in dlTemplateList.Items)
                {
                    
                    if (Item.ItemType == ListItemType.Item || Item.ItemType == ListItemType.AlternatingItem)
                    {
                        CheckBox chkTemplate = Item.FindControl("chkTemplate") as CheckBox;
                        if (chkTemplate.Checked)
                        {
                            int TemplateID = dlTemplateList.DataKeys[Item.ItemIndex].ToInt32();

                            if (clsPerformancetemplate.IsPerformanceInitiationExists(TemplateID))
                            {
                               // strMessge = "Template cannot be deleted,Template in use  ";
                                strMessge =GetLocalResourceObject("TemplateInUse").ToString();
                            }
                            else
                            {
                                if (objTemplate.DeleteTemplate(TemplateID))
                                {
                                    //clsCommonMessage.DeleteMessages(dlDocumentRequest.DataKeys[Item.ItemIndex].ToInt32(), eReferenceTypes.DocumentRequest);
                                    //strMessge = "Template Deleted Successfully";
                                    strMessge =GetLocalResourceObject("TemplateDeletedSuccessfully").ToString();

                                }
                            }

                        }
                    }
                }
                
            }
            else
            {
                if (clsPerformancetemplate.IsPerformanceInitiationExists(hdnTemplateEditID.Value.ToInt32()))
                {
                    //strMessge = "Template cannot be deleted,Template in use  ";
                    strMessge = GetLocalResourceObject("TemplateInUse").ToString();
                }
                else
                {
                    if (objTemplate.DeleteTemplate(hdnTemplateEditID.Value.ToInt32()))
                    {
                        //clsCommonMessage.DeleteMessages(dlDocumentRequest.DataKeys[Item.ItemIndex].ToInt32(), eReferenceTypes.DocumentRequest);
                        //strMessge = "Template Deleted Successfully";
                        strMessge = GetLocalResourceObject("TemplateDeletedSuccessfully").ToString();
                    }
                }
            }
            ViewTemplates();
            ucMessage.InformationalMessage(strMessge);
            mpeMessage.Show();
        }
        catch
        {
        }
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        DeleteRequest();
    }

    protected void dlQuestionsList_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string strMessge1 = string.Empty;
        switch (e.CommandName)
        {
            case "REMOVE_Question":

                objTemplate = new clsPerformancetemplate();

                if (e.CommandArgument != string.Empty)
                {
                    if (clsPerformancetemplate.IsGoalExists(hdnTemplateEditID.Value.ToInt32(), e.CommandArgument.ToInt32(), hdnAddMode.Value))
                    {
                       // strMessge1 = "Goal used in another template,deletion not possible";

                        strMessge1 = GetLocalResourceObject("GoalInUse").ToString();
                    }
                    else
                    {
                        objTemplate.DeleteGoals(hdnTemplateEditID.Value.ToInt32(), Convert.ToInt32(e.CommandArgument), hdnAddMode.Value);
                        //strMessge1 = "Goal deleted successfully";
                        strMessge1 = GetLocalResourceObject("GoaldeletedSuccessfully").ToString();
                            



                        BindDatalistGoals();
                    }
                }
                ucMessage.InformationalMessage(strMessge1);
                mpeMessage.Show();
                break;
            case "Edit_goal":
                HiddenField hdGoalEditID=(HiddenField)e.Item.FindControl("hdGoalEditID");
                DataTable dtGoal = clsPerformancetemplate.ShowGoal(e.CommandArgument.ToInt32());
                txtQuestion.Text=Convert.ToString(dtGoal.Rows[0]["Goal"]);
                hdGoalEditID.Value = Convert.ToString(dtGoal.Rows[0]["GoalID"]);
                ViewState["GoalID"] = hdGoalEditID.Value;
                break;
              
        }
    }
    protected void dlTemplateList_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();
        switch (e.CommandName)
        {
            case "Edit":
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                upMenu.Update();
                hdnAddMode.Value = "0";
                objTemplate=new clsPerformancetemplate();
                int TemplateID=Convert.ToInt32(e.CommandArgument);
                hdnTemplateEditID.Value = e.CommandArgument.ToString();
                DataSet dtTemplate = objTemplate.GetSingleTemplate(TemplateID);
                if (dtTemplate.Tables[0].Rows.Count > 0)
                {
                    SetDivVisiblity(1);
                    txtTemplateName.Text = dtTemplate.Tables[0].Rows[0]["TemplateName"].ToString();
                    ddlDepartment.SelectedValue = dtTemplate.Tables[0].Rows[0]["DepartmentID"].ToStringCustom();

                    if (dtTemplate.Tables[0].Rows[0]["IsGrading"].ToBoolean() == true)
                    {
                        rdlist.SelectedIndex = 1;
                       
                        GradeDivVisibility();
                    }
                    else
                    {
                        rdlist.SelectedIndex = 0;
                        MarkDivVisibility();
                    }
                    txtMaxMark.Text = dtTemplate.Tables[0].Rows[0]["MaxMarksPerQuestion"].ToString();
                    
                    BindDatalistGoals();
                    if(dtTemplate.Tables[1].Rows.Count>0)
                    {
                        foreach (DataRow row in dtTemplate.Tables[1].Rows)
                        {
                            foreach (DataListItem item in dlQuestionsList.Items)
                            {
                                CheckBox chkGoals = (CheckBox)item.FindControl("chkGoals");
                                if (chkGoals == null)
                                    continue;
                                if (dlQuestionsList.DataKeys[item.ItemIndex].ToInt32() == row["GoalID"].ToInt32())
                                    chkGoals.Checked = true;
                            }
                        }
                    }
                }
                break;
            case "View":
                hdnTemplateEditID.Value = Convert.ToString(e.CommandArgument);
                hdnAddMode.Value = "0";
                BindSingleView();
                break;

          
        }
        upMenu.Update();
        
    }
    private void BindSingleView()
    {
        SetDivVisiblity(2);

        dlTemplateList.DataSource = null;
        dlTemplateList.DataBind();

        lnkDelete.OnClientClick = "return confirm('"+GetGlobalResourceObject("ControlsCommon","DeleteConfirmation")+"');";
        upMenu.Update();

        objTemplate = new clsPerformancetemplate();
      
        DataSet dsRequestDetails = objTemplate.GetSingleTemplate(hdnTemplateEditID.Value.ToInt32());
        if (dsRequestDetails != null && dsRequestDetails.Tables[0].Rows.Count > 0)
        {
            divTemplateName.InnerText = Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["TemplateName"]);
            divDepartment.InnerText = Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["Department"]);
            divEvaluationScheme.InnerText = Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["EvaluationScheme"]);
            if (Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["EvaluationScheme"]) == "Mark")
            {
                divMarkScheme.Style["display"] = "block";
               divMaxMarks.InnerText = Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["MaxMarksPerQuestion"]);
            }
            else
                divMarkScheme.Style["display"] = "none";
            divNoOfQuestions.InnerText = Convert.ToString(dsRequestDetails.Tables[0].Rows[0]["NoOfQuestions"]);
            if (dsRequestDetails.Tables[1].Rows.Count > 0)
            {
                dlShowGoals.DataSource = dsRequestDetails.Tables[1];
                dlShowGoals.DataBind();
            }
        }
        
        upnlView.Update();
        upnlSingleView.Update();

    }

    protected void rdlist_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdlist.SelectedIndex == 0)
        {
            MarkDivVisibility();
            
        }
        else
        {
            GradeDivVisibility();
        }
    }
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        txtQuestion.Text = string.Empty;
        ViewState["GoalID"] = "";
    }
    protected void dlQuestionsList_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {



            ImageButton imgEditGoal = (ImageButton)e.Item.FindControl("imgEditGoal");
            HiddenField hdGoalEditID = (HiddenField)e.Item.FindControl("hdGoalEditID");
            if (clsPerformancetemplate.IsEvaluationExists(hdGoalEditID.Value.ToInt32()))
            {
                imgEditGoal.Enabled = false;
                imgEditGoal.ImageUrl = "~/images/edit_disable.png";
            }

        }
    }
}
