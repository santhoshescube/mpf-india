﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.IO;
using System.Data.SqlClient;
using System.Text;

public partial class Public_Employee : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsCandidate objCandidate;
    clsJobPost objJobPost;
    clsSalaryStructure objSalaryStructure;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUser;

    int iLeavePolicyId;
    private bool MblnUpdatePermission = true;  // Edit Button Permission In Datalist

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetGlobalResourceObject("ControlsCommon", "Employee").ToString();

        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();
        pgrEmployees.Fill += new controls_Pager.FillPager(Bind);

        if (!IsPostBack)
        {
            FillAllCompany();

            if (Request.QueryString["EmpId"] != null)
                ShowEmployeeDetails();
            else
            {
                if (Request.QueryString["EmployeeID"] == null)
                {
                    if (Request.QueryString["CandidateId"] == null)
                        Bind();
                    else
                    {
                        ViewState["Candidate"] = true;
                        NewEmployee();
                    }
                }
                else
                {
                    objEmployee = new clsEmployee();
                    BindFormView(Convert.ToInt32(Request.QueryString["EmployeeID"]));

                    if (fvEmployee.DataItemCount == 0)
                    {
                        if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.View_Employee))
                        {
                            Bind();
                            mcMessage.InformationalMessage("EmployeeID does not exist");
                        }
                    }
                }
            }


            DataTable dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Employee); //set Permission For View /Edit/ Delete
            ViewState["Permission"] = dt;
            SetPermission();
        }

        RegisterAutoComplete();




    }

    private void FillAllCompany()
    {
        DataTable dt = new clsCompany().GetCompaniesbyUser(new clsUserMaster().GetUserId());
        DataRow dr = dt.NewRow();
        dr["CompanyName"] = clsUserMaster.GetCulture() == "ar-AE" ? ("أي") : ("Any");
        dr["CompanyId"] = "-1";
        dt.Rows.InsertAt(dr, 0);


        ddlCompany.DataSource = dt;
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Employee, ddlCompany.SelectedValue.ToInt32());
    }

    protected void Bind()
    {
        divEmployees.Style["display"] = "block";
        divSort.Style["display"] = "block";
        divCandidateHistory.Style["display"] = "none";
        divNoData.Style["display"] = "none";
        divPintDetails.Style["display"] = "none";
        divCandidateHistory.Style["display"] = "none";

        objEmployee = new clsEmployee();
        objUser = new clsUserMaster();

        objEmployee.PageIndex = pgrEmployees.CurrentPage + 1;
        objEmployee.PageSize = pgrEmployees.PageSize;
        objEmployee.SearchKey = txtSearch.Text;
        objEmployee.UserId = objUser.GetUserId();
        objEmployee.CompanyID = ddlCompany.SelectedValue.ToInt32();

        if (ddlExpression.SelectedValue.ToInt32() > 0)
            objEmployee.SortExpression = ddlExpression.SelectedValue.ToInt32();
        else
            objEmployee.SortExpression = 1;

            switch (ddlOrder.SelectedValue.ToInt32())
            {
                case 1:
                    objEmployee.SortOrder = "ASC";
                    break;
                case 2:
                    objEmployee.SortOrder = "DESC";
                    break;
                default:
                    objEmployee.SortOrder = "DESC";
                    break;
            }         
    
        pgrEmployees.Total = objEmployee.GetCount();
        DataSet ds = objEmployee.GetAllEmployees();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlEmployees.DataSource = ds;
            dlEmployees.DataBind();

            EnableMenus();
            pgrEmployees.Visible = true;
            divSort.Style["display"] = "block";
        }
        else
        {
            divSort.Style["display"] = "none";
            dlEmployees.DataSource = null;
            dlEmployees.DataBind();
            pgrEmployees.Visible = false;
        }

        fvEmployee.ChangeMode(FormViewMode.ReadOnly);

        fvEmployee.DataSource = null;
        fvEmployee.DataBind();
    }

    public void SetPermission()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();
        if (UserID != 1 && UserID != 2)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                lnkAddEmp.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                lnkListEmp.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                lnkEmail.Enabled = lnkPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
                ViewState["MblnUpdatePermission"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }

            if (lnkListEmp.Enabled)
            {
                Bind();
            }
            else if (lnkAddEmp.Enabled)
            {
                NewEmployee();
            }
            else
            {
                pgrEmployees.Visible = false;
                dlEmployees.DataSource = null;
                dlEmployees.DataBind();
                fvEmployee.Visible = false;

                divEmployees.Style["display"] = "none";
                divSort.Style["display"] = "none";
                divNoData.Style["display"] = "block";
                divPintDetails.Style["display"] = "none";
                divCandidateHistory.Style["display"] = "none";
                lblNoData.Text = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString(); //"You dont have enough permission to view this page.";
                btnSearch.Enabled = false;
            }
        }
        else
        {
            lnkAddEmp.Enabled = lnkDelete.Enabled = lnkEmail.Enabled = lnkListEmp.Enabled = lnkPrint.Enabled = MblnUpdatePermission = true;
            Bind();
        }
    }


    public void EnableMenus()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";
    }
    protected void NewEmployee()
    {
        txtSearch.Text = string.Empty;

        ViewState["Buffer"] = null;
        ViewState["BufferP"] = null;

        pgrEmployees.Visible = false;

        objEmployee = new clsEmployee();

        objEmployee.EmployeeID = -1;

        DataTable dt = objEmployee.GetEmployee().Tables[0];

        DataRow dw = dt.NewRow();

        dt.Rows.InsertAt(dw, 0);

        fvEmployee.ChangeMode(FormViewMode.Edit);

        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;

        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

        fvEmployee.DataSource = dt;
        fvEmployee.DataBind();

        dlEmployees.DataSource = null;
        dlEmployees.DataBind();

    }

    public void ShowEmployeeDetails()
    {
        fvEmployee.ChangeMode(FormViewMode.ReadOnly);
        BindFormView(Convert.ToInt32(Request.QueryString["EmpId"]));
    }

    protected void fvEmployee_DataBound(object sender, EventArgs e)
    {
        upMenu.Update();
        if (fvEmployee.DataKey["EmployeeID"] != null && fvEmployee.DataKey["EmployeeID"] != DBNull.Value && fvEmployee.CurrentMode == FormViewMode.ReadOnly)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Tabs", "$(function() {$('#tabs').tabs();});", true);
        }
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        ViewState["Candidate"] = false;
        pgrEmployees.CurrentPage = 0;
        Bind();
        upEmployees.Update();
    }

    protected void dlEmployees_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objEmployee = new clsEmployee();
        imgDelete.Visible = lnkDelete.Visible = imgPrint.Visible = lnkPrint.Visible = imgEmail.Visible = lnkEmail.Visible = true;

        switch (e.CommandName)
        {
            case "VIEW":
                objEmployee.EmployeeID = Convert.ToInt32(e.CommandArgument);
                fvEmployee.ChangeMode(FormViewMode.ReadOnly);
                BindFormView(Convert.ToInt32(e.CommandArgument));
                divSort.Style["display"] = "none";
                break;
            case "ALTER":
                Response.Redirect("CreateEmployee.aspx?EmpId=" + Convert.ToInt32(e.CommandArgument) + "");
                break;
            case "CONFIRM":
                Response.Redirect("CreateEmployee.aspx?EmpId=" + Convert.ToInt32(e.CommandArgument) + "&Action=Confirm");
                break;
            case "VIEWDOC":
                Response.Redirect("ViewDocument.aspx?EmployeeId=" + Convert.ToInt32(e.CommandArgument));
                break;
            case "CandidateHistory":
                int intTempCandidate = Convert.ToInt32(e.CommandArgument);

                if (intTempCandidate > 0)
                {
                    Candidatehistorycontrol.GetHistory(intTempCandidate, null);
                    divEmployees.Style["display"] = "none";
                    divSort.Style["display"] = "none";
                    divNoData.Style["display"] = "none";
                    divPintDetails.Style["display"] = "none";
                    divCandidateHistory.Style["display"] = "block";

                    imgDelete.Enabled = lnkDelete.Enabled = imgPrint.Enabled = lnkPrint.Enabled = imgEmail.Enabled = lnkEmail.Enabled = false;
                    lnkDelete.OnClientClick = lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
                }
                break;
        }
        upMenu.Update();
    }
    private void BindFormView(int EmployeeID)
    {
        objEmployee = new clsEmployee();
        objEmployee.EmployeeID = EmployeeID;

        DataTable dt = objEmployee.GetEmployee().Tables[0];
        if (dt.Rows.Count == 0) return;
        ViewState["Buffer"] = dt.Rows[0]["RecentPhoto"];

        pgrEmployees.Visible = false;
        fvEmployee.DataSource = dt;
        fvEmployee.DataBind();

        //code for employee kpi/KRA
        //DataSet ds = objEmployee.GetEmployeeKPIKRA();
        //if (ds.Tables.Count > 1)
        //{
        //    GridView gvKPI = (GridView)fvEmployee.FindControl("gvKPI");
        //    HtmlGenericControl divKPI = (HtmlGenericControl)fvEmployee.FindControl("divKPI");
        //    GridView gvKRA = (GridView)fvEmployee.FindControl("gvKRA");
        //    HtmlGenericControl divKRA = (HtmlGenericControl)fvEmployee.FindControl("divKRA");
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        gvKPI.DataSource = ds.Tables[0];
        //        gvKPI.DataBind();
        //        divKPI.Style["display"] = "block";
        //    }
        //    else
        //    {
        //        gvKPI.DataSource = null;
        //        gvKPI.DataBind();
        //        divKPI.Style["display"] = "none";
        //    }
        //    if (ds.Tables[1].Rows.Count > 0)
        //    {
        //        gvKRA.DataSource = ds.Tables[1];
        //        gvKRA.DataBind();
        //        divKRA.Style["display"] = "block";
        //    }
        //    else
        //    {
        //        gvKRA.DataSource = null;
        //        gvKRA.DataBind();
        //        divKRA.Style["display"] = "none";
        //        if (ds.Tables[0].Rows.Count > 0)
        //            divKPI.Style["display"] = "block";
        //    }
        //}

        dlEmployees.DataSource = null;
        dlEmployees.DataBind();

        divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]));

        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (fvEmployee.CurrentMode != FormViewMode.Edit)
        {
            if (lnkPrint.Enabled)
                lnkPrint.OnClientClick = sPrinttbl;
            else
                lnkPrint.OnClientClick = "return false;";
            if (lnkEmail.Enabled)
                lnkEmail.OnClientClick = "return showMailDialog('Type=Employee&EmployeeId=" + Convert.ToString(fvEmployee.DataKey["EmployeeID"]) + "');";
            else
                lnkEmail.OnClientClick = "return false;";
            if (lnkDelete.Enabled)
                lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد أنك تريد حذف؟');") : ("return confirm('Are you sure to delete?');");
            else
                lnkDelete.OnClientClick = "return false;";
        }

    }
    protected void btnEdit_Click(object sender, EventArgs e)
    {

        txtSearch.Text = string.Empty;

        objEmployee = new clsEmployee();

        objEmployee.EmployeeID = Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]);

        fvEmployee.ChangeMode(FormViewMode.Edit);

        fvEmployee.DataSource = objEmployee.GetEmployee();
        fvEmployee.DataBind();

        dlEmployees.DataSource = null;
        dlEmployees.DataBind();

        upEmployees.Update();
    }


    protected string GetEmployeeRecentPhoto(object EmployeeID, object RecentPhoto, int width)
    {
        if (RecentPhoto == DBNull.Value)
            return GetEmployeePassportPhoto(EmployeeID, width);
        else
            return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeeRecentPhotoNew(object EmployeeID, object RecentPhoto, int width)
    {
        if (fvEmployee.DataKey["EmployeeID"] != DBNull.Value)
        {
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (RecentPhoto == DBNull.Value)
                return GetEmployeeRecentPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }

    }
    protected string GetEmployeePassportPhotoNew(object EmployeeID, object PassportPhoto, int width)
    {
        if (fvEmployee.DataKey["EmployeeID"] != DBNull.Value)
        {
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(EmployeeID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
        }
        else
        {
            int iEmpID = -1;
            if (PassportPhoto == DBNull.Value)
                return GetEmployeePassportPhoto(iEmpID, width);
            else
                return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", iEmpID, width, DateTime.Now.Ticks.ToString());
        }

    }

    protected string GetEmployeeRecentPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeeRecentPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetEmployeePassportPhoto(object EmployeeID, int width)
    {
        return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value || Convert.ToString(date) != string.Empty)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
        else
            return string.Empty;

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        pgrEmployees.CurrentPage = 0;
        Bind();
        upEmployees.Update();

        imgDelete.Visible = lnkDelete.Visible = imgPrint.Visible = lnkPrint.Visible = imgEmail.Visible = lnkEmail.Visible = true;
        upMenu.Update();
        // txtSearch.Text = string.Empty;
    }



    private bool CanDelete(int EmployeeID)
    {
        DataTable dt = clsRoleSettings.GetCompanyWisePermission(EmployeeID, new clsUserMaster().GetUserId());
        if (dt.Rows.Count > 0)
        {
            return Convert.ToBoolean(dt.Rows[0]["IsDelete"]);
        }
        else
        {
            return false;
        }
    }



    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objEmployee = new clsEmployee();

        CheckBox chkEmployee;
        LinkButton btnEmployee;

        string message = string.Empty;
        int DeletionFailedCount = 0;
        int PermissionCount = 0;
        int SuccessCount = 0;
        DataTable dtDetails = null;
        
        if (dlEmployees.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployees.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                objEmployee.EmployeeID = Convert.ToInt32(dlEmployees.DataKeys[item.ItemIndex]);
                if (CanDelete(objEmployee.GetCompanyByEmployee(objEmployee.EmployeeID)))
                {

                    dtDetails = objEmployee.CheckExistReferences();

                    if (dtDetails != null && dtDetails.Rows.Count > 0)
                    {
                        DeletionFailedCount = DeletionFailedCount + 1;
                        continue;
                    }
                    else
                    {
                        if (objEmployee.Delete())
                            SuccessCount = SuccessCount + 1;
                    }
                }
                else
                    PermissionCount = PermissionCount + 1;

            }

            if (SuccessCount > 0)
                message = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
            else
                //message = "Failed to delete selected employees since details exists in the system";
                message = GetLocalResourceObject("FailedToDeleteEmployee.Text").ToString();

            if (SuccessCount > 0 && DeletionFailedCount > 0)
            {
                if (PermissionCount > 0 && DeletionFailedCount > 0)
                    message = message + " Failed to delete " + DeletionFailedCount + "employees since details exists in the system and " + PermissionCount + " employees dont have delete permission to delete";
                else


                    // message = message +" Failed to delete " + DeletionFailedCount + "employees since details exists in the system";

                    message = message + GetLocalResourceObject("FailedToDeleteFew.Text").ToString().Replace("10", DeletionFailedCount.ToString());
            }



        }
        else
        {
            if (fvEmployee.CurrentMode == FormViewMode.ReadOnly)
            {
                objEmployee.EmployeeID = Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]);
                DataTable dt = objEmployee.CheckExistReferences();

                if (dt != null && dt.Rows.Count > 0)
                {
                    //message = "Details exists in the system.";
                    //message += "</ol>Please delete the existing details to proceed with employee deletion";


                    message = GetLocalResourceObject("CannotDelete").ToString();
                }
                else
                {
                    objEmployee.Delete();
                    //message = "Employee(s) deleted successfully";

                    message = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                }
            }
        }

        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        Bind();
        upEmployees.Update();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlEmployees.Items.Count > 0)
        {
            objEmployee = new clsEmployee();

            if (fvEmployee.DataKey["EmployeeID"] == DBNull.Value || fvEmployee.DataKey["EmployeeID"] == null)
            {
                CheckBox chkEmployee;
                string sEmployeeIds = string.Empty;

                foreach (DataListItem item in dlEmployees.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sEmployeeIds += "," + dlEmployees.DataKeys[item.ItemIndex];
                }

                sEmployeeIds = sEmployeeIds.Remove(0, 1);

                if (sEmployeeIds.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sEmployeeIds);
                else
                {
                    objEmployee.EmployeeID = Convert.ToInt32(sEmployeeIds);
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sEmployeeIds));
                }
            }
            else
            {
                objEmployee.EmployeeID = Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]);
                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]));//objEmployee.GetPrintText(Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]));
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }
    }





    public string CreateSelectedEmployeesContent(string sEmployeeIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@EmployeeIds", sEmployeeIds));

        DataTable dt = new DataLayer().ExecuteDataTable("HRspEmployeeMaster", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("EmployeeDetails.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td>");
        //sb.Append("<table width='100%' style='font: 11px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "EmployeeNumber").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("ControlsCommon", "Employee").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("ControlsCommon", "WorkingAt").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "EmploymentType").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "WorkStatus").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "WorkPolicy").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("ControlsCommon", "LeavePolicy").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["EmployeeNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Salutation"]) + "-" + Convert.ToString(dt.Rows[i]["FirstName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CompanyName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EmploymentType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["WorkStatus"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Workpolicy"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LeavePolicyName"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }



    public string GetPrintText(int iEmpID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GED"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmpID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        DataTable dt = new DataLayer().ExecuteDataSet("HRspEmployeeMaster", alParameters).Tables[0];

        DataTable dtsalary = GetEmployeeSalaryHead(iEmpID);

        DataRow dw = dt.Rows[0];

        StringBuilder sb = new StringBuilder();

        //sb.Append("<div style='font-family: Verdana; font-size: 11px;'>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>  <td colspan='2' style='font-weight: bold'>");

        sb.Append("<table border='0'>");
        sb.Append("<tr>");

        //return string.Format("../thumbnail.aspx?type=EmployeePassportPhoto&FromDB=true&EmployeeID={0}&width={1}&t={2}", EmployeeID, width, DateTime.Now.Ticks.ToString());


        //src='<%# GetEmployeeRecentPhoto(Eval("EmployeeID"), Eval("RecentPhoto"), 100)%>'
        string path = ConfigurationManager.AppSettings["_VIRTUAL_PATH"];
        if (Convert.ToString(dt.Rows[0]["Recentphoto"]) != string.Empty)
        {


            sb.Append("<td width='60px'><img src='" + "../thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&Width=70&EmployeeId=" + iEmpID + "&t=" + DateTime.Now.Ticks + "'/></td>");

        }

        else
        {
            sb.Append("<td width='60px'><img src='" + path + "thumbnail.aspx?FromDB=true&type=EmployeePassportPhoto&Width=70&EmployeeId=" + iEmpID + "&t=" + DateTime.Now.Ticks + "'/></td>");

        }
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + dw["FirstName"] + " " + dw["LastName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");














        sb.Append("<tr><td style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("EmployeeDetails.Text").ToString() + "</td></tr>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Employee").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append("<b>" + dw["Salutation"] + " " + dw["FirstName"] + " " + dw["MiddleName"] + " " + dw["LastName"] + "</b></td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "WorkingAt").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["CompanyName"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmployeeNumber").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeNumber"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Department").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Department"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Designation").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Designation"] + "</td>");
        sb.Append("</tr>");

        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmploymentType").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmploymentType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "DateOfJoining").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["DateofJoining"].ToDateTime().ToString("dd-MMM-yyyy") + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td> ");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "ProbationEndDate").ToString() + "</td>");
        sb.Append("<td>");
        if (Convert.ToString(dw["ProbationEndDate"]) != string.Empty)
            sb.Append(dw["ProbationEndDate"].ToDateTime().ToString("dd-MMM-yyyy") + "");
        sb.Append("</td>");
        sb.Append("</tr>");


        sb.Append("</table>");












        sb.Append("<h5 >");
        sb.Append(GetLocalResourceObject("WorkProfile.Text").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "WorkStatus").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["WorkStatus"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "WorkPolicy").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["WorkPolicy"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "LeavePolicy").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["LeavePolicy"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "OfficialEmail").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["OfficialEmailID"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<h5>");
        sb.Append(GetLocalResourceObject("PersonalFacts.Text").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Gender").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Gender"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Nationality").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Nationality"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "Religion").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Religion"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "DateOfBirth").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["DateofBirth"].ToDateTime().ToString("dd-MMM-yyyy") + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");





        sb.Append("<h5>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "PermanentInfo").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "AddressLine1").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["AddressLine1"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "PermanentPhone").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Phone"] + "</td>");
        sb.Append("</tr>");
        //sb.Append("<tr>");
        //sb.Append("<td>");
        //sb.Append(GetGlobalResourceObject("ControlsCommon", "PermanentMobile").ToString() + "</td>");
        //sb.Append("<td>");
        //sb.Append(dw["Mobile"] + "</td>");
        //sb.Append("</tr>");
        sb.Append("<tr>");

        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td width='150'>");
        //sb.Append("Email</td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "PermanentEmail").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmailID"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");

        sb.Append("<h5>");










        sb.Append("<h5>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "LocalInfo").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "LocalAddress").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["LocalAddress"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "LocalPhone").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["LocalPhone"] + "</td>");
        sb.Append("</tr>");



        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "LocalMobile").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["Mobile"] + "</td>");
        sb.Append("</tr>");









        sb.Append("</table>");
        sb.Append("<h5>");




        sb.Append("<h5>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmergencyInfo").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmergencyAddress").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmergencyAddress"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmergencyPhone").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmergencyPhone"] + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<h5>");



        sb.Append(GetLocalResourceObject("AccountInfo.Text").ToString() + "</h5>");
        sb.Append("<table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid; border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>");
        sb.Append("<tr>");
        sb.Append("<td style='width:200px'>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "TransactionType").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["TransactionType"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmployerBankName").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployerBank"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmployerBankAcc").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployerAccountNo"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmployeeBankName").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeBank"] + "</td>");
        sb.Append("</tr>");
        sb.Append("<tr>");
        sb.Append("<td>");
        sb.Append(GetGlobalResourceObject("ControlsCommon", "EmployeeBankAcc").ToString() + "</td>");
        sb.Append("<td>");
        sb.Append(dw["EmployeeAccountNo"] + "</td>");
        sb.Append("</tr>");

        sb.Append("</table>");

        return sb.ToString();
    }


    public DataTable GetEmployeeSalaryHead(int iEmpID)
    {
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GAES"));
        alParameters.Add(new SqlParameter("@EmployeeID", iEmpID));

        return new DataLayer().ExecuteDataTable("HRspEmployeeMaster", alParameters);
    }


    protected void ddlExpression_SelectedIndexChanged(object sender, EventArgs e)
    {

        Bind();


    }
    protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
    {

        Bind();

    }























    protected void btnEmail_Click(object sender, EventArgs e)
    {
        if (dlEmployees.Items.Count > 0)
        {
            objEmployee = new clsEmployee();
            string sEmployeeIds = string.Empty;

            if (fvEmployee.DataKey["EmployeeID"] == DBNull.Value || fvEmployee.DataKey["EmployeeID"] == null)
            {
                CheckBox chkEmployee;

                foreach (DataListItem item in dlEmployees.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sEmployeeIds += "," + dlEmployees.DataKeys[item.ItemIndex];
                }

                sEmployeeIds = sEmployeeIds.Remove(0, 1);
            }
            else
            {
                objEmployee.EmployeeID = Convert.ToInt32(fvEmployee.DataKey["EmployeeID"]);
                sEmployeeIds = fvEmployee.DataKey["EmployeeID"].ToString();
            }








            if (sEmployeeIds.Contains(","))
                Session["Content"] = CreateSelectedEmployeesContent(sEmployeeIds);
            else
            {
                Session["Content"] = GetPrintText(Convert.ToInt32(sEmployeeIds));
            }





            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=EmployeeNew&EmployeeId=" + sEmployeeIds + "', 750, 610);", true);
        }

    }
    protected void dlEmployees_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {

            ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            ImageButton imgViewDoc = (ImageButton)e.Item.FindControl("imgViewDoc");
            HiddenField hidWorkStatusID = (HiddenField)e.Item.FindControl("hidWorkStatusID");
            ImageButton imgConfirm = (ImageButton)e.Item.FindControl("imgConfirm");

            if (!imgViewDoc.Enabled)
            {
                imgViewDoc.ImageUrl = "~/images/view_documents_disable.png";
            }
            if (Convert.ToInt32(hidWorkStatusID.Value) == 7)
            {
                if (imgConfirm != null)
                    imgConfirm.Visible = true;
            }
            else
            {
                imgConfirm.Visible = false;
            }
            //if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Employee))
            //    btnEdit.Enabled = true;
            //else
            //    btnEdit.Enabled = false;
            btnEdit.Enabled = ViewState["MblnUpdatePermission"] == null ? true : ((bool)ViewState["MblnUpdatePermission"]);
        }
    }

    public string GetLogo(object oEmployeeID, int width)
    {
        return "../thumbnail.aspx?FromDB=true&type=EmployeeRecentPhoto&width=" + width + "&EmployeeID=" + Convert.ToInt32(oEmployeeID) + "&t=" + DateTime.Now.Ticks;
    }


    protected void btnSalaryStructure_Click(object sender, EventArgs e)
    {
        Response.Redirect("Salarystructure.aspx");
    }
    protected void btnPassport_Click(object sender, EventArgs e)
    {
        Response.Redirect("Passport.aspx");
    }
    protected void btnVisa_Click(object sender, EventArgs e)
    {
        Response.Redirect("Visa.aspx");
    }

    protected void btnOtherDocs_Click(object sender, EventArgs e)
    {
        Response.Redirect("OtherDocument.aspx");
    }

    public bool CheckDocs(object oEmployeeID)
    {
        objEmployee = new clsEmployee();
        objEmployee.EmployeeID = Convert.ToInt32(oEmployeeID);
        return objEmployee.IsEmployeeDocsExists();
    }

    protected void lnkAddEmp_Click(object sender, EventArgs e)
    {
        Response.Redirect("CreateEmployee.aspx", false);
    }
    protected void lnkListEmp_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        ViewState["Candidate"] = false;
        pgrEmployees.CurrentPage = 0;
        Bind();
        upEmployees.Update();

        imgDelete.Enabled = lnkDelete.Enabled = imgPrint.Enabled = lnkPrint.Enabled = imgEmail.Enabled = lnkEmail.Enabled = true;
        EnableMenus();
        SetPermission();
        upMenu.Update();
    }

    protected void lnkListAllDocuments_Click(object sender, EventArgs e)
    {
        string Page = "AllEmployeeDocuments.aspx";
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ListAllDocuments", "window.open('" + Page + "');", true);
    }
}