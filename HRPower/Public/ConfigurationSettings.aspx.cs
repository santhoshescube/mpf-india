﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Public_ConfigurationSettings : System.Web.UI.Page
{
    clsConfiguration objConfiguration;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaster;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            objRoleSettings = new clsRoleSettings();
            objUserMaster = new clsUserMaster();
            DataTable dt = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.ConfigurationHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();
            //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(),  (int)eMenuID.ConfigurationHR ))
            //    BindDataList();
            //else
            //    lblNoData.Text = "You dont have enough permission to view this page.";
        }
    }
    public void SetPermission()
    {
         objUserMaster = new clsUserMaster();
         objRoleSettings = new clsRoleSettings();
         int RoleID;


        RoleID = objUserMaster.GetRoleId();
        if (RoleID != 1 && RoleID != 2 && RoleID != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                if ( dtm.Rows[0]["IsView"].ToBoolean() ==true)
                {
                    BindDataList();
                }
                else
                {
                    lblNoData.Text = "You dont have enough permission to view this page.";
                    lblNoData.Visible = true ;
                }
            }
            else
            {
                lblNoData.Text = "You dont have enough permission to view this page.";
            }
        }
        else
        {
            BindDataList();
        }
    }
    protected void dlConfigurationSettings_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            int iFlg = 0;
            bool bPer = true;
            int RoleID;
            objUserMaster = new clsUserMaster();
            RoleID = objUserMaster.GetRoleId();
            objConfiguration = new clsConfiguration();
            DataTable dtm = (DataTable)ViewState["Permission"];

            if (RoleID != 1 && RoleID != 2 && RoleID != 3)
            {
                bPer = false;
            }
            else
            {
                bPer = true;
            }

            switch (e.CommandName)
            {
                // Modified By Lijo Johns M (All modified lines are commented)
                case "SAVE":
                    if (bPer == false)
                    {
                        if (dtm.Rows[0]["IsUpdate"].ToBoolean() == false && dtm.Rows[0]["IsCreate"].ToBoolean() == false)
                        {
                            iFlg = 1;
                            //lblNoData.Text = "You dont have enough permission to update this page.";
                            msgs.InformationalMessage("You dont have enough permission to update this page.");
                            break;
                        }
                    }
                    //int count=0;
                    foreach (DataListItem item in dlConfigurationSettings.Items)
                    {
                        //count = count++;
                        TextBox txtConfigurationValue = (TextBox)item.FindControl("txtConfigurationValue");

                        objConfiguration.ConfigurationID = Convert.ToInt32(dlConfigurationSettings.DataKeys[item.ItemIndex]);
                        objConfiguration.ConfigurationValue = txtConfigurationValue.Text;
                        // if(count==3)
                        // ScriptManager.RegisterClientScriptBlock(imbShowHistory, imbShowHistory.GetType(), Guid.NewGuid().ToString(), "SlideUpSlideDown('" + divCollapse.ClientID + "', 500)", true); // Passed parameter's are wrong
                        objConfiguration.UpdateConfiguration();
                    }
                    break;

                case "RESET":
                    if (bPer == false)
                    {
                        if (dtm.Rows[0]["IsUpdate"].ToBoolean() == false && dtm.Rows[0]["IsCreate"].ToBoolean() == false)
                        {
                            iFlg = 1;
                            //lblNoData.Text = "You dont have enough permission to update this page.";
                            msgs.InformationalMessage("You dont have enough permission to update this page.");
                            break;
                        }
                    }
                    foreach (DataListItem item in dlConfigurationSettings.Items)
                    {
                        objConfiguration.ConfigurationID = Convert.ToInt32(dlConfigurationSettings.DataKeys[item.ItemIndex]);
                        objConfiguration.ResetConfiguration();
                    }
                    break;
            }

            BindDataList();
            if (iFlg == 0)
            {
                msgs.InformationalMessage("Configuration settings have been saved successfully");
            }
                mpeMessage.Show();
       
        }
        catch
        {
        }
    }
    private void BindDataList()
    {
        objConfiguration = new clsConfiguration();

        DataSet ds = objConfiguration.FillDataset();

        dlConfigurationSettings.DataSource = ds;
        dlConfigurationSettings.DataBind();
    }
    // Modified By Lijo Johns M on 22-11-12
    public string Naming(object oName)
    {
        string sName = oName.ToString();
        if (sName == "DefaultProbationPeriod")
            return "Default Probation Period";
        else if (sName == "WelcomeText")
            return "Welcome Text";
        else if (sName == "ReportFooter")
            return "Report Footer";
        else if (sName == "FromMail")
            return "From Mail";
        else if (sName == "MyAttendance")
            return "My Attendance";
        else if (sName == "CandidateCodePrefix")
            return "Candidate Code Prefix";
        else if (sName == "WeekStartDay")
            return "Week Start Day";
        else if (sName == "Defaultperformancereview")
            return "Default Performance Review";
        else
            return sName;


    }
    public bool IsEnabled(object oPredefined)
    {
        return (Convert.ToBoolean(oPredefined) == true ? false : true);
    }
    public bool IsNumeric(object oDefaultValue)
    {
        try
        {
            bool val = System.Text.RegularExpressions.Regex.IsMatch(oDefaultValue.ToString(),@"\d");     
            //int value = Convert.ToInt32(oDefaultValue);
            return val;
        }
        catch
        {
            return false;
        }
    }
    protected void dlConfigurationSettings_SelectedIndexChanged(object sender, EventArgs e)
    {
      
    }

}
