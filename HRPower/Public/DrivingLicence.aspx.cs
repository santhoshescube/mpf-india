﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Data.SqlClient;

/*******************************************
    *  Created By    : Siny
    *  Created Date  : 21 Oct 2013
    *  Purpose       : Add/update/delete driving license
    ********************************************/
public partial class Public_DrivingLicence : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsDrivingLicence objDrivingLicense;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button

    private string CurrentSelectedValue
    {
        get;
        set;
    }

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterAutoComplete();
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();

        if (!IsPostBack)
        {
            objDrivingLicense = new clsDrivingLicence();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            CurrentSelectedValue = "-1";
            BindCombos();
            SetPermission();
            EnableMenus();
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            //pgrEmployees.Visible = false;
           // lblNoData.Visible = false;

            txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

           // btnList_Click(sender, e);
        }

        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        rnLicenseType.OnSave += new controls_ReferenceControlNew.saveHandler(rnLicenseType_OnSave);

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
    }

    #region Issue Receipt
    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>" + Result + "</h5>";
        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        //lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        //lblCurrentStatus.Text = "Issued";
        //    }

        //    upMenu.Update();
        //}
    }

    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hfMode.Value == "Edit")
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            int intOperationTypeID = (int)OperationType.Employee, intDocType;
            
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdLicenseID.Value.ToInt32();
            intDocType = (int)DocumentType.Driving_License;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtLicenseNumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            btnList_Click(sender, e);
        }
        else
            return;
    }
    #endregion Issue Receipt

    public void FillComboCountry()
    {
        if (rcCountryOfOrigin != null && updCountryOfOrigin != null)
        {
            rcCountryOfOrigin.DataSource = new clsDrivingLicence().FillCountry();
            rcCountryOfOrigin.DataBind();

            rcCountryOfOrigin.DataTextField = "CountryName";
            rcCountryOfOrigin.DataValueField = "CountryId";            
            rcCountryOfOrigin.DataBind();
            rcCountryOfOrigin.SelectedValue = CurrentSelectedValue;
            updCountryOfOrigin.Update();
        }
    }

    public void FillLicenseType()
    {
        if (ddlLicenseType != null && upnlLicenseType != null)
        {
            ddlLicenseType.DataSource = new clsDrivingLicence().FillLicenseType();
            ddlLicenseType.DataTextField = "LicenseType";
            ddlLicenseType.DataValueField = "LicenseTypeID";
            ddlLicenseType.DataBind();
            ddlLicenseType.SelectedValue = CurrentSelectedValue;
            upnlLicenseType.Update();
        }
    }

    protected void Bind()
    {
        objDrivingLicense = new clsDrivingLicence();
        objUser = new clsUserMaster();

        objDrivingLicense.PageIndex = pgrEmployees.CurrentPage + 1;
        objDrivingLicense.PageSize = pgrEmployees.PageSize;
        objDrivingLicense.SearchKey = txtSearch.Text;
        objDrivingLicense.UserId = objUser.GetUserId();
        objUser = new clsUserMaster();
        objDrivingLicense.CompanyID = objUser.GetCompanyId();
        pgrEmployees.Total = objDrivingLicense.GetCount();
        DataSet ds = objDrivingLicense.GetAllEmployeeDrivingLicenses();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlDrivingLicence.DataSource = ds;
            dlDrivingLicence.DataBind();
            pgrEmployees.Visible = true;
            lblNoData.Visible = false;
        }
        else
        {
            dlDrivingLicence.DataSource = null;
            dlDrivingLicence.DataBind();
            pgrEmployees.Visible = false;
            btnPrint.Enabled = btnEmail.Enabled = btnDelete.Enabled = false;
            btnPrint.OnClientClick = btnEmail.OnClientClick = btnDelete.OnClientClick = "return false;";
            lblNoData.Visible = true;

            if (txtSearch.Text == string.Empty)
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("يتم إضافة أية تراخيص القيادة بعد") : ("No Driving Licenses are added yet"); //"No Driving Licenses are added yet";
            else
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا توجد نتائج بحث وجد") : ("No Search results found"); //"No Search results found";
        }

        fvEmployees.Visible = false;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('Employee');");
        upnlNoData.Update();
    }

    protected void NewLicense()
    {
        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        divPassDoc.Style["display"] = "none";
        fvLicense.Visible = false;
        hdLicenseID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        pgrEmployees.Visible = false;
        objDrivingLicense = new clsDrivingLicence();
        fvEmployees.Visible = true;
        txtLicenseNumber.Text = string.Empty;
        txtLicenseName.Text = string.Empty;
        txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        objDrivingLicense.LicenseID = -1;
        objDrivingLicense.EmployeeID = -1;
        txtOtherInfo.Text = string.Empty;
       // DataTable dtvisadoc = objDrivingLicense.GetLicenseDocuments().Tables[0];
        dlVisaDoc.DataSource = null;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = null;
        txtDocname.Text = "";
        btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
        btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
        FillEmployees(0);
        FillComboCountry();
        FillLicenseType();
        dlDrivingLicence.DataSource = null;
        dlDrivingLicence.DataBind();
        upnlNoData.Update();
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        ControlEnableDisble(true);
        lblCurrentStatus.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("جديد") : ("New"); //"New";
        upMenu.Update();
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        else
            return string.Empty;
    }

    private void BindCombos()
    {
        //objEmployee = new clsEmployee();
        //objRoleSettings = new clsRoleSettings();
        //ddlCompany.DataSource = objEmployee.GetAllCompanies();
        //ddlCompany.DataBind();

        //if (ddlCompany.Items.Count == 0)
        //    ddlCompany.Items.Add(new ListItem("Select", "-1"));

        //ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);

        //if (!(ddlCompany.Enabled))
        //    ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

        FillEmployees(0);
        FillComboCountry();
        FillLicenseType();
    }

    private void FillEmployees(long lngEmployeeID)
    {
        objUser = new clsUserMaster();
        if (objDrivingLicense == null)
            objDrivingLicense = new clsDrivingLicence();
        objDrivingLicense.CompanyID = objUser.GetCompanyId();
        ddlEmployee.DataSource = objDrivingLicense.FillEmployees(lngEmployeeID);
        ddlEmployee.DataBind();

        string str = string.Empty;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem(str, "-1"));
    }


    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.DrivingLicense);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
        //DataTable dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.DrivingLicense);
        //ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];
        if (dtm.Rows.Count > 0)
        {
            lnkRenew.Enabled = btnNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            btnList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            btnEmail.Enabled = btnPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            btnDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dtm.Rows[0]["IsUpdate"].ToBoolean();
        }
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();
        //btnNew.Enabled = btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = btnList.Enabled = MblnUpdatePermission = true;

        //if (UserID > 3)
        //{
            if (btnList.Enabled)
            {
                lblNoData.Visible = false;
                txtSearch.ReadOnly = false;
                Bind();
            }
            else if (btnNew.Enabled)
            {
                lblNoData.Visible = false;
                txtSearch.ReadOnly = false;
                NewLicense();
            }
            else if(UserID > 3)
            {
                pgrEmployees.Visible = false;
                dlDrivingLicence.DataSource = null;
                dlDrivingLicence.DataBind();
                fvEmployees.Visible = false;
                lblNoData.Visible = true;
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("ليس لديك إذن ما يكفي لعرض هذه الصفحة.") : ("You dont have enough permission to view this page."); //"You dont have enough permission to view this page.";
                ImgSearch.Enabled = false;
                txtSearch.ReadOnly = true;
            }
       // }

        if (btnDelete.Enabled)
            btnDelete.OnClientClick = "return valDeleteDatalist('" + dlDrivingLicence.ClientID + "');";
        else
            btnDelete.OnClientClick = "return false;";

        if (btnPrint.Enabled)
            btnPrint.OnClientClick = "return valPrintEmailDatalist('" + dlDrivingLicence.ClientID + "');";
        else
            btnPrint.OnClientClick = "return false;";

        if (btnEmail.Enabled)
            btnEmail.OnClientClick = "return valPrintEmailDatalist('" + dlDrivingLicence.ClientID + "');";
        else
            btnEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlDrivingLicence.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.DrivingLicense , objUser.GetCompanyId());
    }

    protected void dlDrivingLicence_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        objDrivingLicense = new clsDrivingLicence();
        objUser = new clsUserMaster();

        switch (e.CommandName)
        {
            case "ALTER":
                hfMode.Value = "Edit";
                objDrivingLicense.LicenseID = Convert.ToInt32(e.CommandArgument);
                hdLicenseID.Value = Convert.ToString(e.CommandArgument);
                btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
                btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
                BindFormView(Convert.ToInt32(e.CommandArgument));
                txtSearch.Text = string.Empty;
                upnlIssueReceipt.Update();
                upEmployeeVisa.Update();
                break;
            case "VIEW":
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                divIssueReceipt.Style["display"] = "none";
                divRenew.Style["display"] = "none";
                break;
        }
    }

    private void BindViewMode(int LicenseID)
    {
        dlDrivingLicence.DataSource = null;
        dlDrivingLicence.DataBind();
        pgrEmployees.Visible = false;
        fvEmployees.Visible = false;
        fvLicense.Visible = true;
        hfMode.Value = "List";
        objDrivingLicense.LicenseID = LicenseID;
        hdLicenseID.Value = Convert.ToString(LicenseID);
        fvLicense.DataSource = objDrivingLicense.GetEmpDrivingLicense();
        fvLicense.DataBind();
        divPrint.InnerHtml = GetPrintText(LicenseID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if(btnPrint.Enabled )
            btnPrint.OnClientClick = sPrinttbl;
            if(btnEmail.Enabled )
            btnEmail.OnClientClick = "return showMailDialog('Type=DrivingLicense&LicenseID=" + Convert.ToString(fvLicense.DataKey["LicenseID"]) + "');";
            if(btnDelete.Enabled )
            btnDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');"; //"return confirm('Are you sure to delete the selected items?')";
           if(lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return true;";
        }
    }

    private void BindFormView(int LicenseID)
    {
        fvEmployees.Visible = true;
        objDrivingLicense.LicenseID = LicenseID;
        DataTable dt = objDrivingLicense.GetEmployeeLicense().Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrEmployees.Visible = false;
        BindLicenseDetails(dt);
        dlDrivingLicence.DataSource = null;
        dlDrivingLicence.DataBind();
        divPrint.InnerHtml = GetPrintText(LicenseID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            btnPrint.OnClientClick = sPrinttbl;
            btnEmail.OnClientClick = "return showMailDialog('Type=DrivingLicense&LicenseID=" + Convert.ToString(fvLicense.DataKey["LicenseID"]) + "');";
            btnDelete.OnClientClick = "return true;";
            lnkRenew.OnClientClick = "return true;";
        }

        if (Convert.ToInt32(dt.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";

        if (Convert.ToBoolean(dt.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            divRenew.Style["display"] = "none";
            lblCurrentStatus.Text += clsUserMaster.GetCulture() == "ar-AE" ? ("[متجدد]") : ("[Renewed]"); //" [Renewed]";
            ControlEnableDisble(false);
        }
        else
        {
            divRenew.Style["display"] = "block";
            ControlEnableDisble(true);
        }

        upMenu.Update();
    }

    private void BindLicenseDetails(DataTable dt)
    {
        upMenu.Update();

        if (dt.Rows.Count > 0)
        {
           // ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            FillEmployees(dt.Rows[0]["EmployeeID"].ToInt64());
            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dt.Rows[0]["EmployeeID"])));

            if (dt.Rows[0]["LicencedCountryID"] != DBNull.Value)
                rcCountryOfOrigin.SelectedValue = Convert.ToString(dt.Rows[0]["LicencedCountryID"]);

            txtLicenseNumber.Text = Convert.ToString(dt.Rows[0]["LicenceNumber"]);
            txtLicenseName.Text = Convert.ToString(dt.Rows[0]["LicenceHoldersName"]);
            ddlLicenseType.SelectedValue = Convert.ToString(dt.Rows[0]["LicenceTypeID"]);
            txtIssuedate.Text = Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtExpiryDate.Text = Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtOtherInfo.Text = dt.Rows[0]["Remarks"].ToStringCustom();
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToStringCustom();
            objDrivingLicense.LicenseID = Convert.ToInt32(dt.Rows[0]["LicenseID"]);
            objDrivingLicense.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"]);
            DataTable dtvisadoc = objDrivingLicense.GetLicenseDocuments().Tables[0];

            dlVisaDoc.DataSource = dtvisadoc;
            dlVisaDoc.DataBind();
            ViewState["PasDoc"] = dtvisadoc;

            if (dlVisaDoc.Items.Count > 0)
                divPassDoc.Style["display"] = "block";
        }
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        ddlEmployee.Enabled = rcCountryOfOrigin.Enabled = btnCountry.Enabled = blnTemp;//ddlCompany.Enabled = 
        txtLicenseNumber.Enabled = blnTemp;
        txtIssuedate.Enabled = txtExpiryDate.Enabled = txtOtherInfo.Enabled = btnIssuedate.Enabled=btnExpiryDate.Enabled = blnTemp;
        txtLicenseName.Enabled = ddlLicenseType.Enabled =btnLicenceType.Enabled = blnTemp;
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objDrivingLicense = new clsDrivingLicence();
        bool blnRenewed = false;
        CheckBox chkEmployee;
        LinkButton btnEmployee;
        HiddenField hdIsRenewed;
        HtmlGenericControl divIsRenewed;
        string message = string.Empty;
        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

        if (dlDrivingLicence.Items.Count > 0)
        {
            foreach (DataListItem item in dlDrivingLicence.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");
                hdIsRenewed = (HiddenField)item.FindControl("hdIsRenewed");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                if (hdIsRenewed.Value.Trim().ToUpper() == "RENEWED" || objDrivingLicense.CheckIfDocumentIssued(Convert.ToInt32(dlDrivingLicence.DataKeys[item.ItemIndex])))
                {
                    blnRenewed = true;
                }
                else
                {
                    objDrivingLicense.LicenseID = Convert.ToInt32(dlDrivingLicence.DataKeys[item.ItemIndex]);

                    DataTable dt = objDrivingLicense.GetLicenseFilenames();
                    objDrivingLicense.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                }
               
            }
            if (!blnRenewed)
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("رخصة القيادة (ق) حذف بنجاح") : ("Driving License(s) deleted successfully"); //"Driving License(s) deleted successfully";
            else
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تجديد رخصة القيادة / تصدر مرة واحدة (ق) لا يمكن حذف") : ("Renewed/Once issued Driving License(s) cannot be deleted"); //"Renewed/Once issued Driving License(s) cannot be deleted";
        }
        else
        {
            if (hfMode.Value == "List")
            {
                divIsRenewed = (HtmlGenericControl)fvLicense.FindControl("divIsRenewed");
                if (divIsRenewed.InnerHtml.Trim().ToUpper() == "YES" || objDrivingLicense.CheckIfDocumentIssued(Convert.ToInt32(fvLicense.DataKey["LicenseID"])))
                {
                   blnRenewed = true;
                }
                else
                {
                     objDrivingLicense.LicenseID = Convert.ToInt32(fvLicense.DataKey["LicenseID"]);
                    DataTable dt = objDrivingLicense.GetLicenseFilenames();
                    objDrivingLicense.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                }
                    
                if (!blnRenewed)
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("رخصة القيادة (ق) حذف بنجاح") : ("Driving License(s) deleted successfully"); //"Driving License(s) deleted successfully";
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تجديد رخصة القيادة / تصدر مرة واحدة (ق) لا يمكن حذف") : ("Renewed/Once issued Driving License(s) cannot be deleted"); //"Renewed/Once issued Driving License(s) cannot be deleted";
            }
        }

        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        btnList_Click(sender, e);
        upEmployeeVisa.Update();
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        txtSearch.Text = string.Empty;
        pgrEmployees.CurrentPage = 0;
        fvLicense.Visible = false;
        fvLicense.DataSource = null;
        fvLicense.DataBind();
        EnableMenus();
        Bind();
        hfMode.Value = "List";
        upEmployeeVisa.Update();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnList_Click(sender, e);
        upMenu.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int id = 0;
        string msg = string.Empty;
        try
        {
            objDrivingLicense = new clsDrivingLicence();
            objAlert = new clsDocumentAlert();

            objDrivingLicense.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objDrivingLicense.LicenceNumber = Convert.ToString(txtLicenseNumber.Text.Trim());
            objDrivingLicense.LicenceHoldersName = Convert.ToString(txtLicenseName.Text.Trim());
            objDrivingLicense.LicenceTypeID = ddlLicenseType.SelectedValue.ToInt32();
            objDrivingLicense.IssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text);
            objDrivingLicense.ExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text);
            objDrivingLicense.LicencedCountryID = Convert.ToInt32(rcCountryOfOrigin.SelectedValue);
            objDrivingLicense.Remarks = txtOtherInfo.Text.Trim();

            if (hfMode.Value == "Insert")
            {
                objDrivingLicense.LicenseID = 0;

                if (objDrivingLicense.IsLicenceNumberExists())
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("القيادة رقم الترخيص موجود") : ("Driving License Number exists");
                    mcMessage.InformationalMessage(msg);
                    //mcMessage.InformationalMessage("Driving License Number exists");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objDrivingLicense.BeginEmp();
                        id = objDrivingLicense.InsertEmployeeLicense();
                        objDrivingLicense.LicenseID = id;
                        objAlert.AlertMessage(Convert.ToInt32(DocumentType.Driving_License), "DrivingLicense", "LicenseNumber", id, txtLicenseNumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Emp", Convert.ToInt32(ddlEmployee.SelectedValue), ddlEmployee.SelectedItem.Text, false);
                        objDrivingLicense.Commit();
                    }
                    catch (Exception ex)
                    {
                        objDrivingLicense.RollBack();
                    }
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objDrivingLicense.LicenseID = Convert.ToInt32(hdLicenseID.Value);

                if (objDrivingLicense.IsLicenceNumberExists())
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("القيادة رقم الترخيص موجود") : ("Driving License Number exists");
                    mcMessage.InformationalMessage(msg);
                    //mcMessage.InformationalMessage("Driving License Number exists");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objDrivingLicense.BeginEmp();
                        id = objDrivingLicense.UpdateEmployeeLicense();
                        objDrivingLicense.LicenseID = id;
                        objDrivingLicense.Commit();
                    }
                    catch (Exception ex)
                    {
                        objDrivingLicense.RollBack();
                    }
                }
            }

            if (id == 0) return;
            //insert into treemaster and treedetails
            Label lblDocname, lblActualfilename, lblFilename;

            foreach (DataListItem item in dlVisaDoc.Items)
            {
                lblDocname = (Label)item.FindControl("lblDocname");
                lblFilename = (Label)item.FindControl("lblFilename");
                lblActualfilename = (Label)item.FindControl("lblActualfilename");
                objDrivingLicense.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objDrivingLicense.LicenseID = id;
                objDrivingLicense.LicenceNumber = Convert.ToString(txtLicenseNumber.Text.Trim());
                objDrivingLicense.Docname = lblDocname.Text.Trim();
                objDrivingLicense.Filename = lblFilename.Text.Trim();

                if (dlVisaDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                {
                    objDrivingLicense.InsertEmployeeLicenseTreemaster();

                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                    {
                        if (!Directory.Exists(Path))
                            Directory.CreateDirectory(Path);

                        File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                    }
                }
            }

            objDrivingLicense.Commit();

            if (hfMode.Value == "Insert")
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم حفظ يقود التفاصيل الترخيص بنجاح") : ("Driving License details has been saved successfully");
                mcMessage.InformationalMessage(msg);
                //mcMessage.InformationalMessage("Driving License details has been saved successfully");
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم حفظ يقود التفاصيل الترخيص بنجاح") : ("Driving License details has been updated successfully");
                mcMessage.InformationalMessage(msg);
                //mcMessage.InformationalMessage("Driving License details has been updated successfully");
            }

            mpeMessage.Show();
            upEmployeeVisa.Update();
            EnableMenus();
            divIssueReceipt.Style["display"] = "block";
            divRenew.Style["display"] = "block";
            hfMode.Value = "Edit";
            hdLicenseID.Value = objDrivingLicense.LicenseID.ToStringCustom();
            upMenu.Update();
            upEmployeeVisa.Update();
            btnList_Click(sender, e);
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfMode.Value == "Edit")
            {
                int id = 0;
                objDrivingLicense = new clsDrivingLicence();
                objAlert = new clsDocumentAlert();
                objDrivingLicense.LicenseID = hdLicenseID.Value.ToInt32();
                try
                {
                    objDrivingLicense.BeginEmp();
                    id = objDrivingLicense.UpdateRenewEmployeeLicense();
                    objDrivingLicense.Commit();
                }
                catch (Exception ex)
                {
                    objDrivingLicense.RollBack();
                }

                objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.Driving_License), id);
                string msg = string.Empty;
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("القيادة التفاصيل الترخيص تجدد بنجاح") : ("Driving License details renewed successfully");
                mcMessage.InformationalMessage(msg);
                //mcMessage.InformationalMessage("Driving License details renewed successfully");
                mpeMessage.Show();
                divIssueReceipt.Style["display"] = "block";
                divRenew.Style["display"] = "block";
                hfMode.Value = "Edit";
                hdLicenseID.Value = objDrivingLicense.LicenseID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                upEmployeeVisa.Update();
                btnList_Click(sender, e);
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {        
        divRenew.Style["display"] = "none";
        pgrEmployees.CurrentPage = 0;
        divIssueReceipt.Style["display"] = "none";
        fvLicense.Visible = false;
        fvLicense.DataSource = null;
        fvLicense.DataBind();
        EnableMenus();
        Bind();
        hfMode.Value = "List";
        upEmployeeVisa.Update();
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        NewLicense();
        upEmployeeVisa.Update();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlDrivingLicence.Items.Count > 0)
        {
            objDrivingLicense = new clsDrivingLicence();

            if (fvLicense.DataKey["LicenseID"] == DBNull.Value || fvLicense.DataKey["LicenseID"] == null)
            {
                CheckBox chkEmployee;
                string sLicenseIDs = string.Empty;

                foreach (DataListItem item in dlDrivingLicence.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sLicenseIDs += "," + dlDrivingLicence.DataKeys[item.ItemIndex];
                }

                sLicenseIDs = sLicenseIDs.Remove(0, 1);

                if (sLicenseIDs.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sLicenseIDs);
                else
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sLicenseIDs));
            }
            else
                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvLicense.DataKey["LicenseID"]));

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }
    }

    public string GetPrintText(int iVisaID)
    {
        StringBuilder sb = new StringBuilder();

        DataTable dt = objDrivingLicense.GetPrintText(iVisaID);// ExecuteDataSet("HRspDrivingLicence", alParameters).Tables[0];


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("DrivingLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("DrivingLicenseNumber.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicenseType.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenseType"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicensedCountry.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td><td>:</td><td style='word-break:break-all'>" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    public string CreateSelectedEmployeesContent(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
        DataTable dt = objDrivingLicense.CreateSelectedEmployeesPrintContent(sVisaIds);  
        
        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("DrivingLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee") + "</td><td width='150px'>" + GetLocalResourceObject("DrivingLicenseNumber.Text") + "</td><td width='150px'>" + GetLocalResourceObject("LicenseType.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td width='100px'>" + GetLocalResourceObject("LicensedCountry.Text") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td></tr>");
        sb.Append("<tr><td colspan='9'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenseType"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style='word-break:break-all'>" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        if (dlDrivingLicence.Items.Count > 0)
        {
            objDrivingLicense = new clsDrivingLicence();
            string sLicenseIDs = string.Empty;

            if (fvLicense.DataKey["LicenseID"] == DBNull.Value || fvLicense.DataKey["LicenseID"] == null)
            {
                CheckBox chkEmployee;

                foreach (DataListItem item in dlDrivingLicence.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sLicenseIDs += "," + dlDrivingLicence.DataKeys[item.ItemIndex];
                }

                sLicenseIDs = sLicenseIDs.Remove(0, 1);
            }
            else
                sLicenseIDs = fvLicense.DataKey["LicenseID"].ToString();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=DrivingLicense&LicenseID=" + sLicenseIDs + "', 835, 585);", true);
        }
    }

    protected void fuVisa_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuVisa.HasFile)
        {
            actualfilename = fuVisa.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuVisa.FileName);
            Session["Filename"] = filename;
            fuVisa.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }

    protected void btnattachvisadoc_Click(object sender, EventArgs e)
    {
        divPassDoc.Style["display"] = "block";
        DataTable dt;
        DataRow dw;
        objDrivingLicense = new clsDrivingLicence();
        objDrivingLicense.LicenseID = -1;
        objDrivingLicense.EmployeeID = -1;

        if (ViewState["PasDoc"] == null)
            dt = objDrivingLicense.GetLicenseDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["PasDoc"];
            dt = dtDoc;
        }

        if (hfMode.Value == "Insert" || hfMode.Value == "Edit")
        {
            dw = dt.NewRow();
            dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
            dw["Filename"] = Session["Filename"];
            dt.Rows.Add(dw);
        }

        dlVisaDoc.DataSource = dt;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = dt;
        txtDocname.Text = string.Empty;
        updldlPassportDoc.Update();
    }

    protected void dlVisaDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objDrivingLicense = new clsDrivingLicence();

                if (e.CommandArgument != string.Empty)
                {
                    objDrivingLicense.Node = Convert.ToInt32(e.CommandArgument);
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objDrivingLicense.DeleteLicenseDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }

                objDrivingLicense.LicenseID = -1;
                objDrivingLicense.EmployeeID = -1;
                DataTable dt = objDrivingLicense.GetLicenseDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlVisaDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    dw["Node"] = dlVisaDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlVisaDoc.DataSource = dt;
                dlVisaDoc.DataBind();
                ViewState["PasDoc"] = dt;
                break;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        //clsDrivingLicence objDrivingLicense = new clsDrivingLicence();
        //FillEmployees(0);
    }

    protected void btnCountry_Click(object sender, EventArgs e)
    {
        ReferenceControlNew1.ClearAll();
        ReferenceControlNew1.TableName = "CountryReference";
        ReferenceControlNew1.DataTextField = "CountryName";
        ReferenceControlNew1.DataValueField = "CountryId";
        ReferenceControlNew1.FunctionName = "FillComboCountry";
        ReferenceControlNew1.SelectedValue = rcCountryOfOrigin.SelectedValue;
        ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
        ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
        ReferenceControlNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    protected void btnLicenceType_Click(object sender, EventArgs e)
    {
        rnLicenseType.ClearAll();
        rnLicenseType.TableName = "LicenseTypeReference";
        rnLicenseType.DataTextField = "LicenseType";
        rnLicenseType.DataValueField = "LicenseTypeID";
        rnLicenseType.FunctionName = "FillLicenseType";
        rnLicenseType.PredefinedField = "Predefined";
        rnLicenseType.SelectedValue = ddlLicenseType.SelectedValue;
        rnLicenseType.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("نوع الترخيص") : ("License Type"); 
        rnLicenseType.DataTextFieldArabic = "LicenseTypeArb";
        rnLicenseType.PopulateData();
        mpeLicenceType.Show();
        upnlPopup.Update();
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void rnLicenseType_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }


    protected void dlDrivingLicence_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }
}
