﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_ViewPerformanceAction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("ViewPerformanceAction.Text").ToString();
        if (!IsPostBack)
        {
            SetPermission();
            GetAllActions();
        }

        EnableDisableMenus();
    }


    private void EnableDisableMenus()
    {
        if (ViewState["IsView"] != null && Convert.ToBoolean(ViewState["IsView"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }

        lnkAdd.Enabled = Convert.ToBoolean(ViewState["IsCreate"]);
        lnkViewInitiation.Enabled = Convert.ToBoolean(ViewState["IsView"]);

    }


    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtm = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.PerformanceAction);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = true;
        }

    }
    private void GetAllActions()
    {
        dlEmployeeList.DataSource = clsPerformanceInitiation.GetAllActions();
        dlEmployeeList.DataBind();

        if (((DataTable)dlEmployeeList.DataSource).Rows.Count == 0)
        {
            dvEmployeeList.Visible =  false;//header.Visible= 
            dvNoRecord.Visible = true;
        }
        else
        {
            dvEmployeeList.Visible  = true;//= header.Visible
            dvNoRecord.Visible = false;
        }
    }
    protected void btnEvaluate_Click(object sender, EventArgs e)
    {
    }
    protected void dlEmployeeList_ItemCommand(object source, DataListCommandEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlGenericControl dvInnerDiv = (HtmlGenericControl)e.Item.FindControl("divShowDetails");
            if (dvInnerDiv != null)
            {
                string sTimeDelay = "var t = setTimeout(\"SlideUpSlideDown('" + dvInnerDiv.ClientID + "', 500)\",50)";
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "toggle", sTimeDelay, true);
            }


        }
    }
    protected void dlEmployeeList_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {

            LinkButton lnkShowDetails = (LinkButton)e.Item.FindControl("lnkShowDetails");
            HtmlGenericControl dvInnerDiv = (HtmlGenericControl)e.Item.FindControl("divShowDetails");
            HiddenField hfIsActionDone = (HiddenField)e.Item.FindControl("hfIsActionDone");
            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");

            if (lnkShowDetails != null && dvInnerDiv != null)
            {
                lnkShowDetails.OnClientClick = "return toggleTabDiv('" + dvInnerDiv.ClientID + "')";
            }

            //    //Action done
            if (hfIsActionDone != null && imgEdit != null && Convert.ToBoolean(hfIsActionDone.Value))
            {
                imgEdit.ImageUrl = "~/images/edit_disable.png";
                imgEdit.Enabled = false;
            }
            else
            {
                imgEdit.ImageUrl = "~/images/edit.png";
                imgEdit.Enabled = Convert.ToBoolean(ViewState["IsUpdate"]);
            }
           
        }
    }
    protected void btnEvaluate_Click1(object sender, EventArgs e)
    {





    }
    protected void dlGoal_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //HiddenField hfGradeID =(HiddenField) e.Item.FindControl("hfGradeID");
        //HiddenField hfMark = (HiddenField)e.Item.FindControl("hfMark");

        //if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        //{
        //    DropDownList ddlMark = ((DropDownList)e.Item.FindControl("ddlMark"));
        //    if (ddlMark != null)
        //    {
        //        DataTable dtMarks = ((DataTable)ViewState["dtMarks"]);
        //        ddlMark.DataSource = dtMarks;
        //        ddlMark.DataTextField = "Mark";
        //        ddlMark.DataValueField = "MarkID";
        //        ddlMark.DataBind();


        //        if (Convert.ToBoolean(hfIsGrade.Value) == true)
        //        {
        //            ddlMark.SelectedValue = hfGradeID.Value.ToString();
        //        }
        //        else
        //            ddlMark.SelectedValue = hfMark.Value.ToString();
        //    }

        //}
    }
    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {

    }
    protected void save_Click(object sender, EventArgs e)
    {
        //    clsPerformanceEvaluation objPeformanceEvaluation = new clsPerformanceEvaluation();
        //    objPeformanceEvaluation.EmployeeID = hfEmployeeID.Value.ToInt32();
        //    objPeformanceEvaluation.EvaluatorID = new clsUserMaster().GetEmployeeId();
        //    objPeformanceEvaluation.PeformanceInitiationID = hfPerformanceInitiationID.Value.ToInt32();
        //    objPeformanceEvaluation.IsGrade = Convert.ToBoolean(hfIsGrade.Value);
        //    objPeformanceEvaluation.PerformanceInitiationEvaluationID = hfPerformanceInitiationEvaluationID.Value.ToInt32();
        //    objPeformanceEvaluation.TemplateID = hfTemplateID.Value.ToInt32();

        //    System.Collections.Generic.List<clsGoalDetails> GoalList = new System.Collections.Generic.List<clsGoalDetails>();
        //    objPeformanceEvaluation.EvaluationDetailsID = dlGoal.DataKeys[0].ToInt32();


        //    foreach (DataListItem dl in dlGoal.Items)
        //    {
        //        if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
        //        {
        //            HiddenField hfGoalID = (HiddenField)dl.FindControl("hfGoalID");
        //            DropDownList ddlMark = (DropDownList)dl.FindControl("ddlMark");

        //            if (hfGoalID != null && ddlMark != null)
        //            {
        //                GoalList.Add(new clsGoalDetails()
        //                {
        //                    GoalID = hfGoalID.Value.ToInt32(),
        //                    GradeID = ddlMark.SelectedValue.ToInt32(),
        //                    Mark = ddlMark.SelectedValue.ToDecimal(),
        //                });
        //            }
        //        }
        //    }
        //    objPeformanceEvaluation.GoalList = GoalList;
        //    if (objPeformanceEvaluation.SavePerformanceEvaluation())
        //    {
        //        ScriptManager.RegisterClientScriptBlock(updGoal, updGoal.GetType(), new Guid().ToString(), "alert('Result saved successfully');", true);
        //        GetAllCompletedEvaluations();
        //    }

    }
    protected void lnkCompletedEvaluation_Click(object sender, EventArgs e)
    {
       
    }
    protected void lnkPendingEvaluations_Click(object sender, EventArgs e)
    {
      
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/PerformanceAction.aspx");
    }
    protected void lnkViewInitiation_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/public/ViewPerformanceAction.aspx");
    }

    protected void dlEmployeeList_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void imgEdit_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/public/Action.aspx?" + string.Format("PerformanceActionID={0}", ((ImageButton)sender).CommandArgument.ToInt32()));

    }
}
