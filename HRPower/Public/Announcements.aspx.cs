﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Public_Announcements : System.Web.UI.Page
{
    clsAnnouncements objAnnouncement;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;

    List<EmployeeTemp> EmployeeList;
    clsMessage objMessage;
    string message = "";

    private bool MblnAddPermission ;  // 
    private bool MblnUpdatePermission ;  // 
    private bool MblnDeletePermission ;  // 
    private bool MblnViewPermission ;  // 
    private bool MblnPrintEmailPermission ;  // 



    protected void Page_Load(object sender, EventArgs e)
    {
       

        AnnouncementPager.Fill += new controls_Pager.FillPager(BindAnnouncements);
        if (!IsPostBack)
        {
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            SetPermission();
            EnableMenus();

            if (MblnViewPermission==true)
            {
                BindAnnouncements();
            }
            else if (MblnAddPermission==true)
            {
                AddAnnouncements();
            }
            else
            {
                AnnouncementPager.Visible = false;
                lblNoData.Style["Display"] = "block";
                lblNoData.Text = "You dont have enough permission to view this page.";
            }

        }
    }


    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
        {
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.announcementsHR);
        }
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }


    public void EnableMenus()
    {

        DataTable dtm = (DataTable)ViewState["Permission"];
        if (dtm.Rows.Count > 0)
        {
            lnkAdd.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean(); 
            lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();

            if (lnkAdd.Enabled == false)
            {
                lnkAdd.OnClientClick = "return false;";
            }

            if (lnkAdd.Enabled == false)
            {
                lnkAdd.OnClientClick = "return false;";
            }
            MblnAddPermission = dtm.Rows[0]["IsCreate"].ToBoolean();
            MblnViewPermission = dtm.Rows[0]["IsView"].ToBoolean();
            MblnDeletePermission = dtm.Rows[0]["IsDelete"].ToBoolean(); 

        }
        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + gvAnnouncements.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";
    }

    protected void fvAnnouncements_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        try
        {
            objUser=new clsUserMaster();
            int RoleId;
            RoleId = objUser.GetRoleId();

            int iAnnouncementId = 0;
            switch (e.CommandName)
            {

                case "Add":
                    if (Convert.ToString(e.CommandArgument) != "")
                    {
                        iAnnouncementId = Convert.ToInt32(e.CommandArgument);
                    }
                    if (RoleId <= 3)
                    {
                        AddUpdateAnnouncement(iAnnouncementId);
                    }
                    else
                    {
                        DataTable dtm = (DataTable)ViewState["Permission"];
                        if (dtm.Rows.Count > 0)
                        {
                            if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                            {
                                if (iAnnouncementId.ToInt32() <= 0)
                                {
                                    AddUpdateAnnouncement(iAnnouncementId);
                                }
                            }
                            //else if (dtm.Rows[0]["IsUpdate"].ToBoolean())
                            //{
                            //    AddUpdateAnnouncement(iAnnouncementId);
                            //}
                        }
                    }
                    break;
                case "CancelRequest":
                    upForm.Update();
                    upDetails.Update();
                    BindAnnouncements();
                    break;
            }
        }
        catch
        {
        }
    }

    protected void fvAnnouncements_DataBound(object sender, EventArgs e)
    {
        if (fvAnnouncements.CurrentMode == FormViewMode.Insert | fvAnnouncements.CurrentMode == FormViewMode.Edit)
        {
            objAnnouncement = new clsAnnouncements();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            DropDownList ddlCompany = (DropDownList)fvAnnouncements.FindControl("ddlCompany");
            objAnnouncement.CompanyId = objUser.GetCompanyId();
            ddlCompany.DataSource = objAnnouncement.GetCompanies();
            ddlCompany.DataBind();

           // ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

            RadioButton rbtnGeneral, rbtnDepartmentWise, rbtnEmployeeWise;
            rbtnGeneral = this.fvAnnouncements.FindControl("rbtnGeneral") as RadioButton;
            rbtnEmployeeWise = this.fvAnnouncements.FindControl("rbtnEmployeeWise") as RadioButton;
            rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
            if (fvAnnouncements.CurrentMode == FormViewMode.Insert)
            {
                TextBox txtFromDate = (TextBox)this.fvAnnouncements.FindControl("txtFromDate");
                TextBox txtToDate = (TextBox)this.fvAnnouncements.FindControl("txtToDate");
                txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }

            

            if (fvAnnouncements.CurrentMode == FormViewMode.Edit)
            {
                ddlCompany.Enabled = false;
                ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(fvAnnouncements.DataKey["CompanyId"])));

                this.objAnnouncement.AnnouncementId = this.fvAnnouncements.DataKey["AnnouncementId"].ToInt32();
                this.objAnnouncement.CompanyId = this.fvAnnouncements.DataKey["CompanyId"].ToInt32();
                // bind announcement details
                using (DataSet ds = objAnnouncement.GetAnnouncementDetails())
                {
                    int intAnnouncementGroupID = this.fvAnnouncements.DataKey["AnnouncementGroupID"].ToInt32();
                    if (intAnnouncementGroupID > 0)
                    {
                        CheckBoxList chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
                        CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
                        HtmlGenericControl divEmployees = this.fvAnnouncements.FindControl("divEmployees") as HtmlGenericControl;

                        this.objAnnouncement.AnnouncementGroup = (eAnnouncementGroup)intAnnouncementGroupID;

                        if (this.objAnnouncement.AnnouncementGroup == eAnnouncementGroup.General)
                        {
                            if (divEmployees != null)
                                divEmployees.Visible = false;

                            rbtnGeneral.Checked = true;
                            rbtnEmployeeWise.Checked = rbtnDepartmentWise.Checked = false;
                        }
                        else
                        {
                            if (ds.Tables.Count > 2 && ds.Tables[2].Rows.Count > 0)
                            {
                                rptrSelectedEmployees.DataSource = ds.Tables[2];
                                rptrSelectedEmployees.DataBind();
                            }

                            if (this.objAnnouncement.AnnouncementGroup == eAnnouncementGroup.EmployeeWise)
                            {
                                rbtnEmployeeWise.Checked = true;
                                rbtnGeneral.Checked = rbtnDepartmentWise.Checked = false;

                                if (divEmployees != null)
                                    divEmployees.Visible = true;

                                this.BindEmployees(this.objAnnouncement.CompanyId, 0, chklstEmployees);

                                if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                                {
                                    foreach (DataRow dr in ds.Tables[1].Rows)
                                    {
                                        foreach (ListItem item in chklstEmployees.Items)
                                        {
                                            if (dr["EmployeeID"].ToInt32() == item.Value.ToInt32())
                                                item.Selected = true;
                                        }
                                    }
                                }
                            }
                            else if (this.objAnnouncement.AnnouncementGroup == eAnnouncementGroup.DepartmentWise)
                            {
                                rbtnDepartmentWise.Checked = true;
                                rbtnGeneral.Checked = rbtnEmployeeWise.Checked = false;

                                this.BindDepartments();

                                if (divEmployees != null)
                                    divEmployees.Visible = true;

                                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dr in ds.Tables[0].Rows)
                                    {
                                        foreach (ListItem item in chklstDepartments.Items)
                                        {
                                            if (dr["DepartmentID"].ToInt32() == item.Value.ToInt32())
                                                item.Selected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (rbtnDepartmentWise != null && rbtnEmployeeWise != null && rbtnGeneral != null)
                {
                    rbtnGeneral.Checked = true;
                    rbtnEmployeeWise.Checked = rbtnDepartmentWise.Checked = false;
                }

                HtmlGenericControl divEmployees = this.fvAnnouncements.FindControl("divEmployees") as HtmlGenericControl;
                if (divEmployees != null)
                    divEmployees.Visible = false;
            }
        }
    }

    protected void gvAnnouncements_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_Edit":

                fvAnnouncements.ChangeMode(FormViewMode.Edit);
                BindAnnouncementsById(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                AnnouncementPager.Visible = false;
                break;
        }
    }

    private void AddUpdateAnnouncement(int iAnnouncementsId)
    {
        string departmentsIds = "";
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        objMessage = new clsMessage();

        TextBox txtMessage = (TextBox)fvAnnouncements.FindControl("txtMessage");
        TextBox txtSDate = (TextBox)fvAnnouncements.FindControl("txtFromDate");
        TextBox txtEDate = (TextBox)fvAnnouncements.FindControl("txtToDate");
        DropDownList ddlCompany = (DropDownList)fvAnnouncements.FindControl("ddlCompany");
        RadioButton rbtnEmployeeWise = this.fvAnnouncements.FindControl("rbtnEmployeeWise") as RadioButton;
        RadioButton rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
        RadioButton rbtnGeneral = this.fvAnnouncements.FindControl("rbtnGeneral") as RadioButton;
        CheckBoxList chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
        CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;

        objAnnouncement = new clsAnnouncements();
        objAnnouncement.AnnouncementId = iAnnouncementsId;
        objAnnouncement.Message = txtMessage.Text;
        objAnnouncement.StartDate = clsCommon.Convert2DateTime(txtSDate.Text);
        objAnnouncement.EndDate = clsCommon.Convert2DateTime(txtEDate.Text);
        objAnnouncement.CompanyId = Convert.ToInt32(ddlCompany.SelectedValue);

        message = txtMessage.Text;
        if (!this.ValidateForm())
            return;

        if (rbtnDepartmentWise.Checked)
        {
            this.objAnnouncement.AnnouncementGroup = eAnnouncementGroup.DepartmentWise;

            // Add departments to the list
            foreach (ListItem item in chklstDepartments.Items)
            {
                if (item.Selected)
                {
                    departmentsIds = departmentsIds + item.Value;
                    this.objAnnouncement.AnnouncementDetails.DepartmentIDs.Add(item.Value.ToInt32());
                    departmentsIds = departmentsIds + ",";
                }
            }
            departmentsIds.Substring(0, departmentsIds.Length - 1);
            using (DataTable dt = objAnnouncement.GetEmployees(Convert.ToInt32(ddlCompany.SelectedValue), departmentsIds))
            {
                objMessage = new clsMessage();
                objMessage.MessageDetailList = new List<clsMessageDetails>();
                foreach (DataRow dr in dt.Rows)
                {
                    clsMessageDetails objClsMessageDetails = new clsMessageDetails();
                    objClsMessageDetails.RequestedTo = Convert.ToInt32(dr["EmployeeId"]);
                    objMessage.MessageDetailList.Add(objClsMessageDetails);
                }
            }
        }
        else if (rbtnEmployeeWise.Checked)
        {
            this.objAnnouncement.AnnouncementGroup = eAnnouncementGroup.EmployeeWise;

            // add employees to the list
            foreach (ListItem item in chklstEmployees.Items)
            {
                if (item.Selected)
                {
                    clsMessageDetails objClsMessageDetails = new clsMessageDetails();
                    objClsMessageDetails.RequestedTo = item.Value.ToInt32();
                    objMessage.MessageDetailList.Add(objClsMessageDetails);
                    this.objAnnouncement.AnnouncementDetails.EmployeeIDs.Add(item.Value.ToInt32());
                }
            }
        }
        else if (rbtnGeneral.Checked)
            this.objAnnouncement.AnnouncementGroup = eAnnouncementGroup.General;

        int announcementId = objAnnouncement.InserUpdateAnnouncements();


        if (lnkView.Enabled == true || lnkAdd.Enabled == true || lnkDelete.Enabled == true)
        {
            BindAnnouncements();
        }
        else
        {
            AddAnnouncements();
        }


        upForm.Update();
        upDetails.Update();

        fvAnnouncements.ChangeMode(FormViewMode.ReadOnly);
        fvAnnouncements.DataSource = null;
        fvAnnouncements.DataBind();
        if (iAnnouncementsId != 0)
        {
            objAnnouncement.DeleteAnnouncementMessage(iAnnouncementsId, Convert.ToInt32(eMessageType.Announcements));
            msgs.InformationalMessage("Announcement updated successfully");
        }
        else
        {
            msgs.InformationalMessage("Announcement saved successfully");
        }
        string MessageID = SaveAnnouncementMessage(announcementId, txtSDate.Text, txtEDate.Text,ddlCompany.SelectedValue.ToInt32());

        if (rbtnGeneral.Checked) // all employees
            this.objAnnouncement.InsertMessageDetails(MessageID, ddlCompany.SelectedValue.ToInt32());

        mpeMessage.Show();
    }
    private string SaveAnnouncementMessage(int announcementId, string StartDate, string EndDate , int CompanyID)
    {
        this.objMessage.Message = message;
        this.objMessage.MessageDate = Convert.ToDateTime(StartDate.Split('/')[1] + "/" + StartDate.Split('/')[0] + "/" + StartDate.Split('/')[2]);
        this.objMessage.MessageDueDate = Convert.ToDateTime(EndDate.Split('/')[1] + "/" + EndDate.Split('/')[0] + "/" + EndDate.Split('/')[2]);
        this.objMessage.MessageType = eMessageType.Announcements;//Primary Key
        this.objMessage.ReferenceID = announcementId;//Primary Key
        this.objMessage.ReferenceType = eReferenceType.Announcements;//Table Name
        this.objMessage.RequestedBy = new clsUserMaster().GetEmployeeId();
        this.objMessage.CompanyID = CompanyID;
        string strMessageID = this.objMessage.Insert();

        return strMessageID;
    }
    private void BindAnnouncements()
    {

        objAnnouncement = new clsAnnouncements();
        objUser = new clsUserMaster();
        objAnnouncement.PageIndex = AnnouncementPager.CurrentPage + 1;
        objAnnouncement.PageSize = AnnouncementPager.PageSize;

        objAnnouncement.UserId = objUser.GetUserId();
        objAnnouncement.CompanyId = objUser.GetCompanyId();
        DataTable dtTable = objAnnouncement.GetAnnouncements();

        if (dtTable.DefaultView.Count > 0)
        {
            gvAnnouncements.DataSource = dtTable;
            gvAnnouncements.DataBind();
            AnnouncementPager.Total = objAnnouncement.GetRecordCount();
            EnableMenus();

            fvAnnouncements.ChangeMode(FormViewMode.ReadOnly);
            fvAnnouncements.DataSource = null;
            fvAnnouncements.DataBind();
            lblNoData.Visible = false;
            AnnouncementPager.Visible = true;
        }
        else
        {
            lblNoData.Text = "No announcements are added yet.";
            lblNoData.Visible = true;
            gvAnnouncements.DataSource = null;
            gvAnnouncements.DataBind();
            AnnouncementPager.Visible = false;
        }
    }

    private void BindAnnouncementsById(int iAnnouncementId)
    {
        objAnnouncement = new clsAnnouncements();
        objAnnouncement.AnnouncementId = iAnnouncementId;

        DataTable dtTable = objAnnouncement.GetAnnouncements();
        if (dtTable.DefaultView.Count > 0)
        {
            gvAnnouncements.DataSource = null;
            gvAnnouncements.DataBind();

            fvAnnouncements.ChangeMode(FormViewMode.Edit);
            fvAnnouncements.DataSource = dtTable;
            fvAnnouncements.DataBind();
        }


    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        AddAnnouncements();
    }

    private void AddAnnouncements()
    {
        lblNoData.Text = string.Empty;
        fvAnnouncements.ChangeMode(FormViewMode.Insert);

        gvAnnouncements.DataSource = null;
        gvAnnouncements.DataBind();

        lnkDelete.Enabled = false;

        lnkDelete.OnClientClick = "return false;";
        AnnouncementPager.Visible = false;

        upForm.Update();
        upDetails.Update();

    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        BindAnnouncements();

        upForm.Update();
        upDetails.Update();

    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objAnnouncement = new clsAnnouncements();

        if (gvAnnouncements.Rows.Count > 0)
        {
            foreach (GridViewRow gr in gvAnnouncements.Rows)
            {
                CheckBox chkAnou = (CheckBox)gr.FindControl("chkAnnouncements");
                if (chkAnou == null)
                    continue;
                if (chkAnou.Checked)
                {
                    int announcementId = Convert.ToInt32(gvAnnouncements.DataKeys[gr.RowIndex].Value);
                    objAnnouncement.AnnouncementId = announcementId;
                    objAnnouncement.DeleteAnnouncements();
                    objAnnouncement.DeleteAnnouncementMessage(announcementId, Convert.ToInt32(eMessageType.Announcements));
                }
            }
            msgs.InformationalMessage("Announcement(s) deleted successfully");
            mpeMessage.Show();
        }
        else if (fvAnnouncements.CurrentMode == FormViewMode.ReadOnly)
        {
            objAnnouncement.AnnouncementId = Convert.ToInt32(fvAnnouncements.DataKey["AnnounceMentId"]);
            objAnnouncement.DeleteAnnouncements();
            msgs.InformationalMessage("Announcement deleted successfully");
            mpeMessage.Show();
        }

        BindAnnouncements();

        upForm.Update();
        upDetails.Update();
    }

    protected void rbtnEmployeeWise_CheckedChanged(object sender, EventArgs e)
    {
        HtmlGenericControl divEmployees = this.fvAnnouncements.FindControl("divEmployees") as HtmlGenericControl;
        if (divEmployees != null)
            divEmployees.Visible = true;

        CheckBoxList chklstDepartments, chklstEmployees;
        DropDownList ddlCompany;
        RadioButton rbtnEmployeeWise, rbtnDepartmentWise;
        CheckBox chkAll;

        try
        {
            
            chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
            chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
            ddlCompany = this.fvAnnouncements.FindControl("ddlCompany") as DropDownList;
            rbtnEmployeeWise = sender as RadioButton;
            rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
            chkAll = this.fvAnnouncements.FindControl("chkAll") as CheckBox;
            Label lblNoSelection = this.fvAnnouncements.FindControl("lblNoSelection") as Label;

            if (lblNoSelection != null) lblNoSelection.Visible = false;

            rptrSelectedEmployees.DataSource = null;
            rptrSelectedEmployees.DataBind();

            if (rbtnEmployeeWise != null)
            {
                if (chkAll != null)
                    chkAll.Checked = false;

                if (rbtnEmployeeWise.Checked)
                {
                    rptrSelectedEmployees.DataSource = null;
                    rptrSelectedEmployees.DataBind();

                    if (ddlCompany.Items.Count == 0)
                    {
                        rbtnEmployeeWise.Checked = false;
                        rbtnDepartmentWise.Checked = true;
                        ScriptManager.RegisterClientScriptBlock(rbtnEmployeeWise, rbtnEmployeeWise.GetType(), Guid.NewGuid().ToString(), "alert('Please select a company to add employees!');", true);
                        return;
                    }

                    if (chklstDepartments != null && chklstEmployees != null)
                    {
                        // Set DepatmentID to zero for getting employees belongs to all departments
                        int DepartmentID = 0;
                        int CompanyID = ddlCompany.SelectedValue.ToInt32();
                        this.BindEmployees(CompanyID, DepartmentID, chklstEmployees);
                    }
                }
                chklstDepartments.Items.Clear();
                chklstDepartments.Visible = !rbtnEmployeeWise.Checked;
                chklstEmployees.Visible = rbtnEmployeeWise.Checked;
            }
        }
        finally
        {
            chklstDepartments = null;
            chklstEmployees = null;
            ddlCompany = null;
        }

    }

    protected void rbtnDepartmentWise_CheckedChanged(object sender, EventArgs e)
    {
        HtmlGenericControl divEmployees = this.fvAnnouncements.FindControl("divEmployees") as HtmlGenericControl;
        if (divEmployees != null)
            divEmployees.Visible = true;

        CheckBoxList chklstDepartments, chklstEmployees;
        RadioButton rdbtnDepartmentWise;
        CheckBox chkAll;
        try
        {
            chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
            chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
            rdbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
            chkAll = this.fvAnnouncements.FindControl("chkAll") as CheckBox;
            Label lblNoSelection = this.fvAnnouncements.FindControl("lblNoSelection") as Label;

            if (lblNoSelection != null) lblNoSelection.Visible = false;

            if (chkAll != null)
                chkAll.Checked = false;

            rptrSelectedEmployees.DataSource = null;
            rptrSelectedEmployees.DataBind();

            if (chklstDepartments != null && chklstEmployees != null && rdbtnDepartmentWise != null)
            {
                chklstDepartments.Visible = rdbtnDepartmentWise.Checked;
                chklstEmployees.Visible = !rdbtnDepartmentWise.Checked;
                chklstEmployees.Items.Clear();
                if (rdbtnDepartmentWise.Checked)
                    this.BindDepartments();
            }
        }
        finally
        {
            chklstDepartments = null;
            chklstEmployees = null;
        }

    }

    protected void rbtnGeneral_CheckedChanged(object sender, EventArgs e)
    {
        HtmlGenericControl divEmployees = this.fvAnnouncements.FindControl("divEmployees") as HtmlGenericControl;
        if (divEmployees != null)
            divEmployees.Visible = false;

    }

    private void BindEmployees(int CompanyID, int DepartmentID, CheckBoxList chklst)
    {
        chklst.DataValueField = "EmployeeID";
        chklst.DataTextField = "EmployeeFullName";
        chklst.DataSource = new clsAnnouncements().GetEmployees(CompanyID, DepartmentID);
        chklst.DataBind();
    }

    private void BindDepartments()
    {
        CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
        if (chklstDepartments != null)
        {
            chklstDepartments.Items.Clear();
            chklstDepartments.DataValueField = "DepartmentID";
            chklstDepartments.DataTextField = "Description";
            chklstDepartments.DataSource = new clsAnnouncements().GetDepatments();
            chklstDepartments.DataBind();
        }
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        System.Text.StringBuilder sbDepartmentIDs = new System.Text.StringBuilder();
        DropDownList ddlCompany = this.fvAnnouncements.FindControl("ddlCompany") as DropDownList;
        DataTable dt = null;
        CheckBox chkAll = sender as CheckBox;
        CheckBoxList chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
        CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
        RadioButton rbtnEmployeeWise = this.fvAnnouncements.FindControl("rbtnEmployeeWise") as RadioButton;
        RadioButton rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
        try
        {
            if (chkAll != null)
            {

                if (ddlCompany.Items.Count == 0)
                {
                    if (chkAll.Checked)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('Please select any company');", true);
                        chkAll.Checked = false;
                        return;
                    }
                }

                if (chkAll.Checked)
                {
                    if (rbtnDepartmentWise != null && rbtnDepartmentWise.Checked)
                    {
                        if (chklstDepartments != null)
                        {
                            foreach (ListItem item in chklstDepartments.Items)
                            {
                                item.Selected = true;
                                sbDepartmentIDs.Append((sbDepartmentIDs.Length > 0 ? "," : string.Empty) + item.Value);
                            }

                            if (sbDepartmentIDs.Length > 0)
                            {
                                dt = new clsAnnouncements().GetEmployees(ddlCompany.SelectedValue.ToInt32(), sbDepartmentIDs.ToString());

                                rptrSelectedEmployees.DataSource = dt;
                                rptrSelectedEmployees.DataBind();
                            }
                        }
                    }
                    else
                    {
                        if (chklstEmployees != null)
                        {
                            rptrSelectedEmployees.DataSource = null;
                            rptrSelectedEmployees.DataBind();

                            this.EmployeeList = new List<EmployeeTemp>();

                            foreach (ListItem item in chklstEmployees.Items)
                            {
                                item.Selected = true;
                                this.EmployeeList.Add(new EmployeeTemp { EmployeeID = item.Value.ToInt32(), EmployeeName = item.Text });

                                rptrSelectedEmployees.DataSource = this.EmployeeList;
                                rptrSelectedEmployees.DataBind();
                            }
                        }
                    }
                }
                else
                {
                    if (rbtnDepartmentWise != null && rbtnDepartmentWise.Checked)
                    {
                        chklstDepartments.ClearSelection();
                        rptrSelectedEmployees.DataSource = null;
                        rptrSelectedEmployees.DataBind();
                    }
                    else
                    {
                        rptrSelectedEmployees.DataSource = null;
                        rptrSelectedEmployees.DataBind();
                        chklstEmployees.ClearSelection();
                    }
                }
            }
        }
        finally
        {
            if (dt != null) dt.Dispose();
            ddlCompany = null;
            sbDepartmentIDs = null;
            chkAll = null;
        }
    }

    protected void chklstDepartments_SelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBoxList chklstDepartments;
        System.Text.StringBuilder sbDepartmentIDs = new System.Text.StringBuilder();
        DropDownList ddlCompany;
        DataTable dt = null;
        try
        {
            chklstDepartments = sender as CheckBoxList;
            ddlCompany = this.fvAnnouncements.FindControl("ddlCompany") as DropDownList;
            if (chklstDepartments != null && ddlCompany != null)
            {
                rptrSelectedEmployees.DataSource = null;
                rptrSelectedEmployees.DataBind();

                foreach (ListItem item in chklstDepartments.Items)
                {
                    if (item.Selected)
                        sbDepartmentIDs.Append((sbDepartmentIDs.Length > 0 ? "," : string.Empty) + item.Value);
                }

                if (sbDepartmentIDs.Length > 0)
                {
                    if (ddlCompany.Items.Count == 0)
                    {
                        foreach (ListItem item in chklstDepartments.Items)
                            item.Selected = false;

                        ScriptManager.RegisterClientScriptBlock(chklstDepartments, chklstDepartments.GetType(), Guid.NewGuid().ToString(), "alert('Please select a company to add employees');", true);
                        return;
                    }

                    dt = new clsAnnouncements().GetEmployees(ddlCompany.SelectedValue.ToInt32(), sbDepartmentIDs.ToString());

                    rptrSelectedEmployees.DataSource = dt;
                    rptrSelectedEmployees.DataBind();
                }
            }
        }
        finally
        {
            sbDepartmentIDs = null;
            if (dt != null) dt.Dispose();
        }
    }

    protected void chklstEmployees_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.EmployeeList = new List<EmployeeTemp>();

        foreach (ListItem item in chklstEmployees.Items)
        {
            if (item.Selected)
                this.EmployeeList.Add(new EmployeeTemp { EmployeeName = item.Text, EmployeeID = item.Value.ToInt32() });
        }

        this.rptrSelectedEmployees.DataSource = this.EmployeeList;
        this.rptrSelectedEmployees.DataBind();
    }

    public bool ValidateForm()
    {
        try
        {
            CheckBoxList chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
            CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;
            RadioButton rbtnEmployeeWise = this.fvAnnouncements.FindControl("rbtnEmployeeWise") as RadioButton;
            RadioButton rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
            RadioButton rbtnGeneral = this.fvAnnouncements.FindControl("rbtnGeneral") as RadioButton;
            Label lblNoSelection = this.fvAnnouncements.FindControl("lblNoSelection") as Label;
            TextBox  txtMessage = this.fvAnnouncements.FindControl("txtMessage") as TextBox;

            if (rbtnGeneral.Checked || chkAll.Checked)
                return true;

            bool Selected = false;
            string strMessage = string.Empty;
            if (rbtnEmployeeWise.Checked)
            {
                // EmployeeWise
                foreach (ListItem item in chklstEmployees.Items)
                {
                    if (item.Selected)
                    {
                        Selected = true;
                        break;
                    }
                }
                strMessage = "Please select an employee.";
            }
            else if (rbtnDepartmentWise.Checked)
            {
                // DepartmentWise
                foreach (ListItem item in chklstDepartments.Items)
                {
                    if (item.Selected)
                    {
                        Selected = true;
                        break;
                    }
                }
                strMessage = "Please select a department";
            }
            else
            {
                Selected = true;
                strMessage = string.Empty;
            }

            if (lblNoSelection != null)
                lblNoSelection.Text = strMessage;

            lblNoSelection.Visible = !Selected;

            return Selected;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
            return false;
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {

    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        // clear selected employees, if user change company as employees
        // are loaded from a particular company.
        this.rptrSelectedEmployees.DataSource = null;
        this.rptrSelectedEmployees.DataBind();

        DropDownList ddlCompany = sender as DropDownList;
        RadioButton rbtnEmployeeWise = this.fvAnnouncements.FindControl("rbtnEmployeeWise") as RadioButton;
        RadioButton rbtnDepartmentWise = this.fvAnnouncements.FindControl("rbtnDepartmentWise") as RadioButton;
        CheckBoxList chklstEmployees = this.fvAnnouncements.FindControl("chklstEmployees") as CheckBoxList;
        CheckBoxList chklstDepartments = this.fvAnnouncements.FindControl("chklstDepartments") as CheckBoxList;

        CheckBox chkAll = this.fvAnnouncements.FindControl("chkAll") as CheckBox;
        if (chkAll != null)
            chkAll.Checked = false;

        if (rbtnDepartmentWise != null && rbtnEmployeeWise != null)
        {
            if (rbtnEmployeeWise.Checked)
            {
                if (ddlCompany != null)
                {
                    // Set DepatmentID to zero for getting employees belongs to all departments
                    int DepartmentID = 0;
                    int CompanyID = ddlCompany.SelectedValue.ToInt32();
                    this.BindEmployees(CompanyID, DepartmentID, chklstEmployees);

                    foreach (ListItem item in chklstEmployees.Items)
                        item.Selected = false;
                }
            }
            else if (rbtnDepartmentWise.Checked)
            {
                if (chklstDepartments != null)
                {
                    foreach (ListItem item in chklstDepartments.Items)
                        item.Selected = false;
                }
            }
        }
    }

    public bool DisableEnable(object date)
    {
        try
        {
            if (date == DBNull.Value || date == null)
                return true;

            return clsCommon.Convert2DateTime(date) >= new clsCommon().GetSysDate().ToDateTime();
        }
        catch
        {
            return true;
        }
    }
}
