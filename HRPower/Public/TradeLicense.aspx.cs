﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.IO;
using HRAutoComplete;
using System.Text;

public partial class Public_TradeLicense : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    clsTradeLicense objTradeLicense;
    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterAutoComplete();
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();

        if (!IsPostBack)
        {
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            BindCombos();
            SetPermission();
            EnableMenus();

            pgrTradeLicense.Visible = false;
            lblNoData.Visible = false;

            txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

            getTradeLicenseList();
        }
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.TradeLicense,objUser.GetCompanyId());
    }

    private void BindCombos()
    {
        if (objRoleSettings == null)
            objRoleSettings = new clsRoleSettings();
        if (objEmployee == null)
            objEmployee = new clsEmployee();
        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();
        if (objUser == null)
            objUser = new clsUserMaster();
        objEmployee.CompanyID = objUser.GetCompanyId();
        ddlCompany.DataSource = objEmployee.GetAllCompanies();
        ddlCompany.DataBind();

        string str = string.Empty;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        if (ddlCompany.Items.Count == 0)
            ddlCompany.Items.Add(new ListItem(str, "-1"));

        ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);

        if (!(ddlCompany.Enabled))
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

        objTradeLicense.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);

        ddlProvince.DataSource = objTradeLicense.FillProvince();
        ddlProvince.DataBind();

        if (ddlProvince.Items.Count == 0)
            ddlProvince.Items.Add(new ListItem(str, "-1"));
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.TradeLicense);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];

        if (dtm.Rows.Count > 0)
        {
            btnNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            btnList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            btnEmail.Enabled = btnPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            btnDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dtm.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (objUser == null)
            objUser = new clsUserMaster();
        if (objRoleSettings == null)
            objRoleSettings = new clsRoleSettings();

        int UserID = objUser.GetRoleId();

        if (UserID > 3)
        {
            if (btnList.Enabled)
            {
                Bind();
                ImgSearch.Enabled = true;
            }
            else if (btnNew.Enabled)
            {
                NewTradeLicense();
            }
            else
            {
                pgrTradeLicense.Visible = false;
                dlTradeLicense.DataSource = null;
                dlTradeLicense.DataBind();
                dvAddTradeLicense.Visible = false;
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("ليس لديك إذن ما يكفي لعرض هذه الصفحة.") : ("You dont have enough permission to view this page."); //"You dont have enough permission to view this page.";
                lblNoData.Visible = true;
                ImgSearch.Enabled = false;
            }
        }

        if (btnDelete.Enabled)
            btnDelete.OnClientClick = "return valDeleteDatalist('" + dlTradeLicense.ClientID + "');";
        else
            btnDelete.OnClientClick = "return false;";

        if (btnPrint.Enabled)
            btnPrint.OnClientClick = "return valPrintEmailDatalist('" + dlTradeLicense.ClientID + "');";
        else
            btnPrint.OnClientClick = "return false;";

        if (btnEmail.Enabled)
            btnEmail.OnClientClick = "return valPrintEmailDatalist('" + dlTradeLicense.ClientID + "');";
        else
            btnEmail.OnClientClick = "return false;";
    }

    protected void Bind()
    {
        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();
        if (objUser == null)
            objUser = new clsUserMaster();

        objTradeLicense.PageIndex = pgrTradeLicense.CurrentPage + 1;
        objTradeLicense.PageSize = pgrTradeLicense.PageSize;
        objTradeLicense.SearchKey = txtSearch.Text;
        objTradeLicense.UserId = objUser.GetUserId();
        objTradeLicense.CompanyID = objUser.GetCompanyId();
        pgrTradeLicense.Total = objTradeLicense.GetCount();
        DataSet ds = objTradeLicense.GetAllTradeLicense();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlTradeLicense.DataSource = ds;
            dlTradeLicense.DataBind();
            pgrTradeLicense.Visible = true;
            lblNoData.Visible = false;
        }
        else
        {
            dlTradeLicense.DataSource = null;
            dlTradeLicense.DataBind();
            pgrTradeLicense.Visible = false;
            btnPrint.Enabled = btnEmail.Enabled = btnDelete.Enabled = false;
            btnPrint.OnClientClick = btnEmail.OnClientClick = btnDelete.OnClientClick = "return false;";
            lblNoData.Visible = true;

            if (txtSearch.Text == string.Empty)
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("يتم إضافة أية تراخيص التجارة بعد") : ("No Trade Licenses are added yet"); //"No Trade Licenses are added yet";
            else
                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("لا توجد نتائج بحث وجد") : ("No Search reasults found"); //"No Search reasults found";
        }

        dvAddTradeLicense.Visible = false;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('Employee');");
        upnlNoData.Update();
    }

    protected void NewTradeLicense()
    {
        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();

        objTradeLicense.TradeLicenseID = -1;
        objTradeLicense.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);

        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        divTradeLicenseDoc.Style["display"] = "none";
        fvTradeLicense.Visible = false;
        hdTradeLicenseID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        pgrTradeLicense.Visible = false;
        dvAddTradeLicense.Visible = true;

        txtSponsor.Text = string.Empty;
        txtSponsorFee.Text = string.Empty;
        txtLicenseNumber.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtPartner.Text = string.Empty;
        txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);        
        txtOtherInfo.Text = string.Empty;

        lblCurrentStatus.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("جديد") : ("New"); //"New";
        DataTable dtTradeLicensedoc = objTradeLicense.GetTradeLicenseDocuments().Tables[0];
        dlTradeLicenseDoc.DataSource = dtTradeLicensedoc;
        dlTradeLicenseDoc.DataBind();
        ViewState["TradeLicenseDoc"] = dtTradeLicensedoc;

        btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
        btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
        
        dlTradeLicense.DataSource = null;
        dlTradeLicense.DataBind();
        upnlNoData.Update();
        upMenu.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int id = 0;
        string msg = string.Empty;
        try
        {
            if (objTradeLicense == null)
                objTradeLicense = new clsTradeLicense();
            if (objAlert == null)
                objAlert = new clsDocumentAlert();

            objTradeLicense.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            objTradeLicense.Sponsor = Convert.ToString(txtSponsor.Text.Trim());
            objTradeLicense.SponserFee = Convert.ToDecimal(txtSponsorFee.Text.Trim());
            objTradeLicense.LicenceNumber = Convert.ToString(txtLicenseNumber.Text.Trim());
            objTradeLicense.City = Convert.ToString(txtCity.Text.Trim());
            objTradeLicense.ProvinceID = Convert.ToInt32(ddlProvince.SelectedValue);
            objTradeLicense.Partner = Convert.ToString(txtPartner.Text.Trim());
            objTradeLicense.IssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text);
            objTradeLicense.ExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text);
            objTradeLicense.Remarks = txtOtherInfo.Text.Trim();

            if (hfMode.Value == "Insert")
            {
                objTradeLicense.TradeLicenseID = 0;

                if (objTradeLicense.IsTradeLicenseNumberExists())
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("رقم الرخصة التجارية موجود") : ("Trade License Number exists");
                    mcMessage.InformationalMessage(msg);
                    //mcMessage.InformationalMessage("Trade License Number exists");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objTradeLicense.BeginEmp();
                        id = objTradeLicense.SaveUpdateTradeLicense();
                        objTradeLicense.TradeLicenseID = id;
                        objAlert.AlertMessage(Convert.ToInt32(DocumentType.Trading_License), "Trade License", "TradeLicense Number", id, txtLicenseNumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Comp", Convert.ToInt32(ddlCompany.SelectedValue), ddlCompany.SelectedItem.Text, true);
                        objTradeLicense.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTradeLicense.RollBack();
                    }
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objTradeLicense.TradeLicenseID = Convert.ToInt32(hdTradeLicenseID.Value);

                if (objTradeLicense.IsTradeLicenseNumberExists())
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("رقم الرخصة التجارية موجود") : ("Trade License Number exists");
                    mcMessage.InformationalMessage(msg);
                    //mcMessage.InformationalMessage("Trade License Number exists");
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objTradeLicense.BeginEmp();
                        id = objTradeLicense.SaveUpdateTradeLicense();
                        objTradeLicense.TradeLicenseID = id;
                        objTradeLicense.Commit();
                    }
                    catch (Exception ex)
                    {
                        objTradeLicense.RollBack();
                    }
                }
            }

            if (id == 0) return;
            //insert into treemaster and treedetails
            Label lblDocname, lblActualfilename, lblFilename;

            foreach (DataListItem item in dlTradeLicenseDoc.Items)
            {
                lblDocname = (Label)item.FindControl("lblDocname");
                lblFilename = (Label)item.FindControl("lblFilename");
                lblActualfilename = (Label)item.FindControl("lblActualfilename");
                objTradeLicense.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
                objTradeLicense.TradeLicenseID = id;
                objTradeLicense.LicenceNumber = Convert.ToString(txtLicenseNumber.Text.Trim());
                objTradeLicense.Docname = lblDocname.Text.Trim();
                objTradeLicense.Filename = lblFilename.Text.Trim();

                if (dlTradeLicenseDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                {
                    objTradeLicense.SaveTradeLicenseTreemaster();

                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                    {
                        if (!Directory.Exists(Path))
                            Directory.CreateDirectory(Path);

                        File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                    }
                }
            }

            objTradeLicense.Commit();

            if (hfMode.Value == "Insert")
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم حفظ التفاصيل الرخصة التجارية بنجاح") : ("Trade license details has been saved successfully");
                mcMessage.InformationalMessage(msg);
                //mcMessage.InformationalMessage("Trade license details has been saved successfully");
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث التفاصيل الرخصة التجارية بنجاح") : ("Trade license details has been updated successfully");
                mcMessage.InformationalMessage(msg);
                //mcMessage.InformationalMessage("Trade license details has been updated successfully");
            }

            mpeMessage.Show();
            upTradeLicense.Update();
            EnableMenus();
            hfMode.Value = "Edit";
            hdTradeLicenseID.Value = objTradeLicense.TradeLicenseID.ToStringCustom();
            upMenu.Update();
            upTradeLicense.Update();
            getTradeLicenseList();
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        getTradeLicenseList();
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        else
            return string.Empty;
    }

    protected void fuTradeLicense_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuTradeLicense.HasFile)
        {
            actualfilename = fuTradeLicense.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuTradeLicense.FileName);
            Session["Filename"] = filename;
            fuTradeLicense.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }

    protected void btnattachTradeLicensedoc_Click(object sender, EventArgs e)
    {
        divTradeLicenseDoc.Style["display"] = "block";
        DataTable dt;
        DataRow dw;

        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();

        objTradeLicense.TradeLicenseID = -1;
        objTradeLicense.CompanyID = -1;

        if (ViewState["TradeLicenseDoc"] == null)
            dt = objTradeLicense.GetTradeLicenseDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["TradeLicenseDoc"];
            dt = dtDoc;
        }

        if (hfMode.Value == "Insert" || hfMode.Value == "Edit")
        {
            dw = dt.NewRow();
            dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
            dw["Filename"] = Session["Filename"];
            dt.Rows.Add(dw);
        }

        dlTradeLicenseDoc.DataSource = dt;
        dlTradeLicenseDoc.DataBind();
        ViewState["TradeLicenseDoc"] = dt;
        txtDocname.Text = string.Empty;
        updTradeLicenseDoc.Update();
    }

    protected void dlTradeLicenseDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;

                if (objTradeLicense == null)
                    objTradeLicense = new clsTradeLicense();

                if (e.CommandArgument != string.Empty)
                {
                    objTradeLicense.Node = Convert.ToInt32(e.CommandArgument);
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objTradeLicense.DeleteTradeLicenseDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }

                objTradeLicense.TradeLicenseID = -1;
                objTradeLicense.CompanyID = -1;
                DataTable dt = objTradeLicense.GetTradeLicenseDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlTradeLicenseDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    dw["Node"] = dlTradeLicenseDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlTradeLicenseDoc.DataSource = dt;
                dlTradeLicenseDoc.DataBind();
                ViewState["TradeLicenseDoc"] = dt;
                break;
        }
    }

    protected void dlTradeLicense_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();
        if (objUser == null)
            objUser = new clsUserMaster();

        switch (e.CommandName)
        {
            case "ALTER":
                hfMode.Value = "Edit";
                objTradeLicense.TradeLicenseID = Convert.ToInt32(e.CommandArgument);
                hdTradeLicenseID.Value = Convert.ToString(e.CommandArgument);
                btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
                btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
                BindFormView(Convert.ToInt32(e.CommandArgument));
                txtSearch.Text = string.Empty;
                upnlIssueReceipt.Update();
                upTradeLicense.Update();
                break;
            case "VIEW":
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                break;
        }
    }

    private void BindFormView(int TradeLicenseID)
    {
        dvAddTradeLicense.Visible = true;
        objTradeLicense.TradeLicenseID = TradeLicenseID;
        DataTable dt = objTradeLicense.GetTradeLicense().Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrTradeLicense.Visible = false;
        BindTradeLicenseDetails(dt);
        dlTradeLicense.DataSource = null;
        dlTradeLicense.DataBind();
        divPrint.InnerHtml = GetPrintText(TradeLicenseID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            btnPrint.OnClientClick = sPrinttbl;
            btnEmail.OnClientClick = "return showMailDialog('Type=TradeLicense&TradeLicenseID=" + Convert.ToString(fvTradeLicense.DataKey["TradeLicenseID"]) + "');";
            btnDelete.OnClientClick = "return true;";
        }

        if (Convert.ToBoolean(dt.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            lblCurrentStatus.Text += clsUserMaster.GetCulture() == "ar-AE" ? ("[متجدد]") : ("[Renewed]"); //" [Renewed]";
            ControlEnableDisble(false);
        }
        else
            ControlEnableDisble(true);

        upMenu.Update();
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        ddlCompany.Enabled = ddlProvince.Enabled = blnTemp;
        txtSponsor.Enabled = txtSponsorFee.Enabled = txtLicenseNumber.Enabled = txtCity.Enabled = txtPartner.Enabled = blnTemp;
        txtIssuedate.Enabled = txtExpiryDate.Enabled = txtOtherInfo.Enabled = blnTemp;
        btnIssuedate.Enabled = btnExpiryDate.Enabled = blnTemp;
        ddlCompany.Enabled = false;
    }

    private void BindTradeLicenseDetails(DataTable dt)
    {
        upMenu.Update();

        if (dt.Rows.Count > 0)
        {
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            objTradeLicense.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);

            txtSponsor.Text = Convert.ToString(dt.Rows[0]["Sponsor"]);
            txtSponsorFee.Text = Convert.ToString(dt.Rows[0]["SponserFee"]);
            txtLicenseNumber.Text = Convert.ToString(dt.Rows[0]["LicenceNumber"]);
            txtCity.Text = Convert.ToString(dt.Rows[0]["City"]);

            if (dt.Rows[0]["ProvinceID"] != DBNull.Value)
                ddlProvince.SelectedIndex = ddlProvince.Items.IndexOf(ddlProvince.Items.FindByValue(Convert.ToString(dt.Rows[0]["ProvinceID"])));

            txtPartner.Text = Convert.ToString(dt.Rows[0]["Partner"]);
            txtIssuedate.Text = Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtExpiryDate.Text = Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtOtherInfo.Text = dt.Rows[0]["Remarks"].ToStringCustom();
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToStringCustom();
            objTradeLicense.TradeLicenseID = Convert.ToInt32(dt.Rows[0]["TradeLicenseID"]);

            DataTable dtTradeLicensedoc = objTradeLicense.GetTradeLicenseDocuments().Tables[0];
            dlTradeLicenseDoc.DataSource = dtTradeLicensedoc;
            dlTradeLicenseDoc.DataBind();
            ViewState["TradeLicenseDoc"] = dtTradeLicensedoc;

            if (dlTradeLicenseDoc.Items.Count > 0)
                divTradeLicenseDoc.Style["display"] = "block";
        }
    }

    private void BindViewMode(int TradeLicenseID)
    {
        dlTradeLicense.DataSource = null;
        dlTradeLicense.DataBind();
        pgrTradeLicense.Visible = false;
        dvAddTradeLicense.Visible = false;
        fvTradeLicense.Visible = true;
        hfMode.Value = "List";
        objTradeLicense.TradeLicenseID = TradeLicenseID;
        hdTradeLicenseID.Value = Convert.ToString(TradeLicenseID);
        fvTradeLicense.DataSource = objTradeLicense.GetTradeLicenseDetailView();
        fvTradeLicense.DataBind();
        divPrint.InnerHtml = GetPrintText(TradeLicenseID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if(btnPrint.Enabled)
            btnPrint.OnClientClick = sPrinttbl;
            if (btnEmail.Enabled)
            btnEmail.OnClientClick = "return showMailDialog('Type=TradeLicense&TradeLicenseID=" + Convert.ToString(fvTradeLicense.DataKey["TradeLicenseID"]) + "');";
            if (btnDelete.Enabled)
            btnDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');"; //"return confirm('Are you sure to delete the selected items?');";
        }
    }

    protected void dlTradeLicense_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (btnList.Enabled)
        {
            EnableMenus();
            pgrTradeLicense.CurrentPage = 0;
            Bind();
            fvTradeLicense.Visible = false;
            upTradeLicense.Update();
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        ControlEnableDisble(true);
        NewTradeLicense();
        upTradeLicense.Update();
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        getTradeLicenseList();
    }

    private void getTradeLicenseList()
    {
        txtSearch.Text = string.Empty;
        pgrTradeLicense.CurrentPage = 0;
        fvTradeLicense.Visible = false;
        fvTradeLicense.DataSource = null;
        fvTradeLicense.DataBind();
        Bind();
        hfMode.Value = "List";
        EnableMenus();
        upMenu.Update();
        upTradeLicense.Update();
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (objTradeLicense == null)
            objTradeLicense = new clsTradeLicense();

        CheckBox chkCompany;
        LinkButton btnCompany;
        Label lblIsDelete;
        string message = string.Empty;
        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

        if (dlTradeLicense.Items.Count > 0)
        {
            foreach (DataListItem item in dlTradeLicense.Items)
            {
                chkCompany = (CheckBox)item.FindControl("chkCompany");
                btnCompany = (LinkButton)item.FindControl("btnCompany");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkCompany == null)
                    continue;

                if (chkCompany.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    objTradeLicense.TradeLicenseID = Convert.ToInt32(dlTradeLicense.DataKeys[item.ItemIndex]);

                    DataTable dt = objTradeLicense.GetTradeLicenseFilenames();
                    objTradeLicense.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }

                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("الترخيص التجاري (ق) حذف بنجاح") : ("Trade License(s) deleted successfully"); //"Trade License(s) deleted successfully";
                }
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("فور صدورها / تجدد الرخصة التجارية (ق) لا يمكن حذف") : ("Once issued/renewed trade license(s) cannot be deleted"); //"Once issued/renewed trade license(s) cannot be deleted";
            }
        }
        else
        {
            if (hfMode.Value == "List")
            {
                objTradeLicense.TradeLicenseID = Convert.ToInt32(fvTradeLicense.DataKey["TradeLicenseID"]);

                if (Convert.ToBoolean(fvTradeLicense.DataKey["IsDelete"]))
                {
                    DataTable dt = objTradeLicense.GetTradeLicenseFilenames();
                    objTradeLicense.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("الترخيص التجاري (ق) حذف بنجاح") : ("Trade License(s) deleted successfully"); //"Trade License(s) deleted successfully";
                }
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("فور صدورها / تجدد الرخصة التجارية (ق) لا يمكن حذف") : ("Once issued/renewed trade license(s) cannot be deleted"); //"Once issued/renewed trade license(s) cannot be deleted";
            }
        }

        if (message == "")
            message = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى تحديد عنصر لحذف") : ("Please select an item for delete"); //"Please select an item for delete";

        mcMessage.InformationalMessage(message);
        mpeMessage.Show();
        getTradeLicenseList();
        upTradeLicense.Update();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlTradeLicense.Items.Count > 0)
        {
            if (objTradeLicense == null)
                objTradeLicense = new clsTradeLicense();

            if (fvTradeLicense.DataKey["TradeLicenseID"] == DBNull.Value || fvTradeLicense.DataKey["TradeLicenseID"] == null)
            {
                CheckBox chkCompany;
                string sTradeLicenseIds = string.Empty;

                foreach (DataListItem item in dlTradeLicense.Items)
                {
                    chkCompany = (CheckBox)item.FindControl("chkCompany");

                    if (chkCompany == null)
                        continue;

                    if (chkCompany.Checked == false)
                        continue;

                    sTradeLicenseIds += "," + dlTradeLicense.DataKeys[item.ItemIndex];
                }

                if (sTradeLicenseIds != "")
                {
                    sTradeLicenseIds = sTradeLicenseIds.Remove(0, 1);

                    if (sTradeLicenseIds.Contains(","))
                        divPrint.InnerHtml = CreateSelectedEmployeesContent(sTradeLicenseIds);
                    else
                        divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sTradeLicenseIds));
                }
            }
            else
                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvTradeLicense.DataKey["TradeLicenseID"]));

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        if (dlTradeLicense.Items.Count > 0)
        {
            if (objTradeLicense == null)
                objTradeLicense = new clsTradeLicense();

            string sTradeLicenseIds = string.Empty;

            if (fvTradeLicense.DataKey["TradeLicenseID"] == DBNull.Value || fvTradeLicense.DataKey["TradeLicenseID"] == null)
            {
                CheckBox chkCompany;

                foreach (DataListItem item in dlTradeLicense.Items)
                {
                    chkCompany = (CheckBox)item.FindControl("chkCompany");

                    if (chkCompany == null)
                        continue;

                    if (chkCompany.Checked == false)
                        continue;

                    sTradeLicenseIds += "," + dlTradeLicense.DataKeys[item.ItemIndex];
                }

                if (sTradeLicenseIds != "")
                    sTradeLicenseIds = sTradeLicenseIds.Remove(0, 1);
            }
            else
                sTradeLicenseIds = fvTradeLicense.DataKey["TradeLicenseID"].ToString();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=TradeLicense&TradeLicenseID=" + sTradeLicenseIds + "', 835, 585);", true);
        }
    }


    public string CreateSelectedEmployeesContent(string sTradeLicenseIDs)
    {
        StringBuilder sb = new StringBuilder();

        DataTable dt = objTradeLicense.CreateSelectedEmployeesPrintContent(sTradeLicenseIDs);  

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetLocalResourceObject("Company.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Sponsor.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("SponsorFee.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("LicenseNumber.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("City.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Province.Text") + "</td>" +
                                                "<td width='100px'>" + GetLocalResourceObject("Partner.Text") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td>" +
                                                "<td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td></tr>");
        sb.Append("<tr><td colspan='11'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CompanyName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Sponsor"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SponserFee"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["LicenceNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["City"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Province"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Partner"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["IssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");
        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    public string GetPrintText(int TradeLicenseID)
    {
        StringBuilder sb = new StringBuilder();

        DataTable dt = objTradeLicense.GetPrintText(TradeLicenseID);

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["CompanyName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>" + GetLocalResourceObject("TradeLicenseInformation.Text") + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("Sponsor.Text") + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Sponsor"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("SponsorFee.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["SponserFee"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("LicenseNumber.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["LicenceNumber"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("City.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["City"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Province.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Province"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetLocalResourceObject("Partner.Text") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Partner"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["IssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["ExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed") + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo") + "</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");
        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
}