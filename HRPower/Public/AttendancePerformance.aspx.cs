﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Globalization;
using System.Drawing;
using System.Web.UI.DataVisualization;
using System.Web.UI.DataVisualization.Charting;

public partial class Public_AttendancePerformance : System.Web.UI.Page
{
    clsSalesPerformance MobjclsSalesPerformance;
    clsUserMaster objUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("AttendancePerformance.Text").ToString();
        if (!IsPostBack)
        {
            FillDropDowns(0);
            txtPeriod.Text = new clsCommon().GetSysDate().ToDateTime().ToString("MMM-yyyy", CultureInfo.InvariantCulture);
            SetPermission();
        }

        EnableDisableMenus();
    }
    private void EnableDisableMenus()
    {
        if (ViewState["IsView"] != null && Convert.ToBoolean(ViewState["IsView"]) == true)
        {
            dvHavePermission.Style["display"] = "block";
            dvNoPermission.Style["display"] = "none";
        }
        else
        {
            dvHavePermission.Style["display"] = "none";
            dvNoPermission.Style["display"] = "block";
        }
    }

    public void SetPermission()
    {
        clsUserMaster objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)
        {
            DataTable dtPermission = new clsRoleSettings().GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.AttendanceTargetBased);

            if (dtPermission.Rows.Count > 0)
            {
                ViewState["IsCreate"] = dtPermission.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = dtPermission.Rows[0]["IsView"].ToBoolean();
                ViewState["IsUpdate"] = dtPermission.Rows[0]["IsUpdate"].ToBoolean();
                ViewState["IsDelete"] = dtPermission.Rows[0]["IsDelete"].ToBoolean();
            }
        }
        else
        {

            ViewState["IsCreate"] = ViewState["IsView"] = ViewState["IsUpdate"] = ViewState["IsDelete"] = true;
        }

    }

    private void FillDropDowns(int Mode)
    {
        if (Mode == 0)
        {
            ddlDepartment.DataTextField = "Department";
            ddlDepartment.DataValueField = "DepartmentID";
            ddlDepartment.DataSource = clsSalesPerformance.GetAllDepartments();
            ddlDepartment.DataBind();
            upnlDepartment.Update();
        }
        if (Mode == 0 || Mode == 1)
        {
            
            clsSalesPerformance MobjclsSalesPerformance = new clsSalesPerformance();
            objUser = new clsUserMaster();
            MobjclsSalesPerformance.CompanyID = objUser.GetCompanyId();
            ddlEmployee.DataTextField = "EmployeeName";
            ddlEmployee.DataValueField = "EmployeeID";
            ddlEmployee.DataSource = clsSalesPerformance.GetAllEmployees(ddlDepartment.SelectedValue.ToInt32(), MobjclsSalesPerformance.CompanyID);
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem(GetGlobalResourceObject("ControlsCommon","Any").ToString(), "-1"));
            upnlEmployee.Update();
            divType.Style["display"] = "none";
            ddlType.SelectedValue = "1";
            updType.Update();
        }

    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDepartment.SelectedValue.ToInt32() > 0)
        {
            FillDropDowns(1);
            
        }
    }


    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEmployee.Items.Count > 0)
        {
            if (ddlEmployee.SelectedValue.ToInt32() > 0)
            {
                divType.Style["display"] = "block";
                upnlEmployee.Update();
                updType.Update();
            }
            else
            {
                divType.Style["display"] = "none";
                ddlType.SelectedValue = "1";
                upnlEmployee.Update();
                updType.Update();
            }
        }
    }


    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
    }


    //Added By Laxmi
    private void FillAttendanceChart()
    {
        //divView.Style["display"] = "none";
        //AttendanceChart.Style["display"] = "block";
        try
        {
            clsSalesPerformance MobjclsSalesPerformance = new clsSalesPerformance();
            objUser = new clsUserMaster();
            MobjclsSalesPerformance.CompanyID = objUser.GetCompanyId();
            DataTable dt = clsHomePage.GetAttendancePerformanceChart(ddlType.SelectedValue.ToInt32() == 1 ? "SAC" : "SLPC", ddlDepartment.SelectedValue.ToInt32(), ddlEmployee.SelectedValue.ToInt32(), Convert.ToDateTime(txtPeriod.Text).Month, Convert.ToDateTime(txtPeriod.Text).Year, DateTime.DaysInMonth(Convert.ToDateTime(txtPeriod.Text).Year, Convert.ToDateTime(txtPeriod.Text).Month), MobjclsSalesPerformance.CompanyID);
            string[] xValues = new string[dt.Rows.Count];
            double[] yValues = new double[dt.Rows.Count]; // Vacancies
            decimal[] y1Values = new decimal[dt.Rows.Count]; // Applied
            double[] y2Values = new double[dt.Rows.Count]; // Scheduled
            double[] y3Values = new double[dt.Rows.Count]; // Selected
            double[] y4Values = new double[dt.Rows.Count]; // Vacant

            if (ddlType.SelectedValue.ToInt32() == 1)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    xValues[i] = Convert.ToString(dt.Rows[i]["Emp"]);

                    yValues[i] = Convert.ToDouble(dt.Rows[i]["WorkingDays"]);
                    y1Values[i] = Convert.ToDecimal(dt.Rows[i]["WorkedDays"]);
                    y2Values[i] = Convert.ToDouble(dt.Rows[i]["Leave"]);
                    y3Values[i] = Convert.ToDouble(dt.Rows[i]["Absent"]);
                }


                Series TotalWorkingDays = new Series();
                TotalWorkingDays.Name = GetLocalResourceObject("TotalWorkingDays.Text").ToString();
                //Vacancies.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

                Series ActualWorkedDays = new Series();
                ActualWorkedDays.Name = GetLocalResourceObject("ActualWorkingDays.Text").ToString(); ;
                //Applied.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

                Series Leaves = new Series();
                Leaves.Name = GetLocalResourceObject("Leaves.Text").ToString(); ;
                // Scheduled.ChartType = SeriesChartType.Bar;//(SeriesChartType)Enum.Parse(typeof(SeriesChartType), "Bar");

                Series Absent = new Series();
                Absent.Name = GetLocalResourceObject("Absent.Text").ToString(); ;


                ChartAttendance.Series.Add(TotalWorkingDays);
                ChartAttendance.Series.Add(ActualWorkedDays);
                ChartAttendance.Series.Add(Leaves);
                ChartAttendance.Series.Add(Absent);


                ChartAttendance.Series[GetLocalResourceObject("TotalWorkingDays.Text").ToString()].Points.DataBindXY(xValues, yValues);
                ChartAttendance.Series[GetLocalResourceObject("ActualWorkingDays.Text").ToString()].Points.DataBindXY(xValues, y1Values);
                ChartAttendance.Series[GetLocalResourceObject("Leaves.Text").ToString()].Points.DataBindXY(xValues, y2Values);
                ChartAttendance.Series[GetLocalResourceObject("Absent.Text").ToString()].Points.DataBindXY(xValues, y3Values);

                ChartAttendance.Series[GetLocalResourceObject("TotalWorkingDays.Text").ToString()].Color = Color.Plum;
                ChartAttendance.Series[GetLocalResourceObject("ActualWorkingDays.Text").ToString()].Color = Color.Green;
                ChartAttendance.Series[GetLocalResourceObject("Leaves.Text").ToString()].Color = Color.Red;
                ChartAttendance.Series[GetLocalResourceObject("Absent.Text").ToString()].Color = Color.Blue;

                ChartAttendance.Titles[0].Text = GetLocalResourceObject("AttendancePerformanceChart.Text").ToString();
                ChartAttendance.Titles[0].ForeColor = Color.DarkBlue;
                ChartAttendance.Titles[0].ShadowColor = Color.LightGray;
                ChartAttendance.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);
                ChartAttendance.Legends[0].Enabled = true;

                ChartAttendance.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                ChartAttendance.Palette = ChartColorPalette.None;
                ChartAttendance.Legends[0].Font = new Font(new FontFamily("Tahoma"), 8);


                ChartAttendance.Series[GetLocalResourceObject("TotalWorkingDays.Text").ToString()].IsValueShownAsLabel = true;
                ChartAttendance.Series[GetLocalResourceObject("ActualWorkingDays.Text").ToString()].IsValueShownAsLabel = true;
                ChartAttendance.Series[GetLocalResourceObject("Leaves.Text").ToString()].IsValueShownAsLabel = true;
                ChartAttendance.Series[GetLocalResourceObject("Absent.Text").ToString()].IsValueShownAsLabel = true;
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    yValues[i] = Convert.ToDouble(dt.Rows[i]["Leave"]);
                    xValues[i] = Convert.ToString(dt.Rows[i]["MonthName"]);
                }

                ChartAttendance.Series["Default"].Points.DataBindXY(xValues, yValues);


                //Set Pie chart type
                ChartAttendance.Series["Default"].ChartType = SeriesChartType.Line;
                ChartAttendance.Series["Default"].Color = Color.Red;

                //Set labels style (Inside, Outside, Disabled)


                ChartAttendance.Series[0]["PieLabelStyle"] = "Inside";

                //Set chart title, color and font
                ChartAttendance.Titles[0].Text = GetLocalResourceObject("LeavePerformanceChart.Text").ToString() + txtPeriod.Text;
                ChartAttendance.Titles[0].ForeColor = Color.DarkBlue;
                ChartAttendance.Titles[0].Font = new Font("Tahoma", 8, FontStyle.Bold);




                ChartAttendance.Series["Default"].IsValueShownAsLabel = true;
                ChartAttendance.Legends[0].Enabled = true;

                ChartAttendance.ChartAreas["ChartArea1"].AxisX.Interval = 1;

            }

        }
        catch (Exception ex)
        {
           
        }

    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        if (txtPeriod.Text != string.Empty)
        {
            AttendanceChart.Visible = true;
            FillAttendanceChart();

            upnlView.Update();
        }

    }
}
