﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Company.aspx.cs" Inherits="Public_Company" Title="Company" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li class="selected"><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li ><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
    function ShowPreviewCompany(source,args)
    {  
                 
        if(!isImage('ctl00_ctl00_content_public_content_fvCompany_fuLogo_ctl02'))
        {
            alert('Invalid file type');            
        }
        else
        {
            var divImage = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "divImage"));
            divImage.style.display = "block"; 
            
            WidthPreview(source,args,150,'imgPreview');
        }
    }
    
    function WidthPreview(source, args, width)  
        {
            var imgPreview = document.getElementById(source.get_id().replace(source.get_id().substring(source.get_id().lastIndexOf('_') + 1, source.get_id().length), "imgPreview"));
            
            imgPreview.style.display = "block";
            imgPreview.src = "../thumbnail.aspx?folder=Temp&file=" + '<%= Session.SessionID %>' + ".jpg&width=" + width + "&t=" + new Date();
        } 
            
    </script>

    <div>
        <div style="display: none">
            <asp:Button ID="btnSubmit" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:UpdatePanel ID="upnlFormview" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:FormView ID="fvCompany" runat="server" Width="700" DefaultMode="Edit" OnDataBound="fvCompany_DataBound"
                CellPadding="10" CellSpacing="10" OnItemCommand="fvCompany_ItemCommand" DataKeyNames="CompanyID,ParentID,CompanyBranchIndicator,IsMonth,DayID,CountryID,CurrencyId,
                            CompanyIndustryID,ProvinceID,CompanyTypeID,ContactPersonDesignationID,FinYearStartDate,BookStartDate"
                OnPageIndexChanging="fvCompany_PageIndexChanging">
               <EditItemTemplate>
                </EditItemTemplate>
                <ItemTemplate>
                    <div>
                        <table cellpadding="3" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td valign="top" class="labeltext">
                                    <span>
                                    <b>
                                    <%#Eval("Name")%></b></span><br />
                                    <%# Eval("Address")%>,
                                    <%# Eval("POBox") %><br />
                                    <%# Eval("Industry")%>
                                </td>
                                <td align="center" width="125" valign="top">
                                    <div id="tabs">
                                        <div style="display: none;" id="divImage" runat="server">
                                            <asp:Image ID="Image1" runat="server" src='<%# FormviewLogo() %>' />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <h4 style="padding-bottom: 5px;">
                            Company Info</h4>
                            
      
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="labeltext">
                            <tr id="trBranch" runat="server" style="display: none;">
                                <td class="trLeft">
                                    Company Name
                                </td>
                                <td class="trRight" valign="top">
                                    <%#IsCompany(Eval("ParentID"), Eval("CompanyID"))%>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft" width="150">
                                    Employer Code
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("EPID") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Short Name
                                </td>
                                <td class="trRight" valign="top">
                                    <%#Eval("ShortName") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Currency
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("Currency")%>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Daily Pay Based On
                                </td>
                                <td class="trRight" valign="top">
                                    <%# IsMonth(Eval("IsMonth")) %>
                                </td>
                            </tr>
                            
                              <tr>
                                <td class="trLeft">
                                    PO Box
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("POBox")%>
                                </td>
                              </tr>
                            
                              <tr>
                                <td class="trLeft">
                                    Company Industry
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("Industry")%>
                                </td>
                              </tr>
                            
                            
                              <tr>
                                <td class="trLeft">
                                    Company Type
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("CompanyType")%>
                                </td>
                              </tr>
                            
                            
                            
                            <tr>
                                <td class="trLeft">
                                    Working Days
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("WorkingDaysInMonth") %>
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Weekly Off Day
                                </td>
                                <td class="trRight" valign="top">
                                    <%# Eval("WeekDay")%>&nbsp;
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="trLeft">
                                    Unearned Policy
                                </td>
                                <td class="trRight" valign="top">
                                    <%#Eval("UnearnedPolicy")%>&nbsp;
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="trLeft">
                                    Website
                                </td>
                                <td class="trRight" valign="top">
                                    <%#Eval("WebSite") %>&nbsp;
                                </td>
                            </tr>
                            
                            
                            
                        </table>
                        <h4 style="padding: 5px;">
                            General Info</h4>
                            
                            
                            
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="labeltext">
                            <tr>
                                <td class="trLeft" width="150">
                                    Province/State
                                </td>
                                <td class="trRight">
                                    <%# Eval("Province")%>&nbsp;
                                </td>
                            </tr>
                        
                            <tr>
                                <td class="trLeft">
                                    Primary Email
                                </td>
                                <td class="trRight">
                                    <%#Eval("PrimaryEmail") %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Secondary Email
                                </td>
                                <td class="trRight">
                                    <%#Eval("SecondaryEmail") %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Contact Person
                                </td>
                                <td class="trRight">
                                    <%#Eval("ContactPerson") %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Designation
                                </td>
                                <td class="trRight">
                                    <%#Eval("Designation")%>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Telephone No.
                                </td>
                                <td class="trRight">
                                    <%#Eval("ContactPersonPhone")%>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Fax No.
                                </td>
                                <td class="trRight">
                                    <%#Eval("FaxNumber") %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Other Informations
                                </td>
                                <td class="trRight">
                                    <%# clsCommon.Crop(Convert.ToString(Eval("OtherInfo")),60) %>&nbsp;
                                </td>
                            </tr>
                        </table>
                        <h4 style="padding: 5px;">
                            Account Info</h4>
                        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="labeltext">
                            <tr>
                                <td class="firstTrLeft" width="150">
                                     Fin Year Start Date
                                </td>
                                <td class="firstTrRight">
                                    <%# ValidDate(Eval("FinYearStartDate")) %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="trLeft">
                                    Book Start Date
                                </td>
                                <td class="trRight">
                                    <%# ValidDate(Eval("BookStartDate"))%>&nbsp;
                                </td>
                            </tr>
                            
                              <tr>
                                <td class="trLeft">
                                    Company Start Date
                                </td>
                                <td class="trRight">
                                    <%# ValidDate(Eval("StartDate"))%>&nbsp;
                                </td>
                              </tr>
                            
                            <tr id="trBank" runat="server">
                                <td class="trLeft">
                                    Bank Details
                                </td>
                                <td class="trRight">
                                    <table style="width: 100%; font-weight: bold;" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="280">
                                                Bank Name
                                            </td>
                                            <td>
                                                Account Number
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:UpdatePanel runat="server" ID="upnlBankView" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="max-height: 100; overflow: auto;">
                                                <asp:DataList ID="dlBankView" runat="server" Width="100%">
                                                    <ItemTemplate>
                                                        <table style="width: 100%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="280">
                                                                    <%# Eval("Bank") %>
                                                                </td>
                                                                <td>
                                                                    <%# Eval("AccountNo") %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="fvCompany" EventName="DataBound" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="pnlSingleCompany" runat="server" Style="display: none;">
                        </asp:Panel>
                    </div>
                </ItemTemplate>
            </asp:FormView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlCompany" EventName="ItemCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:DataList ID="dlCompany" runat="server" Width="100%" OnItemCommand="dlCompany_ItemCommand"
                BorderWidth="0px" CellPadding="0" DataKeyField="CompanyID" OnItemDataBound="dlCompany_ItemDataBound"
                CssClass="labeltext">
                <ItemStyle CssClass="item" />
                <HeaderTemplate>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr style="display:none">
                            <td height="30" width="30" valign="top" align="center" >
                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectHeaderAll(this.id,'chkCompany')" />
                            </td>
                            <td style="padding-left: 5px">
                                Select All
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%" id="tblDetails" runat="server" border="0" cellpadding="0" cellspacing="0"
                        onmouseover="showEdit(this);" onmouseout="hideEdit(this);">
                        <tr>
                            <td valign="top" rowspan="5" width="30" style="padding-top: 4px ;display:none">
                                <asp:CheckBox ID="chkCompany" runat="server" />
                                <asp:HiddenField ID="hfIsCompany" runat="server" Value='<%#Eval("CompanyBranchIndicator") %>' />
                            </td>
                            <td valign="top" width="100" align="left">
                                <asp:LinkButton ID="imgLogo" runat="server" CausesValidation="False" CommandArgument='<%# Eval("CompanyID") %>'
                                    CommandName="_View" Style="padding: 2px"><img alt="" 
                                            src='<%# GetLogo(Eval("CompanyID"), 100) %>' /></a></a></asp:LinkButton>
                            </td>
                            <td valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkCompany" runat="server" Font-Bold="true" CommandArgument='<%# Eval("CompanyID") %>'
                                                            CommandName="_View" CausesValidation="False">
                                                                             <%# Eval("Name")%> </asp:LinkButton>
                                                        [<%# Eval("EPID")%>]
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td class="item_title" valign="top">
                                                        ShortName
                                                    </td>
                                                    <td align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("ShortName")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title" valign="top">
                                                        Address
                                                    </td>
                                                    <td align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("Address")%>
                                                        -
                                                        <%# Eval("POBox")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="item_title" width="100px" valign="top">
                                                        Industry
                                                    </td>
                                                    <td width="10" align="center" valign="top">
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("Industry")%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="top" width="50px" >
                                <div id="divIsBranch" runat="server"  style="display: none;vertical-align:top"><asp:Label ID="lb" runat="server" Text ="Branch"  class="statusspan"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <HeaderStyle CssClass="listItem" />
                <FooterTemplate>
                    <table width="100%">
                        <tr style="display:none">
                            <td width="25" valign="top" style="padding-left: 5px">
                                <asp:CheckBox ID="chk_FooterAll" runat="server" onclick="selectFooterAll(this.id,'chkCompany')" />
                            </td>
                            <td style="padding-left: 7px">
                                Select All
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:DataList>
            <uc:Pager ID="companyPager" runat="server" />
            <asp:Panel ID="pnlSelectedCompanies" runat="server" Style="display: none;">
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlCompany" EventName="ItemCommand" />
        </Triggers>
    </asp:UpdatePanel>
        <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: none;"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div id="search" style="vertical-align: top">
        <div id="text">
        </div>
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        <br />
        <div id="searchimg">
            <a href="">
                            <asp:ImageButton ID="btnGo" runat="server" CausesValidation="false" ImageUrl="../images/search.png"
                ToolTip="Click here to search" ImageAlign="AbsMiddle" OnClick="btnSearch_Click" />
                
                </a></div>
    </div>
    <asp:UpdatePanel ID="upnlMenus" runat="server">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage" style="display:none">
                    <asp:ImageButton ID="imgAddComp" ImageUrl="../images/add company.png" runat="server"
                        OnClick="imgAddComp_Click" Visible="False" />
                </div>
                <div class="name" style="display:none">
                    <asp:LinkButton ID="lnkAddComp" runat="server" OnClick="lnkAddComp_Click" Visible="False"> <h5>
                            Add Company</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListCompany" ImageUrl="../images/listcompanies.png" runat="server"
                        OnClick="imgListCompany_Click" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="lnkListCompany" runat="server" OnClick="lnkListCompany_Click"
                        CausesValidation="False"> <h5>
                           List Companies</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage" style="display:none">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" OnClick="imgDelete_Click"
                        Visible="False" />
                </div>
                <div class="name" style="display:none">
                    <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click1" Visible="False"> <h5>
                           Delete</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage" style="display:none">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" OnClick="imgPrint_Click" />
                </div>
                <div class="name" style="display:none">
                    <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click1">
                          <h5> Print</h5></asp:LinkButton>
                </div>
                <div class="sideboxImage" style="display:none">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name" style="display:none">
                    <asp:LinkButton ID="lnkEmail" runat="server">
                         <h5>
                            Email</h5>  </asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
