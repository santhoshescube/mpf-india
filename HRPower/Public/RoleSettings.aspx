﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="RoleSettings.aspx.cs" Inherits="Public_RoleSettings" Title="Roles" %>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx' ><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>            
            <li><a href="Bank.aspx"><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx"><asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx"><asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li class="selected"><a href="RoleSettings.aspx"><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx"><asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx"><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx"><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx"><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li><a href="RequestSettings.aspx"><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
            
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div id="divRole" runat="server">
        <div style="width: 100%; float: left;">
            <div style="width: 100%; float: left;">
                <div style="display: none">
                    <asp:Button ID="btnSubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="upnlMessage" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="popup"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
            <div style="width: 100%; float: left;">
                <asp:UpdatePanel ID="upRoleSettings" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 100%; padding-bottom: 5px;">
                            <div style="width: 30%; float: left;">
                                <fieldset style="margin-top: 21px; border-radius: 7px; border: 1px #34aee1 solid;
                                    padding: 11px; margin-bottom: 16px; margin-left: 6px;">
                                    <legend style="color: #0776e7;">Select Role</legend>
                                    <div style="width: auto;">
                                        <div style="width: auto; float: left;">
                                            <asp:UpdatePanel ID="upnlRole" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="rcSelectRole" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                        OnSelectedIndexChanged="rcSelectRole_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                        <div style="width: auto; float: left;">
                                            <asp:Button ID="btnRole" runat="server" Text="..." CausesValidation="False" CssClass="referencebutton"
                                                Height="20px" OnClick="btnRole_Click" />
                                        </div>
                                        <div style="width: auto; float: left;">
                                            <asp:UpdatePanel ID="upnlMsg" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:RequiredFieldValidator ID="rfRole" runat="server" ErrorMessage="Select a role"
                                                        Display="Dynamic" SetFocusOnError="true" CssClass="error" ControlToValidate="rcSelectRole"
                                                        ValidationGroup="submit" InitialValue="-1" Width="102px"></asp:RequiredFieldValidator></ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <%-- <uc:ReferenceControl ID="rcSelectRole" runat="server" DataTextField="RoleName" MaxLength="50"
                                                DataValueField="RoleID" OnIndexChanged="rcSelectRole_SelectedIndexChanged" ExcludeItems="-1,1"
                                                Width="120" AutoPostBack="True" TableName="RoleReference" ValueType="Role"  
                                                OnUpdate="rcSelectRole_Update"/>--%>
                                </fieldset>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="width: 100%; float: left;">
                <asp:UpdatePanel ID="uplMenus" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlModules" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                            DataKeyField="ModuleID" OnItemDataBound="dlModules_ItemDataBound" class="moduleTable"
                            CssClass="labeltext">
                            <HeaderTemplate>
                                <div style="width: 98%; float: left;" class="datalistheader">
                                    <div style="width: 40px; float: left;">
                                        <asp:CheckBox ID="chkSelectAll" runat="server" onclick=" selectAllModules2(this.id,'chkModules')" />
                                    </div>
                                    <div style="float: left;width: 25%;">
                                        Modules
                                    </div>
                                    
                                    <div style="float: left;width: 10%; display :none ">
                                        Menu Name
                                    </div>
                                    
                                    <div style="float: left;width: 11%;">
                                        View
                                    </div>
                                    <div style="float: left;width: 12%;">
                                        Add
                                    </div>
                                    <div style="float: left;width: 12%;">
                                        Update
                                    </div>
                                    <div style="float: left;width: 12%;">
                                        Delete
                                    </div>
                                    <div style="float: left;width: 11%;">
                                        PrintEmail
                                    </div>
                                    
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="width: 100%; float: left;" class="inactiverow">
                                
                                    <div style="vertical-align: top; width: 4%; margin-left: 5px; float: left;">
                                        <asp:CheckBox runat="server" ID="chkModules" onclick="selectAllMenus2(this.id,'chkModules')" />
                                    </div>
                                    
                                    <div style="float: left; width: 93%;">
                                        <div style="width: 100%; float: left;" id="tblMenus" runat="server">
                                            <div style="float: left; vertical-align: top;" class="trLeft">
                                                <asp:Label ID="lblModule" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                            </div>
                                            <div style="float: right; width: 30px; vertical-align: top;" class="trRight">
                                                <img id="imgMenus" runat="server" src="../images/down.png" />
                                            </div>
                                        </div>
                                        <div style="display: none; float: left;" runat="server" id="divMenus">
                                            <asp:DataList ID="dlMenus" runat="server" Width="100%" DataKeyField="MenuId">
                                                <ItemTemplate>
                                                    <div style="width: 171%; padding-left: 5px; float: left;">
                                                        <div class="trRight" style="width: 173px; float: left;">
                                                            <%# Eval("MenuName")%>
                                                        </div>
                                                        <div class="trLeft" style="width: 71px; float: left;padding-top: 6px;">
                                                            <asp:CheckBox ID="chkIsView"  Checked='<%#Convert.ToBoolean(Eval("IsView"))%>'
                                                                runat="server" />
                                                        </div>
                                                        <div class="trLeft" style="width: 84px; float: left;padding-top: 6px;">
                                                            <asp:CheckBox ID="chkIsCreate"    Checked='<%#Convert.ToBoolean(Eval("IsCreate"))%>'
                                                                runat="server" />
                                                        </div>
                                                        <div class="trLeft" style="width: 78px; float: left;padding-top: 6px;">
                                                            <asp:CheckBox ID="chkIsUpdate"  Checked='<%#Convert.ToBoolean(Eval("IsUpdate"))%>'
                                                                runat="server" />
                                                        </div>
                                                        <div class="trLeft" style="width: 93px; float: left;padding-top: 6px;">
                                                            <asp:CheckBox ID="chkIsDelete"   Checked='<%#Convert.ToBoolean(Eval("IsDelete"))%>'
                                                                runat="server" />
                                                        </div>
                                                        <div class="trLeft" style="width: 75px; float: left;padding-top: 6px;">
                                                            <asp:CheckBox ID="chkIsPrintEmail"   Checked='<%#Convert.ToBoolean(Eval("IsPrintEmail"))%>'
                                                                runat="server" />
                                                        </div>
                                                        <div class="trLeft" style="width: 75px;float: left;padding-top: 6px; display :none ">
                                                            <asp:CheckBox ID="chkIsEnabled"  Checked='<%#Convert.ToBoolean(Eval("IsEnabled"))%>'
                                                                runat="server" />
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:DataList>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div style="width: 100%; float: left;">
                <asp:UpdatePanel ID="upButton" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divbtn" style="display: block; padding-top: 8px; padding-bottom: 2px; padding-right: 13px;
                            float: right;" runat="server">
                            <asp:Button ID="btnSave" runat="server" CssClass="btnsubmit" Text="Submit" OnClick="btnSave_Click"
                                ValidationGroup="submit" Width="75px" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:Label ID="lblNoData" runat="server" CssClass="error" Style="display: block;"
        Visible="False"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div class="labeltext" style="width: 100%; float: left;">
        <div style="float: left; vertical-align: top; padding: 3px; width: 5%;">
            <img src="../images/Bulbe 14px.png" />
        </div>
        <div style="width: 90%; text-align: justify; float: left;">
            <span class="marquee">Roles refer to the specific functions and responsibilities which
                are relative to a designation or job criteria, in the context of an organization
                or company.</span>
        </div>
    </div>
    <div class="labeltext" style="width: 100%; float: left;">
        <div style="float: left; vertical-align: top; padding: 3px; width: 5%;">
            <img src="../images/Bulbe 14px.png" />
        </div>
        <div style="float: left; text-align: justify; width: 90%;">
            <span class="marquee">Roles may be created in the software by assigning access to modules
                based on designation of the user.</span>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
