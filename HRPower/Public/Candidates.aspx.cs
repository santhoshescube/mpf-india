﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using HRAutoComplete;
using System.Reflection;
using System.Text;


public partial class Public_Candidates : System.Web.UI.Page
{
    clsCandidate objCandidates;  
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;
  
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        // Auto complete
            this.RegisterAutoComplete();
            // 
            this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        objRoleSettings = new clsRoleSettings();
        objUserMaster = new clsUserMaster();
        if(ViewState["Pager"] != null)
            CandidatePager.Fill += new controls_Pager.FillPager(BindAdvancedSearchResults);
        else
           CandidatePager.Fill += new controls_Pager.FillPager(BindDataList);

           if (!IsPostBack)
           {
               if (objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsView"))
               {
                   EnableMenus();
               
                   ViewState["CanRefreshJob"] = true;
                   CurrentSelectedValue = "-1";
                   objUserMaster = new clsUserMaster();
                   //lnkPrint.Style["Disabled"] = "true";
                   //lnkPrint.Enabled = false;

                   if (Request.QueryString["CandidateID"] == null)
                   {
                       divPrintDisplay.Style["display"] = "none";
                       divListCandidate.Style["display"] = "block";
                       BindDataList();
                   }
                   else
                   {
                       divListCandidate.Style["display"] = "none";
                       divPrintDisplay.Style["display"] = "block";
                   }
               }
               else
               {
                  
                   lnkListCandidate.Enabled = lnkPrint.Enabled = lnkDelete.Enabled = ancOfferAcceptance.Enabled = false;
                   lnkAddCandidate.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsCreate");
                   divPrintDisplay.Style["display"] = "none";
                   divListCandidate.Style["display"] = "none";
                   divCandidateHistory.Style["display"] = "none";

                   msgs.WarningMessage("You dont have enough permission to view this page.");
                   mpeMessage.Show();
               }
           }

         
       }


    public void BindDataList()
    {
        ViewState["ChangeMode"] = "Edit";
        divPrintDisplay.Style["display"] = "none";
        divListCandidate.Style["display"] = "block";
        divCandidateHistory.Style["display"] = "none";
        objCandidates = new clsCandidate();
        ancOfferAcceptance.Enabled = false;
        lblOfferStatus.Text = lblOfferRemarks.Text = "";
       

        ViewState["Pager"] = null;
       

        objCandidates.PageIndex = CandidatePager.CurrentPage + 1;
        objCandidates.PageSize = CandidatePager.PageSize;
        objCandidates.SortExpression = "CandidateID";
        objCandidates.SortOrder = "desc";
        objCandidates.SearchKey = txtSearch.Text;


        CandidatePager.Total = objCandidates.GetRecordCount();
        DataSet ds = objCandidates.FillDataList();

        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                dlCandidate.DataSource = ds;
                dlCandidate.DataBind();
                lblCandidates.Text = string.Empty;
                lblCandidates.Attributes.Add("style", "display:none");
             
                CandidatePager.Visible = true;
            }
            else
            {
                dlCandidate.DataSource = null;
                dlCandidate.DataBind();

                lblCandidates.Text = GetLocalResourceObject("NoCandidatesFound.Text").ToString();

                lblCandidates.Attributes.Add("style", "display:block"); ;

                CandidatePager.Visible = false;
            }
        }
    }

   
    public void EnableMenus()
    {
        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        lnkAddCandidate.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsCreate");
        lnkListCandidate.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsView");

        lnkPrint.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsPrintEmail");
        lnkDelete.Enabled = objRoleSettings.IsMenuEnabledForInterviewer(objUserMaster.GetRoleId(), (int)eMenuID.Candidates, "IsDelete");
      
        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteCandidateDatalist('" + dlCandidate.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailCandidateDatalist('" + dlCandidate.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

    }

  

    protected void dlCandidate_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Modify":

                Response.Redirect("CandidateEng.aspx?CandidateID=" + e.CommandArgument.ToString() + "&type=modify");
                break;
            case "History":
                  Candidatehistorycontrol.GetHistory(null,Convert.ToInt32(e.CommandArgument));
                  divListCandidate.Style["display"] = "none";
                  divPrintDisplay.Style["display"] = "none";
                  divCandidateHistory.Style["display"] = "block";
                break ;

            case "View":

                if (e.CommandArgument.ToInt64() > 0)
                {

                   // ancOfferAcceptance.OnClientClick = "return false;";
                    lblOfferStatus.Visible= lblOfferRemarks.Visible  = true;
                    ancOfferAcceptance.Enabled = false;
                    int OfferStatusID = 0;
                    objCandidates = new clsCandidate();
                    objCandidates.CandidateID = e.CommandArgument.ToInt64();
                    DataTable dt = objCandidates.GetOfferRemarks();
                    OfferStatusID = dt.Rows[0]["CandidateStatusId"].ToInt32();
                    objCandidates.CandidateID = e.CommandArgument.ToInt64();


                    if ( OfferStatusID == (int)CandidateStatus.OfferSent)
                        ancOfferAcceptance.Enabled = true;

                    lblOfferStatus.Text = dt.Rows[0]["CandidateStatus"].ToString();                  
                    lblOfferRemarks.Text = dt.Rows[0]["remarks"].ToString();
                  
                    ViewState["CandidateID"] = Convert.ToString(e.CommandArgument);
                    //lnkPrint.Enabled = true;
                    //lnkPrint.Style["Disabled"] = "false";



                    ViewState["ChangeMode"] = "ReadOnly";
                    divListCandidate.Style["display"] = "none";
                    divPrintDisplay.Style["display"] = "block";
                    divCandidateHistory.Style["display"] = "none";
                    CandidateView1.GetCandidateInfo(e.CommandArgument.ToInt64());

                    string sConfirm = clsGlobalization.IsArabicCulture()? "هل أنت متأكد من حذف المرشحين المختارين؟" : "Are you sure to delete this candidate?";
                    if(lnkDelete.Enabled)
                    lnkDelete.OnClientClick = "return confirm('" + sConfirm +"');";
                    if(lnkPrint.Enabled)
                    lnkPrint.OnClientClick = "return true";
                    
                  
                    // divPrintDisplay.InnerHtml = GetPrintText(e.CommandArgument.ToInt64());
                    //lnkPrint.OnClientClick = "return true;";

                    upfvCandidate.Update();
                    upMenu.Update();
                }
                break;

            case "download":

                Response.Redirect("download.aspx?file=" + "documents\\candidateresumes\\" + dlCandidate.DataKeys[e.Item.ItemIndex].ToString() + "_" + e.CommandArgument.ToString());

                break;
        }
    }


  

    public string Getstring(object oExperience, object oJobTitle)
    {
        string strYear = Convert.ToString(oExperience.ToString().Split('.')[0]);
        string strMonth = Convert.ToString(oExperience.ToString().Split('.')[1]);

        if (Convert.ToString(oJobTitle) == "Fresher")
        {
            return Convert.ToString(oJobTitle);
        }
        else if (strMonth == "00")
        {
            return Convert.ToString(strYear + " year ");
        }
        else if (strYear == "0")
        {
            if (strMonth.StartsWith("0"))
                return strMonth.Remove(0, 1) + " months";
            else
                return Convert.ToString(strMonth + " months ");
        }
        else
            return Convert.ToString(strYear + " year " + (strMonth.StartsWith("0") ? strMonth.Remove(0, 1) : strMonth) + " months");
    }

    protected string GetImageUrl(object ResumeAttachment)
    {
        string imgUrl = string.Empty;

        switch (Path.GetExtension(Convert.ToString(ResumeAttachment)))
        {
            case ".doc":

                imgUrl = "../images/word_icon.png";

                break;

            case ".ppt":
                imgUrl = "../images/ppt_icon.png";

                break;

            case ".pdf":
                imgUrl = "../images/pdf_icon.png";

                break;

            case ".xls":
                imgUrl = "../images/excel_icon.png";

                break;

            default:
                imgUrl = "../images/other_document_icon.png";

                break;
        }

        return imgUrl;
    }

  

    public string GetCandidateName(object oCandidatename, object oQualification, object oExperience, object oJobCode)
    {
        string sCandidateName = string.Empty;
        sCandidateName = clsGlobalization.IsArabicCulture() ? "[" + Convert.ToString(oCandidatename) + "  [" + Convert.ToString(oQualification) + " | " + Convert.ToString(oExperience) + " "  :
        Convert.ToString(oCandidatename) + " " + "[" + Convert.ToString(oQualification) + " | " + Convert.ToString(oExperience) +    "]";   
       

        return sCandidateName;
    }
    public string GetCandidatePhoto(object oCandidateId)
    {
        string sFileName = string.Empty;

        if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + oCandidateId.ToString() + ".jpg")))
        {
            sFileName = Convert.ToString(oCandidateId + ".jpg");
            return string.Format("../thumbnail.aspx?isCandidate=true&folder=candidatephotos&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
        }
        else
        {

            sFileName = "../images/candidate.jpg";
            //"../images/sample_passport.jpg";
            return sFileName;
        }
    }

    public string GetCandidateStatus(object oCandidateId)
    {
        objCandidates = new clsCandidate();
        string sStatus = string.Empty;

        objCandidates.CandidateID = Convert.ToInt32(oCandidateId);

        int iStatusRef = objCandidates.GetOfferStatudID();

        if (iStatusRef == 1)
        {
            sStatus = "Associated";
        }
        else
        {
            sStatus = "New";
        }
        return sStatus;
    }

    protected void dlCandidate_ItemDataBound(object sender, DataListItemEventArgs e)
    {
      
        //lnkDelete.OnClientClick = "return valDeleteCandidateDatalist('" + dlCandidate.ClientID + "');";
        ImageButton imgHistory = (ImageButton)e.Item.FindControl("imgHistory");
        objCandidates = new clsCandidate();
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            
            objCandidates.CandidateID = Convert.ToInt32(dlCandidate.DataKeys[e.Item.ItemIndex]);
            if (imgHistory != null)
            {
                imgHistory.Visible = objCandidates.CheckEmployeeExists();
            }

        }
        

    }

    protected string GetDownloadLink(object oCandidateId, object oResumeAttachment)
    {
        return string.Format("download.aspx?file=documents\\candidateresumes\\{0}_{1}", oCandidateId.ToString(), Convert.ToString(oResumeAttachment));
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
       
        if (divPrintDisplay.Style["display"] == "none")
        { 
            objCandidates = new clsCandidate();
           
            string sCandidateIds = string.Empty;
            Table tbl = new Table();
            if (dlCandidate.Items.Count > 0)
            {
                foreach (DataListItem item in dlCandidate.Items)
                {
                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");

                    if (chkSelect == null)
                        return;

                    if (chkSelect.Checked)
                    {
                        sCandidateIds += "," + Convert.ToString(dlCandidate.DataKeys[item.ItemIndex]);
                    }
                }
                if (sCandidateIds != "")
                {
                    sCandidateIds = sCandidateIds.Remove(0, 1);
                    objCandidates.CandidatesID = sCandidateIds;
                    tbl = PrintCandidates(sCandidateIds);
                }
            }
           


            tbl.Width = Unit.Percentage(100);
            tbl.Attributes.Add("Style", "Display:none;");

            pnlPrint.Controls.Add(tbl);

            ScriptManager.RegisterStartupScript(lnkPrint, this.GetType(), "Print", "Print('" + tbl.ClientID + "')", true);
            upfvCandidate.Update();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrintDisplay.ClientID + "')", true);
        }

        
    }
    private Table PrintCandidates(string sCandidateIds)
    {
        Table tblCandidate = new Table();
        TableRow tr = new TableRow();
        TableCell td = new TableCell();

        tblCandidate.ID = "PrintCandidate";

        td.Text = CreateSelectedCandidateContent(sCandidateIds);

        tr.Cells.Add(td);
        tblCandidate.Rows.Add(tr);

        return tblCandidate;
    }
    private string CreateSelectedCandidateContent(string sCandidateIds)
    {
        StringBuilder sb = new StringBuilder();
        objCandidates = new clsCandidate();
        objCandidates.CandidatesID = sCandidateIds;
        DataTable dt = objCandidates.CreateSelectedCandidateContent();

        sb.Append("<table width='100%' style='font-family:Tahoma;font-size:13px;' border='0'>");
        sb.Append("<tr><td style='font-weight:bold;font-size:13px' align='center'>" + GetLocalResourceObject("PageTitle.Title") + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma' border='0px'>");
        sb.Append("<tr><td colspan='5'><hr></td></tr>");
        sb.Append("<tr style='font-weight:bold;'><td width='150px'> " + GetLocalResourceObject("CandidateName.Text") +"</td><td width='200px'>" + GetLocalResourceObject("Address.Text") + "</td><td width='150px'>" + GetLocalResourceObject("Email.Text") + "</td><td width='100px'> " + GetLocalResourceObject("PlaceOfBirth.Text") + "</td><td width='120px'> " + GetLocalResourceObject("Qualification.Text") + "</td></tr>");
        sb.Append("<tr><td colspan='5'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["CandidateName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["CurrentAddress"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EmailID"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofBirth"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Degree"]) + "</td></tr>");
        }
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }
    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        if (dlCandidate.Items.Count > 0)
        {
            string sCandidateIds = string.Empty;

            foreach (DataListItem item in dlCandidate.Items)
            {
                CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");

                if (chkSelect == null)
                    return;

                if (chkSelect.Checked)
                {
                    sCandidateIds += "," + Convert.ToString(dlCandidate.DataKeys[item.ItemIndex]);
                }
            }
           // ScriptManager.RegisterStartupScript(lnkEmail, this.GetType(), "Email", "showMailDialog('Type=Candidate&CandidateId=" + Convert.ToString(sCandidateIds.Remove(0, 1)) + "');", true);
        }
    }

    void ucResume_Update()
    {
        if (ViewState["Pager"] != null)
        {
          //  BindAdvancedSearchResults();
        }
        else
        {
            BindDataList();
        }
        upfvCandidate.Update();

    }

    protected void btnGo_Click(object sender, ImageClickEventArgs e)
    {
        CandidatePager.CurrentPage = 0;
       // upnlMenus.Update();
        BindDataList();

        this.RegisterAutoComplete();

        upfvCandidate.Update();
    }

    private void RegisterAutoComplete()
    {
        objUserMaster = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Candidate , objUserMaster.GetCompanyId());
    }

    protected void lnkResume_Click(object sender, EventArgs e)
    {
        //ucResume.BindResumes();
        //ucResume.Clear();
        //upnlMenus.Update();
        //mpeResume.Show();

    }

    protected void btnAddJobTitle_Click(object sender, ImageClickEventArgs e)
    {
        //    hdMode.Value = "add";
        //    txtValue.Focus();
        //    divBottom.Style.Add("display", "block");
        //    divTop.Style.Add("display", "none");
        //    txtValue.Text = string.Empty;
    }

    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        //    hdMode.Value = "add";
        //    txtValue.Focus();
        //    divBottom.Style.Add("display", "block");
        //    divTop.Style.Add("display", "none");
        //    txtValue.Text = string.Empty;
    }
    protected void btnEdit_Click(object sender, ImageClickEventArgs e)
    {
        //if (ddlList.SelectedItem == null) { return; }
        //hdMode.Value = "edit";
        //divBottom.Style.Add("display", "block");
        //divTop.Style.Add("display", "none");
        //txtValue.Text = ddlList.SelectedItem.Text;
        //txtValue.Focus();
    }
   
   
    protected void btnResume_Click(object sender, ImageClickEventArgs e)
    {
        //divBottom.Style.Add("display", "none");
        //divTop.Style.Add("display", "block");
    }

    protected void lnkAddCandidate_Click(object sender, EventArgs e)
    {
        
        txtSearch.Text = string.Empty;
        Response.Redirect("CandidateEng.aspx?CandidateId=0");
        
        //lnkDelete.OnClientClick = lnkAssociate.OnClientClick = lnkMakeAsHot.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false";
        //lnkDelete.Enabled = lnkAssociate.Enabled = lnkMakeAsHot.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;

        upfvCandidate.Update();

    }
    protected void lnkListCandidate_Click(object sender, EventArgs e)
    {
        ViewState["Pager"] = null;
        objCandidates = new clsCandidate();
        CandidatePager.CurrentPage = 0;
        txtSearch.Text = string.Empty;
        BindDataList();
        upfvCandidate.Update();
        //lnkPrint.OnClientClick = "return valPrintEmailCandidateDatalist('" + dlCandidate.ClientID + "');";
        upMenu.Update();
       
    }
    //protected void DisplayRemarks(object sender, EventArgs e)
    //{
    //    if(rblOffer.SelectedValue == "0")
    //        trRemarks.Style["display"] = "table-row";
    //    else 
    //         trRemarks.Style["display"] = "none";

    //    upOffer.Update();
    //    divOfferAcceptance.Attributes.Add("style", "display:block;max-height:250px"); 
    //}

    protected void lnkOfferAcceptance_Click(object sender, EventArgs e)
    {


        ancOfferAcceptance.Attributes.Add("onclick", "ClearAcceptance(this.id);return false;");

        ScriptManager.RegisterClientScriptBlock(ancOfferAcceptance, ancOfferAcceptance.GetType(), "Offer", "ClearAcceptance('" + ancOfferAcceptance.ClientID + "');", true);
       
        
        
        
    }

    protected void lnkAdvanceSearch_Click(object sender, EventArgs e)
    {
       objCandidates = new clsCandidate();
      
       DataSet  ds = objCandidates.LoadCombos();

        ddlSrchQualification.DataSource = ds.Tables[7];
        ddlSrchQualification.DataBind();

        ddlCountry.DataSource = ds.Tables[5];
        ddlCountry.DataBind();

        ddlReferralType.DataSource = ds.Tables[19];
        ddlReferralType.DataBind();

        string sItemText = clsGlobalization.IsArabicCulture() ? "-أي-" : "-Any-";
         ddlReferralType.Items.Insert(0, new ListItem(sItemText, "-1"));

         ddlReferredBy.Items.Insert(0, new ListItem(sItemText, "-1"));

         ddlCountry.Items.Insert(0, new ListItem(sItemText, "-1"));

         ddlSrchQualification.Items.Insert(0, new ListItem(sItemText, "-1")); 

    

        ddlSrchSpecialization.DataSource = ds.Tables[10];
        ddlSrchSpecialization.DataBind();

      
        if (ddlSrchSpecialization.Items.Count > 0)
            ddlSrchSpecialization.Items.Insert(0, new ListItem(sItemText, "-1"));
        else
            ddlSrchSpecialization.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-لا شيء-" : "-none-", "-1"));

        for (int i = 0; i <= 20; i++)
        {
            ddlSrchMinExperience.Items.Add(Convert.ToString(i));

            ddlSrchMaxExperience.Items.Add(Convert.ToString(i));
        }

        ddlSrchMinExperience.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-دقيقة-" : "-Min-", "-1"));

        ddlSrchMaxExperience.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-ماكس-" : "-Max-", "-1"));

        for (int i = 1980; i <= DateTime.Now.Year + 1; i++)
            ddlSrchPassoutFrom.Items.Add(Convert.ToString(i));

        ddlSrchPassoutFrom.Items.Insert(0, new ListItem(sItemText, "-1"));

        ancAdvanceSearch.Attributes.Add("onclick", "ClearFields(this.id);return false;");

        ScriptManager.RegisterClientScriptBlock(ancAdvanceSearch, ancAdvanceSearch.GetType(), "AdvancedSearch", "ClearFields('" + ancAdvanceSearch.ClientID + "');", true);
    }

    protected void ddlSrchQualification_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        divAdvanceSearch.Attributes.Add("style", "display:block;max-height:400px");  
    }   

    protected void btnadvanceSearch_Click(object sender, EventArgs e)
    {
        CandidatePager.CurrentPage = 0;

        ViewState["Pager"] = "SearchPager";
        BindAdvancedSearchResults();
        divListCandidate.Style["display"] = "block";
        divPrintDisplay.Style["display"] = "none";      
        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteCandidateDatalist('" + dlCandidate.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailCandidateDatalist('" + dlCandidate.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";
                    
    }
    public void BindAdvancedSearchResults()
    {
        objCandidates = new clsCandidate();

        objCandidates.Skill = Convert.ToString(txtSrchSkillSet.Text);
        objCandidates.MinExp = Convert.ToInt32(ddlSrchMinExperience.SelectedValue);
        objCandidates.MaxExp = Convert.ToInt32(ddlSrchMaxExperience.SelectedValue);
        objCandidates.QualificationID = Convert.ToInt32(ddlSrchQualification.SelectedValue);
        objCandidates.QualificationCategoryID = Convert.ToInt32(ddlSrchSpecialization.SelectedValue);
        objCandidates.GraduationYear = Convert.ToInt32(ddlSrchPassoutFrom.Text);
        objCandidates.CountryID = ddlCountry.SelectedValue.ToInt32();
        objCandidates.ReferralType = ddlReferralType.SelectedValue.ToInt32();
        objCandidates.ReferredBy = ddlReferredBy.SelectedValue.ToInt32();

        objCandidates.PageIndex = CandidatePager.CurrentPage + 1;
        objCandidates.PageSize = CandidatePager.PageSize;
        objCandidates.SortExpression = "CandidateId";
        objCandidates.SortOrder = "desc";

        DataSet ds = objCandidates.GetAdvanceSearchDetails();

        if (ds.Tables[1].Rows.Count > 0)
        {
            lblCandidates.Text = string.Empty;
            lblCandidates.Attributes.Add("style", "display:none");
            CandidatePager.Visible = true;

            CandidatePager.Total = Convert.ToInt32(ds.Tables[0].Rows[0]["Total"]);

            dlCandidate.DataSource = ds.Tables[1];
            dlCandidate.DataBind();
        }
        else
        {
            dlCandidate.DataSource = null;
            dlCandidate.DataBind();

            lblCandidates.Text = "No Candidates Found";
            lblCandidates.Attributes.Add("style", "display:block");

            CandidatePager.Visible = false;
        }
        //fvCandidate.ChangeMode(FormViewMode.ReadOnly);
        //fvCandidate.DataSource = null;
        //fvCandidate.DataBind();

        upfvCandidate.Update();
    }


    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        
            objCandidates = new clsCandidate();
            CheckBox chkSelect;
            int chkStatus = 0;
            bool bStatus = false ;
            string sMessage = "";
            int iDeletedCount = 0;
          
            if (divListCandidate.Style["display"] == "none")
            {
                objCandidates.Begin();
                objCandidates.CandidateID = Convert.ToInt32(ViewState["CandidateID"]);
                bStatus = objCandidates.DeleteCandidates();
                if (!bStatus)
                {
                    objCandidates.RollBack();
                    sMessage = GetLocalResourceObject("CanNotDelete.Text").ToString();
                    this.msgs.WarningMessage(sMessage);
                    this.mpeMessage.Show();
                }
                else
                {
                    sMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                    this.msgs.WarningMessage(sMessage);
                    this.mpeMessage.Show();
                    objCandidates.Commit();
                    BindDataList();
                    upfvCandidate.Update();

                    upMenu.Update();
                }
                
            }
            else
            {
                if (dlCandidate.Items.Count > 0)
                {
                    objCandidates.Begin();
                    foreach (DataListItem item in dlCandidate.Items)
                    {
                       
                        chkSelect = (CheckBox)item.FindControl("chkSelect");
                        if (chkSelect.Checked)
                        {   
                            chkStatus = 1;                 
                            objCandidates.CandidateID = Convert.ToInt32(dlCandidate.DataKeys[item.ItemIndex]);
                            bStatus = objCandidates.DeleteCandidates();
                            if (!bStatus)
                            {

                                objCandidates.RollBack();
                                sMessage = GetLocalResourceObject("CanNotDelete.Text").ToString();
                            }
                            else
                            {
                               sMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                            }

                            iDeletedCount = (bStatus) ? iDeletedCount + 1 : iDeletedCount;
                        }
                    }
                    if (chkStatus == 0)
                    {
                        msgs.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString());
                        this.mpeMessage.Show();
                    }

                    if (iDeletedCount > 0)
                    {
                        objCandidates.Commit();
                        divPrintDisplay.Style["display"] = "none";
                        divListCandidate.Style["display"] = "block";
                        BindDataList();
                        upfvCandidate.Update();

                        upMenu.Update();

                    }

                }
            }

          
            this.msgs.WarningMessage(sMessage);
            this.mpeMessage.Show();
           
       
    }
    public void FillComboRefferedBy()
    {
        objCandidates = new clsCandidate();
        DataSet dsTemp = objCandidates.LoadCombos();
        switch (ddlReferralType.SelectedValue)
        {
            case "1":

                if (dsTemp.Tables[1] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[1];
                    ddlReferredBy.DataTextField = "Agency";
                    ddlReferredBy.DataValueField = "AgencyId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-أي-" : "-Any-", "-1"));                   
                   
                }
                break;
            case "2":
                if (dsTemp.Tables[21] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[21];
                    ddlReferredBy.DataTextField = "Description";
                    ddlReferredBy.DataValueField = "JobPortalId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-أي-" : "-Any-", "-1"));

                }
                break;
            case "3":
                if (dsTemp.Tables[20] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[20];
                    ddlReferredBy.DataTextField = "EmployeeFullName";
                    ddlReferredBy.DataValueField = "EmployeeID";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "-أي-" : "-Any-", "-1"));                 
                 

                }
                break;

        }


    }
    protected void ddlReferralType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillComboRefferedBy();
        divAdvanceSearch.Attributes.Add("style", "display:block;max-height:400px");
    }
    protected void btnOfferAcceptance_Click(object sender, EventArgs e)
    {
        try
        {
            long CandidateID = 0;
            string sMessage = string.Empty;

            
                objCandidates = new clsCandidate();
                objCandidates.CandidateID = ViewState["CandidateID"].ToInt64();

                if (rblOffer.SelectedValue == "1")
                    objCandidates.OfferStatusID = (int)CandidateStatus.OfferAccepted;
                else
                    objCandidates.OfferStatusID = (int)CandidateStatus.OfferRejected;

                objCandidates.Remarks = txtRejectRemarks.Text;
             
                CandidateID = objCandidates.UpdateOfferStatus();

                if (CandidateID > 0)
                {
                    if (rblOffer.SelectedValue == "1")
                    {
                        ancOfferAcceptance.Enabled = false;
                        lblOfferStatus.Text = clsGlobalization.IsArabicCulture()? "قبلت العرض":"Offer Accepted";
                        lblOfferRemarks.Text = txtRejectRemarks.Text;
                        sMessage = clsGlobalization.IsArabicCulture() ? " قبلت العرض بنجاح" : "Offer accepted successfully.";
                    }
                    else
                    {
                        ancOfferAcceptance.Enabled = false;
                        lblOfferStatus.Text = clsGlobalization.IsArabicCulture() ? "رفض العرض" : "Offer Rejected";
                        lblOfferRemarks.Text = txtRejectRemarks.Text;
                        sMessage = clsGlobalization.IsArabicCulture() ? "رفض العرض بنجاح" : "Offer rejected successfully.";
                    }
                    msgs.InformationalMessage(sMessage);
                    ancOfferAcceptance.Enabled = false;
                    ancOfferAcceptance.OnClientClick = "return false;";

                }
               
         

        }
        catch
        {
        }

    }
}

