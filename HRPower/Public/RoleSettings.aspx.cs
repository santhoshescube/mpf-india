﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;

public partial class Public_RoleSettings : System.Web.UI.Page
{
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaster;
    private string CurrentSelectedValue { get; set; } 

    protected void Page_Load(object sender, EventArgs e)
    {
        this.ReferenceControlNew1.ModalPopupID = this.mdlPopUpReference.ID;
        this.CurrentSelectedValue = "-1";

        if (!IsPostBack)
        {
            objRoleSettings = new clsRoleSettings();
            objUserMaster = new clsUserMaster();


            DataTable dt = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.RolesHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();


            //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.RoleSettings))
            //{
            //    divRole.Visible = true;

            //    //rcSelectRole.FillList();
            //    //rcSelectRole.ChangeIndex(2);

            //    this.FillRoles();

            //    if (rcSelectRole.Items.Count > 2)
            //        this.rcSelectRole.SelectedIndex = 2;

            //    BindDataList();
            //}
            //else
            //{
            //    divRole.Visible = false;

            //    lblNoData.Text = "You dont have enough permission to view this page.";
            //}
        }
        this.RegisterReferenceControlEvents();
    }


    public void SetPermission()
    {

        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        int UserID;
        UserID = objUserMaster.GetRoleId();
        if (UserID != 1 && UserID != 2 && UserID != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                if (dtm.Rows[0]["IsCreate"].ToBoolean() == true || dtm.Rows[0]["IsUpdate"].ToBoolean() == true)
                {
                    btnSave.Enabled = true;
                }
                else
                {
                    btnSave.Enabled = false;
                }
                btnRole.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();

                if (dtm.Rows[0]["IsView"].ToBoolean() == true || btnSave.Enabled == true)
                {
                    this.FillRoles();
                    BindDataList();
                }

                if (dtm.Rows[0]["IsCreate"].ToBoolean() == true || dtm.Rows[0]["IsUpdate"].ToBoolean() == true || dtm.Rows[0]["IsView"].ToBoolean() == true || dtm.Rows[0]["IsDelete"].ToBoolean() == true)
                {
                    this.FillRoles();
                    BindDataList();
                }
                else
                {
                    divRole.Visible = false;

                    lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("ليس لديك إذن ما يكفي لعرض هذه الصفحة.") : ("You dont have enough permission to view this page.");
                    lblNoData.Visible = true ;
                }
            }
            else
            {
                divRole.Visible = false;

                lblNoData.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("ليس لديك إذن ما يكفي لعرض هذه الصفحة.") : ("You dont have enough permission to view this page.");
            }
        }
        else
        {
            btnSave.Enabled = true;
            this.FillRoles();
            BindDataList();
        }
    }

    private void RegisterReferenceControlEvents()
    {
        this.ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        this.ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    private void BindDataList()
    {
        Int32 iModuleId = 0;
        objRoleSettings = new clsRoleSettings();

        dlModules.DataSource = objRoleSettings.GetAllModules();
        dlModules.DataBind();

        if (rcSelectRole.SelectedValue.ToInt32() <= 5)
        {
            CheckBox chkSelectAll = (CheckBox)dlModules.Controls[0].FindControl("chkSelectAll");

            chkSelectAll.Checked = true;
            chkSelectAll.Enabled = false;

            foreach (DataListItem Item in dlModules.Items)
            {
                CheckBox chkModule = (CheckBox)Item.FindControl("chkModules");

                chkModule.Checked = true;
                chkModule.Enabled = false;
                
                iModuleId = Convert.ToInt32(dlModules.DataKeys[Item.ItemIndex]);
                DataList dlMenus = (DataList)Item.FindControl("dlMenus");

                foreach (DataListItem MenuItem in dlMenus.Items)
                {
                    CheckBox chkIsEnabled = (CheckBox)MenuItem.FindControl("chkIsEnabled");
                    CheckBox chkIsView = (CheckBox)MenuItem.FindControl("chkIsView");
                    CheckBox chkIsCreate = (CheckBox)MenuItem.FindControl("chkIsCreate");
                    CheckBox chkIsUpdate = (CheckBox)MenuItem.FindControl("chkIsUpdate");
                    CheckBox chkIsDelete = (CheckBox)MenuItem.FindControl("chkIsDelete");
                    CheckBox chkIsPrintEmail = (CheckBox)MenuItem.FindControl("chkIsPrintEmail");

                    //if (rcSelectRole.SelectedValue.ToInt32() != 4 && rcSelectRole.SelectedValue.ToInt32() != 5)
                    //{
                        chkIsEnabled.Visible = false;
                        chkIsEnabled.Checked = true;
                        chkIsEnabled.Enabled = false;

                        chkIsView.Checked = true;
                        chkIsView.Enabled = false;

                        chkIsCreate.Checked = true;
                        chkIsCreate.Enabled = false;

                        chkIsUpdate.Checked = true;
                        chkIsUpdate.Enabled = false;


                        chkIsDelete.Checked = true;
                        chkIsDelete.Enabled = false;

                        chkIsPrintEmail.Checked = true;
                        chkIsPrintEmail.Enabled = false;
                    //}

                    //if (rcSelectRole.SelectedValue.ToInt32() == 4 && (iModuleId == 10 || iModuleId == 12))
                    //{
                    //    chkModule.Enabled = true;
                    //    chkIsEnabled.Enabled = true;
                    //    chkIsView.Enabled = true;
                    //    chkIsCreate.Enabled = true;
                    //    chkIsUpdate.Enabled = true;
                    //    chkIsDelete.Enabled = true;
                    //    chkIsPrintEmail.Enabled = true;
                    //}
                    //else if (rcSelectRole.SelectedValue.ToInt32() == 4 )
                    //{
                    //    chkModule.Enabled = false;
                    //    chkModule.Checked =false;
                    //    chkModule.Enabled = false;
                    //    chkIsEnabled.Enabled = false;
                    //    chkIsView.Enabled = false;
                    //    chkIsCreate.Enabled = false;
                    //    chkIsUpdate.Enabled = false;
                    //    chkIsDelete.Enabled = false;
                    //    chkIsPrintEmail.Enabled = false;

                    //    chkModule.Checked = false;
                    //    chkIsEnabled.Checked = false;
                    //    chkIsView.Checked = false;
                    //    chkIsCreate.Checked = false;
                    //    chkIsUpdate.Checked = false;
                    //    chkIsDelete.Checked = false;
                    //    chkIsPrintEmail.Checked = false;
                    //}

//----------------------------------------------------------------------------------------------------------------------------------
                    //if (rcSelectRole.SelectedValue.ToInt32() == 5 &&  iModuleId == 12)
                    //{
                    //    chkModule.Enabled = true;
                    //    chkIsEnabled.Enabled = true;
                    //    chkIsView.Enabled = true;
                    //    chkIsCreate.Enabled = true;
                    //    chkIsUpdate.Enabled = true;
                    //    chkIsDelete.Enabled = true;
                    //    chkIsPrintEmail.Enabled = true;
                    //}
                    //else if (rcSelectRole.SelectedValue.ToInt32() == 5)
                    //{
                    //    chkModule.Enabled = false;
                    //    chkModule.Checked = false;


                    //    chkModule.Enabled = false;
                    //    chkIsEnabled.Enabled = false;
                    //    chkIsView.Enabled = false;
                    //    chkIsCreate.Enabled = false;
                    //    chkIsUpdate.Enabled = false;
                    //    chkIsDelete.Enabled = false;
                    //    chkIsPrintEmail.Enabled = false;


                        chkModule.Checked = false;
                        chkIsEnabled.Checked = false;
                        chkIsView.Checked = false;
                        chkIsCreate.Checked = false;
                        chkIsUpdate.Checked = false;
                        chkIsDelete.Checked = false;
                        chkIsPrintEmail.Checked = false;









                }
            }
        }
        else
            SetEnability();

        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);

    }
    protected void SetEnability()
    {
        bool bModuleFlag = true;

        CheckBox chkSelectAll = (CheckBox)dlModules.Controls[0].FindControl("chkSelectAll");

        foreach (DataListItem Item in dlModules.Items)
        {
            DataList dlMenus = (DataList)Item.FindControl("dlMenus");

            CheckBox chkModule = (CheckBox)Item.FindControl("chkModules");

            bool bMenuflag = false;

            foreach (DataListItem MenuItem in dlMenus.Items)
            {
                CheckBox chkIsEnabled = (CheckBox)MenuItem.FindControl("chkIsEnabled");
                CheckBox chkIsView = (CheckBox)MenuItem.FindControl("chkIsView");
                CheckBox chkIsCreate = (CheckBox)MenuItem.FindControl("chkIsCreate");
                CheckBox chkIsUpdate = (CheckBox)MenuItem.FindControl("chkIsUpdate");
                CheckBox chkIsDelete = (CheckBox)MenuItem.FindControl("chkIsDelete");
                CheckBox chkIsPrintEmail = (CheckBox)MenuItem.FindControl("chkIsPrintEmail");

                if (chkIsView.Checked || chkIsCreate.Checked || chkIsUpdate.Checked || chkIsDelete.Checked || chkIsPrintEmail.Checked)
                {
                    bMenuflag = true;
                    chkIsEnabled.Checked = true;
                }
            }

            chkModule.Checked = bMenuflag;

            if (!chkModule.Checked) bModuleFlag = false;
        }
        chkSelectAll.Checked = bModuleFlag;

    }

    protected void rcSelectRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindDataList();
            uplMenus.Update();

            if (rcSelectRole.SelectedIndex != 0)
                upnlMsg.Update();

            upButton.Update();

        }
        catch { }
    }

    public void rcSelectRole_Update()
    {
        BindDataList();
        uplMenus.Update();


        upButton.Update();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
         objRoleSettings = new clsRoleSettings();

        objRoleSettings.RoleID = Convert.ToInt32(rcSelectRole.SelectedValue);
        objRoleSettings.DeleteRoleDetails();

        foreach (DataListItem Item in dlModules.Items)
        {
            DataList dlMenus = (DataList)Item.FindControl("dlMenus");

            objRoleSettings.ModuleId = Convert.ToInt32(dlModules.DataKeys[Item.ItemIndex]);

            foreach (DataListItem MenuItem in dlMenus.Items)
            {
                CheckBox chkIsEnabled = (CheckBox)MenuItem.FindControl("chkIsEnabled");
                CheckBox chkIsCreate = (CheckBox)MenuItem.FindControl("chkIsCreate");
                CheckBox chkIsView = (CheckBox)MenuItem.FindControl("chkIsView");
                CheckBox chkIsUpdate = (CheckBox)MenuItem.FindControl("chkIsUpdate");
                CheckBox chkIsDelete = (CheckBox)MenuItem.FindControl("chkIsDelete");
                CheckBox chkIsPrintEmail = (CheckBox)MenuItem.FindControl("chkIsPrintEmail");

                objRoleSettings.MenuID = Convert.ToInt32(dlMenus.DataKeys[MenuItem.ItemIndex]);

                objRoleSettings.IsEnabled = chkIsEnabled.Checked;
                objRoleSettings.IsCreate = chkIsCreate.Checked;
                objRoleSettings.IsUpdate = chkIsUpdate.Checked;
                objRoleSettings.IsDelete = chkIsDelete.Checked;
                objRoleSettings.IsPrintEmail = chkIsPrintEmail.Checked;

                if (objRoleSettings.IsCreate == true || objRoleSettings.IsUpdate == true)
                {
                    objRoleSettings.IsView = true;
                }
                else
                {
                    objRoleSettings.IsView = chkIsView.Checked;
                }
                objRoleSettings.InsertRoleDetails();
            }
        }

        if (dlModules.Items.Count != 0)
        {
            msgs.InformationalMessage("Role details saved successfully");
            mpeMessage.Show();
        }
    }

    protected void dlModules_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HtmlContainerControl divMenus = (HtmlContainerControl)e.Item.FindControl("divMenus");

            HtmlContainerControl tblMenus = (HtmlContainerControl)e.Item.FindControl("tblMenus");

            tblMenus.Attributes.Add("onclick", string.Format("toggleMenu('{0}');", divMenus.ClientID));

            objRoleSettings = new clsRoleSettings();

            DataList dlMenus = (DataList)e.Item.FindControl("dlMenus");

            objRoleSettings.ModuleId = Convert.ToInt32(dlModules.DataKeys[e.Item.ItemIndex]);
            objRoleSettings.RoleID = rcSelectRole.SelectedValue.ToInt32();

            dlMenus.DataSource = objRoleSettings.GetAllEnability();
            dlMenus.DataBind();
        }
    }

    protected void btnRole_Click(object sender, EventArgs e)
    {
        try
        {

            this.ReferenceControlNew1.ClearAll();
            this.ReferenceControlNew1.TableName = "RoleReference";
            this.ReferenceControlNew1.DataTextField = "RoleName";
            this.ReferenceControlNew1.DataValueField = "RoleID";
            this.ReferenceControlNew1.FunctionName = "FillRoles";
            this.ReferenceControlNew1.SelectedValue = rcSelectRole.SelectedValue;
            this.ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("الأدوار") : ("Role");
            this.ReferenceControlNew1.ExcludeFiled = "RoleID";
            this.ReferenceControlNew1.ExcludeValues = "1";
            this.ReferenceControlNew1.DataTextFieldArabic = "RoleNameArb";
            this.ReferenceControlNew1.PopulateData();

            this.mdlPopUpReference.Show();
            this.updModalPopUp.Update();

        }
        catch
        {
        }
    }

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource, string insertText)
    {
        ddwn.DataValueField = dataValueField;
        ddwn.DataTextField = dataTextField;
        ddwn.DataSource = dataSource;
        ddwn.DataBind();

        if (insertText != null && insertText.Trim() != string.Empty)
            ddwn.Items.Insert(0, new ListItem(insertText, "-1"));
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    private void SetIndex(DropDownList ddwn, int Value)
    {
        if (ddwn != null && ddwn.Items.FindByValue(Value.ToString()) != null)
            ddwn.SelectedValue = Value.ToString();
    }

    private void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        this.CurrentSelectedValue = SelectedValue;
        System.Reflection.MethodInfo method = this.GetType().GetMethod(functionName);

        if (method != null)
            method.Invoke(this, null);
    }

    public void FillRoles()
    {
        try
        {
            if (rcSelectRole.SelectedIndex != -1)
            {
                this.CurrentSelectedValue = Convert.ToString(rcSelectRole.SelectedValue);
            }
            this.BindDropDown(rcSelectRole, "RoleName", "RoleID", new clsBindComboBox().GetRoles(), "Select");
            this.SetSelectedIndex(rcSelectRole);
            this.upnlRole.Update();
        }
        catch
        {
        }
    }
    
}
