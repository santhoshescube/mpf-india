﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_HomePendingRequest : System.Web.UI.Page
{
    clsHomePage objHomePage;
    clsUserMaster objUserMaster;
    clsEmployee objEmployee;   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["MessageType"] = null;
            FillStatus();
            FillCombo("-1");
            ddlStatus.SelectedValue = "1";
            if (Request.QueryString["RequestType"] != null)
            {
                ddlStatus.SelectedValue =(Session["StatusID"].ToInt32() == 10 ?"-1": Convert.ToString(Session["StatusID"]));
                ddlRequestType.SelectedIndex = ddlRequestType.Items.IndexOf(ddlRequestType.Items.FindByText(Request.QueryString["RequestType"]));             
                ddlRequestType_SelectedIndexChanged(sender, e);
            }
            else
                BindPendingRequests();
        }
    }

    private void FillStatus()
    {
        DataTable dt = clsHomePage.FillStatus();
        DataRow dr = dt.NewRow();
        dr["Status"] = clsGlobalization.IsArabicCulture() ? "أحالت" : "Forwarded";
        dr["StatusId"] = "10";
        dt.Rows.Add(dr);
        ddlStatus.DataSource = dt;
        ddlStatus.DataBind();
    }

    protected void BindPendingRequests()
    {
        lblMessage.Style["display"] = "none";
        objHomePage = new clsHomePage();
        objUserMaster = new clsUserMaster();
        int StatusId = -1;
        //if ((Session["MessageType"] != null))
        //    StatusId = SetRequestStatus();
        StatusId = ddlStatus.SelectedValue.ToInt32();
        DataSet ds = objHomePage.GetPendingRequests(objUserMaster.GetEmployeeId(), ddlCompany.SelectedValue.ToInt32(), pgrPendingRequests.CurrentPage + 1, pgrPendingRequests.PageSize, ddlRequestType.SelectedValue, (txtRequestedDate.Text == string.Empty ? "" : clsCommon.Convert2DateTime(txtRequestedDate.Text).ToString("dd MMM yyyy")), (txtRequestedToDate.Text == string.Empty ? "" : clsCommon.Convert2DateTime(txtRequestedToDate.Text).ToString("dd MMM yyyy")), Convert.ToInt32(ddlRequestBy.SelectedValue), StatusId, GetReqTypeID(Convert.ToString(ddlRequestType.Text)));

        if (ds.Tables[0].Rows.Count == 0)
        {
            dlPendingRequests.ShowHeader = false;
            pgrPendingRequests.Total = 0;
        }
        else
        {
            pgrPendingRequests.Total = Convert.ToInt32(ds.Tables[0].Rows[0]["Total"]);
            dlPendingRequests.ShowHeader = true;
        }

        dlPendingRequests.DataSource = ds;
        dlPendingRequests.DataBind();

        if (dlPendingRequests.Items.Count == 0)
        {
            lblMessage.Style["display"] = "block";
            lblMessage.Text = clsGlobalization.IsArabicCulture() ? " لا يطلب قيد الانتظار" : "No Pending Requests";
        }
        //upPendingRequests.Update();
    }

    private void FillCombo(string ReqType)
    {
        objUserMaster = new clsUserMaster();
        objEmployee = new clsEmployee();
        objHomePage = new clsHomePage();

        if (ReqType == "-1")
        {
            ddlRequestBy.DataSource = objHomePage.GetRequestedByType(objUserMaster.GetEmployeeId(), "-1", (txtRequestedDate.Text == string.Empty ? "" : clsCommon.Convert2DateTime(txtRequestedDate.Text).ToString("dd MMM yyyy")), -1, -1);//objHomePage.GetAllRequestedBy(objUserMaster.GetEmployeeId());
            ddlRequestBy.DataBind();
        }
        else
        {

            ddlRequestBy.DataSource = objHomePage.GetRequestedByType(objUserMaster.GetEmployeeId(), ddlRequestType.SelectedValue, (txtRequestedDate.Text == string.Empty ? "" : clsCommon.Convert2DateTime(txtRequestedDate.Text).ToString("dd MMM yyyy")), Convert.ToInt32(ddlRequestBy.SelectedValue), ddlStatus.SelectedValue.ToInt32());
            ddlRequestBy.DataBind();
        }        
        ddlRequestBy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "أي الطالب" : "Any Requestor", "-1"));

        DataTable dtCompany = new clsCompany().GetUserCompanies(objUserMaster.GetEmployeeId());
        DataRow dr = dtCompany.NewRow();

        //dr["CompanyName"] = "Any";
        //dr["CompanyID"] = -1;
        //dtCompany.Rows.Add(dr);

        ddlCompany.DataSource = dtCompany;
        ddlCompany.DataTextField = "CompanyName";
        ddlCompany.DataValueField = "CompanyID";
        ddlCompany.DataBind();
    }

    public string GetReuestStatus(object oStatusId, object oForwarded)
    {
        string sStatus = string.Empty;
        string sForwarded = Convert.ToString(oForwarded);

        if (sForwarded == "Forwarded" && Convert.ToInt32(oStatusId) == 1)
            sStatus = "Forwarded";
        else
        {
            switch (Convert.ToInt32(oStatusId))
            {
                case 1:
                    sStatus = clsGlobalization.IsArabicCulture() ? "تطبيق" : "Applied";
                    break;

                case 2:
                    sStatus = clsGlobalization.IsArabicCulture() ? "تحويل" : "Processing";
                    break;

                case 3:
                    sStatus = clsGlobalization.IsArabicCulture() ? "مرفوض" : "Rejected";
                    break;

                case 4:
                    sStatus = clsGlobalization.IsArabicCulture() ? "ريثما" : "Pending";
                    break;
                case 5:
                    sStatus = clsGlobalization.IsArabicCulture() ? "وافق" : "Approved";
                    break;
                case 6:
                    sStatus = clsGlobalization.IsArabicCulture() ? "ألغي" : "Cancelled";
                    break;
                case 7:
                    sStatus = clsGlobalization.IsArabicCulture() ? "طلب إلغاء" : "RequestForCancel";
                    break;

            }
        }


        return sStatus;
    }

    //protected string GetRequestLink(object ReqType, object RequestId, object StatusId)
    //{
    //    string url = string.Empty;
    //    string CancelQueryString = string.Empty;
    //    if ((int)StatusId == 5 || (int)StatusId == 6 || (int)StatusId == 3)
    //        return "#";
    //    else if ((int)StatusId == 7)
    //        CancelQueryString = "&Type=Cancel";
    //    switch (Convert.ToString(ReqType).ToTitleCase())
    //    {
    //        case "Leave":
    //        case "ترك":
    //            url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Attendance":
    //        case "الحضور":
    //            url = "AttendanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Vacation Leave":
    //        case "الاجازه":
    //            url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Loan":
    //        case "قرض":
    //            url = "LoanRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Salary Advance":
    //        case "الراتب مقدما":
    //            url = "SalaryAdvanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Transfer":
    //        case "نقل":
    //            url = "TransferRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Travel":
    //            url = "TravelRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Document":
    //        case "وثيقة":
    //            url = "DocumentRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Visa":
    //            url = "VisaRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Extension":
    //        case "TimeExtension":
    //        case "Compensatory Off":
    //        case "ارشادية":
    //        case "تمديد الوقت":
    //        case "التعويضية":
    //            url = "LeaveExtensionRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Time Sheet":
    //            url = "TimeSheet.aspx?RequestId=" + RequestId;
    //            break;

    //        case "Training":
    //            url = "TrainingRequest.aspx?RequestId=" + RequestId + CancelQueryString; ;
    //            break;

    //        case "Hiring Plan":
    //            url = "HiringPlans.aspx?RequestId=" + RequestId;
    //            break;


    //        case "Resignation":
    //        case "Ticket":
    //        case "استقالة":
    //        case "تذكرة":
    //            url = "GeneralRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;
    //        case "Expense":
    //        case "نفقة":
    //            url = "Expense.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;
    //        case "Offer Letter":
    //            url = "OfferLetter.aspx?OfferID=" + RequestId;
    //            break;
    //    }

    //    return url;
    //}


    //protected string GetRequestLink(object ReqType, object RequestId, object StatusId)
    //{
    //    string url = string.Empty;


    //    string CancelQueryString = string.Empty;
    //    if ((int)StatusId == 5 || (int)StatusId == 6 || (int)StatusId == 3)
    //        return "#";
    //    else if ((int)StatusId == 7)
    //        CancelQueryString = "&Type=Cancel";
    //    switch (Convert.ToString(ReqType).ToTitleCase())
    //    {

    //        case "Vacancy":

    //            url = "Job.aspx?JobId=" + RequestId + "&Approval=true";

    //            break;

    //        case "Leave":
    //        case "ترك":
    //            url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Attendance":
    //        case "الحضور":
    //            url = "AttendanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Vacation Leave":
    //        case "الاجازه":
    //            url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Loan":
    //        case "قرض":
    //            url = "LoanRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Salary Advance":
    //        case "الراتب مقدما":
    //            url = "SalaryAdvanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Transfer":
    //        case "نقل":
    //            url = "TransferRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Travel":
    //            url = "TravelRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Document":
    //        case "وثيقة":
    //            url = "DocumentRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Visa":
    //            url = "VisaRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Leave Extension":
    //        case "Time Extension":
    //        case "Compensatory Off":
    //        case "ارشادية":
    //        case "تمديد الوقت":
    //        case "التعويضية":
    //            url = "LeaveExtensionRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;

    //        case "Time Sheet":

    //            url = "TimeSheet.aspx?RequestId=" + RequestId;

    //            break;

    //        case "Training":
    //            url = "TrainingRequest.aspx?RequestId=" + RequestId + CancelQueryString;
    //            break;

    //        case "Hiring Plan":
    //            url = "HiringPlans.aspx?RequestId=" + RequestId;

    //            break;

    //        case "Resignation":
    //        case "Ticket":
    //        case "استقالة":
    //        case "تذكرة":
    //            url = "GeneralRequest.aspx?RequestId=" + RequestId + CancelQueryString;

    //            break;
    //        case "Expense":
    //        case "نفقة":
    //            url = "Expense.aspx?RequestId=" + RequestId + CancelQueryString; ;
    //            break;
    //        case "Offer Letter":

    //            url = "OfferLetter.aspx?OfferID=" + RequestId;
    //            break;
    //    }

    //    return url;
    //}


    protected string GetRequestLink(object ReqType, object RequestId, object StatusId)
    {
        string url = string.Empty;


        string CancelQueryString = string.Empty;
        if ((int)StatusId == 5 || (int)StatusId == 6 || (int)StatusId == 3)
            return "#";
        else if ((int)StatusId == 7)
            CancelQueryString = "&Type=Cancel";
        switch (Convert.ToString(ReqType).ToTitleCase())
        {

            case "Vacancy":
            case "وظيفة":
                url = "VacancyAddEdit.aspx?JobId=" + RequestId + "&Approval=true";

                break;

            case "Leave":
            case "ترك":
                url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;
            case "Attendance":
            case "الحضور":
                url = "AttendanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;
            case "Vacation Leave":
            case "الاجازه":
                url = "LeaveRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;
            case "Loan":
            case "قرض":
                url = "LoanRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;

            case "Salary Advance":
            case "الراتب مقدما":
                url = "SalaryAdvanceRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;

            case "Transfer":
            case "نقل":
                url = "TransferRequest.aspx?RequestId=" + RequestId + CancelQueryString;
                break;

            case "Travel":
            case "سفر":
                url = "TravelRequest.aspx?RequestId=" + RequestId + CancelQueryString;
                break;

            case "Document":
            case "وثيقة":
                url = "DocumentRequest.aspx?RequestId=" + RequestId + CancelQueryString;
                break;

            case "Visa":
            case "تأشيرة":
                url = "VisaRequest.aspx?RequestId=" + RequestId + CancelQueryString;
                break;

            case "Leave Extension":
            case "Time Extension":
            case "Vacation Extension":
            case "ارشادية":
            case "تمديد الوقت":
            case "التعويضية":
                url = "LeaveExtensionRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;

            case "Time Sheet":

                url = "TimeSheet.aspx?RequestId=" + RequestId;

                break;

            case "Training":
                url = "TrainingRequest.aspx?RequestId=" + RequestId + CancelQueryString;
                break;

            case "Hiring Plan":
                url = "HiringPlans.aspx?RequestId=" + RequestId;

                break;

            case "Resignation":
            case "Rejoin":
            case "Ticket":
            case "Asset":
            case "General":
            case "Salary Certificate":
            case "Salary Bank Statement":
            case "Salary Card Lost":
            case "Salary Slip":
            case "Visa Letter":
            case "Noc-Umarah Visa":
            case "Card Lost":
            case "استقالة":
            case "تذكرة":
            case "الأصول":
            case "عام":
                url = "GeneralRequest.aspx?RequestId=" + RequestId + CancelQueryString;

                break;
            case "Expense":
            case "نفقة":
                url = "Expense.aspx?RequestId=" + RequestId + CancelQueryString; ;
                break;
            case "Offer Letter":
            case "خطاب العرض":
                url = "OfferLetter.aspx?OfferID=" + RequestId;
                break;
        }

        return url;
    }


    public bool IsApproveVisible(object RequestID, object oStatusId, object Forwarded, object ReqType,object RequestedBy)
    {
        if (ReqType.ToString().Trim() == "Offer Letter") return true;

        objUserMaster = new clsUserMaster();
        int EmployeeID = objUserMaster.GetEmployeeId();

        if ((int)oStatusId == 5 || (int)oStatusId == 3 || (int)oStatusId == 6) // Approved cancelled Rejected pending
            return false;
        else
        {
            //Previous code 
            //string Type = GetReuestStatus((int)oStatusId, Convert.ToString(Forwarded));

            //if ((int)oStatusId == 4 && !(clsLeaveRequest.IsHigherAuthority(GetReqTypeID(Convert.ToString(ReqType)), EmployeeID)))
            //    return false;
            //else
            //    return true;

            bool result = clsCommonMessage.CanShowApproveButton(RequestID.ToInt32(), ReqType.ToInt32(), EmployeeID, RequestedBy.ToInt32());
            return result;


            //(Type == "Forwarded" || Type == "RequestForCancel") && 
            //if (!(clsLeaveRequest.IsHigherAuthority(GetReqTypeID(Convert.ToString(ReqType)), EmployeeID)))
            //{
            //    return false;
            //}
            //else
            //    return true;
        }
    }

    //private int GetReqTypeID(string ReqType)
    //{
    //    switch (Convert.ToString(ReqType))
    //    {
    //        case "Leave":
    //            return (int)RequestType.Leave;
    //        case "Attendance":
    //            return (int)RequestType.AttendanceRequest;
    //        case "Vacation Leave":
    //            return (int)RequestType.Leave;              
    //        case "Loan":
    //            return (int)RequestType.Loan ;              
    //        case "Salary Advance":
    //            return (int)RequestType.SalaryAdvance ;
    //        case "Transfer":
    //            return (int)RequestType.Transfer;
    //        case "Document":
    //            return (int)RequestType.Document;
    //        case "Visa":
    //            return (int)RequestType.Visa;
    //        case "Extension":
    //        case "Time Extension":
    //        case "Compensatory Off":
    //            return (int)RequestType.LeaveExtension;
    //        case "Training":
    //            return (int)RequestType.Training;
    //        case "Resignation":
    //            return (int)RequestType.Resignation;
    //        case "Ticket":
    //            return (int)RequestType.Ticket;
    //        case "Expense":
    //            return (int)RequestType.Expense;
    //        case "Travel":
    //            return (int)eReferenceTypes.TravelRequest;


    //        default:
    //            return -1;
    //    }
    //}


    //private int GetReqTypeID(string ReqType)
    //{
    //    switch (Convert.ToString(ReqType))
    //    {
    //        case "Leave":
    //            return (int)RequestType.Leave;
    //        case "Attendance":
    //            return (int)RequestType.AttendanceRequest;

    //        case "Vacation Leave":
    //            return (int)RequestType.Leave;

    //        case "Loan":
    //            return (int)RequestType.Loan;

    //        case "Salary Advance":
    //            return (int)RequestType.SalaryAdvance;

    //        case "Transfer":
    //            return (int)RequestType.Transfer;

    //        case "Document":
    //            return (int)RequestType.Document;

    //        case "Visa":
    //            return (int)RequestType.Visa;

    //        case "Extension":
    //        case "Time Extension":
    //        case "Compensatory Off":
    //            return (int)RequestType.LeaveExtension;

    //        case "Training":
    //            return (int)RequestType.Training;

    //        case "Resignation":
    //            return (int)RequestType.Resignation;
    //        case "Ticket":
    //            return (int)RequestType.Ticket;

    //        case "Expense":
    //            return (int)RequestType.Expense;


    //        case "Vacancy":
    //            return (int)eReferenceTypes.JobApproval;


    //        case "Travel":
    //            return (int)eReferenceTypes.TravelRequest;


    //        default:
    //            return -1;
    //    }
    //}

    private int GetReqTypeID(string ReqType)
    {
        switch (Convert.ToString(ReqType))
        {
            case "LEAVE":
            case "ترك":
                return (int)RequestType.Leave;
            case "ATTENDANCE":
            case "الحضور":
                return (int)RequestType.AttendanceRequest;

            case "VACATION LEAVE":
            case "الاجازه":
                return (int)RequestType.VacationLeaveRequest;

            case "LOAN":
            case "قرض":
                return (int)RequestType.Loan;

            case "SALARY ADVANCE":
            case "الراتب مقدما":
                return (int)RequestType.SalaryAdvance;

            case "TRANSFER":
            case "نقل":
                return (int)RequestType.Transfer;

            case "DOCUMENT":
            case "وثائق":
                return (int)RequestType.Document;

            case "VISA":
                return (int)RequestType.Visa;

            case "TIME EXTENSION":
            case "تمديد الوقت":
                return (int)RequestType.TimeExtensionRequest;

            case "LEAVE EXTENSION":
            case "ترك التمديد":
                return (int)RequestType.LeaveExtension;

            case "VACATION EXTENSION":
            case "عطلة التمديد":
                return (int)RequestType.VacationExtensionRequest;

            case "COMPENSATORY OFF":
            case "التعويضية":
                return (int)RequestType.CompensatoryOffRequest;

            case "TRAINING":
                return (int)RequestType.Training;

            case "RESIGNATION":
            case "استقالة":
                return (int)RequestType.Resignation;

            //case "REJOIN":
           
            //    return (int)RequestType.Rejoin;

           case "SALARY CERTIFICATE":

                return (int)RequestType.SalaryCertificate;

           case "SALARY BANK STATEMENT":

                return (int)RequestType.SalaryBankStatement;

           case "SALARY CARD LOST":

                return (int)RequestType.SalaryCardLost;
           case "SALARY SLIP":

                return (int)RequestType.Salaryslip;
           case "VISA LETTER":

                return (int)RequestType.VisaLetter;
           case "NOC-UMARAH VISA ":

                return (int)RequestType.NocUmarahVisa;

           case "CARD LOST ":

                return (int)RequestType.CardLost;
            case "TICKET":
            case "تذكرة":
                return (int)RequestType.Ticket;

            case "ASSET":
            case "الأصول":
                return (int)RequestType.Asset;
            case "GENERAL":
            case "عام":
                return (int)RequestType.General;

            case "EXPENSE":
            case "نفقة":
                return (int)RequestType.Expense;


            case "VACANCY":
            case "وظيفة":
                return (int)eReferenceTypes.JobApproval;


            case "TRAVEL":
            case "السفر":
                return (int)eReferenceTypes.TravelRequest;


            default:
                return -1;
        }
    }
    protected void txtRequestedDate_TextChanged(object sender, EventArgs e)
    {
        pgrPendingRequests.CurrentPage = 0;
        BindPendingRequests();
    }
    protected void ddlRequestType_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPendingRequests.CurrentPage = 0;
        ddlRequestBy.SelectedIndex = -1;
        FillCombo(ddlRequestType.SelectedValue);
        BindPendingRequests();
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPendingRequests.CurrentPage = 0;
        FillCombo(ddlRequestType.SelectedValue);
        BindPendingRequests();
    }

    protected void ddlRequestBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        pgrPendingRequests.CurrentPage = 0;
        BindPendingRequests();

    }
    protected void dlPendingRequests_PreRender(object sender, EventArgs e)
    {
        if (dlPendingRequests.Items.Count == 0)
            pgrPendingRequests.Visible = false;
        else
            pgrPendingRequests.Visible = true;

    }



    #region RightMenuEvents
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {

        TableCell cell = (TableCell)e.Cell;
        cell.Width = Unit.Pixel(30);
        cell.Height = Unit.Pixel(30);
        if (e.Day.Date.ToString("dd MMM yyyy") == System.DateTime.Now.Date.ToString("dd MMM yyyy"))
        {
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Red;
            //cell.CssClass = "Currendate";
        }
        //cell.BorderStyle = BorderStyle.Solid;
        //cell.BorderWidth = Unit.Pixel(1);
        //cell.BorderColor = System.Drawing.Color.Black;
        cell.Style["font-size"] = "10px";
        //cell.Style["padding"] = "0";
        cell.Attributes.Add("title", "");
        if (e.Day.IsOtherMonth)
        {
            cell.Text = "";
            cell.CssClass = "tblBackground1";

        }
        else
        {

            objHomePage = new clsHomePage();
            objUserMaster = new clsUserMaster();

            DataSet ds = objHomePage.GetCalendarDetails(objUserMaster.GetEmployeeId(), e.Day.Date.ToString("dd MMM yyyy"));
            cell.CssClass = "tblBackground1";
            string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : " Holiday:");
            if (ds.Tables[0].Rows.Count > 0)
            {
                sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                cell.ForeColor = System.Drawing.Color.Red;
            }
            string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : " Events: \n");
            int icount = 1;
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                icount++;
            }
            if (sEvent != string.Empty)
                cell.ForeColor = System.Drawing.Color.Green;
            if (sEvent != string.Empty && sHoliday != string.Empty)
                cell.ForeColor = System.Drawing.Color.FromArgb(244, 166, 29);
            //cell.ForeColor = System.Drawing.Color.Magenta;
            string sTitle = (sHoliday == string.Empty ? string.Empty : sHoliday) + (sEvent == string.Empty ? string.Empty : sEvent);
            if (sTitle != string.Empty)
                cell.Attributes.Add("title", sTitle);
        }
    }
    #endregion RightMenuEvents
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindPendingRequests();
    }
    protected void txtRequestedToDate_TextChanged(object sender, EventArgs e)
    {
        pgrPendingRequests.CurrentPage = 0;
        BindPendingRequests();
    }

    protected void dlPendingRequests_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "_ViewReason")
        {
            objHomePage = new clsHomePage();

            UpdatePanel upLink = (UpdatePanel)e.Item.FindControl("upLink");
            if (upLink == null) return;
            LinkButton lbReason = (LinkButton)upLink.FindControl("lbReason");
            HiddenField hfLink = (HiddenField)upLink.FindControl("hfLink");

            UpdatePanel upViewDetails = (UpdatePanel)e.Item.FindControl("upViewDetails");
            GridView gvReasons = (GridView)upViewDetails.FindControl("gvReasons");
            if (upViewDetails == null) return;
            if (lbReason != null)
            {
                if (Convert.ToInt32(hfLink.Value) == 0)
                {
                    hfLink.Value = "1";
                    int EssFormReferenceID = -1;

                    switch (Convert.ToString(e.CommandArgument).Split(',')[1].ToInt32())
                    {
                        case 1:
                        case 18:
                        case 19:
                            EssFormReferenceID = (int)essReferenceTypes.Leave;
                            break;
                        case 2:
                            EssFormReferenceID = (int)essReferenceTypes.Loan;
                            break;
                        case 3:
                            EssFormReferenceID = (int)essReferenceTypes.SalaryAdvance;
                            break;
                        case 4:
                            EssFormReferenceID = (int)essReferenceTypes.Transfer;
                            break;
                        case 5:
                            EssFormReferenceID = (int)essReferenceTypes.Expense;
                            break;
                        case 7:
                            EssFormReferenceID = (int)essReferenceTypes.Document;
                            break;
                        case 8:
                        case 20:
                        case 21:
                            EssFormReferenceID = (int)essReferenceTypes.Extension;
                            break;
                        case 14:
                            EssFormReferenceID = (int)essReferenceTypes.Attendence;
                            break;
                        case 9:
                        case 16:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                        case 30:

                           EssFormReferenceID = (int)essReferenceTypes.General;
                            break;
                        case 17:
                            EssFormReferenceID = (int)essReferenceTypes.Travel;
                            break;
                    }

                    DataTable dt = objHomePage.DisplayReasons(EssFormReferenceID, Convert.ToString(e.CommandArgument).Split(',')[0].ToInt32());

                    if (dt.Rows.Count > 0)
                    {
                        gvReasons.Visible = true;
                        gvReasons.DataSource = dt;
                        gvReasons.DataBind();
                        upViewDetails.Update();
                    }
                }
                else
                {
                    hfLink.Value = "0";
                    gvReasons.Visible = false;
                    upViewDetails.Update();
                }
            }
        }
    }
}
