﻿<%@ Page Language="C#" MasterPageFile="~/Master/DocumentMasterPage.master" AutoEventWireup="true"
    CodeFile="ViewPolicy.aspx.cs" Inherits="Public_ViewPolicy" Title="View Policy"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="upMain" runat="server">
        <ContentTemplate>
            <div style="width: 99%; height: 50px; margin-top: 10px; margin-bottom: 10px; padding-left:1%; float: left;">
                <ul class="dark_menu">
                    <li><a href=""><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:ControlsCommon,Menu%>'></asp:Literal> &nbsp; &#x25Bc</a>
                       <%-- <ul>                                
                            <li><a href="Employee.aspx">Employee</a></li>
                            <li><a href="SalaryStructure.aspx">Salary Structure</a></li>
                            <li class="selected"><a href="ViewPolicy.aspx">View Policies</a></li>
                            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
                     <%--   </ul>--%>
                        
                        
                        <ul>
            <li ><a href='Employee.aspx'><span>
                <%--  Employee --%>
                <asp:Literal ID="Literal57" runat="server" Text='<%$Resources:MasterPageCommon,Employee %>'>
                </asp:Literal>
            </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>
                <%--    Salary Structure--%>
                <asp:Literal ID="Literal1000" runat="server" Text='<%$Resources:MasterPageCommon,SalaryStructure %>'>
                </asp:Literal>
            </span></a></li>
            <li class="selected"><a href="ViewPolicy.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal58" runat="server" Text='<%$Resources:MasterPageCommon,ViewPolicies %>'>
                </asp:Literal>
            </a></li>
             <li class="selected"><a href="EmployeeSalaryCertificate.aspx">
                <%-- View Policies--%>
                <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:MasterPageCommon,SalaryCertificate %>'>
                </asp:Literal>
            </a></li>
            <li><a href="EmployeeDutyRoaster.aspx">
                <%-- View Policies--%>
               <asp:Literal ID="Literal56" runat="server" Text='<%$Resources:MasterPageCommon,DutyRoaster %>'>
                </asp:Literal>
            </a></li>
            <%--<li><a href="ChangePolicy.aspx">Change Policies</a></li>--%>
        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                    </li>
                </ul>
            </div>
            <div style="width: 100%; float: left;">
                <div style="width: 34%; padding-left: 1%; float: left; background-color: #FFFFFF;">
                    <div style="width: 100%; height: 25px; margin-top: 20px; margin-bottom: 20px; float: left;">
                        <div style="width: 45%; margin-left: 5%; height: 25px; float: left;">
                            <asp:RadioButton ID="rbEmployeeMode" runat="server" GroupName="ViewMode" OnCheckedChanged="rbEmployeeMode_CheckedChanged"
                                AutoPostBack="True" />
                                <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="EmployeeWise">
                                </asp:Literal>
                        </div>
                        <div style="width: 45%; height: 25px; margin-right: 5%; float: left;">
                            <asp:RadioButton ID="rbPolicyMode" runat="server" GroupName="ViewMode" OnCheckedChanged="rbPolicyMode_CheckedChanged"
                                AutoPostBack="True" />
                                <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="PolicyWise">
                                </asp:Literal>
                        </div>
                    </div>
                    <div id="divEmployeeMode" runat="server" style="width: 100%; height: 100%">
                        <div style="width: 100%; height: 20px; float: left; padding-left: 2%; padding-bottom: 5%;">
                            <div style="width: 60%; height: 20px; float: left;">
                                <asp:TextBox ID="txtSearch" runat="server" meta:resourcekey="SearchEmployee" Style="width: 100%;"></asp:TextBox>
                            </div>
                            <div style="width: 25%; padding-left: 5%; height: 20px; float: left;">
                                <asp:ImageButton ID="imgBtnSearch" runat="server" OnClick="imgBtnSearch_Click" ImageUrl="../images/search.png"
                                    ToolTip='<%$Resources:ControlsCommon,Search %>' />
                            </div>
                        </div>
                        <div style="width: 100%; float: left; overflow: auto;">
                            <asp:UpdatePanel ID="upEmployeePolicies" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="min-height: 800px; height:auto; display: block;" runat="server">
                                        <asp:TreeView ID="tvEmployeePolicies" runat="server" NodeStyle-CssClass="treeNode2"
                                            SelectedNodeStyle-CssClass="treeNodeSelected2" ShowLines="True" OnSelectedNodeChanged="tvEmployeePolicies_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div id="divPolicyMode" runat="server" style="width: 100%; height: 100%">
                        <div style="width: 100%; float: left; overflow: auto;">
                            <asp:UpdatePanel ID="upAllPolicies" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div style="min-height: 800px; height:auto; display: block;">
                                        <asp:TreeView ID="tvAllPolicies" runat="server" NodeStyle-CssClass="treeNode2" SelectedNodeStyle-CssClass="treeNodeSelected2"
                                            ShowLines="True" OnSelectedNodeChanged="tvAllPolicies_SelectedNodeChanged">
                                        </asp:TreeView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="width: 61%; margin-left: 2%; float: left">
                            <div id="divContent" runat="server" style="width: 98%; padding-left: 1%; padding-right: 1%;
                                padding-bottom: 5%; float: left; border-radius: 5px;">
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
