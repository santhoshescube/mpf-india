﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Reflection;

public partial class Public_Candidate : System.Web.UI.Page
{
    clsCandidate objclsCandidate;
    clsUserMaster objclsUserMaster;

    private string CurrentSelectedValue
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["CandidateID"] == null)
                AddNew();
            else
            {
                long lngCandidateID = Request.QueryString["CandidateID"].ToInt64();

                if (lngCandidateID > 0)
                    getCandidateDetails(Request.QueryString["CandidateID"].ToInt64());
                else
                    AddNew();
            }
        }
        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        CandidateArabicReference.OnSave -= new Controls_CandidateArabicReferenceControl.saveHandler(CandidateArabicReference_OnSave);
        CandidateArabicReference.OnSave += new Controls_CandidateArabicReferenceControl.saveHandler(CandidateArabicReference_OnSave);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }

    void CandidateArabicReference_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void CandidateArabicReference_onRefresh()
    {
        mdlPopUpReference1.Show();
    }

    protected void lnkAddCandidate_Click(object sender, EventArgs e)
    {
        AddNew();
    }
    protected void lnkCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("CandidateArabicView.aspx");
    }
    private void AddNew()
    {
        objclsCandidate = new clsCandidate();
        objclsCandidate.CandidateID = 0;
        ViewState["CandidateID"] = null;
        Session["Photo"] = null;

        getCandidateCode();
        LoadCombos(0);
        LoadGraduationYear();

        // Basic Info
        imgPreview.Src = "";
        rblGender.SelectedValue = "0";
        txtEnglishFirstName.Text = string.Empty;
        txtEnglishSecondName.Text = string.Empty;
        txtEnglishThirdName.Text = string.Empty;
        txtArabicFirstName.Text = string.Empty;
        txtArabicSecondName.Text = string.Empty;
        txtArabicThirdName.Text = string.Empty;
        txtCitizenship.Text = string.Empty;
        txtPlaceOfBirth.Text = string.Empty;
        txtDateofBirth.Text = string.Empty;
        txtNoOfSon.Text = string.Empty;
        txtMotherName.Text = string.Empty;
        txtDoctrine.Text = string.Empty;
        txtCurrentAddress.Text = string.Empty;
        txtCurrentPOBox.Text = string.Empty;
        txtCurrentTelephoneNumber.Text = string.Empty;
        txtBasicBusinessPairAddress.Text = string.Empty;
        txtPlaceofBusinessPair.Text = string.Empty;

        // Professional Info
        txtPreviousJob.Text = string.Empty;
        txtPlaceofGraduation.Text = string.Empty;
        txtCollegeSchool.Text = string.Empty;
        txtSector.Text = string.Empty;
        txtPosition.Text = string.Empty;
        txtTypeofExperience.Text = string.Empty;
        txtCompensation.Text = string.Empty;
        txtSpecialization.Text = string.Empty;
        txtDateofTransactionEntry.Text = string.Empty;
        txtExpectedJoinDate.Text = string.Empty;
        rcDegree.SelectedIndex = -1;

        // Document Info
        txtPassportNumber.Text = string.Empty;
        txtPlaceofPassportIssue.Text = string.Empty;
        txtPassportExpiryDate.Text = string.Empty;
        txtPassportReleaseDate.Text = string.Empty;
        txtVisaNumber.Text = string.Empty;
        txtUnifiedNumber.Text = string.Empty;
        txtVisaPlaceofIssue.Text = string.Empty;
        txtVisaExpiryDate.Text = string.Empty;
        txtVisaReleaseDate.Text = string.Empty;
        txtNationalCardNumber.Text = string.Empty;
        txtNationalCardExpiryDate.Text = string.Empty;

        // Other Info
        txtPermenantAddress.Text = string.Empty;
        txtHomeStreet.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtHomeTelephone.Text = string.Empty;
        txtBusinessAddress.Text = string.Empty;
        txtArea.Text = string.Empty;
        txtPermenantPOBox.Text = string.Empty;
        txtFax.Text = string.Empty;
        txtPhoneNumber.Text = string.Empty;
        txtMobileNumber.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtInsuranceNumber.Text = string.Empty;
        txtSignUpDate.Text = string.Empty;
        txtBusinessStreet.Text = "";

        // Languages
        chlArabic.Items.Clear();
        chlArabic.Items.Add("قراءة [Reading]");
        chlArabic.Items.Add("كتابة [Writing]");
        chlEnglish.Items.Clear();
        chlEnglish.Items.Add("قراءة [Reading]");
        chlEnglish.Items.Add("كتابة [Writing]");
        chlOtherLanguages.Items.Clear();
        chlOtherLanguages.Items.Add("قراءة [Reading]");
        chlOtherLanguages.Items.Add("كتابة [Writing]");
        txtOtherLanguages.Text = string.Empty;


        // Basic Info Arb
        txtCitizenshipArb.Text = string.Empty;
        txtPlaceOfBirthArb.Text = string.Empty;
        txtDateofBirthArb.Text = string.Empty;
        txtNoOfSonArb.Text = string.Empty;
        txtMotherNameArb.Text = string.Empty;
        txtDoctrineArb.Text = string.Empty;
        txtCurrentAddressArb.Text = string.Empty;
        txtCurrentPOBoxArb.Text = string.Empty;
        txtCurrentTelephoneNumberArb.Text = string.Empty;
        txtBasicBusinessPairAddressArb.Text = string.Empty;
        txtPlaceofBusinessPairArb.Text = string.Empty;

        // Professional Info Arb
        txtPreviousJobArb.Text = string.Empty;
        txtPlaceofGraduationArb.Text = string.Empty;
        txtCollegeSchoolArb.Text = string.Empty;
        txtSectorArb.Text = string.Empty;
        txtPositionArb.Text = string.Empty;
        txtTypeofExperienceArb.Text = string.Empty;
        txtCompensationArb.Text = string.Empty;
        txtSpecializationArb.Text = string.Empty;
        txtDateofTransactionEntryArb.Text = string.Empty;
        txtExpectedJoinDateArb.Text = string.Empty;
        rcDegree.SelectedIndex = -1;

        // Document Info Arb
        txtPassportNumberArb.Text = string.Empty;
        txtPlaceofPassportIssueArb.Text = string.Empty;
        txtPassportExpiryDateArb.Text = string.Empty;
        txtPassportReleaseDateArb.Text = string.Empty;
        txtVisaNumberArb.Text = string.Empty;
        txtUnifiedNumberArb.Text = string.Empty;
        txtVisaPlaceofIssueArb.Text = string.Empty;
        txtVisaExpiryDateArb.Text = string.Empty;
        txtVisaReleaseDateArb.Text = string.Empty;
        txtNationalCardNumberArb.Text = string.Empty;
        txtNationalCardExpiryDateArb.Text = string.Empty;

        // Other Info Arb
        txtPermenantAddressArb.Text = string.Empty;
        txtHomeStreetArb.Text = string.Empty;
        txtCityArb.Text = string.Empty;
        txtHomeTelephoneArb.Text = string.Empty;
        txtBusinessAddressArb.Text = string.Empty;
        txtAreaArb.Text = string.Empty;
        txtPermenantPOBoxArb.Text = string.Empty;
        txtFaxArb.Text = string.Empty;
        txtPhoneNumberArb.Text = string.Empty;
        txtMobileNumberArb.Text = string.Empty;
        txtInsuranceNumberArb.Text = string.Empty;
        txtSignUpDateArb.Text = string.Empty;
        txtBusinessStreetArb.Text = string.Empty;

        upfvCandidate.Update();
        ViewState["Documents"] = null;
        GetCandidateAttachedDocuments(0);
    }

    private void getCandidateCode()
    {
        Random rd = new Random();
        txtCandidateCode.Text = rd.Next(1, 99999999).ToStringCustom();
    }

    private void LoadCombos(int intType)
    {
        objclsCandidate = new clsCandidate();
        DataSet dsTemp = objclsCandidate.LoadCombos();

        if (dsTemp != null)
        {
            if (intType == 0 || intType == 1)
            {
                if (dsTemp.Tables[0] != null)
                {
                    ddlJob.DataSource = dsTemp.Tables[0];
                    ddlJob.DataTextField = "JobCode";
                    ddlJob.DataValueField = "JobId";
                    ddlJob.DataBind();
                    ddlJob.Items.Insert(0, new ListItem("Select", "-1"));
                    updJob.Update();
                }
            }

            //if (intType == 0 || intType == 2)
            //{
            //    if (dsTemp.Tables[1] != null)
            //    {
            //        //ddlAgency.DataSource = dsTemp.Tables[1];
            //        //ddlAgency.DataTextField = "Agency";
            //        //ddlAgency.DataValueField = "AgencyID";
            //        //ddlAgency.DataBind();
            //        //ddlAgency.Items.Insert(0, new ListItem("Select", "-1"));
            //        //updAgency.Update();

            //        //if (intType > 0)
            //        //    this.SetSelectedIndex(this.ddlAgency);
            //    }
            //}

            if (intType == 0 || intType == 3)
            {
                if (dsTemp.Tables[2] != null)
                {
                    rcSalutation.DataSource = dsTemp.Tables[2];
                    rcSalutation.DataTextField = "Salutation";
                    rcSalutation.DataValueField = "SalutationID";
                    rcSalutation.DataBind();
                    rcSalutation.Items.Insert(0, new ListItem("Select", "-1"));
                    updSalutation.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcSalutation);
                }
            }

            if (intType == 0 || intType == 4)
            {
                if (dsTemp.Tables[3] != null)
                {
                    ddlMaritalStatus.DataSource = dsTemp.Tables[3];
                    ddlMaritalStatus.DataTextField = "MaritalStatus";
                    ddlMaritalStatus.DataValueField = "MaritalStatusID";
                    ddlMaritalStatus.DataBind();
                    ddlMaritalStatus.Items.Insert(0, new ListItem("Select", "-1"));
                    updMaritalStatus.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlMaritalStatus);
                }
            }

            if (intType == 0 || intType == 5)
            {
                if (dsTemp.Tables[4] != null)
                {
                    rcReligion.DataSource = dsTemp.Tables[4];
                    rcReligion.DataTextField = "Religion";
                    rcReligion.DataValueField = "ReligionID";
                    rcReligion.DataBind();
                    rcReligion.Items.Insert(0, new ListItem("Select", "-1"));
                    updReligion.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcReligion);
                }
            }

            if (intType == 0 || intType == 6)
            {
                if (dsTemp.Tables[5] != null)
                {
                    DataTable datTemp1 = dsTemp.Tables[5];
                    rcCountry.DataSource = datTemp1;
                    rcCountry.DataTextField = "CountryName";
                    rcCountry.DataValueField = "CountryID";
                    rcCountry.DataBind();
                    rcCountry.Items.Insert(0, new ListItem("Select", "-1"));
                    updCountry.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcCountry);
                }
            }

            if (intType == 0 || intType == 7)
            {
                if (dsTemp.Tables[5] != null)
                {
                    DataTable datTemp2 = dsTemp.Tables[5];
                    ddlPassportCountry.DataSource = datTemp2;
                    ddlPassportCountry.DataTextField = "CountryName";
                    ddlPassportCountry.DataValueField = "CountryID";
                    ddlPassportCountry.DataBind();
                    ddlPassportCountry.Items.Insert(0, new ListItem("Select", "-1"));
                    updPassportCountry.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlPassportCountry);
                }
            }

            if (intType == 0 || intType == 8)
            {
                if (dsTemp.Tables[5] != null)
                {
                    DataTable datTemp3 = dsTemp.Tables[5];
                    ddlVisaCountry.DataSource = datTemp3;
                    ddlVisaCountry.DataTextField = "CountryName";
                    ddlVisaCountry.DataValueField = "CountryID";
                    ddlVisaCountry.DataBind();
                    ddlVisaCountry.Items.Insert(0, new ListItem("Select", "-1"));
                    updVisaCountry.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlVisaCountry);
                }
            }

            if (intType == 0 || intType == 9)
            {
                if (dsTemp.Tables[6] != null)
                {
                    ddlCompanyType.DataSource = dsTemp.Tables[6];
                    ddlCompanyType.DataTextField = "CompanyType";
                    ddlCompanyType.DataValueField = "CompanyTypeID";
                    ddlCompanyType.DataBind();
                    ddlCompanyType.Items.Insert(0, new ListItem("Select", "-1"));
                    updCompanyType.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlCompanyType);
                }
            }

            if (intType == 0 || intType == 10)
            {
                if (dsTemp.Tables[7] != null)
                {
                    rcDegree.DataSource = dsTemp.Tables[7];
                    rcDegree.DataTextField = "Qualification";
                    rcDegree.DataValueField = "DegreeID";
                    rcDegree.DataBind();
                    rcDegree.Items.Insert(0, new ListItem("Select", "-1"));
                    uprcDegree.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcDegree);
                }
            }

            //if (intType == 0 || intType == 11)
            //{
            //    if (dsTemp.Tables[8] != null)
            //    {
            //        chlSkills.DataSource = dsTemp.Tables[8];
            //        chlSkills.DataTextField = "Skill";
            //        chlSkills.DataValueField = "SkillID";
            //        chlSkills.DataBind();
            //        updSkills.Update();
            //    }
            //}

            if (intType == 0 || intType == 12)
            {
                if (dsTemp.Tables[9] != null)
                {
                    DataTable datTemp1 = dsTemp.Tables[9];

                    ddlCurrentNationality.DataSource = datTemp1;
                    ddlCurrentNationality.DataTextField = "Nationality";
                    ddlCurrentNationality.DataValueField = "NationalityID";
                    ddlCurrentNationality.DataBind();
                    ddlCurrentNationality.Items.Insert(0, new ListItem("Select", "-1"));
                    updCurrentNationality.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlCurrentNationality);
                }
            }

            if (intType == 0 || intType == 13)
            {
                if (dsTemp.Tables[9] != null)
                {
                    DataTable datTemp2 = dsTemp.Tables[9];
                    ddlPreviousNationality.DataSource = datTemp2;
                    ddlPreviousNationality.DataTextField = "Nationality";
                    ddlPreviousNationality.DataValueField = "NationalityID";
                    ddlPreviousNationality.DataBind();
                    ddlPreviousNationality.Items.Insert(0, new ListItem("Select", "-1"));
                    updPreviousNationality.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlPreviousNationality);
                }
            }

            if (intType == 0 || intType == 14)
            {
                if (dsTemp.Tables[10] != null)
                {
                    ddlQualificationCategory.DataSource = dsTemp.Tables[10];
                    ddlQualificationCategory.DataTextField = "QualificationCategory";
                    ddlQualificationCategory.DataValueField = "QualificationCategoryID";
                    ddlQualificationCategory.DataBind();
                    ddlQualificationCategory.Items.Insert(0, new ListItem("Select", "-1"));
                    updQualificationCategory.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlQualificationCategory);
                }
            }

            if (intType == 0 || intType == 15)
            {
                if (dsTemp.Tables[11] != null)
                {
                    rcSalutationArb.DataSource = dsTemp.Tables[11];
                    rcSalutationArb.DataTextField = "SalutationArb";
                    rcSalutationArb.DataValueField = "SalutationID";
                    rcSalutationArb.DataBind();
                    rcSalutationArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updSalutationArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcSalutationArb);
                }
            }

            if (intType == 0 || intType == 16)
            {
                if (dsTemp.Tables[12] != null)
                {
                    ddlMaritalStatusArb.DataSource = dsTemp.Tables[12];
                    ddlMaritalStatusArb.DataTextField = "MaritalStatusArb";
                    ddlMaritalStatusArb.DataValueField = "MaritalStatusID";
                    ddlMaritalStatusArb.DataBind();
                    ddlMaritalStatusArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updMaritalStatusArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlMaritalStatusArb);
                }
            }

            if (intType == 0 || intType == 17)
            {
                if (dsTemp.Tables[13] != null)
                {
                    rcReligionArb.DataSource = dsTemp.Tables[13];
                    rcReligionArb.DataTextField = "ReligionArb";
                    rcReligionArb.DataValueField = "ReligionID";
                    rcReligionArb.DataBind();
                    rcReligionArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updReligionArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcReligionArb);
                }
            }

            if (intType == 0 || intType == 18)
            {
                if (dsTemp.Tables[14] != null)
                {
                    DataTable datTemp1 = dsTemp.Tables[14];
                    rcCountryArb.DataSource = datTemp1;
                    rcCountryArb.DataTextField = "CountryNameArb";
                    rcCountryArb.DataValueField = "CountryID";
                    rcCountryArb.DataBind();
                    rcCountryArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updCountryArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcCountryArb);
                }
            }

            if (intType == 0 || intType == 19)
            {
                if (dsTemp.Tables[14] != null)
                {
                    DataTable datTemp2 = dsTemp.Tables[14];
                    ddlPassportCountryArb.DataSource = datTemp2;
                    ddlPassportCountryArb.DataTextField = "CountryNameArb";
                    ddlPassportCountryArb.DataValueField = "CountryID";
                    ddlPassportCountryArb.DataBind();
                    ddlPassportCountryArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updPassportCountryArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlPassportCountryArb);
                }
            }

            if (intType == 0 || intType == 20)
            {
                if (dsTemp.Tables[14] != null)
                {
                    DataTable datTemp3 = dsTemp.Tables[14];
                    ddlVisaCountryArb.DataSource = datTemp3;
                    ddlVisaCountryArb.DataTextField = "CountryNameArb";
                    ddlVisaCountryArb.DataValueField = "CountryID";
                    ddlVisaCountryArb.DataBind();
                    ddlVisaCountryArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updVisaCountryArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlVisaCountryArb);
                }
            }

            if (intType == 0 || intType == 21)
            {
                if (dsTemp.Tables[15] != null)
                {
                    ddlCompanyTypeArb.DataSource = dsTemp.Tables[15];
                    ddlCompanyTypeArb.DataTextField = "CompanyTypeArb";
                    ddlCompanyTypeArb.DataValueField = "CompanyTypeID";
                    ddlCompanyTypeArb.DataBind();
                    ddlCompanyTypeArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updCompanyTypeArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlCompanyTypeArb);
                }
            }

            if (intType == 0 || intType == 22)
            {
                if (dsTemp.Tables[16] != null)
                {
                    rcDegreeArb.DataSource = dsTemp.Tables[16];
                    rcDegreeArb.DataTextField = "QualificationArb";
                    rcDegreeArb.DataValueField = "DegreeID";
                    rcDegreeArb.DataBind();
                    rcDegreeArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updDegreeArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.rcDegreeArb);
                }
            }

            if (intType == 0 || intType == 23)
            {
                if (dsTemp.Tables[17] != null)
                {
                    DataTable datTemp1 = dsTemp.Tables[17];

                    ddlCurrentNationalityArb.DataSource = datTemp1;
                    ddlCurrentNationalityArb.DataTextField = "NationalityArb";
                    ddlCurrentNationalityArb.DataValueField = "NationalityID";
                    ddlCurrentNationalityArb.DataBind();
                    ddlCurrentNationalityArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updCurrentNationalityArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlCurrentNationalityArb);
                }
            }

            if (intType == 0 || intType == 24)
            {
                if (dsTemp.Tables[17] != null)
                {
                    DataTable datTemp2 = dsTemp.Tables[17];
                    ddlPreviousNationalityArb.DataSource = datTemp2;
                    ddlPreviousNationalityArb.DataTextField = "NationalityArb";
                    ddlPreviousNationalityArb.DataValueField = "NationalityID";
                    ddlPreviousNationalityArb.DataBind();
                    ddlPreviousNationalityArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updPreviousNationalityArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlPreviousNationalityArb);
                }
            }

            if (intType == 0 || intType == 25)
            {
                if (dsTemp.Tables[18] != null)
                {
                    ddlQualificationCategoryArb.DataSource = dsTemp.Tables[18];
                    ddlQualificationCategoryArb.DataTextField = "QualificationCategoryArb";
                    ddlQualificationCategoryArb.DataValueField = "QualificationCategoryID";
                    ddlQualificationCategoryArb.DataBind();
                    ddlQualificationCategoryArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updQualificationCategoryArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlQualificationCategoryArb);
                }
            }
            if (intType == 0 || intType == 26)
            {
                if (dsTemp.Tables[19] != null)
                {
                    ddlReferralType.DataSource = dsTemp.Tables[19];
                    ddlReferralType.DataTextField = "RefferalType";
                    ddlReferralType.DataValueField = "RefferalTypeId";
                    ddlReferralType.DataBind();
                    ddlReferralType.Items.Insert(0, new ListItem("Select", "-1"));
                    updReferralType.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferralType);
                }
            }

            if (intType == 27)
            {
                if (dsTemp.Tables[22] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[22];
                    ddlReferredBy.DataTextField = "Agency";
                    ddlReferredBy.DataValueField = "AgencyID";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                    updReferredBy.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredBy);
                }
            }
            if (intType == 28)
            {
                if (dsTemp.Tables[22] != null)
                {
                    ddlReferredByArb.DataSource = dsTemp.Tables[22];
                    ddlReferredByArb.DataTextField = "AgencyArb";
                    ddlReferredByArb.DataValueField = "AgencyID";
                    ddlReferredByArb.DataBind();
                    ddlReferredByArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updReferredByArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredByArb);
                }
            }
            if (intType == 29)
            {
                if (dsTemp.Tables[20] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[20];
                    ddlReferredBy.DataTextField = "EmployeeFullName";
                    ddlReferredBy.DataValueField = "EmployeeID";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                    updReferredBy.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredBy);
                }
            }
            if (intType == 30)
            {
                if (dsTemp.Tables[21] != null)
                {
                    ddlReferredBy.DataSource = dsTemp.Tables[21];
                    ddlReferredBy.DataTextField = "Description";
                    ddlReferredBy.DataValueField = "JobPortalId";
                    ddlReferredBy.DataBind();
                    ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                    updReferredBy.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredBy);
                }
            }
            if (intType == 0 || intType == 31)
            {
                if (dsTemp.Tables[19] != null)
                {
                    ddlReferralTypeArb.DataSource = dsTemp.Tables[19];
                    ddlReferralTypeArb.DataTextField = "RefferalTypeArb";
                    ddlReferralTypeArb.DataValueField = "RefferalTypeId";
                    ddlReferralTypeArb.DataBind();
                    ddlReferralTypeArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updReferralTypeArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferralTypeArb);
                }
            }
            if (intType == 32)
            {
                if (dsTemp.Tables[20] != null)
                {
                    ddlReferredByArb.DataSource = dsTemp.Tables[20];
                    ddlReferredByArb.DataTextField = "EmployeeFullNameArb";
                    ddlReferredByArb.DataValueField = "EmployeeID";
                    ddlReferredByArb.DataBind();
                    ddlReferredByArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updReferredByArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredByArb);
                }
            }
            if (intType == 33)
            {
                if (dsTemp.Tables[21] != null)
                {
                    ddlReferredByArb.DataSource = dsTemp.Tables[21];
                    ddlReferredByArb.DataTextField = "DescriptionArb";
                    ddlReferredByArb.DataValueField = "JobPortalId";
                    ddlReferredByArb.DataBind();
                    ddlReferredByArb.Items.Insert(0, new ListItem("حدد", "-1"));
                    updReferredByArb.Update();

                    if (intType > 0)
                        this.SetSelectedIndex(this.ddlReferredByArb);
                }
            }
            if (intType == -1 || intType == 0)
            {
                ddlReferredBy.Items.Clear();
                ddlReferredBy.DataSource = null;
                ddlReferredBy.DataBind();
                ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                updReferredBy.Update();

                ddlReferredByArb.Items.Clear();
                ddlReferredByArb.DataSource = null;
                ddlReferredByArb.DataBind();
                ddlReferredByArb.Items.Insert(0, new ListItem("حدد", "-1"));
                updReferredByArb.Update();
            }
        }
    }

    public void FillReferredBy()
    {
        switch (ddlReferralType.SelectedValue.ToInt32())
        {
            case -1:
                ddlReferredBy.Items.Clear();
                ddlReferredBy.DataSource = null;
                ddlReferredBy.DataBind();
                ddlReferredBy.Items.Insert(0, new ListItem("Select", "-1"));
                divRefferedBy.Style["display"] = "none";
                break;
            case 1:
                LoadCombos(27);
                divRefferedBy.Style["display"] = "block";
                break;
            case 2:
                LoadCombos(30);
                divRefferedBy.Style["display"] = "block";
                break;
            case 3:
                LoadCombos(29);
                divRefferedBy.Style["display"] = "none";
                break;
        }
        updReferralType.Update();
        updReferredBy.Update();
    }

    public void FillReferredByArb()
    {
        switch (ddlReferralTypeArb.SelectedValue.ToInt32())
        {
            case -1:
                ddlReferredByArb.Items.Clear();
                ddlReferredByArb.DataSource = null;
                ddlReferredByArb.DataBind();
                ddlReferredByArb.Items.Insert(0, new ListItem("حدد", "-1"));
                divRefferedByArb.Style["display"] = "none";
                break;
            case 1:
                LoadCombos(28);
                divRefferedByArb.Style["display"] = "block";
                break;
            case 2:
                LoadCombos(33);
                divRefferedByArb.Style["display"] = "block";
                break;
            case 3:
                LoadCombos(32);
                divRefferedByArb.Style["display"] = "none";
                break;
        }
        updReferralTypeArb.Update();
        updReferredByArb.Update();
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    private void LoadGraduationYear()
    {
        ddlDegreeCompletionYear.Items.Clear();
        ddlDegreeCompletionYear.Items.Insert(0, new ListItem("Select", "-1"));

        ddlDegreeCompletionYearArb.Items.Clear();
        ddlDegreeCompletionYearArb.Items.Insert(0, new ListItem("حدد", "-1"));

        for (int iCounter = DateTime.Now.Year - 30; iCounter <= DateTime.Now.Year + 1; iCounter++)
        {
            ddlDegreeCompletionYear.Items.Add(Convert.ToString(iCounter));
            ddlDegreeCompletionYearArb.Items.Add(Convert.ToString(iCounter));
        }
    }

    protected void btnCountry_Click(object sender, EventArgs e)
    {
        if (rcCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "getCountry";
            ReferenceControlNew1.SelectedValue = rcCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnCountryArb_Click(object sender, EventArgs e)
    {
        if (rcCountryArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "CountryReference";
            CandidateArabicReference.DataTextField = "CountryName";
            CandidateArabicReference.DataValueField = "CountryId";
            CandidateArabicReference.FunctionName = "getCountryArb";
            CandidateArabicReference.SelectedValue = rcCountryArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            CandidateArabicReference.DataTextFieldArabic = "CountryNameArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnPassportCountry_Click(object sender, EventArgs e)
    {
        if (ddlPassportCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "getCountryPassport";
            ReferenceControlNew1.SelectedValue = ddlPassportCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnPassportCountryArb_Click(object sender, EventArgs e)
    {
        if (ddlPassportCountryArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "CountryReference";
            CandidateArabicReference.DataTextField = "CountryName";
            CandidateArabicReference.DataValueField = "CountryId";
            CandidateArabicReference.FunctionName = "getPassportCountryArb";
            CandidateArabicReference.SelectedValue = ddlPassportCountryArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            CandidateArabicReference.DataTextFieldArabic = "CountryNameArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnVisaCountry_Click(object sender, EventArgs e)
    {
        if (ddlVisaCountry != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CountryReference";
            ReferenceControlNew1.DataTextField = "CountryName";
            ReferenceControlNew1.DataValueField = "CountryId";
            ReferenceControlNew1.FunctionName = "getCountryVisa";
            ReferenceControlNew1.SelectedValue = ddlVisaCountry.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnVisaCountryArb_Click(object sender, EventArgs e)
    {
        if (ddlVisaCountryArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "CountryReference";
            CandidateArabicReference.DataTextField = "CountryName";
            CandidateArabicReference.DataValueField = "CountryId";
            CandidateArabicReference.FunctionName = "getVisaCountryArb";
            CandidateArabicReference.SelectedValue = ddlVisaCountryArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
            CandidateArabicReference.DataTextFieldArabic = "CountryNameArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnReligion_Click(object sender, EventArgs e)
    {
        if (rcReligion != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "ReligionReference";
            ReferenceControlNew1.DataTextField = "Religion";
            ReferenceControlNew1.DataValueField = "ReligionId";
            ReferenceControlNew1.FunctionName = "getReligion";
            ReferenceControlNew1.SelectedValue = rcReligion.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("دين") : ("Religion");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnReligionArb_Click(object sender, EventArgs e)
    {
        if (rcReligionArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "ReligionReference";
            CandidateArabicReference.DataTextField = "Religion";
            CandidateArabicReference.DataValueField = "ReligionId";
            CandidateArabicReference.FunctionName = "getReligionArb";
            CandidateArabicReference.SelectedValue = rcReligionArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("دين") : ("Religion");
            CandidateArabicReference.DataTextFieldArabic = "ReligionArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnSalutation_Click(object sender, EventArgs e)
    {
        if (rcSalutation != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "SalutationReference";
            ReferenceControlNew1.DataTextField = "Salutation";
            ReferenceControlNew1.DataValueField = "SalutationId";
            ReferenceControlNew1.FunctionName = "getSalutation";
            ReferenceControlNew1.SelectedValue = rcSalutation.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("تحية") : ("Salutation");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnSalutationArb_Click(object sender, EventArgs e)
    {
        if (rcSalutationArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "SalutationReference";
            CandidateArabicReference.DataTextField = "Salutation";
            CandidateArabicReference.DataValueField = "SalutationId";
            CandidateArabicReference.FunctionName = "getSalutationArb";
            CandidateArabicReference.SelectedValue = rcSalutationArb.SelectedValue;
            CandidateArabicReference.DisplayName = "Salutation";
            CandidateArabicReference.DataTextFieldArabic = "SalutationArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnDegree_Click(object sender, EventArgs e)
    {
        AddNewDegree();
    }

    public void AddNewDegree()
    {
        ucDegreeReference.FillList();
        mpeDegreeReference.Show();
        upDegreeReference.Update();
    }

    protected void btnDegreeArb_Click(object sender, EventArgs e)
    {
        AddNewDegreeArb();
    }

    public void AddNewDegreeArb()
    {
        ucDegreeReference1.FillList();
        mpeDegreeReference1.Show();
        upDegreeReference1.Update();
    }

    protected void btnQualificationCategory_Click(object sender, EventArgs e)
    {
        if (ddlQualificationCategory != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "QualificationCategoryReference";
            ReferenceControlNew1.DataTextField = "QualificationCategory";
            ReferenceControlNew1.DataValueField = "QualificationCategoryID";
            ReferenceControlNew1.FunctionName = "getQualificationCategory";
            ReferenceControlNew1.SelectedValue = ddlQualificationCategory.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("المؤهل") : ("QualificationCategory");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnQualificationCategoryArb_Click(object sender, EventArgs e)
    {
        if (ddlQualificationCategoryArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "QualificationCategoryReference";
            CandidateArabicReference.DataTextField = "QualificationCategory";
            CandidateArabicReference.DataValueField = "QualificationCategoryID";
            CandidateArabicReference.FunctionName = "getQualificationCategoryArb";
            CandidateArabicReference.SelectedValue = ddlQualificationCategoryArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("المؤهل") : ("QualificationCategory");
            CandidateArabicReference.DataTextFieldArabic = "QualificationCategoryArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnCompanyType_Click(object sender, EventArgs e)
    {
        if (ddlCompanyType != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "CompanyTypeReference";
            ReferenceControlNew1.DataTextField = "CompanyType";
            ReferenceControlNew1.DataValueField = "CompanyTypeID";
            ReferenceControlNew1.FunctionName = "getCompanyType";
            ReferenceControlNew1.SelectedValue = ddlCompanyType.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("نوع المؤسسة") : ("Company Type"); 
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnCompanyTypeArb_Click(object sender, EventArgs e)
    {
        if (ddlCompanyTypeArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "CompanyTypeReference";
            CandidateArabicReference.DataTextField = "CompanyType";
            CandidateArabicReference.DataValueField = "CompanyTypeID";
            CandidateArabicReference.FunctionName = "getCompanyTypeArb";
            CandidateArabicReference.SelectedValue = ddlCompanyTypeArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("نوع المؤسسة") : ("Company Type"); 
            CandidateArabicReference.DataTextFieldArabic = "CompanyTypeArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnReferredByArb_Click(object sender, EventArgs e)
    {
        if (ddlReferredByArb != null)
        {
            switch (ddlReferralTypeArb.SelectedValue)
            {
                case "1":
                    CandidateArabicReference.ClearAll();
                    CandidateArabicReference.TableName = "AgencyReference";
                    CandidateArabicReference.DataTextField = "Agency";
                    CandidateArabicReference.DataValueField = "AgencyID";
                    CandidateArabicReference.FunctionName = "getAgencyArb";
                    CandidateArabicReference.SelectedValue = ddlReferralTypeArb.SelectedValue;
                    CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("يحيلها") : ("Referred By"); 
                    CandidateArabicReference.DataTextFieldArabic = "AgencyArb";
                    CandidateArabicReference.PopulateData();

                    mdlPopUpReference1.Show();
                    updModalPopUp1.Update();
                    break;
                case "2":
                    CandidateArabicReference.ClearAll();
                    CandidateArabicReference.TableName = "HRJobPortalreference";
                    CandidateArabicReference.DataTextField = "Description";
                    CandidateArabicReference.DataValueField = "JobPortalId";
                    CandidateArabicReference.FunctionName = "FillComboRefferedByArb";
                    CandidateArabicReference.SelectedValue = ddlReferredByArb.SelectedValue;
                    CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("وصف") : ("Description"); 
                    CandidateArabicReference.DataTextFieldArabic = "DescriptionArb";
                    CandidateArabicReference.PopulateData();

                    mdlPopUpReference1.Show();
                    updModalPopUp1.Update();
                    break;

            }

        }
    }

    protected void btnReferredBy_Click(object sender, EventArgs e)
    {
        if (ddlReferredBy != null)
        {

            switch (ddlReferralType.SelectedValue)
            {
                case "1":
                    ReferenceControlNew1.ClearAll();
                    ReferenceControlNew1.TableName = "AgencyReference";
                    ReferenceControlNew1.DataTextField = "Agency";
                    ReferenceControlNew1.DataValueField = "AgencyID";
                    ReferenceControlNew1.FunctionName = "getAgency";
                    ReferenceControlNew1.SelectedValue = ddlReferredBy.SelectedValue;
                    ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("يحيلها") : ("Referred By"); 
                    ReferenceControlNew1.PopulateData();

                    mdlPopUpReference.Show();
                    updModalPopUp.Update();
                    break;
                case "2":
                    ReferenceControlNew1.ClearAll();
                    ReferenceControlNew1.TableName = "HRJobPortalreference";
                    ReferenceControlNew1.DataTextField = "Description";
                    ReferenceControlNew1.DataValueField = "JobPortalId";
                    ReferenceControlNew1.FunctionName = "FillComboRefferedBy";
                    ReferenceControlNew1.SelectedValue = ddlReferredBy.SelectedValue;
                    ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("وصف") : ("Description"); 
                    ReferenceControlNew1.PopulateData();

                    mdlPopUpReference.Show();
                    updModalPopUp.Update();
                    break;

            }
        }
    }

    protected void ddlReferralType_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillReferredBy();
    }

    protected void ddlReferralTypeArb_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillReferredByArb();
    }

    protected void btnCurrentNationality_Click(object sender, EventArgs e)
    {
        if (ddlCurrentNationality != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "NationalityReference";
            ReferenceControlNew1.DataTextField = "Nationality";
            ReferenceControlNew1.DataValueField = "NationalityID";
            ReferenceControlNew1.FunctionName = "getCurrentNationality";
            ReferenceControlNew1.SelectedValue = ddlCurrentNationality.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("الجنسية") : ("Nationality");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnCurrentNationalityArb_Click(object sender, EventArgs e)
    {
        if (ddlCurrentNationalityArb != null)
        {
            CandidateArabicReference.ClearAll();
            CandidateArabicReference.TableName = "NationalityReference";
            CandidateArabicReference.DataTextField = "Nationality";
            CandidateArabicReference.DataValueField = "NationalityID";
            CandidateArabicReference.FunctionName = "getCurrentNationalityArb";
            CandidateArabicReference.SelectedValue = ddlCurrentNationalityArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("الجنسية") : ("Nationality");
            CandidateArabicReference.DataTextFieldArabic = "NationalityArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnPreviousNationality_Click(object sender, EventArgs e)
    {
        if (ddlPreviousNationality != null)
        {
            ReferenceControlNew1.TableName = "NationalityReference";
            ReferenceControlNew1.DataTextField = "Nationality";
            ReferenceControlNew1.DataValueField = "NationalityID";
            ReferenceControlNew1.FunctionName = "getPreviousNationality";
            ReferenceControlNew1.SelectedValue = ddlPreviousNationality.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("الجنسية") : ("Nationality");
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }

    protected void btnPreviousNationalityArb_Click(object sender, EventArgs e)
    {
        if (ddlPreviousNationalityArb != null)
        {
            CandidateArabicReference.TableName = "NationalityReference";
            CandidateArabicReference.DataTextField = "Nationality";
            CandidateArabicReference.DataValueField = "NationalityID";
            CandidateArabicReference.FunctionName = "getPreviousNationalityArb";
            CandidateArabicReference.SelectedValue = ddlPreviousNationalityArb.SelectedValue;
            CandidateArabicReference.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("الجنسية") : ("Nationality");
            CandidateArabicReference.DataTextFieldArabic = "NationalityArb";
            CandidateArabicReference.PopulateData();

            mdlPopUpReference1.Show();
            updModalPopUp1.Update();
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (FormValidation())
        {
            SaveCandidate();
        }
    }

    private bool FormValidation()
    {
        bool isValidate = true;
        string Message = "";
        string strScript = "";
        DateTime T = DateTime.Now;

        objclsCandidate = new clsCandidate();
        objclsCandidate.CandidateID = ViewState["CandidateID"].ToInt64();
        objclsCandidate.DateofBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()).ToString("dd MMM yyyy");
        objclsCandidate.CandidateCode = txtCandidateCode.Text.Trim();
        objclsCandidate.PassportNumber = txtPassportNumber.Text.Trim();

        if (rcSalutation.SelectedIndex == 0 || ddlMaritalStatus.SelectedIndex == 0 || rcReligion.SelectedIndex == 0 ||
            ddlCurrentNationality.SelectedIndex == 0 || rcCountry.SelectedIndex == 0 ||
            txtCandidateCode.Text == string.Empty || txtEnglishFirstName.Text == string.Empty || txtEnglishSecondName.Text == string.Empty ||
            txtEnglishThirdName.Text == string.Empty || txtDateofBirth.Text == string.Empty ||
            txtPlaceOfBirth.Text == string.Empty || txtCurrentTelephoneNumber.Text == string.Empty ||
            txtCurrentAddress.Text == string.Empty)
        {
            isValidate = false;
            Message = "Some mandatory fields are missing!!!";
            strScript = "pnlTab1";
        }

        if (!(DateTime.TryParseExact(txtDateofBirth.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        {
            isValidate = false;
            Message = "Invalid date of birth(valid format:dd/MM/yyyy)";
            strScript = "pnlTab1";
        }

        else if (clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()) > clsCommon.Convert2DateTime(DateTime.Now.Date.AddYears(-18).Date.ToString("dd/MM/yyyy")))
        {
            isValidate = false;
            Message = "Age must be greater than or equal to 18 years!!!";
            strScript = "pnlTab1";
        }

        else if (txtNoOfSon.Text.Trim().ToInt32() < 0)
        {
            isValidate = false;
            Message = "No. of son should not be less than 0!!!";
            strScript = "pnlTab1";
        }

        else if (txtEmail.Text == string.Empty)
        {
            isValidate = false;
            Message = "Some mandatory fields are missing!!!";
            strScript = "pnlTab1";
        }

        else if (rcDegree.SelectedIndex == 0 || ddlQualificationCategory.SelectedIndex == 0)
        {
            isValidate = false;
            Message = "Some mandatory fields are missing!!!";
            strScript = "pnlTab2";
        }

        //else if (ddlPassportCountry.SelectedIndex == 0 || txtPassportNumber.Text == string.Empty || txtPlaceofPassportIssue.Text == string.Empty ||
        //    txtPassportExpiryDate.Text == string.Empty || txtPassportReleaseDate.Text == string.Empty)
        //{
        //    isValidate = false;
        //    Message = "Some mandatory fields are missing!!!";
        //    strScript = "pnlTab3";
        //}

        //else if (objclsCandidate.CheckExistsCandidatePassport())
        //{
        //    isValidate = false;
        //    Message = "Passport Number is already exists!!!";
        //    strScript = "pnlTab3";
        //}

        //else if (!(DateTime.TryParseExact(txtPassportExpiryDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        //{
        //    isValidate = false;
        //    Message = "Invalid passport expiry date format(valid format:dd/MM/yyyy)";
        //    strScript = "pnlTab3";
        //}

        //else if (!(DateTime.TryParseExact(txtPassportReleaseDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out T)))
        //{
        //    isValidate = false;
        //    Message = "Invalid passport issue date format(valid format:dd/MM/yyyy)";
        //    strScript = "pnlTab3";
        //}

        //else if (clsCommon.Convert2DateTime(txtPassportExpiryDate.Text.Trim()) < clsCommon.Convert2DateTime(txtPassportReleaseDate.Text.Trim()))
        //{
        //    isValidate = false;
        //    Message = "Passport expiry date must be greater than the issue date!!!";
        //    strScript = "pnlTab3";
        //}        
        else if (txtEmail.Text == string.Empty)
        {
            isValidate = false;
            Message = "Some mandatory fields are missing!!!";
            strScript = "pnlTab4";
        }
        if (!isValidate)
        {
            ScriptManager.RegisterClientScriptBlock(btnSubmit, btnSubmit.GetType(), Guid.NewGuid().ToString(), "SetCandidateTab('" + strScript + "'); alert('" + Message + "');", true);
            return false;
        }

        return true;
    }

    public void SaveCandidate()
    {

        objclsCandidate = new clsCandidate();
        objclsUserMaster = new clsUserMaster();
        objclsCandidate.Begin();

        try
        {
            objclsCandidate.CandidateID = ViewState["CandidateID"].ToInt64();
            // Basic Info
            objclsCandidate.JobID = ddlJob.SelectedValue.ToInt32();
            //objclsCandidate.IsThroughAgency = chkThroughAgency.Checked ? true : false;
            //objclsCandidate.AgencyID = ddlAgency.SelectedValue.ToInt32();
            objclsCandidate.SalutationID = rcSalutation.SelectedValue.ToInt32();
            objclsCandidate.FirstNameEng = txtEnglishFirstName.Text.Trim();
            objclsCandidate.SecondNameEng = txtEnglishSecondName.Text.Trim();
            objclsCandidate.ThirdNameEng = txtEnglishThirdName.Text.Trim();
            objclsCandidate.FirstNameArb = txtArabicFirstName.Text.Trim();
            objclsCandidate.SecondNameArb = txtArabicSecondName.Text.Trim();
            objclsCandidate.ThirdNameArb = txtArabicThirdName.Text.Trim();
            objclsCandidate.Citizenship = txtCitizenship.Text.Trim();
            objclsCandidate.CitizenshipArb = txtCitizenshipArb.Text.Trim();
            objclsCandidate.PlaceofBirth = txtPlaceOfBirth.Text.Trim();
            objclsCandidate.PlaceofBirthArb = txtPlaceOfBirthArb.Text.Trim();

            if (txtDateofBirth.Text.Trim() != "")
                objclsCandidate.DateofBirth = clsCommon.Convert2DateTime(txtDateofBirth.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.DateofBirth = "";

            if (txtDateofBirthArb.Text.Trim() != "")
                objclsCandidate.DateofBirthArb = clsCommon.Convert2DateTime(txtDateofBirthArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.DateofBirthArb = "";

            objclsCandidate.Gender = (rblGender.SelectedValue == "0" ? true : false);
            objclsCandidate.MaritalStatusID = ddlMaritalStatus.SelectedValue.ToInt32();
            objclsCandidate.NoofSon = txtNoOfSon.Text.ToInt32();
            objclsCandidate.NoofSonArb = txtNoOfSonArb.Text.ToInt32();
            objclsCandidate.MotherName = txtMotherName.Text.Trim();
            objclsCandidate.MotherNameArb = txtMotherNameArb.Text.Trim();
            objclsCandidate.CurrentNationalityID = ddlCurrentNationality.SelectedValue.ToInt32();
            objclsCandidate.PreviousNationalityID = ddlPreviousNationality.SelectedValue.ToInt32();
            objclsCandidate.Doctrine = txtDoctrine.Text.Trim();
            objclsCandidate.DoctrineArb = txtDoctrineArb.Text.Trim();
            objclsCandidate.ReligionID = rcReligion.SelectedValue.ToInt32();
            objclsCandidate.CurrentAddress = txtCurrentAddress.Text.Trim();
            objclsCandidate.CurrentAddressArb = txtCurrentAddressArb.Text.Trim();
            objclsCandidate.CurrentPOBox = txtCurrentPOBox.Text.Trim();
            objclsCandidate.CurrentPOBoxArb = txtCurrentPOBoxArb.Text.Trim();
            objclsCandidate.CountryID = rcCountry.SelectedValue.ToInt32();
            objclsCandidate.CurrentTelephone = txtCurrentTelephoneNumber.Text.Trim();
            objclsCandidate.CurrentTelephoneArb = txtCurrentTelephoneNumberArb.Text.Trim();
            objclsCandidate.BasicBusinessPair = txtPlaceofBusinessPair.Text.Trim();
            objclsCandidate.BasicBusinessPairArb = txtPlaceofBusinessPairArb.Text.Trim();
            objclsCandidate.BasicBusinessPairAddress = txtBasicBusinessPairAddress.Text.Trim();
            objclsCandidate.BasicBusinessPairAddressArb = txtBasicBusinessPairAddressArb.Text.Trim();
            objclsCandidate.ReferralType = ddlReferralType.SelectedValue.ToInt32();
            objclsCandidate.ReferredBy = ddlReferredBy.SelectedValue.ToInt32();
            objclsCandidate.BasicCompanyTypeID = ddlCompanyType.SelectedValue.ToInt32();
            objclsCandidate.CreatedBy = objclsUserMaster.GetUserId();

            if (objclsCandidate.CreatedBy == 0)
                objclsCandidate.IsSelf = true;
            else
                objclsCandidate.IsSelf = false;

            // Professional Info
            objclsCandidate.PreviousJob = txtPreviousJob.Text.Trim();
            objclsCandidate.PreviousJobArb = txtPreviousJobArb.Text.Trim();
            objclsCandidate.QualificationCategoryID = ddlQualificationCategory.SelectedValue.ToInt32();
            objclsCandidate.QualificationID = rcDegree.SelectedValue.ToInt32();
            //objclsCandidate.Graduation = txtGraduation.Text.Trim();
            //objclsCandidate.GraduationArb = txtGraduationArb.Text.Trim();
            objclsCandidate.GraduationYear = ddlDegreeCompletionYear.SelectedValue.ToInt32();
            objclsCandidate.GraduationYearArb = ddlDegreeCompletionYearArb.SelectedValue.ToInt32();
            objclsCandidate.PlaceOfGraduation = txtPlaceofGraduation.Text.Trim();
            objclsCandidate.PlaceOfGraduationArb = txtPlaceofGraduationArb.Text.Trim();
            objclsCandidate.CollegeSchool = txtCollegeSchool.Text.Trim();
            objclsCandidate.CollegeSchoolArb = txtCollegeSchoolArb.Text.Trim();
            objclsCandidate.Sector = txtSector.Text.Trim();
            objclsCandidate.SectorArb = txtSectorArb.Text.Trim();
            objclsCandidate.Position = txtPosition.Text.Trim();
            objclsCandidate.PositionArb = txtPositionArb.Text.Trim();
            objclsCandidate.TypeofExperience = txtTypeofExperience.Text.Trim();
            objclsCandidate.TypeofExperienceArb = txtTypeofExperienceArb.Text.Trim();
            objclsCandidate.ExperienceYear = ddlYear.SelectedValue.ToInt32();
            objclsCandidate.ExperienceYearArb = ddlYearArb.SelectedValue.ToInt32();
            objclsCandidate.ExperienceMonth = ddlMonth.SelectedValue.ToInt32();
            objclsCandidate.ExperienceMonthArb = ddlMonthArb.SelectedValue.ToInt32();
            objclsCandidate.Compensation = txtCompensation.Text.Trim();
            objclsCandidate.CompensationArb = txtCompensationArb.Text.Trim();
            objclsCandidate.Specialization = txtSpecialization.Text.Trim();
            objclsCandidate.SpecializationArb = txtSpecializationArb.Text.Trim();

            if (txtDateofTransactionEntry.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtDateofTransactionEntry.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.TransactionEntryDate = clsCommon.Convert2DateTime(txtDateofTransactionEntry.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.TransactionEntryDate = "";

            if (txtDateofTransactionEntryArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtDateofTransactionEntryArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.TransactionEntryDateArb = clsCommon.Convert2DateTime(txtDateofTransactionEntryArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.TransactionEntryDateArb = "";

            if (txtExpectedJoinDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtExpectedJoinDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.ExpectedJoinDate = clsCommon.Convert2DateTime(txtExpectedJoinDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.ExpectedJoinDate = "";

            if (txtExpectedJoinDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtExpectedJoinDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.ExpectedJoinDateArb = clsCommon.Convert2DateTime(txtExpectedJoinDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.ExpectedJoinDateArb = "";

            // Document Info
            objclsCandidate.PassportNumber = txtPassportNumber.Text.Trim();
            objclsCandidate.PassportNumberArb = txtPassportNumberArb.Text.Trim();
            objclsCandidate.PassportCountryID = ddlPassportCountry.SelectedValue.ToInt32();
            objclsCandidate.PassportIssuePlace = txtPlaceofPassportIssue.Text.Trim();
            objclsCandidate.PassportIssuePlaceArb = txtPlaceofPassportIssueArb.Text.Trim();

            if (txtPassportExpiryDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtPassportExpiryDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.PassportExpiryDate = clsCommon.Convert2DateTime(txtPassportExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportExpiryDate = "";

            if (txtPassportExpiryDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtPassportExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.PassportExpiryDateArb = clsCommon.Convert2DateTime(txtPassportExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportExpiryDateArb = "";

            if (txtPassportReleaseDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtPassportReleaseDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.PassportReleaseDate = clsCommon.Convert2DateTime(txtPassportReleaseDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportReleaseDate = "";

            if (txtPassportReleaseDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtPassportReleaseDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.PassportReleaseDateArb = clsCommon.Convert2DateTime(txtPassportReleaseDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.PassportReleaseDateArb = "";

            objclsCandidate.VisaNumber = txtVisaNumber.Text.Trim();
            objclsCandidate.VisaNumberArb = txtVisaNumberArb.Text.Trim();
            objclsCandidate.UnifiedNumber = txtUnifiedNumber.Text.Trim();
            objclsCandidate.UnifiedNumberArb = txtUnifiedNumberArb.Text.Trim();
            objclsCandidate.VisaCountryID = ddlVisaCountry.SelectedValue.ToInt32();
            objclsCandidate.VisaIssuePlace = txtVisaPlaceofIssue.Text.Trim();
            objclsCandidate.VisaIssuePlaceArb = txtVisaPlaceofIssueArb.Text.Trim();

            if (txtVisaExpiryDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtVisaExpiryDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.VisaExpiryDate = clsCommon.Convert2DateTime(txtVisaExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaExpiryDate = "";

            if (txtVisaExpiryDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtVisaExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.VisaExpiryDateArb = clsCommon.Convert2DateTime(txtVisaExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaExpiryDateArb = "";

            if (txtVisaReleaseDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtVisaReleaseDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.VisaReleaseDate = clsCommon.Convert2DateTime(txtVisaReleaseDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaReleaseDate = "";

            if (txtVisaReleaseDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtVisaReleaseDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.VisaReleaseDateArb = clsCommon.Convert2DateTime(txtVisaReleaseDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.VisaReleaseDateArb = "";

            objclsCandidate.NationalCardNumber = txtNationalCardNumber.Text.Trim();
            objclsCandidate.NationalCardNumberArb = txtNationalCardNumberArb.Text.Trim();
            objclsCandidate.CardNumber = txtCardNumber.Text.Trim();
            objclsCandidate.CardNumberArb = txtCardNumberArb.Text.Trim();

            if (txtNationalCardExpiryDate.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtNationalCardExpiryDate.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.NationalCardExpiryDate = clsCommon.Convert2DateTime(txtNationalCardExpiryDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.NationalCardExpiryDate = "";

            if (txtNationalCardExpiryDateArb.Text.Trim() != "" &&
                clsCommon.Convert2DateTime(txtNationalCardExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy") != "01 Jan 1900")
                objclsCandidate.NationalCardExpiryDateArb = clsCommon.Convert2DateTime(txtNationalCardExpiryDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.NationalCardExpiryDateArb = "";

            // Other Info
            objclsCandidate.PermanentAddress = txtPermenantAddress.Text.Trim();
            objclsCandidate.PermanentAddressArb = txtPermenantAddressArb.Text.Trim();
            objclsCandidate.HomeStreet = txtHomeStreet.Text.Trim();
            objclsCandidate.HomeStreetArb = txtHomeStreetArb.Text.Trim();
            objclsCandidate.City = txtCity.Text.Trim();
            objclsCandidate.CityArb = txtCityArb.Text.Trim();
            objclsCandidate.HomeTelephone = txtHomeTelephone.Text.Trim();
            objclsCandidate.HomeTelephoneArb = txtHomeTelephoneArb.Text.Trim();
            objclsCandidate.BusinessAddress = txtBusinessAddress.Text.Trim();
            objclsCandidate.BusinessAddressArb = txtBusinessAddressArb.Text.Trim();
            objclsCandidate.BusinessStreet = txtBusinessStreet.Text.Trim();
            objclsCandidate.BusinessStreetArb = txtBusinessStreetArb.Text.Trim();
            objclsCandidate.Area = txtArea.Text.Trim();
            objclsCandidate.AreaArb = txtAreaArb.Text.Trim();
            objclsCandidate.PermanentPOBox = txtPermenantPOBox.Text.Trim();
            objclsCandidate.PermanentPOBoxArb = txtPermenantPOBoxArb.Text.Trim();
            objclsCandidate.Fax = txtFax.Text.Trim();
            objclsCandidate.FaxArb = txtFaxArb.Text.Trim();
            objclsCandidate.PermanentTelephone = txtPhoneNumber.Text.Trim();
            objclsCandidate.PermanentTelephoneArb = txtPhoneNumberArb.Text.Trim();
            objclsCandidate.MobileNumber = txtMobileNumber.Text.Trim();
            objclsCandidate.MobileNumberArb = txtMobileNumberArb.Text.Trim();
            objclsCandidate.EmailID = txtEmail.Text.Trim();
            objclsCandidate.InsuranceNumber = txtInsuranceNumber.Text.Trim();
            objclsCandidate.InsuranceNumberArb = txtInsuranceNumberArb.Text.Trim();

            if (txtSignUpDate.Text.Trim() != "")
                objclsCandidate.SignUpDate = clsCommon.Convert2DateTime(txtSignUpDate.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.SignUpDate = "";

            if (txtSignUpDateArb.Text.Trim() != "")
                objclsCandidate.SignUpDateArb = clsCommon.Convert2DateTime(txtSignUpDateArb.Text.Trim()).ToString("dd MMM yyyy");
            else
                objclsCandidate.SignUpDateArb = "";

            // Skills
            DataTable datTemp = new DataTable();
            objclsCandidate.Skill = txtSkills.Text.Trim();
            //datTemp.Columns.Add("SkillID");

            //foreach (ListItem item in chlSkills.Items)
            //{
            //    if (item.Selected)
            //    {
            //        DataRow dr = datTemp.NewRow();
            //        dr["SkillID"] = item.Value.ToInt32();
            //        datTemp.Rows.Add(dr);
            //    }
            //}

            // Languages
            foreach (ListItem item in chlArabic.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToString() == "قراءة [Reading]")
                        objclsCandidate.ReadingArb = true;

                    if (item.Value.ToString() == "كتابة [Writing]")
                        objclsCandidate.WritingArb = true;
                }
            }

            foreach (ListItem item in chlEnglish.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToString() == "قراءة [Reading]")
                        objclsCandidate.ReadingEng = true;

                    if (item.Value.ToString() == "كتابة [Writing]")
                        objclsCandidate.WritingEng = true;
                }
            }

            objclsCandidate.OtherLanguages = txtOtherLanguages.Text.Trim();

            foreach (ListItem item in chlOtherLanguages.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.ToString() == "قراءة [Reading]")
                        objclsCandidate.ReadingOtherLanguages = true;

                    if (item.Value.ToString() == "كتابة [Writing]")
                        objclsCandidate.WritingOtherLanguages = true;
                }
            }

            bool blnIsAddMode = true;
            if (objclsCandidate.CandidateID > 0)
                blnIsAddMode = false;

            if (objclsCandidate.CheckExistsCandidateCode())
                getCandidateCode();

            objclsCandidate.CandidateCode = txtCandidateCode.Text.Trim();

            //if (txtPassword.Text.Trim() == "")
            //    objclsCandidate.Password = clsCommon.Encrypt(txtCandidateCode.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);
            //else
            //    objclsCandidate.Password = clsCommon.Encrypt(txtPassword.Text.Trim(), ConfigurationManager.AppSettings["_ENCRYPTION"]);

            long lngTempCID = objclsCandidate.InsertCandidateBasicInfo(datTemp, blnIsAddMode);

            if (lngTempCID > 0)
            {
                if (Session["Photo"] != null)
                {
                    System.IO.DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/documents/candidatephotos"));

                    foreach (System.IO.FileInfo file in dirInfo.GetFiles(lngTempCID + ".*"))
                    {
                        file.Delete();
                    }
                    File.Move(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()), Server.MapPath("~/documents/candidatephotos/" + lngTempCID + ".jpg"));
                }

                string sMessage = string.Empty;

                if (blnIsAddMode)
                {
                    sMessage = "Application No: AN " + lngTempCID + ", Passport No: " + txtPassportNumber.Text + "\\nCongratulations, You have successfully registered for the vacancy";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + sMessage + "' );", true);
                }
                else
                {
                    sMessage = "Candidate Info (" + txtCandidateCode.Text + " ) Updated Successfully";
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), new Guid().ToString(), "alert('" + sMessage + "');", true);
                }
                objclsCandidate.Commit();

                objclsCandidate.CandidateID = lngTempCID;
                SaveCandidateDocuments();

                AddNew();
            }
        }
        catch (Exception ex)
        {
            objclsCandidate.RollBack();
        }
    }

    protected void lnkListCandidate_Click(object sender, EventArgs e)
    {
        Response.Redirect("CandidateArabicView.aspx");
    }

    protected void lnkFeverOption_Click(object sender, EventArgs e)
    {
        Response.Redirect("CandidateEng.aspx?CandidateID=" + ViewState["CandidateID"].ToInt64() + "");
    }

    protected void lnkUniformMeasurement_Click(object sender, EventArgs e)
    {
        Response.Redirect("UniformMeasurement.ascx");
    }

    private void getCandidateDetails(long CandidateID)
    {
        objclsCandidate = new clsCandidate();
        AddNew();
        objclsCandidate.CandidateID = CandidateID;
        //hfCandidateID.Value = CandidateID.ToString();
        ViewState["CandidateID"] = CandidateID.ToString();
        DataSet dsTemp = objclsCandidate.getCandidateDetails();

        if (dsTemp != null)
        {
            if (dsTemp.Tables.Count > 0)
            {
                if (dsTemp.Tables[0] != null) // Basic Info
                {
                    if (dsTemp.Tables[0].Rows.Count > 0)
                    {
                        ddlJob.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["JobID"]);
                        txtCandidateCode.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CandidateCode"]);
                        rcSalutation.SelectedValue = rcSalutationArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["SalutationID"]);
                        txtEnglishFirstName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["FirstNameEng"]);
                        txtEnglishSecondName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["SecondNameEng"]);
                        txtEnglishThirdName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["ThirdNameEng"]);
                        txtArabicFirstName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["FirstNameArb"]);
                        txtArabicSecondName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["SecondNameArb"]);
                        txtArabicThirdName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["ThirdNameArb"]);
                        txtCitizenship.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["Citizenship"]);
                        txtCitizenshipArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CitizenshipArb"]);
                        txtPlaceOfBirth.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["PlaceofBirth"]);
                        txtPlaceOfBirthArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["PlaceofBirthArb"]);
                        txtDateofBirth.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["DateofBirth"]);
                        txtDateofBirthArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["DateofBirth"]);

                        foreach (DataRow row in dsTemp.Tables[0].Rows)
                        {
                            if (row["Gender"].ToBoolean())
                                rblGender.Items.FindByValue("0").Selected = true;
                            else
                                rblGender.Items.FindByValue("1").Selected = true;
                        }

                        ddlMaritalStatus.SelectedValue = ddlMaritalStatusArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["MaritalStatusID"]);
                        txtNoOfSon.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["NoofSon"]);
                        txtNoOfSonArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["NoofSon"]);
                        txtMotherName.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["MotherName"]);
                        txtMotherNameArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["MotherNameArb"]);
                        ddlCurrentNationality.SelectedValue = ddlCurrentNationalityArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentNationalityID"]);
                        ddlPreviousNationality.SelectedValue = ddlPreviousNationalityArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["PreviousNationalityID"]);
                        txtDoctrine.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["Doctrine"]);
                        txtDoctrineArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["DoctrineArb"]);
                        rcReligion.SelectedValue = rcReligionArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["ReligionID"]);
                        txtCurrentAddress.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentAddress"]);
                        txtCurrentAddressArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentAddressArb"]);
                        txtCurrentPOBox.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentPOBox"]);
                        txtCurrentPOBoxArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentPOBox"]);
                        rcCountry.SelectedValue = rcCountryArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["CountryID"]);
                        txtCurrentTelephoneNumber.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentTelephone"]);
                        txtCurrentTelephoneNumberArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["CurrentTelephone"]);
                        txtPlaceofBusinessPair.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["BusinessPair"]);
                        txtPlaceofBusinessPairArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["BusinessPairArb"]);
                        txtBasicBusinessPairAddress.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["BusinessPairAddress"]);
                        txtBasicBusinessPairAddressArb.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["BusinessPairAddressArb"]);
                        ddlReferralType.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["ReferralTypeId"]);
                        FillReferredBy();
                        ddlReferralTypeArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["ReferralTypeId"]);
                        FillReferredByArb();
                        ddlReferredBy.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["RefferedBy"]);
                        ddlReferredByArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["RefferedBy"]);
                        ddlCompanyType.SelectedValue = ddlCompanyTypeArb.SelectedValue = Convert.ToString(dsTemp.Tables[0].Rows[0]["CompanyTypeID"]);
                        txtEmail.Text = Convert.ToString(dsTemp.Tables[0].Rows[0]["EmailID"]);
                        if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + CandidateID.ToString() + ".jpg")))
                        {
                            divImage.Attributes.Add("style", "display:block");
                            btnRemoveRecentPhoto.Attributes.Add("style", "display:block");

                            string sFileName = Convert.ToString(CandidateID.ToString() + ".jpg");
                            imgPreview.Src = string.Format("../thumbnail.aspx?folder=candidatephotos&isCandidate=true&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
                        }
                    }
                }

                if (dsTemp.Tables[1] != null) // Professional Info
                {
                    if (dsTemp.Tables[1].Rows.Count > 0)
                    {
                        txtPreviousJob.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["PreviousJob"]);
                        txtPreviousJobArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["PreviousJobArb"]);
                        ddlQualificationCategory.SelectedValue = ddlQualificationCategoryArb.SelectedValue = Convert.ToString(dsTemp.Tables[1].Rows[0]["QualificationCategoryID"]);
                        rcDegree.SelectedValue = rcDegreeArb.SelectedValue = Convert.ToString(dsTemp.Tables[1].Rows[0]["DegreeID"]);
                        // txtGraduation.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["GraduationYear"]);
                        //  txtGraduationArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["GraduationArb"]);
                        ddlDegreeCompletionYear.SelectedValue = Convert.ToString(dsTemp.Tables[1].Rows[0]["GraduationYear"]);
                        ddlDegreeCompletionYearArb.SelectedValue = Convert.ToString(dsTemp.Tables[1].Rows[0]["GraduationYear"]);

                        txtPlaceofGraduation.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["PlaceOfGraduation"]);
                        txtPlaceofGraduationArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["PlaceOfGraduationArb"]);
                        txtCollegeSchool.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["CollegeSchool"]);
                        txtCollegeSchoolArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["CollegeSchoolArb"]);
                        txtSector.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["Sector"]);
                        txtSectorArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["SectorArb"]);
                        txtPosition.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["Position"]);
                        txtPositionArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["PositionArb"]);
                        txtTypeofExperience.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["TypeofExperience"]);
                        txtTypeofExperienceArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["TypeofExperienceArb"]);

                        try
                        {

                            ddlYear.SelectedValue = dsTemp.Tables[1].Rows[0]["ExperienceYear"].ToString();
                            //if (Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"]).Length > 1)
                            //    ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue(Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"])));
                            //else
                            //    ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue("0" + Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"])));
                        }
                        catch { }

                        try
                        {
                            ddlYearArb.SelectedValue = dsTemp.Tables[1].Rows[0]["ExperienceYear"].ToString();
                            //if (Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"]).Length > 1)//ExperienceYearArb
                            //    ddlYearArb.SelectedIndex = ddlYearArb.Items.IndexOf(ddlYearArb.Items.FindByValue(Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"])));
                            //else
                            //    ddlYearArb.SelectedIndex = ddlYearArb.Items.IndexOf(ddlYearArb.Items.FindByValue("0" + Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceYear"])));//ExperienceYearArb
                        }
                        catch { }

                        try
                        {

                            if (Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"]).Length > 1)
                                ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue(Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"])));
                            else
                                ddlMonth.SelectedIndex = ddlMonth.Items.IndexOf(ddlMonth.Items.FindByValue("0" + Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"])));
                        }
                        catch { }

                        try
                        {

                            if (Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"]).Length > 1)//ExperienceMonthArb
                                ddlMonthArb.SelectedIndex = ddlMonthArb.Items.IndexOf(ddlMonthArb.Items.FindByValue(Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"])));
                            else
                                ddlMonthArb.SelectedIndex = ddlMonthArb.Items.IndexOf(ddlMonthArb.Items.FindByValue("0" + Convert.ToString(dsTemp.Tables[1].Rows[0]["ExperienceMonth"])));  //   ExperienceMonthArb                       
                        }
                        catch { }

                        txtCompensation.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["Compensation"]);
                        txtCompensationArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["CompensationArb"]);
                        txtSpecialization.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["Specialization"]);
                        txtSpecializationArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["SpecializationArb"]);
                        txtDateofTransactionEntry.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["TransactionEntryDate"]);
                        txtDateofTransactionEntryArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["TransactionEntryDate"]);
                        txtExpectedJoinDate.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["ExpectedJoinDate"]);
                        txtExpectedJoinDateArb.Text = Convert.ToString(dsTemp.Tables[1].Rows[0]["ExpectedJoinDate"]);
                        txtSkills.Text = dsTemp.Tables[1].Rows[0]["Skills"].ToStringCustom();
                    }
                }

                if (dsTemp.Tables[2] != null) // Document Info
                {
                    if (dsTemp.Tables[2].Rows.Count > 0)
                    {
                        txtPassportNumber.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportNumber"]);
                        txtPassportNumberArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportNumberArb"]);
                        ddlPassportCountry.SelectedValue = ddlPassportCountryArb.SelectedValue = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportCountryID"]);
                        txtPlaceofPassportIssue.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportIssuePlace"]);
                        txtPlaceofPassportIssueArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportIssuePlaceArb"]);
                        txtPassportExpiryDate.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportExpiryDate"]);
                        txtPassportExpiryDateArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportExpiryDate"]);
                        txtPassportReleaseDate.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportReleaseDate"]);
                        txtPassportReleaseDateArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["PassportReleaseDate"]);
                        txtVisaNumber.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaNumber"]);
                        txtVisaNumberArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaNumberArb"]);
                        txtUnifiedNumber.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["UnifiedNumber"]);
                        txtUnifiedNumberArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["UnifiedNumberArb"]);
                        ddlVisaCountry.SelectedValue = ddlVisaCountryArb.SelectedValue = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaCountryID"]);
                        txtVisaPlaceofIssue.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaIssuePlace"]);
                        txtVisaPlaceofIssueArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaIssuePlaceArb"]);
                        txtVisaExpiryDate.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaExpiryDate"]);
                        txtVisaExpiryDateArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaExpiryDate"]);
                        txtVisaReleaseDate.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaReleaseDate"]);
                        txtVisaReleaseDateArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["VisaReleaseDate"]);
                        txtNationalCardNumber.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["NationalCardNumber"]);
                        txtNationalCardNumberArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["NationalCardNumberArb"]);
                        txtCardNumber.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["CardNumber"]);
                        txtCardNumberArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["CardNumberArb"]);
                        txtNationalCardExpiryDate.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["NationalCardExpiryDate"]);
                        txtNationalCardExpiryDateArb.Text = Convert.ToString(dsTemp.Tables[2].Rows[0]["NationalCardExpiryDate"]);
                    }
                }

                if (dsTemp.Tables[3] != null) // Other Info
                {
                    if (dsTemp.Tables[3].Rows.Count > 0)
                    {
                        txtPermenantAddress.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentAddress"]);
                        txtPermenantAddressArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentAddressArb"]);
                        txtHomeStreet.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["HomeStreet"]);
                        txtHomeStreetArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["HomeStreetArb"]);
                        txtCity.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["City"]);
                        txtCityArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["CityArb"]);
                        txtHomeTelephone.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["HomeTelephone"]);
                        txtHomeTelephoneArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["HomeTelephone"]);
                        txtBusinessAddress.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["BusinessAddress"]);
                        txtBusinessAddressArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["BusinessAddressArb"]);
                        txtBusinessStreet.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["BusinessStreet"]);
                        txtBusinessStreetArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["BusinessStreetArb"]);
                        txtArea.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["Area"]);
                        txtAreaArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["AreaArb"]);
                        txtPermenantPOBox.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentPOBox"]);
                        txtPermenantPOBoxArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentPOBox"]);
                        txtFax.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["Fax"]);
                        txtFaxArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["Fax"]);
                        txtPhoneNumber.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentTelephone"]);
                        txtPhoneNumberArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["PermanentTelephone"]);
                        txtMobileNumber.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["MobileNumber"]);
                        txtMobileNumberArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["MobileNumber"]);

                        txtInsuranceNumber.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["InsuranceNumber"]);
                        txtInsuranceNumberArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["InsuranceNumberArb"]);
                        txtSignUpDate.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["SignUpDate"]);
                        txtSignUpDateArb.Text = Convert.ToString(dsTemp.Tables[3].Rows[0]["SignUpDate"]);
                    }
                }

                if (dsTemp.Tables[4] != null) // Languages
                {
                    if (dsTemp.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row in dsTemp.Tables[4].Rows)
                        {
                            if (row["ReadingArb"].ToBoolean())
                                chlArabic.Items.FindByValue("قراءة [Reading]").Selected = true;

                            if (row["WritingArb"].ToBoolean())
                                chlArabic.Items.FindByValue("كتابة [Writing]").Selected = true;

                            if (row["ReadingEng"].ToBoolean())
                                chlEnglish.Items.FindByValue("قراءة [Reading]").Selected = true;

                            if (row["WritingEng"].ToBoolean())
                                chlEnglish.Items.FindByValue("كتابة [Writing]").Selected = true;

                            if (row["ReadingOther"].ToBoolean())
                                chlOtherLanguages.Items.FindByValue("قراءة [Reading]").Selected = true;

                            if (row["WritingOther"].ToBoolean())
                                chlOtherLanguages.Items.FindByValue("كتابة [Writing]").Selected = true;
                        }

                        txtOtherLanguages.Text = Convert.ToString(dsTemp.Tables[4].Rows[0]["Other"]);
                    }
                }

                //if (dsTemp.Tables[5] != null) // Skills
                //{
                //    if (dsTemp.Tables[5].Rows.Count > 0)
                //    {
                //        foreach (DataRow row in dsTemp.Tables[5].Rows)
                //        {
                //            if (chlSkills.Items.FindByValue(Convert.ToString(row["SkillID"])) != null)
                //                chlSkills.Items.FindByValue(Convert.ToString(row["SkillID"])).Selected = true;
                //        }
                //    }
                //}
            }
        }

        GetCandidateAttachedDocuments(CandidateID);
    }


    private void GetCandidateAttachedDocuments(Int64 CandidateID)
    {
        try
        {
            DataTable dt = clsCandidate.GetCandidateAttachedDocuments(CandidateID);

            ViewState["Documents"] = dt;

            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();

            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;


        }
        catch (Exception ex)
        {
        }
    }

    protected void lnkAttachDocuments_Click(object sender, EventArgs e)
    {
        string strCandidateName = txtEnglishFirstName.Text.Trim() + " " + txtEnglishSecondName.Text.Trim() +
            " " + txtEnglishThirdName.Text.Trim();

        Response.Redirect("CandidateDoc.aspx?CandidateID=" + ViewState["CandidateID"].ToInt64() +
            "&CandidateName=" + strCandidateName + "&CandidateCode=" + txtCandidateCode.Text.Trim());
    }


    protected void fuRecentPhoto_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        btnClearRecentPhoto.Attributes.Add("style", "display:block");

        if (!Directory.Exists(Server.MapPath("~/Documents/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/"));

        if (!Directory.Exists(Server.MapPath("~/Documents/temp/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/temp/"));

        if (!Directory.Exists(Server.MapPath("~/Documents/candidatephotos/")))
            Directory.CreateDirectory(Server.MapPath("~/Documents/candidatephotos/"));

        if (fuRecentPhoto.HasFile)
        {
            filename = Session.SessionID + ".jpg";
            Session["Photo"] = filename;
            fuRecentPhoto.SaveAs(Server.MapPath("~/Documents/temp/") + filename);
        }
    }

    protected void btnClearRecentPhoto_Click(object sender, EventArgs e)
    {
        ClearPhoto();
    }

    protected void btnRemoveRecentPhoto_Click(object sender, EventArgs e)
    {
        RemovePhoto();
    }

    public void ClearPhoto()
    {
        if (fuRecentPhoto.HasFile)
        {
            File.Delete(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()));
            fuRecentPhoto.ClearFileFromPersistedStore();
            Session["Photo"] = null;
        }

        ScriptManager.RegisterClientScriptBlock(btnClearRecentPhoto, btnClearRecentPhoto.GetType(), "Clear", "SetCandidateTab('pnlTab1')", true);
    }

    public void RemovePhoto()
    {
        if (ViewState["CandidateID"] != "" && ViewState["CandidateID"] != "0")
        {
            if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + ViewState["CandidateID"] + ".jpg")))
            {
                System.IO.DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/documents/candidatephotos"));

                foreach (System.IO.FileInfo file in dirInfo.GetFiles(ViewState["CandidateID"] + ".*"))
                {
                    file.Delete();
                }
            }
        }

        //if (fuRecentPhoto.HasFile)
        //{
        //    File.Delete(Server.MapPath("~/documents/temp/" + Session["Photo"].ToString()));
        //    fuRecentPhoto.ClearFileFromPersistedStore();
        //    Session["Photo"] = null;
        //}

        divImage.Attributes.Add("style", "display:none");
        btnRemoveRecentPhoto.Attributes.Add("style", "display:none");
        imgPreview.Src = "";

        ScriptManager.RegisterClientScriptBlock(btnRemoveRecentPhoto, btnRemoveRecentPhoto.GetType(), "Clear", "SetCandidateTab('pnlTab1')", true);
    }

    public string GetCandidatePhoto(object oCandidateId)
    {
        string sFileName = string.Empty;

        if (File.Exists(Server.MapPath("~/documents/candidatephotos/" + oCandidateId.ToString() + ".jpg")))
        {
            sFileName = Convert.ToString(oCandidateId + ".jpg");
            return string.Format("../thumbnail.aspx?folder=candidatephotos&file={0}&width=100&t={1}", sFileName, DateTime.Now.Ticks);
        }
        else
        {
            sFileName = "../images/candidate.jpg";
            return sFileName;
        }
    }
    protected void btnAttachCandidateDocument_Click(object sender, EventArgs e)
    {
        try
        {
            if (fuCandidate.HasFile)
                fuCandidate.ClearAllFilesFromPersistedStore();
            else
            {
                ScriptManager.RegisterClientScriptBlock(upCandidateDocument, upCandidateDocument.GetType(), new Guid().ToString(), "alert('please attach a document');", true);
                return;
            }

            DataTable dt = null;

            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);
            else
            {
                dt = new DataTable();
                dt.Columns.Add("FileName");
                dt.Columns.Add("DocumentName");
                dt.Columns.Add("Location");
            }

            DataRow dr = dt.NewRow();
            dr["FileName"] = Session["FileName"];
            dr["DocumentName"] = txtCandidateDocumentName.Text;
            dr["Location"] = "../Documents/Others/" + Session["FileName"].ToString();
            dt.Rows.Add(dr);

            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            ViewState["Documents"] = dt;

            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;

            txtCandidateDocumentName.Text = "";
        }
        catch (Exception ex)
        {
        }
    }

    private void SaveCandidateDocuments()
    {
        try
        {
            DataTable dt = null;
            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            if (dt != null && dt.Rows.Count > 0)
            {
                new clsCandidate().DeleteCandidateAttachedDocuments(objclsCandidate.CandidateID, string.Empty);

                foreach (DataRow dr in dt.Rows)
                {
                    new clsCandidate().SaveCandidateDocuments(dr["FileName"].ToString(), dr["DocumentName"].ToString(), objclsCandidate.CandidateID);
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void fuCandidate_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            string FileName = string.Empty;
            string ActualFileName = string.Empty;

            if (!Directory.Exists(Server.MapPath("~/Documents/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/"));

            if (!Directory.Exists(Server.MapPath("~/Documents/Others/")))
                Directory.CreateDirectory(Server.MapPath("~/Documents/Others/"));

            if (fuCandidate.HasFile)
            {
                ActualFileName = fuCandidate.FileName;
                FileName = DateTime.Now.Ticks.ToString() + new Random().Next(0, 1000).ToString() + Path.GetExtension(fuCandidate.FileName);
                Session["FileName"] = FileName;
                fuCandidate.SaveAs(Server.MapPath("~/Documents/Others/") + FileName);
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataTable dt = null;
            string FileName = ((System.Web.UI.WebControls.ImageButton)(sender)).CommandArgument;
            new clsCandidate().DeleteCandidateAttachedDocuments(ViewState["CandidateID"].ToInt64(), FileName);

            if (ViewState["Documents"] != null)
                dt = ((DataTable)ViewState["Documents"]);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FileName"].ToString() == FileName)
                {
                    dt.Rows.Remove(dt.Rows[i]);
                }
            }

            dlCandidateDocument.DataSource = dt;
            dlCandidateDocument.DataBind();
            if (dt.Rows.Count > 0)
                dvAttachedDocuments.Visible = true;
            else
                dvAttachedDocuments.Visible = false;
        }
        catch (Exception ex)
        {
        }
    }

    //public  void getAgency()
    //{
    //    LoadCombos(2);
    //}

    public void getSalutation()
    {
        LoadCombos(3);
    }

    public void getMaritalStatus()
    {
        LoadCombos(4);
    }

    public void getReligion()
    {
        LoadCombos(5);
    }

    public void getCountry()
    {
        LoadCombos(6);
    }

    public void getCountryPassport()
    {
        LoadCombos(7);
    }

    public void getCountryVisa()
    {
        LoadCombos(8);
    }

    public void getCompanyType()
    {
        LoadCombos(9);
    }

    public void getQualification()
    {
        LoadCombos(10);
    }

    public void getCurrentNationality()
    {
        LoadCombos(12);
    }

    public void getPreviousNationality()
    {
        LoadCombos(13);
    }

    public void getQualificationCategory()
    {
        LoadCombos(14);
    }

    public void getSalutationArb()
    {
        LoadCombos(15);
    }

    public void getAmritalStatusArb()
    {
        LoadCombos(16);
    }

    public void getReligionArb()
    {
        LoadCombos(17);
    }

    public void getCountryArb()
    {
        LoadCombos(18);
    }

    public void getPassportCountryArb()
    {
        LoadCombos(19);
    }

    public void getVisaCountryArb()
    {
        LoadCombos(20);
    }

    public void getCompanyTypeArb()
    {
        LoadCombos(21);
    }

    public void getQualificationArb()
    {
        LoadCombos(22);
    }

    public void getCurrentNationalityArb()
    {
        LoadCombos(23);
    }

    public void getPreviousNationalityArb()
    {
        LoadCombos(24);
    }

    public void getQualificationCategoryArb()
    {
        LoadCombos(25);
    }

    public void getRefferalType()
    {
        LoadCombos(26);
    }

    public void getRefferalTypeArb()
    {
        LoadCombos(31);
    }
    public void getAgency()
    {
        LoadCombos(27);
    }

    public void getAgencyArb()
    {
        LoadCombos(28);
    }

    public void FillComboRefferedBy()
    {
        LoadCombos(21);
    }

    public void FillComboRefferedByArb()
    {
        LoadCombos(33);
    }

}
