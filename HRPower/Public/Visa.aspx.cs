﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.IO;
using System.Globalization;
using System.Reflection;
using System.Data.SqlClient;
using System.Text;

public partial class Public_Visa : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsEmployeeVisa objEmployeevisa;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    private bool MblnUpdatePermission = true;  // FOR datalist Edit Button

    private string CurrentSelectedValue
    {
        get;
        set;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("Visa.Title").ToString();

        this.RegisterAutoComplete();
      
        if (!IsPostBack)
        {
            objEmployeevisa = new clsEmployeeVisa();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            CurrentSelectedValue = "-1";
            BindCombos();
            SetPermission();
            EnableMenus();
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";
            pgrEmployees.Visible = false;
            lblNoData.Visible = false;

            txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");

            getVisaList();
        }
       
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);

        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
    }

    #region Issue Receipt
    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>" + Result + "</h5>";
        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        //lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        //lblCurrentStatus.Text = "Issued";
        //    }

        //    upMenu.Update();
        //}
    }

    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hfMode.Value == "Edit")
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);
            
            int intOperationTypeID = (int)OperationType.Employee, intDocType;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdVisaID.Value.ToInt32();
            intDocType = (int)DocumentType.Visa;
            ucDocIssueReceipt.eDocumentType = (DocumentType)intDocType;
            ucDocIssueReceipt.DocumentNumber = txtvisanumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();

            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            getVisaList();
        }
        else
            return;
    }
    #endregion Issue Receipt

    public void FillComboCountry()
    {   
        if (rcCountryOfOrigin != null && updCountryOfOrigin != null)
        {
            rcCountryOfOrigin.DataSource = new clsEmployeeVisa().FillCountry();
            rcCountryOfOrigin.DataTextField = "CountryName";
            rcCountryOfOrigin.DataValueField = "CountryId";
            rcCountryOfOrigin.DataBind();
            rcCountryOfOrigin.SelectedValue = CurrentSelectedValue;
            updCountryOfOrigin.Update();
        }
    }

    public void FillComboVisaType()
    {
        if (ddlVisaType != null && upVisaType != null)
        {
            ddlVisaType.DataSource = new clsEmployeeVisa().FillVisaType();
            ddlVisaType.DataTextField = "VisaType";
            ddlVisaType.DataValueField = "VisaTypeID";
            ddlVisaType.DataBind();
            //    rcCountryOfOrigin.SelectedValue = CurrentSelectedValue;
            upVisaType.Update();
        }
    }
    protected void Bind()
    {
        objEmployeevisa = new clsEmployeeVisa();
        objUser = new clsUserMaster();

        objEmployeevisa.PageIndex = pgrEmployees.CurrentPage + 1;
        objEmployeevisa.PageSize = pgrEmployees.PageSize;
        objEmployeevisa.SearchKey = txtSearch.Text;
        objEmployeevisa.UserId = objUser.GetUserId();
        objEmployeevisa.CompanyID = objUser.GetCompanyId();
        if (ddlExpression.SelectedValue.ToInt32() > 0)
            objEmployeevisa.SortExpression = ddlExpression.SelectedValue.ToInt32();
        else
            objEmployeevisa.SortExpression = 1;

        switch (ddlOrder.SelectedValue.ToInt32())
        {
            case 1:
                objEmployeevisa.SortOrder = "ASC";
                break;
            case 2:
                objEmployeevisa.SortOrder = "DESC";
                break;
            default:
                objEmployeevisa.SortOrder = "DESC";
                break;
        } 
        pgrEmployees.Total = objEmployeevisa.GetCount();
        DataSet ds = objEmployeevisa.GetAllEmployeeSalaryStructures();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dlEmployeevisa.DataSource = ds;
            dlEmployeevisa.DataBind();
            pgrEmployees.Visible = true;
            lblNoData.Visible = false;
            divSort.Style["display"] = "block";
        }
        else
        {
            dlEmployeevisa.DataSource = null;
            dlEmployeevisa.DataBind();
            pgrEmployees.Visible = false;
            btnPrint.Enabled = btnEmail.Enabled = btnDelete.Enabled = false;
            btnPrint.OnClientClick = btnEmail.OnClientClick = btnDelete.OnClientClick = "return false;";
            lblNoData.Visible = true;
            divSort.Style["display"] = "none";
            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetLocalResourceObject("NoVisasareaddedyet.Text").ToString(); 
            else
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();  
        }

        fvEmployees.Visible = false;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('Employee');");
        upnlNoData.Update();
    }

    protected void NewVisa()
    {
        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        divPassDoc.Style["display"] = "none";
        fvvisa.Visible = false;
        hdVisaID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        pgrEmployees.Visible = false;
        objEmployeevisa = new clsEmployeeVisa();
        fvEmployees.Visible = true;
        txtvisanumber.Text = string.Empty;
        txtPlaceofIssue.Text = string.Empty;
        txtIssuedate.Text = DateTime.Now.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
        txtExpiryDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture); 
        objEmployeevisa.VisaID = -1;
        objEmployeevisa.EmployeeID = -1;
        txtOtherInfo.Text = string.Empty;
        DataTable dtvisadoc = objEmployeevisa.GetVisaDocuments().Tables[0];
        dlVisaDoc.DataSource = dtvisadoc;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = dtvisadoc;
        btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
        btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
        objEmployeevisa.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);
        dlEmployeevisa.DataSource = null;
        dlEmployeevisa.DataBind();
        upnlNoData.Update();
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";

        upMenu.Update();
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        else
            return string.Empty;
    }

    private void BindCombos()
    {
        clsUserMaster objuser = new clsUserMaster();
        objEmployee = new clsEmployee();
        objRoleSettings = new clsRoleSettings();
        objEmployee.CompanyID = objuser.GetCompanyId();
        ddlCompany.DataSource = objEmployee.GetAllCompanies();
        ddlCompany.DataBind();
        
        if (ddlCompany.Items.Count == 0)
            ddlCompany.Items.Add(new ListItem(GetGlobalResourceObject("DocumentsCommon", "Select").ToString(), "-1"));

        ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);

        if (!(ddlCompany.Enabled))
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));//
        ddlCompany.Enabled = false;   
        objEmployeevisa.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);        
        FillComboCountry();
        FillComboVisaType();
    }

    private void FillEmployees(long lngEmployeeID)
    {
        if (objEmployeevisa == null)
            objEmployeevisa = new clsEmployeeVisa();

        ddlEmployee.DataSource = objEmployeevisa.FillEmployees(lngEmployeeID);
        ddlEmployee.DataBind();

        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem( GetGlobalResourceObject("DocumentsCommon", "Select").ToString() , "-1"));
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Visa);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }

    public void EnableMenus()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];

        if (dtm.Rows.Count > 0)
        {
            lnkRenew.Enabled = btnNew.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            btnList.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
            btnEmail.Enabled = btnPrint.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            btnDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dtm.Rows[0]["IsUpdate"].ToBoolean();
        }

        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int UserID;
        UserID = objUser.GetRoleId();

        if (UserID > 3)
        {
            if (btnList.Enabled)
            {
                Bind();
                ImgSearch.Enabled = true;
            }
            else if (btnNew.Enabled)
                NewVisa();
            else
            {
                pgrEmployees.Visible = false;
                dlEmployeevisa.DataSource = null;
                dlEmployeevisa.DataBind();
                fvEmployees.Visible = false;
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString(); 
                lblNoData.Visible = true;
                ImgSearch.Enabled = false;
            }
        }
        
        if (btnDelete.Enabled)
            btnDelete.OnClientClick = "return valDeleteDatalist('" + dlEmployeevisa.ClientID + "');";
        else
            btnDelete.OnClientClick = "return false;";

        if (btnPrint.Enabled)
            btnPrint.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeevisa.ClientID + "');";
        else
            btnPrint.OnClientClick = "return false;";

        if (btnEmail.Enabled)
            btnEmail.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeevisa.ClientID + "');";
        else
            btnEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlEmployeevisa.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }
    
    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Visa,objUser.GetCompanyId());
    }

    protected void dlEmployeevisa_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        objEmployeevisa = new clsEmployeeVisa();
        objUser = new clsUserMaster();

        switch (e.CommandName)
        {
            case "ALTER":
                hfMode.Value = "Edit";
                objEmployeevisa.VisaID = Convert.ToInt32(e.CommandArgument);
                hdVisaID.Value = Convert.ToString(e.CommandArgument);
                btnDelete.Enabled = btnEmail.Enabled = btnPrint.Enabled = false;
                btnDelete.OnClientClick = btnEmail.OnClientClick = btnPrint.OnClientClick = "return false;";
                BindFormView(Convert.ToInt32(e.CommandArgument));
                txtSearch.Text = string.Empty;
                divSort.Style["display"] = "none";
                upnlIssueReceipt.Update();
                upEmployeeVisa.Update();
                break;
            case "VIEW":
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                divIssueReceipt.Style["display"] = "none";
                divRenew.Style["display"] = "none";
                divSort.Style["display"] = "none";
                break;
        }
    }

    private void BindViewMode(int VisaID)
    {
        dlEmployeevisa.DataSource = null;
        dlEmployeevisa.DataBind();
        pgrEmployees.Visible = false;
        fvEmployees.Visible = false;
        fvvisa.Visible = true;
        hfMode.Value = "List";
        objEmployeevisa.VisaID = VisaID;
        hdVisaID.Value = Convert.ToString(VisaID);
        fvvisa.DataSource = objEmployeevisa.GetEmpSalaryStructure();
        fvvisa.DataBind();
        divPrint.InnerHtml = GetPrintText(VisaID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if(btnPrint.Enabled)
            btnPrint.OnClientClick = sPrinttbl;
            if(btnEmail.Enabled)
            btnEmail.OnClientClick = "return showMailDialog('Type=Visa&VisaID=" + Convert.ToString(fvvisa.DataKey["VisaID"]) + "');";
            if(btnDelete.Enabled )
            btnDelete.OnClientClick = "return confirm(' " + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + " ');";
            if(lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return confirm(' " + GetGlobalResourceObject("DocumentsCommon", "Areyousuretorenewtheselecteditems").ToString() + " ');";
        }
    }

    private void BindFormView(int VisaID)
    {
        fvEmployees.Visible = true;
        objEmployeevisa.VisaID = VisaID;
        DataTable dt = objEmployeevisa.GetEmployeeSalaryStructure().Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrEmployees.Visible = false;
        BindVisaDetails(dt);
        dlEmployeevisa.DataSource = null;
        dlEmployeevisa.DataBind();
        divPrint.InnerHtml = objEmployeevisa.GetPrintText(VisaID);
        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            btnPrint.OnClientClick = sPrinttbl;
            btnEmail.OnClientClick = "return showMailDialog('Type=Visa&VisaId=" + Convert.ToString(fvvisa.DataKey["VisaID"]) + "');";
            btnDelete.OnClientClick = "return true;";
            lnkRenew.OnClientClick = "return true;";
        }

        if (Convert.ToInt32(dt.Rows[0]["StatusID"]) == 1) // Receipt
            divIssueReceipt.Style["display"] = "none";
        else
            divIssueReceipt.Style["display"] = "block";

        if (Convert.ToBoolean(dt.Rows[0]["IsRenewed"]) == true) // Renewed
        {
            divRenew.Style["display"] = "none";
            lblCurrentStatus.Text += " [Renewed]";
            ControlEnableDisble(false);
        }
        else
        {
            divRenew.Style["display"] = "block";
            ControlEnableDisble(true);
        }

        upMenu.Update();
    }

    private void BindVisaDetails(DataTable dt)
    {
        upMenu.Update();

        if (dt.Rows.Count > 0)
        {
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            objEmployeevisa.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            FillEmployees(dt.Rows[0]["EmployeeID"].ToInt64());
            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dt.Rows[0]["EmployeeID"])));
            FillComboVisaType();
            ddlVisaType.SelectedIndex = ddlVisaType.Items.IndexOf(ddlVisaType.Items.FindByValue(Convert.ToString(dt.Rows[0]["VisaTypeID"])));
            if(dt.Rows[0]["IssueCountryID"] != DBNull.Value)
                rcCountryOfOrigin.SelectedValue = Convert.ToString(dt.Rows[0]["IssueCountryID"]);

            txtvisanumber.Text = Convert.ToString(dt.Rows[0]["VisaNumber"]);
            txtPlaceofIssue.Text = Convert.ToString(dt.Rows[0]["PlaceofIssue"]);
            txtIssuedate.Text = Convert.ToDateTime(dt.Rows[0]["VisaIssueDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtExpiryDate.Text = Convert.ToDateTime(dt.Rows[0]["VisaExpiryDate"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtOtherInfo.Text = dt.Rows[0]["Remarks"].ToStringCustom();
            lblCurrentStatus.Text = dt.Rows[0]["Status"].ToStringCustom();
            objEmployeevisa.VisaID = Convert.ToInt32(dt.Rows[0]["VisaID"]);
            objEmployeevisa.EmployeeID = Convert.ToInt32(dt.Rows[0]["EmployeeID"]);
            DataTable dtvisadoc = objEmployeevisa.GetVisaDocuments().Tables[0];

            dlVisaDoc.DataSource = dtvisadoc;
            dlVisaDoc.DataBind();
            ViewState["PasDoc"] = dtvisadoc;

            if (dlVisaDoc.Items.Count > 0)
                divPassDoc.Style["display"] = "block";
        }
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        //ddlCompany.Enabled = ddlEmployee.Enabled = rcCountryOfOrigin.Enabled = blnTemp;
        txtvisanumber.Enabled = txtPlaceofIssue.Enabled = blnTemp;
        txtIssuedate.Enabled = txtExpiryDate.Enabled = txtOtherInfo.Enabled = blnTemp;
        ddlCompany.Enabled = false;
    }
  
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        objEmployeevisa = new clsEmployeeVisa();
        CheckBox chkEmployee;
        LinkButton btnEmployee;
        Label lblIsDelete;
        string message = string.Empty;
        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

        if (dlEmployeevisa.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployeevisa.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    objEmployeevisa.VisaID = Convert.ToInt32(dlEmployeevisa.DataKeys[item.ItemIndex]);

                    DataTable dt = objEmployeevisa.GetVisaFilenames();
                    objEmployeevisa.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }

                    message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString(); //"Deleted successfully";
                }
                else
                    message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();   //"Once issued/renewed visa(s) cannot be deleted";
            }
        }
        else
        {
            if (hfMode.Value == "List")
            {
                objEmployeevisa.VisaID = Convert.ToInt32(fvvisa.DataKey["VisaID"]);

                if (Convert.ToBoolean(fvvisa.DataKey["IsDelete"]))
                {
                    DataTable dt = objEmployeevisa.GetVisaFilenames();
                    objEmployeevisa.Delete();

                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string sFilename = Convert.ToString(dt.Rows[i]["Filename"]);

                            if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                                File.Delete(Path + "/" + Convert.ToString(sFilename));
                        }
                    }
                    message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString();  // "Visa(s) deleted successfully";
                }
                else
                    message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();//"Once issued/renewed visa(s) cannot be deleted";
            }
        }

        if (message == "")
            message = GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString(); //"Please select an item for delete";
        
        mcMessage.InformationalMessage(message);       
        mpeMessage.Show();
        getVisaList();
        upEmployeeVisa.Update();
    }

    protected void btnList_Click(object sender, EventArgs e)
    {
        getVisaList();
    }

    private void getVisaList()
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        txtSearch.Text = string.Empty;
        pgrEmployees.CurrentPage = 0;
        fvvisa.Visible = false;
        fvvisa.DataSource = null;
        fvvisa.DataBind();
        Bind();
        hfMode.Value = "List";
        EnableMenus();
        upEmployeeVisa.Update();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        Bind();
        upMenu.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int id = 0;
        try
        {
            objEmployeevisa = new clsEmployeeVisa();
            objAlert = new clsDocumentAlert();

            objEmployeevisa.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objEmployeevisa.VisaNumber = Convert.ToString(txtvisanumber.Text.Trim());
            objEmployeevisa.VisaTypeID = ddlVisaType.SelectedValue.ToInt32();
            objEmployeevisa.PlaceofIssue = Convert.ToString(txtPlaceofIssue.Text.Trim());
            objEmployeevisa.VisaIssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text);
            objEmployeevisa.VisaExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text);
            objEmployeevisa.IssueCountryID = Convert.ToInt32(rcCountryOfOrigin.SelectedValue);
            objEmployeevisa.Remarks = txtOtherInfo.Text.Trim();

            if (hfMode.Value == "Insert")
            {
                objEmployeevisa.VisaID = 0;

                if (objEmployeevisa.IsVisaNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("VisaNumberexists.Text").ToString());  //VisaNumberexists.Text
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objEmployeevisa.BeginEmp();
                        id = objEmployeevisa.InsertEmployeevisa();
                        objEmployeevisa.VisaID = id;
                        objAlert.AlertMessage(Convert.ToInt32(DocumentType.Visa), "Visa", "VisaNumber", id, txtvisanumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), "Emp", Convert.ToInt32(ddlEmployee.SelectedValue), ddlEmployee.SelectedItem.Text, false);
                        objEmployeevisa.Commit();
                    }
                    catch (Exception ex)
                    {
                        objEmployeevisa.RollBack();
                    }                    
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objEmployeevisa.VisaID = Convert.ToInt32(hdVisaID.Value);

                if (objEmployeevisa.IsVisaNumberExists())
                {
                    mcMessage.InformationalMessage(GetLocalResourceObject("VisaNumberexists.Text").ToString());
                    mpeMessage.Show();
                    return;
                }
                else
                {
                    try
                    {
                        objEmployeevisa.BeginEmp();
                        id = objEmployeevisa.UpdateEmployeeVisa();
                        objEmployeevisa.VisaID = id;
                        objEmployeevisa.Commit();
                    }
                    catch (Exception ex)
                    {
                        objEmployeevisa.RollBack();
                    }                    
                }
            }            

            if (id == 0) return;
            //insert into treemaster and treedetails
            Label lblDocname, lblActualfilename, lblFilename;
             if (hfMode.Value == "Insert")
             {

                objEmployeevisa.Docname = Convert.ToString(txtDocname.Text.Trim());
                objEmployeevisa.Filename = Session["Filename"].ToString();
                objEmployeevisa.InsertEmployeeVisaTreemaster();  
             }

            foreach (DataListItem item in dlVisaDoc.Items)
            {
                lblDocname = (Label)item.FindControl("lblDocname");
                lblFilename = (Label)item.FindControl("lblFilename");
                lblActualfilename = (Label)item.FindControl("lblActualfilename");
                objEmployeevisa.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objEmployeevisa.VisaID = id;
                objEmployeevisa.VisaNumber = Convert.ToString(txtvisanumber.Text.Trim());
                objEmployeevisa.Docname = lblDocname.Text.Trim();
                objEmployeevisa.Filename = lblFilename.Text.Trim();

                if (dlVisaDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                {
                    objEmployeevisa.InsertEmployeeVisaTreemaster();

                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                    {
                        if (!Directory.Exists(Path))
                            Directory.CreateDirectory(Path);

                        File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                    }
                }
            }
            
            objEmployeevisa.Commit();

            if (hfMode.Value == "Insert")
            {
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString()); 
            }
            else
            {
                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString());
            }

            mpeMessage.Show();
            upEmployeeVisa.Update();
            EnableMenus();
            divIssueReceipt.Style["display"] = "block";
            divRenew.Style["display"] = "block";
            hfMode.Value = "Edit";
            hdVisaID.Value = objEmployeevisa.VisaID.ToStringCustom();
            upMenu.Update();
            upEmployeeVisa.Update();
            getVisaList();
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        {
            if (hfMode.Value == "Edit")
            {
                int id = 0;
                objEmployeevisa = new clsEmployeeVisa();
                objAlert = new clsDocumentAlert();
                objEmployeevisa.VisaID = hdVisaID.Value.ToInt32();
                try
                {
                    objEmployeevisa.BeginEmp();
                    id = objEmployeevisa.UpdateRenewEmployeeVisa();
                    objEmployeevisa.Commit();
                }
                catch (Exception ex)
                {
                    objEmployeevisa.RollBack();
                }

                objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.Visa), id);

                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "RenewedSuccessfully").ToString());
                mpeMessage.Show();
                divIssueReceipt.Style["display"] = "block";
                divRenew.Style["display"] = "block";
                hfMode.Value = "Edit";
                hdVisaID.Value = objEmployeevisa.VisaID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                upEmployeeVisa.Update();
                getVisaList();
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (btnList.Enabled)
        {
            divIssueReceipt.Style["display"] = "none";
            divRenew.Style["display"] = "none";             
            EnableMenus();
            pgrEmployees.CurrentPage = 0;
            Bind();
            fvvisa.Visible = false;
            upEmployeeVisa.Update();
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        ControlEnableDisble(true);
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        NewVisa();
        divSort.Style["display"] = "none";
        upEmployeeVisa.Update();
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlEmployeevisa.Items.Count > 0)
        {
            objEmployeevisa = new clsEmployeeVisa();

            if (fvvisa.DataKey["VisaID"] == DBNull.Value || fvvisa.DataKey["VisaID"] == null)
            {
                CheckBox chkEmployee;
                string sVisaIds = string.Empty;

                foreach (DataListItem item in dlEmployeevisa.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sVisaIds += "," + dlEmployeevisa.DataKeys[item.ItemIndex];
                }

                sVisaIds = sVisaIds.Remove(0, 1);

                if (sVisaIds.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sVisaIds);
                else
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sVisaIds));
            }
            else
                divPrint.InnerHtml = objUser.GetPrintText(Convert.ToInt32(fvvisa.DataKey["VisaID"]));

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }
    }

    private string GetPrintText(int iVisaID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@VisaID", iVisaID));
        alParameters.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));

        DataTable dt =new DataLayer(). ExecuteDataSet("HRspEmployeeVisa", alParameters).Tables[0];


        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + " " + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + " [" + Convert.ToString(dt.Rows[0]["EmployeeNumber"]) + "]" + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'> " + GetLocalResourceObject("VisaInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("VisaNumber.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaNumber"]) + "</td></tr>");
        sb.Append("<tr><td width='150px'>" + GetLocalResourceObject("VisaType.Text").ToString() + "</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["VisaType"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "PlaceOfIssue").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["PlaceofIssue"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + Convert.ToDateTime(dt.Rows[0]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Country").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Country"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["IsRenewed"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td>:</td><td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[0]["Remarks"]) + "</td></tr>");

        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }
    private string CreateSelectedEmployeesContent(string sVisaIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@VisaIds", sVisaIds));
        arraylst.Add(new SqlParameter("@IsArabic", clsGlobalization.IsArabicCulture()));
        DataTable dt = new DataLayer().ExecuteDataTable("HRspEmployeeVisa", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>" + GetLocalResourceObject("VisaInformation.Text").ToString() + "</td></tr>");
        sb.Append("<tr><td>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Employee").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("VisaNumber.Text").ToString() + "</td><td width='150px'>" + GetLocalResourceObject("VisaType.Text").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("DocumentsCommon", "PlaceOfIssue").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "Country").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='9'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + " " + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaNumber"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["VisaType"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["PlaceofIssue"]) + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaIssueDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToDateTime(dt.Rows[i]["VisaExpiryDate"]).ToString("dd/MM/yyyy") + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Country"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["IsRenewed"]) + "</td>");
            sb.Append("<td style=\"word-break: break-all;\">" + Convert.ToString(dt.Rows[i]["Remarks"]) + "</td></tr>");

        }
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }

    protected void btnEmail_Click(object sender, EventArgs e)
    {
        if (dlEmployeevisa.Items.Count > 0)
        {
            objEmployeevisa = new clsEmployeeVisa();
            string sVisaIds = string.Empty;

            if (fvvisa.DataKey["VisaID"] == DBNull.Value || fvvisa.DataKey["VisaID"] == null)
            {
                CheckBox chkEmployee;

                foreach (DataListItem item in dlEmployeevisa.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sVisaIds += "," + dlEmployeevisa.DataKeys[item.ItemIndex];
                }

                sVisaIds = sVisaIds.Remove(0, 1);
            }
            else
                sVisaIds = fvvisa.DataKey["VisaID"].ToString();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Visa&VisaID=" + sVisaIds + "', 835, 585);", true);
        }
    }

    protected void fuVisa_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuVisa.HasFile)
        {
            actualfilename = fuVisa.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuVisa.FileName);
            Session["Filename"] = filename;
            fuVisa.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }

    protected void btnattachvisadoc_Click(object sender, EventArgs e)
    {
        divPassDoc.Style["display"] = "block";
        DataTable dt;
        DataRow dw;
        objEmployeevisa = new clsEmployeeVisa();
        objEmployeevisa.VisaID = -1;
        objEmployeevisa.EmployeeID = -1;

        if (ViewState["PasDoc"] == null)
            dt = objEmployeevisa.GetVisaDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["PasDoc"];
            dt = dtDoc;
        }

        if (hfMode.Value == "Insert" || hfMode.Value == "Edit")
        {
            dw = dt.NewRow();
            dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
            dw["Filename"] = Session["Filename"];
            dt.Rows.Add(dw);
        }

        dlVisaDoc.DataSource = dt;
        dlVisaDoc.DataBind();
        ViewState["PasDoc"] = dt;
        txtDocname.Text = string.Empty;
        updldlPassportDoc.Update();
    }

    protected void dlVisaDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objEmployeevisa = new clsEmployeeVisa();

                if (e.CommandArgument != string.Empty)
                {
                    objEmployeevisa.Node = Convert.ToInt32(e.CommandArgument);
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objEmployeevisa.DeleteVisaDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }

                objEmployeevisa.VisaID = -1;
                objEmployeevisa.EmployeeID = -1;
                DataTable dt = objEmployeevisa.GetVisaDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlVisaDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    dw["Node"] = dlVisaDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlVisaDoc.DataSource = dt;
                dlVisaDoc.DataBind();
                ViewState["PasDoc"] = dt;
                break;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        objEmployeevisa = new clsEmployeeVisa();
        objEmployeevisa.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        FillEmployees(0);
    }

    protected void btnCountry_Click(object sender, EventArgs e)
    {
        ReferenceControlNew1.ClearAll();
        ReferenceControlNew1.TableName = "CountryReference";
        ReferenceControlNew1.DataTextField = "CountryName";
        ReferenceControlNew1.DataValueField = "CountryId";
        ReferenceControlNew1.FunctionName = "FillComboCountry";
        ReferenceControlNew1.SelectedValue = rcCountryOfOrigin.SelectedValue;
        ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بلد") : ("Country");
        ReferenceControlNew1.DataTextFieldArabic = "CountryNameArb";
        ReferenceControlNew1.PopulateData();
        mdlPopUpReference.Show();
        updModalPopUp.Update();
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
    
    protected void dlEmployeevisa_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }
    }

    protected void ddlExpression_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind();
    }
    protected void ddlOrder_SelectedIndexChanged(object sender, EventArgs e)
    {
        Bind();
    }
}