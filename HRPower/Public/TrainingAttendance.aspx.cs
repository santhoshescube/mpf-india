﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_TrainingAttendance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtStartDate.Text = DateTime.Now.AddDays(-8).ToString("MM-dd-yyyy");

            txtEndDate.Text = DateTime.Now.ToString("MM-dd-yyyy");

            BindTable();

        }


    }


    private void BindTable()
    {
        TableRow row = new TableRow();
        TableCell cell;

        DateTime sDt, eDt;
        DateTime.TryParse(txtStartDate.Text, out sDt);
        DateTime.TryParse(txtEndDate.Text, out eDt);



        int n = eDt.Day - sDt.Day;

        cell = new TableCell();
        cell.Text = "Employee";
        row.Cells.Add(cell);

        for (int i = 0 ; i <= n; i++)
        {
            CheckBox chkDate = new CheckBox();
            cell = new TableCell();
           
            chkDate.Text = txtStartDate.Text.ToDateTime().AddDays(i).ToString("MMM d");
            cell.Controls.Add(chkDate);
            cell.HorizontalAlign = HorizontalAlign.Center;
         
            row.Cells.Add(cell);
          
        }
        tblAttendance.GridLines = GridLines.Both;
        row.CssClass = "datalistHead";
        tblAttendance.Rows.Add(row);

        row = new TableRow();
        cell = new TableCell();
        cell.Text = "Sk Nair";
        row.Cells.Add(cell);
        for ( int i = 1 ;i < tblAttendance.Rows[0].Cells.Count ;i++)
        {
            CheckBox chkDate = new CheckBox();
            cell = new TableCell();           
            cell.Controls.Add(chkDate);
            cell.HorizontalAlign = HorizontalAlign.Center;
         
            row.Cells.Add(cell);
        }
        tblAttendance.Rows.Add(row);

        row = new TableRow();
        cell = new TableCell();
        cell.Text = "Mohammed";
        row.Cells.Add(cell);
        for (int i = 1; i < tblAttendance.Rows[0].Cells.Count; i++)
        {
            CheckBox chkDate = new CheckBox();
            cell = new TableCell();
            cell.Controls.Add(chkDate);
            cell.HorizontalAlign = HorizontalAlign.Center;
         
            row.Cells.Add(cell);
        }
        tblAttendance.Rows.Add(row);


    }


    protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindTable();
    }
   
}
