﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;

public partial class Public_Settings : System.Web.UI.Page
{
    clsUserMaster objUserMaster;
    clsRoleSettings objRoleSettings;

    protected void Page_Load(object sender, EventArgs e)
    {
        UserSettingsPager.Fill += new controls_Pager.FillPager(BindGrid);
        // Auto complete
        this.RegisterAutoComplete();
        // 
        if (!IsPostBack)
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            int UserID;
            UserID = objUserMaster.GetRoleId();
            DataTable dt = objRoleSettings.GetPermissions(UserID, (int)eMenuID.UserHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();
        }

        ((Controls_AddUser)AddUser1).OnRefresh -= new Controls_AddUser.Refresh(Public_Settings_OnRefresh); 
        ((Controls_AddUser)AddUser1).OnRefresh += new Controls_AddUser.Refresh(Public_Settings_OnRefresh); 
    }

    void Public_Settings_OnRefresh()
    {
        BindGrid();
        upGridView.Update();
    }

    public void SetPermission()
    {

        int RoleID;
        RoleID = objUserMaster.GetRoleId();
        if (RoleID != 1 && RoleID != 2 && RoleID != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                lnkAdd.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();


                if (lnkAdd.Enabled == true || lnkDelete.Enabled == true || dtm.Rows[0]["IsView"].ToBoolean() == true)
                {
                    BindGrid();
                }
                else
                {
                    lblUserSettings.Text = "You dont have enough permission to view this page.";
                    lblUserSettings.Visible = true;
                    UserSettingsPager.Visible = false;
                    lnkAdd.Enabled = false;
                    lnkDelete.Enabled = false;

                }
            }
            else
            {
                lblUserSettings.Text = "You dont have enough permission to view this page.";
                lblUserSettings.Visible = true;
                UserSettingsPager.Visible = false;
                lnkAdd.Enabled = false;
                lnkDelete.Enabled = false;
                
            }
        }
        else
        {
            BindGrid();
            lnkAdd.Enabled = true;
            lnkDelete.Enabled = true;

        }
    }




    private void BindGrid()
    {
        objUserMaster = new clsUserMaster();

        objUserMaster.PageIndex = UserSettingsPager.CurrentPage + 1;
        objUserMaster.PageSize = UserSettingsPager.PageSize;
        objUserMaster.SortExpression = "UserID";
        objUserMaster.SortOrder = "desc";
        objUserMaster.UserID = objUserMaster.GetUserId();
        objUserMaster.SearchKey = txtSearch.Text;




        objUserMaster.CompanyID = objUserMaster.GetCompanyId();

        DataSet ds = objUserMaster.FillDataSet();
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvUserSettings.DataSource = ds;
            gvUserSettings.DataBind();

            objUserMaster.UserID = objUserMaster.GetUserId();
            objUserMaster.SearchKey = txtSearch.Text;
            objUserMaster.CompanyID = objUserMaster.GetCompanyId();

            UserSettingsPager.Total = objUserMaster.GetRecordCount();
            UserSettingsPager.Visible = true;
            lblUserSettings.Text = string.Empty;

            //lnkDelete.Enabled = true;
            lnkDelete.OnClientClick = "return valDeleteDatalistSettings('" + gvUserSettings.ClientID + "');";
        }
        else
        {
            gvUserSettings.DataSource = null;
            gvUserSettings.DataBind();

            UserSettingsPager.Visible = false;
            lblUserSettings.Text = "No users added yet";

            lnkDelete.OnClientClick = "return false";
            lnkDelete.Enabled = false;
        }
    }

    protected void gvUserSettings_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        objUserMaster = new clsUserMaster();
       
        GridViewRow gr;
        gr = ((GridViewRow)((ImageButton)e.CommandSource).Parent.Parent);

        DropDownList ddlEmployee = (DropDownList)gr.FindControl("ddlEmployee");
        DropDownList ddlRole = (DropDownList)gr.FindControl("ddlRole");
        TextBox txtUserName = (TextBox)gr.FindControl("txtUserName");
        TextBox txtPassword = (TextBox)gr.FindControl("txtPassword");
        RequiredFieldValidator rfvddlEmployee = (RequiredFieldValidator)gr.FindControl("rfvddlEmployee");

        //TextBox txtPassword = (TextBox)gr.FindControl("txtPassword");
        //TextBox txtPassword = (TextBox)gr.FindControl("txtPassword");



        //CheckBox chksms = (CheckBox)gr.FindControl("chksms");

        switch (e.CommandName)
        {
            case "Add":

                objUserMaster.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                objUserMaster.UserName = txtUserName.Text;
                objUserMaster.Password = txtPassword.Text;
                objUserMaster.RoleID = Convert.ToInt32(ddlRole.SelectedValue);

                if (Convert.ToInt32(ddlRole.SelectedValue) < 3)
                {
                    rfvddlEmployee.ValidationGroup = "";
                }
                else
                    rfvddlEmployee.ValidationGroup = "submit";

                if (gvUserSettings.DataKeys[gr.RowIndex]["UserID"] == DBNull.Value)
                {

                    if (GetRoleCnt(true) == 0)
                    {
                        msgs.InformationalMessage("To add more users, Please upgrade your product version.");
                        mpeMessage.Show();
                        break;
                    }

                    if (rfvddlEmployee.ValidationGroup == "submit" && ddlEmployee.SelectedValue.ToInt32() <= 0)
                    {
                        msgs.InformationalMessage("Select employee");
                    }

                    if (!objUserMaster.CheckUserName())
                    {
                        int id = objUserMaster.InsertUserDetails();
                        if (id > -1)
                        { 
                            gvUserSettings.EditIndex = -1;
                            BindGrid();

                            msgs.InformationalMessage("User details saved successfully");
                        }
                        else
                        {
                            msgs.InformationalMessage("User exists for this employee.");
                        }
                    }
                   else if (rfvddlEmployee.ValidationGroup == "submit" && ddlEmployee.SelectedValue.ToInt32() <= 0)
                    {
                        msgs.InformationalMessage("Select employee");
                    }
                    else
                    {
                        msgs.InformationalMessage("Username already exists");
                    }
                }
                else
                {
                    objUserMaster.UserID = Convert.ToInt32(gvUserSettings.DataKeys[gr.RowIndex]["UserID"]);

                    if (GetRoleCnt(false) == 0)
                    {
                        msgs.InformationalMessage("Upgrade version only allow add  more users");
                        mpeMessage.Show();
                        break;
                    }
                     if (rfvddlEmployee.ValidationGroup == "submit" && ddlEmployee.SelectedValue.ToInt32() <= 0)
                    {
                        msgs.InformationalMessage("Please Select employee");
                    }
                    else if (!objUserMaster.CheckUserNameExist())
                    {
                        objUserMaster.UpdateUserDetails();

                        gvUserSettings.EditIndex = -1;

                        BindGrid();

                        msgs.InformationalMessage("User details updated successfully");
                    }
                    else
                    {
                        msgs.InformationalMessage("Username already exists ");
                    }
                }
               
                mpeMessage.Show();

                break;

            case "Resume":

                gvUserSettings.EditIndex = -1;

                BindGrid();

                break;

            case "Alter":

                
                mdAddUser.Show();
                updAddUser.Update();

                ((Controls_AddUser)AddUser1).Edit(e.CommandArgument.ToInt32());

                break;
               // AddUser
               // uc1
                

                /*
                int RoleID;
                RoleID = objUserMaster.GetRoleId();
                if (RoleID != 1 && RoleID != 2 && RoleID != 3)
                {
                    bool IsUpdate = false;
                    DataTable dtm = (DataTable)ViewState["Permission"];
                    if (dtm.Rows.Count > 0)
                    {
                        IsUpdate = dtm.Rows[0]["IsUpdate"].ToBoolean();

                    }
                    if (IsUpdate == false)
                    {

                        break;
                    }
                }

                gvUserSettings.EditIndex = gr.RowIndex;

                BindGrid();

                Label lblPassword = (Label)gvUserSettings.HeaderRow.FindControl("lblPassword");
                lblPassword.Text = "Password";

                upGridView.Update();

                break;
                 * */
        }
    }
    private int GetRoleCnt(bool lnAddStatus)
    {
        try
        {
            DataTable DT;
            DT = objUserMaster.GetRoleCount(lnAddStatus);
            if (DT.Rows.Count > 0)
            {
                if (DT.Rows[0][0].ToInt32() == 1)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        catch
        {
            return 0;
        }
    }
    protected void gvUserSettings_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        objUserMaster = new clsUserMaster();

        if (e.Row.RowIndex == gvUserSettings.EditIndex)
        {
            DropDownList ddlEmployee = (DropDownList)e.Row.FindControl("ddlEmployee");

            if (ddlEmployee == null)
                return;

            objUserMaster.UserID = objUserMaster.GetUserId();
            ddlEmployee.DataSource = objUserMaster.FillEmployees();
            ddlEmployee.DataBind();

            ddlEmployee.Items.Insert(0, new ListItem("select", "-1"));

            if (gvUserSettings.DataKeys[e.Row.RowIndex]["EmployeeID"] != DBNull.Value)
                ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(gvUserSettings.DataKeys[e.Row.RowIndex]["EmployeeID"])));

            DropDownList ddlRole = (DropDownList)e.Row.FindControl("ddlRole");

            if (ddlRole == null)
                return;

            ddlRole.DataSource = objUserMaster.FillRoles();
            ddlRole.DataBind();

            ddlRole.Items.Insert(0, new ListItem("select", "-1"));

            if (gvUserSettings.DataKeys[e.Row.RowIndex]["RoleID"] != DBNull.Value)
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue(Convert.ToString(gvUserSettings.DataKeys[e.Row.RowIndex]["RoleID"])));

            TextBox txtPassword = (TextBox)e.Row.FindControl("txtPassword");

            if (txtPassword != null)
            {
                txtPassword.TextMode = TextBoxMode.Password;

                HiddenField hdpass = (HiddenField)e.Row.FindControl("hdpass");

                if (hdpass.Value  != "")
                    txtPassword.Attributes.Add("value",clsCommon.Decrypt(hdpass.Value,ConfigurationManager.AppSettings["_ENCRYPTION"]));
                else
                    txtPassword.Attributes.Add("value", "");
            }

            CheckBox chksms = (CheckBox)e.Row.FindControl("chksms");

            if (chksms == null)
                return;

            if (gvUserSettings.DataKeys[e.Row.RowIndex]["SendSms"] != DBNull.Value)
                chksms.Checked = Convert.ToBoolean(gvUserSettings.DataKeys[e.Row.RowIndex]["SendSms"]);
        }
        else
        {
            CheckBox chkSendSms = (CheckBox)e.Row.FindControl("chkSendSms");

            if (chkSendSms == null)
                return;

            if (gvUserSettings.DataKeys[e.Row.RowIndex]["SendSms"] != DBNull.Value)
                chkSendSms.Checked = Convert.ToBoolean(gvUserSettings.DataKeys[e.Row.RowIndex]["SendSms"]);
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {

        mdAddUser.Show();
       
        updAddUser.Update();
        ((Controls_AddUser)AddUser1).ClearControls();   
            
            
            
            
            // ClearControls(), AddUser1

        /*Commented by Rajesh
        lblUserSettings.Text = string.Empty;

        objUserMaster = new clsUserMaster();

        objUserMaster.PageIndex = UserSettingsPager.CurrentPage + 1;
        objUserMaster.PageSize = UserSettingsPager.PageSize;
        objUserMaster.SortExpression = "UserID";
        objUserMaster.SortOrder = "desc";
        objUserMaster.UserID = objUserMaster.GetUserId();
        gvUserSettings.EditIndex = -1;

        DataSet ds = objUserMaster.FillDataSet();

        DataTable dt = ds.Tables[0];
        DataRow dw = dt.NewRow();

        dt.Rows.InsertAt(dw, 0);

        gvUserSettings.EditIndex = 0;

        gvUserSettings.DataSource = dt;
        gvUserSettings.DataBind();

        UserSettingsPager.Total = objUserMaster.GetRecordCount();

        Label lblPassword = (Label)gvUserSettings.HeaderRow.FindControl("lblPassword");
        lblPassword.Text = "Password";

        upGridView.Update();
        */
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            objUserMaster = new clsUserMaster();

            int RoleID;
            bool IsDelete = false;
            RoleID = objUserMaster.GetRoleId();
            if (RoleID != 1 && RoleID != 2 && RoleID != 3)
            {
                DataTable dtm = (DataTable)ViewState["Permission"];
                if (dtm.Rows.Count > 0)
                {
                    IsDelete = dtm.Rows[0]["IsDelete"].ToBoolean();
                }
            }
            else
            {
                IsDelete = true;
            }


            if (gvUserSettings.Rows.Count > 0)
            {
                int intDeletedItemsCount = 0;
                bool blnCheckedCurrentUser = false;
                foreach (GridViewRow gr in gvUserSettings.Rows)
                {
                    if (gr.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkUserSetting = (CheckBox)gr.FindControl("chkUserSettings");

                        if (chkUserSetting == null)
                            return;

                        if (chkUserSetting.Checked==true)
                        {
                            objUserMaster.UserID = Convert.ToInt32(gvUserSettings.DataKeys[gr.RowIndex]["UserID"]);

                            if (!objUserMaster.UserExists())
                            {
                                if (objUserMaster.UserID != new clsUserMaster().GetUserId())
                                {
                                    objUserMaster.DeleteUserDetails();
                                    intDeletedItemsCount++;
                                }
                                else
                                {
                                    blnCheckedCurrentUser = true;
                                }
                            }
                            else
                            {
                                msgs.InformationalMessage("You can't delete some user detail exists in the system");
                                mpeMessage.Show();
                            }
                        }
                    }
                
                }
                if (IsDelete == false)
                    msgs.InformationalMessage("No Permission to delete");
                else if (intDeletedItemsCount > 0 && blnCheckedCurrentUser)
                    msgs.InformationalMessage("You can't delete current user.Other user details deleted successfully");
                else if (intDeletedItemsCount == 0 && blnCheckedCurrentUser)
                    msgs.InformationalMessage("You can't delete current user");
                else
                    msgs.InformationalMessage("User details deleted successfully");

                mpeMessage.Show();
                BindGrid();
            }
            upGridView.Update();
        }
        catch (Exception)
        {
            msgs.InformationalMessage("User details exists.");
        }

    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        UserSettingsPager.CurrentPage = 0;
        BindGrid();

        upGridView.Update();
    }

    private void RegisterAutoComplete()
    {
        objUserMaster = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.UserSettings,objUserMaster.GetCompanyId());
    }
    protected void gvUserSettings_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
