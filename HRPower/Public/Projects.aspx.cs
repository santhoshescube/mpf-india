﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using HRAutoComplete;

public partial class Public_Projects : System.Web.UI.Page
{
    clsProjectReference objProject;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    protected void  Page_Load(object sender, EventArgs e)
    {
        this.projectPager.Fill += new controls_Pager.FillPager(BindDataList);
       
        this.RegisterAutoComplete();

        objProject = new clsProjectReference();
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        this.Title = GetGlobalResourceObject("SettingsCommon", "Projects").ToString();
        if (!IsPostBack)
        {
            lblNoData.Visible = false;
            upnlDatalist.Update();
            if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsView"))
            {
                BindDataList();
            }
            else if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsCreate"))
            {
                NewProject();
            }
            else
            {
                projectPager.Visible = false;

                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Visible = true;

            }
            SetPermissions();
        }

    }

    private void SetPermissions()
    {
        lnkAdd.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsCreate");
        lnkView.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsView");
        lnkDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsDelete");
        if(!lnkDelete.Enabled )
            lnkDelete.OnClientClick = "return false" ;

        lnkEmail.Enabled = lnkPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsPrintEmail");

        if (lnkEmail.Enabled)
            lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlProject.ClientID + "');";
        else
            lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return false;";
       
    }



    private void BindDataList()
    {

        clsUserMaster objUser = new clsUserMaster();

        objProject.PageSize = projectPager.PageSize;
        objProject.PageIndex = projectPager.CurrentPage + 1;



        objProject.SortExpression = "ProjectID";
        objProject.SortOrder = "DESC";
        objProject.SearchKey = txtSearch.Text;

        fvProjects.ChangeMode(FormViewMode.ReadOnly);
        this.fvProjects.DataSource = null;
        fvProjects.DataBind();



        objProject.UserId = objUser.GetUserId();
        DataSet ds = objProject.GetAllProjects();

        if (ds.Tables[0].Rows.Count > 0)
        {
            projectPager.Total = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            projectPager.Visible = true;

            if( ds.Tables[1].Rows.Count > 0)
            {
                dlProject.DataSource = ds.Tables[1];
                dlProject.DataBind();
                dlProject.Visible = true;
            }
            else
            {
                dlProject.Visible = false ;
                projectPager.Visible = false ;
            }


            lnkDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsDelete");

            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlProject.ClientID + "');";

        }
        else
        {
            projectPager.Visible = false;

            msgs.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "NoRecordFound").ToString());
            mpeMessage.Show();

            dlProject.DataSource = null;
            dlProject.DataBind();
        }

        upnlDatalist.Update();

    }
    protected void dlProject_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            ImageButton imgEdit = (ImageButton)e.Item.FindControl("imgEdit");

            imgEdit.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsUpdate");

            HiddenField hidStatus = (HiddenField) e.Item.FindControl("hidStatus");
            
            if (imgEdit.Enabled)
            {
                imgEdit.Enabled = Convert.ToBoolean(hidStatus.Value);
            }
            if (!Convert.ToBoolean(hidStatus.Value))
            {
                LinkButton lnkProject = (LinkButton)e.Item.FindControl("lnkProject");
                lnkProject.ForeColor = System.Drawing.Color.Red;
                imgEdit.Visible = false;
                
            }
           
        }
    }

    protected void dlProject_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objProject = new clsProjectReference();

        objProject.ProjectID = Convert.ToInt32(fvProjects.CurrentMode == FormViewMode.Insert ? "0" : fvProjects.DataKey["ProjectId"]);

        switch (e.CommandName)
        {

            case "_Edit":
                
                txtSearch.Text = string.Empty;

                fvProjects.ChangeMode(FormViewMode.Edit);

                lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
                lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

                objProject = new clsProjectReference();
                objProject.ProjectID = Convert.ToInt32(e.CommandArgument);
                DataSet ds = objProject.GetProjectDetails();
                ViewState["status"] = ds.Tables[0].Rows[0]["Statusid"].ToString();

                TextBox txtBudget = (TextBox)fvProjects.FindControl("txtBudget");
                if (txtBudget != null)
                    txtBudget.Attributes["onkeypress"] = "return KeyRestrict(event, 'Number', '');";

                fvProjects.DataSource = ds.Tables[0];
                fvProjects.DataBind();



                if (ds.Tables.Count > 0)
                {
                    DataList dlEmployeesList = (DataList)fvProjects.FindControl("dlEmployeesList");
                    TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");
                    TextBox txtEndDate = (TextBox)fvProjects.FindControl("txtToDate");

                    if (dlEmployeesList != null)
                    {
                        objProject.EmpFinishDate = clsCommon.Convert2DateTime(txtEndDate.Text);
                        objProject.EmpWorkStartDate = clsCommon.Convert2DateTime(txtStartDate.Text);
                        dlEmployeesList.DataSource = objProject.GetAllEmployees();
                        dlEmployeesList.DataBind();
                        if (dlEmployeesList.Items.Count > 0)
                        {

                            foreach (DataListItem item in dlEmployeesList.Items)
                            {
                                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                                {
                                    CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
                                    TextBox txtEmpStartDate = (TextBox)item.FindControl("txtStartDate");
                                    TextBox txtEmpEndDate = (TextBox)item.FindControl("txtEndDate");
                                    TextBox txtDuration = (TextBox)item.FindControl("txtDuration");
                                    Label lblInfoEmployeeAdd = (Label)item.FindControl("lblInfoEmployeeAdd");
                                    if (chkSelect != null)
                                    {
                                        for (int i = 0; i < ds.Tables[1].Rows.Count;i++ )
                                        {
                                            chkSelect.Checked = dlEmployeesList.DataKeys[item.ItemIndex].ToInt32() == ds.Tables[1].Rows[i][0].ToInt32();
                                            if (chkSelect.Checked)
                                            {
                                                txtEmpStartDate.Text = Convert.ToDateTime(ds.Tables[1].Rows[i][2].ToString()).ToString("dd/MM/yyyy");
                                                txtEmpEndDate.Text = Convert.ToDateTime(ds.Tables[1].Rows[i][4].ToString()).ToString("dd/MM/yyyy");
                                                txtDuration.Text = ds.Tables[1].Rows[i][3].ToString();

                                                txtEmpStartDate.Attributes["onchange"] = string.Format("javascript:CalculateWorkDuration('{0}', '{1}', '{2}', '{3}');", txtEmpStartDate.ClientID, txtEmpEndDate.ClientID, txtDuration.ClientID, lblInfoEmployeeAdd.ClientID);
                                                txtEmpEndDate.Attributes["onchange"] = string.Format("javascript:CalculateWorkDuration('{0}', '{1}', '{2}', '{3}');", txtEmpStartDate.ClientID, txtEmpEndDate.ClientID, txtDuration.ClientID, lblInfoEmployeeAdd.ClientID);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                      
                    }
                }

                fvProjects.Visible = true;

                this.projectPager.Visible = false;
                dlProject.DataSource = null;
                dlProject.DataBind();


                upnlDatalist.Update();
                break;

            case "_View":

                fvProjects.ChangeMode(FormViewMode.ReadOnly);


                Bindformview(Convert.ToInt32(e.CommandArgument));

                break;
        }
    }

    private void Bindformview(int iProjectID)
    {
        objProject = new clsProjectReference();
        objProject.ProjectID = iProjectID;
        DataSet ds1 = objProject.GetProjectDetails();
        fvProjects.DataSource = ds1.Tables[0];
        fvProjects.DataBind();
             

        if (ds1.Tables.Count == 2 && ds1.Tables[1].Rows.Count > 0)
        {
            DataList dlEmployeesList;
            if (fvProjects.CurrentMode == FormViewMode.ReadOnly)
            {
                dlEmployeesList = (DataList)fvProjects.FindControl("dlEmployeesViewList");
               
                lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return true;";

            }
            else
            {
                dlEmployeesList = (DataList)fvProjects.FindControl("dlEmployeesList");

            }
            if (dlEmployeesList != null)
            {
                dlEmployeesList.DataSource = ds1.Tables[1];
                dlEmployeesList.DataBind();

               

            }
        }

        fvProjects.Visible = true;

        this.projectPager.Visible = false;

        dlProject.DataSource = null;
        dlProject.DataBind();


        upnlDatalist.Update();
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        DataList dlEMployeeList = (DataList)fvProjects.FindControl("dlEmployeesList");

        foreach (DataListItem item in dlEMployeeList.Items)
        {

            object chkSelect = item.FindControl("chkSelect");

            if (chkSelect != null)
            {
                ((CheckBox)chkSelect).Checked = ((System.Web.UI.WebControls.CheckBox)(sender)).Checked;
            }

        }

    }
    protected void BindEmployees(object sender, EventArgs e)
    {
        DataList dlEMployeeList = (DataList)fvProjects.FindControl("dlEmployeesList");
        TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");
        TextBox txtEndDate = (TextBox)fvProjects.FindControl("txtToDate");
        DataTable dt;
        if (dlEMployeeList != null)
        {
            objProject.EmpWorkStartDate = clsCommon.Convert2DateTime(txtStartDate.Text);
            objProject.EmpFinishDate = clsCommon.Convert2DateTime(txtEndDate.Text);
            
            dt = objProject.GetAllEmployees();
           

            if (dt.Rows.Count > 0)
            {
                dlEMployeeList.DataSource = dt;
                dlEMployeeList.DataBind();
                dlEMployeeList.Visible = true;
            }
            else
            {
                dlEMployeeList.Visible = false;
            }
        }


    }

    protected void fvProjects_DataBound(object sender, EventArgs e)
    {

        if (fvProjects.CurrentMode == FormViewMode.Insert || fvProjects.CurrentMode == FormViewMode.Edit)
        {
            DataList dlEMployeeList = (DataList)fvProjects.FindControl("dlEmployeesList");
            TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");
            TextBox txtEndDate = (TextBox)fvProjects.FindControl("txtToDate");
            bool bInsert = false;
            RadioButtonList rdbStatus = (RadioButtonList)fvProjects.FindControl("rdbStatus");
            if (fvProjects.CurrentMode == FormViewMode.Insert)
                rdbStatus.Enabled = false;
            else
            {
                rdbStatus.Enabled = true;

                rdbStatus.SelectedValue = ViewState["status"].ToString();

                txtEndDate.Text = Convert.ToDateTime(txtEndDate.Text).ToString("dd/MM/yyyy");
                txtStartDate.Text = Convert.ToDateTime(txtStartDate.Text).ToString("dd/MM/yyyy");
            }
            if (dlEMployeeList != null)
            {
               
                objProject.EmpWorkStartDate = clsCommon.Convert2DateTime(txtStartDate.Text);
                objProject.EmpFinishDate = clsCommon.Convert2DateTime(txtEndDate.Text);
               
                DataTable dt = objProject.GetAllEmployees();
                dlEMployeeList.DataSource = dt;
                dlEMployeeList.DataBind();
            }
            DisableLinks();
            
            
        }
        else if (fvProjects.CurrentMode == FormViewMode.ReadOnly)
        {
            lnkDelete.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsDelete");

            lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');";
        }

    }

    private void InsertUpdateProjectTeamMembers(DataList dlEmployees, int ProjectId)
    {
       
        if (dlEmployees.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployees.Items)
            {
                CheckBox chkEmployee = (CheckBox)item.FindControl("chkSelect");
                TextBox txtEmpStartDate = (TextBox)item.FindControl("txtStartDate");
                TextBox txtEmpEndDate = (TextBox)item.FindControl("txtEndDate");
                TimeSpan span = clsCommon.Convert2DateTime(txtEmpEndDate.Text).Subtract(clsCommon.Convert2DateTime(txtEmpStartDate.Text));
                int Duration = Convert.ToInt32(Math.Abs((span.Days >= 0 ? span.Days + 1 : span.Days)).ToString());
                Label lblInfoEmployeeAdd = (Label)item.FindControl("lblInfoEmployeeAdd");
                if (chkEmployee.Checked)
                {
                   
                        objProject.ProjectID = ProjectId;
                        objProject.EmpWorkStartDate = clsCommon.Convert2DateTime(txtEmpStartDate.Text);
                        objProject.EmpFinishDate = clsCommon.Convert2DateTime(txtEmpEndDate.Text);
                        objProject.Duration = Duration;
                        objProject.EmployeeID = dlEmployees.DataKeys[item.ItemIndex].ToInt32();

                        objProject.InsertTeamMember();
                 

                }
            }
        }
      
    }

    private bool ValidateForm(DataList dlEmployees)
    {
        bool bStatus = true;

        if (objProject.IsProjectExist())
        {
            msgs.WarningMessage(GetLocalResourceObject("Exists.Text").ToString());
            mpeMessage.Show();
            bStatus = false;

        }

        if (dlEmployees.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployees.Items)
            {
                CheckBox chkEmployee = (CheckBox)item.FindControl("chkSelect");
                TextBox txtEmpStartDate = (TextBox)item.FindControl("txtStartDate");
                TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");

                if (chkEmployee.Checked)
                {
                    if (clsCommon.Convert2DateTime(txtEmpStartDate.Text) < clsCommon.Convert2DateTime(txtStartDate.Text))
                    {
                        msgs.InformationalMessage(GetLocalResourceObject("WorkStartDate.Text").ToString());
                        mpeMessage.Show();
                        bStatus = false;
                        break;

                    }
                }
            }
        }
        return bStatus;
    }


    protected void  fvProjects_ItemCommand(object sender, FormViewCommandEventArgs e)
    {

        TextBox txtProjectCode = (TextBox)fvProjects.FindControl("txtProjectCode");
        TextBox txtProjectName = (TextBox)fvProjects.FindControl("txtProjectName");
        TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");
        TextBox txtEndDate = (TextBox)fvProjects.FindControl("txtToDate");
        TextBox txtDescription = (TextBox)fvProjects.FindControl("txtDescription");
        TextBox txtBudget = (TextBox)fvProjects.FindControl("txtBudget");
        DataList dlEmployeeList = (DataList)fvProjects.FindControl("dlEmployeesList");
         RadioButtonList rdbStatus = (RadioButtonList)fvProjects.FindControl("rdbStatus");
       
        string message = "";
        int iProjectId = 0;
        try
        {
            switch (e.CommandName)
            {
                case "Add":

                    objProject.ProjectID = this.fvProjects.CurrentMode == FormViewMode.Insert ? 0 : Convert.ToInt32(this.fvProjects.DataKey["ProjectId"]);
                    objProject.ProjectCode = txtProjectCode.Text;
                    objProject.Description = txtProjectName.Text;
                    objProject.StartDate = clsCommon.Convert2DateTime(txtStartDate.Text);
                    objProject.EstimatedFinishDate = clsCommon.Convert2DateTime(txtEndDate.Text);
                    objProject.Remarks = txtDescription.Text;
                    objProject.Budget = Convert.ToDecimal(txtBudget.Text);
                   
                    if (ValidateForm(dlEmployeeList))
                    { 
                        
                        if (fvProjects.CurrentMode == FormViewMode.Insert)
                        {
                          
                            iProjectId = objProject.Insert();
                            if (iProjectId > 0)
                            {
                                InsertUpdateProjectTeamMembers(dlEmployeeList, iProjectId);
                                 
                                message = GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString();
                                
                            }
                           

                        }
                        else
                        {
                            objProject.Status = Convert.ToInt32(rdbStatus.SelectedValue);
                            iProjectId = objProject.Update();
                            InsertUpdateProjectTeamMembers(dlEmployeeList, iProjectId);
                            message = GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString();

                        }
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();


                        BindDataList();
                        upnlDatalist.Update();
                    }



                    break;
                case "CancelProject":
                    BindDataList();
                    upnlDatalist.Update();
                    fvProjects.ChangeMode(FormViewMode.ReadOnly);
                    break;
            }
        }
        catch (Exception)
        {

        }
    }
  
    protected void EnableDates(object sender, EventArgs e)
    {
        DataList dlEMployeeList = (DataList)fvProjects.FindControl("dlEmployeesList");
        CheckBox chkSelected = (CheckBox)sender;
        foreach (DataListItem item in dlEMployeeList.Items)
        {

            CheckBox chkSelect = (CheckBox)item.FindControl("chkSelect");
            HtmlTableCell tdSDate = (HtmlTableCell)item.FindControl("tdSDate");
            HtmlTableCell tdEDate = (HtmlTableCell)item.FindControl("tdEDate");
            CustomValidator cvStartDate = (CustomValidator)item.FindControl("cvStartDate");
            CustomValidator cvEndDate = (CustomValidator)item.FindControl("cvEndDate");

            TextBox txtEmpStartDate = (TextBox)item.FindControl("txtStartDate");
            TextBox txtEmpEndDate = (TextBox)item.FindControl("txtEndDate");
            TextBox txtDuration = (TextBox)item.FindControl("txtDuration");
            Label lblInfoEmployeeAdd = (Label)item.FindControl("lblInfoEmployeeAdd");

            txtEmpStartDate.Attributes["onchange"] = string.Format("javascript:CalculateWorkDuration('{0}', '{1}', '{2}', '{3}');", txtEmpStartDate.ClientID, txtEmpEndDate.ClientID, txtDuration.ClientID, lblInfoEmployeeAdd.ClientID);
            txtEmpEndDate.Attributes["onchange"] = string.Format("javascript:CalculateWorkDuration('{0}', '{1}', '{2}', '{3}');", txtEmpStartDate.ClientID, txtEmpEndDate.ClientID, txtDuration.ClientID, lblInfoEmployeeAdd.ClientID);

            if (chkSelect != null)
            {
             
                if (chkSelect.Checked)
                {

                    cvEndDate.ValidationGroup = "submit";
                    cvStartDate.ValidationGroup = "submit";
                    chkSelect.Focus();
                  
                }
                else
                {

                    cvEndDate.ValidationGroup = "";
                    cvStartDate.ValidationGroup = "";
                }
             
            }
           
        }
        chkSelected.Focus();
    }

    private void NewProject()
    {
        // clear value in the ViewState before adding a new project
    


        fvProjects.ChangeMode(FormViewMode.Insert);


        fvProjects.DataSource = null;
        fvProjects.DataBind();

        fvProjects.Visible = true;

        dlProject.DataSource = null;
        dlProject.DataBind();

        this.projectPager.Visible = false;

        TextBox txtStartDate = (TextBox)fvProjects.FindControl("txtFromDate");
        TextBox txtFinishDate = (TextBox)fvProjects.FindControl("txtToDate");

        txtStartDate.Text = txtFinishDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtSearch.Text = string.Empty;

        TextBox txtProjectName = (TextBox)fvProjects.FindControl("txtProjectName");
        TextBox txtProjectCode = (TextBox)fvProjects.FindControl("txtProjectCode");
        //lblProjectCode.Text = txtProjectCode.Text;


        objProject = new clsProjectReference();


        this.upnlDatalist.Update();

    }
    private void DisableLinks()
    {
        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        DisableLinks();
        NewProject();
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        projectPager.CurrentPage = 0;
        BindDataList();
        lnkEmail.Enabled = lnkPrint.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), (int)eMenuID.Projects, "IsPrintEmail");
        if (lnkEmail.Enabled)
           lnkPrint.OnClientClick = lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlProject.ClientID + "');";
        else
            lnkPrint.OnClientClick =  lnkEmail.OnClientClick = "return false;";
       
        upnlDatalist.Update();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objProject = new clsProjectReference();

        bool deleted;
      
        if (dlProject.Items.Count > 0)
        {
            CheckBox chkSelect;
            int iDeletedCount = 0;

            string sStartTag = "<ul style='padding:0; margin-left:25px;margin-top:5px;'>";
            string sEndTag = "</ul>";
            string sErrorHeading = "<span>" + GetLocalResourceObject("DeletionFailed.Text").ToString() + "</span> </ br>";
            string sMessage = string.Empty;
            string sProject = string.Empty;

            HtmlGenericControl span = new HtmlGenericControl();
            objProject.Begin();
            foreach (DataListItem item in dlProject.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    chkSelect = (CheckBox)item.FindControl("chkSelect");
                    if (chkSelect != null && chkSelect.Checked)
                    {
                        span = (HtmlGenericControl)item.FindControl("spanProject");

                        sProject = span != null ? span.InnerHtml.Trim() : string.Empty;

                        objProject.ProjectID = Convert.ToInt32(dlProject.DataKeys[item.ItemIndex]);
                        deleted = false;

                        deleted = objProject.Delete();

                        if (!deleted)
                        {
                            objProject.RollBack();
                            sMessage =  GetLocalResourceObject("DeletionFailed.Text").ToString();
                            mpeMessage.Show();

                        }
                        else
                        {
                            sMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                        }

                        iDeletedCount = (deleted) ? iDeletedCount + 1 : iDeletedCount;
                    }
                }
            }

            if (iDeletedCount > 0)
            {
                objProject.Commit();             
            }

            if (sMessage.Length > 0)
            {
                msgs.WarningMessage(sMessage);
                mpeMessage.Show();
            }

            BindDataList();
            upnlDatalist.Update();

        }
        else
        {
            if (this.fvProjects.CurrentMode == FormViewMode.ReadOnly && Convert.ToInt32(this.fvProjects.DataKey["ProjectId"]) > 0)
            {
                int ProjectID = this.fvProjects.DataKey["ProjectId"].ToInt32();
                objProject.Begin();
                if (ProjectID > 0)
                {
                    objProject.ProjectID = ProjectID;
                    deleted = false;

                    deleted = objProject.Delete();

                    if (deleted)
                    {                       
                        objProject.Commit(); 
                        BindDataList();
                        upnlDatalist.Update();
                       
                        msgs.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString());
                        mpeMessage.Show();
                    }
                    else
                    {
                        objProject.RollBack();
                        msgs.InformationalMessage(GetLocalResourceObject("DeletionFailed.Text").ToString());
                        mpeMessage.Show();
                    }
                    return;
                }
            }

        }
    }
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        
      
            CheckBox chkItem;

            if (dlProject.Items.Count > 0)
            {
                string sProjectIds = string.Empty;

                foreach (DataListItem item in dlProject.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkSelect");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    sProjectIds += "," + dlProject.DataKeys[item.ItemIndex];
                }

                if (sProjectIds != string.Empty)
                    sProjectIds = sProjectIds.Remove(0, 1);

                if (sProjectIds.Contains(","))
                    divPrint.InnerHtml = Printdetails(sProjectIds, false);
                else
                {
                    objProject.ProjectID = Convert.ToInt32(sProjectIds);
                    divPrint.InnerHtml = Printdetails(sProjectIds, true);
                }
            }       
            else if (this.fvProjects.CurrentMode == FormViewMode.ReadOnly && Convert.ToInt32(this.fvProjects.DataKey["ProjectId"]) > 0)
            {
                objProject.ProjectID = fvProjects.DataKey["ProjectId"].ToInt32();
                divPrint.InnerHtml = Printdetails(this.fvProjects.DataKey["ProjectId"].ToString(), true);
            }

           ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {

        
        CheckBox chkItem;
        string sProjectIds = string.Empty;

        if (dlProject.Items.Count > 0)
        {
           
            foreach (DataListItem item in dlProject.Items)
            {
                chkItem = (CheckBox)item.FindControl("chkSelect");

                if (chkItem == null)
                    continue;

                if (chkItem.Checked == false)
                    continue;

                sProjectIds += "," + dlProject.DataKeys[item.ItemIndex];
            }

            if (sProjectIds != string.Empty)
                sProjectIds = sProjectIds.Remove(0, 1);

          
        }
        else if (this.fvProjects.CurrentMode == FormViewMode.ReadOnly && Convert.ToInt32(this.fvProjects.DataKey["ProjectId"]) > 0)
        {
            sProjectIds = fvProjects.DataKey["ProjectId"].ToString();
            
        }

        if (sProjectIds.Length > 0)
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Project&ProjectId=" + sProjectIds + "', 835, 585);", true);
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        projectPager.CurrentPage = 0;
        BindDataList();
        upnlDatalist.Update();
        this.RegisterAutoComplete();
    }
    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Project,objUser.GetCompanyId());
    }
    private string Printdetails(string sProjectIds, bool isSingle)
    {
        StringBuilder sb = new StringBuilder();
        objProject.ProjectIDs = sProjectIds;

        DataSet ds = objProject.Print();
       

        if (ds != null)
        {
            DataTable dt = ds.Tables[0];
            if (isSingle)
            {
               
                string sFontWeight = "font-weight:normal";
                sb.Append("<div style='padding:10px; margin:5px;'>");
                sb.Append("<center> <h5 style='margin:0; padding:0; text-transform:uppercase;'>");
                sb.Append( GetLocalResourceObject("ProjectDetails.Text").ToString() + "</h5> </center>");
                sb.Append("<hr style='border:solic 1px #CCCCCC;'/>");
                sb.Append("<div style='margin-left:10px; padding:5px 0;'>"); // apply 10px margin

                sb.Append("<span style='font-size:12px; font-weight:bold' > " + GetLocalResourceObject("General.Text").ToString() + " </span>");
                // General Info
                sb.Append("<table style='font-size: 11px; line-height: 20px; margin-top:5px;'>");

                sb.Append("<tr> <td style=''" + sFontWeight + "'width='200'> "+GetLocalResourceObject("ProjectCode.Text").ToString() +" </td> ");
                sb.Append("<td> : " + dt.Rows[0]["ProjectCode"] + "<td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Description.Text").ToString() + " </td>");
                sb.Append("<td> : " + dt.Rows[0]["Description"] + "</td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("StartDate.Text").ToString() + "</td> ");
                sb.Append("<td> : " + dt.Rows[0]["StartDate"] + "</td> </tr>");

                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("EstimatedDate.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["EstimatedFinishDate"] + "</td> </tr>");




                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("TeamMembers.Text").ToString() + " </td>");
                sb.Append("<td> : " + dt.Rows[0]["NoOfStaff"] + "</td> </tr>");


                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Budget.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["Budget"] + "</td> </tr>");


                sb.Append("<tr> <td style='" + sFontWeight + "'> " + GetLocalResourceObject("Remarks.Text").ToString() + "</td>");
                sb.Append("<td> : " + dt.Rows[0]["Remarks"] + "</td> </tr>");

                sb.Append("</table>");

                // Project Team Details
                if ( ds.Tables[1].Rows.Count > 0)
                {
                    sb.Append("</br>");
                    sb.Append("<span style='font-size:12px; font-weight:bold'> " + GetLocalResourceObject("TeamInfo.Text").ToString() + " </span>");
                    sb.Append("</br> </br>");
                    sb.Append("<table style='border:solid 1px #F0F0F0; border-collapse:collapse; width:90%; font-size:11px;' cellpadding='4'>");
                    sb.Append("<tr style='background-color:#F0F0F0; font-size:11px'> <td style='font-weight:bold'>  " + GetGlobalResourceObject("DocumentsCommon","Employee").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> " + GetLocalResourceObject("StartDate.Text").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'> " + GetLocalResourceObject("EndDate.Text").ToString() + " </td>");
                    sb.Append("<td style='font-weight:bold; border:solid 1px #F0F0F0;'>  " + GetLocalResourceObject("Duration.Text").ToString() + "</td> </tr>");

                    foreach (DataRow Row in ds.Tables[1].Rows)
                    {
                        sb.Append("<tr> <td style='border:solid 1px #F0F0F0;'> " + Row["Employee"] + "</td>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["WorkStartDate"] + "</td>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["FinishDate"] + "</d>");
                        sb.Append("<td style='border:solid 1px #F0F0F0;'> " + Row["Duration"] + " </td> </tr>");
                    }

                    sb.Append("</table>");
                }

                sb.Append("</div>");
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<table width='100%' style='font-family:Tahoma;font-size:11px;' border='0'>");
                sb.Append("<tr><td style='font-weight:bold;font-size:13px; text-transform:uppercase;' align='center'>" + GetLocalResourceObject("ProjectDetails.Text").ToString() +"</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table style='font-size:11px;font-family:Tahoma; width:100%;' border='0px'>");
                sb.Append("<tr><td colspan='6'><hr></td></tr>");
                sb.Append("<tr style='font-weight:bold;'>");
                // table header
                sb.Append("<td>" + GetLocalResourceObject("ProjectCode.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("Description.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("StartDate.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("EstimatedDate.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("TeamMembers.Text").ToString() + "</td>");
                sb.Append("<td>" + GetLocalResourceObject("Budget.Text").ToString() + "</td>");

                sb.Append("<tr><td colspan='6'><hr></td></tr>");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("<tr><td>" + Convert.ToString(dt.Rows[i]["ProjectCode"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Description"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["StartDate"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["EstimatedFinishDate"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["NoOfStaff"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Budget"]) + "</td>");
                }
                sb.Append("</td></tr> </table> </td> </tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }

}
