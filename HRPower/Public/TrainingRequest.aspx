﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="TrainingRequest.aspx.cs" Inherits="Public_TrainingRequest" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">





</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
  <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
                    <div style="width: 100%">
                        <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="TrainingCourse"></asp:Literal>
                              <font color="Red">*</font>
                        </div>
                         <div class="trRight" style="float: left; width: 70%; height: 29px">
                            
                                <asp:DropDownList ID="ddltrainingcource" runat="server">
                                </asp:DropDownList>
                           
                           <%--<div style="float: right; width: 72%; height: 29px; padding-left: 3px; line-height: normal">
                              
                                    
                                    
                                            <asp:RequiredFieldValidator ID="rfvtrainingcourcTab1" runat="server" ControlToValidate="ddltrainingcource"
                                                CssClass="error" Display="Dynamic" meta:resourcekey="PleaseSelectCource"
                                                InitialValue="-1"></asp:RequiredFieldValidator>
                                          
                                        </div>--%>
                                    
                           
                        </div>
                        <div style="height: auto">
                            
                                 <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                 </div>
                               
                                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                           
                       
                                        
                                        
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" meta:resourcekey="Self" Value="0">
                          
                                            </asp:ListItem>
                                            <asp:ListItem  meta:resourcekey="Department" Value="1">
                               
                                            </asp:ListItem>
                                            </asp:RadioButtonList>
                                           
                                        
                                   
                            </div>
                         <div style="height: auto">
                          <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                        </div>
                         <div class="trRight" style="float: left; width: 70%; height: 29px">
                             <asp:TextBox ID="txtcurrentdate" runat="server"></asp:TextBox>
                         </div>
                         </div>
                         
                         
                         <div style="height: auto">
                          <div class="trLeft" style="float: left; width: 25%; height: 29px">
                            <%--   Employee Number--%>
                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Remark"></asp:Literal>
                        </div>
                         <div class="trRight" style="float: left; width: 70%; height: 70px">
                             <asp:TextBox ID="txtremark" runat="server" Width="185px" Height="69px" 
                                 TextMode="MultiLine"></asp:TextBox>
                                
                         </div>
                         </div>
                          
                          <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    </div>
                     <div class="trRight" style="float: left; width: 70%; height: 29px">
                  </div>
                  <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,SUbmit %>' 
                        CommandName="SUBMIT" 
                        CausesValidation="true"
                            Text='<%$Resources:ControlsCommon,SUbmit %>'  Width="75px" 
                            onclick="btnSubmit_Click" />
                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel %>' CausesValidation="false"
                            Text='<%$Resources:ControlsCommon,Cancel %>' CommandName="_Cancel"  Width="75px" />
                    </div>
                           
                   
                        </div>
                   
                </div>
                </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

