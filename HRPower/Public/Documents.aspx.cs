﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;
using System.IO;
using System.Reflection;
using HRAutoComplete;
using System.Text;

public partial class Public_Documents : System.Web.UI.Page
{
    private bool MblnUpdatePermission = true;  // Edit Button Permission In Datalist

    private string CurrentSelectedValue { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        RegisterAutoComplete();

        if (!IsPostBack)
        {
            tdReferenceName.InnerText = clsGlobalization.IsArabicCulture() ? "عامل" : "Employee";
            FillCombos();
            SetPermission();
            ClearControls();
            lnkListDoc_Click(sender, e);
        }
        ucDocIssueReceipt.OnSave -= new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);
        ucDocIssueReceipt.OnSave += new Controls_DocumentReceiptIssue.Save(ucDocIssueReceipt_OnSave);

        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);

    }

    private void RegisterAutoComplete()
    {
        clsUserMaster objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.OtherDocument, objUser.GetCompanyId());
    }

    void ucDocIssueReceipt_OnSave(string Result)
    {
        //if (Result != null)
        //{
        //    lnkDocumentIR.Text = "<h5>"+Result+"</h5>";
        //    if (Result == "Issue")
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_issue.png";
        //        lblCurrentStatus.Text = "Receipted";
        //    }
        //    else
        //    {
        //        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //        lblCurrentStatus.Text = "Issued";
        //    }

        //    upEmployeeOtherDoc.Update();
        //    upMenu.Update();
        //}
    }

    public void BindDataList()
    {
        upMenu.Update();
        clsUserMaster ojUser = new clsUserMaster();
        clsDocumentMaster objclsDocumentMaster = new clsDocumentMaster();
        objclsDocumentMaster.PageIndex = pgrDocument.CurrentPage + 1;
        objclsDocumentMaster.PageSize = pgrDocument.PageSize;
        objclsDocumentMaster.SearchKey = txtSearch.Text;
        objclsDocumentMaster.UserID = new clsUserMaster().GetUserId();
        objclsDocumentMaster.CompanyID = ojUser.GetCompanyId();
        pgrDocument.Total = objclsDocumentMaster.GetCount();

        DataTable datDocument = objclsDocumentMaster.GetAllDocuments();


        if (datDocument != null && datDocument.Rows.Count > 0)
        {
            dlEmployeeOtherDoc.DataSource = datDocument;
            dlEmployeeOtherDoc.DataBind();
            lblNoData.Visible = false;
            pgrDocument.Visible = true;
            EnableMenus();
        }
        else
        {
            dlEmployeeOtherDoc.DataSource = null;
            dlEmployeeOtherDoc.DataBind();
            pgrDocument.Visible = false;
            lblNoData.Visible = true;
            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetLocalResourceObject("NoDocuments.Text").ToString();
            else
                lblNoData.Text = GetGlobalResourceObject("DocumentsCommon", "NoSearchresultsfound").ToString();

            lnkPrint.Enabled = lnkEmail.Enabled = lnkDelete.Enabled = false;
            lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = "return false;";
        }

        upnlNoData.Update();
    }

    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(RoleId, (int)eMenuID.OtherDocuments);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;

        if (dt.Rows.Count > 0)
        {
            lnkAddDoc.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListDoc.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (RoleId > 3)
        {
            if (lnkListDoc.Enabled)
            {
                ViewDocuments();
                btnSearch.Enabled = true;
            }
            else if (lnkAddDoc.Enabled)
                AddNewDocument();
            else
            {
                pgrDocument.Visible = false;
                dlEmployeeOtherDoc.DataSource = null;
                dlEmployeeOtherDoc.DataBind();
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Visible = true;
                btnSearch.Enabled = false;
            }
        }
        else
            ViewDocuments();
    }

    public void EnableMenus()
    {
        DataTable dt = (DataTable)ViewState["Permission"];

        if (dt.Rows.Count > 0)
        {
            lnkAddDoc.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListDoc.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            MblnUpdatePermission = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlEmployeeOtherDoc.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeeOtherDoc.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlEmployeeOtherDoc.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";

        if (lnkRenew.Enabled)
            lnkRenew.OnClientClick = "return valRenewDatalist('" + dlEmployeeOtherDoc.ClientID + "');";
        else
            lnkRenew.OnClientClick = "return false;";
    }

    protected string GetDate(object date)
    {
        if (date != DBNull.Value)
            return Convert.ToDateTime(date).ToString("dd/MM/yyyy");
        else
            return string.Empty;

    }

    protected void rbEmployee_CheckedChanged(object sender, EventArgs e)
    {
        if (rbEmployee.Checked)
        {
            tdReferenceName.InnerText = GetGlobalResourceObject("DocumentsCommon", "Employee").ToString();
            GetAllReferenceNumberByOperationTypeID(true);
            ddlReferenceName.Enabled = true;
        }
    }



    public void GetAllDocumentTypes()
    {

        DataTable dt = clsDocumentMaster.GetAllOtherDocumentTypes();
        DataRow dr = dt.NewRow();

        dr["DocumentType"] = GetGlobalResourceObject("DocumentsCommon", "Select").ToString();
        dr["DocumentTypeID"] = "-1";

        dt.Rows.InsertAt(dr, 0);
        rcDocType.DataSource = dt;
        rcDocType.DataTextField = "DocumentType";
        rcDocType.DataValueField = "DocumentTypeID";
        rcDocType.DataBind();
        rcDocType.SelectedIndex = 0;

        if (CurrentSelectedValue != null && CurrentSelectedValue != string.Empty)
        {
            if (rcDocType.Items.FindByValue(CurrentSelectedValue) != null)
                rcDocType.SelectedValue = CurrentSelectedValue;
        }

        updDocumentType.Update();
    }


    private void GetAllBin()
    {

        DataTable dt = clsDocumentMaster.GetAllBin();
        DataRow dr = dt.NewRow();
        dr["BinName"] = GetGlobalResourceObject("DocumentsCommon", "Select").ToString();
        dr["BinID"] = "-1";
        dt.Rows.InsertAt(dr, 0);

        ddlBinNumber.DataSource = dt;
        ddlBinNumber.DataTextField = "BinName";
        ddlBinNumber.DataValueField = "BinID";
        ddlBinNumber.DataBind();

        ddlBinNumber.SelectedIndex = 0;
    }


    private void GetAllReferenceNumberByOperationTypeID(bool InserviceOnly)
    {
        clsDocumentMaster objclsDocumentMaster = new clsDocumentMaster();
        clsUserMaster objUser = new clsUserMaster();
        int UserID = objUser.GetUserId();
        int CompanyId = objUser.GetCompanyId();//clsCommon.CompanyID;
        //DataTable dt = clsDocumentMaster.GetAllReferenceNumberByOperationTypeID(rbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee, InserviceOnly);
        DataTable dt = clsDocumentMaster.GetAllReferenceNumberByOperationTypeIDByCompany(rbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee, InserviceOnly, UserID, CompanyId);

        DataRow dr = dt.NewRow();
        dr["ReferenceNumber"] = GetGlobalResourceObject("DocumentsCommon", "Select");
        dr["ReferenceID"] = "-1";
        dt.Rows.InsertAt(dr, 0);

        ddlReferenceName.SelectedValue = "-1";
        ddlReferenceName.DataSource = dt;
        ddlReferenceName.DataTextField = "ReferenceNumber";
        ddlReferenceName.DataValueField = "ReferenceID";
        ddlReferenceName.DataBind();

        if (rbCompany.Checked)
            ddlReferenceName.SelectedValue = new clsUserMaster().GetCompanyId().ToString();

        upnlReferenceName.Update();

        ddlReferenceName.Enabled = rbEmployee.Checked;
    }

    private void FillCombos()
    {
        GetAllBin();
        GetAllDocumentTypes();
        GetAllReferenceNumberByOperationTypeID(true);
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);

    }

    private void BindCombo(DataTable datCombo, DropDownList ddlDropDown, string strTextField, string strValueField, bool InsertNewRow)
    {

        ddlDropDown.DataTextField = strTextField;
        ddlDropDown.DataValueField = strValueField;

        if (InsertNewRow)
        {
            DataRow drRow = datCombo.NewRow();
            drRow[strTextField] = GetGlobalResourceObject("DocumentsCommon", "Select");
            drRow[strValueField] = "-1";
            datCombo.Rows.InsertAt(drRow, 0);
        }
        ddlDropDown.DataSource = datCombo;
        ddlDropDown.DataBind();
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        ViewDocuments();
    }

    protected void lnkAddDoc_Click(object sender, EventArgs e)
    {
        AddNewDocument();
    }

    private void AddNewDocument()
    {
        ControlEnableDisble(true);
        ClearControls();
        divAddDocument.Style["display"] = "block";
        divDataList.Style["display"] = "none";
        divViewDocuments.Style["display"] = "none";
        ddlReferenceName.Enabled = rbEmployee.Checked;
        upEmployeeOtherDoc.Update();
        upMenu.Update();
    }

    private void ClearControls()
    {
        EnableMenus();
        hdnDocumentID.Value = "0";
        hdnMode.Value = "0";
        //rbEmployee.Checked = true;       
        if(rbEmployee.Checked)
            ddlReferenceName.SelectedIndex = -1;
        rcDocType.SelectedIndex = 0;
        txtDocumentNumber.Text = string.Empty;
        txtIssuedate.Text = new clsCommon().GetSysDate().ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtExpiryDate.Text = new clsCommon().GetSysDate().ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtOtherInfo.Text = string.Empty;
        lblCurrentStatus.Text = GetGlobalResourceObject("DocumentsCommon", "New").ToString();
        ddlBinNumber.SelectedIndex = 0;
        txtDocname.Text = string.Empty;
        if (fuDocument.HasFile)
            fuDocument.ClearAllFilesFromPersistedStore();
        dlEmployeeOtherDoc.DataSource = null;
        dlEmployeeOtherDoc.DataBind();
        dlOtherDoc.DataSource = null;
        dlOtherDoc.DataBind();
        updOtherDoc.Update();
        pgrDocument.CurrentPage = 0;
        pgrDocument.Visible = false;
        txtSearch.Text = string.Empty;
        lblNoData.Visible = false;
        trBinNumber.Style["display"] = "none";
        imgDocIR.ImageUrl = "~/images/document_reciept.png";
        //lnkDocumentIR.Text = "<h5>Receipt</h5>";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        upMenu.Update();
        upnlNoData.Update();
        lnkPrint.Enabled = lnkEmail.Enabled = lnkDelete.Enabled = false;
        lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = "return false;";
        ViewState["OtherDoc"] = null;
        rbEmployee.Enabled = rbCompany.Enabled = true;
    }
    protected void lnkListDoc_Click(object sender, EventArgs e)
    {
        ClearControls();
        ViewDocuments();
    }

    private void ViewDocuments()
    {
        divAddDocument.Style["display"] = "none";
        divDataList.Style["display"] = "block";
        divViewDocuments.Style["display"] = "none";
        divIssueReceipt.Style["display"] = "none";
        divRenew.Style["display"] = "none";
        BindDataList();
        RegisterAutoComplete();
        upEmployeeOtherDoc.Update();
        upMenu.Update();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        CheckBox chkItem;
        LinkButton lnkDocument;
        Label lblIsDelete;
        string strMessage = string.Empty;

        if (dlEmployeeOtherDoc.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployeeOtherDoc.Items)
            {
                chkItem = (CheckBox)item.FindControl("chkItem");
                lnkDocument = (LinkButton)item.FindControl("lnkDocument");
                lblIsDelete = (Label)item.FindControl("lblIsDelete");

                if (chkItem == null)
                    continue;

                if (chkItem.Checked == false)
                    continue;

                if (Convert.ToBoolean(lblIsDelete.Text.ToInt32()))
                {
                    clsDocumentMaster.DeleteDocument(dlEmployeeOtherDoc.DataKeys[item.ItemIndex].ToInt32());
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
                }
                else
                    strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
            }
        }
        else
        {
            if (Convert.ToBoolean(divIsDelete.InnerText.ToInt32()))
            {
                clsDocumentMaster.DeleteDocument(ViewState["CardID"].ToInt32());
                strMessage = GetGlobalResourceObject("DocumentsCommon", "DeletedSuccessfully").ToString();
            }
            else
                strMessage = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();
        }

        if (strMessage == "")
            strMessage = GetGlobalResourceObject("DocumentsCommon", "Pleaseselectanitemfordelete").ToString();


        mcMessage.InformationalMessage(strMessage);
        mpeMessage.Show();

        ViewDocuments();
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        upMenu.Update();
        if (hdnDocumentID.Value.ToInt32() == 0)
        {
            CheckBox chkItem;

            if (dlEmployeeOtherDoc.Items.Count > 0)
            {
                string sDocumentIDs = string.Empty;

                foreach (DataListItem item in dlEmployeeOtherDoc.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    sDocumentIDs += "," + dlEmployeeOtherDoc.DataKeys[item.ItemIndex];
                }

                if (sDocumentIDs != string.Empty)
                    sDocumentIDs = sDocumentIDs.Remove(0, 1);

                if (sDocumentIDs.Contains(","))
                    divPrint.InnerHtml = Printdetails(sDocumentIDs, false);
                else
                {
                    divPrint.InnerHtml = Printdetails(sDocumentIDs, true);
                }
            }

        }
        else
        {

            divPrint.InnerHtml = Printdetails(hdnDocumentID.Value.ToString(), true);
        }

        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

    }
    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        upMenu.Update();
        string sDocumentIDs = string.Empty;

        if (hdnDocumentID.Value.ToInt32() == 0)
        {
            if (dlEmployeeOtherDoc.Items.Count > 0)
            {
                CheckBox chkItem;

                foreach (DataListItem item in dlEmployeeOtherDoc.Items)
                {
                    chkItem = (CheckBox)item.FindControl("chkItem");

                    if (chkItem == null)
                        continue;

                    if (chkItem.Checked == false)
                        continue;

                    sDocumentIDs += "," + dlEmployeeOtherDoc.DataKeys[item.ItemIndex];
                }
                if (sDocumentIDs != string.Empty)
                    sDocumentIDs = sDocumentIDs.Remove(0, 1);
            }
        }
        else
            sDocumentIDs = hdnDocumentID.Value.ToString();

        if (sDocumentIDs.Length > 0)
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=OtherDocuments&DocumentID=" + sDocumentIDs + "', 835, 585);", true);
    }
    protected void rbCompany_CheckedChanged(object sender, EventArgs e)
    {
        if (rbCompany.Checked)
        {
            tdReferenceName.InnerText = clsGlobalization.IsArabicCulture() ? "شركة" : "Company";
            GetAllReferenceNumberByOperationTypeID(true);
            ddlReferenceName.Enabled = false;

        }

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SaveDocument();
    }

    protected void btnDocumentType_Click(object sender, EventArgs e)
    {

        if (updDocumentType != null && rcDocType != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "DocumentTypeReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
            ReferenceControlNew1.DataValueField = "DocumentTypeID";
            ReferenceControlNew1.FunctionName = "GetAllDocumentTypes";
            ReferenceControlNew1.SelectedValue = rcDocType.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("DocumentType.Text").ToString();
            ReferenceControlNew1.PredefinedField = "Predefined";
            ReferenceControlNew1.ExcludeFiled = "DocumentTypeID";
            ReferenceControlNew1.ExcludeValues = "17,18,19,20";
            ReferenceControlNew1.PopulateData();
            mdlPopUpReference.Show();
            updModalPopUp.Update();

        }
    }

    private void SaveDocument()
    {
        string strDoctypeName = string.Empty;

        DateTime IssuedDate, ExpiryDate;
        bool isUpdate = false;

        if (hdnDocumentID.Value.ToInt32() > 0)
            isUpdate = true;

        DateTime.TryParseExact(txtIssuedate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out IssuedDate);
        DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);


        if (clsDocumentMaster.IsExistsDocumentNumber(hdnDocumentID.Value.ToInt32(), txtDocumentNumber.Text.Trim()))
        {
            string smessage = GetLocalResourceObject("DocumentNumberexists.Text").ToString();
            mcMessage.InformationalMessage(smessage);
            mpeMessage.Show();
        }
        else
        {

            int DocumentID = new clsDocumentMaster()
            {
                DocumentID = hdnDocumentID.Value.ToInt32(),
                OperationTypeID = rbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee,
                DocumentTypeID = rcDocType.SelectedValue.ToInt32(),
                ReferenceNumber = ddlReferenceName.SelectedItem.Text.Trim(),
                ReferenceID = ddlReferenceName.SelectedValue.ToInt32(),
                DocumentNumber = txtDocumentNumber.Text.Trim(),
                IssuedDate = IssuedDate,
                ExpiryDate = ExpiryDate,
                Remarks = txtOtherInfo.Text.Trim(),
                CompanyID = new clsUserMaster().GetCompanyId()
            }.SaveDocument();

            string strMode = "Emp";
            if (rbCompany.Checked)
            {
                strMode = "Comp";
            }
            else if (rbEmployee.Checked)
            {
                strMode = "Emp";
            }
            clsDocumentAlert objAlert = new clsDocumentAlert();
            if (!isUpdate)
            {
                objAlert.AlertMessage(rcDocType.SelectedValue.ToInt32(), rcDocType.SelectedItem.Text, rcDocType.SelectedItem.Text + " Number", DocumentID, txtDocumentNumber.Text, clsCommon.Convert2DateTime(txtExpiryDate.Text), strMode, ddlReferenceName.SelectedValue.ToInt32(), "", false);
            }

            if (rcDocType.SelectedValue.ToInt32() > 13)
            {
                strDoctypeName = GetLocalResourceObject("Pagetitle.Title").ToString();
            }
            else
            {
                strDoctypeName = clsDocumentMaster.GetDocumentType(rcDocType.SelectedValue.ToInt32());
            }

            if (DocumentID > 0)
            {
                Label lblDocname, lblActualfilename, lblFilename;

                clsDocumentMaster.DeleteTreeMaster(DocumentID, rcDocType.SelectedValue.ToInt32());

                foreach (DataListItem item in dlOtherDoc.Items)
                {

                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    lblActualfilename = (Label)item.FindControl("lblActualfilename");

                    clsDocumentMaster objclsDocumentMaster = new clsDocumentMaster();
                    objclsDocumentMaster.DocumentID = DocumentID;
                    objclsDocumentMaster.DocumentTypeID = rcDocType.SelectedValue.ToInt32();
                    objclsDocumentMaster.ReferenceID = ddlReferenceName.SelectedValue.ToInt64();
                    objclsDocumentMaster.ReferenceNumber = ddlReferenceName.SelectedItem.Text.Trim();
                    objclsDocumentMaster.DocumentName = lblDocname.Text.Trim();
                    objclsDocumentMaster.FileName = lblFilename.Text.Trim();
                    objclsDocumentMaster.OperationTypeID = rbCompany.Checked ? (int)OperationType.Company : (int)OperationType.Employee;

                    if (dlOtherDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objclsDocumentMaster.SaveTreeMaster();

                        string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";


                        if (File.Exists(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(strPath))
                                Directory.CreateDirectory(strPath);

                            File.Move(Server.MapPath("~/Documents/temp/" + lblFilename.Text.Trim().ToString()), strPath + "/" + lblFilename.Text.Trim().ToString());
                        }

                    }
                    else
                    {
                        objclsDocumentMaster.SaveTreeMaster();

                        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                        if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(Path))
                                Directory.CreateDirectory(Path);

                            File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                        }
                    }

                }

            }

            if (hdnMode.Value == "0")
                mcMessage.InformationalMessage(GetGlobalResourceObject("documentscommon", "SavedSuccessfully").ToString());
            else
                mcMessage.InformationalMessage(GetGlobalResourceObject("documentscommon", "UpdatedSuccessfully").ToString());


            mpeMessage.Show();

            hdnDocumentID.Value = DocumentID.ToString();
            hdnMode.Value = "1";
            upMenu.Update();
            upEmployeeOtherDoc.Update();
            lnkListDoc_Click(null, null);
        }

    }

    protected void lnkRenew_Click(object sender, EventArgs e)
    {
        try
        {
            if (hdnMode.Value != "0")
            {
                int id = 0;
                clsDocumentAlert objAlert = new clsDocumentAlert();
                clsDocumentMaster objDocumentMaster = new clsDocumentMaster();
                objDocumentMaster.DocumentID = hdnDocumentID.Value.ToInt32();
                try
                {
                    objDocumentMaster.BeginEmp();
                    id = objDocumentMaster.UpdateRenewDocument();
                    objDocumentMaster.Commit();
                }
                catch (Exception ex)
                {
                    objDocumentMaster.RollBack();
                }

                objAlert.DeleteExpiryAlertMessage(rcDocType.SelectedValue.ToInt32(), id);

                mcMessage.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "RenewedSuccessfully").ToString());
                mpeMessage.Show();
                divIssueReceipt.Style["display"] = "block";
                divRenew.Style["display"] = "block";
                hdnDocumentID.Value = objDocumentMaster.DocumentID.ToStringCustom();
                EnableMenus();
                upMenu.Update();
                lnkListDoc_Click(sender, e);
            }
            else
                return;
        }
        catch (Exception ex)
        {
            clsCommon.WriteErrorLog(ex);
        }
    }

    protected void dlEmployeeOtherDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();
        switch (e.CommandName)
        {
            case "ALTER":
                hdnDocumentID.Value = e.CommandArgument.ToString();
                hdnMode.Value = "1";
                EditDocument();
                break;

            case "VIEW": hdnDocumentID.Value = e.CommandArgument.ToString();
                divViewDocuments.Style["display"] = "block";
                DisplayDocument();
                ViewState["CardID"] = e.CommandArgument;
                break;
        }
        upMenu.Update();
        upEmployeeOtherDoc.Update();
    }

    private void EditDocument()
    {
        GetAllReferenceNumberByOperationTypeID(true);
        pgrDocument.Visible = false;
        upEmployeeOtherDoc.Update();
        divAddDocument.Style["display"] = "block";
        divDataList.Style["display"] = "none";
        divViewDocuments.Style["display"] = "none";
        lnkPrint.Enabled = lnkEmail.Enabled = lnkDelete.Enabled = false;
        lnkPrint.OnClientClick = lnkEmail.OnClientClick = lnkDelete.OnClientClick = "return false;";
        clsUserMaster objUser = new clsUserMaster();



        DataTable datDocument = clsDocumentMaster.GetDocumentDetails(0, 0, hdnDocumentID.Value.ToInt32(), 0, objUser.GetCompanyId(), txtSearch.Text.Trim());

        if (datDocument != null && datDocument.Rows.Count > 0)
        {

            rbEmployee.Checked = datDocument.Rows[0]["OperationTypeID"].ToInt32() == (int)eOperationType.Employee ? true : false;
            rbCompany.Checked = datDocument.Rows[0]["OperationTypeID"].ToInt32() == (int)eOperationType.Company ? true : false;
            FillCombos();

            if (rbCompany.Checked)
            {
                tdReferenceName.InnerText = clsGlobalization.IsArabicCulture() ? "شركة" : "Company";
            }
            else
            {
                tdReferenceName.InnerText = clsGlobalization.IsArabicCulture() ? "عامل" : "Employee";
            }

            if (ddlReferenceName.Items.FindByValue(datDocument.Rows[0]["ReferenceID"].ToStringCustom()) != null)
                ddlReferenceName.SelectedValue = datDocument.Rows[0]["ReferenceID"].ToStringCustom();

            if (rcDocType.Items.FindByValue(datDocument.Rows[0]["DocumentTypeID"].ToStringCustom()) != null)
                rcDocType.SelectedValue = datDocument.Rows[0]["DocumentTypeID"].ToStringCustom();

            txtDocumentNumber.Text = datDocument.Rows[0]["DocumentNumber"].ToStringCustom();
            txtIssuedate.Text = datDocument.Rows[0]["IssuedDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtExpiryDate.Text = datDocument.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            txtOtherInfo.Text = datDocument.Rows[0]["Remarks"].ToStringCustom();
            lblCurrentStatus.Text = datDocument.Rows[0]["Status"].ToStringCustom();

            if (datDocument.Rows[0]["BinID"] != DBNull.Value)
            {
                trBinNumber.Style["display"] = "table-row";
                if (ddlBinNumber.Items.FindByValue(datDocument.Rows[0]["BinID"].ToStringCustom()) != null)
                    ddlBinNumber.SelectedValue = datDocument.Rows[0]["BinID"].ToStringCustom();
            }
            else
            {
                ddlBinNumber.SelectedValue = "-1";
                trBinNumber.Style["display"] = "none";
            }

            if (datDocument.Rows[0]["StatusID"] != DBNull.Value)
            {
                if (Convert.ToInt32(datDocument.Rows[0]["StatusID"]) == 1) // Receipt
                    divIssueReceipt.Style["display"] = "none";
                else
                    divIssueReceipt.Style["display"] = "block";
            }

            if (Convert.ToBoolean(datDocument.Rows[0]["IsRenewed"]) == true) // Renewed
            {
                divRenew.Style["display"] = "none";
                if (clsGlobalization.IsArabicCulture())
                    lblCurrentStatus.Text += " ] " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";
                else
                    lblCurrentStatus.Text += " [ " + GetGlobalResourceObject("DocumentsCommon", "renewed") + " ] ";

                ControlEnableDisble(false);
            }
            else
            {
                divRenew.Style["display"] = "block";
                ControlEnableDisble(true);
            }

            upMenu.Update();

            //if(clsDocumentMaster.IsReceipted(datDocument.Rows[0]["OperationTypeID"].ToInt32(),datDocument.Rows[0]["DocumentTypeID"].ToInt32(),hdnDocumentID.Value.ToInt32()))
            //{
            //    imgDocIR.ImageUrl = "~/images/document_issue.png";
            //    lnkDocumentIR.Text = "<h5>Issue</h5>";
            //}
            //else
            //{
            //    imgDocIR.ImageUrl = "~/images/document_reciept.png";
            //    lnkDocumentIR.Text = "<h5>Receipt</h5>";
            //}

            //upMenu.Update();

            rbCompany.Enabled = false;
            rbEmployee.Enabled = false;
        }


        datDocument = new DataTable();
        datDocument = clsDocumentMaster.GetDocumentsAttached(hdnDocumentID.Value.ToInt32(), ddlReferenceName.SelectedValue.ToInt64());

        dlOtherDoc.DataSource = datDocument;
        dlOtherDoc.DataBind();

        ViewState["OtherDoc"] = datDocument;

        if (dlOtherDoc.Items.Count > 0)
            divOtherDoc.Style["display"] = "block";
        else
            divOtherDoc.Style["display"] = "none";

        upMenu.Update();
        upEmployeeOtherDoc.Update();
    }

    private void ControlEnableDisble(bool blnTemp)
    {
        rbEmployee.Enabled = rbCompany.Enabled = ddlReferenceName.Enabled = rcDocType.Enabled = blnTemp;
        txtDocumentNumber.Enabled = txtIssuedate.Enabled = txtExpiryDate.Enabled = txtOtherInfo.Enabled = blnTemp;
        ddlReferenceName.Enabled = false;
    }
    private string Printdetails(string sDocumentIds, bool isSingle)
    {
        StringBuilder sb = new StringBuilder();
        DataTable datPrint = clsDocumentMaster.PrintSelected(sDocumentIds);
        if (datPrint != null && datPrint.Rows.Count > 0)
        {
            if (isSingle)
            {
                sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
                sb.Append("<tr><td><table border='0'>");
                sb.Append("<tr>");
                sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(datPrint.Rows[0]["DocumentNumber"]) + "</td>");
                sb.Append("</tr>");
                sb.Append("</table>");
                sb.Append("<tr><td style='padding-left:15px;'>");
                sb.Append("<table width='100%' style='font-size:11px;' align='left'>");

                sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>Other Document Information</td></tr>");
                sb.Append("<tr><td width='15%'>" + Convert.ToString(datPrint.Rows[0]["OperationType"]) + "</td><td width='5px'>:</td><td style='width:auto:max-width:85%'>" + Convert.ToString(datPrint.Rows[0]["DocumentNumber"]) + "</td></tr>");
                sb.Append("<tr><td>" + GetLocalResourceObject("DocumentNumber.Text").ToString() + " </td><td width='5px'>:</td><td>" + Convert.ToString(datPrint.Rows[0]["DocumentType"]) + "</td></tr>");
                sb.Append("<tr><td> " + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + " </td><td>:</td><td>" + (datPrint.Rows[0]["IssuedDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td> " + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td>:</td><td>" + (datPrint.Rows[0]["ExpiryDate"]).ToString() + "</td></tr>");
                sb.Append("<tr><td>" + GetGlobalResourceObject("DocumentsCommon", "Isrenewed").ToString() + " </td><td>:</td><td>" + (datPrint.Rows[0]["IsRenewed"]).ToString() + "</td></tr>");
                sb.Append("<tr><td valign='top' >" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td><td width='5px' valign='top'>:</td><td style='word-break:break-all;'>" + Convert.ToString(datPrint.Rows[0]["Remarks"]) + "</td></tr>");

                sb.Append("</td>");
                sb.Append("</table>");
                sb.Append("</td>");
                sb.Append("</tr></table>");
            }
            else
            {
                sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
                sb.Append("<tr><td style='font-weight:bold;' align='center'>Other Document Information</td></tr>");
                sb.Append("<tr><td>");
                sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");
                sb.Append("<tr style='font-weight:bold;'><td width='100px'> " + GetLocalResourceObject("Type.Text").ToString() + "  </td><td width='150px'>" + GetLocalResourceObject("DocumentNumber.Text").ToString() + " </td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IssueDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "IsRenewed").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</td></tr>");
                sb.Append("<tr><td colspan='7'><hr></td></tr>");
                for (int i = 0; i < datPrint.Rows.Count; i++)
                {
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["OperationType"]) + "</td>");
                    sb.Append("<td>" + Convert.ToString(datPrint.Rows[i]["DocumentNumber"]) + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IssuedDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["ExpiryDate"].ToString() + "</td>");
                    sb.Append("<td>" + datPrint.Rows[i]["IsRenewed"].ToString() + "</td>");
                    sb.Append("<td style='width:200px;word-break:break-all'>" + datPrint.Rows[i]["Remarks"].ToString() + "</td></tr>");

                }
                sb.Append("</td></tr>");
                sb.Append("</table>");
            }
        }
        return sb.ToString();
    }



    private void DisplayDocument()
    {
        pgrDocument.Visible = false;
        dlEmployeeOtherDoc.DataSource = null;
        dlEmployeeOtherDoc.DataBind();
        divAddDocument.Style["display"] = "none";
        divDataList.Style["display"] = "none";
        divViewDocuments.Style["display"] = "block";

        lnkPrint.OnClientClick = "return true;";
        lnkEmail.OnClientClick = "return true;";
        if(lnkDelete.Enabled)
        lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');";


        DataTable datDocument = clsDocumentMaster.GetOtherDocumentDetails(0, 0, hdnDocumentID.Value.ToInt32(), 0);
        if (datDocument != null && datDocument.Rows.Count > 0)
        {
            divOperationType.InnerText = datDocument.Rows[0]["OperationType"].ToStringCustom();
            divReferenceType.InnerText = datDocument.Rows[0]["OperationType"].ToStringCustom();
            divReferenceName.InnerText = datDocument.Rows[0]["ReferenceNumber"].ToStringCustom();
            divDoumentType.InnerText = datDocument.Rows[0]["DocumentType"].ToStringCustom();
            divDocumentNumber.InnerText = datDocument.Rows[0]["DocumentNumber"].ToStringCustom();
            divIssuedDate.InnerText = datDocument.Rows[0]["IssuedDate"].ToDateTime().ToString("dd MMM yyyy");
            divExpiryDate.InnerText = datDocument.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd MMM yyyy");
            divIsRenewed.InnerText = datDocument.Rows[0]["IsRenewed"].ToStringCustom();
            divRemarks.InnerText = datDocument.Rows[0]["Remarks"].ToStringCustom();
            divIsDelete.InnerText = datDocument.Rows[0]["IsDelete"].ToStringCustom();
        }
    }

    protected void fuDocument_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string strFilename = string.Empty;
        string strActualFilename = string.Empty;
        if (fuDocument.HasFile)
        {
            strActualFilename = fuDocument.FileName;
            strFilename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuDocument.FileName);
            Session["Filename"] = strFilename;
            fuDocument.SaveAs(Server.MapPath("~/Documents/temp/") + strFilename);

        }
    }

    protected void btnattachpassdoc_Click(object sender, EventArgs e)
    {
        divOtherDoc.Style["display"] = "block";

        DataTable dtDocs;
        DataRow drRow;

        if (ViewState["OtherDoc"] == null)
            dtDocs = clsDocumentMaster.GetDocumentsAttached(-1, -1);
        else
        {
            DataTable dtTemp = (DataTable)this.ViewState["OtherDoc"];
            dtDocs = dtTemp;
        }


        drRow = dtDocs.NewRow();

        drRow["Docname"] = Convert.ToString(txtDocname.Text.Trim());
        drRow["Filename"] = Session["Filename"];
        drRow["Actualfilename"] = Session["Filename"];
        dtDocs.Rows.Add(drRow);


        dlOtherDoc.DataSource = dtDocs;
        dlOtherDoc.DataBind();
        ViewState["OtherDoc"] = dtDocs;
        txtDocname.Text = string.Empty;

        updOtherDoc.Update();
    }

    protected void dlOtherDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":

                Label lblDocname, lblFilename;

                if (e.CommandArgument != string.Empty)
                {

                    lblFilename = (Label)e.Item.FindControl("lblFilename");

                    string strFilename = lblFilename.Text.Trim();
                    string strFolder = string.Empty;


                    clsDocumentMaster.SingleNodeDelete(e.CommandArgument.ToInt32());

                    if (Convert.ToInt32(rcDocType.SelectedValue) > 25)
                        strFolder = "OtherDocuments";
                    else
                    {
                        strFolder = clsDocumentMaster.GetDocumentType(rcDocType.SelectedValue.ToInt32());
                    }

                    string strPath = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(strPath + "/" + Convert.ToString(strFilename)))
                        File.Delete(strPath + "/" + Convert.ToString(strFilename));

                }



                DataTable dtDocs = clsDocumentMaster.GetDocumentsAttached(-1, -1);
                DataRow drRow;
                foreach (DataListItem Item in dlOtherDoc.Items)
                {
                    if (e.Item.ItemIndex == Item.ItemIndex)
                        continue;

                    drRow = dtDocs.NewRow();

                    lblDocname = (Label)Item.FindControl("lblDocname");
                    lblFilename = (Label)Item.FindControl("lblFilename");

                    drRow["Node"] = dlOtherDoc.DataKeys[Item.ItemIndex];
                    drRow["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    drRow["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dtDocs.Rows.Add(drRow);
                }

                dlOtherDoc.DataSource = dtDocs;
                dlOtherDoc.DataBind();

                ViewState["OtherDoc"] = dtDocs;
                break;
        }
    }
    protected void lnkDocumentIR_Click(object sender, EventArgs e)
    {
        if (hdnMode.Value == "1")
        {
            DateTime ExpiryDate;
            DateTime.TryParseExact(txtExpiryDate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out ExpiryDate);

            int intOperationTypeID = rbEmployee.Checked ? (int)OperationType.Employee : (int)OperationType.Company;
            ucDocIssueReceipt.eOperationType = (OperationType)intOperationTypeID;
            ucDocIssueReceipt.DocumentID = hdnDocumentID.Value.ToInt32();
            ucDocIssueReceipt.eDocumentType = (DocumentType)(rcDocType.SelectedValue.ToInt32());
            ucDocIssueReceipt.DocumentNumber = txtDocumentNumber.Text.Trim();
            ucDocIssueReceipt.EmployeeID = ddlReferenceName.SelectedValue.ToInt32();
            ucDocIssueReceipt.dtExpiryDate = ExpiryDate;
            ucDocIssueReceipt.Call();
            mpeIssueReceipt.Show();
            upnlIssueReceipt.Update();
            lnkListDoc_Click(sender, e);
        }
        else
            return;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearControls();
        ViewDocuments();
        upMenu.Update();
    }

    protected void dlEmployeeOtherDoc_ItemBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = MblnUpdatePermission;
        }

    }
}
