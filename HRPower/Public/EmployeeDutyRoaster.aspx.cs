﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_EmployeeDutyRoaster : System.Web.UI.Page
{
    clsEmployeeDutyRoaster objclsEmployeeDutyRoaster;
    DataTable DtRoaster;
    string Date;
    string msg;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadCombos();         
        }
    }
    public Public_EmployeeDutyRoaster()
    {
        objclsEmployeeDutyRoaster = new clsEmployeeDutyRoaster();
    }

    public void LoadCombos()
    {
        DataTable dtCombo = new DataTable();
        dtCombo = objclsEmployeeDutyRoaster.LoadCombo(1);
        drpCompany.DataSource = dtCombo;
        drpCompany.DataBind();
        drpCompany.DataTextField = "CompanyName";
        drpCompany.DataValueField = "CompanyID";
        drpCompany.DataBind();
        dtCombo = null;
        dtCombo = objclsEmployeeDutyRoaster.LoadCombo(2);
        drpDepartment.DataSource = dtCombo;
        drpDepartment.DataTextField = "Department";
        drpDepartment.DataValueField = "DepartmentID";
        drpDepartment.DataBind();
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
       if (Validates())
        {
            lblEmpty.Visible = false;
            int iDaycount = 30;
            System.DateTime dpDate = default(System.DateTime);
            dpDate = GetStratDate().ToDateTime();
            iDaycount = (dpDate.AddMonths(1) - dpDate).TotalDays.ToInt32();
            Session["DayCount"] = iDaycount;
            string SetDate = GetStratDate();
            DataTable DtRoaster = objclsEmployeeDutyRoaster.GetOffDayMarkDetails(drpCompany.SelectedValue.ToInt32(), SetDate, drpDepartment.SelectedValue.ToInt32());
            Session["RoasterTable"] = DtRoaster;
            var EmpIDs = (from DataRow r in DtRoaster.AsEnumerable() orderby r["EmployeeID"] ascending select r.Field<Int32>("EmployeeID")).Distinct();
            var Names = (from DataRow r in DtRoaster.AsEnumerable() orderby r["EmployeeID"] ascending select r.Field<String>("Employee")).Distinct();
            DataTable dtNames = new DataTable();
            dtNames.Columns.Add("EmployeeFullName", typeof(string));
            dtNames.Columns.Add("EmployeeID", typeof(int));

            for (int c = 0; c <= Names.Count() - 1; c++)
            {
                DataRow RowName = dtNames.NewRow();
                string iEmpName = Names.ElementAt(c);
                int iEmpID = EmpIDs.ElementAt(c);
                RowName["EmployeeFullName"] = iEmpName.ToString();
                RowName["EmployeeID"] = iEmpID.ToString();
                dtNames.Rows.Add(RowName);
            }
            GrdRoaster.DataSource = dtNames;
            GrdRoaster.DataBind();       
            int SetVal = -1;

            for (int SetRow = 0; SetRow < GrdRoaster.Rows.Count; SetRow++)
            {
                for (int SetCol = 2; SetCol < iDaycount + 2; SetCol++)
                {
                    SetVal++;
                    SetRoasterID(SetRow, SetCol, SetVal);
                }
            }
        }
    }
    private string GetStratDate()
    {
        //'Getting Start date
        DateTime dt;
        int date = 0;
        int Year = 0;
        string sMonth = "";
        Date = txtMonth.Text;
        //string Date =  (Convert.ToDateTime(Ddate)).ToString("dd MMM yyyy");
        if (DateTime.TryParse(Date, out dt))
        {
            date = dt.Month;
            Year = dt.Year;
        }
        switch (Convert.ToDateTime(Date).Month)
        {
            case 1:
                sMonth = "Jan";
                break;
            case 2:
                sMonth = "Feb";
                break;
            case 3:
                sMonth = "Mar";
                break;
            case 4:
                sMonth = "Apr";
                break;
            case 5:
                sMonth = "May";
                break;
            case 6:
                sMonth = "Jun";
                break;
            case 7:
                sMonth = "Jul";
                break;
            case 8:
                sMonth = "Aug";
                break;
            case 9:
                sMonth = "Sep";
                break;
            case 10:
                sMonth = "Oct";
                break;
            case 11:
                sMonth = "Nov";
                break;
            case 12:
                sMonth = "Dec";
                break;
        }
        return ("01-" + sMonth + "-" + Year).ToString();
    }
    protected void GrdRoaster_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int iDaycount = Convert.ToInt32(Session["DayCount"]);
        if (e.Row.RowIndex > -1)
        {
            string name = "";
            DataRowView drv = (DataRowView)e.Row.DataItem;
            Label EmpID = (Label)e.Row.FindControl("EmployeeID");
            Label EmpName = (Label)e.Row.FindControl("lblEmployee");
            EmpName.Text = Convert.ToString(drv["EmployeeFullName"]);
            EmpID.Text = Convert.ToString(drv["EmployeeID"]);
            DataTable dtShift = objclsEmployeeDutyRoaster.FillCombos("select 0 as ShiftID,'None' as ShiftName Union select -1 ShiftID,'Rest' ShiftName Union select ShiftID,ShiftName from PayShiftReference");
            var ddl1 = (DropDownList)e.Row.FindControl("drpShift1");
            ddl1.DataSource = dtShift;
            ddl1.DataTextField = "ShiftName";
            ddl1.DataValueField = "ShiftID";
            ddl1.DataBind();
            var ddl2 = (DropDownList)e.Row.FindControl("drpShift2");
            ddl2.DataSource = dtShift;
            ddl2.DataTextField = "ShiftName";
            ddl2.DataValueField = "ShiftID";
            ddl2.DataBind();
            var ddl3 = (DropDownList)e.Row.FindControl("drpShift3");
            ddl3.DataSource = dtShift;
            ddl3.DataTextField = "ShiftName";
            ddl3.DataValueField = "ShiftID";
            ddl3.DataBind();
            var ddl4 = (DropDownList)e.Row.FindControl("drpShift4");
            ddl4.DataSource = dtShift;
            ddl4.DataTextField = "ShiftName";
            ddl4.DataValueField = "ShiftID";
            ddl4.DataBind();
            var ddl5 = (DropDownList)e.Row.FindControl("drpShift5");
            ddl5.DataSource = dtShift;
            ddl5.DataTextField = "ShiftName";
            ddl5.DataValueField = "ShiftID";
            ddl5.DataBind();
            var ddl6 = (DropDownList)e.Row.FindControl("drpShift6");
            ddl6.DataSource = dtShift;
            ddl6.DataTextField = "ShiftName";
            ddl6.DataValueField = "ShiftID";
            ddl6.DataBind();
            var ddl7 = (DropDownList)e.Row.FindControl("drpShift7");
            ddl7.DataSource = dtShift;
            ddl7.DataTextField = "ShiftName";
            ddl7.DataValueField = "ShiftID";
            ddl7.DataBind();
            var ddl8 = (DropDownList)e.Row.FindControl("drpShift8");
            ddl8.DataSource = dtShift;
            ddl8.DataTextField = "ShiftName";
            ddl8.DataValueField = "ShiftID";
            ddl8.DataBind();
            var ddl9 = (DropDownList)e.Row.FindControl("drpShift9");
            ddl9.DataSource = dtShift;
            ddl9.DataTextField = "ShiftName";
            ddl9.DataValueField = "ShiftID";
            ddl9.DataBind();
            var ddl10 = (DropDownList)e.Row.FindControl("drpShift10");
            ddl10.DataSource = dtShift;
            ddl10.DataTextField = "ShiftName";
            ddl10.DataValueField = "ShiftID";
            ddl10.DataBind();
            var ddl11 = (DropDownList)e.Row.FindControl("drpShift11");
            ddl11.DataSource = dtShift;
            ddl11.DataTextField = "ShiftName";
            ddl11.DataValueField = "ShiftID";
            ddl11.DataBind();
            var ddl12 = (DropDownList)e.Row.FindControl("drpShift12");
            ddl12.DataSource = dtShift;
            ddl12.DataTextField = "ShiftName";
            ddl12.DataValueField = "ShiftID";
            ddl12.DataBind();
            var ddl13 = (DropDownList)e.Row.FindControl("drpShift13");
            ddl13.DataSource = dtShift;
            ddl13.DataTextField = "ShiftName";
            ddl13.DataValueField = "ShiftID";
            ddl13.DataBind();
            var ddl14 = (DropDownList)e.Row.FindControl("drpShift14");
            ddl14.DataSource = dtShift;
            ddl14.DataTextField = "ShiftName";
            ddl14.DataValueField = "ShiftID";
            ddl14.DataBind();
            var ddl15 = (DropDownList)e.Row.FindControl("drpShift15");
            ddl15.DataSource = dtShift;
            ddl15.DataTextField = "ShiftName";
            ddl15.DataValueField = "ShiftID";
            ddl15.DataBind();
            var ddl16 = (DropDownList)e.Row.FindControl("drpShift16");
            ddl16.DataSource = dtShift;
            ddl16.DataTextField = "ShiftName";
            ddl16.DataValueField = "ShiftID";
            ddl16.DataBind();
            var ddl17 = (DropDownList)e.Row.FindControl("drpShift17");
            ddl17.DataSource = dtShift;
            ddl17.DataTextField = "ShiftName";
            ddl17.DataValueField = "ShiftID";
            ddl17.DataBind();
            var ddl18 = (DropDownList)e.Row.FindControl("drpShift18");
            ddl18.DataSource = dtShift;
            ddl18.DataTextField = "ShiftName";
            ddl18.DataValueField = "ShiftID";
            ddl18.DataBind();
            var ddl19 = (DropDownList)e.Row.FindControl("drpShift19");
            ddl19.DataSource = dtShift;
            ddl19.DataTextField = "ShiftName";
            ddl19.DataValueField = "ShiftID";
            ddl19.DataBind();
            var ddl20 = (DropDownList)e.Row.FindControl("drpShift20");
            ddl20.DataSource = dtShift;
            ddl20.DataTextField = "ShiftName";
            ddl20.DataValueField = "ShiftID";
            ddl20.DataBind();
            var ddl21 = (DropDownList)e.Row.FindControl("drpShift21");
            ddl21.DataSource = dtShift;
            ddl21.DataTextField = "ShiftName";
            ddl21.DataValueField = "ShiftID";
            ddl21.DataBind();
            var ddl22 = (DropDownList)e.Row.FindControl("drpShift22");
            ddl22.DataSource = dtShift;
            ddl22.DataTextField = "ShiftName";
            ddl22.DataValueField = "ShiftID";
            ddl22.DataBind();
            var ddl23 = (DropDownList)e.Row.FindControl("drpShift23");
            ddl23.DataSource = dtShift;
            ddl23.DataTextField = "ShiftName";
            ddl23.DataValueField = "ShiftID";
            ddl23.DataBind();
            var ddl24 = (DropDownList)e.Row.FindControl("drpShift24");
            ddl24.DataSource = dtShift;
            ddl24.DataTextField = "ShiftName";
            ddl24.DataValueField = "ShiftID";
            ddl24.DataBind();
            var ddl25 = (DropDownList)e.Row.FindControl("drpShift25");
            ddl25.DataSource = dtShift;
            ddl25.DataTextField = "ShiftName";
            ddl25.DataValueField = "ShiftID";
            ddl25.DataBind();
            var ddl26 = (DropDownList)e.Row.FindControl("drpShift26");
            ddl26.DataSource = dtShift;
            ddl26.DataTextField = "ShiftName";
            ddl26.DataValueField = "ShiftID";
            ddl26.DataBind();
            var ddl27 = (DropDownList)e.Row.FindControl("drpShift27");
            ddl27.DataSource = dtShift;
            ddl27.DataTextField = "ShiftName";
            ddl27.DataValueField = "ShiftID";
            ddl27.DataBind();
            var ddl28 = (DropDownList)e.Row.FindControl("drpShift28");
            ddl28.DataSource = dtShift;
            ddl28.DataTextField = "ShiftName";
            ddl28.DataValueField = "ShiftID";
            ddl28.DataBind();
            var ddl29 = (DropDownList)e.Row.FindControl("drpShift29");
            ddl29.DataSource = dtShift;
            ddl29.DataTextField = "ShiftName";
            ddl29.DataValueField = "ShiftID";
            ddl29.DataBind();
            var ddl30 = (DropDownList)e.Row.FindControl("drpShift30");
            ddl30.DataSource = dtShift;
            ddl30.DataTextField = "ShiftName";
            ddl30.DataValueField = "ShiftID";
            ddl30.DataBind();
            var ddl31 = (DropDownList)e.Row.FindControl("drpShift31");
            ddl31.DataSource = dtShift;
            ddl31.DataTextField = "ShiftName";
            ddl31.DataValueField = "ShiftID";
            ddl31.DataBind();


            if (iDaycount < 28)
            {
                GrdRoaster.Visible = false;
            }
            if (iDaycount == 28)
            {
                GrdRoaster.Columns[30].Visible = false;
                GrdRoaster.Columns[31].Visible = false;
                GrdRoaster.Columns[32].Visible = false;
            }
            if (iDaycount > 28)
            {
                GrdRoaster.Columns[30].Visible = true;
                GrdRoaster.Columns[31].Visible = true;
                GrdRoaster.Columns[32].Visible = true;
            }
            if (iDaycount == 29)
            {
                GrdRoaster.Columns[31].Visible = false;
                GrdRoaster.Columns[32].Visible = false;
            }
            if (iDaycount > 29)
            {
                GrdRoaster.Columns[31].Visible = true;
                GrdRoaster.Columns[32].Visible = true;
            }
            if (iDaycount == 30)
            {
                GrdRoaster.Columns[32].Visible = false;
            }
            if (iDaycount > 30)
            {
                GrdRoaster.Columns[32].Visible = true;
            }
        }
    }
    protected void ButtonSave_Click(object sender, ImageClickEventArgs e)
    {    
        if (Validates())
        {
            System.Threading.Thread.Sleep(5000);
            if (SaveOffDayMarkInfo())
            {
                lblEmpty.Visible = false;

                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وأضاف اجب مشوي بنجاح") : ("Duty Roaster Added Successfully");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                //this.Page.Form.Style.Add("cursor", "default");
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا توجد بيانات لانقاذ") : ("No Data To Save ");
                msgs.InformationalMessage(msg);
                mpeMessage.Show();
                //Response.Write("<Script>alert('Empty Data')</Script>");
                //lblEmpty.Text = "No Data To Save ";
                //lblEmpty.ForeColor = System.Drawing.Color.Red;
                //lblEmpty.Visible = true;
               //msgs.InformationalMessage(GetLocalResourceObject("No Data To Save").ToString());

                
            }
        }

    }
   
    private bool Validates()
    {
        DateTime dtDate;
        if (txtMonth.Text == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد التاريخ") : ("Select Date ");
            lblrequired.Text = msg; 
            lblrequired.ForeColor = System.Drawing.Color.Red;
            lblrequired.Visible = true;          
            //Response.Write("<Script>alert('Select RoasterDate')</Script>");
            return false;
        }
        else if (!(DateTime.TryParse(txtMonth.Text, out dtDate)))
        {
            // ShowMessage("Invalid from date");
            msg = clsUserMaster.GetCulture() == "ar-AE" ? (" تنسيق التاريخ غير صالح ") : ("Invalid Date Format");
            lblrequired.Text = msg;
            lblrequired.ForeColor = System.Drawing.Color.Red;
            lblrequired.Visible = true;
            //Response.Write("<Script>alert(' Invalid Date Format')</Script>");

            return false;
        }
        else
        {
            return true;
        }
    }
    private bool SaveOffDayMarkInfo()
    {
        //Saving the OffDayMark info
        try
        {
            if (GrdRoaster.Rows.Count > 0)
            {
               SaveDetails();
                return true;
                //FillParameters(); 
            }
            else
            {
                lblEmpty.Visible = true;
                return false;  
            }
        }
        catch (Exception ex)
        {
            return false;
        }
        //bool blnRetValue = false;
        //if (SaveOffDayMarkNew())
        //{
        //    blnRetValue = true;
        //}

        //return blnRetValue;
    }

    public bool SaveDetails()
    {
        bool blnRetValue = false;
        clsUserMaster objUser = new clsUserMaster();
        int iDaycount = Convert.ToInt32(Session["DayCount"]);
        for (int iIndex = 0; iIndex < GrdRoaster.Rows.Count; iIndex++)
        {
            for (int Cot = 1, dayID = 1; Cot <= iDaycount; Cot++, dayID++)
            {
                objclsEmployeeDutyRoaster.intCompanyID = Convert.ToInt32(drpCompany.SelectedValue);
                objclsEmployeeDutyRoaster.strRosterDate = dayID + " " + (Convert.ToDateTime(txtMonth.Text)).ToString("MMM") + " " + (Convert.ToDateTime(txtMonth.Text)).ToString("yyyy");
                Label EmpID = (Label)GrdRoaster.Rows[iIndex].Cells[0].FindControl("EmployeeID");
                objclsEmployeeDutyRoaster.intEmployeeID = Convert.ToInt32(EmpID.Text);
                objclsEmployeeDutyRoaster.intUserID = objUser.GetUserId();
                objclsEmployeeDutyRoaster.intDepartmentID = Convert.ToInt32(drpDepartment.SelectedValue);
                objclsEmployeeDutyRoaster.intRosterStatusID = GetRoasterID(iIndex, Cot);
                objclsEmployeeDutyRoaster.SaveDutyRoaster();
                blnRetValue = true;
            }
        }
        return blnRetValue;
    }
    public int GetRoasterID(int IndexValue, int Value)
    {
        int Inner = Value;
        int Columns = Value;
        DropDownList ddl;
        int RoasterStatusID = 0;
        if (Inner == 1)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift1");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 2)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift2");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 3)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift3");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 4)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift4");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 5)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift5");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 6)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift6");
            return RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 7)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift7");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 8)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift8");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 9)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift9");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 10)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift10");
            return RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 11)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift11");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 12)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift12");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 13)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift13");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 14)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift14");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 15)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift15");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 16)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift16");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 17)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift17");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 18)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift18");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 19)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift19");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 20)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift20");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 21)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift21");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 22)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift22");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 23)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift23");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 24)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift24");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 25)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift25");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 26)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift26");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 27)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift27");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 28)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift28");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 29)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift29");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        else if (Inner == 30)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift30");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);

        }
        else if (Inner == 31)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift31");
            RoasterStatusID = Convert.ToInt32(ddl.SelectedValue);
        }
        return RoasterStatusID;

    }
    public void  SetRoasterID(int IndexValue, int Value,int RowVal)
    {
      int iDays = Convert.ToInt32(Session["DayCount"]);
        DataTable dt = (DataTable)Session["RoasterTable"];
        int Inner = Value;
        int Columns = Value;
        DropDownList ddl;
        if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) ==1)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift1");
            ddl.SelectedValue = Convert.ToString(dt.Rows[ RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 2)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift2");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 3)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift3");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);        
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 4)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift4");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 5)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift5");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 6)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift6");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 7)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift7");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 8)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift8");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 9)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift9");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 10)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift10");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 11)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift11");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);            
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 12)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift12");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 13)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift13");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 14)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift14");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 15)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift15");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);            
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 16)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift16");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 17)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift17");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 18)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift18");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 19)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift19");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);            
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 20)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift20");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 21)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift21");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 22)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift22");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 23)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift23");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 24)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift24");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);            
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 25)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift25");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 26)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift26");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 27)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift27");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 28)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift28");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 29)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift29");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);          
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 30)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift30");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);           
        }
        else if (Convert.ToInt32(dt.Rows[RowVal]["DayID"]) == 31)
        {
            ddl = (DropDownList)GrdRoaster.Rows[IndexValue].FindControl("drpShift31");
            ddl.SelectedValue = Convert.ToString(dt.Rows[RowVal]["StatusDes"]);
        }
    }

    protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
    {
        if (DeleteValidating())
        {
            return;
        }
        FillParameter();
        objclsEmployeeDutyRoaster.DeleteRoaster();
        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("بنجاح واجب مشوي محذوفة") : ("Duty Roaster Deleted Successfully");
        msgs.InformationalMessage(msg);
        mpeMessage.Show();
        btnShow_Click(sender, e);
    }

    private bool DeleteValidating()
    {
        try
        {
            bool Returns = false;
            if (FillParameter())
            {               
                DataTable DT = objclsEmployeeDutyRoaster.DeleteValid();
                if (DT.Rows.Count > 0)
                {
                    Returns = false;
                }
                else
                {
                    Returns = true;
                }
            }
            return Returns;
        } 
        catch
        {
            return false;
        }
    }
    public bool FillParameter()
    {
        bool ReVal = false;
        if (txtMonth.Text == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("حدد التاريخ") : ("Select Date ");
            lblrequired.Text = msg;
            lblrequired.ForeColor = System.Drawing.Color.Red;
            lblrequired.Visible = true;
            ReVal = true;
        }
        else
        {
            lblrequired.Visible = false;
            string DelDate = GetStratDate();
            objclsEmployeeDutyRoaster.intCompanyID = Convert.ToInt32(drpCompany.SelectedValue);
            objclsEmployeeDutyRoaster.strRosterDate = DelDate;
            objclsEmployeeDutyRoaster.intDepartmentID = Convert.ToInt32(drpDepartment.SelectedValue);
            ReVal = false;
        }
        return ReVal;
    }
    protected void ClearButton_Click(object sender, ImageClickEventArgs e)
    {
        ClearAllControls();
        Reset();
    }
    public void ClearAllControls()
    {
        txtMonth.Text = String.Empty;
        drpCompany.SelectedIndex = -1;
        drpDepartment.SelectedIndex = -1;
        lblrequired.Visible = false;
    }

    protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        Reset();
    }
    public void Reset()
    {
        DataTable dtEmpty = new DataTable();
        GrdRoaster.DataSource = dtEmpty;
        GrdRoaster.DataBind();
    }
    protected void PrintButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/reporting/RptOffDayMark.aspx");
    }

    protected void txtMonth_TextChanged(object sender, EventArgs e)
    {
        if (txtMonth.Text != "")
        {
            lblrequired.Visible = false;
            Date = txtMonth.Text;
        }
    }

}
