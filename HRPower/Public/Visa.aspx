﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Visa.aspx.cs" Inherits="Public_Visa" Title="Visa" %>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
select
{
	margin-left:5px;
}
</style>
    <div id='cssmenu'>
        <ul>
            <li><a href="Passport.aspx">
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li class="selected"><a href="Visa.aspx">
                <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx">
                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx">
                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1">
                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2">
                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3">
                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx">
                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx">
                <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx">
                <asp:Literal ID="Literal38" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx">
                <asp:Literal ID="Literal39" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx">
                <asp:Literal ID="Literal40" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx">
                <asp:Literal ID="Literal41" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <asp:UpdatePanel ID="upEmployeeVisa" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="fvEmployees" runat="server">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <%-- Company--%>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:DocumentsCommon,Company%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                        DataValueField="CompanyID" DataTextField="CompanyName" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150px" class="trLeft">
                                                    <%--Employee--%>
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="EmployeeID" DataTextField="EmployeeName">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hdVisaID" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150px" class="trLeft">
                                                    <%--Visa Type--%>
                                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="VisaType"></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:UpdatePanel ID="upVisaType" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlVisaType" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="VisaTypeID" DataTextField="VisaType">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rfvddlVisaType" runat="server" ControlToValidate="ddlVisaType"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <%--Visa Number--%>
                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="VisaNumber"></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtvisanumber" CssClass="textbox_mandatory" MaxLength="45" runat="server"
                                                        Width="182px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvpassport" runat="server" ControlToValidate="txtvisanumber"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <%--Place Of Issue--%>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:DocumentsCommon,PlaceOfIssue%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtPlaceofIssue" CssClass="textbox_mandatory" MaxLength="45" runat="server"
                                                        Width="182px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--Issue Date--%>
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("IssueDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvVisaIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                                                    ClientValidationFunction="validatevisaIssuedate" CssClass="error" ValidateEmptyText="True"
                                                                    ValidationGroup="Employee" Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%--  Expiry Date--%>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("ExpiryDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvVisaExpirydate" runat="server" ControlToValidate="txtExpiryDate"
                                                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Employee" ClientValidationFunction="validatevisaExpirydate"
                                                                    Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <%-- Country--%>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                                </td>
                                                <td valign="top" class="trRight">
                                                    <asp:UpdatePanel ID="updCountryOfOrigin" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="rcCountryOfOrigin" runat="server" CssClass="dropdownlist_mandatory"
                                                                Width="190px">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnCountry" runat="server" Text="..." CausesValidation="False" CssClass="referencebutton"
                                                                Height="20px" OnClick="btnCountry_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div style="display: none">
                                                                <asp:Button ID="Button1" runat="server" />
                                                            </div>
                                                            <div id="pnlModalPopUp" runat="server" style="display: none;">
                                                                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
                                                            </div>
                                                            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                                                                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                                                            </AjaxControlToolkit:ModalPopupExtender>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <%--Other Info--%>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="textbox" Width="300px" TextMode="MultiLine"
                                                        Height="76px" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <%--Current Status--%>
                                                    <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:Label ID="lblCurrentStatus" runat="server" Text='<%$Resources:DocumentsCommon,New%>'
                                                        Width="30%"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft" colspan="2">
                                                    <div id="divParse">
                                                        <fieldset style="padding: 10px 3px">
                                                            <legend>
                                                                <%--Add Documents--%>
                                                                <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>
                                                            </legend>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top">
                                                                        <%--Document Name--%>
                                                                        <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 3px" valign="top">
                                                                    </td>
                                                                    <td valign="top">
                                                                        <%--Choose file..--%><asp:Literal ID="Literal13" runat="server" Text='<%$Resources:DocumentsCommon,ChooseFile%>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="175px">
                                                                        <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="50" Width="150px"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee1"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="cvVisadocnameduplicate" runat="server" ClientValidationFunction="validatevisadocnameduplicate"
                                                                            ControlToValidate="txtDocname" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'
                                                                            ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="width: 2px">
                                                                    </td>
                                                                    <td valign="top" width="300px">
                                                                        <AjaxControlToolkit:AsyncFileUpload ID="fuVisa" runat="server" OnUploadedComplete="fuVisa_UploadedComplete"
                                                                            PersistFile="True" Width="310px" />
                                                                        <asp:CustomValidator ID="cvVisaAttachment" runat="server" ClientValidationFunction="validatevisaAttachment"
                                                                            CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                                                            ValidateEmptyText="True" ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="padding-left: 15px" valign="top" width="200px">
                                                                        <asp:LinkButton ID="btnattachvisadoc" runat="server" Enabled="true" OnClick="btnattachvisadoc_Click"
                                                                            Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="Employee1">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft" colspan="2">
                                                    <div id="divPassDoc" runat="server" class="container_content" style="display: none;
                                                        height: 150px; overflow: auto">
                                                        <asp:UpdatePanel ID="updldlPassportDoc" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DataList ID="dlVisaDoc" runat="server" Width="100%" DataKeyField="Node" GridLines="Horizontal"
                                                                    OnItemCommand="dlVisaDoc_ItemCommand">
                                                                    <HeaderStyle CssClass="datalistheader" />
                                                                    <HeaderTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="170">
                                                                                    <%-- Document Name--%>
                                                                                    <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                                                </td>
                                                                                <td width="150" align="left">
                                                                                    <%--File Name--%>
                                                                                    <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="170">
                                                                                    <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="170px"></asp:Label>
                                                                                </td>
                                                                                <td width="150" align="left">
                                                                                    <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                                                                </td>
                                                                                <td width="25" align="left">
                                                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"
                                                                                        CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                <tr align="right">
                                    <td>
                                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Submit%>'
                                            Text='<%$Resources:ControlsCommon,Submit%>' CommandName="SUBMIT" ValidationGroup="Employee"
                                            OnClick="btnSubmit_Click" />
                                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                                            Text='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="false" OnClick="btnCancel_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divSort" runat="server" style="padding-left: 379px; display: block">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSort" runat="server" meta:resourcekey="SortBy"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExpression" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlExpression_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1" meta:resourcekey="EmployeeNumber"></asp:ListItem>
                                            <asp:ListItem Value="2" meta:resourcekey="EmployeeName"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOrder" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1" meta:resourcekey="Ascending"></asp:ListItem>
                                            <asp:ListItem Value="2" meta:resourcekey="Descending"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:DataList ID="dlEmployeevisa" runat="server" BorderWidth="0px" CellPadding="0"
                            CssClass="labeltext" Width="100%" OnItemCommand="dlEmployeevisa_ItemCommand"
                            DataKeyField="VisaID" OnItemDataBound="dlEmployeevisa_ItemDataBound">
                            <ItemStyle CssClass="labeltext" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 5px;">
                                    <tr>
                                        <td align="left" height="30" width="30">
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkEmployee');" />
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <%--Select All--%>
                                            <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; margin-left: 3px;" id="tblDetails"
                                    runat="server">
                                    <%--onmouseover="showEdit(this);" onmouseout="hideEdit(this);"--%>
                                    <tr>
                                        <td valign="top" width="30" style="padding-top: 5px">
                                            <asp:CheckBox ID="chkEmployee" runat="server" />
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEmployee" CausesValidation="false" runat="server" CommandArgument='<%# Eval("VisaID")%>'
                                                                        CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("EmployeeFullName")) %>'
                                                                        CssClass="listHeader bold"></asp:LinkButton>
                                                                    [<%# Eval("EmployeeNumber")%>] &nbsp
                                                                    <%--Visa No. --%>
                                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="VisaNumber"></asp:Literal>
                                                                    -
                                                                    <%# Eval("VisaNumber")%>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                        display: block;" CommandArgument='<%# Eval("VisaID")%>' CommandName="ALTER" ImageUrl="~/images/edit.png"
                                                                        ToolTip="Edit"></asp:ImageButton>
                                                                </td>
                                                                <td style="visibility: hidden">
                                                                    <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="padding-left: 20px">
                                                        <table width="100%">
                                                            <tr>
                                                                <td class="innerdivheader">
                                                                    <%--Place of Issue--%>
                                                                    <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:DocumentsCommon,PlaceOfIssue%>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("PlaceofIssue")%>
                                                                </td>
                                                                <td style="color: #FF0000; font-weight: bold; float: right;">
                                                                    <%# Eval("IsRenewed")%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader">
                                                                    <%--Country--%>
                                                                    <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# Eval("Country")%>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader">
                                                                    <%-- Visa IssueDate--%>
                                                                    <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    :
                                                                </td>
                                                                <td>
                                                                    <%# GetDate(Eval("VisaIssueDate"))%>&nbsp;
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:FormView ID="fvvisa" runat="server" BorderWidth="0px" CellPadding="0" Width="100%"
                            CellSpacing="0" DataKeyNames="VisaID,IsDelete">
                            <ItemTemplate>
                                <div id="mainwrap">
                                    <div id="main2" style="margin-top: 10px; margin-left: 10px">
                                        <div class="t22">
                                            <div class="ta22">
                                                <div class="maindivheader">
                                                    <div class="innerdivheader">
                                                        <%-- Employee Name--%><asp:Literal ID="Literal16" runat="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%--Visa Type--%>
                                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="VisaType"></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%-- Visa Number--%>
                                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="VisaNumber"></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%-- Place of Issue--%><asp:Literal ID="Literal18" runat="server" Text='<%$Resources:DocumentsCommon,PlaceOfIssue%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%--  Issue Date--%><asp:Literal ID="Literal19" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%--Expiry Date--%><asp:Literal ID="Literal20" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%-- Is Renewed--%><asp:Literal ID="Literal21" runat="server" Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%--Country--%><asp:Literal ID="Literal22" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader">
                                                        <%-- Other Info--%><asp:Literal ID="Literal23" runat="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader" style="visibility: hidden">
                                                        <%--  Is Delete Possible--%><asp:Literal ID="Literal24" runat="server" meta:resourcekey="IsDeletePossible"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ta3">
                                                <div class="maindivheaderValue">
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("EmployeeFullName")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("VisaType")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("VisaNumber")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("PlaceofIssue")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# GetDate(Eval("VisaIssueDate"))%>
                                                    </div>
                                                    <div class="innerdivheaderValue">
                                                        <%# GetDate(Eval("VisaExpiryDate"))%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("IsRenewed")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("Country")%></div>
                                                    <div class="innerdivheaderValue" style="word-break: break-all;">
                                                        <%# Eval("Remarks")%></div>
                                                    <div class="innerdivheaderValue" style="visibility: hidden">
                                                        <%# Eval("IsDelete")%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                        <uc:Pager ID="pgrEmployees" runat="server" OnFill="Bind" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fuVisa" EventName="UploadedComplete" />
                    </Triggers>
                </asp:UpdatePanel>
                <div style="text-align: center; margin-top: 10px;">
                    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlEmployeeVisa" EventName="ItemCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="upPrint" runat="server">
                    <ContentTemplate>
                        <div id="divPrint" runat="server" style="display: none">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnIssueReceipt" runat="server" />
                        </div>
                        <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                            <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                            </uc1:DocumentReceiptIssue>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                            PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:ControlsCommon,SearchBy%>'
                    WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="ImgSearch" OnClick="btnSearch_Click" ImageUrl="~/images/search.png"
                        runat="server" />
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img src="../images/New_Visa_Large.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnNew" runat="server" CausesValidation="False" OnClick="btnNew_Click">
                            <%-- Add Visa--%><asp:Literal ID="Literal24" runat="server" meta:resourcekey="AddVisa"></asp:Literal>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <img src="../images/List_Visa_Large.png" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnList" runat="server" OnClick="btnList_Click" CausesValidation="False">
                            <%--View Visa--%><asp:Literal ID="Literal25" runat="server" meta:resourcekey="ViewVisa"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'> 
                           <%--Delete--%>
                    </asp:LinkButton>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="btnPrint" runat="server" OnClick="btnPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'>
                         
                         <%--  Print--%>
                        </asp:LinkButton>
                    </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <asp:LinkButton ID="btnEmail" runat="server" OnClick="btnEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'>
                         
                           <%-- Email--%>
                    </asp:LinkButton>
                </div>
                <div id="divIssueReceipt" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div id="Div2" class="name" runat="server">
                        <asp:LinkButton ID="lnkDocumentIR" runat="server" OnClick="lnkDocumentIR_Click" Text='<%$Resources:DocumentsCommon,Receipt%>'>
                             
                               <%-- Receipt--%>
                        </asp:LinkButton>
                    </div>
                </div>
                <div id="divRenew" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div id="Div3" class="name" runat="server">
                        <asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click" Text='<%$Resources:DocumentsCommon,Renew%>'>
                            
                                <%--Renew--%>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
