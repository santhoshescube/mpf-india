﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_RequestSettings : System.Web.UI.Page
{
    clsRequestSettings objRequestSettings;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaser;
   
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Title = GetLocalResourceObject("RequestSettingsTitle.Title").ToString();

        if (!IsPostBack)
        {
            clsUserMaster objUser = new clsUserMaster();

            clsRoleSettings objRoleSettings = new clsRoleSettings();
            int RoleId = objUser.GetRoleId();

            DataTable dt = objRoleSettings.GetPermissions(RoleId, (int)eMenuID.RequestSettingHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;

           
            SetPermission(RoleId);          
        }
    }
    public void SetPermission(int RoleID)
    {
     
        if (RoleID != 1 && RoleID != 2 && RoleID != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {

                if (dtm.Rows[0]["IsView"].ToBoolean() == true || dtm.Rows[0]["IsCreate"].ToBoolean() == true || dtm.Rows[0]["IsUpdate"].ToBoolean() == true || dtm.Rows[0]["IsDelete"].ToBoolean() == true)
                {
                    tblPermission.Visible = true;
                    if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                        btnSubmit.Enabled = true;
                    else
                        btnSubmit.Enabled = false;
                    FillCompany();
                    FillRequestTypes();
                   
                }
                else
                {
                    tblPermission.Visible = false;
                    lblPermission.Text = "You dont have enough permission to view this page.";
                    lblPermission.Visible = true;
                }
            }
            else
            {
                
                lblPermission.Text = "You dont have enough permission to view this page.";
                lblPermission.Visible = true;
            }
        }
        else
        {  
            FillCompany();
            FillRequestTypes();
          
     
        }

    }

    private void FillCompany()
    {

        objRequestSettings = new clsRequestSettings();
        objUserMaser = new clsUserMaster();

        objRequestSettings.CompanyID = objUserMaser.GetCompanyId();

        ddlCompany.DataSource = objRequestSettings.GetAllCompany();
        ddlCompany.DataBind();

    }
    private void FillRequestTypes()
    {
        objRequestSettings = new clsRequestSettings();
        objUserMaser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        ddlRequestType.DataSource = objRequestSettings.GetAllRequestTypes();
        ddlRequestType.DataBind();

        ddlRequestType_SelectedIndexChanged(null, null);

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (chkIsLevelNeeded.Checked && txtOfferLevel.Text.ToInt32()  == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), Guid.NewGuid().ToString(), "alert('Please enter levels')", true);
            return;
        }
        int k = 1;
        objRequestSettings = new clsRequestSettings();

        if (dlRequestSettings.Items.Count > 0)
        {            
            for (int i = 0; i < dlRequestSettings.Items.Count; i++)
            {
                DropDownList ddlAuthority = (DropDownList)dlRequestSettings.Items[i].FindControl("ddlAuthority");
                if (dlRequestSettings.Items.Count == 1 && ddlAuthority.SelectedValue.ToInt32() == 0)
                {
                    mcMessage.InformationalMessage("Cannot Save Settings as no employee exists");
                    mpeMessage.Show();
                    return;
                }
                for (int j = i + 1; j < dlRequestSettings.Items.Count; j++)
                {                   
                    DropDownList ddlAuthority1 = (DropDownList)dlRequestSettings.Items[j].FindControl("ddlAuthority");

                    if (ddlAuthority.SelectedValue == ddlAuthority1.SelectedValue)
                    {

                        mcMessage.InformationalMessage("Duplicate Authority not allowed");
                        mpeMessage.Show();
                        return;
                    }
                }
            }
           
        }

            objRequestSettings.RequestTypeId = ddlRequestType.SelectedValue.ToInt32();
            objRequestSettings.ReportingToRequired = chkReportingTo.Checked;
            objRequestSettings.HODRequired = chkHOD.Checked;
            objRequestSettings.IsLevelNeeded = chkIsLevelNeeded.Checked;
            objRequestSettings.IsLevelBasedForwarding = chlLevelForwarded.Checked;
            objRequestSettings.NoOfLevel = txtOfferLevel.Text.ToInt32();
            objRequestSettings.CompanyID = ddlCompany.SelectedValue.ToInt32();

            System.Collections.Generic.List<clsAuthorisedLevels> AuthorisedLevels = new System.Collections.Generic.List<clsAuthorisedLevels>();
           
            foreach (DataListItem dl in dlRequestSettings.Items)
            {
                if (dl.ItemType == ListItemType.AlternatingItem || dl.ItemType == ListItemType.Item)
                {
                    DropDownList ddlAuthority = (DropDownList)dl.FindControl("ddlAuthority");

                    if (ddlAuthority != null && ddlAuthority.SelectedValue.ToInt32() > 0)
                    {
                        AuthorisedLevels.Add(new clsAuthorisedLevels ()
                        {
                            EmployeeID  = Convert.ToInt32(ddlAuthority.SelectedValue),
                            LevelID =  k++
                          
                        });

                    }
                }
            }
            objRequestSettings.AuthorisedLevels = AuthorisedLevels;

            if (objRequestSettings.InsertApprovalAuthority() > 0)
            {
                mcMessage.InformationalMessage("Saved Successfully");
            }
            else
            {
                mcMessage.InformationalMessage("Failed");
            }
            mpeMessage.Show();

    }

    private void GetRequestSettings()
    {
        objRequestSettings = new clsRequestSettings();
        objUserMaser = new clsUserMaster();
        objRequestSettings.RequestTypeId = ddlRequestType.SelectedValue.ToInt32();
        objRequestSettings.CompanyID = ddlCompany.SelectedValue.ToInt32();

        ViewState["NoOflevels"] = 0;
        DataSet ds = objRequestSettings.GetRequestApprovals();
        if (ds.Tables[0].Rows.Count > 0)
        {
            chkReportingTo.Checked = (ds.Tables[0].Rows[0]["ReportingToRequired"].ToString() == "") ? false : Convert.ToBoolean(ds.Tables[0].Rows[0]["ReportingToRequired"].ToString());// Convert.ToBoolean(ds.Tables[0].Rows[0]["ReportingToRequired"]);
            chkHOD.Checked = (ds.Tables[0].Rows[0]["HODRequired"].ToString() == "") ? false : Convert.ToBoolean(ds.Tables[0].Rows[0]["HODRequired"].ToString());
            chkIsLevelNeeded.Checked = (ds.Tables[0].Rows[0]["IsLevelNeeded"].ToString() == "") ? false : Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLevelNeeded"].ToString()); //Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLevelNeeded"]);



            chlLevelForwarded.Checked = (ds.Tables[0].Rows[0]["IsLevelBasedForwarding"].ToString() == "") ? false : Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLevelBasedForwarding"].ToString()); //Convert.ToBoolean(ds.Tables[0].Rows[0]["IsLevelBasedForwarding"]);
            txtOfferLevel.Text = Convert.ToString(ds.Tables[0].Rows[0]["NoOfLevel"]);
            ViewState["NoOflevels"] = ds.Tables[0].Rows[0]["NoOfLevel"].ToInt32();
            DataTable dtLevels = ds.Tables[1];
            if (dtLevels.Rows.Count > 0)
            {
                dlRequestSettings.DataSource = dtLevels;
                dlRequestSettings.DataBind();
                chkEmployees.Visible = true;

                for (int i = 0; i < dlRequestSettings.Items.Count; i++)
                {
                    DropDownList ddlAuthority = (DropDownList)dlRequestSettings.Items[i].FindControl("ddlAuthority");
                    Label lblLevel = (Label)dlRequestSettings.Items[i].FindControl("lblLevel");
                    lblLevel.Text = "Level" + (i + 1).ToString();
                    if (ddlAuthority != null)
                    {
                        objRequestSettings.CompanyID = objUserMaser.GetCompanyId();
                        if (dtLevels.Rows[i]["EmployeeID"].ToInt32() > 0)
                        {
                            objRequestSettings.EmployeeID = dtLevels.Rows[i]["EmployeeID"].ToInt32();
                        }
                        else
                            objRequestSettings.EmployeeID = 0;

                        if (chkEmployees.Checked)
                            objRequestSettings.UserID = objUserMaser.GetUserId();
                        else
                            objRequestSettings.UserID = 0;

                        ddlAuthority.DataSource = objRequestSettings.GetAllAuthorisedEmployees();
                        ddlAuthority.DataBind();

                        if (ddlAuthority.Items.Count > 0)
                        {
                            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(dtLevels.Rows[i]["EmployeeID"].ToString())); ;
                        }
                    }
                }
                ViewState["Level"] = dtLevels;


            }
            else
            {
                dlRequestSettings.DataSource = null;
                dlRequestSettings.DataBind();
                chkEmployees.Visible = false;
            }
        }
        else
        {

            chkReportingTo.Checked = chkHOD.Checked = chlLevelForwarded.Checked = chkIsLevelNeeded.Checked = false;
            txtOfferLevel.Text = string.Empty;
            txtOfferLevel.Enabled = chlLevelForwarded.Enabled =imgShowLevels.Enabled =   false;
            dlRequestSettings.DataSource = null;
            dlRequestSettings.DataBind();
            chkEmployees.Visible = false;
        }
    }
    protected void ddlRequestType_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetRequestSettings();
        chkIsLevelNeeded_CheckedChanged(new EventArgs(), e);
    }


    protected void txtOfferLevel_OnTextChanged(object sender, EventArgs e)
    {
        objRequestSettings = new clsRequestSettings(); 
        if (txtOfferLevel.Text.ToInt32() <= 5 && txtOfferLevel.Text.ToInt32() >= 1)
        {
            int NoOfLevels = 0;
            if (txtOfferLevel.Text.ToInt32() < ViewState["NoOflevels"].ToInt32())
            {
                dlRequestSettings.DataSource = null;
                dlRequestSettings.DataBind();
                NoOfLevels = txtOfferLevel.Text.ToInt32();
            }
            else
            {
                NoOfLevels = txtOfferLevel.Text.ToInt32() - ViewState["NoOflevels"].ToInt32();
            }

            ViewState["Level"] = null;
            DataTable dt = new DataTable();

            dt.Columns.Add("EmployeeID");
            dt.Columns.Add("Level");
            dt.Columns.Add("LevelId");

            DataRow dw;
            int j = 0;
            int EmployeeID = 0;
            foreach (DataListItem item in dlRequestSettings.Items)
            {
                j = j + 1;
                DropDownList ddlAuthority = (DropDownList)item.FindControl("ddlAuthority");
                Label lblLevel = (Label)item.FindControl("lblLevel");
                dw = dt.NewRow();
                dw["EmployeeID"] = EmployeeID = Convert.ToInt32(ddlAuthority.SelectedValue == "" ? "0" : ddlAuthority.SelectedValue);
                dw["Level"] = "Level" + (j ).ToString();
                dw["LevelId"] = j;
                dt.Rows.Add(dw);
                //lblLevel.Text = "Level" + (j +1).ToString();
                
            }
            ViewState["Level"] = dt;

            DataTable dtNew = (DataTable)ViewState["Level"];

            if (dtNew == null)
            {
                dtNew = new DataTable();

                dtNew.Columns.Add("EmployeeID");
                dtNew.Columns.Add("Level");
                dtNew.Columns.Add("LevelId");
            }

            DataRow dwNew;

            for (int i = 0; i < NoOfLevels; i++)
            {
                dwNew = dtNew.NewRow();
                j = j + 1;
                dwNew["EmployeeID"] = -1;
                dwNew["Level"] = "Level" + j;
                dwNew["LevelId"] = j;
                //dt.Rows.Add(dt.NewRow());
                dt.Rows.Add(dwNew);
                
            }

            dlRequestSettings.DataSource = dtNew;
            dlRequestSettings.DataBind();
            chkEmployees.Visible = true;

           
            ViewState["NoOflevels"] = txtOfferLevel.Text.ToInt32();

        }
        else
        {           
            string msg = string.Empty;
            msg = "Authorization levels Should be with in the range 1-5";
            mcMessage.InformationalMessage(msg);
            //mcMessage.InformationalMessage("authorization levels Should be with in the range 1-5");
            mpeMessage.Show();
            return;
        }
        chkEmployees_CheckedChanged(new EventArgs(), e);
    }
    protected void dlRequestSettings_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objRequestSettings  = new clsRequestSettings ();
        switch (e.CommandName)
        {
            case "REMOVE_LEVEL":

                DataTable dt = new DataTable();

                dt.Columns.Add("EmployeeID");
                dt.Columns.Add("Level");
                dt.Columns.Add("LevelId");
               

                DataRow dw;

                foreach (DataListItem item in dlRequestSettings.Items)
                {
                    DropDownList ddlAuthority = (DropDownList)item.FindControl("ddlAuthority");
                    Label lblLevel = (Label)item.FindControl("lblLevel");

                    dw = dt.NewRow();

                    dw["LevelID"] = e.CommandArgument.ToInt32();
                    dw["Level"] = Convert.ToString(lblLevel.Text.Trim());
                    dw["EmployeeID"] = Convert.ToInt32(ddlAuthority.SelectedValue == "" ?"0" : ddlAuthority.SelectedValue);

                    if (item != e.Item)
                    {
                        dt.Rows.Add(dw);
                    }
                    ViewState["Level"] = dt;
                }
                dlRequestSettings.DataSource = ViewState["Level"];
                dlRequestSettings.DataBind();

                txtOfferLevel.Text =Convert.ToString( dt.Rows.Count);
                updLevels.Update();
                ViewState["NoOflevels"] = dt.Rows.Count;
                chkEmployees_CheckedChanged(new EventArgs(), e);
                break;
        }
    }

    protected void dlRequestSettings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objRequestSettings = new clsRequestSettings();

        DropDownList ddlAuthority = (DropDownList)e.Item.FindControl("ddlAuthority");
        if (ddlAuthority == null) return;
        HiddenField hfEmpID = (HiddenField)e.Item.FindControl("hfEmpID");
        ddlAuthority.DataSource = objRequestSettings.GetAllAuthorisedEmployees();
        ddlAuthority.DataBind();

        if (Convert.ToString(hfEmpID.Value) != "")
        {
            ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(Convert.ToString(hfEmpID.Value)));
        }
    }
    protected void chkIsLevelNeeded_CheckedChanged(object sender, EventArgs e)
    {
        if (chkIsLevelNeeded.Checked)
        {
            chlLevelForwarded.Enabled = txtOfferLevel.Enabled = imgShowLevels.Enabled =true;

        }
        else
        {
            chlLevelForwarded.Checked = false;
            txtOfferLevel.Text = string.Empty;
            dlRequestSettings.DataSource = null;
            dlRequestSettings.DataBind();
            chlLevelForwarded.Enabled = txtOfferLevel.Enabled =imgShowLevels.Enabled = false;
            ViewState["NoOflevels"] = 0;
        }
        updIsLevelNeeded.Update();
    }
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetRequestSettings();
        chkIsLevelNeeded_CheckedChanged(new EventArgs(), e);
    }
    protected void chkEmployees_CheckedChanged(object sender, EventArgs e)
    {
            DataTable dt  =  (DataTable) ViewState["Level"];
            if (dt.Rows.Count > 0)
            {
                dlRequestSettings.DataSource = dt;
                dlRequestSettings.DataBind();
                chkEmployees.Visible = true;
                objUserMaser = new clsUserMaster();
                for (int i = 0; i < dlRequestSettings.Items.Count; i++)
                {
                    DropDownList ddlAuthority = (DropDownList)dlRequestSettings.Items[i].FindControl("ddlAuthority");
                    Label lblLevel = (Label)dlRequestSettings.Items[i].FindControl("lblLevel");
                    lblLevel.Text = "Level" + (i + 1).ToString();
                    if (ddlAuthority != null)
                    {
                       
                            objRequestSettings.CompanyID = objUserMaser.GetCompanyId();
                            if (dt.Rows[i]["EmployeeID"].ToInt32() > 0)
                            { 
                               // objRequestSettings.UserID = objUserMaser.GetUserId();
                                objRequestSettings.EmployeeID = dt.Rows[i]["EmployeeID"].ToInt32();
                            }
                            else
                                objRequestSettings.EmployeeID = 0;

                            if (chkEmployees.Checked)
                                objRequestSettings.UserID = objUserMaser.GetUserId();
                            else
                                objRequestSettings.UserID = 0;

                          
                           
                            ddlAuthority.DataSource = objRequestSettings.GetAllAuthorisedEmployees();
                            ddlAuthority.DataBind();

                            if (ddlAuthority.Items.Count > 0)
                            {
                                ddlAuthority.SelectedIndex = ddlAuthority.Items.IndexOf(ddlAuthority.Items.FindByValue(dt.Rows[i]["EmployeeID"].ToString())); ;
                            }
                        
                    }
                }
            }
       
    }

    protected void imgShowLevels_Click(object sender, EventArgs e)
    {
        txtOfferLevel_OnTextChanged(txtOfferLevel, null);
    }
}
