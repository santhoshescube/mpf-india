﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Globalization;
using System.Reflection;

/// <summary> 
/// Created By      :       Sreelekshmi
/// Created Date    :       26 -Dec-2013
/// Description     :       General Request
/// </summary>
/// 

public partial class Public_GeneralRequest : System.Web.UI.Page
{

    clsGeneralRequest objRequest;
    clsUserMaster objUser;
    clsLeaveRequest objLeaveRequest;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        RequestPager.Fill += new controls_Pager.FillPager(BindDataList);


        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;

            txtFromDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            FillRequestType();

            int RequestID = 0;
            if (Request.QueryString["RequestId"] != null && Request.QueryString["Type"] == "Cancel")
            {
                ViewState["RequestID"] = RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["RequestForCancel"] = true;
                // fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;

            }
            else if (Request.QueryString["RequestId"] != null)
            {
                ViewState["RequestID"] = RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["Approve"] = true;

                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;


            }
            else
            {
                BindDataList();
            }

        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlType.SelectedValue.ToInt32())
        {
            case 9: // Resignation
                litDate.Text = GetLocalResourceObject("ResignationDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divFlightNumber.Style["display"] = "none";
                txtFlightNumber.Text = string.Empty;
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
               //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                objUser = new clsUserMaster();
                objRequest = new clsGeneralRequest();
                objRequest.EmployeeID =  objUser.GetEmployeeId();
                gdvPendingLoan.DataSource = objRequest.GetUnclosedLoan();
                gdvPendingLoan.DataBind();
                //gdvPendingLoan.Visible = true ;
                //lblUnclosedLoans.Visible = true;
                if (gdvPendingLoan.Rows.Count > 0)
                {
                    divLeft.Visible = true;
                    divRight.Visible = true;
                }

              
                break;
            case 16: // Ticket
                litDate.Text = GetLocalResourceObject("TicketDate.Text").ToString();
                divFlightNumber.Style["display"] = "block";
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;
                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 22://Asset
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "block";

                ddlAssetType.DataSource = clsGeneralRequest.FillAssetType();
                ddlAssetType.DataBind();
                ddlAssetType.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"), "-1"));

                divFlightNumber.Style["display"] = "none";
                txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none"; 
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;
                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 23://General
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 24://Salary Certificate
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
               // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 25://Salart Bank Statement
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 26://Salary Card Lost
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 27://Salary Slip
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 28://Visa Letter
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 29://Noc- Umarah Visa
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
            case 30://Card Lost
                litDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                litRemarks.Text = GetLocalResourceObject("Reason.Text").ToString();
                divAsset.Style["display"] = "none";
                ddlAssetType.Items.Clear();
                //gvAssetDetails.DataSource = null;
                //gvAssetDetails.DataBind();
                divFlightNumber.Style["display"] = "none";
                // txtFlightNumber.Text = string.Empty;
                //divLeave.Style["display"] = "none";
                divRejoinDate.Style["display"] = "none";
                //rfvl.ValidationGroup = "";
                rfvl2.ValidationGroup = "";
                //gdvPendingLoan.Visible = false;
                //lblUnclosedLoans.Visible = false;

                divLeft.Visible = false;
                divRight.Visible = false;
                break;
        }

    }
    protected void ddlAssetType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //objUser = new clsUserMaster();
        //gvAssetDetails.DataSource = clsGeneralRequest.GetAssetDetails(ddlAssetType.SelectedValue.ToInt32(), objUser.GetCompanyId());
        //gvAssetDetails.DataBind();
    }
    //protected void ddlLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    switch (ddlLeaveType.SelectedValue.ToInt32())
    //    {
    //        case 0:
    //            divRejoinDate.Style["display"] = "none";
    //            break;
    //        case 1:
    //            GetRejoinDate(1);
    //            break;
    //        case 2:
    //            GetRejoinDate(-1);
    //            break;
    //    }
    //}
    protected void txtRejoinDate_OnTextChanged(object sender, EventArgs e)
    {
        bool val = CheckRejoinDate();
    }

    private bool CheckRejoinDate()
    {
        bool val = true;
        if (ViewState["FDate"] != null && ViewState["TDate"] != null)
        {
            if (clsCommon.Convert2DateTime(txtRejoinDate.Text) < clsCommon.Convert2DateTime(ViewState["FDate"]))
            {
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("Rejoin Date cannot be earlier than leave From Date.") : ("Rejoin Date cannot be earlier than leave From Date.");
                msgs.InformationalMessage(message);
                mpeMessage.Show();
                val = false;
            }
            else val = true;
        } return val;
    }
    private void GetRejoinDate(int type)
    {
        objRequest = new clsGeneralRequest();
        DataTable dt = objRequest.GetRejoinDate(new clsUserMaster().GetEmployeeId(), type);
        if (dt.Rows.Count > 0)
        {
            string strDate = dt.Rows[0]["RejoinDate"].ToString();
            hdnID.Value = dt.Rows[0]["ID"].ToString();
            if (strDate.Trim() != string.Empty)
            {
                txtRejoinDate.Text = Convert.ToDateTime(strDate).ToString("dd/MM/yyyy");
                divRejoinDate.Style["display"] = "block";
                if (type == -1)
                    lblVacation.Text = "Vacation Leave Taken from " + Convert.ToDateTime(dt.Rows[0]["FromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dt.Rows[0]["ToDate"]).ToString("dd/MM/yyyy");
                else
                    lblVacation.Text = "Sick Leave Taken from " + Convert.ToDateTime(dt.Rows[0]["FromDate"].ToString()).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dt.Rows[0]["ToDate"].ToString()).ToString("dd/MM/yyyy");
                ViewState["FDate"] = Convert.ToDateTime(dt.Rows[0]["FromDate"]).ToString("dd/MM/yyyy");
                ViewState["TDate"] = Convert.ToDateTime(dt.Rows[0]["ToDate"]).ToString("dd/MM/yyyy");
            }
            else
            {
                txtRejoinDate.Text = lblVacation.Text = "";
                divRejoinDate.Style["display"] = "none";
            }
        }
        else
        {
            txtRejoinDate.Text = lblVacation.Text = "";
            divRejoinDate.Style["display"] = "none";
        }
    }
    private void FillRequestType()
    {
        ddlType.DataSource = clsGeneralRequest.FillRequestType();
        ddlType.DataBind();
        ddlType.Items.Insert(0, new ListItem(clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"), "-1"));

    }
    private void RemoveQueryString(string Query)
    {
        PropertyInfo isReadOnly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isReadOnly.SetValue(this.Request.QueryString, false, null);

        Request.QueryString.Remove(Query);

        Response.ClearContent();
    }

    private void BindDataList()
    {
        setDivVisibility(2);
        objRequest = new clsGeneralRequest();
        objUser = new clsUserMaster();
        objRequest.EmployeeID = objUser.GetEmployeeId();
        objRequest.Mode = "VIEW";
        objRequest.PageIndex = RequestPager.CurrentPage + 1;
        objRequest.PageSize = RequestPager.PageSize;
        DataTable oTable = objRequest.GetRequests();
        if (oTable.DefaultView.Count > 0)
        {
            lnkView.Enabled = true;
            dlRequest.DataSource = oTable;
            dlRequest.DataBind();

            RequestPager.Total = objRequest.GetRecordCount();
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlRequest.ClientID + "');";

            lnkDelete.Enabled = true;
            RequestPager.Visible = true;

        }
        else
        {
            lnkView.Enabled = false;
            dlRequest.DataSource = null;
            dlRequest.DataBind();
            lnkDelete.Enabled = false;
            // fvSalaryDetails.ChangeMode(FormViewMode.Insert);
            setDivVisibility(1);
            RequestPager.Visible = false;
        }
        upViewRequest.Update();
        upMenu.Update();
    }

    private void BindRequestDetails(int RequestID)
    {


        objRequest = new clsGeneralRequest();
        objRequest.Mode = "EDT";
        ViewState["RequestID"] = objRequest.RequestID = RequestID;

        DataTable dtSingleView = objRequest.GetRequestDetails();

        hfRequestID.Value = Convert.ToString(dtSingleView.Rows[0]["RequestID"]);
        hfReqTypeID.Value = Convert.ToString(dtSingleView.Rows[0]["RequestTypeID"]);
        ddlType.SelectedIndex = ddlType.Items.IndexOf(ddlType.Items.FindByValue(Convert.ToString(dtSingleView.Rows[0]["RequestTypeID"])));
        ddlType_SelectedIndexChanged(null, null);
        ddlType.Enabled = false;
        ddlAssetType.SelectedIndex = ddlAssetType.Items.IndexOf(ddlAssetType.Items.FindByValue(Convert.ToString(dtSingleView.Rows[0]["BenefitTypeID"])));
        ddlAssetType.Enabled = false;
        //ddlAssetType_SelectedIndexChanged(null, null);
        txtFlightNumber.Text = Convert.ToString(dtSingleView.Rows[0]["FlightNumber"]);
        txtFromDate.Text = dtSingleView.Rows[0]["FromDate"].ToStringCustom();
        txtRemarks.Text = Convert.ToString(dtSingleView.Rows[0]["Reason"]);
        dlRequest.DataSource = null;
        dlRequest.DataBind();
        //ddlLeaveType.Enabled = txtRejoinDate.Enabled = ibtnRejoin.Enabled = false;
        hdnID.Value = Convert.ToString(dtSingleView.Rows[0]["LeaveReferenceID"]);
        //if (Convert.ToInt32(dtSingleView.Rows[0]["LeaveTypeID"]) == -1)
        //    ddlLeaveType.SelectedValue = (Convert.ToString(dtSingleView.Rows[0]["LeaveTypeID"]) == "-1") ? "2" : Convert.ToString(dtSingleView.Rows[0]["LeaveTypeID"]);
        //else
            //ddlLeaveType.SelectedValue = Convert.ToString(ddlLeaveType.Items.IndexOf(ddlLeaveType.Items.FindByValue(Convert.ToString(dtSingleView.Rows[0]["LeaveTypeID"]))));
        txtRejoinDate.Text = (dtSingleView.Rows[0]["RejoinDate"].ToString() != "") ? Convert.ToDateTime(dtSingleView.Rows[0]["RejoinDate"]).ToString("dd/MM/yyyy") : "";
        if (txtRejoinDate.Text != "" && hfReqTypeID.Value.ToInt32() == 24)
        {
            divRejoinDate.Style["display"] = "block";
            if (Convert.ToInt32(dtSingleView.Rows[0]["LeaveTypeID"]) == -1)
                lblVacation.Text = "Vacation Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            else
                lblVacation.Text = "Sick Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            ViewState["FDate"] = Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"].ToString()).ToString("dd/MM/yyyy");
            ViewState["TDate"] = Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"].ToString()).ToString("dd/MM/yyyy");
        }
        else { lblVacation.Text = ""; divRejoinDate.Style["display"] = "none"; }
        RequestPager.Visible = false;
    }

    private void ClearControls()
    {
        ddlType.SelectedIndex = -1;
        //ddlLeaveType.SelectedValue = "0";
        hfRequestID.Value = string.Empty;
        txtRemarks.Text = txtFlightNumber.Text = string.Empty;
        divAsset.Style["display"] = "none";
        //divLeave.Style["display"] = "none";
        divRejoinDate.Style["display"] = "none";
        ddlType.Enabled = ddlAssetType.Enabled =  txtRejoinDate.Enabled = ibtnRejoin.Enabled = true;
        //ddlLeaveType.Enabled = true;
        hfRequestID.Value = "0";
        ViewState["FDate"] = ViewState["TDate"] = lblVacation.Text = "";
    }

    private void BindSingleView(int RequestID)
    {
        objRequest = new clsGeneralRequest();
        objUser = new clsUserMaster();
        objRequest.Mode = "EDT";
        objRequest.RequestID = RequestID;
        int RequestTypeID = 0;
        DataTable dtSingleView = objRequest.GetRequestDetails();
        dlRequest.DataSource = null;
        dlRequest.DataBind();

        hfRequestID.Value = RequestID.ToString();

        if (dtSingleView.Rows.Count > 0)
        {
            RequestTypeID = Convert.ToInt32(dtSingleView.Rows[0]["RequestTypeID"]);

            switch (RequestTypeID)
            {
                case 9: // Resignation
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 16: // Ticket
                    divFlightSingleView.Style["display"] = "block";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;

                case 22: // Asset
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "block";
                    divRejoinSingleView.Style["display"] = "none";
                    break;

                case 23: // General
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 24: //Salary Certificate
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 25: //Salary Bank Statement
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 26: //Salary Card Lost
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 27: //Salary Slip
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 28: //Visa Letter
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 29: //Noc- Umarah Visa
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
                case 30: //Card Lost
                    divFlightSingleView.Style["display"] = "none";
                    divAssetSingleView.Style["display"] = "none";
                    divRejoinSingleView.Style["display"] = "none";
                    break;
            }

            lblRequestedBy.Text = Convert.ToString(dtSingleView.Rows[0]["RequestedBY"]);
            lblType.Text = Convert.ToString(dtSingleView.Rows[0]["RequestType"]);
            lblDate.Text = dtSingleView.Rows[0]["FromDate"].ToStringCustom();
            lblRequestedToNames.Text = Convert.ToString(dtSingleView.Rows[0]["Requested"]);
            lblStatus.Text = Convert.ToString(dtSingleView.Rows[0]["Status"]);
            lblRemarks.Text = Convert.ToString(dtSingleView.Rows[0]["Reason"]);
            lblFlightNo.Text = Convert.ToString(dtSingleView.Rows[0]["FlightNumber"]);
            lblAssetType.Text = Convert.ToString(dtSingleView.Rows[0]["BenefitTypeName"]);
            objRequest.BenefitTypeID = Convert.ToInt32(dtSingleView.Rows[0]["RequestTypeid"]);
            //if (objRequest.BenefitTypeID.ToInt32() == 24)
            //{
            //    objRequest.LeaveReferenceID = (dtSingleView.Rows[0]["LeaveReferenceID"].ToInt32() > 0) ? Convert.ToInt32(dtSingleView.Rows[0]["LeaveReferenceID"]) : 0;
            //    lblRejoinDate.Text = (dtSingleView.Rows[0]["RejoinDate"].ToString() != "") ? Convert.ToDateTime(dtSingleView.Rows[0]["RejoinDate"]).ToString("dd/MM/yyyy") : "";
            //    lblLeaveType.Text = (dtSingleView.Rows[0]["LeaveType"].ToString() != "") ? Convert.ToString(dtSingleView.Rows[0]["LeaveType"]) : "";

            //    if (Convert.ToInt32(dtSingleView.Rows[0]["LeaveTypeID"]) == -1)
            //        lblVacationView.Text = "Vacation Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            //    else
            //        lblVacationView.Text = "Sick Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            //    ViewState["FDate"] = Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy");
            //    ViewState["TDate"] = Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            //}
            //else lblVacationView.Text = "";
            //gvAssetDisp.DataSource = clsGeneralRequest.GetAssetDetails(Convert.ToInt32(dtSingleView.Rows[0]["BenefitTypeid"]), objUser.GetCompanyId());
            //gvAssetDisp.DataBind();
            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
                ibtnBack.Visible = true;
            }
            else
            {
                ViewState["RequestID"] = objRequest.RequestID;
                ibtnBack.Visible = false;
            }
        }
        objRequest.CompanyID = objRequest.GetCompanyID();
        DataTable dt2 = objRequest.GetApprovalHierarchy();
        if (dt2.Rows.Count > 0)
        {
            dl1.DataSource = dt2;
            dl1.DataBind();
        }
    }

    private void BindForActions(int RequestID)
    {

        objRequest = new clsGeneralRequest();

        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();
        int EmployeeID = objUser.GetEmployeeId();
        setDivVisibility(4);
        objRequest.RequestID = RequestID;
        objRequest.Mode = "EDT";
        DataTable dtSingleView = objRequest.GetRequestDetails();

        int requestedById = dtSingleView.Rows[0]["EmployeeId"].ToInt32();
        if (dtSingleView.Rows.Count > 0)
        {
            hfTypeID.Value = Convert.ToString(dtSingleView.Rows[0]["RequestTypeID"]);

            switch (hfTypeID.Value.ToInt32())
            {
                case 9: // Resignation
                    litApDate.Text = GetLocalResourceObject("ResignationDate.Text").ToString();
                    litApReason.Text = GetLocalResourceObject("Reason.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = true;
                    imgPending.Visible = true;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    //clsUserMaster objUser1 = new clsUserMaster();
                    //clsGeneralRequest objRequest1 = new clsGeneralRequest();
                    objRequest.EmployeeID = requestedById;
                    DataTable DtPndReq = objRequest.GetUnclosedLoan();
                    GridView1.DataSource = DtPndReq;
                    GridView1.DataBind();
                    if (GridView1.Rows.Count > 0)
                    {
                        divLeft1.Visible = true;
                        divRight1.Visible = true;
                    }
                    objRequest.EmployeeID = EmployeeID;
                    break;
                case 16: // Ticket
                    litApDate.Text = GetLocalResourceObject("TicketDate.Text").ToString();
                    divFlightApproval.Style["display"] = "block";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;

                case 22: // asset
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "block";
                    divLeaveApprove.Style["display"] = "none";
                    break;

                case 23: // general
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;

                case 24: //Salary Certificate
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 25: //Salary Bank Statement
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 26: //Salary Card Lost
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 27: //Salary Slip
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 28: //Visa Letter
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 29: //Noc- Umarah Visa
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
                case 30: //Card Lost
                    litApDate.Text = GetLocalResourceObject("RequestDate.Text").ToString();
                    divFlightApproval.Style["display"] = "none";
                    lnkShowDocs.Visible = false;
                    imgPending.Visible = false;
                    divAssetApprove.Style["display"] = "none";
                    divLeaveApprove.Style["display"] = "none";
                    break;
            }

            lblApproveRequestedBy.Text = Convert.ToString(dtSingleView.Rows[0]["RequestedBY"]);
            lblApproveType.Text = Convert.ToString(dtSingleView.Rows[0]["RequestType"]);
            lblApproveDate.Text = dtSingleView.Rows[0]["FromDate"].ToStringCustom();
            lblApReason.Text = dtSingleView.Rows[0]["Reason"].ToStringCustom();
            lblCurrentStatus.Text = Convert.ToString(dtSingleView.Rows[0]["Status"]);
            txtApproveRemarks.Text = dtSingleView.Rows[0]["Remarks"].ToStringCustom();
            lblApproveFlightNo.Text = Convert.ToString(dtSingleView.Rows[0]["FlightNumber"]);
            lblApproveAssetType.Text = Convert.ToString(dtSingleView.Rows[0]["BenefitTypeName"]);
            objRequest.BenefitTypeID = Convert.ToInt32(dtSingleView.Rows[0]["RequestTypeid"]);

            //if (objRequest.BenefitTypeID.ToInt32() == 24)
            //{
            //    objRequest.LeaveReferenceID = (dtSingleView.Rows[0]["LeaveReferenceID"].ToInt32() > 0) ? Convert.ToInt32(dtSingleView.Rows[0]["LeaveReferenceID"]) : 0;
            //    lblRejoinDate1.Text = (dtSingleView.Rows[0]["RejoinDate"].ToString() != "") ? Convert.ToDateTime(dtSingleView.Rows[0]["RejoinDate"]).ToString("dd/MM/yyyy") : "";
            //    lblLeaveType1.Text = (dtSingleView.Rows[0]["LeaveType"].ToString() != "") ? Convert.ToString(dtSingleView.Rows[0]["LeaveType"]) : "";

            //    if (Convert.ToInt32(dtSingleView.Rows[0]["LeaveTypeID"]) == -1)
            //        lblVacationApprove.Text = "Vacation Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            //    else
            //        lblVacationApprove.Text = "Sick Leave Taken from " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveFromDate"]).ToString("dd/MM/yyyy") + " - " + Convert.ToDateTime(dtSingleView.Rows[0]["LeaveToDate"]).ToString("dd/MM/yyyy");
            //}
            //else lblVacationApprove.Text = "";

            //gvApproveAssetDetails.DataSource = clsGeneralRequest.GetAssetDetails(Convert.ToInt32(dtSingleView.Rows[0]["BenefitTypeid"]), objUser.GetCompanyId());
            //gvApproveAssetDetails.DataBind();
        }
        if (Convert.ToBoolean(ViewState["Approve"]) == true) // Approval
        {
            divApproveTheRequest.Style["display"] = "block";
            divCancelTheRequest.Style["display"] = "none";
            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("5"));

            if (!clsLeaveRequest.IsHigherAuthority(hfTypeID.Value.ToInt32(), Convert.ToInt64(EmployeeID), RequestID))
            {
                ddlApproveStatus.Items.Remove(new ListItem("Pending", "4"));
            }

        }
        else if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            divApproveTheRequest.Style["display"] = "none";
            divCancelTheRequest.Style["display"] = "block";

            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("6"));
            ddlApproveStatus.Enabled = false;
        }

        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            ibtnBack.Visible =  true;
        }
        else
        {
            ViewState["RequestID"] = objRequest.RequestID;
            ibtnBack.Visible =  false;
        }
        objRequest.CompanyID = objRequest.GetCompanyID();
        DataTable dt2 = objRequest.GetApprovalHierarchy();
        if (dt2.Rows.Count > 0)
        {
            dl2.DataSource = dt2;
            dl2.DataBind();
        }
    }

    protected void dlRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_View":
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
                setDivVisibility(3);
                BindSingleView(Convert.ToInt32(e.CommandArgument));
                if (clsUserMaster.GetCulture() == "ar-AE")
                    lnkDelete.OnClientClick = "return confirm('هل أنت متأكد أنك تريد حذف؟');";
                else
                    lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;

                break;

            case "_Edit":

                //fvSalaryDetails.ChangeMode(FormViewMode.Edit);
                setDivVisibility(1);
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;

            case "_Cancel":
                Label lbRequested = (Label)e.Item.FindControl("lbRequested");
                HiddenField hfType = (HiddenField)e.Item.FindControl("hfType");

                RequestCancel(Convert.ToInt32(e.CommandArgument), lbRequested.Text, hfType.Value.ToInt32());
                //lnkDelete.Enabled = false;
                //lnkDelete.OnClientClick = "return false;";
                break;

        }
        upMenu.Update();
    }

    private void setDivVisibility(int intMode)
    {
        //Mode 1:-divAddRequest
        //Mode 2:-divViewRequest
        //Mode 3:-divSingleView
        //Mode 4:-divApproveRequest

        if (intMode == 1)
        {
            divAddRequest.Style["display"] = "block";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "none";
        }
        else if (intMode == 2)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "block";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "none";
        }
        else if (intMode == 3)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "block";
            divApproveRequest.Style["display"] = "none";
        }
        else if (intMode == 4)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "block";
        }

        upViewRequest.Update();
        upAddRequest.Update();
        upSingleView.Update();
        upApproveRequest.Update();
    }

    private void RequestCancel(int RequestId, string sRequestedIds, int RequestTypeID)
    {
        objUser = new clsUserMaster();
        objRequest = new clsGeneralRequest();

        int EmployeeID = objUser.GetEmployeeId();

        objRequest.RequestID = RequestId;
        objRequest.StatusID = Convert.ToInt32(RequestStatus.RequestForCancel);
        objRequest.Reason = " Request for Cancellation";
        objRequest.UpdateCancelStatus();
        switch (RequestTypeID)
        {
            case 9:// Resignation request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.ResignationRequest, eAction.RequestForCancel);
                break;
            case 16: // ticket request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.TicketRequest, eAction.RequestForCancel);
                break;
            case 22: // Asset request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.AssetRequest, eAction.RequestForCancel);
                break;
            case 23: // General request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.GeneralRequest, eAction.RequestForCancel);
                break;
            case 24: // Salary Certificate request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCertificate, eAction.RequestForCancel);
                break;
            case 25: // Salary Bank Statement request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.SalaryBankStatement, eAction.RequestForCancel);
                break;
            case 26: // Salary Card Lost request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCardLost, eAction.RequestForCancel);
                break;
            case 27: // Salary Slip request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.Salaryslip, eAction.RequestForCancel);
                break;
            case 28: // Visa Letter request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.VisaLetter, eAction.RequestForCancel);
                break;
            case 29: // Noc- Umarah Visa request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.NocUmarahVisa, eAction.RequestForCancel);
                break;
            case 30: // Card Lost request
                clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.RequestForCancel, "");
                clsCommonMail.SendMail(RequestId, MailRequestType.CardLost, eAction.RequestForCancel);
                break;

        }

        //--------------------------------------------------------------------------------------------------------------------------------------------------

        BindDataList();
        if (clsUserMaster.GetCulture() == "ar-AE")
            msgs.InformationalMessage("تم تحديث التفاصيل بنجاح.");
        else
            msgs.InformationalMessage("Successfully updated request details.");

        mpeMessage.Show();

    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ClearControls();
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;

        ViewState["RequestForCancel"] = false;

        //fvSalaryDetails.ChangeMode(FormViewMode.Insert);
        setDivVisibility(1);

        dlRequest.DataSource = null;
        dlRequest.DataBind();

        lnkDelete.Enabled = false;

        lnkDelete.OnClientClick = "return false;";
        RequestPager.Visible = false;
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        ViewState["Approve"] = false;
        ViewState["RequestCancel"] = false;
        ViewState["RequestForCancel"] = false;

        BindDataList();
    }

    protected void lnkShowDocs_Click(object sender, EventArgs e)
    {
        objRequest = new clsGeneralRequest();

        objRequest.RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);

        DataTable dt = clsGeneralRequest.GetPendingDocs(objRequest.GetRequestedById());
        if (dt.Rows.Count > 0)
        {
            dlDocs.DataSource = dt;
            dlDocs.DataBind();
        }
        else
        {
            dlDocs.DataSource = null;
            dlDocs.DataBind();
            lblMessage.Visible = true;
        }

        upnlPendingDocs.Update();
        mpeDocs.Show();
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;

        if (dlRequest.Items.Count > 0)
        {
            foreach (DataListItem item in dlRequest.Items)
            {
                CheckBox chkRequest = (CheckBox)item.FindControl("chkRequest");
                HiddenField hfType = (HiddenField)item.FindControl("hfType");
                if (chkRequest == null)
                    continue;
                if (chkRequest.Checked)
                {
                    if (DeleteRequest(Convert.ToInt32(dlRequest.DataKeys[item.ItemIndex]), hfType.Value.ToInt32()))
                        message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : ("Request(s) deleted successfully");
                    else
                        message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted.");
                }
            }

        }
        else
        {
            if (hfRequestID.Value.ToInt32() > 0)
            {
                if (DeleteRequest(Convert.ToInt32(hfRequestID.Value), hfReqTypeID.Value.ToInt32()))
                    message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : "Request deleted successfully";
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب لا يمكن حذفها.") : "Processing started request cannot be deleted.";
            }
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();


    }

    private bool DeleteRequest(int requestId, int ReqTypeID)
    {
        string message = string.Empty;

        objRequest = new clsGeneralRequest();
        objRequest.RequestID = requestId;

        if (objRequest.IsRequestApproved())
        {
            return false;
        }
        else
        {
            objRequest.Mode = "DEL";
            objRequest.DeleteRequest();

            switch (ReqTypeID)
            {
                case 9:     //Resignation Request
                    clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.ResignationRequest);
                    break;

                case 16:    //Ticket Request
                    clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.TicketRequest);
                    break;

                case 22:    //Asset Request
                    clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.AssetRequest);
                    break;

                case 23:    //General Request
                    clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.GeneralRequest);
                    break;

            }
            return true;
        }
    }

    protected void dlRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hfForwarded");
            HiddenField hdStatusID = (HiddenField)e.Item.FindControl("hfStatusID");
            Int32 intStatusID = hdStatusID.Value.ToInt32();
            bool blnForwared = hdForwarded.Value == "True" ? true : false;

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            if (intStatusID != (int)RequestStatus.Applied || blnForwared)
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton btnCancel = (ImageButton)e.Item.FindControl("btnCancel");
            btnCancel.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm(' هل تريد إلغاء هذا الطلب؟');") : ("return confirm('Do you want to cancel this request?');");
            if (intStatusID == (int)RequestStatus.Cancelled || intStatusID == (int)RequestStatus.RequestForCancel || intStatusID == (int)RequestStatus.Rejected || (intStatusID == (int)RequestStatus.Applied && !blnForwared))
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }
        }
    }

    public bool GetVisibility(object oForwardedBy)
    {
        if (Convert.ToString(oForwardedBy) != "")
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Crop the string if it exceeds the width of container 
    /// </summary>
    protected string Crop(string value, int length)
    {
        if (value.Length <= length)
            return value;
        else
            return value.Substring(0, length) + "&hellip;";
    }

    protected void btnApproveCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        objRequest = new clsGeneralRequest();
        objUser = new clsUserMaster();
        objLeaveRequest = new clsLeaveRequest();

        int EmployeeID = objUser.GetEmployeeId();

        string sToEmployees = string.Empty;

        string sRequestedIds = string.Empty;
        string sToMailId = string.Empty;
        int RequestedById = 0;
        int RequestId = 0;
        int RequestedTypeID = 0;

        if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.Resignation)
            RequestedTypeID = Convert.ToInt32(RequestType.Resignation);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.Ticket)
            RequestedTypeID = Convert.ToInt32(RequestType.Ticket);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.Asset)
            RequestedTypeID = Convert.ToInt32(RequestType.Asset);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.General)
            RequestedTypeID = Convert.ToInt32(RequestType.General);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.SalaryCertificate)
            RequestedTypeID = Convert.ToInt32(RequestType.SalaryCertificate);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.SalaryBankStatement)
            RequestedTypeID = Convert.ToInt32(RequestType.SalaryBankStatement);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.SalaryCardLost)
            RequestedTypeID = Convert.ToInt32(RequestType.SalaryCardLost);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.Salaryslip)
            RequestedTypeID = Convert.ToInt32(RequestType.Salaryslip);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.VisaLetter)
            RequestedTypeID = Convert.ToInt32(RequestType.VisaLetter);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.NocUmarahVisa)
            RequestedTypeID = Convert.ToInt32(RequestType.NocUmarahVisa);
        else if (ddlType.SelectedValue.ToInt32() == (int)eGeneralRequest.CardLost)
            RequestedTypeID = Convert.ToInt32(RequestType.CardLost);
        bool val = true;
        //if ((hfRequestID.Value.ToInt32() == 0 && hdnID.Value.ToInt32() > 0))
        //{
        //    if (Convert.ToInt32(objRequest.CheckLeaveExistance(Convert.ToInt32(hdnID.Value), Convert.ToInt32(ddlLeaveType.SelectedValue))) > 0)
        //    {
        //        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("Request Exists.") : ("Request Exists.");
        //        msgs.InformationalMessage(message);
        //        mpeMessage.Show();
        //        val = false;
        //    }
        //    val = CheckRejoinDate();

        //}
        if (val)
        {
            string RequestedTo = clsLeaveRequest.GetRequestedTo(hfRequestID.Value.ToInt32(), Convert.ToInt64(objUser.GetEmployeeId()), RequestedTypeID);
            if (RequestedTo == string.Empty)
            {
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في الوقت الحالي.يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
                msgs.InformationalMessage(message);
                mpeMessage.Show();
            }
            else if (txtRemarks.Text == "")
            {
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في الوقت الحالي.يرجى الاتصال بقسم الموارد البشرية") : ("Please Enter Reason");
                msgs.InformationalMessage(message);
                mpeMessage.Show();
            }
            else
            {
                bool bIsInsert = true;
                if (hfRequestID.Value.ToInt32() > 0)
                {
                    RequestId = hfRequestID.Value.ToInt32();
                }

                objRequest.EmployeeID = EmployeeID;
                if (RequestId == 0)
                {
                    //set values 
                    objRequest.Mode = "INS";
                }
                else
                {
                    objRequest.Mode = "UPT";
                    objRequest.RequestID = RequestId;
                    bIsInsert = false;
                }

                objRequest.RequestTypeId = ddlType.SelectedValue.ToInt32();
                objRequest.Reason = txtRemarks.Text;

                objRequest.RequestedTo = RequestedTo;
                objRequest.RequestFromDate = clsCommon.Convert2DateTime(txtFromDate.Text);
                objRequest.RequestToDate = clsCommon.Convert2DateTime(txtFromDate.Text);
                objRequest.StatusID = Convert.ToInt32(RequestStatus.Applied);
                objRequest.FlightNumber = txtFlightNumber.Text.Trim();
                objRequest.BenefitTypeID = ddlAssetType.SelectedValue.ToInt32();
                //objRequest.LeaveTypeID = (Convert.ToInt32(ddlLeaveType.SelectedValue) == 2) ? -1 : Convert.ToInt32(ddlLeaveType.SelectedValue);
                objRequest.LeaveReferenceID = hdnID.Value.ToInt32();
                objRequest.RejoinDate = clsCommon.Convert2DateTime(txtRejoinDate.Text);
                //ddlType.SelectedValue = null; //   To avoid multiple insertion of requst while submitting.
                RequestId = objRequest.InsertRequest();

                // Message and mail settings

                switch (ddlType.SelectedValue.ToInt32())
                {
                    case 9:     //Resignation Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.ResignationRequest, eAction.Applied);
                        break;

                    case 16:    //Ticket Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.TicketRequest, eAction.Applied);
                        break;

                    case 22:    //Asset Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.AssetRequest, eAction.Applied);
                        break;

                    case 23:    //General Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.GeneralRequest, eAction.Applied);
                        break;

                    case 24:    //Salary Certificate Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCertificate, eAction.Applied);
                        break;
                    case 25:    //Salary Bank Statement Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.SalaryBankStatement, eAction.Applied);
                        break;
                    case 26:    //Salary Card Lost Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCardLost, eAction.Applied);
                        break;
                    case 27:    //Salary Slip Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.Salaryslip, eAction.Applied);
                        break;
                    case 28:    //Visa Letter Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.VisaLetter, eAction.Applied);
                        break;
                    case 29:    //Noc- Umarah Visa Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.NocUmarahVisa, eAction.Applied);
                        break;
                    case 30:    //Card Lost Request
                        clsCommonMessage.SendMessage(EmployeeID, RequestId, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Applied, "");
                        clsCommonMail.SendMail(RequestId, MailRequestType.CardLost, eAction.Applied);
                        break;
                }

                // Message and mail settings

                string message = "";
                if (bIsInsert)
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم اضافة تفاصيل الطلب بنجاح") : ("Request details added successfully.");
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث تفاصيل الطلب بنجاح") : ("Request details updated successfully.");

                BindDataList();
                msgs.InformationalMessage(message);
                mpeMessage.Show();
                //ddlType.SelectedIndex=-1;//   To avoid multiple insertion of requst while submitting.
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lnkView_Click(null, null);
    }

    protected void btnApproveRequest_Click(object sender, EventArgs e)
    {
        objUser = new clsUserMaster();
        objRequest = new clsGeneralRequest();
        objLeaveRequest = new clsLeaveRequest();
        int EmployeeID = objUser.GetEmployeeId();

        int RequestID = 0;
        int iRequestedById = 0;
        int RequestTypeID = 0;
        RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);

        objRequest.RequestID = RequestID;
        iRequestedById = objRequest.GetRequestedById();

        objRequest.RequestID = RequestID;
        objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
        objLeaveRequest.EmployeeId = EmployeeID;

        if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5) // Approve
        {
            if (objRequest.GetWorkStatusId() < 6)
            {
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Cannot approve the request. The requested employee is no longer in service.");
                msgs.InformationalMessage(message);
                mpeMessage.Show();

            }
            else
            {
                if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.Resignation)
                    RequestTypeID = Convert.ToInt32(RequestType.Resignation);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.Ticket)
                    RequestTypeID = Convert.ToInt32(RequestType.Ticket);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.Asset)
                    RequestTypeID = Convert.ToInt32(RequestType.Asset);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.General)
                    RequestTypeID = Convert.ToInt32(RequestType.General);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.SalaryCertificate)
                    RequestTypeID = Convert.ToInt32(RequestType.SalaryCertificate);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.SalaryBankStatement)
                    RequestTypeID = Convert.ToInt32(RequestType.SalaryBankStatement);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.SalaryCardLost)
                    RequestTypeID = Convert.ToInt32(RequestType.SalaryCardLost);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.Salaryslip)
                    RequestTypeID = Convert.ToInt32(RequestType.Salaryslip);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.VisaLetter)
                    RequestTypeID = Convert.ToInt32(RequestType.VisaLetter);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.NocUmarahVisa)
                    RequestTypeID = Convert.ToInt32(RequestType.NocUmarahVisa);
                else if (hfTypeID.Value.ToInt32() == (int)eGeneralRequest.CardLost)
                    RequestTypeID = Convert.ToInt32(RequestType.CardLost);


                if (clsLeaveRequest.IsHigherAuthority(RequestTypeID, Convert.ToInt64(EmployeeID), objRequest.RequestID))
                {
                    objRequest.StatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue);
                    if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5)
                    {
                        objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
                        objRequest.StatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue);
                        objRequest.UserID = objUser.GetUserId();
                        objRequest.Remarks = txtApproveRemarks.Text;
                        objRequest.UpdateAdvanceStatus(true);
                        divPrint.Style["display"] = "block";
                        upMenu.Update();

                    }
                    else
                    {
                        objRequest.Remarks = txtApproveRemarks.Text;
                        objRequest.UpdateAdvanceStatus(true);
                        divPrint.Style["display"] = "none";
                        upMenu.Update();
                    }

                    objRequest.RequestID = RequestID;

                    if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5 || Convert.ToInt32(ddlApproveStatus.SelectedValue) == 3 || Convert.ToInt32(ddlApproveStatus.SelectedValue) == 4)
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();

                        if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5) // Approved
                        {

                            switch (hfTypeID.Value.ToInt32())
                            {
                                case 9:     //Resignation Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.ResignationRequest, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;

                                case 16:    //Ticket Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.TicketRequest, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;

                                case 22:    //Asset Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.AssetRequest, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;

                                case 23:    //General Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.GeneralRequest, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;

                                case 24:    //Salary Certificate Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCertificate, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 25:    //Salary Bank Statement   Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryBankStatement, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 26:    //Salary Card Lost Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCardLost, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 27:    //Salary  Slip  Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.Salaryslip, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 28:    // Visa Letter Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.VisaLetter, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 29:    //Noc- Umarah Visa Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.NocUmarahVisa, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                                case 30:    // Card Lost Request
                                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Approved, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.CardLost, eAction.Approved);
                                    ////-----------------------------------------------------------------------------------------------------------
                                    break;
                            }

                        }
                        else if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 3) // Rejected
                        {
                            switch (hfTypeID.Value.ToInt32())
                            {
                                case 9:     //Resignation Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.ResignationRequest, eAction.Reject);
                                    break;

                                case 16:    //Ticket Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.TicketRequest, eAction.Reject);
                                    break;

                                case 22:    //Asset Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.AssetRequest, eAction.Reject);
                                    break;

                                case 23:    //General Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.GeneralRequest, eAction.Reject);
                                    break;
                                case 24:    //Salary Certificate Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCertificate, eAction.Reject);
                                    break;
                                case 25:    //Salary Bank Statement Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryBankStatement, eAction.Reject);
                                    break;
                                case 26:    //Salary Card Lost Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCardLost, eAction.Reject);
                                    break;
                                case 27:    //Salary Slip Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.Salaryslip, eAction.Reject);
                                    break;
                                case 28:    //Visa Letter Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.VisaLetter, eAction.Reject);
                                    break;
                                case 29:    //Noc- Umarah Visa Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.NocUmarahVisa, eAction.Reject);
                                    break;
                                case 30:    //Card Lost Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Reject, "");
                                    clsCommonMail.SendMail(RequestID, MailRequestType.CardLost, eAction.Reject);
                                    break;
                            }
                        }

                        else if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 4) // Pending
                        {
                            switch (hfTypeID.Value.ToInt32())
                            {
                                case 9:     //Resignation Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Pending, "");
                                    break;

                                case 16:    //Ticket Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Pending, "");
                                    break;

                                case 22:    //Asset Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Pending, "");
                                    break;

                                case 23:    //General Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Pending, "");
                                    break;
                                case 24:    //Salary Certificate Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Pending, "");
                                    break;
                                case 25:    //Salary Bank Statement Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Pending, "");
                                    break;
                                case 26:    //Salary Card Lost Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Pending, "");
                                    break;
                                case 27:    //Salary Slip Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Pending, "");
                                    break;
                                case 28:    //Visa Letter Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Pending, "");
                                    break;
                                case 29:    //Noc- Umarah Visa Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Pending, "");
                                    break;
                                case 30:    //Card Lost Request
                                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Pending, "");
                                    break;
                            }

                        }
                    }
                    else
                    {
                        string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                        msgs.InformationalMessage(message);
                        mpeMessage.Show();
                    }
                }

                else
                {
                    objRequest.StatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5 ? 1 : Convert.ToInt32(ddlApproveStatus.SelectedValue);
                    objRequest.Remarks = txtApproveRemarks.Text.Trim();
                    objRequest.UpdateAdvanceStatus(false);
                    string message = "";
                    if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5)
                    {
                        switch (hfTypeID.Value.ToInt32())
                        {
                            case 9:     //Resignation Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.ResignationRequest, eAction.Approved);
                                break;

                            case 16:    //Ticket Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.TicketRequest, eAction.Approved);
                                break;

                            case 22:    //Asset Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.AssetRequest, eAction.Approved);
                                break;

                            case 23:    //General Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.GeneralRequest, eAction.Approved);
                                break;

                            case 24:    //Salary Certificate Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCertificate, eAction.Approved);
                                break;
                            case 25:    //Salary Bank Statement Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryBankStatement, eAction.Approved);
                                break;
                            case 26:    //Salary Card Lost Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCardLost, eAction.Approved);
                                break;
                            case 27:    //Salary Slip Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.Salaryslip, eAction.Approved);
                                break;
                            case 28:    //Visa Letter Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.VisaLetter, eAction.Approved);
                                break;
                            case 29:    //Noc- Umarah Visa Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.NocUmarahVisa, eAction.Approved);
                                break;
                            case 30:    //Card Lost Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Approved, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.CardLost, eAction.Approved);
                                break;
                        }
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب إرسالها بنجاح") : ("Request forwarded successfully.");
                    }
                    else
                    {
                        switch (hfTypeID.Value.ToInt32())
                        {
                            case 9:     //Resignation Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.ResignationRequest, eAction.Reject);
                                break;

                            case 16:    //Ticket Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.TicketRequest, eAction.Reject);
                                break;

                            case 22:    //Asset Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.AssetRequest, eAction.Reject);
                                break;

                            case 23:    //General Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.GeneralRequest, eAction.Reject);
                                break;
                            case 24:    //Salary Certificate Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCertificate, eAction.Reject);
                                break;
                            case 25:    //Salary Bank Statement Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryBankStatement, eAction.Reject);
                                break;
                            case 26:    //Salary Card Lost Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCardLost, eAction.Reject);
                                break;
                            case 27:    //Salary Slip Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.Salaryslip, eAction.Reject);
                                break;
                            case 28:    //Visa Letter Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.VisaLetter, eAction.Reject);
                                break;
                            case 29:    //Noc- Umarah Visa Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.NocUmarahVisa, eAction.Reject);
                                break;
                            case 30:    //Card Lost Request
                                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Reject, "");
                                clsCommonMail.SendMail(RequestID, MailRequestType.CardLost, eAction.Reject);
                                break;
                        }
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مرفوض بنجاح") : ("Request rejected successfully.");

                    }

                    msgs.InformationalMessage(message);
                    mpeMessage.Show();

                }
            }


            ViewState["Approve"] = false;
            ViewState["RequestedAdvAmt"] = null;

            setDivVisibility(3);

            BindSingleView(RequestID);
            //BindRequestDetails(RequestID);
            lnkDelete.OnClientClick = "return false;";
            lnkDelete.Enabled = false;

            RemoveQueryString("RequestId");


        }
        else
        {
            objRequest.RequestID = RequestID;
            iRequestedById = objRequest.GetRequestedById();
            objRequest.StatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue);

            objRequest.EmployeeID = iRequestedById;
            objRequest.Remarks = txtApproveRemarks.Text;
            objRequest.UpdateAdvanceStatus(true);
            string message = "";
            if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 3)
            {

                switch (hfTypeID.Value.ToInt32())
                {
                    case 9:     //Resignation Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.ResignationRequest, eAction.Reject);


                        break;

                    case 16:    //Ticket Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.TicketRequest, eAction.Reject);

                        break;

                    case 22:    //Asset Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.AssetRequest, eAction.Reject);

                        break;

                    case 23:    //General Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.GeneralRequest, eAction.Reject);

                        break;

                    case 24:    //Salary Certificate Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCertificate, eAction.Reject);

                        break;

                    case 25:    //Salary Bank Statement Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.SalaryBankStatement, eAction.Reject);

                        break;

                    case 26:    //Salary Card Lost Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.SalaryCardLost, eAction.Reject);

                        break;

                    case 27:    //Salary Slip Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.Salaryslip, eAction.Reject);

                        break;

                    case 28:    //Visa Letter Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.VisaLetter, eAction.Reject);

                        break;

                    case 29:    //Noc- Umarah Visa Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.NocUmarahVisa, eAction.Reject);

                        break;

                    case 30:    //Card Lost Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Reject, "");
                        clsCommonMail.SendMail(RequestID, MailRequestType.CardLost, eAction.Reject);

                        break;
                }



                message = clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مرفوض بنجاح") : ("Request rejected successfully.");
            }
            else if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 4)
            {

                switch (hfTypeID.Value.ToInt32())
                {
                    case 9:     //Resignation Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Pending, "");

                        break;

                    case 16:    //Ticket Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Pending, "");
                        break;

                    case 22:    //Asset Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Pending, "");
                        break;

                    case 23:    //General Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Pending, "");
                        break;
                    case 24:    //Salary Certificate Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Pending, "");
                        break;
                    case 25:    //Salary Bank Statement Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Pending, "");
                        break;
                    case 26:    //Salary Card Lost Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Pending, "");
                        break;
                    case 27:    //Salary Slip Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Pending, "");
                        break;
                    case 28:    //Visa Letter Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Pending, "");
                        break;
                    case 29:    //Noc- Umarah Visa Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Pending, "");
                        break;
                    case 30:    //Card Lost Request
                        clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Pending, "");
                        break;
                }

                message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");

            }
            ViewState["Approve"] = false;
            ViewState["RequestedAdvAmt"] = null;

            //BindRequestDetails(RequestID);
            setDivVisibility(3);

            BindSingleView(RequestID);

            lnkDelete.OnClientClick = "return false;";
            lnkDelete.Enabled = false;
            msgs.InformationalMessage(message);
            mpeMessage.Show();

            RemoveQueryString("RequestId");

        }

    }

    protected void btnCancelExpense_Click(object sender, EventArgs e)
    {
        objRequest = new clsGeneralRequest();
        objUser = new clsUserMaster();
        int RequestId = 0;
        int RequestedById = 0;
        RequestId = Request.QueryString["Requestid"].ToInt32();
        objRequest.RequestID = RequestId;

        string strmessage = "";
        if (objRequest.GetWorkStatusId() < 6)
        {
            strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل في إلغاء الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Failed to cancel the request. The requested employee is no longer in service.");
        }

        else
        {
            objRequest.RequestID = RequestId;
            objRequest.StatusID = Convert.ToInt32(ddlApproveStatus.SelectedValue);
            objRequest.EmployeeID = objUser.GetEmployeeId();
            objRequest.Remarks = txtApproveRemarks.Text; //txtEditReason.Text;//
            objRequest.SalaryAdvanceRequestCancelled();

            RequestedById = objRequest.GetRequestedById();
            switch (hfTypeID.Value.ToInt32())
            {
                case 9:     //Resignation Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.ResignationRequest, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.ResignationRequest, eMessageTypes.Resignation_Request, eAction.Cancelled, "");

                    break;

                case 16:    //Ticket Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.TicketRequest, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.TicketRequest, eMessageTypes.Ticket_Request, eAction.Cancelled, "");

                    break;

                case 22:    //Asset Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.AssetRequest, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.AssetRequest, eMessageTypes.Asset_Request, eAction.Cancelled, "");

                    break;

                case 23:    //General Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.GeneralRequest, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.GeneralRequest, eMessageTypes.General_Request, eAction.Cancelled, "");

                    break;

                case 24:    //Salary Certificate Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCertificate, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryCertificate, eMessageTypes.SalaryCertificate_Request, eAction.Cancelled, "");

                    break;
                case 25:    //Salary Bank Statement Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.SalaryBankStatement, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryBankStatement, eMessageTypes.SalaryBankStatement_Request, eAction.Cancelled, "");

                    break;
                case 26:    //Salary Card Lost Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.SalaryCardLost, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.SalaryCardLost, eMessageTypes.SalaryCardLost_Request, eAction.Cancelled, "");

                    break;
                case 27:    //Salary Slip Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.Salaryslip, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.Salaryslip, eMessageTypes.Salaryslip_Request, eAction.Cancelled, "");

                    break;
                case 28:    //Visa Letter Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.VisaLetter, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.VisaLetter, eMessageTypes.VisaLetter_Request, eAction.Cancelled, "");

                    break;
                case 29:    //Noc- Umarah Visa Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.NocUmarahVisa, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.NocUmarahVisa, eMessageTypes.NocUmarahVisa_Request, eAction.Cancelled, "");

                    break;
                case 30:    //Card Lost Request
                    clsCommonMail.SendMail(RequestId, MailRequestType.CardLost, eAction.Cancelled);

                    //--------------------------------------------Message insertion into HRMessages-----------------------------------------
                    clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.CardLost, eMessageTypes.CardLost_Request, eAction.Cancelled, "");

                    break;
            }
            //-----------------------------------------------------------------------------------------------------------          

            ViewState["RequestForCancel"] = false;
            // BindRequestDetails(RequestId );
            setDivVisibility(3);
            BindSingleView(RequestId);
            strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
        }
        msgs.InformationalMessage(strmessage);
        mpeMessage.Show();


    }

    protected void btnCancelCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }
    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsGeneralRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestID = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                trReasons.Style["display"] = "block";

                DataTable dt = objRequest.DisplayReasons(objRequest.RequestID);
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
            }
        }
    }
    protected void lbApproveReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsGeneralRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestID = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfApprovelink.Value) == 0)
            {
                hfApprovelink.Value = "1";
                trApproveReasons.Style["display"] = "block";

                DataTable dt = objRequest.DisplayReasons(objRequest.RequestID);
                if (dt.Rows.Count > 0)
                {
                    gvApproveReasons.DataSource = dt;
                    gvApproveReasons.DataBind();
                }
            }
            else
            {
                hfApprovelink.Value = "0";
                trApproveReasons.Style["display"] = "none";
            }
        }
    }
    public enum eGeneralRequest
    {
        Resignation = 9,
        Ticket = 16,
        Asset = 22,
        General = 23,
        SalaryCertificate = 24,
        SalaryBankStatement=25,
        SalaryCardLost=26,
        Salaryslip=27,
        VisaLetter=28,
        NocUmarahVisa=29,
        CardLost=30
    }
    protected void gdvPendingLoan_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gdvPendingLoan.PageIndex = e.NewPageIndex;
        objUser = new clsUserMaster();
        objRequest = new clsGeneralRequest();
        objRequest.EmployeeID = objUser.GetEmployeeId();
        gdvPendingLoan.DataSource = objRequest.GetUnclosedLoan();
        gdvPendingLoan.DataBind();
        //gdvPendingLoan.Visible = true ;
        //lblUnclosedLoans.Visible = true;
       

        divLeft.Visible = true;
        divRight.Visible = true;
    }
}
