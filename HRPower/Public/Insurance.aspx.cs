﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
using System.Text;
using System.Reflection;
using System.Xml.Linq;
/// <summary>
/// Summary description for clsInsurance
/// Created By:    Sanju
/// Created Date:  29-Oct-2013
/// Description:   Insurance
/// </summary>
public partial class Public_Insurance : System.Web.UI.Page
{
    #region DECLARETIONS
    clsInsurance objclsInsurance;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    private string CurrentSelectedValue { get; set; }

    #endregion
    #region EVENTS

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();

        InsurancePager.Fill += new controls_Pager.FillPager(BindDataList);
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, HRAutoComplete.AutoCompletePage.Insurance, objUser.GetCompanyId());

        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        if (!IsPostBack)
        {
            objclsInsurance = new clsInsurance();
            SetPermission();
            fillCombo();
            DataTable datData = objclsInsurance.GetInsuranceCount();
            if (datData.Rows.Count > 0)
            {
                if (datData.Rows[0]["Count"].ToInt32() > 0)
                {
                    lnkView_Click(null, null);
                }
                else
                {
                    if (lnkAdd.Enabled)
                    {
                        setDivVisibility(1);
                    }
                    else
                    {
                        setDivVisibility(4);
                    }
                }
            }
            else
            {
                if (lnkAdd.Enabled)
                {
                    setDivVisibility(1);
                }
                else
                {
                    setDivVisibility(4);
                }
            }
            enableAddViewButtons();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objclsInsurance = new clsInsurance();
        string msg = string.Empty;

        FillParameters(objclsInsurance);
        if (objclsInsurance.intCompanyID <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الرجاء اختيار شركة") : ("Please select a Company");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Please select a Company");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.intCompanyAssetID <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى تحديد الأصول") : ("Please select a Asset");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Please select a Asset");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.strPolicyNumber == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال رقم السياسة") : ("Please enter Policy Number");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Please enter Policy Number");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.isPolicyNumberExist())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال رقم السياسة") : ("Policy number already exist");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Policy number already exist");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.strPolicyName == "")
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال رقم السياسة") : ("Please enter Policy Name");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Please enter Policy Name");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.intInsuranceCompanyID == 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال رقم السياسة") : ("Please select a Insurance Company");
            msgs.InformationalMessage(msg);
            // msgs.InformationalMessage("Please select a Insurance Company");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.decPolicyAmount <= 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("يرجى إدخال رقم السياسة") : ("Please enter Policy Amount");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Please enter Policy Amount");
            mpeMessage.Show();
            return;
        }
        if (objclsInsurance.dteIssueDate > objclsInsurance.dteExpiryDate)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تاريخ القضية أكبر من تاريخ انتهاء الصلاحية") : ("Issue date greater than Expiry date");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("Issue date greater than Expiry date");
            mpeMessage.Show();
            return;
        }
        DataTable datData = objclsInsurance.SaveInsurance();
        if (datData.Rows.Count > 0)
        {
            if (datData.Rows[0]["ID"].ToInt32() > 0)
            {
                objAlert = new clsDocumentAlert();
                objAlert.AlertMessage((int)DocumentType.Insurance, "Insurance", "InsurancePolicyNumber", datData.Rows[0]["ID"].ToInt32(), objclsInsurance.strPolicyNumber, objclsInsurance.dteExpiryDate, "Comp", objclsInsurance.intCompanyID, ddlCompany.Text.Trim(), false);
                clsInsurance objclsInsuranceDocument = new clsInsurance();
                Label lblDocname, lblActualfilename, lblFilename;

                foreach (DataListItem item in dlListDoc.Items)
                {
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    lblActualfilename = (Label)item.FindControl("lblActualfilename");
                    objclsInsuranceDocument.intInsuranceID = datData.Rows[0]["ID"].ToInt32();
                    objclsInsuranceDocument.strPolicyNumber = objclsInsurance.strPolicyNumber;
                    objclsInsuranceDocument.strDocname = lblDocname.Text.Trim();
                    objclsInsuranceDocument.strFilename = lblFilename.Text.Trim();

                    if (dlListDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objclsInsuranceDocument.InsertInsuranceTreemaster();

                        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";
                        //clsCommon.WriteToLog("Physical path is" + Path);
                        if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(Path))
                                Directory.CreateDirectory(Path);
                            //clsCommon.WriteToLog("Directory is created");
                            File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                            //clsCommon.WriteToLog("File moved successfully");
                        }
                    }
                }
                if (objclsInsurance.intInsuranceID > 0)
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث بنجاح.") : ("Successfully updated.");
                    msgs.InformationalMessage(msg);
                    //msgs.InformationalMessage("Successfully updated.");
                }
                else
                {
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("حفظ بنجاح.") : ("Successfully saved.");
                    msgs.InformationalMessage(msg);
                    //msgs.InformationalMessage("Successfully saved.");
                }
                mpeMessage.Show();
                lnkView_Click(null, null);
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lnkView_Click(null, null);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        if (!lnkAdd.Enabled)
        {
            setDivVisibility(4);
        }
        else
        {
            ClearControls();
            setDivVisibility(1);
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        InsurancePager.CurrentPage = 0;
        hfInsuranceID.Value = "";
        txtSearch.Text = "";
        BindDataList();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        string msg = string.Empty;
        objclsInsurance = new clsInsurance();

        if (hfInsuranceID.Value.ToInt32() > 0)
        {
            objclsInsurance.intInsuranceID = hfInsuranceID.Value.ToInt32();

            if (objclsInsurance.isInsuranceDeletePossible())
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("الحذف غير ممكن، أو تجديد التأمين فور صدورها.") : ("Deletion not possible,Insurance renewed or once issued.");
                msgs.InformationalMessage(msg);
                //msgs.InformationalMessage("Deletion not possible,Insurance renewed or once issued.");
                mpeMessage.Show();
                return;
            }
            else
            {
                objclsInsurance.DeleteInsurance();

                message = clsUserMaster.GetCulture() == "ar-AE" ? ("التأمين حذف بنجاح") : ("Insurance deleted successfully"); //"Insurance deleted successfully";
            }

        }
        else
        {
            foreach (DataListItem item in dlInsurance.Items)
            {
                CheckBox chkInsurance = (CheckBox)item.FindControl("chkInsurance");
                if (chkInsurance == null)
                    continue;
                if (chkInsurance.Checked)
                {
                    objclsInsurance.intInsuranceID = dlInsurance.DataKeys[item.ItemIndex].ToInt32();
                    if (objclsInsurance.isInsuranceDeletePossible())
                    {
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("بعض الحذف غير ممكن، التأمين (ق) تجدد مرة واحدة أو إصدارها.") : ("Some deletion not possible,Insurance(s) renewed or once issued."); //"Some deletion not possible,Insurance(s) renewed or once issued.";
                    }
                    else
                    {
                        objclsInsurance.DeleteInsurance();
                        message = clsUserMaster.GetCulture() == "ar-AE" ? ("التأمين حذف بنجاح") : ("Insurance deleted successfully"); //"Insurance deleted successfully";

                    }
                }
            }
        }
        if (message != string.Empty)
        {
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
        lnkView_Click(null, null);
    }
    protected void dlInsurance_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {

            case "_View":

                singleViewInsurance(e.CommandArgument.ToInt32());
                break;

            case "_Edit":

                fillUpdateInsurance(e.CommandArgument.ToInt32());
                break;

        }
    }
    protected void dlInsurance_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //    HiddenField hfIsRenewed = (HiddenField)e.Item.FindControl("hfIsRenewed");
            //    //HiddenField hdStatusID = (HiddenField)e.Item.FindControl("hfStatusID");
            //    Int32 intStatusID = hfIsRenewed.Value.ToInt32();
            //    bool blnIsRenewed = hfIsRenewed.Value == "True" ? true : false;

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            //    if (blnIsRenewed)
            //    {
            //        lbtnEdit.Enabled = false;
            //        lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            //    }


            DataTable dtm = (DataTable)ViewState["Permission"];

            if (dtm.Rows.Count > 0)
            {
                if (!dtm.Rows[0]["IsUpdate"].ToBoolean())
                {
                    lbtnEdit.Enabled = false;
                    lbtnEdit.ImageUrl = "~/images/edit_disable.png";
                }
            }
            else
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }
        }
    }
    protected void lnkPrint_Click(object sender, EventArgs e)
    {

        if (hfInsuranceID.Value.ToInt32() > 0)
        {
            objclsInsurance = new clsInsurance();
            objclsInsurance.intInsuranceID = hfInsuranceID.Value.ToInt32();
            string strData = "";
            strData = GetInsurancePrint();
            if (strData != "")
            {
                divPrint.InnerHtml = strData;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

            }

        }
        else
        {
            if (dlInsurance.Items.Count > 0)
            {

                CheckBox chkInsurance;
                string strInsuranceIDs = string.Empty;

                foreach (DataListItem item in dlInsurance.Items)
                {
                    chkInsurance = (CheckBox)item.FindControl("chkInsurance");

                    if (chkInsurance == null)
                        continue;

                    if (chkInsurance.Checked == false)
                        continue;

                    strInsuranceIDs += "," + dlInsurance.DataKeys[item.ItemIndex];
                }
                if (strInsuranceIDs != "")
                {
                    strInsuranceIDs = strInsuranceIDs.Remove(0, 1);

                    objclsInsurance = new clsInsurance();
                    string strData = "";
                    if (strInsuranceIDs.Contains(","))
                    {
                        strData = GetMultipleInsurancePrint(strInsuranceIDs);
                    }
                    else
                    {
                        objclsInsurance.intInsuranceID = strInsuranceIDs.ToInt32();
                        strData = GetInsurancePrint();
                    }
                    if (strData != "")
                    {
                        divPrint.InnerHtml = strData;
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

                    }
                }
            }

        }
    }

    public string GetMultipleInsurancePrint(string MultipleInsuranceIDs)
    {
        DataTable datData = objclsInsurance.GetMultipleInsurance(MultipleInsuranceIDs);
        if (datData.Rows.Count > 0)
        {
            string str = string.Empty;
            str = clsUserMaster.GetCulture() == "ar-AE" ? ("تأمين") : ("Insurance");
            return GetDocumentHtml(datData, str, 2);
        }
        return "";

    }

    public string GetInsurancePrint()
    {
        DataTable datData = objclsInsurance.GetInsurance();
        if (datData.Rows.Count > 0)
        {
            string str = string.Empty;
            str = clsUserMaster.GetCulture() == "ar-AE" ? ("تأمين") : ("Insurance");
            return GetDocumentHtml(datData, str, 1);
        }
        return "";

    }

    private string GetDocumentHtml(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";
        //string strSubHeaderStyle2 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:rgb(232, 238, 240); font-size:14px; color:#000000'";
        //string strSubHeaderStyle3 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#000000; font-size:14px; color:#FFFFFF'";
        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
        string strHeaderRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left; font-size:14px; color:#000000''";
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";
       
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";        
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        string strAltRowInner20Style = "style='width:20%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + " : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("InsuranceDetails.Text") + " </div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Asset.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Asset"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyNumber.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyName.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyName"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("InsuranceCompany.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["InsuranceCompany"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PolicyAmount.Text") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["PolicyAmount"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["IssueDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["ExpiryDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("ControlsCommon", "Remarks") + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");
        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "Company") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Asset.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyNumber.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyName.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("InsuranceCompany.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("PolicyAmount.Text") + "</div><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "IssueDate") + "</div><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "ExpiryDate") + "</div><div " + strHeaderRowInner20Style + ">" + GetGlobalResourceObject("ControlsCommon", "Remarks") + "</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Asset"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyNumber"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["InsuranceCompany"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["PolicyAmount"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["IssueDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ExpiryDate"].ToStringCustom() + "</div><div " + strAltRowInner20Style + ">" + drCurrentRow["Remarks"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        if (hfInsuranceID.Value.ToInt32() > 0)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Insurance&InsuranceID=" + hfInsuranceID.Value.ToStringCustom() + "', 835, 585);", true);
        }
        else
        {
            if (dlInsurance.Items.Count > 0)
            {

                CheckBox chkInsurance;
                string strInsuranceIDs = string.Empty;

                foreach (DataListItem item in dlInsurance.Items)
                {
                    chkInsurance = (CheckBox)item.FindControl("chkInsurance");

                    if (chkInsurance == null)
                        continue;

                    if (chkInsurance.Checked == false)
                        continue;

                    strInsuranceIDs += "," + dlInsurance.DataKeys[item.ItemIndex];
                }
                if (strInsuranceIDs != "")
                {
                    strInsuranceIDs = strInsuranceIDs.Remove(0, 1);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=Insurance&InsuranceID=" + strInsuranceIDs + "', 835, 585);", true);
                }


            }

        }
    }
    //protected void lnkRenew_Click(object sender, EventArgs e)
    //{
    //    objclsInsurance = new clsInsurance();

    //    if (hfInsuranceID.Value.ToInt32() > 0)
    //    {

    //        objclsInsurance.intInsuranceID = hfInsuranceID.Value.ToInt32();
    //        objclsInsurance.RenewInsurance();
    //        msgs.InformationalMessage("Lease Agreement renewed.");
    //        mpeMessage.Show();
    //        lnkView_Click(null, null);
    //    }
    //}
    //protected void lnkDocumentReceipt_Click(object sender, EventArgs e)
    //{
    //}
    //protected void dlPassportDoc_ItemCommand(object source, DataListCommandEventArgs e)
    //{
    //    switch (e.CommandName)
    //    {
    //        case "REMOVE_ATTACHMENT":
    //            Label lblDocname, lblFilename;
    //            objEmployeePassport = new clsEmployeePassport();

    //            if (e.CommandArgument != string.Empty)
    //            {
    //                objEmployeePassport.Node = Convert.ToInt32(e.CommandArgument);
    //                lblFilename = (Label)e.Item.FindControl("lblFilename");
    //                string sFilename = lblFilename.Text.Trim();
    //                objEmployeePassport.DeletePassportDocument();
    //                string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

    //                if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
    //                    File.Delete(Path + "/" + Convert.ToString(sFilename));
    //            }

    //            objEmployeePassport.PassportID = -1;
    //            objEmployeePassport.EmployeeID = -1;
    //            DataTable dt = objEmployeePassport.GetPassportDocuments().Tables[0];
    //            DataRow dw;

    //            foreach (DataListItem item in dlPassportDoc.Items)
    //            {
    //                if (e.Item.ItemIndex == item.ItemIndex)
    //                    continue;

    //                dw = dt.NewRow();

    //                lblDocname = (Label)item.FindControl("lblDocname");
    //                lblFilename = (Label)item.FindControl("lblFilename");

    //                dw["Node"] = dlPassportDoc.DataKeys[item.ItemIndex];
    //                dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
    //                dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
    //                dt.Rows.Add(dw);
    //            }

    //            dlPassportDoc.DataSource = dt;
    //            dlPassportDoc.DataBind();
    //            ViewState["ListDoc"] = dt;
    //            break;
    //    }
    //}
    protected void dlListDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objclsInsurance = new clsInsurance();

                if (e.CommandArgument != string.Empty)
                {
                    objclsInsurance.intNode = e.CommandArgument.ToInt32();
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objclsInsurance.DeleteInsuranceDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }
                DataTable dt = objclsInsurance.GetInsuranceDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlListDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();

                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");

                    dw["Node"] = dlListDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlListDoc.DataSource = dt;
                dlListDoc.DataBind();
                ViewState["ListDoc"] = dt;
                break;
        }
    }
    protected void fuInsurance_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuInsurance.HasFile)
        {
            actualfilename = fuInsurance.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuInsurance.FileName);
            Session["Filename"] = filename;
            fuInsurance.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }
    protected void btnattachdoc_Click(object sender, EventArgs e)
    {
        divListDoc.Style["display"] = "block";

        DataTable dt;
        DataRow dw;
        objclsInsurance = new clsInsurance();
        if (ViewState["ListDoc"] == null)
            dt = objclsInsurance.GetInsuranceDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["ListDoc"];
            dt = dtDoc;
        }
        dw = dt.NewRow();
        dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
        dw["Filename"] = Session["Filename"];
        dt.Rows.Add(dw);
        dlListDoc.DataSource = dt;
        dlListDoc.DataBind();
        ViewState["ListDoc"] = dt;
        txtDocname.Text = string.Empty;
        upListDoc.Update();
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        InsurancePager.CurrentPage = 0;
        hfInsuranceID.Value = "";
        BindDataList();
    }
    protected void imgInsuranceCompany_Click(object sender, EventArgs e)
    {
        if (ddlInsuranceCompany != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "InsuranceCompanyReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataValueField = "InsuranceCompanyID";
            ReferenceControlNew1.FunctionName = "FillInsuranceCompany";
            ReferenceControlNew1.SelectedValue = ddlInsuranceCompany.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("InsuranceCompany.Text").ToString();
            ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
            upInsuranceCompany.Update();
        }
    } 
    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCompanyAsset(ddlCompany.SelectedValue.ToInt32());


    }

    #endregion
    #region METHODS
    private void fillCombo()
    {
        objclsInsurance = new clsInsurance();
        objUser = new clsUserMaster();
       // objclsInsurance.intUserID = objUser.GetUserId();
        objclsInsurance.intCompanyID = objUser.GetCompanyId();
        DataSet dtsData = objclsInsurance.GetReference();
        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {

                //DataRow dr = dtsData.Tables[0].NewRow();
                //dr["CompanyName"] = "Select";
                //dr["CompanyID"] = "-1";
                //dtsData.Tables[0].Rows.InsertAt(dr, 0);
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyID";
                ddlCompany.DataSource = dtsData.Tables[0];
                ddlCompany.DataBind();
                //string str = string.Empty;
                //str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
                //ddlCompany.Items.Insert(0, new ListItem(str, "-1"));
                ddlCompany.Enabled = false;
            }
        }
        if (dtsData.Tables.Count > 1)
        {
            if (dtsData.Tables[1].Rows.Count > 0)
            {

                DataRow dr = dtsData.Tables[1].NewRow();
                dr["Description"] = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");// "Select";
                dr["InsuranceCompanyID"] = "-1";
                dtsData.Tables[1].Rows.InsertAt(dr, 0);
                ddlInsuranceCompany.DataTextField = "Description";
                ddlInsuranceCompany.DataValueField = "InsuranceCompanyID";
                ddlInsuranceCompany.DataSource = dtsData.Tables[1];
                ddlInsuranceCompany.DataBind();


            }
        }

        DataTable datAsset = new DataTable();
        datAsset.Columns.Add("CompanyAssetID");
        datAsset.Columns.Add("Description");
        DataRow drAsset = datAsset.NewRow();
        drAsset["Description"] = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");//"Select";
        drAsset["CompanyAssetID"] = "-1";
        datAsset.Rows.InsertAt(drAsset, 0);
        ddlAsset.DataTextField = "Description";
        ddlAsset.DataValueField = "CompanyAssetID";
        ddlAsset.DataSource = datAsset;
        ddlAsset.DataBind();

        fillCompanyAsset(ddlCompany.SelectedValue.ToInt32());

    }
    private void BindDataList()
    {
        objUser = new clsUserMaster();
        objclsInsurance = new clsInsurance();
        objclsInsurance.intPageIndex = InsurancePager.CurrentPage + 1;
        objclsInsurance.intPageSize = InsurancePager.PageSize;
        objclsInsurance.strSearchKey = txtSearch.Text.Trim();
        objclsInsurance.intCompanyID = objUser.GetCompanyId();
        DataSet dtsData = objclsInsurance.GetInsuranceToBind();

        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                dlInsurance.DataSource = dtsData.Tables[0];
                dlInsurance.DataBind();
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        InsurancePager.Total = dtsData.Tables[1].Rows[0]["Count"].ToInt32();
                    }
                    else
                    {
                        InsurancePager.Total = 0;
                    }
                }
                else
                {
                    InsurancePager.Total = 0;
                }
                setDivVisibility(2);
                hfInsuranceID.Value = "";
                upMenu.Update();
            }

            else
            {
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        if (dtsData.Tables[1].Rows[0]["Count"].ToInt32() > 0)
                        {
                            if (InsurancePager.CurrentPage > 0)
                            {
                                InsurancePager.CurrentPage = InsurancePager.CurrentPage - 1;
                                BindDataList();
                            }
                            else
                            {
                                lnkAdd_Click(null, null);
                            }
                        }
                        else
                        {
                            setDivVisibility(5);
                           // lnkAdd_Click(null, null);
                        }
                    }
                    else
                    {
                        lnkAdd_Click(null, null);

                    }
                }
                else
                {
                    lnkAdd_Click(null, null);

                }
            }
        }
        else
        {
            lnkAdd_Click(null, null);
        }

    }
    private void FillParameters(clsInsurance objclsInsurance)
    {
        objclsInsurance.intInsuranceID = hfInsuranceID.Value.ToInt32();

        objclsInsurance.intCompanyID = ddlCompany.SelectedValue.ToInt32();
        objclsInsurance.intCompanyAssetID = ddlAsset.SelectedValue.ToInt32();
        objclsInsurance.strPolicyNumber = txtPolicyNumber.Text.Trim();
        objclsInsurance.strPolicyName = txtPolicyName.Text.Trim();
        objclsInsurance.decPolicyAmount = txtPolicyAmount.Text.Trim().ToDecimal();
        objclsInsurance.intInsuranceCompanyID = ddlInsuranceCompany.SelectedValue.ToInt32();
        objclsInsurance.dteIssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text.Trim());
        objclsInsurance.dteExpiryDate = clsCommon.Convert2DateTime(txtExpirydate.Text.Trim());
        objclsInsurance.strRemarks = txtRemarks.Text.Trim();

    }
    private void ClearControls()
    {
        fillCombo();
        hfInsuranceID.Value = "";
        ddlCompany.SelectedIndex = 0;
        ddlAsset.SelectedIndex = 0;
        txtPolicyNumber.Text = "";
        txtPolicyName.Text = "";
        txtPolicyAmount.Text = "";
        ddlInsuranceCompany.SelectedIndex = 0;
        txtIssuedate.Text = "";
        txtExpirydate.Text = "";
        txtRemarks.Text = "";
        txtDocname.Text = "";
        dlListDoc.DataSource = null;
        dlListDoc.DataBind();
        lblStatusAdd.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("جديد") : ("New"); //"New";
        divAddInsuranceDataPart.Style["pointer-events"] = "auto";


        //divRenew.Style["display"] = "none";
        //divIssueReceipt.Style["display"] = "none";
    }
    private void setDivVisibility(int intMode)
    {
        //Mode 1:-divAddInsurance
        //Mode 2:-divViewInsurance
        //Mode 3:-divSingleViewInsurance
        //Mode 4:-divNoInformationFound


        if (intMode == 1)
        {
            divAddInsurance.Style["display"] = "block";
            divViewInsurance.Style["display"] = "none";
            divSingleViewInsurance.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "none";
            enablePrintEmailDeleteButton(false, false);
        }
        else if (intMode == 2)
        {
            divAddInsurance.Style["display"] = "none";
            divViewInsurance.Style["display"] = "block";
            divSingleViewInsurance.Style["display"] = "none";
            enablePrintEmailDeleteButton(true, true);
            divNoInformationFound.Style["display"] = "none";

        }
        else if (intMode == 3)
        {
            divAddInsurance.Style["display"] = "none";
            divViewInsurance.Style["display"] = "none";
            divSingleViewInsurance.Style["display"] = "block";
            divNoInformationFound.Style["display"] = "none";
            enablePrintEmailDeleteButton(true, false);
        }
        else if (intMode == 4)
        {
            divAddInsurance.Style["display"] = "none";
            divViewInsurance.Style["display"] = "none";
            divSingleViewInsurance.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "block";
            enablePrintEmailDeleteButton(false, false);
        }

        else if (intMode == 5)
        {
            divAddInsurance.Style["display"] = "none";
            divViewInsurance.Style["display"] = "none";
            divSingleViewInsurance.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "block";
            enablePrintEmailDeleteButton(false, false);
        }
        upViewInsurance.Update();
        upAddInsurance.Update();
        upSingleViewInsurance.Update();
        upNoInformationFound.Update();
    }
    private void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.Insurance);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }
    private void enableAddViewButtons()
    {
        string msg = string.Empty;
        DataTable dtm = (DataTable)ViewState["Permission"];

        if (dtm.Rows.Count > 0)
        {
            if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
            {
                ImageButton1.Enabled = true;
            }
            else
            {
                ImageButton1.Enabled = false;
            }
            if (!dtm.Rows[0]["IsView"].ToBoolean())
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لم يكن لديك ما يكفي إذن") : ("You don't have enough permission");
                msgs.InformationalMessage(msg);
                //msgs.InformationalMessage("You don't have enough permission");
                mpeMessage.Show();
                setDivVisibility(4);
            }
            lnkAdd.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
        }
        else
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لم يكن لديك ما يكفي إذن") : ("You don't have enough permission");
            msgs.InformationalMessage(msg);
            //msgs.InformationalMessage("You don't have enough permission");
            mpeMessage.Show();
            setDivVisibility(4);
        }
        //enablePrintEmailDeleteButton(true, true);

    }
    private void enablePrintEmailDeleteButton(bool IsEnable, bool isList)
    {
        DataTable dtm = (DataTable)ViewState["Permission"];
        if (dtm.Rows.Count > 0)
        {
            if (IsEnable)
            {
                if (dtm.Rows[0]["IsDelete"].ToBoolean())
                {
                    if (isList)
                    {
                        lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlInsurance.ClientID + "');";
                    }
                    else
                    {
                        lnkDelete.OnClientClick = "return confirm('" + GetGlobalResourceObject("DocumentsCommon", "Areyousuretodeletetheselecteditems").ToString() + "');"; //"return confirm('Are you sure you want to delete?');";
                    }
                    lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                }
                else
                {
                    lnkDelete.Enabled = false;
                    lnkDelete.OnClientClick = "return false;";
                }
                if (dtm.Rows[0]["IsPrintEmail"].ToBoolean())
                {
                    if (isList)
                    {
                        lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlInsurance.ClientID + "');";
                        lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlInsurance.ClientID + "');";
                    }
                    else
                    {
                        lnkPrint.OnClientClick = "return true;";
                        lnkEmail.OnClientClick = "return true;";
                    }
                    lnkPrint.Enabled = lnkEmail.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
                }
                else
                {
                    lnkPrint.Enabled = lnkEmail.Enabled = false;
                    lnkPrint.OnClientClick = "return false;";
                    lnkEmail.OnClientClick = "return false;";
                }
            }
            else
            {
                lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
                lnkPrint.OnClientClick = "return false;";
                lnkEmail.OnClientClick = "return false;";
                lnkDelete.OnClientClick = "return false;";
            }
        }
        else
        {
            lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
            lnkPrint.OnClientClick = "return false;";
            lnkEmail.OnClientClick = "return false;";
            lnkDelete.OnClientClick = "return false;";
        }
        upMenu.Update();
    }
    //private void enablePrintEmailButton(bool IsEnable)
    //{
    //    DataTable dtm = (DataTable)ViewState["Permission"];
    //    if (IsEnable)
    //    {
    //        if (dtm.Rows[0]["IsDelete"].ToBoolean())
    //        {
    //            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlInsurance.ClientID + "');";
    //            //lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
    //            lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
    //        }
    //        else
    //        {
    //            lnkDelete.Enabled = false;
    //            lnkDelete.OnClientClick = "return false;";
    //        }
    //    }
    //    else
    //    {
    //        lnkDelete.Enabled = false;
    //        lnkDelete.OnClientClick = "return false;";
    //    }
    //    upMenu.Update();
    //}
    private void singleViewInsurance(int intInsuranceID)
    {
        objclsInsurance = new clsInsurance();
        objclsInsurance.intInsuranceID = intInsuranceID;
        DataTable datData = objclsInsurance.GetInsurance();
        if (datData.Rows.Count > 0)
        {
            hfInsuranceID.Value = datData.Rows[0]["InsuranceID"].ToStringCustom();
            lblCompany.Text = datData.Rows[0]["CompanyName"].ToStringCustom();
            lblRenewed.Text = datData.Rows[0]["Renewed"].ToStringCustom();
            lblAsset.Text = datData.Rows[0]["Asset"].ToStringCustom();
            lblPolicyNumber.Text = datData.Rows[0]["PolicyNumber"].ToStringCustom();
            lblPolicyName.Text = datData.Rows[0]["PolicyName"].ToStringCustom();
            lblInsuranceCompany.Text = datData.Rows[0]["PolicyAmount"].ToStringCustom();
            lblPolicyAmount.Text = datData.Rows[0]["PolicyAmount"].ToStringCustom();
            lblIssueDate.Text = datData.Rows[0]["IssueDate"].ToStringCustom();
            lblExpiryDate.Text = datData.Rows[0]["ExpiryDate"].ToStringCustom();
            lblRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();
            lblStatus.Text = datData.Rows[0]["Status"].ToStringCustom();
            setDivVisibility(3);

        }


    }
    private void fillUpdateInsurance(int intInsuranceID)
    {
        objclsInsurance = new clsInsurance();
        objclsInsurance.intInsuranceID = intInsuranceID;
        DataTable datData = objclsInsurance.GetInsurance();
        if (datData.Rows.Count > 0)
        {
            hfInsuranceID.Value = datData.Rows[0]["InsuranceID"].ToStringCustom();
            ddlCompany.SelectedValue = datData.Rows[0]["CompanyID"].ToStringCustom();
            fillCompanyAsset(ddlCompany.SelectedValue.ToInt32());
            ddlAsset.SelectedValue = datData.Rows[0]["CompanyAssetID"].ToStringCustom();
            txtPolicyNumber.Text = datData.Rows[0]["PolicyNumber"].ToStringCustom();
            txtPolicyName.Text = datData.Rows[0]["PolicyName"].ToStringCustom();
            txtPolicyAmount.Text = datData.Rows[0]["PolicyAmount"].ToStringCustom();
            ddlInsuranceCompany.SelectedValue = datData.Rows[0]["InsuranceCompanyID"].ToStringCustom();
            txtIssuedate.Text = datData.Rows[0]["IssueDate"].ToDateTime().ToString("dd/MM/yyy");
            txtExpirydate.Text = datData.Rows[0]["ExpiryDate"].ToDateTime().ToString("dd/MM/yyy");
            txtRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();
            lblStatusAdd.Text = datData.Rows[0]["Status"].ToStringCustom();


            setDivVisibility(1);
            if (Convert.ToBoolean(datData.Rows[0]["IsRenewed"]))
            {
                divAddInsuranceDataPart.Style["pointer-events"] = "none";
            }
            else
            {
                divAddInsuranceDataPart.Style["pointer-events"] = "auto";

            }
            objclsInsurance.intInsuranceID = intInsuranceID;
            DataTable datdoc = objclsInsurance.GetInsuranceDocuments().Tables[0];
            dlListDoc.DataSource = datdoc;
            dlListDoc.DataBind();
            ViewState["ListDoc"] = datdoc;

            if (dlListDoc.Items.Count > 0)
                divListDoc.Style["display"] = "block";
            else
                divListDoc.Style["display"] = "none";
            //if (!Convert.ToBoolean(datData.Rows[0]["IsRenewed"]))
            //{
            //    divRenew.Style["display"] = "block";
            //}
            //divIssueReceipt.Style["display"] = "block";
        }
    }
    public void FillInsuranceCompany()
    {
        objclsInsurance = new clsInsurance();
        DataSet dtsData = objclsInsurance.GetReference();
        if (dtsData.Tables.Count > 1)
        {
            if (dtsData.Tables[1].Rows.Count > 0)
            {

                DataRow dr = dtsData.Tables[1].NewRow();
                dr["Description"] = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"); //"Select";
                dr["InsuranceCompanyID"] = "-1";
                dtsData.Tables[1].Rows.InsertAt(dr, 0);
                ddlInsuranceCompany.DataTextField = "Description";
                ddlInsuranceCompany.DataValueField = "InsuranceCompanyID";
                ddlInsuranceCompany.DataSource = dtsData.Tables[1];
                ddlInsuranceCompany.DataBind();


            }
        }

        if (CurrentSelectedValue != null && CurrentSelectedValue != string.Empty)
        {
            if (ddlInsuranceCompany.Items.FindByValue(CurrentSelectedValue) != null)
                ddlInsuranceCompany.SelectedValue = CurrentSelectedValue;
        }

        upInsuranceCompany.Update();
    } 
    private void fillCompanyAsset(int intCompanyID)
    {
        objclsInsurance = new clsInsurance();
        objclsInsurance.intCompanyID = intCompanyID;
        DataTable datData = objclsInsurance.GetCompanyAsset();
        DataRow dr = datData.NewRow();
        dr["Description"] = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select"); //"Select";
        dr["CompanyAssetID"] = "-1";
        datData.Rows.InsertAt(dr, 0);
        ddlAsset.DataTextField = "Description";
        ddlAsset.DataValueField = "CompanyAssetID";
        ddlAsset.DataSource = datData;
        ddlAsset.DataBind();
        upAsset.Update();
    }
    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
    #endregion

    
}