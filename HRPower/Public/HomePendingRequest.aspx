﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="HomePendingRequest.aspx.cs" Inherits="Public_HomePendingRequest" Title='<%$Resources:Requests,PendingRequests%>' %>


<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">



    <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/dash1.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b>
                <asp:Literal ID="lit1" runat="server" Text='<%$Resources:Requests,PendingRequests%>'></asp:Literal>
            </b>
        </div>
    </div>
 

    <div class="grid">
        <asp:UpdatePanel ID="upPendingRequests" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
                <table  class="table-responsive table" style="overflow:scroll" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td valign="top">
                            <table cellpadding="3" cellspacing="0" id="tblPendingRequests" runat="server" align="right">
                                <tr class="Dashboradlink">
                                    <td>
                                        <asp:TextBox ID="txtRequestedDate" runat="server" CssClass="textbox" AutoPostBack="True"
                                            onkeypress="return false;" OnTextChanged="txtRequestedDate_TextChanged" Width="89px"></asp:TextBox>
                                        <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server"
                                            TargetControlID="txtRequestedDate" WatermarkText='<%$Resources:ControlsCommon,Fromdate%>'
                                            WatermarkCssClass="WmCss">
                                        </AjaxControlToolkit:TextBoxWatermarkExtender>
                                        &nbsp;
                                        <asp:ImageButton ID="ibtnCalender" runat="server" CausesValidation="false" CssClass="imagebutton" style="margin-top: 2px; padding-top: 5px;"
                                            ImageUrl="~/images/Calendar_scheduleHS.png" />
                                        <AjaxControlToolkit:CalendarExtender ID="ajcRequestedDate" runat="server" Format="dd/MM/yyyy"
                                            PopupButtonID="ibtnCalender" TargetControlID="txtRequestedDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRequestedToDate" runat="server" CssClass="textbox" AutoPostBack="True"
                                            onkeypress="return false;" OnTextChanged="txtRequestedToDate_TextChanged" Width="75px"></asp:TextBox>
                                        <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                            TargetControlID="txtRequestedToDate" WatermarkText='<%$Resources:ControlsCommon,Todate%>'
                                            WatermarkCssClass="WmCss">
                                        </AjaxControlToolkit:TextBoxWatermarkExtender>
                                        &nbsp;
                                        <asp:ImageButton ID="ibtnToCalender" runat="server" CausesValidation="false" CssClass="imagebutton"
                                            ImageUrl="~/images/Calendar_scheduleHS.png" style="margin-top: 5px;" />
                                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                                            PopupButtonID="ibtnToCalender" TargetControlID="txtRequestedToDate">
                                        </AjaxControlToolkit:CalendarExtender>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCompany" runat="server" DataTextField="EmployeeName" DataValueField="EmployeeId"
                                            CssClass="dropdownlist" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRequestType" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                            OnSelectedIndexChanged="ddlRequestType_SelectedIndexChanged">
                                            <asp:ListItem Value="-1" Text='<%$Resources:MasterPageCommon,AnyRequest%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Document%>'> </asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Leave%>'></asp:ListItem>
                                            <%-- <asp:ListItem Text='<%$Resources:MasterPageCommon,Attendance%>'></asp:ListItem>--%>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Loan%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,LeaveExtension%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,TimeExtension%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,VacationExtension%>'></asp:ListItem>
                                            <asp:ListItem Value='<%$Resources:MasterPageCommon,CompensatoryOff%>' Text='<%$Resources:MasterPageCommon,CompensatoryOff%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,SalaryAdvance%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Transfer%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Expense%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,VacationLeave%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Resignation%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Ticket%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Travel%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Job%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Asset%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,General%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,Rejoinrequest%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,SalaryCertificate%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,SalaryBankStatement%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,SalaryCardLost%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,SalarySlip%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,VisaLetter%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,NocUmarahVisa%>'></asp:ListItem>
                                            <asp:ListItem Text='<%$Resources:MasterPageCommon,CardLost%>'></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                            DataTextField="Status" DataValueField="StatusId" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                                            <%-- <asp:ListItem Value="-1">Any Status</asp:ListItem>
                                            <asp:ListItem Value="1">Applied</asp:ListItem>
                                            <asp:ListItem Value="3">Rejected</asp:ListItem>
                                            <asp:ListItem Value="4">Pending</asp:ListItem>
                                            <asp:ListItem Value="5">Approved</asp:ListItem>
                                            <asp:ListItem Value="6" >Cancelled</asp:ListItem>
                                            <asp:ListItem Value="7" >Request For cancel</asp:ListItem>
                                            <asp:ListItem Value="10">Forwarded</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlRequestBy" runat="server" DataTextField="EmployeeName" DataValueField="EmployeeId"
                                            CssClass="dropdownlist"  AutoPostBack="True" OnSelectedIndexChanged="ddlRequestBy_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 5px;">
                            <asp:DataList ID="dlPendingRequests" runat="server" BorderWidth="0px" CellPadding="0"
                                Width="100%" OnPreRender="dlPendingRequests_PreRender" OnItemCommand="dlPendingRequests_ItemCommand">
                                <HeaderTemplate>
                                    <div style="float: left; width: 100%;" class="datalistheader">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td align="left" width="25%">
                                                    <%--Requestor--%>
                                                    <asp:Literal ID="lit1" runat="server" Text='<%$Resources:Requests,Requestor%>'></asp:Literal>
                                                </td>
                                                <td width="25%" align="left">
                                                    <%--  Request Type--%>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:Requests,RequestType%>'></asp:Literal>
                                                </td>
                                                <td width="20%" align="left">
                                                    <%--Date--%>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:Requests,Date%>'></asp:Literal>
                                                </td>
                                                <td width="15%" align="left">
                                                    <%--Status--%>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:Requests,Status%>'></asp:Literal>
                                                </td>
                                                <td width="15%" align="right">
                                                    <%--Action--%>
                                                    <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:Requests,Action%>'></asp:Literal>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-top: 5px;
                                        padding-left: 5px;">
                                        <tr class="Dashboradlink">
                                            <td class="datalistTrLeft" align="left" width="25%">
                                                <%--   <a href='<%# "Employee.aspx?EmployeeId=" + Eval("EmployeeId") %>' title="Click here to view employee details">
                                                    <%# Eval("EmployeeName")%></a>--%>
                                                <%# Eval("EmployeeName")%>
                                            </td>
                                            <td width="25%" class="datalistTrRight" align="left">
                                                <asp:UpdatePanel UpdateMode="Conditional" runat="server" ID="upLink">
                                                    <ContentTemplate>
                                                        <asp:LinkButton Style="color: rgb(5,170,230)" CommandName="_ViewReason" CommandArgument='<%# Eval("RequestId")+"," + Eval("ReqTypeId")%>'
                                                            ID="lbReason" runat="server"> <%#Eval("ReqType")%>
                                                <%#Convert.ToString(Eval("Document")) == "" ? "" : "[" + Eval("Document") + "]"%></asp:LinkButton>
                                                        <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td width="20%" class="datalistTrRight" align="left">
                                                <%# Eval("Date")%>
                                            </td>
                                            <td width="15%" class="datalistTrRight" align="left">
                                                <%#GetReuestStatus(Eval("StatusId"), Eval("Forwarded"))%>
                                            </td>
                                            <td width="15%" class="datalistTrRight" align="right">
                                                <%--<asp:HiddenField ID ="hfStausID" runat ="server"  Value ='<%# Eval("StatusId") %>'  />
                                            <asp:HiddenField ID ="hfReqType" runat ="server" Value ='<%# Eval("ReqType") %>' />
                                            <asp:HiddenField ID ="hfForwarded" runat ="server" Value ='<%# Eval("Forwarded") %>' />--%>
                                                <a class="actionbutton" id="ancApprove" runat="server" href='<%# GetRequestLink(Eval("ReqType"), Eval("RequestId"),Eval("StatusId")) %>'
                                                    visible='<%#IsApproveVisible( Eval("RequestId"),  Eval("StatusId"),Eval("Forwarded"),Eval("ReqTypeID"),Eval("EmployeeId")) %>'
                                                    title='<%$Resources:Requests,Clickheretoapprove%>'>Action </a>
                                            </td>
                                        </tr>
                                        <tr style="margin-top: 20px">
                                            <td class="trLeft" style="padding-top: 5px;" width="100%" colspan="5">
                                                <asp:UpdatePanel UpdateMode="Conditional" runat="server" ID="upViewDetails">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                                            Width="90%" Visible="false">
                                                            <HeaderStyle BackColor="LightGray" Font-Size="Smaller" />
                                                            <RowStyle Font-Size="Smaller" />
                                                            <Columns>
                                                                <asp:BoundField DataField="EntryDate" HeaderText='<%$Resources:Requests,Date%>' />
                                                                <asp:BoundField ItemStyle-Width="300px" DataField="EmployeeFullName" HeaderText='<%$Resources:Requests,Authority%>' />
                                                                <asp:BoundField DataField="Description" HeaderText='<%$Resources:Requests,Status1%>' />
                                                                <asp:BoundField DataField="Reason" HeaderText='<%$Resources:Requests,ApprovalDetails%>'
                                                                    ItemStyle-Wrap="true" ItemStyle-Width="300px" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="pgrPendingRequests" PageSize="20" runat="server" OnFill="BindPendingRequests" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: none;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" runat="Server">
    <div style="display: block; width: 100%;" id="divCalendar">
        <div style="display: block; width: 100%; padding: 5px 2px 2px 2px; margin-left: 1px;">
            <asp:UpdatePanel ID="upcalender" runat="server">
                <ContentTemplate>
                    <asp:Calendar ID="Calendar5" Width="98%" runat="server" CellPadding="1" NextMonthText="&amp;gt;&amp;gt;  "
                        PrevMonthText="&amp;lt;&amp;lt;" BorderStyle="None">
                        <SelectedDayStyle ForeColor="Black" BorderColor="#0099FF" />
                        <TodayDayStyle CssClass="CalendarDay1" BorderColor="#CC0066" />
                        <DayStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <DayHeaderStyle CssClass="CalendarDay" />
                        <DayStyle CssClass="CalendarDay1" />
                        <SelectedDayStyle ForeColor="Red" />
                        <TitleStyle CssClass="CalendarDay1" />
                        <NextPrevStyle ForeColor="White" CssClass="CalendarNextPrev" />
                    </asp:Calendar>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="float: left; font-size: 10px; padding: 2px 2px 2px 2px; width: 100%;">
            <img src="../images/green.png" />
            Event
            <img src="../images/red.PNG" />
            Holiday
            <img src="../images/golden.png" />Both
        </div>
    </div>
</asp:Content>
