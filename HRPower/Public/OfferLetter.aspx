﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="OfferLetter.aspx.cs" Inherits="Public_OfferLetter" Title="Offer Letter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Vacancy.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Job%>'></asp:Literal></span></a></li>
            <li><a href='Candidates.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text='<%$Resources:MasterPageCommon,Candidates%>'></asp:Literal></span></a></li>
            <li><a href="InterviewSchedule.aspx">
                <asp:Literal ID="Literal92" runat="server" Text='<%$Resources:MasterPageCommon,Interviewschedule%>'></asp:Literal></a></li>
            <li><a href="InterviewProcess.aspx">
                <asp:Literal ID="Literal93" runat="server" Text='<%$Resources:MasterPageCommon,Interviewprocess%>'></asp:Literal></a></li>
            <li class='selected'><a href="Offerletter.aspx">
                <asp:Literal ID="Literal94" runat="server" Text='<%$Resources:MasterPageCommon,Offerletter%>'></asp:Literal></a></li>
            <li><a href="VisaTicketProcessing.aspx">
                <asp:Literal ID="Literal82" runat="server" Text='<%$Resources:MasterPageCommon,VisaTicketProcessing%>'></asp:Literal></a></li>
            <li><a href='GeneralExpense.aspx'>
                <asp:Literal ID="LiteralGE" runat="server" Text='<%$Resources:SettingsCommon,GeneralExpense%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <style type="text/css">
        .background
        {
            background-color: Transparent;
            filter: alpha(opacity=10);
            opacity: 0.1;
        }
        .updateprogress
        {
            background-position: center center;
            position: absolute;
            width: 31px;
            height: 31px;
            background-image: url(    '../images/loading.gif' );
            background-repeat: no-repeat;
            display: none;
        }
    </style>

    <script type="text/javascript">
        function WidthPreview(source, args, width)  
        {
            var imgPreview = document.getElementById('ctl00_public_content_imgPreview');
            
            imgPreview.style.display = "block";
            imgPreview.src = "../thumbnail.aspx?FromDB=false&isCandidate=true&folder=Temp&file=" + '<%= Session.SessionID %>' + ".jpg&width=" + width + "&t=" + new Date();
        }
   var _updateprogress, _background;        
        function pageLoad(sender, args)
        {
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            if(args._isPartialLoad == false)
            {
                _updateprogress = $get('updateprogress');
                
                _background = document.createElement('div');
                _background.style.display = 'none';
                _background.style.zIndex = 10000;
                _background.className = 'background';
                document.getElementById('ctl00_public_content_container').appendChild(_background);
            }
        }

        function beginRequest(sender, args)
        {
            _updateprogress.style.display = 'block';
            _background.style.display = 'block';

            var bounds = Sys.UI.DomElement.getBounds(document.getElementById('ctl00_public_content_container'));
            var progressbounds = Sys.UI.DomElement.getBounds(_updateprogress);

            var x = bounds.x + Math.round(bounds.width / 2) - Math.round(progressbounds.width / 2);
            var y = bounds.y + Math.round(bounds.height / 2) - Math.round(progressbounds.height / 2);

            _background.style.width = bounds.width + "px";
            _background.style.height = bounds.height + "px";

            Sys.UI.DomElement.setLocation(_updateprogress, x, y);            
            Sys.UI.DomElement.setLocation(_background, bounds.x, bounds.y);
        }

        function endRequest(sender, args)
        {
            _updateprogress.style.display = 'none';
            _background.style.display = 'none';
        }
        
    </script>

    <div id="updateprogress" class="updateprogress">
        &nbsp;
    </div>

    <script type="text/javascript">
        function DisError(source, args)
        {
            alert('Upload not finished yet!!');
        }
        
        function CheckJoiningDate(source, args)
        { 

           var txtJoiningDate = document.getElementById('ctl00_public_content_fvOfferletter_txtDateofJoining');
           
           var val = document.getElementById('ctl00_content_public_content_fvOfferletter_cvJoiningDate');
           
            if(typeof(txtJoiningDate) == 'object')
            {
                if(txtJoiningDate.value == '')
                {
                     source.textContent = "Please enter expected join date."
                     args.IsValid = false;
                     return;
                     
                }
                else
                {
                    if( isDate(txtJoiningDate.value, source) )
                    {
                        args.IsValid = true;
                    }
                    else
                    {
                        args.IsValid = false;
                        return;
                    }
                }
            }
        }
        
        function isValidDocument(source, args)
        {
            if(!isDocument(args._fileName))
            {
                alert('Invalid file type'); 
                
                var arrInputs = source._element.getElementsByTagName("input");
                
                if( arrInputs && arrInputs.length > 0)
                {
                    for(var i = 0; i < arrInputs.length; i++)
                    {
                        arrInputs[i].value = '';
                    }
                }
                
                args._fileName = '';
                return false;                          
            }
       }
        
    </script>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="labeltext">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnSubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="upnlMessage" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="fvOfferletter" EventName="ItemCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div id="container" runat="server">
                    <asp:UpdatePanel ID="upPopuppnl" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:FormView ID="fvOfferletter" runat="server" DefaultMode="Edit" Width="100%" OnDataBound="fvOfferletter_DataBound"
                                OnItemCommand="fvOfferletter_ItemCommand" DataKeyNames="OfferId" CssClass="labeltext">
                                <EditItemTemplate>
                                    <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td width="120px" class="trLeft">
                                                <%--Salary Offered--%>
                                                <asp:Literal ID="Literal20" runat="server" meta:resourcekey="SalaryOffered"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="uppnlSalary" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtSalary" runat="server" CssClass="textbox_disabled" Width="150px"
                                                            Enabled="False"></asp:TextBox>&nbsp;
                                                        <asp:Label ID="lbAnnumCurrency" runat="server"></asp:Label>
                                                        <asp:Literal ID="lblperanum" runat="server" meta:resourcekey="Annum"></asp:Literal>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="dlAllowanceDeduction" EventName="ItemCommand" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <asp:HiddenField ID="hfCurrentDate" runat="server" />
                                                <asp:HiddenField ID="hfCandidateId" runat="server" Value='<%# Eval("CandidateId") %>' />
                                                <asp:HiddenField ID="hfJobId" runat="server" Value='<%# Eval("JobId") %>' />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120px" class="trLeft">
                                                <%--Basic Pay--%>
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="BasicPay"></asp:Literal>
                                            </td>
                                            <%--    onblur="javascript:CheckDefaultValue(this)" onkeypress="return KeyRestrict(event, 'Number', '');"--%>
                                            <td class="trRight">
                                                <asp:TextBox ID="txtBasicPay" runat="server" CssClass="textbox" Width="150px" Text='<%# Eval("BasicPay") %>'
                                                    onblur="CheckDefaultValue(this); calSalaryOffered(this.id,1); " onkeypress="return KeyRestrict(event, 'Number', '');"
                                                    MaxLength="13"></asp:TextBox>&nbsp;
                                                <asp:Label ID="lbBPCurrency" runat="server"></asp:Label>
                                                <asp:RequiredFieldValidator ID="rfvBasicPay" runat="server" ControlToValidate="txtBasicPay"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter basic pay" SetFocusOnError="True"
                                                    meta:resourcekey="Pleaseenterbasicpay" ValidationGroup="Send"></asp:RequiredFieldValidator>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeBasicPay" runat="server" FilterType="Numbers,Custom"
                                                    FilterMode="ValidChars" TargetControlID="txtBasicPay" ValidChars=". ,">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="trLeft" width="120px">
                                                <%-- Particulars--%>
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Particulars"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="upAllowanceDeduction" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td width="95%">
                                                                    <div style="max-height: 300px; overflow: auto;">
                                                                        <asp:DataList ID="dlAllowanceDeduction" runat="server" CellPadding="0" BorderWidth="1px"
                                                                            BorderStyle="Solid" BorderColor="LightBlue" OnItemDataBound="dlAllowanceDeduction_ItemDataBound"
                                                                            DataKeyField="AdditionDeduction" OnItemCommand="dlAllowanceDeduction_ItemCommand"
                                                                            Width="95%">
                                                                            <HeaderStyle CssClass="item_title" />
                                                                            <HeaderTemplate>
                                                                                <table width="100%">
                                                                                    <tr>
                                                                                        <td width="150px">
                                                                                            <%--Allowances/Deductions--%>
                                                                                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AllowancesDeductions"></asp:Literal>
                                                                                        </td>
                                                                                        <td width="100px">
                                                                                            <%--Amount--%><asp:Literal ID="Literal3" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                                                                        </td>
                                                                                        <td>
                                                                                            <%-- Particular Type--%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="ParticularType"></asp:Literal>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <fieldset>
                                                                                    <table width="100%">
                                                                                        <tr>
                                                                                            <td width="150px" class="item_title" valign="top">
                                                                                                <%--   <asp:Label ID="lblAddition" Text ='<%# Eval("AdditionDeduction") %>' runat="server"></asp:Label>--%>
                                                                                                <asp:DropDownList ID="ddlAllowancedeductions" runat="server" Height="20px" Visible="true"
                                                                                                    DataTextField="AllowanceType" DataValueField="AllowanceTypeId" Width="130px"
                                                                                                    OnSelectedIndexChanged="ddlAllowancedeductions_SelectedIndexChanged" onchange="chkDuplicateAllowanceDeduction(this);">
                                                                                                </asp:DropDownList>
                                                                                                <asp:HiddenField ID="hfAdditionDeductionID" runat="server" Value='<%# Eval("AdditionDeductionID") %>' />
                                                                                            </td>
                                                                                            <td width="100px" valign="top">
                                                                                                <div id="txtsmall" style="padding-top: 2px;">
                                                                                                    <asp:TextBox ID="txtAmt" runat="server" Width="80px" Text='<%# Eval("Amount") %>'
                                                                                                        onblur="calSalaryOffered(this.id,0)" MaxLength="13"></asp:TextBox>
                                                                                                </div>
                                                                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftbeAmt" runat="server" FilterType="Numbers,Custom"
                                                                                                    FilterMode="ValidChars" TargetControlID="txtAmt" ValidChars=". ,">
                                                                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                                                                <asp:HiddenField ID="hfPayPolicy" runat="server" Value='<%# Eval("PayPolicy") %>' />
                                                                                                <asp:Label ID="lblPayPolicyName" runat="server" Style="display: none;"></asp:Label>
                                                                                            </td>
                                                                                            <td valign="top">
                                                                                                <asp:RadioButtonList ID="rbtnAllowanceDeduction" runat="server" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem meta:resourcekey="Allowance" Selected="True" Value="1"></asp:ListItem>
                                                                                                    <asp:ListItem meta:resourcekey="Deduction" Value="0"></asp:ListItem>
                                                                                                </asp:RadioButtonList>
                                                                                                <asp:HiddenField ID="hfAddition" runat="server" Value='<%# Eval("IsAddition") %>' />
                                                                                            </td>
                                                                                            <td valign="top">
                                                                                                <asp:ImageButton ID="ibtnRemoveAllowance" runat="server" CausesValidation="False"
                                                                                                    CommandArgument='<%# Eval("AdditionDeduction") %>' CommandName="_Remove" ToolTip="Remove Allowance"
                                                                                                    ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </fieldset>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </div>
                                                                </td>
                                                                <td style="padding-top: 32px; padding-left: 5px" valign="top">
                                                                    <asp:ImageButton ID="ibtnNewAllowance" runat="server" CausesValidation="False" CommandArgument='<%# Eval("AllowanceType") %>'
                                                                        CommandName="_AddAllowance" ToolTip="New Allowance" ImageUrl="~/images/new_icon_reference.PNG" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120px" class="trLeft">
                                                <%--Payment Mode--%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="PaymentMode"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:DropDownList ID="ddlPaymentMode" runat="server" CssClass="dropdownlist" DataTextField="PaymentMode"
                                                    DataValueField="PaymentModeID" onchange="calSalaryOffered(this.id,-1)">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120px" class="trLeft">
                                                <%--Designation--%><asp:Literal ID="Literal5" runat="server" meta:resourcekey="Designation"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:DropDownList ID="ddlDesignation" runat="server" Enabled="false" CssClass="dropdownlist"
                                                    DataTextField="Designation" DataValueField="DesignationID" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120px" class="trLeft" valign="top">
                                                <%--Terms &amp; Conditions--%><asp:Literal ID="Literal6" runat="server" meta:resourcekey="TermsAndConditions"></asp:Literal>
                                            </td>
                                            <td class="trRight" valign="top">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <%--OnClientUploadComplete="isValidDoc"--%>
                                                            <asp:TextBox ID="txtTerms" runat="server" TextMode="MultiLine" Text='<%#  Eval("LegalTerms") %>'
                                                                Width="500px" Height="60px" onchange="RestrictMulilineLength(this, 4000);" onkeyup="RestrictMulilineLength(this, 4000);"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td width="25">
                                                                        <img alt="" id="imgViewAttachment" runat="server" src="../images/Attachment.png"
                                                                            visible="false" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblFileName" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120px" class="trLeft">
                                                <%--  Exp. Joining Date--%>
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ExpJoiningDate"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:TextBox ID="txtDateofJoining" runat="server" CssClass="textbox" Width="80px"
                                                    Text='<%#  Eval("DateofJoining") %>'></asp:TextBox>
                                                <asp:ImageButton ID="ibtnCalender" runat="server" CausesValidation="false" CssClass="imagebutton"
                                                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                <AjaxControlToolkit:CalendarExtender ID="ajcCalenderDateofjoining" runat="server"
                                                    Format="dd/MM/yyyy" PopupButtonID="ibtnCalender" TargetControlID="txtDateofJoining">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDateofJoining"
                                                    meta:resourcekey="Pleaseenterdateofjoining" CssClass="error" Display="Dynamic"
                                                    ErrorMessage="Please enter date of joining" SetFocusOnError="True" ValidationGroup="Send"></asp:RequiredFieldValidator>
                                                <%--  <asp:CustomValidator ID="cvJoiningDate" runat="server" CssClass="error" ErrorMessage="CustomValidator"
                                                Display="Dynamic" ValidationGroup="Send" ClientValidationFunction="CheckJoiningDate"
                                                ControlToValidate="txtDateofJoining"></asp:CustomValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="120px">
                                                <%--Templates--%>
                                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Templates"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="upTemplate" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlTemplates" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                            DataTextField="Template" DataValueField="TemplateId" OnSelectedIndexChanged="ddlTemplates_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="rfvTemplates" runat="server" ControlToValidate="ddlTemplates"
                                                            meta:resourcekey="Pleaseselectatemplate" CssClass="error" Display="Dynamic" ErrorMessage="Please select a template"
                                                            InitialValue="-1" SetFocusOnError="True" ValidationGroup="Send"></asp:RequiredFieldValidator>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft">
                                                <%--To--%>
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="To"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:TextBox ID="txtTo" runat="server" CssClass="textbox_disabled" Width="500px"
                                                    Enabled="False" Text='<%# Eval("Email") %>'></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft">
                                                <%--Cc--%>
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Cc"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:TextBox ID="txtCcto" runat="server" CssClass="textbox" Width="500px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revCcMail" runat="server" ControlToValidate="txtCcto"
                                                    CssClass="error" Display="Dynamic" ErrorMessage="Please enter correct email-id"
                                                    SetFocusOnError="True" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft">
                                                <%--Subject--%><asp:Literal ID="Literal11" runat="server" meta:resourcekey="Subject"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="upSubject" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtSubject" runat="server" CssClass="textbox" Width="500px" Text='<%# Eval("Subject") %>'
                                                            MaxLength="250"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="txtSubject"
                                                            CssClass="error" Display="Dynamic" ErrorMessage="Please enter subject" SetFocusOnError="True"
                                                            meta:resourcekey="Pleaseentersubject" ValidationGroup="Send"></asp:RequiredFieldValidator>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlTemplates" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="trLeft">
                                                <%--Content--%>
                                                <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Content"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="upContent" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <AjaxControlToolkit:Editor ID="ajcOfferEditor" runat="server" Height="300px" Width="550px" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlTemplates" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="fvOfferletter" EventName="DataBound" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr id="trStatus" runat="server">
                                            <td valign="top" class="trLeft">
                                                <%--Status--%>
                                                <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlOfferStatus" runat="server" CssClass="dropdownlist" AutoPostBack="true"
                                                            DataTextField="OfferStatus" DataValueField="OfferStatusId" OnSelectedIndexChanged="ddlOfferStatus_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="fvOfferletter" EventName="DataBound" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <table width="80%">
                                                            <tr id="trRemarks" runat="server" style="display: none">
                                                                <td valign="top" class="trLeft" align="left">
                                                                    <%--Remarks--%>
                                                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                                                </td>
                                                                <td class="trRight" style="padding-left: 40px">
                                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="300px" Height="60px"
                                                                        onchange="RestrictMulilineLength(this, 250);" onkeyup="RestrictMulilineLength(this, 250);"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtRemarks"
                                                                        CssClass="error" Display="Dynamic" ErrorMessage="Please enter remarks" SetFocusOnError="True"
                                                                        meta:resourcekey="Pleaseenterremarks" ValidationGroup="Send"></asp:RequiredFieldValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlOfferStatus" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="fvOfferletter" EventName="DataBound" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" align="right">
                                                <asp:UpdatePanel ID="upButtons" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:HiddenField ID="hfConfirmHistory" runat="server" />
                                                        <asp:Button ID="btnPreview" runat="server" CssClass="btnsubmit" Width="60px" Text='<%$Resources:ControlsCommon,Preview%>'
                                                            CommandName="_Preview" />
                                                        <asp:Button ID="btnSend" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,SaveSend%>'
                                                            CommandName="_Send" Visible="false" ValidationGroup="Send" />
                                                        <asp:Button ID="btnSave" runat="server" CssClass="btnsubmit" Width="60px" Text='<%$Resources:ControlsCommon,Save%>'
                                                            CausesValidation="False" CommandName="_Save" ValidationGroup="Send" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlTemplates" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </EditItemTemplate>
                            </asp:FormView>
                            <asp:DataList ID="dlSentOffers" runat="server" Width="100%" OnItemCommand="dlSentOffers_ItemCommand"
                                DataKeyField="OfferId" CellPadding="0" BorderWidth="0px" OnItemDataBound="dlSentOffers_ItemDataBound"
                                CssClass="labeltext">
                                <ItemStyle CssClass="item" />
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td valign="top" width="25">
                                                <asp:CheckBox ID="chkOffers" runat="server" Enabled='<%# IsHired(Eval("Status")) %>' />
                                            </td>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0">
                                                    <tr>
                                                        <td width="400">
                                                            <asp:LinkButton ID="lnkViewCandidateOffer" runat="server" CssClass="listHeader bold"
                                                                CommandArgument='<%# Eval("CandidateId") %>' CommandName="_EditCandidateOffer">
                                                           <%# Eval("CandidateName")%> </asp:LinkButton><%--[ For <%# Eval("Vacancy")%>] --%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" style="margin-left: 20px;" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td class="innerdivheader">
                                                                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="BasicPay"></asp:Literal>
                                                                                </td>
                                                                                <td align="center">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <%# Eval("BasicPay")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="innerdivheader" width="100px">
                                                                                    <%--Payment Mode--%>
                                                                                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="PaymentMode"></asp:Literal>
                                                                                </td>
                                                                                <td width="10" align="center">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <%# Eval("PaymentMode")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="innerdivheader">
                                                                                    <%-- Date of Joining--%><asp:Literal ID="Literal16" runat="server" meta:resourcekey="DateofJoining"></asp:Literal>
                                                                                </td>
                                                                                <td align="center">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <%#Eval("DateofJoining")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="innerdivheader">
                                                                                    <%-- Sent Date--%><asp:Literal ID="Literal17" runat="server" meta:resourcekey="SentDate"></asp:Literal>
                                                                                </td>
                                                                                <td align="center">
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <%# Eval("SentDate")%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td valign="bottom" align="left" width="150">
                                                                        <span class="statusspan">
                                                                            <%# Eval("Status")%></span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div id="divAttachment" runat="server" style="display: none;">
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td width="20">
                                                                            <img alt="" runat="server" id="imgFile" />
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblAttachment" runat="server"></asp:Label>
                                                                            &nbsp;<a runat="server" id="ancDownload"> Download</a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 20px;" valign="bottom">
                                                            <asp:HiddenField ID="hfJobId" runat="server" Value='<%# Eval("JobId") %>' />
                                                            <%-- <asp:HiddenField ID="hfHistoryCount" runat="server" Value='<%# Eval("HistoryCount") %>' />--%>
                                                            <asp:HiddenField ID="hfStatus" runat="server" Value='<%# Eval("Status") %>' />
                                                            <asp:ImageButton ID="imgOfferLetter" runat="server" ImageUrl="~/images/offer.png"
                                                                CommandArgument='<%# Eval("CandidateId") %>' CommandName="_ViewOffer" meta:resourcekey="OfferLetter"
                                                                CausesValidation="False" />
                                                            &nbsp;
                                                            <asp:ImageButton ID="imgPrint" CommandName="_Print" CausesValidation="false" meta:resourcekey="Print"
                                                                runat="server" ImageUrl="~/images/Print.PNG" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Panel ID="pnlPrint" runat="server" Style="display: none;">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField ID="hfHistory" runat="server" Value="0" />
                                    <div id="divHistory" runat="server" style="display: none;">
                                        <asp:DataList ID="dlHistoryOffers" runat="server" Width="100%" OnItemDataBound="dlHistoryOffers_ItemDataBound"
                                            DataKeyField="OfferId" BorderWidth="0" CellPadding="0">
                                            <HeaderTemplate>
                                                <table width="100%" style="margin-left: 20px;">
                                                    <tr>
                                                        <td valign="top" width="25">
                                                            &nbsp;
                                                        </td>
                                                        <td class="listItem">
                                                            <strong>
                                                                <%-- Offer History--%><asp:Literal ID="Literal17" runat="server" meta:resourcekey="OfferHistory"></asp:Literal>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%" style="margin-left: 20px;">
                                                    <tr>
                                                        <td valign="top" width="25">
                                                            &nbsp;
                                                        </td>
                                                        <td class="listItem">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td width="70" class="item_title">
                                                                                    <%-- Basic Pay--%>
                                                                                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="BasicPay"></asp:Literal>:
                                                                                </td>
                                                                                <td width="120">
                                                                                    <%# Eval("BasicPay")%>
                                                                                </td>
                                                                                <td width="100" class="item_title">
                                                                                    <%-- Payment Mode--%><asp:Literal ID="Literal18" runat="server" meta:resourcekey="PaymentMode"></asp:Literal>
                                                                                    :
                                                                                </td>
                                                                                <td>
                                                                                    <%# Eval("PaymentMode")%>
                                                                                </td>
                                                                                <td valign="top" width="70" class="item_title">
                                                                                    <%--Sent Date--%><asp:Literal ID="Literal19" runat="server" meta:resourcekey="SentDate"></asp:Literal>
                                                                                    :
                                                                                </td>
                                                                                <td valign="top" width="100">
                                                                                    <%# Eval("SentDate")%>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="item_title">
                                                                            <%--Date of Joining--%><asp:Literal ID="Literal21" runat="server" meta:resourcekey="DateofJoining"></asp:Literal>
                                                                            : </span>
                                                                        <%# Eval("DateofJoining")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:DataList ID="dlAllowancedeductions" runat="server" RepeatColumns="4" BorderWidth="0"
                                                                            CellPadding="0">
                                                                            <ItemTemplate>
                                                                                <table border="0" cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <td class="item_title" style="padding-right: 5px;">
                                                                                            <%#Eval("AllowanceName") %>
                                                                                        </td>
                                                                                        <td style="padding-right: 5px;">
                                                                                            :
                                                                                        </td>
                                                                                        <td style="padding-right: 5px;">
                                                                                            <%# Eval("Amount") %>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:DataList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="OfferletterPager" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
        </tr>
    </table>
    <div style="display: none">
        <input type="button" runat="server" id="btn" />
    </div>
    <AjaxControlToolkit:ModalPopupExtender ID="mpePreviewOffer" runat="server" PopupControlID="pnlPreview"
        TargetControlID="btn" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlPreview"
        CancelControlID="ibtnClose">
    </AjaxControlToolkit:ModalPopupExtender>
    <asp:Panel ID="pnlPreview" runat="server" Width="600px">
        <%--Style="display: none;"--%>
        <div id="popupmainwrap">
            <div id="popupmain" style="width: 700px;">
                <div id="header11">
                    <div id="hbox1">
                        <div id="headername" runat="server" style="color: White;">
                            <%--Preview Offer Letter--%>
                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="PreviewOfferLetter"></asp:Literal>
                        </div>
                    </div>
                    <div id="hbox2">
                        <div id="close1">
                            <a href="">
                                <asp:ImageButton ID="ibtnClose" runat="server" ImageUrl="~/Controls/images/icon-close.png" />
                            </a>
                        </div>
                    </div>
                </div>
                <div id="contentwrap">
                    <div id="content">
                        <div style="max-height: 400px; overflow: auto;">
                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td width="100%">
                                        <asp:UpdatePanel ID="upPreview" runat="server">
                                            <ContentTemplate>
                                                <table width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Label ID="lblHeader" runat="server" CssClass="listHeader"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            <asp:Literal ID="literalContent" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="footerwrapper">
                    <div id="footer11">
                        <div id="buttons">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenus" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div>
                    <asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search"></asp:TextBox>
                    <AjaxControlToolkit:TextBoxWatermarkExtender ID="wmDuration" runat="server" TargetControlID="txtSearch"
                        meta:resourcekey="SearchBy" WatermarkText="Search By" WatermarkCssClass="textWmcss"
                        Enabled="True">
                    </AjaxControlToolkit:TextBoxWatermarkExtender>
                </div>
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="btnGo" runat="server" CausesValidation="false" ImageUrl="../images/search.png"
                        meta:resourcekey="Clickheretosearch" ToolTip="Click here to search" ImageAlign="AbsMiddle"
                        OnClick="btnGo_Click" />
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgView" ImageUrl="~/images/View offer letter.png" CausesValidation="false"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkView" runat="server" CausesValidation="false" OnClick="lnkView_Click1">
                            <%--View Offer Letter--%><asp:Literal ID="Literal22" runat="server" meta:resourcekey="ViewOfferLetter"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgSend" ImageUrl="~/images/E-Mail.png" CausesValidation="false"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkSend" runat="server" CausesValidation="false" OnClick="lnkSend_Click1">
                            <%--  Send Offer Letter--%><asp:Literal ID="Literal23" runat="server" meta:resourcekey="SendOfferLetter"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgCancel" ImageUrl="~/images/View offer letter.png" CausesValidation="false"
                        runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkCancelOfferLetter" runat="server" CausesValidation="false"
                            OnClick="lnkCancelOfferLetter_Click">
                            <%--Cancel Offer Letter--%><asp:Literal ID="Literal24" runat="server" meta:resourcekey="CancelOfferLetter"></asp:Literal>
                        </asp:LinkButton></h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
