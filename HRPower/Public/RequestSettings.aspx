﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="RequestSettings.aspx.cs" Inherits="Public_RequestSettings" Title="RequestSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='Company.aspx'>
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:SettingsCommon,Company%>'></asp:Literal></a></li>
            <li><a href='Organizationhierarchy.aspx'>
                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:SettingsCommon,Hierarchy%>'></asp:Literal></a></li>
            <li><a href="Bank.aspx">
                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:SettingsCommon,Bank%>'></asp:Literal></a></li>
            <li><a href="ConfigurationSettings.aspx">
                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:SettingsCommon,Configuration%>'></asp:Literal></a></li>
            <li><a href="Settings.aspx">
                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:SettingsCommon,Users%>'></asp:Literal></a></li>
            <li><a href="RoleSettings.aspx">
                <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:SettingsCommon,Roles%>'></asp:Literal></a></li>
            <li><a href="MailSettings.aspx">
                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:SettingsCommon,Mail%>'></asp:Literal></a></li>
            <li><a href="Templates.aspx">
                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:SettingsCommon,Templates%>'></asp:Literal></a></li>
            <li><a href="Announcements.aspx">
                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:SettingsCommon,Announcements%>'></asp:Literal></a></li>
            <li><a href="HolidayCalendar.aspx">
                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:SettingsCommon,CompanyCalendar%>'></asp:Literal></a></li>
            <li class="selected"><a href="RequestSettings.aspx">
                <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:SettingsCommon,Requestsettings%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="divmain" runat="server" style="padding-bottom: 5px">
                <table runat="server" id="tblPermission" cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                            <div>
                                <div style="display: none">
                                    <asp:Button ID="btnProxy" runat="server" Text="submit" />
                                </div>
                                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                                    BorderStyle="None">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                                    Drag="true">
                                </AjaxControlToolkit:ModalPopupExtender>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft" style="width: 25%;">
                            <%-- Request Type--%>
                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Company"></asp:Literal>
                        </td>
                        <td class="trRight" style="width: 70%;">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlCompany" DataTextField="CompanyName" DataValueField="CompanyID"
                                        AutoPostBack="true" runat="server" Width="216px" 
                                        CssClass="dropdownlist_mandatory" 
                                        onselectedindexchanged="ddlCompany_SelectedIndexChanged" Enabled="False">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlCompany"
                                        CssClass=" error" Display="Dynamic" ErrorMessage="Please select any Company"
                                        InitialValue="-1" ForeColor="" SetFocusOnError="True" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft" style="width: 25%;">
                            <%-- Request Type--%>
                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="RequestType"></asp:Literal>
                        </td>
                        <td class="trRight" style="width: 70%;">
                            <asp:UpdatePanel ID="updReqType" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlRequestType" DataTextField="RequestType" DataValueField="RequestId"
                                        AutoPostBack="true" runat="server" Width="216px" CssClass="dropdownlist_mandatory"
                                        OnSelectedIndexChanged="ddlRequestType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rqddlRequestType" runat="server" ControlToValidate="ddlRequestType"
                                        CssClass=" error" Display="Dynamic" ErrorMessage="Please select any Request Type"
                                        InitialValue="-1" ForeColor="" SetFocusOnError="True" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight">
                            <asp:CheckBox ID="chkReportingTo" runat="server" Checked="true" meta:resourcekey="LineManagerRequired" />
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight">
                            <asp:CheckBox ID="chkHOD" runat="server" Checked="false" Text='<%$Resources:ControlsCommon,IsHODRequired %>'/>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight">
                            <asp:UpdatePanel ID="updIsLevelNeeded" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:CheckBox ID="chkIsLevelNeeded" runat="server" Checked="false" AutoPostBack="true"
                                        meta:resourcekey="RequireMultipleApprovalLevel" OnCheckedChanged="chkIsLevelNeeded_CheckedChanged" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight">
                            <asp:UpdatePanel ID="updIsLevelForwarded" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:CheckBox ID="chlLevelForwarded" runat="server" Checked="false" meta:resourcekey="ForwardingBasedOnLevel" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight" style="width: 70%;">
                            <asp:UpdatePanel ID="updOfferLevel" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td valign ="top" >
                                                <asp:TextBox ID="txtOfferLevel" runat="server" Width="50px" MaxLength="1" OnTextChanged="txtOfferLevel_OnTextChanged"
                                                    AutoPostBack="true" >
                                                </asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="imgShowLevels" runat="server" CausesValidation="false" Text="View Levels"
                                                    CssClass="showlevel" OnClick="imgShowLevels_Click"  ToolTip ="View Levels"/>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:RangeValidator ID="RvOfferLevel" runat="server" ControlToValidate="txtOfferLevel"
                                MinimumValue="1" MaximumValue="5" Display="Dynamic" ValidationGroup="submit"
                                CssClass="error" meta:resourcekey="Levelnumbersshouldbe"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="trLeft">
                        </td>
                        <td class="trRight">
                            <asp:UpdatePanel ID="updLevels" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                   <%-- <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                                        <asp:Literal ID="LitCompany" runat="server" meta:resourcekey="Company"  Visible="false"></asp:Literal></div>
                                    <div class="trRight" style="width: 70%; height: 30px; float: right;">
                                        <asp:DropDownList ID="ddlLevelCompany" DataTextField="CompanyName" DataValueField="CompanyID"
                                            AutoPostBack="true" runat="server" Width="216px" CssClass="dropdownlist_mandatory" Visible="false">
                                        </asp:DropDownList>
                                    </div>--%>
                                    <asp:CheckBox ID="chkEmployees" runat="server" 
                                        meta:ResourceKey="SelectAllEmployees" AutoPostBack="True" 
                                        oncheckedchanged="chkEmployees_CheckedChanged"/>
                                    <asp:DataList ID="dlRequestSettings" runat="server" Width="100%" CellPadding="3"
                                        OnItemDataBound="dlRequestSettings_ItemDataBound" OnItemCommand="dlRequestSettings_ItemCommand"
                                        DataKeyField="EmployeeID" BorderColor="#307296" BorderWidth="1px" GridLines="Both">
                                        <HeaderStyle HorizontalAlign="Left" CssClass="datalistheader" BackColor="#307296" />
                                        <HeaderTemplate>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 95%; height: 50px;" align="center">
                                                        <asp:Literal ID="Literal40" runat="server" meta:resourcekey="PleaseSelectAuthoritiesforApproval"></asp:Literal>
                                                        <p style="font-size: 9px;">
                                                            (<asp:Literal ID="Literal41" runat="server" meta:resourcekey="Followingareinthehierarchy"></asp:Literal>)</p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle BackColor="#17acf6" Font-Bold="True" ForeColor="White" />
                                        <ItemTemplate>
                                            <table style="width: 100%">
                                                <tr style="height: 30px;">
                                                    <td>
                                                        <div style="width: 20%; margin-left: 20px;">
                                                            <asp:Label ID="lblLevel" runat="server" Text='<%# Eval("Level") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div style="width: 100%; margin-left: 144px;">
                                                            <asp:DropDownList ID="ddlAuthority" runat="server" Width="200px" DataTextField="EmployeeFullName"
                                                                AutoPostBack="true" DataValueField="EmployeeID" CssClass="dropdownlist">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rvAuthority" runat="server" CssClass="error" ErrorMessage="Please select an Authority"
                                                                Display="Dynamic" ControlToValidate="ddlAuthority" ValidationGroup="submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                            <asp:HiddenField ID="hfEmpID" runat="server" Value='<%# Eval("EmployeeID") %>' />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnRemove" meta:resourcekey="RemoveLevel" runat="server" CommandArgument='<% # Eval("LevelId") %>'
                                                            CommandName="REMOVE_LEVEL" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr align="right">
                        <td class="trLeft">
                            &nbsp;
                        </td>
                        <td class="trRight">
                            <asp:Button ID="btnSubmit" Width="60" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>'
                                OnClick="btnSubmit_Click" ValidationGroup="Submit" ToolTip='<%$Resources:ControlsCommon,Submit%>' />&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Label ID="lblPermission" runat="server" CssClass="error " Visible="false"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <div style="width: 100%; float: left; padding-top: 10px; padding-bottom: 15px">
        <div style="vertical-align: top; padding: 3px; float: left; width: 10%;">
            <img src="../images/Bulbe 14px.png" />
        </div>
        <div style="text-align: justify; float: left; width: 85%;">
            <span class="labeltext">
                <asp:Literal ID="lit100" runat="server" meta:resourcekey="Description">
                </asp:Literal>
            </span>
        </div>
    </div>
</asp:Content>
