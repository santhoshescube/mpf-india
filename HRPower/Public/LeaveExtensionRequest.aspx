<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="LeaveExtensionRequest.aspx.cs" EnableViewState="true" Inherits="Public_LeaveExtensionRequest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
<style type="text/css">
  .trRight, .firstTrLeft, .firstTrRight
  {
  	margin-bottom: 0px;
  }
  .trRight, .firstTrLeft, .firstTrRight
  {
  	padding: 0px;
  }
  .LlblSpce
  {
  	margin-left:10px;
  	width
  }
 
  	.innerdivheaderValue
  	{
  	width: 75% !important;
  	}
  	.ac_results
  	{
  		background:none !important;
  		border: none !important;
  	}
  	table
  	{
  		width: 100% !important;
  	}
 
</style>
 <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=fvLeaveRequest.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Leave Extension Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>
 <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png" Width= "22px"
            meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <div class="grid" id="divmsg">
        <div style="display: none">
            <asp:Button ID="btnProxy" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
        <%--<asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>--%>
    </div>
    <div class="grid" id="LeaveRequestForm">
        <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:HiddenField ID="hfPostedUrl" runat="server" />
                <asp:FormView ID="fvLeaveRequest" runat="server" Width="100%" DataKeyNames="LeaveTypeId,RequestId,ExtensionTypeId,StatusId,RequestedTo,FromDate,MonthYear"
                    OnDataBound="fvLeaveRequest_DataBound" OnItemCommand="fvLeaveRequest_ItemCommand"
                    CellSpacing="1">
                    <EditItemTemplate>
                        <div id="R1" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%">
                                <%--Extension type--%>
                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Extensiontype"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%">
                                <asp:UpdatePanel ID="upd" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 35%">
                                            <asp:DropDownList ID="ddlExtensionType" runat="server" DataTextField="LeaveExtensionType"
                                                CausesValidation="false" DataValueField="LeaveExtensionTypeId" CssClass="dropdownlist_mandatory"
                                                AutoPostBack="true" OnSelectedIndexChanged="LeaveTypeVisibility" Width="200px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdEType" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        
                        <div id="divVacationType" runat="server" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%">
                                <asp:Literal ID="Literal20" runat="server" meta:resourcekey="ModeofExtension"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%">
                                <asp:UpdatePanel ID="upnlVacationType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:RadioButtonList  ID="rblVacationType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" 
                                            CellPadding="1" CellSpacing="1" OnSelectedIndexChanged="rblVacationType_SelectedIndexChanged"
                                            CausesValidation="false">
                                            <asp:ListItem Selected="True" Value="1" meta:resourcekey="ExtendedVacation" Style="Margin-Right :10px;Margin-left:0px"  ></asp:ListItem>
                                            <asp:ListItem Value="0" meta:resourcekey="ShortenedVacation"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="divleaveType" runat="server" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%--Leave Type--%>
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <div style="float: left; width: 35%;">
                                    <asp:DropDownList ID="ddlLeaveType" runat="server" DataTextField="LeaveType" DataValueField="LeaveTypeID"
                                        CssClass="dropdownlist_mandatory" AutoPostBack="true" OnSelectedIndexChanged="GetBalanceLeave"
                                        CausesValidation="false" Width="200px">
                                    </asp:DropDownList>
                                    <asp:CustomValidator ID="CvLeaveType" runat="server" ValidateEmptyText="true" ControlToValidate="ddlLeaveType"
                                        Display="Dynamic" ClientValidationFunction="ValidateleaveType" ValidationGroup="Submit"
                                        CssClass="error"></asp:CustomValidator>
                                </div>
                                <div style="float: right; width: 20%;">
                                    <%--Balance Leave:--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="BalanceLeave"></asp:Literal>
                                    <div style="float: right; width: 3%;">
                                        <asp:TextBox ID="txtBalanceLeave" runat="server" Enabled="false" CssClass="textbox_disabled"
                                            Width="40px"></asp:TextBox></div>
                                </div>
                            </div>
                        </div>
                        <div id="divVacationChk" runat="server" style="width: auto; display: block;">
                            <div class="trLeft" style="float: left; width: 1%;">
                                &nbsp;
                            </div>
                            <div class="trRight" style="float: left; width: 80%; margin-left: 244px;">
                                <asp:UpdatePanel ID="upnlchk" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 75%;">
                                            <asp:CheckBox ID="chkCO" runat="server" meta:resourcekey="FromComboOff" TextAlign="Left" 
                                                AutoPostBack="true" OnCheckedChanged="chkCO_CheckedChanged" CausesValidation="false" CssClass="ac_results"  />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="divLeaveExtension" runat="server" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%--Period--%>
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Period"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:UpdatePanel ID="updPolicyName" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%;">
                                            <asp:DropDownList ID="ddlPeriod" runat="server" CssClass="dropdownlist_mandatory"
                                                DataTextField="finperiod" DataValueField="FinYearStartDate" Width="200px">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="divTimeExtensionDay" runat="server" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%--Extension From Date--%>
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="ExtensionFromDate"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%;">
                                            <asp:TextBox ID="txtExtensionFromDate" runat="server" CssClass="textbox_mandatory"
                                                AutoPostBack="true" OnTextChanged="GetBalanceLeave" Width="100px" Text='<%# Eval("FromDate") %>'></asp:TextBox>
                                            <asp:HiddenField ID="hfLoanDate" runat="server" Value='<%# Eval("FromDate") %>' />
                                            <asp:ImageButton ID="ibtnLeaveFromdate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                Style="margin-left: 5px;margin-top: 9px;" />
                                        </div>
                                        <div style="float: right; width: 72%; padding-left: 3px">
                                            <AjaxControlToolkit:CalendarExtender ID="extenderLeaveFromDate" runat="server" TargetControlID="txtExtensionFromDate"
                                                PopupButtonID="ibtnLeaveFromdate" Format="dd/MM/yyyy">
                                            </AjaxControlToolkit:CalendarExtender>
                                            <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtExtensionFromDate"
                                                Display="Dynamic" ClientValidationFunction="valRequestFromToDate" ValidationGroup="Submit"
                                                CssClass="error"></asp:CustomValidator>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="divTimeExtensionMonth" runat="server" style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%--Month & Year--%>
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="MonthAndYear"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%;">
                                            <asp:TextBox ID="txtMonthYear" runat="server" CssClass="textbox_mandatory" AutoPostBack="true"
                                                Width="100px"></asp:TextBox>
                                            <asp:ImageButton ID="ibtnMonthYear" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                        </div>
                                        <div style="float: right; width: 72%; padding-left: 3px">
                                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtMonthYear"
                                                PopupButtonID="ibtnMonthYear" Format="MMM-yyyy">
                                            </AjaxControlToolkit:CalendarExtender>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div id="divLastVacation" runat="server" style="width: auto; display: none;">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <asp:Label ID="lblPrevious" runat="server" meta:resourcekey="PreviousVacationDate"></asp:Label><%--meta:resourcekey="MonthAndYear"--%>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <div style="float: left; width: 25%;">
                                    <asp:Label ID="lblPreviousVacation" runat="server" Text='<%# Eval("PreviousVacation") %>'
                                        Width="100px" Style="margin-left:8px"></asp:Label>
                                    <asp:HiddenField ID="hfVacationFromDate" runat="server" />
                                    <asp:HiddenField ID="hfVacationRejoinDate" runat="server" />
                                </div>
                            </div>
                            <div style="float: left; width: 25%;">
                                <asp:Label class="trLeft" ID="lblVacationExtensionToDate" runat="server" meta:resourcekey="ExtensionToDate"></asp:Label>
                            </div>
                            <div style="float:left; width: 70%; padding-right: 32px">
                                <asp:TextBox ID="txtVacationExtensionToDate" runat="server" CssClass="textbox_mandatory"
                                    AutoPostBack="true" OnTextChanged="GetVacationExtensionCount" Width="100px" Style="Margin-Right :-3px"></asp:TextBox>
                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtVacationExtensionToDate"
                                    PopupButtonID="txtVacationExtensionToDate" Format="dd/MM/yyyy">
                                </AjaxControlToolkit:CalendarExtender>
                                <%-- <asp:CustomValidator ID="CustomValidator1" runat="server" ValidateEmptyText="true"
                                    ControlToValidate="txtVacationExtensionToDate" Display="Dynamic" ClientValidationFunction="valRequestFromToDate"
                                    ValidationGroup="Submit" CssClass="error"></asp:CustomValidator>--%>
                            </div>
                        </div>
                        <div style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <asp:Label ID="lbDuration" runat="server"></asp:Label>
                            </div>
                            <div class="trRight" style="float: left; width: 20%;">
                                <asp:TextBox ID="txtDuration" runat="server" CssClass="textbox_mandatory" Width="50px"
                                    OnTextChanged="GetVacationExtensionCount" AutoPostBack="true" onblur="javascript:CheckDefaultValue(this)"
                                    MaxLength="8" Text='<%# Eval("NoofDays") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDuration"
                                    Display="Dynamic" ErrorMessage="*" SetFocusOnError="True"
                                    CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmountExtender" runat="server"
                                    FilterType="Numbers,Custom" TargetControlID="txtDuration" ValidChars=".">
                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                <asp:CustomValidator ID="rgvDuration" runat="server" ControlToValidate="txtDuration"
                                    CssClass="error" ValidateEmptyText="true" ValidationGroup="Submit" ClientValidationFunction="ValidateDuration"></asp:CustomValidator>
                            </div>
                            <div class="trRight" style="float: right; width: 45%;">
                                <div class="trLeft" style="float: left; width: 41%;">
                                    <asp:Label ID="lblBalanceCombo" runat="server" meta:resourcekey="BalanceCompensatoryOff"></asp:Label>
                                </div>
                                <div class="trRight" style="float: left; width: 40%;">
                                    <asp:TextBox ID="txtBalance" Width="20px" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div style="width: 100%; height: 68px; float: left;">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%--Reason--%>
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Reason"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%;">
                                            <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500" CausesValidation="false"></asp:TextBox>
                                        </div>
                                        <div style="float: left; width: 30%; padding-left: 3px; margin-left: 150px;">
                                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                                                meta:resourcekey="Pleaseenterreason" Display="Dynamic" ErrorMessage="*"
                                                SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="width: auto; display: none">
                            <div class="trLeft" style="float: left; width: 25%;">
                                <%-- Status--%>
                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Status"></asp:Literal>
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div style="float: left; width: 25%; height: 29px">
                                            <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                DataValueField="StatusId" Width="200px">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div style="width: auto">
                            <div class="trLeft" style="float: left; width: 25%;">
                                &nbsp;
                            </div>
                            <div class="trRight" style="float: left; width: 70%;">
                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" CssClass="btnsubmit"
                                    meta:resourcekey="Submit" Width="75px" Text="Submit" CommandName="Add" UseSubmitBehavior="false"
                                    CommandArgument='<%# Eval("RequestId") %>' />&nbsp;
                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandName="CancelRequest"
                                    meta:resourcekey="Cancel" Text="Cancel" Width="75px" />
                            </div>
                        </div>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <div class="grid">
                            <table cellpadding="5" cellspacing="0" width="100%">
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <%-- Requested By--%>
                                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="60%">
                                        <asp:Label ID="Label1" Text='<%# Eval("Employee")%>' runat="server"></asp:Label>
                                    </td>
                                    <td class="trRight" width="10%">
                                       <%-- <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                                            meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <%-- Extension Type--%>
                                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="ExtensionType"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:Label ID="lblExtensionType" Text='<%# Eval("ExtensionType")%>' runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdExtensionType" runat="server" Value='<%# Eval("ExtensionTypeId") %>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="ModeofExtension"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <%#Eval("IsExtension")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <asp:Literal ID="Literal18" runat="server" meta:resourcekey="FromComboOff"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <%#Eval("Credit")%> <%--<%# Convert.ToBoolean(Eval("IsCredit")) == true ? "Yes":"No"%>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <asp:Label ID="lblExtension" runat="server" Text='<%# GetLabel(Eval("ExtensionTypeId")) %>'></asp:Label>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:Label ID="lblExtensionFromDate" runat="server" Text='<%# Eval("ExtensionFromDate") %>'> </asp:Label>
                                    </td>
                                </tr>
                                <tr id="trleaveType" runat="server" style="display: none;">
                                    <td class="trLeft" width="25%">
                                        <asp:Label ID="Label2" runat="server" Text="Leave Type" meta:resourcekey="LeaveType"></asp:Label>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("LeaveType") %>'> </asp:Label>
                                        <asp:HiddenField ID="hdFmViewleaveTypeID" runat="server" Value='<%# Eval("LeaveTypeId") %>' />
                                    </td>
                                </tr>
                                <!-- display  days!-->
                                <%--<tr id="trComboOff" runat="server" style="display: none; width: 133%; height: auto;
                                    padding-left: 10px; overflow: scroll;" align="right">
                                    <td>
                                        <asp:GridView ID="gvComboOff" runat="server" AutoGenerateColumns="false" Width="140%"
                                            CellPadding="10" BorderColor="#307296">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="WorkedDay" meta:resourcekey="Datesofextradaysworked" ItemStyle-HorizontalAlign="Center" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <%--Duration--%>
                                        <asp:Label ID="lblDay" runat="server"></asp:Label>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:Label ID="lblDuration" runat="server" Text='<%# Eval("NOofDays") %>'></asp:Label>
                                        <asp:HiddenField ID="HdNOofDays" runat="server" Value='<%# Eval("NOofDays") %>' />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <%-- Reason--%>
                                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%" style="word-break: break-all">
                                        <asp:Label ID="lblReason" runat="server" Text='<%# Eval("Reason") %>'> </asp:Label>
                                        <asp:TextBox ID="txtEditReason" runat="server" Text='<%# Eval("Reason") %>' Visible="false"
                                            Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                            onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                        <%--Status--%>
                                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Status"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                            Visible="false" DataValueField="StatusId">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblStatus" runat="server" Text=' <%# Eval("status") %>'></asp:Label>
                                    </td>
                                </tr>
                                <tr runat="server" style="display: none;" id="trlink">
                                    <td class="trLeft" width="50%" colspan="2">
                                        <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                                            OnClick="lbReason_OnClick"></asp:LinkButton>
                                        <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                    </td>
                                </tr>
                                <tr runat="server" style="display: none;" id="trReasons">
                                    <td class="trLeft" width="75%" colspan="2">
                                        <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296" Width="800px">
                                            <HeaderStyle CssClass="datalistheader" />
                                            <Columns>
                                                <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                                <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                                    ItemStyle-Width="300px" />
                                                <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                                <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr runat="server" style="display: none; width: 100%;" id="trAuthority">
                                            <td class="innerdivheader" style="width: 25%">
                                                <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label>
                                            </td>
                                            <td class="trRight" width="75%">
                                                <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                                    <ItemStyle ForeColor="#307296" Font-Bold="true" />
                                                     <ItemTemplate>
                                                         <asp:Table Width="99%">
                                                            <tr style="width:99%; color:#307296;" >
                                                               <td>                                                              
                                                                    <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>                                                                
                                                                    <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </asp:Table>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                            </td>
                                        </tr>
                                <tr style="display: none">
                                    <td class="trLeft" width="25%">
                                        <%-- Requested To--%>
                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <%# Eval("Requested") %>
                                    </td>
                                </tr>
                                <tr id="trForwardedBy" runat="server" style="display: none">
                                    <%--visible='<%# GetVisibility(Eval("ForwardedBy"))%>'--%>
                                    <td class="trLeft" width="25%">
                                        <%-- Forwarded By--%>
                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="ForwardedBy"></asp:Literal>
                                    </td>
                                    <td class="trRight" width="75%">
                                        <%# Eval("ForwardedBy")%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="trLeft" width="25%">
                                    </td>
                                    <td class="trRight" width="75%">
                                        <asp:Button ID="btnApprove" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                            CommandName="Approve" Visible="false" Width="100px" meta:resourcekey="Submit" />
                                        <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                            CommandName="RequestCancel" Visible="false" Width="120px" Text="Request For Cancel" />
                                        <asp:Button ID="btnRequestForCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                            CommandName="RequestForCancel" Visible="false" Width="150px" meta:resourcekey="Submit" />
                                        <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" Width="80px" Text='<%$Resources:ControlsCommon,Cancel%>'
                                            ToolTip='<%$Resources:ControlsCommon,Cancel%>' CommandName="Cancel" Visible="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="dlLeaveRequest" EventName="ItemCommand" />
                <asp:AsyncPostBackTrigger ControlID="fvLeaveRequest" EventName="ItemCommand" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div id="divUpdateDetails">
        <asp:UpdatePanel ID="upDetails" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:DataList ID="dlLeaveRequest" runat="server" Width="100%" 
                    OnItemCommand="dlLeaveRequest_ItemCommand" OnItemDataBound="dlLeaveRequest_ItemDataBound"
                    CssClass="labeltext" DataKeyField="requestId">
                    <ItemStyle CssClass="labeltext" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td width="25" valign="top" style="padding-left: 5px">
                                    <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkLeave')" />
                                </td>
                                <td style="padding-left: 7px">
                                    <%--Select All--%>
                                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server">
                            <%--onmouseover="showCancel(this);" onmouseout="hideCancel(this);"--%>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td valign="top" rowspan="6" width="25" style="padding-left: 7px">
                                                <asp:CheckBox ID="chkLeave" runat="server" />
                                            </td>
                                            <td width="100%">
                                                <table border="0" cellpadding="3" cellspacing="0" style="width: 97%">
                                                    <tr>
                                                        <td style="width: 857px">
                                                            <asp:LinkButton ID="lnkLeave" runat="server" CssClass="listHeader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                CommandName="_View" CausesValidation="False"><%# Eval("Date")%></asp:LinkButton>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <asp:ImageButton ImageUrl="~/images/edit.png" ID="btnEdit" runat="server" CausesValidation="False"
                                                                meta:resourcekey="TooltipEdit" CommandArgument='<%# Eval("RequestId") %>' CommandName="_Edit"
                                                                Style="display: block;"></asp:ImageButton>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <asp:ImageButton ImageUrl="~/images/cancel.png" ID="btnCancel" runat="server" CausesValidation="False"
                                                                meta:resourcekey="TooltipCancel" CommandArgument='<%# Eval("RequestId") %>' CommandName="_Cancel"
                                                                Style="display: block">
                                                                <%--OnClientClick="return confirm('Do you want to cancel this request?')"--%>
                                                            </asp:ImageButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" style="padding-left: 25px">
                                                <table cellpadding="2" cellspacing="0" border="0" width="90%">
                                                    <tr>
                                                        <td class="innerdivheader" width="30%" valign="top">
                                                            <%-- Extension Type--%>
                                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="ExtensionType"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue">
                                                            <%# Eval("ExtensionType") %>
                                                            <asp:HiddenField ID="hdExtensionTypeId" runat="server" Value='<%# Eval("ExtensionTypeId") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" valign="top" class="innerdivheader">
                                                            <asp:Label ID="lblExtension" runat="server" Text='<%# GetLabel(Eval("ExtensionTypeId")) %>'></asp:Label>
                                                            <%--  Extension From Date--%>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue">
                                                            <%# Eval("ExtensionFromDate")%>
                                                            <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr id="trLeaveTypedl" runat="server" style="display: none">
                                                        <td width="30%" valign="top" class="innerdivheader">
                                                            <%-- Leave Type--%>
                                                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="LeaveType"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue">
                                                            <%# Eval("LeaveType") %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" valign="top" class="innerdivheader">
                                                            <asp:Label ID="lbDlDuration" runat="server"></asp:Label>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue">
                                                            <%# Eval("NOofDays") %>
                                                            <asp:HiddenField ID="hdLeaveTypeID" runat="server" Value='<%# Eval("LeaveTypeId") %>' />
                                                            <asp:HiddenField ID="hdNOofDays" runat="server" Value='<%# Eval("NOofDays") %>' />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" valign="top" class="innerdivheader">
                                                            <%-- Reason--%>
                                                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue" style="word-break: break-all">
                                                            <%# Eval("Reason") %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" valign="top" class="innerdivheader" style="margin-top: 15px;">
                                                            <%--Status--%>
                                                            <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%" style="margin-top: 15px;">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue" style="margin-top: 15px;">
                                                            <%# Eval("status")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%" valign="top" class="innerdivheader">
                                                            <%--Requested To--%>
                                                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="2%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" class="innerdivheaderValue" style="word-break: break-all">
                                                            <%# Eval("Requested") %>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="LeaveExtensionRequestPager" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="dlLeaveRequest" EventName="ItemCommand" />
                <asp:AsyncPostBackTrigger ControlID="fvLeaveRequest" EventName="ItemCommand" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upnlMenus" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <img id="imgAdd" imageurl="~/images/Insurance_add.png" runat="server" src="../images/Add leave extension request.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" CausesValidation="False"
                                meta:resourcekey="RequestExtension">
                            <%--Request Extension--%>
                            
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <img src="../images/View leave extension request.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" CausesValidation="False" OnClick="lnkView_Click"
                                meta:resourcekey="ViewRequests">
                     
                    <%-- View Requests--%>  
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <img src="../images/Delete.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" CausesValidation="False"
                                meta:resourcekey="DelRequest"> 
                          <%-- Delete Request--%>
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                        
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
