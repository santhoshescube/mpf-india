﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="PerformanceTemplate.aspx.cs" Inherits="Public_PerformanceTemplate" Title="Performance Template" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <style type="text/css">
        .style1
        {
            color: #696C6D;
            font-family: verdana;
            font-size: 13px;
            height: 29px;
            width: 193px;
        }
        .style2
        {
            height: 29px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
    <div id='cssmenu'>
       <%-- <ul>
            <li><a href="SalesPerformance.aspx">Target Performance</a></li>
            <li><a href="AttendancePerformance.aspx">Attendance Performance</a></li>
            <li class='selected'><a href="PerformanceTemplate.aspx"><span>Performance Template </span></a></li>
            <li><a href="PerformanceInitiation.aspx"><span>Performance Initiation</span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>Performace Evaluation</span></a></li>
            <li><a href="PerformanceAction.aspx"><span>Performance Action</span></a></li>
            
        </ul>--%>
        
        
        
             <ul>
            <li><a href="SalesPerformance.aspx">
                <%--Target Performance--%>
                <asp:Literal ID="Lit1" runat="server" Text='<%$Resources:MasterPageCommon,TargetPerformance %>'>
                </asp:Literal>
            </a></li>
            <li><a href="AttendancePerformance.aspx">
                <%--Attendance Performance--%>
                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:MasterPageCommon,AttendancePerformance %>'>
                </asp:Literal>
            </a></li>
            <li class='selected'><a href="PerformanceTemplate.aspx"><span>
                <%--  Performance Template --%>
                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceTemplate %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceInitiation.aspx"><span>
                <%--        Performance Initiation--%>
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceInitiation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceEvaluation.aspx"><span>
                <%-- Performace Evaluation--%>
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceEvaluation %>'>
                </asp:Literal>
            </span></a></li>
            <li ><a href="PerformanceAction.aspx">
                <%--        
            Performance Action--%>
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,PerformanceAction %>'>
                </asp:Literal>
            </a></li>
        </ul>
        
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">



<%--<script type ="text/javascript" >
function ValidateMark1(sender, args)
{
debugger;

    var rdbMark = document.getElementById(sender.id.replace('CvMark', 'rdMark'));  
    var txtmark = document.getElementById(sender.id.replace('CvMark', 'txtMaxMark')); 
     var cmp = document.getElementById('ctl00_public_content_cmp'); 
    if(rdbMark.checked)
    {
     if(txtmark.value =="")
     {
          sender.innerHTML = 'Please enter a mark'; 
          
           args.IsValid = false;   
     }
     
      if(cmp)
      {
      
        Page_ClientValidate();
   
      
      }
          
          
          
    }
    else
    
    {
      
            args.IsValid = true;   
    }
} 

</script>
--%>






 <div id="divPopUp">
        <div style="display: none">
            <asp:Button ID="btnMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="ucMessage" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:UpdatePanel ID="updMessage" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="dvViewHavePermission" runat="server">
                <asp:UpdatePanel ID="upnlAdd" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divAdd" runat="server" style="width: 100%; display: block; height: auto;">
                            <div style="width: 100%; padding-bottom: 10px;">
                                <div style="width: 100%; padding-bottom: 10px;">
                                    <div id="Div1" style="color: #30a5d6">
                                        <img src="../images/Interview Schedule.png" />
                                    <%--    <b>Performance Template</b>--%>
                                        
                                          <b> <asp:Literal ID="Literal1" runat ="server" meta:resourcekey="TemplateName" ></asp:Literal>                                        </b>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div style="height: 29px; width: 100%">
                                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                               <%--     Template Name--%>
                                    
                                   <asp:Literal ID="Literal2" runat ="server" meta:resourcekey="TemplateName" ></asp:Literal> 
                                    
                                </div>
                                <div class="trRight" style="float: left; width: 70%; height: 29px;">
                                    <div style="float: left; width: 35%;">
                                        <asp:TextBox ID="txtTemplateName" runat="server" Width="250px" MaxLength="200"></asp:TextBox>
                                    </div>
                                    <div style="float: right; width: 40%; padding-left: 1px">
                                        <asp:RequiredFieldValidator ID="rfvtemplateName" runat="server" ControlToValidate="txtTemplateName"
                                            CssClass="error" Display="Dynamic" meta:resourcekey="Pleaseentertemplatename"
                                            ValidationGroup="ValTemplate"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 29px; width: 100%">
                                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                 <%--   Department--%>
                                 
                                 <asp:Literal ID="Literal3" runat ="server" meta:resourcekey="Department" ></asp:Literal>
                                </div>
                                <div class="trRight" style="float: left; width: 70%; height: 29px;">
                                    <div style="float: left; width: 35%;">
                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="dropdownlist_mandatory"
                                            Width="250px" AutoPostBack="true" DataValueField="DepartmentID" DataTextField="Department">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div style="height: 29px; width: 100%">
                                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                 <%--   Set Mark/Grade--%>
                                 
                                 <asp:Literal ID="Literal4" runat ="server" meta:resourcekey="SetMarkPerGrade" ></asp:Literal>
                                 
                                </div>
                                <div class="trRight" style="float: left; width: 70%; height: 29px;">
                                    <asp:RadioButtonList ID="rdlist" runat="server" OnSelectedIndexChanged="rdlist_SelectedIndexChanged"
                                        RepeatDirection="Horizontal" AutoPostBack="true">
                                      <%--  <asp:ListItem>Mark</asp:ListItem>
                                        <asp:ListItem>Grade</asp:ListItem>--%>
                                         <asp:ListItem meta:resourcekey="Mark"></asp:ListItem>
                                        <asp:ListItem meta:resourcekey="Grade"></asp:ListItem>
                                        
                                        
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div id="divgrades" style="height: 29px; width: 100%; display: none; padding-left: 20px"
                                runat="server">
                                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                </div>
                                <div class="trRight" style="float: left; width: 70%; height: 29px;">
                                    <ul id="navlist">
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <li>
                                                    <%#Eval("Grade")%>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                            <div id="divSetMaxmarks" style="height: 50px; width: 100%; display: none" runat="server">
                                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                                  <%--  Maximum marks/Goal--%>
                                  
                                  <asp:Literal ID="Literal5" runat ="server" meta:resourcekey="MaximummarksPerGoal" ></asp:Literal>
                                  
                                  
                                </div>
                                <div class="trRight" style="float: left; width: 70%; height: 29px;">
                                    <div style="float: left; width: 30%;">
                                        <asp:TextBox ID="txtMaxMark" runat="server" MaxLength="5" ValidationGroup="ValTemplate"></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteMaxMark" runat="server" FilterType="CUSTOM, Numbers"
                                            ValidChars="." FilterMode="ValidChars" TargetControlID="txtMaxMark" />
                                    </div>
                                    <div style="float: right; width: 30%; padding-left: 1px">
                                        <asp:CompareValidator ID="cmp" Display="Dynamic" runat="server" ControlToValidate="txtMaxMark"
                                            Type="Double" ErrorMessage="Enter valid number" ValidationGroup="ValTemplate">
                                        </asp:CompareValidator>
                                        <%--  <asp:CustomValidator ID="CvMark" runat="server" ValidateEmptyText="true" ControlToValidate="txtMaxMark"
                                                Display="Dynamic" ClientValidationFunction="ValidateMark" ValidationGroup="ValTemplate"
                                                CssClass="error"></asp:CustomValidator>--%>
                                    </div>
                                </div>
                            </div>
                            <div id="divAddQuestions" style="height: auto; width: 100%; padding-bottom: 20px">
                                <fieldset style="padding: 5px 5px 10px 5px; border-radius: 5px; margin-left: 10px;float:left">
                                 <%--   <legend>Add Goal</legend>--%>
                                    
                                 <legend>     
                              <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="AddGoal" ></asp:Literal>
                              </legend>

                                    
                                    
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left: 10px">
                                        <tr style="padding-top: 5px">
                                            <td valign="top" width="175px">
                                           <div>
                                                <asp:TextBox ID="txtQuestion" runat="server" CssClass="textbox_mandatory" Width="600px"
                                                    Height="30px" TextMode="MultiLine" MaxLength="2000"></asp:TextBox>
                                                    </div>
                                                <div style="clear: both;margin-top:38px">
                                                    <asp:RequiredFieldValidator ID="rfvQuestion" runat="server" ControlToValidate="txtQuestion"
                                                        CssClass="error" Display="Dynamic"         meta:resourcekey="Pleaseentergoal" ValidationGroup="AddQuestions"></asp:RequiredFieldValidator>
                                                    <%--  <asp:CustomValidator ID="cvotherdocnameduplicate" runat="server" ClientValidationFunction="validateotherdocnameduplicate"
                                                                            ControlToValidate="txtDocname" ValidationGroup="AddDocs" Display="Dynamic" ErrorMessage="Duplicate document name"></asp:CustomValidator>--%>
                                                </div>
                                            </td>
                                            <td valign="top" width="30px">
                                                <asp:LinkButton ID="btnAddQusetions" Enabled="true" runat="server" ValidationGroup="AddQuestions"
                                                    OnClick="btnAddQusetions_Click" Width="28px" Height="15px"  ToolTip='<%$Resources:ControlsCommon,Save%>'     ><img src="../images/Save Blue.png" style="margin-top:12px;" /> </asp:LinkButton>
                                            </td>
                                            <td valign="top" width="20px">
                                                <asp:LinkButton ID="lnkCancel" Enabled="true" runat="server" ValidationGroup="AddQuestions"
                                                    Width="28px" Height="15px" ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClick="lnkCancel_Click"><img src="../images/cancel.png" style="margin-top:12px;" /> </asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            <%--  <fieldset style="padding: 3px 1px;;border-radius:5px;margin-left:10px;">
                                                            <legend>Select Goals</legend>--%>
                            <div id="divQuestionsList" runat="server" class="container_content" style="display: block;
                                height: 300px; overflow: auto; padding-top: 20px; padding-left: 20px;clear:both">
                                <%--   <asp:UpdatePanel ID="updQuestions" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>--%>
                                <asp:DataList ID="dlQuestionsList" runat="server" Width="95%" GridLines="Horizontal"
                                    DataKeyField="GoalID" CssClass="labeltext" OnItemCommand="dlQuestionsList_ItemCommand"
                                    OnItemDataBound="dlQuestionsList_ItemDataBound">
                                    <ItemStyle CssClass="labeltext" />
                                    <HeaderStyle CssClass="listItem" />
                                    <HeaderTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td width="25" valign="top" style="padding-left: 5px">
                                                    <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkGoals')" />
                                                </td>
                                                <td width="110" align="left">
                                             <%--       <b>GOALS</b>--%>
                                             
                                             <b>
                                             
                                              <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="Goals" />
                                             </b>
                                                </td>
                                                <td width="20" style="float: right;">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td valign="top" rowspan="6" width="25" style="padding-left: 7px">
                                                    <asp:CheckBox ID="chkGoals" runat="server" />
                                                </td>
                                                <td width="90%" align="left" style="word-break: break-all;">
                                               <asp:Label ID="lblQuestions" runat="server" Text='<%# Eval("Goal")%>' Width="100%"></asp:Label>
                                                </td>
                                                <td width="5%" valign="top" align="right" style="padding-right: 10px">
                                                    <asp:ImageButton ID="imgEditGoal" runat="server" CausesValidation="False" CommandArgument='<%# Eval("GoalID") %>'
                                                        CommandName="Edit_goal" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>'            />
                                                    <asp:HiddenField ID="hdGoalEditID" runat="server" Value='<%# Eval("GoalID") %>' />
                                                </td>
                                                <td width="5%" valign="top" align="right" style="padding-right: 10px">
                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/delete_icon.png"
                                                        CommandName="REMOVE_Question" CommandArgument='<% # Eval("GoalID") %>'
                                                        
                                                        ToolTip='<%$Resources:ControlsCommon,Delete%>'
                                                        
                                                         />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                                <%--</ContentTemplate>
                                                        </asp:UpdatePanel>--%>
                            </div>
                            <%--     </fieldset>--%>
                            <div id="Div2" runat="server" style="width: 100%; display: block; height: auto; padding-top: 20px">
                                <div align="right" colspan="2">
                                    <asp:Button ID="btnSubmit" runat="server" Text='<%$Resources:ControlsCommon,Submit%>' CssClass="btnsubmit" Width="75px"
                                        ValidationGroup="ValTemplate" OnClick="btnSubmit_Click" />
                                    <asp:Button ID="btnCancel" runat="server" Text='<%$Resources:ControlsCommon,Cancel%>' CssClass="btnsubmit" Width="75px"
                                        OnClick="btnCancel_Click" />
                                    <asp:HiddenField ID="hdntemplateID" runat="server" />
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnTemplateEditID" runat="server" />
                            <asp:HiddenField ID="hdnAddMode" runat="server" />
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlView" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divView" runat="server" style="width: 100%; display: block;">
                            <asp:DataList ID="dlTemplateList" runat="server" Width="100%" DataKeyField="TemplateID"
                                CssClass="labeltext" OnItemDataBound="dlTemplateList_ItemDataBound" OnItemCommand="dlTemplateList_ItemCommand">
                                <ItemStyle CssClass="item" />
                                <HeaderStyle CssClass="listItem" />
                                <HeaderTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td width="25" valign="top" style="padding-left: 5px; padding-top: 5px;">
                                                <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkTemplate')" />
                                            </td>
                                            <td style="padding-left: 7px">
                                              <%--  Select All--%>
                                                <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="SelectAll"/>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server">
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td valign="top" rowspan="5" width="25" style="padding-left: 7px; padding-top: 5px;">
                                                            <asp:CheckBox ID="chkTemplate" Style="margin-top: 5px;" runat="server" />
                                                        </td>
                                                        <td width="100%">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left: 7px">
                                                                <tr>
                                                                    <td width="150">
                                                                        <asp:LinkButton ID="lnkView" runat="server" CommandArgument='<%# Eval("TemplateID") %>'
                                                                            CommandName="View" CausesValidation="False" Style="color: #30a5d6; font-weight: bold;">
                                                               <%# Eval("TemplateName")%>  </asp:LinkButton>
                                                                    </td>
                                                                    <td valign="top" align="right">
                                                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("TemplateID") %>'
                                                                            CommandName="Edit" ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>' />
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="padding-left: 25px">
                                                            <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                                <tr>
                                                                    <td width="30%" valign="top" class="innerdivheader">
                                                                       <%-- Template Name--%>
                                                                       
                                                                            <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="TemplateName"/>
                                                                    </td>
                                                                    <td valign="top" width="5%">
                                                                        :
                                                                    </td>
                                                                    <td align="left" valign="top" width="65%">
                                                                        <%# Eval("TemplateName")%>
                                                                        <asp:HiddenField ID="hdIsGrading" runat="server" Value='<%# Eval("IsGrading")%>' />
                                                                        <asp:HiddenField ID="hddlTemplateID" runat="server" Value='<%# Eval("TemplateID")%>' />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="30%" valign="top" class="style1">
                                                                      <%--  Department--%>
                                                                           <asp:Literal ID="Literal7" runat ="server" meta:resourcekey="Department" />
                                                                    </td>
                                                                    <td valign="top" width="5%" class="style2">
                                                                        :
                                                                    </td>
                                                                    <td align="left" valign="top" width="65%" class="style2">
                                                                        <%# Eval("Department")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="30%" valign="top" class="innerdivheader">
                                                                     <%--   Evaluation Scheme--%>
                                                                          <asp:Literal ID="Literal8" runat ="server" meta:resourcekey="EvaluationScheme" />
                                                                    </td>
                                                                    <td valign="top" width="5%">
                                                                        :
                                                                    </td>
                                                                    <td align="left" valign="top" width="65%">
                                                                      
                                                                        
                                                                        
                                                                        
                                                                       <asp:Literal ID="LitEvaluation" runat ="server" Text ='  <%# Eval("EvaluationScheme")%>'>
                                                                       </asp:Literal> 
                                                                        
                                                                     
                                                                        
                                                                        
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr id="trGradeScheme" style="display: none;" runat="server">
                                                                    <td width="30%" valign="top" class="innerdivheader">
                                                                       <%-- Grades--%>
                                                                            <asp:Literal ID="Literal9" runat ="server" meta:resourcekey="Grades" />
                                                                    </td>
                                                                    <td width="5%">
                                                                        :
                                                                    </td>
                                                                    <td valign="top" align="left" width="65%">
                                                                        <%# Eval("Grades")%>
                                                                    </td>
                                                                </tr>
                                                                <tr id="trMarkScheme" style="display: none;" runat="server">
                                                                    <td width="30%" valign="top" class="innerdivheader">
                                                                        <%--Maximum Marks/Goal--%>
                                                                             <asp:Literal ID="Literal10" runat ="server" meta:resourcekey="MaximumMarksPerGoal" />
                                                                    </td>
                                                                    <td width="5%">
                                                                        :
                                                                    </td>
                                                                    <td valign="top" align="left" width="65%">
                                                                        <%# Eval("MaxMarksPerQuestion")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="30%" valign="top" class="innerdivheader">
                                                                       <%-- No Of Goals--%>
                                                                            <asp:Literal ID="Literal11" runat ="server" meta:resourcekey="NoOfGoals" />
                                                                    </td>
                                                                    <td width="5%">
                                                                        :
                                                                    </td>
                                                                    <td valign="top" align="left" width="65%">
                                                                        <%# Eval("NoOfQuestions")%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                            <uc:Pager ID="Templatepager" runat="server" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="dlTemplateList" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlSingleView" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divSingleView" runat="server" style="display: none; width: 100%">
                            <div class="trLeft" style="float: left; width: 25%; height: 20px">
                              <%--  TemplateName--%>
                                   <asp:Literal ID="Literal12" runat ="server" meta:resourcekey="TemplateName" />
                            </div>
                            <div class="trRight" id="divTemplateName" runat="server" style="float: left; color: Black;
                                width: 70%; height: 20px">
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 20px">
                               <%-- Department--%>
                                    <asp:Literal ID="Literal13" runat ="server" meta:resourcekey="Department" />
                            </div>
                            <div class="trRight" id="divDepartment" runat="server" style="float: left; color: Black;
                                width: 70%; height: 20px">
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 20px">
                               <%-- Evaluation Scheme--%>
                                    <asp:Literal ID="Literal14" runat ="server" meta:resourcekey="EvaluationScheme" />
                            </div>
                            <div class="trRight" id="divEvaluationScheme" runat="server" style="float: left;
                                color: Black; width: 70%; height: 20px">
                            </div>
                            <div id="divMarkScheme" style="display: none;" runat="server">
                                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                                   <%-- Maximum Marks/Question--%>
                                        <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="MaximumMarksPerQuestion" />
                                </div>
                                <div class="trRight" id="divMaxMarks" runat="server" style="float: left; color: Black;
                                    width: 70%; height: 20px">
                                </div>
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 20px">
                               <%-- No Of Goals--%>
                                    <asp:Literal ID="Literal16" runat ="server" meta:resourcekey="NoOfGoals" />
                            </div>
                            <div class="trRight" id="divNoOfQuestions" runat="server" style="float: left; color: Black;
                                width: 70%; word-break: break-all; height: auto; max-height: 250px">
                            </div>
                            <div class="trLeft" style="float: left; width: 25%; height: 20px; padding-top: 1px;
                                padding-bottom: 5px">
                                <asp:Label ID="lblGOals" CssClass="labeltext" runat="server" ForeColor="Black" Text='GOALS'
                                    Font-Bold="true"></asp:Label>
                            </div>
                            <asp:DataList ID="dlShowGoals" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="Solid" BorderWidth="1px" RepeatLayout="Table" CellPadding="3" GridLines="Both">
                                <ItemStyle  />
                                <HeaderStyle Font-Bold="true" ForeColor="Black" />
                                <HeaderTemplate>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 7%">
                                               <%-- Sl No--%>
                                                    <asp:Literal ID="Literal6" runat ="server" meta:resourcekey="SlNo" />
                                            </td>
                                            <td style="width: 93%">
                                               <%-- Goal--%>
                                                    <asp:Literal ID="Literal17" runat ="server" meta:resourcekey="Goal" />
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 7%">
                                                <%# Eval("RowNumber")%>
                                            </td>
                                            <td style="width: 93%; word-break: break-all;">
                                                <%# Eval("Goal")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="dvNoPermission" runat="server" style="margin-top: 10px; display: none; text-align: center;
                width: 100%">
                <asp:Label ID="lblNoPermission" runat="server" CssClass="error" Text="You have no permission to view this page">
                </asp:Label>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add_DocumentRequestBig.png" runat="server" CausesValidation="false" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkAdd" runat="server"   meta:resourcekey="AddTemplate" onclick="lnkAdd_Click" > 
		   
	            <%--    Add Template--%>
	                
	               </asp:LinkButton> </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgView" ImageUrl="~/images/View document request.png" runat="server" CausesValidation="false"/>
                </div>
                <div class="name">
                    <h5> <asp:LinkButton ID="lnkView" runat="server" meta:resourcekey="ViewTemplates"  
                            onclick="lnkView_Click" >
		
	             <%--   View Templates--%>
	                
	                </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" CausesValidation="false"/>
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkDelete" runat="server" Text='<%$Resources:ControlsCommon,Delete %>' onclick="lnkDelete_Click"  >
		
	                   <%-- Delete--%>
	                    
	                    </asp:LinkButton>
	                    </h5>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

