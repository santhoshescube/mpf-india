﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="TrainingSchedule.aspx.cs" Inherits="Public_TrainingSchedule" Title="Training Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../css/Boostrap TimePicker/bootstrap-timepicker.css" rel="stylesheet"
        type="text/css" />

    <script src="../js/jquery/Boostrap TimePicker/bootstrap-timepicker.js" type="text/javascript"></script>

    <script src="../js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>

    <script src="../js/jquery/Boostrap TimePicker/bootstrap-datepicker.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <div id='cssmenu'>
        <ul>
            <li><a href='TrainingCourse.aspx'><span>
                <asp:Literal ID="Literal90" runat="server" Text="Training Course"></asp:Literal></span></a></li>
            <li class='selected'><a href='TrainingSchedule.aspx'><span>
                <asp:Literal ID="Literal91" runat="server" Text="Training Schedule"></asp:Literal></span></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal92" runat="server" Text="Attendance"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal93" runat="server" Text="Feedback"></asp:Literal></a></li>
            <li><a href="#">
                <asp:Literal ID="Literal94" runat="server" Text="Employee Review"></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" runat="Server">
    <div id="div1" class="TabbedPanels" style="width: 100%;">
        <ul class="TabbedPanelsTabGroup">
            <li class="TabbedPanelsTabSelected" id="pnlTab1" onclick="SetTrainingTab(this.id)">
                <asp:Literal ID="Literal11" runat="server" meta:ResourceKey="Schedule"></asp:Literal>
            </li>
            <li class="TabbedPanelsTab" id="pnlTab2" onclick="SetTrainingTab(this.id)">
                <asp:Literal ID="Literal53" runat="server" meta:ResourceKey="Registration"></asp:Literal>
            </li>
            <li class="TabbedPanelsTab" id="pnlTab3" onclick="SetTrainingTab(this.id)">
                <asp:Literal ID="Literal54" runat="server" meta:ResourceKey="Feedback"></asp:Literal>
            </li>
        </ul>
    </div>
    <div class="TabbedPanelsContentGroup">
        <div class="TabbedPanelsContent" id="divTab1" style="display: block;">
            <div id="divAdd" runat="server" style="width: 100%; float: left; padding-top: 20px;">
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal8" runat="server" meta:ResourceKey="ScheduleName"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtSchedule" runat="server" CssClass="textbox_mandatory" Width="221px"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal1" runat="server" meta:ResourceKey="Course"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:DropDownList ID="ddlCourse" runat="server" Height="25px" Width="222px">
                        <asp:ListItem Text="Training Course1"></asp:ListItem>
                        <asp:ListItem Text="Training Course2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal12" runat="server" meta:ResourceKey="Purpose"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 60px; float: left;">
                    <asp:TextBox ID="txtPurpose" runat="server" CssClass="textbox_mandatory" TextMode="MultiLine"
                        Width="320px" Height="57px"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal4" runat="server" meta:ResourceKey="Startdate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <div style="width: 34%; height: 30px; float: left;">
                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                            MaxLength="10"></asp:TextBox>
                        <asp:ImageButton ID="btnFromDate" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <AjaxControlToolkit:CalendarExtender ID="AjaxCalDate" runat="server" Format="dd/MM/yyyy"
                            PopupButtonID="btnFromDate" TargetControlID="txtStartDate" Enabled="True">
                        </AjaxControlToolkit:CalendarExtender>
                    </div>
                    <div style="width: 60%; height: 30px; float: left;">
                        <div class="trLeft" style="width: 31%; height: 30px; float: left;">
                            <asp:Literal ID="Literal13" runat="server" meta:ResourceKey="StartTime"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left; vertical-align: text-top">
                            <div class="input-append bootstrap-timepicker">
                                <input id="txtStartTime" style="width: 50px" data-template="dropdown" data-minute-step="10"
                                    data-disable-focus="false" data-modal-backdrop="true" type="text" class="input-small"
                                    runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal5" runat="server" meta:ResourceKey="Enddate"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <div style="width: 34%; height: 30px; float: left;">
                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                            MaxLength="10"></asp:TextBox>
                        <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                            PopupButtonID="btnFromDate" TargetControlID="txtStartDate" Enabled="True">
                        </AjaxControlToolkit:CalendarExtender>
                    </div>
                    <div style="width: 60%; height: 30px; float: left;">
                        <div class="trLeft" style="width: 31%; height: 30px; float: left;">
                            <asp:Literal ID="Literal10" runat="server" meta:ResourceKey="EndTime"></asp:Literal>
                        </div>
                        <div class="trRight" style="width: 60%; height: 30px; float: left; vertical-align: text-top">
                            <div class="input-append bootstrap-timepicker">
                                <input id="txtEndTime" style="width: 50px" data-template="dropdown" data-minute-step="10"
                                    data-disable-focus="false" data-modal-backdrop="true" type="text" class="input-small"
                                    runat="server" />
                                <span class="add-on"><i class="icon-time"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal2" runat="server" meta:ResourceKey="Registration"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:RadioButtonList ID="rblIssued" RepeatColumns="2" RepeatDirection="Horizontal"
                        runat="server">
                        <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                        <asp:ListItem Text="No" Value="0" Selected="True"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal3" runat="server" meta:ResourceKey="Fee"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtCost" runat="server" Text='<%# Eval("Cost") %>' CssClass="textbox"
                        Width="100px" MaxLength="12"></asp:TextBox>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteCost" runat="server" TargetControlID="txtCost"
                        ValidChars="." FilterType="Custom, Numbers" Enabled="True">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal6" runat="server" meta:ResourceKey="CompanyContribution"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtCostPercentage" runat="server" CssClass="textbox_mandatory" Width="58px"
                        MaxLength="5" onblur="disbableRegFee(this);"></asp:TextBox>
                    <AjaxControlToolkit:FilteredTextBoxExtender ID="fteCostPercentage" runat="server"
                        TargetControlID="txtCostPercentage" ValidChars="." FilterType="Custom, Numbers"
                        Enabled="True">
                    </AjaxControlToolkit:FilteredTextBoxExtender>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal9" runat="server" meta:ResourceKey="Venue"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:TextBox ID="txtVenue" runat="server" CssClass="textbox_mandatory" Width="252px"
                        MaxLength="50"></asp:TextBox>
                </div>
                <div class="trLeft" style="width: 25%; height: 30px; float: left;">
                    <asp:Literal ID="Literal14" runat="server" meta:ResourceKey="Employees"></asp:Literal>
                </div>
                <div class="trRight" style="width: 70%; height: 30px; float: left;">
                    <asp:CheckBoxList ID="chkScheduleType" runat="server" RepeatDirection="Horizontal"
                        CellPadding="5" CellSpacing="5">
                        <asp:ListItem meta:ResourceKey="General" Value="1"></asp:ListItem>
                        <asp:ListItem meta:ResourceKey="FromPerformance" Value="2"></asp:ListItem>
                        <asp:ListItem meta:ResourceKey="FromRequest" Value="3"></asp:ListItem>
                    </asp:CheckBoxList>
                </div>
                <div class="trRight" style="width: 99%;">
                    <asp:UpdatePanel ID="updEmpList" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <fieldset>
                                <div>
                                    <asp:RadioButton ID="rbtnDepartmentWise" runat="server" AutoPostBack="True" GroupName="D"
                                        meta:ResourceKey="DepartmentWise" Checked="True" OnCheckedChanged="rbtnDepartmentWise_CheckedChanged" />
                                    <asp:RadioButton ID="rbtnEmployeeWise" runat="server" AutoPostBack="True" GroupName="D"
                                        meta:ResourceKey="EmployeeWise" OnCheckedChanged="rbtnEmployeeWise_CheckedChanged" />
                                </div>
                            </fieldset>
                            <div style="height: 250px; width: 100%; float: left; overflow: auto">
                                <asp:DataList ID="dlEmployeeList" runat="server" Width="99%" RepeatColumns="4" RepeatDirection="Horizontal"
                                    SeparatorStyle-BackColor="AliceBlue" BackColor="White" BorderColor="#CCCCCC"
                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Both" >
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                    <SeparatorStyle BackColor="AliceBlue" />
                                    <SelectedItemStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server" Text='<%# Eval("Name") %>'/>                                       
                                        <asp:Label ID="lblID" runat="server" Visible ="false"  Text='<%# Eval("id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="clear: both; height: auto">
                </div>
                <div style="float: right; margin-top: 6px; width: 25%">
                    <asp:Button ID="btnSave" Width="75px" runat="server" Text='<%$ Resources:ControlsCommon,Save %>'
                        CssClass="btnsubmit" ValidationGroup="SaveInitiation" OnClientClick="return Save(true);" />&nbsp
                    <asp:Button Width="75px" ID="btnCancel" runat="server" Text='<%$ Resources:ControlsCommon,Cancel %>'
                        CssClass="btnsubmit" />
                </div>
            </div>
        </div>
        <div class="TabbedPanelsContent" id="divTab2" style="display: block;">
            <asp:GridView ID="dgvRegistration" runat="server" AutoGenerateColumns="False" Width="549px">
                <HeaderStyle CssClass="datalistHead" />
                <RowStyle CssClass="labeltext" />
                <Columns>
                    <asp:BoundField HeaderText="Employee" />
                    <asp:BoundField HeaderText="Registration Date" />
                    <asp:BoundField HeaderText="Amount Paid" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="TabbedPanelsContent" id="divTab3" style="display: block;">
            <asp:GridView ID="dgvFeedback" runat="server" AutoGenerateColumns="False" Width="549px">
                <HeaderStyle CssClass="datalistHead" />
                <RowStyle CssClass="labeltext" />
                <Columns>
                    <asp:BoundField HeaderText="Employee" />
                    <asp:BoundField HeaderText="Feedback" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" runat="Server">
    <div id="sidebarsettings">
        <div class="sideboxImage">
            <asp:ImageButton ID="imgAddCourse" ImageUrl="~/images/TrainingShedule_Big.png" runat="server"
                CausesValidation="false" />
        </div>
        <div class="name">
            <h5>
                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" Text="Schedule Training">
                    
                </asp:LinkButton></h5>
        </div>
        <div class="sideboxImage">
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/TrainingSheduleList_Big.PNG"
                runat="server" CausesValidation="false" />
        </div>
        <div class="name">
            <h5>
                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Text="Training List">
                    
                </asp:LinkButton></h5>
        </div>
        <div class="sideboxImage">
            <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/Delete.png" runat="server"
                CausesValidation="false" />
        </div>
        <div class="name">
            <h5>
                <asp:LinkButton ID="lnkDelete" runat="server" Enabled="true" CausesValidation="false"
                    Text="Delete">
                </asp:LinkButton></h5>
        </div>
    </div>

    <script type="text/javascript">
   
   
   document.getElementById('<%=txtStartTime.ClientID%>').timePicker();
      $('#txtStartTime').timepicker();

  function pageLoad(sender,args)
    {
    
    
       $('#txtEndTime').timepicker();



       
      

     

}
    </script>

</asp:Content>
