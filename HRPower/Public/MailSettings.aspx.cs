﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_MailSettings : System.Web.UI.Page
{
    clsMailSettings objMailSettings;
    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaster;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            objRoleSettings = new clsRoleSettings();
            objUserMaster = new clsUserMaster();


            DataTable dt = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.MailHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();

            //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Mail_Settings))
            //{
            //    MailSetting.Visible = true;
            //    BindMailSettings(clsMailSettings.AccountType.Email);
            //}
            //else
            //{
            //    MailSetting.Visible = false;
            //    lblNoData.Text = "You dont have enough permission to view this page.";
            //}

        }
    }

    public void SetPermission()
    {
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        int RoleId;
        RoleId = objUser.GetRoleId();
        if (RoleId != 1 && RoleId != 2 && RoleId != 3)
        {
            DataTable dtm = (DataTable)ViewState["Permission"];
            if (dtm.Rows.Count > 0)
            {
                if (dtm.Rows[0]["IsView"].ToBoolean()==true)
                {
                    MailSetting.Visible = true;
                    BindMailSettings(clsMailSettings.AccountType.Email);
                }

                if (dtm.Rows[0]["IsCreate"].ToBoolean() == false  || dtm.Rows[0]["IsUpdate"].ToBoolean() == false )
                {
                    Button btnSave = (Button)fvMailSettings.FindControl("btnSaveSettings");
                    if (btnSave != null)
                    {
                        btnSave.Enabled = false;
                    }
                }
            }
            else
            {
                MailSetting.Visible = false;
                lblNoData.Text = "You dont have enough permission to view this page.";
            }
        }
        else
        {
            MailSetting.Visible = true;
            BindMailSettings(clsMailSettings.AccountType.Email);
        }
    }

    protected void fvMailSettings_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        objMailSettings = new clsMailSettings();

        RadioButtonList rblType = (RadioButtonList)fvMailSettings.FindControl("rblType");
        TextBox txtUsername = (TextBox)fvMailSettings.FindControl("txtUsername");
        TextBox txtPassword = (TextBox)fvMailSettings.FindControl("txtPassword");
        TextBox txtIncomingServer = (TextBox)fvMailSettings.FindControl("txtIncomingServer");
        TextBox txtOutgoingServer = (TextBox)fvMailSettings.FindControl("txtOutgoingServer");
        TextBox txtPortNumber = (TextBox)fvMailSettings.FindControl("txtPortNumber");
        CheckBox chkEnableSSL = (CheckBox)fvMailSettings.FindControl("chkEnableSSL");

        switch (e.CommandName)
        {
            case "SAVE":

                objMailSettings.aAccountType = rblType.SelectedValue == "1" ? clsMailSettings.AccountType.Email : clsMailSettings.AccountType.SMS;
                objMailSettings.UserName = txtUsername.Text;
                objMailSettings.Password = txtPassword.Text;
                objMailSettings.IncomingServer = txtIncomingServer.Text;
                objMailSettings.OutgoingServer = txtOutgoingServer.Text;
                objMailSettings.PortNumber = (txtPortNumber.Text == string.Empty ? 0 : Convert.ToInt32(txtPortNumber.Text));
                objMailSettings.EnableSsl = Convert.ToBoolean(chkEnableSSL.Checked);

                objMailSettings.Insert();


                if (rblType.SelectedValue == "1")
                {
                    msgs.InformationalMessage("Mail settings have been saved successfully");
                }
                else
                    msgs.InformationalMessage("SMS settings have been saved successfully");
                mpeMessage.Show();
                break;
        }
    }
    protected void fvMailSettings_DataBound(object sender, EventArgs e)
    {
        RadioButtonList rblType = (RadioButtonList)fvMailSettings.FindControl("rblType");
        if (rblType == null)
            return;

        rblType.SelectedValue = Convert.ToString(fvMailSettings.DataKey["AccountType"]) == "Email" ? "1" : "2";

        //rblType.SelectedIndex = rblType.Items.IndexOf(rblType.Items.FindByText(fvMailSettings.DataKey["AccountType"].ToString()));

        CheckBox chkEnableSSL = (CheckBox)fvMailSettings.FindControl("chkEnableSSL");
        if (chkEnableSSL == null)
            return;
        if (fvMailSettings.DataKey["EnableSsl"] != DBNull.Value)
            chkEnableSSL.Checked = Convert.ToBoolean(fvMailSettings.DataKey["EnableSsl"]);

        TextBox txtPassword = (TextBox)fvMailSettings.FindControl("txtPassword");
        if (txtPassword == null)
            return;
        txtPassword.Attributes.Add("value", Convert.ToString(fvMailSettings.DataKey["Password"]));
    }

    private void BindMailSettings(clsMailSettings.AccountType AccountType)
    {
        objMailSettings = new clsMailSettings();

        DataSet ds = objMailSettings.GetMailSettings(AccountType);
        if (ds.Tables[0].Rows.Count == 0)
        {
            DataRow dw = ds.Tables[0].NewRow();
            dw["AccountType"] = AccountType.ToString();
            ds.Tables[0].Rows.Add(dw);
        }
        fvMailSettings.DataSource = ds;
        fvMailSettings.DataBind();
    }

    protected void rblType_SelectedIndexChanged(object sender, EventArgs e)
    {
        objMailSettings = new clsMailSettings();

        RadioButtonList rblType = (RadioButtonList)fvMailSettings.FindControl("rblType");

        if (rblType.SelectedValue == "1")
        {
            BindMailSettings(clsMailSettings.AccountType.Email);

            Label lblIncomingServer = (Label)fvMailSettings.FindControl("lblIncomingServer");
            Label lblOutgoingServer = (Label)fvMailSettings.FindControl("lblOutgoingServer");
            lblIncomingServer.Text = "Incoming Server";
            lblOutgoingServer.Text = "Outgoing Server";

            RequiredFieldValidator rfvIncomingServer = (RequiredFieldValidator)fvMailSettings.FindControl("rfvIncomingServer");
            RequiredFieldValidator rfvOutgoingServer = (RequiredFieldValidator)fvMailSettings.FindControl("rfvOutgoingServer");
            rfvIncomingServer.ErrorMessage = "Please enter incoming server name";
            rfvOutgoingServer.ErrorMessage = "Please enter outgoing server name";
        }
        else
        {
            BindMailSettings(clsMailSettings.AccountType.SMS);

            Label lblIncomingServer = (Label)fvMailSettings.FindControl("lblIncomingServer");
            Label lblOutgoingServer = (Label)fvMailSettings.FindControl("lblOutgoingServer");
            lblIncomingServer.Text = "SMS Url";
            lblOutgoingServer.Text = "Mail Server";

            RequiredFieldValidator rfvIncomingServer = (RequiredFieldValidator)fvMailSettings.FindControl("rfvIncomingServer");
            RequiredFieldValidator rfvOutgoingServer = (RequiredFieldValidator)fvMailSettings.FindControl("rfvOutgoingServer");
            rfvIncomingServer.ErrorMessage = "Please enter SMSUrl";
            rfvOutgoingServer.ErrorMessage = "Please enter MailServer";

            RegularExpressionValidator revUsername = (RegularExpressionValidator)fvMailSettings.FindControl("revUsername");
            revUsername.ValidationGroup = "";            
           
        }
     
    }
}
