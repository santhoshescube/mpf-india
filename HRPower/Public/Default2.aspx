﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Public_Default2" Title="Untitled Page" %>

<%@ Register src="../Controls/CandidateToEmployeeHistory.ascx" tagname="CandidateToEmployeeHistory" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
  <div id='cssmenu'>
        <ul>
            <li class="selected"><a href='Employee.aspx'><span>Employee </span></a></li>
            <li><a href='SalaryStructure.aspx'><span>Salary Structure </span></a></li>
            <li><a href="ViewPolicy.aspx">View Policies</a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">








    <table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;
        border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>
        <td style='font-weight: bold' align='center'>
            <table border='0'>
                <tr>
                    <td width='60px'>
                        <img src='http://localhost:52793/HRPower/thumbnail.aspx?FromDB=true&type=EmployeePassportPhoto&Width=70&EmployeeId=10&t=635195230235176429' />
                    </td>
                    <td style='font-size: 14px; font-weight: bold;' align='left' valign='center'>
                        Sindya
                    </td>
                </tr>
            </table>
            <tr>
                <td colspan='3' style='font-weight: bold; padding: 2px;'>
                    Employee Details
                </td>
            </tr>
            <tr>
                <td style='width: 200px'>
                    Employee
                </td>
                <td>
                    <b>Dr. Sindya </b>
                </td>
            </tr>
            <tr>
                <td>
                    Working At
                </td>
                <td>
                    Appled
                </td>
            </tr>
            <tr>
                <td>
                    Employee Number
                </td>
                <td>
                    9
                </td>
            </tr>
            <tr>
                <td>
                    Department
                </td>
                <td>
                    Administration
                </td>
            </tr>
            <tr>
                <td>
                    Designation
                </td>
                <td>
                    Accounts Manager
                </td>
            </tr>
            <tr>
                <td>
                    Employment Type
                </td>
                <td>
                    Permanent
                </td>
            </tr>
            <tr>
                <td>
                    Date of Joining
                </td>
                <td>
                    09-Oct-2012
                </td>
            </tr>
            <tr>
                <td>
                    Probation End Date
                </td>
                <td>
                </td>
            </tr>
    </table>
    <h5>
        Work Profile</h5>
    <table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;
        border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>
        <tr>
            <td style='width: 200px'>
                Work Status
            </td>
            <td>
                In Service
            </td>
        </tr>
        <tr>
            <td>
                Work Policy
            </td>
            <td>
                WorkPolicy
            </td>
        </tr>
        <tr>
            <td>
                Leave Policy
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td width='150'>
                Official Email
            </td>
            <td>
            </td>
        </tr>
    </table>
    <h5>
        Personal Facts</h5>
    <table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;
        border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>
        <tr>
            <td style='width: 200px'>
                Gender
            </td>
            <td>
                Male
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
                Nationality
            </td>
            <td>
                Bahraini
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>
                Religion
            </td>
            <td>
                Budhist
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td width='150'>
                Date of Birth
            </td>
            <td>
                11-Oct-1985
            </td>
        </tr>
    </table>
    <h5>
        Local Info</h5>
    <table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;
        border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>
        <tr>
            <td style='width: 200px'>
                Local Address
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Phone
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Mobile
            </td>
            <td>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td width='150'>
                Email
            </td>
            <td>
            </td>
        </tr>
    </table>
    <h5>
        Accounts & Bank Identity</h5>
    <table width='100%' style='font-size: 11px; border-top-style: solid; border-bottom-style: solid;
        border-top-width: 1px; border-bottom-width: 1px; border-top-color: #F0F0F0; border-bottom-color: #F0F0F0'>
        <tr>
            <td style='width: 200px'>
                Transaction Type
            </td>
            <td>
                Cash
            </td>
        </tr>
        <tr>
            <td>
                Employer Bank Name
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Employer Bank A/C
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Employee Bank Name
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Employee Bank A/C
            </td>
            <td>
            </td>
        </tr>
    </table>























</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
</asp:Content>

