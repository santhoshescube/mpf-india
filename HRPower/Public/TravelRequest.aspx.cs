﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_TravelRequest : System.Web.UI.Page
{
    clsTravelRequest objRequest;
    clsUserMaster objUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        objRequest = new clsTravelRequest();
        objUser = new clsUserMaster();
        if (!IsPostBack)
        {
            ViewState["PrevPage"] = Request.UrlReferrer;

            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
            {
                ibtnBack.Visible = true;
            }
            else
                ibtnBack.Visible = false;
            LoadCombo();
            int RequestID = 0;
            if (Request.QueryString["RequestId"] != null && Request.QueryString["Type"] == "Cancel")
            {
                RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["RequestForCancel"] = true;
                // fvSalaryDetails.ChangeMode(FormViewMode.ReadOnly);
                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;

            }
            else if (Request.QueryString["RequestId"] != null)
            {
                ViewState["RequestID"] = RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
                if (RequestID > 0)
                    ViewState["Approve"] = true;

                BindForActions(RequestID);

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                lnkAdd.Enabled = false;
                lnkView.Enabled = false;


            }
            else
            {
                BindDataList();
            }
            txtvehcleReqRemarks.Enabled = chkVehicleRequired.Checked;
        }


    }

    private void LoadCombo()
    {
        ddlTravelMode.DataSource = objRequest.GetTravelMode();
        ddlTravelMode.DataTextField = "TravelMode";
        ddlTravelMode.DataValueField = "TravelModeId";
        ddlTravelMode.DataBind();
    }

    private void BindForActions(int RequestID)
    {
        int EmployeeID = objUser.GetEmployeeId();

        if (Convert.ToBoolean(ViewState["Approve"]) == true) // Approval
        {
            divApproveTheRequest.Style["display"] = "block";
            divCancelTheRequest.Style["display"] = "none";
            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllRequestStatus(1);
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("5"));

            if (!clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.Travel), Convert.ToInt64(EmployeeID), RequestID))
            {
                ddlApproveStatus.Items.Remove(new ListItem("Pending", "4"));
            }
        }
        else if (Convert.ToBoolean(ViewState["RequestForCancel"]) == true)
        {
            divApproveTheRequest.Style["display"] = "none";
            divCancelTheRequest.Style["display"] = "block";

            ddlApproveStatus.DataSource = new clsLeaveRequest().GetAllStatus();
            ddlApproveStatus.DataBind();

            ddlApproveStatus.SelectedIndex = ddlApproveStatus.Items.IndexOf(ddlApproveStatus.Items.FindByValue("6"));
            ddlApproveStatus.Enabled = false;
        }
        setDivVisibility(4);

        BindRequestDetails(RequestID);
    }
    private void RequestTravel()
    {
        int iRequestId = 0;
        int EmployeeID = objUser.GetEmployeeId().ToInt32();
        string sRequestedTo = clsLeaveRequest.GetRequestedTo(hfRequestID.Value.ToInt32(), Convert.ToInt64(EmployeeID), Convert.ToInt32(RequestType.Travel));
        if (sRequestedTo == string.Empty)
        {
            string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن معالجة طلبك في الوقت الحالي.يرجى الاتصال بقسم الموارد البشرية") : ("Your request cannot process this time. Please contact HR department.");
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
        else
        {
            bool bIsInsert = true;

            if (hfRequestID.Value.ToInt32() > 0)
            {
                bIsInsert = false;
                objRequest.Mode = "UP";
                objRequest.RequestId = Convert.ToInt32(hfRequestID.Value);

            }
            else
            {
                objRequest.Mode = "IN";
            }


            objRequest.EmployeeId = Convert.ToInt32(objUser.GetEmployeeId());
            objRequest.TravelDate = clsCommon.Convert2DateTime(txtTravelDate.Text);
            objRequest.Place = txtPlace.Text;
            objRequest.TravelDays = Convert.ToInt32(txtNoofDays.Text);
            objRequest.Purpose = txtPurpose.Text;
            objRequest.TravelMode = Convert.ToInt32(ddlTravelMode.SelectedValue);
            objRequest.IsArrangementsMade = (rdblArrangements.SelectedValue.ToInt32() == 1);
            objRequest.Route = txtRoute.Text;
            objRequest.IsVehicleRequired = chkVehicleRequired.Checked;
            objRequest.VehicleRemarks = txtvehcleReqRemarks.Text;
            objRequest.IsPassport = (rblPassport.SelectedValue.ToInt32() == 1);
            objRequest.IsAdvance = (rblAdvance.SelectedValue.ToInt32() == 1);
            objRequest.Amount = txtAdvanceAmount.Text.ToDouble();
            objRequest.StatusId = Convert.ToInt32(RequestStatus.Applied);
            objRequest.RequestedTo = sRequestedTo;
            objRequest.IsPassportWithEmployee = chkWithEmployee.Checked;
            iRequestId = objRequest.InsertUpdateRequest();


            clsCommonMessage.SendMessage(EmployeeID, iRequestId, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Applied, "");
            clsCommonMail.SendMail(iRequestId, MailRequestType.TravelRequest, eAction.Applied);


            string message = "";
            if (bIsInsert)
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم اضافة تفاصيل الطلب بنجاح") : ("Request details added successfully.");
            else
                message = clsUserMaster.GetCulture() == "ar-AE" ? ("تم تحديث تفاصيل الطلب بنجاح") : ("Request details updated successfully.");

            ClearControls();
            BindDataList();
            msgs.InformationalMessage(message);
            mpeMessage.Show();
            upViewRequest.Update();
        }
    }
    private void setDivVisibility(int intMode)
    {
        //Mode 1:-divAddRequest
        //Mode 2:-divViewRequest
        //Mode 3:-divSingleView
        //Mode 4:-divApproveRequest

        if (intMode == 1)
        {
            divAddRequest.Style["display"] = "block";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "none";
        }
        else if (intMode == 2)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "block";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "none";
        }
        else if (intMode == 3)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "block";
            divApproveRequest.Style["display"] = "none";
            //link to display approval level remarks      
            //if (trSingleLink!= null)
            //    trSingleLink.Style["display"] = "block";
            if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
                ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
            else
                ViewState["RequestID"] = objRequest.RequestId;
        }
        else if (intMode == 4)
        {
            divAddRequest.Style["display"] = "none";
            divViewRequest.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
            divApproveRequest.Style["display"] = "block";
        }

        upViewRequest.Update();
        upAddRequest.Update();
        upSingleView.Update();
        upApproveRequest.Update();
        upMenu.Update();
    }

    private void BindDataList()
    {
        objRequest.EmployeeId = Convert.ToInt32(objUser.GetEmployeeId());
        objRequest.PageIndex = RequestPager.CurrentPage + 1;
        objRequest.PageSize = RequestPager.PageSize;

        DataSet ds = objRequest.GetRequests();
        DataTable dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            dlRequest.DataSource = dt;
            dlRequest.DataBind();

            RequestPager.Total = Convert.ToInt32(ds.Tables[1].Rows[0]["Total"].ToString());
            RequestPager.Visible = true;

            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlRequest.ClientID + "');";

            lnkDelete.Enabled = true;

            setDivVisibility(2);


        }
        else
        {
            RequestPager.Visible = false;
            lnkDelete.Enabled = false;
            ClearControls();
            setDivVisibility(1);
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        RequestTravel();
    }

    private void BindRequestDetails(int RequestId)
    {
        objRequest.RequestId = RequestId;
        DataTable dt = objRequest.GetRequestDetails();

        if (dt.Rows.Count > 0)
        {
            hfRequestID.Value = dt.Rows[0]["RequestId"].ToString();
            ViewState["RequestedBy"] = dt.Rows[0]["Employeeid"].ToString();
            lblAppRequestedDate.Text = dt.Rows[0]["Date"].ToString();
            lblAppTravelDate.Text = lblTravelDate.Text = txtTravelDate.Text = dt.Rows[0]["TravelDate"].ToString();
            txtTravelDate.Text = clsCommon.Convert2DateTime(txtTravelDate.Text).ToString("dd/MM/yyyy");
            ddlTravelMode.SelectedValue = dt.Rows[0]["TravelModeid"].ToString();
            lblAppTravelMode.Text = lblTravelMode.Text = dt.Rows[0]["TravelMode"].ToString();
            lblAppNoofDays.Text = lblTravelDays.Text = txtNoofDays.Text = dt.Rows[0]["TravelDays"].ToString();
            lblAppPlaceofTravel.Text = lblPlace.Text = txtPlace.Text = dt.Rows[0]["Place"].ToString();
            lblAppPurpose.Text = lblPurpose.Text = txtPurpose.Text = dt.Rows[0]["Purpose"].ToString();
            lblAppRoute.Text = lblRoute.Text = txtRoute.Text = dt.Rows[0]["Route"].ToString();
            lblAppRequestedBy.Text = lblRequestedBy.Text = dt.Rows[0]["RequestedBy"].ToString();
            lblAppAmount.Text = lblViewAdvAmount.Text = txtAdvanceAmount.Text = dt.Rows[0]["AdvanceAmount"].ToString();
            txtvehcleReqRemarks.Text = dt.Rows[0]["VehicleRemarks"].ToString();
            rblAdvance.SelectedValue = dt.Rows[0]["IsAdvanceRequired"].ToBoolean() == false ? "0" : "1";
            lblViewAdvance.Text = lbAppAdvance.Text = dt.Rows[0]["IsAdvanceRequired"].ToBoolean() == false ? GetLocalResourceObject("No.Text").ToString() : GetLocalResourceObject("Yes.Text").ToString();//"No" : "Yes";
            rblPassport.SelectedValue = dt.Rows[0]["IsPassportRequired"].ToBoolean() == false ? "0" : "1";
            lblViewPassport.Text = lblAppPassport.Text = dt.Rows[0]["IsPassportRequired"].ToBoolean() == false ? GetLocalResourceObject("No.Text").ToString() : GetLocalResourceObject("Yes.Text").ToString();//"No" : "Yes";
            rdblArrangements.SelectedValue = dt.Rows[0]["IsArrangementsMade"].ToBoolean() == false ? "0" : "1";
            lblViewArrangements.Text = lblAppArrangements.Text = dt.Rows[0]["IsArrangementsMade"].ToBoolean() == false ? GetLocalResourceObject("TobeMade.Text").ToString() : GetLocalResourceObject("TobeMade.Text").ToString();//"To be Made" : "Made";                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
            lblCurrentStatus.Text = lblStatus.Text = dt.Rows[0]["Status"].ToString();
            chkVehicleRequired.Checked = dt.Rows[0]["IsVehicleRequired"].ToBoolean();
            lblViewRequireVehicle.Text = lblAppRequireVehicle.Text = dt.Rows[0]["IsVehicleRequired"].ToBoolean() == false ? GetLocalResourceObject("No.Text").ToString() : GetLocalResourceObject("Yes.Text").ToString();//"No" : "Yes";
            chkWithEmployee.Checked = dt.Rows[0]["IsPassportWithEmployee"].ToBoolean();
            lblAppWithEmployee.Text = lblWithEmployee.Text = dt.Rows[0]["IsPassportWithEmployee"].ToBoolean() == false ? GetLocalResourceObject("No.Text").ToString() : GetLocalResourceObject("Yes.Text").ToString();//"No" : "Yes";
            txtAdvanceAmount.Enabled = (rblAdvance.SelectedValue.ToInt32() == 1);
            txtvehcleReqRemarks.Enabled = chkVehicleRequired.Checked;
            lblViewRemarks.Text = lblAppRemarks.Text = dt.Rows[0]["VehicleRemarks"].ToString();
            rfvAdvAmount.ValidationGroup = txtAdvanceAmount.Enabled ? "Submit" : "";
            rfvVehRemarks.ValidationGroup = txtvehcleReqRemarks.Enabled ? "Submit" : "";
            txtApproveRemarks.Text = dt.Rows[0]["Remarks"].ToString();
        }
        //link to display approval level remarks      
        if (trlink != null)
            trlink.Style["display"] = "block";

        objRequest.RequestId = RequestId;
        objRequest.CompanyId = objRequest.GetCompanyID();
        DataTable dt2 = objRequest.GetApprovalHierarchy();

        if (Convert.ToInt32(Request.QueryString["RequestId"]) > 0)
        {
            if (dt2.Rows.Count > 0)
            {
                dl1.DataSource = dt2;
                dl1.DataBind();
                dl2.DataSource = dt2;
                dl2.DataBind();
            } upApproveRequest.Update();
            upSingleView.Update();
            ViewState["RequestID"] = Convert.ToInt32(Request.QueryString["RequestId"]);
           // imgBack2.Visible = true;
        }
        else
        {
            if (dt2.Rows.Count > 0)
            {
                dl1.DataSource = dt2;
                dl1.DataBind();
            } upSingleView.Update();
            ViewState["RequestID"] = objRequest.RequestId;
          //  imgBack2.Visible = false;
        }
    }

    private void AddNewRequest()
    {
        ClearControls();
        rfvAdvAmount.ValidationGroup = txtAdvanceAmount.Enabled ? "Submit" : "";
        rfvVehRemarks.ValidationGroup = txtvehcleReqRemarks.Enabled ? "Submit" : "";

        setDivVisibility(1);
    }

    private void ClearControls()
    {
        hfRequestID.Value = string.Empty;
        txtAdvanceAmount.Text = "";
        txtNoofDays.Text = "";
        txtPlace.Text = "";
        txtPurpose.Text = "";
        txtRoute.Text = "";
        txtTravelDate.Text = "";
        txtvehcleReqRemarks.Enabled = false;
        txtvehcleReqRemarks.Text = "";
        chkVehicleRequired.Checked = false;
        rblAdvance.SelectedValue = "0";
        rblPassport.SelectedValue = "0";
        rdblArrangements.SelectedValue = "0";
        txtAdvanceAmount.Enabled = false;

        lnkDelete.Enabled = false;
        lnkDelete.OnClientClick = "return false;";

        lnkView.Enabled = true;


    }

    private bool DeleteRequest(int requestId)
    {
        string message = string.Empty;
        objRequest.RequestId = requestId;

        if (objRequest.IsRequestApproved())
        {
            return false;
        }
        else
        {
            objRequest.Mode = "DEL";
            objRequest.DeleteRequest();
            clsCommonMessage.DeleteMessages(requestId, eReferenceTypes.TravelRequest);
            return true;
        }
    }

    protected void dlRequest_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {

            case "_Edit":
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));

                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";

                setDivVisibility(1);
                upAddRequest.Update();
                break;

            case "_View":
                hfLink.Value = hfSingleLink.Value = "0";
                trReasons.Style["display"] = trSingleReason.Style["display"] = "none";
                BindRequestDetails(Convert.ToInt32(e.CommandArgument));

                if (clsUserMaster.GetCulture() == "ar-AE")
                    lnkDelete.OnClientClick = "return confirm('هل أنت متأكد أنك تريد حذف؟');";
                else
                    lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
                lnkDelete.Enabled = true;

                setDivVisibility(3);
                upMenu.Update();
                upSingleView.Update();
                break;
            case "_Cancel":
                Label lbRequested = (Label)e.Item.FindControl("lbRequested");
                RequestCancel(Convert.ToInt32(e.CommandArgument), lbRequested.Text);
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
                break;


        }
    }
    private void RequestCancel(int RequestId, string sRequestedIds)
    {

        int EmployeeID = objUser.GetEmployeeId();

        objRequest.RequestId = RequestId;
        objRequest.EmployeeId = objUser.GetEmployeeId();
        objRequest.StatusId = Convert.ToInt32(RequestStatus.RequestForCancel);
        objRequest.Remarks = "Travel Request for Cancellation";
        objRequest.UpdateCancelStatus();


        clsCommonMessage.SendMessage(null, RequestId, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.RequestForCancel, "");
        clsCommonMail.SendMail(RequestId, MailRequestType.TravelRequest, eAction.RequestForCancel);

        //--------------------------------------------------------------------------------------------------------------------------------------------------

        BindDataList();
        if (clsUserMaster.GetCulture() == "ar-AE")
            msgs.InformationalMessage("تم تحديث التفاصيل بنجاح.");
        else
            msgs.InformationalMessage("Successfully updated request details.");

        mpeMessage.Show();

    }

    protected void chkVehicleRequired_CheckedChanged(object sender, EventArgs e)
    {
        txtvehcleReqRemarks.Enabled = chkVehicleRequired.Checked;
        rfvVehRemarks.ValidationGroup = txtvehcleReqRemarks.Enabled ? "Submit" : "";
    }
    protected void rblAdvance_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtAdvanceAmount.Enabled = (rblAdvance.SelectedValue.ToInt32() == 1);
        rfvAdvAmount.ValidationGroup = txtAdvanceAmount.Enabled ? "Submit" : "";
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        AddNewRequest();
        upAddRequest.Update();
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        BindDataList();
        upViewRequest.Update();
        upMenu.Update();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        BindDataList();

    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;

        if (divSingleView.Style["display"] == "none")
        {
            if (dlRequest.Items.Count > 0)
            {
                foreach (DataListItem item in dlRequest.Items)
                {
                    CheckBox chkRequest = (CheckBox)item.FindControl("chkRequest");
                    HiddenField hfType = (HiddenField)item.FindControl("hfType");
                    if (chkRequest == null)
                        continue;
                    if (chkRequest.Checked)
                    {
                        if (DeleteRequest(Convert.ToInt32(dlRequest.DataKeys[item.ItemIndex])))
                            message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : ("Request(s) deleted successfully");
                        else
                            message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب (ق) لا يمكن حذفها.") : ("Processing started request(s) cannot be deleted.");
                    }
                }

            }
        }
        else
        {
            if (hfRequestID.Value.ToInt32() > 0)
            {
                if (DeleteRequest(Convert.ToInt32(hfRequestID.Value)))
                    message = clsUserMaster.GetCulture() == "ar-AE" ? "طلب حذف بنجاح" : "Request deleted successfully";
                else
                    message = clsUserMaster.GetCulture() == "ar-AE" ? (" بدأت معالجة الطلب لا يمكن حذفها.") : "Processing started request cannot be deleted.";
            }
        }

        msgs.InformationalMessage(message);
        mpeMessage.Show();

        BindDataList();


    }
    protected void btnApproveCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }
    protected void btnApproveRequest_Click(object sender, EventArgs e)
    {
        int RequestID = 0;
        int iRequestedById = 0;

        RequestID = Convert.ToInt32(Request.QueryString["RequestId"]);
        iRequestedById = Convert.ToInt32(ViewState["RequestedBy"]);
        objRequest.RequestId = RequestID;
        int EmployeeId = objUser.GetEmployeeId();
        objRequest.ApprovedBy = Convert.ToString(objUser.GetEmployeeId());
        if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5) // Approve
        {
            if (objRequest.GetWorkStatusId() < 6)
            {
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن الموافقة على الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Cannot approve the request. The requested employee is no longer in service.");
                msgs.InformationalMessage(message);
                mpeMessage.Show();

            }
            else
            {

                if (clsLeaveRequest.IsHigherAuthority(Convert.ToInt32(RequestType.Travel), Convert.ToInt64(EmployeeId), objRequest.RequestId))
                {
                    divPrint.Style["display"] = "block";
                    upMenu.Update();
                    objRequest.StatusId = Convert.ToInt32(ddlApproveStatus.SelectedValue);

                    objRequest.StatusId = Convert.ToInt32(ddlApproveStatus.SelectedValue);
                    objRequest.RequestedDate = clsCommon.Convert2DateTime(lblAppRequestedDate.Text);
                    objRequest.Amount = txtAdvanceAmount.Text.ToDouble();
                    objRequest.Remarks = txtApproveRemarks.Text;

                    objRequest.ApproveRequest(true);


                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Approved, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.TravelRequest, eAction.Approved);
                }
                else
                {
                    divPrint.Style["display"] = "none";
                    upMenu.Update();
                    objRequest.StatusId = Convert.ToInt32(ddlApproveStatus.SelectedValue) == 5 ? 1 : Convert.ToInt32(ddlApproveStatus.SelectedValue);
                    objRequest.Remarks = txtApproveRemarks.Text.Trim();

                    objRequest.ApproveRequest(false);

                    clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Approved, "");
                    clsCommonMail.SendMail(RequestID, MailRequestType.TravelRequest, eAction.Approved);



                }
                string message = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
                msgs.InformationalMessage(message);
                mpeMessage.Show();



            }
        }
        else
        {

            objRequest.RequestId = RequestID;
            objRequest.StatusId = Convert.ToInt32(ddlApproveStatus.SelectedValue);
            objRequest.EmployeeId = iRequestedById;
            objRequest.Remarks = txtApproveRemarks.Text;
            objRequest.ApproveRequest(true);
            if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 3)
            {
                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Reject, "");
                clsCommonMail.SendMail(RequestID, MailRequestType.TravelRequest, eAction.Reject);
                msgs.InformationalMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("طلب مرفوض بنجاح") : ("Request rejected successfully."));
            }
            else if (Convert.ToInt32(ddlApproveStatus.SelectedValue) == 4)
            {

                clsCommonMessage.SendMessage(null, RequestID, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Pending, "");
                msgs.InformationalMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully."));

            }
            mpeMessage.Show();
        }
        ViewState["Approve"] = false;

        setDivVisibility(3);

        BindRequestDetails(RequestID);

        lnkDelete.OnClientClick = "return false;";
        lnkDelete.Enabled = false;
    }
    protected void btnCancelCancel_Click(object sender, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            this.Response.Redirect(ViewState["PrevPage"].ToString());
        else
            this.Response.Redirect("home.aspx");
    }
    protected void btnCancelSubmit_Click(object sender, EventArgs e)
    {

        int RequestId = Request.QueryString["Requestid"].ToInt32();
        objRequest.RequestId = RequestId;

        string strmessage = "";
        if (objRequest.GetWorkStatusId() < 6)
        {
            strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل في إلغاء الطلب. الموظف المطلوب لم يعد في الخدمة.") : ("Failed to cancel the request. The requested employee is no longer in service.");
        }

        else
        {
            objRequest.RequestId = RequestId;
            objRequest.StatusId = Convert.ToInt32(ddlApproveStatus.SelectedValue);
            objRequest.EmployeeId = objUser.GetEmployeeId();
            objRequest.Remarks = txtApproveRemarks.Text; //txtEditReason.Text;//
            objRequest.CancelRequest();

            int RequestedById = Convert.ToInt32(ViewState["RequestedBy"]);

            clsCommonMail.SendMail(RequestId, MailRequestType.TravelRequest, eAction.Cancelled);

            //--------------------------------------------Message insertion into HRMessages-----------------------------------------
            clsCommonMessage.SendMessage(RequestedById, RequestId, eReferenceTypes.TravelRequest, eMessageTypes.Travel_Request, eAction.Cancelled, "");



            ViewState["RequestForCancel"] = false;
            // BindRequestDetails(RequestId );
            setDivVisibility(3);
            BindRequestDetails(RequestId);
            strmessage = clsUserMaster.GetCulture() == "ar-AE" ? ("الطلب تفاصيل تحديث بنجاح") : ("Request updated successfully.");
        }
        msgs.InformationalMessage(strmessage);
        mpeMessage.Show();

    }
    protected void dlRequest_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hdForwarded = (HiddenField)e.Item.FindControl("hfForwarded");
            HiddenField hdStatusID = (HiddenField)e.Item.FindControl("hfStatusID");
            Int32 intStatusID = hdStatusID.Value.ToInt32();
            bool blnForwared = hdForwarded.Value == "True" ? true : false;

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            if (intStatusID != (int)RequestStatus.Applied || blnForwared)
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }

            ImageButton btnCancel = (ImageButton)e.Item.FindControl("btnCancel");
            btnCancel.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm(' هل تريد إلغاء هذا الطلب؟');") : ("return confirm('Do you want to cancel this request?');");
            if (intStatusID == (int)RequestStatus.Cancelled || intStatusID == (int)RequestStatus.RequestForCancel || intStatusID == (int)RequestStatus.Rejected || (intStatusID == (int)RequestStatus.Applied && !blnForwared))
            {
                btnCancel.Enabled = false;
                btnCancel.ImageUrl = "~/images/cancel_disable.png";
            }
        }
    }

    protected void imgback_Click(object source, EventArgs e)
    {
        if (ViewState["PrevPage"] != null)
            Response.Redirect(ViewState["PrevPage"].ToString());
    }
    protected void lbReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsTravelRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfLink.Value) == 0)
            {
                hfLink.Value = "1";
                trReasons.Style["display"] = "block";

                DataTable dt = objRequest.DisplayReasons();
                if (dt.Rows.Count > 0)
                {
                    gvReasons.DataSource = dt;
                    gvReasons.DataBind();
                }
            }
            else
            {
                hfLink.Value = "0";
                trReasons.Style["display"] = "none";
            }
        }
    }
    protected void lbSingleReason_OnClick(object source, EventArgs e)
    {
        objRequest = new clsTravelRequest();
        if (Convert.ToInt32(ViewState["RequestID"]) > 0)
        {
            objRequest.RequestId = ViewState["RequestID"].ToInt32();
            if (Convert.ToInt32(hfSingleLink.Value) == 0)
            {
                hfSingleLink.Value = "1";
                trSingleReason.Style["display"] = "block";

                DataTable dt = objRequest.DisplayReasons();
                if (dt.Rows.Count > 0)
                {
                    gvSingleReason.DataSource = dt;
                    gvSingleReason.DataBind();
                }
            }
            else
            {
                hfSingleLink.Value = "0";
                trSingleReason.Style["display"] = "none";
            }
        }
    }
}
