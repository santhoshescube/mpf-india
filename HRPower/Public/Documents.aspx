<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Documents.aspx.cs" Inherits="Public_Documents"%>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
 <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
function ValidateReferenceName(sender,args)
{
    var ReferenceName = document.getElementById('<%=ddlReferenceName.ClientID %>').value;
    var ReferenceType = document.getElementById('<%=tdReferenceName.ClientID %>').innerText;
    var Message = "*";
    sender.innerText = "";
    
    if(ReferenceName == "-1")
    {
        if(sender.innerText == undefined) 
        { 
            sender.textContent =  Message; 
        }
        else
        {
            sender.innerText =  Message; 
        }
        args.IsValid = false;
    }
    else
    args.IsValid = true;
}
    </script>

   
    <div id='cssmenu'>
        <ul>
            <li ><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx"><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx"><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx"><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx"><asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li class="selected"><a href="Documents.aspx" ><asp:Literal ID="Literal10" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx"><asp:Literal ID="Literal11" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%" class="labeltext">
        <tr>
            <td>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Label ID="lblHeading" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </div>
                <asp:UpdatePanel ID="upEmployeeOtherDoc" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divAddDocument" runat="server" style="display: block">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%" class="labeltext">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                   <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="Type"></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:RadioButton ID="rbEmployee" runat="server" Checked="true"  Text='<%$Resources:DocumentsCommon,Employee%>' OnCheckedChanged="rbEmployee_CheckedChanged"
                                                        GroupName="grpType" AutoPostBack="true" />
                                                    <asp:RadioButton ID="rbCompany" runat="server"  Text='<%$Resources:DocumentsCommon,Company%>' GroupName="grpType"
                                                        OnCheckedChanged="rbCompany_CheckedChanged" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft" id="tdReferenceName" runat="server">
                                                    Reference Name
                                                </td>
                                                <td class="trRight">
                                                    <asp:UpdatePanel ID="upnlReferenceName" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlReferenceName" runat="server" Width="190px">
                                                            </asp:DropDownList>
                                                            <%--<asp:CustomValidator ID="cvReferenceName" runat="server" ClientValidationFunction="ValidateReferenceName"
                                                                ValidateEmptyText="true" ControlToValidate="ddlReferenceName" ValidationGroup="Submit" ErrorMessage="*" Display="Dynamic"
                                                               ></asp:CustomValidator>--%>
                                                               <asp:RequiredFieldValidator ID="rfvRefname" runat="server" ControlToValidate="ddlReferenceName"
                                                                 Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                  <asp:Literal ID="Literal13" runat ="server" meta:resourcekey="DocumentType"></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:UpdatePanel ID="updDocumentType" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="rcDocType" CssClass="dropdownlist_mandatory" Width="190px" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnDocumentType" runat="server" Text="..." CausesValidation="False"
                                                                CssClass="referencebutton" Height="20px" OnClick="btnDocumentType_Click" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rcDocType"
                                                                 Display="Dynamic" ErrorMessage="*"
                                                                ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <asp:Literal ID="Literal14" runat ="server" meta:resourcekey="DocumentNumber"></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtDocumentNumber" runat="server" MaxLength="50" Width="182px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvDocumentNumber" runat="server" ErrorMessage="*"
                                                        ValidationGroup="Submit" ControlToValidate="txtDocumentNumber"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                     <asp:Literal ID="Literal20" runat ="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnIssueDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnIssueDate" TargetControlID="txtIssuedate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvIssueDate" runat="server" ControlToValidate="txtIssuedate"
                                                                    ClientValidationFunction="validatepassportIssuedate" CssClass="error" ValidateEmptyText="True"
                                                                    ValidationGroup="Submit" Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <asp:Literal ID="Literal21" runat ="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvExpiryDate" runat="server" ControlToValidate="txtExpiryDate"
                                                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Submit" ClientValidationFunction="ValidateOtherDocExpiryDate"
                                                                    Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <asp:Literal ID="Literal22" runat ="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="textbox" Width="300px" TextMode="MultiLine"
                                                        Height="76px" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>                                                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                     <asp:Literal ID="Literal32" runat ="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:Label ID="lblCurrentStatus" runat="server" Text="New" Width="30%"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trBinNumber" runat="server">
                                                <td width="120px" class="trLeft">
                                                    <asp:Literal ID="Literal33" runat ="server" meta:resourcekey="BinNumber"></asp:Literal> 
                                                </td>
                                                <td class="trRight">
                                                    <asp:DropDownList ID="ddlBinNumber" runat="server" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft" colspan="2">
                                                    <div id="divParse">
                                                        <fieldset style="padding: 3px 1px;">
                                                            <legend> <asp:Literal ID="Literal23" runat ="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal> </legend>
                                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left: 10px">
                                                                <tr>
                                                                    <td valign="top">
                                                                         <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal> 
                                                                    </td>
                                                                    <td valign="top" style="width: 3px">
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:Literal ID="Literal25" runat ="server" Text='<%$Resources:DocumentsCommon,ChooseFile%>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="175px">
                                                                        <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox_mandatory" Width="150px"
                                                                            MaxLength="50"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                                            Display="Dynamic" ErrorMessage="*"
                                                                            ValidationGroup="AddDocs"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="cvotherdocnameduplicate" runat="server" ClientValidationFunction="validateotherdocnameduplicate"
                                                                            ControlToValidate="txtDocname" ValidationGroup="AddDocs" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'></asp:CustomValidator>
                                                                    </td>
                                                                    <td style="width: 2px">
                                                                    </td>
                                                                    <td valign="top" width="250px">
                                                                        <AjaxControlToolkit:AsyncFileUpload ID="fuDocument" runat="server" PersistFile="True"
                                                                            Width="250px" Visible="true" CompleteBackColor="White" ErrorBackColor="White"
                                                                            OnUploadedComplete="fuDocument_UploadedComplete" />
                                                                        <asp:CustomValidator ID="cvotherdocumentAttachment" runat="server" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                                                            CssClass="error" ValidateEmptyText="True" ValidationGroup="AddDocs" ClientValidationFunction="validateotherdocumentAttachment"></asp:CustomValidator>
                                                                    </td>
                                                                    <td valign="top" width="70px">
                                                                        <asp:LinkButton ID="btnattachpassdoc" Text='<%$Resources:DocumentsCommon,Addtolist%>' Enabled="true" runat="server"
                                                                            ValidationGroup="AddDocs" OnClick="btnattachpassdoc_Click"> </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft" colspan="2">                                                    
                                                    <div id="divOtherDoc" runat="server" class="container_content" style="display: none;
                                                        height: 150px; overflow: auto; margin-top: 5px;">
                                                        <asp:UpdatePanel ID="updOtherDoc" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DataList ID="dlOtherDoc" runat="server" Width="100%" DataKeyField="Node" GridLines="Horizontal"
                                                                    OnItemCommand="dlOtherDoc_ItemCommand">
                                                                    <HeaderStyle CssClass="datalistheader" />
                                                                    <HeaderTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="241">
                                                                                   <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                                                </td>
                                                                                <td width="110" align="left">
                                                                                   <asp:Literal ID="Literal26" runat ="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal>
                                                                                </td>
                                                                                <td width="20" style="float: right;">
                                                                                    &nbsp;
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="150">
                                                                                    <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="100%"></asp:Label>
                                                                                </td>
                                                                                <td width="130" align="left">
                                                                                    <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="100%"></asp:Label>
                                                                                </td>
                                                                                <td width="20" align="left">
                                                                                    <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"
                                                                                        CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <asp:Button ID="btnSubmit" runat="server" Text ='<%$Resources:ControlsCommon,Submit%>' CssClass="btnsubmit" Width="75px"
                                                        ValidationGroup="Submit" OnClick="btnSubmit_Click" />
                                                    <asp:Button ID="btnCancel" runat="server"  Text ='<%$Resources:ControlsCommon,Cancel%>'  CssClass="btnsubmit" Width="75px"
                                                        OnClick="btnCancel_Click" />
                                                    <asp:HiddenField ID="hdnDocumentID" runat="server" />
                                                    <asp:HiddenField ID="hdnMode" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divDataList" runat="server" style="display: none">
                            <asp:DataList ID="dlEmployeeOtherDoc" runat="server" BorderWidth="0px" CellPadding="0"
                                Width="100%" DataKeyField="DocumentID" OnItemCommand="dlEmployeeOtherDoc_ItemCommand" OnItemDataBound = "dlEmployeeOtherDoc_ItemBound">
                                <ItemStyle CssClass="item" />
                                <HeaderStyle CssClass="listItem" />
                                <HeaderTemplate>
                                    <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 5px">
                                        <tr>
                                            <td height="30" width="30">
                                                <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkItem');" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle CssClass="listItem" />
                                <ItemTemplate>
                                    <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 5px" id="tblDetails"
                                        runat="server"> <%--onmouseover="showEdit(this);" onmouseout="hideEdit(this);"--%>
                                        <tr>
                                            <td valign="top" width="30" style="padding-top: 4px">
                                                <asp:CheckBox ID="chkItem" runat="server" />
                                            </td>
                                            <td valign="top">
                                                <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:LinkButton ID="lnkDocument" CausesValidation="false" runat="server" CommandArgument='<%# Eval("DocumentID")%>'
                                                                            CommandName="VIEW" Text='<%# Eval("ReferenceName") %>' CssClass="listHeader bold"></asp:LinkButton>
                                                                            [<%# Eval("EmployeeNumber")%>] &nbsp  <asp:Literal ID="Literal34" runat ="server" meta:resourcekey="DocumentNumber"></asp:Literal>   - <%# Eval("DocumentNumber")%>
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;
                                                                    </td>
                                                                    <td valign="top" align="right">
                                                                        <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                            display: block;" CommandArgument='<%# Eval("DocumentID")%>' CommandName="ALTER" ImageUrl="~/images/edit.png" ToolTip="Edit"></asp:ImageButton>
                                                                    </td>
                                                                    <td style="visibility: hidden">
                                                                        <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 20px">
                                                            <table width="100%">
                                                                <tr>
                                                                    <td class="innerdivheader" width="150px">
                                                                        <asp:Literal ID="Literal19" runat ="server" meta:resourcekey="Type"></asp:Literal> 
                                                                    </td>
                                                                    <td width="5">
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("OperationType")%>
                                                                    </td>
                                                                    <td style="color: #FF0000; font-weight: bold; float: right;">
                                                                        <%# Eval("IsRenewed")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="innerdivheader" width="150px">
                                                                       <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="DocumentType"></asp:Literal> 
                                                                    </td>
                                                                    <td width="5">
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("DocumentType")%>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="innerdivheader">
                                                                        <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <%# GetDate(Eval("IssuedDate"))%>&nbsp;
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="innerdivheader">
                                                                         <asp:Literal ID="Literal27" runat ="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                                    </td>
                                                                    <td>
                                                                        :
                                                                    </td>
                                                                    <td>
                                                                        <%# GetDate(Eval("ExpiryDate"))%>&nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                        <div id="divViewDocuments" style="width: 100%; margin-left: 20px; display: none;"
                            runat="server">
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                               <asp:Literal ID="Literal18" runat ="server" meta:resourcekey="Type"></asp:Literal> 
                            </div>
                            <div class="innerdivheaderValue" style="width: 70%; float: left; height: 20px; margin-top: 10px;"
                                id="divOperationType" runat="server">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;"
                                id="divReferenceType" runat="server">
                                Reference Type
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px;"
                                id="divReferenceName" runat="server">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                <asp:Literal ID="Literal16" runat ="server" meta:resourcekey="DocumentType"></asp:Literal> 
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px;"
                                id="divDoumentType" runat="server">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                <asp:Literal ID="Literal17" runat ="server" meta:resourcekey="DocumentNumber"></asp:Literal> 
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px;"
                                id="divDocumentNumber" runat="server">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                 <asp:Literal ID="Literal28" runat ="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                            </div>
                            <div id="divIssuedDate" runat="server" class="innerdivheaderValue" style="width: 73%;
                                float: left; height: 20px; margin-top: 10px;">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                <asp:Literal ID="Literal29" runat ="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px;"
                                id="divExpiryDate" runat="server">
                            </div>
                             <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                <asp:Literal ID="Literal30" runat ="server" Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px;"
                                id="divIsRenewed" runat="server">
                            </div>
                            <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin-top: 10px;">
                                 <asp:Literal ID="Literal31" runat ="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                            </div>
                            <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin-top: 10px; word-break:break-all;"
                                id="divRemarks" runat="server">
                            </div>
                            <div id="divIsDelete" runat="server" style="visibility: hidden"></div>
                        </div>
                        <div>
                            <uc:Pager ID="pgrDocument" runat="server" OnFill="BindDataList"></uc:Pager>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="text-align: center; margin-top: 10px;">
                    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlEmployeeOtherDoc" EventName="ItemCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="Button1" runat="server" />
                        </div>
                        <asp:Panel ID="pnlModalPopUp" Style="display: none" runat="server">
                            <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                            PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div style="display: none">
                            <asp:Button ID="btnIssueReceipt" runat="server" />
                        </div>
                        <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                            <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                            </uc1:DocumentReceiptIssue>
                        </asp:Panel>
                        <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                            PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                        </AjaxControlToolkit:ModalPopupExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upPrint" runat="server">
                    <ContentTemplate>
                        <div id="divPrint" runat="server" style="display: none">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender runat="server" TargetControlID = "txtSearch"  WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="../images/search.png" OnClick="btnSearch_Click">
                    </asp:ImageButton>
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddDoc" ImageUrl="~/images/New_OtherDocument_Big.png" runat="server" />
                </div>
                <div class="name">
                    <h5> <asp:LinkButton ID="lnkAddDoc" runat="server" OnClick="lnkAddDoc_Click" meta:resourcekey="AddDocument">
                           </asp:LinkButton> </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListDoc" ImageUrl="~/images/List_Otherdocument_Big.png" runat="server" />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkListDoc" runat="server" OnClick="lnkListDoc_Click" meta:resourcekey="ViewDocument"> 
                          </asp:LinkButton> </h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'> 
                         </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                  <h5>  <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'>>
                           </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                <h5>    <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'>>
                         
                           </asp:LinkButton>  </h5> 
                </div>
                <div id="divIssueReceipt" runat="server" style="display: block">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div class="name" runat="server">
                          <h5>  <asp:LinkButton ID="lnkDocumentIR" runat="server" OnClick="lnkDocumentIR_Click" Text='<%$Resources:DocumentsCommon,Receipt%>'>> 
                         
                                </asp:LinkButton>  </h5>
                    </div>
                </div>
                
                <div id="divRenew" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div id="Div3" class="name" runat="server">
                        <h5> <asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click" Text='<%$Resources:DocumentsCommon,Renew%>'>>
                            
                                 </asp:LinkButton></h5> 
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
