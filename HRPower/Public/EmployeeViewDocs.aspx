﻿<%@ Page Language="C#" MasterPageFile="~/Master/DocumentMasterPage.master" AutoEventWireup="true"
    CodeFile="EmployeeViewDocs.aspx.cs" Inherits="Public_EmployeeViewDocs" Title="View Documents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">

    <script src="../js/Common.js" type="text/javascript"></script>
    <div id='cssmenu1'>
       <ul>
            <a href="Passport.aspx"><li>Passport</li></a>
            <a href="Visa.aspx"><li>Visa</li></a>
            <a href="EmiratesHealthLabourCard.aspx?typeid=1"><li id="liHealth" runat="server">Health Card</li></a>
            <a href="EmiratesHealthLabourCard.aspx?typeid=2"><li id="liLabour" runat="server">Labour Card</li></a>
            <a href="EmiratesHealthLabourCard.aspx?typeid=3"><li id="liEmirates" runat="server">NATIONAL ID CARD</li></a>
            <a href="InsuranceCard.aspx"><li>Insurance Card</li></a>
            <a href="Documents.aspx"><li>Other Documents</li></a>
            <a href="EmployeeViewDocs.aspx"><li class="selected">View Documents</li></a>            
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <div id="container" style="min-height: 500px; overflow: visible;width:100%">
                    <div id="MySplitter" style="width:100%">
                        <div id="LeftPane" style="float:left;width:25%">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td width="5">
                                        &nbsp;
                                    </td>
                                    <td align="center">
                                        &nbsp;
                                    </td>
                                    <td width="5">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="center">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr runat="server" id="trJobTray">
                                                <td align="center">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td width="8" height="40" align="left" valign="top" class="left_corner_small">
                                                                &nbsp;
                                                            </td>
                                                            <td height="40" align="left" valign="bottom" class="top_header_small">
                                                             
                                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="cursor: pointer;">
                                                                    <tr>
                                                                        <td valign="top" style="font-size:12px">
                                                                            <img src="../images/Employees.png" width="16" height="16" />
                                                                            Select Employee
                                                                        </td>
                                                                        <td align="right" width="30" valign="top">
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="8" height="40" align="left" valign="top" class="right_corner_small">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" class="left_border">
                                                                &nbsp;
                                                            </td>
                                                            <td align="left" style="padding-top: 5px">
                                                                <div id="divJobTray">
                                                                    <asp:UpdatePanel ID="upJobTray" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="padding-left: 20px">
                                                                                        <asp:UpdatePanel ID="upEmployee" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                                                            <ContentTemplate>
                                                                                                <asp:DropDownList ID="ddlEmployee" runat="server" Width="180px" CssClass="dropdownlist_mandatory"
                                                                                                    DataValueField="EmployeeID" DataTextField="EmployeeName" AutoPostBack="True"
                                                                                                    OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged">
                                                                                                </asp:DropDownList>
                                                                                            </ContentTemplate>
                                                                                        </asp:UpdatePanel>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        &nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <div style="overflow: auto; min-height: 750px; display: block;">
                                                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                                                                <ContentTemplate>
                                                                                                    <asp:TreeView ID="tvEmployeeDocs" runat="server" onclick="SelectAllChildNodes(event);"
                                                                                                        NodeStyle-CssClass="treeNode" RootNodeStyle-CssClass="treeRootNode" SelectedNodeStyle-CssClass="treeNodeSelected" ShowLines="True" OnTreeNodePopulate="tvEmployeeDocs_TreeNodePopulate"
                                                                                                        OnSelectedNodeChanged="tvEmployeeDocs_SelectedNodeChanged">
                                                                                                    </asp:TreeView>
                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </td>
                                                            <td align="left" valign="top" class="right_border">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="6" class="left_corner">
                                                                &nbsp;
                                                            </td>
                                                            <td align="left" valign="top" class="bottom_border">
                                                                &nbsp;
                                                            </td>
                                                            <td align="left" valign="top" class="right_corner">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="center">
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="RightPane"  style="float:right;width:75%">
                            <table cellpadding="0" cellspacing="0" border="0" style="width:100%">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="8" height="40" align="left" valign="top" class="left_corner_big">
                                        &nbsp;
                                    </td>
                                    <td width="700" height="40" align="left" valign="top" class="top_header_big">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always" RenderMode="Inline">
                                            <ContentTemplate>
                                                <table border="0" cellpadding="0" cellspacing="0" style="padding-top: 20px">
                                                    <tr>
                                                        <td class="titleicon" width="35">
                                                        </td>
                                                        <td class="Title">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td width="13" height="40" align="left" valign="top" class="right_corner_big">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                   <td align="left" valign="top" height="150" class="left_border">
                                        &nbsp;
                                    </td>
                                    <td valign="top" height="800" colspan="3">
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <iframe id="ifDocument" runat="server" width="100%" visible="false" height="100%"
                                                    style="border: 1px solid #29537C;" frameborder="0"></iframe>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="tvEmployeeDocs" EventName="SelectedNodeChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlEmployee" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td align="left" valign="top" class="right_border">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="8" height="6" align="left" valign="top" class="left_corner">
                                        &nbsp;
                                    </td>
                                    <td width="700" height="6" align="left" valign="bottom">
                                        &nbsp;
                                    </td>
                                    <td width="13" height="6" align="left" valign="top" class="right_corner">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
