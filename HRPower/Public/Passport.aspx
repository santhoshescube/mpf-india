﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="Passport.aspx.cs" Inherits="Public_Passport" %>

<%@ Register Src="~/Controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc" %>
<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
select
{
	margin-left:5px;
}
</style>
    <div id='cssmenu'>
        <ul>
            <li class="selected"><a href="Passport.aspx">
                <asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx">
                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx">
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx">
                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1">
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server" ><a href="EmiratesHealthLabourCard.aspx?typeid=2" >
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3">
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx">
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx">
                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx">
                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx">
                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx">
                <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx">
                <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td>
                <div>
                    <div style="display: none">
                        <asp:Button ID="btnProxy" runat="server" Text="submit" />
                    </div>
                    <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                        BorderStyle="None">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                        PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                        Drag="true">
                    </AjaxControlToolkit:ModalPopupExtender>
                </div>
                <asp:UpdatePanel ID="upEmployeePassport" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="fvEmployees" runat="server">
                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="4" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="200px" class="trLeft">
                                                    <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:DocumentsCommon,Company%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                        DataValueField="CompanyID" DataTextField="CompanyName" AutoPostBack="true" OnSelectedIndexChanged="ddlCompany_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlEmployee" runat="server" Width="190px" CssClass="dropdownlist_mandatory"
                                                                DataValueField="EmployeeID" DataTextField="EmployeeName">
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlCompany" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                    <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee" InitialValue="-1"></asp:RequiredFieldValidator>
                                                    <asp:HiddenField ID="hdPassportID" runat="server" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="NameInPassport"></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <asp:TextBox ID="txtnameinpassport" BackColor="Info" MaxLength="45" runat="server"
                                                        Width="182px"></asp:TextBox>
                                                    <asp:HiddenField ID="hfMode" Value="Insert" runat="server" Visible="false" />
                                                    <asp:RequiredFieldValidator ID="rfvnameinpassport" runat="server" ControlToValidate="txtnameinpassport"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="PassportNumber"></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <asp:TextBox ID="txtPassport" CssClass="textbox_mandatory" BackColor="Info" MaxLength="45"
                                                        runat="server" Width="182px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvpassport" runat="server" ControlToValidate="txtPassport"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="trLeft">
                                                    <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:DocumentsCommon,PlaceofIssue%>'></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <asp:TextBox ID="txtPlaceofIssue" CssClass="textbox_mandatory" BackColor="Info" MaxLength="45"
                                                        runat="server" Width="182px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvplaceofissue" runat="server" ControlToValidate="txtPlaceofIssue"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                                </td>
                                                <td valign="top" class="trRight" style="width: 384px">
                                                    <asp:UpdatePanel ID="updCountryOfOrigin" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddlCountryOfOrigin" runat="server" CssClass="dropdownlist_mandatory"
                                                                Width="190px" DataTextField="CountryName" DataValueField="CountryID">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="imgCountryOfOrigin" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                Text="..." OnClick="imgCountryOfOrigin_Click" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <%-- <uc:ReferenceControlNew ID="rcCountryOfOrigin" runat="server" ValueType="country" ExcludeItems="-1"
                                                        TableName="CountryReference" DataValueField="CountryID" DataTextField="Description"
                                                        ValidationGroup="Employee" MaxLength="45" />--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="PlaceofBirth"></asp:Literal>
                                                </td>
                                                <td valign="top" class="trRight" style="width: 384px">
                                                    <asp:TextBox ID="txtplaceofbirth" CssClass="textbox" MaxLength="45" runat="server"
                                                        Width="182px"></asp:TextBox>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("IssueDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnIssuedate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceIssuedate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnIssuedate" TargetControlID="txtIssuedate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvIssuedate" runat="server" ControlToValidate="txtIssuedate"
                                                                    ClientValidationFunction="validatepassportIssuedate" CssClass="error" ValidateEmptyText="True"
                                                                    ValidationGroup="Employee" Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft">
                                                    <asp:Literal ID="Literal21" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                </td>
                                                <td class="trRight" style="width: 384px">
                                                    <table cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td width="110">
                                                                <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" Width="100px" MaxLength="10"
                                                                    Text='<%# GetDate(Eval("ExpiryDate")) %>'></asp:TextBox>
                                                            </td>
                                                            <td style="padding-top: 3px" width="20">
                                                                <asp:ImageButton ID="btnExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                                    CausesValidation="False" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Animated="true"
                                                                    Format="dd/MM/yyyy" PopupButtonID="btnExpirydate" TargetControlID="txtExpiryDate" />
                                                            </td>
                                                            <td>
                                                                <asp:CustomValidator ID="cvExpirydate" runat="server" ControlToValidate="txtExpiryDate"
                                                                    CssClass="error" ValidateEmptyText="True" ValidationGroup="Employee" ClientValidationFunction="validatepassportExpirydate"
                                                                    Display="Dynamic"></asp:CustomValidator>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <asp:CheckBox ID="chkPagesAvailable" runat="server" meta:resourcekey="Pages" TextAlign="Left" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px" class="trLeft">
                                                    <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal>
                                                </td>
                                                <td class="trRight">
                                                    <asp:Label ID="lblCurrentStatus" runat="server" Text="New" Width="30%"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 95%; padding-left: 5px; height: auto;" colspan="2">
                                                    <asp:UpdatePanel ID="upnlRemarks" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <fieldset style="padding: 3px 1px; border: 1px #34aee1 solid; width: 72%; margin-left: 5px;">
                                                                <legend class="innerdivheaderValue" style="width: auto">
                                                                    <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>
                                                                </legend>
                                                                <div style="width: 100%; height: auto">
                                                                    <div class="trLeft" style="width: 40%; float: left; height: 100%; margin-top: 5px;">
                                                                        <asp:DropDownList ID="ddlRemarks" runat="server" Width="190px" AutoPostBack="true"
                                                                            OnSelectedIndexChanged="ddlRemarks_SelectedIndexChanged">
                                                                            <asp:ListItem Value="1" meta:resourcekey="Residence"></asp:ListItem>
                                                                            <asp:ListItem Value="2" meta:resourcekey="OldPassport"></asp:ListItem>
                                                                            <asp:ListItem Value="3" meta:resourcekey="EntryRestrictions"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="trRight" style="width: 50%; float: right; padding-left: 10px; margin-bottom: 10px;">
                                                                        <div id="divResidencePermit" runat="server" style="float: right; display: block;
                                                                            width: 100%">
                                                                            <asp:TextBox ID="txtResidencePermit" runat="server" Width="93%" TextMode="MultiLine"
                                                                                Height="76px" MaxLength="200" onchange="RestrictMulilineLength(this, 200);" onkeyup="RestrictMulilineLength(this, 200);"
                                                                                Style="padding: 5px;"></asp:TextBox>
                                                                        </div>
                                                                        <div id="divOldPassport" runat="server" style="float: left; display: none; width: 100%">
                                                                            <asp:TextBox ID="txtOldPassport" runat="server" CssClass="textbox" Width="215px"
                                                                                TextMode="MultiLine" Height="76px" MaxLength="50" onchange="RestrictMulilineLength(this, 50);"
                                                                                onkeyup="RestrictMulilineLength(this, 50);" Style="padding: 5px;"></asp:TextBox>
                                                                        </div>
                                                                        <div id="divEntryRestrictions" runat="server" style="float: left; display: none;
                                                                            width: 100%">
                                                                            <asp:TextBox ID="txtEntryRestrictions" runat="server" CssClass="textbox" Width="215px"
                                                                                TextMode="MultiLine" Height="76px" MaxLength="50" onchange="RestrictMulilineLength(this, 50);"
                                                                                onkeyup="RestrictMulilineLength(this, 50);" Style="padding: 5px;"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top" class="trLeft" colspan="2">
                                                    <div id="divParse" style="width: 100%; padding-left: 1px">
                                                        <fieldset style="padding: 3px 1px;">
                                                            <legend>
                                                                <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>
                                                            </legend>
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" width="175px" style="padding-left: 10px;">
                                                                        <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                                    </td>
                                                                    <td style="width: 2px">
                                                                    </td>
                                                                    <td valign="top" width="250px">
                                                                        <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:DocumentsCommon,ChooseFile%>'></asp:Literal>
                                                                    </td>
                                                                    <td valign="top" width="80px">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="175px" style="padding-left: 10px;">
                                                                        <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox" MaxLength="30" Width="200px"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 2px">
                                                                    </td>
                                                                    <td valign="top" width="250px">
                                                                        <AjaxControlToolkit:AsyncFileUpload ID="fuPassport" runat="server" CompleteBackColor="White"
                                                                            ErrorBackColor="White" OnUploadedComplete="fuPassport_UploadedComplete" PersistFile="True"
                                                                            Visible="true" Width="250px" />
                                                                    </td>
                                                                    <td valign="top" width="80px">
                                                                        <asp:LinkButton ID="btnattachpassdoc" runat="server" Enabled="true" OnClick="btnattachpassdoc_Click"
                                                                            Text='<%$Resources:DocumentsCommon,Addtolist%>' ValidationGroup="Employee1" ForeColor="#33a3d5">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="Employee1"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="cvPassportdocnameduplicate" runat="server" ClientValidationFunction="validatePassportdocnameduplicate"
                                                                            ControlToValidate="txtDocname" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'
                                                                            ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                    <td colspan="3">
                                                                        <asp:CustomValidator ID="cvPassportAttachment" runat="server" ClientValidationFunction="validatepassportAttachment"
                                                                            CssClass="error" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                                                            ValidateEmptyText="True" ValidationGroup="Employee1"></asp:CustomValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </fieldset>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="100px" valign="top" class="trLeft" colspan="2">
                                                    <div id="divPassDoc" runat="server" class="container_content" style="display: inline;
                                                        width: 100%; padding-left: 1px; overflow: auto">
                                                        <asp:UpdatePanel ID="updldlPassportDoc" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DataList ID="dlPassportDoc" runat="server" DataKeyField="Node" GridLines="Horizontal"
                                                                    OnItemCommand="dlPassportDoc_ItemCommand" Width="100%">
                                                                    <HeaderStyle CssClass="datalistheader" />
                                                                    <HeaderTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="150">
                                                                                    <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                                                </td>
                                                                                <td width="130">
                                                                                    <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal>
                                                                                </td>
                                                                                <td align="right" width="20">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table width="100%">
                                                                            <tr>
                                                                                <td width="150">
                                                                                    <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="150px"></asp:Label>
                                                                                </td>
                                                                                <td width="130">
                                                                                    <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="150px"></asp:Label>
                                                                                </td>
                                                                                <td align="right" width="20">
                                                                                    <asp:ImageButton ID="btnRemove" runat="server" CommandArgument='<% # Eval("Node") %>'
                                                                                        ToolTip='<%$Resources:ControlsCommon,Delete%>' CommandName="REMOVE_ATTACHMENT"
                                                                                        SkinID="DeleteIconDatalist" ImageUrl="~/images/Delete_Icon_Datalist.png" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="btnattachpassdoc" EventName="Click" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%--<fieldset>--%>
                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td class="trRight">
                                        <div style="width: 98%" align="right">
                                            <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" Width="75px" Text='<%$Resources:ControlsCommon,Submit%>'
                                                ToolTip='<%$Resources:ControlsCommon,Submit%>' CommandName="SUBMIT" ValidationGroup="Employee"
                                                OnClick="btnSubmit_Click" />
                                            &nbsp;
                                            <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" Width="75px" Text='<%$Resources:ControlsCommon,Cancel%>'
                                                ToolTip='<%$Resources:ControlsCommon,Cancel%>' OnClick="btnCancel_Click" CausesValidation="false" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <%-- </fieldset>--%>
                        </div>
                        <div id="divSort" runat="server" style="padding-left: 367px;display: block">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblSort" runat="server" meta:resourcekey="SortBy"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlExpression" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlExpression_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1" meta:resourcekey="EmployeeNumber"></asp:ListItem>
                                            <asp:ListItem Value="2" meta:resourcekey="EmployeeName"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOrder" runat="server" BackColor="AliceBlue" OnSelectedIndexChanged="ddlOrder_SelectedIndexChanged"
                                            AutoPostBack="true">
                                            <asp:ListItem Value="1" meta:resourcekey="Ascending"></asp:ListItem>
                                            <asp:ListItem Value="2" meta:resourcekey="Descending"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <asp:DataList ID="dlEmployeepassport" runat="server" BorderWidth="0px" CellPadding="0"
                            CssClass="labeltext" Width="100%" OnItemCommand="dlEmployeepassport_ItemCommand"
                            DataKeyField="PassportID" OnItemDataBound="dlEmployeepassport_ItemDataBound">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td align="center" height="30" width="30">
                                            <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkEmployee');" />
                                        </td>
                                        <td style="padding-left: 5px;">
                                            <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>'></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <HeaderStyle CssClass="listItem" />
                            <ItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 4px" id="tblDetails"
                                    runat="server">
                                    <%--onmouseover="showEdit(this);" onmouseout="hideEdit(this);"--%>
                                    <tr>
                                        <td valign="top" width="30" style="padding-top: 10px">
                                            <asp:CheckBox ID="chkEmployee" runat="server" />
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%; margin-top: 5px" cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="btnEmployee" CausesValidation="false" runat="server" CommandArgument='<%# Eval("PassportID")%>'
                                                                        CommandName="VIEW" Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("EmployeeFullName")) %>'
                                                                        CssClass="listHeader bold"></asp:LinkButton>
                                                                    [<%# Eval("EmployeeNumber")%>] &nbsp
                                                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="PassportNumber"></asp:Literal>
                                                                    -
                                                                    <%# Eval("PassportNumber")%>
                                                                </td>
                                                                <td>
                                                                    &nbsp;
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                        display: block;" CommandArgument='<%# Eval("PassportID")%>' CommandName="ALTER"
                                                                        ImageUrl="~/images/edit.png" ToolTip='<%$Resources:ControlsCommon,Edit%>'></asp:ImageButton>
                                                                </td>
                                                                <td style="visibility: hidden">
                                                                    <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 20px">
                                            <table width="100%">
                                                <tr>
                                                    <td class="innerdivheader">
                                                        <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:DocumentsCommon,PlaceofIssue%>'></asp:Literal>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("PlaceofIssue")%>
                                                    </td>
                                                    <td style="color: #FF0000; font-weight: bold; float: right;">
                                                        <%# Eval("IsRenewed")%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="innerdivheader">
                                                        <asp:Literal ID="Literal28" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# Eval("Country")%>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="innerdivheader">
                                                        <asp:Literal ID="Literal29" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                                    </td>
                                                    <td>
                                                        :
                                                    </td>
                                                    <td>
                                                        <%# GetDate(Eval("ExpiryDate")) %>&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                </td> </tr> </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <asp:FormView ID="fvPassport" runat="server" BorderWidth="0px" CellPadding="0" Width="100%"
                            CellSpacing="0" DataKeyNames="PassportID,IsDelete" CssClass="labeltext">
                            <ItemTemplate>
                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                    <tr>
                                        <td class="trLeft" width="150">
                                            <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("EmployeeFullName")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal30" runat="server" meta:resourcekey="PassportNumber"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("PassportNumber")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal31" runat="server" meta:resourcekey="NameinPassport"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("NameinPassport")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:DocumentsCommon,PlaceofIssue%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("PlaceofIssue")%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:DocumentsCommon,Country%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("Country")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal34" runat="server" meta:resourcekey="PlaceofBirth"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("PlaceOfBirth")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# GetDate(Eval("IssueDate")) %>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# GetDate(Eval("ExpiryDate")) %>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:DocumentsCommon,IsRenewed%>'></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left">
                                            <%#Eval("IsRenewed")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: auto;">
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal38" runat="server" meta:resourcekey="OldPassport"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left" style="word-break: break-all;">
                                            <%# Eval("OldPassportNumbers")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: auto;">
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal39" runat="server" meta:resourcekey="EntryRestrictions"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left" style="word-break: break-all;">
                                            <%#Eval("EntryRestrictions")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="height: auto;">
                                        <td class="trLeft">
                                            <asp:Literal ID="Literal40" runat="server" meta:resourcekey="Residence"></asp:Literal>
                                        </td>
                                        <td class="trRight" align="left" style="word-break: break-all;">
                                            <%#Eval("ResidencePermits")%>&nbsp;
                                        </td>
                                    </tr>
                                    <tr style="visibility: hidden">
                                        <td class="trLeft">
                                            Is Delete Possible
                                        </td>
                                        <td class="trRight" align="left">
                                            <%# Eval("IsDelete")%>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:FormView>
                        <uc:Pager ID="pgrEmployees" runat="server" OnFill="Bind" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div style="text-align: center; margin-top: 10px;">
                    <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="dlEmployeepassport" EventName="ItemCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="upPrint" runat="server">
                    <ContentTemplate>
                        <div id="divPrint" runat="server" style="display: none">
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btnIssueReceipt" runat="server" />
            </div>
            <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                </uc1:DocumentReceiptIssue>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                    TargetControlID="txtSearch" WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>'
                    WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="ImgSearch" ImageUrl="~/images/search.png " OnClick="btnSearch_Click"
                        runat="server" />
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddPass" ImageUrl="~/images/New_Passport_Large.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkAddPass" runat="server" OnClick="lnkAddPass_Click" meta:resourcekey="AddPassport">
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListPass" ImageUrl="~/images/List_Passport_large.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkListPass" runat="server" OnClick="lnkListEmp_Click" meta:resourcekey="ViewPassport"> 
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'>
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkPrint" runat="server" OnClick="lnkPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'>
		
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name">
                    <h5>
                        <asp:LinkButton ID="lnkEmail" runat="server" OnClick="lnkEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'>
                        </asp:LinkButton></h5>
                </div>
                <div id="divIssueReceipt" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div id="Div2" class="name" runat="server">
                        <h5>
                            <asp:LinkButton ID="lnkDocumentIR" runat="server" OnClick="lnkDocumentIR_Click" Text='<%$Resources:DocumentsCommon,Receipt%>'>
                             
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div id="divRenew" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div id="Div3" class="name" runat="server">
                        <h5>
                            <asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click" Text='<%$Resources:DocumentsCommon,Renew%>'>
                             
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
