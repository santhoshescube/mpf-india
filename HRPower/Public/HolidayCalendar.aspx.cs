﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;

public partial class Public_HolidayCalendar : System.Web.UI.Page
{
    clsHoliday objHoliday;

    clsRoleSettings objRoleSettings;
    clsUserMaster objUserMaster;
    private string CurrentSelectedValue
    {
        get;
        set;
    }
    bool isSameDate = true;
    bool blnAddMode = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        //master_public master = (master_public)this.Master;
        //if (master != null) master.ParentMenu = master_public.Menu.Settings;

        if (!IsPostBack)
        {
            objRoleSettings = new clsRoleSettings();
            objUserMaster = new clsUserMaster();

            string WeekDay = new clsConfiguration().GetConfigurationValue(clsConfiguration.ConfigurationItems.WeekStartDay).ToUpper();
            lbMsg.Style["display"] = "none";
            switch (WeekDay)
            {
                case "MONDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Monday;
                    break;

                case "TUESDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Tuesday;
                    break;

                case "WEDNESDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Wednesday;
                    break;

                case "THURSDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Thursday;
                    break;

                case "FRIDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Friday;
                    break;

                case "SATURDAY":
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Saturday;
                    break;

                default:
                    Calendar1.FirstDayOfWeek = FirstDayOfWeek.Sunday;
                    break;

            }
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            DataTable dt = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.BankHR);  // Permission Such as view /add /delete / update 
            ViewState["Permission"] = dt;
            SetPermission();
            //if (objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Calender))
            //{
            //    FillCombo();
            //    Calender.Visible = true;
            //    //rdbHoliday.Checked = true;
            //    rdbEvents.Checked = true;
            //    if (rdbEvents.Checked)
            //    {
            //        ShowEvent();
            //    }
            //    else if (rdbHoliday.Checked)
            //    {
            //        ShowHoliday();
            //    }
            //    lblNoData.Style["display"] = "none";
            //}
            //else
            //{
            //    Calender.Visible = false;
            //    lblNoData.Style["display"]="block";
            //    lblNoData.Text = "You dont have enough permission to view this page.";
            //}

        }
        else
        {
            if (isSameDate)
                lbMsg.Text = "";
        }

        ReferenceControlNew1.onRefresh -= new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew1_onRefresh);
        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.onRefresh += new controls_ReferenceControlNew.OnPageRefresh(ReferenceControlNew1_onRefresh);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }


    public void SetPermission()
    {
        try
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            int UserID;
            UserID = objUserMaster.GetRoleId();
            if (UserID != 1 && UserID != 2 && UserID != 3)
            {
                DataTable dtm = (DataTable)ViewState["Permission"];
                if (dtm.Rows.Count > 0)
                {
                    if (dtm.Rows[0]["IsView"].ToBoolean())
                    {
                        FillCombo();
                        Calender.Visible = true;
                        rdbEvents.Checked = true;
                        if (rdbEvents.Checked)
                        {
                            ShowEvent();
                        }
                        else if (rdbHoliday.Checked)
                        {
                            ShowHoliday();
                        }
                        lblNoData.Style["display"] = "none";
                    }
                }
                else
                {
                    Calender.Visible = false;
                    lblNoData.Style["display"] = "block";
                    lblNoData.Text = "You dont have enough permission to view this page.";
                }
            }
            else
            {
                FillCombo();
                Calender.Visible = true;
                rdbEvents.Checked = true;
                if (rdbEvents.Checked)
                {
                    ShowEvent();
                }
                else if (rdbHoliday.Checked)
                {
                    ShowHoliday();
                }
                lblNoData.Style["display"] = "none";
            }
        }
        catch
        {
        }
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }
    private void FillCombo()
    {
        objHoliday = new clsHoliday();
        objUserMaster = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();

        //ddlCompany.DataSource = objHoliday.GetCompanies();
        //ddlCompany.DataBind();
        objHoliday.CompanyId = objUserMaster.GetCompanyId();
        ddlSelectCompany.DataSource = objHoliday.GetCompanies();
        ddlSelectCompany.DataBind();

       // ddlSelectCompany.Enabled = objRoleSettings.IsMenuEnabled(objUserMaster.GetRoleId(), clsRoleSettings._MENU.Manage_All);
        ddlSelectCompany.SelectedIndex = ddlSelectCompany.Items.IndexOf(ddlSelectCompany.Items.FindByValue(objUserMaster.GetCompanyId().ToString()));


        objHoliday.EmployeeId = objUserMaster.GetEmployeeId();
        int iCompanyId = 0;
        if (objHoliday.EmployeeId != -1)
        {
            objUserMaster.EmployeeID = objUserMaster.GetEmployeeId();
            iCompanyId = Convert.ToInt32(objUserMaster.GetEmployeeCompany().Rows[0]["CompanyID"]);
        }

        ddlSelectCompany.SelectedIndex = ddlSelectCompany.Items.IndexOf(ddlSelectCompany.Items.FindByValue(iCompanyId.ToString()));

        FillComboHolidays();
        //rcHolidayType.FillList();

    }

    public void SaveEventHoliday()
    {
        objHoliday = new clsHoliday();
        objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);

        objHoliday.Date = clsCommon.Convert2DateTime(lblDate.Text.Trim());
        objHoliday.Description = txtDescription.Text;
        if (rdbHoliday.Checked)
        {
            objHoliday.HolidayType = Convert.ToInt32(rcHolidayType.SelectedValue);
            if (hfId.Value != "" && Convert.ToInt32(hfId.Value) == 0)
            {
                if (!objHoliday.IsHoliday())
                {
                    if (!objHoliday.CheckPaymentExists())
                    {
                        objHoliday.InsertHoliday();
                        lbMsg.Style["display"] = "block";
                        lbMsg.Text = "Holiday saved successfully";
                        // ddlSelectCompany.ClearSelection();
                        txtDescription.Text = "";
                        rcHolidayType.SelectedIndex = -1;
                        ShowHoliday();
                        upnlCompany.Update();
                        UpdatePanel2.Update();
                        ShowHoliday();
                    }

                    else
                    {
                        lbMsg.Style["display"] = "block";
                        lbMsg.Text = "Some employee payments exists in this month. Can't edit holidays.";
                        
                    }
                }
                else
                {
                    lbMsg.Style["display"] = "block";
                    lbMsg.Text = "Holiday exists.";
                }
            }
            else
            {
                objHoliday.ID = Convert.ToInt32(hfId.Value);
                objHoliday.UpdateHoliday();
                lbMsg.Style["display"] = "block";
                lbMsg.Text = "Holiday updated successfully.";
                hfId.Value = "0";
                // ddlSelectCompany.ClearSelection();
                txtDescription.Text = "";
                rcHolidayType.SelectedIndex = -1;
                ShowHoliday();
                upnlCompany.Update();
                UpdatePanel2.Update();
                ShowHoliday();
            }

        }
        else if (rdbEvents.Checked)
        {
            objHoliday.Title = txtTitle.Text;
            if (Convert.ToUInt32(hfId.Value) == 0)
            {
                objHoliday.InsertEvent();
                lbMsg.Style["display"] = "block";
                lbMsg.Text = "Event saved successfully.";
            }
            else
            {
                objHoliday.ID = Convert.ToInt32(hfId.Value);
                objHoliday.UpdateEvent();
                lbMsg.Style["display"] = "block";
                lbMsg.Text = "Event updated successfully.";
                hfId.Value = "0";
            }

            // ddlCompany.ClearSelection();
            txtDescription.Text = "";
            txtTitle.Text = "";
            //rdbHoliday.Checked = true;
            //rdbEvents.Checked = false;
            ShowEvent();

        }
        isSameDate = true;
        //mpeHoliday.Hide();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        SaveEventHoliday();


    }
   
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)

    {

        try
        {
            bool flg = false;
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            int RoleId;
            RoleId = objUserMaster.GetRoleId();
            if (RoleId != 1 && RoleId != 2 && RoleId != 3)
            {
                DataTable dtm = (DataTable)ViewState["Permission"];
                if (dtm.Rows.Count > 0)
                {
                    if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                    {
                        flg = true;
                    }
                }
            }
            else
            {
                flg = true;
            }

            if (flg == true)
            {
                lbMsg.Style["display"] = "none";
                lbMsg.Text = "";
                txtDescription.Text = "";
                lblDate.Text = Calendar1.SelectedDate.ToString("dd/MM/yyyy");
                txtTitle.Text = "";

                objHoliday = new clsHoliday();
                objHoliday.Date = clsCommon.Convert2DateTime(Calendar1.SelectedDate.ToString("dd/MM/yyyy"));
                objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);

                ShowHoliday();
                ShowEvent();

                isSameDate = false;

                UpdatePanel1.Update();
                updModalPopUp.Update();

                mpeHoliday.Show();
                Calendar1.SelectedDates.Clear();
            }

        }
        catch
        {
        }


    }

   
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {


        try
        {
            objUserMaster = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            int RoleId;
            RoleId = objUserMaster.GetRoleId();


            TableCell cell = (TableCell)e.Cell;
            cell.Width = Unit.Pixel(96);
            cell.Height = Unit.Pixel(100);
            cell.BorderStyle = BorderStyle.Solid;
            cell.BorderWidth = Unit.Pixel(1);
            cell.BorderColor = System.Drawing.Color.Black;
            cell.Attributes.Add("title", string.Empty);

            lblDate.Text = "";
            //cell.Style["padding"] = "0";

            if (e.Day.IsOtherMonth)
            {
                e.Cell.Text = "";
            }
            else
            {
                //e.Cell.CssClass = "tblBackground";

                objHoliday = new clsHoliday();
                objHoliday.Date = clsCommon.Convert2DateTime(e.Day.Date.ToString("dd/MM/yyyy"));
                objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
                DataSet ds = objHoliday.GetEvents();
                cell.CssClass = "tblBackground";

                objHoliday = new clsHoliday();
                objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
                DataTable offDayTable = objHoliday.GetOffday();
                if (offDayTable.DefaultView.Count > 0)
                {
                    if (offDayTable.Rows[0]["Offday"].ToString() == e.Day.Date.DayOfWeek.ToString())
                    {
                        e.Cell.CssClass = "tblOffDayBackground";
                    }
                }

                string sHoliday = (ds.Tables[0].Rows.Count == 0 ? string.Empty : "Holiday:");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    sHoliday = sHoliday + " " + Convert.ToString(ds.Tables[0].Rows[0]["Holiday"]) + "\n";
                    cell.CssClass = "tblHolidayBackground";
                    cell.Font.Bold = true;
                    cell.ToolTip = sHoliday;

                }

                string sEvent = (ds.Tables[1].Rows.Count == 0 ? string.Empty : "Events: \n");
                int icount = 1;
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    sEvent = sEvent + "   " + icount.ToString() + ". " + Convert.ToString(dr["EventName"]) + "\n";
                    icount++;
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    Image imgEvent = new Image();
                    imgEvent.ImageUrl = "~/images/SavedStar.png";
                    imgEvent.ImageAlign = ImageAlign.Right;

                    imgEvent.ToolTip = sEvent;
                    e.Cell.Controls.Add(imgEvent);

                    //if (RoleId != 1 && RoleId != 2 && RoleId != 3)
                    //{
                    //    DataTable dtm = (DataTable)ViewState["Permission"];
                    //    if (dtm.Rows.Count > 0)
                    //    {
                    //        if (!dtm.Rows[0]["IsUpdate"].ToBoolean())
                    //        {
                    //            imgEvent.EnableViewState = false;
                    //            //e.Cell.Enabled = false;  
                    //        }
                    //    }
                    //}

                }



            }
        }
        catch
        {

        }

    }

    public void FillComboHolidays()
    {
        objHoliday = new clsHoliday();
        rcHolidayType.DataSource = objHoliday.GetHolidays();
        rcHolidayType.DataTextField = "Description";
        rcHolidayType.DataValueField = "HolidayTypeId";
        rcHolidayType.DataBind();

        rcHolidayType.Items.Insert(0, new ListItem("Select", "-1"));

        rcHolidayType.SelectedValue = CurrentSelectedValue;
    }

    protected void rdbHoliday_CheckedChanged(object sender, EventArgs e)
    {
        FillComboHolidays();
        ShowHoliday();
    }


    protected void rdbEvents_CheckedChanged(object sender, EventArgs e)
    {
        ShowEvent();

    }

    protected void ibtnClose_Click(object sender, ImageClickEventArgs e)
    {
        mpeHoliday.Hide();
    }
    protected void UpdatePanel2_PreRender(object sender, EventArgs e)
    {
        //if (lblDate.Text != "")
        //{           
        //    mpeHoliday.Show();

        //}
        //else
        //{
        //    lblDate.Text = Calendar1.SelectedDate.ToString("dd/MM/yyyy");
        //}
    }


    private void ShowHoliday()
    {
        tdEvent.InnerText = "Holiday";
        rdbHoliday.Checked = true;
        rdbEvents.Checked = false;
        if (Request.Browser.Browser == "IE")
            trHolidayType.Style["display"] = "block";
        else
            trHolidayType.Style["display"] = "table-row";

        trEventTitle.Style["display"] = "none";

        txtDescription.CssClass = "textbox_mandatory";
        rfvDesc.ValidationGroup = "Submit";
        rcHolidayType.ValidationGroup = "Submit";
        rfvHolidayType.ValidationGroup = "Submit";
        rfvTitle.ValidationGroup = "";
        DisplayHoliday(0);

    }
    private void ShowEvent()
    {
        tdEvent.InnerText = "Event";
        rdbEvents.Checked = true;
        rdbHoliday.Checked = false;
        if (Request.Browser.Browser == "IE")
            trEventTitle.Style["display"] = "block";
        else
            trEventTitle.Style["display"] = "table-row";

        trHolidayType.Style["display"] = "none";

        rfvHolidayType.ValidationGroup = "";
        rfvDesc.ValidationGroup = "";
        rfvTitle.ValidationGroup = "Submit";
        txtDescription.CssClass = "textbox";
        rcHolidayType.ValidationGroup = "";
        DisplayEvent(0);



    }
    private void DisplayEvent(int intEventID)
    {
        //if (ddlSelectCompany.SelectedIndex != -1)
        //{
       
        txtDescription.Text = "";
        txtTitle.Text = "";
        objHoliday = new clsHoliday();
        objHoliday.Date = clsCommon.Convert2DateTime(lblDate.Text.Trim());
        objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
        DataSet ds = objHoliday.GetFullEvents(intEventID);
        dlHoliday.DataSource = ds.Tables[0];
        dlHoliday.DataBind();
        divListHead.Visible = (dlHoliday.Items.Count > 0);
        //}

    }
    private void DisplayHoliday(int intHolidayID)
    {
        //if (ddlSelectCompany.SelectedIndex != -1)
        //{
      
        rcHolidayType.SelectedIndex = -1;
        txtDescription.Text = "";
        objHoliday = new clsHoliday();
        objHoliday.Date = clsCommon.Convert2DateTime(lblDate.Text.Trim());
        objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
        DataSet ds = objHoliday.GetFullHoliday(intHolidayID);
        dlHoliday.DataSource = ds.Tables[0];
        dlHoliday.DataBind();
        divListHead.Visible = (dlHoliday.Items.Count > 0);
        //}

    }
    protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {
        lblDate.Text = "";
    }


    protected void ddlSelectCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblDate.Text = "";
        txtTitle.Text = "";
        txtDescription.Text = "";
        lbMsg.Style["display"] = "none";
        lbMsg.Text = "";
        isSameDate = false;

    }
    protected void imgHolidayType_Click(object sender, ImageClickEventArgs e)
    {
        //    ReferenceControlNew1.TableName = "HolidayTypeReference";
        //    ReferenceControlNew1.DataTextField = "Description";
        //    ReferenceControlNew1.DataValueField = "HolidayTypeId";
        //    ReferenceControlNew1.FunctionName = "FillComboHolidays";
        //    ReferenceControlNew1.SelectedValue = rcHolidayType.SelectedValue;
        //    ReferenceControlNew1.DisplayName = "Holiday Type";
        //    ReferenceControlNew1.PopulateData();

        //    mdlPopUpReference.Show();
        //    updModalPopUp.Update();
    }
    protected void dlCurrency_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        if (rdbHoliday.Checked == true)
        {
            objHoliday = new clsHoliday();
            objHoliday.DeleteHoliday(Convert.ToInt32(((ImageButton)(sender)).CommandArgument));
            lbMsg.Style["display"] = "none";
            DisplayHoliday(0);
        }
        else if (rdbEvents.Checked == true)
        {
            objHoliday = new clsHoliday();
            objHoliday.DeleteEvent(Convert.ToInt32(((ImageButton)(sender)).CommandArgument));
            lbMsg.Style["display"] = "none";
            DisplayEvent(0);
        }

        upnlCompany.Update();
        UpdatePanel2.Update();
    }
    protected void dlHoliday_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkEvent_Click(object sender, EventArgs e)
    {
        if (rdbHoliday.Checked == true)
        {
            objHoliday = new clsHoliday();
            objHoliday.Date = clsCommon.Convert2DateTime(lblDate.Text.Trim());
            objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
            DataSet ds = objHoliday.GetParticularHoliday(Convert.ToInt32(((LinkButton)(sender)).CommandArgument));
            rcHolidayType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HolidayTypeID"]);
            txtDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
            hfId.Value = Convert.ToString(((LinkButton)(sender)).CommandArgument);
        }
        else if (rdbEvents.Checked == true)
        {
            objHoliday = new clsHoliday();
            objHoliday.Date = clsCommon.Convert2DateTime(lblDate.Text.Trim());
            objHoliday.CompanyId = Convert.ToInt32(ddlSelectCompany.SelectedValue);
            DataSet ds = objHoliday.GetParticularEvents(Convert.ToInt32(((LinkButton)(sender)).CommandArgument));
            txtTitle.Text = ds.Tables[0].Rows[0]["Name"].ToString();
            txtDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();
            hfId.Value = Convert.ToString(((LinkButton)(sender)).CommandArgument);
        }

        UpdatePanel1.Update();
        blnAddMode = false;
    }
    protected void imgAddNew_Click(object sender, ImageClickEventArgs e)
    {
        if (rdbHoliday.Checked == true)
        {
            rcHolidayType.SelectedIndex = -1;
            txtDescription.Text = "";
        }
        else if (rdbEvents.Checked == true)
        {
            txtDescription.Text = "";
            txtTitle.Text = "";
        }
        blnAddMode = true;
        hfId.Value = "0";
        lbMsg.Style["display"] = "none";
        UpdatePanel1.Update();

       
    }
    protected void Calendar1_PreRender(object sender, EventArgs e)
    {

    }
    protected void btnSaveData_Click(object sender, ImageClickEventArgs e)
    {
        SaveEventHoliday();
    }
}
