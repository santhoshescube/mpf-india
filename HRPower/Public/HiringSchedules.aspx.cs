﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_HiringSchedules : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillCalender();

        if (!IsPostBack)
        {
            FillYear();
        }
    }


    private void FillYear()
    {
        for (int i = 2014; i <= 2030; i++)
        {
            ddlYear.Items.Add(new ListItem() { Text = i.ToString(), Value = i.ToString() });
        }
    }

    private void FillCalender()
    {
        DataTable dt = new DataTable();
        DataRow dr;
        dt.Columns.Add("Designation");
        dt.Columns.Add("Required");
        dt.Columns.Add("Hired");
        dt.Columns.Add("Jan");
        dt.Columns.Add("Feb");
        dt.Columns.Add("Mar");
        dt.Columns.Add("Apr");
        dt.Columns.Add("May");
        dt.Columns.Add("Jun");
        dt.Columns.Add("July");
        dt.Columns.Add("Aug");
        dt.Columns.Add("Sep");
        dt.Columns.Add("Oct");
        dt.Columns.Add("Nov");
        dt.Columns.Add("Dec");


        dr = dt.NewRow();
        dr["Designation"] = "Software Engineer";
        dr["Required"] = 10;
        dr["Hired"] = 5;
        dr["Jan"] = "";
        dr["Feb"] = "";
        dr["Mar"] = "";
        dr["Apr"] = "";
        dr["May"] = "";
        dr["Jun"] = "";
        dr["July"] = "";
        dr["Aug"] = "";
        dr["Sep"] = "";
        dr["Oct"] = "";
        dr["Nov"] = "";
        dr["Dec"] = "";


        dt.Rows.Add(dr);
        dgHiringSchedules.DataSource = dt;
        dgHiringSchedules.DataBind();





    }
    protected void btnNew_Click(object sender, EventArgs e)
    {

    }
    protected void btnList_Click(object sender, EventArgs e)
    {

    }
    protected void lnkSchedule_Click(object sender, EventArgs e)
    {

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

    }
    protected void btnEmail_Click(object sender, EventArgs e)
    {

    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {

    }
}