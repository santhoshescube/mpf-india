<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="DocumentRequest.aspx.cs" Inherits="Public_DocumentRequest" Title="Document Request" %>

<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=divSingleView.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Document Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>
<div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png" Width= "22px"
            meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <div id="divPopUp">
        <div style="display: none">
            <asp:Button ID="btnMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="ucMessage" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <div id="divRefrenceControl">
        <asp:UpdatePanel ID="upnlModalPopUp" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="display: none">
                    <asp:Button ID="btn" runat="server" />
                </div>
                <div id="pnlModalPopUp" runat="server" style="display: none;">
                    <uc:ReferenceNew ID="ReferenceControlNew" runat="server" ModalPopupID="mdlPopUpReference" />
                </div>
                <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                    PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
                </AjaxControlToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upnlAdd" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div id="divAdd" runat="server" style="height: 100%; width: 100%; display: none">
                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    <%-- Document Type--%>
                    <asp:Literal ID="Literal19" runat="server" meta:resourcekey="DocumentType"></asp:Literal>
                </div>
                <div class="trRight" style="float: left; width: 60%; height: 29px">
                    <asp:UpdatePanel ID="upnlDocumentType" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="float: left; width: 40%; height: 29px">
                                <asp:DropDownList ID="ddlDocumentType" runat="server" Width="90%" OnSelectedIndexChanged="ddlDocumentType_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div style="float: right; width: 58%; height: 29px; padding-left: 3px">
                                <asp:RequiredFieldValidator ID="rfvDocumentType" runat="server" ControlToValidate="ddlDocumentType"
                                    meta:resourcekey="PleaseSelectDocumentType" CssClass="error" Display="Dynamic"
                                    ErrorMessage="Please Select Document Type" ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="trRight" style="width: 10%; height: 30px; float: left;">
                    <asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                </div>--%>
                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    <%-- Document Number--%>
                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="DocumentNumber"></asp:Literal>
                </div>
                <div class="trRight" style="float: left; width: 70%; height: 29px">
                    <asp:UpdatePanel ID="upnlDocumentNumber" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="float: left; width: 40%; height: 29px">
                                <asp:DropDownList ID="ddlDocumentNumber" runat="server" Width="90%">
                                </asp:DropDownList>
                            </div>
                            <div style="float: right; width: 58%; height: 29px; padding-left: 3px">
                                <asp:RequiredFieldValidator ID="rfvDocumentNumber" runat="server" ControlToValidate="ddlDocumentNumber"
                                    meta:resourcekey="PleaseSelectDocumentNumber" CssClass="error" Display="Dynamic"
                                    ErrorMessage="Please Select Document Number" ValidationGroup="Submit" InitialValue="-1"></asp:RequiredFieldValidator>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 29px">
                    <%-- Document From Date--%>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="DocumentFromDate"></asp:Literal>
                </div>
                <div class="trRight" style="float: left; width: 70%; height: 29px">
                    <div style="float: left; width: 25%; height: 29px">
                        <asp:TextBox ID="txtDocumentDate" runat="server" CssClass="textbox_mandatory" Width="97px"
                            Style="margin-right: 10px;" MaxLength="10"></asp:TextBox>
                        <asp:ImageButton ID="ibtnDocumentDate" style="margin-top: 9px;" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                            CssClass="imagebutton" CausesValidation="false" />
                        <AjaxControlToolkit:CalendarExtender ID="ceDocumentDate" runat="server" TargetControlID="txtDocumentDate"
                            PopupButtonID="ibtnDocumentDate" Format="dd/MM/yyyy">
                        </AjaxControlToolkit:CalendarExtender>
                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteDocumentDate" TargetControlID="txtDocumentDate"
                            runat="server" FilterType="Custom,Numbers" ValidChars="/" />
                    </div>
                    <div style="float: right; width: 72%; height: 29px; padding-left: 3px">
                        <asp:CustomValidator ID="cvDocumentDate" runat="server" ValidateEmptyText="true"
                            ControlToValidate="txtDocumentDate" Display="Dynamic" ClientValidationFunction="ValidateDocumentDate"
                            ValidationGroup="Submit" CssClass="error"></asp:CustomValidator>
                    </div>
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 55px">
                    <%-- Reason--%>
                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Reason"></asp:Literal>
                </div>
                <div class="trRight" style="float: left; width: 70%; height: 60px;">
                    <div style="float: left; width: 50%; height: 100%;">
                        <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Width="250px"
                            Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                            onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox>
                    </div>
                    <div style="float: right; width: 48%; height: 100%; padding-left: 3px">
                        <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                            meta:resourcekey="PleaseEnterReason" CssClass="error" Display="Dynamic" ErrorMessage="Please Enter Reason"
                            ValidationGroup="Submit"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div id="divIssueStatus" runat="server">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%-- Status--%>
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Status"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:DropDownList ID="ddlIssueStatus" runat="server" Width="90%" OnSelectedIndexChanged="ddlIssueStatus_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div id="divIssue" runat="server" style="display: none;">
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%--Issued By--%>
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="IssuedBy"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:Label ID="lblIssuedBy" runat="server" Text="" Style="color: Black;"></asp:Label>
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%-- Reason For Issue--%>
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ReasonForIssue"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 29px">
                        <asp:UpdatePanel ID="upnlReason" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="float: left; width: 45%; height: 29px">
                                    <asp:DropDownList ID="ddlReason" runat="server" Width="90%">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnReason" runat="server" CausesValidation="False" CssClass="referencebutton"
                                        Text="..." OnClick="btnReason_Click" />
                                </div>
                                <div style="float: right; width: 50%; height: 29px; padding-left: 3px;">
                                    <asp:CustomValidator ID="cvReasonForIssue" runat="server" ValidationGroup="Submit"
                                        ClientValidationFunction="ValidateIssueReason" ControlToValidate="ddlReason"></asp:CustomValidator>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%--Bin Number--%>
                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="BinNumber"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 72%; height: 29px">
                        <asp:Label ID="lblBinNumber" runat="server" Style="color: Black;" />
                        <asp:Label ID="lblBinID" runat="server" Visible="false" />
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%--  Issue Date--%>
                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="IssueDate"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 72%; height: 29px">
                        <asp:TextBox ID="txtIssueDate" runat="server" CssClass="textbox_mandatory" Width="85px"
                            Style="margin-right: 10px;" MaxLength="10"></asp:TextBox>
                        <asp:ImageButton ID="ibtnIssueDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                            CssClass="imagebutton" CausesValidation="false" />
                        <AjaxControlToolkit:CalendarExtender ID="ceIssueDate" runat="server" TargetControlID="txtIssueDate"
                            PopupButtonID="ibtnIssueDate" Format="dd/MM/yyyy">
                        </AjaxControlToolkit:CalendarExtender>
                        <asp:CustomValidator ID="cvIssueDate" runat="server" ValidationGroup="Submit" ValidateEmptyText="true"
                            ClientValidationFunction="ValidateIssueDate" ControlToValidate="txtIssueDate"></asp:CustomValidator>
                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteIssueDate" TargetControlID="txtIssueDate"
                            runat="server" FilterType="Custom,Numbers" ValidChars="/" />
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 29px">
                        <%-- Expected Return Date--%>
                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="ExpectedReturnDate"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 72%; height: 29px">
                        <asp:TextBox ID="txtExpectedReturnDate" runat="server" Width="85px" MaxLength="10"
                            Style="margin-right: 10px;"></asp:TextBox>
                        <asp:ImageButton ID="ibtnExpectedReturnDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                            CssClass="imagebutton" CausesValidation="false" />
                        <AjaxControlToolkit:CalendarExtender ID="ceExpectedReturnDate" runat="server" TargetControlID="txtExpectedReturnDate"
                            PopupButtonID="ibtnExpectedReturnDate" Format="dd/MM/yyyy">
                        </AjaxControlToolkit:CalendarExtender>
                        <asp:CustomValidator ID="cvReturnDate" runat="server" ValidationGroup="Submit" ClientValidationFunction="ValidateReturnDate"
                            ControlToValidate="txtExpectedReturnDate"></asp:CustomValidator>
                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteExpectedReturnDate" TargetControlID="txtExpectedReturnDate"
                            runat="server" FilterType="Custom,Numbers" ValidChars="/" />
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 55px">
                        <%--  Remarks--%>
                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 60px;">
                        <asp:TextBox ID="txtRemarks" runat="server" Width="250px" Height="50px" TextMode="MultiLine"
                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLength(this, 500);"
                            MaxLength="500"></asp:TextBox>
                    </div>
                    <div class="trLeft" style="width: 75%; height: 30px; float: left;">
                        <asp:LinkButton ID="lbEditReason" Text="Click to View Approval Details" runat="server"
                            OnClick="lbEditReason_OnClick"></asp:LinkButton>
                        <asp:HiddenField ID="hfEditlink" runat="server" Value="0" />
                    </div>
                    <div class="trLeft" style="width: 75%; height: auto; float: left; overflow-y: scroll;
                        overflow-x: hidden; word-break: break-all;" id="trEditReasons" runat="server">
                        <asp:GridView ID="gvEditReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                            Width="800px">
                            <HeaderStyle CssClass="datalistheader" />
                            <Columns>
                                <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                    ItemStyle-Width="300px" />
                                <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                        <asp:Label ID="lbl3" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                    <div id="div3" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                        overflow-x: hidden;" runat="server">                      
                        <asp:DataList ID="dl3" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                            <ItemStyle ForeColor="#307296" Font-Bold="true" />
                            <ItemTemplate>
                                <asp:Table Width="99%">
                                    <tr style="width: 99%; color: #307296;">
                                        <td>
                                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                            <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Table>
                            </ItemTemplate>
                        </asp:DataList></div>
                </div>
                <div style="width: 100%; height: auto; text-align: center; margin-top: 15px;">
                    <div class="trLeft" style="float: left; width: 25%; height: 30px">
                    </div>
                    <div class="trRight" style="float: left; width: 70%; height: 30px;">
                        <asp:Button ID="btnSubmit" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Submit%>'
                            Text='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="Submit" Width="75px"
                            OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnCancel" CssClass="btnsubmit" runat="server" ToolTip='<%$Resources:ControlsCommon,Cancel%>'
                            CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>' Width="75px"
                            OnClick="btnCancel_Click" />
                    </div>
                </div>
                <asp:HiddenField ID="hdnRequestID" runat="server" />
                <asp:HiddenField ID="hdnEmployeeID" runat="server" />
                <asp:HiddenField ID="hdnAddMode" runat="server" />
                <asp:HiddenField ID="hdnIsHigherAuthority" runat="server" />
                <asp:HiddenField ID="hdnIsFromHomePage" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divView" runat="server" style="width: 100%; display: block;">
                <asp:DataList ID="dlDocumentRequest" runat="server" Width="100%" DataKeyField="RequestId"
                    CssClass="labeltext" OnItemCommand="dlDocumentRequest_ItemCommand" OnItemDataBound="dlDocumentRequest_ItemDataBound">
                    <ItemStyle CssClass="item" />
                    <HeaderStyle CssClass="listItem" />
                    <HeaderTemplate>
                        <table width="100%">
                            <tr>
                                <td width="25" valign="top" style="padding-left: 5px; padding-top: 5px;">
                                    <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkDocument')" />
                                </td>
                                <td style="padding-left: 7px">
                                    <%--Select All  ToolTip='<%$Resources:ControlsCommon,Save%>'--%>
                                    <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,SelectAll%>' />
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                        <tr>
                                            <td valign="top" rowspan="5" width="25" style="padding-left: 7px; padding-top: 5px;">
                                                <asp:CheckBox ID="chkDocument" Style="margin-top: 5px;" runat="server" />
                                            </td>
                                            <td width="100%">
                                                <table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding-left: 7px">
                                                    <tr>
                                                        <td width="150">
                                                            <asp:LinkButton ID="lnkView" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                CommandName="View" CausesValidation="False">
                                                               <%# Eval("Date")%>  </asp:LinkButton>
                                                        </td>
                                                        <td valign="top" align="right">
                                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                meta:resourcekey="TooltipEdit" CommandName="Edit" ImageUrl="~/images/edit.png" />
                                                            &nbsp;
                                                            <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                meta:resourcekey="TooltipCancel" CommandName="Cancel" ImageUrl="~/images/cancel.png"
                                                                OnClientClick="return confirm('Do you want to cancel this request?')" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top" style="padding-left: 25px">
                                                <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="20%" valign="top" class="innerdivheader">
                                                            <%--Document Type--%>
                                                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="DocumentType"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="5%">
                                                            :
                                                        </td>
                                                        <td align="left" valign="top" width="75%">
                                                            <%# Eval("DocumentType") %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" valign="top" class="innerdivheader">
                                                            <%--  Document Number--%>
                                                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="DocumentNumber"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="5%">
                                                            :
                                                        </td>
                                                        <td align="left" valign="top" width="75%">
                                                            <%# Eval("DocumentNumber")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" valign="top" class="innerdivheader">
                                                            <%-- Document From Date--%>
                                                            <asp:Literal ID="Literal13" runat="server" meta:resourcekey="DocumentFromDate"></asp:Literal>
                                                        </td>
                                                        <td valign="top" width="5%">
                                                            :
                                                        </td>
                                                        <td align="left" valign="top" width="75%">
                                                            <%# Eval("DocumentFromDate")%>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" valign="top" class="innerdivheader">
                                                            <%-- Status--%>
                                                            <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                        </td>
                                                        <td width="5%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" width="75%">
                                                            <%# Eval("Status") %>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" valign="top" class="innerdivheader">
                                                            <%--Requested To--%>
                                                            <asp:Literal ID="Literal15" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                        </td>
                                                        <td width="5%">
                                                            :
                                                        </td>
                                                        <td valign="top" align="left" width="75%">
                                                            <%# Eval("RequestedTo")%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblStatusID" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:Label>
                                                <asp:Label ID="lblForwarded" Text='<%# Eval("Forwarded")%>' Visible="false" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
                <uc:Pager ID="DocumentRequestPager" runat="server" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="dlDocumentRequest" EventName="ItemCommand" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlSingleView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSingleView" runat="server" style="display: none; width: 100%">
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%--Document Type--%>
                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="DocumentType"></asp:Literal>
                </div>
                <div class="trRight" id="divDocumentType" runat="server" style="float: left; color: Black;
                    width: 60%; height: 20px">
                </div>
                <%--<div class="trLeft" style="width: 8%; height: 30px; float: left;">
                    <asp:ImageButton ID="imgBack2" runat="server" ImageUrl="~/images/back-button.png"
                        meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />
                </div>--%>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Document Number--%>
                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="DocumentNumber"></asp:Literal>
                </div>
                <div class="trRight" id="divDocumentNumber" runat="server" style="float: left; color: Black;
                    width: 70%; height: 20px">
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Document From Date--%>
                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="DocumentFromDate"></asp:Literal>
                </div>
                <div class="trRight" id="divDocumentDate" runat="server" style="float: left; color: Black;
                    width: 70%; height: 20px">
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Reason--%>
                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Reason"></asp:Literal>
                </div>
                <div class="trRight" id="divReason" runat="server" style="float: left; color: Black;
                    width: 70%; word-break: break-all; height: auto; max-height: 250px">
                </div>
                <div id="divViewIssue" runat="server" style="display: none">
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%--   Issued By--%>
                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="IssuedBy"></asp:Literal>
                    </div>
                    <div class="trRight" id="divIssuedBy" runat="server" style="float: left; color: Black;
                        width: 70%; height: 20px">
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%--Reason For Issue--%>
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="ReasonForIssue"></asp:Literal>
                    </div>
                    <div class="trRight" id="divReasonForIssue" runat="server" style="float: left; color: Black;
                        word-break: break-all; max-height: 500px; width: 70%;">
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%-- Bin Number--%>
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="BinNumber"></asp:Literal>
                    </div>
                    <div class="trRight" id="divBinNumber" runat="server" style="float: left; color: Black;
                        width: 70%; height: 20px">
                    </div>
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%-- Issue Date--%>
                        <asp:Literal ID="Literal23" runat="server" meta:resourcekey="IssueDate"></asp:Literal>
                    </div>
                    <div class="trRight" id="divIssueDate" runat="server" style="float: left; color: Black;
                        width: 70%; height: 20px">
                    </div>
                    <div id="divViewReturnDate" runat="server">
                        <div class="trLeft" style="float: left; width: 25%; height: 20px">
                            <%--  Expected Return Date--%>
                            <asp:Literal ID="Literal24" runat="server" meta:resourcekey="ExpectedReturnDate"></asp:Literal>
                        </div>
                        <div class="trRight" id="divExpectedReturnDate" runat="server" style="float: left;
                            color: Black; width: 70%; height: 20px">
                        </div>
                    </div>
                    <div id="divViewRemarks" runat="server">
                        <div class="trLeft" style="float: left; width: 25%; height: 20px">
                            <%--  Remarks--%>
                            <asp:Literal ID="Literal25" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                        </div>
                        <div class="trRight" id="divRemarks" runat="server" style="float: left; color: Black;
                            width: 70%; word-break: break-all; max-height: 500px;">
                        </div>
                    </div>
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%--Status--%>
                    <asp:Literal ID="Literal26" runat="server" meta:resourcekey="Status"></asp:Literal>
                </div>
                <div class="trRight" id="divStatus" runat="server" style="float: left; color: Black;
                    width: 70%; height: 20px">
                </div>
                <div>
                    <asp:HiddenField ID="hdnStatusID" runat="server" Value="0" />
                    <asp:HiddenField ID="hdnForwarded" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 75%; height: 30px; float: left;">
                    <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                        OnClick="lbReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 75%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden; word-break: break-all;" id="trReasons" runat="server">
                    <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="Div1" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                        <ItemTemplate>
                                <asp:Table Width="99%">
                                    <tr style="width: 99%; color: #307296;">
                                        <td>
                                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                            <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Table>
                            </ItemTemplate>
                    </asp:DataList></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnlAuthorityView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divAuthorityView" runat="server" style="display: none; width: 100%">
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Document Type--%>
                    <asp:Literal ID="Literal27" runat="server" meta:resourcekey="DocumentType"></asp:Literal>
                </div>
                <div class="trRight" id="divViewDocumentType" runat="server" style="float: left;
                    color: Black; width: 60%; height: 20px">
                </div>
                <%--<div class="trRight" style="width: 10%; height: 30px; float: left;">
                    <asp:ImageButton ID="imgBackApporove" runat="server" ImageUrl="~/images/back-button.png"
                        ToolTip="Back to previous page" OnClick="imgback_Click" />
                </div>--%>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Document Number--%>
                    <asp:Literal ID="Literal28" runat="server" meta:resourcekey="DocumentNumber"></asp:Literal>
                </div>
                <div class="trRight" id="divViewDocumentNumber" runat="server" style="float: left;
                    color: Black; width: 70%; height: 20px">
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Requested By--%>
                    <asp:Literal ID="Literal29" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                </div>
                <div class="trRight" id="divViewRequestedBy" runat="server" style="float: left; width: 70%;
                    color: Black; height: 20px">
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    <%-- Document From Date--%>
                    <asp:Literal ID="Literal30" runat="server" meta:resourcekey="DocumentFromDate"></asp:Literal>
                </div>
                <div class="trRight" id="divViewDocumentDate" runat="server" style="float: left;
                    color: Black; width: 70%; height: 20px">
                </div>
                <div class="trLeft" style="float: left; width: 25%; height: 20px;">
                    <%--Reason--%>
                    <asp:Literal ID="Literal31" runat="server" meta:resourcekey="Reason"></asp:Literal>
                </div>
                <div class="trRight" runat="server" style="float: left; width: 70%; color: Black;
                    word-break: break-all; height: auto; max-height: 250px">
                    <%--id="divViewReason"--%>
                    <asp:Label ID="lblEditReason" runat="server"></asp:Label>
                </div>
                <div class="trLeft" style="float: left; width: 75%; margin-left: 241px;">
                    <asp:TextBox ID="txtEditReason" runat="server" CssClass="textbox_mandatory" Width="250px"
                        Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                        onkeyup="RestrictMulilineLength(this, 500);" MaxLength="500"></asp:TextBox><%--Visible="false" --%>
                </div>
                <div id="divStatusText" runat="server" style="display: none">
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%--  Status--%>
                        <asp:Literal ID="Literal32" runat="server" meta:resourcekey="Status"></asp:Literal>
                    </div>
                    <div class="trRight" id="divViewStatus" runat="server" style="float: left; width: 70%;
                        color: Black; height: 20px">
                    </div>
                </div>
                <div id="divddlStatus" runat="server">
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                        <%--Status--%>
                        <asp:Literal ID="Literal33" runat="server" meta:resourcekey="Status"></asp:Literal>
                    </div>
                    <div class="trRight" runat="server" style="float: left; width: 70%; height: 20px">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="trLeft" style="width: 75%; height: 30px; float: left;">
                    <asp:LinkButton ID="lbApproveLink" meta:resourcekey="ClicktoViewApprovalDetails"
                        runat="server" OnClick="lbApproveReason_OnClick"></asp:LinkButton>
                    <asp:HiddenField ID="hfApproveLink" runat="server" Value="0" />
                </div>
                <div class="trLeft" style="width: 75%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden; word-break: break-all;" id="trApproveReason" runat="server">
                    <asp:GridView ID="gvApproveReason" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                        Width="800px">
                        <HeaderStyle CssClass="datalistheader" />
                        <Columns>
                            <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                            <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                ItemStyle-Width="300px" />
                            <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                            <asp:BoundField DataField="EntryDate" meta:resourcekey="Date" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                    <asp:Label ID="lbl2" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                <div id="div2" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                    overflow-x: hidden;" runat="server">
                    <asp:DataList ID="dl2" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                       <ItemTemplate>
                                <asp:Table Width="99%">
                                    <tr style="width: 99%; color: #307296;">
                                        <td>
                                            <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                            <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </asp:Table>
                            </ItemTemplate>
                    </asp:DataList></div>
                <div id="divApprove" runat="server">
                    <div class="trLeft" style="float: left; width: 25%; height: 20px">
                    </div>
                    <div class="trRight" runat="server" style="float: left; width: 70%; height: 20px">
                        <asp:Button ID="btnApprove" runat="server" Text='<%$Resources:ControlsCommon,Submit%>'
                            CssClass="btnsubmit" OnClick="btnApprove_Click" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add_DocumentRequestBig.png" runat="server"
                            CausesValidation="false" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="RequestDocument"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/View document request.png" runat="server"
                            CausesValidation="false" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="ViewRequest"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" CausesValidation="false" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="DeleteRequest"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
