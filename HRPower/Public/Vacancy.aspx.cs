﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;

public partial class Public_Vacancy : System.Web.UI.Page
{
    #region Declarations

    clsVacancy objVacancy = new clsVacancy();
    clsUserMaster objUserMaster = new clsUserMaster();

    #endregion

    #region Events
    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        // Auto complete
        this.RegisterAutoComplete();
        pgrJobs.Fill += new controls_Pager.FillPager(BindDetails);

        if (!IsPostBack)
        {
            EnableMenus();
            ListJobs();
            if (Request.QueryString["dispMsg"] != null)
            {
                string msg = string.Empty;
                switch (Request.QueryString["dispMsg"].ToString())
                {
                    case "Rejected":
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد رفض الوظيفة") : ("Vacancy has been Rejected");
                        break;
                    case "Approved":
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد وافق المهمة بنجاح") : ("Vacancy has been Approved successfully");
                        break;
                    case "forwarded":
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("وقد وافق المهمة بنجاح") : ("Vacancy has been forwarded successfully");
                        break;

                }
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }        
    }

    /// <summary>
    /// Binds all Jobs onto datalist
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dlAllJobs_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            //ViewState["IsNewJob"] = "";
            string msg = string.Empty;

            switch (e.CommandName)
            {
                case "VIEW":
                    Response.Redirect("VacancyView.aspx?JobID=" + Server.UrlEncode(e.CommandArgument.ToString()));

                    break;
                case "ALTER":       //binds data onto controls for updations                   
                    btnSearch.Visible = txtSearch.Visible = false;
                    txtSearch.Text = "";
                    if (clsVacancy.IsVacancyScheduled(e.CommandArgument.ToInt32()) > 0)
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحرير الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Edit Vacancy as it has been scheduled for Interview");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                    }
                    else
                    {
                        Response.Redirect("VacancyAddEdit.aspx?JobID=" + Server.UrlEncode(e.CommandArgument.ToString()));
                    }
                    break;

                case "CLOSE":       //To close a job
                    if (objUserMaster.RoleID.ToInt32() <= 3)
                    {
                        if (clsVacancy.UpdateVacancyWhenClosed(e.CommandArgument.ToInt32()) > 0)
                        {
                            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تم إغلاقه العمل") : ("Vacancy has been Closed");
                            mcMessage.InformationalMessage(msg);
                            mpeMessage.Show();
                            ListJobs();
                        }
                    }
                    else
                    {
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إغلاق الوظيفة بسبب وصول المستخدمين غير المصرح به") : ("Cannot Close Vacancy because of unauthorized user access");
                        mcMessage.InformationalMessage(msg);
                        mpeMessage.Show();
                    }
                    break;
            }
        }
        catch { }
    }
    protected void dlAllJobs_ItemDataBound(object sender, DataListItemEventArgs e)
    {
       // lnkDeleteJob.OnClientClick = "return valDeleteJobDatalist('" + dlAllJobs.ClientID + "');";
        foreach (DataListItem item in dlAllJobs.Items)
        {
            ImageButton btnEdit = (ImageButton)item.FindControl("btnEdit");
            btnEdit.Enabled = Convert.ToBoolean(ViewState["IsUpdate"]);
        }
    }

    protected void lnkAddCriteria_Click(object sender, EventArgs e)
    {
        if (CriteriaReferenceControl != null && mdlPopUpReference != null)
        {
            CriteriaReferenceControl.BindDataList();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
        }
    }
    protected void lnkAddparticular_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Insert);
        controls_AdditionDeduction ucAdditionDeduction = (controls_AdditionDeduction)FormView1.FindControl("ucAdditionDeduction");

        AjaxControlToolkit.ModalPopupExtender mpeAdditionDeduction = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeAdditionDeduction");
        if (ucAdditionDeduction != null && mpeAdditionDeduction != null)
        {
            ucAdditionDeduction.BindDataList();
            mpeAdditionDeduction.Show();
            upnlAdditionDeduction.Update();
        }
    }

    protected void lnkDeleteJob_Click(object sender, EventArgs e)
    {
        try
        {
            int count1 = 0;
            string msg = string.Empty;
            if (dlAllJobs.Items.Count > 0)
            {
                for (int i = 0; i < dlAllJobs.Items.Count; i++)
                {
                    CheckBox chkSelect = (CheckBox)dlAllJobs.Items[i].FindControl("chkSelect");
                    if (chkSelect == null)
                        continue;
                    if (chkSelect.Checked)
                    {
                        if (clsVacancy.IsVacancyScheduled(dlAllJobs.DataKeys[dlAllJobs.Items[i].ItemIndex].ToInt32()) > 0)
                        {
                            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
                            mcMessage.InformationalMessage(msg);
                            mpeMessage.Show();
                        }
                        else if (clsVacancy.IsCandidateAssigned(dlAllJobs.DataKeys[dlAllJobs.Items[i].ItemIndex].ToInt32()) > 0)
                        {
                            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
                            mcMessage.InformationalMessage(msg);
                            mpeMessage.Show();
                        }
                        else
                        {
                            DeleteVacancy(dlAllJobs.DataKeys[dlAllJobs.Items[i].ItemIndex].ToInt32());
                        }
                    }
                }
            }
            ListJobs();
        }
        catch { }
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        divNoData.Visible = false;
        pgrJobs.CurrentPage = 0;
        BindDetails();
        this.RegisterAutoComplete();
    }

    #endregion

    #region Methods
    public void EnableMenus()
    {
        objUserMaster = new clsUserMaster();
        int RoleID = objUserMaster.GetRoleId();
        clsRoleSettings objRoleSettings = new clsRoleSettings();

        if (RoleID > 4)//RoleID based checking
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.JobOpening);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = lnkNewJob.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = lnkViewjob.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = lnkDeleteJob.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
                if (dtm.Rows[0]["IsView"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                    btnSearch.Enabled = true;
                else
                    btnSearch.Enabled = false;
                if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = true;
                else
                    lnkAddCriteria.Enabled = lnkAddparticular.Enabled = false;
            }
        }
        else
        {
            lnkNewJob.Enabled = lnkViewjob.Enabled = lnkDeleteJob.Enabled = true;
            btnSearch.Enabled = true;
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }
        if (ViewState["IsView"].ToBoolean() || ViewState["IsUpdate"].ToBoolean())
            btnSearch.Enabled = true;
        else
            btnSearch.Enabled = false;
        if (ViewState["IsCreate"].ToBoolean() || ViewState["IsUpdate"].ToBoolean())
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = true;
        else
            lnkAddCriteria.Enabled = lnkAddparticular.Enabled = false;


        if (lnkDeleteJob.Enabled)
            lnkDeleteJob.OnClientClick = "return valDeleteJobDatalist('" + dlAllJobs.ClientID + "');";
        else
            lnkDeleteJob.OnClientClick = "return false;";

        updviewAll.Update();

    }
    private void RegisterAutoComplete()
    {
        objUserMaster = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.JobPost, objUserMaster.GetCompanyId());
    }
    private void ListJobs()
    {
        int RecordCount = clsVacancy.GetTotalRecordCount(txtSearch.Text.Trim(), objUserMaster.GetCompanyId());
        if (RecordCount > 0)
        {
            //lnkDeleteJob.OnClientClick = "return true;";
            //lnkDeleteJob.Enabled = ViewState["IsDelete"].ToBoolean();

            if (ViewState["IsView"].ToBoolean())
            {
                BindDetails();
                pgrJobs.Visible = true;
                btnSearch.Enabled = true;
            }
            else
            {
                dlAllJobs.Visible = pgrJobs.Visible = false;
                btnSearch.Enabled = false;
                pgrJobs.Total = 0;

                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
                divNoData.Visible = true;
                divViewAll.Style["display"] = "block";
                lnkNewJob.Enabled = lnkViewjob.Enabled = lnkDeleteJob.Enabled = false;
            }
            updviewAll.Update();
        }
        else
        {
            if (!ViewState["IsView"].ToBoolean())
            {
                dlAllJobs.Visible = pgrJobs.Visible = false;
                btnSearch.Enabled = false;
                pgrJobs.Total = 0;

                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
                divNoData.Visible = true;
                divViewAll.Style["display"] = "block";
                lnkNewJob.Enabled = lnkViewjob.Enabled = lnkDeleteJob.Enabled = false;
            }
            else if (Request.QueryString["dispMsg"] == null && ViewState["IsCreate"].ToBoolean())
                Response.Redirect("VacancyAddEdit.aspx");
            else
            {
                dlAllJobs.Visible = pgrJobs.Visible = false;
                btnSearch.Enabled = false;
                pgrJobs.Total = 0;

                divNoData.InnerHtml = GetLocalResourceObject("NoJobsFound.Text").ToString();
                divNoData.Visible = true;
                divViewAll.Style["display"] = "block";
            }

        }
    }
    private void BindDetails()
    {
        objVacancy.PageIndex = pgrJobs.CurrentPage + 1;
        objVacancy.PageSize = pgrJobs.PageSize;
        objVacancy.SearchKey = txtSearch.Text;
        DataTable dt = clsVacancy.GetAllVacancies(objVacancy.PageSize, objVacancy.PageIndex, objVacancy.SearchKey, objUserMaster.GetCompanyId());

        btnSearch.Visible = txtSearch.Visible = true;
        if (dt.Rows.Count == 0)
        {
            dlAllJobs.Visible = pgrJobs.Visible = false;
            pgrJobs.Total = 0;
            divNoData.InnerHtml = GetLocalResourceObject("NoJobsFound.Text").ToString();
            divNoData.Visible = true;
        }
        else
        {
            dlAllJobs.Visible = pgrJobs.Visible = true;
            pgrJobs.Total = clsVacancy.GetTotalRecordCount(objVacancy.SearchKey, objUserMaster.GetCompanyId());
            divNoData.Visible = false;
            updviewAll.Update();
        }
        dlAllJobs.DataSource = dt;
        dlAllJobs.DataBind();
        for (int i = 0; i < dlAllJobs.Items.Count; i++)
        {
            LinkButton btnJob = (LinkButton)dlAllJobs.Items[i].FindControl("btnJob");
            ImageButton btnEdit = (ImageButton)dlAllJobs.Items[i].FindControl("btnEdit");

            ImageButton lnkClose = (ImageButton)dlAllJobs.Items[i].FindControl("lnkClose");
            Label lblStatus = (Label)dlAllJobs.Items[i].FindControl("lblStatus");
            HiddenField hfclosed = (HiddenField)dlAllJobs.Items[i].FindControl("hfclosed");
            HiddenField hfJobID = (HiddenField)dlAllJobs.Items[i].FindControl("hfJobID");

            //lnkDeleteJob.Enabled = true;//Convert.ToBoolean(ViewState["IsDelete"]);

            lblStatus.Text = dt.Rows[i]["Status"].ToString();
            if (dt.Rows[i]["JobStatusId"].ToInt32() == 4)
            {
                hfclosed.Value = clsUserMaster.GetCulture() == "ar-AE" ? ("مغلق") : ("Closed");
            }
            else if (dt.Rows[i]["JobStatusId"].ToInt32() == 5)
            {
                lblStatus.Text = clsUserMaster.GetCulture() == "ar-AE" ? ("الموافقة على") : ("Approved");
            }
            if (hfclosed.Value == "Closed" || hfclosed.Value == "مغلق")
                btnJob.ForeColor = System.Drawing.Color.FromName("#FF0000");

            if (hfclosed.Value == "Closed" || hfclosed.Value == "مغلق" || lblStatus.Text == "Rejected" || lblStatus.Text == "مرفوض")
            {
                btnEdit.Enabled = false;
                btnEdit.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحرير الوظيفة كما هو") : ("Cannot Edit Vacancy as it is ") + dt.Rows[i]["Status"].ToString();
                btnEdit.ImageUrl = "~/images/edit_disable.png";

                lnkClose.Enabled = false;
                lnkClose.ImageUrl = "~/images/closed_grey.png";
                lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("هو وظيفة") : ("Vacancy is ") + dt.Rows[i]["Status"].ToString();
            }
            else if (lblStatus.Text == "Approved" || lblStatus.Text == "الموافقة على")
            {
                btnEdit.Enabled = false;
                btnEdit.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحرير الوظيفة كما هو") : ("Cannot Edit Vacancy as it is ") + lblStatus.Text;
                btnEdit.ImageUrl = "~/images/edit_disable.png";
            }
            else
            {
                btnEdit.Enabled = true;//Convert.ToBoolean(ViewState["IsUpdate"]);
                if (btnEdit.Enabled)
                {
                    btnEdit.ImageUrl = "~/images/edit.png";
                    btnEdit.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("انقر ل تحرير العمل") : ("Click to Edit Vacancy");
                }
                else
                {
                    btnEdit.ImageUrl = "~/images/edit_disable.png";
                    btnEdit.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن تحرير الوظيفة لكونها مستخدم غير مصرح به.") : ("Cannot Edit Vacancy as for being an unauthorized user.");
                }
                if (hfclosed.Value == "Closed" || hfclosed.Value == "مغلق")
                {
                    lnkClose.Enabled = false;
                    lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("و أغلقت المهمة.") : ("Vacancy is Closed.");
                }
                //else
                //{
                //    int dtAuth = objJob.GetAuthority(hfJobID.Value.ToInt32());

                //    if (dtAuth > 0)
                //    {
                //        if (objUserMaster.GetEmployeeId().ToInt32() == dtAuth)
                //        {
                //            lnkClose.Enabled = true;
                //            lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("انقر لإغلاق العمل.") : ("Click to Close Job.");
                //        }
                //        else
                //        {
                //            lnkClose.Enabled = false;
                //            lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إغلاق كما لكونها مستخدم غير مصرح به.") : ("Cannot Close as for being an unauthorized user.");
                //        }
                //    }
                //    else
                //    {
                //        if (objUserMaster.GetRoleId().ToInt32() <= 3)
                //        {
                //            lnkClose.Enabled = true;
                //            lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("انقر لإغلاق العمل.") : ("Click to Close Job.");
                //        }
                //        else
                //        {
                //            lnkClose.Enabled = false;
                //            lnkClose.ToolTip = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن إغلاق كما لكونها مستخدم غير مصرح به.") : ("Cannot Close as for being an unauthorized user.");
                //        }
                //    }
                //}
            }
        }
        updviewAll.Update();
    }
    private void DeleteVacancy(int JobID)
    {
        string msg = string.Empty;
        ViewState["JobID"] = JobID;

        //if (clsVacancy.IsVacancyScheduled(JobID) > 0)
        //{
        //    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
        //    mcMessage.InformationalMessage(msg);
        //    mpeMessage.Show();
        //    return;
        //}
        //else if (clsVacancy.IsCandidateAssigned(JobID) > 0)
        //{
        //    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة وقد تقرر ذلك ل مقابلة") : ("Cannot Delete Vacancy as it has been scheduled for Interview");
        //    mcMessage.InformationalMessage(msg);
        //    mpeMessage.Show();
        //    return;
        //}
        //else 
        if (clsVacancy.IsUsedInGE(JobID) > 0)
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الوظيفة موجود المرجعي") : ("Cannot Delete Vacancy as Reference Exists");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            return;
        }
        else
        {
            if (objVacancy.DeleteVacancy(JobID))
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("حذف المهمة بنجاح") : ("Vacancy Deleted successfully");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
                clsCommonMessage.DeleteMessages(ViewState["JobID"].ToInt32(), eReferenceTypes.JobApproval);
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("فشل الحذف") : ("Deletion Failed");
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }
    }
    #endregion
}
