﻿<%@ Page Language="C#" MasterPageFile="~/Master/DocumentMasterPage.master" AutoEventWireup="true"
    CodeFile="Candidate.aspx.cs" Inherits="Public_Candidate" Title="Candidate" %>

<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="candidatetoplinks">
                <div style="width: 45%; height: 50px; margin-top: 10px; margin-bottom: 10px; padding-left: 1%;
                    float: left;">
                    <ul class="dark_menu">
                        <li><a href="">Menu &nbsp; &#x25Bc</a>
                            <ul>
                                <li><a href='Vacancy.aspx'><span>Vacancy</span></a></li>
                                <li class='selected'><a href='Candidates.aspx'><span>Candidates</span></a></li>
                                <li><a href="InterviewSchedule.aspx">Interview Schedule</a></li>
                                <li><a href="InterviewProcess.aspx">Interview Process</a></li>
                                <li><a href="Offerletter.aspx">Offer Letter</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <%-- <div class="linkimage">
                    <asp:ImageButton ID="imgAddCandidate" ImageUrl="~/images/Add user.png" CausesValidation="false"
                        runat="server" /></div>
                <div class="linkname">
                    <asp:LinkButton ID="lnkAddCandidate" runat="server" OnClick="lnkAddCandidate_Click"
                        CausesValidation="false"><h5>
                        Add Candidate
                    </h5></asp:LinkButton></div>--%>
                <div class="linkimage">
                    <asp:ImageButton ID="imgListCandidate" ImageUrl="~/images/list_candidate.png" CausesValidation="false"
                        runat="server" /></div>
                <div class="linkname">
                    <asp:LinkButton ID="lnkListCandidate" runat="server" OnClick="lnkListCandidate_Click"
                        CausesValidation="false"><h5>
                        List Candidate
                    </h5></asp:LinkButton></div>
                <div class="linkimage">
                    <asp:ImageButton ID="imgFeverOption" ImageUrl="../images/less.png" CausesValidation="false"
                        runat="server" /></div>
                <div class="linkname">
                    <asp:LinkButton ID="lnkFeverOption" runat="server" OnClick="lnkFeverOption_Click"
                        CausesValidation="false"><h5>
                        English Option
                    </h5></asp:LinkButton></div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="public_content" runat="Server">
    <style type="text/css">
        .background
        {
            background-color: Transparent;
            filter: alpha(opacity=10);
            opacity: 0.1;
        }
        .updateprogress
        {
            background-position: center center;
            position: absolute;
            width: 31px;
            height: 31px;
            background-image: url(   '../images/loading.gif' );
            background-repeat: no-repeat;
            display: none;
        }
    </style>

    <script src="../js/yasArabic.js" type="text/javascript"></script>

    <script type="text/javascript">
        function WidthPreview(source, args, width)  
        {
            var imgPreview = document.getElementById('ctl00_public_content_imgPreview');
            
            imgPreview.style.display = "block";
            imgPreview.src = "../thumbnail.aspx?FromDB=false&isCandidate=true&folder=Temp&file=" + '<%= Session.SessionID %>' + ".jpg&width=" + width + "&t=" + new Date();
        }
        
         var _updateprogress, _background;        
        function pageLoad(sender, args)
        {
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequest);

            if(args._isPartialLoad == false)
            {
                _updateprogress = $get('updateprogress');
                
                _background = document.createElement('div');
                _background.style.display = 'none';
                _background.style.zIndex = 10000;
                _background.className = 'background';
                document.getElementById('ctl00_public_content_container').appendChild(_background);
            }
        }

        function beginRequest(sender, args)
        {
            _updateprogress.style.display = 'block';
            _background.style.display = 'block';

            var bounds = Sys.UI.DomElement.getBounds(document.getElementById('ctl00_public_content_container'));
            var progressbounds = Sys.UI.DomElement.getBounds(_updateprogress);

            var x = bounds.x + Math.round(bounds.width / 2) - Math.round(progressbounds.width / 2);
            var y = bounds.y + Math.round(bounds.height / 2) - Math.round(progressbounds.height / 2);

            _background.style.width = bounds.width + "px";
            _background.style.height = bounds.height + "px";

            Sys.UI.DomElement.setLocation(_updateprogress, x, y);            
            Sys.UI.DomElement.setLocation(_background, bounds.x, bounds.y);
        }

        function endRequest(sender, args)
        {
            _updateprogress.style.display = 'none';
            _background.style.display = 'none';
        }
    </script>

    <div id="updateprogress" class="updateprogress">
        &nbsp;
    </div>
    <div id="divInsideFirm" runat="server">
        <div style="display: none">
            <asp:Button ID="btnSubmitMessage" runat="server" Text="submit" />
        </div>
        <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
            BorderStyle="None">
            <asp:UpdatePanel ID="upnlMessage" runat="server">
                <ContentTemplate>
                    <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
        <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnSubmitMessage"
            PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
            Drag="true">
        </AjaxControlToolkit:ModalPopupExtender>
    </div>
    <asp:HiddenField ID="hfCandidateID" runat="server" />
    <asp:UpdatePanel ID="upfvCandidate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="container" runat="server">
                <div id="divCandidate" class="labeltext" runat="server">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="100%">
                                <%--<div style="width: 100%; height: 20px; margin-top: 20px; text-align: center; text-transform: capitalize;
                                font-weight: bold; text-align: center; font-size: 18px;">
                                Candidate Registration</div>--%>
                                <div id="div1" class="TabbedPanels" style="width: 100%;">
                                    <div style="height: 60px; margin-top: 25px; float: right; width: 46%;">
                                        <ul class="TabbedPanelsTabGroup">
                                            <li id="pnlTab4" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">Other Info</li>
                                            <li id="pnlTab3" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">Document
                                                Info</li>
                                            <li id="pnlTab2" class="TabbedPanelsTab" onclick="SetCandidateTab(this.id)">Professional
                                                Info</li>
                                            <li id="pnlTab1" class="TabbedPanelsTabSelected" onclick="SetCandidateTab(this.id)">
                                                Basic Info</li>
                                        </ul>
                                    </div>
                                    <div class="TabbedPanelsContentGroup">
                                        <div id="divTab1" class="TabbedPanelsContent" style="display: block; height: 500px;">
                                            <div style="width: 100%">
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                    </div>
                                                    <div class="B2">
                                                    </div>
                                                    <div class="B3">
                                                        Photo
                                                    </div>
                                                    <div class="B4" style="width: 27%;">
                                                        <AjaxControlToolkit:AsyncFileUpload ID="fuRecentPhoto" runat="server" CompleteBackColor="White"
                                                            OnClientUploadComplete="ShowCandidatePreview" OnUploadedComplete="fuRecentPhoto_UploadedComplete"
                                                            PersistFile="true" />
                                                        <asp:LinkButton ID="btnClearRecentPhoto" runat="server" CausesValidation="False"
                                                            CssClass="linkbutton" OnClick="btnClearRecentPhoto_Click" Style="display: none;">Clear</asp:LinkButton>
                                                        <div id="divImage" runat="server" style="display: block;">
                                                            <img id="imgPreview" runat="server" alt="" src="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div style="width: 10%; margin-left: 73px; margin-top: 170px;">
                                                    <asp:LinkButton ID="btnRemoveRecentPhoto" runat="server" CausesValidation="False"
                                                        ToolTip="Click here ro remove" OnClick="btnRemoveRecentPhoto_Click" Style="display: none;">Remove</asp:LinkButton>
                                                </div>
                                                <div class="MainDiv" style="margin-top: -32px;">
                                                    <div class="B1" style="width: 15%;">
                                                        اسم الوظيفة[JobName]
                                                        <%--   <asp:RequiredFieldValidator ID="rfvJob" runat="server" ControlToValidate="ddlJob"
                                                        Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"
                                                        InitialValue="-1"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updJob" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlJob" runat="server">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4" style="width: 27%;">
                                                    </div>
                                                </div>
                                                <div style="clear: both">
                                                </div>
                                                <%-- <div class="MainDiv">
                                                <div class="B1">
                                                    <asp:CheckBox ID="chkThroughAgency" runat="server" AutoPostBack="True" OnCheckedChanged="chkThroughAgency_CheckedChanged" />
                                                    وكالة [Agency]
                                                </div>
                                                <div class="B2">
                                                    <asp:UpdatePanel ID="updAgency" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div class="B4" style="width: 100%;">
                                                                <div class="B4" style="width: 75%;">
                                                                    <asp:DropDownList ID="ddlAgency"  Width="98%"
                                                                        runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                    <asp:Button ID="btnAgency" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                        Text="..." OnClick="btnAgency_Click" />
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                                <div class="B3">
                                                </div>
                                                <div class="B4">
                                                </div>
                                            </div>--%>
                                                <div class="MainDiv" style="display: none;">
                                                    <div class="B1">
                                                        <div style="width: 100%; height: 50%;">
                                                            [Candidate Code]</div>
                                                        <div style="width: 100%;">
                                                            رمز مرشح <font color="Red">*</font><asp:RequiredFieldValidator ID="rfvtxtCandidateCodepnlTab1"
                                                                runat="server" ControlToValidate="txtCandidateCode" Display="Dynamic" ErrorMessage="*"
                                                                SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator></div>
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCandidateCode" runat="server" Style="background-color: InfoBackground;"
                                                            Enabled="false" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="margin-top: 10px;">
                                                    <div class="B1">
                                                        تحية
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updSalutationArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcSalutationArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnSalutationArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnSalutationArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Salutation<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvrcSalutation" runat="server" ControlToValidate="rcSalutation"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updSalutation" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcSalutation" Style="background-color: InfoBackground;" Width="98%"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnSalutation" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnSalutation_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 60px;">
                                                    <div class="B1">
                                                        Name<font color="Red">*</font>
                                                    </div>
                                                    <div class="B2" style="direction: ltr;">
                                                        First Name<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvEnglishFisrtName" runat="server" ControlToValidate="txtEnglishFirstName"
                                                            Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtEnglishFirstName" runat="server" Style="background-color: InfoBackground;"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                    <div class="B2" style="width: 50.5%;">
                                                        <div class="B2" style="width: 50%; direction: ltr;">
                                                            Second Name<font color="Red">*</font>
                                                            <asp:RequiredFieldValidator ID="rfvEnglishSecondName" runat="server" ControlToValidate="txtEnglishSecondName"
                                                                Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtEnglishSecondName" Style="background-color: InfoBackground;"
                                                                runat="server" CssClass="textbox" MaxLength="25">
                                                            </asp:TextBox>
                                                        </div>
                                                        <div class="B2" style="width: 50%; direction: ltr;">
                                                            Third Name<font color="Red">*</font>
                                                            <asp:RequiredFieldValidator ID="rfvEnglishThirdName" runat="server" ControlToValidate="txtEnglishThirdName"
                                                                Display="Dynamic" ErrorMessage="*" SetFocusOnError="False" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtEnglishThirdName" runat="server" Style="background-color: InfoBackground;"
                                                                CssClass="textbox" MaxLength="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 60px;">
                                                    <div class="B1">
                                                        اسم
                                                    </div>
                                                    <div class="B2">
                                                        <div style="width: 100%;">
                                                            الاسم الاول
                                                        </div>
                                                        <div style="width: 100%;">
                                                            <asp:TextBox ID="txtArabicFirstName" onkeyup="arabicValue(this);" runat="server"
                                                                MaxLength="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="B2" style="width: 50.5%;">
                                                        <div class="B2" style="width: 50%;">
                                                            الاسم الثانى
                                                            <asp:TextBox ID="txtArabicSecondName" onkeyup="arabicValue(this);" runat="server"
                                                                CssClass="textbox" MaxLength="25"></asp:TextBox>
                                                        </div>
                                                        <div class="B2" style="width: 50%;">
                                                            الاسم الثالث
                                                            <asp:TextBox ID="txtArabicThirdName" onkeyup="arabicValue(this);" runat="server"
                                                                CssClass="textbox" MaxLength="25"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        مواطنية
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCitizenshipArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Citizenship
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCitizenship" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        مكان الميلاد
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPlaceOfBirthArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Place of Birth<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvtxtPlaceOfBirth" runat="server" ControlToValidate="txtPlaceOfBirth"
                                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPlaceOfBirth" Style="background-color: InfoBackground;" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                    جنس [Gender]
                                                    </div>
                                                    <div style="height: 35px; margin-top: 5px;" align="right">
                                                        <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Selected="True" Value="0">ذكر [Male]</asp:ListItem>
                                                            <asp:ListItem Value="1">أنثى [Female]</asp:ListItem> 
                                                        </asp:RadioButtonList>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الميلاد
                                                    </div>
                                                    <div class="B2" style="height: 35px; margin-top: 5px;">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B2" style="width: 75%;">
                                                                <asp:TextBox ID="txtDateofBirthArb" runat="server" MaxLength="10" Width="95%"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnDateofBirthArb" runat="server" CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceDateofBirthArb" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnDateofBirthArb" TargetControlID="txtDateofBirthArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        <div>
                                                            Date of Birth<font color="Red">*</font>
                                                            <asp:RequiredFieldValidator ID="RfvDOB" runat="server" ControlToValidate="txtDateofBirth"
                                                                Display="Dynamic" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator></div>
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtDateofBirth" runat="server" Style="background-color: InfoBackground;"
                                                                    MaxLength="10" Width="95%"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnDateofBirth" Style="background-color: InfoBackground;" runat="server"
                                                                    CausesValidation="False" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="ceDateofBirth" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnDateofBirth" TargetControlID="txtDateofBirth" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--************************Style for Validation***********************--%>
                                                <div style="width: 100%; height: auto;">
                                                    <div style="width: 37%; height: auto; float: left; padding-left: 107px;">
                                                        <font color="Red">
                                                            <asp:CustomValidator ID="CustomValidator4" runat="server" ClientValidationFunction="validateDateofBirth"
                                                                ControlToValidate="txtDateofBirth" Display="Dynamic" ErrorMessage="Invalid Age"
                                                                ForeColor="" ValidateEmptyText="True" ValidationGroup="submit"></asp:CustomValidator>
                                                        </font>
                                                    </div>
                                                    <div style="width: 37%; height: auto; float: right; padding-right: 176px; text-align: right;">
                                                        <font color="Red">
                                                            <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="validateDateofBirth"
                                                                ControlToValidate="txtDateofBirthArb" Display="Dynamic" ErrorMessage="Invalid Age"
                                                                ForeColor="" ValidateEmptyText="True" ValidationGroup="submit"></asp:CustomValidator>
                                                        </font>
                                                    </div>
                                                </div>
                                                <%--***************Style for Validation**********--%>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الحالة الاجتماعية
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updMaritalStatusArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlMaritalStatusArb" runat="server">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Marital Status<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvddlMaritalStatus" runat="server" ControlToValidate="ddlMaritalStatus"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updMaritalStatus" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlMaritalStatus" Style="background-color: InfoBackground;"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        عدد الابناء
                                                    </div>
                                                    <div class="B2">
                                                        <AjaxControlToolkit:FilteredTextBoxExtender runat="server" ID="txtSonFiltered" FilterType="Numbers"
                                                            TargetControlID="txtNoOfSonArb" FilterMode="ValidChars">
                                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                                        <asp:TextBox ID="txtNoOfSonArb" runat="server" CssClass="textbox" MaxLength="2"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Number of Son
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtNoOfSon" runat="server" CssClass="textbox" MaxLength="2"></asp:TextBox>
                                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="fteSon" runat="server" TargetControlID="txtNoOfSon"
                                                            FilterType="Numbers" FilterMode="ValidChars">
                                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الديانة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updReligionArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcReligionArb" runat="server" Width="98%">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnReligionArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnReligionArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Religion<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvrcReligion" runat="server" ControlToValidate="rcReligion"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updReligion" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcReligion" Style="background-color: InfoBackground;" Width="98%"
                                                                            runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnReligion" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnReligion_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        إسم الام
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtMotherNameArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="25"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Mother Name
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtMotherName" runat="server" CssClass="textbox" MaxLength="25"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الجنسية الحالية
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updCurrentNationalityArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlCurrentNationalityArb" Style="width: 98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCurrentNationalityArb" runat="server" CausesValidation="False"
                                                                            CssClass="referencebutton" Text="..." OnClick="btnCurrentNationalityArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Current Nationality<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlCurrentNationality"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updCurrentNationality" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlCurrentNationality" Style="background-color: InfoBackground;"
                                                                            Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCurrentNationality" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnCurrentNationality_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الجنسية السابقة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updPreviousNationalityArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlPreviousNationalityArb" Style="width: 98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnPreviousNationalityArb" runat="server" CausesValidation="False"
                                                                            CssClass="referencebutton" Text="..." OnClick="btnPreviousNationalityArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Previous Nationality
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updPreviousNationality" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlPreviousNationality" Style="background-color: InfoBackground;"
                                                                            Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnPreviousNationality" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnPreviousNationality_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        المذهب
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtDoctrineArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Doctrine
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtDoctrine" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم الهاتف خارج الدولة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCurrentTelephoneNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Telephone Number<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvCurrentTelephoneNumber" runat="server" ControlToValidate="txtCurrentTelephoneNumber"
                                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCurrentTelephoneNumber" Style="background-color: InfoBackground;"
                                                            runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 90px;">
                                                    <div class="B1">
                                                        العنوان
                                                    </div>
                                                    <div class="B2" style="height: 85px;">
                                                        <asp:TextBox ID="txtCurrentAddressArb" runat="server" MaxLength="250" Height="90%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidateArabic(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Address<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvtxtCurrentAddress" runat="server" ControlToValidate="txtCurrentAddress"
                                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="submit"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4" style="height: 85px;">
                                                        <asp:TextBox ID="txtCurrentAddress" Style="background-color: InfoBackground;" runat="server"
                                                            MaxLength="250" Height="90%" onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidate(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        صندوق البريد
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCurrentPOBoxArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        PO Box
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCurrentPOBox" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        صندوق البريد خارج الدولة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updCountryArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcCountryArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCountryArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnCountryArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Country<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvrcCountry" runat="server" ControlToValidate="rcCountry"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updCountry" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcCountry" Width="98%" runat="server" Style="background-color: InfoBackground;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnCountry_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        اسم مكان عمل الزوج
                                                    </div>
                                                    <div class="B2" style="height: 85px;">
                                                        <asp:TextBox ID="txtPlaceofBusinessPairArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3" style="height: 85px;">
                                                        Place of Business Pair
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPlaceofBusinessPair" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        نوع المؤسسة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updCompanyTypeArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlCompanyTypeArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCompanyTypeArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnCompanyTypeArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Company Type
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updCompanyType" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlCompanyType" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnCompanyType" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnCompanyType_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 90px;">
                                                    <div class="B1">
                                                        العنوان
                                                    </div>
                                                    <div class="B2" style="height: 85px;">
                                                        <asp:TextBox ID="txtBasicBusinessPairAddressArb" runat="server" MaxLength="500" Height="90%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidateArabic(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Address
                                                    </div>
                                                    <div class="B4" style="height: 85px;">
                                                        <asp:TextBox ID="txtBasicBusinessPairAddress" runat="server" MaxLength="500" Height="90%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidate(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                    </div>
                                                    <div class="B2">
                                                    </div>
                                                    <div class="B3">
                                                        البريد الإلكتروني [E-mail]<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtEmail"
                                                            ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="revEmailpnlTab1" runat="server" ControlToValidate="txtEmail"
                                                            CssClass="error" Display="Dynamic" ErrorMessage="Invalid Email" SetFocusOnError="False"
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="submit"></asp:RegularExpressionValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtEmail" Style="background-color: InfoBackground;" runat="server"
                                                            CssClass="textbox" MaxLength="30" CausesValidation="True"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        نوع الإحالة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updReferralTypeArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlReferralTypeArb" Width="98%" runat="server" OnSelectedIndexChanged="ddlReferralTypeArb_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <%--<asp:Button ID="btnReferralTypeArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                        Text="..." OnClick="btnReferralTypeArb_Click" />--%>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Referral Type
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updReferralType" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlReferralType" Width="98%" runat="server" OnSelectedIndexChanged="ddlReferralType_SelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <%--<asp:Button ID="btnReferralType" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                        Text="..." OnClick="btnAgency_Click" /> --%>
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        يحيلها 
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updReferredByArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlReferredByArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRefferedByArb" runat="server" class="B2" style="width: 15%; height: 35px;
                                                                        margin-top: 2px;">
                                                                        <asp:Button ID="btnReferredByArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnReferredByArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Referred By
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updReferredBy" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlReferredBy" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div id="divRefferedBy" runat="server" class="B2" style="width: 15%; height: 35px;
                                                                        margin-top: 2px;">
                                                                        <asp:Button ID="btnReferredBy" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnReferredBy_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab2" class="TabbedPanelsContent" style="display: none; height: 500px;">
                                            <div style="width: 100%">
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        المؤهل الدراسي
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updDegreeArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcDegreeArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnDegreeArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnDegreeArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Qualification<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvQualificationpnlTab2" runat="server" ControlToValidate="rcDegree"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="uprcDegree" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="rcDegree" Width="98%" runat="server" Style="background-color: InfoBackground;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnDegree" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnDegree_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        فئة المؤهل/ التخصص
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updQualificationCategoryArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlQualificationCategoryArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnQualificationCategoryArb" runat="server" CausesValidation="False"
                                                                            CssClass="referencebutton" Text="..." OnClick="btnQualificationCategoryArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Category<font color="Red">*</font>
                                                        <asp:RequiredFieldValidator ID="rfvQualificationCategorypnlTab2" runat="server" ControlToValidate="ddlQualificationCategory"
                                                            ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updQualificationCategory" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlQualificationCategory" Width="98%" runat="server" Style="background-color: InfoBackground;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnQualificationCategory" runat="server" CausesValidation="False"
                                                                            CssClass="referencebutton" Text="..." OnClick="btnQualificationCategory_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <%--  <div class="MainDiv" style ="display:none;">
                                            <div class="B1">
                                                سنة التخرج
                                            </div>
                                            <div class="B2">
                                                <asp:TextBox ID="txtGraduationArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox" MaxLength="30"></asp:TextBox>
                                            </div>
                                            <div class="B3">
                                                Graduation
                                            </div>
                                            <div class="B4">
                                                <asp:TextBox ID="txtGraduation" runat="server" CssClass="textbox" MaxLength="30"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        سنة التخرج
                                                    </div>
                                                    <div class="B2">
                                                        <asp:DropDownList ID="ddlDegreeCompletionYearArb" runat="server" CssClass="dropdownlist">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="B3">
                                                        Graduation Year
                                                    </div>
                                                    <div class="B4">
                                                        <asp:DropDownList ID="ddlDegreeCompletionYear" runat="server" CssClass="dropdownlist">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        مكان التخرج
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPlaceofGraduationArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Place of Graduation
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPlaceofGraduation" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الوظيفة السابقة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPreviousJobArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Previous Job
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPreviousJob" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الكلية / المدرسة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCollegeSchoolArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        College/School
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCollegeSchool" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الجهة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtSectorArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Sector
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtSector" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الوظيفة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPositionArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Position
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPosition" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        نوع الخبرة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtTypeofExperienceArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Type of Experience
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtTypeofExperience" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        سنوات الخبرة
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 92.5%">
                                                            <div class="B4" style="width: 34%;">
                                                                <asp:UpdatePanel ID="updExpYearArb" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddlYearArb" runat="server" CssClass="dropdownlist">
                                                                            <asp:ListItem Value="0"></asp:ListItem>
                                                                            <asp:ListItem Value="1"></asp:ListItem>
                                                                            <asp:ListItem Value="2"></asp:ListItem>
                                                                            <asp:ListItem Value="3"></asp:ListItem>
                                                                            <asp:ListItem Value="4"></asp:ListItem>
                                                                            <asp:ListItem Value="5"></asp:ListItem>
                                                                            <asp:ListItem Value="6"></asp:ListItem>
                                                                            <asp:ListItem Value="7"></asp:ListItem>
                                                                            <asp:ListItem Value="8"></asp:ListItem>
                                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                                            <asp:ListItem Value="12"></asp:ListItem>
                                                                            <asp:ListItem Value="13"></asp:ListItem>
                                                                            <asp:ListItem Value="14"></asp:ListItem>
                                                                            <asp:ListItem Value="15"></asp:ListItem>
                                                                            <asp:ListItem Value="16"></asp:ListItem>
                                                                            <asp:ListItem Value="17"></asp:ListItem>
                                                                            <asp:ListItem Value="18"></asp:ListItem>
                                                                            <asp:ListItem Value="19"></asp:ListItem>
                                                                            <asp:ListItem Value="20"></asp:ListItem>
                                                                            <asp:ListItem Value="21"></asp:ListItem>
                                                                            <asp:ListItem Value="22"></asp:ListItem>
                                                                            <asp:ListItem Value="23"></asp:ListItem>
                                                                            <asp:ListItem Value="24"></asp:ListItem>
                                                                            <asp:ListItem Value="25"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div class="B4" style="width: 12%; margin-left: 1%; height: 30px;">
                                                                سنوات
                                                            </div>
                                                            <div class="B4" style="width: 34%; margin-left: 2%;">
                                                                <asp:UpdatePanel ID="updExpMonthArb" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddlMonthArb" runat="server" CssClass="dropdownlist">
                                                                            <asp:ListItem Value="00"></asp:ListItem>
                                                                            <asp:ListItem Value="01"></asp:ListItem>
                                                                            <asp:ListItem Value="02"></asp:ListItem>
                                                                            <asp:ListItem Value="03"></asp:ListItem>
                                                                            <asp:ListItem Value="04"></asp:ListItem>
                                                                            <asp:ListItem Value="05"></asp:ListItem>
                                                                            <asp:ListItem Value="06"></asp:ListItem>
                                                                            <asp:ListItem Value="07"></asp:ListItem>
                                                                            <asp:ListItem Value="08"></asp:ListItem>
                                                                            <asp:ListItem Value="09"></asp:ListItem>
                                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div class="B4" style="width: 12%; margin-left: 1%; height: 30px;">
                                                                شهر
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Year(s) of Experience
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 92.5%">
                                                            <div class="B4" style="width: 34%;">
                                                                <asp:UpdatePanel ID="updExpYear" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddlYear" runat="server" CssClass="dropdownlist">
                                                                            <asp:ListItem Value="0"></asp:ListItem>
                                                                            <asp:ListItem Value="1"></asp:ListItem>
                                                                            <asp:ListItem Value="2"></asp:ListItem>
                                                                            <asp:ListItem Value="3"></asp:ListItem>
                                                                            <asp:ListItem Value="4"></asp:ListItem>
                                                                            <asp:ListItem Value="5"></asp:ListItem>
                                                                            <asp:ListItem Value="6"></asp:ListItem>
                                                                            <asp:ListItem Value="7"></asp:ListItem>
                                                                            <asp:ListItem Value="8"></asp:ListItem>
                                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                                            <asp:ListItem Value="12"></asp:ListItem>
                                                                            <asp:ListItem Value="13"></asp:ListItem>
                                                                            <asp:ListItem Value="14"></asp:ListItem>
                                                                            <asp:ListItem Value="15"></asp:ListItem>
                                                                            <asp:ListItem Value="16"></asp:ListItem>
                                                                            <asp:ListItem Value="17"></asp:ListItem>
                                                                            <asp:ListItem Value="18"></asp:ListItem>
                                                                            <asp:ListItem Value="19"></asp:ListItem>
                                                                            <asp:ListItem Value="20"></asp:ListItem>
                                                                            <asp:ListItem Value="21"></asp:ListItem>
                                                                            <asp:ListItem Value="22"></asp:ListItem>
                                                                            <asp:ListItem Value="23"></asp:ListItem>
                                                                            <asp:ListItem Value="24"></asp:ListItem>
                                                                            <asp:ListItem Value="25"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div class="B4" style="width: 12%; margin-left: 1%; height: 30px;">
                                                                Years</div>
                                                            <div class="B4" style="width: 34%; margin-left: 2%;">
                                                                <asp:UpdatePanel ID="updExpMonth" runat="server" UpdateMode="Always">
                                                                    <ContentTemplate>
                                                                        <asp:DropDownList ID="ddlMonth" runat="server" CssClass="dropdownlist">
                                                                            <asp:ListItem Value="00"></asp:ListItem>
                                                                            <asp:ListItem Value="01"></asp:ListItem>
                                                                            <asp:ListItem Value="02"></asp:ListItem>
                                                                            <asp:ListItem Value="03"></asp:ListItem>
                                                                            <asp:ListItem Value="04"></asp:ListItem>
                                                                            <asp:ListItem Value="05"></asp:ListItem>
                                                                            <asp:ListItem Value="06"></asp:ListItem>
                                                                            <asp:ListItem Value="07"></asp:ListItem>
                                                                            <asp:ListItem Value="08"></asp:ListItem>
                                                                            <asp:ListItem Value="09"></asp:ListItem>
                                                                            <asp:ListItem Value="10"></asp:ListItem>
                                                                            <asp:ListItem Value="11"></asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                            <div class="B4" style="width: 12%; margin-left: 1%; height: 30px;">
                                                                Months</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الراتب المتوقع
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCompensationArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Compensation
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCompensation" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        التخصص
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtSpecializationArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Specialization
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtSpecialization" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ إدخال المعاملة
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B2" style="width: 75%;">
                                                                <asp:TextBox ID="txtDateofTransactionEntryArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnDateofTransactionEntryArb" runat="server" CausesValidation="False"
                                                                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender10" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnDateofTransactionEntryArb" TargetControlID="txtDateofTransactionEntryArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Date of Transaction
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtDateofTransactionEntry" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnDateofTransactionEntry" runat="server" CausesValidation="False"
                                                                    ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnDateofTransactionEntry" TargetControlID="txtDateofTransactionEntry" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ التعيين المتوقع
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B2" style="width: 75%;">
                                                            <asp:TextBox ID="txtExpectedJoinDateArb" runat="server" Width="95%" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                        <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                            <asp:ImageButton ID="btnExpectedJoinDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender11" runat="server" Format="dd/MM/yyyy"
                                                                PopupButtonID="btnExpectedJoinDateArb" TargetControlID="txtExpectedJoinDateArb" />
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Expected Join Date
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 75%;">
                                                            <asp:TextBox ID="txtExpectedJoinDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                        </div>
                                                        <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                            <asp:ImageButton ID="btnExpectedJoinDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy"
                                                                PopupButtonID="btnExpectedJoinDate" TargetControlID="txtExpectedJoinDate" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 100px; margin-top: 20px;">
                                                    <div class="B1">
                                                        المهارات [Skills]
                                                    </div>
                                                    <div class="B2" style="width: 85%;">
                                                        <asp:UpdatePanel ID="updSkills" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtSkills" Width="95%" runat="server" MaxLength="1000" TextMode="MultiLine"></asp:TextBox>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 60px; margin-top: 20px;">
                                                    <div class="B1" style="color: #33CCFF; font-weight: bold;">
                                                        اللغات [Languages]
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B2" style="width: 50%;">
                                                            1) العربية [Arabic]
                                                        </div>
                                                        <div class="B2" style="width: 50%;">
                                                            <asp:CheckBoxList ID="chlArabic" runat="server" RepeatColumns="2" DataTextField="Skills"
                                                                DataValueField="SkillID" Width="100%">
                                                                <asp:ListItem Value=" [Reading]"></asp:ListItem>
                                                                <asp:ListItem Value="Writing"></asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 60px; margin-top: 20px;">
                                                    <div class="B1" style="color: #33CCFF;">
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B2" style="width: 50%;">
                                                            2) الإنجليزية [English]
                                                        </div>
                                                        <div class="B2" style="width: 50%;">
                                                            <asp:CheckBoxList ID="chlEnglish" runat="server" RepeatColumns="2" DataTextField="Skills"
                                                                DataValueField="SkillID" Width="100%">
                                                                <asp:ListItem Value="Reading"></asp:ListItem>
                                                                <asp:ListItem Value="Writing"></asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 60px; margin-top: 20px;">
                                                    <div class="B1" style="color: #33CCFF;">
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B2" style="width: 50%;">
                                                            <div class="B2" style="width: 10%;">
                                                                3)&nbsp
                                                            </div>
                                                            <div class="B2" style="width: 90%;">
                                                                <asp:TextBox ID="txtOtherLanguages" runat="server" CssClass="textbox" MaxLength="30"
                                                                    Width="80%"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="B2" style="width: 50%;">
                                                            <asp:CheckBoxList ID="chlOtherLanguages" runat="server" RepeatColumns="2" DataTextField="Skills"
                                                                DataValueField="SkillID" Width="100%">
                                                                <asp:ListItem Value="Reading"></asp:ListItem>
                                                                <asp:ListItem Value="Writing"></asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab3" class="TabbedPanelsContent" style="display: none; height: 1000px;">
                                            <div style="width: 100%">
                                                <div class="MainDiv">
                                                    <div class="B1" style="color: #33CCFF; font-weight: bold;">
                                                        جواز سفر [Passport]
                                                    </div>
                                                    <div class="B2">
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم جواز السفر
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPassportNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Number<%--<font color="Red">*</font>--%>
                                                        <%--  <asp:RequiredFieldValidator ID="rfvPassportNumber" runat="server" ControlToValidate="txtPassportNumber"
                                                    ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPassportNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        جهة الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updPassportCountryArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlPassportCountryArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnPassportCountryArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnPassportCountryArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Country<%--<font color="Red">*</font>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="rfdPassportCountry" runat="server" ControlToValidate="ddlPassportCountry"
                                                    ValidationGroup="submit" InitialValue="-1" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updPassportCountry" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlPassportCountry" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnPassportCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnPassportCountry_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        مكان الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPlaceofPassportIssueArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Place of Issue<%--<font color="Red">*</font>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="rfvPlaceofPassportIssue" runat="server" ControlToValidate="txtPlaceofPassportIssue"
                                                    ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPlaceofPassportIssue" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtPassportReleaseDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnPassportReleaseDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender13" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnPassportReleaseDateArb" TargetControlID="txtPassportReleaseDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Issue Date<%--<font color="Red">*</font>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="rfvPassportReleaseDate" runat="server" ControlToValidate="txtPassportReleaseDate"
                                                    ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtPassportReleaseDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnPassportReleaseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender8" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnPassportReleaseDate" TargetControlID="txtPassportReleaseDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الانتهاء
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtPassportExpiryDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnPassportExpiryDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender12" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnPassportExpiryDateArb" TargetControlID="txtPassportExpiryDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Expiry Date<%--<font color="Red">*</font>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="rfvPassportExpiryDate" runat="server" ControlToValidate="txtPassportExpiryDate"
                                                    ValidationGroup="submit" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtPassportExpiryDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnPassportExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender7" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnPassportExpiryDate" TargetControlID="txtPassportExpiryDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="color: #33CCFF; font-weight: bold;">
                                                    <div class="B1">
                                                        تأشيرة [Visa]
                                                    </div>
                                                    <div class="B2">
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم الاقامة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtVisaNumberArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtVisaNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الرقم الموحد
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtUnifiedNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Unified Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtUnifiedNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        جهة الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <asp:UpdatePanel ID="updVisaCountryArb" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlVisaCountryArb" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnVisaCountryArb" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnVisaCountryArb_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div class="B3">
                                                        Country
                                                    </div>
                                                    <div class="B4">
                                                        <asp:UpdatePanel ID="updVisaCountry" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div class="B4" style="width: 100%;">
                                                                    <div class="B4" style="width: 75%;">
                                                                        <asp:DropDownList ID="ddlVisaCountry" Width="98%" runat="server">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                        <asp:Button ID="btnVisaCountry" runat="server" CausesValidation="False" CssClass="referencebutton"
                                                                            Text="..." OnClick="btnVisaCountry_Click" />
                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        مكان الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtVisaPlaceofIssueArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Place of Issue
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtVisaPlaceofIssue" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الاصدار
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtVisaReleaseDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnVisaReleaseDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender15" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnVisaReleaseDateArb" TargetControlID="txtVisaReleaseDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Issue Date
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtVisaReleaseDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnVisaReleaseDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnVisaReleaseDate" TargetControlID="txtVisaReleaseDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الانتهاء
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtVisaExpiryDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnVisaExpiryDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender14" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnVisaExpiryDateArb" TargetControlID="txtVisaExpiryDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Expiry Date
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtVisaExpiryDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnVisaExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnVisaExpiryDate" TargetControlID="txtVisaExpiryDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1" style="color: rgb(51, 204, 255); font-weight: bold; width: 181px;">
                                                        بطاقة وطنية [National Card]
                                                    </div>
                                                    <div class="B2">
                                                    </div>
                                                    <div class="B3">
                                                    </div>
                                                    <div class="B4">
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم الهوية
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtNationalCardNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtNationalCardNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم البطاقة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCardNumberArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Card Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الانتهاء
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtNationalCardExpiryDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btntxtNationalCardExpiryDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender16" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btntxtNationalCardExpiryDateArb" TargetControlID="txtNationalCardExpiryDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Expiry Date
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtNationalCardExpiryDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnNationalCardExpiryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnNationalCardExpiryDate" TargetControlID="txtNationalCardExpiryDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div style="float: left; width: 18%; color: #33CCFF;">
                                                        Attach Documents
                                                    </div>
                                                    <div style="clear: both">
                                                    </div>
                                                    <div style="width: 100%; height: auto; float: left; padding-left: 209px; text-align: left;">
                                                        <asp:RequiredFieldValidator ID="rfvDocumentName" Text="Please enter a document name"
                                                            ValidationGroup="AttachedDocument" runat="server" ControlToValidate="txtCandidateDocumentName">
                            
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                    <div style="width: 100%">
                                                        <asp:UpdatePanel ID="upCandidateDocument" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div style="float: left; width: 17%">
                                                                    <asp:Label ID="lblDocumentCaption" runat="server" Text="Document Name"></asp:Label>
                                                                </div>
                                                                <div style="float: left; width: 58%">
                                                                    <asp:TextBox ID="txtCandidateDocumentName" runat="server" CssClass="textbox" MaxLength="30"
                                                                        Style="width: 200px; margin-left: 5px;"></asp:TextBox>
                                                                    <AjaxControlToolkit:AsyncFileUpload ID="fuCandidate" runat="server" ErrorBackColor="White"
                                                                        OnUploadedComplete="fuCandidate_UploadedComplete" PersistFile="True" CompleteBackColor="Green"
                                                                        Visible="true" Width="250px" />
                                                                    <asp:LinkButton ID="lnkAttachCandidateDocument" runat="server" Enabled="true" OnClick="btnAttachCandidateDocument_Click"
                                                                        ValidationGroup="AttachedDocument" Text="Add to list" ForeColor="#33a3d5">
                                                                    </asp:LinkButton>
                                                                </div>
                                                                <div style="clear: both">
                                                                </div>
                                                                <div id="dvAttachedDocuments" runat="server" class="container_content" style="float: left;
                                                                    width: 100%; height: 150px; overflow: auto; padding: 10px 5px 10px 5px;">
                                                                    <asp:DataList ID="dlCandidateDocument" runat="server" GridLines="Horizontal" Width="100%"
                                                                        BorderColor="#33a3d5">
                                                                        <HeaderStyle CssClass="datalistheader" />
                                                                        <HeaderTemplate>
                                                                            <div style="float: left; width: 100%;">
                                                                                <div style="float: left; width: 30%;" class="trLeft">
                                                                                    Document Name
                                                                                </div>
                                                                                <div style="float: left; width: 40%;" class="trLeft">
                                                                                    File Name
                                                                                </div>
                                                                                <div style="float: left; width: 15%;" class="trRight">
                                                                                    Image
                                                                                </div>
                                                                                <div style="float: left; width: 05%;" class="trRight">
                                                                                </div>
                                                                                <%-- <div style="float:left;width:05;" class="trRight">
                                                                        </div>--%>
                                                                            </div>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <div style="float: left; width: 100%;">
                                                                                <div style="float: left; width: 30%;" class="trLeft">
                                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%# Eval("DocumentName")%>'
                                                                                        Width="150px"></asp:Label>
                                                                                </div>
                                                                                <div style="float: left; width: 40%;" class="trLeft">
                                                                                    <asp:Label ID="lblFileName" runat="server" Text='<%# Eval("FileName")%>' Width="150px"></asp:Label>
                                                                                </div>
                                                                                <div style="float: left; width: 15%;" class="trRight">
                                                                                    <asp:Image ID="img" Width="50px" Height="50px" runat="server" ImageUrl='<%# Eval("Location") %>' />
                                                                                </div>
                                                                                <div style="float: left; width: 5%;" class="trRight">
                                                                                    <asp:ImageButton ID="imgDelete" ImageUrl="~/images/Delete18x18.png" runat="server"
                                                                                        CommandArgument='<%# Eval("FileName") %>' OnClick="imgDelete_Click" />
                                                                                </div>
                                                                                <%--<div style="float:left; width:5%;" class="trLeft">
                                                                            <a href='<%#GetDownloadLink(Eval("Node"),Eval("FileName")) %>'> Download</a>
                                                                        </div>--%>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:DataList>
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divTab4" class="TabbedPanelsContent" style="display: none; height: 400px;">
                                            <div style="width: 100%">
                                                <div class="MainDiv" style="height: 100px;">
                                                    <div class="B1">
                                                        العنوان التفصيلي
                                                    </div>
                                                    <div class="B2" style="height: 85px;">
                                                        <asp:TextBox ID="txtPermenantAddressArb" runat="server" MaxLength="500" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidateArabic(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="B3" style="height: 85px;">
                                                        Permanent Address
                                                    </div>
                                                    <div class="B4" style="height: 85px;">
                                                        <asp:TextBox ID="txtPermenantAddress" runat="server" MaxLength="500" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidate(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        اسم شارع المنزل
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtHomeStreetArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="30"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Home Street
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtHomeStreet" runat="server" CssClass="textbox" MaxLength="30"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الإمارة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtCityArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        City
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv" style="height: 100px;">
                                                    <div class="B1">
                                                        عنوان العمل
                                                    </div>
                                                    <div class="B2" style="height: 85px;">
                                                        <asp:TextBox ID="txtBusinessAddressArb" runat="server" MaxLength="500" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidateArabic(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Business Address
                                                    </div>
                                                    <div class="B4" style="height: 85px;">
                                                        <asp:TextBox ID="txtBusinessAddress" runat="server" MaxLength="500" Height="100%"
                                                            onchange="RestrictMulilineLength(this, 500);" onkeyup="RestrictMulilineLengthCandidate(this, 500);"
                                                            TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        هاتف المنزل
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtHomeTelephoneArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Home Telephone
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtHomeTelephone" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        اسم شارع الأعمال
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtBusinessStreetArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="30"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Business Street
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtBusinessStreet" runat="server" CssClass="textbox" MaxLength="30"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        هاتف العمل
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPhoneNumberArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Telephone Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPhoneNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        المنطقة
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtAreaArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Area
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtArea" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        رقم الهاتف المتحرك
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtMobileNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Mobile Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        صندوق البريد
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtPermenantPOBoxArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        PO Box
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtPermenantPOBox" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        تاريخ الاشتراك
                                                    </div>
                                                    <div class="B2">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtSignUpDateArb" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnSignUpDateArb" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender17" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnSignUpDateArb" TargetControlID="txtSignUpDateArb" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="B3">
                                                        Sign Up Date
                                                    </div>
                                                    <div class="B4">
                                                        <div class="B4" style="width: 100%;">
                                                            <div class="B4" style="width: 75%;">
                                                                <asp:TextBox ID="txtSignUpDate" Width="95%" runat="server" MaxLength="10"></asp:TextBox>
                                                            </div>
                                                            <div class="B2" style="width: 15%; height: 35px; margin-top: 2px;">
                                                                <asp:ImageButton ID="btnSignUpDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" />
                                                                <AjaxControlToolkit:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy"
                                                                    PopupButtonID="btnSignUpDate" TargetControlID="txtSignUpDate" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الفاكس
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtFaxArb" onkeyup="arabicValue(this);" runat="server" CssClass="textbox"
                                                            MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Fax
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtFax" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="MainDiv">
                                                    <div class="B1">
                                                        الرقم التاميني
                                                    </div>
                                                    <div class="B2">
                                                        <asp:TextBox ID="txtInsuranceNumberArb" onkeyup="arabicValue(this);" runat="server"
                                                            CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                    <div class="B3">
                                                        Insurance Number
                                                    </div>
                                                    <div class="B4">
                                                        <asp:TextBox ID="txtInsuranceNumber" runat="server" CssClass="textbox" MaxLength="20"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <%--  <div class="MainDiv">
                                            <div class="B1">
                                            </div>
                                            <div class="B2">
                                            </div>
                                            <div class="B3">
                                                Login Password
                                            </div>
                                            <div class="B4">
                                                <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" MaxLength="20" TextMode="Password"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr align="right">
                    <td class="MainDiv" style="width: 95%; height: 100px; margin-top: 60px; margin-right: 5%;
                        text-align: right">
                        <div style="float: left;">
                            <font color="Red">* Mandatory fields &nbsp &nbsp &nbsp &nbsp* * Form submitted and the
                                field's entry found invalid</font></div>
                        <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" OnClientClick="return setTabSelected('submit','divTab1');"
                            ToolTip="Submit" ValidationGroup="submit" Text="Submit" OnClick="btnSubmit_Click" />
                        <asp:Button ID="btnCancelSubmit" runat="server" CssClass="btnsubmit" Text="Cancel"
                            ToolTip="Cancel" CausesValidation="false" OnClick="lnkCancel_Click" />
                    </td>
                </tr>
            </table>
            </div>
            <center>
                <asp:Label ID="lblCandidates" runat="server" Style="display: none;" Width="100%"></asp:Label>
            </center>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn2" runat="server" />
            </div>
            <div style="display: none">
                <asp:Button ID="btn3" runat="server" />
            </div>
            <asp:UpdatePanel ID="upDegreeReference" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none;">
                        <input type="button" id="btnDegreeReference" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeDegreeReference" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlDegreeReference" TargetControlID="btnDegreeReference">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlDegreeReference" runat="server" Style="display: none;">
                        <uc:DegreeReference ID="ucDegreeReference" runat="server" ModalPopupID="mpeDegreeReference" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upDegreeReference1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="display: none;">
                        <input type="button" id="btnDegreeReference1" runat="server" />
                    </div>
                    <AjaxControlToolkit:ModalPopupExtender ID="mpeDegreeReference1" runat="server" BackgroundCssClass="modalBackground"
                        PopupControlID="pnlDegreeReference1" TargetControlID="btnDegreeReference1">
                    </AjaxControlToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlDegreeReference1" runat="server" Style="display: none;">
                        <uc:DegreeReferenceArabic ID="ucDegreeReference1" runat="server" ModalPopupID="mpeDegreeReference1" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button1" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc:ReferenceNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="Button1"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updModalPopUp1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="Button2" runat="server" />
            </div>
            <div id="pnlModalPopUp1" runat="server" style="display: none;">
                <uc:CandidateArabicReference ID="CandidateArabicReference" runat="server" ModalPopupID="mdlPopUpReference1" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference1" runat="server" TargetControlID="Button2"
                PopupControlID="pnlModalPopUp1" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="menu_content" Runat="Server">   
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgAddCandidate" ImageUrl="~/images/Add candidatesBig.png" runat="server"  CausesValidation="false"/>
                </div>
                
                <div class="name">
                    <asp:LinkButton ID="lnkAddCandidate" runat="server" OnClick="lnkAddCandidate_Click" CausesValidation="false"><h5>
                        Add Candidate
                    </h5></asp:LinkButton>
                </div>
                
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgListCandidate" ImageUrl="~/images/Candidate_Details.png"  CausesValidation="false"
                        runat="server" />
                </div>
                
                <div class="name">
                    <asp:LinkButton ID="lnkListCandidate" runat="server" OnClick="lnkListCandidate_Click" CausesValidation="false"><h5>
                        List Candidates
                    </h5></asp:LinkButton>
                </div>
                
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Candidate_Details.png"  CausesValidation="false"
                        runat="server" />
                </div>
                
                <div class="name">
                    <asp:LinkButton ID="LinkButton3" runat="server"  Enabled="false">
                         <h5>Offer Acceptance</h5>  
                    </asp:LinkButton>
                </div>
                
                <div class="sideboxImage">
                    <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/Candidate_Details.png"  CausesValidation="false"
                        runat="server" />
                </div>
                
               <%-- <div class="name">
                    <asp:LinkButton ID="lnkAttachDocuments" runat="server" OnClick="lnkAttachDocuments_Click" CausesValidation="false"><h5>
                        Attach Documents
                    </h5></asp:LinkButton>
                </div>
                
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server"  CausesValidation="false"/>
                </div>
               
                <div class="name">
                    <asp:LinkButton ID="lnkPrint" runat="server" Enabled="false"  CausesValidation="false">
                          <h5> Print</h5></asp:LinkButton>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>--%>
