﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.Globalization;
using System.Collections.Generic;

public partial class Public_VisaTicketProcessing : System.Web.UI.Page
{
    #region VariableDeclarations

    private string CurrentSelectedValue { get; set; }
    clsVisaTicketProcessing obj = new clsVisaTicketProcessing();
    clsCommon objCommon;
    clsUserMaster objUserMaster;

    List<clsVisaDetails> visaDetails = new List<clsVisaDetails>();
    #endregion

    #region Events

    protected void Page_preInit(object sender, EventArgs e)
    {
        clsGlobalization.SetCulture(((Page)this));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("PageTitle.Title").ToString();
        Pager.Fill += new controls_Pager.FillPager(BindDetails);
        if (!IsPostBack)
        {
            EnableMenus();
            ListAll();

            objCommon = new clsCommon();
            string strCurrentTime = objCommon.GetSysTime();
            string s = strCurrentTime.Substring(strCurrentTime.Length - 2, 2);
            txtArrivalTime.Text = strCurrentTime.Substring(0, strCurrentTime.LastIndexOf(s)) + " " + s;
        }
        SetTiming();
        ReferenceNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
    }

    protected void ddlCandidate_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    //protected void chkVisaSentStatus_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (chkVisaSentStatus.Checked)
    //        divVisaSentDate.Style["display"] = "block";
    //    else
    //        divVisaSentDate.Style["display"] = "none";
    //}

    /// <summary>
    /// Loads the reference control for Processing Status
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnProcessingStatusReference_Click(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow dr in gvVisaDetails.Rows)
            {
                DropDownList ddlProcessingStatus = (DropDownList)dr.FindControl("ddlProcessingStatus");
                if (ddlProcessingStatus != null)
                {
                    ReferenceNew1.ClearAll();
                    ReferenceNew1.TableName = "HRVisaTicketStatusReference";
                    ReferenceNew1.DataTextField = "Status";
                    ReferenceNew1.DataTextFieldArabic = "StatusArb";
                    ReferenceNew1.DataValueField = "StatusID";
                    ReferenceNew1.FunctionName = "LoadProcessingStatus";
                    ReferenceNew1.SelectedValue = ddlProcessingStatus.SelectedValue;
                    ReferenceNew1.DisplayName = GetLocalResourceObject("ProcessingStatus.Text").ToString();
                    ReferenceNew1.PopulateData();

                    mdlPopUpReference.Show();
                    updModalPopUp.Update();
                }
            }
        }
        catch { }
    }

    protected void btnVenueReferenece_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlVenue != null)
            {
                ReferenceNew1.ClearAll();
                ReferenceNew1.TableName = "HRVenueReference";
                ReferenceNew1.DataTextField = "Venue";
                ReferenceNew1.DataTextFieldArabic = "VenueArb";
                ReferenceNew1.DataValueField = "VenueID";
                ReferenceNew1.FunctionName = "LoadVenue";
                ReferenceNew1.SelectedValue = ddlVenue.SelectedValue;
                ReferenceNew1.DisplayName = GetLocalResourceObject("Venue.Text").ToString();
                ReferenceNew1.PopulateData();

                mdlPopUpReference.Show();
                updModalPopUp.Update();
            }
        }
        catch { }
    }

    protected void chkTicketSent_CheckedChanged(object sender, EventArgs e)
    {
        if (chkTicketSent.Checked)
            divTicketDetails.Style["display"] = "block";
        else
        {
            divTicketDetails.Style["display"] = "none";
            ClearTicketDetails();
        }
    }

    private void ClearTicketDetails()
    {
        txtTicketDate.Text = string.Empty;
        txtArrival.Text = string.Empty;
        ddlVenue.SelectedIndex = -1;
        rblPickUpReq.SelectedValue = "0";
        txtTicketAmount.Text = "0";
        txtTicketRemarks.Text = string.Empty;
        upnlAddEdit.Update();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Save();
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        objUserMaster = new clsUserMaster();
        if (clsVisaTicketProcessing.GetTotalRecordCount(objUserMaster.GetCompanyId()) > 0)
            ListAll();
        else
            lnkAdd_Click(null, null);
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ClearControls();
        SetTiming();
        BindInitials();
        SetPreviousRow();
        lnkDelete.OnClientClick = "return false;";
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        //objUserMaster = new clsUserMaster();
        //if (clsVisaTicketProcessing.GetTotalRecordCount(objUserMaster.GetCompanyId()) > 0)
        //    ListAll();
        //else
        //    lnkAdd_Click(null, null);
        Pager.CurrentPage = 0;
        EnableMenus();
        BindDetails();
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = string.Empty;
            if (dlVisaTicket.Items.Count > 0)//Deleting selected records
            {
                for (int i = 0; i < dlVisaTicket.Items.Count; i++)
                {
                    CheckBox chkSelect = (CheckBox)dlVisaTicket.Items[i].FindControl("chkSelect");

                    if (chkSelect == null)
                        continue;
                    if (chkSelect.Checked)
                    {
                        obj.VisaTicketProcessID = dlVisaTicket.DataKeys[dlVisaTicket.Items[i].ItemIndex].ToInt32();

                        Delete(Convert.ToInt32(obj.VisaTicketProcessID));

                    }
                }
            }
            if (ViewState["VisaTicketProcessID"].ToInt32() > 0)//deleting a single record
            {
                obj.VisaTicketProcessID = ViewState["VisaTicketProcessID"].ToInt32();
                Delete(obj.VisaTicketProcessID.ToInt32());
            }
            ListAll();

        }
        catch { }
    }

    private void Delete(int VisaTicketProcessID)
    {
        string msg = string.Empty;
        clsDocumentAlert objAlert = new clsDocumentAlert();
        if (obj.Delete())
        {
            msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحذوفة بنجاح") : ("Deleted successfully");
            mcMessage.InformationalMessage(msg);
            mpeMessage.Show();
            objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.CandidateVisaTicket), VisaTicketProcessID);
            objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.CandidateArrivalDate), VisaTicketProcessID);
        }
    }

    protected void dlVisaTicket_ItemCommand(object source, DataListCommandEventArgs e)
    {
        string msg = string.Empty;
        objUserMaster = new clsUserMaster();
        obj.VisaTicketProcessID = Convert.ToInt32(e.CommandArgument);

        switch (e.CommandName)
        {
            case "VIEW":
                DataSet ds = obj.GetSingleRecord(objUserMaster.GetCompanyId());
                if (ds.Tables.Count > 0)
                {
                    DataTable dtView = ds.Tables[0];
                    if (dtView.Rows.Count > 0)
                        dlSingleView.DataSource = dtView;
                    dlSingleView.DataBind();

                    lnkDelete.Enabled = ViewState["IsDelete"].ToBoolean();
                    lnkDelete.OnClientClick = clsUserMaster.GetCulture() == "ar-AE" ? ("return confirm('هل أنت متأكد من حذف هذه الوظيفة ؟');") : ("return confirm('Are you sure to delete this ?');");
                    DivVisibility("SingleView");
                }

                break;

            case "EDIT":       //binds data onto controls for updations
                if (ViewState["IsUpdate"].ToBoolean())
                {
                    Edit(obj.VisaTicketProcessID.ToInt32());
                    lnkDelete.Enabled = false;
                    lnkDelete.OnClientClick = "return false";
                    hfMode.Value = "1";
                    DivVisibility("Edit");
                }
                break;
        }
    }

    protected void dlVisaTicket_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlVisaTicket.ClientID + "');";
    }
    protected void dlSingleView_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            GridView gvVisa = (GridView)e.Item.FindControl("gvVisa");
            if (gvVisa != null)
            {
                DataSet ds = obj.GetSingleRecord(objUserMaster.GetCompanyId());

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[1];
                    if (dt.Rows.Count > 0)
                    {
                        gvVisa.DataSource = dt;
                        gvVisa.DataBind();
                    }
                }
            }
        }
    }

    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        string msg = string.Empty;
        if (ViewState["Visa"] != null)
        {
            int Id = ((ImageButton)sender).CommandArgument.ToInt32();
            if (Id > 0)
            {
                int rCount = 0;
                foreach (GridViewRow dr in gvVisaDetails.Rows)
                {
                    DropDownList ddlProcessingStatus = (DropDownList)dr.FindControl("ddlProcessingStatus");
                    //TextBox txtVisaNumber = (TextBox)dr.FindControl("txtVisaNumber");
                    //DropDownList ddlVisaType = (DropDownList)dr.FindControl("ddlVisaType");
                    TextBox txtIssuedate = (TextBox)dr.FindControl("txtIssuedate");
                    //TextBox txtVisaValidityDate = (TextBox)dr.FindControl("txtVisaValidityDate");
                    TextBox txtExpiryDate = (TextBox)dr.FindControl("txtExpiryDate");
                    TextBox txtVisaAmount = (TextBox)dr.FindControl("txtVisaAmount");
                    TextBox txtVisaRemarks = (TextBox)dr.FindControl("txtVisaRemarks");
                    CheckBox chkAlert = (CheckBox)dr.FindControl("chkAlert");
                    //UpdatePanel upnlVisa = (UpdatePanel)dr.FindControl("upnlVisa");

                    if (rCount != Id)
                    {
                        visaDetails.Add(new clsVisaDetails()
                        {
                            StatusID = ddlProcessingStatus.SelectedValue.ToInt32(),
                            //VisaNumber = txtVisaNumber.Text.Trim().ToString(),
                            //VisaTypeID = ddlVisaType.SelectedValue.ToInt32(),
                            VisaIssueDate = txtIssuedate.Text.ToString(),
                            //VisaValidityDate = txtVisaValidityDate.Text.ToString(),
                            VisaExpiryDate = txtExpiryDate.Text.ToString(),
                            VisaAmount = txtVisaAmount.Text.Trim().ToDecimal(),
                            VisaDetailsRemarks = txtVisaRemarks.Text.ToString(),
                            IsAlertRequired = chkAlert.Checked,
                            VisaID = rCount
                        });

                    }
                    rCount++;
                }
                ViewState["Visa"] = visaDetails;
                gvVisaDetails.DataSource = visaDetails;
                gvVisaDetails.DataBind();
                SetPreviousRow();
                upnlVisa.Update();
            }
            else
            {
                msg = clsUserMaster.GetCulture() == "ar-AE" ? ("لا يمكن حذف الصف الأول.") : ("Cannot delete first row.");
                mcMessage.WarningMessage(msg);
                mpeMessage.Show();
            }
        }
    }

    protected void gvVisaDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex >= 0)
        {
            DropDownList ddlProcessingStatus = (DropDownList)e.Row.FindControl("ddlProcessingStatus");
            ImageButton btnAdd = (ImageButton)e.Row.FindControl("btnAdd");

            if (ddlProcessingStatus != null && btnAdd != null)
            {
                ddlProcessingStatus.DataSource = clsVisaTicketProcessing.GetProcessingStatus();
                ddlProcessingStatus.DataBind();
                //ddlProcessingStatus.Items.Insert(0, new ListItem("Select", "-1"));

                // LoadProcessingStatus();
            }
            //DropDownList ddlVisaType = (DropDownList)e.Row.FindControl("ddlVisaType");
            //if (ddlVisaType != null && btnAdd != null)
            //{
            //    ddlVisaType.DataSource = clsVisaTicketProcessing.GetVisaTypes();
            //    ddlVisaType.DataBind();
            //}

            if (visaDetails.Count > 0)
                btnAdd.Visible = (e.Row.RowIndex == visaDetails.Count - 1);
            else if (((DataTable)ViewState["Visa"]).Rows.Count > 0)
                btnAdd.Visible = (e.Row.RowIndex == ((DataTable)ViewState["Visa"]).Rows.Count - 1);
        }
    }

    protected void btnAdd_Click(object sender, ImageClickEventArgs e)
    {
        AddNewRow();
    }
    #endregion

    #region Methods

    private void SetInitialRow()
    {
        visaDetails.Add(new clsVisaDetails()
        {
            StatusID = 0,
            //VisaNumber = "",
            //VisaTypeID = 0,
            VisaIssueDate = DateTime.Now.Date.ToString("dd/MM/yyyy"),
            //VisaValidityDate = DateTime.Now.Date.ToString("dd/MM/yyyy"),
            VisaExpiryDate = DateTime.Now.AddDays(1).Date.ToString("dd/MM/yyyy"),
            VisaAmount = new decimal(0),
            VisaDetailsRemarks = "",
            IsAlertRequired = false,
            VisaID = 0
        });
        ViewState["Visa"] = visaDetails;

        gvVisaDetails.DataSource = visaDetails;
        gvVisaDetails.DataBind();
    }

    private void AddNewRow()
    {
        if (ViewState["Visa"] != null)
        {
            int rCount = 0;
            visaDetails = new List<clsVisaDetails>();
            foreach (GridViewRow dr in gvVisaDetails.Rows)
            {
                DropDownList ddlProcessingStatus = (DropDownList)dr.FindControl("ddlProcessingStatus");
                //TextBox txtVisaNumber = (TextBox)dr.FindControl("txtVisaNumber");
                //DropDownList ddlVisaType = (DropDownList)dr.FindControl("ddlVisaType");
                TextBox txtIssuedate = (TextBox)dr.FindControl("txtIssuedate");
                //TextBox txtVisaValidityDate = (TextBox)dr.FindControl("txtVisaValidityDate");
                TextBox txtExpiryDate = (TextBox)dr.FindControl("txtExpiryDate");
                TextBox txtVisaAmount = (TextBox)dr.FindControl("txtVisaAmount");
                TextBox txtVisaRemarks = (TextBox)dr.FindControl("txtVisaRemarks");
                CheckBox chkAlert = (CheckBox)dr.FindControl("chkAlert");

                if (ddlProcessingStatus.SelectedIndex == 0 || ddlProcessingStatus.SelectedValue == "-1")
                {
                    mcMessage.WarningMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("حدد معالجة الحالة") : ("Select Processing Status"));
                    mpeMessage.Show();  
                    break;
                }
                if (chkAlert.Checked)
                {
                    if (txtExpiryDate.Text == string.Empty)
                    {
                        mcMessage.WarningMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("أدخل تاريخ الانتهاء") : ("Enter Expiry Date"));
                        mpeMessage.Show();
                        break;
                    }
                }
                //if (txtVisaNumber.Text == string.Empty)
                //{
                //    DispEmptyControlMessage("Enter Visa Number", "txt");
                //    break;
                //}
                //if (ddlVisaType.SelectedIndex == 0 || ddlVisaType.SelectedValue == "-1")
                //{
                //    DispEmptyControlMessage("Select Visa Type", "ddl");
                //    break;
                //}

                visaDetails.Add(new clsVisaDetails()
                {
                    StatusID = ddlProcessingStatus.SelectedValue.ToInt32(),
                    //VisaNumber = txtVisaNumber.Text.Trim().ToString(),
                    //VisaTypeID = ddlVisaType.SelectedValue.ToInt32(),
                    VisaIssueDate = txtIssuedate.Text.ToString(),
                    //VisaValidityDate = txtVisaValidityDate.Text.ToString(),
                    VisaExpiryDate = txtExpiryDate.Text.ToString(),
                    VisaAmount = txtVisaAmount.Text.Trim().ToDecimal(),
                    VisaDetailsRemarks = txtVisaRemarks.Text.ToString(),
                    IsAlertRequired = chkAlert.Checked,
                    VisaID = rCount
                });
                rCount++;
            }
            visaDetails.Add(new clsVisaDetails()
            {
                StatusID = 0,
                //VisaNumber = "",
                //VisaTypeID = 0,
                VisaIssueDate = DateTime.Now.Date.ToString("dd/MM/yyyy"),
                //VisaValidityDate = DateTime.Now.Date.ToString("dd/MM/yyyy"),
                VisaExpiryDate = DateTime.Now.AddDays(1).Date.ToString("dd/MM/yyyy"),
                VisaAmount = new decimal(0),
                VisaDetailsRemarks = "",
                IsAlertRequired = false,
                VisaID = rCount
            });

            ViewState["Visa"] = visaDetails;
            gvVisaDetails.DataSource = visaDetails;
            gvVisaDetails.DataBind();
        }
        SetPreviousRow();
        upnlVisa.Update();
    }
    
    private void SetPreviousRow()
    {
        if (ViewState["Visa"] != null)
        {
            if (visaDetails.Count > 0)
            {
                int i = 0;
                foreach (clsVisaDetails item in visaDetails)
                {
                    DropDownList ddlProcessingStatus = (DropDownList)gvVisaDetails.Rows[i].FindControl("ddlProcessingStatus");
                    //TextBox txtVisaNumber = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaNumber");
                    //DropDownList ddlVisaType = (DropDownList)gvVisaDetails.Rows[i].FindControl("ddlVisaType");
                    TextBox txtIssuedate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtIssuedate");
                    TextBox txtVisaValidityDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaValidityDate");
                    TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtExpiryDate");
                    TextBox txtVisaAmount = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaAmount");
                    TextBox txtVisaRemarks = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaRemarks");
                    CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[i].FindControl("chkAlert");

                    ddlProcessingStatus.SelectedValue = item.StatusID.ToString();
                    //txtVisaNumber.Text = item.VisaNumber.ToString();
                    //ddlVisaType.SelectedValue = item.VisaTypeID.ToString();
                    txtIssuedate.Text = item.VisaIssueDate.ToString();
                    //txtVisaValidityDate.Text = item.VisaValidityDate.ToString();
                    txtExpiryDate.Text = item.VisaExpiryDate.ToString();
                    txtVisaAmount.Text = item.VisaAmount.ToString();
                    txtVisaRemarks.Text = item.VisaDetailsRemarks.ToString();
                    chkAlert.Checked = item.IsAlertRequired.ToBoolean();
                    i++;
                }
            }
            else if (((DataTable)ViewState["Visa"]).Rows.Count > 0)
            {
                DataTable dtVisa = (DataTable)ViewState["Visa"];
                for (int iCounter = 0; iCounter < gvVisaDetails.Rows.Count; iCounter++)
                {
                    DropDownList ddlProcessingStatus = (DropDownList)gvVisaDetails.Rows[iCounter].FindControl("ddlProcessingStatus");
                    //TextBox txtVisaNumber = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaNumber");
                    //DropDownList ddlVisaType = (DropDownList)gvVisaDetails.Rows[iCounter].FindControl("ddlVisaType");
                    TextBox txtIssuedate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtIssuedate");
                    //TextBox txtVisaValidityDate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaValidityDate");
                    TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtExpiryDate");
                    TextBox txtVisaAmount = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaAmount");
                    TextBox txtVisaRemarks = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaRemarks");
                    CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[iCounter].FindControl("chkAlert");

                    ddlProcessingStatus.SelectedValue = dtVisa.Rows[iCounter]["StatusID"].ToString();
                    //txtVisaNumber.Text = dtVisa.Rows[iCounter]["VisaNumber"].ToString();
                    //ddlVisaType.SelectedValue = dtVisa.Rows[iCounter]["VisaTypeID"].ToString();
                    txtIssuedate.Text = dtVisa.Rows[iCounter]["VisaIssuedate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    //txtVisaValidityDate.Text = dtVisa.Rows[iCounter]["VisaValidityDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtExpiryDate.Text = dtVisa.Rows[iCounter]["VisaExpiryDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    txtVisaAmount.Text = dtVisa.Rows[iCounter]["VisaAmount"].ToString();
                    txtVisaRemarks.Text = dtVisa.Rows[iCounter]["VisaDetailsRemarks"].ToString();
                    chkAlert.Checked = Convert.ToBoolean(dtVisa.Rows[iCounter]["IsAlertRequired"]);
                }
            }
        }
    }


    public void EnableMenus()
    {
        objUserMaster = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        int RoleID = objUserMaster.GetRoleId();

        if (RoleID > 4)//RoleID based checking
        {
            DataTable dtm = objRoleSettings.GetPermissions(objUserMaster.GetRoleId(), (int)eMenuID.CandidateVisaTicket);

            if (dtm.Rows.Count > 0)
            {
                ViewState["IsCreate"] = lnkAdd.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
                ViewState["IsView"] = lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
                ViewState["IsDelete"] = lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
                ViewState["IsUpdate"] = dtm.Rows[0]["IsUpdate"].ToBoolean();
            }
        }
        else
        {
            lnkAdd.Enabled = lnkView.Enabled = lnkDelete.Enabled = true;
            ViewState["IsView"] = ViewState["IsDelete"] = ViewState["IsCreate"] = ViewState["IsUpdate"] = true;
        }
        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlVisaTicket.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";
        upnlAddEdit.Update();
    }

    private void SetTiming()
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "ArrivalTime", "$(function () { $('#" + txtArrivalTime.ClientID + "').timeEntry(); });", true);
    }

    private void BindInitials()
    {
        LoadCandidateNames();
        LoadProcessingStatus();
        //LoadVisatype();
        SetInitialRow();
        LoadVenue();
        DivVisibility("Add");
        ddlCandidate.Enabled = true;
    }

    //private void LoadVisatype()
    //{
    //    ddlVisaType.DataSource = new clsEmployeeVisa().FillVisaType();
    //    ddlVisaType.DataTextField = "VisaType";
    //    ddlVisaType.DataValueField = "VisaTypeID";
    //    ddlVisaType.DataBind();
    //}

    private void ListAll()
    {
        objUserMaster = new clsUserMaster();
        int count = clsVisaTicketProcessing.GetTotalRecordCount(objUserMaster.GetCompanyId());
        if (count > 0)
        {
            DivVisibility("ViewAll");
            lnkDelete.OnClientClick = "return true;";
            lnkDelete.Enabled = ViewState["IsDelete"].ToBoolean();

            if (ViewState["IsView"].ToBoolean())
            {
                BindDetails();
                Pager.Visible = true;
            }
            else
            {
                dlVisaTicket.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
                DivVisibility("NoData");
            }
        }
        else
        {
            if (!ViewState["IsView"].ToBoolean())
            {
                lnkAdd.Enabled = lnkView.Enabled = lnkDelete.Enabled = dlVisaTicket.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetLocalResourceObject("Youhavenopermissiontoviewthispage.Text").ToString();
                DivVisibility("NoData");
                lnkDelete.OnClientClick = "return false;";
            }
            else if (ViewState["IsCreate"].ToBoolean())
                lnkAdd_Click(null, null);
            else
            {
                dlVisaTicket.Visible = Pager.Visible = false;
                Pager.Total = 0;
                divNoData.InnerHtml = GetGlobalResourceObject("ControlsCommon", "NoRecordFound").ToString();
                DivVisibility("NoData");
            }
        }
    }

    public void DivVisibility(string Val)
    {
        if (Val == "Add" || Val == "Edit")
        {
            divAddEdit.Style["display"] = "block";
            divViewAll.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
        }
        else if (Val == "SingleView")
        {
            divAddEdit.Style["display"] = "none";
            divViewAll.Style["display"] = "none";
            divSingleView.Style["display"] = "block";
        }
        else if (Val == "ViewAll")
        {

            divAddEdit.Style["display"] = "none";
            divViewAll.Style["display"] = "block";
            divSingleView.Style["display"] = "none";
        }
        else if (Val == "NoData")
        {
            divNoData.Style["display"] = "block";
            divAddEdit.Style["display"] = "none";
            divViewAll.Style["display"] = "none";
            divSingleView.Style["display"] = "none";
        }

        upnlViewAll.Update();
        upnlAddEdit.Update();
        upnlSingleView.Update();
    }

    private void BindDetails()
    {
        try
        {
            objUserMaster = new clsUserMaster();
            obj.PageIndex = Pager.CurrentPage + 1;
            obj.PageSize = Pager.PageSize;
            DataTable dt = obj.GetAllRecords(objUserMaster.GetCompanyId());

            if (dt.Rows.Count == 0)
            {
                dlVisaTicket.Visible = Pager.Visible = false;
                Pager.Total = 0;
            }
            else
            {
                dlVisaTicket.Visible = Pager.Visible = true;
                Pager.Total = clsVisaTicketProcessing.GetTotalRecordCount(objUserMaster.GetCompanyId());

                DivVisibility("ViewAll");
                dlVisaTicket.DataSource = dt;
                dlVisaTicket.DataBind();
            }
            upnlViewAll.Update();
            upnlAddEdit.Update();
            upnlSingleView.Update();
        }
        catch (Exception ex)
        { }
    }

    public void LoadCandidateNames()
    {
        string str = string.Empty;
        objUserMaster = new clsUserMaster();
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlCandidate.DataSource = clsVisaTicketProcessing.GetCandidateNames(objUserMaster.GetCompanyId());
        ddlCandidate.DataBind();
        ddlCandidate.Items.Insert(0, new ListItem(str, "-1"));
    }

    public void LoadProcessingStatus()
    {
        //string str = string.Empty;
        //str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        foreach (GridViewRow dr in gvVisaDetails.Rows)
        {
            DropDownList ddlProcessingStatus = (DropDownList)dr.FindControl("ddlProcessingStatus");
            UpdatePanel upnlProcessingStatus = (UpdatePanel)dr.FindControl("upnlProcessingStatus");
            ddlProcessingStatus.DataSource = clsVisaTicketProcessing.GetProcessingStatus();
            ddlProcessingStatus.DataBind();

            //ddlProcessingStatus.Items.Insert(0, new ListItem(str, "-1"));
            this.SetSelectedIndex(ddlProcessingStatus);
            upnlProcessingStatus.Update();
        }
    }

    public void LoadVenue()
    {
        string str = string.Empty;
        str = clsUserMaster.GetCulture() == "ar-AE" ? ("اختر") : ("Select");
        ddlVenue.DataSource = clsVisaTicketProcessing.GetVenue();
        ddlVenue.DataBind();
        ddlVenue.Items.Insert(0, new ListItem(str, "-1"));
        this.SetSelectedIndex(this.ddlVenue);
        upnlVenue.Update();
    }

    private void SetSelectedIndex(DropDownList ddwn)
    {
        if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
            ddwn.SelectedValue = CurrentSelectedValue;
    }

    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }

    void ReferenceControlNew1_onRefresh()
    {
        mdlPopUpReference.Show();
    }

    private void Save()
    {
        objUserMaster = new clsUserMaster();
        bool val = true;
        if (Validate())
        {
            for (int iCounter = 0; iCounter < gvVisaDetails.Rows.Count; iCounter++)
            {
                TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtExpiryDate");
                CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[iCounter].FindControl("chkAlert");
                DropDownList ddlProcessingStatus = (DropDownList)gvVisaDetails.Rows[iCounter].FindControl("ddlProcessingStatus");
                if (chkAlert.Checked)
                {
                    if (txtExpiryDate.Text == string.Empty)
                    {
                        val = false;
                        mcMessage.WarningMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("أدخل تاريخ الانتهاء") : ("Enter Expiry Date"));
                        mpeMessage.Show();
                        break;
                    }
                }
                if (ddlProcessingStatus.SelectedValue.ToInt32() <= 0)
                {
                    val = false;
                    mcMessage.WarningMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("حدد معالجة الحالة ") : ("Select Processing Status"));
                    mpeMessage.Show();                   
                    break;
                }
            }
            if (hfMode.Value == "0")
            {
                if ((obj.CheckCandidateDuplication(ddlCandidate.SelectedValue.ToInt32())).ToInt32() > 0)
                {
                    val = false;
                    mcMessage.WarningMessage(clsUserMaster.GetCulture() == "ar-AE" ? ("التفاصيل فيزا / تذكرة أرسلت بالفعل لهذا المرشح ") : ("Visa/Ticket details already sent to this candidate"));
                    mpeMessage.Show();
                }
            }
            if (val)
            {
                obj.CandidateID = ddlCandidate.SelectedValue.ToInt32();
                obj.VisaRemarks = txtRemarks.Text.Trim();
                obj.TicketSent = chkTicketSent.Checked;
                obj.TicketSentdate = clsCommon.Convert2DateTime(txtTicketDate.Text.Trim());
                obj.Arrivaldate = clsCommon.Convert2DateTime(txtArrival.Text.Trim());
                obj.ArrivalTime = (chkTicketSent.Checked == false) ? string.Empty : txtArrivalTime.Text.Trim();
                obj.VenueID = ddlVenue.SelectedValue.ToInt32();
                obj.PickUpReq = rblPickUpReq.SelectedValue.ToInt32() == 1 ? true : false;
                obj.TicketAmount = Convert.ToDecimal(txtTicketAmount.Text.Trim());
                obj.TicketRemarks = txtTicketRemarks.Text.Trim();
                obj.CompanyID = objUserMaster.GetCompanyId();

                string msg = string.Empty;
                int save = 0;
                clsDocumentAlert objAlert = new clsDocumentAlert();
                if (hfMode.Value == "0")
                {
                    int ID = obj.Save();
                    if (ID > 0)
                        msg = clsUserMaster.GetCulture() == "ar-AE" ? ("المحفوظة بنجاح") : ("Successfully Saved");
                    save = 1;
                    ViewState["VisaTicketProcessID"] = ID;
                    for (int i = 0; i < gvVisaDetails.Rows.Count; i++)
                    {
                        //TextBox txtVisaNumber = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaNumber");
                        //TextBox txtVisaValidityDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaValidityDate");
                        TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtExpiryDate");
                        CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[i].FindControl("chkAlert");
                        if (chkAlert.Checked)
                            objAlert.AlertMessage(Convert.ToInt32(DocumentType.CandidateVisaTicket), "Candidate Visa", "Visa validity", ID, "Visa", clsCommon.Convert2DateTime(txtExpiryDate.Text), "Candidate", Convert.ToInt32(ddlCandidate.SelectedValue), ddlCandidate.SelectedItem.Text, false);
                        else
                            objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.CandidateVisaTicket), ID);
                    }
                    if (chkTicketSent.Checked && txtArrival.Text != string.Empty)
                        objAlert.AlertMessage(Convert.ToInt32(DocumentType.CandidateArrivalDate), "Candidate Arrival Date", "Candidate Arrival Date", ID, "Ticket", clsCommon.Convert2DateTime(txtArrival.Text), "Candidate", Convert.ToInt32(ddlCandidate.SelectedValue), ddlCandidate.SelectedItem.Text, false);
                    hfMode.Value = "1";
                    ddlCandidate.Enabled = false;
                }
                else
                {
                    obj.VisaTicketProcessID = ViewState["VisaTicketProcessID"].ToInt32();
                    obj.Save();
                    msg = clsUserMaster.GetCulture() == "ar-AE" ? ("تحديث بنجاح") : ("Successfully Updated");
                    save = 1;
                    for (int i = 0; i < gvVisaDetails.Rows.Count; i++)
                    {
                        ////TextBox txtVisaNumber = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaNumber");
                        ////TextBox txtVisaValidityDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtVisaValidityDate");
                        TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[i].FindControl("txtExpiryDate");
                        CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[i].FindControl("chkAlert");
                        if (chkAlert.Checked)
                            objAlert.AlertMessage(Convert.ToInt32(DocumentType.CandidateVisaTicket), "Candidate Visa", "Visa validity", ViewState["VisaTicketProcessID"].ToInt32(), "Visa", clsCommon.Convert2DateTime(txtExpiryDate.Text), "Candidate", Convert.ToInt32(ddlCandidate.SelectedValue), ddlCandidate.SelectedItem.Text, false);
                        else
                            objAlert.DeleteExpiryAlertMessage(Convert.ToInt32(DocumentType.CandidateVisaTicket), ViewState["VisaTicketProcessID"].ToInt32());
                    }
                    if (chkTicketSent.Checked && txtArrival.Text != string.Empty)
                        objAlert.AlertMessage(Convert.ToInt32(DocumentType.CandidateArrivalDate), "Candidate Arrival Date", "Candidate Arrival Date", ViewState["VisaTicketProcessID"].ToInt32(), "Ticket", clsCommon.Convert2DateTime(txtArrival.Text), "Candidate", Convert.ToInt32(ddlCandidate.SelectedValue), ddlCandidate.SelectedItem.Text, false);
                }
                if (save == 1)
                {
                    obj.DeleteVisaDetails(ViewState["VisaTicketProcessID"].ToInt32());
                    for (int iCounter = 0; iCounter < gvVisaDetails.Rows.Count; iCounter++)
                    {
                        DropDownList ddlProcessingStatus = (DropDownList)gvVisaDetails.Rows[iCounter].FindControl("ddlProcessingStatus");
                        //TextBox txtVisaNumber = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaNumber");
                        //DropDownList ddlVisaType = (DropDownList)gvVisaDetails.Rows[iCounter].FindControl("ddlVisaType");
                        TextBox txtIssuedate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtIssuedate");
                        //TextBox txtVisaValidityDate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaValidityDate");
                        TextBox txtExpiryDate = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtExpiryDate");
                        TextBox txtVisaAmount = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaAmount");
                        TextBox txtVisaRemarks = (TextBox)gvVisaDetails.Rows[iCounter].FindControl("txtVisaRemarks");
                        CheckBox chkAlert = (CheckBox)gvVisaDetails.Rows[iCounter].FindControl("chkAlert");

                        visaDetails.Add(new clsVisaDetails()
                        {
                            StatusID = ddlProcessingStatus.SelectedValue.ToInt32(),
                            //VisaNumber = txtVisaNumber.Text.Trim().ToString(),
                            //VisaTypeID = ddlVisaType.SelectedValue.ToInt32(),
                            VisaIssueDate = clsCommon.Convert2DateTime(txtIssuedate.Text.Trim()).ToString(),
                            //VisaValidityDate = clsCommon.Convert2DateTime(txtVisaValidityDate.Text.Trim()).ToString(),
                            VisaExpiryDate = clsCommon.Convert2DateTime(txtExpiryDate.Text.Trim()).ToString(),
                            VisaAmount = txtVisaAmount.Text.Trim().ToDecimal(),
                            VisaDetailsRemarks = txtVisaRemarks.Text.ToString(),
                            IsAlertRequired = chkAlert.Checked
                        });
                    }
                    obj.SaveVisaDetails(visaDetails, ViewState["VisaTicketProcessID"].ToInt32());
                }
                mcMessage.InformationalMessage(msg);
                mpeMessage.Show();
            }
        }
    }

    private bool Validate()
    {
        bool val = true;
        if (ddlCandidate.SelectedValue == "-1")
        {
            val = false;
            //return val;
        }
        //if (chkVisaSentStatus.Checked)
        //{
        //    if (txtVisaSentdate.Text == string.Empty)
        //    {
        //        rfvtxtVisaSentdatepnlTab1.Validate();
        //        val = false;
        //        return val;
        //    }
        //    else if (txtVisaNumber.Text == string.Empty)
        //    {
        //        rfvVisaNumber.Validate();
        //        val = false;
        //        return val;
        //    }
        //    else if (txtIssuedate.Text == string.Empty)
        //    {
        //        cvVisaIssuedate.ValidateEmptyText = true;
        //        rfvIssuedate.Validate();
        //        val = false;
        //        return val;
        //    }
        //    else if (txtExpiryDate.Text == string.Empty)
        //    {
        //        cvVisaExpirydate.ValidateEmptyText = true;
        //        rfvExpiryDate.Validate();
        //        val = false;
        //        return val;
        //    }
        //    else if (txtVisaValidityDate.Text == string.Empty)
        //    {
        //        rfvvalidity.Validate();
        //        val = false;
        //        return val;
        //    }
        //    else if (ddlVisaType.SelectedValue == "-1")
        //    {
        //        rfvVisatype.Validate();
        //        val = false;
        //        return val;
        //    }
        //return val;
        //}
        if (chkTicketSent.Checked)
        {
            if (txtTicketDate.Text == string.Empty)
            {
                rfvTicketDate.Validate();
                val = false;
                //return val;
            }
            else if (txtArrival.Text == string.Empty)
            {
                rfvArrivalDate.Validate();
                val = false;
                //return val;
            }
            else if (txtArrivalTime.Text == string.Empty)
            {
                rfvtxtArrivalTime.Validate();
                val = false;
                //return val;
            }
            else if (ddlVenue.SelectedValue == "-1")
            {
                rfvVenue.Validate();
                val = false;
                //return val;
            }
            return val;
        }

        return val;
    }

    private void Edit(int VisaTicketProcessID)
    {
        try
        {
            objUserMaster = new clsUserMaster();
            ViewState["VisaTicketProcessID"] = VisaTicketProcessID;
            obj.VisaTicketProcessID = VisaTicketProcessID;
            DataSet ds = obj.GetSingleRecord(objUserMaster.GetCompanyId());
            if (ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    obj.VisaTicketProcessID = dt.Rows[0]["VisaTicketProcessID"].ToInt32();
                    BindInitials();

                    ddlCandidate.SelectedValue = dt.Rows[0]["CandidateID"].ToString();
                    ddlCandidate.Enabled = false;
                    DataTable dtVisa = ds.Tables[1];
                    if (dtVisa.Rows.Count > 0)
                    {
                        visaDetails.Clear();
                        ViewState["Visa"] = ds.Tables[1];
                        gvVisaDetails.DataSource = dtVisa;
                        gvVisaDetails.DataBind();
                        SetPreviousRow();
                    }
                    else
                        SetPreviousRow();
                    txtRemarks.Text = dt.Rows[0]["VisaRemarks"].ToString();
                    chkTicketSent.Checked = dt.Rows[0]["TicketSentBool"].ToBoolean();
                    if (chkTicketSent.Checked)
                    {
                        divTicketDetails.Style["display"] = "block";
                        txtTicketDate.Text = dt.Rows[0]["TicketSentDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        txtArrival.Text = dt.Rows[0]["ArrivalDate"].ToDateTime().ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        txtArrivalTime.Text = dt.Rows[0]["ArrivalTime"].ToString();
                        ddlVenue.SelectedValue = dt.Rows[0]["VenueID"].ToString();
                        rblPickUpReq.SelectedValue = (dt.Rows[0]["PickUpReq"].ToString() == "True") ? "1" : "0";
                        txtTicketAmount.Text = dt.Rows[0]["TicketAmount"].ToString();
                        txtTicketRemarks.Text = dt.Rows[0]["TicketRemarks"].ToString();
                    }
                    else
                    {
                        divTicketDetails.Style["display"] = "none";
                        txtTicketDate.Text = string.Empty;
                        txtArrival.Text = string.Empty;
                        txtArrivalTime.Text = string.Empty;
                        ddlVenue.SelectedIndex = -1;
                        rblPickUpReq.SelectedValue = "0";
                        txtTicketAmount.Text = "0";
                        txtTicketRemarks.Text = string.Empty;
                    }
                }
            }
            upnlAddEdit.Update();
        }
        catch { }
    }

    private void ClearControls()
    {
        ddlCandidate.SelectedIndex = -1;

        visaDetails.Clear();
        gvVisaDetails.DataSource = null;
        gvVisaDetails.DataBind();
        txtRemarks.Text = string.Empty;
        chkTicketSent.Checked = false;
        divTicketDetails.Style["display"] = "none";
        txtTicketDate.Text = string.Empty;
        txtArrival.Text = string.Empty;
        txtTicketAmount.Text = "0";
        ddlVenue.SelectedIndex = -1;
        rblPickUpReq.SelectedValue = "0";
        txtTicketRemarks.Text = string.Empty;
        ViewState["VisaTicketProcessID"] = ViewState["Visa"] = null;
        hfMode.Value = "0";
        upnlAddEdit.Update();
    }

    #endregion
}
