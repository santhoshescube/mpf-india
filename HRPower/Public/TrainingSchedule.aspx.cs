﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

public partial class Public_TrainingSchedule : System.Web.UI.Page
{
    clsTrainingSchedule objTrainingSchedule;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        objTrainingSchedule = new clsTrainingSchedule();
        if (!IsPostBack)
        {
            BindCourse();
            BindList();
        }
    }
    #region Userdefined Functions 

    private void BindCourse()
    {
        ddlCourse.DataSource = objTrainingSchedule.GetCourse();
        ddlCourse.DataTextField = "Topic";
        ddlCourse.DataValueField = "CourseID";
        ddlCourse.DataBind();
    }

    private void BindList()
    {
       
        DataTable dt = null;
        if (rbtnDepartmentWise.Checked )
            dt = objTrainingSchedule.GetDepatments();
        else
            dt = objTrainingSchedule.GetEmployees(0);


        dlEmployeeList.DataSource = dt;
        dlEmployeeList.DataBind();

       
    }

    private bool  InsertUpdateSchedule()
    {
        int ScheduleID = 0;
        if(ViewState["ScheduleID"] != null)
            ScheduleID = Convert.ToInt32(ViewState["ScheduleID"]);

        objTrainingSchedule = new clsTrainingSchedule();
        if (ScheduleID > 0)
            objTrainingSchedule.ScheduleID = ScheduleID;
        objTrainingSchedule.ScheduleName = txtSchedule.Text;
        objTrainingSchedule.CourseId = Convert.ToInt32(ddlCourse.SelectedValue);
        objTrainingSchedule.Purpose = txtPurpose.Text;
        objTrainingSchedule.StartDate = txtStartDate.Text;
        objTrainingSchedule.EndDate = txtEndDate.Text;
        objTrainingSchedule.StartTime = txtStartTime.Value;
        objTrainingSchedule.EndTime = txtEndTime.Value;
        objTrainingSchedule.IsRegistration = rblIssued.SelectedValue == "1" ? true : false;
        objTrainingSchedule.Fee = txtCost.Text.ToDecimal();
        objTrainingSchedule.CompanyPercentage = txtCostPercentage.Text.ToInt32();
        objTrainingSchedule.Venue = txtVenue.Text;
        objTrainingSchedule.ScheduleType = chkScheduleType.SelectedValue.ToInt32();
        objTrainingSchedule.ScheduleById = new clsUserMaster().GetEmployeeId();
        ScheduleID = objTrainingSchedule.InsertSchedule();
        if (ScheduleID > 0)
        {
            InsertScheduleDetails(ScheduleID);

            return true ;
        }
        else
            return false ;


    }

    private void InsertScheduleDetails(int scheduleID)
    {
        objTrainingSchedule = new clsTrainingSchedule();

        for (int i = 0; i < dlEmployeeList.Items.Count; i++)
        {
            CheckBox chkScheduleDetails = dlEmployeeList.Items[i].FindControl("chkItem") as CheckBox;
            Label lblID = dlEmployeeList.Items[i].FindControl("lblID") as Label;
            if (chkScheduleDetails.Checked)
            {
                objTrainingSchedule.ScheduleID = scheduleID;
                objTrainingSchedule.EmployeeID = lblID.Text.ToInt64();
            }

        }
    }

    #endregion


    protected void rbtnDepartmentWise_CheckedChanged(object sender, EventArgs e)
    {
        BindList();
    }

  
    protected void rbtnEmployeeWise_CheckedChanged(object sender, EventArgs e)
    {
        BindList();
    }
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        InsertUpdateSchedule();
    }
    protected void rbtnDepartmentWise_CheckedChanged1(object sender, EventArgs e)
    {
       
    }
    protected void rbtnEmployeeWise_CheckedChanged1(object sender, EventArgs e)
    {

    }
    protected void rdbDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindList();
    }
}
