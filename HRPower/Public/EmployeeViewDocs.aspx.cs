﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_EmployeeViewDocs : System.Web.UI.Page
{
    clsUserMaster objUserMaster;
    clsEmployee objEmployee;
    clsEmployeeviewDocs objEmpViewDocs = new clsEmployeeviewDocs();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            objEmpViewDocs = new clsEmployeeviewDocs();
            objUserMaster = new clsUserMaster();
            objEmployee = new clsEmployee();

            objEmployee.EmployeeID = objUserMaster.GetEmployeeId();

            BindCombos();
            if (Request.QueryString["EmployeeID"] == null)
            {
            }
            else
            {
                ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Request.QueryString["EmployeeID"].ToString()));
            }
            Bind();
        }
    }

    private void BindCombos()
    {
        ddlEmployee.DataSource = clsEmployeeNew.GetEmplooyeesHavingDocuments();   //objEmpViewDocs.FillEmployees();
        ddlEmployee.DataBind();

        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem("Select", "0"));

        upEmployee.Update();

    }

    private void Bind()
    {
        if (ddlEmployee.Items.Count > 0)
        {
            if (Convert.ToInt32(ddlEmployee.SelectedValue) > 0)
            {
                objEmpViewDocs = new clsEmployeeviewDocs();

                objEmpViewDocs.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
                tvEmployeeDocs.Nodes.Clear();

                DataTable dt = objEmpViewDocs.GetAllTreeParents();
                PopulateNodes(dt, tvEmployeeDocs.Nodes);
                tvEmployeeDocs.ExpandAll();
                if (tvEmployeeDocs.Nodes.Count > 0)
                {
                    if (tvEmployeeDocs.Nodes[0].ChildNodes.Count > 0)
                    {
                        tvEmployeeDocs.Nodes[0].ChildNodes[0].Selected = true;
                        LoadEmpScandocs();
                    }
                }
                else
                    ifDocument.Visible = false;
            }
        }

    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dw in dt.Rows)
        {
            TreeNode tn = new TreeNode();

            tn.Text = dw["Description"].ToString();
            tn.Value = dw["Node"].ToString();

            nodes.Add(tn);

            tn.PopulateOnDemand = ((int)(dw["childnodecount"]) > 0);
        }
    }
    protected void tvEmployeeDocs_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        Populate(Convert.ToInt32(e.Node.Value), e.Node);
    }

    private void Populate(int iParentID, TreeNode parentNode)
    {
        objEmpViewDocs = new clsEmployeeviewDocs();

        objEmpViewDocs.ParentId = iParentID;

        DataTable dt = objEmpViewDocs.GetAllNodes();

        PopulateNodes(dt, parentNode.ChildNodes);
    }

    private void LoadEmpScandocs()
    {
        string sDoctypename = string.Empty;
        string sFilename = string.Empty;
        int iDoctypeID = 0;
        int iNode = Convert.ToInt32(tvEmployeeDocs.SelectedNode.Value);
        objEmpViewDocs = new clsEmployeeviewDocs();
        objEmpViewDocs.ParentId = iNode;
        DataTable dt = objEmpViewDocs.GetDocumentDetails();
        if (dt.Rows.Count > 0)
        {
            ifDocument.Attributes.Add("src", "");
            string sRootpath = string.Empty;
            sDoctypename = Convert.ToString(dt.Rows[0]["Filetypename"]);
            sFilename = Convert.ToString(dt.Rows[0]["filename"]);
            iDoctypeID = Convert.ToInt32(dt.Rows[0]["DocumentTypeID"]);

            if (iDoctypeID > 13) sDoctypename = "Other Documents";
            if (sFilename == string.Empty)
                ifDocument.Visible = false;
            else
            {
                ifDocument.Visible = true;
                string sVirpath = ConfigurationManager.AppSettings["_VIRP_PATH"];
                sRootpath = sVirpath + sDoctypename + "/";
                ifDocument.Attributes.Add("src", sRootpath + sFilename);
                
            }

        }

    }

    protected void tvEmployeeDocs_SelectedNodeChanged(object sender, EventArgs e)
    {
        if(tvEmployeeDocs.SelectedNode != null && tvEmployeeDocs.SelectedNode.Parent != null)
            LoadEmpScandocs();
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEmployee.Items.Count > 0)
        {
            Bind();
        }
    }
}
