﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true" CodeFile="HomeAlerts.aspx.cs" Inherits="Public_HomeAlerts"  Title='<%$Resources:Requests,Alerts%>' %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="pageheader" Runat="Server">
  <%--<div id='cssmenu'>
        <ul>
            <li ><a href='ManagerHomeNew.aspx'>DASH BOARD </a></li>
             <li ><a href='HomeRecentActivity.aspx'><span>RECENT ACTIVITY </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>REQUESTS </span></a></li>
        <li ><a href='HomeActiveVacancies.aspx'><span>ACTIVE VACANCIES </span></a></li>
            <li ><a href='HomeNewHires.aspx'><span>NEW HIRES </span></a></li>
          <li class="selected"><a href='HomeAlerts.aspx'><span>ALERTS </span></a></li>
        </ul>
    </div>--%>
       <div id='cssmenu'>
        <ul>
          
            <li ><a href='ManagerHomeNew.aspx'>
                <asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DashBoard%>'>
            
                </asp:Literal>
            </a></li>
            <li><a href='HomeRecentActivity.aspx'><span>
                <asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,RecentActivity%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomePendingRequests.aspx'><span>
                <asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,Requests%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeActiveVacancies.aspx'><span>
                <asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,ActiveVacancies%>'></asp:Literal>
            </span></a></li>
            <li><a href='HomeNewHires.aspx'><span>
                <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NewHires%>'></asp:Literal>
            </span></a></li>
            <li class="selected"><a href='HomeAlerts.aspx'><span>
                <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,Alerts%>'></asp:Literal>
            </span></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="public_content" Runat="Server">
 <div id="dashimg">
        <div style="width: 50px; float: left;">
            <img src="../images/alerts.png" />
        </div>
        <div id="dashboardh5" style="color: #30a5d6">
            <b>
           <%-- Alerts --%><asp:Literal ID= "lit1" runat ="server" Text ='<%$Resources:Requests,Alerts%>' ></asp:Literal>
            </b>
        </div>
    </div>
     <div style="float: left; width: 99%;">
            <asp:UpdatePanel ID="updAlertPopup" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                  <table style="width: 100%;">
                                    <tr>
                                        <td width="100%">
                                            <asp:UpdatePanel ID="upnlPopup" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                               
                                                    <div style="width: 100%; float: left;padding-left:137px;padding-top: 4px; font-size: 14px;">
                                                        <div style="float: left; width: 20%">
                                                            <asp:RadioButton runat="server" GroupName="Status" AutoPostBack="true" ID="rdbExpiryAlert"
                                                                Text='<%$Resources:Requests,Expiry%>' ForeColor="DarkBlue" OnCheckedChanged="rdbExpiryAlert_CheckedChanged" />
                                                            <asp:RadioButton runat="server" GroupName="Status" ID="rdbReturnAlert" AutoPostBack="true"
                                                                 Text='<%$Resources:Requests,Return%>'  ForeColor="DarkBlue" OnCheckedChanged="rdbReturnAlert_CheckedChanged" />
                                                        </div>
                                                        <div style="width: 78%; float: right; padding-top: 4px">
                                                            <asp:DropDownList ID="ddlDocType" runat="server" Width="200px" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:HiddenField ID="hfdIsExpiry" runat="server" />
                                                            <asp:TextBox runat="server" ID="txtfrom" Width="85px" AutoPostBack="true" Style="margin-left: 10px;"
                                                                OnTextChanged="txtfrom_TextChanged">
                                                            </asp:TextBox>
                                                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="twm" runat="server" TargetControlID="txtfrom"
                                                                WatermarkText='<%$Resources:Requests,Fromdate%>' WatermarkCssClass="WmCss">
                                                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                                                            <AjaxControlToolkit:CalendarExtender ID="extenderLoanDate" runat="server" TargetControlID="txtfrom"
                                                                PopupButtonID="txtfrom" Format="dd/MM/yyyy">
                                                            </AjaxControlToolkit:CalendarExtender>
                                                            <asp:TextBox runat="server" ID="txtTo" Width="85px" AutoPostBack="true" Style="margin-left: 10px;"
                                                                OnTextChanged="txtTo_TextChanged">
                                                            </asp:TextBox>
                                                            <AjaxControlToolkit:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                                                TargetControlID="txtTo" WatermarkText='<%$Resources:Requests,Todate%>' WatermarkCssClass="WmCss">
                                                            </AjaxControlToolkit:TextBoxWatermarkExtender>
                                                            <AjaxControlToolkit:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTo"
                                                                PopupButtonID="txtTo" Format="dd/MM/yyyy">
                                                            </AjaxControlToolkit:CalendarExtender>
                                                        </div>
                                                    </div>
                                                
                                                   <div style="width: 100%; float: left;padding-top:4px ; max-height: 300px; overflow: auto;">
                                                    <asp:DataList ID="dlAlerts" runat="server" BorderWidth="0px" CellPadding="0"
                                                CellSpacing="0" Width="98%" onitemdatabound="dlAlerts_ItemDataBound" >
                                                        <HeaderTemplate>
                                                            <table width="98%" cellpadding="0" cellspacing="0" class="datalistheader">
                                                                <tr>
                                                                    <td height="30" width="30" valign="top" align="left">
                                                                      <asp:CheckBox ID="chk_All" runat="server" onclick="selectHeaderAll(this.id,'chkAlert')" />
                                                                    </td>
                                                                    <td style="color: rgb(9, 101, 153);" align ="left" >
                                                                       <asp:Literal ID="lit1" runat ="server" Text ='<%$Resources:Requests,Message%>'></asp:Literal>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </HeaderTemplate>
                                               
                                                <ItemTemplate>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr style="font-size:12px"; color:#6a8bab;" >
                                                            <td  width="30" style="vertical-align: bottom;">
                                                                 <asp:CheckBox ID="chkAlert" runat="server" />
                                                                 <asp:HiddenField ID="hfdDocTypeID" runat ="server" Value ='<%# Eval("DocumentTypeID")%>' />
                                                                 <asp:HiddenField ID="hfdCommonID" runat ="server" Value ='<%# Eval("CommonID")%>' />
                                                            </td>
                                                            <td  style="padding-top:8px;color: rgb(9, 101, 153)"; align ="left" >
                                                                <%# Eval("Message")%>
                                                              
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="imgConfirm" runat="server" Visible="false" ToolTip='<%$Resources:ControlsCommon,ConfirmProbation%>' ImageUrl="~/images/confirm1_20x20.png"/>
                                                           
                                                             </td>
                                                           
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                      
                                            </asp:DataList>
                                                  </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            
                                        </td>
                                    </tr>
                      <tr>
                          <td align="right" style="margin-right: 350px;">
                              <asp:UpdatePanel ID="upnlAddEmployee" runat="server" UpdateMode="Conditional">
                                  <ContentTemplate>
                                      <asp:Button ID="btnAddEmployee" runat="server" ValidationGroup="AddPhase" OnClick="btnAddEmployee_Click" Text='<%$Resources:ControlsCommon,DoNotNotifyAgain%>'
                                          CssClass="btnsubmit" ToolTip='<%$Resources:ControlsCommon,DoNotNotifyAgain%>' />
                                      <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" OnClick="btnCancel_Click" Text='<%$Resources:ControlsCommon,Cancel%>'
                                          ToolTip='<%$Resources:ControlsCommon,Cancel%>' />
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                          </td>
                      </tr>
                                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upMessage" runat="server" RenderMode="Inline">
            <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" CssClass="error" Style="display: none;"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="menu_content" Runat="Server">
 <div style="display: block; width: 100%;" id="divCalendar">
        <div style="display: block; width: 100%; padding: 5px 2px 2px 2px; margin-left: 1px;">
            <asp:UpdatePanel ID="upcalender" runat="server">
              <ContentTemplate>
                      <asp:Calendar ID="Calendar5" Width="98%" runat="server" CellPadding="1" 
                        NextMonthText="&amp;gt;&amp;gt;  " PrevMonthText="&amp;lt;&amp;lt;" BorderStyle="None">
                        <SelectedDayStyle ForeColor="Black" BorderColor="#0099FF" />
                        <TodayDayStyle CssClass="CalendarDay1" BorderColor="#CC0066" />
                        <DayStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <DayHeaderStyle CssClass="CalendarDay" />
                        <DayStyle CssClass="CalendarDay1" />
                        <SelectedDayStyle ForeColor="Red" />
                        <TitleStyle CssClass="CalendarDay1" />
                        <NextPrevStyle ForeColor="White" CssClass="CalendarNextPrev" />
                    </asp:Calendar>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="float: left; font-size: 10px; padding: 2px 2px 2px 2px; width: 100%;">
                        <img src="../images/green.png" />
                        <%--Event--%> <asp:Literal ID="Literal8" runat ="server" Text='<%$Resources:ControlsCommon,Event%>'></asp:Literal>
                        <img src="../images/red.PNG" />
                        <%--Holiday--%><asp:Literal ID="Literal7" runat ="server" Text='<%$Resources:ControlsCommon,Holiday%>'></asp:Literal>
                        <img src="../images/golden.png" />
                        <%--Both--%> <asp:Literal ID="Literal9" runat ="server" Text='<%$Resources:ControlsCommon,Both%>'></asp:Literal>
                    </div>
    </div>
</asp:Content>

