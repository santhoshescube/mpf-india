﻿<%@ Page Language="C#" MasterPageFile="~/Master/PublicmasterPage.master" AutoEventWireup="true"
    CodeFile="EmiratesHealthLabourCard.aspx.cs" Inherits="Public_EmiratesHealthLabourCard"
    Title="Card" %>

<%@ Register Src="../Controls/DocumentReceiptIssue.ascx" TagName="DocumentReceiptIssue"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="pageheader" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />
   <div id='cssmenu'>
        <ul>
            <li ><a href="Passport.aspx"><asp:Literal ID="Literal90" runat="server" Text='<%$Resources:MasterPageCommon,Passport%>'></asp:Literal></a></li>
            <li><a href="Visa.aspx"><asp:Literal ID="Literal12" runat="server" Text='<%$Resources:MasterPageCommon,Visa%>'></asp:Literal></a></li>
            <li><a href="DrivingLicence.aspx"><asp:Literal ID="Literal1" runat="server" Text='<%$Resources:MasterPageCommon,DrivingLicense%>'></asp:Literal></a></li>
            <li><a href="Qualification.aspx"><asp:Literal ID="Literal2" runat="server" Text='<%$Resources:MasterPageCommon,Qualification%>'></asp:Literal></a></li>
            <li id="liHealth" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=1"><asp:Literal ID="Literal3" runat="server" Text='<%$Resources:MasterPageCommon,HealthCard%>'></asp:Literal></a></li>
            <li id="liLabour" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=2"><asp:Literal ID="Literal4" runat="server" Text='<%$Resources:MasterPageCommon,LabourCard%>'></asp:Literal></a></li>
            <li id="liEmirates" runat="server"><a href="EmiratesHealthLabourCard.aspx?typeid=3"><asp:Literal ID="Literal5" runat="server" Text='<%$Resources:MasterPageCommon,NationalIDCard%>'></asp:Literal></a></li>
            <li><a href="InsuranceCard.aspx"><asp:Literal ID="Literal6" runat="server" Text='<%$Resources:MasterPageCommon,InsuranceCard%>'></asp:Literal></a></li>
            <li><a href="TradeLicense.aspx"><asp:Literal ID="Literal7" runat="server" Text='<%$Resources:MasterPageCommon,TradeLicense%>'></asp:Literal></a></li>
            <li><a href="LeaseAgreement.aspx"><asp:Literal ID="Literal8" runat="server" Text='<%$Resources:MasterPageCommon,LeaseAgreement%>'></asp:Literal></a></li>
            <li><a href="Insurance.aspx"><asp:Literal ID="Literal9" runat="server" Text='<%$Resources:MasterPageCommon,Insurance%>'></asp:Literal></a></li>
            <li><a href="Documents.aspx"><asp:Literal ID="Literal10" runat="server" Text='<%$Resources:MasterPageCommon,OtherDocuments%>'></asp:Literal></a></li>
            <li><a href="ViewDocument.aspx"><asp:Literal ID="Literal11" runat="server" Text='<%$Resources:MasterPageCommon,ViewDocuments%>'></asp:Literal></a></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <div>
        <div>
            <div style="display: none">
                <asp:Button ID="btnProxy" runat="server" Text="submit" />
            </div>
            <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                BorderStyle="None">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <uc:Message ID="mcMessage" runat="server" ModalPopupId="mpeMessage" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnProxy"
                PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                Drag="true">
            </AjaxControlToolkit:ModalPopupExtender>
        </div>
        <asp:UpdatePanel ID="upnlCreateCard" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="width: 100%; display: block" id="divAddNew" runat="server">
                    <div class="trLeft" style="float: left; width: 24%; height: 29px">
                       <asp:Literal ID="Literal14" runat ="server" Text='<%$Resources:DocumentsCommon,Employee%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 29px">
                        <asp:DropDownList ID="ddlEmployee" runat="server" CssClass="dropdown_mandatory" Width="190px"
                            DataTextField="Employee" DataValueField="EmployeeID">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvEmployee" runat="server" ControlToValidate="ddlEmployee"
                           Display="Dynamic" ErrorMessage="*"
                            SetFocusOnError="False" ValidationGroup="submit" InitialValue="0"></asp:RequiredFieldValidator>
                    </div>
                    <div class="trLeft" style="float: left; width: 24%; height: 29px">
                         <asp:Literal ID="Literal13" runat ="server" Text='<%$Resources:DocumentsCommon,CardNumber%>'></asp:Literal> 
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 29px">
                        <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textbox_mandatory" MaxLength="25"
                            Width="182px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvCardNumber" runat="server" ControlToValidate="txtCardNumber"
                          Display="Dynamic" ErrorMessage="*" SetFocusOnError="False"
                            ValidationGroup="submit"></asp:RequiredFieldValidator>
                        <asp:HiddenField ID="hfCardID" runat="server" />
                    </div>
                    <div class="trLeft" style="float: left; width: 24%; height: 29px">
                       <asp:Literal ID="Literal15" runat ="server" meta:resourcekey="PersonalNumber"></asp:Literal>   
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 29px">
                        <asp:TextBox ID="txtPersonalNumber" runat="server" CssClass="textbox" MaxLength="25"
                            Width="182px"></asp:TextBox>
                    </div>
                    <div class="trLeft" style="float: left; width: 24%; height: 29px">
                       <asp:Literal ID="Literal19" runat ="server" Text='<%$Resources:DocumentsCommon,IssueDate%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 29px">
                        <asp:TextBox ID="txtIssuedate" runat="server" CssClass="textbox" MaxLength="10" Width="100px"
                            OnTextChanged="txtIssueDate_TextChanged"></asp:TextBox>&nbsp
                        <asp:ImageButton ID="ibtnIssueDate" runat="server" CausesValidation="False" style="margin-top:6px;" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <AjaxControlToolkit:CalendarExtender ID="ceIssueDate" runat="server" Format="dd/MM/yyyy"
                            PopupButtonID="ibtnIssueDate" TargetControlID="txtIssuedate" />
                        <asp:CustomValidator ID="cvIssueDate" runat="server" ControlToValidate="txtIssuedate"
                            ClientValidationFunction="validatepassportIssuedate" CssClass="error" ValidateEmptyText="True"
                            ValidationGroup="Submit" Display="Dynamic"></asp:CustomValidator>
                    </div>
                    <div class="trLeft" style="float: left; width: 24%; height: 29px">
                       <asp:Literal ID="Literal16" runat ="server" Text='<%$Resources:DocumentsCommon,ExpiryDate%>'></asp:Literal>   
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 29px">
                        <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textbox" MaxLength="10"
                            Width="100px"></asp:TextBox>&nbsp
                        <asp:ImageButton ID="ibtnExpiryDate" runat="server" CausesValidation="False" style="margin-top:6px;" ImageUrl="~/images/Calendar_scheduleHS.png" />
                        <AjaxControlToolkit:CalendarExtender ID="ceExpiryDate" runat="server" Format="dd/MM/yyyy"
                            PopupButtonID="ibtnExpiryDate" TargetControlID="txtExpiryDate" />
                        <asp:CustomValidator ID="cvExpiryDate" runat="server" ControlToValidate="txtExpiryDate"
                            CssClass="error" ValidateEmptyText="True" ValidationGroup="Submit" ClientValidationFunction="ValidateOtherDocExpiryDate"
                            Display="Dynamic"></asp:CustomValidator>
                    </div>
                    
                    <div class="trLeft" style="float: left; width: 24%; height: 60px">
                        <asp:Literal ID="Literal17" runat ="server" Text='<%$Resources:DocumentsCommon,OtherInfo%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: left; width: 73%; height: 60px">
                        <asp:TextBox ID="txtOtherInfo" runat="server" CssClass="textbox" MaxLength="200"
                            Width="300px" TextMode="MultiLine" Height="76px" onchange="RestrictMulilineLength(this, 200);"
                            onkeyup="RestrictMulilineLength(this, 200);"></asp:TextBox>
                    </div>
                    <div class="trLeft" style="float: left; width: 20%; height: 29px; padding-top: 20px">
                       <asp:Literal ID="Literal18" runat ="server" Text='<%$Resources:DocumentsCommon,CurrentStatus%>'></asp:Literal>    
                    </div>
                    <div class="trRight" style="float: right; width: 73%; height: 29px; padding-top: 20px">
                        <asp:Label ID="lblCurrentStatus" runat="server" Text="New" Width="30%"></asp:Label>
                    </div>
                    <div class="trLeft" style="float: left; width: 95%; height: 100px; margin-top: 5px;">
                        <div id="divParse">
                            <fieldset style="padding: 3px 1px;">
                                <legend><asp:Literal ID="Literal20" runat ="server" Text='<%$Resources:DocumentsCommon,AddDocuments%>'></asp:Literal>    </legend>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding-left: 10px">
                                    <tr>
                                        <td valign="top">
                                           <asp:Literal ID="Literal21" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>    
                                        </td>
                                        <td valign="top" style="width: 3px">
                                        </td>
                                        <td valign="top">
                                            <asp:Literal ID="Literal22" runat ="server" Text='<%$Resources:DocumentsCommon,Choosefile%>'></asp:Literal>    
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" width="175px">
                                            <asp:TextBox ID="txtDocname" runat="server" CssClass="textbox_mandatory" Width="150px"
                                                MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDocname" runat="server" ControlToValidate="txtDocname"
                                                Display="Dynamic" ErrorMessage="*"
                                                ValidationGroup="AddDocs"></asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvotherdocnameduplicate" runat="server" ClientValidationFunction="validateotherdocnameduplicate"
                                                ControlToValidate="txtDocname" ValidationGroup="AddDocs" Display="Dynamic" ErrorMessage='<%$Resources:DocumentsCommon,Duplicatedocumentname%>'></asp:CustomValidator>
                                        </td>
                                        <td style="width: 2px">
                                        </td>
                                        <td valign="top" width="250px">
                                            <AjaxControlToolkit:AsyncFileUpload ID="fuDocument" runat="server" PersistFile="True"
                                                Width="250px" Visible="true" CompleteBackColor="White" ErrorBackColor="White"
                                                OnUploadedComplete="fuDocument_UploadedComplete" />
                                            <asp:CustomValidator ID="cvotherdocumentAttachment" runat="server" ErrorMessage='<%$Resources:DocumentsCommon,Pleaseattachfile%>'
                                                CssClass="error" ValidateEmptyText="True" ValidationGroup="AddDocs" ClientValidationFunction="validateotherdocumentAttachment"></asp:CustomValidator>
                                        </td>
                                        <td valign="top" width="70px">
                                            <asp:LinkButton ID="btnattachpassdoc" Text='<%$Resources:DocumentsCommon,Addtolist%>' Enabled="true" runat="server"
                                                ValidationGroup="AddDocs" OnClick="btnattachpassdoc_Click"> </asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </div>
                    </div>
                    <div class="trLeft" 
                        style="float: left; width: 95%; height: 100px; margin-top: 20px;">
                        <div id="divOtherDoc" runat="server" class="container_content" style="display: none;
                            height: 150px; overflow: auto;">
                            <asp:UpdatePanel ID="updOtherDoc" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:DataList ID="dlOtherDoc" runat="server" Width="100%" DataKeyField="Node" GridLines="Horizontal"
                                        OnItemCommand="dlOtherDoc_ItemCommand">
                                        <HeaderStyle CssClass="datalistheader" />
                                        <HeaderTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td width="241">
                                                         <asp:Literal ID="Literal24" runat ="server" Text='<%$Resources:DocumentsCommon,DocumentName%>'></asp:Literal>
                                                    </td>
                                                    <td width="110" align="left">
                                                       <asp:Literal ID="Literal23" runat ="server" Text='<%$Resources:DocumentsCommon,FileName%>'></asp:Literal>
                                                    </td>
                                                    <td width="20" style="float: right;">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td width="150">
                                                        <asp:Label ID="lblDocname" runat="server" Text='<%# Eval("Docname")%>' Width="100%"></asp:Label>
                                                    </td>
                                                    <td width="130" align="left">
                                                        <asp:Label ID="lblFilename" runat="server" Text='<%# Eval("Filename")%>' Width="100%"></asp:Label>
                                                    </td>
                                                    <td width="20" align="left">
                                                        <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/images/Delete_Icon_Datalist.png"
                                                            CommandArgument='<% # Eval("Node") %>' CommandName="REMOVE_ATTACHMENT" ToolTip='<%$Resources:ControlsCommon,Delete%>'/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:DataList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="trRight" style="float: right; height: 29px; text-align: right">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Submit%>' ToolTip='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="submit"
                                Width="75px" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" Text='<%$Resources:ControlsCommon,Cancel%>' ToolTip='<%$Resources:ControlsCommon,Cancel%>' CausesValidation="False"
                                Width="75px" OnClick="btnCancel_Click" />
                        </div>
                    </div>
                </div>                            
                <div id="divDataList" runat="server" style="display: none">
                    <asp:DataList ID="dlCards" runat="server" BorderWidth="0px" CellPadding="0" Width="100%" CssClass="labeltext"
                        OnItemCommand="dlCards_ItemCommand" DataKeyField="CardID" OnItemDataBound = "dlCards_ItemBound">
                        <ItemStyle CssClass="item" />
                        <HeaderStyle CssClass="listItem" />
                        <HeaderTemplate>
                            <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 5px">
                                <tr>
                                    <td height="30" width="30">
                                        <asp:CheckBox ID="chkAll" runat="server" onclick="selectAll(this.id, 'chkItem');" />
                                    </td>
                                    <td class="trLeft">
                                         <asp:Literal ID="Literal26" runat ="server"  Text='<%$Resources:ControlsCommon,SelectAll%>' ></asp:Literal>  
                                    </td>
                                </tr>
                            </table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <table cellpadding="0" cellspacing="0" style="width: 100%; padding-left: 5px" id="tblDetails"
                                runat="server">
                                <tr>
                                    <td valign="top" width="30" style="padding-top: 4px">
                                        <asp:CheckBox ID="chkItem" runat="server" />
                                    </td>
                                    <td valign="top">
                                        <table style="width: 100%" border="0" cellpadding="3" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table style="width: 100%;" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                            <td>
                                                                <asp:LinkButton ID="lnkCard" CausesValidation="false" runat="server" CommandArgument='<%# Eval("CardID")%>'
                                                                    Text='<%# string.Format("{0} {1}", Eval("SalutationName"), Eval("Employee")) %>' CssClass="listHeader bold" CommandName="VIEW"></asp:LinkButton>
                                                                    [<%# Eval("EmployeeNumber")%>] &nbsp  <asp:Literal ID="Literal13" runat ="server" Text='<%$Resources:DocumentsCommon,CardNumber%>'></asp:Literal>  - <%# Eval("CardNumber") %>
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td valign="top" align="right">
                                                                <asp:ImageButton ID="btnEdit" runat="server" CausesValidation="False" Style="border-style: none;
                                                                    display: block;" CommandArgument='<%# Eval("CardID")%>' CommandName="EDIT" ImageUrl="~/images/edit.png"  ToolTip='<%$Resources:ControlsCommon,Edit%>' ></asp:ImageButton>
                                                            </td>
                                                            <td style="visibility: hidden">
                                                                <asp:Label ID="lblIsDelete" runat="server" Text='<%# Eval("IsDelete")%>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left: 20px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="innerdivheader">
                                                               <asp:Literal ID="Literal26" runat ="server"  Text='<%$Resources:DocumentsCommon,IssueDate%>' ></asp:Literal> 
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("IssueDate")%>&nbsp;
                                                            </td>
                                                            <td style="color: #FF0000; font-weight: bold; float: right;">
                                                                <%# Eval("IsRenewed")%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="innerdivheader">
                                                                <asp:Literal ID="Literal25" runat ="server"  Text='<%$Resources:DocumentsCommon,ExpiryDate%>' ></asp:Literal> 
                                                            </td>
                                                            <td>
                                                                :
                                                            </td>
                                                            <td>
                                                                <%# Eval("ExpiryDate")%>&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
                <div id="divViewCardDetails" style="width: 100%; margin-left: 20px; display: none"
                    runat="server">
                    <div style="width: 100%; float: left;padding-top:4px; height: 30px;font-weight:bold" id="divHeading" runat="server">
                        <h4>
                             <asp:Literal ID="Literal32" runat ="server" meta:resourcekey="EmployeeCardDetails"></asp:Literal>  </h4>
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin: 5px">
                        <asp:Literal ID="Literal25" runat ="server"  Text='<%$Resources:DocumentsCommon,Employee%>' ></asp:Literal>
                    </div>
                    <div class="innerdivheaderValue" style="width: 73%; float: left; margin: 5px; height: 20px;"
                        id="divEmployee" runat="server">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; margin: 5px; height: 20px;">
                        <asp:Literal ID="Literal27" runat ="server"  Text='<%$Resources:DocumentsCommon,CardNumber%>' ></asp:Literal>
                    </div>
                    <div class="innerdivheaderValue" style="width: 73%; float: left; margin: 5px; height: 20px;"
                        id="divCardNumber" runat="server">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; margin: 5px; height: 20px;">
                         <asp:Literal ID="Literal33" runat ="server" meta:resourcekey="PersonalNumber"></asp:Literal>  
                    </div>
                    <div class="innerdivheaderValue" style="width: 73%; float: left; margin: 5px; height: 20px;"
                        id="divPersonalNumber" runat="server">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin: 5px;">
                        <asp:Literal ID="Literal28" runat ="server"  Text='<%$Resources:DocumentsCommon,IssueDate%>' ></asp:Literal>
                    </div>
                    <div id="divIssueDate" runat="server" class="innerdivheaderValue" style="width: 73%;
                        float: left; margin: 5px; height: 20px;">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin: 5px;">
                         <asp:Literal ID="Literal29" runat ="server"  Text='<%$Resources:DocumentsCommon,ExpiryDate%>' ></asp:Literal>
                    </div>
                    <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin: 5px;"
                        id="divExpiryDate" runat="server">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin: 5px;"
                        id="divRenewStatus" runat="server">
                         <asp:Literal ID="Literal30" runat ="server"  Text='<%$Resources:DocumentsCommon,IsRenewed%>' ></asp:Literal>
                    </div>
                    <div class="innerdivheaderValue" style="width: 73%; float: left; height: 20px; margin: 5px;"
                        id="divRenew1" runat="server">
                    </div>
                    <div class="innerdivheader" style="width: 24%; float: left; height: 20px; margin: 5px;">
                         <asp:Literal ID="Literal31" runat ="server"  Text='<%$Resources:DocumentsCommon,OtherInfo%>' ></asp:Literal>
                    </div>
                    <div class="innerdivheaderValue" style="width: 70%; float: left; height: 20px; margin: 5px;
                        word-break: break-all" id="divRemarks" runat="server">
                    </div>
                    <div id="divIsDelete" runat="server" style="visibility: hidden"></div>
                </div>
                <div>
                    <uc:Pager ID="pgrCards" runat="server" OnFill="BindDatalist"></uc:Pager>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="text-align: center; margin-top: 10px;">
            <asp:UpdatePanel ID="upnlNoData" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblNoData" runat="server" CssClass="error" Visible="false"></asp:Label>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dlCards" EventName="ItemCommand" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <asp:UpdatePanel ID="upnlIssueReceipt" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="display: none">
                    <asp:Button ID="btnIssueReceipt" runat="server" />
                </div>
                <asp:Panel ID="pnlIssueReceipt" Style="display: none" runat="server">
                    <uc1:DocumentReceiptIssue ID="ucDocIssueReceipt" runat="server" ModalPopupID="mpeIssueReceipt">
                    </uc1:DocumentReceiptIssue>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeIssueReceipt" runat="server" TargetControlID="btnIssueReceipt"
                    PopupControlID="pnlIssueReceipt" BackgroundCssClass="modalBackground">
                </AjaxControlToolkit:ModalPopupExtender>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upPrint" runat="server">
            <ContentTemplate>
                <div id="divPrint" runat="server" style="display: none">
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="search" style="vertical-align: top">
                <div id="text">
                </div>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                <AjaxControlToolkit:TextBoxWatermarkExtender runat="server" TargetControlID="txtSearch"
                    WatermarkText='<%$Resources:DocumentsCommon,SearchBy%>' WatermarkCssClass="WmCss" />
                <br />
                <div id="searchimg">
                    <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="../images/search.png" CausesValidation="false"
                        OnClick="btnSearch_Click"></asp:ImageButton>
                </div>
            </div>
            <div id="sidebarsettings">
                <div class="sideboxImage">
                    <img id="imgAddDoc" src='<%=GetImageUrl("Add") %>'
                       />
                </div>
                <div class="name">
                    <h5><asp:LinkButton ID="lnkAddCards" runat="server" CausesValidation="false" OnClick="lnkAddCards_Click" meta:resourcekey="AddCard"> 
                         </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                        <img id="imgListDoc" src='<%=GetImageUrl("List") %>'
                       />
                  <%--  <asp:ImageButton ID="imgListDoc" ImageUrl="~/images/List_Otherdocument_Big.png" runat="server" />--%>
                </div>
                <div class="name">
                    <h5> <asp:LinkButton ID="lnkListCards" runat="server" meta:resourcekey="ViewCard" CausesValidation="false" OnClick="lnkListCards_Click">
                        </asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                </div>
                <div class="name">
                   <h5> <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" OnClick="lnkDelete_Click" Text='<%$Resources:ControlsCommon,Delete%>'>
                           
		</asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgPrint" ImageUrl="../images/printer.png" runat="server" />
                </div>
                <div class="name"><h5>
                    <asp:LinkButton ID="lnkPrint" runat="server" CausesValidation="false" OnClick="lnkPrint_Click" Text='<%$Resources:ControlsCommon,Print%>'> 
		</asp:LinkButton></h5>
                </div>
                <div class="sideboxImage">
                    <asp:ImageButton ID="imgEmail" ImageUrl="../images/E-Mail.png" runat="server" />
                </div>
                <div class="name"><h5>
                    <asp:LinkButton ID="lnkEmail" runat="server" CausesValidation="false" OnClick="lnkEmail_Click" Text='<%$Resources:ControlsCommon,Email%>'> 
		</asp:LinkButton></h5>
                </div>
                <div id="divIssueReceipt" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgDocIR" ImageUrl="~/images/document_reciept.png" runat="server" />
                    </div>
                    <div id="Div1" class="name" runat="server">
                        <h5> <asp:LinkButton ID="lnkDocumentIR" runat="server" OnClick="lnkDocumentIR_Click" Text='<%$Resources:DocumentsCommon,Receipt%>'>
                            
                                 </asp:LinkButton> </h5>
                    </div>
                </div>
                
                <div id="divRenew" runat="server">
                    <div class="sideboxImage">
                        <asp:ImageButton ID="imgRenew" ImageUrl="~/images/ShiftPolicy.png" runat="server" />
                    </div>
                    <div id="Div3" class="name" runat="server">
                      <h5>   <asp:LinkButton ID="lnkRenew" runat="server" OnClick="lnkRenew_Click" Text='<%$Resources:DocumentsCommon,Renew%>'>
                            
                                 </asp:LinkButton></h5> 
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
