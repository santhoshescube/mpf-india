﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="SalaryAdvanceRequest.aspx.cs" Inherits="Public_SalaryAdvanceRequest"
    meta:resourcekey="PageTitle" %>

<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">

    <script type="text/javascript">
        function printFunction() {
            var divContent = document.getElementById("<%=fvSalaryDetails.ClientID %>");
            link = "about:blank";
            var win = window.open(link, "_new");
            win.document.open();
            win.document.write(makepage(divContent.innerHTML));
            win.document.close();
        }

        function makepage(Src) {
            return "<html>\n" +
    "<head>\n" +
    "<title>Salary Advance Request</title>\n" +
    "<script>\n" +
    "function step1() {\n" +
    "setTimeout('step2()', 10);\n" +
    "}\n" +
    "function step2() {\n" +
    " window.print(); \n" +
    " window.close(); \n" +
    "}\n" +
    "</scr" + "ipt>\n" +
    "</head>\n" +
    "<body onLoad='step1()'>\n" +
    "<div style='width=100%, height=100%'>" + Src + "</div>\n" +
    "</body>\n" +
    "</html>\n";
        }
    </script>

    <div class="trLeft" style="float: right;">
        <asp:ImageButton ID="ibtnBack" runat="server" ImageUrl="~/images/back-button.png"
            Width="22px" meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" Visible="false" />
    </div>
    <table style="width: 100%" cellpadding="4" cellspacing="0">
        <tr>
            <td>
                <div style="display: none">
                    <asp:Button ID="btnsubmit" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="btnsubmit"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </td>
        </tr>
        <tr>
            <td width="100%">
                <asp:UpdatePanel ID="upForm" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FormView ID="fvSalaryDetails" runat="server" Width="100%" OnItemCommand="fvSalaryDetails_ItemCommand"
                            DataKeyNames="RequestId,RequestedTo,StatusId,Employee" OnDataBound="fvSalaryDetails_DataBound"
                            CssClass="labeltext">
                            <EditItemTemplate>
                                <div>
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Date"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtSalaryDate" runat="server" CssClass="textbox_mandatory" Width="100px"
                                                    Text='<%# Eval("RequestedDate") %>' AutoPostBack="true" OnTextChanged="SetValues"></asp:TextBox>
                                                <asp:HiddenField ID="hfLoanDate" runat="server" Value='<%# Eval("RequestedDate") %>' />
                                                <asp:ImageButton ID="ibtnSalaryDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png"
                                                    CssClass="imagebutton" style="margin-top:9px;"/>
                                                <AjaxControlToolkit:CalendarExtender ID="extenderSalaryDate" runat="server" TargetControlID="txtSalaryDate"
                                                    PopupButtonID="ibtnSalaryDate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:CustomValidator ID="cvEndDate" runat="server" ValidateEmptyText="true" ControlToValidate="txtSalaryDate"
                                                    Display="Dynamic" ClientValidationFunction="CheckRequestdate" ValidationGroup="Submit"
                                                    CssClass="error"></asp:CustomValidator>
                                            </td>
                                            <td rowspan="5" align="left" width="25%" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbRequestedAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbApprovedAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbLoanAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AdvanceLimit"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="lblAdvanceAmount" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                                (<asp:Label ID="lblCurrency" runat="server"></asp:Label>)
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtAmount" runat="server" CssClass="textbox_mandatory" MaxLength="12"
                                                    Width="100px" Text='<%# Eval("Amount") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvSlaryAccnt" runat="server" ControlToValidate="txtAmount"
                                                    Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"
                                                    meta:resourcekey="EnterAmount"></asp:RequiredFieldValidator>
                                                <asp:Label ID="lbl" runat="server" CssClass="error" Visible="false"></asp:Label>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAmountExtender" runat="server"
                                                    FilterType="Numbers,Custom" TargetControlID="txtAmount" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Width="80%" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvReason" runat="server" ControlToValidate="txtReason"
                                                    Display="Dynamic" meta:resourcekey="EnterReason" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td class="trLeft" width="25%">
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:DropDownList ID="ddlStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    DataValueField="StatusId">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="trLeft" width="25%">
                                                &nbsp;
                                            </td>
                                            <td class="trRight" width="50%" align="right">
                                                <asp:Button ID="btnSubmit" runat="server" ValidationGroup="Submit" Width="75px" CommandName="Add"
                                                    CssClass="btnsubmit" UseSubmitBehavior="false" CommandArgument='<%# Eval("RequestId") %>'
                                                    meta:resourcekey="Submit" />&nbsp;
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" meta:resourcekey="Cancel"
                                                    CommandName="CancelRequest" Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <div id="divPrintable" runat="server">
                                    <table cellpadding="5" cellspacing="0" width="100%" class="tablecss">
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="RequestedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="lbRequestedBY" runat="server" Text=' <%# Eval("RequestedBY") %>'></asp:Label>
                                            </td>
                                            <td class="trRight" width="10%" style="padding-left: 185px;">
                                                <%--<asp:ImageButton ID="imgBack" runat="server" ImageUrl="~/images/back-button.png"
                                                    meta:resourcekey="Backtopreviouspage" OnClick="imgback_Click" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Date"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Label ID="lblDate" runat="server" Text=' <%# Eval("Date") %>'></asp:Label>
                                            </td>
                                            <td rowspan="6" align="left" width="25%" valign="top">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbAdvanceLimit" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbRequestedAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbApprovedAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbLoanAmount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lbGrosssalary" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr runat="server">
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Amount"></asp:Literal>
                                                (<asp:Label ID="lblViewCurrency" runat="server"></asp:Label>)
                                            </td>
                                            <td class="trRight" width="50%" id="trAmount">
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                            </td>
                                            <td class="trRight" width="50%" id="trAdvAmount" style="display: none;">
                                                <asp:TextBox ID="txtAdvanceAmount" runat="server" Text='<%# Eval("Amount") %>' CssClass="textbox_mandatory"
                                                    MaxLength="18" Width="100px"></asp:TextBox>
                                                <AjaxControlToolkit:FilteredTextBoxExtender ID="ftxtAdvanceAmountExtender" runat="server"
                                                    FilterType="Numbers,Custom" TargetControlID="txtAdvanceAmount" ValidChars=".">
                                                </AjaxControlToolkit:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="rfvAdvanceAmount" runat="server" ControlToValidate="txtAdvanceAmount"
                                                    Display="Dynamic" meta:resourcekey="EnterAmount" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="ApproveRequest"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="trappAmount" runat="server" style="display: none">
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ApprovedAmount"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%" style="word-break: break-all">
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("ApprovedAmount") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <%-- <tr>
                                            <td class="innerdivheader" width="25%">
                                                  <asp:Literal ID="Literal4" runat ="server" Text='<%$Resources:ControlsCommon,Remarks%>'></asp:Literal>  
                                            </td>
                                            <td class="trRight" width="50%" style="word-break: break-all">
                                                <asp:Label ID="lblReason" runat="server" Text='<%# Eval("Reason") %>'>
                                                </asp:Label>
                                                <asp:TextBox ID="txtEditReason" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                  Visible="false"  Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                            </td>
                                        </tr>--%>
                                        <tr runat="server" style="display: none;" id="trAccDate">
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="AccountDate"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtAccDate" runat="server" Width="100px" Text='<%# Eval("RequestedDate") %>'
                                                    AutoPostBack="true"></asp:TextBox>
                                                <asp:ImageButton ID="ibtnAccDate" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" style="margin-top: 10px;" />
                                                <AjaxControlToolkit:CalendarExtender ID="extenderSalaryDate" runat="server" TargetControlID="txtAccDate"
                                                    PopupButtonID="ibtnAccDate" Format="dd/MM/yyyy">
                                                </AjaxControlToolkit:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAccDate"
                                                    Display="Dynamic" meta:resourcekey="EnterAccountdate" SetFocusOnError="True"
                                                    CssClass="error" ValidationGroup="ApproveRequest"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Status"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:DropDownList ID="ddlEditStatus" CssClass="dropdownlist" runat="server" DataTextField="Description"
                                                    Visible="false" DataValueField="StatusId">
                                                </asp:DropDownList>
                                                <asp:Label ID="lblStatus" runat="server" Text=' <%# Eval("Status") %>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr id="trForwardedBy" runat="server" visible='<%# GetVisibility(Eval("ForwardedBy"))%>'>
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal10" runat="server" meta:resourcekey="ForwardedBy"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <%# Eval("ForwardedBy")%>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trRemarks">
                                            <td class="innerdivheader" width="25%">
                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Remarks"></asp:Literal>
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="textbox_mandatory" Text='<%# Eval("Reason") %>'
                                                    Width="250px" Height="50px" TextMode="MultiLine" onchange="RestrictMulilineLength(this, 500);"
                                                    onkeyup="RestrictMulilineLength(this, 500);"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                    Display="Dynamic" meta:resourcekey="EnterRemarks" SetFocusOnError="True" CssClass="error"
                                                    ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trlink">
                                            <td class="trLeft" width="50%" colspan="2">
                                                <asp:LinkButton ID="lbReasons" meta:resourcekey="ClicktoViewApprovalDetails" runat="server"
                                                    OnClick="lbReason_OnClick"></asp:LinkButton>
                                                <asp:HiddenField ID="hfLink" runat="server" Value="0" />
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trReasons">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <asp:GridView ID="gvReasons" runat="server" AutoGenerateColumns="false" BorderColor="#307296"
                                                    Width="600px">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="EmployeeFullName" meta:resourcekey="Authority" />
                                                        <asp:BoundField DataField="Reason" meta:resourcekey="ApprovalDetails" ItemStyle-Wrap="true"
                                                            ItemStyle-Width="300px" />
                                                        <asp:BoundField DataField="Description" meta:resourcekey="Status1" />
                                                        <asp:BoundField DataField="EntryDate" meta:resourcekey="Date1" />
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr runat="server" style="display: none;" id="trAuthority">
                                            <td class="trLeft" width="75%" colspan="2">
                                                <div class="trLeft" style="width: 24.5%; height: 30px; float: left;">
                                                    <asp:Label ID="lbl1" runat="server" Text='<%$Resources:ControlsCommon,ApprovalHierarchy %>'></asp:Label></div>
                                                <div id="ddl" class="trRight" style="width: 70%; height: auto; float: left; overflow-y: scroll;
                                                    overflow-x: hidden;" runat="server">
                                                    <asp:DataList ID="dl1" runat="server" RepeatColumns="5" RepeatDirection="Horizontal">
                                                        <ItemStyle ForeColor="#307296" Font-Bold="true" />
                                                        <ItemTemplate>
                                                            <asp:Table Width="99%">
                                                                <tr style="width: 99%; color: #307296;">
                                                                    <td>
                                                                        <asp:Label ID="lbl2" runat="server" Text='<%# Eval("symbol") %>'></asp:Label>
                                                                        <asp:Label ID="lbl" runat="server" Text='<%# Eval("EmployeeFullName") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </asp:Table>
                                                        </ItemTemplate>
                                                    </asp:DataList></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="innerdivheader" width="25%">
                                            </td>
                                            <td class="trRight" width="50%">
                                                <asp:Button ID="btnApprove" runat="server" CssClass="btnsubmit" meta:resourcekey="Submit"
                                                    CommandArgument=' <%# Eval("StatusId") %>' CommandName="Approve" Visible="false"
                                                    ValidationGroup="ApproveRequest" Width="75px" OnClick="btnApprove_Click" />
                                                <asp:Button ID="btCancel" runat="server" CssClass="btnsubmit" CommandName="Cancel"
                                                    meta:resourcekey="Cancel" Visible="false" Width="75px" />
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btnsubmit" CommandArgument=' <%# Eval("StatusId") %>'
                                                    meta:resourcekey="Submit" Width="75px" CommandName="RequestCancel" Visible="false" />
                                                <asp:Button ID="btnRequestForCancel" runat="server" CssClass="btnsubmit" CommandArgument='<%# Eval("StatusId") %>'
                                                    CommandName="RequestForCancel" Visible="false" meta:resourcekey="Submit" Width="75px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvSalaryDetails" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="dlSalaryRequest" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upnlDatalist" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DataList ID="dlSalaryRequest" runat="server" Width="100%" DataKeyField="RequestId"
                            OnItemCommand="dlSalaryRequest_ItemCommand" OnItemDataBound="dlSalaryRequest_ItemDataBound"
                            CssClass="labeltext">
                            <ItemStyle CssClass="item" />
                            <HeaderStyle CssClass="listItem" />
                            <HeaderTemplate>
                                <table width="100%">
                                    <tr>
                                        <td width="25" valign="top" style="padding-left: 5px">
                                            <asp:CheckBox ID="chk_All" runat="server" onclick="selectAll(this.id,'chkAdvance')" />
                                        </td>
                                        <td style="padding-left: 7px">
                                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="SelectAll"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStatusId" Text='<%# Eval("StatusId")%>' Visible="false" runat="server"></asp:TextBox>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tblDetails" runat="server"
                                    onmouseover="showCancel(this);" onmouseout="hideCancel(this);">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                <tr>
                                                    <td valign="top" rowspan="5" width="25" style="padding-left: 7px">
                                                        <asp:CheckBox ID="chkAdvance" runat="server" />
                                                    </td>
                                                    <td width="100%">
                                                        <table width="100%" border="0" cellpadding="3" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:LinkButton ID="lnkAdvance" runat="server" CssClass="listheader bold" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_View" CausesValidation="False">
                                                                             <%# Eval("Date")%>  </asp:LinkButton>
                                                                </td>
                                                                <td valign="top" align="right">
                                                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Edit" ImageUrl="~/images/edit.png" meta:resourcekey="Edit" />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" CommandArgument='<%# Eval("RequestId") %>'
                                                                        CommandName="_Cancel" ImageUrl="~/images/cancel.png" meta:resourcekey="Cancel"
                                                                        OnClientClick="return ConfirmCancel(this.id)" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" style="padding-left: 25px">
                                                        <table cellpadding="2" cellspacing="0" border="0" width="100%">
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="RequestDate"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("SalaryDate") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="RequestedAmount"></asp:Literal>
                                                                    (<asp:Label ID="lblDlCurrency" runat="server"></asp:Label>)
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("Amount") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ApprovedAmount"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("ApprovedAmount") %>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Reason"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left" style="word-break: break-all">
                                                                    <%# Eval("Reason")%>
                                                                    <asp:HiddenField ID="hdForwarded" runat="server" Value='<%# Eval("Forwarded") %>' />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="30%" valign="top" align="left">
                                                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Status"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td valign="top" align="left">
                                                                    <%# Eval("status") %>
                                                                    <%--    <%# Eval("RequestStatusDate")%> --%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="innerdivheader" width="40%" valign="top">
                                                                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="RequestedTo"></asp:Literal>
                                                                </td>
                                                                <td valign="top" width="2%">
                                                                    :
                                                                </td>
                                                                <td class="innerdivheadervalue" valign="top" align="left">
                                                                    <%# Eval("Requested") %>
                                                                    <asp:Label ID="lbRequested" runat="server" Text=' <%# Eval("RequestedTo") %>' Visible="false"> </asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <uc:Pager ID="SalaryRequestpager" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="fvSalaryDetails" EventName="ItemCommand" />
                        <asp:AsyncPostBackTrigger ControlID="dlSalaryRequest" EventName="ItemCommand" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgAdd" ImageUrl="~/images/Add salary advance.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkAdd" runat="server" OnClick="lnkAdd_Click" meta:resourcekey="Add"> </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgView" ImageUrl="~/images/Salary_Advance Request.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkView" runat="server" OnClick="lnkView_Click" meta:resourcekey="View"> 
		
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" style="display: none;">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgDelete" ImageUrl="../images/delete.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkDelete" runat="server" OnClick="lnkDelete_Click" meta:resourcekey="Delete"></asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright" id="divPrint" runat="server" style="display: none;">
                    <div class="mainrightimg">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/printer.png" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPrint" runat="server" OnClientClick="printFunction();" Text='<%$Resources:ControlsCommon,Print%>'>                           
                                
                            </asp:LinkButton></h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
