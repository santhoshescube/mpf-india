﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Collections.Specialized;
using System.Text;
using System.IO;

/*
 Modified By : Megha    
 Date        : 23 Sep 2013
 Description : Fine Tuning and Modifications in File Attachments
 */
public partial class Public_SendMail : System.Web.UI.Page
{
    #region Declarations

    //Object Instantiation
    clsXML objXML;
    clsTemplates objTemplates;
    clsMailSettings objMailSettings;
    clsEmployee objEmployee;

    #endregion Declarations

    #region Events

    /// <summary>
    /// Page Load Event For Send Mail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sugg"] != null)
            {
                dvTemplate.Visible = false;
                string strSuggestionMailAddress = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.FromMail);

                this.txtToMailId.Text = strSuggestionMailAddress;
            }
            else
            {
                dvTemplate.Visible = false;
                ddlTemplates.CssClass = "dropdownlist_disabled";
            }
            objEmployee = new clsEmployee();
            objEmployee.EmployeeID = new clsUserMaster().GetEmployeeId();
            objEmployee.GetOfficialEmailDetails();
            txtFrom.Text = objEmployee.OfficialEmail;
        }
    }

    /// <summary>
    /// For Attaches Files to be send
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAttachFiles_Click(object sender, ImageClickEventArgs e)
    {
        HttpFileCollection Attachments = Request.Files;
        DataTable dt = (DataTable)ViewState["Files"];

        if (dt == null)
        {
            dt = new DataTable();
            dt.Columns.Add("FileName");
            dt.Columns.Add("FilePath");
        }
        DataRow dr;
        string filepath = string.Empty;
        HttpPostedFile file;

        for (int i = 0; i < Attachments.Count; i++)
        {
            file = Attachments[i];
            if (file.ContentLength == 0)
                continue;

            filepath = Server.MapPath("~/documents/mail/" + Path.GetFileName(file.FileName));

            if (!Directory.Exists(Server.MapPath("~/documents/mail/")))
                Directory.CreateDirectory(Server.MapPath("~/documents/mail/"));

            file.SaveAs(filepath);
            dr = dt.NewRow();
            dr["FileName"] = Path.GetFileName(file.FileName);
            dr["FilePath"] = filepath;
            dt.Rows.Add(dr);
        }
        BindAttachments(dt);
        hdAttachCount.Value = "0";
    }

    /// <summary>
    /// Event for sending a mail
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        string msg = string.Empty;
        objTemplates = new clsTemplates();

        DataTable dt = (DataTable)ViewState["Files"];

        objMailSettings = new clsMailSettings();
        StringBuilder sb = new StringBuilder();

        if (objMailSettings.IsMailexists() == false)
        {
            msgs.InformationalMessage("Please set mail settings.");
            mpeMessage.Show();
            return;
        }

        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                    sb.Append("," + dr["FilePath"].ToString());

                sb.Remove(0, 1);
            }
        }

        //------- Replace Section----- //
        string sFrom = txtFrom.Text.Trim();
        string sBody = (Request.QueryString["PlanId"] != null ? GetVancancyContent().Body : txtEditor.Content);

        if (sFrom != string.Empty)
            msg = (objMailSettings.SendMail(sFrom, txtToMailId.Text, txtCc.Text, txtSubject.Text, sBody, sb.ToString(), true, clsMailSettings.AccountType.Email) == 1 ? string.Empty : "Mail could not send");
        else
            msg = "Please set 'From mail' config item.";

        if (msg == string.Empty)
        {
            RemoveFiles();
            msgs.InformationalMessage("Mail has been sent successfully.");
        }
        else
            msgs.InformationalMessage(msg);

        mpeMessage.Show();
    }

    /// <summary>
    /// For deleting attached files
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    protected void dlFiles_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "_Delete":
                DataTable dt = (DataTable)ViewState["Files"];
                DataRow dr = dt.NewRow();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["FilePath"].ToString() == e.CommandArgument.ToString())
                    {
                        dr = dt.Rows[i];

                        if (e.CommandArgument.ToString().Contains("@/documents/mail/"))
                        {
                            if (File.Exists(e.CommandArgument.ToString()))
                                File.Delete(e.CommandArgument.ToString());
                        }
                    }
                }

                dt.Rows.Remove(dr);
                BindAttachments(dt);
                break;
        }
    }

    protected void ddlTemplates_SelectedIndexChanged(object sender, EventArgs e)
    {
        objTemplates = new clsTemplates();
        objXML = new clsXML();

        if (Convert.ToInt32(ddlTemplates.SelectedValue) == -1)
            txtEditor.Content = string.Empty;
        else
        {
            objXML.AttributeId = Convert.ToInt32(ddlTemplates.SelectedValue);
            txtEditor.Content = objXML.GetTemplateMessage();
        }
    }    

    #endregion Events

    #region Functions

    private void BindTemplates()
    {
        objTemplates = new clsTemplates();
        objTemplates.Type = clsTemplates.TemplateType.VacancyPosting;

        ddlTemplates.DataSource = objTemplates.GetTemplates();
        ddlTemplates.DataBind();

        ddlTemplates.Items.Insert(0, new ListItem("Select", "-1"));
    }

    public void BindAttachments(DataTable dt)
    {
        if (dt.Rows.Count == 0)
            dt = null;

        dlFiles.DataSource = dt;
        dlFiles.DataBind();
        ViewState["Files"] = dt;
    }

    public MailMessage GetVancancyContent()
    {
        MailDefinition mdSendMail = new MailDefinition();
        ListDictionary ldSendMail = new ListDictionary();
        MailMessage mmContent;
        string sMessage;

        if (ViewState["ActivityId"] != null)
        {
            objTemplates.ActivityId = Convert.ToInt32(ViewState["ActivityId"]);
            DataTable dtvacancy = objTemplates.GetActivityVacancyDetails();
            
            if (dtvacancy.Rows.Count > 0)
            {
                ldSendMail.Add("<%Job Title%>", Convert.ToString(dtvacancy.Rows[0]["JobTitle"]));
                ldSendMail.Add("<%Job Code%>", Convert.ToString(dtvacancy.Rows[0]["JobCode"]));
                ldSendMail.Add("<%No of Vacancies%>", Convert.ToString(dtvacancy.Rows[0]["NoOfVacancies"]));
                ldSendMail.Add("<%Company Name%>", Convert.ToString(dtvacancy.Rows[0]["Company"]));
                ldSendMail.Add("<%Location%>", Convert.ToString(dtvacancy.Rows[0]["Location"]));
                mdSendMail.From = new clsConfiguration().GetConfiguration(clsConfiguration.ConfigurationItems.FromMail);// "info@HrPower.com";
                mdSendMail.Priority = MailPriority.High;
            }
        }
        sMessage = txtEditor.Content.Replace("{$$", "<%");
        sMessage = sMessage.Replace("$$}", "%>");
        mmContent = mdSendMail.CreateMailMessage(txtToMailId.Text, ldSendMail, sMessage, this);
        return mmContent;
    }

    private void RemoveFiles()
    {
        DataTable dt = (DataTable)ViewState["Files"];
        if (dt != null)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["FilePath"].ToString().Contains(@"\documents\mail\"))
                {
                    if (File.Exists(dt.Rows[i]["FilePath"].ToString()))
                        File.Delete(dt.Rows[i]["FilePath"].ToString());
                }
            }
        }
        ViewState["Files"] = null;
    }

    #endregion Functions            
}
