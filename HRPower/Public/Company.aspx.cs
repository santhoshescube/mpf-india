﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Public_Company : System.Web.UI.Page
{
    clsCompany objCompany;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    private bool MblnAddPermission;  // 
    private bool MblnUpdatePermission;  // 
    private bool MblnDeletePermission;  // 
    private bool MblnViewPermission;  // 
    private bool MblnPrintEmailPermission;  // 
 // private controls_LeavePolicy tempUc;

    private string CurrentSelectedValue { get; set; }
    //private controls_LeavePolicy uc_LeavePolicy
    //{
    //    get
    //    {
    //        if (tempUc == null)
    //            tempUc = (controls_LeavePolicy)Page.LoadControl("~/controls/LeavePolicy.ascx");

    //        return tempUc;
    //    }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        // Auto complete
        this.RegisterAutoComplete();


        companyPager.Fill += new controls_Pager.FillPager(BindDatalist);

        if (!IsPostBack)
        {
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();

            SetPermission(); 
            EnableMenus();

            if (MblnViewPermission ==true)
            {
                BindDatalist();
            }
            else
            {
                companyPager.Visible = false;
                lblNoData.Style["display"] = "block";
                lblNoData.Text = "You dont have enough permission to view this page.";
            }
      }


    }


    public void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
        {
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.CompnayHR);
        }
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }
    public void EnableMenus()
    {

        DataTable dtm = (DataTable)ViewState["Permission"];
        if (dtm.Rows.Count > 0)
        {
            //lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
           
            MblnAddPermission = dtm.Rows[0]["IsCreate"].ToBoolean();
            MblnViewPermission = dtm.Rows[0]["IsView"].ToBoolean();
            MblnDeletePermission = dtm.Rows[0]["IsDelete"].ToBoolean();

            lnkListCompany.Enabled = MblnViewPermission;

        }
        //if (lnkDelete.Enabled)
        //    lnkDelete.OnClientClick = "return valDeleteDatalist('" + gvAnnouncements.ClientID + "');";
        //else
        //    lnkDelete.OnClientClick = "return false;";
    }

    void ReferenceControlNew_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        ////CurrentSelectedValue = SelectedValue;
        //Type thisType = this.GetType();
        //MethodInfo theMethod = thisType.GetMethod(functionName);
        //theMethod.Invoke(this, null);
    }

    void ReferenceControlNew_onRefresh()
    {
        //mdlPopUpReference.Show();
    }

    #region Functions

    

    //public void FillComboCountry()
    //{
    //    //DropDownList rcCountry = (DropDownList)fvCompany.FindControl("rcCountry");
    //    //UpdatePanel updCountry = (UpdatePanel)fvCompany.FindControl("updCountry");

    //    //if (rcCountry != null && updCountry != null)
    //    //{
    //    //    this.BindDropDown(this.rcCountry, "Description", "CountryId", new clsCompany().GetAllCountries(), "Select");
    //    //    this.SetSelectedIndex(this.rcCountry);
    //    //    updCountry.Update();
    //    //}
    //}

    //public void FillComboCompanyIndustry()
    //{
    //    DropDownList rcCompanyIndustry = (DropDownList)fvCompany.FindControl("rcCompanyIndustry");
    //    UpdatePanel updCompanyIndustry = (UpdatePanel)fvCompany.FindControl("updCompanyIndustry");

    //    if (rcCompanyIndustry != null && updCompanyIndustry != null)
    //    {
    //        this.BindDropDown(this.rcCompanyIndustry, "Description", "CompanyIndustryId", new clsCompany().GetAllIndustries(), "Select");
    //        this.SetSelectedIndex(this.rcCompanyIndustry);
    //        updCompanyIndustry.Update();
    //    }
    //}

    //public void FillComboProvince()
    //{
    //    DropDownList rcState = (DropDownList)fvCompany.FindControl("rcState");
    //    UpdatePanel updState = (UpdatePanel)fvCompany.FindControl("updState");

    //    if (rcState != null && updState != null)
    //    {
    //        this.BindDropDown(this.rcState, "Description", "ProvinceId", new clsCompany().GetAllProvince(), "Select");
    //        this.SetSelectedIndex(this.rcState);
    //        updState.Update();
    //    }
    //}

    //public void FillComboCompanyType()
    //{
    //    DropDownList rcCompanyType = (DropDownList)fvCompany.FindControl("rcCompanyType");
    //    UpdatePanel updCompanyType = (UpdatePanel)fvCompany.FindControl("updCompanyType");

    //    if (rcCompanyType != null && updCompanyType != null)
    //    {
    //        this.BindDropDown(this.rcCompanyType, "Description", "CompanyTypeId", new clsCompany().GetAllCompanyTypes(), "Select");
    //        this.SetSelectedIndex(this.rcCompanyType);
    //        updCompanyType.Update();
    //    }
    //}

    //private void SetSelectedIndex(DropDownList ddwn)
    //{
    //    //if (ddwn != null && this.CurrentSelectedValue != null && ddwn.Items.FindByValue(this.CurrentSelectedValue) != null)
    //    //    ddwn.SelectedValue = CurrentSelectedValue;
    //}

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource)
    {
        this.BindDropDown(ddwn, dataTextField, dataValueField, dataSource, null);
    }

    private void BindDropDown(DropDownList ddwn, string dataTextField, string dataValueField, DataTable dataSource, string insertText)
    {
        ddwn.DataValueField = dataValueField;
        ddwn.DataTextField = dataTextField;
        ddwn.DataSource = dataSource;
        ddwn.DataBind();

        if (insertText != null && insertText.Trim() != string.Empty)
            ddwn.Items.Insert(0, new ListItem(insertText, "-1"));
    }

    //public void FillComboDesignation()
    //{
    //    DropDownList rcDesignation = (DropDownList)fvCompany.FindControl("rcDesignation");
    //    UpdatePanel updDesignation = (UpdatePanel)fvCompany.FindControl("updDesignation");

    //    if (rcDesignation != null && updDesignation != null)
    //    {
    //        this.BindDropDown(this.rcDesignation, "Description", "DesignationId", new clsCompany().GetAllDesignationType(), "Select");
    //        this.SetSelectedIndex(this.rcDesignation);
    //        updDesignation.Update();
    //    }
    //}

    private void FillAllCombos()
    {
        try
        {
            DropDownList rcCountry = (DropDownList)fvCompany.FindControl("rcCountry");
            DropDownList rcCompanyIndustry = (DropDownList)fvCompany.FindControl("rcCompanyIndustry");
            DropDownList rcState = (DropDownList)fvCompany.FindControl("rcState");
            DropDownList rcCompanyType = (DropDownList)fvCompany.FindControl("rcCompanyType");
            DropDownList rcDesignation = (DropDownList)fvCompany.FindControl("rcDesignation");

            UpdatePanel updCountry = (UpdatePanel)fvCompany.FindControl("updCountry");
            UpdatePanel updCompanyIndustry = (UpdatePanel)fvCompany.FindControl("updCompanyIndustry");
            UpdatePanel updState = (UpdatePanel)fvCompany.FindControl("updState");
            UpdatePanel updDesignation = (UpdatePanel)fvCompany.FindControl("updDesignation");
            UpdatePanel updCompanyType = (UpdatePanel)fvCompany.FindControl("updCompanyType");

            if (rcCountry != null && rcCompanyIndustry != null && rcState != null && rcCompanyType != null && rcDesignation != null
                && updCountry != null && updCompanyIndustry != null && updState != null && updDesignation != null && updCompanyType != null)
            {
                objCompany = new clsCompany();

                using (DataSet ds = objCompany.FillAllCombos())
                {
                    if (ds.Tables.Count > 0)
                        this.BindDropDown(rcCountry, "Description", "CountryId", ds.Tables[0], "Select");

                    if (ds.Tables.Count > 1)
                        this.BindDropDown(rcCompanyIndustry, "Description", "CompanyIndustryId", ds.Tables[1], "Select");

                    if (ds.Tables.Count > 2)
                        this.BindDropDown(rcState, "Description", "ProvinceId", ds.Tables[2], "Select");

                    if (ds.Tables.Count > 3)
                        this.BindDropDown(rcCompanyType, "Description", "CompanyTypeId", ds.Tables[3], "Select");

                    if (ds.Tables.Count > 4)
                        this.BindDropDown(rcDesignation, "Description", "DesignationId", ds.Tables[4], "Select");

                    updCompanyIndustry.Update();
                    updCountry.Update();
                    updState.Update();
                    updDesignation.Update();
                    updCompanyType.Update();
                }
            }
        }
        finally
        {
            objCompany = null;
        }
    }

    void rcCurrency_Update()
    {
        objCompany = new clsCompany();
        DropDownList ddlCurrency = (DropDownList)fvCompany.FindControl("ddlCurrency");

        if (ddlCurrency != null)
        {
            ddlCurrency.DataTextField = "Description";
            ddlCurrency.DataValueField = "CurrencyID";

            ddlCurrency.DataSource = objCompany.FillCurrency(1);
            ddlCurrency.DataBind();

            ddlCurrency.Items.Insert(0, new ListItem("Select", "-1"));

            // ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(Convert.ToString(rcCurrency.CurrencyId)));
        }

        upnlFormview.Update();
    }

    public string GetLogo(object oCompanyId, int width)
    {
        return "../thumbnail.aspx?FromDB=true&type=Company&width=" + width + "&CompanyId=" + Convert.ToInt32(oCompanyId) + "&t=" + DateTime.Now.Ticks;
    }

    public string FormviewLogo()
    {
        HtmlGenericControl divImage = (HtmlGenericControl)fvCompany.FindControl("divImage");

        divImage.Attributes.Add("style", "display:block;");
        return "../thumbnail.aspx?FromDB=true&type=Company&Width=150&CompanyId=" + Convert.ToInt32((fvCompany.CurrentMode == FormViewMode.Insert ? -1 : fvCompany.DataKey["CompanyID"])) + "&t=" + DateTime.Now.Ticks;
    }

    public void BindDatalist()
    {
        objCompany = new clsCompany();

        objCompany.PageIndex = companyPager.CurrentPage + 1;
        objCompany.PageSize = companyPager.PageSize;
        objCompany.SortExpression = "CompanyID";
        objCompany.SortOrder = "Asc";
        objCompany.SearchKey = txtSearch.Text;

        DataSet ds = objCompany.GetAllCompanies();

        dlCompany.DataSource = ds;
        dlCompany.DataBind();

        fvCompany.ChangeMode(FormViewMode.ReadOnly);

        fvCompany.DataSource = null;
        fvCompany.DataBind();


        if (ds.Tables[0].Rows.Count > 0)
        {
            objCompany.SearchKey = txtSearch.Text;
            companyPager.Total = objCompany.GetCompanyCount();
            companyPager.Visible = true;
            dlCompany.Visible = true;
        }
        else
        {
            dlCompany.Visible = false;
            companyPager.Visible = false;
        }

    }

    public void BindFormView(int iCompanyId)
    {
        objCompany = new clsCompany();

        objCompany.CompanyID = iCompanyId;

        DataTable dt = objCompany.SelectedCompany();

        ViewState["Buffer"] = dt.Rows[0]["LogoFile"];

        fvCompany.DataSource = dt;
        fvCompany.DataBind();

        dlCompany.DataSource = null;
        dlCompany.DataBind();

        companyPager.Visible = false;

        Table tbl = objCompany.PrintCompany(Convert.ToString(fvCompany.DataKey["CompanyID"]));

        tbl.Width = Unit.Percentage(100);
        tbl.Attributes.Add("Style", "Display:none;");

        Panel pnlSingleCompany = (Panel)fvCompany.FindControl("pnlSingleCompany");

        if (pnlSingleCompany != null)
            pnlSingleCompany.Controls.Add(tbl);

        string sPrinttbl = "Print('" + tbl.ClientID + "')";

        //if (fvCompany.CurrentMode != FormViewMode.Edit)
        //{
        //    lnkPrint.OnClientClick = sPrinttbl;
        //    lnkEmail.OnClientClick = "return showMailDialog('Type=Company&CompanyId=" + Convert.ToString(fvCompany.DataKey["CompanyID"]) + "');";
        //    lnkDelete.OnClientClick = "return confirm('Are you sure to delete?');";
        //}
    }

    public string IsCompany(object oParentID, object oCompanyID)
    {
        objCompany = new clsCompany();

        string sName = string.Empty;
        HtmlTableRow tr = (HtmlTableRow)fvCompany.FindControl("trBranch");

        if (tr != null)
        {
            if (Convert.ToInt32(oParentID) == 0)
            {
                tr.Attributes.Add("style", "display:none;");

                objCompany.ParentID = Convert.ToInt32(oCompanyID);
                sName = Convert.ToString(objCompany.GetParentCompanyName());
            }
            else
            {
                tr.Attributes.Add("style", "display:block;");

                objCompany.ParentID = Convert.ToInt32(oParentID);
                sName = Convert.ToString(objCompany.GetParentCompanyName());
            }
        }
        return sName;
    }

    public string IsMonth(object oIsMonth)
    {
        if (Convert.ToBoolean(oIsMonth))
            return "Month";
        else
            return "Year";
    }

    //public string DeleteCompany(int iCompanyId)
    //{
    //    objCompany = new clsCompany();
    //    string sCompanyId = string.Empty;

    //    objCompany.CompanyID = iCompanyId;

    //    if (objCompany.CheckBranchExists())
    //    {
    //        objCompany.ParentID = iCompanyId;

    //        sCompanyId = objCompany.GetParentCompanyName();
    //    }
    //    else
    //    {
    //        string result = objCompany.DeleteCompany();

    //        if (result != "0")
    //        {
    //            objCompany.ParentID = iCompanyId;

    //            sCompanyId = objCompany.GetParentCompanyName();
    //        }
    //    }
    //    return Convert.ToString(sCompanyId);
    //}

    public string ValidDate(object oDate)
    {
        return (fvCompany.CurrentMode == FormViewMode.Insert ? string.Empty : (Convert.ToString(oDate) == "1/1/1900 12:00:00 AM" ? string.Empty : (Convert.ToString(oDate) == "01 Jan 1900" ? string.Empty : clsCommon.Convert2DateTime(oDate).ToString((fvCompany.CurrentMode == FormViewMode.Edit) ? "dd/MM/yyyy" : "dd MMM yyyy"))));
    }

    public void Addcompany()
    {
        objCompany = new clsCompany();

        fvCompany.ChangeMode(FormViewMode.Insert);

        //lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;

        //lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

        dlCompany.DataSource = null;
        dlCompany.DataBind();

        companyPager.Visible = false;

      //  txtSearch.Text = string.Empty;

        ViewState["Buffer"] = null;



        upnlFormview.Update();
        upnlDatalist.Update();

    }

    #endregion

    #region Events

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        Addcompany();
    }

    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            //txtSearch.Text = string.Empty;
            BindDatalist();
            upnlFormview.Update();
            upnlDatalist.Update();
        }
        catch
        {
        }
    }

    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        objCompany = new clsCompany();

        if (dlCompany.Items.Count > 0)
        {
            string sCompanyIds = string.Empty;
            foreach (DataListItem item in dlCompany.Items)
            {
                CheckBox chkCompany = (CheckBox)item.FindControl("chkCompany");

                if (chkCompany == null)
                    return;
                if (chkCompany.Checked)
                {
                    if (sCompanyIds == string.Empty)
                        sCompanyIds = Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                    else
                        sCompanyIds += "," + Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                }
            }
            Table tbl = objCompany.PrintCompany(sCompanyIds);

            tbl.Width = Unit.Percentage(100);
            tbl.Attributes.Add("Style", "Display:none;");

            pnlSelectedCompanies.Controls.Add(tbl);

           // ScriptManager.RegisterStartupScript(lnkPrint, this.GetType(), "Print", "Print('" + tbl.ClientID + "')", true);

            upnlDatalist.Update();
            upnlFormview.Update();
        }
    }

    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        if (dlCompany.Items.Count > 0)
        {
            string sCompanyIds = string.Empty;
            foreach (DataListItem item in dlCompany.Items)
            {
                CheckBox chkCompany = (CheckBox)item.FindControl("chkCompany");

                if (chkCompany == null)
                    return;
                if (chkCompany.Checked)
                {
                    if (sCompanyIds == string.Empty)
                        sCompanyIds = Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                    else
                        sCompanyIds += "," + Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                }
            }
           // ScriptManager.RegisterStartupScript(lnkEmail, this.GetType(), "Email", "showMailDialog('Type=Company&CompanyId=" + Convert.ToString(sCompanyIds) + "');", true);

            upnlDatalist.Update();
            upnlFormview.Update();
        }
    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
       // string sCompanyId = string.Empty;

       //// txtSearch.Text = string.Empty;

       // if (dlCompany.Items.Count > 0)
       // {
       //     foreach (DataListItem item in dlCompany.Items)
       //     {
       //         CheckBox chkCompany = (CheckBox)item.FindControl("chkCompany");

       //         if (chkCompany != null)
       //         {
       //             if (chkCompany.Checked)
       //                 sCompanyId = (sCompanyId == string.Empty ? DeleteCompany(Convert.ToInt32(dlCompany.DataKeys[item.ItemIndex])) : sCompanyId + "," + DeleteCompany(Convert.ToInt32(dlCompany.DataKeys[item.ItemIndex])));
       //         }
       //     }
       // }
       // else
       // {
       //     if (fvCompany.CurrentMode == FormViewMode.ReadOnly)
       //         sCompanyId = DeleteCompany(Convert.ToInt32(fvCompany.DataKey["CompanyID"]));
       // }

       // if (sCompanyId == string.Empty)
       // {
       //     fvCompany.DataSource = null;
       //     fvCompany.DataBind();

       //     BindDatalist();

       //     //msgs.InformationalMessage("Selected company(s) deleted successfully.");
       // }
       // else
       // {
       //    // msgs.InformationalMessage("Cannot delete " + sCompanyId);

       //     BindDatalist();
       // }

       //// mpeMessage.Show();

       // upnlDatalist.Update();
       // upnlFormview.Update();
    }

    protected void fvCompany_DataBound(object sender, EventArgs e)
    {
        FillAllCombos();
        objCompany = new clsCompany();

        if (fvCompany.CurrentMode == FormViewMode.ReadOnly)
        {
            DataList dlBankView = (DataList)fvCompany.FindControl("dlBankView");

            //imgHeading.ImageUrl = "../images/Company.png";
            //lblHeading.Text = "Company Details";

            if (dlBankView == null)
                return;
            objCompany.CompanyID = Convert.ToInt32(fvCompany.DataKey["CompanyID"]);

            DataTable dt = objCompany.GetCompanyBanks();

            HtmlTableRow trBank = (HtmlTableRow)fvCompany.FindControl("trBank");

            if (dt.Rows.Count > 0)
            {
                if (Request.Browser.Browser == "IE")
                    trBank.Style["display"] = "block";
                else
                    trBank.Style["display"] = "table-row";

                dlBankView.DataSource = dt;
                dlBankView.DataBind();
            }
            else
            {
                trBank.Style["display"] = "none";
            }

            HtmlTableRow trBranch = (HtmlTableRow)fvCompany.FindControl("trBranch");

            if (Convert.ToBoolean(fvCompany.DataKey["CompanyBranchIndicator"]))
                trBranch.Attributes.Add("style", "display:none;");
            else
                trBranch.Attributes.Add("style", "display:table-row");
        }
        else if (fvCompany.CurrentMode == FormViewMode.Insert)
        {
            //imgHeading.ImageUrl = "../images/Add_CompanyBig.PNG";
            //lblHeading.Text = "Add Company Details";

        }

        RadioButtonList rbtnCreationMode = (RadioButtonList)fvCompany.FindControl("rbtnCreationMode");
        RadioButtonList rbtnDailyPayMode = (RadioButtonList)fvCompany.FindControl("rbtnDailyPayMode");
        TextBox txtWorkingDays = (TextBox)fvCompany.FindControl("txtWorkingDays");
        DropDownList ddlOffDay = (DropDownList)fvCompany.FindControl("ddlOffDay");
        DropDownList ddlCompany = (DropDownList)fvCompany.FindControl("ddlCompany");
        DropDownList rcCountry = (DropDownList)fvCompany.FindControl("rcCountry");
        DropDownList ddlCurrency = (DropDownList)fvCompany.FindControl("ddlCurrency");
        DropDownList rcCompanyIndustry = (DropDownList)fvCompany.FindControl("rcCompanyIndustry");
        DropDownList rcState = (DropDownList)fvCompany.FindControl("rcState");
        DropDownList rcCompanyType = (DropDownList)fvCompany.FindControl("rcCompanyType");
        DropDownList rcDesignation = (DropDownList)fvCompany.FindControl("rcDesignation");
        DropDownList ddlBankName = (DropDownList)fvCompany.FindControl("ddlBankName");
        ListView lvBank = (ListView)fvCompany.FindControl("lvBank");
        DataList dlBank = (DataList)fvCompany.FindControl("dlBank");
        TextBox txtFinancialStartDate = (TextBox)fvCompany.FindControl("txtFinancialStartDate");
        TextBox txtBookStartDate = (TextBox)fvCompany.FindControl("txtBookStartDate");
        AjaxControlToolkit.AsyncFileUpload fuLogo = (AjaxControlToolkit.AsyncFileUpload)fvCompany.FindControl("fuLogo");

        if (fvCompany.CurrentMode != FormViewMode.ReadOnly)
            fuLogo.ClearFileFromPersistedStore();

        if (rbtnCreationMode == null || rbtnDailyPayMode == null || ddlOffDay == null || ddlCompany == null || ddlCurrency == null ||
            txtFinancialStartDate == null)
            return;

        if (txtWorkingDays != null)
        {
            if (fvCompany.CurrentMode == FormViewMode.Insert)
                txtWorkingDays.Text = "365";
        }

        //Fill Off days
        ddlOffDay.DataSource = objCompany.FillWeekdays();
        ddlOffDay.DataBind();

        ddlOffDay.Items.Insert(0, new ListItem("Select", "-1"));

        // Fill Company
        objUser = new clsUserMaster();
        ddlCompany.DataSource = objCompany.FillCompany();
        ddlCompany.DataBind();
        ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(objUser.GetCompanyId())));
        ddlCompany.Enabled = new clsRoleSettings().IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);
        ddlCompany.Items.Insert(0, new ListItem("Select", "-1"));

        //Fill Currency    
        ddlCurrency.DataSource = objCompany.FillCurrency(Convert.ToInt32(fvCompany.DataKey["CompanyID"]));
        ddlCurrency.DataBind();

        ddlCurrency.Items.Insert(0, new ListItem("Select", "-1"));

        /* Fill Bank*/
        objCompany.CompanyID = Convert.ToInt32(fvCompany.DataKey["CompanyID"]);

        DataTable dtBank = objCompany.GetCompanyBanks();

        if (dtBank.Rows.Count == 0)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("CompanyAccID");
            dt.Columns.Add("BankNameID");
            dt.Columns.Add("Bank");
            dt.Columns.Add("AccountNo");

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);

            ViewState["Bank"] = dt;
        }
        else
            ViewState["Bank"] = dtBank;

        dlBank.DataSource = ViewState["Bank"];
        dlBank.DataBind();

        if (fvCompany.DataKey["FinYearStartDate"] == null)
            txtFinancialStartDate.Text = "1/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
        else if (fvCompany.DataKey["FinYearStartDate"] == DBNull.Value)
            txtFinancialStartDate.Text = "1/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
        else
        {
            objCompany.CompanyID = Convert.ToInt32(fvCompany.DataKey["CompanyID"]);

            if (objCompany.CompanyHasLeavePolicy())
            {
                ImageButton ibtnFinancialStartDate = (ImageButton)fvCompany.FindControl("ibtnFinancialStartDate");

                ibtnFinancialStartDate.Enabled = false;

                txtFinancialStartDate.Enabled = false;
                txtFinancialStartDate.CssClass = "textbox_disabled";
            }
        }

        if (fvCompany.DataKey["BookStartDate"] == null)
            txtBookStartDate.Text = txtFinancialStartDate.Text;
        else if (fvCompany.DataKey["BookStartDate"] == DBNull.Value)
            txtBookStartDate.Text = txtFinancialStartDate.Text;

        RequiredFieldValidator rfvCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvCompany");
        RequiredFieldValidator rfvBranch = (RequiredFieldValidator)fvCompany.FindControl("rfvBranch");
        RequiredFieldValidator rfvddlCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvddlCompany");

        if (rbtnCreationMode.SelectedValue == "1")
        {
            rfvCompany.Enabled = true;
            rfvBranch.Enabled = false;
            rfvddlCompany.Enabled = false;
        }
        else
        {
            rfvBranch.Enabled = true;
            rfvCompany.Enabled = false;
            rfvddlCompany.Enabled = true;
        }


        if (fvCompany.CurrentMode == FormViewMode.Edit)
        {

            //imgHeading.ImageUrl = "../images/Edit company detail.png";
            //lblHeading.Text = "Edit Company Details";

            rbtnCreationMode.SelectedIndex = (Convert.ToInt32(fvCompany.DataKey["CompanyBranchIndicator"]) == 1 ? 0 : 1);

            rbtnDailyPayMode.SelectedIndex = (Convert.ToInt32(fvCompany.DataKey["IsMonth"]) == 1 ? 1 : 0);

            if (rbtnCreationMode.SelectedIndex == 1)
                ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToInt32(fvCompany.DataKey["ParentID"]) == 0 ? Convert.ToString(fvCompany.DataKey["CompanyID"]) : Convert.ToString(fvCompany.DataKey["ParentID"])));

            ddlOffDay.SelectedIndex = ddlOffDay.Items.IndexOf(ddlOffDay.Items.FindByValue(Convert.ToString(fvCompany.DataKey["DayID"])));

            ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(Convert.ToString(fvCompany.DataKey["CurrencyId"])));

            //rcCompanyIndustry.ChangeIndex(Convert.ToInt32(fvCompany.DataKey["CompanyIndustryID"]));
            rcCompanyIndustry.SelectedValue = fvCompany.DataKey["CompanyIndustryID"].ToString();

            if (fvCompany.DataKey["ProvinceID"] != DBNull.Value)
                //rcState.ChangeIndex(Convert.ToInt32(fvCompany.DataKey["ProvinceID"]));
                rcState.SelectedValue = fvCompany.DataKey["ProvinceID"].ToString();

            if (fvCompany.DataKey["CompanyTypeID"] != DBNull.Value)
                //rcCompanyType.ChangeIndex(Convert.ToInt32(fvCompany.DataKey["CompanyTypeID"]));
                rcCompanyType.SelectedValue = fvCompany.DataKey["CompanyTypeID"].ToString();

            if (fvCompany.DataKey["ContactPersonDesignationID"] != DBNull.Value)
                //rcDesignation.ChangeIndex(Convert.ToInt32(fvCompany.DataKey["ContactPersonDesignationID"]));
                rcDesignation.SelectedValue = fvCompany.DataKey["ContactPersonDesignationID"].ToString();


            rcCountry.SelectedValue = fvCompany.DataKey["CountryID"].ToString(); ;

            HtmlGenericControl divCompany = (HtmlGenericControl)fvCompany.FindControl("divCompany");
            HtmlGenericControl divBranch = (HtmlGenericControl)fvCompany.FindControl("divBranch");
            HtmlGenericControl trAddBranch = (HtmlGenericControl)fvCompany.FindControl("trAddBranch");
            TextBox txtBranchName = (TextBox)fvCompany.FindControl("txtBranchName");

            rbtnCreationMode.Items[0].Selected = Convert.ToBoolean(fvCompany.DataKey["CompanyBranchIndicator"]);

            if (rbtnCreationMode.SelectedValue == "1")
            {
                divCompany.Attributes.Add("style", "display:block;height:29px;float: left; width: 76%");
                divBranch.Attributes.Add("style", "display:none;");
                //ivBranch.Attributes.Add("style", ":none;");

                trAddBranch.Attributes.Add("style", "display:none;");

                txtBranchName.Text = string.Empty;
                txtBranchName.Enabled = false;

            }
            else
            {
                divCompany.Attributes.Add("style", "display:none");
                divBranch.Attributes.Add("style", "display:block;height:29px;float: left; width: 76%");
                trAddBranch.Attributes.Add("style", "display:table-row");

                txtBranchName.Enabled = true;
            }

            HiddenField hfCompanyFinYear = (HiddenField)fvCompany.FindControl("hfCompanyFinYear");

            objCompany.CompanyID = Convert.ToInt32(fvCompany.DataKey["ParentID"]);

            hfCompanyFinYear.Value = objCompany.GetFinancialYear().ToString("dd/MM/yyyy");//clsCommon.Convert2DateTime(objCompany.GetFinancialYear()).ToString("dd/MM/yyyy");
        }
    }

    protected void fvCompany_ItemCommand(object sender, FormViewCommandEventArgs e)
    {

        objCompany = new clsCompany();

        RadioButtonList rbtnCreationMode = (RadioButtonList)fvCompany.FindControl("rbtnCreationMode");
        TextBox txtBranchName = (TextBox)fvCompany.FindControl("txtBranchName");
        TextBox txtCompanyName = (TextBox)fvCompany.FindControl("txtCompanyName");

        objCompany.CompanyID = Convert.ToInt32((fvCompany.CurrentMode == FormViewMode.Insert ? 0 : fvCompany.DataKey["CompanyID"]));
        objCompany.Name = (rbtnCreationMode.SelectedValue == "1" ? txtCompanyName.Text : txtBranchName.Text);

        DropDownList ddlCurrency;
        DataList dlBank = (DataList)fvCompany.FindControl("dlBank");

        DropDownList ddlBanks;
        TextBox txtAccountNo;
        HiddenField hfCompanyAccId;

        switch (e.CommandName)
        {
            case "_Submit":

                if (!Page.IsValid)
                    return;
                else
                {
                    int result = -1;

                    if (objCompany.IsCompanyExists() == 0)
                    {
                    //    msgs.InformationalMessage("Company name already exists.");
                    //    mpeMessage.Show();
                    }
                    else
                    {
                        DropDownList ddlCompany = (DropDownList)fvCompany.FindControl("ddlCompany");
                        TextBox txtEmployerCode = (TextBox)fvCompany.FindControl("txtEmployerCode");
                        TextBox txtShortName = (TextBox)fvCompany.FindControl("txtShortName");
                        TextBox txtPOBox = (TextBox)fvCompany.FindControl("txtPOBox");
                        DropDownList rcCountry = (DropDownList)fvCompany.FindControl("rcCountry");
                        ddlCurrency = (DropDownList)fvCompany.FindControl("ddlCurrency");
                        DropDownList rcCompanyIndustry = (DropDownList)fvCompany.FindControl("rcCompanyIndustry");
                        RadioButtonList rbtnDailyPayMode = (RadioButtonList)fvCompany.FindControl("rbtnDailyPayMode");
                        TextBox txtWorkingDays = (TextBox)fvCompany.FindControl("txtWorkingDays");
                        DropDownList ddlOffDay = (DropDownList)fvCompany.FindControl("ddlOffDay");
                        TextBox txtWebsite = (TextBox)fvCompany.FindControl("txtWebsite");
                        TextBox txtArea = (TextBox)fvCompany.FindControl("txtArea");
                        TextBox txtBlock = (TextBox)fvCompany.FindControl("txtBlock");
                        TextBox txtStreet = (TextBox)fvCompany.FindControl("txtStreet");
                        TextBox txtCity = (TextBox)fvCompany.FindControl("txtCity");
                        DropDownList rcState = (DropDownList)fvCompany.FindControl("rcState");
                        DropDownList rcCompanyType = (DropDownList)fvCompany.FindControl("rcCompanyType");
                        TextBox txtPrimaryEmail = (TextBox)fvCompany.FindControl("txtPrimaryEmail");
                        TextBox txtSecondaryEmail = (TextBox)fvCompany.FindControl("txtSecondaryEmail");
                        TextBox txtContactPerson = (TextBox)fvCompany.FindControl("txtContactPerson");
                        DropDownList rcDesignation = (DropDownList)fvCompany.FindControl("rcDesignation");
                        TextBox txtTelephone = (TextBox)fvCompany.FindControl("txtTelephone");
                        TextBox txtFax = (TextBox)fvCompany.FindControl("txtFax");
                        TextBox txtOtherInformations = (TextBox)fvCompany.FindControl("txtOtherInformations");
                        TextBox txtFinancialStartDate = (TextBox)fvCompany.FindControl("txtFinancialStartDate");
                        TextBox txtBookStartDate = (TextBox)fvCompany.FindControl("txtBookStartDate");
                        AjaxControlToolkit.AsyncFileUpload fuLogo = (AjaxControlToolkit.AsyncFileUpload)fvCompany.FindControl("fuLogo");

                        objCompany.CompanyFlag = (fvCompany.CurrentMode == FormViewMode.Insert ? 1 : 2);

                        if (rbtnCreationMode.SelectedIndex == 0)
                        {
                            objCompany.ParentID = 0;
                            objCompany.CompanyBranchIndicator = (rbtnCreationMode.SelectedValue == "1" ? true : false);
                            objCompany.Name = txtCompanyName.Text;
                        }
                        else
                        {
                            objCompany.ParentID = Convert.ToInt32(ddlCompany.SelectedValue);
                            objCompany.CompanyBranchIndicator = (rbtnCreationMode.SelectedValue == "1" ? true : false);
                            objCompany.Name = txtBranchName.Text;
                        }

                        objCompany.ShortName = txtShortName.Text;
                        objCompany.POBox = txtPOBox.Text;
                        objCompany.Road = txtStreet.Text;
                        objCompany.Area = txtArea.Text;
                        objCompany.Block = txtBlock.Text;
                        objCompany.City = txtCity.Text;
                        objCompany.ProvinceID = Convert.ToInt32(rcState.SelectedValue);
                        objCompany.CountryID = Convert.ToInt32(rcCountry.SelectedValue);
                        objCompany.PrimaryEmail = txtPrimaryEmail.Text;
                        objCompany.SecondaryEmail = txtSecondaryEmail.Text;
                        objCompany.ContactPerson = txtContactPerson.Text;
                        objCompany.ContactPersonDesignationID = Convert.ToInt32(rcDesignation.SelectedValue);
                        objCompany.ContactPersonPhone = txtTelephone.Text;
                        objCompany.PABXNumber = txtFax.Text;
                        objCompany.WebSite = txtWebsite.Text;
                        objCompany.CompanyIndustryID = Convert.ToInt32(rcCompanyIndustry.SelectedValue);
                        objCompany.CompanyTypeID = Convert.ToInt32(rcCompanyType.SelectedValue);

                        byte[] buffer = null;

                        if (fuLogo.HasFile)
                        {
                            buffer = new byte[fuLogo.PostedFile.ContentLength];

                            var Stream = fuLogo.PostedFile.InputStream;

                            Stream.Seek(0, System.IO.SeekOrigin.Begin);

                            Stream.Read(buffer, 0, fuLogo.PostedFile.ContentLength);

                            fuLogo.ClearFileFromPersistedStore();
                        }
                        else
                        {
                            if (ViewState["Buffer"] != DBNull.Value)
                            {
                                if (ViewState["Buffer"] != null)
                                    buffer = (byte[])ViewState["Buffer"];
                            }
                        }

                        objCompany.LogoFile = buffer;
                        objCompany.OtherInfo = txtOtherInformations.Text;
                        objCompany.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);
                        objCompany.FinYearStartDate1 = clsCommon.Convert2DateTime(txtFinancialStartDate.Text).ToString("dd MMM yyyy");
                        objCompany.WorkingDaysInMonth = txtWorkingDays.Text;
                        objCompany.OffDayID = Convert.ToInt32(ddlOffDay.SelectedValue);
                        objCompany.BookStartDate1 = clsCommon.Convert2DateTime(txtBookStartDate.Text).ToString("dd MMM yyyy");
                        objCompany.IsMonth = (rbtnDailyPayMode.SelectedValue == "1" ? true : false);
                        objCompany.EPID = txtEmployerCode.Text;

                        result = objCompany.SubmitCompanyDetails();

                        objCompany.Begin();

                        try
                        {
                            if (result > 0)
                            {
                                foreach (DataListItem item in dlBank.Items)
                                {
                                    ddlBanks = (DropDownList)item.FindControl("ddlBanks");
                                    txtAccountNo = (TextBox)item.FindControl("txtAccountNo");
                                    hfCompanyAccId = (HiddenField)item.FindControl("hfCompanyAccId");

                                    if (ddlBanks == null || txtAccountNo == null)
                                        continue;

                                    objCompany.CompanyID = result;
                                    objCompany.BankNameId = Convert.ToInt32(ddlBanks.SelectedValue);
                                    objCompany.AccountNo = txtAccountNo.Text;

                                    if (Convert.ToInt32(ddlBanks.SelectedValue) != -1 && txtAccountNo.Text != string.Empty)
                                    {
                                        if (hfCompanyAccId.Value == string.Empty)
                                            objCompany.InsertCompanyBankDetails(true, -1);
                                        else
                                            objCompany.InsertCompanyBankDetails(false, Convert.ToInt32(hfCompanyAccId.Value));
                                    }
                                    else
                                        continue;
                                }
                            }
                            objCompany.Commit();
                        }
                        catch (Exception ex)
                        {
                            objCompany.RollBack();
                            throw ex;
                        }
                        //msgs.InformationalMessage("Company details submited successfully.");

                        //mpeMessage.Show();
                    }

                    if (result > 0)
                    {
                        if (fvCompany.CurrentMode == FormViewMode.Insert)
                        {
                            fvCompany.ChangeMode(FormViewMode.ReadOnly);

                            companyPager.CurrentPage = 0;
                            //BindDatalist();
                          //  EnableMenus();
                            BindFormView(result);

                        }
                        else
                        {
                            fvCompany.ChangeMode(FormViewMode.ReadOnly);
                           // EnableMenus();

                            BindFormView(Convert.ToInt32(fvCompany.DataKey["CompanyID"]));
                        }
                    }
                    else
                    {
                        HtmlContainerControl divBranch = (HtmlContainerControl)fvCompany.FindControl("divBranch");
                        HtmlContainerControl divCompany = (HtmlContainerControl)fvCompany.FindControl("divCompany");
                        HtmlTableRow trAddBranch = (HtmlTableRow)fvCompany.FindControl("trAddBranch");

                        RequiredFieldValidator rfvCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvCompany");
                        RequiredFieldValidator rfvBranch = (RequiredFieldValidator)fvCompany.FindControl("rfvBranch");

                        rfvBranch.Style["display"] = "none";
                        rfvCompany.Style["display"] = "none";

                        if (rbtnCreationMode.SelectedValue == "1")
                        {
                            rfvCompany.ValidationGroup = "Submit";
                            rfvBranch.ValidationGroup = "";

                            divCompany.Style["display"] = "block";
                            divBranch.Style["display"] = "none";

                            trAddBranch.Style["display"] = "none";
                        }
                        else
                        {
                            rfvCompany.ValidationGroup = "";
                            rfvBranch.ValidationGroup = "Submit";

                            divCompany.Style["display"] = "none";
                            divBranch.Style["display"] = "block";

                            if (Request.Browser.Browser == "IE")
                                trAddBranch.Style["display"] = "block";
                            else
                                trAddBranch.Style["display"] = "table-row";

                            txtBranchName.Enabled = true;
                        }
                    }
                }
                break;
            case "_ShowCurrency":

                ddlCurrency = (DropDownList)fvCompany.FindControl("ddlCurrency");
                // controls_CurrencyReference ucCurrency = (controls_CurrencyReference)Page.LoadControl("~/controls/CurrencyReference.ascx");

                //if (ddlCurrency != null && rcCurrency != null)
                //{
                //    rcCurrency.ModalPopupID = "mpeCurrency";
                //    rcCurrency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);
                //    //pnlCurrency.Controls.Add(ucCurrency);
                //    rcCurrency.FillList();
                //    updCurrency.Update();
                //    mpeCurrency.Show();
                //}

                // rcCurrency.CurrencyId = Convert.ToInt32(ddlCurrency.SelectedValue);

                // rcCurrency.FillList();
                // mpeCurrency.Show();

                break;

            case "_Cancel":

                fvCompany.ChangeMode(FormViewMode.ReadOnly);
                BindDatalist();
                break;
        }
    }

    protected void dlCompany_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objCompany = new clsCompany();

        switch (e.CommandName)
        {
            case "_View":

                fvCompany.ChangeMode(FormViewMode.ReadOnly);

                BindFormView(Convert.ToInt32(e.CommandArgument));

                break;

            case "_Edit":

                fvCompany.ChangeMode(FormViewMode.Edit);

                //lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
                //lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

                BindFormView(Convert.ToInt32(e.CommandArgument));

               // txtSearch.Text = string.Empty;
                break;
        }
    }

    protected void dlCompany_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        if (e.Item.ItemType != ListItemType.Header && e.Item.ItemType != ListItemType.Footer)
        {
            HiddenField hfIsCompany = (HiddenField)e.Item.FindControl("hfIsCompany");
            HtmlContainerControl divIsBranch = (HtmlContainerControl)e.Item.FindControl("divIsBranch");
            //LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");

            if (!Convert.ToBoolean(hfIsCompany.Value))
                divIsBranch.Attributes.Add("style", "display:none");
            else
            {
                divIsBranch.Attributes.Add("style", "display:block");
                
            }

           // if (objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Add_Company))
                //btnEdit.Enabled = true;
            //else
            //btnEdit.Enabled = false;
        }
    }

    protected void fuLogo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        AjaxControlToolkit.AsyncFileUpload fuLogo = (AjaxControlToolkit.AsyncFileUpload)fvCompany.FindControl("fuLogo");
        string filename = string.Empty;

        if (fuLogo.HasFile)
        {
            if (fuLogo.PostedFile.ContentType.Contains("image"))
            {
                Image imgPreview = (Image)fvCompany.FindControl("imgPreview");

                filename = Session.SessionID + ".jpg";

                fuLogo.SaveAs(Server.MapPath("~/documents/temp/") + filename);
            }
        }
    }
    protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        AjaxControlToolkit.AsyncFileUpload fuLogo = (AjaxControlToolkit.AsyncFileUpload)fvCompany.FindControl("AsyncFileUpload1");
        string filename = string.Empty;

        if (fuLogo.HasFile)
        {
            if (fuLogo.PostedFile.ContentType.Contains("image"))
            {
                Image imgPreview = (Image)fvCompany.FindControl("imgPreview");

                filename = Session.SessionID + ".jpg";

                fuLogo.SaveAs(Server.MapPath("~/documents/temp/") + filename);
            }
        }
    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        companyPager.CurrentPage = 0;
        BindDatalist();
        this.RegisterAutoComplete();

        upnlFormview.Update();
        upnlDatalist.Update();
    }

    protected void dlBank_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //objCompany = new clsCompany();

        //if (e.Item.ItemIndex != -1 && e.Item.ItemType != ListItemType.Header)
        //{
        //    DropDownList ddlBanks = (DropDownList)e.Item.FindControl("ddlBanks");

        //    ddlBanks.DataSource = objCompany.FillBank();
        //    ddlBanks.DataBind();

        //    ddlBanks.Items.Insert(0, new ListItem("Select", "-1"));

        //    ddlBanks.SelectedIndex = ddlBanks.Items.IndexOf(ddlBanks.Items.FindByValue(Convert.ToString(dlBank.DataKeys[e.Item.ItemIndex])));

        //    //set deletebtn disabled when account is used by any employee

        //    ImageButton ibtnRemoveBank = (ImageButton)e.Item.FindControl("ibtnRemoveBank");
        //    HiddenField hfCompanyAccId = (HiddenField)e.Item.FindControl("hfCompanyAccId");

        //    if (objCompany.CheckAccIdforEmp(Convert.ToInt32(hfCompanyAccId.Value == string.Empty ? "-1" : hfCompanyAccId.Value)))
        //        ibtnRemoveBank.OnClientClick = "alert('Account exists for employee cannot delete.');return false;";
        //    else
        //        ibtnRemoveBank.OnClientClick = "return true;";
        //}
    }

    protected void dlBank_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objCompany = new clsCompany();

        DataTable dtOld, dt;
        DataRow dr;

        DropDownList ddlBanks;
        TextBox txtAccountNo;
        HiddenField hfCompanyAccId;

        switch (e.CommandName)
        {
            case "_AddBank":

                //dtOld = new DataTable();

                //dtOld.Columns.Add("CompanyAccID");
                //dtOld.Columns.Add("BankNameID");
                //dtOld.Columns.Add("Bank");
                //dtOld.Columns.Add("AccountNo");

                ////foreach (DataListItem item in dlBank.Items)
                ////{
                ////    ddlBanks = (DropDownList)item.FindControl("ddlBanks");
                ////    txtAccountNo = (TextBox)item.FindControl("txtAccountNo");
                ////    hfCompanyAccId = (HiddenField)item.FindControl("hfCompanyAccId");

                ////    dr = dtOld.NewRow();

                ////    dr["CompanyAccID"] = Convert.ToString(hfCompanyAccId.Value);
                ////    dr["BankNameID"] = Convert.ToString(ddlBanks.SelectedValue);
                ////    dr["Bank"] = Convert.ToString(ddlBanks.SelectedItem.Text);
                ////    dr["AccountNo"] = txtAccountNo.Text;

                ////    dtOld.Rows.Add(dr);
                ////}

                //dt = dtOld;

                //if (dt == null)
                //{
                //    dt = new DataTable();

                //    dt.Columns.Add("CompanyAccID");
                //    dt.Columns.Add("BankNameID");
                //    dt.Columns.Add("Bank");
                //    dt.Columns.Add("AccountNo");
                //}

                //dr = dt.NewRow();
                //dt.Rows.Add(dr);

                //dlBank.DataSource = dt;
                //dlBank.DataBind();

                //ViewState["Bank"] = dt;

                //break;

            case "_Remove":

                dt = new DataTable();

                //dt.Columns.Add("CompanyAccID");
                //dt.Columns.Add("BankNameID");
                //dt.Columns.Add("Bank");
                //dt.Columns.Add("AccountNo");

                //foreach (DataListItem item in dlBank.Items)
                //{
                //    ddlBanks = (DropDownList)item.FindControl("ddlBanks");
                //    txtAccountNo = (TextBox)item.FindControl("txtAccountNo");
                //    hfCompanyAccId = (HiddenField)item.FindControl("hfCompanyAccId");

                //    dr = dt.NewRow();

                //    dr["CompanyAccID"] = Convert.ToString(hfCompanyAccId.Value);
                //    dr["BankNameID"] = Convert.ToString(ddlBanks.SelectedValue);
                //    dr["Bank"] = ddlBanks.SelectedItem.Text;
                //    dr["AccountNo"] = txtAccountNo.Text;

                //    if (item != e.Item)
                //        dt.Rows.Add(dr);
                //    else
                //    {
                //        if (fvCompany.DataKey["CompanyID"] != DBNull.Value)
                //        {
                //            if (fvCompany.DataKey["CompanyID"] != null)
                //            {
                //                objCompany.CompanyID = Convert.ToInt32(fvCompany.DataKey["CompanyID"]);
                //                objCompany.BankNameId = Convert.ToInt32(ddlBanks.SelectedValue);

                //                if (objCompany.CheckBankExistforCompany())
                //                {
                //                    objCompany.DeleteBank(Convert.ToInt32(hfCompanyAccId.Value));
                //                }
                //            }
                //        }
                //    }

                //    ViewState["Bank"] = dt;
                //}

                //if (dt.Rows.Count > 0)
                //{
                //    dlBank.DataSource = ViewState["Bank"];
                //    dlBank.DataBind();
                //}
                //else
                //{
                //    dr = dt.NewRow();
                //    dt.Rows.Add(dr);

                //    dlBank.DataSource = dt;
                //    dlBank.DataBind();
                //}
                break;
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        objCompany = new clsCompany();

        HiddenField hfCompanyFinYear = (HiddenField)fvCompany.FindControl("hfCompanyFinYear");

        objCompany.CompanyID = Convert.ToInt32(((DropDownList)sender).SelectedValue);
        hfCompanyFinYear.Value = objCompany.GetFinancialYear().ToString("dd/MM/yyyy"); //clsCommon.Convert2DateTime(objCompany.GetFinancialYear()).ToString("dd/MM/yyyy");

        RadioButtonList rbtnCreationMode = (RadioButtonList)fvCompany.FindControl("rbtnCreationMode");
        RequiredFieldValidator rfvCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvCompany");
        RequiredFieldValidator rfvBranch = (RequiredFieldValidator)fvCompany.FindControl("rfvBranch");



        if (rbtnCreationMode.SelectedValue == "1")
        {

            rfvCompany.Style["display"] = "block";
            rfvBranch.Style["display"] = "none";
        }
        else
        {

            rfvBranch.Style["display"] = "block";
            rfvCompany.Style["display"] = "none";
        }



    }

    #endregion

    private void RegisterAutoComplete()
    {
        //clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Company);
    }

    protected void rbtnCreationMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rbtnCreationMode = (RadioButtonList)fvCompany.FindControl("rbtnCreationMode");
        HtmlGenericControl divCompany = (HtmlGenericControl)fvCompany.FindControl("divCompany");
        HtmlGenericControl divBranch = (HtmlGenericControl)fvCompany.FindControl("divBranch");
        HtmlGenericControl trAddBranch = (HtmlGenericControl)fvCompany.FindControl("trAddBranch");
        RequiredFieldValidator rfvBranch = (RequiredFieldValidator)fvCompany.FindControl("rfvBranch");
        RequiredFieldValidator rfvCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvCompany");
        RequiredFieldValidator rfvddlCompany = (RequiredFieldValidator)fvCompany.FindControl("rfvddlCompany");

        if (rbtnCreationMode.SelectedValue == "1")
        {
            divCompany.Style["display"] = "block";
            divBranch.Style["display"] = "none";
            //trAddBranch.Style["display"] = "none";
            rfvBranch.Enabled = false;
            rfvCompany.Enabled = true;
            rfvddlCompany.Enabled = false;
        }
        else
        {
            divCompany.Style["display"] = "none";
            divBranch.Style["display"] = "block";
            //trAddBranch.Style["display"] = "block";
            rfvBranch.Enabled = true;
            rfvCompany.Enabled = false;
            rfvddlCompany.Enabled = true;
        }
        if (Request.Browser.Browser == "IE")
            trAddBranch.Style["display"] = "block";
        else
            trAddBranch.Style["display"] = "table-row";

    }

    //protected void lnkShiftPolicy_Click(object sender, EventArgs e)
    //{
        //FormView3.ChangeMode(FormViewMode.Insert);
        //FormView1.ChangeMode(FormViewMode.Edit);
        //FormView2.ChangeMode(FormViewMode.Edit);

        ////controls_ShiftPolicy ucShiftPolicy = (controls_ShiftPolicy)FormView3.FindControl("ucShiftPolicy");
        ////UpdatePanel updMdlWorkPolicy = (UpdatePanel)FormView1.FindControl("updMdlWorkPolicy");
        //AjaxControlToolkit.ModalPopupExtender mpeShiftPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView3.FindControl("mpeShiftPolicy");
        //if (ucShiftPolicy != null && mpeShiftPolicy != null)
        //{
        //    ucShiftPolicy.ShiftID = -1;
        //    ucShiftPolicy.Fill();
        //    //updMdllShiftPolicy.Update();
        //    mpeShiftPolicy.Show();
        //    //updbtnShiftPolicy.Update();
        //}
        //ucShiftPolicy.ShiftID = -1;
        //ucShiftPolicy.Fill();

        //mpeShiftPolicy.Show();
    //}

    protected void lnkWorkPolicy_Click(object sender, EventArgs e)
    {
        //FormView1.ChangeMode(FormViewMode.Insert);
        //FormView2.ChangeMode(FormViewMode.Edit);
        //FormView3.ChangeMode(FormViewMode.Edit);
        //controls_WorkPolicy ucWorkPolicy = (controls_WorkPolicy)FormView1.FindControl("ucWorkPolicy");
        ////UpdatePanel updMdlWorkPolicy = (UpdatePanel)FormView1.FindControl("updMdlWorkPolicy");
        //AjaxControlToolkit.ModalPopupExtender mpeWorkPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeWorkPolicy");
        //if (ucWorkPolicy != null && mpeWorkPolicy != null)
        //{
        //    ucWorkPolicy.PolicyID = -1;
        //    ucWorkPolicy.Fill();
        //    //updMdlWorkPolicy.Update();
        //    mpeWorkPolicy.Show();
        //    //updbtnWorkPolicy.Update();
        //}
        //mpeWorkPolicy.Show();
    }

    private void LoadLeavePolicy()
    {
        //if (pnlModalPopUp.Controls.Count == 1)
        //{
        //    pnlModalPopUp.Controls.Add(uc_LeavePolicy);
        //    uc_LeavePolicy.LeavePolicyID = -1;
        //    uc_LeavePolicy.ModalPopupID = "mdlPopUpLeave";
        //    uc_LeavePolicy.Fill(); 
        //    updMdlLeavePolicy.Update();
        //    mdlPopUpLeave.Show();
        //}
    }

    

    protected void Button2_Click(object sender, EventArgs e)
    {

    }

    protected void fvCompany_PageIndexChanging(object sender, FormViewPageEventArgs e)
    {

    }

    protected void imgCountry_Click(object sender, ImageClickEventArgs e)
    {

        DropDownList rcCountry = (DropDownList)fvCompany.FindControl("rcCountry");

        if (rcCountry != null)
        {
            //    ReferenceControlNew.TableName = "CountryReference";
            //    ReferenceControlNew.DataTextField = "Description";
            //    ReferenceControlNew.DataValueField = "CountryId";
            //    ReferenceControlNew.FunctionName = "FillComboCountry";
            //    ReferenceControlNew.SelectedValue = rcCountry.SelectedValue;
            //    ReferenceControlNew.DisplayName = "Country";
            //    ReferenceControlNew.PopulateData();

            //    mdlPopUpReference.Show();
            //    updModalPopUp.Update();
        }


    }

    protected void imgCompanyIndustry_Click(object sender, ImageClickEventArgs e)
    {
        DropDownList rcCompanyIndustry = (DropDownList)fvCompany.FindControl("rcCompanyIndustry");
        if (rcCompanyIndustry != null)
        {
            //ReferenceControlNew.ClearAll();
            //ReferenceControlNew.TableName = "IndustryReference";
            //ReferenceControlNew.DataTextField = "Description";
            //ReferenceControlNew.DataValueField = "CompanyIndustryId";
            //ReferenceControlNew.FunctionName = "FillComboCompanyIndustry";
            //ReferenceControlNew.SelectedValue = rcCompanyIndustry.SelectedValue;
            //ReferenceControlNew.DisplayName = "Industry";
            //ReferenceControlNew.PopulateData();

            //mdlPopUpReference.Show();
            //updModalPopUp.Update();
        }

    }

    protected void imgState_Click(object sender, ImageClickEventArgs e)
    {
        DropDownList rcState = (DropDownList)fvCompany.FindControl("rcState");
        if (rcState != null)
        {
            //ReferenceControlNew.ClearAll();
            //ReferenceControlNew.TableName = "ProvinceReference";
            //ReferenceControlNew.DataTextField = "Description";
            //ReferenceControlNew.DataValueField = "ProvinceId";
            //ReferenceControlNew.FunctionName = "FillComboProvince";
            //ReferenceControlNew.SelectedValue = rcState.SelectedValue;
            //ReferenceControlNew.DisplayName = "Province";
            //ReferenceControlNew.PopulateData();

            //mdlPopUpReference.Show();
            //updModalPopUp.Update();
        }
    }

    protected void imgCompanyType_Click(object sender, ImageClickEventArgs e)
    {
        DropDownList rcCompanyType = (DropDownList)fvCompany.FindControl("rcCompanyType");

        if (rcCompanyType != null)
        {
            //ReferenceControlNew.ClearAll();
            //ReferenceControlNew.TableName = "CompanyTypeReference";
            //ReferenceControlNew.DataTextField = "Description";
            //ReferenceControlNew.DataValueField = "CompanyTypeId";
            //ReferenceControlNew.FunctionName = "FillComboCompanyType";
            //ReferenceControlNew.SelectedValue = rcCompanyType.SelectedValue;
            //ReferenceControlNew.DisplayName = "Company Type";
            //ReferenceControlNew.PopulateData();

            //mdlPopUpReference.Show();
            //updModalPopUp.Update();
        }
    }



    protected void imgDesignation_Click(object sender, ImageClickEventArgs e)
    {
        DropDownList rcDesignation = (DropDownList)fvCompany.FindControl("rcDesignation");

        if (rcDesignation != null)
        {
            //ReferenceControlNew.ClearAll();
            //ReferenceControlNew.TableName = "DesignationReference";
            //ReferenceControlNew.DataTextField = "Description";
            //ReferenceControlNew.DataValueField = "DesignationId";
            //ReferenceControlNew.FunctionName = "FillComboDesignation";
            //ReferenceControlNew.SelectedValue = rcDesignation.SelectedValue;
            //ReferenceControlNew.DisplayName = "Designation";
            //ReferenceControlNew.PopulateData();

            //mdlPopUpReference.Show();
            //updModalPopUp.Update();
        }
    }
    protected void imgAddComp_Click(object sender, ImageClickEventArgs e)
    {
        Addcompany();
    }
    protected void lnkAddComp_Click(object sender, EventArgs e)
    {
        Addcompany();
    }
    protected void lnkListCompany_Click(object sender, EventArgs e)
    {
        try
        {
            txtSearch.Text = "";
            txtSearch.Text = string.Empty;
            BindDatalist();
            upnlFormview.Update();
            upnlDatalist.Update();
        }
        catch
        {
        }
    }
    protected void imgListCompany_Click(object sender, ImageClickEventArgs e)
    {
        BindDatalist();
    }
    protected void lnkPrint_Click1(object sender, EventArgs e)
    {
        PrintCompany();
    }

    private void PrintCompany()
    {
        objCompany = new clsCompany();

        if (dlCompany.Items.Count > 0)
        {
            string sCompanyIds = string.Empty;
            foreach (DataListItem item in dlCompany.Items)
            {
                CheckBox chkCompany = (CheckBox)item.FindControl("chkCompany");

                if (chkCompany == null)
                    return;
                if (chkCompany.Checked)
                {
                    if (sCompanyIds == string.Empty)
                        sCompanyIds = Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                    else
                        sCompanyIds += "," + Convert.ToString(dlCompany.DataKeys[item.ItemIndex]);
                }
            }
            Table tbl = objCompany.PrintCompany(sCompanyIds);

            tbl.Width = Unit.Percentage(100);
            tbl.Attributes.Add("Style", "Display:none;");

            pnlSelectedCompanies.Controls.Add(tbl);

            ScriptManager.RegisterStartupScript(lnkPrint, this.GetType(), "Print", "Print('" + tbl.ClientID + "')", true);

            upnlDatalist.Update();
            upnlFormview.Update();
        }
    }
    protected void imgPrint_Click(object sender, ImageClickEventArgs e)
    {
        PrintCompany();
    }

    private void DeleteCompany()
    {
        //string sCompanyId = string.Empty;

        //txtSearch.Text = string.Empty;

        //if (dlCompany.Items.Count > 0)
        //{
        //    foreach (DataListItem item in dlCompany.Items)
        //    {
        //        CheckBox chkCompany = (CheckBox)item.FindControl("chkCompany");

        //        if (chkCompany != null)
        //        {
        //            if (chkCompany.Checked)
        //                sCompanyId = (sCompanyId == string.Empty ? DeleteCompany(Convert.ToInt32(dlCompany.DataKeys[item.ItemIndex])) : sCompanyId + "," + DeleteCompany(Convert.ToInt32(dlCompany.DataKeys[item.ItemIndex])));
        //        }
        //    }
        //}
        //else
        //{
        //    if (fvCompany.CurrentMode == FormViewMode.ReadOnly)
        //        sCompanyId = DeleteCompany(Convert.ToInt32(fvCompany.DataKey["CompanyID"]));
        //}

        //if (sCompanyId == string.Empty)
        //{
        //     fvCompany.DataSource = null;
        //    fvCompany.DataBind();

        //    BindDatalist();

        //    msgs.InformationalMessage("Selected company(s) deleted successfully.");
        //}
        //else
        //{
        //    msgs.InformationalMessage("Cannot delete " + sCompanyId);

        //    BindDatalist();
        //}

        //mpeMessage.Show();

        //upnlDatalist.Update();
        //upnlFormview.Update();
    }
    protected void imgDelete_Click(object sender, ImageClickEventArgs e)
    {
        DeleteCompany();
    }
    protected void lnkDelete_Click1(object sender, EventArgs e)
    {
        DeleteCompany();
       
    }
    //protected void lnkShiftpolicy_Click(object sender, EventArgs e)
    //{
    //    FormView3.ChangeMode(FormViewMode.Insert);
    //    FormView1.ChangeMode(FormViewMode.Edit);
    //    //FormView2.ChangeMode(FormViewMode.Edit);

    //    controls_ShiftPolicy ucShiftPolicy = (controls_ShiftPolicy)FormView3.FindControl("ucShiftPolicy");
    //    //UpdatePanel updMdlWorkPolicy = (UpdatePanel)FormView1.FindControl("updMdlWorkPolicy");
    //    AjaxControlToolkit.ModalPopupExtender mpeShiftPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView3.FindControl("mpeShiftPolicy");
    //    if (ucShiftPolicy != null && mpeShiftPolicy != null)
    //    {
    //        ucShiftPolicy.ShiftID = -1;
    //        ucShiftPolicy.Fill();
    //        updMdllShiftPolicy.Update();
    //        mpeShiftPolicy.Show();
    //        updbtnShiftPolicy.Update();
    //    }
    //}
    //protected void lnkWorkPolicy_Click1(object sender, EventArgs e)
    //{
    //   FormView1.ChangeMode(FormViewMode.Insert);
    //    //FormView2.ChangeMode(FormViewMode.Edit);
    //    FormView3.ChangeMode(FormViewMode.Edit);
    //    controls_WorkPolicy ucWorkPolicy = (controls_WorkPolicy)FormView1.FindControl("ucWorkPolicy");
    //    //UpdatePanel updMdlWorkPolicy = (UpdatePanel)FormView1.FindControl("updMdlWorkPolicy");
    //    AjaxControlToolkit.ModalPopupExtender mpeWorkPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeWorkPolicy");
    //    if (ucWorkPolicy != null && mpeWorkPolicy != null)
    //    {
    //        ucWorkPolicy.PolicyID = -1;
    //        ucWorkPolicy.Fill();
    //        updMdlWorkPolicy.Update();
    //        mpeWorkPolicy.Show();
    //        //updbtnWorkPolicy.Update();
    //    }
    //}
    
    //protected void lnkLeavePolicy_Click(object sender, EventArgs e)
    //{
    //    FormView2.ChangeMode(FormViewMode.Insert);
    //    FormView1.ChangeMode(FormViewMode.Edit);
    //    FormView3.ChangeMode(FormViewMode.Edit);
    //    controls_LeavePolicy ucLeavePolicy = (controls_LeavePolicy)FormView2.FindControl("ucLeavePolicy");
    //    AjaxControlToolkit.ModalPopupExtender mdlPopUpLeave = (AjaxControlToolkit.ModalPopupExtender)FormView2.FindControl("mdlPopUpLeave");
    //    if (ucLeavePolicy != null && mdlPopUpLeave != null)
    //    {
    //        ucLeavePolicy.LeavePolicyID = -1;
    //       ucLeavePolicy.Fill();
    //        updMdlLeavePolicy.Update();
    //        mdlPopUpLeave.Show();
    //    }
    //}
    //protected void lnkExchange_Click(object sender, EventArgs e)
    //{
    //    FormView5.ChangeMode(FormViewMode.Insert);
    //    FormView1.ChangeMode(FormViewMode.Edit);
    //    FormView2.ChangeMode(FormViewMode.Edit);
    //    FormView3.ChangeMode(FormViewMode.Edit);
    //    //FormView4.ChangeMode(FormViewMode.Edit);
    //    controls_CurrencyExchangeRate ucCurrencyExchangeRate = (controls_CurrencyExchangeRate)FormView5.FindControl("ucCurrencyExchangeRate");
    //    AjaxControlToolkit.ModalPopupExtender mdlPopUpCurrencyExchangeRate = (AjaxControlToolkit.ModalPopupExtender)FormView5.FindControl("mdlPopUpCurrencyExchangeRate");
    //    if (ucCurrencyExchangeRate != null && mdlPopUpCurrencyExchangeRate != null)
    //    {
    //        updMdlCurrencyExchangeRate.Update();
    //        ucCurrencyExchangeRate.CurrencyDetailID = 0;
    //        mdlPopUpCurrencyExchangeRate.Show();
    //    }
    //}
}
