﻿<%@ Page Language="C#" MasterPageFile="~/Master/ESSMaster.master" AutoEventWireup="true"
    CodeFile="EmployeeProfile.aspx.cs" Inherits="Public_EmployeeProfile" meta:resourcekey="PageTitle" %>

<%@ Register Src="../controls/ReferenceControlNew.ascx" TagName="ReferenceControlNew"
    TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="public_content" runat="Server">
    <link href="../css/employee.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
 
    function ShowHide()
    {
        var trDisp = document.getElementById("trSettings"); 
        var trProfile = document.getElementById("trProfile");
        
        if(navigator.appName == 'Microsoft Internet Explorer')
        {       
            trProfile.style.display = (trProfile.style.display == "none" ? "block" : "none"); 
            trDisp.style.display = (trDisp.style.display == "block" ? "none" :"block"); 
        }
        else
        {             
            trProfile.style.display = (trProfile.style.display == "none" ? "block" : "none"); 
            trDisp.style.display = (trDisp.style.display == "block" ? "none" :"block");
        }     
    }
 
    function ShowEditProfile()
    {
        var trProfile = document.getElementById("trProfile");   
        var trDisp = document.getElementById("trSettings");
        var trProfileEdit = document.getElementById("divEditProfileDetails"); 
         
        trProfile.style.display = trDisplay();
        trDisp.style.display  = "none";
        trProfileEdit.style.display = "none";
    }
    
    function ShowSettings()
    {
        var trProfile = document.getElementById("trProfile");   
        var trDisp = document.getElementById("trSettings"); 
        var trProfileEdit = document.getElementById("divEditProfileDetails"); 
        
        trProfile.style.display = "none";
        trDisp.style.display  = trDisplay();           
        trProfileEdit.style.display = "none";
    }
    
    function ShowEditProfileEdit()
    {
        var trProfile = document.getElementById("trProfile");   
        var trDisp = document.getElementById("trSettings");
        var trProfileEdit = document.getElementById("divEditProfileDetails"); 
         
        trProfile.style.display = "none";
        trDisp.style.display  = "none";
        trProfileEdit.style.display = trDisplay();
    }
     
    </script>

    <div style="width: 100%;">
        <div>
            <div>
                <div style="display: none">
                    <asp:Button ID="Button1" runat="server" Text="submit" />
                </div>
                <asp:Panel ID="pnlMessage" runat="server" Style="display: none;" BorderWidth="0px"
                    BorderStyle="None">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <uc:Message ID="msgs" runat="server" ModalPopupId="mpeMessage" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <AjaxControlToolkit:ModalPopupExtender ID="mpeMessage" runat="server" TargetControlID="Button1"
                    PopupControlID="pnlMessage" PopupDragHandleControlID="pnlMessage" BackgroundCssClass="modalBackground"
                    Drag="true">
                </AjaxControlToolkit:ModalPopupExtender>
            </div>
        </div>
        <div id="trProfile">
            <div style="width: 100%;">
                <asp:UpdatePanel ID="upEmployees" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:FormView ID="fvEmployee" runat="server" BorderWidth="0px" CellPadding="10" 
                            DataKeyNames="EmployeeID,Gender,NationalityID,ReligionID,OfficialEmailPassword,DegreeID"
                            Width="100%" CellSpacing="10" OnDataBound="fvEmployee_DataBound" OnItemCommand="fvEmployee_ItemCommand">
                            <ItemTemplate>
                                <div class="grid" id="mainwrap">
                                    <div id="main" style="width:100%; background-color:rgb(145, 208, 238)">
                                        <div id="names" style="width: 100%">
                                            <div style="float: left; width: 100%">
                                                <div style="float: left; width: 49%">
                                                    <h3>
                                                        <%# Eval("Salutation")%>
                                                        <%# Eval("FirstName")%>
                                                        <%# Eval("MiddleName")%>
                                                        <%# Eval("LastName")%>
                                                    </h3>
                                                </div>
                                                <div style="float: right; width: 49%; text-align: right;">
                                                    <h3>
                                                        <%# Eval("FirstNameArb")%>
                                                        <%# Eval("MiddleNameArb")%>
                                                        <%# Eval("LastNameArb")%>
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="t1" style="margin-top: 30px">
                                            <div class="ta1">
                                                <div class="common">
                                                    <div class="innerdivheader" style="color: Black">
                                                        <%--  Employee Number--%>
                                                        <asp:Literal ID="Literal111" runat="server" Text='<%$Resources:ControlsCommon,EmployeeNumber%>'>
                                                        </asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader" style="color: Black">
                                                        <%-- Working At--%>
                                                        <asp:Literal ID="Literal5" runat="server" Text='<%$Resources:ControlsCommon,WorkingAt %>'></asp:Literal>
                                                    </div>
                                                    <div class="innerdivheader" style="color: Black">
                                                        <%--  Date Of Joining--%>
                                                        <asp:Literal ID="Literal6" runat="server" Text='<%$Resources:ControlsCommon,DateOfJoining %>'></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="ta2">
                                                <div class="common">
                                                    <div class="innerdivcolon">
                                                        :</div>
                                                    <div class="innerdivcolon">
                                                        :</div>
                                                    <div class="innerdivcolon">
                                                        :
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ta3" style="width: 40%">
                                                <div class="common">
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("EmployeeNumber")%></div>
                                                    <div class="innerdivheaderValue">
                                                        <%# Eval("CompanyName")%>
                                                    </div>
                                                    <div class="innerdivheaderValue">
                                                        <%#   Convert.ToDateTime(Eval("DateofJoining")).ToString("dd-MMM-yyyy")%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ta4">
                                                <div id="tabs" style="float: right;">
                                                    <div id="RecentPhoto">
                                                        <img src='<%# GetEmployeeRecentPhoto(Eval("EmployeeID"),120)%>' style="margin: 2px;" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%--Basic Info --%>
                                                <asp:Literal ID="Literal48" runat="server" Text='<%$Resources:ControlsCommon,BasicInfo %>'></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--Gender--%>
                                                <asp:Literal ID="Literal7" runat="server" Text='<%$Resources:ControlsCommon,Gender %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Eval("Gender") == "" ? "-" : Eval("Gender")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Date of birth--%>
                                                <asp:Literal ID="Literal8" runat="server" Text='<%$Resources:ControlsCommon,DateOfBirth %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Eval("DateOfBirth") == "" ? "-" :Convert.ToDateTime(Eval("DateOfBirth")).ToString("dd-MMM-yyyy") %>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--Country--%>
                                                <asp:Literal ID="Literal9" runat="server" Text='<%$Resources:ControlsCommon,Country %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("CountryName")) == "" ? "-" : Eval("CountryName")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Nationality--%>
                                                <asp:Literal ID="Literal10" runat="server" Text='<%$Resources:ControlsCommon,Nationality %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Nationality")) == "" ? "-" : Eval("Nationality")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Religion--%>
                                                <asp:Literal ID="Literal11" runat="server" Text='<%$Resources:ControlsCommon,Religion %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Religion")) == "" ? "-" : Eval("Religion")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Employment Type--%>
                                                <asp:Literal ID="Literal12" runat="server" Text='<%$Resources:ControlsCommon,EmploymentType %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EmploymentType")) == "" ? "-" : Eval("EmploymentType")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Probation End Date--%>
                                                <asp:Literal ID="Literal13" runat="server" Text='<%$Resources:ControlsCommon,ProbationEndDate %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("ProbationEndDate")) == "" ? "-" : (Eval("ProbationEndDate").ToDateTime()).ToString("dd-MMM-yyyy")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%-- Work Profile --%>
                                                <asp:Literal ID="Literal45" runat="server" meta:resourcekey="WorkProfile"></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--Department--%>
                                                <asp:Literal ID="Literal14" runat="server" Text='<%$Resources:ControlsCommon,Department%>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Department")) == "" ? "-" : Eval("Department")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Desigation--%>
                                                <asp:Literal ID="Literal15" runat="server" Text='<%$Resources:ControlsCommon,Designation %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Designation")) == "" ? "-" : Eval("Designation")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--   Work Policy--%>
                                                <asp:Literal ID="Literal16" runat="server" Text='<%$Resources:ControlsCommon,WorkPolicy %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("WorkPolicy")) == "" ? "-" : Eval("WorkPolicy")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Leave Policy--%>
                                                <asp:Literal ID="Literal17" runat="server" Text='<%$Resources:ControlsCommon,LeavePolicy %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("LeavePolicy")) == "" ? "-" : Eval("LeavePolicy")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Work Location--%>
                                                <asp:Literal ID="Literal18" runat="server" Text='<%$Resources:ControlsCommon,WorkLocation %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("WorkLocation")) == "" ? "-" : Eval("WorkLocation")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--   Work Status--%>
                                                <asp:Literal ID="Literal19" runat="server" Text='<%$Resources:ControlsCommon,WorkStatus %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("WorkStatus")) == "" ? "-" : Eval("WorkStatus")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Official Email--%>
                                                <asp:Literal ID="Literal20" runat="server" Text='<%$Resources:ControlsCommon,OfficialEmail %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("OfficialEmailID")) == "" ? "-" : Eval("OfficialEmailID")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <asp:Literal ID="Literal46" runat="server" meta:resourcekey="PersonalFacts"></asp:Literal>
                                                <%--  Personal Facts--%>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <asp:Literal ID="Literal21" runat="server" meta:resourcekey="FathersName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("FathersName")) == "" ? "-" : Eval("FathersName")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Mother Name--%>
                                                <asp:Literal ID="Literal38" runat="server" meta:resourcekey="MothersName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("MothersName")) == "" ? "-" : Eval("MothersName")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--Spouse Name--%>
                                                <asp:Literal ID="Literal39" runat="server" meta:resourcekey="SpouseName"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("SpouseName")) == "" ? "-" : Eval("SpouseName")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Languages--%>
                                                <asp:Literal ID="Literal40" runat="server" meta:resourcekey="Languages"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("LanguagesKnown")) == "" ? "-" : Eval("LanguagesKnown")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Mother Tongue--%>
                                                <asp:Literal ID="Literal41" runat="server" meta:resourcekey="MotherTongue"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("MotherTongue")) == "" ? "-" : Eval("MotherTongue")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Identification--%>
                                                <asp:Literal ID="Literal42" runat="server" meta:resourcekey="Identification"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("IdentificationMarks")) == "" ? "-" : Eval("IdentificationMarks")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Blood Group--%>
                                                <asp:Literal ID="Literal43" runat="server" meta:resourcekey="BloodGroup"></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("BloodGroup")) == "" ? "-" : Eval("BloodGroup")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%--  Accounts & Bank Identity --%>
                                                <asp:Literal ID="Literal44" runat="server" meta:resourcekey="AccountInfo"></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Transaction Type--%>
                                                <asp:Literal ID="Literal22" runat="server" Text='<%$Resources:ControlsCommon,TransactionType %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("TransactionType")) == "" ? "-" : Eval("TransactionType")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Employer Bank Name--%>
                                                <asp:Literal ID="Literal23" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankName %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("EmployerBank")) == "" ? "-" : Eval("EmployerBank")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Employer Bank A/C--%>
                                                <asp:Literal ID="Literal24" runat="server" Text='<%$Resources:ControlsCommon,EmployerBankAcc %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EmployerAccountNo")) == "" ? "-" : Eval("EmployerAccountNo")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Employee Bank Name--%>
                                                <asp:Literal ID="Literal25" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankName %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("EmployeeBank")) == "" ? "-" : Eval("EmployeeBank")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Employee Bank A/C--%>
                                                <asp:Literal ID="Literal26" runat="server" Text='<%$Resources:ControlsCommon,EmployeeBankAcc %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EmployeeAccountNo")) == "" ? "-" : Eval("EmployeeAccountNo")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%--  Local Contact--%>
                                                <asp:Literal ID="Literal211" runat="server" Text='<%$Resources:ControlsCommon,LocalInfo %>'></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Address--%>
                                                <asp:Literal ID="Literal27" runat="server" Text='<%$Resources:ControlsCommon,LocalAddress %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("LocalAddress")) == "" ? "-" : Eval("LocalAddress")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Phone--%>
                                                <asp:Literal ID="Literal28" runat="server" Text='<%$Resources:ControlsCommon,LocalPhone %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("LocalPhone")) == "" ? "-" : Eval("LocalPhone")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Mobile--%>
                                                <asp:Literal ID="Literal29" runat="server" Text='<%$Resources:ControlsCommon,LocalMobile %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Mobile")) == "" ? "-" : Eval("Mobile")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Other Info--%>
                                                <asp:Literal ID="Literal30" runat="server" Text='<%$Resources:ControlsCommon,OtherInfo %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("Remarks")) == "" ? "-" : Eval("Remarks")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%-- Emergency Contact--%>
                                                <asp:Literal ID="Literal31" runat="server" Text='<%$Resources:ControlsCommon,EmergencyInfo %>'></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Emergency Address--%>
                                                <asp:Literal ID="Literal32" runat="server" Text='<%$Resources:ControlsCommon,EmergencyAddress %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("EmergencyAddress")) == "" ? "-" : Eval("EmergencyAddress")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Emergency Phone--%>
                                                <asp:Literal ID="Literal33" runat="server" Text='<%$Resources:ControlsCommon,EmergencyPhone %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EmergencyPhone")) == "" ? "-" : Eval("EmergencyPhone")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%--Permanent Contact--%>
                                                <asp:Literal ID="Literal34" runat="server" Text='<%$Resources:ControlsCommon,PermanentInfo%>'></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Address--%>
                                                <asp:Literal ID="Literal35" runat="server" Text='<%$Resources:ControlsCommon,AddressLine1 %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("AddressLine1")) == "" ? "-" : Eval("AddressLine1")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Address--%>
                                                <asp:Literal ID="Literal52" runat="server" Text='<%$Resources:ControlsCommon,AddressLine2 %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%; word-break: break-all;">
                                                <%# Convert.ToString(Eval("AddressLine2")) == "" ? "-" : Eval("AddressLine2")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Address--%>
                                                <asp:Literal ID="Literal53" runat="server" Text='<%$Resources:ControlsCommon,City %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("City")) == "" ? "-" : Eval("City")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Address--%>
                                                <asp:Literal ID="Literal54" runat="server" Text='<%$Resources:ControlsCommon,State %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("StateProvinceRegion")) == "" ? "-" : Eval("StateProvinceRegion")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Address--%>
                                                <asp:Literal ID="Literal55" runat="server" Text='<%$Resources:ControlsCommon,Zip%>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("ZipCode")) == "" ? "-" : Eval("ZipCode")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Permanent Phone--%>
                                                <asp:Literal ID="Literal36" runat="server" Text='<%$Resources:ControlsCommon,PermanentPhone %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("Phone")) == "" ? "-" : Eval("Phone")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%-- Permanent Email--%>
                                                <asp:Literal ID="Literal37" runat="server" Text='<%$Resources:ControlsCommon,PermanentEmail %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EmailID")) == "" ? "-" : Eval("EmailID")%>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <%--Vacation Info--%>
                                                <asp:Literal ID="Literal49" runat="server" Text='<%$Resources:ControlsCommon,VacationInfo%>'></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Eligible Leaves--%>
                                                <asp:Literal ID="Literal50" runat="server" Text='<%$Resources:ControlsCommon,EligibleLeaves %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("EligibleLeaves")) == "" ? "-" : Eval("EligibleLeaves")%>
                                            </div>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div style="float: left; width: 20%;">
                                                <%--  Vacation2--%>
                                                <asp:Literal ID="Literal51" runat="server" Text='<%$Resources:ControlsCommon,TotalEligibleLeavePayDays %>'></asp:Literal>
                                            </div>
                                            <div style="float: left; width: 5%;">
                                                :
                                            </div>
                                            <div style="float: left; width: 70%;">
                                                <%# Convert.ToString(Eval("TotalEligibleLeavePayDays")) == "" ? "-" : Eval("TotalEligibleLeavePayDays")%>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Display Employee KPI/KRA Details !-->
                                    <%-- <div style="width: 100%;" class="labeltext">
                                        <div style="float: left; width: 100%">
                                            <b style="padding-bottom: 5px;" class="listHeader">
                                                <asp:Literal ID="Literal52" runat="server" Text="KPI/KRA"></asp:Literal>
                                            </b>
                                        </div>
                                        <div style="float: left; width: 100%; margin-left: 20px;">
                                            <div id="divKPI" runat="server" style="float: left; width: 47%;">
                                                <asp:GridView ID="gvKPI" runat="server" AutoGenerateColumns="false" Width="100%"
                                                    CellPadding="10" BorderColor="#307296" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Kpi" HeaderText="KPI" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="KPIValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <div id="divKRA" runat="server" style="float: left; width: 47%; padding-left: 10px;">
                                                <asp:GridView ID="gvKRA" runat="server" AutoGenerateColumns="false" Width="100%"
                                                    CellPadding="10" BorderColor="#307296" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderStyle CssClass="datalistheader" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Kra" HeaderText="KRA" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:BoundField DataField="KRAValue" HeaderText="Value" ItemStyle-HorizontalAlign="Center"/>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="trSettings" style="display: none; width: 100%" class="labeltext">
            <div style="width: 100%">
                <asp:UpdatePanel ID="upSettings" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSettings" runat="server" Width="100%">
                            <div style="width: 100%">
                                <div style="float: left; width: 100%;">
                                    <b style="padding-bottom: 5px;" class="listHeader">
                                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="ChangePassword"></asp:Literal>
                                    </b>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Password"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="50%" CssClass="textbox_mandatory">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                            Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"
                                            meta:resourcekey="EnterPassword">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="ConfirmPassword"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtConPassword" runat="server" TextMode="Password" CssClass="textbox_mandatory"
                                            Width="50%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvConPassword" runat="server" ControlToValidate="txtConPassword"
                                            Display="Dynamic" SetFocusOnError="True" CssClass="error" ValidationGroup="Submit"
                                            meta:resourcekey="EnterConfirmpassword"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="cmpPassword" runat="server" ControlToValidate="txtConPassword"
                                            meta:resourcekey="PasswordMatch" ControlToCompare="txtPassword" ValidationGroup="Submit"
                                            CssClass="error">
                                        </asp:CompareValidator>
                                    </div>
                                </div>
                                <div class="TabbedPanelsContent">
                                    <div style="clear: both; height: 1px;">
                                    </div>
                                    <div class="trLeft" style="float: left; width: 38%; height: 29px">
                                    </div>
                                    <div class="trRight" style="float: left; width: 40%; height: 29px">
                                        <asp:Button ID="btnPasswordSubmit" runat="server" CssClass="btnsubmit" ValidationGroup="Submit"
                                            Text='<%$Resources:ControlsCommon,Submit%>' Width="75px" OnClick="btnPasswordSubmit_Click" />
                                        <asp:Button ID="btnCancelPassword" runat="server" CssClass="btnsubmit" CausesValidation="false"
                                            Text='<%$Resources:ControlsCommon,Cancel%>' Width="75px" OnClientClick="ShowHide();" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div id="divEditProfileDetails" style="display: none; width: 100%" class="labeltext">
            <div style="width: 100%">
                <asp:UpdatePanel ID="upEditProfileDetails" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel ID="pnlEditProfileDetails" runat="server" Width="100%">
                            <div style="width: 100%">
                                <div style="float: left; width: 100%;">
                                    <b style="padding-bottom: 5px;" class="listHeader">
                                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="LocalContact"></asp:Literal>
                                    </b>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Phone"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtPhone" runat="server" CssClass="textbox" MaxLength="39" Width="50%"
                                            Text='<%# Eval("Phone") %>'></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtPhone" runat="server" FilterType="Numbers,Custom"
                                            TargetControlID="txtPhone" ValidChars="+ ()-">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Mobile"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtMobile" runat="server" CssClass="textbox" MaxLength="39" Width="50%"
                                            Text='<%# Eval("Mobile") %>'></asp:TextBox>
                                        <AjaxControlToolkit:FilteredTextBoxExtender ID="ftetxtMobile" runat="server" FilterType="Numbers,Custom"
                                            TargetControlID="txtMobile" ValidChars="+ ()-">
                                        </AjaxControlToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Email"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="textbox" Width="50%" MaxLength="50"
                                            Text='<%# Eval("EmailID")%>'></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1pnlTab5" runat="server"
                                            ControlToValidate="txtEmail" CssClass="error" Display="Dynamic" meta:resourcekey="ValidEmail"
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Employee">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div style="float: left; width: 100%; margin-left: 20px;">
                                    <div style="float: left; width: 20%; margin-bottom: 1%;">
                                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="LocalAddress"></asp:Literal>
                                    </div>
                                    <div style="float: left; width: 70%;">
                                        <asp:TextBox ID="txtAddress" TextMode="MultiLine" runat="server" CssClass="textbox"
                                            MaxLength="100" Width="50%" Height="60px" Text='<%# Eval("Address") %>' onchange="RestrictMulilineLength(this, 100);"
                                            onkeyup="RestrictMulilineLength(this, 100);">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="TabbedPanelsContent">
                                    <div style="clear: both; height: 1px;">
                                    </div>
                                    <div class="trLeft" style="float: left; width: 38.5%; height: 29px">
                                    </div>
                                    <div class="trRight" style="float: left; width: 40%; height: 29px">
                                        <asp:Button ID="btnSubmitEditProfile" CssClass="btnsubmit" runat="server" ToolTip="Submit"
                                            CommandName="SUBMIT" Text='<%$Resources:ControlsCommon,Submit%>' ValidationGroup="Employee"
                                            OnClick="btnSubmitEditProfile_Click" Width="75px" />
                                        <asp:Button ID="btnCancelEditProfile" CssClass="btnsubmit" runat="server" ToolTip="Cancel"
                                            CausesValidation="false" Text='<%$Resources:ControlsCommon,Cancel%>' CommandName="_Cancel"
                                            Width="75px" OnClick="btnCancelEditProfile_Click" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="updModalPopUp" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="display: none">
                <asp:Button ID="btn" runat="server" />
            </div>
            <div id="pnlModalPopUp" runat="server" style="display: none;">
                <uc1:ReferenceControlNew ID="ReferenceControlNew1" runat="server" ModalPopupID="mdlPopUpReference" />
            </div>
            <AjaxControlToolkit:ModalPopupExtender ID="mdlPopUpReference" runat="server" TargetControlID="btn"
                PopupControlID="pnlModalPopUp" BackgroundCssClass="modalBackground">
            </AjaxControlToolkit:ModalPopupExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="menu_content" runat="Server">
    <asp:UpdatePanel ID="upMenu" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgEdit" ImageUrl="~/images/New_Employee_Big.PNG" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkEdit_Click" OnClientClick="ShowEditProfile();"
                                meta:resourcekey="ViewProfile"> 
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgEditProfile" ImageUrl="~/images/Edit_Employee_Big.PNG" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkEditProfile" runat="server" OnClick="lnkEditProfile_Click"
                                OnClientClick="ShowEditProfileEdit();" meta:resourcekey="EditProfile"> 
                            </asp:LinkButton></h5>
                    </div>
                </div>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgChange" ImageUrl="~/images/ChangePassword.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkChange" runat="server" OnClick="lnkChange_Click" OnClientClick="ShowSettings();"
                                meta:resourcekey="ChangePassword"> 
                            </asp:LinkButton></h5>
                    </div>
                </div>
              <%--  <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/Payslip.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkPaySlip" runat="server" OnClick="lnkPaySlip_Click" meta:resourcekey="PaySlip"> 
                            </asp:LinkButton></h5>
                    </div>
                </div>--%>
                <div class="mainright">
                    <div class="mainrightimg">
                        <asp:ImageButton ID="imgListAllDocuments" ImageUrl="~/images/Document_list.png" runat="server" />
                    </div>
                    <div class="mainrightname">
                        <h5>
                            <asp:LinkButton ID="lnkListAllDocuments" runat="server" meta:resourcekey="MyDocuments"
                                OnClick="lnkListAllDocuments_Click">              
                            </asp:LinkButton>
                        </h5>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
