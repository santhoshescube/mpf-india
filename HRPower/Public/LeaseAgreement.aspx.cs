﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using System.Reflection;
/// <summary>
/// Summary description for clsLeaseAgreement
/// Created By:    Sanju
/// Created Date:  22-Oct-2013
/// Description:   Lease Agreement
/// </summary>
public partial class Public_LeaseAgreement : System.Web.UI.Page
{
    #region DECLARETIONS

    clsLeaseAgreement objclsLeaseAgreement;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    clsDocumentAlert objAlert;
    private string CurrentSelectedValue { get; set; }

    #endregion
    #region EVENTS
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = GetLocalResourceObject("LeaseTitle.Title").ToString();

        LeaseAgreementPager.Fill += new controls_Pager.FillPager(BindDataList);
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, HRAutoComplete.AutoCompletePage.LeaseAgreement , objUser.GetCompanyId());
        ReferenceControlNew1.OnSave -= new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        ReferenceControlNew1.OnSave += new controls_ReferenceControlNew.saveHandler(ReferenceControlNew1_OnSave);
        if (!IsPostBack)
        {
            objclsLeaseAgreement = new clsLeaseAgreement();
            SetPermission();
            fillCombo();
            DataTable datData = objclsLeaseAgreement.GetLeaseAgreementCount();
            if (datData.Rows.Count > 0)
            {
                if (datData.Rows[0]["Count"].ToInt32() > 0)
                {
                    lnkView_Click(null, null);
                }
                else
                {
                    if (lnkAdd.Enabled)
                    {
                        setDivVisibility(1);
                    }
                    else
                    {
                        setDivVisibility(4);
                    }
                }
            }
            else
            {
                if (lnkAdd.Enabled)
                {
                    setDivVisibility(1);
                }
                else
                {
                    setDivVisibility(4);
                }
            }

            enableAddViewButtons();
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        FillParameters(objclsLeaseAgreement);
        if (objclsLeaseAgreement.intCompanyID <= 0)
        {
            msgs.InformationalMessage(GetLocalResourceObject("PleaseselectaCompany.Text").ToString());
            mpeMessage.Show();
            return;
        }
        if (objclsLeaseAgreement.strAgreementNo == "")
        {
            msgs.InformationalMessage(GetLocalResourceObject("PleaseenterAgreementnumber.Text").ToString());
            mpeMessage.Show();
            return;
        }
        if (objclsLeaseAgreement.isAgreementNumberExist())
        {
            msgs.InformationalMessage(GetLocalResourceObject("Agreementnumberalreadyexist.Text").ToString());
            mpeMessage.Show();
            return;
        }
        if (objclsLeaseAgreement.strLessor == "")
        {
            msgs.InformationalMessage(GetLocalResourceObject("Pleaseenterlessor.Text").ToString());
            mpeMessage.Show();
            return;
        }
        if (objclsLeaseAgreement.dteStartDate > objclsLeaseAgreement.dteEndDate)
        {
            msgs.InformationalMessage(GetLocalResourceObject("StartdategreaterthanEnddate.Text").ToString());
            mpeMessage.Show();
            return;
        }
        DataTable datData = objclsLeaseAgreement.SaveLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            if (datData.Rows[0]["ID"].ToInt32() > 0)
            {
                objAlert = new clsDocumentAlert();
                objAlert.AlertMessage((int)DocumentType.Lease_Agreement, "Lease Agreement", "Agreement Number", datData.Rows[0]["ID"].ToInt32(), objclsLeaseAgreement.strAgreementNo, objclsLeaseAgreement.dteEndDate, "Comp", objclsLeaseAgreement.intCompanyID, ddlCompany.SelectedItem.Text, true);
                clsLeaseAgreement objclsLeaseAgreementDocument = new clsLeaseAgreement();
                Label lblDocname, lblActualfilename, lblFilename;

                foreach (DataListItem item in dlListDoc.Items)
                {
                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");
                    lblActualfilename = (Label)item.FindControl("lblActualfilename");
                    objclsLeaseAgreementDocument.intLeaseAgreementID = datData.Rows[0]["ID"].ToInt32();
                    objclsLeaseAgreementDocument.strAgreementNo = txtAgreementNumber.Text.Trim();
                    objclsLeaseAgreementDocument.strDocname = lblDocname.Text.Trim();
                    objclsLeaseAgreementDocument.strFilename = lblFilename.Text.Trim();

                    if (dlListDoc.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objclsLeaseAgreementDocument.InsertLeaseAgreementTreemaster();

                        string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";
                        //clsCommon.WriteToLog("Physical path is" + Path);
                        if (File.Exists(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString())))
                        {
                            if (!Directory.Exists(Path))
                                Directory.CreateDirectory(Path);
                            //clsCommon.WriteToLog("Directory is created");
                            File.Move(Server.MapPath("~/documents/temp/" + lblFilename.Text.Trim().ToString()), Path + "/" + lblFilename.Text.Trim().ToString());
                            //clsCommon.WriteToLog("File moved successfully");
                        }
                    }
                }
                if (objclsLeaseAgreement.intLeaseAgreementID > 0)
                {
                    msgs.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "UpdatedSuccessfully").ToString());
                }
                else
                {
                    msgs.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "SavedSuccessfully").ToString());
                }
                mpeMessage.Show();
                lnkView_Click(null, null);
            }
        }

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lnkView_Click(null, null);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        if (!lnkAdd.Enabled)
        {
            setDivVisibility(4);
        }
        else
        {
            ClearControls();
            setDivVisibility(1);
        }
    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        LeaseAgreementPager.CurrentPage = 0;
        hfLeaseAgreementID.Value = "";
        txtSearch.Text = "";
        BindDataList();
    }
    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        string message = string.Empty;
        objclsLeaseAgreement = new clsLeaseAgreement();

        if (hfLeaseAgreementID.Value.ToInt32() > 0)
        {

            objclsLeaseAgreement.intLeaseAgreementID = hfLeaseAgreementID.Value.ToInt32();

            if (objclsLeaseAgreement.isLeaseAgreementDeletePossible())
            {
                msgs.InformationalMessage(GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString());
                mpeMessage.Show();
                return;
            }
            else
            {
                objclsLeaseAgreement.DeleteLeaseAgreement();
                message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString();//"Lease Agreement deleted successfully";
            }

        }
        else
        {
            foreach (DataListItem item in dlLeaseAgreement.Items)
            {
                CheckBox chkLeaseAgreement = (CheckBox)item.FindControl("chkLeaseAgreement");
                if (chkLeaseAgreement == null)
                    continue;
                if (chkLeaseAgreement.Checked)
                {
                    objclsLeaseAgreement.intLeaseAgreementID = dlLeaseAgreement.DataKeys[item.ItemIndex].ToInt32();
                    if (objclsLeaseAgreement.isLeaseAgreementDeletePossible())
                    {
                        message = GetGlobalResourceObject("DocumentsCommon", "Onceissuedorrenewed").ToString();//"Some deletion not possible,Lease Agreement(s) renewed or once issued.";
                    }
                    else
                    {
                        objclsLeaseAgreement.DeleteLeaseAgreement();
                        message = GetGlobalResourceObject("DocumentsCommon", "Deletedsuccessfully").ToString(); //"Lease Agreement deleted successfully";

                    }
                }
            }
        }
        if (message != string.Empty)
        {
            msgs.InformationalMessage(message);
            mpeMessage.Show();
        }
        lnkView_Click(null, null);
    }
    protected void dlLeaseAgreement_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {

            case "_View":

                singleViewLeaseAgreement(e.CommandArgument.ToInt32());
                break;

            case "_Edit":

                fillUpdateLeaseAgreement(e.CommandArgument.ToInt32());
                break;

        }
    }
    protected void dlLeaseAgreement_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item | e.Item.ItemType == ListItemType.AlternatingItem)
        {
            //    HiddenField hfIsRenewed = (HiddenField)e.Item.FindControl("hfIsRenewed");
            //    //HiddenField hdStatusID = (HiddenField)e.Item.FindControl("hfStatusID");
            //    Int32 intStatusID = hfIsRenewed.Value.ToInt32();
            //    bool blnIsRenewed = hfIsRenewed.Value == "True" ? true : false;

            ImageButton lbtnEdit = (ImageButton)e.Item.FindControl("btnEdit");
            //    if (blnIsRenewed)
            //    {
            //        lbtnEdit.Enabled = false;
            //        lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            //    }


            DataTable dtm = (DataTable)ViewState["Permission"];

            if (dtm.Rows.Count > 0)
            {
                if (!dtm.Rows[0]["IsUpdate"].ToBoolean())
                {
                    lbtnEdit.Enabled = false;
                    lbtnEdit.ImageUrl = "~/images/edit_disable.png";
                }
            }
            else
            {
                lbtnEdit.Enabled = false;
                lbtnEdit.ImageUrl = "~/images/edit_disable.png";
            }
        }
    }
    protected void lnkPrint_Click(object sender, EventArgs e)
    {

        if (hfLeaseAgreementID.Value.ToInt32() > 0)
        {
            objclsLeaseAgreement = new clsLeaseAgreement();
            objclsLeaseAgreement.intLeaseAgreementID = hfLeaseAgreementID.Value.ToInt32();
            string strData = "";
            strData = GetLeaseAgreementPrint(hfLeaseAgreementID.Value.ToInt32());
            if (strData != "")
            {
                divPrint.InnerHtml = strData;
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

            }

        }
        else
        {
            if (dlLeaseAgreement.Items.Count > 0)
            {

                CheckBox chkLeaseAgreement;
                string strLeaseAgreementIDs = string.Empty;

                foreach (DataListItem item in dlLeaseAgreement.Items)
                {
                    chkLeaseAgreement = (CheckBox)item.FindControl("chkLeaseAgreement");

                    if (chkLeaseAgreement == null)
                        continue;

                    if (chkLeaseAgreement.Checked == false)
                        continue;

                    strLeaseAgreementIDs += "," + dlLeaseAgreement.DataKeys[item.ItemIndex];
                }
                if (strLeaseAgreementIDs != "")
                {
                    strLeaseAgreementIDs = strLeaseAgreementIDs.Remove(0, 1);

                    objclsLeaseAgreement = new clsLeaseAgreement();
                    string strData = "";
                    if (strLeaseAgreementIDs.Contains(","))
                    {
                        strData = GetMultipleLeaseAgreementPrint(strLeaseAgreementIDs);
                    }
                    else
                    {
                        objclsLeaseAgreement.intLeaseAgreementID = strLeaseAgreementIDs.ToInt32();
                        strData = GetLeaseAgreementPrint(strLeaseAgreementIDs.ToInt32());
                    }
                    if (strData != "")
                    {
                        divPrint.InnerHtml = strData;
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);

                    }
                }


            }

        }
    }

    private string GetLeaseAgreementPrint(int LeaseID)
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intLeaseAgreementID = LeaseID;
        DataTable datData = objclsLeaseAgreement.GetLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, Convert.ToString(GetLocalResourceObject("LeaseAgreementDetails.Text")), 1);
        }
        return "";

    }

    private  string GetMultipleLeaseAgreementPrint(string MultipleAgreementIDs)
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        DataTable datData = objclsLeaseAgreement.GetMultipleLeaseAgreement(MultipleAgreementIDs);
        if (datData.Rows.Count > 0)
        {
            return GetDocumentHtml(datData, "Lease Agreement", 2);
        }
        return "";

    }
    private string GetDocumentHtml(DataTable datSource, string MstrCaption, int intMode)
    {
        StringBuilder MsbHtml = new StringBuilder();

        string strHeaderStyle = "style='width:98%;padding-top:20px;height:30px;float:left;font-weight: bold; #FFFFFF, 207); font-size:16px; color:#000000;text-align:center' ";
        string strSubHeaderStyle = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000'";

        string strSubHeaderStyle4 = "style='width:97%;padding-left:1%;margin-left:1%;margin-right:1%;padding-top:7px;height:23px;float:left;font-weight: bold; background-color:#FFFFFF; font-size:14px; color:#000000; border-bottom: 1px dashed #cdcece;'";

        string strHeaderRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;font-weight: bold; border-bottom: 1px dashed #cdcece;'";
 
        string strHeaderRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left; font-size:12px; color:#000000''";


     
        string strAltRowStyle = "style='width:96%;padding-left:2%;margin-left:1%;margin-right:1%;padding-top:5px;height:auto; min-height:20px;float:left;background-color:#FFFFFF;'";

    
        string strAltRowInnerLeftStyle = "style='width:30%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

      
        string strAltRowInnerRightStyle = "style='width:70%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

     
        string strAltRowInner10Style = "style='width:10%;height:auto; min-height:20px;float:left;font-size:12px; word-break:break-all; color:#000000'";

        if (datSource.Rows.Count <= 0)
        {
            return "";
        }
        MsbHtml.Append("<div " + strHeaderStyle + ">" + MstrCaption + "</div>");

        if (intMode == 1)
        {
            DataRow DrData;
            DrData = datSource.Rows[0];
            MsbHtml.Append("<div   " + strSubHeaderStyle4 + ">" + GetGlobalResourceObject("DocumentsCommon", "Company").ToString() + " : " + DrData["CompanyName"].ToStringCustom() + "</div>");
            MsbHtml.Append("<div   " + strSubHeaderStyle + ">" + GetLocalResourceObject("LeaseAgreementDetails.Text").ToString() + "</div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("AgreementNumber.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["AgreementNo"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Lessor.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Lessor"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Startdate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["StartDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Enddate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["EndDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("DocumentDate.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["DocumentDate"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeasePeriod.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeasePeriod"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("RentalValue.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["RentalValue"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("DepositHeld.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["DepositHeld"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Tenant.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Tenant"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("ContactNumbers.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["ContactNumbers"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Address.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["Address"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LandLord.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["Landlord"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("UnitNumber.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["UnitNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("LeaseUnitType.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["LeaseUnitType"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("BuildingNumber.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["BuildingNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Location.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["Location"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("PlotNumber.Text").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["PlotNumber"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetLocalResourceObject("Total.Text").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Total"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "Renewed").ToString() + " </div><div " + strAltRowInnerRightStyle + ">" + DrData["IsRenewed"].ToStringCustom() + "</div></div>");
            MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInnerLeftStyle + ">" + GetGlobalResourceObject("DocumentsCommon", "OtherInfo").ToString() + "</div><div " + strAltRowInnerRightStyle + ">" + DrData["Remarks"].ToStringCustom() + "</div></div>");

        }
        else if (intMode == 2)
        {

            MsbHtml.Append("<div " + strHeaderRowStyle + "><div " + strHeaderRowInner10Style + ">" + GetGlobalResourceObject("DocumentsCommon", "Company").ToString() + " </div> <div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("AgreementNumber.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Lessor.Text").ToString() + "</div> <div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Tenant.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("RentalValue.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("ContactNumbers.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Startdate.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Enddate.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("LeasePeriod.Text").ToString() + "</div><div " + strHeaderRowInner10Style + ">" + GetLocalResourceObject("Documentdate.Text").ToString() + "</div></div>");

            foreach (DataRow drCurrentRow in datSource.Rows)
            {

                MsbHtml.Append("<div " + strAltRowStyle + "><div " + strAltRowInner10Style + ">" + drCurrentRow["CompanyName"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["AgreementNo"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Tenant"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["Lessor"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["RentalValue"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["ContactNumbers"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["StartDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["EndDate"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["LeasePeriod"].ToStringCustom() + "</div><div " + strAltRowInner10Style + ">" + drCurrentRow["DocumentDate"].ToStringCustom() + "</div></div>");


            }

        }

        return MsbHtml.ToString();

    }
    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        if (hfLeaseAgreementID.Value.ToInt32() > 0)
        {
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=LeaseAgreement&LeaseAgreementID=" + hfLeaseAgreementID.Value.ToStringCustom() + "', 835, 585);", true);
        }
        else
        {
            if (dlLeaseAgreement.Items.Count > 0)
            {

                CheckBox chkLeaseAgreement;
                string strLeaseAgreementIDs = string.Empty;

                foreach (DataListItem item in dlLeaseAgreement.Items)
                {
                    chkLeaseAgreement = (CheckBox)item.FindControl("chkLeaseAgreement");

                    if (chkLeaseAgreement == null)
                        continue;

                    if (chkLeaseAgreement.Checked == false)
                        continue;

                    strLeaseAgreementIDs += "," + dlLeaseAgreement.DataKeys[item.ItemIndex];
                }
                if (strLeaseAgreementIDs != "")
                {
                    strLeaseAgreementIDs = strLeaseAgreementIDs.Remove(0, 1);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=LeaseAgreement&LeaseAgreementID=" + strLeaseAgreementIDs + "', 835, 585);", true);
                }


            }

        }
    }
    //protected void lnkRenew_Click(object sender, EventArgs e)
    //{
    //    objclsLeaseAgreement = new clsLeaseAgreement();

    //    if (hfLeaseAgreementID.Value.ToInt32() > 0)
    //    {

    //        objclsLeaseAgreement.intLeaseAgreementID = hfLeaseAgreementID.Value.ToInt32();
    //        objclsLeaseAgreement.RenewLeaseAgreement();
    //        msgs.InformationalMessage("Lease Agreement renewed.");
    //        mpeMessage.Show();
    //        lnkView_Click(null, null);
    //    }
    //}
    //protected void lnkDocumentReceipt_Click(object sender, EventArgs e)
    //{
    //}
    //protected void dlPassportDoc_ItemCommand(object source, DataListCommandEventArgs e)
    //{
    //    switch (e.CommandName)
    //    {
    //        case "REMOVE_ATTACHMENT":
    //            Label lblDocname, lblFilename;
    //            objEmployeePassport = new clsEmployeePassport();

    //            if (e.CommandArgument != string.Empty)
    //            {
    //                objEmployeePassport.Node = Convert.ToInt32(e.CommandArgument);
    //                lblFilename = (Label)e.Item.FindControl("lblFilename");
    //                string sFilename = lblFilename.Text.Trim();
    //                objEmployeePassport.DeletePassportDocument();
    //                string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

    //                if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
    //                    File.Delete(Path + "/" + Convert.ToString(sFilename));
    //            }

    //            objEmployeePassport.PassportID = -1;
    //            objEmployeePassport.EmployeeID = -1;
    //            DataTable dt = objEmployeePassport.GetPassportDocuments().Tables[0];
    //            DataRow dw;

    //            foreach (DataListItem item in dlPassportDoc.Items)
    //            {
    //                if (e.Item.ItemIndex == item.ItemIndex)
    //                    continue;

    //                dw = dt.NewRow();

    //                lblDocname = (Label)item.FindControl("lblDocname");
    //                lblFilename = (Label)item.FindControl("lblFilename");

    //                dw["Node"] = dlPassportDoc.DataKeys[item.ItemIndex];
    //                dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
    //                dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
    //                dt.Rows.Add(dw);
    //            }

    //            dlPassportDoc.DataSource = dt;
    //            dlPassportDoc.DataBind();
    //            ViewState["ListDoc"] = dt;
    //            break;
    //    }
    //}
    protected void dlListDoc_ItemCommand(object source, DataListCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "REMOVE_ATTACHMENT":
                Label lblDocname, lblFilename;
                objclsLeaseAgreement = new clsLeaseAgreement();

                if (e.CommandArgument != string.Empty)
                {
                    objclsLeaseAgreement.intNode = e.CommandArgument.ToInt32();
                    lblFilename = (Label)e.Item.FindControl("lblFilename");
                    string sFilename = lblFilename.Text.Trim();
                    objclsLeaseAgreement.DeleteLeaseAgreementDocument();
                    string Path = ConfigurationManager.AppSettings["_PHY_PATH"] + "DocumentImages";

                    if (File.Exists(Path + "/" + Convert.ToString(sFilename)))
                        File.Delete(Path + "/" + Convert.ToString(sFilename));
                }
                DataTable dt = objclsLeaseAgreement.GetLeaseAgreementDocuments().Tables[0];
                DataRow dw;

                foreach (DataListItem item in dlListDoc.Items)
                {
                    if (e.Item.ItemIndex == item.ItemIndex)
                        continue;

                    dw = dt.NewRow();

                    lblDocname = (Label)item.FindControl("lblDocname");
                    lblFilename = (Label)item.FindControl("lblFilename");

                    dw["Node"] = dlListDoc.DataKeys[item.ItemIndex];
                    dw["Docname"] = Convert.ToString(lblDocname.Text.Trim());
                    dw["Filename"] = Convert.ToString(lblFilename.Text.Trim());
                    dt.Rows.Add(dw);
                }

                dlListDoc.DataSource = dt;
                dlListDoc.DataBind();
                ViewState["ListDoc"] = dt;
                break;
        }
    }
    protected void fuLeaseAgreement_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        string filename = string.Empty;
        string actualfilename = string.Empty;

        if (fuLeaseAgreement.HasFile)
        {
            actualfilename = fuLeaseAgreement.FileName;
            filename = DateTime.Now.Ticks.ToString() + Path.GetExtension(fuLeaseAgreement.FileName);
            Session["Filename"] = filename;
            fuLeaseAgreement.SaveAs(Server.MapPath("~/documents/temp/") + filename);
        }
    }
    protected void btnattachdoc_Click(object sender, EventArgs e)
    {
        divListDoc.Style["display"] = "block";

        DataTable dt;
        DataRow dw;
        objclsLeaseAgreement = new clsLeaseAgreement();
        if (ViewState["ListDoc"] == null)
            dt = objclsLeaseAgreement.GetLeaseAgreementDocuments().Tables[0];
        else
        {
            DataTable dtDoc = (DataTable)this.ViewState["ListDoc"];
            dt = dtDoc;
        }
        dw = dt.NewRow();
        dw["Docname"] = Convert.ToString(txtDocname.Text.Trim());
        dw["Filename"] = Session["Filename"];
        dt.Rows.Add(dw);
        dlListDoc.DataSource = dt;
        dlListDoc.DataBind();
        ViewState["ListDoc"] = dt;
        txtDocname.Text = string.Empty;
        upListDoc.Update();
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        LeaseAgreementPager.CurrentPage = 0;
        hfLeaseAgreementID.Value = "";
        BindDataList();
    }
    protected void imgLeaseUnitType_Click(object sender, EventArgs e)
    {
        if (ddlLeaseUnitType != null)
        {
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "LeaseUnitTypeReference";
            ReferenceControlNew1.DataTextField = "Description";
            ReferenceControlNew1.DataValueField = "LeaseUnitTypeID";
            ReferenceControlNew1.FunctionName = "FillLeaseUnitType";
            ReferenceControlNew1.SelectedValue = ddlLeaseUnitType.SelectedValue;
            ReferenceControlNew1.DisplayName = GetLocalResourceObject("LeaseUnitType.Text").ToString();
            ReferenceControlNew1.DataTextFieldArabic = "DescriptionArb";
            ReferenceControlNew1.PopulateData();

            mdlPopUpReference.Show();
            updModalPopUp.Update();
            upLeaseUnitType.Update();
        }
    }

    #endregion
    #region METHODS
    private void fillCombo()
    {
        objUser = new clsUserMaster();
        objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intCompanyID = objUser.GetCompanyId();
       
        //objclsLeaseAgreement.intUserID= objUser.GetUserId();
        
        DataSet dtsData = objclsLeaseAgreement.GetReference();
        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                ddlCompany.DataTextField = "CompanyName";
                ddlCompany.DataValueField = "CompanyID";
                ddlCompany.DataSource = dtsData.Tables[0];
                ddlCompany.DataBind();
                
            }
        }

        if (dtsData.Tables.Count > 1)
        {
            if (dtsData.Tables[1].Rows.Count > 0)
            {

                DataRow dr = dtsData.Tables[1].NewRow();
                dr["Description"] = GetGlobalResourceObject("DocumentsCommon", "Select"); 
                dr["LeaseUnitTypeID"] = "-1";
                dtsData.Tables[1].Rows.InsertAt(dr, 0);
                ddlLeaseUnitType.DataTextField = "Description";
                ddlLeaseUnitType.DataValueField = "LeaseUnitTypeID";
                ddlLeaseUnitType.DataSource = dtsData.Tables[1];
                ddlLeaseUnitType.DataBind();


            }
        }
        ddlCompany.Enabled = false;



    }
    private void BindDataList()
    {
        objUser = new clsUserMaster();
        objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intPageIndex = LeaseAgreementPager.CurrentPage + 1;
        objclsLeaseAgreement.intPageSize = LeaseAgreementPager.PageSize;
        objclsLeaseAgreement.strSearchKey = txtSearch.Text.Trim();
        objclsLeaseAgreement.intCompanyID = objUser.GetCompanyId();
        DataSet dtsData = objclsLeaseAgreement.GetLeaseAgreementToBind();

        if (dtsData.Tables.Count > 0)
        {
            if (dtsData.Tables[0].Rows.Count > 0)
            {
                dlLeaseAgreement.DataSource = dtsData.Tables[0];
                dlLeaseAgreement.DataBind();
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        LeaseAgreementPager.Total = dtsData.Tables[1].Rows[0]["Count"].ToInt32();
                    }
                    else
                    {
                        LeaseAgreementPager.Total = 0;
                    }
                }
                else
                {
                    LeaseAgreementPager.Total = 0;
                }
                setDivVisibility(2);
                hfLeaseAgreementID.Value = "";
                upMenu.Update();
            }

            else
            {
                if (dtsData.Tables.Count > 1)
                {
                    if (dtsData.Tables[1].Rows.Count > 0)
                    {
                        if (dtsData.Tables[1].Rows[0]["Count"].ToInt32() > 0)
                        {
                            if (LeaseAgreementPager.CurrentPage > 0)
                            {
                                LeaseAgreementPager.CurrentPage = LeaseAgreementPager.CurrentPage - 1;
                                BindDataList();
                            }
                            else
                            {
                                lnkAdd_Click(null, null);
                            }
                        }
                        else
                        {
                            setDivVisibility(5);
                            //lnkAdd_Click(null, null);
                        }
                    }
                    else
                    {
                        lnkAdd_Click(null, null);

                    }
                }
                else
                {
                    lnkAdd_Click(null, null);

                }
            }
        }
        else
        {
            lnkAdd_Click(null, null);
        }

    }
    private void FillParameters(clsLeaseAgreement objclsLeaseAgreement)
    {
        objclsLeaseAgreement.intLeaseAgreementID = hfLeaseAgreementID.Value.ToInt32();

        objclsLeaseAgreement.intCompanyID = ddlCompany.SelectedValue.ToInt32();
        objclsLeaseAgreement.strAgreementNo = txtAgreementNumber.Text.Trim();
        objclsLeaseAgreement.strLessor = txtLessor.Text.Trim();

        objclsLeaseAgreement.dteStartDate = clsCommon.Convert2DateTime(txtFromDate.Text.Trim());
        objclsLeaseAgreement.dteEndDate = clsCommon.Convert2DateTime(txtToDate.Text.Trim());
        objclsLeaseAgreement.dteDocumentDate = clsCommon.Convert2DateTime(txtDocumentDate.Text.Trim());
        objclsLeaseAgreement.strLeasePeriod = txtLeasePeriod.Text.Trim();
        objclsLeaseAgreement.decRentalValue = txtRentalValue.Text.ToDecimal();
        objclsLeaseAgreement.blnDepositHeld = chkDepositHeld.Checked;
        objclsLeaseAgreement.strTenant = txtTenant.Text.Trim();
        objclsLeaseAgreement.strContactNumbers = txtContactNumbers.Text.Trim();
        objclsLeaseAgreement.strAddress = txtAddress.Text.Trim();

        objclsLeaseAgreement.strLandLord = txtLandLord.Text.Trim();
        objclsLeaseAgreement.strUnitNumber = txtUnitNumber.Text.Trim();
        objclsLeaseAgreement.intLeaseUnitTypeID = ddlLeaseUnitType.SelectedValue.ToInt32();
        objclsLeaseAgreement.strBuildingNumber = txtBuildingNumber.Text.Trim();
        objclsLeaseAgreement.strLocation = txtLocation.Text.Trim();
        objclsLeaseAgreement.strPlotNumber = txtPlotNumber.Text.Trim();
        objclsLeaseAgreement.decTotal = txtTotal.Text.ToDecimal();
        objclsLeaseAgreement.strRemarks = txtRemarks.Text.Trim();
        objclsLeaseAgreement.blnIsRenewed = false;// lblRenewed.Visible ? true : false;
    }
    private void ClearControls()
    {
        hfLeaseAgreementID.Value = "";
        ddlCompany.SelectedIndex = 0;
        txtAgreementNumber.Text = "";
        txtLessor.Text = "";
        txtRentalValue.Text = "";
        chkDepositHeld.Checked = false;
        txtTenant.Text = "";
        txtContactNumbers.Text = "";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        txtLeasePeriod.Text = "";
        txtDocumentDate.Text = "";
        txtAddress.Text = "";
        txtLandLord.Text = "";
        txtUnitNumber.Text = "";
        ddlLeaseUnitType.SelectedValue = "-1";
        txtBuildingNumber.Text = "";
        txtLocation.Text = "";
        txtPlotNumber.Text = "";
        txtTotal.Text = "";
        txtRemarks.Text = "";
        txtDocname.Text = "";
        dlListDoc.DataSource = null;
        dlListDoc.DataBind();
        lblStatusAdd.Text =Convert.ToString( GetGlobalResourceObject("DocumentsCommon", "New"));
        divAddLeaseAgreementDataPart.Style["pointer-events"] = "auto";
        ddlCompany.Enabled = false;
        //divRenew.Style["display"] = "none";
        //divIssueReceipt.Style["display"] = "none";
    }
    private void setDivVisibility(int intMode)
    {
        //Mode 1:-divAddLeaseAgreement
        //Mode 2:-divViewLeaseAgreement
        //Mode 3:-divSingleViewLeaseAgreement
        //Mode 4:-divNoInformationFound

        //dvNoRecord.Visible = false;

        if (intMode == 1)
        {
            divAddLeaseAgreement.Style["display"] = "block";
            divViewLeaseAgreement.Style["display"] = "none";
            divSingleViewLeaseAgreement.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "none";
            enablePrintEmailDeleteButton(false, false);
        }
        else if (intMode == 2)
        {
            divAddLeaseAgreement.Style["display"] = "none";
            divViewLeaseAgreement.Style["display"] = "block";
            divSingleViewLeaseAgreement.Style["display"] = "none";
            enablePrintEmailDeleteButton(true, true);
            divNoInformationFound.Style["display"] = "none";

        }
        else if (intMode == 3)
        {
            divAddLeaseAgreement.Style["display"] = "none";
            divViewLeaseAgreement.Style["display"] = "none";
            divSingleViewLeaseAgreement.Style["display"] = "block";
            divNoInformationFound.Style["display"] = "none";
            enablePrintEmailDeleteButton(true, false);
        }
        else if (intMode == 4)
        {
            divAddLeaseAgreement.Style["display"] = "none";
            divViewLeaseAgreement.Style["display"] = "none";
            divSingleViewLeaseAgreement.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "block";
            enablePrintEmailDeleteButton(false, false);
        }
        else if (intMode == 5)
        {
           // dvNoRecord.Visible = true;
            divAddLeaseAgreement.Style["display"] = "none";
            divViewLeaseAgreement.Style["display"] = "none";
            divSingleViewLeaseAgreement.Style["display"] = "none";
            divNoInformationFound.Style["display"] = "block";
            enablePrintEmailDeleteButton(false, false);

        }
        upViewLeaseAgreement.Update();
        upAddLeaseAgreement.Update();
        upSingleViewLeaseAgreement.Update();
        upNoInformationFound.Update();
    }
    private void SetPermission()
    {
        int RoleId;
        DataTable dt = new DataTable();
        clsUserMaster objUser = new clsUserMaster();
        clsRoleSettings objRoleSettings = new clsRoleSettings();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.LeaseAgreement);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;
    }
    private void enableAddViewButtons()
    {
        DataTable dtm = (DataTable)ViewState["Permission"];

        if (dtm.Rows.Count > 0)
        {
            if (dtm.Rows[0]["IsCreate"].ToBoolean() || dtm.Rows[0]["IsUpdate"].ToBoolean())
            {
                ImageButton1.Enabled = true;
            }
            else
            {
                ImageButton1.Enabled = false;
            }
            if (!dtm.Rows[0]["IsView"].ToBoolean())
            {
                msgs.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString());
                mpeMessage.Show();
                setDivVisibility(4);
            }
            lnkAdd.Enabled = dtm.Rows[0]["IsCreate"].ToBoolean();
            lnkView.Enabled = dtm.Rows[0]["IsView"].ToBoolean();
        }
        else
        {
            msgs.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString());
            mpeMessage.Show();
            setDivVisibility(4);
        }
        //enablePrintEmailDeleteButton(true, true);

    }
    private void enablePrintEmailDeleteButton(bool IsEnable, bool isList)
    {
        DataTable dtm = (DataTable)ViewState["Permission"];
        if (IsEnable)
        {
            if (dtm.Rows[0]["IsDelete"].ToBoolean())
            {
                if (isList)
                {
                    lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLeaseAgreement.ClientID + "');";
                }
                else
                {
                    if (clsUserMaster.GetCulture() == "ar-AE")
                        lnkDelete.OnClientClick = "return confirm('هل أنت متأكد أنك تريد حذف؟');";
                    else
                        lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
                }
                lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
            }
            else
            {
                lnkDelete.Enabled = false;
                lnkDelete.OnClientClick = "return false;";
            }
            if (dtm.Rows[0]["IsPrintEmail"].ToBoolean())
            {
                if (isList)
                {
                    lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlLeaseAgreement.ClientID + "');";
                    lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlLeaseAgreement.ClientID + "');";
                }
                else
                {
                    lnkPrint.OnClientClick = "return true;";
                    lnkEmail.OnClientClick = "return true;";
                }
                lnkPrint.Enabled = lnkEmail.Enabled = dtm.Rows[0]["IsPrintEmail"].ToBoolean();
            }
            else
            {
                lnkPrint.Enabled = lnkEmail.Enabled = false;
                lnkPrint.OnClientClick = "return false;";
                lnkEmail.OnClientClick = "return false;";
            }
        }
        else
        {
            lnkDelete.Enabled = lnkPrint.Enabled = lnkEmail.Enabled = false;
            lnkPrint.OnClientClick = "return false;";
            lnkEmail.OnClientClick = "return false;";
            lnkDelete.OnClientClick = "return false;";
        }
        upMenu.Update();
    }
    //private void enablePrintEmailButton(bool IsEnable)
    //{
    //    DataTable dtm = (DataTable)ViewState["Permission"];
    //    if (IsEnable)
    //    {
    //        if (dtm.Rows[0]["IsDelete"].ToBoolean())
    //        {
    //            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlLeaseAgreement.ClientID + "');";
    //            //lnkDelete.OnClientClick = "return confirm('Are you sure you want to delete?');";
    //            lnkDelete.Enabled = dtm.Rows[0]["IsDelete"].ToBoolean();
    //        }
    //        else
    //        {
    //            lnkDelete.Enabled = false;
    //            lnkDelete.OnClientClick = "return false;";
    //        }
    //    }
    //    else
    //    {
    //        lnkDelete.Enabled = false;
    //        lnkDelete.OnClientClick = "return false;";
    //    }
    //    upMenu.Update();
    //}
    private void singleViewLeaseAgreement(int intLeaseAgreementID)
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intLeaseAgreementID = intLeaseAgreementID;
        DataTable datData = objclsLeaseAgreement.GetLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            hfLeaseAgreementID.Value = datData.Rows[0]["LeaseID"].ToStringCustom();
            lblCompany.Text = datData.Rows[0]["CompanyName"].ToStringCustom();
            lblRenewed.Text = datData.Rows[0]["Renewed"].ToStringCustom();
            lblAgreementNumber.Text = datData.Rows[0]["AgreementNo"].ToStringCustom();
            lblLessor.Text = datData.Rows[0]["Lessor"].ToStringCustom();
            lblRentalValue.Text = datData.Rows[0]["RentalValue"].ToStringCustom();
            lblDepositHeld.Text = datData.Rows[0]["DepositHeldDisplay"].ToStringCustom();
            lblTenant.Text = datData.Rows[0]["Tenant"].ToStringCustom();
            lblContactNumbers.Text = datData.Rows[0]["ContactNumbers"].ToStringCustom();
            lblFromdate.Text = datData.Rows[0]["StartDate"].ToStringCustom();
            lblTodate.Text = datData.Rows[0]["EndDate"].ToStringCustom();
            lblLeasePeriod.Text = datData.Rows[0]["LeasePeriod"].ToStringCustom();
            lblDocumentdate.Text = datData.Rows[0]["DocumentDate"].ToStringCustom();
            lblAddress.Text = datData.Rows[0]["Address"].ToStringCustom();
            lblLandLord.Text = datData.Rows[0]["LandLord"].ToStringCustom();
            lblUnitNumber.Text = datData.Rows[0]["UnitNumber"].ToStringCustom();
            lblLeaseUnitType.Text = datData.Rows[0]["LeaseUnitType"].ToStringCustom();
            lblBuildingNumber.Text = datData.Rows[0]["BuildingNumber"].ToStringCustom();
            lblLocation.Text = datData.Rows[0]["Location"].ToStringCustom();
            lblPlotNumber.Text = datData.Rows[0]["PlotNumber"].ToStringCustom();
            lblTotal.Text = datData.Rows[0]["Total"].ToStringCustom();
            lblRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();
            lblStatus.Text = datData.Rows[0]["Status"].ToStringCustom();

            setDivVisibility(3);

        }


    }
    private void fillUpdateLeaseAgreement(int intLeaseAgreementID)
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        objclsLeaseAgreement.intLeaseAgreementID = intLeaseAgreementID;
        DataTable datData = objclsLeaseAgreement.GetLeaseAgreement();
        if (datData.Rows.Count > 0)
        {
            hfLeaseAgreementID.Value = datData.Rows[0]["LeaseID"].ToStringCustom();
            ddlCompany.SelectedValue = datData.Rows[0]["CompanyID"].ToStringCustom();
            txtAgreementNumber.Text = datData.Rows[0]["AgreementNo"].ToStringCustom();
            txtLessor.Text = datData.Rows[0]["Lessor"].ToStringCustom();
            txtRentalValue.Text = datData.Rows[0]["RentalValue"].ToStringCustom();
            chkDepositHeld.Checked = Convert.ToBoolean(datData.Rows[0]["DepositHeld"]);
            txtTenant.Text = datData.Rows[0]["Tenant"].ToStringCustom();
            txtContactNumbers.Text = datData.Rows[0]["ContactNumbers"].ToStringCustom();
            txtFromDate.Text = datData.Rows[0]["StartDate"].ToDateTime().ToString("dd/MM/yyy");
            txtToDate.Text = datData.Rows[0]["EndDate"].ToDateTime().ToString("dd/MM/yyy");
            txtLeasePeriod.Text = datData.Rows[0]["LeasePeriod"].ToStringCustom();
            txtDocumentDate.Text = datData.Rows[0]["DocumentDate"].ToDateTime().ToString("dd/MM/yyy");
            txtAddress.Text = datData.Rows[0]["Address"].ToStringCustom();
            txtLandLord.Text = datData.Rows[0]["LandLord"].ToStringCustom();
            txtUnitNumber.Text = datData.Rows[0]["UnitNumber"].ToStringCustom();
            ddlLeaseUnitType.SelectedValue = datData.Rows[0]["LeaseUnitTypeID"].ToStringCustom() == "" ? "-1" : datData.Rows[0]["LeaseUnitTypeID"].ToStringCustom();
            txtBuildingNumber.Text = datData.Rows[0]["BuildingNumber"].ToStringCustom();
            txtLocation.Text = datData.Rows[0]["Location"].ToStringCustom();
            txtPlotNumber.Text = datData.Rows[0]["PlotNumber"].ToStringCustom();
            txtTotal.Text = datData.Rows[0]["Total"].ToStringCustom();
            txtRemarks.Text = datData.Rows[0]["Remarks"].ToStringCustom();
            lblStatusAdd.Text = datData.Rows[0]["Status"].ToStringCustom();

            setDivVisibility(1);
            if (Convert.ToString(datData.Rows[0]["IsRenewed"]) == "Yes" || Convert.ToString(datData.Rows[0]["IsRenewed"]) == "نعم")
            {
                divAddLeaseAgreementDataPart.Style["pointer-events"] = "none";
            }
            else
            {
                divAddLeaseAgreementDataPart.Style["pointer-events"] = "auto";

            }
            DataTable datdoc = objclsLeaseAgreement.GetLeaseAgreementDocuments().Tables[0];
            dlListDoc.DataSource = datdoc;
            dlListDoc.DataBind();
            ViewState["ListDoc"] = datdoc;

            if (dlListDoc.Items.Count > 0)
                divListDoc.Style["display"] = "block";
            else
                divListDoc.Style["display"] = "none";
            //if (!Convert.ToBoolean(datData.Rows[0]["IsRenewed"]))
            //{
            //    divRenew.Style["display"] = "block";
            //}
            //divIssueReceipt.Style["display"] = "block";
        }
    }
    public void FillLeaseUnitType()
    {
        objclsLeaseAgreement = new clsLeaseAgreement();
        DataSet dtsData = objclsLeaseAgreement.GetReference();
        if (dtsData.Tables.Count > 1)
        {
            if (dtsData.Tables[1].Rows.Count > 0)
            {

                DataRow dr = dtsData.Tables[1].NewRow();
                dr["Description"] = GetGlobalResourceObject("DocumentsCommon", "Select"); 
                dr["LeaseUnitTypeID"] = "-1";
                dtsData.Tables[1].Rows.InsertAt(dr, 0);
                ddlLeaseUnitType.DataTextField = "Description";
                ddlLeaseUnitType.DataValueField = "LeaseUnitTypeID";
                ddlLeaseUnitType.DataSource = dtsData.Tables[1];
                ddlLeaseUnitType.DataBind();


            }
        }

        if (CurrentSelectedValue != null && CurrentSelectedValue != string.Empty)
        {
            if (ddlLeaseUnitType.Items.FindByValue(CurrentSelectedValue) != null)
                ddlLeaseUnitType.SelectedValue = CurrentSelectedValue;
        }

        upLeaseUnitType.Update();
    }
    void ReferenceControlNew1_OnSave(DataTable dt, string SelectedValue, string functionName)
    {
        CurrentSelectedValue = SelectedValue;
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod(functionName);
        theMethod.Invoke(this, null);
    }
    #endregion
}
