﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using HRAutoComplete;
using System.Text;
using System.Data.SqlClient;

public partial class Public_SalaryStructure : System.Web.UI.Page
{
    clsEmployee objEmployee;
    clsSalaryStructure objSalaryStructure;
    clsUserMaster objUser;
    clsRoleSettings objRoleSettings;
    private bool MblnUpdatePermission = true;  // Edit Button Permission In Datalist

    protected void Page_Load(object sender, EventArgs e)
    {

        this.Title = GetGlobalResourceObject("ControlsCommon", "SalaryStructure").ToString();
        RegisterAutoComplete();
        if (!IsPostBack)
        {

          
            this.fvSalarystructure.ChangeMode(FormViewMode.Edit);
            objSalaryStructure = new clsSalaryStructure();
            objUser = new clsUserMaster();
            objRoleSettings = new clsRoleSettings();
            BindCombos();


            //setButtonPermissions();
            DataTable dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.SalaryStructure); //set Permission For View /Edit/ Delete
            ViewState["Permission"] = dt;
            SetPermission();

            ViewState["strSalaryDayIsEditable"] = new clsConfiguration().GetConfigurationValue(clsConfiguration.ConfigurationItems.SalaryDayIsEditable);

            if (Convert.ToString(ViewState["strSalaryDayIsEditable"]).ToUpper() != "YES")
                ddlSalaryProcessDay.Enabled = false;
            else
                ddlSalaryProcessDay.Enabled = true;

            ddlCompany_SelectedIndexChanged(null, null);
            pgrEmployees.Visible = false;

            if (Request.QueryString["EmpId"] != null)
            {
                DataTable dtSalary = objSalaryStructure.GetSalaryStructureID(Convert.ToInt32(Request.QueryString["EmpId"]));
                if (dtSalary.Rows.Count > 0)
                {
                    hfMode.Value = "Edit";
                    objSalaryStructure.SalaryID = Convert.ToInt32(dtSalary.Rows[0]["SalaryStructureID"]);
                    hdSalaryID.Value = dtSalary.Rows[0]["SalaryStructureID"].ToString();
                    lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
                    lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

                    BindFormView(Convert.ToInt32(dtSalary.Rows[0]["SalaryStructureID"]));

                    txtSearch.Text = string.Empty;
                }
            }

            else
            {
                if(lnkAddEmp.Enabled)
                btnList_Click(sender, e);
            }
        }
       
    }

    private void RegisterAutoComplete()
    {
        objUser = new clsUserMaster();
        clsCommon.RegisterAutoComplete(this.Page, this.txtSearch, AutoCompletePage.Salarystructure,objUser.GetCompanyId());
    }

    protected void Bind()
    {
        objSalaryStructure = new clsSalaryStructure();
        objUser = new clsUserMaster();

        objSalaryStructure.PageIndex = pgrEmployees.CurrentPage + 1;
        objSalaryStructure.PageSize = pgrEmployees.PageSize;
        objSalaryStructure.SearchKey = txtSearch.Text;
        objSalaryStructure.CompanyID  = objUser.GetCompanyId();
        pgrEmployees.Total = objSalaryStructure.GetCount();
        DataSet ds = objSalaryStructure.GetAllEmployeeSalaryStructures();
        fvSalarystructure.DataSource = null;
        fvSalarystructure.DataBind();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dlEmployees.DataSource = ds;
            dlEmployees.DataBind();

            EnableMenus();
            pgrEmployees.Visible = true;
            lblNoData.Visible = false;



        }
        else
        {
            dlEmployees.DataSource = null;
            dlEmployees.DataBind();
            pgrEmployees.Visible = false;

            lblNoData.Visible = true;
            //if (txtSearch.Text == string.Empty)
            //    lblNoData.Text = "No Salary structures are added yet";
            //else
            //    lblNoData.Text = "No Search Results found";


            if (txtSearch.Text == string.Empty)
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoSalaryStructuresAddedYet").ToString();
            else
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoSearchResultsFound").ToString();
        }
        fvEmployees.Visible = false;
        //lblHeading.Text = "Salary Structure";
        //imgHeader.ImageUrl = "../images/List_salarystructure_Large.png";


        string sValidationGroup = clsCommon.GetValidationGroup();
        clsCommon.WriteLog(sValidationGroup);


        rfvEmployee.ValidationGroup = cvAmount.ValidationGroup = cvAllowances.ValidationGroup =cvRemuneration.ValidationGroup=cvRemunerationAmount.ValidationGroup= sValidationGroup;

        btnSubmit.Attributes.Add("onclick", "return confirmSave('" + sValidationGroup + "');");
        upnlNoData.Update();

        txtSearch.Text = string.Empty;
    }
    protected void btnList_Click(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        pgrEmployees.CurrentPage = 0;   // Tells which page to show ?

        //fvSalarystructure.DataSource = null;
        //fvSalarystructure.DataBind();

        fvSalarystructure.Visible = false;  // Change of visibilty
        Bind();
        hfMode.Value = "List";
        upEmployees.Update();
        upMenu.Update();
    }
    protected void lnkAddEmp_Click(object sender, EventArgs e)
    {
        NewEmployeeSalaryStructure();
        upEmployees.Update();
    }
    protected void NewEmployeeSalaryStructure()
    {
        lblNoData.Visible = false;
        hfMode.Value = "Insert";
        fvSalarystructure.Visible = false;
        hdSalaryID.Value = string.Empty;
        txtSearch.Text = string.Empty;
        txtRemarks.Text = string.Empty;
        pgrEmployees.Visible = false;

        objSalaryStructure = new clsSalaryStructure();
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();

        fvEmployees.Visible = true;

        objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlEmployee.DataSource = objSalaryStructure.FillFilterEmployees();
        ddlEmployee.DataBind();
        ddlEmployee.SelectedIndex = -1;
        if (ddlEmployee.Items.Count == 0)
            ddlEmployee.Items.Add(new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : clsGlobalization.IsArabicCulture() ? "اختر" : "Select", "-1"));
        //txtBasicPay.Text = string.Empty;
        // chkCurApplicable.Checked = false;
        ddlCurrency.SelectedIndex = -1;
        ddlAbsentPolicy.SelectedIndex = -1;
        ddlEncashPolicy.SelectedIndex = -1;
        ddlOvertimePolicy.SelectedIndex = -1;
        ddlHolidayPolicy.SelectedIndex = -1;
        //ddlIncentiveType.SelectedIndex = -1;
        //ddlSettlement.SelectedIndex = -1;
        txtAdditionAmt.Text = string.Empty;
        txtDeductionAmt.Text = string.Empty;
        txtNetAmount.Text = string.Empty;
        dlDesignationAllowances.DataSource = null;
        dlDesignationAllowances.DataBind();

        dlOtherRemuneration.DataSource = null;
        dlOtherRemuneration.DataBind();

        ddlPaymentMode.Enabled = false;
        ddlPaymentMode.SelectedIndex = 0;

        ddlPaymentCalculation.Enabled = false;
        ddlPaymentCalculation.SelectedIndex = 0;


        ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);
       // if (!(ddlCompany.Enabled))
        ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));
        ddlCompany.Enabled = false;

        ddlEmployee.Enabled = true;

        lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
        lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";
        dlEmployees.DataSource = null;
        dlEmployees.DataBind();
        upnlNoData.Update();
    }

    private void BindCombos()
    {
        objEmployee = new clsEmployee();
        objRoleSettings = new clsRoleSettings();
        objUser = new clsUserMaster();
        objEmployee.CompanyID = objUser.GetCompanyId();
        ddlCompany.DataSource = objEmployee.GetAllCompanies();
        ddlCompany.DataBind();
        if (ddlCompany.Items.Count == 0)
            ddlCompany.Items.Add(new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "Select", "-1"));

        ddlCompany.Enabled = objRoleSettings.IsMenuEnabled(objUser.GetRoleId(), clsRoleSettings._MENU.Manage_All);
       // if (!(ddlCompany.Enabled))
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(objUser.GetCompanyId().ToString()));

        ddlCompany.Enabled = false;

        objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlEmployee.DataSource = objSalaryStructure.FillEmployees();
        ddlEmployee.DataBind();
        
        ddlPaymentMode.DataSource = objSalaryStructure.FillPaymentMode();
        ddlPaymentMode.DataBind();

        ddlCurrency.DataSource = objSalaryStructure.FillEmpCurrencies();
        ddlCurrency.DataBind();

        if (Convert.ToInt32(ddlPaymentMode.SelectedValue) == 4)
        {
            ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculation();
            ddlPaymentCalculation.DataBind();
        }
        else
        {
            ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculationOther();
            ddlPaymentCalculation.DataBind();
        }

        if (ddlEmployee.Items.Count > 0)
        {
            objSalaryStructure.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            ddlCurrency.DataSource = objSalaryStructure.FillEmpCurrencies();
            ddlCurrency.DataBind();
        }
        ddlAbsentPolicy.DataSource = objSalaryStructure.FillAbsentPolicies();
        ddlAbsentPolicy.DataBind();
        ddlAbsentPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        ddlEncashPolicy.DataSource = objSalaryStructure.FillEncashPolicies();
        ddlEncashPolicy.DataBind();
        ddlEncashPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        ddlHolidayPolicy.DataSource = objSalaryStructure.FillHolidayPolicies();
        ddlHolidayPolicy.DataBind();
        ddlHolidayPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        ddlOvertimePolicy.DataSource = objSalaryStructure.FillOvertimePolicies();
        ddlOvertimePolicy.DataBind();
        ddlOvertimePolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        //ddlIncentiveType.DataSource = objSalaryStructure.FillCompanyIncentivePolicies();
        //ddlIncentiveType.DataBind();
        //ddlIncentiveType.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //ddlSettlement.DataSource = objSalaryStructure.FillSettlementPolicies();
        //ddlSettlement.DataBind();
        //ddlSettlement.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

    }

    protected void lnkDelete_Click(object sender, EventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();
        bool deleted = false;
        CheckBox chkEmployee;
        LinkButton btnEmployee;

        string message = string.Empty;

        if (dlEmployees.Items.Count > 0)
        {
            foreach (DataListItem item in dlEmployees.Items)
            {
                chkEmployee = (CheckBox)item.FindControl("chkEmployee");
                btnEmployee = (LinkButton)item.FindControl("btnEmployee");

                if (chkEmployee == null)
                    continue;

                if (chkEmployee.Checked == false)
                    continue;

                objSalaryStructure.SalaryID = Convert.ToInt32(dlEmployees.DataKeys[item.ItemIndex]);

                if (objSalaryStructure.IsSalaryIDExists())
                {
                    //message = "Details exists in the system.";
                    //message += "</ol>Please delete the existing details to proceed with salary structure deletion";

                    message = GetGlobalResourceObject("ControlsCommon", "DetailsExists").ToString();
                    message += GetGlobalResourceObject("ControlsCommon", "PleaseDeleteExistingDetailsToProceedWithSalaryStructureDeletion").ToString();


                }
                else
                {
                    objSalaryStructure.Delete();
                    //message = "Employee salary structures(s) deleted successfully";
                    message = GetGlobalResourceObject("ControlsCommon", "SalaryStructureDeletedSuccessfully").ToString();
                    deleted = true;

                }

            }
        }
        else
        {
            if (hfMode.Value == "List")
            {
                objSalaryStructure.SalaryID = Convert.ToInt32(fvSalarystructure.DataKey["SalaryID"]);
                if (objSalaryStructure.IsSalaryIDExists())
                {
                    //message = "Details exists in the system.";
                    //message += "</ol>Please delete the existing details to proceed with salary structure deletion";
                    message = GetGlobalResourceObject("ControlsCommon", "DetailsExists").ToString();
                    message += GetGlobalResourceObject("ControlsCommon", "PleaseDeleteExistingDetailsToProceedWithSalaryStructureDeletion").ToString();


                }
                else
                {
                    objSalaryStructure.Delete();
                   // message = "Employee salary structures(s) deleted successfully";
                    message = GetGlobalResourceObject("ControlsCommon", "SalaryStructureDeletedSuccessfully").ToString();

                    deleted = true;
                }
            }

        }

        if (message == string.Empty)
            //message = "Please select an item to remove";
            message = GetGlobalResourceObject("ControlsCommon", "PleaseSelectAnItemToRemove").ToString();


        mcMessage.InformationalMessage(message);
        mpeMessage.Show();

        if (deleted)
            Bind();

        upEmployees.Update();
        btnList_Click(sender, e);
    }
    protected void lnkPrint_Click(object sender, EventArgs e)
    {
        if (dlEmployees.Items.Count > 0)
        {
            objUser = new clsUserMaster();

            if (fvSalarystructure.DataKey["SalaryID"] == DBNull.Value || fvSalarystructure.DataKey["SalaryID"] == null)
            {
                CheckBox chkEmployee;

                string sSalaryIds = string.Empty;

                foreach (DataListItem item in dlEmployees.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sSalaryIds += "," + dlEmployees.DataKeys[item.ItemIndex];
                }
                sSalaryIds = sSalaryIds.Remove(0, 1);
                if (sSalaryIds.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sSalaryIds);
                else
                {
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sSalaryIds));
                }

            }
            else
            {

                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvSalarystructure.DataKey["SalaryID"]));
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }   
    }




    public string GetPrintText(int iSalID)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList alParameters = new ArrayList();

        alParameters.Add(new SqlParameter("@Mode", "GSS"));
        alParameters.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        alParameters.Add(new SqlParameter("@SalaryID", iSalID));

        DataTable dt = new DataLayer(). ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters).Tables[0];

        ArrayList alParameters1 = new ArrayList();

        alParameters1.Add(new SqlParameter("@Mode", "GSD"));
        alParameters1.Add(new SqlParameter("@SalaryID", iSalID));


        DataTable dtsalaryDet = new DataLayer().ExecuteDataSet("HRspEmployeeSalaryStructure", alParameters1).Tables[0];

        sb.Append("<table width='100%' border='0' style='font-family:'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<tr><td><table border='0'>");
        sb.Append("<tr>");
        sb.Append("<td style='font-size:14px; font-weight:bold;' align='left' valign='center' >" + Convert.ToString(dt.Rows[0]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[0]["EmployeeFullName"]) + "</td>");
        sb.Append("</tr>");
        sb.Append("</table>");
        sb.Append("<tr><td style='padding-left:15px;'>");
        sb.Append("<table width='100%' style='font-size:11px;' align='left'>");
        sb.Append("<tr><td colspan='3' style='font-weight:bold;padding:2px;'>"+GetGlobalResourceObject("ControlsCommon","SalaryStructureInformation").ToString()+"</td></tr>");
        sb.Append("<tr><td width='150px'>"+GetGlobalResourceObject("ControlsCommon","PaymentMode").ToString()+"</td><td width='5px'>:</td><td>" + Convert.ToString(dt.Rows[0]["Payclassification"]) + "</td></tr>");
        sb.Append("<tr><td>"+GetGlobalResourceObject("ControlsCommon","PaymentCalculation").ToString()+"</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Paycalculation"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("ControlsCommon", "Currency").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["Currency"]) + "</td></tr>");
        //sb.Append("<tr><td>Basic Pay</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["BasicPay"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("ControlsCommon", "AbsentPolicy").ToString() + "/td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["AbsentPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("ControlsCommon", "EncashPolicy").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["EncashPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("ControlsCommon", "HolidayPolicy").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["HolidayPolicy"]) + "</td></tr>");
        sb.Append("<tr><td>" + GetGlobalResourceObject("ControlsCommon", "OverTimePolicy").ToString() + "</td><td>:</td><td>" + Convert.ToString(dt.Rows[0]["OTPolicy"]) + "</td></tr>");


        if (dtsalaryDet.Rows.Count > 0)
        {
            sb.Append("<tr><td width='150px' valign='top'>" + GetGlobalResourceObject("ControlsCommon", "AdditionDeductionDetails").ToString() + "</td><td valign='top'>:</td>");
            sb.Append("<td>");
            sb.Append("<table border='0' cellspacing='0' cellpadding='0' style='font-size:11px;'>");
            sb.Append("<tr><td width='150'><b>" + GetGlobalResourceObject("ControlsCommon", "Particulars").ToString() + "</b></td><td width='150'><b>" + GetGlobalResourceObject("ControlsCommon", "Category").ToString() + "</b></td><td width='150'><b>" + GetGlobalResourceObject("ControlsCommon", "Amount").ToString() + "</b></td><td width='150'><b> " + GetGlobalResourceObject("ControlsCommon", "DeductionPolicy").ToString() + "</b></td></tr>");
            for (int i = 0; i < dtsalaryDet.Rows.Count; i++)
            {
                sb.Append("<tr><td>" + Convert.ToString(dtsalaryDet.Rows[i]["AddDed"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["Category"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["Amount"]) + "</td><td>" + Convert.ToString(dtsalaryDet.Rows[i]["payPolicy"]) + "</td></tr>");
            }
            sb.Append("</table>");
        }
        sb.Append("</td>");
        sb.Append("</table>");
        sb.Append("</td>");
        sb.Append("</tr></table>");

        return sb.ToString();
    }




    public string CreateSelectedEmployeesContent(string sSalaryIds)
    {
        StringBuilder sb = new StringBuilder();
        ArrayList arraylst = new ArrayList();

        arraylst.Add(new SqlParameter("@Mode", "GPTAE"));
        arraylst.Add(new SqlParameter("@IsArabic",clsGlobalization.IsArabicCulture()));
        arraylst.Add(new SqlParameter("@SalaryIds", sSalaryIds));

        DataTable dt = new DataLayer().ExecuteDataTable("HRspEmployeeSalaryStructure", arraylst);

        sb.Append("<table width='100%' style='font: 13px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;' border='1px'>");
        sb.Append("<tr><td style='font-weight:bold;' align='center'>"+GetGlobalResourceObject("ControlsCommon","SalaryStructureInformation")+"</td></tr>");
        sb.Append("<tr><td>");
        //sb.Append("<table width='100%' style='font: 11px/165% 'Lucida Grande' , Verdana, Helvetica, sans-serif;'>");
        sb.Append("<table width='100%' style='font-size:11px;font-family:Tahoma;margin-top:15px;' border='0px' >");

        sb.Append("<tr style='font-weight:bold;'><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "Employee").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("ControlsCommon", "Currency").ToString() + "</td><td width='150px'>" + GetGlobalResourceObject("ControlsCommon", "PaymentMode").ToString() + "</td><td width='100px'>" + GetGlobalResourceObject("ControlsCommon", "PaymentCalculation").ToString() + "</td></tr>");
        sb.Append("<tr><td colspan='7'><hr></td></tr>");
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            sb.Append("<tr><td width='150px'>" + Convert.ToString(dt.Rows[i]["SalutationName"]) + "-" + Convert.ToString(dt.Rows[i]["EmployeeFullName"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Currency"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Payclassification"]) + "</td>");
            sb.Append("<td>" + Convert.ToString(dt.Rows[i]["Paycalculation"]) + "</td></tr>");
            //sb.Append("<td>" + Convert.ToString(dt.Rows[i]["BasicPay"]) + "</td> </tr>");

        }
        sb.Append("</table>");
        sb.Append("</td></tr>");
        sb.Append("</table>");

        return sb.ToString();
    }










    protected void lnkEmail_Click(object sender, EventArgs e)
    {
        //if (dlEmployees.Items.Count > 0)
        //{
        //    objUser = new clsUserMaster();

        //    string sEmployeeIds = string.Empty;

        //    if (fvSalarystructure.DataKey["SalaryID"] == DBNull.Value || fvSalarystructure.DataKey["SalaryID"] == null)
        //    {
        //        CheckBox chkEmployee;

        //        foreach (DataListItem item in dlEmployees.Items)
        //        {
        //            chkEmployee = (CheckBox)item.FindControl("chkEmployee");

        //            if (chkEmployee == null)
        //                continue;

        //            if (chkEmployee.Checked == false)
        //                continue;

        //            sEmployeeIds += "," + dlEmployees.DataKeys[item.ItemIndex];
        //        }

        //        sEmployeeIds = sEmployeeIds.Remove(0, 1);
        //    }
        //    else
        //    {

        //        sEmployeeIds = fvSalarystructure.DataKey["SalaryID"].ToString();
        //    }


        //       }



        string sSalaryIds = string.Empty;



        if (dlEmployees.Items.Count > 0)
        {
            objUser = new clsUserMaster();

            if (fvSalarystructure.DataKey["SalaryID"] == DBNull.Value || fvSalarystructure.DataKey["SalaryID"] == null)
            {
                CheckBox chkEmployee;

                

                foreach (DataListItem item in dlEmployees.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sSalaryIds += "," + dlEmployees.DataKeys[item.ItemIndex];
                }
                sSalaryIds = sSalaryIds.Remove(0, 1);
                if (sSalaryIds.Contains(","))
                    divPrint.InnerHtml = CreateSelectedEmployeesContent(sSalaryIds);
                else
                {
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sSalaryIds));
                }

            }
            else
            {

                divPrint.InnerHtml = GetPrintText(Convert.ToInt32(fvSalarystructure.DataKey["SalaryID"]));
            }


        }

        Session["Content"] = divPrint.InnerHtml;
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Email", "openWindow('ComposeMail.aspx?type=SalaryStructureNew&SalaryId=" + sSalaryIds + "', 835, 585);", true);
   



    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        if (dlEmployees.Items.Count > 0)
        {
            objUser = new clsUserMaster();

            if (fvSalarystructure.DataKey["SalaryID"] == DBNull.Value || fvSalarystructure.DataKey["SalaryID"] == null)
            {
                CheckBox chkEmployee;

                string sSalaryIds = string.Empty;

                foreach (DataListItem item in dlEmployees.Items)
                {
                    chkEmployee = (CheckBox)item.FindControl("chkEmployee");

                    if (chkEmployee == null)
                        continue;

                    if (chkEmployee.Checked == false)
                        continue;

                    sSalaryIds += "," + dlEmployees.DataKeys[item.ItemIndex];
                }
                sSalaryIds = sSalaryIds.Remove(0, 1);
                if (sSalaryIds.Contains(","))
                    divPrint.InnerHtml = objUser.CreateSelectedEmployeesContent(sSalaryIds);
                else
                {
                    divPrint.InnerHtml = GetPrintText(Convert.ToInt32(sSalaryIds));
                }

            }
            else
            {

                divPrint.InnerHtml = objUser.GetPrintText(Convert.ToInt32(fvSalarystructure.DataKey["SalaryID"]));
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "Print", "Print('" + divPrint.ClientID + "')", true);
        }   

    }
    protected void lnkAddDed_Click(object sender, EventArgs e)
    {
        FormView1.ChangeMode(FormViewMode.Insert);
        controls_AdditionDeduction ucAdditionDeduction = (controls_AdditionDeduction)FormView1.FindControl("ucAdditionDeduction");
        AjaxControlToolkit.ModalPopupExtender mpeAdditionDeduction = (AjaxControlToolkit.ModalPopupExtender)FormView1.FindControl("mpeAdditionDeduction");
        
        if (ucAdditionDeduction != null && mpeAdditionDeduction != null)
        {
            ucAdditionDeduction.BindDataList();
            mpeAdditionDeduction.Show();
            upnlAdditionDeduction.Update();
        }
    }

    protected void ApEmployee_Update()
    {

        objSalaryStructure = new clsSalaryStructure();

        ddlAbsentPolicy.DataSource = objSalaryStructure.FillAbsentPolicies();
        ddlAbsentPolicy.DataBind();
        ddlAbsentPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //ddlAbsentPolicy.SelectedIndex = ddlAbsentPolicy.Items.IndexOf(ddlAbsentPolicy.Items.FindByValue(ApEmployee.AbsentPolicyID.ToString()));

        if (upAbsentPolicy != null)
            upAbsentPolicy.Update();


    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Bind();
    }
    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        pgrEmployees.CurrentPage = 0;
        Bind();
        this.RegisterAutoComplete();
        upEmployees.Update();
    }

    //protected void lnkDedPolicy_Click(object sender, EventArgs e)
    //{
    //    FormView2.ChangeMode(FormViewMode.Insert);
    //    controls_DeductionPolicy ucDeductionPolicy = (controls_DeductionPolicy)FormView2.FindControl("ucDeductionPolicy1");
    //    ucDeductionPolicy.IsAddition = false;
    //    AjaxControlToolkit.ModalPopupExtender mpeDeductionPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView2.FindControl("mpeDeductionPolicy");
        
    //    if (ucDeductionPolicy != null && mpeDeductionPolicy != null)
    //    {
    //        ucDeductionPolicy.DeductionPolicyId = -1;
    //        ucDeductionPolicy.Fill();
    //        mpeDeductionPolicy.Show();
    //        upDeductionPolicy.Update();
    //    }
    //}

    protected void dlEmployees_ItemCommand(object source, DataListCommandEventArgs e)
    {
        upMenu.Update();

        objSalaryStructure = new clsSalaryStructure();
        objUser = new clsUserMaster();
        switch (e.CommandName)
        {
            case "ALTER":
                hfMode.Value = "Edit";
                objSalaryStructure.SalaryID = Convert.ToInt32(e.CommandArgument);
                hdSalaryID.Value = Convert.ToString(e.CommandArgument);
                lnkDelete.Enabled = lnkEmail.Enabled = lnkPrint.Enabled = false;
                lnkDelete.OnClientClick = lnkEmail.OnClientClick = lnkPrint.OnClientClick = "return false;";

                BindFormView(Convert.ToInt32(e.CommandArgument));

                txtSearch.Text = string.Empty;
                break;
            case "VIEW":
               
                fvSalarystructure.ChangeMode(FormViewMode.ReadOnly);
                BindViewMode(Convert.ToInt32(e.CommandArgument));
                break;
        }

    }
      private void BindViewMode(int SalID)
    {
        dlEmployees.DataSource = null;
        dlEmployees.DataBind();
        pgrEmployees.Visible = false;
        fvEmployees.Visible = false; 
        fvSalarystructure.Visible = true;
        hfMode.Value = "List";
        objSalaryStructure.SalaryID = SalID;
        hdSalaryID.Value = Convert.ToString(SalID);
        fvSalarystructure.ChangeMode(FormViewMode.ReadOnly);
        fvSalarystructure.DataSource = objSalaryStructure.GetEmpSalaryStructure();
        fvSalarystructure.DataBind();
        dlAllowance.DataSource =objSalaryStructure.GetEmpAllowanceDeductionDetails();
        dlAllowance.DataBind();

        dlRemuneration.DataSource = objSalaryStructure.GetEmployeeRemuneration(hdSalaryID.Value.ToInt32());
        dlRemuneration.DataBind();

        objUser = new clsUserMaster();
        divPrint.InnerHtml = objUser.GetPrintText(SalID);

        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            if (lnkPrint.Enabled)
                lnkPrint.OnClientClick = sPrinttbl;
            if(lnkEmail.Enabled )
                lnkEmail.OnClientClick = "return showMailDialog('Type=SalaryStructure&SalaryId=" + Convert.ToString(fvSalarystructure.DataKey["SalaryID"]) + "');";
            if(lnkDelete.Enabled)
                lnkDelete.OnClientClick = "return confirm('"+GetGlobalResourceObject("ControlsCommon","DeleteConfirmationSelectedItem")+"');";
        }
    }

    private void BindFormView(int SalaryID)
    {

        fvEmployees.Visible = true;

        objSalaryStructure.SalaryID = SalaryID;

        DataTable dt = objSalaryStructure.GetEmployeeSalaryStructure("E").Tables[0];

        if (dt.Rows.Count == 0) return;

        pgrEmployees.Visible = false;

        BindSalaryStructureDetails(dt);

        dlEmployees.DataSource = null;
        dlEmployees.DataBind();

        divPrint.InnerHtml = objUser.GetPrintText(SalaryID);

        string sPrinttbl = "Print('" + divPrint.ClientID + "')";

        if (hfMode.Value != "Edit")
        {
            lnkPrint.OnClientClick = sPrinttbl;
            lnkEmail.OnClientClick = "return showMailDialog('Type=SalaryStructure&SalaryId=" + Convert.ToString(fvSalarystructure.DataKey["SalaryID"]) + "');";
            lnkDelete.OnClientClick = "return true;";
        }

    }
    //protected void lnkAddPolicy_Click(object sender, EventArgs e)
    //{
    //    FormView2.ChangeMode(FormViewMode.Insert);
    //    controls_DeductionPolicy ucDeductionPolicy = (controls_DeductionPolicy)FormView2.FindControl("ucDeductionPolicy1");
    //    ucDeductionPolicy.IsAddition = true;
    //    //UpdatePanel upnlAdditionDeduction = (UpdatePanel)FormView1.FindControl("upnlAdditionDeduction");
    //    AjaxControlToolkit.ModalPopupExtender mpeDeductionPolicy = (AjaxControlToolkit.ModalPopupExtender)FormView2.FindControl("mpeDeductionPolicy");
    //    if (ucDeductionPolicy != null && mpeDeductionPolicy != null)
    //    {
    //        ucDeductionPolicy.DeductionPolicyId = -1;
    //        ucDeductionPolicy.Fill();
    //        mpeDeductionPolicy.Show();
    //        upDeductionPolicy.Update();

    //    }
    //}

    protected void btnSubmit_Click(object sender, EventArgs e)
    {

        if (txtNetAmount.Text.ToDecimal() < 0)
        {
           // mcMessage.InformationalMessage("Net amount cannot be negative");
            mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "NetAmountCannotBeNegative").ToString());
            mpeMessage.Show();
            return;
        }

        int id = 0;
        int iHistoryId = 0;
        try
        {
            decimal dBasicpay = 0.000M;

            objSalaryStructure = new clsSalaryStructure();
            objSalaryStructure.BeginEmp();

            objSalaryStructure.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            objSalaryStructure.PaymentClassificationID = Convert.ToInt32(ddlPaymentMode.SelectedValue);
            objSalaryStructure.PayCalculationTypeID = Convert.ToInt32(ddlPaymentCalculation.SelectedValue);

            //if (decimal.TryParse(txtBasicPay.Text, out dBasicpay)) { }

            //objSalaryStructure.BasicPay = dBasicpay;
            objSalaryStructure.CurrencyID = Convert.ToInt32(ddlCurrency.SelectedValue);
            //   objSalaryStructure.IsCurrencyForBP = Convert.ToBoolean(chkCurApplicable.Checked);
            objSalaryStructure.OTPolicyID = Convert.ToInt32(ddlOvertimePolicy.SelectedValue);
            objSalaryStructure.AbsentPolicyID = Convert.ToInt32(ddlAbsentPolicy.SelectedValue);
            objSalaryStructure.HolidayPolicyID = Convert.ToInt32(ddlHolidayPolicy.SelectedValue);
            objSalaryStructure.EncashPolicyID = Convert.ToInt32(ddlEncashPolicy.SelectedValue);
            objSalaryStructure.Remarks = Convert.ToString(txtRemarks.Text);
            objSalaryStructure.NetAmount = txtNetAmount.Text.ToDecimal();

            //objSalaryStructure.IncentiveTypeID = Convert.ToInt32(ddlIncentiveType.SelectedValue);
            //objSalaryStructure.SetPolicyID = Convert.ToInt32(ddlSettlement.SelectedValue);
            objSalaryStructure.SalaryDay = Convert.ToInt32(ddlSalaryProcessDay.SelectedValue);

            bool blnResult = true;
            foreach (DataListItem item in dlDesignationAllowances.Items)
            {
                DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");
                int intAddedID = Convert.ToInt32(ddlParticulars.SelectedValue);
                int intCategory = Convert.ToInt32(ddlCategory.SelectedValue);
                if (intAddedID == 1)
                {
                    blnResult = false;
                    if (intCategory != 1)
                    {
                        //mcMessage.InformationalMessage("For BasicPay,Category must be fixed");

                        mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "ForBasicPayCategoryMustBeFixed").ToString());
                        mpeMessage.Show();
                        return;
                    }
                }
            }
            if (blnResult == true)
            {
                // mcMessage.InformationalMessage("Please Enter BasicPay");
                mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "PleaseEnterBasicPay").ToString());
                mpeMessage.Show();
                return;
            }

            if (hfMode.Value == "Insert")
            {
                id = objSalaryStructure.InsertEmployeeSalaryHead(false);
                iHistoryId = objSalaryStructure.InsertEmployeeSalaryHead(true);
                objSalaryStructure.SalaryID = id;
                objSalaryStructure.SalaryHistoryID = iHistoryId;

                foreach (DataListItem item in dlDesignationAllowances.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                    DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");

                    objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
                    objSalaryStructure.CategoryID = Convert.ToInt32(ddlCategory.SelectedValue);
                    objSalaryStructure.Amount = txtAmount.Text.ToDecimal(); //Convert.ToDecimal((txtAmount.Text == string.Empty ? "0" : txtAmount.Text));
                    objSalaryStructure.DeductionPolicyID = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

                    objSalaryStructure.InsertEmployeeSalaryHeadDetail(false);
                    objSalaryStructure.InsertEmployeeSalaryHeadDetail(true);
                }


                foreach (DataListItem item in dlOtherRemuneration.Items)
                {
                    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
                    TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

                    objSalaryStructure.OtherRemunerationID = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
                    objSalaryStructure.RemunerationAmount = txtRemunerationAmount.Text.ToDecimal();

                    objSalaryStructure.InsertEmployeeRemuneration(false);
                }
            }
            else if (hfMode.Value == "Edit")
            {
                objSalaryStructure.SalaryID = Convert.ToInt32(hdSalaryID.Value);
                id = objSalaryStructure.UpdateEmployeeSalaryHead(false);
                objSalaryStructure.SalaryID = id;

                if (dlDesignationAllowances.Items.Count > 0)
                {
                    objSalaryStructure.DeleteSalaryHeadDetailByID();
                }
                foreach (DataListItem item in dlDesignationAllowances.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                    DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");

                    objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
                    objSalaryStructure.CategoryID = Convert.ToInt32(ddlCategory.SelectedValue);
                    objSalaryStructure.Amount = txtAmount.Text.ToDecimal(); //Convert.ToDecimal((txtAmount.Text == string.Empty ? "0" : txtAmount.Text));
                    objSalaryStructure.DeductionPolicyID = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

                    if (dlDesignationAllowances.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objSalaryStructure.InsertEmployeeSalaryHeadDetail(false);
                    }
                    else
                    {
                        objSalaryStructure.SalaryDetailID = Convert.ToInt32(dlDesignationAllowances.DataKeys[item.ItemIndex]);
                        objSalaryStructure.InsertEmployeeSalaryHeadDetail(false);
                    }

                }

                objSalaryStructure.DeleteEmployeeRemunerationByID();

                foreach (DataListItem item in dlOtherRemuneration.Items)
                {
                    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
                    TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

                    objSalaryStructure.OtherRemunerationID = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
                    objSalaryStructure.RemunerationAmount = txtRemunerationAmount.Text.ToDecimal();

                    if (dlOtherRemuneration.DataKeys[item.ItemIndex] == DBNull.Value)
                    {
                        objSalaryStructure.InsertEmployeeRemuneration(false);
                    }
                    else
                    {
                        objSalaryStructure.SalaryDetailID = Convert.ToInt32(dlOtherRemuneration.DataKeys[item.ItemIndex]);

                        objSalaryStructure.InsertEmployeeRemuneration(false);

                    }

                }
                bool bHistoryExists = objSalaryStructure.CheckEmployeeHistoryExists();
                if (bHistoryExists == true)
                {
                    objSalaryStructure.SalaryHistoryID = objSalaryStructure.GetSalaryStructureHistoryID(); ;
                    iHistoryId = objSalaryStructure.UpdateEmployeeSalaryHead(true);
                    objSalaryStructure.SalaryHistoryID = iHistoryId;

                    objSalaryStructure.DeleteEmpHistoryHeadDetail();

                    foreach (DataListItem item in dlDesignationAllowances.Items)
                    {
                        DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                        DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                        DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");

                        objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
                        objSalaryStructure.CategoryID = Convert.ToInt32(ddlCategory.SelectedValue);
                        objSalaryStructure.Amount = txtAmount.Text.ToDecimal(); //Convert.ToDecimal((txtAmount.Text == string.Empty ? "0" : txtAmount.Text));
                        objSalaryStructure.DeductionPolicyID = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

                        objSalaryStructure.InsertEmployeeSalaryHeadDetail(true);
                    }

                    //objSalaryStructure.DeleteEmployeeRemunerationByID();

                    //foreach (DataListItem item in dlOtherRemuneration.Items)
                    //{
                    //    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
                    //    TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

                    //    objSalaryStructure.AddDedID = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
                    //    objSalaryStructure.Amount = txtRemunerationAmount.Text.ToDecimal();

                    //    objSalaryStructure.InsertEmployeeRemuneration(false);
                    //}

                }
                else
                {
                    iHistoryId = objSalaryStructure.InsertEmployeeSalaryHead(true);
                    objSalaryStructure.SalaryHistoryID = iHistoryId;

                    foreach (DataListItem item in dlDesignationAllowances.Items)
                    {
                        DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                        DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                        TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                        DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");

                        objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
                        objSalaryStructure.CategoryID = Convert.ToInt32(ddlCategory.SelectedValue);
                        objSalaryStructure.Amount = txtAmount.Text.ToDecimal();
                        objSalaryStructure.DeductionPolicyID = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

                        objSalaryStructure.InsertEmployeeSalaryHeadDetail(true);
                    }
                    //foreach (DataListItem item in dlOtherRemuneration.Items)
                    //{
                    //    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
                    //    TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

                    //    objSalaryStructure.OtherRemunerationID = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
                    //    objSalaryStructure.RemunerationAmount = txtRemunerationAmount.Text.ToDecimal();

                    //    objSalaryStructure.InsertEmployeeRemuneration(false);
                    //}

                }

            }



            objSalaryStructure.Commit();
            //if (hfMode.Value == "Insert")
            //    mcMessage.InformationalMessage("Employee Salary structure has been saved successfully");
            //else
            //    mcMessage.InformationalMessage("Employee Salary structure has been updated successfully");



            if (hfMode.Value == "Insert")
                //mcMessage.InformationalMessage("Employee Salary structure has been saved successfully");

                mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "EmployeeSalaryStructureSavedSuccessfully").ToString());
            else
                //mcMessage.InformationalMessage("Employee Salary structure has been updated successfully");
                mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "EmployeeSalaryStructureSavedSuccessfully").ToString());

            mpeMessage.Show();

            EnableMenus();

            BindViewMode(id);
            upMenu.Update();
            btnList_Click(sender, e);
        }
        catch (Exception ex)
        {
            objSalaryStructure.RollBack();
        }

    }
    public void SetPermission()
    {
        objUser = new clsUserMaster();
        objRoleSettings = new clsRoleSettings();
        int RoleId;
        DataTable dt = new DataTable();
        RoleId = objUser.GetRoleId();

        if (RoleId > 3)
            dt = objRoleSettings.GetPermissions(objUser.GetRoleId(), (int)eMenuID.SalaryStructure);
        else
        {
            dt.Columns.Add("IsCreate");
            dt.Columns.Add("IsView");
            dt.Columns.Add("IsUpdate");
            dt.Columns.Add("IsDelete");
            dt.Columns.Add("IsPrintEmail");

            DataRow dr = dt.NewRow();
            dr["IsCreate"] = "True";
            dr["IsView"] = "True";
            dr["IsUpdate"] = "True";
            dr["IsDelete"] = "True";
            dr["IsPrintEmail"] = "True";
            dt.Rows.Add(dr);
        }

        ViewState["Permission"] = dt;

        if (dt.Rows.Count > 0)
        {
            lnkAddEmp.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListEmp.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            ViewState["MblnUpdatePermission"] = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (RoleId > 3)
        {
            if (lnkListEmp.Enabled)
            {
                Bind();
                ImgSearch.Enabled = true;
            }
            else if (lnkAddEmp.Enabled)
            {
                NewEmployeeSalaryStructure();
            }
            else
            {
                pgrEmployees.Visible = false;
                dlEmployees.DataSource = null;
                dlEmployees.DataBind();
                fvEmployees.Visible = false;
               // lblNoData.Text = "You dont have enough permission to view this page.";
                lblNoData.Text = GetGlobalResourceObject("ControlsCommon", "NoPermission").ToString();
                lblNoData.Visible = true;
                ImgSearch.Enabled= false;
            }
        }
    }

    public void EnableMenus()
    {
        DataTable dt = (DataTable)ViewState["Permission"];

        if (dt.Rows.Count > 0)
        {
            lnkAddEmp.Enabled = dt.Rows[0]["IsCreate"].ToBoolean();
            lnkListEmp.Enabled = dt.Rows[0]["IsView"].ToBoolean();
            lnkEmail.Enabled = lnkPrint.Enabled = dt.Rows[0]["IsPrintEmail"].ToBoolean();
            lnkDelete.Enabled = dt.Rows[0]["IsDelete"].ToBoolean();
            ViewState["MblnUpdatePermission"] = dt.Rows[0]["IsUpdate"].ToBoolean();
        }

        if (lnkDelete.Enabled)
            lnkDelete.OnClientClick = "return valDeleteDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkDelete.OnClientClick = "return false;";

        if (lnkPrint.Enabled)
            lnkPrint.OnClientClick = "return valPrintEmailDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkPrint.OnClientClick = "return false;";

        if (lnkEmail.Enabled)
            lnkEmail.OnClientClick = "return valPrintEmailDatalist('" + dlEmployees.ClientID + "');";
        else
            lnkEmail.OnClientClick = "return false;";
    }

    private void BindSalaryStructureDetails(DataTable dt)
    {
        upMenu.Update();

        if (dt.Rows.Count > 0)
        {
            ddlCompany.SelectedIndex = ddlCompany.Items.IndexOf(ddlCompany.Items.FindByValue(Convert.ToString(dt.Rows[0]["CompanyID"])));
            objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            ddlEmployee.DataSource = objSalaryStructure.FillEmployees();
            ddlEmployee.DataBind();

            ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(dt.Rows[0]["EmployeeID"])));

            objSalaryStructure.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            ddlCurrency.DataSource = objSalaryStructure.FillEmpCurrencies();
            ddlCurrency.DataBind();

            ddlPaymentMode.SelectedIndex = ddlPaymentMode.Items.IndexOf(ddlPaymentMode.Items.FindByValue(Convert.ToString(dt.Rows[0]["PaymentClassificationID"])));
            ddlSalaryProcessDay.SelectedIndex = ddlSalaryProcessDay.Items.IndexOf(ddlSalaryProcessDay.Items.FindByValue(Convert.ToString(dt.Rows[0]["SalaryDay"])));
            if (Convert.ToInt32(ddlPaymentMode.SelectedValue) == 4)
            {
                ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculation();
                ddlPaymentCalculation.DataBind();
            }
            else
            {
                ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculationOther();
                ddlPaymentCalculation.DataBind();
            }


            ddlPaymentCalculation.SelectedIndex = ddlPaymentCalculation.Items.IndexOf(ddlPaymentCalculation.Items.FindByValue(Convert.ToString(dt.Rows[0]["PayCalculationTypeID"])));
            //txtBasicPay.Text = Convert.ToString(dt.Rows[0]["BasicPay"]);

            //dBasicPay = Convert.ToDecimal(dt.Rows[0]["BasicPay"].ToString());
            //txtBasicPay.Text = string.Format("{0:#,###.00}", dBasicPay);

            // chkCurApplicable.Checked = Convert.ToBoolean(dt.Rows[0]["IsCurrencyForBP"]);
            ddlCurrency.SelectedIndex = ddlCurrency.Items.IndexOf(ddlCurrency.Items.FindByValue(Convert.ToString(dt.Rows[0]["CurrencyId"])));

            ddlAbsentPolicy.SelectedIndex = ddlAbsentPolicy.Items.IndexOf(ddlAbsentPolicy.Items.FindByValue(Convert.ToString(dt.Rows[0]["AbsentPolicyId"])));
            ddlEncashPolicy.SelectedIndex = ddlEncashPolicy.Items.IndexOf(ddlEncashPolicy.Items.FindByValue(Convert.ToString(dt.Rows[0]["EncashPolicyId"])));
            ddlHolidayPolicy.SelectedIndex = ddlHolidayPolicy.Items.IndexOf(ddlHolidayPolicy.Items.FindByValue(Convert.ToString(dt.Rows[0]["HolidayPolicyId"])));
            ddlOvertimePolicy.SelectedIndex = ddlOvertimePolicy.Items.IndexOf(ddlOvertimePolicy.Items.FindByValue(Convert.ToString(dt.Rows[0]["OTPolicyId"])));

            txtRemarks.Text = Convert.ToString(dt.Rows[0]["Remarks"]);

            ddlCompany.Enabled = false;
            ddlEmployee.Enabled = false;

            objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
            //ddlIncentiveType.DataSource = objSalaryStructure.FillCompanyIncentivePolicies();
            //ddlIncentiveType.DataBind();
            //ddlIncentiveType.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));



            //ddlIncentiveType.SelectedIndex = ddlIncentiveType.Items.IndexOf(ddlIncentiveType.Items.FindByValue(Convert.ToString(dt.Rows[0]["IncentiveTypeID"])));

            //ddlSettlement.DataSource = objSalaryStructure.FillSettlementPolicies();
            //ddlSettlement.DataBind();
            //ddlSettlement.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

            //ddlSettlement.SelectedIndex = ddlSettlement.Items.IndexOf(ddlSettlement.Items.FindByValue(Convert.ToString(dt.Rows[0]["SetPolicyID"])));

            objSalaryStructure.SalaryID = Convert.ToInt32(dt.Rows[0]["SalaryID"]);
            int intSalaryID = Convert.ToInt32(dt.Rows[0]["SalaryID"]);
            dlDesignationAllowances.DataSource = objSalaryStructure.GetEmployeeAllowanceDeductionDetails("E");
            dlDesignationAllowances.DataBind();

            foreach (DataListItem item in dlDesignationAllowances.Items)
            {
                TextBox txtAmount = (TextBox)item.FindControl("txtAmount");
                if (txtAmount == null) return;
                ScriptManager.RegisterClientScriptBlock(txtAmount, txtAmount.GetType(), "Calculate", "CalculateSalaryStructure('" + txtAmount.ClientID + "')", true);
            }


            dlOtherRemuneration.DataSource = objSalaryStructure.GetEmployeeRemunerationDetails(intSalaryID);
            dlOtherRemuneration.DataBind();
            foreach (DataListItem item in dlOtherRemuneration.Items)
            {
                TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");
                if (txtRemunerationAmount == null) return;
            }
          
        }

    }

    protected void ddlPaymentMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDeductionPolicy();
        objSalaryStructure = new clsSalaryStructure();

        if (Convert.ToInt32(ddlPaymentMode.SelectedValue) == 4)
        {
            ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculation();
            ddlPaymentCalculation.DataBind();
        }
        else
        {
            ddlPaymentCalculation.DataSource = objSalaryStructure.FillPaymentCalculationOther();
            ddlPaymentCalculation.DataBind();
        }

    }
    private void BindDeductionPolicy()
    {
        objSalaryStructure = new clsSalaryStructure();

        objSalaryStructure.PaymentClassificationID = Convert.ToInt32(ddlPaymentMode.SelectedValue);
        objSalaryStructure.PayCalculationTypeID = Convert.ToInt32(ddlPaymentCalculation.SelectedValue);

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");

            ddlDeductionPolicy.DataSource = objSalaryStructure.FillDeductionPolicy();
            ddlDeductionPolicy.DataBind();

            ddlDeductionPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));
        }

        updlDesignationAllowance.Update();
    }

    protected void btnAllowances_Click(object sender, EventArgs e)
    {
        ViewState["Allowances"] = null;

        UpdateDataList();
        NewAllowancesAdd();

    }
    private void UpdateDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("SalaryDetailID");
        dt.Columns.Add("AddDedID");
        dt.Columns.Add("CategoryID");
        dt.Columns.Add("Amount");
        dt.Columns.Add("DeductionPolicyID");

        DataRow dw;

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
            DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

            dw = dt.NewRow();

            dw["SalaryDetailID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
            dw["AddDedID"] = Convert.ToInt32(ddlParticulars.SelectedValue);
            dw["CategoryID"] = Convert.ToInt32(ddlCategory.SelectedValue);
            dw["Amount"] = Convert.ToDecimal(txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text);
            dw["DeductionPolicyID"] = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

            dt.Rows.Add(dw);
        }
        ViewState["Allowances"] = dt;
    }
    private void NewAllowancesAdd()
    {
        DataTable dt = (DataTable)ViewState["Allowances"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("SalaryDetailID");
            dt.Columns.Add("AddDedID");
            dt.Columns.Add("CategoryID");
            dt.Columns.Add("Amount");
            dt.Columns.Add("DeductionPolicyID");
        }
        DataRow dw = dt.NewRow();
        dt.Rows.Add(dw);

        dlDesignationAllowances.DataSource = dt;
        dlDesignationAllowances.DataBind();
    }
    protected void dlDesignationAllowances_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();
        switch (e.CommandName)
        {
            case "REMOVE_ALLOWANCE":


                DataTable dt = new DataTable();

                dt.Columns.Add("SalaryDetailID");
                dt.Columns.Add("AddDedID");
                dt.Columns.Add("CategoryID");
                dt.Columns.Add("Amount");
                dt.Columns.Add("DeductionPolicyID");

                DataRow dw;

                foreach (DataListItem item in dlDesignationAllowances.Items)
                {
                    DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
                    DropDownList ddlCategory = (DropDownList)item.FindControl("ddlCategory");
                    DropDownList ddlDeductionPolicy = (DropDownList)item.FindControl("ddlDeductionPolicy");
                    TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

                    dw = dt.NewRow();

                    dw["SalaryDetailID"] = dlDesignationAllowances.DataKeys[item.ItemIndex];
                    dw["AddDedID"] = Convert.ToInt32(ddlParticulars.SelectedValue);
                    dw["CategoryID"] = Convert.ToInt32(ddlCategory.SelectedValue);
                    dw["Amount"] = Convert.ToDecimal(txtAmount.Text == string.Empty ? "0.000" : txtAmount.Text);
                    dw["DeductionPolicyID"] = Convert.ToInt32(ddlDeductionPolicy.SelectedValue);

                    if (item != e.Item)
                    {
                        dt.Rows.Add(dw);
                    }

                    if (e.CommandArgument.ToInt32() == 1)
                    {
                       // mcMessage.InformationalMessage("Basic pay is essential thing in employee salary structure.");

                        mcMessage.InformationalMessage(GetGlobalResourceObject("ControlsCommon", "BasicPayEssential").ToString());
                        mpeMessage.Show();

                        bool IsExists = false;

                        foreach (DataRow row in dt.Rows)
                        {                            
                            if (dw["AddDedID"].ToInt32() == row["AddDedID"].ToInt32())
                            {
                                IsExists = true;
                                break;
                            }
                        }

                        if (!IsExists)
                            dt.Rows.Add(dw);
                    }
                    else
                    {
                        if (e.CommandArgument != string.Empty)
                        {
                            objSalaryStructure.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
                            objSalaryStructure.AddDedID = Convert.ToInt32(e.CommandArgument);

                            objSalaryStructure.RemoveEmpSalaryAllowanceOrDeduction(false);
                            objSalaryStructure.RemoveEmpSalaryAllowanceOrDeduction(true);
                        }
                    }

                    ViewState["Allowances"] = dt;
                }
                dlDesignationAllowances.DataSource = ViewState["Allowances"];
                dlDesignationAllowances.DataBind();
                CalculateNetAmount();

                break;
        }
    }

    protected void dlDesignationAllowances_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();

        DropDownList ddlParticulars = (DropDownList)e.Item.FindControl("ddlParticulars");
        DropDownList ddlCategory = (DropDownList)e.Item.FindControl("ddlCategory");
        DropDownList ddlDeductionPolicy = (DropDownList)e.Item.FindControl("ddlDeductionPolicy");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");
        HiddenField hdDeductionPolicy = (HiddenField)e.Item.FindControl("hdDeductionPolicy");
        HiddenField hdParticulars = (HiddenField)e.Item.FindControl("hdParticulars");
        HiddenField hdCategory = (HiddenField)e.Item.FindControl("hdCategory");

        ddlParticulars.DataSource = objSalaryStructure.FillParticulars();
        ddlParticulars.DataBind();
        ddlParticulars.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        ddlCategory.DataSource = objSalaryStructure.FillCategory();
        ddlCategory.DataBind();

        objSalaryStructure.PaymentClassificationID = Convert.ToInt32(ddlPaymentMode.SelectedValue);
        objSalaryStructure.PayCalculationTypeID = Convert.ToInt32(ddlPaymentCalculation.SelectedValue);
        if(hdParticulars.Value.ToInt64() > 0)
        objSalaryStructure.AddDedID = Convert.ToInt32(hdParticulars.Value);
        ddlDeductionPolicy.DataSource = objSalaryStructure.FillDeductionPolicy();
        ddlDeductionPolicy.DataBind();
        ddlDeductionPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);

        if (Convert.ToString(hdParticulars.Value) != "")
        {
            ddlParticulars.SelectedIndex = ddlParticulars.Items.IndexOf(ddlParticulars.Items.FindByValue(Convert.ToString(hdParticulars.Value)));
            //ddlParticulars.SelectedValue = hdParticulars.Value;
            objSalaryStructure.AddDedID = Convert.ToInt32(hdParticulars.Value);

            string iPolicyId = objSalaryStructure.GetDeductionPolicy();
            if (iPolicyId != "")           //deduction policy exists
            {
                txtAmount.CssClass = "textbox_disabled";
                txtAmount.Enabled = false;
                txtAmount.Text = string.Empty;
                //ddlDeductionPolicy.CssClass = "dropdownlist_disabled";
                //ddlDeductionPolicy.Enabled = false;
            }
            else
            {
                txtAmount.CssClass = "textbox";
                txtAmount.Enabled = true;
                ddlDeductionPolicy.CssClass = "dropdownlist";
                ddlDeductionPolicy.Enabled = true;
            }

            bool bAddition = objSalaryStructure.GetAdditionorDeduction();
            if (!bAddition)
            {
                ddlDeductionPolicy.CssClass = "dropdownlist";
                ddlDeductionPolicy.Enabled = true;
            }
            else
            {
                ddlDeductionPolicy.SelectedIndex = 0;
                //ddlDeductionPolicy.CssClass = "dropdownlist_disabled";
                //ddlDeductionPolicy.Enabled = false;
            }
        }

        if (Convert.ToString(hdCategory.Value) != "")
        {
            ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(Convert.ToString(hdCategory.Value)));

        }

        if (Convert.ToString(hdDeductionPolicy.Value) != "")
            ddlDeductionPolicy.SelectedIndex = ddlDeductionPolicy.Items.IndexOf(ddlDeductionPolicy.Items.FindByValue(Convert.ToString(hdDeductionPolicy.Value)));


    }

    private void CalculateNetAmount()
    {
        objSalaryStructure = new clsSalaryStructure();

        bool bAddDedID = true;
        decimal dNetAmt = 0.000M;
        //if (txtBasicPay.Text == "" || txtBasicPay.Text == string.Empty)
        //    txtBasicPay.Text = "0";


        //dNetAmt = txtBasicPay.Text.ToDecimal();

        foreach (DataListItem item in dlDesignationAllowances.Items)
        {
            DropDownList ddlParticulars = (DropDownList)item.FindControl("ddlParticulars");
            TextBox txtAmount = (TextBox)item.FindControl("txtAmount");

            objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
            bAddDedID = objSalaryStructure.GetAdditionorDeduction();
            if (bAddDedID == true && txtAmount.Text != "")
            {
                dNetAmt = Convert.ToDecimal(dNetAmt) + Convert.ToDecimal(txtAmount.Text);

            }
            else if (bAddDedID == false && txtAmount.Text != "")
            {
                dNetAmt = Convert.ToDecimal(dNetAmt) - Convert.ToDecimal(txtAmount.Text);
            }

            if (txtAmount == null) return;
            ScriptManager.RegisterClientScriptBlock(txtAmount, txtAmount.GetType(), "Calculate", "CalculateSalaryStructure('" + txtAmount.ClientID + "')", true);
        }


        txtNetAmount.Text = string.Format("{0:#,###.000}", dNetAmt);

    }
   
   

    protected void EpEmployee_Update()
    {

        objSalaryStructure = new clsSalaryStructure();

        ddlEncashPolicy.DataSource = objSalaryStructure.FillEncashPolicies();
        ddlEncashPolicy.DataBind();
        ddlEncashPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //ddlEncashPolicy.SelectedIndex = ddlEncashPolicy.Items.IndexOf(ddlEncashPolicy.Items.FindByValue(EpEmployee.EncashPolicyID.ToString()));

        if (upEncashPolicy != null)
            upEncashPolicy.Update();

    }
    protected void HpEmployee_Update()
    {

        objSalaryStructure = new clsSalaryStructure();

        ddlHolidayPolicy.DataSource = objSalaryStructure.FillHolidayPolicies();
        ddlHolidayPolicy.DataBind();
        ddlHolidayPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //ddlHolidayPolicy.SelectedIndex = ddlHolidayPolicy.Items.IndexOf(ddlHolidayPolicy.Items.FindByValue(HpEmployee.HolidayPolicyID.ToString()));

        if (upHolidayPolicy != null)
            upHolidayPolicy.Update();

    }
    protected void OpEmployee_Update()
    {

        objSalaryStructure = new clsSalaryStructure();

        ddlOvertimePolicy.DataSource = objSalaryStructure.FillOvertimePolicies();
        ddlOvertimePolicy.DataBind();
        ddlOvertimePolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        //ddlOvertimePolicy.SelectedIndex = ddlOvertimePolicy.Items.IndexOf(ddlOvertimePolicy.Items.FindByValue(OpEmployee.OTPolicyID.ToString()));

        if (upOvertimePolicy != null)
            upOvertimePolicy.Update();

    }
    protected void ddlDeductionPolicy_SelectedIndexChanged(object sender, EventArgs e)
    {
        //DropDownList ddlDeductionPolicy = (DropDownList)sender;
        //TextBox txtAmount = (TextBox)ddlDeductionPolicy.Parent.FindControl("txtAmount");
        //txtAmount.Text = string.Empty;
        CalculateNetAmount();

    }
    protected void ddlPaymentCalculation_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDeductionPolicy();
    }
    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        int ScaleID = 0;
        decimal GrossAmount = 0.00M;
        int IntScale = 2;
        if (ddlEmployee.Items.Count > 0)
        {
            objSalaryStructure = new clsSalaryStructure();

            objSalaryStructure.EmployeeID = Convert.ToInt32(ddlEmployee.SelectedValue);
            //ddlCurrency.DataSource = objSalaryStructure.FillEmpCurrencies();
            //ddlCurrency.DataBind();

            //DataSet ds = objSalaryStructure.GetEmployeeSalaryScaleMaster();
            //if (ds.Tables[0].Rows.Count > 0)
            //{
            //    ScaleID = ds.Tables[0].Rows[0]["ScaleID"].ToInt32();
            //    GrossAmount = ds.Tables[0].Rows[0]["GrossSalary"].ToDecimal();

            //    objSalaryStructure.ScaleID = ScaleID;
            //    DataTable dtDetails = objSalaryStructure.GetEmployeeSalaryScaleDetails();
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add("SalaryDetailID");
            //    dt.Columns.Add("AddDedID");
            //    dt.Columns.Add("Amount");
            //    dt.Columns.Add("DeductionPolicyID");
            //    DataRow dw;

            //    //if (dtDetails.Rows.Count > 0)
            //    //{
            //    //    txtBasicPay.Text = "0.00";
            //    //    if (dtDetails.Rows[0]["RateOnly"] != DBNull.Value)
            //    //    {
            //    //        if (Convert.ToBoolean(dtDetails.Rows[0]["RateOnly"]) == true)
            //    //            txtBasicPay.Text = Convert.ToString(dtDetails.Rows[0]["EmployerAmount"]);
            //    //        else
            //    //            txtBasicPay.Text = Math.Round((GrossAmount * (Convert.ToDecimal(dtDetails.Rows[0]["EmployerAmount"]) / 100)), IntScale).ToString();
            //    //    }
            //    //}

            //    if (dtDetails.Rows.Count > 0)
            //    {
            //        for (int i = 0; i < dtDetails.Rows.Count; i++)
            //        {
            //            if (i == 0)
            //            {
            //                txtBasicPay.Text = "0.00";
            //                if (dtDetails.Rows[i]["RateOnly"] != DBNull.Value)
            //                {
            //                    if (Convert.ToBoolean(dtDetails.Rows[i]["RateOnly"]) == true)
            //                        txtBasicPay.Text = Convert.ToString(dtDetails.Rows[i]["EmployerAmount"]);
            //                    else
            //                        txtBasicPay.Text = Math.Round((GrossAmount * (Convert.ToDecimal(dtDetails.Rows[i]["EmployerAmount"]) / 100)), IntScale).ToString();
            //                }
            //            }
            //            else
            //            {
            //                dw = dt.NewRow();
            //                dw["AddDedID"] = Convert.ToInt32(dtDetails.Rows[i]["AddDedID"]);
            //                if (dtDetails.Rows[i]["RateOnly"] != DBNull.Value)
            //                {
            //                    if (Convert.ToBoolean(dtDetails.Rows[i]["IsAddition"]) == true)
            //                    {
            //                        if (Convert.ToBoolean(dtDetails.Rows[i]["RateOnly"]) == true)
            //                            dw["Amount"] = dtDetails.Rows[i]["EmployerAmount"].ToString();
            //                        else
            //                        {
            //                            int DeductionPolicyID = Convert.ToInt32(dtDetails.Rows[i]["DeductionPolicyID"]);
            //                            int intAddDedID = (objSalaryStructure.GetAdditionID());
            //                            if (intAddDedID == 0)
            //                                intAddDedID = 1;
            //                            if (intAddDedID == 1)
            //                                dw["Amount"] = Math.Round((Convert.ToDecimal(txtBasicPay.Text) * (Convert.ToDecimal(dtDetails.Rows[i]["EmployerAmount"]) / 100)), IntScale).ToString();
            //                            else
            //                                dw["Amount"] = Math.Round((GrossAmount * (Convert.ToDecimal(dtDetails.Rows[i]["EmployerAmount"]) / 100)), IntScale).ToString();
            //                        }
            //                    }
            //                    else
            //                        dw["DeductionPolicyID"] = Convert.ToInt32(dtDetails.Rows[i]["DeductionPolicyID"]);
            //                }
            //                dt.Rows.Add(dw);
            //            }
            //        }
            //    }
            //    dlDesignationAllowances.DataSource = dt;
            //    dlDesignationAllowances.DataBind();

            //}

           //ScriptManager.RegisterClientScriptBlock(txtBasicPay, txtBasicPay.GetType(), "Calculate", "CalculateNetAmount('" + txtBasicPay.ClientID + "')", true);
        }
    }

    protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();

        objSalaryStructure.CompanyID = Convert.ToInt32(ddlCompany.SelectedValue);
        ddlEmployee.DataSource = objSalaryStructure.FillFilterEmployees();
        ddlEmployee.DataBind();
        ddlCurrency.DataSource = objSalaryStructure.FillEmpCurrencies();
        ddlCurrency.DataBind();

        //if (ddlEmployee.Items.Count == 0)
        ddlEmployee.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "Select", "-1"));

        int value = objSalaryStructure.GetSalaryDay();
        ddlSalaryProcessDay.SelectedIndex = ddlSalaryProcessDay.Items.IndexOf(ddlSalaryProcessDay.Items.FindByValue(Convert.ToString(value)));
        //ddlIncentiveType.DataSource = objSalaryStructure.FillCompanyIncentivePolicies();
        //ddlIncentiveType.DataBind();
        //ddlIncentiveType.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "Select", "-1"));

    }
    protected void ucAdditionDeduction_Update()
    {
        objSalaryStructure = new clsSalaryStructure();

        if (dlDesignationAllowances != null)
        {
            ViewState["Allowances"] = null;

            UpdateDataList();

            DataTable dt = (DataTable)ViewState["Allowances"];

            dlDesignationAllowances.DataSource = dt;
            dlDesignationAllowances.DataBind();

            //if (txtBasicPay.Text != string.Empty)
            //{
                CalculateNetAmount();
            //}
        }
        updlDesignationAllowance.Update();
    }

    protected void ucDeductionPolicy_Update()
    {

        BindDeductionPolicy();

        objSalaryStructure = new clsSalaryStructure();

        if (dlDesignationAllowances != null)
        {
            ViewState["Allowances"] = null;

            UpdateDataList();

            DataTable dt = (DataTable)ViewState["Allowances"];

            dlDesignationAllowances.DataSource = dt;
            dlDesignationAllowances.DataBind();
        }

    }
    protected void ddlParticulars_SelectedIndexChanged(object sender, EventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();
        DropDownList ddlParticulars = (DropDownList)sender;
        TextBox txtAmount = (TextBox)ddlParticulars.Parent.FindControl("txtAmount");
        DropDownList ddlDeductionPolicy = (DropDownList)ddlParticulars.Parent.FindControl("ddlDeductionPolicy");

        objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
        string iPolicyId = objSalaryStructure.GetDeductionPolicy();
        if (iPolicyId != "")           //deduction policy exists
        {
            txtAmount.CssClass = "textbox_disabled";
            txtAmount.Enabled = ddlDeductionPolicy.Enabled = false;
            //txtAmount.Text = string.Empty;
            ddlDeductionPolicy.CssClass = "dropdownlist_disabled";
        }
        else
        {
            txtAmount.CssClass = "textbox";
            txtAmount.Enabled = true;
            ddlDeductionPolicy.CssClass = "dropdownlist";
            ddlDeductionPolicy.Enabled = true;
        }
        objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
        bool bAddition = objSalaryStructure.GetAdditionorDeduction();
        if (bAddition == false)         //   only for addition payment policy can be selected
        {
            txtAmount.Enabled = true;
            txtAmount.CssClass = "textbox";
            ddlDeductionPolicy.CssClass = "dropdownlist";
            ddlDeductionPolicy.Enabled = true;
            objSalaryStructure.AddDedID = Convert.ToInt32(ddlParticulars.SelectedValue);
            ddlDeductionPolicy.DataSource = objSalaryStructure.GetDeductionPolicies();
            ddlDeductionPolicy.DataBind();
            ddlDeductionPolicy.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        }
        else
        {
            ddlDeductionPolicy.SelectedIndex = 0;
            ddlDeductionPolicy.CssClass = "dropdownlist_disabled";
            ddlDeductionPolicy.Enabled = false;
        }
    }

 

    //protected void btnAbsentPolicyReference_Click1(object sender, EventArgs e)
    //{
    //    ApEmployee.AbsentPolicyID = Convert.ToInt32(ddlAbsentPolicy.SelectedValue);
    //    ApEmployee.Fill();
    //    mpeAbsentPolicy.Show();
    //}
    //protected void btnEncashPolicyReference_Click(object sender, EventArgs e)
    //{
    //    EpEmployee.EncashPolicyID = Convert.ToInt32(ddlEncashPolicy.SelectedValue);
    //    EpEmployee.Fill();
    //    mpeEncashPolicy.Show();
    //}
    //protected void btnHolidayPolicyReference_Click(object sender, EventArgs e)
    //{
    //    HpEmployee.HolidayPolicyID = Convert.ToInt32(ddlHolidayPolicy.SelectedValue);
    //    HpEmployee.Fill();
    //    mpeHolidayPolicy.Show();
    //}
    //protected void btnOvertimePolicyReference_Click(object sender, EventArgs e)
    //{
    //    OpEmployee.OTPolicyID = Convert.ToInt32(ddlOvertimePolicy.SelectedValue);
    //    OpEmployee.Fill();
    //    mpeOvertimePolicy.Show();
    //}
    protected void dlOtherRemuneration_ItemCommand(object source, DataListCommandEventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();
        switch (e.CommandName)
        {
            case "REMOVE_Remuneration":


                DataTable dt = new DataTable();

                dt.Columns.Add("SalaryID");
                dt.Columns.Add("OtherRemunerationID");
                dt.Columns.Add("Amount");

                DataRow dw;

                foreach (DataListItem item in dlOtherRemuneration.Items)
                {
                    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
                    TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

                    dw = dt.NewRow();

                    dw["SalaryID"] = dlOtherRemuneration.DataKeys[item.ItemIndex];
                    dw["OtherRemunerationID"] = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
                    dw["Amount"] = Convert.ToDecimal(txtRemunerationAmount.Text == string.Empty ? "0.000" : txtRemunerationAmount.Text);

                    if (item != e.Item)
                    {
                        dt.Rows.Add(dw);
                    }
                    if (e.CommandArgument != string.Empty)
                    {
                        objSalaryStructure.EmployeeID = ddlEmployee.SelectedValue.ToInt32();
                        objSalaryStructure.OtherRemunerationID = Convert.ToInt32(e.CommandArgument);

                        objSalaryStructure.RemoveEmpRemuneration(false);
                        objSalaryStructure.RemoveEmpRemuneration(true);
                    }

                    ViewState["Remuneration"] = dt;
                }
                dlOtherRemuneration.DataSource = ViewState["Remuneration"];
                dlOtherRemuneration.DataBind();

                break;
        }
    }
    protected void dlOtherRemuneration_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        objSalaryStructure = new clsSalaryStructure();

        DropDownList ddlRemunerationParticulars = (DropDownList)e.Item.FindControl("ddlRemunerationParticulars");
        TextBox txtAmount = (TextBox)e.Item.FindControl("txtRemunerationAmount");
        HiddenField hdRemunerationParticulars = (HiddenField)e.Item.FindControl("hdRemunerationParticulars");

        ddlRemunerationParticulars.DataSource = objSalaryStructure.FillRemunerationParticulars();
        ddlRemunerationParticulars.DataBind();
        ddlRemunerationParticulars.Items.Insert(0, new ListItem(clsGlobalization.IsArabicCulture() ? "اختر" : "--Select--", "-1"));

        if (hdRemunerationParticulars.Value.ToInt64() > 0)
            objSalaryStructure.OtherRemunerationID = Convert.ToInt32(hdRemunerationParticulars.Value);

        objSalaryStructure.OtherRemunerationID = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);

        if (Convert.ToString(hdRemunerationParticulars.Value) != "")
        {
            ddlRemunerationParticulars.SelectedIndex = ddlRemunerationParticulars.Items.IndexOf(ddlRemunerationParticulars.Items.FindByValue(Convert.ToString(hdRemunerationParticulars.Value)));
            //ddlParticulars.SelectedValue = hdParticulars.Value;
            objSalaryStructure.OtherRemunerationID = Convert.ToInt32(hdRemunerationParticulars.Value);
        }

    }
    protected void btnOtherRemuneration_Click(object sender, EventArgs e)
    {
        ViewState["Remuneration"] = null;

        UpdateRemunerationDataList();
        NewRemunerationAdd();


        ScriptManager.RegisterClientScriptBlock(this, typeof(string), "setPolicyTab", "SetSalaryTab('pnlpTab2')", true);
        //Panel pnlpTab2 = (Panel)pnlpTab2.ite
       // SetTab(pnlpTab2);
    }

    private void UpdateRemunerationDataList()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("SalaryID");
        dt.Columns.Add("OtherRemunerationID");
        dt.Columns.Add("Amount");

        DataRow dw;

        foreach (DataListItem item in dlOtherRemuneration.Items)
        {
            DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
            TextBox txtRemunerationAmount = (TextBox)item.FindControl("txtRemunerationAmount");

            dw = dt.NewRow();

            dw["SalaryID"] = dlOtherRemuneration.DataKeys[item.ItemIndex];
            dw["OtherRemunerationID"] = Convert.ToInt32(ddlRemunerationParticulars.SelectedValue);
            dw["Amount"] = Convert.ToDecimal(txtRemunerationAmount.Text == string.Empty ? "0.000" : txtRemunerationAmount.Text);

            dt.Rows.Add(dw);
        }
        ViewState["Remuneration"] = dt;
    }
    private void NewRemunerationAdd()
    {
        DataTable dt = (DataTable)ViewState["Remuneration"];

        if (dt == null)
        {
            dt = new DataTable();

            dt.Columns.Add("SalaryID");
            dt.Columns.Add("OtherRemunerationID");
            dt.Columns.Add("Amount");
        }
        DataRow dw = dt.NewRow();
        dt.Rows.Add(dw);

        dlOtherRemuneration.DataSource = dt;
        dlOtherRemuneration.DataBind();
        //this.SetTab();
    }

    /// <summary>
    /// Function for selecting the Tab
    /// </summary>
    /// <param name="TabIndex"></param>
    private void SetTab(object sender)
    {

        HtmlGenericControl control = (HtmlGenericControl)sender;

        string script = "var t = setTimeout(\"setTaskTab('" + control.ClientID + "')\", 1000);";
        ScriptManager.RegisterClientScriptBlock(this.updlOtherRemuneration, this.updlOtherRemuneration.GetType(), Guid.NewGuid().ToString(), script, true);
    }
    protected void lnkRemuneration_Click(object sender, EventArgs e)
    {
        //foreach (DataListItem item in dlOtherRemuneration.Items)
        //{
        //    DropDownList ddlRemunerationParticulars = (DropDownList)item.FindControl("ddlRemunerationParticulars");
        ////UpdatePanel updLoanType = (UpdatePanel)fvLoanDetails.FindControl("updLoanType");
        //if (ddlRemunerationParticulars != null )
        //{
            ReferenceControlNew1.ClearAll();
            ReferenceControlNew1.TableName = "OtherRemunerationReference";
            ReferenceControlNew1.DataTextField = "OtherRemuneration";
            ReferenceControlNew1.DataValueField = "OtherRemunerationID";
            ReferenceControlNew1.FunctionName = "FillRemunerationParticulars";
            //ReferenceControlNew1.SelectedValue = ddlRemunerationParticulars.SelectedValue;
            ReferenceControlNew1.DisplayName = clsUserMaster.GetCulture() == "ar-AE" ? ("بمرتبات أخرى") : ("Other Remuneration"); 
            ReferenceControlNew1.DataTextFieldArabic = "OtherRemunerationArb";
            //ReferenceControlNew1.PredefinedField = "Predefined";
            //ReferenceControlNew1.FilterColumn = "OPerationModeId";
            //ReferenceControlNew1.FilterId = 9;
            ReferenceControlNew1.PopulateData();
            mdlPopUpReference.Show();
            updModalPopUp.Update();
        //}
       
            
        //}
    }

    protected void dlEmployees_ItemBound(object sender, DataListItemEventArgs e)
    {
        ImageButton btnEdit = (ImageButton)e.Item.FindControl("btnEdit");
        if (btnEdit != null)
        {
            btnEdit.Enabled = ViewState["MblnUpdatePermission"] == null ? true : ((bool)ViewState["MblnUpdatePermission"]);
        }

    }

}
